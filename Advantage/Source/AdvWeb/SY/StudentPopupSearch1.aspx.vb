' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' StudentPopupSearch.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================

Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports BO = Advantage.Business.Objects

Partial Class StudentPopupSearch1
    Inherits System.Web.UI.Page
    Private pObj As New UserPagePermissionInfo
    Private CampusId As String
    Private campusDesc As String = String.Empty
    Private resourceId As Integer
    Private MaxFieldLengths(5) As Integer
    ' DE7569 5/1/2012 Janet Robinson
    Dim userId As String = String.Empty
   


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblStudentID As System.Web.UI.WebControls.Label
    Protected WithEvents txtStudentID As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents btnNew As System.Web.UI.WebControls.Button
    Protected WithEvents btnDelete As System.Web.UI.WebControls.Button
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents StudentLNameLinkButton As System.Web.UI.WebControls.LinkButton


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        'Put user code to initialize the page here
        'Dim userId As String
        'Dim m_Context As HttpContext
        'Dim fac As New UserSecurityFacade

        
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        ' resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'campusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        ' DE7569 5/1/2012 Janet Robinson
        userId = AdvantageSession.UserState.UserId.ToString

        resourceId = CInt(Request.QueryString("resid"))
        CampusId = Request.QueryString("cmpid")
        'userId = Request.QueryString("userid")

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, CampusId)

        If Not Page.IsPostBack Then
            '   build dropdownlists
            BuildDropdownlists()
            BindDataListMRU()
            If campusDesc = String.Empty Then
                lbHdr.Text = "Recently Accessed Students"
            Else
                lbHdr.Text = "Recently Accessed Students from " & campusDesc
            End If
        End If

    End Sub
    Private Sub BuildDropdownlists()
        BuildStatusDDL()
        'BuildCampusDDL()
        BuildPrgVerDDL()
        BuildTermsDDL()
        BuildAcademicYearsDDL()
    End Sub
    Private Sub BuildStatusDDL()
        'Bind the Status DropDownList
        Dim ddlStatusId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStatusId"), DropDownList)
        Dim statuses As New StatusCodeFacade
        With ddlStatusId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataSource = statuses.GetAllStatusCodesForStudents(CampusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
        'With ddlStatusId
        '    .DataTextField = "Status"
        '    .DataValueField = "StatusId"
        '    .DataSource = (New StatusesFacade).GetAllStatuses()
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        '    .SelectedIndex = 0
        'End With
    End Sub
    Private Sub BuildPrgVerDDL()
        'Bind the Programs DrowDownList
        Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStudentPrgVerId"), DropDownList)
        With ddlStudentPrgVerId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = (New StudentSearchFacade).GetAllEnrollmentsUsedByStudents()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildTermsDDL()
        '   bind the Terms DDL
        Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
        Dim terms As New StudentsAccountsFacade
        With ddlTermId
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = terms.GetAllTerms(True)
            .DataBind()
            '.Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildTermsDDL(ByVal stuEnrollId As String, ByVal campusid As String)
        '   bind the Terms DDL
        Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
        Dim terms As New StudentSearchFacade

        With ddlTermId
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = terms.getAllStudentTerm(stuEnrollId, campusid)
            .SelectedIndex = -1
            .DataBind()
            '.Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
            If .Items.Count > 0 Then .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildAcademicYearsDDL()
        '   bind the AcademicYearss DDL
        Dim ddlAcademicYearId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlAcademicYearId"), DropDownList)
        Dim academicYears As New StudentsAccountsFacade
        With ddlAcademicYearId
            .DataTextField = "AcademicYearDescrip"
            .DataValueField = "AcademicYearId"
            .DataSource = academicYears.GetAllAcademicYears()
            .DataBind()
            .SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYearId.Items)
        End With
    End Sub
    Private Sub BindDataList()
        ''   Bind datalist
        'dgrdTransactionSearch.DataSource = (New StudentSearchFacade).GetStudentSearchDS(txtLastName.Text, txtFirstName.Text, txtSSN.Text, txtEnrollment.Text, ddlStatusId.SelectedValue, ddlStudentPrgVerId.SelectedValue, ddlCampGrpID.SelectedValue)
        'dgrdTransactionSearch.DataBind()

        '   Bind datalist
        'dgrdTransactionSearch.DataSource = (New StudentSearchFacade).GetStudentSearchDS(txtLastName.Text, txtFirstName.Text, txtSSN.Text, txtEnrollment.Text, ddlStatusId.SelectedValue, ddlStudentPrgVerId.SelectedValue, CampusId)
        'dgrdTransactionSearch.DataBind()

        'Modified by MR on 05/19/2005
        '   Bind datalist
        Dim txtLastName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtLastName"), TextBox)
        Dim txtFirstName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtFirstName"), TextBox)
        Dim txtEnrollment As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtEnrollment"), TextBox)
        Dim ddlStatusId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStatusId"), DropDownList)
        Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStudentPrgVerId"), DropDownList)
        Dim txtSSN As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtSSN"), TextBox)
        dgrdTransactionSearch.DataSource = New DataView((New StudentSearchFacade).GetStudentSearchDS(txtLastName.Text, txtFirstName.Text, txtSSN.Text, txtEnrollment.Text, ddlStatusId.SelectedValue, ddlStudentPrgVerId.SelectedValue, CampusId).Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
        dgrdTransactionSearch.DataBind()

    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'If txtLastName.Text = "" And txtFirstName.Text = "" And txtEnrollment.Text = "" And ddlStatusId.SelectedValue = Guid.Empty.ToString And ddlCampGrpID.SelectedValue = Guid.Empty.ToString And ddlStudentPrgVerId.SelectedValue = Guid.Empty.ToString And txtSSN.Text = "" Then
        '    DisplayErrorMessage("Please enter a value to search")
        '    Exit Sub
        'End If
        ''   bind datalist
        'BindDataList()
        Dim txtLastName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtLastName"), TextBox)
        Dim txtFirstName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtFirstName"), TextBox)
        Dim txtEnrollment As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtEnrollment"), TextBox)
        Dim ddlStatusId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStatusId"), DropDownList)
        Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStudentPrgVerId"), DropDownList)
        Dim txtSSN As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtSSN"), TextBox)
        If txtLastName.Text = "" And txtFirstName.Text = "" And txtEnrollment.Text = "" And ddlStatusId.SelectedValue = Guid.Empty.ToString And ddlStudentPrgVerId.SelectedValue = Guid.Empty.ToString And txtSSN.Text = "" Then
            DisplayErrorMessage("Please enter a value to search")
            Exit Sub
        End If

        '   bind datalist
        BindDataList()
    End Sub
    Private Sub dgrdTransactionSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTransactionSearch.ItemCommand
        Dim parametersArray(7) As String '= {e.CommandArgument, (New StudentSearchFacade).GetStudentNameByID(e.CommandArgument)}

        '   set StudentName
        parametersArray(0) = CType(e.Item.FindControl("lnkLastName"), LinkButton).Text + " " + CType(e.Item.FindControl("lblFirstName1"), Label).Text

        '   set StuEnrollment
        Dim ddlEnrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)
        parametersArray(1) = ddlEnrollments.SelectedItem.Value
        parametersArray(2) = ddlEnrollments.SelectedItem.Text

        '   set Terms
        Dim ddlTerms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)
        parametersArray(3) = ddlTerms.SelectedItem.Value
        parametersArray(4) = ddlTerms.SelectedItem.Text

        '   set AcademicYears
        Dim ddlAcademicYears As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)
        parametersArray(5) = ddlAcademicYears.SelectedItem.Value
        parametersArray(6) = ddlAcademicYears.SelectedItem.Text

        '   set StudentId
        parametersArray(7) = CType(e.Item.FindControl("label2"), Label).Text()

        '   send parameters back to the parent page
        'CommonWebUtilities.PassDataBackToParentPage(Page, parametersArray)
        PassDataBackToParentPage(Page, parametersArray)
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub dgrdTransactionSearch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdTransactionSearch.ItemDataBound

        '   process only rows in the items section
        Select Case e.Item.ItemType

            Case ListItemType.Item, ListItemType.AlternatingItem


                '   Register Max field length for Last Name
                RegisterMaxFieldLength(0, e.Item.DataItem("LastName"))

                '   Register Max field length for First Name
                RegisterMaxFieldLength(1, e.Item.DataItem("FirstName"))

                '   Register Max field length for SSN
                RegisterMaxFieldLength(2, e.Item.DataItem("SSN"))


                '   get Enrollments dropdownlist
                Dim enrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)

                '   populate  Enrollments ddl
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim drows() As DataRow = dr.Row.GetChildRows("StudentsStudentEnrollments")
                Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStudentPrgVerId"), DropDownList)
                If ddlStudentPrgVerId.SelectedIndex = 0 Then
                    '   If no Prg Version is selected in the dropdownlist, 
                    '   add the 'All Enrollments' item in the dropdownlist.
                    enrollments.Items.Add(New ListItem("All Enrollments", System.Guid.Empty.ToString))

                    '   Register Max field length for Enrollments. Add some slack because it is a dropdownlist
                    RegisterMaxFieldLength(3, "All Enrollments" + "XXXX")
                End If
                For i As Integer = 0 To drows.Length - 1
                    enrollments.Items.Add(New ListItem(drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment"), CType(drows(i)("StuEnrollId"), Guid).ToString))

                    '   Register Max field length for Enrollments. Add some slack because it is a dropdownlist
                    RegisterMaxFieldLength(3, drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment") + "XXXX")
                Next

                '   first Enrollment is selected
                'enrollments.SelectedIndex = 0

                'select latest enrollment
                If Not drows.Length = 0 Then
                    'sort terms by date
                    Dim idx(drows.Length - 1, 1) As Double
                    For i As Integer = 0 To drows.Length - 1
                        idx(i, 0) = i
                        If Not drows(i)("StartDate") Is System.DBNull.Value Then
                            idx(i, 1) = CType(drows(i)("StartDate"), Date).Ticks
                        Else
                            idx(i, 1) = 0
                        End If
                    Next

                    'sort the terms by Date
                    If drows.Length > 1 Then
                        For j As Integer = 1 To drows.Length - 1
                            For k As Integer = j To drows.Length - 1
                                If idx(j, 1) > idx(j - 1, 1) Then
                                    Dim t0 As Double = idx(j - 1, 0)
                                    Dim t1 As Double = idx(j - 1, 1)
                                    idx(j - 1, 0) = idx(j, 0)
                                    idx(j - 1, 1) = idx(j, 1)
                                    idx(j, 0) = t0
                                    idx(j, 1) = t1
                                End If
                            Next
                        Next
                    End If

                    '   latest Enrollment is selected
                    enrollments.SelectedIndex = idx(0, 0)
                Else
                    enrollments.SelectedIndex = 0
                    CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                End If



                ''   get Terms dropdownlist
                'Dim terms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)

                ''   populate Terms ddl
                'For i As Integer = 0 To ddlTermId.Items.Count - 1
                '    terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                '    '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                '    RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                'Next

                ''   first Term is selected
                'ddlTermId.SelectedIndex = 0


                '   get Terms dropdownlist
                Dim terms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)

                'get all student terms
                Dim trows() As DataRow = dr.Row.GetChildRows("StudentStudentTerms")


                'if the student has no Terms ... populate all terms
                If Not trows.Length = 0 Then
                    'sort terms by date
                    Dim idx(trows.Length - 1, 1) As Double
                    For i As Integer = 0 To trows.Length - 1
                        idx(i, 0) = i
                        idx(i, 1) = CType(trows(i)("StartDate"), Date).Ticks
                    Next

                    'sort the terms by Date
                    If trows.Length > 1 Then
                        For j As Integer = 1 To trows.Length - 1
                            For k As Integer = j To trows.Length - 1
                                If idx(j, 1) > idx(j - 1, 1) Then
                                    Dim t0 As Double = idx(j - 1, 0)
                                    Dim t1 As Double = idx(j - 1, 1)
                                    idx(j - 1, 0) = idx(j, 0)
                                    idx(j - 1, 1) = idx(j, 1)
                                    idx(j, 0) = t0
                                    idx(j, 1) = t1
                                End If
                            Next
                        Next
                    End If
                    Dim cnt As Integer = 0
                    Dim allCnt As Integer = 0
                    If enrollments.SelectedValue = "00000000-0000-0000-0000-000000000000" Then
                        For i As Integer = 0 To trows.Length - 1
                            'populate only student terms
                            'populate only student terms
                            terms.Items.Add(New ListItem(trows(idx(i, 0)).GetParentRow("TermsStudentTerms")("TermDescrip"), CType(trows(idx(i, 0))("TermId"), Guid).ToString))

                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                            RegisterMaxFieldLength(4, terms.Items(i).Text + "XXXX")

                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist



                        Next
                    Else
                        For i As Integer = 0 To trows.Length - 1
                            If trows(idx(i, 0))("StuEnrollId").ToString = enrollments.SelectedValue Then
                                'populate only student terms
                                terms.Items.Add(New ListItem(trows(idx(i, 0)).GetParentRow("TermsStudentTerms")("TermDescrip"), CType(trows(idx(i, 0))("TermId"), Guid).ToString))

                                '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                                RegisterMaxFieldLength(4, terms.Items(cnt).Text + "XXXX")
                                cnt = cnt + 1
                            End If
                        Next
                    End If

                    Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                    '   select latest term for the student
                    If Not dr.Row("CurrentTerm") Is System.DBNull.Value Then
                        terms.SelectedValue = CType(dr.Row("CurrentTerm"), Guid).ToString
                    Else
                        terms.SelectedIndex = 0
                    End If
                    If terms.Items.Count = 0 Then
                        BuildTermsDDL(enrollments.SelectedValue.ToString(), CampusId)
                        For i As Integer = 0 To ddlTermId.Items.Count - 1
                            terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                            RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                        Next
                        If ddlTermId.Items.Count = 0 Then
                            CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                        End If
                        '   first Enrollment is selected
                        terms.SelectedIndex = 0
                    End If

                Else
                    Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                    '   populate Terms ddl
                    BuildTermsDDL(enrollments.SelectedValue.ToString(), CampusId)
                    For i As Integer = 0 To ddlTermId.Items.Count - 1
                        terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                        '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                        RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                    Next
                    If ddlTermId.Items.Count = 0 Then
                        CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                    End If
                    '   first Enrollment is selected
                    terms.SelectedIndex = 0

                End If


                Dim ddlAcademicYearId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlAcademicYearId"), DropDownList)
                '   get AcademicYear dropdownlist
                Dim academicYear As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)

                '   populate AcademicYear ddl
                For Each item As ListItem In ddlAcademicYearId.Items
                    Dim newItem As New ListItem(item.Text, item.Value)
                    newItem.Selected = item.Selected
                    academicYear.Items.Add(newItem)

                    '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                    RegisterMaxFieldLength(5, item.Text + "XXXX")
                Next

            Case ListItemType.Footer

                If SumOfMaxFieldLengths() > 0 Then
                    '   calculate the width percentage of each cell in the footer
                    Dim cells As ControlCollection = e.Item.Controls
                    For i As Integer = 0 To cells.Count - 1
                        Dim cell As TableCell = CType(cells(i), TableCell)
                        '   add the width attribute to each cell only if there are records
                        cell.Attributes.Add("width", CType(MaxFieldLengths(i) * 100.0 / SumOfMaxFieldLengths(), Integer).ToString + "%")
                    Next
                End If
        End Select
    End Sub
    Private Sub RegisterMaxFieldLength(ByVal idx As Integer, ByVal txt As Object)
        If Not txt.GetType.ToString = "System.DBNull" Then
            Dim text As String = CType(txt, String)
            If text.Length > MaxFieldLengths(idx) Then
                MaxFieldLengths(idx) = text.Length
            End If
        End If
    End Sub
    Private Function SumOfMaxFieldLengths() As Integer
        SumOfMaxFieldLengths = 0
        For i As Integer = 0 To MaxFieldLengths.Length - 1
            SumOfMaxFieldLengths += MaxFieldLengths(i)
        Next
    End Function
    Public Sub ddqty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        'Dim drop As DropDownList = CType(sender, DropDownList)
        'Response.Write(drop.SelectedItem.Value)
        ''Dim ddlTermId As DropDownList = CType(dgrdTransactionSearch.Items(dgrdTransactionSearch.Items).FindControl("ddlTermId"), DropDownList)
        ''ddlTermId.Items.Clear()
        'Dim cell As TableCell = CType(drop.Parent, TableCell)
        'Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
        'Dim i As Integer = item.ItemIndex
        'Dim content As String = item.Cells(0).Text


        Dim ddllist As DropDownList = CType(sender, DropDownList)
        Dim cell As TableCell = CType(ddllist.Parent, TableCell)
        Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
        Dim content As String = item.Cells(0).Text
        Dim ddlType As DropDownList = CType(item.Cells(0).FindControl("ddlEnrollmentsId"), DropDownList)
        Dim ddlItem As DropDownList = CType(item.Cells(1).FindControl("ddlTermsId"), DropDownList)
        Dim studentId As String = CType(item.Cells(0).FindControl("lnkLastName"), LinkButton).CommandArgument
        ddlItem.Items.Clear()
        If ddlType.SelectedValue = "00000000-0000-0000-0000-000000000000" Then
            With ddlItem
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataSource = (New StudentSearchFacade).getStudentAllTerm(studentId, CampusId)
                .DataBind()
                '.SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYearId.Items)
            End With
        Else
            With ddlItem
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataSource = (New StudentSearchFacade).getStudentTerm(ddlType.SelectedValue, CampusId)
                .DataBind()
                '.SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYearId.Items)
            End With
        End If
        If ddlItem.Items.Count = 0 Then
            With ddlItem
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataSource = (New StudentSearchFacade).getAllStudentTerm(ddlType.SelectedValue, CampusId)
                .DataBind()
            End With
        End If
        If ddlItem.Items.Count = 0 Then
            CType(item.FindControl("lnkLastName"), LinkButton).Enabled = False
        Else
            CType(item.FindControl("lnkLastName"), LinkButton).Enabled = True
        End If
    End Sub
    Public Sub ddqtyMRU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        'Dim drop As DropDownList = CType(sender, DropDownList)
        'Response.Write(drop.SelectedItem.Value)
        ''Dim ddlTermId As DropDownList = CType(dgrdTransactionSearch.Items(dgrdTransactionSearch.Items).FindControl("ddlTermId"), DropDownList)
        ''ddlTermId.Items.Clear()
        'Dim cell As TableCell = CType(drop.Parent, TableCell)
        'Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
        'Dim i As Integer = item.ItemIndex
        'Dim content As String = item.Cells(0).Text


        Dim ddllist As DropDownList = CType(sender, DropDownList)
        Dim cell As TableCell = CType(ddllist.Parent, TableCell)
        Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
        Dim content As String = item.Cells(0).Text
        Dim ddlType As DropDownList = CType(item.Cells(0).FindControl("ddlEnrollmentsId"), DropDownList)
        Dim ddlItem As DropDownList = CType(item.Cells(1).FindControl("ddlTermsId"), DropDownList)
        Dim studentId As String = CType(item.Cells(0).FindControl("lnkLastName"), LinkButton).CommandArgument
        ddlItem.Items.Clear()
        If ddlType.SelectedValue = "00000000-0000-0000-0000-000000000000" Then
            With ddlItem
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataSource = (New StudentSearchFacade).getStudentAllTerm(studentId, CampusId)
                .DataBind()
                '.SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYearId.Items)
            End With
        Else
            With ddlItem
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataSource = (New StudentSearchFacade).getStudentTerm(ddlType.SelectedValue, CampusId)
                .DataBind()
                '.SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYearId.Items)
            End With
        End If
        If ddlItem.Items.Count = 0 Then
            With ddlItem
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataSource = (New StudentSearchFacade).getAllStudentTerm(ddlType.SelectedValue, CampusId)
                .DataBind()
            End With
        End If
        If ddlItem.Items.Count = 0 Then
            CType(item.FindControl("lnkLastName"), LinkButton).Enabled = False
        Else
            CType(item.FindControl("lnkLastName"), LinkButton).Enabled = True
        End If
    End Sub
    Private Function RemoveDuplicateItems(ByVal ddl As DropDownList) As DropDownList
        Dim newddl As New DropDownList
        newddl.Items.Add(New ListItem(ddl.Items(0).Text, ddl.Items(0).Value))
        Dim i As Integer
        For i = 1 To ddl.Items.Count - 1
            If (ddl.Items(i).Value <> ddl.Items(i - 1).Value) Then
                newddl.Items.Add(New ListItem(ddl.Items(i).Text, ddl.Items(i).Value))
            End If
        Next
        Return newddl
    End Function
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub
    Private Sub dgrdTransactionSearchMRU_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTransactionSearchMRU.ItemCommand
        Dim parametersArray(7) As String '= {e.CommandArgument, (New StudentSearchFacade).GetStudentNameByID(e.CommandArgument)}

        '   set StudentName
        parametersArray(0) = CType(e.Item.FindControl("lnkLastName"), LinkButton).Text + " " + CType(e.Item.FindControl("lblFirstName1"), Label).Text

        '   set StuEnrollment
        Dim ddlEnrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)
        parametersArray(1) = ddlEnrollments.SelectedItem.Value
        parametersArray(2) = ddlEnrollments.SelectedItem.Text

        '   set Terms
        Dim ddlTerms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)
        parametersArray(3) = ddlTerms.SelectedItem.Value
        parametersArray(4) = ddlTerms.SelectedItem.Text

        '   set AcademicYears
        Dim ddlAcademicYears As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)
        parametersArray(5) = ddlAcademicYears.SelectedItem.Value
        parametersArray(6) = ddlAcademicYears.SelectedItem.Text

        '   set StudentId
        parametersArray(7) = CType(e.Item.FindControl("label2"), Label).Text()

        '   send parameters back to the parent page
        'CommonWebUtilities.PassDataBackToParentPage(Page, parametersArray)
        PassDataBackToParentPage(Page, parametersArray)
    End Sub
    Private Sub PassDataBackToParentPage(ByVal page As Page, ByVal parametersArray() As String)
        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            '   this is the beginning of the javascript code  
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=PassData();function PassData(){"

            '   this is the middle of the javascript
            '   build the parameter list to be returned to the parent page
            Dim scriptMiddle As String = ""
            For i As Integer = 0 To parametersArray.Length - 1
                ''Thic Condition added by Saraswathi Since it gave error
                ''When the query string didnot have eneough argements
                ''Modified by Saraswathi on Jan 8 2009

                If page.Request.QueryString.Keys.Count + 1 > parametersArray.Length + 3 Then

                    If page.Request.QueryString.Keys(i + 3).ToString = "ddlShiftId" Then
                        scriptMiddle += "window.opener.document.getElementById('ddlShiftId').SelectedValue='" + ReplaceSpecialCharactersInJavascriptMessage(parametersArray(8).Trim) + "';"
                    End If
                    scriptMiddle += "window.opener.document.getElementById('" + page.Request.QueryString.Keys(i + 3).ToString + "').value='" + ReplaceSpecialCharactersInJavascriptMessage(parametersArray(i).Trim) + "';"
                End If
            Next



            '   this is the end of the javascript code
            Dim scriptEnd As String = "window.close();}</script>"

            '   Register the javascript code
            ScriptManager.RegisterStartupScript(page, page.GetType(), "PassDataBackToParentPage", scriptBegin + scriptMiddle + scriptEnd, False)
        End If
    End Sub
    Private Sub BindDataListMRU()
        Dim resourceId As Integer
        Dim fac As New ResourcesRelationsFacade
        Dim fac2 As New MRUFacade
        Dim mruTypeId As Integer
        Dim mruType As String = "Students"
        Dim modName As String
        Dim procMod As Boolean
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        Dim dsMRU As New DataSet

        If Not resourceId = 264 Then
            modName = HttpContext.Current.Request.Params("mod")
            Select Case modName.ToUpper
                Case "AD"
                    mruType = "Leads"
                    procMod = True
                Case "AR", "SA", "FA", "FC"
                    mruType = "Students"
                    procMod = True
                Case "PL"
                    mruTypeId = fac2.GetResourceMRUTypeId(resourceId)
                    Select Case mruTypeId
                        Case 1
                            mruType = "Students"
                            procMod = True
                        Case 2
                            mruType = "Employers"
                            procMod = True
                    End Select
                Case "HR"
                    mruType = "Employees"
                    procMod = True
            End Select
            'If procMod = True Then
            '    Dim objMRUFac As New MRUFacade
            '    dsMRU = objMRUFac.LoadMRU(mruType, Session("UserId").ToString(), HttpContext.Current.Request.Params("cmpid"))
            'End If
        Else 'Home Page. For this we will use the module to determine what MRU should be displayed
            modName = HttpContext.Current.Request.Params("mod")
            Select Case modName.ToUpper
                Case "AD"
                    mruType = "Leads"
                    procMod = True
                Case "AR", "SA", "FA", "PL", "FC"
                    mruType = "Students"
                    procMod = True
                Case "HR"
                    mruType = "Employees"
                    procMod = True
            End Select
        End If
        If procMod = True Then
            ' US3054 4/17/2012 Janet Robinson switch MRU return to match left MRU bar for Students
            If mruType = "Students" Then
                Dim objMRUFac As New MRUFacade
                dsMRU = objMRUFac.LoadMRUForStudentSearch(userId, HttpContext.Current.Request.Params("cmpid"))
                If Not IsNothing(dsMRU) Then
                    '   campusDesc = dsMRU.Tables(0).Rows(0).Item("CampusDescrip")

                    Dim dr As DataRow()
                    dr = dsMRU.Tables(0).Select("CampusID='" + HttpContext.Current.Request.Params("cmpid") + "'")
                    If dr.Length > 0 Then
                        For Each row In dr
                            campusDesc = row("CampusDescrip")
                            Exit For
                        Next

                    End If
                End If
            End If
        End If

        If dsMRU.Tables.Count > 0 Then
            If dsMRU.Tables("MRUList").Rows.Count > 0 Then
                dgrdTransactionSearchMRU.DataSource = New DataView((New StudentSearchFacade).GetStudentSearchDSMRU(dsMRU, CampusId).Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
                dgrdTransactionSearchMRU.DataBind()
            End If
        End If
    End Sub

    Private Sub dgrdTransactionSearchMRU_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdTransactionSearchMRU.ItemDataBound

        '   process only rows in the items section
        Select Case e.Item.ItemType

            Case ListItemType.Item, ListItemType.AlternatingItem


                '   Register Max field length for Last Name
                RegisterMaxFieldLength(0, e.Item.DataItem("LastName"))

                '   Register Max field length for First Name
                RegisterMaxFieldLength(1, e.Item.DataItem("FirstName"))

                '   Register Max field length for SSN
                RegisterMaxFieldLength(2, e.Item.DataItem("SSN"))


                '   get Enrollments dropdownlist
                Dim enrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)

                '   populate  Enrollments ddl
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim drows() As DataRow = dr.Row.GetChildRows("StudentsStudentEnrollments")
                Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStudentPrgVerId"), DropDownList)
                If ddlStudentPrgVerId.SelectedIndex = 0 Then
                    '   If no Prg Version is selected in the dropdownlist, 
                    '   add the 'All Enrollments' item in the dropdownlist.
                    enrollments.Items.Add(New ListItem("All Enrollments", System.Guid.Empty.ToString))

                    '   Register Max field length for Enrollments. Add some slack because it is a dropdownlist
                    RegisterMaxFieldLength(3, "All Enrollments" + "XXXX")
                End If
                For i As Integer = 0 To drows.Length - 1
                    enrollments.Items.Add(New ListItem(drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment"), CType(drows(i)("StuEnrollId"), Guid).ToString))

                    '   Register Max field length for Enrollments. Add some slack because it is a dropdownlist
                    RegisterMaxFieldLength(3, drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment") + "XXXX")
                Next

                '   first Enrollment is selected
                'enrollments.SelectedIndex = 0

                'select latest enrollment
                If Not drows.Length = 0 Then
                    'sort terms by date
                    Dim idx(drows.Length - 1, 1) As Double
                    For i As Integer = 0 To drows.Length - 1
                        idx(i, 0) = i
                        If Not drows(i)("StartDate") Is System.DBNull.Value Then
                            idx(i, 1) = CType(drows(i)("StartDate"), Date).Ticks
                        Else
                            idx(i, 1) = 0
                        End If
                    Next

                    'sort the terms by Date
                    If drows.Length > 1 Then
                        For j As Integer = 1 To drows.Length - 1
                            For k As Integer = j To drows.Length - 1
                                If idx(j, 1) > idx(j - 1, 1) Then
                                    Dim t0 As Double = idx(j - 1, 0)
                                    Dim t1 As Double = idx(j - 1, 1)
                                    idx(j - 1, 0) = idx(j, 0)
                                    idx(j - 1, 1) = idx(j, 1)
                                    idx(j, 0) = t0
                                    idx(j, 1) = t1
                                End If
                            Next
                        Next
                    End If

                    '   latest Enrollment is selected
                    enrollments.SelectedIndex = idx(0, 0)
                Else
                    enrollments.SelectedIndex = 0
                    CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                End If



                ''   get Terms dropdownlist
                'Dim terms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)

                ''   populate Terms ddl
                'For i As Integer = 0 To ddlTermId.Items.Count - 1
                '    terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                '    '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                '    RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                'Next

                ''   first Term is selected
                'ddlTermId.SelectedIndex = 0


                '   get Terms dropdownlist
                Dim terms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)

                'get all student terms
                Dim trows() As DataRow = dr.Row.GetChildRows("StudentStudentTerms")


                'if the student has no Terms ... populate all terms
                If Not trows.Length = 0 Then
                    'sort terms by date
                    Dim idx(trows.Length - 1, 1) As Double
                    For i As Integer = 0 To trows.Length - 1
                        idx(i, 0) = i
                        If Not IsDBNull(trows(i)("StartDate")) Then
                            idx(i, 1) = CType(trows(i)("StartDate"), Date).Ticks
                        End If
                    Next

                    'sort the terms by Date
                    If trows.Length > 1 Then
                        For j As Integer = 1 To trows.Length - 1
                            For k As Integer = j To trows.Length - 1
                                If idx(j, 1) > idx(j - 1, 1) Then
                                    Dim t0 As Double = idx(j - 1, 0)
                                    Dim t1 As Double = idx(j - 1, 1)
                                    idx(j - 1, 0) = idx(j, 0)
                                    idx(j - 1, 1) = idx(j, 1)
                                    idx(j, 0) = t0
                                    idx(j, 1) = t1
                                End If
                            Next
                        Next
                    End If
                    Dim cnt As Integer = 0
                    Dim allCnt As Integer = 0
                    If enrollments.SelectedValue = "00000000-0000-0000-0000-000000000000" Then
                        For i As Integer = 0 To trows.Length - 1
                            'populate only student terms
                            'populate only student terms
                            terms.Items.Add(New ListItem(trows(idx(i, 0)).GetParentRow("TermsStudentTerms")("TermDescrip"), CType(trows(idx(i, 0))("TermId"), Guid).ToString))
                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                            RegisterMaxFieldLength(4, terms.Items(i).Text + "XXXX")
                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                        Next
                    Else
                        For i As Integer = 0 To trows.Length - 1
                            If trows(idx(i, 0))("StuEnrollId").ToString = enrollments.SelectedValue Then
                                'populate only student terms
                                terms.Items.Add(New ListItem(trows(idx(i, 0)).GetParentRow("TermsStudentTerms")("TermDescrip"), CType(trows(idx(i, 0))("TermId"), Guid).ToString))

                                '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                                RegisterMaxFieldLength(4, terms.Items(cnt).Text + "XXXX")
                                cnt = cnt + 1
                            End If
                        Next
                    End If

                    Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                    '   select latest term for the student
                    If Not dr.Row("CurrentTerm") Is System.DBNull.Value Then
                        terms.SelectedValue = CType(dr.Row("CurrentTerm"), Guid).ToString
                    Else
                        terms.SelectedIndex = 0
                    End If
                    If terms.Items.Count = 0 Then
                        BuildTermsDDL(enrollments.SelectedValue.ToString(), CampusId)
                        For i As Integer = 0 To ddlTermId.Items.Count - 1
                            terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                            RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                        Next
                        If ddlTermId.Items.Count = 0 Then
                            CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                        End If
                        '   first Enrollment is selected
                        terms.SelectedIndex = 0
                    End If

                Else
                    Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                    '   populate Terms ddl
                    BuildTermsDDL(enrollments.SelectedValue.ToString(), CampusId)
                    For i As Integer = 0 To ddlTermId.Items.Count - 1
                        terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                        '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                        RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                    Next
                    If ddlTermId.Items.Count = 0 Then
                        CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                    End If
                    '   first Enrollment is selected
                    terms.SelectedIndex = 0

                End If


                Dim ddlAcademicYearId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlAcademicYearId"), DropDownList)
                '   get AcademicYear dropdownlist
                Dim academicYear As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)

                '   populate AcademicYear ddl
                For Each item As ListItem In ddlAcademicYearId.Items
                    Dim newItem As New ListItem(item.Text, item.Value)
                    newItem.Selected = item.Selected
                    academicYear.Items.Add(newItem)

                    '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                    RegisterMaxFieldLength(5, item.Text + "XXXX")
                Next

            Case ListItemType.Footer

                If SumOfMaxFieldLengths() > 0 Then
                    '   calculate the width percentage of each cell in the footer
                    Dim cells As ControlCollection = e.Item.Controls
                    For i As Integer = 0 To cells.Count - 1
                        Dim cell As TableCell = CType(cells(i), TableCell)
                        '   add the width attribute to each cell only if there are records
                        cell.Attributes.Add("width", CType(MaxFieldLengths(i) * 100.0 / SumOfMaxFieldLengths(), Integer).ToString + "%")
                    Next
                End If
        End Select
    End Sub
    Private Function ReplaceSpecialCharactersInJavascriptMessage(ByVal s As String) As String
        '   replace \ by \\, ' by \', " by "", CR by \r, LF by \n
        Return s.Trim().Replace("\", "\\").Replace("'", "\'").Replace("""", "\""").Replace(vbCr, "\r").Replace(vbLf, "\n")
    End Function

    Protected Sub rpbAdvanceOption_ItemClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadPanelBarEventArgs) Handles rpbAdvanceOption.ItemClick
        Dim txtLast As TextBox
        txtLast = CType(rpbAdvanceOption.Items(0).Controls(0).FindControl("txtLastName"), TextBox)
        txtLast.Focus()
    End Sub
End Class
