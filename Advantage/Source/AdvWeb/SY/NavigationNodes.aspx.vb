
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Partial Class NavigationNodes
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim chkADOldState, chkAROldState, chkPLOldState, chkSAOldState, chkHROldState As Boolean
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Dim intSelectedMaintenancePageId As Integer
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim facade As New NavigationFacade
        If Not Page.IsPostBack Then
            BuildModules()
            dlstLeadNames.DataSource = facade.GetPagesByModulePageNameSP(0, "")
            dlstLeadNames.DataBind()
        End If

        'The page name was disabled
        txtPageName.Enabled = False

        '  Dim m_Context2 As HttpContext
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        Dim m_Context As HttpContext
        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        Dim dsResource As String
        dsResource = facade.GetAllParentNodes()

        Dim arrRange() As String
        arrRange = dsResource.Split(",")

        txtADParentId.Text = arrRange(0)
        txtARParentId.Text = arrRange(1)
        txtPLParentId.Text = arrRange(2)
        txtSAParentId.Text = arrRange(3)
        txtHRParentId.Text = arrRange(4)
        txtFinAidParentId.Text = arrRange(5)
        txtFACParentId.Text = arrRange(6)
        txtSysParentId.Text = arrRange(7)
    End Sub
    Private Sub BuildModules()
        Dim facade As New NavigationFacade
        With ddlModules
            .DataTextField = "Resource"
            .DataValueField = "ResourceId"
            .DataSource = facade.GetAllResourceModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", 0))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        ResetCheckBox()
        Dim facade As New NavigationFacade
        dlstLeadNames.DataSource = facade.GetPagesByModulePageNameSP(ddlModules.SelectedValue, txtStartsWith.Text)
        dlstLeadNames.DataBind()
    End Sub
    Private Sub dlstLeadNames_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstLeadNames.ItemCommand
        Dim dsResource As DataSet
        Dim facade As New NavigationFacade
        dsResource = facade.GetModulesByResource(e.CommandArgument)

        ResetCheckBox()
        txtPageName.Text = e.CommandName
        txtResourceId.Text = e.CommandArgument

        ViewState("SelectedPageId") = e.CommandArgument

        txtHierarchyIndex.Text = facade.GetHierarchyIndex(e.CommandArgument)

        Dim dr As DataRow

        For Each dr In dsResource.Tables(0).Rows
            Select Case dr("ModuleId")
                Case Is = 189
                    chkAD.Checked = True
                Case Is = 26
                    chkAR.Checked = True
                Case Is = 193
                    chkPL.Checked = True
                Case Is = 194
                    chkSA.Checked = True
                Case Is = 192
                    chkHR.Checked = True
                Case Is = 191
                    chkFinAid.Checked = True
                Case Is = 300
                    chkFAC.Checked = True
                Case Is = 195
                    chkSys.Checked = True
            End Select
        Next

        Select Case txtResourceId.Text.Trim
            Case Is = "20", "29", "34", "339", "387", "388", "389", "390", "491", "12", "14", "302", "15", "216", "215", " 212", " 214", "358", "336", "344", "337", "13", "16", "18", "241", "296", "21", "35", "239", "28", "350", "44", "38"
                chkAD.Enabled = False
                txtADParentId.Enabled = False
            Case Is = "4", "6", "10", "39", "43", "66", "67", "280", "297", "316", "323", "505", "552", "581", "627", "40", "14", "161", "1", "11", "100", "101", "350", "44", "38", "623", "624", "361", "150", "149", "5", "289", "41"
                chkAR.Enabled = False
                txtARParentId.Enabled = False
            Case Is = "57", "72", "75", "81", "154", "162", "164", "528", "571", "12", "161", "486", "85"
                chkSA.Enabled = False
                txtSAParentId.Enabled = False
            Case Is = "88", "99", "282", "304", "24", "27", "25", "93", "93", "37", "302", "15"
                chkPL.Enabled = False
                txtPLParentId.Enabled = False
            Case Is = "139"
                chkFinAid.Enabled = False
                txtFinAidParentId.Enabled = False
            Case Is = "22", "31", "269", "278", "324", "326", "378", "381", "382", "385", "481", "487", "616", "464", "478", "465", "430", "431", "432", "433", "314", "348", "131"
                chkSys.Enabled = False
                txtSysParentId.Enabled = False
        End Select

        If chkAD.Checked = True Then
            Session("chkADOldState") = True
        Else
            Session("chkADOldState") = False
        End If

        If chkAR.Checked = True Then
            Session("chkAROldState") = True
        Else
            Session("chkAROldState") = False
        End If

        If chkPL.Checked = True Then
            Session("chkPLOldState") = True
        Else
            Session("chkPLOldState") = False
        End If

        If chkSA.Checked = True Then
            Session("chkSAOldState") = True
        Else
            Session("chkSAOldState") = False
        End If

        If chkHR.Checked = True Then
            Session("chkHROldState") = True
        Else
            Session("chkHROldState") = False
        End If

        If chkFinAid.Checked = True Then
            Session("chkFinAidOldState") = True
        Else
            Session("chkFinAidOldState") = False
        End If

        If chkFAC.Checked = True Then
            Session("chkFacOldState") = True
        Else
            Session("chkFacOldState") = False
        End If

        If chkSys.Checked = True Then
            Session("chkSysOldState") = True
        Else
            Session("chkSysOldState") = False
        End If
        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstLeadNames, e.CommandArgument, ViewState, Header1)
    End Sub
    Private Sub ResetCheckBox()
        chkAD.Checked = False
        chkAR.Checked = False
        chkPL.Checked = False
        chkSA.Checked = False
        chkHR.Checked = False
        chkFinAid.Checked = False
        chkFAC.Checked = False
        chkSys.Checked = False
        txtPageName.Text = ""
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim facade As New NavigationFacade
        If chkAD.Checked = False And chkAR.Checked = False And chkPL.Checked = False And chkSA.Checked = False And chkHR.Checked = False And chkFinAid.Checked = False And chkFAC.Checked = False And chkSys.Checked = False Then
            DisplayErrorMessage("The page has to be added to any one of the modules")
            Exit Sub
        End If
        facade.InsertNodeSP(chkAD.Checked, chkAR.Checked, chkSA.Checked, chkPL.Checked, _
                            chkFinAid.Checked, chkHR.Checked, chkFAC.Checked, chkSys.Checked, ViewState("SelectedPageId"), Session("UserName"))

        'Call BuildNavigation Method from Master Page
        '  Master.BuildNavigation(Master.CurrentCampusId)



        'facade.InsertNode(BuildNodes(), Session("UserName"))

        'After First Insert
        If chkAD.Checked = True Then
            Session("chkADOldState") = True
        Else
            Session("chkADOldState") = False
        End If

        If chkAR.Checked = True Then
            Session("chkAROldState") = True
        Else
            Session("chkAROldState") = False
        End If

        If chkPL.Checked = True Then
            Session("chkPLOldState") = True
        Else
            Session("chkPLOldState") = False
        End If

        If chkSA.Checked = True Then
            Session("chkSAOldState") = True
        Else
            Session("chkSAOldState") = False
        End If

        If chkHR.Checked = True Then
            Session("chkHROldState") = True
        Else
            Session("chkHROldState") = False
        End If

        If chkFinAid.Checked = True Then
            Session("chkFinAidOldState") = True
        Else
            Session("chkFinAidOldState") = False
        End If

        If chkFAC.Checked = True Then
            Session("chkFacOldState") = True
        Else
            Session("chkFacOldState") = False
        End If

        If chkSys.Checked = True Then
            Session("chkSysOldState") = True
        Else
            Session("chkSysOldState") = False
        End If
        dlstLeadNames.DataSource = facade.GetPagesByModulePageNameSP(ddlModules.SelectedValue, txtStartsWith.Text)
        dlstLeadNames.DataBind()
    End Sub
    Private Function BuildNodes() As NavigationNodeInfo
        Dim Nodes As New NavigationNodeInfo
        With Nodes
            'Get ResourceId
            .strResourceId = CInt(txtResourceId.Text)

            'Get HierarchyName
            .strHierarchyName = txtPageName.Text

            'Get strADParentId
            .strADParentId = txtADParentId.Text

            'Get strARParentId
            .strARParentId = txtARParentId.Text

            'Get strPLParentId
            .strPLParentId = txtPLParentId.Text

            'Get HR ParentId
            .strHRParentId = txtHRParentId.Text

            'Get SA ParentId
            .strSAParentId = txtSAParentId.Text

            'Get FinAid
            .strFinAidParentId = txtFinAidParentId.Text

            'Get Fac
            .strFACParentId = txtFACParentId.Text

            'Get Sys
            .strSysParentId = txtSysParentId.Text

            'Get HierarchyIndex
            .HierarchyIndex = txtHierarchyIndex.Text


            If chkAD.Checked = True Then
                If Not chkAD.Checked = Session("chkADOldState") Then
                    .StrADModified = True
                End If
            End If

            If chkAR.Checked = True Then
                If Not chkAR.Checked = Session("chkAROldState") Then
                    .StrARModified = True
                End If
            End If

            If chkPL.Checked = True Then
                If Not chkPL.Checked = Session("chkPLOldState") Then
                    .StrPLModified = True
                End If
            End If

            If chkSA.Checked = True Then
                If Not chkSA.Checked = Session("chkSAOldState") Then
                    .StrSAModified = True
                End If
            End If

            If chkHR.Checked = True Then
                If Not chkHR.Checked = Session("chkHROldState") Then
                    .StrHRModified = True
                End If
            End If

            If chkFinAid.Checked = True Then
                If Not chkFinAid.Checked = Session("chkFinAidOldState") Then
                    .StrFinAidModified = True
                End If
            End If

            If chkFAC.Checked = True Then
                If Not chkFAC.Checked = Session("chkFacOldState") Then
                    .StrFACModified = True
                End If
            End If

            If chkSys.Checked = True Then
                If Not chkSys.Checked = Session("chkSysOldState") Then
                    .StrSysModified = True
                End If
            End If

            'Check For Deletions
            If chkAD.Checked = False And Session("chkADOldState") = True Then
                .StrADDeleted = True
            End If

            If chkAR.Checked = False And Session("chkAROldState") = True Then
                .StrARDeleted = True
            End If

            If chkPL.Checked = False And Session("chkPLOldState") = True Then
                .StrPLDeleted = True
            End If

            If chkSA.Checked = False And Session("chkSAOldState") = True Then
                .StrSADeleted = True
            End If

            If chkHR.Checked = False And Session("chkHROldState") = True Then
                .StrHRDeleted = True
            End If

            If chkFAC.Checked = False And Session("chkFACOldState") = True Then
                .StrFACDeleted = True
            End If

            If chkFinAid.Checked = False And Session("chkFinAidOldState") = True Then
                .StrFinAidDeleted = True
            End If

            If chkSys.Checked = False And Session("chkSysOldState") = True Then
                .StrSysDeleted = True
            End If
            .strModDate = Date.Now
        End With
        Return Nodes
    End Function
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        controlsToIgnore.Add(btnSearch)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
