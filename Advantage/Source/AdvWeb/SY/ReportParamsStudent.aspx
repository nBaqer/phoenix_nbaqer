﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ReportParamsStudent.aspx.vb" Inherits="ReportParamsStudent" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>

    <title>
        <%# PageTitle%>
    </title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="300px" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>


            <%--LEFT PANE CONTENT HERE--%>

            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop">
                        <asp:Label ID="lblPreferences" runat="server" CssClass="label" Text="Saved User Reports">Saved Report Selections</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleft">
                            <asp:DataList ID="dlstPrefs" DataKeyField="PrefId" RepeatDirection="Vertical" runat="server">
                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Container.DataItem("PrefName")%>' runat="server" CssClass="itemstyle"
                                        CommandArgument='<%# Container.DataItem("PrefId")%>' ID="Linkbutton1" CausesValidation="False" />
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>


        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <div class="boxContainerNoHeight full">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                    ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                        </td>
                    </tr>
                </table>
            </div>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer full">
                            <!-- begin content table-->


                            <%-- MAIN CONTENT HERE--%>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contenttable">
                                <!-- begin top panel -->
                                <tr>
                                    <td style="width: 50%">
                                        <asp:Panel ID="pnlPreference" runat="server" Visible="true">
                                            <table cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
                                                <tr>
                                                    <td style="vertical-align: top">
                                                        <asp:Label ID="lblPrefName" runat="server" CssClass="labelbold">Preference Name</asp:Label>
                                                    </td>
                                                    <td style="vertical-align: top">
                                                        <asp:TextBox ID="txtPrefName" runat="server" CssClass="textbox" MaxLength="50"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                    <td style="width: 50%">
                                        <asp:Panel ID="pnl5" runat="server" Visible="false">
                                            <table cellspacing="0" cellpadding="0" width="100%" align="left" border="0">
                                                <tr>
                                                    <td style="vertical-align: top">
                                                        <asp:Label ID="lbl4" CssClass="labelbold" runat="server">Required Filter(s)</asp:Label>
                                                    </td>
                                                    <td style="vertical-align: top">
                                                        <asp:Label ID="lblRequiredFilters" CssClass="textbox" runat="server">Student, Enrollment</asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                            <!-- end top panel -->
                            <!-- Start Step 1 to Step 4 -->
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contenttable">
                                <tr>
                                    <td class="lsreport">
                                        <!-- begin left side report params -->
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contenttable">
                                            <tr>
                                                <td colspan="3" class="lsheaderreport">
                                                    <asp:Label ID="lblselectsort" runat="server" CssClass="label" Font-Bold="true">Step 1: please select the fields to sort the report by:</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="font-blue">Available sort order</td>
                                                <td class="">&nbsp;</td>
                                                <td class="font-blue">Selected sort order</td>
                                            </tr>
                                            <tr>
                                                <td class="lscontentreport3" width="40%">
                                                    <asp:ListBox ID="lbxsort" runat="server" Rows="7" CssClass="todocumentskills2"></asp:ListBox></td>
                                                <td class="lscontentreport2" width="20%" style="text-align: center">
                                                    <div class="margin-b-5px">
                                                        <asp:Button ID="btnadd" Text="Add >" runat="server" CausesValidation="false"></asp:Button>
                                                    </div>
                                                    <asp:Button ID="btnremove" Text="< Remove" runat="server" CausesValidation="false"></asp:Button>
                                                </td>
                                                <td class="lscontentreport3" width="40%">
                                                    <asp:ListBox ID="lbxselsort" runat="server" Rows="7" CssClass="todocumentskills2"></asp:ListBox></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="lsheaderreport">
                                                    <asp:Label ID="lblselectfilters" runat="server" CssClass="label" Font-Bold="true">Step 2: please select the filters to apply to the report:</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="font-blue">Filter</td>
                                                <td class="">&nbsp;</td>
                                                <td class="font-blue">Values</td>
                                            </tr>
                                            <tr>
                                                <td class="lscontentreport3">
                                                    <asp:ListBox ID="lbxfilterlist" runat="server" Rows="7" CssClass="todocumentskills2" AutoPostBack="true"></asp:ListBox></td>
                                                <td class="lscontentreport2">&nbsp;</td>
                                                <td class="lscontentreport3">
                                                    <asp:ListBox ID="lbxselfilterlist" runat="server" Rows="7" CssClass="todocumentskills2" AutoPostBack="true"
                                                        SelectionMode="multiple"></asp:ListBox></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="lsfooterreport">
                                                    <br />
                                                    <asp:Panel ID="pnl4" CssClass="textbox" runat="server" Visible="false" Width="98%">
                                                        <table cellspacing="0" cellpadding="1" width="100%" border="0">
                                                            <tr>
                                                                <td nowrap></td>
                                                                <td valign="middle" nowrap></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td nowrap></td>
                                                                <td nowrap></td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:Table ID="tblStudentSearch" CssClass="label" runat="server" Width="100%">
                                                                        <asp:TableRow>
                                                                            <asp:TableCell Width="28%"></asp:TableCell>
                                                                            <asp:TableCell Width="42%"></asp:TableCell>
                                                                            <asp:TableCell Width="30%"></asp:TableCell>
                                                                        </asp:TableRow>
                                                                        <asp:TableRow>
                                                                            <asp:TableCell>
                                                                                <asp:Label ID="lblStudent" CssClass="label" runat="server">Student</asp:Label>
                                                                            </asp:TableCell>
                                                                            <asp:TableCell Wrap="false">
                                                                                <asp:TextBox ID="txtStudentId" CssClass="textbox" runat="server" Width="200px" ContentEditable="False"></asp:TextBox>
                                                                                &nbsp;&nbsp;
                                                                                <asp:RequiredFieldValidator ID="requiredFldValStudent" runat="server" Display="None"
                                                                                    ControlToValidate="txtStudentId" ErrorMessage="Student is a required filter."></asp:RequiredFieldValidator>
                                                                                &nbsp;&nbsp;
                                                                                <asp:LinkButton ID="lbtSearch" runat="server" CausesValidation="False"
                                                                                    CssClass="save"><span class="k-icon k-i-search"></span></asp:LinkButton>
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                        <asp:TableRow>
                                                                            <asp:TableCell>
                                                                                <asp:Label ID="lblEnrollmentId" CssClass="label" runat="server" Visible="True">Enrollment</asp:Label>
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                <asp:TextBox ID="txtStuEnrollment" runat="server" CssClass="textbox" Width="200px"
                                                                                    ContentEditable="False"></asp:TextBox>
                                                                                <asp:RequiredFieldValidator ID="requiredFldValStuEnroll" runat="server" Display="None"
                                                                                    ControlToValidate="txtStuEnrollment" ErrorMessage="Enrollment is a required filter."></asp:RequiredFieldValidator>
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
                                                                                <asp:HiddenField ID="txtstuenrollmentid" runat="server" />
                                                                                <asp:HiddenField ID="txttermid" runat="server" />
                                                                                <asp:HiddenField ID="txtterm" runat="server" />
                                                                                <asp:HiddenField ID="txtacademicyearid" runat="server" />
                                                                                <asp:HiddenField ID="txtacademicyear" runat="server" />
                                                                                <asp:HiddenField ID="txtstudentidentifier" runat="server" />

                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                        <asp:TableRow>
                                                                            <asp:TableCell>
                                                                                <asp:Label ID="lblSpeed" CssClass="label" runat="server" Visible="false">Speed</asp:Label>
                                                                            </asp:TableCell>
                                                                            <asp:TableCell Wrap="false">
                                                                                <asp:DropDownList ID="ddlSpeedOperator" runat="server" Visible="false" CssClass="dropdownlist"
                                                                                    Width="150px" AutoPostBack="true">
                                                                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                                                                    <asp:ListItem Value="1">Between</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Equal To</asp:ListItem>
                                                                                    <asp:ListItem Value="3">Greater Than</asp:ListItem>
                                                                                    <asp:ListItem Value="4">Greater Than Equal to</asp:ListItem>
                                                                                    <asp:ListItem Value="5">Less Than</asp:ListItem>
                                                                                    <asp:ListItem Value="6">Less Than Equal to</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                &nbsp;
                                                                <asp:TextBox ID="txtSpeedVal1" CssClass="dropdownlist" runat="server" Width="50px"
                                                                    Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lblBetweenAnd" runat="server" Text=" and " Visible="false"></asp:Label>
                                                                                <asp:TextBox ID="txtSpeedVal2" CssClass="dropdownlist" runat="server" Width="50px"
                                                                                    Visible="false"></asp:TextBox>
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
																							
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                        <asp:TableRow>
                                                                            <asp:TableCell>
                                                                                <asp:Label ID="lblAccuracy" CssClass="label" runat="server" Visible="false">Accuracy</asp:Label>
                                                                            </asp:TableCell>
                                                                            <asp:TableCell Wrap="false">
                                                                                <asp:DropDownList ID="ddlAccuracyOperator" runat="server" Visible="false" CssClass="dropdownlist"
                                                                                    Width="150px" AutoPostBack="true">
                                                                                    <asp:ListItem Value="0">Select</asp:ListItem>
                                                                                    <asp:ListItem Value="1">Between</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Equal To</asp:ListItem>
                                                                                    <asp:ListItem Value="3">Greater Than</asp:ListItem>
                                                                                    <asp:ListItem Value="4">Greater Than Equal to</asp:ListItem>
                                                                                    <asp:ListItem Value="5">Less Than</asp:ListItem>
                                                                                    <asp:ListItem Value="6">Less Than Equal to</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                &nbsp;
                                                                <asp:TextBox ID="txtAccuracyVal1" CssClass="dropdownlist" runat="server" Width="50px"
                                                                    Visible="false"></asp:TextBox>
                                                                                <asp:Label ID="lblaccuracybetweenand" runat="server" Text=" and " Visible="false"></asp:Label>
                                                                                <asp:TextBox ID="txtAccuracyVal2" CssClass="dropdownlist" runat="server" Width="50px"
                                                                                    Visible="false"></asp:TextBox>
                                                                            </asp:TableCell>
                                                                            <asp:TableCell>
																							
                                                                            </asp:TableCell>
                                                                        </asp:TableRow>
                                                                    </asp:Table>
                                                                    <asp:Table ID="Table1" CssClass="label" runat="server" Width="100%">
                                                                    </asp:Table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" colspan="3" rowspan="6">
                                                                    <asp:Table ID="tblFilterOthers" CssClass="label" runat="server" Width="100%">
                                                                        <asp:TableRow>
                                                                            <asp:TableCell Width="29%"></asp:TableCell>
                                                                            <asp:TableCell Width="21%"></asp:TableCell>
                                                                            <asp:TableCell Width="25%"></asp:TableCell>
                                                                            <asp:TableCell Width="25%"></asp:TableCell>
                                                                        </asp:TableRow>
                                                                    </asp:Table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- end left side report params -->
                                    </td>
                                    <td class="rsspacerreport"></td>
                                    <td class="rsreport">
                                        <!-- begin right side -->
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contenttable">
                                            <tr>
                                                <td>
                                                    <!-- begin right side top -->
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contenttable">
                                                        <tr>
                                                            <td class="rtheaderreport">
                                                                <asp:Label ID="lblAdditionalInfo" runat="server" CssClass="label" Font-Bold="True">Step 3: Additional information to print on the report:</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="rtcontentreport">
                                                                <asp:CheckBox ID="chkRptFilters" runat="server" Text="Selected Filter criteria" CssClass="label"></asp:CheckBox><br>
                                                                <asp:CheckBox ID="chkRptSort" runat="server" Text="Selected Sort Order" CssClass="label"></asp:CheckBox><br>
                                                                <asp:CheckBox ID="chkRptDescrip" runat="server" Text="Report Description" CssClass="label"></asp:CheckBox><br>
                                                                <asp:CheckBox ID="chkRptInstructions" runat="server" Text="Report Instructions" CssClass="label"></asp:CheckBox><br>
                                                                <asp:CheckBox ID="chkRptNotes" runat="server" Text="Report Notes" CssClass="label"></asp:CheckBox><br>
                                                                <asp:CheckBox ID="chkRptCosts" runat="server" Text="Show Costs" CssClass="label"></asp:CheckBox><br>
                                                                <asp:CheckBox ID="chkRptExpectedFunding" runat="server" Text="Show Expected Funding"
                                                                    CssClass="label"></asp:CheckBox><br>
                                                                <asp:CheckBox ID="chkRptCategoryBreakdown" runat="server" Text="Show Category Breakdown"
                                                                    CssClass="label"></asp:CheckBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <!-- end right side top -->
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- begin right side bottom -->
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contenttable">
                                                        <tr>
                                                            <td colspan="2" class="rtheaderreport2">
                                                                <asp:Label ID="lblStep4" runat="server" CssClass="label" Font-Bold="True">Step 4: </asp:Label>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td class="rbdropdownreport">
                                                                <asp:DropDownList ID="ddlExportTo" runat="server" CssClass="dropdownlist">
                                                                    <asp:ListItem Value="pdf">Adobe Acrobat</asp:ListItem>
                                                                    <asp:ListItem Value="xls">Microsoft Excel</asp:ListItem>
                                                                    <asp:ListItem Value="doc">Microsoft Word</asp:ListItem>
                                                                    <asp:ListItem Value="xls1">Microsoft Excel (Data) </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="rbbutton">
                                                                
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" class="rtcontentreport">
                                                                <div class="margin-b-5px"></div>
                                                                <asp:Button ID="btnExport" Text="Export" runat="server"></asp:Button>
                                                                <div class="margin-b-5px"></div>
                                                                <asp:Button ID="btnSearch" Text="  View Report" runat="server"></asp:Button>
                                                                <div class="margin-b-5px"></div>
                                                                <asp:Button ID="btnFriendlyPrint" Visible="False" Text=" Printer Friendly Report"
                                                                    runat="server" Enabled="False"></asp:Button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <!-- end right side bottom -->
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- end right side -->
                                    </td>
                                </tr>
                            </table>
                            <!--end table content-->
                            <asp:LinkButton ID="lbtPrompt" runat="server" Width="0px"></asp:LinkButton>
                            <asp:Label ID="lblErrorMessage" runat="server" Font-Bold="True" EnableViewState="False" Font-Size="Medium" ForeColor="#FF8000"></asp:Label>
                            <asp:TextBox ID="txtRESID" runat="server" Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtPrefId" runat="server" Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtAllCampGrps" runat="server" Visible="false" />



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

