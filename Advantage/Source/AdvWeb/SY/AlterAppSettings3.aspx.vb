﻿Imports FAME.AdvantageV1.BusinessFacade
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports Telerik.Web.UI
Imports System.Data
Imports FAME.Advantage.Api.Library.Models
Imports FAME.Advantage.Common

Partial Class SY_AlterAppSettings3
    Inherits BasePage
    Public Shared connectionString As String
    Public SqlConnect As New SqlConnection()
    Public Shared dtTable As DataTable
    Public Shared dtTable_Source As New DataTable
    Public Shared blnRowAdded As Boolean
    Public SqlDataAdapter As New SqlDataAdapter()
    Public SQLDataSet_Source As New DataSet
    Public P_intSettingId As Integer
    Public P_strActualValue As String
    Public P_KeyName, P_Description, P_Value, P_Campus As String
    Public UpdateTrans As SqlClient.SqlTransaction
    Public P_blnExport As Boolean
    Protected MyAdvAppSettings As AdvAppSettings
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Place the data into a table
        connectionString = CType(MyAdvAppSettings.AppSettings("ConnectionString"), String)
        SqlConnect = New SqlConnection(connectionString)
        If IsPostBack = False Then
            Call LoadDataSource()
            lblNotification.Visible = False
            RadGrid1.GroupingSettings.CaseSensitive = False
        End If


        'This is from the msgbox that is called from SY/DisplayMessage for the deletion of Campus Specific Settings
        'when they are the same as the Global Value.
        If CampusSpecificDelete.Value = "yes" Then
            Call RemoveCampusSpecificWhereSameAsGlobal(CType(ViewState("SettingId"), Integer), CType(ViewState("ValueString"), String))
            Call UpdateItem()

            Call LoadDataSource()
            'Rebind the grid in the Load Complete page event.
            'Update the values in the singleton
            Call MyAdvAppSettings.RetrieveSettings()
            HttpContext.Current.Session("AdvAppSettings") = MyAdvAppSettings
            Exit Sub
        End If

        headerTitle.Text = Header.Title
    End Sub
    Public Sub LoadDataSource()
        dtTable_Source.Clear()

        If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()
        SqlConnect.Open()



        Try
            Dim daDataAdapter As New SqlDataAdapter("EXEC sp_GetAppSettings", SqlConnect)
            daDataAdapter.Fill(dtTable_Source)


        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        Finally
            If SqlConnect.State = ConnectionState.Open Then
                SqlConnect.Close()
            End If
        End Try



    End Sub
    'Private Sub SetDataSource()

    '    'Place the data into a dataset
    '    dtTable_Source.Clear()
    '    Dim daDataAdapter As New SqlDataAdapter("EXEC sp_GetAppSettings", SqlConnect)
    '    daDataAdapter.Fill(dtTable_Source)


    '    RadGrid1.MasterTableView.DataSource = dtTable_Source
    '    'Dim dbCon As New SqlConnection(SingletonAppSettings.AppSettings("ConnectionString"))

    '    'With SqlDataSource1
    '    '    .ConnectionString = connectionString
    '    '    .SelectCommandType = SqlDataSourceCommandType.StoredProcedure
    '    '    .SelectCommand = "sp_GetAppSettings"
    '    'End With

    'End Sub

    Protected Sub RadGrid1_DeleteCommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles RadGrid1.DeleteCommand
        'We will delete the campus specific selection here.
        'Since the user already confirmed the delete, we will just make it happen here.
        If (TypeOf e.Item Is GridDataItem) = True Then
            Dim strDelete As String
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim strValueId As String = item("ValueId").Text

            If SqlConnect.State = ConnectionState.Open Then
                SqlConnect.Close()
            End If

            SqlConnect.Open()

            strDelete = "DELETE FROM syConfigAppSetValues WHERE ValueId = '" & strValueId & "'"

            Try
                Dim myCommand As SqlCommand = New SqlCommand(strDelete, SqlConnect)
                myCommand.CommandType = CommandType.Text
                'Delete the record
                myCommand.ExecuteNonQuery()

                'Reload the grid
                Call LoadDataSource()
                RadGrid1.Rebind()

                'Update the values in the singleton
                Call MyAdvAppSettings.RetrieveSettings()
                HttpContext.Current.Session("AdvAppSettings") = MyAdvAppSettings

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            Finally
                If SqlConnect.State = ConnectionState.Open Then
                    SqlConnect.Close()
                End If
            End Try


        End If

    End Sub
    Protected Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs) Handles RadGrid1.NeedDataSource


        RadGrid1.MasterTableView.DataSource = dtTable_Source


        '************OLD***************
        'With SqlDataSource1
        '    .ConnectionString = connectionString
        '    .SelectCommandType = SqlDataSourceCommandType.StoredProcedure
        '    .SelectCommand = "sp_GetAppSettings"
        'End With

        'RadGrid1.MasterTableView.DataSource = SqlDataSource1

    End Sub
    Protected Sub RadGrid1_InsertCommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles RadGrid1.InsertCommand

        If (TypeOf e.Item Is GridDataItem) = True And (TypeOf e.Item Is GridDataInsertItem) = False Then
            Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)

            Session("SettingId") = item("SettingId").Text
            P_KeyName = item("KeyName").Text
            P_Description = item("Description").Text

            'Load the current setting from the template columns
            P_Value = CType(item("Value").FindControl("lblValue"), Label).Text
            P_Campus = CType(item("Campus").FindControl("lblCampus"), Label).Text

            'Here, we're going to add the new row into the datasource, then rebind the grid
            'Call AddNewRow_ToTable()
            Call AddNewRow_ToGrid()
        End If

        'For the INSERT
        If (TypeOf e.Item Is GridDataItem) = True And (TypeOf e.Item Is GridDataInsertItem) = True Then

            'Dim item As GridDataItem = DirectCast(e.Item, GridDataInsertItem)

            'We need to gather the Value and Campus
            Dim strValue As String = String.Empty
            Dim strCampusDescrip, strCampusIdentifier As String
            Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
            Dim dropDownListValue As DropDownList = DirectCast(gridEditFormItem("Value").FindControl("ddlValue"), DropDownList)
            'Dim dropDownList_Campus As DropDownList = DirectCast(gridEditFormItem("Campus").FindControl("ddlCampus"), DropDownList)
            Dim dropDownListCampus As RadComboBox = DirectCast(gridEditFormItem("Campus").FindControl("ddlCampus"), RadComboBox)
            Dim textboxValue As TextBox = DirectCast(gridEditFormItem("Value").FindControl("txtValue"), TextBox)

            If dropDownListValue.Visible = True Then
                strValue = dropDownListValue.Text
            End If

            If textboxValue.Visible = True Then
                strValue = textboxValue.Text
            End If

            'Now, grab the campus and convert to the uniqueidentifier if not ALL
            strCampusDescrip = dropDownListCampus.Text


            'Cancel the automatic insert
            If e.CommandName = RadGrid.PerformInsertCommandName Then
                e.Canceled = True
            End If

            'Perform the manual insert
            Dim strSqlInsert As String

            'Before we create the insert, we want to be sure that the value is not the same as the Global.
            If IsValueSameAsGlobal(CType(Session("SettingId"), Integer), strValue) = True Then

                Const strMessage As String = "You cannot create a Campus Specific setting with the same value as a Global setting." & vbCrLf _
                                             & "Please choose a different value or cancel the insert."

                CommonWebUtilities.DisplayErrorInMessageBox(Page, strMessage)

                Exit Sub
            End If



            If strCampusDescrip = "ALL" Then
                strSqlInsert = CType(("INSERT INTO syConfigAppSetValues (ValueId, SettingId, ModUser, ModDate, Active, Value, CampusId) " _
                                      & "VALUES('" & GenerateValueId() & "', '" & Session("SettingId") & "', 'sa', CONVERT(smalldatetime, GETDATE()), 1, '" & strValue & "', NULL)"), String)
            Else
                'Get the actual identifier from the campus description
                strCampusIdentifier = GetCampusIdentifier(strCampusDescrip)
                If DoesSettingCampusValueAlreadyExist(CType(Session("SettingId"), Integer), strCampusIdentifier) Then
                    'skip insert if the record already exists to stop re-inserts on refresh - should really be handled by a new radgrid with updated methods instead
                    RadGrid1.MasterTableView.IsItemInserted = False
                    Call LoadDataSource()
                    RadGrid1.Rebind()
                    Exit Sub
                Else
                    strSqlInsert = CType(("INSERT INTO syConfigAppSetValues (ValueId, SettingId, ModUser, ModDate, Active, Value, CampusId) " _
                                          & "VALUES('" & GenerateValueId() & "', '" & Session("SettingId") & "', 'sa', CONVERT(smalldatetime, GETDATE()), 1, '" & strValue & "', '" & strCampusIdentifier & "')"), String)
                End If
            End If

            If SqlConnect.State = ConnectionState.Open Then
                SqlConnect.Close()
            End If

            SqlConnect.Open()

            Dim myCommand As SqlCommand = New SqlCommand(strSqlInsert, SqlConnect)
            myCommand.CommandType = CommandType.Text

            Try


                'Writes the insert
                myCommand.ExecuteReader()

                'Now, take out of insert mode
                RadGrid1.MasterTableView.IsItemInserted = False

                'Now, reload the grid
                Call LoadDataSource()

                'Rebind the grid
                RadGrid1.Rebind()

                'Notify the user he must restart
                'lblNotification.Visible = True

                'Update the values in the singleton
                Call MyAdvAppSettings.RetrieveSettings()
                HttpContext.Current.Session("AdvAppSettings") = MyAdvAppSettings

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            Finally
                If SqlConnect.State = ConnectionState.Open Then
                    SqlConnect.Close()
                End If
            End Try

        End If

    End Sub
    Private Function IsValueSameAsGlobal(ByVal intSettingId As Integer, ByVal strValue As String) As Boolean
        Dim drSameValue As SqlDataReader
        Dim myCommand5 As SqlCommand = New SqlCommand("SELECT * FROM syConfigAppSetValues WHERE SettingId = '" & intSettingId & "' " _
        & "AND Value = '" & strValue & "' AND CampusId IS NULL", SqlConnect)
        Dim blnSameValue As Boolean

        myCommand5.CommandType = CommandType.Text

        If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()


        SqlConnect.Open()

        Try

            drSameValue = myCommand5.ExecuteReader

            While drSameValue.Read()
                blnSameValue = True
            End While

            Return blnSameValue

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        Finally
            If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()
        End Try
        Return False
    End Function

    Private Function IsValueSameAsCampusSpecific(ByVal intSettingId As Integer, ByVal strValue As String) As Boolean
        Dim drSameValue As SqlDataReader
        Dim myCommand5 As SqlCommand = New SqlCommand("SELECT * FROM syConfigAppSetValues WHERE SettingId = '" & intSettingId & "' " _
        & "AND Value = '" & strValue & "' AND CampusId IS NOT NULL", SqlConnect)
        Dim blnSameValue As Boolean

        myCommand5.CommandType = CommandType.Text

        If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()


        SqlConnect.Open()

        Try

            drSameValue = myCommand5.ExecuteReader

            While drSameValue.Read()
                blnSameValue = True
            End While

            Return blnSameValue

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        Finally
            If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()
        End Try
        Return False
    End Function
    Private Function DoesSettingCampusValueAlreadyExist(ByVal intSettingId As Integer, ByVal strCampusDescrip As String) As Boolean
        Dim drSameValue As SqlDataReader
        Dim myCommand5 As SqlCommand = New SqlCommand("SELECT * FROM syConfigAppSetValues WHERE SettingId = '" & intSettingId & "' " _
        & "AND CampusId = '" & strCampusDescrip & "'", SqlConnect)
        Dim blnSameValue As Boolean

        myCommand5.CommandType = CommandType.Text

        If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()


        SqlConnect.Open()

        Try

            drSameValue = myCommand5.ExecuteReader

            While drSameValue.Read()
                blnSameValue = True
            End While

            Return blnSameValue

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        Finally
            If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()
        End Try
        Return False

    End Function

    Private Function GetCampusIdentifier(ByVal strCampusDescrip As String) As String
        Dim drCampusId As SqlDataReader
        Dim myCommand2 As SqlCommand = New SqlCommand("SELECT CampusId FROM syCampuses WHERE CampDescrip = '" & strCampusDescrip & "'", SqlConnect)
        Dim strCampusIdentifier As String = String.Empty

        myCommand2.CommandType = CommandType.Text

        If SqlConnect.State = ConnectionState.Open Then
            SqlConnect.Close()
        End If

        SqlConnect.Open()

        drCampusId = myCommand2.ExecuteReader
        Try
            While drCampusId.Read() = True
                strCampusIdentifier = drCampusId("CampusId").ToString
            End While

            Return strCampusIdentifier

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        Finally
            If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()
            drCampusId.Dispose()
        End Try
        Return String.Empty
    End Function
    Public Sub AddNewRow_ToGrid()
        'We need to simulate a click on the add new record (UniqueId="Button_Add") and populate the 
        'data gathered in the RadGrid1_InsertCommand. The InitInsertCommandName command reference handles this.
        RadGrid1.MasterTableView.IsItemInserted = True
        RadGrid1.Rebind()

    End Sub

    Public Sub AddNewRow_ToTable()
        Dim strValueId As String = String.Empty
        Dim dtRow As DataRow = dtTable_Source.NewRow
        'This Sub will add the new row to the dataset in memory only.
        'First, we need to retrieve a new ValueId from the database so we can add that to our row.
        Dim drValueId As SqlDataReader
        Dim myCommand As SqlCommand = New SqlCommand("SELECT NEWID() as ValueId", SqlConnect)
        myCommand.CommandType = CommandType.Text

        If SqlConnect.State = ConnectionState.Closed Then
            SqlConnect.Open()
        End If
        Try

            drValueId = myCommand.ExecuteReader

            While drValueId.Read() = True
                strValueId = drValueId("ValueId").ToString
            End While

            'Now that we have the new ValueId, let's add the info for the new row to the public dataset.
            dtRow("ValueId") = strValueId
            dtRow("SettingId") = Session("SettingId")
            dtRow("Campus") = P_Campus
            dtRow("KeyName") = P_KeyName
            dtRow("Value") = P_Value
            dtRow("Description") = P_Description

            dtTable_Source.Rows.Add(dtRow)

            blnRowAdded = True

            If SqlConnect.State = ConnectionState.Open Then
                SqlConnect.Close()
            End If

            'Now, we need to sort and rebind the grid
            Const columnKey As String = "KeyName"
            Const sortDirection As String = "ASC"
            Const sortFormat As String = "{0} {1}"
            'Initiates the sort on the datatable
            dtTable_Source.DefaultView.Sort = String.Format(sortFormat, columnKey, sortDirection)
            'Rebind the grid
            RadGrid1.Rebind()

            'Now, we need to put the new row into edit mode



        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        Finally
            If SqlConnect.State = ConnectionState.Open Then
                SqlConnect.Close()
            End If
        End Try

    End Sub
    Protected Sub RadGrid1_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrid1.ItemDataBound
        dtTable = New DataTable()
        Try

            'This will trigger whenever the grid is loaded or rebound
            'We will use this to set our Add and Delete buttons
            If (TypeOf e.Item Is GridDataItem) And e.Item.IsInEditMode = False Then
                Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
                Dim intSettingId As Integer = CType(item.DataItem("SettingId"), Integer)
                Dim blnCampusSpecific As Boolean = CType(item.DataItem("CampusSpecific"), Boolean)
                Dim labelCampus As Label = DirectCast(item("Campus").FindControl("lblCampus"), Label)

                If intSettingId <> 0 Then
                    If GetAssignedCampuses(intSettingId).Tables(0).Rows.Count = RetrieveAllCampuses.Tables(0).Rows.Count Then
                        If labelCampus.Text = "ALL" Then
                            item("Button_Add").Controls(0).Visible = False
                        Else
                            item("Button_Add").Controls(0).Visible = False
                        End If
                    Else
                        If labelCampus.Text = "ALL" And blnCampusSpecific = True Then
                            item("Button_Add").Controls(0).Visible = True
                        Else
                            item("Button_Add").Controls(0).Visible = False
                        End If
                    End If
                End If

                'Need to hide the Delete button for items that have the campus of all (Global)
                Dim strCampus As String = CType(item.DataItem("Campus"), String)
                If strCampus = "ALL" Then
                    item("Button_Delete").Controls(0).Visible = False
                Else
                    item("Button_Delete").Controls(0).Visible = True
                End If

                'Add the confirmation box to the delete button
                Dim deleteButton As ImageButton = CType(item("Button_Delete").Controls(0), ImageButton)
                Dim addButton As ImageButton = CType(item("Button_Add").Controls(0), ImageButton)
                deleteButton.Attributes("onclick") = CType(("return confirm('Are you sure you want to delete this campus specific setting for Key: " & item.DataItem("KeyName") & "?')"), String)

                'Add the Tooltips
                addButton.ToolTip = "Add"
                deleteButton.ToolTip = "Delete"


                item("KeyName").ToolTip = item.DataItem("KeyName").ToString
                item("Description").ToolTip = item.DataItem("Description").ToString
                item("Campus").ToolTip = item.DataItem("Campus").ToString

                Dim labelValue As Label = DirectCast(item("Value").FindControl("lblValue"), Label)
                labelValue.ToolTip = labelValue.Text
            End If

            'Bind the dropdownlist or textbox to the Value column. Fill the dropdown with possible values, if they exist
            'or else just display a textbox with the currently assigned value.
            If (TypeOf e.Item Is GridDataItem) = True And e.Item.IsInEditMode = True And (TypeOf e.Item Is GridDataInsertItem) = False Then

                Dim item As GridEditableItem = DirectCast(e.Item, GridEditableItem)
                Session("SettingId") = item.DataItem("SettingId").ToString
                Session("ValueSettingId") = item.DataItem("ValueId").ToString

                Dim selectQuery As String = CType(("sp_GetKeyValues_Lookup " & Session("SettingId")), String)

                If SqlConnect.State = ConnectionState.Open Then
                    SqlConnect.Close()
                End If

                SqlConnect.Open()

                SqlDataAdapter.SelectCommand = New SqlCommand(selectQuery, SqlConnect)
                SqlDataAdapter.Fill(dtTable)


                Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                'Load the current setting into the dropdownlist or textbox
                Dim dropDownList1 As DropDownList = DirectCast(gridEditFormItem("Value").FindControl("ddlValue"), DropDownList)
                Dim textbox As TextBox = DirectCast(gridEditFormItem("Value").FindControl("txtValue"), TextBox)

                If dtTable.Rows.Count > 0 Then
                    dropDownList1.Items.Clear()
                    dropDownList1.DataSource = dtTable
                    dropDownList1.DataTextField = "ValueOptions"
                    dropDownList1.DataValueField = "ValueOptions"
                    If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                        dropDownList1.SelectedValue = DirectCast((gridEditFormItem.DataItem), DataRowView).Row.ItemArray(4).ToString()
                    End If
                    dropDownList1.DataBind()
                End If

                If dtTable.Rows.Count > 0 Then
                    dropDownList1.Visible = True
                    textbox.Visible = False
                    Session("Old_Value") = dropDownList1.Text
                Else
                    dropDownList1.Visible = False
                    textbox.Visible = True
                    Session("Old_Value") = textbox.Text
                End If
            End If


            'Bind the dropdownlist to the Campus column. Fill the dropdown with possible campuses.
            If (TypeOf e.Item Is GridDataItem) = True And e.Item.IsInEditMode = True And (TypeOf e.Item Is GridDataInsertItem) = False Then

                'Dim item As GridEditableItem = DirectCast(e.Item, GridEditableItem)

                Const selectQuery As String = "sp_RetrieveCampuses 1"

                If SqlConnect.State = ConnectionState.Open Then
                    SqlConnect.Close()
                End If

                SqlConnect.Open()

                SqlDataAdapter.SelectCommand = New SqlCommand(selectQuery, SqlConnect)
                dtTable.Clear()
                SqlDataAdapter.Fill(dtTable)


                'We need to remove the campuses from dtTable that already exist for this key
                Dim dsReturn As DataSet
                dsReturn = GetAssignedCampuses(CType(Session("SettingId"), Integer))

                ' Loop though the table and remove the campuses that already exist for the key
                Dim strRowString As String
                For Each dr As DataRow In dtTable.Rows
                    strRowString = CType(dr("CampusDescrip"), String)
                    Dim i As Integer
                    For i = 0 To dsReturn.Tables(0).Rows.Count - 1
                        If strRowString = dsReturn.Tables(0).Rows(i).Item("CampDescrip").ToString Then
                            dr.Delete()
                        End If
                    Next
                Next

                dtTable.AcceptChanges()


                Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                'Load the current setting into the dropdownlist or textbox
                'Dim dropDownList_Campus As DropDownList = DirectCast(gridEditFormItem("Campus").FindControl("ddlCampus"), DropDownList)
                Dim dropDownListCampus As RadComboBox = DirectCast(gridEditFormItem("Campus").FindControl("ddlCampus"), RadComboBox)

                'Add the current selection back in so we can set the SelectedValue
                Dim strSelection As String = DirectCast((gridEditFormItem.DataItem), DataRowView).Row.ItemArray(2).ToString()
                Dim drCampus As DataRow = dtTable.NewRow
                drCampus("CampusDescrip") = strSelection
                dtTable.Rows.Add(drCampus)

                If dtTable.Rows.Count > 0 Then
                    dropDownListCampus.Items.Clear()
                    dropDownListCampus.DataSource = dtTable
                    dropDownListCampus.DataTextField = "CampusDescrip"
                    dropDownListCampus.DataValueField = "CampusDescrip"
                    If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                        dropDownListCampus.SelectedValue = DirectCast((gridEditFormItem.DataItem), DataRowView).Row.ItemArray(2).ToString()
                    End If
                    dropDownListCampus.DataBind()

                    'Add the tooltip for items in the Campus dropdown
                    Dim i As Integer
                    For i = 0 To dropDownListCampus.Items.Count - 1
                        'dropDownList_Campus.Items(i).Attributes.Add("title", dropDownList_Campus.Items(i).ToString)
                        dropDownListCampus.Items(i).ToolTip = dropDownListCampus.Items(i).Text
                    Next

                End If

                If dtTable.Rows.Count > 0 Then
                    dropDownListCampus.Visible = True
                    Session("Old_Campus") = dropDownListCampus.SelectedValue
                End If

                'If the campus is set to ALL, lock down the control.
                If dropDownListCampus.SelectedValue = "ALL" Then
                    dropDownListCampus.Enabled = False
                Else
                    'We need to remove ALL from the ddl for the campuses since ALL is a Global setting
                    'dropDownList_Campus.Items.Remove("ALL")
                    Dim raditem As RadComboBoxItem = dropDownListCampus.FindItemByText("ALL")
                    If Not raditem Is Nothing Then raditem.Remove()
                    dropDownListCampus.Enabled = True
                End If

                'if all the campuses have been assigned, then disable the campus ddl
                If GetAssignedCampuses(CType(Session("SettingId"), Integer)).Tables(0).Rows.Count = RetrieveAllCampuses.Tables(0).Rows.Count Or dropDownListCampus.Text = "ALL" Then
                    dropDownListCampus.Enabled = False
                Else
                    dropDownListCampus.Enabled = True
                End If
                If dropDownListCampus.SelectedValue = "ALL" Then
                    dropDownListCampus.Enabled = False
                End If

            End If





            '**This is used for the Insert ONLY
            If (TypeOf e.Item Is GridDataItem) = True And e.Item.IsInEditMode = True And (TypeOf e.Item Is GridDataInsertItem) = True Then
                'Add the data into the insert line
                'For Insert items
                Dim item As GridDataInsertItem = DirectCast(e.Item, GridEditableItem)
                'For Data items
                Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                'Load the current setting into the dropdownlist or textbox
                Dim selectQuery As String = CType(("sp_GetKeyValues_Lookup " & Session("SettingId")), String)

                If SqlConnect.State = ConnectionState.Open Then
                    SqlConnect.Close()
                End If

                SqlConnect.Open()

                SqlDataAdapter.SelectCommand = New SqlCommand(selectQuery, SqlConnect)
                SqlDataAdapter.Fill(dtTable)
                SqlDataAdapter.Dispose()

                Dim dropDownList1 As DropDownList = DirectCast(gridEditFormItem("Value").FindControl("ddlValue"), DropDownList)
                Dim textbox As TextBox = DirectCast(gridEditFormItem("Value").FindControl("txtValue"), TextBox)

                If dtTable.Rows.Count > 0 Then
                    dropDownList1.Items.Clear()
                    dropDownList1.DataSource = dtTable
                    dropDownList1.DataTextField = "ValueOptions"
                    dropDownList1.DataValueField = "ValueOptions"
                    If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                        dropDownList1.SelectedValue = DirectCast((gridEditFormItem.DataItem), DataRowView).Row.ItemArray(4).ToString()
                    End If
                    dropDownList1.DataBind()
                End If

                If dtTable.Rows.Count > 0 Then
                    dropDownList1.Visible = True
                    textbox.Visible = False
                    Session("Old_Value") = dropDownList1.Text
                Else
                    dropDownList1.Visible = False
                    textbox.Visible = True
                    Session("Old_Value") = P_Value
                    textbox.Text = P_Value
                End If

                'Let's also add the KeyName, Description, and ValueId
                item("KeyName").Text = P_KeyName
                item("Description").Text = P_Description
                item("ValueId").Text = GenerateValueId()
            End If

            '**This is used for the Insert ONLY
            'For Campuses in the dropdown
            If (TypeOf e.Item Is GridDataItem) = True And e.Item.IsInEditMode = True And (TypeOf e.Item Is GridDataInsertItem) = True Then
                'Add the data into the insert line
                'For Data items
                Dim gridEditFormItem2 As GridDataItem = DirectCast(e.Item, GridDataItem)

                Const selectQuery2 As String = "sp_RetrieveCampuses 1"
                Dim sqlDataAdapter2 As New SqlDataAdapter()

                If SqlConnect.State = ConnectionState.Open Then
                    SqlConnect.Close()
                End If

                SqlConnect.Open()

                sqlDataAdapter2.SelectCommand = New SqlCommand(selectQuery2, SqlConnect)
                dtTable.Clear()
                sqlDataAdapter2.Fill(dtTable)

                'We need to remove the campuses from dtTable that already exist for this key
                Dim dsReturn As DataSet
                dsReturn = GetAssignedCampuses(CType(Session("SettingId"), Integer))

                ' Loop though the table and remove the campuses that already exist for the key
                Dim strRowString As String
                For Each dr As DataRow In dtTable.Rows
                    strRowString = CType(dr("CampusDescrip"), String)
                    Dim i As Integer
                    For i = 0 To dsReturn.Tables(0).Rows.Count - 1
                        If strRowString = dsReturn.Tables(0).Rows(i).Item("CampDescrip").ToString Then
                            dr.Delete()
                        End If
                    Next
                Next

                dtTable.AcceptChanges()

                'Load the current setting into the dropdownlist or textbox
                'Dim dropDownList2 As DropDownList = DirectCast(gridEditFormItem2("Campus").FindControl("ddlCampus"), DropDownList)
                Dim dropDownList2 As RadComboBox = DirectCast(gridEditFormItem2("Campus").FindControl("ddlCampus"), RadComboBox)

                If dtTable.Rows.Count > 0 Then
                    dropDownList2.Items.Clear()
                    dropDownList2.DataSource = dtTable
                    dropDownList2.DataTextField = "CampusDescrip"
                    dropDownList2.DataValueField = "CampusDescrip"
                    If Not TypeOf gridEditFormItem2.DataItem Is GridInsertionObject Then
                        dropDownList2.SelectedValue = DirectCast((gridEditFormItem2.DataItem), System.Data.DataRowView).Row.ItemArray(2).ToString()
                    End If
                    dropDownList2.DataBind()
                End If

                If dtTable.Rows.Count > 0 Then
                    dropDownList2.Visible = True
                    Session("Old_Campus") = dropDownList2.Text
                End If

            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        Finally
            SqlConnect.Close()
        End Try

    End Sub
    Public Function RetrieveAllCampuses() As DataSet

        Dim dtRetrieveCampuses As New DataSet
        Const selectQuery2 As String = "sp_RetrieveCampuses 1"
        Dim sqlDataAdapter2 As New SqlDataAdapter()

        If SqlConnect.State = ConnectionState.Open Then
            SqlConnect.Close()
        End If

        SqlConnect.Open()

        sqlDataAdapter2.SelectCommand = New SqlCommand(selectQuery2, SqlConnect)
        dtTable.Clear()
        sqlDataAdapter2.Fill(dtRetrieveCampuses)

        Return dtRetrieveCampuses
    End Function
    Private Function GenerateValueId() As String
        Dim strValueId As String = String.Empty
        Dim drValueId As SqlDataReader
        Dim myCommand As SqlCommand = New SqlCommand("SELECT NEWID() as ValueId", SqlConnect)
        myCommand.CommandType = CommandType.Text

        If SqlConnect.State = ConnectionState.Open Then
            SqlConnect.Close()
        End If

        SqlConnect.Open()

        drValueId = myCommand.ExecuteReader

        While drValueId.Read() = True
            strValueId = drValueId("ValueId").ToString
        End While
        Return strValueId
    End Function

    Public Function GetAssignedCampuses(ByVal intSettingId As Integer) As DataSet

        'Dim dtRow As DataRow = GetAssignedCampuses.NewRow
        Dim dsReturn As New DataSet
        Dim strSqlString As String

        If SqlConnect.State = ConnectionState.Open Then
            SqlConnect.Close()
        End If

        SqlConnect.Open()

        strSqlString = "SELECT " _
         & "COALESCE((SELECT CampDescrip FROM syCampuses B WHERE B.CampusId = A.CampusId), 'ALL') as CampDescrip " _
         & "FROM syConfigAppSetValues A WHERE A.SettingId = '" & intSettingId & "'"

        Try

            Dim sqlAdapter As New SqlDataAdapter(strSqlString, SqlConnect)
            sqlAdapter.Fill(dsReturn)

            Return dsReturn

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        Finally
            SqlConnect.Close()
        End Try
        Return Nothing

    End Function
    Protected Sub RadGrid1_UpdateCommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles RadGrid1.UpdateCommand
        'Updates the table
        'Dim item As GridEditableItem = DirectCast(e.Item, GridEditableItem)
        Dim strActualValue As String = String.Empty
        Dim strCampus, strCampusId As String ', strValueId
        Dim modUser As String

        'Gather the value setting
        Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim dropDownListValue As DropDownList = DirectCast(gridEditFormItem("Value").FindControl("ddlValue"), DropDownList)
        Dim textboxValue As TextBox = DirectCast(gridEditFormItem("Value").FindControl("txtValue"), TextBox)

        P_intSettingId = CType(Session("SettingId"), Integer)

        If dropDownListValue.Visible = True Then
            strActualValue = dropDownListValue.Text
        End If

        If textboxValue.Visible = True Then
            strActualValue = textboxValue.Text
        End If


        'Gather the Campus Setting
        'Dim dropDownList_Campus As DropDownList = DirectCast(gridEditFormItem("Campus").FindControl("ddlCampus"), DropDownList)
        Dim dropDownListCampus As RadComboBox = DirectCast(gridEditFormItem("Campus").FindControl("ddlCampus"), RadComboBox)
        strCampus = dropDownListCampus.Text


        'If the user changes a campus specific setting to the same as Global, do not allow and flag user.
        If strCampus <> "ALL" Then
            If IsValueSameAsGlobal(P_intSettingId, strActualValue) = True Then

                e.Canceled = True
                RadGrid1.MasterTableView.ClearEditItems()
                Call LoadDataSource()
                RadGrid1.Rebind()

                Dim strMessage As String = "You cannot update a Campus Specific setting with the same value as a Global setting." & Environment.NewLine & "Please choose a different value."

                CommonWebUtilities.DisplayErrorInMessageBox(Page, strMessage)

                Exit Sub
            End If
        End If

        Dim updateQuery As String = String.Empty

        'Open the SqlConnect
        SqlConnect.Open()
        modUser = HttpContext.Current.Session("UserName").ToString()


        'Include the campus in the update and check if the value exists
        If strCampus = "ALL" And strActualValue <> "" Then
            updateQuery = CType(("UPDATE syConfigAppSetValues set Value='" & strActualValue & "', CampusId = NULL , ModDate = CONVERT(smalldatetime, GETDATE()), ModUser = '" & modUser & "' where ValueId='" & Session("ValueSettingId") & "'"), String)
        ElseIf strCampus <> "ALL" And strActualValue <> "" Then
            'We need to get the uniqueidentifier for the campus, then build the update string
            strCampusId = GetCampusIdentifier(strCampus)
            updateQuery = CType(("UPDATE syConfigAppSetValues set Value='" & strActualValue & "', CampusId = '" & strCampusId & "', ModDate = CONVERT(smalldatetime, GETDATE()), ModUser = '" & modUser & "' where ValueId='" & Session("ValueSettingId") & "'"), String)
        ElseIf strCampus = "ALL" And strActualValue = "" Then
            updateQuery = CType(("UPDATE syConfigAppSetValues set Value= NULL, CampusId = NULL , ModDate = CONVERT(smalldatetime, GETDATE()), ModUser = '" & modUser & "' where ValueId='" & Session("ValueSettingId") & "'"), String)
        ElseIf strCampus <> "ALL" And strActualValue = "" Then
            'We need to get the uniqueidentifier for the campus, then build the update string
            strCampusId = GetCampusIdentifier(strCampus)
            updateQuery = CType(("UPDATE syConfigAppSetValues set Value= NULL, CampusId = '" & strCampusId & "' , ModDate = CONVERT(smalldatetime, GETDATE()), ModUser = '" & modUser & "' where ValueId='" & Session("ValueSettingId") & "'"), String)
        End If


        If Session("Old_Value").ToString() <> strActualValue Then
            lblNotification.Visible = True
        End If

        'If the Global Setting value is changed to a campus specific value, ask the user if they would
        'like to remove the campus specific setting(s).
        If strCampus = "ALL" Then
            If IsValueSameAsCampusSpecific(P_intSettingId, strActualValue) = True Then

                'Dim winSettings As String = "toolbar=no,status=no,width=450px,height=150px,left=450px,top=400px,modal=yes"
                'Dim name As String = "DeleteCampusSpecific"


                Const strMessage As String = "There are also child Campus Specific Settings with this new Value." & vbCrLf _
                                             & "Selecting 'Continue' will remove these child Campus Specific Settings."

                CommonWebUtilities.DisplayErrorInMessageBox(Page, strMessage)
                e.Canceled = True


                'Set the view state so it's values will be retained for the call in pageload.
                ViewState("SettingId") = P_intSettingId
                ViewState("ValueString") = strActualValue
                ViewState("UpdateQuery") = updateQuery
                RadGrid1.MasterTableView.ClearEditItems()
                Call LoadDataSource()
                RadGrid1.Rebind()
                Exit Sub
                '' GoTo UPDATESETTING
            Else
                GoTo UPDATESETTING
            End If
        Else
UPDATESETTING:
            Try
                'Close the connection if it is open
                If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()
                're-open the connection
                SqlConnect.Open()

                'Create an update transaction
                UpdateTrans = SqlConnect.BeginTransaction
                Dim sqlCommand As New SqlCommand
                'Update Query to update the Datatable
                sqlCommand.Connection = SqlConnect
                sqlCommand.Transaction = UpdateTrans

                sqlCommand.CommandText = updateQuery

                sqlCommand.ExecuteNonQuery()
                UpdateTrans.Commit()
                'Close the SqlConnect
                SqlConnect.Close()
                e.Canceled = True
                RadGrid1.MasterTableView.ClearEditItems()
                Call LoadDataSource()
                RadGrid1.Rebind()
                'call api
                Dim campusId = AdvantageSession.UserState.CampusId.ToString
                Dim token As TokenResponse
                token = CType(Session("AdvantageApiToken"), TokenResponse)
                Dim SchoolLogo As New FAME.Advantage.Api.Library.SystemCatalog.SchoolLogo(token.ApiUrl, token.Token)
                If (Not String.IsNullOrEmpty(campusId)) Then
                    SchoolLogo.ClearSchoolCash(campusId)
                End If

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                RadGrid1.Controls.Add(New LiteralControl("Unable to update syConfigAppSetValues. Reason: " + ex.Message))
                e.Canceled = True
            Finally
                If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()
            End Try
        End If


        'Update the values in the singleton
        Call MyAdvAppSettings.RetrieveSettings()
        HttpContext.Current.Session("AdvAppSettings") = MyAdvAppSettings

        'Clear Cache

        Dim facade As CampusGroupsFacade = New CampusGroupsFacade()
        Dim guids() As Guid = facade.GetAllCampusGroupsIdByUser(AdvantageSession.UserId)
        Dim i As Integer = 0
        While i <= guids.Length - 1
            Dim campId As String = guids(i).ToString()
            Cache.Remove("CommonTask" & campId)
            Cache.Remove("Maintenance" & campId)
            Cache.Remove("Report" & campId)
            Cache.Remove("Tabs" & campId)
            Cache.Remove("Tools" & campId)
            i += 1
        End While

        '------------------------------------------------------------
        ' Clear the session storage when the user changes a config
        ' setting so that the changes to exclusion of menu items are
        ' seen immediately without closing the browser first
        '------------------------------------------------------------
        Page.ClientScript.RegisterStartupScript([GetType](), "MyKey", "clearSessionStorage();", True)

    End Sub

    Private Sub UpdateItem()
        Try

            'Close the connection if it is open
            If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()
            're-open the connection
            SqlConnect.Open()

            'Create an update transaction
            UpdateTrans = SqlConnect.BeginTransaction
            Dim sqlCommand As New SqlCommand
            'Update Query to update the Datatable
            sqlCommand.Connection = SqlConnect
            sqlCommand.Transaction = UpdateTrans

            sqlCommand.CommandText = CType(ViewState("UpdateQuery"), String)

            sqlCommand.ExecuteNonQuery()
            UpdateTrans.Commit()
            'Close the SqlConnect
            SqlConnect.Close()
            RadGrid1.MasterTableView.ClearEditItems()
            Call LoadDataSource()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            RadGrid1.Controls.Add(New LiteralControl("Unable to update syConfigAppSetValues. Reason: " + ex.Message))
        Finally
            If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()
        End Try
    End Sub

    Protected Sub RemoveCampusSpecificWhereSameAsGlobal(ByVal intSettingId As Integer, ByVal strValue As String)

        Dim deleteQuery As String = "DELETE FROM syConfigAppSetValues WHERE SettingId = '" & intSettingId & "' " _
        & "AND Value = '" & strValue & "' AND CampusId IS NOT NULL"

        Dim sqlCommand As New SqlCommand
        'Update Query to update the Datatable
        sqlCommand.CommandText = deleteQuery
        sqlCommand.Connection = SqlConnect

        If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()

        SqlConnect.Open()

        Try
            sqlCommand.ExecuteNonQuery()


        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        Finally
            If SqlConnect.State = ConnectionState.Open Then SqlConnect.Close()
        End Try

    End Sub

    Protected Sub RadGrid1_SortCommand(ByVal source As Object, ByVal e As GridSortCommandEventArgs) Handles RadGrid1.SortCommand

        With SqlDataSource1
            .ConnectionString = connectionString
            .SelectCommandType = SqlDataSourceCommandType.StoredProcedure
            .SelectCommand = "sp_GetAppSettings"
        End With

        e.Canceled = True
        Dim expression As New GridSortExpression()
        expression.FieldName = "KeyName"

        If (e.OldSortOrder = GridSortOrder.None Or e.OldSortOrder = GridSortOrder.Ascending) Then
            expression.SortOrder = GridSortOrder.Descending

            Dim dv As DataView = CType(SqlDataSource1.Select(New DataSourceSelectArguments), DataView)
            'Dim dt As New DataTable
            'dt = dv.ToTable()
            dv.Sort = "KeyName Desc"
            'RadGrid1.MasterTableView.DataSource = dv
            e.Item.OwnerTableView.DataSource = dv
            e.Item.OwnerTableView.SortExpressions.AddSortExpression(expression)
            e.Item.OwnerTableView.Rebind()
        Else
            expression.SortOrder = GridSortOrder.Ascending

            Dim dv As DataView = CType(SqlDataSource1.Select(New DataSourceSelectArguments), DataView)
            'Dim dt As New DataTable
            'dt = dv.ToTable()
            dv.Sort = "KeyName ASC"
            'RadGrid1.MasterTableView.DataSource = dv
            e.Item.OwnerTableView.DataSource = dv
            e.Item.OwnerTableView.SortExpressions.AddSortExpression(expression)
            e.Item.OwnerTableView.Rebind()
        End If

    End Sub

    Protected Sub btnExporttoPDF_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExporttoPDF.Click

        P_blnExport = True

        RadGrid1.ExportSettings.FileName = "Export Config Settings"
        RadGrid1.MasterTableView.GridLines = GridLines.Both
        RadGrid1.ExportSettings.IgnorePaging = True
        RadGrid1.ExportSettings.OpenInNewWindow = True
        RadGrid1.MasterTableView.Columns.FindByUniqueName("Edit").Visible = False
        RadGrid1.MasterTableView.Columns.FindByUniqueName("Button_Delete").Visible = False
        RadGrid1.MasterTableView.Columns.FindByUniqueName("Button_Add").Visible = False
        RadGrid1.MasterTableView.ExportToPdf()
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As EventArgs) Handles Me.LoadComplete
        RadGrid1.Rebind()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'Shrink the Description column so it will fit on the PDF during Export
        If P_blnExport = True Then
            Dim myColumn As GridColumn = RadGrid1.MasterTableView.GetColumn("Description")
            myColumn.HeaderStyle.Width = Unit.Pixel(260)

            P_blnExport = False
        End If

    End Sub
End Class
