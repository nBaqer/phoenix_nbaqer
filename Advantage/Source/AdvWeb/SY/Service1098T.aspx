﻿<%@ Page Title="1098T Service" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Service1098T.aspx.vb" Inherits="AdvWeb.SY.Service1098T" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../Scripts/Advantage.Client.SY.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <style type="text/css">
        a {
            text-decoration: underline !important;
        }

        #ftpHeader {
            width: 100%;
            background-color: gainsboro
        }

            #ftpHeader div {
                width: 100%;
                font-size: 28px;
                margin-left: 5px;
            }



        #kendoContainer {
            margin-left: 5px;
        }

        /*#TResult {
		width: 600px;
		height: 200px;
		border: 2px solid gainsboro;
	}*/

        #SendBy {
            height: 30px;
            margin-left: 10px;
            width: 100px;
        }
    </style>


    <div class="boxContainer">
        <h3><%=Header.Title  %></h3>
        <section id="kendoContainer">

            <div>
                <table>
                    <tr>
                        <td style="font-weight: bold; font-size: 10pt;">This tool should only be run from January 1st to April 30th.  Data sent during any other time will NOT be processed by FAME.
                     <br />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bold; ">To request a duplicate or make corrections to a 1098T, click 
                        <a style="font-weight: bold; " href="https://support.fameinc.com/hc/en-us/articles/115002597811" target="_blank">here</a>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td>All student data, including demographic information, attendance and ledger activity must be entered in Advantage for the tax year selected
                        <br />
                            <br />
                            The 1098T processing software will gather all data required and transmit the data to FAME.  Then FAME will:
                        <ul>
                            <li>review your data, notify you of any corrections needed, and process those corrections,</li>
                            <li>generate the 1098T forms, and mail them to your students,</li>
                            <li>electronically submit all 1098T's to the IRS,</li>
                            <li>provide the school a 1098T Summary Status Report and a copy of each 1098T reported.</li>
                        </ul>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <b>Instructions:</b>
                            <ol>
                                <li>Verify the Transaction Types that have been selected for the 1098T process are accurate.  
                                   To access this information navigate to Maintenance\Student Accounts\Transaction Types and select the Transaction Type on the left to review. 
                                Once selected, confirm the 1098T check box is populated. If the box is not checked, the Transaction Type will not be included in the upload.</li>
                                <li>Verify the Programs selected for the 1098T process are accurate.  
                                To access this information, navigate to Maintenance\Academics\Programs and select the "Include in 1098T extract" check box.
                                If the box is not checked, the Program will not be included in the upload.</li>
                                <li>Select "View Exceptions" to review data that will not be included in 1098T processing.</li>
                                <li>Select check boxes to confirm Programs and Transaction Types have been verified.</li>
                                <li>Select the calendar year for 1098T processing.</li>
                                <li>Select "Upload" to send data to FAME.</li>
                            </ol>
                            <input id="checkbox1" type="checkbox" value="check1" /><label for="checkbox1">I have verified that all Program Versions, to be included in the FAME 1098T extract, are selected.</label>
                            <br />
                            <input id="checkbox2" type="checkbox" value="check2" /><label for="checkbox2">I have verified that all Transaction Types, to be included in the FAME 1098T extract, are selected.</label>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div>
                <asp:Button ID="btnExceptions" runat="server" Text="View Exceptions" OnClick="btnExceptions_Click" class="k-button" Style="display: inline-block; margin-left: 8px; vertical-align: text-top;" />
                <input id="FtpYearCombobox" style="display: inline-block; margin-left: 8px; vertical-align: text-top;" />
                <button id="T1098Go" type="button" class="k-button" style="display: inline-block; margin-left: 8px; vertical-align: text-top;">
                    Upload</button>

            </div>

        </section>
    </div>
    <%-- ReSharper disable UnusedLocals --%>
    <script type="text/javascript">

        $(document).ready(function () {

            var ftpService = new Sy1098T.Service1098T();

        });
    </script>
    <%-- ReSharper restore UnusedLocals --%>
</asp:Content>

