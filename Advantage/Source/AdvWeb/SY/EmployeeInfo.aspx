﻿<%--<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>--%>

<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="EmployeeInfo.aspx.vb" Inherits="EmployeeInfo" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%" border="0">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                            <!-- begin table content-->
                                            <asp:Panel ID="pnlRHS" runat="server">
                                                <table class="contentleadmastertable" cellspacing="0" cellpadding="0" width="100%">
                                                    <asp:TextBox ID="txtEmpId" runat="server" Visible="False"></asp:TextBox>
                                                    <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                                    <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                                    <asp:DropDownList ID="ddlEmpId" runat="server" Visible="False">
                                                    </asp:DropDownList>
                                                    <tr>
                                                        <td class="contentcellheader" nowrap colspan="8" style="border-top: 0; border-left: 0; border-right: 0">
                                                            <asp:Label ID="lblgeneral" runat="server" CssClass="label" Font-Bold="true">General Information</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="leadcell4columnleftei" nowrap style="padding-top: 16px">
                                                            <asp:Label ID="lblFirstName" CssClass="label" runat="server">First Name</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnei" nowrap style="padding-top: 16px">
                                                            <asp:TextBox ID="txtFirstName" TabIndex="1" CssClass="textbox" runat="server" MaxLength="128"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Enabled="False"
                                                                ControlToValidate="txtFirstName" ErrorMessage="First Name can not be blank" Display="None">First Name can not be blank</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="leadcell4columnei" nowrap style="padding-top: 16px">
                                                            <asp:Label ID="lblPrefix" runat="server" CssClass="label">Prefix</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnrightei" nowrap style="padding-top: 16px">
                                                            <asp:DropDownList ID="ddlPrefixId" TabIndex="8" runat="server" CssClass="dropdownlist" Width="200px" >
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="leadcell4columnleftei" nowrap>
                                                            <asp:Label ID="lblMI" runat="server" CssClass="label">Middle Name</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnei">
                                                            <asp:TextBox ID="txtMI" TabIndex="2" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <asp:CheckBox ID="cbxIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                                        </td>
                                                        <td class="leadcell4columnei">
                                                            <asp:Label ID="lblSuffix" runat="server" CssClass="label">Suffix</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnrightei">
                                                            <asp:DropDownList ID="ddlSuffixId" TabIndex="9" runat="server" CssClass="dropdownlist" Width="200px" >
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="leadcell4columnleftei" nowrap>
                                                            <asp:Label ID="lblLastName" runat="server" CssClass="label">Last Name</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnei">
                                                            <asp:TextBox ID="txtLastName" TabIndex="3" runat="server" CssClass="textbox"></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Enabled="False"
                                                                ControlToValidate="txtLastName" ErrorMessage="Last Name can not be blank" Display="None">Last Name can not be blank</asp:RequiredFieldValidator>
                                                        </td>
                                                        <td class="leadcell4columnei" nowrap>
                                                            <asp:Label ID="lblAge" runat="server" CssClass="label">Age</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnrightei">
                                                            <asp:TextBox ID="txtAge" TabIndex="10" CssClass="textbox" Enabled="False" runat="server"
                                                                MaxLength="128"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="leadcell4columnleftei" nowrap>
                                                            <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnei">
                                                            <asp:DropDownList ID="ddlStatusId" TabIndex="4" runat="server" CssClass="dropdownlist" Width="200px" >
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="leadcell4columnei" nowrap>
                                                            <asp:Label ID="lblID" runat="server" CssClass="label">Employee ID</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnrightei">
                                                            <asp:TextBox ID="txtID" TabIndex="11" runat="server" CssClass="textbox"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="leadcell4columnleftei" nowrap>
                                                            <asp:Label ID="lblSSN" runat="server" CssClass="label">SSN</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnei">
                                                            <telerik:RadMaskedTextBox ID="txtSSN" TabIndex="5" runat="server" CssClass="textbox" Width="200px" 
                                                                DisplayFormatPosition="Right" Mask="#########" DisplayMask="###-##-####"
                                                                DisplayPromptChar="">
                                                            </telerik:RadMaskedTextBox>
                                                        </td>
                                                        <td class="leadcell4columnei" nowrap>
                                                            <asp:Label ID="lblGender" runat="server" CssClass="label">Gender</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnrightei">
                                                            <asp:DropDownList ID="ddlGenderId" TabIndex="12" runat="server" CssClass="dropdownlist" Width="200px" >
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="leadcell4columnleftei">
                                                            <asp:Label ID="lblBirthDate" CssClass="label" runat="server">Birth Date</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnei">
                                                            <telerik:RadDatePicker ID="txtBirthDate" MinDate="1/1/1925" runat="server" AutoPostBack="True" Width="200px" 
                                                                OnSelectedDateChanged="ValidateDate">
                                                            </telerik:RadDatePicker>
                                                        </td>
                                                        <td class="leadcell4columnei">
                                                            <asp:Label ID="lblMaritalStatus" runat="server" CssClass="label">Marital Status</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnrightei">
                                                            <asp:DropDownList ID="ddlMaritalStatId" TabIndex="13" runat="server" CssClass="dropdownlist" Width="200px" >
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="leadcell4columnleftei" nowrap style="padding-bottom: 16px">
                                                            <asp:Label ID="lblRace" runat="server" CssClass="label">Race</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnei" style="padding-bottom: 16px">
                                                            <asp:DropDownList ID="ddlRaceId" TabIndex="7" runat="server" CssClass="dropdownlist" Width="200px" >
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td class="leadcell4columnei" nowrap style="padding-bottom: 16px">&nbsp;
                                                        </td>
                                                        <td class="contentcell4columnrightei" style="padding-bottom: 16px">&nbsp;
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                                    width="100%">
                                                    <tr>
                                                        <td class="contentcellheader" nowrap align="left" colspan="4" style="border-top: 0; border-right: 0; border-left: 0">
                                                            <asp:Label ID="lblAddress" runat="server" CssClass="label" Font-Bold="true">Address</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="leadcell4columnleftei" style="padding-top: 10px" nowrap>&nbsp;
                                                        </td>
                                                        <td class="contentcell4columnei" style="padding-top: 10px">
                                                            <asp:CheckBox ID="chkForeignZip" TabIndex="14" CssClass="checkboxinternational" Text="International"
                                                                runat="server" AutoPostBack="true"></asp:CheckBox>
                                                        </td>
                                                        <td class="leadcell4columnei" style="padding-top: 10px">&nbsp;
                                                        </td>
                                                        <td class="contentcell4columnrightei" style="padding-top: 10px">&nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="leadcell4columnleftei" nowrap>
                                                            <asp:Label ID="lblAddress1" runat="server" CssClass="label">Address 1</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnei">
                                                            <asp:TextBox ID="txtAddress1" TabIndex="15" CssClass="textbox" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td class="leadcell4columnei" nowrap>
                                                            <asp:Label ID="lblStateId" runat="server" CssClass="label">State</asp:Label>
                                                            <asp:Label ID="lblOtherState" CssClass="label" runat="server" Visible="False">Other State Not Listed</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnrightei">
                                                            <asp:DropDownList ID="ddlStateId" TabIndex="18" CssClass="dropdownlist" runat="server" Width="200px" >
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="txtOtherState" CssClass="textbox" runat="server" AutoPostBack="true"
                                                                Visible="False"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="leadcell4columnleftei" nowrap>
                                                            <asp:Label ID="lblAddress2" runat="server" CssClass="label">Address 2</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnei">
                                                            <asp:TextBox ID="txtAddress2" TabIndex="16" CssClass="textbox" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td class="leadcell4columnei" nowrap>
                                                            <asp:Label ID="lblZip" runat="server" CssClass="label" Width="180px">Zip</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnrightei">
                                                            <telerik:RadMaskedTextBox ID="txtzip" TabIndex="19" runat="server"  CssClass="textbox" DisplayFormatPosition="Left" Width="200px" 
                                                                DisplayPromptChar=" " AutoPostBack="false">
                                                            </telerik:RadMaskedTextBox>
                                                    </tr>
                                                    <tr>
                                                        <td class="leadcell4columnleftei" style="padding-bottom: 16px" nowrap>
                                                            <asp:Label ID="lblCity" runat="server" CssClass="label">City</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnei" style="padding-bottom: 16px">
                                                            <asp:TextBox ID="txtcity" TabIndex="17" CssClass="textbox" runat="server" AutoPostBack="True"></asp:TextBox>
                                                        </td>
                                                        <td class="leadcell4columnei" style="padding-bottom: 16px" nowrap>
                                                            <asp:Label ID="lblCountryId" runat="server" CssClass="label">Country</asp:Label>
                                                        </td>
                                                        <td class="contentcell4columnrightei" style="padding-bottom: 16px">
                                                            <asp:DropDownList ID="ddlCountryId" TabIndex="20" CssClass="dropdownlist" runat="server" Width="200px" >
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <asp:Calendar ID="Calendar1" runat="server" CssClass="calendar" Visible="False"></asp:Calendar>
                                                </table>
                                                <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="spacertables"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="contentcellheader" nowrap colspan="6">
                                                                <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="spacertables"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="contentcell2" colspan="6">
                                                                <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </asp:Panel>
                                        </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

