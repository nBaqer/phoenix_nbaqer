﻿<%@ Page Title="90/10 Data Export" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="Mappings9010.aspx.vb" Inherits="Mappings9010" %>

<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../CSS/ImportLeads.css" />
    <link href="../css/AdvantageValidator.css" rel="stylesheet" />
    <link href="../css/SY/StateBoardSettings.css" rel="stylesheet" />
    <link href="../css/SY/Mappings9010.css" rel="stylesheet" />
    <link href="../css/AR/font-awesome.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/Storage/storageCache.js"></script>
    <%--<script type="text/javascript" src="../Scripts/common-util.js"></script>--%>
    <script src="../Scripts/Fame.Advantage.API.Client.js"></script>


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            var hdnHostName = $("#hdnHostName").val();
            var mappings9010 = new Api.ViewModels.Maintenance.Mappings9010();
            mappings9010.initialize();
        });
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <div class="boxContainer">
        <h3>
            <asp:Label ID="headerTitle" runat="server"></asp:Label>
        </h3>
        <div id="tabsForm">


            <div class="formRow">
                <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                    Campus <span class="red-color">*</span>
                </div>
                <div class="inLineBlock left-margin">
                    <input id="ddCampus" class="fieldlabel1 inputCustom form-control" required />
                </div>
            </div>

            <div class="formRow">
                <div>
                    <span></span>
                    <div class="step1-margin">
                        <div id="awardMapping9010Grid" style="height: 100%;"></div>
                        <div class="k-footer" style="width: 100%; height: 35px;">
                            <button class="k-button" type="button" style="float: right; margin-right: 5px; margin-top: 1%;" id="btnSave">Save</button>
                            <button class="k-button" type="button" style="float: right; margin-right: 5px; margin-top: 1%;" id="btnPrint">Print</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
