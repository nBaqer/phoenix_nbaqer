﻿
Partial Class SY_DisplaySSRSReport
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' If Not Page.IsPostBack Then
        HttpContext.Current.Response.ClearContent()
        HttpContext.Current.Response.ContentType = Session("SSRS_MimeType")
        If Session("SSRS_FileExtension") = "csv" Then
            HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=MyReport.") + Session("SSRS_FileExtension"))
        ElseIf Session("SSRS_FileExtension") = "xls" Then
            HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=MyReport.") + Session("SSRS_FileExtension"))
        ElseIf Session("SSRS_FileExtension") = "doc" Then
            HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=MyReport.") + Session("SSRS_FileExtension"))
        ElseIf Session("SSRS_FileExtension") = "pdf" Then
            HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=MyReport.") + Session("SSRS_FileExtension"))
        End If
        HttpContext.Current.Response.BinaryWrite(Session("SSRS_ReportOutput"))
        'End If
    End Sub
End Class
