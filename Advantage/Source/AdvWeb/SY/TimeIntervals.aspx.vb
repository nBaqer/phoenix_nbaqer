Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class TimeIntervals
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As System.Web.UI.WebControls.Label
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        If Not IsPostBack Then
            'objCommon.PageSetup(Form1, "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")

            'ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            'prepare page 'Init called in PreparePage routine
            PreparePage()

            '   disable the first time.
            'Header1.EnableHistoryButton(False)

        Else
            InitButtonsForEdit()
            'objCommon.PageSetup(Form1, "EDIT")
        End If
        headerTitle.Text = Header.Title

    End Sub
    Private Sub BindDataList()

        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radstatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind TimeIntervals datalist
        dlstTimeIntervals.DataSource = New DataView((New StudentsAccountsFacade).GetAllTimeIntervals().Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstTimeIntervals.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    Private Sub dlstTimeIntervals_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstTimeIntervals.ItemCommand

        '   get the TimeIntervalId from the backend and display it
        GetTimeIntervalId(e.CommandArgument)

        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.SetHiddenControlForAudit()
        '   set Style to Selected Item
        ' CommonWebUtilities.SetStyleToSelectedItem(dlstTimeIntervals, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()

        CommonWebUtilities.RestoreItemValues(dlstTimeIntervals, e.CommandArgument.ToString)
    End Sub
    Private Sub BindTimeIntervalData(ByVal TimeInterval As TimeIntervalInfo)
        With TimeInterval
            chkIsInDB.Checked = .IsInDB
            txtTimeIntervalId.Text = .TimeIntervalId
            If Not (TimeInterval.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = TimeInterval.StatusId
            txtTimeIntervalDescrip.Text = .TimeInterval
            txtTimeIntervalDescrip.ReadOnly = .IsBeingUsed
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        If Not DoesUserEnteredValidValuesForTimeInterval() Then
            '   Display Error Message
            DisplayErrorMessage("Invalid Time Interval Data: " + txtTimeIntervalDescrip.Text)
            Exit Sub
        End If

        '   instantiate component
        Dim result As String
        With New StudentsAccountsFacade
            '   update TimeInterval Info 
            result = .UpdateTimeIntervalInfo(BuildTimeIntervalInfo(txtTimeIntervalId.Text), Session("UserName"))
        End With

        '   bind datalist
        BindDataList()

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstTimeIntervals, txtTimeIntervalId.Text, ViewState, Header1)

        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            '   get the TimeIntervalId from the backend and display it
            GetTimeIntervalId(txtTimeIntervalId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new TimeIntervalInfo
            'BindTimeIntervalData(New TimeIntervalInfo)

            '   initialize buttons
            'InitButtonsForLoad()
            InitButtonsForEdit()

        End If
        CommonWebUtilities.RestoreItemValues(dlstTimeIntervals, txtTimeIntervalId.Text)
    End Sub
    Private Function BuildTimeIntervalInfo(ByVal TimeIntervalId As String) As TimeIntervalInfo
        'instantiate class
        Dim TimeIntervalInfo As New TimeIntervalInfo

        With TimeIntervalInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get TimeIntervalId
            .TimeIntervalId = TimeIntervalId

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'get TimeInterval description
            '.TimeIntervalDescrip = CDate(txtTimeIntervalDescrip.Text.Trim)

            .TimeIntervalDescrip = .ConvertTimeDataToDate(txtTimeIntervalDescrip.Text.Trim)

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With
        'return data
        Return TimeIntervalInfo
    End Function
    Private Function BuildTimeIntervalInfoMulti(ByVal TimeIntervalId As String, ByVal statusid As String, ByVal TimeIntervalDescrip As String, ByVal ModUser As String, ByVal ModDate As String) As TimeIntervalInfo
        'instantiate class
        Dim TimeIntervalInfo As New TimeIntervalInfo

        With TimeIntervalInfo
            'get IsInDB
            .IsInDB = False

            'get TimeIntervalId
            .TimeIntervalId = TimeIntervalId

            'get StatusId
            .StatusId = statusid

            'get TimeInterval description
            .TimeIntervalDescrip = TimeIntervalDescrip

            'get ModUser
            .ModUser = ModUser

            'get ModDate
            .ModDate = ModDate

        End With
        'return data
        Return TimeIntervalInfo
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        '   bind an empty new TimeIntervalInfo
        BindTimeIntervalData(New TimeIntervalInfo)

        'Reset Style in the Datalist
        ' CommonWebUtilities.SetStyleToSelectedItem(dlstTimeIntervals, Guid.Empty.ToString, ViewState, Header1)

        'initialize buttons
        InitButtonsForNew()

        CommonWebUtilities.RestoreItemValues(dlstTimeIntervals, Guid.Empty.ToString)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        If Not (txtTimeIntervalId.Text = Guid.Empty.ToString) Then

            'update TimeInterval Info 
            Dim result As String = (New StudentsAccountsFacade).DeleteTimeIntervalInfo(txtTimeIntervalId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                'prepare page
                PreparePage()
            End If
            CommonWebUtilities.RestoreItemValues(dlstTimeIntervals, Guid.Empty.ToString)
        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If
        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If
        btndelete.Enabled = False
        'btnsave.Enabled = False

        'set panels visibility
        pnlCreateTimeInterval.Visible = True
        pnlIntervalData.Visible = False

    End Sub
    Private Sub InitButtonsForEdit()

        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        'btnnew.Enabled = True
        'btndelete.Enabled = True
        'btnsave.Enabled = True

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        'set panels visibility
        pnlCreateTimeInterval.Visible = False
        pnlIntervalData.Visible = True
    End Sub
    Private Sub InitButtonsForNew()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If
        btnnew.Enabled = False
        btndelete.Enabled = False
        'btnsave.Enabled = True

        'set panels visibility
        pnlCreateTimeInterval.Visible = True
        pnlIntervalData.Visible = True

    End Sub
    Private Sub GetTimeIntervalId(ByVal TimeIntervalId As String)

        '   bind TimeInterval properties
        BindTimeIntervalData((New StudentsAccountsFacade).GetTimeIntervalInfo(TimeIntervalId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        InitButtonsForLoad()
        '   bind datalist
        BindDataList()
    End Sub
    Private Function DoesUserEnteredValidValuesForTimeInterval() As Boolean
        If Not IsValidTime(txtTimeIntervalDescrip.Text) Then Return False
        Return True
    End Function
    Private Function DoesUserEnteredValidValuesToCreateTimeIntervalsTableErrorMessage() As String
        'concatenate error message
        Dim err As String = IsValidTimeErrorMessage(txtStartTime.Text) + IsValidTimeErrorMessage(txtEndTime.Text) + IsValidIntervalErrorMessage(txtTimeInterval.Text)
        If err = "" Then
            If Date.Parse(txtStartTime.Text).CompareTo(Date.Parse(txtEndTime.Text)) >= 0 Then Return "End Time must be greater than Start Time" Else Return ""
        Else
            Return err
        End If
    End Function
    Private Function DoesUserEnteredValidValuesToCreateTimeIntervalsTable() As Boolean
        If Not (IsValidTime(txtStartTime.Text) And IsValidTime(txtEndTime.Text) And IsValidInterval(txtTimeInterval.Text)) Then Return False
        If Date.Parse(txtStartTime.Text).CompareTo(Date.Parse(txtEndTime.Text)) >= 0 Then Return False
        Return True
    End Function
    Private Sub btnGenerateTimeRecords_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerateTimeRecords.Click
        If DoesUserEnteredValidValuesToCreateTimeIntervalsTable() Then
            Dim dtTimeIntervals As New DataTable
            'dtTimeIntervals.Columns.Add("TimeIntervalId", GetType(String))
            dtTimeIntervals.Columns.Add("StatusId", GetType(String))
            dtTimeIntervals.Columns.Add("TimeIntervalDescrip", GetType(String))
            dtTimeIntervals.Columns.Add("ModDate", GetType(String))
            dtTimeIntervals.Columns.Add("ModUser", GetType(String))
            dtTimeIntervals.Columns.Add("CampGrpId", GetType(String))

            Dim starttime As DateTime = CDate(txtStartTime.Text)
            Dim endtime As DateTime = CDate(txtEndTime.Text)
            Dim current As DateTime = Date.Parse("1899-12-30 " + starttime.ToString("hh:mm tt"))
            Dim endHour = Date.Parse("1899-12-30 " + endtime.ToString("hh:mm tt"))
            Dim now As DateTime = Date.Now
            now = now.Subtract(New TimeSpan(0, 0, 0, 0, now.Millisecond))
            While current <= endHour
                Dim newRow As DataRow = dtTimeIntervals.NewRow()
                newRow("StatusId") = New Guid(CType(AdvantageCommonValues.ActiveGuid, String))
                newRow("TimeIntervalDescrip") = current
                newRow("ModDate") = now
                newRow("ModUser") = User
                newRow("CampGrpId") = New Guid(CType("2AC97FC1-BB9B-450A-AA84-7C503C90A1EB", String))
                dtTimeIntervals.Rows.Add(newRow)
                current = current.AddMinutes(txtTimeInterval.Text)
            End While

            Dim result As String
            For Each row As DataRow In dtTimeIntervals.Rows
                With New StudentsAccountsFacade
                    Dim TII As New TimeIntervalInfo
                    TII = BuildTimeIntervalInfoMulti(String.Empty, row.Item("StatusID"), row.Item("TimeIntervalDescrip"), row.Item("ModUser"), row.Item("ModDate"))
                    If Not cbxDeleteAllExistingRecords.Checked Then
                        result = .UpdateTimeIntervalInfo(TII, Session("UserName"))
                    Else
                        result = .DeleteTimeIntervalInfoMulti(row.Item("TimeIntervalDescrip"))
                    End If

                    If Not result = "" Then
                        DisplayErrorMessage(result)
                        Exit Sub
                    End If
                End With
            Next
            PreparePage()
        Else
            DisplayErrorMessage(DoesUserEnteredValidValuesToCreateTimeIntervalsTableErrorMessage())
        End If

        'If DoesUserEnteredValidValuesToCreateTimeIntervalsTable() Then
        '    Dim result As String = (New StudentsAccountsFacade).CreateTimeIntervalsTable(Date.Parse(txtStartTime.Text), Date.Parse(txtEndTime.Text), Integer.Parse(txtTimeInterval.Text), cbxDeleteAllExistingRecords.Checked, Session("UserName"))
        '    If Not result = "" Then
        '        DisplayErrorMessage(result)
        '    Else
        '        'prepare page
        '        PreparePage()
        '    End If
        'Else
        '    DisplayErrorMessage(DoesUserEnteredValidValuesToCreateTimeIntervalsTableErrorMessage())
        'End If



    End Sub
    Private Function IsValidTime(ByVal txt As String) As Boolean
        Try
            Date.Parse(txt)
            Return True
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Private Function IsValidTimeErrorMessage(ByVal txt As String) As String
        If Not IsValidTime(txt) Then
            Return "Invalid Time: " + txt + vbCrLf
        Else
            Return ""
        End If
    End Function
    Private Function IsValidIntervalErrorMessage(ByVal txt As String) As String
        If Not IsValidInterval(txt) Then
            Return "Invalid Time Interval: " + txt + vbCrLf
        Else
            Return ""
        End If
    End Function
    Private Function IsValidInterval(ByVal txt As String) As Boolean
        Try
            Integer.Parse(txt)
            Return True
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Private Sub PreparePage()
        '   bind datalist
        BindDataList()

        '   bind an empty new TimeIntervalInfo
        BindTimeIntervalData(New TimeIntervalInfo)

        '   initialize buttons
        InitButtonsForLoad()
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
