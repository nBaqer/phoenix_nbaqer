﻿<%@ Page Title="Setup Required Fields" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SchlReqFldsForResources.aspx.vb" Inherits="SchlReqFldsForResources" EnableViewState="true" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script language="javascript" src="../js/CheckAll.js" type="text/javascript"/>
   <script type="text/javascript">

       function OldPageResized(sender, args) {
           $telerik.repaintChildren(sender);
       }

   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstSetupRequiredFields">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="divContent"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstSetupRequiredFields"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlModules">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="dlstSetupRequiredFields"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings> 
</telerik:RadAjaxManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="250" Scrolling="Y">
     <%-- Add class ListFrameTop2 to the table below --%>   
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="listframetop2">
                    <table id="AutoNumber6" width="80%">
                        <tr>
                            <td class="employersearch">
                                <asp:Label ID="lblModules" runat="server" CssClass="label">Module:</asp:Label>
                            </td>
                            <td class="employersearch2">
                                <asp:DropDownList ID="ddlModules" runat="server" CssClass="label" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="employersearch">
                                <asp:Label ID="lblTypes" runat="server" CssClass="label" Visible="false">Type</asp:Label>
                            </td>
                            <td class="employersearch2">
                                <asp:DropDownList ID="ddlTypes" runat="server" CssClass="label" Visible="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td class="employersearch">
                            </td>
                                <td class="employersearch2">
                                    <asp:Button ID="btnBuildList" runat="server"  Text="Build List" Visible="false">
                                    </asp:Button>
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="listframebottom">
                    <div class="scrollleft" style="height: expression(document.body.clientHeight - 272 + 'px')">
                         <asp:datalist id="dlstSetupRequiredFields" runat="server" width="100%" datakeyfield="ResourceId">
                            <selecteditemstyle cssclass="selecteditemstyle" />
                                <itemstyle cssclass="itemstyle"></itemstyle>
                                <itemtemplate>
                                    <asp:linkbutton text='<%# DataBinder.Eval(Container.DataItem, "Resource") %>' 
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ResourceId") %>' 
                                    CommandName='<%# DataBinder.Eval(Container.DataItem, "Resource") %>' 
                                    runat="server" cssclass="itemstyle"  id="linkbutton1" causesvalidation="false" />
                                </itemtemplate>
                            </asp:datalist>
                    </div>
                </td>
            </tr>
        </table>
    </telerik:RadPane>
    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                        ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                                    </asp:Button>
                                    </td>
                        </tr>
                    </table>
        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
        <td class="detailsframe">
        <div class="scrollright2" id="divContent" runat="server">
        <!-- begin content table-->


            <table cellspacing="0" cellpadding="0" width="97%" border="0" align="center">
                <tr>
                    <td class="contentcellheader">
                        <asp:Label ID="lblCaption" runat="server" CssClass="labelbold"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="padding-top: 10px">
                        <asp:DataGrid ID="dgPageFields" Width="100%" CellPadding="0" BorderStyle="Solid"
                            BorderColor="#E0E0E0" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True"
                            HeaderStyle-Wrap="true" EditItemStyle-Wrap="false" HorizontalAlign="Center" runat="server">
                            <EditItemStyle Wrap="False"></EditItemStyle>
                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                            <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="Field Name">
                                    <HeaderStyle CssClass="datagridheader" Width="45%" Font-Bold="true"></HeaderStyle>
                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="lblFldId" runat="server" Text='<%# Container.DataItem("TblFldsId")  %>'
                                            Visible="False" />
                                        <asp:Label ID="label1" runat="server" Text='<%# Container.DataItem("FldName")  %>'
                                            CssClass="label" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Caption">
                                    <HeaderStyle CssClass="datagridheader" Width="45%" Font-Bold="true"></HeaderStyle>
                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:Label ID="Term" runat="server" Text='<%# Container.DataItem("Caption")  %>'
                                            CssClass="label" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Required" HeaderStyle-HorizontalAlign="Center">
                                    <HeaderStyle CssClass="datagridheader" Width="10%" Font-Bold="true"></HeaderStyle>
                                    <ItemStyle CssClass="datagriditemstyle" HorizontalAlign="center"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkReq" runat="server" Checked='<%# Container.DataItem("SchlReq")%>'
                                            CssClass="labelchkbox" />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr><td></td>                </tr>
            </table>

                        <asp:TextBox ID="HiddenText" Style="display: none" runat="server" CssClass="label"></asp:TextBox>
                        <asp:TextBox ID="txtResourceId" runat="server" Visible="False"></asp:TextBox>

        <!-- end content table-->
        </div>
        </td>
        </tr>
        </table>
        
    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

