<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Holidays.aspx.vb" Inherits="Holidays" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:content id="content1" contentplaceholderid="additional_head" runat="server">
<script language="javascript" src="../js/checkall.js" type="text/javascript"/>
<script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <%--<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstHolidays">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstHolidays"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
   <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstHolidays"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="pnlRHS"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstHolidays"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings> 
</telerik:RadAjaxManagerProxy>--%>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstHolidays" runat="server" DataKeyField="HolidayId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# Container.DataItem("HolidayId")%>' Text='<%# Container.DataItem("HolidayDescrip") + " " + Ctype(Container.DataItem("HolidayStartDate"), Date).ToShortDateString %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="98%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%;" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table style="width: 100%;" align="center" class="contenttable" cellpadding="0" cellspacing="0">
                                        <asp:TextBox ID="txtHolidayId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox
                                            ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox><asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox><asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox><tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblHolidayCode" runat="server" CssClass="label">Code<span style="COLOR: red">*</span></asp:Label></td>
                                                <td class="contentcell4">
                                                    <asp:TextBox ID="txtHolidayCode" runat="server" CssClass="textbox" TabIndex="1" MaxLength="12"></asp:TextBox>

                                                    <asp:RegularExpressionValidator ID="revCode" runat="server" ControlToValidate="txtHolidayCode"
                                                        Display="None" ErrorMessage="too many characters" ValidationExpression=".{0,50}"></asp:RegularExpressionValidator></td>
                                            </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status<span style="COLOR: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist" TabIndex="2">
                                                </asp:DropDownList></td>

                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblHolidayDate" runat="server" CssClass="label">Holiday Dates</asp:Label></td>
                                            <td class="contentcell4" style="text-align: left">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td width="50%" class="label" nowrap>From:
                                                          <telerik:RadDatePicker ID="txtHolidayStartDate" MinDate="1/1/1945" runat="server">
                                                          </telerik:RadDatePicker>
                                                        </td>
                                                        <td width="50%" class="label" nowrap><span style="margin-left:5px;">To:</span>
                                                           <telerik:RadDatePicker ID="txtHolidayEndDate" MinDate="1/1/1945" runat="server">
                                                           </telerik:RadDatePicker>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblHolidayDescrip" CssClass="label" runat="server">Description<span style="COLOR: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtHolidayDescrip" CssClass="textbox" MaxLength="50" runat="server"
                                                    TabIndex="4"></asp:TextBox>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="label" runat="server">Campus Group<span style="COLOR: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" TabIndex="5">
                                                </asp:DropDownList>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" style="border-bottom: 0px">
                                                <asp:Label ID="lblclose" runat="server" Font-Size="12px" CssClass="label" Font-Bold="true">Close<span style="COLOR: red">*</span></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-right: 4px; padding-left: 4px; padding-bottom: 4px; width: 100%; padding-top: 4px;"
                                                colspan="2">
                                                <asp:CheckBox ID="cbxAllDay" runat="server" CssClass="checkbox" Text="All Day ?"
                                                    TabIndex="6"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" align="center" colspan="2">
                                                <asp:Label ID="lblor" runat="server" CssClass="label" Font-Bold="true" Font-Size="Large">Or</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStartTime" runat="server" CssClass="label">Start Time</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStartTimeId" runat="server" CssClass="dropdownlist" TabIndex="7">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblEndTime" runat="server" CssClass="label">End Time</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlEndTimeId" runat="server" CssClass="dropdownlist" TabIndex="8">
                                                </asp:DropDownList>

                                            </td>
                                        </tr>
                                    </table>

                                    <asp:TextBox ID="txtRowIds" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                                    <!--end table content-->
                                </div>
                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="customvalidator"
            Display="none"></asp:CustomValidator>
        <asp:Panel ID="pnlrequiredfieldvalidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="validationsummary1" runat="server" CssClass="validationsummary" ShowMessageBox="true"
            ShowSummary="false"></asp:ValidationSummary>
    </div>

</asp:Content>


