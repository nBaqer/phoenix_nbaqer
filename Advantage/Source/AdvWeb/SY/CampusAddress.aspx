<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="CampusAddress.aspx.vb" Inherits="CampusAddress" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script src="../js/checkall.js" type="text/javascript"> </script>
    <script src="../js/Capitalize.js" type="text/javascript"> </script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

        $(document).ready(function () {
            var $FSEOGMatchTypeDDL = $("select[id*='FSEOGMatchTypeDDL']");
            var $txtFSEOGMatchType = $("input[id*='txtFSEOGMatchType']");

            $FSEOGMatchTypeDDL.val($txtFSEOGMatchType.val())
            $FSEOGMatchTypeDDL.on('change', function () {
                $txtFSEOGMatchType.val($FSEOGMatchTypeDDL.val());
            })

            $txtFSEOGMatchType.hide();
        });
    </script>

</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%"
            Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized"
            Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap="nowrap" align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                    </td>
                                    <td width="85%" nowrap="nowrap">
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server"
                                            RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="True" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstCmpAddr" runat="server" DataKeyField="CampusId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server"
                                            Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>'
                                            CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>'
                                            CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server"
                                            CommandArgument='<%# container.dataitem("CampusId")%>' Text='<%# container.dataitem("CampDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both"
                orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                        <%--CAMPUS INFO AND ADDRESS--%>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCampCode" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtCampCode" runat="server" CssClass="TextBox" MaxLength="128" TabIndex="1"
                                                    ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="DropDownList" TabIndex="2">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblOPEID" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtOPEID" runat="server" CssClass="TextBox" TabIndex="3"  MaxLength="8"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSchoolName" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtSChoolName" runat="server" CssClass="TextBox" TabIndex="3" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCampDescrip" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtCampDescrip" runat="server" CssClass="TextBox" TabIndex="3" ></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCampusIsBranch" runat="server" CssClass="label" Text="Is a branch?"></asp:Label>
                                                <font color="red">*</font>
                                            </td>

                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlCampusIsBranch" TabIndex="41" AppendDataBoundItems="true" CssClass="dropdownlist" runat="server" AutoPostBack="true">
                                                    <asp:ListItem Text="No" Value="0" />
                                                    <asp:ListItem Text="Yes" Value="1" />
                                                </asp:DropDownList>

                                            </td>
                                        </tr>

                                        <tr id="parentCampusContainer" runat="server" visible="false">
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblParentCampus" runat="server" CssClass="label" Text="Parent"></asp:Label>
                                                <font color="red">*</font></td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlParentCampus" TabIndex="41" CssClass="dropdownlist" runat="server">
                                                </asp:DropDownList>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCmsId" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <telerik:RadMaskedTextBox ID="txtCmsId" TabIndex="8" runat="server" CssClass="textbox" BorderColor="#A9A9A9"
                                                    Mask="####-##" DisplayMask="####-##" DisplayPromptChar="" AutoPostBack="false" Width="200px">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblAddress1" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtAddress1" runat="server" CssClass="TextBox" TabIndex="4" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblAddress2" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtAddress2" runat="server" CssClass="TextBox" TabIndex="5" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCity" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtCity" runat="server" CssClass="TextBox" TabIndex="6" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblStateId" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlStateId" runat="server" CssClass="DropDownList" TabIndex="7" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblZip" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <telerik:RadMaskedTextBox ID="txtZip" TabIndex="8" runat="server" CssClass="textbox" BorderColor="#A9A9A9"
                                                    Mask="#####" DisplayMask="#####" DisplayPromptChar="" AutoPostBack="false" Width="200px">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCountryId" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlCountryId" runat="server" CssClass="DropDownList" TabIndex="9">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <%--CAMPUS PHONES AND FAX--%>
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="Label1" runat="server" Font-Bold="true" CssClass="Label">Phones</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblPhone1" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" valign="bottom" nowrap="nowrap">
                                                <telerik:RadMaskedTextBox ID="txtPhone1" TabIndex="10" runat="server" CssClass="textbox" BorderColor="#A9A9A9"
                                                    Mask="(###)###-####" DisplayMask="(###)###-####" DisplayPromptChar="" AutoPostBack="false" 
                                                    Width="200px">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblPhone2" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" valign="bottom" nowrap="nowrap">
                                                <telerik:RadMaskedTextBox ID="txtPhone2" TabIndex="10" runat="server" CssClass="textbox" BorderColor="#A9A9A9"
                                                    Mask="(###)###-####" DisplayMask="(###)###-####" DisplayPromptChar="" AutoPostBack="false"
                                                    Width="200px">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblPhone3" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" valign="bottom" nowrap="nowrap">
                                                <telerik:RadMaskedTextBox ID="txtPhone3" TabIndex="10" runat="server" CssClass="textbox" BorderColor="#A9A9A9"
                                                    Mask="(###)###-####" DisplayMask="(###)###-####" DisplayPromptChar="" AutoPostBack="false"
                                                    Width="200px">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblFax" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" valign="bottom" nowrap="nowrap">
                                                <telerik:RadMaskedTextBox ID="txtFax" TabIndex="10" runat="server" CssClass="textbox" BorderColor="#A9A9A9"
                                                    Mask="(###)###-####" DisplayMask="(###)###-####" DisplayPromptChar="" AutoPostBack="false"
                                                    Width="200px">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <%--CAMPUS CONTACT INFORMATION--%>
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="Label2" runat="server" Font-Bold="true" CssClass="Label">Contact Information</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblEmail" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="TextBox" MaxLength="128" TabIndex="14"
                                                    ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblWebsite" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtWebsite" runat="server" CssClass="TextBox" MaxLength="128" TabIndex="15"
                                                    ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <%--CAMPUS TRANSCRIPT AUTHORIZATION--%>
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="Label3" runat="server" Font-Bold="true" CssClass="Label">Transcript Authorization</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblTranscriptAuthZnTitle" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtTranscriptAuthZnTitle" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="16" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblTranscriptAuthZnName" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtTranscriptAuthZnName" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="17" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <%--CAMPUS TIME CLOCK INFORMATION (TIME CLOCK SCHOOLS ONLY)--%>
                                        <tr>
                                            <td class="contentcellheader" id="tcinfo" colspan="2" runat="server">
                                                <asp:Label ID="lbltcinfo" runat="server" Font-Bold="true" CssClass="Label">Time Clock Information</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblIsRemoteServer1" runat="server" CssClass="Label" Width="194px">Remote Server</asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:CheckBox ID="chkIsRemoteServer" AutoPostBack="true" runat="server" CssClass="CheckBox"
                                                    TabIndex="18" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblTCSourcePath" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtTCSourcePath" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="19" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblTCTargetPath" runat="server" CssClass="Label" Width="237px"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtTCTargetPath" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="20" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell" visible="false">
                                                <asp:Label ID="lblSourceFolderLoc" runat="server" CssClass="Label" Visible="false"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" visible="false">
                                                <asp:TextBox ID="txtSourceFolderLoc" runat="server" Visible="false" CssClass="TextBox"
                                                    MaxLength="128" TabIndex="21" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell" visible="false">
                                                <asp:Label ID="lblTargetFolderLoc" runat="server" CssClass="Label" Visible="false"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" visible="false">
                                                <asp:TextBox ID="txtTargetFolderLoc" runat="server" Visible="false" CssClass="TextBox"
                                                    MaxLength="128" TabIndex="22" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblRemoteServerUsrNm" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtRemoteServerUsrNm" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="23" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblRemoteServerPwd" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtRemoteServerPwd" runat="server" TextMode="Password" CssClass="TextBox"
                                                    MaxLength="128" TabIndex="24" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <%--CAMPUS FAME LINK INFORMATION--%>
                                        <tr>
                                            <td class="contentcellheader" id="flinfo" colspan="2" runat="server">
                                                <asp:Label ID="lblflinfo" runat="server" Font-Bold="true" CssClass="Label">FAMELink Information</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblIsRemoteServer1FL" runat="server" CssClass="Label" Width="194px">Remote Server</asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:CheckBox ID="chkIsRemoteServerFL" AutoPostBack="true" runat="server" CssClass="CheckBoxStyle"
                                                    TabIndex="25" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblFLSourcePath" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtFLSourcePath" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="26" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblFLTargetPath" runat="server" CssClass="Label" Width="237px"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtFLTargetPath" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="27" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblFLExceptionPath" runat="server" CssClass="Label" Width="237px"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtFLExceptionPath" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="28" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell" visible="false">
                                                <asp:Label ID="lblSourceFolderLocFL" runat="server" CssClass="Label" Visible="false"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" visible="false">
                                                <asp:TextBox ID="txtSourceFolderLocFL" runat="server" Visible="false" CssClass="TextBox"
                                                    MaxLength="128" TabIndex="29" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell" visible="false">
                                                <asp:Label ID="lblTargetFolderLocFL" runat="server" CssClass="Label" Visible="false"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" visible="false">
                                                <asp:TextBox ID="txtTargetFolderLocFL" runat="server" Visible="false" CssClass="TextBox"
                                                    MaxLength="128" TabIndex="30" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblRemoteServerUsrNmFL" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtRemoteServerUsrNmFL" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="31" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblRemoteServerPwdFL" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtRemoteServerPwdFL" runat="server" TextMode="Password" CssClass="TextBox"
                                                    MaxLength="128" TabIndex="32" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <%--CAMPUS IMPORT LEADS INFORMATION--%>
                                        <tr>
                                            <td class="contentcellheader" id="TD1" colspan="2" runat="server">
                                                <asp:Label ID="Label6" runat="server" Font-Bold="true" CssClass="Label">Import Leads Information</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblIsRemoteServerIL" runat="server" CssClass="Label" Width="194px">Remote Server</asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:CheckBox ID="chkIsRemoteServerIL" AutoPostBack="true" runat="server" CssClass="CheckBoxStyle"
                                                    Text="Yes" TabIndex="33" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblILSourcePath" runat="server" CssClass="Label" Text="Import Path"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtILSourcePath" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="34" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblILArchivePath" runat="server" CssClass="Label" Width="237px" Text="Archive Path"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtILArchivePath" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="35" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblILExceptionPath" runat="server" CssClass="Label" Width="237px"
                                                    Text="Exception Path"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtILExceptionPath" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="36" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblRemoteServerUserNameIL" runat="server" CssClass="Label" Text="Remote Server User Name"
                                                    Visible="false"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtRemoteServerUserNameIL" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="37" Visible="false" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblRemoteServerPasswordIL" runat="server" CssClass="Label" Text="Remote Server Password"
                                                    Visible="false"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtRemoteServerPasswordIL" runat="server" TextMode="Password" CssClass="TextBox"
                                                    MaxLength="128" Visible="false" TabIndex="38" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>

                                        <%--CAMPUS INVOICE ADDRESS--%>
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="Label4" runat="server" Font-Bold="true" CssClass="Label">Invoice Address</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCampusAddress" runat="server" CssClass="Label" Text="Use Campus Address"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcelldate">
                                                <asp:CheckBox ID="chkUseCampusAddress" runat="server" CssClass="Label" AutoPostBack="true" TabIndex="41"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblInvAddress1" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtInvAddress1" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="42" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblInvAddress2" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtInvAddress2" runat="server" CssClass="TextBox" MaxLength="128"
                                                    TabIndex="43" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblInvCity" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtInvCity" runat="server" CssClass="TextBox" MaxLength="128" TabIndex="44"
                                                    ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblInvStateID" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlInvStateID" runat="server" CssClass="DropDownList" TabIndex="45" Width="200px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblInvZip" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <telerik:RadMaskedTextBox ID="txtInvZip" TabIndex="46" runat="server" CssClass="textbox"
                                                    Mask="#####" DisplayMask="#####" DisplayPromptChar="" AutoPostBack="false" Width="200px">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblInvCountryID" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlInvCountryID" runat="server" CssClass="DropDownList" TabIndex="47">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblInvPhone1" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" valign="bottom" nowrap="nowrap">
                                                <telerik:RadMaskedTextBox ID="txtInvPhone1" TabIndex="48" runat="server" CssClass="textbox"
                                                    Mask="(###)###-####" DisplayMask="(###)###-####" DisplayPromptChar="" AutoPostBack="false"
                                                    Width="200px">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblInvFax" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" valign="bottom" nowrap="nowrap">
                                                <telerik:RadMaskedTextBox ID="txtInvFax" TabIndex="49" runat="server" CssClass="textbox"
                                                    Mask="(###)###-####" DisplayMask="(###)###-####" DisplayPromptChar="" AutoPostBack="false"
                                                    Width="200px">
                                                </telerik:RadMaskedTextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:TextBox ID="txtCampusId" runat="server" CssClass="TextBox" Visible="false"></asp:TextBox>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="donothing" Width="0"></asp:TextBox>
                                                <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="donothing" Width="0"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                    <%--1098T Settings--%>
                                    <table id="tbl1098T" runat="server" class="contenttable" cellspacing="0" cellpadding="0" width="65%" align="center" visible="false">
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="Label8" runat="server" Font-Bold="true" class="Label">1098T Info</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblToken1098TService" runat="server" class="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtToken1098TService" runat="server" class="TextBox" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSchoolCodeKissSchoolId" runat="server" class="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtSchoolCodeKissSchoolId" runat="server" class="TextBox" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                    </table>

                                    <%--Financial Settings--%>

                                    <table id="FinTable" runat="server" class="contenttable" cellspacing="0" cellpadding="0" width="65%" align="center" visible="True">
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="FinancialInformationLbl" runat="server" Font-Bold="true" class="Label">Financial Information</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>

                                        <tr>
                                            <td class="twocolumnlabelcell" style="width: 28%;">
                                                <asp:Label ID="MatchTypeLbl" runat="server" CssClass="Label" Text="FSEOG Match Type"></asp:Label>
                                            </td>

                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="FSEOGMatchTypeDDL" runat="server">
                                                    <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Scholarship" Value="S"></asp:ListItem>
                                                    <asp:ListItem Text="Cash Map" Value="C"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="twocolumnlabelcell" style="width: 28%;">
                                                <asp:TextBox ID="txtFSEOGMatchType" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                    </table>

                                    <%--CAMPUS STUDENT PORTAL INFORMATION--%>
                                    <table runat="server" id="tblPortalCustomer" class="contenttable" cellspacing="0" cellpadding="0" width="65%" align="center" visible="false">
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="Label5" runat="server" Font-Bold="true" CssClass="Label">Student Portal Information</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table class="gridtable">
                                                    <tr>
                                                        <th>
                                                            <asp:Label ID="lblDefaultSetting" runat="server" Text="Default Setting"></asp:Label>
                                                        </th>
                                                        <th>
                                                            <asp:Label ID="lblDefaultValue" runat="server" Text="Default Value"></asp:Label>
                                                        </th>
                                                        <th width="20%">
                                                            <asp:Label ID="Label7" runat="server" ToolTip="The selected default value can be applied to all campuses by selecting the checkbox">Apply default value <br/> to all campuses</asp:Label>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell">
                                                            <asp:Label ID="lblAddType" runat="server" Text=" Address Type" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td class="portaltablecell">
                                                            <asp:DropDownList ID="ddlAddType" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                        </td>
                                                        <td class="portaltablecellcheck">
                                                            <asp:CheckBox ID="chkAddTypeAll" runat="server" Checked="False" ToolTip="Check this box to apply the selected address type value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:Label ID="lblAdmRep" runat="server" Text=" Admissions Rep" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:DropDownList ID="ddlAdmRep" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                        </td>
                                                        <td class="portaltablecellcheck" style="background-color: #F0F7FF;">
                                                            <asp:CheckBox ID="chkAdmRepAll" runat="server" Checked="False" ToolTip="Check this box to apply the selected admission rep type value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell">
                                                            <asp:Label ID="lblCountry" runat="server" Text=" Country" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td class="portaltablecell">
                                                            <asp:DropDownList ID="ddlPortalCountry" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                        </td>
                                                        <td class="portaltablecellcheck">
                                                            <asp:CheckBox ID="chkPortalCountryAll" runat="server" Checked="False" ToolTip="Check this box to apply the selected country value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:Label ID="lblGender" runat="server" Text=" Gender" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:DropDownList ID="ddlGender" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                        </td>
                                                        <td class="portaltablecellcheck" style="background-color: #F0F7FF;">
                                                            <asp:CheckBox ID="chkGenderAll" runat="server" Checked="False" ToolTip="Check this box to apply the selected gender value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell">
                                                            <asp:Label ID="lblLeadStatus" runat="server" Text=" Lead Status" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td class="portaltablecell">
                                                            <asp:DropDownList ID="ddlLeadStatus" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                        </td>
                                                        <td class="portaltablecellcheck">
                                                            <asp:CheckBox ID="chkLeadStatusAll" runat="server" Checked="False" ToolTip="Check this box to apply the selected lead status value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:Label ID="lblPhoneType1" runat="server" Text=" Phone Type 1" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:DropDownList ID="ddlPhoneType1" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                        </td>
                                                        <td class="portaltablecellcheck" style="background-color: #F0F7FF;">
                                                            <asp:CheckBox ID="chkPhoneType1All" runat="server" Checked="False" ToolTip="Check this box to apply the selected phone type value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell">
                                                            <asp:Label ID="lblPhoneType2" runat="server" Text=" Phone Type 2" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td class="portaltablecell">
                                                            <asp:DropDownList ID="ddlPhoneType2" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                        </td>
                                                        <td class="portaltablecellcheck">
                                                            <asp:CheckBox ID="chkPhoneType2All" runat="server" Checked="False" ToolTip="Check this box to apply the selected phone type value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:Label ID="lblPortalContactEmail" runat="server" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:TextBox ID="txtPortalContactEmail" runat="server" CssClass="textbox" MaxLength="128"
                                                                TabIndex="39" ></asp:TextBox>
                                                        </td>
                                                        <td class="portaltablecellcheck" style="background-color: #F0F7FF;">
                                                            <asp:CheckBox ID="chkPortalContactEmailAll" runat="server" Checked="False" ToolTip="Check this box to apply the contact email value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell">
                                                            <asp:Label ID="lblSubject" runat="server" CssClass="Label">Response Subject (Email) </asp:Label>
                                                        </td>
                                                        <td class="portaltablecell">
                                                            <asp:TextBox ID="txtEmailSubject" runat="server" CssClass="textbox" MaxLength="1000"  Height="40px" TextMode="SingleLine"></asp:TextBox>
                                                        </td>
                                                        <td class="portaltablecellcheck">
                                                            <asp:CheckBox ID="chkEmailSubjectAll" runat="server" Checked="False" ToolTip="Check this box to apply the response email subject value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:Label ID="lblBody" runat="server" CssClass="Label">Response Body (Email) </asp:Label>
                                                        </td>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:TextBox ID="txtEmailBody" runat="server" CssClass="textbox" MaxLength="1000"  Height="200px" TextMode="MultiLine"></asp:TextBox>
                                                        </td>
                                                        <td class="portaltablecellcheck" style="background-color: #F0F7FF;">
                                                            <asp:CheckBox ID="chkEmailBodyAll" runat="server" Checked="False" ToolTip="Check this box to apply the response email body value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell">
                                                            <asp:Label ID="lblSource" runat="server" Text=" Source Category" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td class="portaltablecell">
                                                            <asp:DropDownList ID="ddlSourceCategory" runat="server" CssClass="dropdownlist" TabIndex="51" AutoPostBack="True"></asp:DropDownList>
                                                        </td>
                                                        <td class="portaltablecellcheck">
                                                            <asp:CheckBox ID="chkSourceCategoryAll" runat="server" Checked="False" ToolTip="Check this box to apply the selected source category value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:Label ID="lblSourceType" runat="server" Text=" Source Type" CssClass="Label"></asp:Label>
                                                        </td>
                                                        <td class="portaltablecell" style="background-color: #F0F7FF;">
                                                            <asp:DropDownList ID="ddlSourceType" runat="server" CssClass="dropdownlist"></asp:DropDownList>
                                                        </td>
                                                        <td class="portaltablecellcheck" style="background-color: #F0F7FF;">
                                                            <asp:CheckBox ID="chkSourceTypeAll" runat="server" Checked="False" ToolTip="Check this box to apply the selected source type value for all campuses"></asp:CheckBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell"></td>
                                            <td class="twocolumncontentcelldate">
                                                <asp:CheckBox ID="chkIsCorporate" runat="server" CssClass="Label" Text="Corporate" TabIndex="40"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                    </table>
                                    <!--end table content-->
                                    <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap="nowrap" colspan="6">
                                                    <asp:Label ID="lblSDF" runat="server" Font-Bold="true" CssClass="label">School Defined Fields</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="contentcell2" colspan="6">
                                                    <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false">
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <!--end table content-->
                            </td>
                        </tr>
                    </table>
                </asp:Panel>

            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
    </div>
</asp:Content>
