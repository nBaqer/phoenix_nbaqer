﻿Imports System.Data
Imports System.IO
Imports System.Web
Imports System.Collections.Generic
Imports Telerik.Web.UI.com.hisoftware.api2
Imports NHibernate.Linq
Imports Telerik.Charting.Styles
Imports Telerik.Web.UI.Widgets
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.AdvantageV1.BusinessFacade
Imports Ionic.Zip
Imports System.Data.OleDb
Partial Class NSLDSDataExport
    Inherits BasePage
    Dim strRootFolderName As String
    Dim datefile As String = Guid.NewGuid().ToString()
    Protected Sub btnToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnToExcel.Click
        Try
            pnlUploadMessage.Visible = True
            GenerateDataToExport()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


            DisplayRadNotification("Error in btnToExcel event: ", ex.Message)

        End Try

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.RadPopupContol = RadNotification1
        Try

            Dim ProviderTypeName As String
            ProviderTypeName = GetType(Telerik.Web.UI.Widgets.FileSystemContentProvider).AssemblyQualifiedName
            RadFileExplorer1.Configuration.ContentProviderTypeName = ProviderTypeName
            If Not Page.IsPostBack Then
                BuildAwardYearDDL()
                BuildCampusDDL()
                ddlCampuses.SelectedValue = Request.QueryString("cmpid")
                BuildProgramsDDL()
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


            DisplayRadNotification("Error in Page Load event", ex.Message)
        End Try

    End Sub
    Private Sub BuildCampusDDL()
        Try
            Dim campus As New CampusGroupsFacade
            With ddlCampuses
                .DataTextField = "CampDescrip"
                .DataValueField = "CampusId"
                .DataSource = campus.GetAllCampuses()
                .DataBind()
            End With
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayRadNotification("Error in BuildCampusDDL", ex.Message)
        End Try

    End Sub
    Private Sub BuildProgramsDDL()

        Try
            With ddlProgram
                .DataTextField = "Descrip"
                .DataValueField = "Id"
                .DataSource = ProgramsFacade.GetPrograms(ddlCampuses.SelectedValue, True, True)
                .DataBind()
                .Items.Insert(0, New ListItem("All Programs", ""))
                .SelectedIndex = 0
            End With
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayRadNotification("Error in BuildProgramsDDL", ex.Message)

        End Try

    End Sub

    Private Sub BuildAwardYearDDL()

        Try

            With ddlAwardYearId

                Dim currentYearVal As String = DateTime.Now.Year.ToString()
                Dim currentYearText As String = DateTime.Now.Year.ToString()

                'currentYearVal = DateTime.Now.AddYears(-9).Year.ToString() + DateTime.Now.AddYears(-8).Year.ToString()
                'currentYearText = DateTime.Now.AddYears(-9).Year.ToString() + "-" + DateTime.Now.AddYears(-8).Year.ToString()
                '.Items.Add(New ListItem(currentYearText, currentYearVal))

                'currentYearVal = DateTime.Now.AddYears(-8).Year.ToString() + DateTime.Now.AddYears(-7).Year.ToString()
                'currentYearText = DateTime.Now.AddYears(-8).Year.ToString() + "-" + DateTime.Now.AddYears(-7).Year.ToString()
                '.Items.Add(New ListItem(currentYearText, currentYearVal))

                currentYearVal = DateTime.Now.AddYears(-7).Year.ToString() + DateTime.Now.AddYears(-6).Year.ToString()
                currentYearText = DateTime.Now.AddYears(-7).Year.ToString() + "-" + DateTime.Now.AddYears(-6).Year.ToString()
                .Items.Add(New ListItem(currentYearText, currentYearVal))

                currentYearVal = DateTime.Now.AddYears(-6).Year.ToString() + DateTime.Now.AddYears(-5).Year.ToString()
                currentYearText = DateTime.Now.AddYears(-6).Year.ToString() + "-" + DateTime.Now.AddYears(-5).Year.ToString()
                .Items.Add(New ListItem(currentYearText, currentYearVal))

                currentYearVal = DateTime.Now.AddYears(-5).Year.ToString() + DateTime.Now.AddYears(-4).Year.ToString()
                currentYearText = DateTime.Now.AddYears(-5).Year.ToString() + "-" + DateTime.Now.AddYears(-4).Year.ToString()
                .Items.Add(New ListItem(currentYearText, currentYearVal))

                currentYearVal = DateTime.Now.AddYears(-4).Year.ToString() + DateTime.Now.AddYears(-3).Year.ToString()
                currentYearText = DateTime.Now.AddYears(-4).Year.ToString() + "-" + DateTime.Now.AddYears(-3).Year.ToString()
                .Items.Add(New ListItem(currentYearText, currentYearVal))

                currentYearVal = DateTime.Now.AddYears(-3).Year.ToString() + DateTime.Now.AddYears(-2).Year.ToString()
                currentYearText = DateTime.Now.AddYears(-3).Year.ToString() + "-" + DateTime.Now.AddYears(-2).Year.ToString()
                .Items.Add(New ListItem(currentYearText, currentYearVal))

                currentYearVal = DateTime.Now.AddYears(-2).Year.ToString() + DateTime.Now.AddYears(-1).Year.ToString()
                currentYearText = DateTime.Now.AddYears(-2).Year.ToString() + "-" + DateTime.Now.AddYears(-1).Year.ToString()
                .Items.Add(New ListItem(currentYearText, currentYearVal))

                currentYearVal = DateTime.Now.AddYears(-1).Year.ToString() + DateTime.Now.Year.ToString()
                currentYearText = DateTime.Now.AddYears(-1).Year.ToString() + "-" + DateTime.Now.Year.ToString()
                .Items.Add(New ListItem(currentYearText, currentYearVal))


                'add the current award year only if we are passed july 1st
                Dim curAwardYearStart As DateTime = New DateTime(DateTime.Now.Year, 7, 1)
                If DateTime.Compare(DateTime.Now.Date, curAwardYearStart) >= 0 Then
                    currentYearVal = DateTime.Now.Year.ToString() + DateTime.Now.AddYears(1).Year.ToString()
                    currentYearText = DateTime.Now.Year.ToString() + "-" + DateTime.Now.AddYears(1).Year.ToString()
                    .Items.Add(New ListItem(currentYearText, currentYearVal))
                End If



                .SelectedIndex = 0
            End With
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)



            DisplayRadNotification("Error in BuildAwardYearDDL", ex.Message)
        End Try


    End Sub

    Protected Sub ddlCampuses_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampuses.SelectedIndexChanged
        Try
            pnlUploadMessage.Visible = False
            BuildProgramsDDL()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


            DisplayRadNotification("Error in ddlCampuses_SelectedIndexChanged", ex.Message)

        End Try

    End Sub
    Private Sub GenerateDataToExport()

        Dim dsData As DataSet

        Dim campusCode As String = ""
        Dim progCode As String = ddlProgram.SelectedItem.Text

        Dim j As Integer   ' total of files to divide the DS
        Dim intStart As Integer = 1
        Dim totRecForFile As Integer = 1000
        Dim FileExtention As String = ".xls"
        Dim ZipFileExtention As String = ".zip"
        'List of excel file generated to include in Zip file
        Dim filesToInclude As New System.Collections.Generic.List(Of String)()

        'Get the DS of records to export to excel
        If ddlProgram.SelectedIndex = 0 Then 'If all programs is selected
            dsData = (New GradesFacade).BuildDSForDataExport(ddlAwardYearId.SelectedValue, ddlCampuses.SelectedValue, "")
        Else
            dsData = (New GradesFacade).BuildDSForDataExport(ddlAwardYearId.SelectedValue, ddlCampuses.SelectedValue, ddlProgram.SelectedValue)
            progCode = (New GradesFacade).GetProgCodeFromProgId(ddlProgram.SelectedValue)

        End If

        campusCode = (New GradesFacade).GetCampusCodeFromCampusId(ddlCampuses.SelectedValue)

        'renumerate the rows that was lost when the SP order and use union queries
        'This RowNumber will used to control the total of records for each file
        For i = 0 To dsData.Tables(0).Rows.Count - 1
            dsData.Tables(0).Rows(i).Item("RowNumber") = (i + 1).ToString()
        Next

        'Calculate total of files (Each file will have 50 records
        Dim dsTotalCount As Integer
        If (dsData.Tables(0).Rows.Count) >= 1 And dsData.Tables(0).Rows.Count < totRecForFile Then
            dsTotalCount = 1
        Else
            dsTotalCount = Math.Ceiling((dsData.Tables(0).Rows.Count) / totRecForFile)
        End If

        'select and format the award year
        Dim strAwardYearFolder As String
        strAwardYearFolder = Mid(ddlAwardYearId.SelectedValue, 1, 4) + "-" + Mid(ddlAwardYearId.SelectedValue, 5, 4)

        ' zip file name
        Dim getZipFileName As String = campusCode & "_" _
                                       & progCode & "_" _
                                       & (Session("UserName").ToString()) & "_" _
                                       & Trim(DateTime.Now.ToString("MMddyyyyHmmss")) 'Trim(Format(Now, "MMddyyyy Hmmss"))

        getZipFileName = RemoveSpecialCharacters(getZipFileName)

        Dim p As String = "~/NSLDSDataExport/" & strAwardYearFolder & "/" & getZipFileName & ZipFileExtention
        Dim newPath As String = p.Replace(" ", "")
        Dim finalPath As String = Server.MapPath(newPath)

        Dim zipFileName As String = finalPath
        Dim zip As New ZipFile

        Try
            strRootFolderName = Server.MapPath("~/NSLDSDataExport/" & strAwardYearFolder & "/")
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayRadNotification("Error Mapping the zip file path", ex.Message)
        End Try

        'for each file to be generated 
        For i = 0 To dsTotalCount - 1
            Dim fileName As String = Trim(RemoveSpecialCharacters(ddlCampuses.SelectedItem.Text)) + "_" _
                                    + Trim(Session("UserName")) + "_" _
                                    + Trim(DateTime.Now.ToString("MMddyyyyHmmss")) + "_" _
                                    + Trim((i + 1).ToString) + FileExtention
            fileName = strRootFolderName & fileName

            Using conn As New OleDbConnection(String.Format("provider=Microsoft.ACE.OLEDB.12.0; Data Source='{0}';" _
                                                            & "Extended Properties='Excel 8.0;HDR=NO;'", fileName)) 'Original
                Try
                    conn.Open()
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    DisplayRadNotification("Error opening connection for OLEDB 12.0", ex.Message)
                End Try

                '        ''''''''''''''''''''' Original content - Do not delete ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                '        Using cmd1 As OleDbCommand = New OleDbCommand("CREATE TABLE [data]([Award Year] varchar(8),[Student Social Security Number] varchar(9),[Student First Name] varchar(35),[Student Middle Name] varchar(35), [Student Last Name] varchar(35), " & _
                '                                                    "[Student Date of Birth] varchar(10),[Institution Code (OPEID)] varchar(8),[Institution Name] varchar(65),[Program Name] varchar(80),[CIP Code] varchar(6),[Credential Level] Varchar(2), " & _
                '                                                    "[Medical or Dental Inernship or Residency] varchar(1), [Program Attendance Begin Date] varchar(10),[Program Attendance Begin Date for This Award Year] varchar(10), [Program Attendance Status During Award Year] varchar(1), " & _
                '                                                    "[Program Attendance Status Date] varchar(10), [Private Loans Amount] varchar(7),[Institutional Debt] VARCHAR(7),[Tuition and Fees Amount] Varchar(50), " & _
                '                                                    "[Allowance For Books, Supplies, and Equipment] varchar(7),[Length of GE Program] VARCHAR(7),[Length of GE Program Measurement] VARCHAR(1), " & _
                '                                                    "[Student's Enrollment Status 1st Day Enrollment in Program] VARCHAR(1))", conn)


                '            '''''''''''''''''''''''''''' Original - Do not delete '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                '            'Dim _strSheetName As String = "Upload File"
                '            'Dim _strSheetRange As String = "A3:AB52"
                '            'Using cmd1 As OleDbCommand = New OleDbCommand("Select * from [" _
                '            '                             + _strSheetName _
                '            '                             + "$" + _strSheetRange _
                '            '                             + "]", conn)
                'making table query
                Dim strTableQ As String
                strTableQ = "CREATE TABLE [" & dsData.Tables(0).TableName & "]("
                For j = 0 To dsData.Tables(0).Columns.Count - 2           ' it is -2 to exclude RowNumber column
                    strTableQ &= " [F" & (j + 1).ToString() & "] varchar(255) , "
                Next
                strTableQ = strTableQ.Substring(0, strTableQ.Length - 2)
                strTableQ &= ")"
                Using cmd1 As OleDbCommand = New OleDbCommand(strTableQ, conn)
                    Try
                        cmd1.ExecuteNonQuery()
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisplayRadNotification("Creating Table record error: ", ex.Message)
                    End Try
                End Using

                'Insert Data
                Dim strparname As String
                Dim strExpr As String = "RowNumber >=" & intStart.ToString & " and RowNumber<=" & (intStart + totRecForFile - 1).ToString
                Dim cmd As OleDbCommand = New OleDbCommand("INSERT INTO  [" & dsData.Tables(0).TableName & "]  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
                cmd.Connection = conn
                'Session.Timeout = System.Threading.Timeout.Infinite  
                cmd.CommandTimeout = 300 '5 minutes

                For Each dr As DataRow In dsData.Tables(0).Select(strExpr)
                    For j = 0 To dsData.Tables(0).Columns.Count - 2           ' it is -2 to exclude RowNumber column
                        strparname = "@p" & (j + 1).ToString()
                        cmd.Parameters.Add(strparname, OleDbType.VarChar)     'cmd.Parameters.Add("@p1", OleDbType.VarChar)
                    Next
                    cmd.Parameters("@p1").Value = dr("Award Year").ToString
                    Try
                        cmd.Parameters("@p2").Value = dr("Student Social Security Number") 'celltext 'String.Format("{000000000}", dr("Student Social Security Number"))
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        cmd.Parameters("@p2").Value = dr("Student Social Security Number")
                    End Try
                    cmd.Parameters("@p3").Value = dr("Student First Name").ToString
                    cmd.Parameters("@p4").Value = dr("Student Middle Name").ToString
                    cmd.Parameters("@p5").Value = dr("Student Last Name").ToString
                    Try
                        cmd.Parameters("@p6").Value = Convert.ToDateTime(dr("Student Date of Birth")).ToString("MM/dd/yyyy") 'FormatDateTime(dr("Student Date of Birth").ToString, DateFormat.ShortDate)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        cmd.Parameters("@p6").Value = dr("Student Date of Birth").ToString
                    End Try
                    cmd.Parameters("@p7").Value = dr("Institution Code (OPEID)").ToString
                    cmd.Parameters("@p8").Value = dr("Institution Name").ToString
                    cmd.Parameters("@p9").Value = dr("Program Name").ToString
                    cmd.Parameters("@p10").Value = dr("CIP Code").ToString
                    cmd.Parameters("@p11").Value = dr("Credential Level").ToString
                    cmd.Parameters("@p12").Value = dr("Medical or Dental Internship or Residency").ToString
                    Try
                        cmd.Parameters("@p13").Value = Convert.ToDateTime(dr("Program Attendance Begin Date")).ToString("MM/dd/yyyy") 'FormatDateTime(dr("Program Attendance Begin Date").ToString, DateFormat.ShortDate)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        cmd.Parameters("@p13").Value = dr("Program Attendance Begin Date").ToString
                    End Try
                    Try
                        cmd.Parameters("@p14").Value = Convert.ToDateTime(dr("Program Attendance Begin Date for This Award Year")).ToString("MM/dd/yyyy") 'FormatDateTime(dr("Program Attendance Begin Date for This Award Year").ToString, DateFormat.ShortDate)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        cmd.Parameters("@p14").Value = dr("Program Attendance Begin Date for This Award Year").ToString
                    End Try
                    cmd.Parameters("@p15").Value = dr("Program Attendance Status During Award Year").ToString
                    Try
                        cmd.Parameters("@p16").Value = Convert.ToDateTime(dr("Program Attendance Status Date")).ToString("MM/dd/yyyy") 'FormatDateTime(dr("Program Attendance Status Date").ToString, DateFormat.ShortDate)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        cmd.Parameters("@p16").Value = dr("Program Attendance Status Date").ToString
                    End Try
                    Try
                        cmd.Parameters("@p17").Value = dr("Private Loans Amount").ToString 'Format(dr("Private Loans Amount"), "0.00").ToString       Format(dr("Private Loans Amount"), "#").ToString  
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        cmd.Parameters("@p17").Value = dr("Private Loans Amount").ToString
                    End Try
                    cmd.Parameters("@p18").Value = dr("Institutional Debt").ToString
                    Try
                        cmd.Parameters("@p19").Value = dr("Tuition and Fees Amount").ToString    'Format(dr("Tuition and Fees Amount"), "0.00").ToString
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        cmd.Parameters("@p19").Value = dr("Tuition and Fees Amount").ToString
                    End Try
                    cmd.Parameters("@p20").Value = dr("Allowance For Books, Supplies, and Equipment").ToString
                    'should  formated "nnnnnn" where there are implicit decimal point between third and fourth digits Numeric and > 0
                    cmd.Parameters("@p21").Value = Format(CType(dr("Length of GE Program").ToString(), Integer) * 1000, "000000").ToString
                    cmd.Parameters("@p22").Value = dr("Length of GE Program Measurement").ToString
                    cmd.Parameters("@p23").Value = dr("Student's Enrollment Status as of the 1st Day of Enrollment in Program").ToString
                    Try
                        cmd.ExecuteNonQuery()
                        cmd.Parameters.Clear()
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisplayRadNotification("Insert record error: ", ex.Message)
                    End Try
                Next
                'Rename the fist row
                'rename the fields update 
                Dim strUpdateq As String
                strUpdateq = "Update [" & dsData.Tables(0).TableName & "$A1:X1] SET  "
                For j = 0 To dsData.Tables(0).Columns.Count - 2           ' it is -2 to exclude RowNumber column
                    strUpdateq &= " F" & (j + 1).ToString() & " = '" & dsData.Tables(0).Columns(j).ColumnName.Replace("'", "''") & "' , "
                Next
                strUpdateq = strUpdateq.Substring(0, strUpdateq.Length - 2)

                Using cmd1 As OleDbCommand = New OleDbCommand(strUpdateq, conn)
                    Try
                        cmd1.ExecuteNonQuery()
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisplayRadNotification("UPDATING Table record error: ", ex.Message)
                    End Try
                End Using

                filesToInclude.Add(System.IO.Path.Combine(fileName))
            End Using
            intStart += totRecForFile
        Next

        Try
            zip.AddFiles(filesToInclude, "NSLDS_ExportedFiles")
            zip.Save(zipFileName)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayRadNotification("Error adding and saving zip file", ex.Message)
        End Try

        Try
            For i As Integer = 0 To filesToInclude.Count - 1
                Dim file As FileInfo = New FileInfo(filesToInclude(i))

                If file.Exists Then
                    file.Delete()
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayRadNotification("Error deleting file: ", ex.Message)
        End Try

    End Sub


    Public Shared Function RemoveSpecialCharacters(str As String) As String

        Dim reservedChars As Char() = {"*"c, "?"c, """"c, "<"c, _
           ">"c, "|"c, "&"c, "#"c, "$"c, "%"c, "^"c, "!"c, ":"c}   ', "("c, ")"c, ","c, "'"c}

        Return reservedChars.Aggregate(str, Function(current, strChar) current.Replace(strChar.ToString(), ""))
    End Function

    Public Function OleDbConnectionString(ByVal fileName As String, ByVal pHeader As String) As String
        Dim builder As New OleDb.OleDbConnectionStringBuilder
        If IO.Path.GetExtension(fileName).ToUpper = ".XLS" Then
            'builder.Provider = "Microsoft.Jet.OLEDB.4.0"
            builder.Provider = "Microsoft.ACE.OLEDB.12.0"
            builder.Add("Extended Properties", String.Format("Excel 8.0;IMEX=1;HDR={0};", pHeader))
        Else
            builder.Provider = "Microsoft.ACE.OLEDB.12.0"
            builder.Add("Extended Properties", String.Format("Excel 12.0;IMEX=1;HDR={0};", pHeader))
        End If

        builder.DataSource = fileName

        Return builder.ConnectionString
    End Function

End Class
Public Class CustomFileBrowserProviderWithFilter
    Inherits FileSystemContentProvider
    Public Sub New(ByVal context As HttpContext, ByVal searchPatterns As String(), ByVal viewPaths As String(), ByVal uploadPaths As String(), ByVal deletePaths As String(), ByVal selectedUrl As String, _
        ByVal selectedItemTag As String)
        MyBase.New(context, searchPatterns, viewPaths, uploadPaths, deletePaths, selectedUrl, _
            selectedItemTag)
    End Sub

    Public Overloads Overrides Function ResolveDirectory(ByVal path As String) As DirectoryItem


        Dim originalFolder As DirectoryItem = MyBase.ResolveDirectory(path)
        Dim originalFiles As FileItem() = originalFolder.Files
        Dim filteredFiles As New List(Of FileItem)()

        Try
            ' Filter the files
            For Each originalFile As FileItem In originalFiles
                If Not Me.IsFiltered(originalFile.Name) Then
                    filteredFiles.Add(originalFile)
                End If
            Next

            Dim newFolder As New DirectoryItem(originalFolder.Name, originalFolder.Location, originalFolder.FullPath, originalFolder.Tag, originalFolder.Permissions, filteredFiles.ToArray(), _
                originalFolder.Directories)

            Return newFolder

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception("Error in ResolveDirectory", ex.InnerException)
        End Try

    End Function

    Public Overloads Overrides Function ResolveRootDirectoryAsTree(ByVal path As String) As DirectoryItem
        Dim originalFolder As DirectoryItem = MyBase.ResolveRootDirectoryAsTree(path)
        Dim originalDirectories As DirectoryItem() = originalFolder.Directories
        Dim filteredDirectories As New List(Of DirectoryItem)()

        Try
            ' Filter the folders 
            For Each originalDir As DirectoryItem In originalDirectories
                If Not Me.IsFiltered(originalDir.Name) Then
                    filteredDirectories.Add(originalDir)
                End If
            Next
            Dim newFolder As New DirectoryItem(originalFolder.Name, originalFolder.Location, originalFolder.FullPath, originalFolder.Tag, originalFolder.Permissions, originalFolder.Files, _
            filteredDirectories.ToArray())

            Return newFolder
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception("Error in ResolveDirectory", ex.InnerException)
        End Try

    End Function

    Private Function IsFiltered(ByVal name As String) As Boolean
        If name.ToLower().EndsWith(".sys") OrElse name.ToLower().Contains("_sys") OrElse name.Contains("Flower") Then
            Return True
        End If

        Return False
    End Function



End Class
