﻿<%@ Page Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="__show_SessionVars__.aspx.vb" Inherits="SY__show_SessionVars__" %>

<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:content id="content4" contentplaceholderid="additional_head" runat="server">
</asp:content>
<asp:content id="content1" contentplaceholderid="contentmain2" runat="server">
	<div id='frame'>
		<div id='body'>
			<telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
		<telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			<asp:Label runat="server" ID='lblSessions'></asp:Label>
		</telerik:RadPane>
		</telerik:RadSplitter>
		</div>    
	</div>
</asp:content>  