<%@ MasterType virtualpath="~/NewSite.master" %>
<%@ page title="" language="vb" MasterPageFile="~/NewSite.master" autoeventwireup="false" codefile="NavigationNodes.aspx.vb" inherits="NavigationNodes" %>
<asp:content id="content1" contentplaceholderid="additional_head" runat="server">
<script language="javascript" src="../js/checkall.js" type="text/javascript"/>
<script type="text/javascript">

    function OldPageResized(sender, args) {
        $telerik.repaintChildren(sender);
    }

   </script>
</asp:content>
<asp:content id="content2" contentplaceholderid="contentmain1" runat="server">
</asp:content>
<asp:content id="content3" contentplaceholderid="contenterror" runat="server">
</asp:content>
<asp:content id="content4" contentplaceholderid="contentmain2" runat="server">
<asp:scriptmanagerproxy id="scriptmanagerproxy1" runat="server">
</asp:scriptmanagerproxy>
<div style="overflow:auto;">
<telerik:radsplitter id="oldcontentsplitter" runat="server" collapsemode="none" height="100%" orientation="vertical" 
visibleduringinit="false" borderwidth="0px" onclientresized="OldPageResized" style="overflow:auto;">
<telerik:radpane id="oldmenupane" runat="server" BackColor="#FAFAFA" width="350" scrolling="Y">
<table cellspacing="0" cellpadding="0" width="100%" border="0">
<tr>
<td class="listframetop">
<table width="100%">
											<tr>
												<td class="contentcell" noWrap style="background:transparent;"><asp:label id="lblModules" CssClass="label" Runat="server">Module</asp:label></td>
												<td class="contentcell4" style="background:transparent;"><asp:dropdownlist id="ddlModules" CssClass="dropdownlist" Runat="server"></asp:dropdownlist></td>
											</tr>
											<tr>
												<td class="contentcell" style="white-space: normal;background:transparent;"><asp:label id="lblStartsWith" CssClass="label" Runat="server">Page Name Starts With</asp:label></td>
												<td class="contentcell4" align="left" style="background:transparent;"><asp:textbox id="txtStartsWith" CssClass="textbox" Runat="server"></asp:textbox></td>
											</tr>
											<tr>
												<td class="contentcell" style="background:transparent;">&nbsp;</td>
												<td class="contentcell4" style="text-align: center;background:transparent;"><asp:button id="btnSearch" runat="server"  Text="Build Pages" causesValidation="False"></asp:button></td>
											</tr>
</table>
</td>
</tr>
<tr>
         <td class="listframebottom">
    <table>
                               <tr>
                                <td>
                                    	<div class="scrollleftfilters">
											<asp:datalist id="dlstLeadNames" runat="server" Width="100%" DataKeyField="ResourceId">
												<SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
												<ItemStyle CssClass="itemstyle"></ItemStyle>
												<SelectedItemTemplate>
													<asp:Label ID="label11" Runat="server" CssClass="selecteditemstyle" text ='<%# Container.DataItem("Resource")%>'>
													</asp:Label>
												</SelectedItemTemplate>
												<ItemTemplate>
													<asp:LinkButton text='<%# Container.DataItem("Resource")%>' Runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("ResourceId")%>' CommandName='<%# Container.DataItem("Resource")%>' ID="Linkbutton1" CausesValidation="False" />
												</ItemTemplate>
											</asp:datalist></div>
                                </td>
                               
                               </tr>
    </table>
    </td>
</tr>
</table>
</telerik:radpane>
<telerik:radpane id="oldcontentpane" runat="server" borderwidth="0px" scrolling="both" orientation="horizontaltop">
<table cellspacing="0" cellpadding="0" width="100%" border="0">
<!-- begin top menu (save,new,reset,delete,history)-->
<tr>
<td class="menuframe" align="right"><asp:button id="btnsave" runat="server" cssclass="save" text="Save"></asp:button><asp:button id="btnnew" runat="server" cssclass="new" text="New" causesvalidation="false"></asp:button>
<asp:button id="btndelete" runat="server" cssclass="delete" text="Delete" causesvalidation="false"></asp:button></td>
</tr>
</table>
<table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
<tr>
<td class="detailsframe">
<!-- begin content table-->
<asp:panel id="pnlrhs" runat="server">
<div class="scrollright2">
<asp:Panel id="pnlLeadEnrollment" Runat="server">
																		<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="60%" align="center">
																			<TR>
																				<TD class="contentcell">
																					<asp:label id="lblPageName" CssClass="label" Runat="server">Page Name</asp:label></TD>
																				<TD class="contentcell4" style="text-align:left">
																					<asp:textbox id="txtPageName" CssClass="textbox" Runat="server"></asp:textbox></TD>
																			</TR>
																			<TR>
																				<TD class="contentcell">
																					<asp:label id="lblVisibleModules" CssClass="label" Runat="server">Add Page To</asp:label></TD>
																				<TD class="contentcell4" style="text-align:left">
																					<asp:CheckBox id="chkAD" runat="server" CssClass="checkboxbold" Text="Admissions"></asp:CheckBox></TD>
																				<asp:TextBox id="txtADParentId" Runat="server" Visible="False"></asp:TextBox>
																			</TR>
																			<TR>
																				<TD class="contentcell"></TD>
																				<TD class="contentcell4" style="text-align:left">
																					<asp:CheckBox id="chkAR" runat="server" CssClass="checkboxbold" Text="Academic Records"></asp:CheckBox></TD>
																				<asp:TextBox id="txtARParentId" Runat="server" Visible="false"></asp:TextBox>
																			</TR>
																			<TR>
																				<TD class="contentcell"></TD>
																				<TD class="contentcell4" style="text-align:left">
																					<asp:CheckBox id="chkFAC" runat="server" CssClass="checkboxbold" Text="Faculty"></asp:CheckBox></TD>
																				<asp:TextBox id="txtFACParentId" Runat="server" Visible="false"></asp:TextBox>
																			</TR>
																			<TR>
																				<TD class="contentcell"></TD>
																				<TD class="contentcell4" style="text-align:left">
																					<asp:CheckBox id="chkFinAid" CssClass="checkboxbold" Runat="server" Text="Financial Aid"></asp:CheckBox></TD>
																				<asp:TextBox id="txtFinAidParentId" Runat="server" Visible="false"></asp:TextBox>
																			</TR>
																			<TR>
																				<TD class="contentcell"></TD>
																				<TD class="contentcell4" style="text-align:left">
																					<asp:CheckBox id="chkHR" CssClass="checkboxbold" Runat="server" Text="Human Resources"></asp:CheckBox></TD>
																				<asp:TextBox id="txtHRParentId" Runat="server" Visible="false"></asp:TextBox>
																			</TR>
																			<TR>
																				<TD class="contentcell"></TD>
																				<TD class="contentcell4" style="text-align:left">
																					<asp:CheckBox id="chkPL" CssClass="checkboxbold" Runat="server" Text="Placement"></asp:CheckBox></TD>
																				<asp:TextBox id="txtPLParentId" Runat="server" Visible="false"></asp:TextBox>
																			</TR>
																			<TR>
																				<TD class="contentcell"></TD>
																				<TD class="contentcell4" style="text-align:left">
																					<asp:CheckBox id="chkSA" CssClass="checkboxbold" Runat="server" Text="Student Accounts"></asp:CheckBox></TD>
																				<asp:TextBox id="txtSAParentId" Runat="server" Visible="false"></asp:TextBox>
																			</TR>
																			<TR>
																				<TD class="contentcell"></TD>
																				<TD class="contentcell4" style="text-align:left">
																					<asp:CheckBox id="chkSys" CssClass="checkboxbold" Runat="server" Text="System"></asp:CheckBox>
																				<asp:TextBox id="txtSysParentId" Runat="server" Visible="false"></asp:TextBox>
															</td>
														</tr>
												</table>
	<!--			end table content-->							</asp:Panel>
</div>
</asp:panel>


</td>
</tr>
</table>
</telerik:radpane>
</telerik:radsplitter>
<asp:textbox id="txtResourceId" Runat="server" Visible="False"></asp:textbox>
<asp:TextBox ID="txtSelectedPageId" runat="server" Visible="false"></asp:TextBox>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<TD class="contentcell" noWrap><asp:textbox id="txtStuEnrollId" Runat="server" Visible="false"></asp:textbox><asp:checkbox id="chkIsInDB" runat="server" Visible="false" checked="False"></asp:checkbox><asp:TextBox ID="txtHierarchyIndex" Runat="server" Visible="False" /></TD>
					<td></td>
				</tr>
				<TR>
					<TD class="contentcell" noWrap></TD>
					<TD class="contentcell4"><asp:textbox id="txtLeadId" CssClass="textbox" Runat="server" Visible="false"></asp:textbox></TD>
				</TR>
			</table>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:validationsummary>
</div> 

</asp:content> 


