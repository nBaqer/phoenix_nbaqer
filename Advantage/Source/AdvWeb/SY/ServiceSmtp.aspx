﻿<%@ Page Title="Smtp Service Configuration" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ServiceSmtp.aspx.vb" Inherits="SY_ServiceSmtp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../Scripts/Advantage.Client.SY.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <style type="text/css">
        h1 {
            width: 100%;
            margin: 5px;
            background-color: white;
        }

        button {
            margin-right: 10px !important;
        }


        #smtpKendoContainer {
            margin-left: 5px;
        }

        .smtp-input:checked ~ .smtp-content {
            display: inline;
            border: cadetblue 1px solid;
            margin: 5px;
            visibility: visible;
        }

        .input[type=text] {
            padding-left: 5px;
        }

        .smtp-content {
            width: 500px;
            border: cadetblue 1px solid;
            margin: 5px;
            padding: 10px;
            /*visibility: hidden;*/
            /*display: none;*/
        }

        .smtpConfig {
            width: 170px !important;
            margin: 5px;
        }



        .smtpLargeTxt {
            width: 500px !important;
            background-color: gainsboro !important;
            margin: 0;
            padding: 5px;
        }

        #SendBy {
            height: 30px;
            margin-left: 10px;
            width: 100px;
        }
    </style>

    <div class="boxContainer">
        <h3>
            <asp:Label ID="headerTitle" runat="server"></asp:Label>
        </h3>
        <div id="smtpKendoContainer">
            <div style="position: relative">
                <br />
                <div>
                    <label id="smtpLabelEmail" class="smtpConfig">Username (Email)</label>
                    <input id="smtpEmail" type="email" required class="k-textbox smtpConfig" />
                    <label id="smtpLabelPaswsword" class="smtpConfig">Password</label>
                    <input id="smtpPassword" type="password" required class="k-textbox smtpConfig" />
                </div>

                <div>
                    <label id="smtpLabelServer" class="smtpConfig" style="margin-right: 68px;">Server</label>
                    <input id="smtpServer" type="text" required class="k-textbox smtpConfig" />
                    <label id="smtpLabelPort" style="margin-right: 38px; margin-left: 6px;">Port</label>
                    <input id="smtpPort" type="number" name="smtpPort" />
                </div>

                <div>
                    <label id="smtpLabelFrom" class="smtpConfig" style="margin-right: 73px;">From</label>
                    <input id="smtpFrom" type="text" required class="k-textbox smtpConfig" />

                </div>

                <div>
                    <input id="smtpSslEnabled" type="checkbox" />
                    <label id="smtpLabelSslEnabled">SSL Enabled</label>
                </div>
            </div>
            <div>
                <br />
                <div>
                    <input id="smtpOauthEnabled" type="checkbox" class="smtp-input" />
                    <label id="smtpLabelOauthEnabled">GMAIL OAuth Enabled</label>
                </div>
                <br />
                <div class="collapsible">
                    <fieldset id="smtpInformativePanel" class="smtp-content">
                        <legend>Important Information about OAuth Configuration</legend>

                        In order to use OAuth Gmail, you need to configure the authorization protocol.
                <br />
                        Use the Test OAuth configuration to be sure that the step was fulfilled. If the test is negative
            you need to remote in the server where Advantage Services (FAME.ADvantages Services)
            is installed and run the console program: OAuthGmailAdvantageConsole.exe 
               
                    </fieldset>

                    <br />
                    <fieldset class="smtp-content">
                        <legend>Status JSON GMAIL SMTP Configuration File</legend>
                        <div style="border: 1px cadetblue solid; width: 500px; border-radius: 3px;">
                            <div class="k-content">
                                <input name="smtpOauthFiles" id="smtpOauthFiles" type="file" />
                            </div>
                        </div>
                        <br />
                        <div>
                            <textarea id="smtpOauthFile" class="k-textbox smtpLargeTxt" readonly="readonly" rows="5"></textarea>
                        </div>
                    </fieldset>
                    <br />
                </div>

                <div>
                    <button id="smtpOauthSave" type="button" class="k-button" style="margin-left: 5px;">Save Configuration</button>
                    <button id="smtpOauthAuth" type="button" class="k-button">Test GMAIL OAuth Configuration</button>
                    <button id="smtpOauthTest" type="button" class="k-button">Send Test Email</button>

                </div>
                <br />

            </div>
        </div>
    </div>



    <!-- Configure page -->
    <script type="text/javascript">

        $(document).ready(function () {

            var smtpService = new SY_SMTP.ServiceSmtp();

        });
    </script>

</asp:Content>

