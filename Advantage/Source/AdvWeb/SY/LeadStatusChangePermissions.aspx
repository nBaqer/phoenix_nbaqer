<%@ page title="" language="vb" MasterPageFile="~/NewSite.master" autoeventwireup="false" codefile="LeadStatusChangePermissions.aspx.vb" inherits="LeadStatusChangePermissions" %>
<%@ mastertype virtualpath="~/NewSite.master"%>
<asp:content id="content1" contentplaceholderid="additional_head" runat="server">
<script language="javascript" src="../js/checkall.js" type="text/javascript"/>
<script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }
    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstLeadStatusChanges">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstLeadStatusChanges" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstLeadStatusChanges" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstLeadStatusChanges" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <asp:Panel ID="panFilter" runat="server">
                                <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                                    <tr>
                                        <td class="contentcell" style="background: transparent;">
                                            <asp:Label ID="lblCampusGroup" runat="server" CssClass="label">Campus Group</asp:Label></td>
                                        <td class="contentcell4" style="background: transparent;">
                                            <asp:DropDownList ID="ddlCampusGroup" runat="server" AutoPostBack="True" CssClass="dropdownlist">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" style="background: transparent;">
                                            <asp:Label ID="lblOrigStatus" runat="server" CssClass="label">Current Status</asp:Label></td>
                                        <td class="contentcell4" style="background: transparent;">
                                            <asp:DropDownList ID="ddlOrigStatus" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" style="background: transparent;">
                                            <asp:Label ID="lblNewStatus" runat="server" CssClass="label" Width="100%">New Status</asp:Label></td>
                                        <td class="contentcell4" style="background: transparent;">
                                            <asp:DropDownList ID="ddlNewStatus" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" style="background: transparent;"></td>
                                        <td class="contentcell4" style="background: transparent;">
                                            <asp:Button ID="btnBuildList" runat="server" CausesValidation="False"
                                                Text="Build List" Width="130"></asp:Button></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstLeadStatusChanges" runat="server" Width="100%" RepeatLayout="Table"
                                    RepeatDirection="Vertical" DataKeyField="LeadStatusChangeId" GridLines="horizontal">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <HeaderTemplate>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="threecolumndatalist" style="width: 40%; border: 1px solid #999">
                                                    <asp:Label ID="lblCampGrpDescrip" runat="server" CssClass="labelbold">Campus Group</asp:Label></td>
                                                <td class="threecolumndatalist" style="width: 30%; border-bottom: 1px solid #999; border-right: 1px solid #999; border-top: 1px solid #999">
                                                    <asp:Label ID="lblOrigStatusDescrip" runat="server" CssClass="labelbold">Current Status</asp:Label></td>
                                                <td class="threecolumndatalist" style="width: 30%; border-bottom: 1px solid #999; border-right: 1px solid #999; border-top: 1px solid #999">
                                                    <asp:Label ID="lblNewStatusDescrip" runat="server" CssClass="labelbold">New Status</asp:Label></td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <SelectedItemTemplate>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="threecolumndatalist" style="width: 40%;">
                                                    <asp:Label ID="label3" runat="server" Text='<%# Container.DataItem("CampGrpDescrip")%>'>
                                                    </asp:Label></td>
                                                <td class="threecolumndatalist" style="width: 30%;">
                                                    <asp:Label ID="label6" runat="server" Text='<%# Container.DataItem("OrigStatusDescrip")%>'>
                                                    </asp:Label></td>
                                                <td class="threecolumndatalist" style="width: 30%;">
                                                    <asp:Label ID="label1" runat="server" Text='<%# Container.DataItem("NewStatusDescrip")%>'>
                                                    </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </SelectedItemTemplate>
                                    <ItemTemplate>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="threecolumndatalist" style="width: 40%;">
                                                    <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                        Text='<%# Container.DataItem("CampGrpDescrip")%>' CommandArgument='<%# Container.DataItem("LeadStatusChangeId")%>'>
                                                    </asp:LinkButton></td>
                                                <td class="threecolumndatalist" style="width: 30%;">
                                                    <asp:LinkButton ID="Linkbutton4" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                        Text='<%# Container.DataItem("OrigStatusDescrip")%>' CommandArgument='<%# Container.DataItem("LeadStatusChangeId")%>'>
                                                    </asp:LinkButton></td>
                                                <td class="threecolumndatalist" style="width: 30%;">
                                                    <asp:LinkButton ID="Linkbutton1" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                        Text='<%# Container.DataItem("NewStatusDescrip")%>' CommandArgument='<%# Container.DataItem("LeadStatusChangeId")%>'>
                                                    </asp:LinkButton></td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                        <tr>
                                            <td colspan="2">
                                                <asp:Panel ID="pnlNotes" runat="server">
                                                    <table>
                                                        <tr>
                                                            <td align="center">
                                                                <asp:Label ID="lblNote" CssClass="label" runat="server" Font-Bold="true">Note: By default all sequences are granted to the Director of Admissions and the System Administrator.</asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="spacertables2"></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <asp:TextBox ID="txtLeadStatusChangeId" runat="server" Visible="False"></asp:TextBox>
                                        <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                        <asp:TextBox ID="txtModDate" runat="server" Visible="False"></asp:TextBox>
                                        <asp:TextBox ID="txtModUser" runat="server" Visible="False"></asp:TextBox>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label">Campus Group</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" AutoPostBack="True" Width="250px" CssClass="dropdownlist"
                                                    TabIndex="1">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="cvCampGrpId" runat="server" Display="None" ControlToValidate="ddlCampGrpId"
                                                    ValueToCompare="00000000-0000-0000-0000-000000000000" Operator="NotEqual" ErrorMessage="Campus Group is required">Campus Group is required</asp:CompareValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblOrigStatusId" runat="server" CssClass="label">Current Status</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlOrigStatusId" runat="server" Width="250px" CssClass="dropdownlist" AutoPostBack="True"
                                                    TabIndex="2">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="cvOrigStatusId" runat="server" Display="None" ControlToValidate="ddlOrigStatusId"
                                                    ValueToCompare="00000000-0000-0000-0000-000000000000" Operator="NotEqual" ErrorMessage="Original Status is required">Original Status is required</asp:CompareValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblNewStatusId" runat="server" CssClass="label">New Status</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlNewStatusId" runat="server" Width="250px" CssClass="dropdownlist" TabIndex="3">
                                                </asp:DropDownList>
                                                <asp:CompareValidator ID="cvNewStatusId" runat="server" Display="None" ControlToValidate="ddlNewStatusId"
                                                    ValueToCompare="00000000-0000-0000-0000-000000000000" Operator="NotEqual" ErrorMessage="New Status is required">New Status is required</asp:CompareValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4" style="text-align: left">
                                                <asp:CheckBox ID="chkIsReversal" runat="server" CssClass="label" Checked="false"
                                                    Text="Is a Reversal" /></td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table width="100%" align="center" border="0">
                                        <tr>
                                            <td class="contentcellheader">
                                                <asp:Label ID="lblAuthorizedRoles" runat="server" CssClass="labelbold">Authorized Roles</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top: 10px">
                                                <asp:DataGrid ID="dgrdRoles" TabIndex="4" runat="server" Width="100%" Visible="True"
                                                    BorderColor="#E0E0E0" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid"
                                                    ShowFooter="True" BorderWidth="1px">
                                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <HeaderStyle CssClass="datagridheaderstyle" Width="50%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Role">
                                                            <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRoleDescrip" runat="server" CssClass="label" Text='<%# Ctype(Container.DataItem, System.Data.DataRowView).Row.GetParentRow("RolesLeadStatusChangePermissions")("RoleDescrip") %>'>
                                                                </asp:Label>
                                                                <asp:Label ID="lblRoleId" runat="server" Text='<%# Container.DataItem("RoleId") %>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:DropDownList ID="ddlFooterRoleId" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="ddlEditRoleId" runat="server" CssClass="dropdownlist" Width="90%">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblEditRoleId" runat="server" CssClass="label" Visible="False" Text='<%# Container.DataItem("RoleId") %>'>
                                                                </asp:Label>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Status">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="30%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle" HorizontalAlign="Left"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblStatusDescrip" runat="server" CssClass="label" Text='<%# Ctype(Container.DataItem, System.Data.DataRowView).Row.GetParentRow("StatusesLeadStatusChangePermissions")("Status") %>'>
                                                                </asp:Label>
                                                                <asp:Label ID="lblStatusId" runat="server" Text='<%# Container.DataItem("StatusId") %>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                            <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                                            <FooterTemplate>
                                                                <asp:DropDownList ID="ddlFooterStatusId" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:DropDownList ID="ddlEditStatusId" runat="server" CssClass="dropdownlist">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblEditStatusId" runat="server" CssClass="label" Visible="False" Text='<%# Container.DataItem("StatusId") %>'>
                                                                </asp:Label>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn>
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="20%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnlButEdit" Text="<img border=0 src=../images//im_edit.gif alt= edit>"
                                                                    CausesValidation="False" runat="server" CommandName="Edit">
																			<img border="0" src="../images/im_edit.gif" alt="edit"></asp:LinkButton>
                                                            </ItemTemplate>
                                                            <FooterStyle Wrap="False"></FooterStyle>
                                                            <FooterTemplate>
                                                                <div style="padding: 5px;">
                                                                    <asp:Button ID="btnAddRow" Text="Add" runat="server" CommandName="AddNewRow"></asp:Button>
                                                                </div>
                                                            </FooterTemplate>
                                                            <EditItemTemplate>
                                                                <asp:LinkButton ID="lnkbutUpdate" Text="<img border=0 src=../images/im_update.gif alt=update>"
                                                                    runat="server" CommandName="Update">
																			<img border="0" src="../images/im_update.gif" alt="update"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkbutDelete" Text="<img border=0 src=../images/delete.gif alt=Delete>"
                                                                    CausesValidation="False" runat="server" CommandName="Delete"></asp:LinkButton>
                                                                <asp:LinkButton ID="lnkbutCancel" Text="<img border=0 src=../images//im_delete.gif alt=Cancel>"
                                                                    CausesValidation="False" runat="server" CommandName="Cancel"></asp:LinkButton>
                                                                <asp:Label ID="lblLeadStatusChangePermissionId" runat="server" Text='<%# Container.DataItem("LeadStatusChangePermissionId") %>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                                <asp:Label ID="lblEditLeadStatusChangePermissionId" runat="server" Text='<%# Container.DataItem("LeadStatusChangePermissionId") %>'
                                                                    Visible="False">
                                                                </asp:Label>
                                                            </EditItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid></td>
                                        </tr>
                                    </table>
                                </div>
                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
                runat="server">
            </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
        <!--end validation panel-->
    </div>

</asp:Content>


