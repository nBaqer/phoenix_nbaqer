﻿Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports Advantage.Business.Logic.Layer
Imports FAME.Advantage.Common

Partial Class EmployeeInfo
    Inherits BasePage

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected state As AdvantageSessionState
    Protected WithEvents lbl2 As System.Web.UI.WebControls.Label
    Protected WithEvents chkForeign As System.Web.UI.WebControls.CheckBox
    Protected empId As String
    Protected strVID As String
    Dim m_Context As HttpContext

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

#End Region

    'Dim x As AdvantageDropDownListName
    Private pObj As New UserPagePermissionInfo
    Protected resourceId As Integer
    Protected moduleid As String
    Protected sdfcontrols As New SDFComponent
    Protected strDefaultCountry As String
    Private campusId As String
    Protected userId As String

    Private mruProvider As MRURoutines
    Protected EmployeeId As String
    Protected boolSwitchCampus As Boolean = False
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function getEmployeeFromStateObject(ByVal paramResourceId As Integer) As BO.EmployeeMRU

        Dim objStateInfo As New AdvantageStateInfo
        Dim objEmployeeState As New BO.EmployeeMRU

        MyBase.GlobalSearchHandler(3)

        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
        HttpContext.Current.Items("Language") = "En-US"

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            EmployeeId = Guid.Empty.ToString()
        Else
            EmployeeId = AdvantageSession.MasterEmployeeId
        End If


        txtEmpId.Text = EmployeeId


        Dim objGetStudentStatusBar As New AdvantageStateInfo

        mruProvider = New MRURoutines(Me.Context, GetAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)
        objGetStudentStatusBar = mruProvider.BuildEmployeeStatusBar(EmployeeId)
        With objGetStudentStatusBar

            AdvantageSession.MasterEmployeeName = objGetStudentStatusBar.NameValue
            AdvantageSession.MasterEmployeeAddress1 = objGetStudentStatusBar.Address1
            AdvantageSession.MasterEmployeeAddress2 = objGetStudentStatusBar.Address2
            AdvantageSession.MasterEmployeeCity = objGetStudentStatusBar.City
            AdvantageSession.MasterEmployeeState = objGetStudentStatusBar.State
            AdvantageSession.MasterEmployeeZip = objGetStudentStatusBar.Zip

        End With

        If Not String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            Master.ShowHideStatusBarControl(True)
            Master.PageObjectId = EmployeeId
            Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
            Master.SetHiddenControlForAudit()
        Else
            Master.ShowHideStatusBarControl(False)
        End If

        With objEmployeeState
            .EmployeeId = New Guid(EmployeeId)
            .Name = objGetStudentStatusBar.NameValue
        End With

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            Return Nothing
        Else
            Return objEmployeeState
        End If


    End Function

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Session("SEARCH") = 0
        Session("EmployerID") = Nothing

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        Dim advantageUserState As User = AdvantageSession.UserState
        userId = AdvantageSession.UserState.UserId.ToString

        Dim objEmployeeState As New BO.EmployeeMRU
        objEmployeeState = getEmployeeFromStateObject(52) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objEmployeeState Is Nothing Then
            RedirectToEmployeeSearchPage(campusId)
            Exit Sub
        End If
        With objEmployeeState
            empId = .EmployeeId.ToString

        End With



        Dim objCommon As New CommonUtilities
        'pObj = Header1.UserPagePermission
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(3, objEmployeeState.Name)
            End If
            'objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"

            '   initialize Date Range Validator. 
            '   Birthdates must be at least 10 years ago and maximum 80 years ago
            '  RangeValidator1.MaximumValue = Date.Now.Subtract(New TimeSpan(3652, 0, 0, 0, 0)).ToShortDateString
            ' RangeValidator1.MinimumValue = Date.Now.Subtract(New TimeSpan(29220, 0, 0, 0, 0)).ToShortDateString
            MyBase.uSearchEntityControlId.Value = ""

            '   Build dropdownlists
            BuildDropDownLists()

            '   populate fields
            Dim employeeInfo As FAME.AdvantageV1.Common.EmployeeInfo = (New EmployeesFacade).GetInfo(empId)
            PopulateFields(employeeInfo)

            '   Set header info and save state
            SetHeaderInfoAndSaveState(employeeInfo)

            MyBase.uSearchEntityControlId.Value = ""

        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT")

            ResetAgeField()
        End If

        '   init buttons for edit
        InitButtonsForEdit()

        ''Set the Delete Button so it prompts the user for confirmation when clicked
        'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        'Set the Birth Date button so that it generates a postback in the onblur event
        'txtBirthDate.Attributes.Add("onblur", "__doPostBack('txtBirthDate','')")
        'GetInputMaskValue()

        'Make OtherState textbox visible
        If chkForeignZip.Checked = True Then
            lblOtherState.Visible = True
            txtOtherState.Visible = True
            ddlStateId.Visible = False
            lblStateId.Visible = False
        Else
            lblOtherState.Visible = False
            txtOtherState.Visible = False
            ddlStateId.Visible = True
            lblStateId.Visible = True
        End If



        'moduleid = HttpContext.Current.Request.Params("Mod").ToString
        moduleid = AdvantageSession.UserState.ModuleCode.ToString
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))


        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfcontrols.GetSDFExists(resourceId, moduleid)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtFirstName.Text) <> "" Then
            sdfcontrols.GenerateControlsEdit(pnlSDF, resourceId, empId, moduleid)
        Else
            sdfcontrols.GenerateControlsNew(pnlSDF, resourceId, moduleid)
        End If

        btnNew.Enabled = False

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '   instantiate component
        Dim empDB As New EmployeesFacade
        Dim errorMessage As String

        'Check If Mask is successful only if Foreign Is not checked
        'If chkForeignZip.Checked = False Then
        '    errorMessage = ValidateFieldsWithInputMasks()
        'Else
        '    errorMessage = ""
        'End If

        errorMessage = ValidateDate()
        If errorMessage = "" Then
            '   update Employee Info 
            Dim result As String = empDB.UpdateInfo(BuildEmployeeInfo(empId), AdvantageSession.UserState.UserName)
            If result <> "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error1", result, "Save Error")
                Exit Sub
            Else
                cbxIsInDB.Checked = True
            End If
        Else
            ' already displayed error
            Exit Sub
        End If
        'End If

        '   populate fields
        Dim employeeInfo As FAME.AdvantageV1.Common.EmployeeInfo = (New EmployeesFacade).GetInfo(empId)
        PopulateFields(employeeInfo)

        '   Set header info and save state
        SetHeaderInfoAndSaveState(BuildEmployeeInfo(empId))


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim sdfid As ArrayList
        Dim sdfidValue As ArrayList
        '        Dim newArr As ArrayList
        Dim z As Integer
        Dim sdfControl As New SDFComponent
        Try
            sdfControl.DeleteSDFValue(empId)
            sdfid = sdfControl.GetAllLabels(pnlSDF)
            sdfidValue = sdfControl.GetAllValues(pnlSDF)
            For z = 0 To sdfid.Count - 1
                sdfControl.InsertValues(empId, Mid(sdfid(z).id, 5), sdfidValue(z))
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    End Sub
    Private Sub ResetAgeField()
        'lblAge.Enabled = False
        'txtAge.Visible = False
    End Sub
    Protected Function ValidateDate() As String
        Dim errMessage As String
        Dim strDOB As String
        errMessage = ""
        txtAge.Text = ""
        If Not IsNothing(txtBirthDate.SelectedDate) Then
            strDOB = txtBirthDate.SelectedDate.Value.ToString
            If IsTextBoxValidDate(strDOB) Then
                txtAge.Text = Age(Date.Parse(strDOB)).ToString
            Else
                If Not FAME.AdvantageV1.Common.EmployeeInfo.IsDOBValid(txtBirthDate.SelectedDate.ToString) Then
                    errMessage = "Invalid Birth Date." + vbCrLf + "Only employees older than " + AdvantageCommonValues.MinimumAcceptableEmployeeAge.ToString + " years are accepted."
                Else
                    errMessage = "Invalid Birth Date"
                End If
                '   Display Error Message
                DisplayRADAlert(CallbackType.Postback, "Error2", errMessage, "Date Error")
            End If
        End If

        Return errMessage
    End Function
    Private Function Age(ByVal DOBDate As Date) As Integer
        If Now.DayOfYear < DOBDate.DayOfYear Then
            Return Now.Year - DOBDate.Year - 1
        Else
            Return Now.Year - DOBDate.Year
        End If
    End Function

    Private Sub PopulateFields(ByVal employeeInfo As FAME.AdvantageV1.Common.EmployeeInfo)
        '   bind Status

        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        Dim ssnMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        With employeeInfo

            'bind Zip checkbox
            If Not (employeeInfo.StatusId = Guid.Empty.ToString) Then
                'ddlStatusId.SelectedValue = employeeInfo.StatusId
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStatusId, .StatusId, .Status)
            End If

            '   IsInDB
            cbxIsInDB.Checked = .IsInDB

            '   bind LastName
            txtLastName.Text = .LastName

            '   bind FirstName
            txtFirstName.Text = .FirstName

            '   bind MI
            txtMI.Text = .MI

            '   bind prefix
            'If Not (.PrefixId = Guid.Empty.ToString) Then ddlPrefixId.SelectedValue = .PrefixId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrefixId, .PrefixId, .Prefix)

            '   bind suffix
            'If Not (.SuffixId = Guid.Empty.ToString) Then ddlSuffixId.SelectedValue = .SuffixId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSuffixId, .SuffixId, .Suffix)

            '   bind SSN
            txtSSN.Text = .SSN
            'If txtSSN.Text <> "" Then
            '    txtSSN.Text = facInputMasks.ApplyMask(ssnMask, txtSSN.Text)
            'End If

            '   bind ID
            txtID.Text = .ID


            '   bind BirthDate
            If Not (.BirthDate = Date.MaxValue) Then
                txtBirthDate.SelectedDate = .BirthDate
                '   Age
                txtAge.Text = Age(.BirthDate).ToString
            Else
                txtBirthDate.SelectedDate = Nothing
                txtAge.Text = ""
            End If

            '   bind Gender
            'If Not (.GenderId = Guid.Empty.ToString) Then ddlGenderId.SelectedValue = .GenderId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGenderId, .GenderId, .Gender)

            '   bind Race
            'If Not (.RaceId = Guid.Empty.ToString) Then ddlRaceId.SelectedValue = .RaceId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRaceId, .RaceId, .Race)

            '   bind Marital Status
            'If Not (.MaritalStatId = Guid.Empty.ToString) Then ddlMaritalStatId.SelectedValue = .MaritalStatId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlMaritalStatId, .MaritalStatId, .MaritalStat)

            '   bind address1
            txtAddress1.Text = .Address1

            '   bind address2
            txtAddress2.Text = .Address2

            '   bind city
            txtcity.Text = .City

            '   bind StateId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateId, .StateId, .State)

            '   bind Zip
            chkForeignZip.Checked = .ForeignZip

            'Get Zip
            If .ForeignZip = 0 Then
                txtzip.Mask = "#####"
                lblZip.ToolTip = zipMask
                lblOtherState.Visible = False
                txtOtherState.Visible = False
                ddlStateId.Visible = True
                lblStateId.Visible = True
            Else
                txtzip.Mask = "aaaaaaaaaaaaaaaaaaaa"
                lblZip.ToolTip = ""
                lblOtherState.Visible = True
                txtOtherState.Visible = True
                ddlStateId.Visible = False
                lblStateId.Visible = False
            End If
            txtzip.Text = .Zip
            'If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            '    txtZip.Text = facInputMasks.ApplyMask(zipMask, txtZip.Text)
            'End If

            '   bind Country
            'ddlCountryId.SelectedValue = .CountryId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountryId, .CountryId, .Country)

            If chkForeignZip.Checked = True Then
                lblOtherState.Visible = True
                txtOtherState.Visible = True
                txtOtherState.Text = .OtherState
                ddlStateId.Visible = False
                lblStateId.Visible = False
            Else
                lblOtherState.Visible = False
                txtOtherState.Visible = False
                ddlStateId.Visible = True
                lblStateId.Visible = True
            End If

            '   bind modUser
            txtModUser.Text = .ModUser

            '   bind ModDate
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Function BuildEmployeeInfo(ByVal empId As String) As FAME.AdvantageV1.Common.EmployeeInfo
        '   instantiate class
        Dim employeeInfo As New FAME.AdvantageV1.Common.EmployeeInfo
        Dim facInputMask As New InputMasksFacade
        '        Dim phoneMask As String
        Dim zipMask As String

        With employeeInfo
            '   IsInDB
            .IsInDB = cbxIsInDB.Checked

            '   get EmpId
            .EmpId = empId

            '   get Status
            .StatusId = ddlStatusId.SelectedValue

            '   get LastName
            .LastName = txtLastName.Text.Trim

            '   get FirstName
            .FirstName = txtFirstName.Text.Trim

            '   get MI
            .MI = txtMI.Text

            '   get prefix
            .PrefixId = ddlPrefixId.SelectedValue

            '   get suffix
            .SuffixId = ddlSuffixId.SelectedValue

            '   get SSN
            'If txtSSN.Text <> "" Then
            '    ssnMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
            '    .SSN = facInputMask.RemoveMask(ssnMask, txtSSN.Text)
            'Else
            '    .SSN = txtSSN.Text
            'End If
            .SSN = txtSSN.Text
            '   bind ID
            .ID = txtID.Text.Trim

            '   get BirthDate
            If Not (txtBirthDate.SelectedDate.ToString = Nothing) Then
                .BirthDate = Date.Parse(CType(txtBirthDate.SelectedDate, String))
            Else
                .BirthDate = Date.MaxValue
            End If

            '   get Gender
            .GenderId = ddlGenderId.SelectedValue

            '   get Race
            .RaceId = ddlRaceId.SelectedValue

            '   get Marital Status
            .MaritalStatId = ddlMaritalStatId.SelectedValue

            '   get address1
            .Address1 = txtAddress1.Text

            '   get address2
            .Address2 = txtAddress2.Text

            '   get city
            .City = txtcity.Text

            '   get StateId
            .StateId = ddlStateId.SelectedValue

            'get checkbox value
            .ForeignZip = chkForeignZip.Checked

            '   get Zip
            If txtzip.Text <> "" And chkForeignZip.Checked = False Then
                zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
                .Zip = facInputMask.RemoveMask(zipMask, txtzip.Text)
            Else
                .Zip = txtzip.Text
            End If

            '   get Country
            .CountryId = ddlCountryId.SelectedValue

            'get otherstate value
            .OtherState = txtOtherState.Text

            'get campusId
            .CampusId = campusId

            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)
        End With
        '   return data
        Return employeeInfo
    End Function
    Private Sub BuildDropDownLists()
        'BuildStatusDDL()
        'BuildPrefixesDDL()
        'BuildSuffixesDDL()
        'BuildRacesDDL()
        'BuildGendersDDL()
        'BuildMaritalStatusesDDL()
        'BuildStatesDDL()
        'BuildCountriesDDL()

        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Dim x1 As StatusesDDLMetadata
        'Dim x2 As PrefixesDDLMetadata
        'Dim x3 As SuffixesDDLMetadata
        'Dim x4 As RacesDDLMetadata
        'Dim x5 As GendersDDLMetadata
        'Dim x6 As MaritalStatusDDLMetadata
        'Dim x7 As StatesDDLMetadata
        'Dim x8 As CountiesDDLMetadata

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing))

        'Prefixes DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlPrefixId, AdvantageDropDownListName.Prefixes, Nothing, True, True))

        'Suffixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlSuffixId, AdvantageDropDownListName.Suffixes, Nothing, True, True))

        'Races DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlRaceId, AdvantageDropDownListName.Races, Nothing, True, True))

        'Genders DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlGenderId, AdvantageDropDownListName.Genders, Nothing, True, True))

        'Marital Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlMaritalStatId, AdvantageDropDownListName.MaritalStatus, Nothing, True, True))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStateId, AdvantageDropDownListName.States, Nothing, True, True))

        'Countries DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlCountryId, AdvantageDropDownListName.Countries, Nothing, True, True))
        'selected index must be the default country if it is defined in configuration file
        'ddlCountryId.SelectedIndex = CommonWebUtilities.GetDefaultCountryIndexInDDL(ddlCountryId)

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub
    'Private Sub BuildStatusDDL()
    '    '   bind the status DDL
    '    Dim statuses As New StatusesFacade

    '    With ddlStatusId
    '        .DataTextField = "Status"
    '        .DataValueField = "StatusId"
    '        .DataSource = statuses.GetAllStatuses()
    '        .DataBind()
    '    End With

    'End Sub
    'Private Sub BuildPrefixesDDL()
    '    '   bind the prefix DDL
    '    Dim prefixes As New PrefixesFacade

    '    With ddlPrefixId
    '        .DataTextField = "PrefixDescrip"
    '        .DataValueField = "PrefixId"
    '        .DataSource = prefixes.GetAllPrefixes()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildSuffixesDDL()
    '    '   bind the suffix DDL
    '    Dim suffixes As New SuffixesFacade

    '    With ddlSuffixId
    '        .DataTextField = "SuffixDescrip"
    '        .DataValueField = "SuffixId"
    '        .DataSource = suffixes.GetAllSuffixes()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub

    'Private Sub BuildRacesDDL()
    '    '   bind the race DDL
    '    Dim races As New RacesFacade

    '    With ddlRaceId
    '        .DataTextField = "RaceDescrip"
    '        .DataValueField = "RaceId"
    '        .DataSource = races.GetAllRaces()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildGendersDDL()
    '    '   bind the Gender DDL
    '    Dim Genders As New GendersFacade

    '    With ddlGenderId
    '        .DataTextField = "GenderDescrip"
    '        .DataValueField = "GenderId"
    '        .DataSource = Genders.GetAllGenders()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildMaritalStatusesDDL()
    '    '   bind the MaritalStatus DDL
    '    Dim maritalStatuses As New MaritalStatusesFacade

    '    With ddlMaritalStatId
    '        .DataTextField = "MaritalStatusDescrip"
    '        .DataValueField = "MaritalStatId"
    '        .DataSource = maritalStatuses.GetAllMaritalStatuses()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildStatesDDL()
    '    '   bind the State DDL
    '    Dim states As New StatesFacade

    '    With ddlStateId
    '        .DataTextField = "StateDescrip"
    '        .DataValueField = "StateId"
    '        .DataSource = states.GetAllStates()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .Items.Insert(1, New ListItem("Other States Not Listed", "5451720D-583D-4364-802A-BF500EDA1234"))
    '        .SelectedIndex = 0
    '    End With

    'End Sub
    'Private Sub BuildCountriesDDL()
    '    '   bind Countries ddl
    '    Dim countries As New PrefixesFacade

    '    '   build Employer Countries ddl
    '    With ddlCountryId
    '        .DataTextField = "CountryDescrip"
    '        .DataValueField = "CountryId"
    '        .DataSource = countries.GetAllCountries()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)

    '    '   Set error condition
    '    Customvalidator1.ErrorMessage = errorMessage
    '    Customvalidator1.IsValid = False

    '    If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
    '        '   Display error in message box in the client
    '        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    '    End If

    'End Sub

    Private Sub SetHeaderInfoAndSaveState(ByVal employeeInfo As FAME.AdvantageV1.Common.EmployeeInfo)

        '   set values for the header
        If CommonWebUtilities.IsValidGuid(strVID) Then
            DirectCast(state(strVID), AdvantageStateInfo).NameCaption = "Employee : "
            DirectCast(state(strVID), AdvantageStateInfo).NameValue = employeeInfo.LastName + ", " + employeeInfo.FirstName
        End If

    End Sub
    Private Sub ClearHeaderInfoAndSaveState()

        '   set values for the header
        If CommonWebUtilities.IsValidGuid(strVID) Then
            DirectCast(state(strVID), AdvantageStateInfo).NameCaption = ""
            DirectCast(state(strVID), AdvantageStateInfo).NameValue = ""
        End If

    End Sub
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreRender
        'state("employeeInfo") = BuildEmployeeInfo(empId)
        ''save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        'Dim controlsToIgnore As New ArrayList()
        ''add save button 
        'controlsToIgnore.Add(btnSave)
        'controlsToIgnore.Add(chkForeignZip)
        ''Add javascript code to warn the user about non saved changes 
        'CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    'Private Sub InitButtonsForLoad()
    '    'btnNew.Enabled = True
    '    'btnDelete.Enabled = False
    '    If pObj.HasFull Or pObj.HasAdd Then
    '        btnSave.Enabled = True
    '    Else
    '        btnSave.Enabled = False
    '    End If

    '    'btnNew.Enabled = False
    '    If pObj.HasFull Or pObj.HasAdd Then
    '        btnNew.Enabled = False
    '    Else
    '        btnNew.Enabled = False
    '    End If

    '    btnDelete.Enabled = False
    'End Sub
    Private Sub InitButtonsForEdit()
        ''btnNew.Enabled = True
        'btnDelete.Enabled = True

        ''Set the Delete Button so it prompts the user for confirmation when clicked
        'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If
        btnNew.Enabled = False
        'If pObj.HasFull Or pObj.HasAdd Then
        '    btnNew.Enabled = True
        'Else
        '    btnNew.Enabled = False
        'End If
    End Sub
    Private Function IsTextBoxValidDate(ByVal birthDate As String) As Boolean
        If birthDate = "" Then Return True
        Try
            Return FAME.AdvantageV1.Common.EmployeeInfo.IsDOBValid(Date.Parse(birthDate))
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function

    'Private Sub txtBirthDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBirthDate.TextChanged
    '    'ValidateDate()
    'End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        If CommonWebUtilities.IsValidGuid(txtEmpId.Text) Then

            'update EmployeeEmergencyContact Info 
            Dim result As String = (New EmployeesFacade).DeleteEmployee(txtEmpId.Text)
            If Not result = "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error4", result, "Delete Error")
            Else
                '   Set header info and save state
                ClearHeaderInfoAndSaveState()

                Try
                    ddlCountryId.SelectedValue = strDefaultCountry
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlCountryId.SelectedIndex = 0
                End Try

                '   redirect to employeesearch page
                'Response.Redirect("EmployeeSearch.aspx?resid=136&mod=HR&cmpid=" + HttpContext.Current.Request.Params("cmpid") + "&VID=" + strVID, True)
                Response.Redirect("NewEmployeeInfo.aspx?resid=292&mod=HR&cmpid=" + HttpContext.Current.Request.Params("cmpid") + "&VID=1", True)

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim sdfControl As New SDFComponent
                sdfControl.DeleteSDFValue(empId)
                sdfControl.GenerateControlsNew(pnlSDF, resourceId, moduleid)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



            End If

        End If
    End Sub
    'Private Function GetInputMaskValue() As String
    '    Dim facInputMasks As New InputMasksFacade
    '    'Dim correctFormat As Boolean
    '    'Dim strMask As String
    '    Dim zipMask As String
    '    'Dim errorMessage As String
    '    'Dim strPhoneReq As String
    '    Dim strZipReq As String
    '    ' Dim strFaxReq As String
    '    Dim ssnMask As String
    '    Dim objCommon As New CommonUtilities

    '    'Get The Input Mask for Phone/Fax and Zip
    '    'Apply Mask Only If Its Local Zip
    '    ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
    '    txtSSN.Mask = Replace(ssnMask, "#", "9")
    '    lblSSN.ToolTip = ""


    '    If chkForeignZip.Checked = False Then
    '        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
    '        txtzip.Mask = Replace(zipMask, "#", "9")
    '        lblZip.ToolTip = zipMask
    '    Else
    '        txtzip.Mask = ""
    '        lblZip.ToolTip = ""
    '    End If


    '    'Get The RequiredField Value
    '    Dim ssnReq As String
    '    strZipReq = objCommon.SetRequiredColorMask("Zip")
    '    ssnReq = objCommon.SetRequiredColorMask("SSN")

    '    'If The Field Is Required Field Then Color The Masked
    '    'Edit Control
    '    'If InStr(strZipReq, "Y") >= 1 Then
    '    'If strZipReq = "Yes" Then
    '    '    txtZip.BackColor = Color.FromName("#ffff99")
    '    'End If
    '    'If ssnReq = "Yes" Then
    '    '    txtSSN.BackColor = Color.FromName("#ffff99")
    '    'End If
    'End Function

    'Private Function ValidateFieldsWithInputMasks() As String
    '    Dim facInputMasks As New InputMasksFacade
    '    Dim correctFormat As Boolean
    '    Dim strMask As String
    '    Dim zipMask As String
    '    Dim errorMessage As String = String.Empty
    '    Dim ssnMask As String

    '    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
    '    zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
    '    ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
    '    'Validate the phone field format. If the field is empty we should not apply the mask
    '    'agaist it.

    '    'Validate the zip field format. If the field is empty we should not apply the mask
    '    'against it.
    '    If txtSSN.Text <> "" Then
    '        correctFormat = facInputMasks.ValidateStringWithInputMask(ssnMask, txtSSN.Text)
    '        If correctFormat = False Then
    '            errorMessage = "Incorrect format for ssn field."
    '        End If
    '    End If


    '    If txtzip.Text <> "" And chkForeignZip.Checked = False Then
    '        correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtzip.Text)
    '        If correctFormat = False Then
    '            errorMessage &= "Incorrect format for zip field."
    '        End If
    '    End If

    '    Return errorMessage

    'End Function

    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignZip.CheckedChanged
        'Dim objCommon As New CommonWebUtilities
        If chkForeignZip.Checked = True Then
            txtOtherState.Enabled = True
            txtzip.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtzip.DisplayMask = ""
            txtzip.Text = String.Empty
            ddlStateId.SelectedIndex = 0
            txtzip.MaxLength = 20
        Else
            txtzip.Mask = "#####"
            'txtzip.DisplayMask = "#####"
            txtzip.Text = String.Empty
            txtzip.MaxLength = 5
        End If
        CommonWebUtilities.SetFocus(Page, txtAddress1)
    End Sub
    Private Sub ddlAddressStateID_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlStateId.SelectedIndexChanged

    End Sub

    Private Sub txtcity_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtcity.TextChanged
        If chkForeignZip.Checked = True Then
            'Dim objCommon As New CommonWebUtilities
            CommonWebUtilities.SetFocus(Page, txtOtherState)
        End If
    End Sub

    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtOtherState.TextChanged
        'Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Page, txtzip)
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click

        Dim sdfControlsx As New SDFComponent
        sdfControlsx.GenerateControlsNewSingleCell(pnlSDF, resourceId, moduleid)
    End Sub

End Class