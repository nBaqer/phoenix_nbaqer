﻿Imports System.Data
Imports System.IO
Imports System.Web
Imports System.Collections.Generic
Imports Telerik.Web.UI.com.hisoftware.api2
Imports NHibernate.Linq
Imports Telerik.Charting.Styles
Imports Telerik.Web.UI.Widgets
Imports Fame.AdvantageV1.BusinessFacade.AR
Imports Fame.AdvantageV1.BusinessFacade
Imports Ionic.Zip
Imports System.Data.OleDb
Imports Fame.Advantage.Common
Imports Fame.Parameters.Info
Imports Telerik.Web.UI

Partial Class DataExport9010
    Inherits BasePage
    Dim strRootFolderName As String
    Dim datefile As String = Guid.NewGuid().ToString()

    Protected MyAdvAppSettings As AdvAppSettings

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Me.RadPopupContol = RadNotification1

        Try
            If Not Page.IsPostBack Then

                BuildCampusRadList()
                RadListBoxCampuses.Items.Where(Function(x) x.Value = Request.QueryString("cmpid")).FirstOrDefault().Checked = True
                BuildProgramsRadList()
                BuildGroupsRadList()

                StudSearch1.ShowEnrollment = False
                StudSearch1.ShowTerm = False
                StudSearch1.ShowAcaYr = False

                activitiesConductedAmount.Text = ".00"
                RadListBoxCampuses.Localization.CheckAll = "All Campuses"
                RadListBoxPrograms.Localization.CheckAll = "All Programs"
                RadListBoxGroups.Localization.CheckAll = "All Groups"
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            DisplayRadNotification("Error in Page Load event", ex.Message)
        End Try

    End Sub

    Protected Sub generateExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles generateExportBtn.Click
        Try
            If Page.IsValid Then

            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            DisplayRadNotification("Error in btnToExcel event: ", ex.Message)
        End Try
    End Sub

    Private Sub BuildCampusRadList()
        Try
            Dim campus As New CampusGroupsFacade
            With RadListBoxCampuses
                .DataTextField = "CampDescrip"
                .DataValueField = "CampusId"
                .DataSource = campus.GetAllCampuses()
                .DataBind()
            End With

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            DisplayRadNotification("Error in BuildCampusRadList", ex.Message)
        End Try

    End Sub
    Private Sub BuildProgramsRadList()

        Try
            Dim selectedCampuses = RadListBoxCampuses.Items.Where(Function(x) x.Checked = True)

            If (Not selectedCampuses.Any()) Then
                Return
            End If

            Dim programDataSet = ProgramsFacade.GetPrograms(selectedCampuses.FirstOrDefault().Value, True, True)

            For Each row As DataRow In programDataSet.Tables(0).Rows
                Dim newItem As New RadListBoxItem

                For Each column As DataColumn In programDataSet.Tables(0).Columns

                    If column.ColumnName = "Descrip" Then
                        newItem.Text = row(column).ToString()
                    End If

                    If column.ColumnName = "Id" Then
                        newItem.Value = row(column).ToString()
                    End If
                Next

                RadListBoxPrograms.Items.Add(newItem)
            Next
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            DisplayRadNotification("Error in BuildProgramsDDL", ex.Message)
        End Try

    End Sub

    Private Sub BuildGroupsRadList()

        Try
            Dim selectedCampuses = RadListBoxCampuses.Items.Where(Function(x) x.Checked = True)

            If (Not selectedCampuses.Any()) Then
                Return
            End If

            Dim groupsFacade = New AdReqsFacade()
            Dim programDataSet = groupsFacade.GetAllLeadGroupsForExistingLeads(selectedCampuses.FirstOrDefault().Value)

            For Each row As DataRow In programDataSet.Tables(0).Rows
                Dim newItem As New RadListBoxItem

                For Each column As DataColumn In programDataSet.Tables(0).Columns

                    If column.ColumnName = "Descrip" Then
                        newItem.Text = row(column).ToString()
                    End If

                    If column.ColumnName = "LeadGrpId" Then
                        newItem.Value = row(column).ToString()
                    End If
                Next

                RadListBoxGroups.Items.Add(newItem)
            Next
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            DisplayRadNotification("Error in BuildProgramsDDL", ex.Message)

        End Try

    End Sub
    Protected Sub RadListBoxCampusesCampuses_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadListBoxCampuses.SelectedIndexChanged
        Try
            BuildProgramsRadList()
            BuildGroupsRadList()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            DisplayRadNotification("Error in ddlCampuses_SelectedIndexChanged", ex.Message)
        End Try
    End Sub

    Protected Sub TransferToParent(ByVal enrollid As String, ByVal fullname As String, ByVal termid As String,
                               ByVal termdescrip As String, ByVal academicyearid As String, ByVal academicyeardescrip As String,
                               ByVal hourtype As String)

        hiddenStuEnrollmentId.Value = enrollid
    End Sub

End Class