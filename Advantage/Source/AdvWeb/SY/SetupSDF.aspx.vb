Imports System.Diagnostics
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections


Partial Class SetupSDF
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblFieldName As Label
    Protected WithEvents txtFieldName As TextBox
    Protected WithEvents lblDataType As Label
    Protected WithEvents ddlDataTypeId As DropDownList
    Protected WithEvents ddlDecimals As TextBox
    Protected WithEvents lblFieldLength As Label
    Protected WithEvents ddlFieldLength As DropDownList
    Protected WithEvents lblComments As Label
    Protected WithEvents txtComments As TextBox
    Protected WithEvents txtStEmploymentId As TextBox
    Protected boolStatus As String
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim userId As String
        Dim advantageUserState As User = AdvantageSession.UserState
        'advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = AdvantageSession.UserState.ModuleCode.ToString
        txtResourceID.Text = ResourceId
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceID.Text, campusId)
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        If Not Page.IsPostBack Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "New")
            BuildStatusDDL()
            BuildDataTypesDDL()
            BindSDF()
            txtSDFId.Text = Guid.NewGuid.ToString
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            InitButtonsForLoad()
        Else
            InitButtonsForEdit()
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
        End If
        txtLen.MaxLength = 5
        headerTitle.Text = Header.Title
    End Sub
    Private Sub BuildStatusDDL()
        'Bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With
    End Sub
    Private Sub BuildDataTypesDDL()
        'Bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlDTypeId
            .DataTextField = "DType"
            .DataValueField = "DTypeId"
            .DataSource = statuses.GetAllDataTypes()
            .DataBind()
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim result As String
        Dim udfsave As New SDFModulePageFacade
        'Dim resList As String
        result = udfsave.UpdateUDFField(BuildUDFInfo(), AdvantageSession.UserState.UserName)
        ChkIsInDB.Checked = True
        If Radiobuttonlist1.SelectedValue = "List" Then
            UpdateSDFList()
        End If
        BindSDF()
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, txtSDFId.Text, ViewState, header1)
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, txtSDFId.Text)
        InitButtonsForEdit()
    End Sub
    Private Function BuildUDFInfo() As UDFInfo
        Dim udfData As New UDFInfo

        With udfData
            .IsInDB = ChkIsInDB.Checked
            .SDFId = txtSDFId.Text
            .SDFDescrip = txtSDFDescrip.Text
            .DtypeId = CType(ddlDTypeId.SelectedValue, Integer)
            If txtLen.Text = "" Then
                .Len = 0
            Else
                .Len = CType(txtLen.Text, Integer)
            End If
            If txtDecimals.Text = "" Then
                .Decimals = 0
            Else
                .Decimals = CInt(txtDecimals.Text)
            End If
            .Comments = txtHelpText.Text
            .IsRequired = chkIsRequired.Checked
            .StatusId = ddlStatusId.SelectedValue
            If Radiobuttonlist1.SelectedValue = "List" Then
                .ValidationType = 2
            ElseIf Radiobuttonlist1.SelectedValue = "Range" Then
                .ValidationType = 1
                .MinValue = txtMinVal.Text
                .MaxValue = txtMaxVal.Text
            Else
                .ValidationType = 0
            End If
            txtModDate.Text = Date.Now
            .ModDate = txtModDate.Text
        End With
        Return udfData
    End Function
    Private Sub BindUDFInfo(ByVal UDFInformations As UDFInfo, ByVal UDFId As String)
        With UDFInformations
            ChkIsInDB.Checked = .IsInDB
            txtSDFId.Text = .SDFId
            txtSDFDescrip.Text = .SDFDescrip
            ddlDTypeId.SelectedValue = .DtypeId
            txtLen.Text = .Len
            txtDecimals.Text = .Decimals
            txtHelpText.Text = .Comments
            chkIsRequired.Checked = .IsRequired
            ddlStatusId.SelectedValue = .StatusId
            If .ValidationType = 1 Then
                Radiobuttonlist1.SelectedValue = "Range"
                pnlListVal.Visible = False
                pnlRange.Visible = True
            ElseIf .ValidationType = 2 Then
                Radiobuttonlist1.SelectedValue = "List"
                pnlListVal.Visible = True
                pnlRange.Visible = False
            Else
                Radiobuttonlist1.SelectedValue = "None"
                pnlListVal.Visible = False
                pnlRange.Visible = False
            End If
            txtMinVal.Text = .MinValue
            txtMaxVal.Text = .MaxValue
            txtModDate.Text = .ModDate


            If Len(.ValueList) >= 1 And Radiobuttonlist1.SelectedValue = "List" Then
                Dim strValueList As String = .ValueList
                Dim strArrayVal As Array = strValueList.Split(",")
                lbxList.DataSource = strArrayVal
                lbxList.DataBind()
            End If
        End With
    End Sub
    Private Sub BindSDF()
        Dim sdfValue As New SDFModulePageFacade
        If radStatus.SelectedItem.Text = "Active" Then
            boolStatus = "True"
        ElseIf radStatus.SelectedItem.Text = "Inactive" Then
            boolStatus = "False"
        Else
            boolStatus = "All"
        End If
        dlstEmployerContact.DataSource = sdfValue.GetAllUDFDatas(boolStatus)
        dlstEmployerContact.DataBind()
    End Sub

    Private Sub GetUDFDetails(ByVal SDFId As String)
        Dim udfDetail As New SDFModulePageFacade
        BindUDFInfo(udfDetail.GetUDFDetails(SDFId), SDFId)
    End Sub

    Private Sub dlstEmployerContact_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstEmployerContact.ItemCommand
        GetUDFDetails(e.CommandArgument)
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, e.CommandArgument, ViewState, header1)
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, e.CommandArgument)
        InitButtonsForEdit()
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        BindEmptyControls()
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)
        InitButtonsForLoad()
    End Sub
    Private Sub Radiobuttonlist1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles Radiobuttonlist1.SelectedIndexChanged
        If Radiobuttonlist1.SelectedValue = "List" Then
            pnlListVal.Visible = True
            pnlRange.Visible = False
            txtMinVal.Text = ""
            txtMaxVal.Text = ""
        ElseIf Radiobuttonlist1.SelectedValue = "Range" Then
            pnlRange.Visible = True
            pnlListVal.Visible = False
            lbxList.Items.Clear()
        Else
            pnlListVal.Visible = False
            pnlRange.Visible = False
            txtMinVal.Text = ""
            txtMaxVal.Text = ""
            lbxList.Items.Clear()
        End If
    End Sub
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        'Dim objcommon As New CommonWebUtilities
        If txtListItem.Text = "" Then
            DisplayErrorMessage("Please specify the item to be added")
            Exit Sub
        End If
        lbxList.Items.Add(txtListItem.Text)
        txtListItem.Text = ""
        CommonWebUtilities.SetFocus(Me.Page, txtListItem)
    End Sub
    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemove.Click
        Dim intSelIndex As Integer
        If lbxList.SelectedIndex >= 0 Then
            intSelIndex = lbxList.SelectedIndex
            lbxList.Items.RemoveAt(intSelIndex)
        Else
            DisplayErrorMessage("Please select the item that needs to be removed")
            Exit Sub
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        ' If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        ' End If
    End Sub
    Private Function UpdateSDFList() As String
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), lbxList.Items.Count)
        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In lbxList.Items
            selectedDegrees.SetValue(item.Value.ToString, i)
            i += 1
        Next

        If i <= 0 Then
            Return -1
        End If

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)

        'update Selected Jobs
        Dim leadGrpFacade As New SDFModulePageFacade
        Dim resUpdateList As String
        resUpdateList = leadGrpFacade.UpdateListValue(CType(Session("UserName"), String), selectedDegrees, txtSDFId.Text)
        If Not resUpdateList = "" Then
            DisplayErrorMessage("A related record exists,you can not perform this operation")
            'Exit Function
        End If
        Return ""
    End Function
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreRender
        If Not Trim(Request.Form("scrollposition")) = "" Then
            Dim i As Integer = CInt(Trim(Request.Form("scrollposition")))
            Session("ScrollValue") = i
        End If
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        If Not (txtSDFId.Text = "") Then
            'instantiate component
            Dim sdffacade As New SDFModulePageFacade
            'Delete The Row Based on EmployerId 
            Dim result As String = sdffacade.DeleteSDF(txtSDFId.Text, Date.Parse(txtModDate.Text))

            'If Delete Fails
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                Exit Sub
            Else
                ChkIsInDB.Checked = False
                BindEmptyControls()
                BindSDF()
                CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)
                InitButtonsForLoad()
            End If
        End If
    End Sub
    Private Sub BindEmptyControls()
        ChkIsInDB.Checked = False
        txtSDFDescrip.Text = ""
        ddlDTypeId.SelectedIndex = 0
        txtLen.Text = ""
        txtDecimals.Text = 0
        txtHelpText.Text = ""
        ddlStatusId.SelectedIndex = 0
        txtSDFId.Text = Guid.NewGuid.ToString
        Radiobuttonlist1.SelectedValue = "None"
        pnlListVal.Visible = False
        pnlRange.Visible = False
        lbxList.Items.Clear()
        txtMinVal.Text = ""
        txtMaxVal.Text = ""
        chkIsRequired.Checked = False
    End Sub
    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radStatus.SelectedIndexChanged
        BindEmptyControls()
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)
        InitButtonsForLoad()
        BindSDF()

    End Sub

    Private Sub ddlDTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlDTypeId.SelectedIndexChanged

        '2 -- Numeric
        '3 -- Date
        '4 -- Boolean
        '1 -- Character

        If ddlDTypeId.SelectedValue = 2 Then
            txtDecimals.Enabled = True
            txtLen.Text = 4
            txtLen.ReadOnly = False
        ElseIf ddlDTypeId.SelectedValue = 3 Then
            txtDecimals.Enabled = False
            txtLen.Text = 8
            txtDecimals.Text = ""
            txtLen.ReadOnly = False
        ElseIf ddlDTypeId.SelectedValue = 4 Then
            txtDecimals.Enabled = False
            txtLen.Text = 1
            txtDecimals.Text = ""
            txtLen.ReadOnly = False
        ElseIf ddlDTypeId.SelectedValue = 1 Then
            txtDecimals.Text = ""
            txtDecimals.Enabled = False
            txtLen.ReadOnly = True
        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then

            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If
        SetbtnDeleteOnClickStandard()
        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            Dim sdffacade As New SDFModulePageFacade
            Dim udf As UDFInfo = sdffacade.GetUDFDetails(txtSDFId.Text)
            If (Not udf.HasData) AndAlso (Not udf.InUse OrElse udf.StatusCode = "I") Then
                SetbtnDeleteOnClickStandard()
            Else
                SetbtnDeleteOnClickInUse(udf.StatusCode, udf.InUse)
            End If
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub

    Private Sub SetbtnDeleteOnClickStandard()
        btnDelete.Attributes.Remove("onclick")
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
    End Sub

    Private Sub SetbtnDeleteOnClickInUse(udfStatusCode As String, inUse As Boolean)
        btnDelete.Attributes.Remove("onclick")
        If (udfStatusCode = "I") Then
            btnDelete.Attributes.Add("onclick", "MasterPage.SHOW_ERROR_WINDOW('This Custom Field is linked to student data<br/>and cannot be deleted.');return false;")
        Else
            If inUse Then
                btnDelete.Attributes.Add("onclick", "MasterPage.SHOW_ERROR_WINDOW('This Custom Field<br/>is currently in use and cannot be deleted.<br/>To Inactivate, set the Status field to Inactive<br/>and save.');return false;")
            Else
                btnDelete.Attributes.Add("onclick", "MasterPage.SHOW_ERROR_WINDOW('This Custom Field is linked to student data<br/>and cannot be deleted.<br/>To Inactivate, set the Status field to Inactive<br/>and save.');return false;")
            End If
        End If
    End Sub

End Class
