﻿<%@ Page Title="Manage Security" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentPageNavigation.aspx.vb" Inherits="StudentPageNavigation" ClientIDMode="AutoID"%>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"/>
   <script type="text/javascript">

       function OldPageResized(sender, args) {
           $telerik.repaintChildren(sender);
       }
   </script>
<style type="text/css">
    .loading
        {
            background-color: #fff;
            height: 100%;
            width: 100%;
        }

        .ie7buttondist .rbDecorated {
            overflow: hidden !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelContent" runat="server" Transparency="30">
        <div class="loading">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/loading3.gif" AlternateText="loading" />
        </div>
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnExportToPdf">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnExportToExcel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <%-- <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingMainPanel"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%-- 
            Comment added by Balaji on April 19 2012
            Need to disable this Ajax code and force a postback as 
            the added page needs to show up under Faculty
    --%>
    <%-- <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>--%>

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                        <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="false"></asp:Button>
                        <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False" Enabled="false"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3>
                                <asp:Label ID="headerTitle" runat="server"></asp:Label>
                            </h3>
                            <!-- begin content table-->
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                <tr>
                                    <td class="contentcell">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="700px">
                                            <tr>
                                                <td class="contentcellheadernoborderleft" nowrap width="500px">
                                                    <asp:Label ID="Label1" runat="server" Font-Bold="true" CssClass="label" Width="883px">Add student pages to selected modules</asp:Label></td>
                                                <td class="contentcellheadernobordercenter" nowrap width="150px">
                                                    <asp:Button ID="btnExportToPdf" runat="server" Text="Export to PDF" OnClick="btnExportToPdf_Click" CssClass="exporttopdfbutton" Visible="false" />
                                                <td class="contentcellheadernoborderright" nowrap width="150px">
                                                    <asp:Button ID="btnExportToExcel" runat="server" Text="Export to Excel" OnClick="btnExportToExcel_Click" CssClass="exporttopdfbutton" Visible="false" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <%-- 
                                                       Comment added by Balaji on April 19 2012
                                                        
                                                       Paging has been disabled in this radGrid for the following reason

                                                       RadGrid.MasterTableView.Items will return only GridDataItems on the current page, i.e PageSize will be the same as RadGrid.MasterTableView.Items.Count. 
                                                       This are the items which RadGrid is bound to. It does not persist all items that is supplied with the DataSource property, it takes only PageSize items

                                                       This will force users to make changes in Page 1 and hit Save. Move on to Page 2, make changes and Save. In other words, users will have to 
                                                       hit Save button to save changes when they are on each page, even though its just one grid. 

                                                       To avoid this design flaw in telerik radGrid, we will disable paging and let users see all data in one page
                                                       
                                                       
                                                      
                                        --%>

                                        <table>
                                            <tr>
                                                <td>
                                                    <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="true"
                                                        AutoGenerateColumns="False" AllowSorting="false"
                                                        AllowMultiRowSelection="false" GridLines="None"
                                                        PageSize="100" Height="600px"
                                                        Visible="true" HeaderStyle-BackColor="#355b99" OnNeedDataSource="RadGrid1_NeedDataSource">
                                                        <PagerStyle Mode="NextPrevAndNumeric"></PagerStyle>
                                                        <ClientSettings>
                                                            <Scrolling AllowScroll="true" EnableVirtualScrollPaging="false" UseStaticHeaders="true" />
                                                        </ClientSettings>
                                                        <ItemStyle HorizontalAlign="Left" />
                                                        <AlternatingItemStyle HorizontalAlign="Left" />
                                                        <MasterTableView DataKeyNames="ChildResourceId" TableLayout="Fixed">
                                                            <Columns>
                                                                <telerik:GridTemplateColumn Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox runat="server" ID="txtParentResourceId" Text='<%# Bind("ParentResourceId") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtResourceTypeId" Text='<%# Bind("ResourceTypeId") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtChildResourceId" Text='<%# Bind("ChildResourceId") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtChildResource" Text='<%# Bind("ChildResource") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtAccessLevel" Text=''></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtTabId" Text='<%# Bind("TabId") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtIsInAR" Text='<%# Bind("isPageInAR") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtIsInFAC" Text='<%# Bind("isPageInFAC") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtIsInFA" Text='<%# Bind("isPageInFA") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtIsInPL" Text='<%# Bind("isPageInPL") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtIsInSA" Text='<%# Bind("isPageInSA") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtIsPageShippedWithAR" Text='<%# Bind("isPageShippedWithAR") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtIsPageShippedWithFAC" Text='<%# Bind("isPageShippedWithFAC") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtIsPageShippedWithFA" Text='<%# Bind("isPageShippedWithFA") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtIsPageShippedWithPL" Text='<%# Bind("isPageShippedWithPL") %>'></asp:TextBox>
                                                                        <asp:TextBox runat="server" ID="txtIsPageShippedWithSA" Text='<%# Bind("isPageShippedWithSA") %>'></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderStyle-Width="170px" ItemStyle-Width="170px">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblResource" runat="server" Text="Resource" CssClass="labelbold" Style="color: White;"></asp:Label>
                                                                    </HeaderTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                    <ItemTemplate>
                                                                        <asp:Label runat="server" ID="lblParentModule" Text='<%# Bind("ParentResource") %>' CssClass="label" Style="font-weight: bold; padding-left: 0px;"></asp:Label>
                                                                        <asp:Label runat="server" ID="txtParentResource" Text='<%# Bind("ParentResource") %>' CssClass="label" Style="font-weight: bold; padding-left: 2px; color: White;"></asp:Label>
                                                                        <asp:Label runat="server" ID="lblChildResourceDesc" Text='<%# Bind("ChildResource") %>' CssClass="label" Style="padding-left: 15px;"></asp:Label>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderStyle-Width="70px" ItemStyle-Width="70px">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblPermission1" runat="server" Text="Academics" CssClass="labelbold" Style="color: White;"></asp:Label>
                                                                    </HeaderTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                    <ItemTemplate>
                                                                        <telerik:RadButton ID="chkAR" runat="server" Text="" ToggleType="CheckBox" ButtonType="ToggleButton" AutoPostBack="true"></telerik:RadButton>

                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderStyle-Width="50px" ItemStyle-Width="50px">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblPermission2" runat="server" Text="Faculty" CssClass="labelbold" Style="color: White;" AutoPostBack="true"></asp:Label>
                                                                    </HeaderTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                    <ItemTemplate>
                                                                        <telerik:RadButton ID="chkFAC" runat="server" Text="" ToggleType="CheckBox" ButtonType="ToggleButton" AutoPostBack="true"></telerik:RadButton>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderStyle-Width="70px" ItemStyle-Width="70px">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblPermission3" runat="server" Text="Financial Aid" CssClass="labelbold" Style="color: White;"></asp:Label>
                                                                    </HeaderTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                    <ItemTemplate>
                                                                        <telerik:RadButton ID="chkFA" runat="server" Text="" ToggleType="CheckBox" ButtonType="ToggleButton" AutoPostBack="true"></telerik:RadButton>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderStyle-Width="60px" ItemStyle-Width="60px">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblPermission4" runat="server" Text="Placement" CssClass="labelbold" Style="color: White;"></asp:Label>
                                                                    </HeaderTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                    <ItemTemplate>
                                                                        <telerik:RadButton ID="chkPL" runat="server" Text="" ToggleType="CheckBox" ButtonType="ToggleButton" AutoPostBack="true"></telerik:RadButton>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridTemplateColumn HeaderStyle-Width="80px" ItemStyle-Width="80px">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblPermission5" runat="server" Text="Student Accounts" CssClass="labelbold" Style="color: White;"></asp:Label>
                                                                    </HeaderTemplate>
                                                                    <ItemStyle Wrap="true" />
                                                                    <ItemTemplate>
                                                                        <telerik:RadButton ID="chkSA" runat="server" Text="" ToggleType="CheckBox" ButtonType="ToggleButton" AutoPostBack="true"></telerik:RadButton>
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>

                                                            </Columns>
                                                        </MasterTableView>
                                                        <%--<clientsettings>
                                                              <Selecting AllowRowSelect="True"></Selecting>
                                                            </clientsettings>--%>
                                                    </telerik:RadGrid>
                                                </td>

                                            </tr>

                                        </table>

                                    </td>
                                </tr>
                            </table>



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
            <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
            <telerik:RadNotification runat="server" ID="RadNotification1"
                Text="This is a test" ShowCloseButton="true"
                Width="500px" Height="205px"
                TitleIcon=""
                Position="Center" Title="Message"
                EnableRoundedCorners="true"
                EnableShadow="true"
                Animation="Fade"
                AnimationDuration="1000"
                Style="padding-left: 120px; padding-top: 5px; word-spacing: 2pt;">
            </telerik:RadNotification>
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>


