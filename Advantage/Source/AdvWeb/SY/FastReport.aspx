﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="FastReport.aspx.vb" Inherits="AdvWeb.SY.SyFastReport" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../Scripts/Advantage.Client.SY.js"></script>
   <%-- <script>
        function pageLoad() {
            var element = document.querySelector('table[id*=_fixedTable] > tbody > tr:last-child > td:last-child > div');
            if (element) {
                element.style.overflow = "scroll";
                element.style.maxWidth = "800px";
            }
        }
    </script>--%>
    <style >
        .repViewer{overflow-y:auto !important;}
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <div id="panelF" style="width: 100%; height: 30px; background-color: gainsboro; font-size: 18px; font-weight: bold; padding-left: 10px"></div>
    <aside style="width: 20%; max-width: 250px; float: left; position: relative; margin-left: 10px; margin-top: 5px;">
        <div>
            <%-- <div style="width: 200px;">
                <h3 style="width: 100%">Reports List</h3>
            </div>--%>
            <div id="FastReportTreeView"></div>
        </div>
    </aside>
    <article style="float: left; position: relative; height: 30px; width: 80%; min-height: 730px; margin-top: 10px; background-color: azure; ">
        <div style="background-color: gainsboro; height: 30px">
            <label id="fastReportTitle" style="font-size: 16px; margin-left: 10px; font-weight: bold;">Please select a report</label>
        </div>
        <div id="hideReportDiv" style="position: relative; height: 770px; visibility: visible" >
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt"  ForeColor="#FFF"
                KeepSessionAlive="False" ProcessingMode="Remote" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"
                Width="100%" Height="700px" CssClass="repViewer">
            </rsweb:ReportViewer>
        </div>
    </article>

    <%-- ReSharper disable UnusedLocals --%>
    <script type="text/javascript">

        $(document).ready(function () {

            var reportService = new SY_FAST_REPORT.FastReport();

        });
    </script>
    <%-- ReSharper restore UnusedLocals --%>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

