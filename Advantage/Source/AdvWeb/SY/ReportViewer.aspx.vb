Imports System.Data
Imports AdvWeb.VBCode.ComponentClasses
Imports CrystalDecisions.CrystalReports.Engine
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common

Namespace AdvWeb.SY

    Partial Class ReportViewer
        Inherits Page

        Protected PageTitle As String
        'Variable Added By Vijay Ramteke om May12,2009
        Protected MRptParamInfo As ReportParamInfo
        'Variable Added By Vijay Ramteke om May12,2009
        Dim objReport As New ReportDocument
        Protected MyAdvAppSettings As AdvAppSettings

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected Sub Page_Disposed(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Disposed
            objReport.Close()
            objReport.Dispose()
        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()

            LoadReport()
        End Sub

#End Region

        Private Sub LoadReport()
            '        Dim i As Integer
            Dim rptParamInfo As Object
            Dim ds As DataSet


            ' get report parameters
            Select Case Session("RptAgency")
                Case IPEDSCommon.AgencyName
                    rptParamInfo = DirectCast(Session("RptInfo"), ReportParamInfoIPEDS)
                Case Else
                    rptParamInfo = DirectCast(Session("RptInfo"), ReportParamInfo)
            End Select

            ' set page title
            PageTitle &= CType(rptParamInfo.RptTitle, String)

            ' get report DataSet
            ds = DirectCast(Session("ReportDS"), DataSet)
            Select Case Session("RptAgency")
                Case IPEDSCommon.AgencyName
                    objReport = New AdvantageReportIPEDS(ds, CType(rptParamInfo, ReportParamInfoIPEDS))
                Case Else
                    'DisplayErrorMessage(Utils.crystalCount)
                    objReport = (New AdvantageReportFactory).CreateAdvantageReport(ds, CType(rptParamInfo, ReportParamInfo))
            End Select

            ' Try
            MyAdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim transcriptTypeString = CType(MyAdvAppSettings.AppSettings("TranscriptType"), String)

            MRptParamInfo = New ReportParamInfo
            MRptParamInfo = CType(rptParamInfo, ReportParamInfo)

            If MRptParamInfo.ResId = 295 Then
                '**8/14/09 - DD** If no OrderBy selected, default to sort by StartDate, TimeIn (Class Start Time)
                If MRptParamInfo.OrderBy = "" Then
                    objReport.DataDefinition.FormulaFields("ClassStartDate").Text = "{ClassMeetings.StartDate}"
                    objReport.DataDefinition.FormulaFields("ClassStartTime").Text = "{ClassMeetings.TimeIn}"
                End If
            End If

            If MRptParamInfo.ResId = 238 Then
                ' For Single Transcript
                If transcriptTypeString = "Traditional" Then
                    If MRptParamInfo.ShowRptDateIssue = True Then
                        objReport.ReportDefinition.ReportObjects("Descrip1").Width = 2640
                    Else
                        objReport.ReportDefinition.ReportObjects("Descrip1").Width = 3720
                    End If
                ElseIf transcriptTypeString = "Traditional_A" Then
                    If MRptParamInfo.ShowRptDateIssue = True Then
                        objReport.ReportDefinition.ReportObjects("Descrip1").Width = 3120
                    Else
                        objReport.ReportDefinition.ReportObjects("Descrip1").Width = 4035
                    End If
                ElseIf transcriptTypeString = "Traditional_B" Then
                    If MRptParamInfo.ShowRptDateIssue = True Then
                        objReport.ReportDefinition.ReportObjects("Descrip").Width = 3180
                    Else
                        objReport.ReportDefinition.ReportObjects("Descrip").Width = 4080
                    End If
                Else
                    'If m_rptParamInfo.ShowRptDateIssue = True Then
                    '    objReport.ReportDefinition.ReportObjects("Descrip1").Width = 3075
                    'Else
                    '    objReport.ReportDefinition.ReportObjects("Descrip1").Width = 3840
                    'End If

                End If
            End If

            If MRptParamInfo.ResId = 245 Then
                ' For Multiple Transcript
                If transcriptTypeString = "Traditional" Then
                    If MRptParamInfo.ShowRptDateIssue = True Then
                        objReport.ReportDefinition.ReportObjects("Descrip1").Width = 2520
                    Else
                        objReport.ReportDefinition.ReportObjects("Descrip1").Width = 3510
                    End If
                ElseIf transcriptTypeString = "Traditional_A" Then
                    If MRptParamInfo.ShowRptDateIssue = True Then
                        objReport.ReportDefinition.ReportObjects("Descrip1").Width = 4455
                    Else
                        objReport.ReportDefinition.ReportObjects("Descrip1").Width = 5370
                    End If
                ElseIf transcriptTypeString = "Traditional_B" Then
                    If MRptParamInfo.ShowRptDateIssue = True Then
                        objReport.ReportDefinition.ReportObjects("Descrip1").Width = 2640
                    Else
                        objReport.ReportDefinition.ReportObjects("Descrip1").Width = 3465
                    End If
                Else
                End If
            End If
            ' Catch ex As Exception
             ' 	Dim exTracker = new AdvApplicationInsightsInitializer()
            ' 	exTracker.TrackExceptionWrapper(ex)


            'End Try

            'Code Added By Vijay Ramteke on May 12, 2009
            Session("RptObject") = objReport

            'End If

            ' no need to explicitly set ReportSource property of Crystal Report Viewer object, will be done
            '	by code in user control CrystalRptViewer.ascx.vb
        End Sub
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load

            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' set page title
            Page.DataBind()
        End Sub

        'Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '    '   Display error in message box in the client
        '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        'End Sub

        Protected Sub Page_Unload(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Unload
            'created by balaji on 06/19/2007
            'This code was added to page-unload event to fix the following error
            'The maximum report processing job limits configured by your system administrator has been reached
            'Approach 2 : dispose as each and every page is navigated
            objReport.Close()
            objReport.Dispose()
            Session("RptObject") = Nothing
        End Sub
    End Class
End Namespace