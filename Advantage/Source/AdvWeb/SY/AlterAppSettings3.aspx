﻿<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="AlterAppSettings3.aspx.vb" Inherits="SY_AlterAppSettings3" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }



    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save" Enabled="false"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false" Enabled="false"></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false" Enabled="false"></asp:Button></td>
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>

                                    <asp:Label ID="lblNotification" runat="server" CssClass="tothemebold" ForeColor="Red" Font-Bold="true"></asp:Label>

                                    <div id="ManageConfigExportBtn" class="manageconfigexport" style="vertical-align: left;">
                                        <div style="margin-bottom: 10px;">
                                            <asp:Button ID="btnExporttoPDF" runat="server" Text="Export to PDF" />

                                        </div>
                                    </div>


                                    <div id="ConfigSettRadGrid" class="manageconfigradgrid">

                                        <%--Until we are ready with the Global and Campus Specific coding, set 
Visible prop to false for: Button_Add,  Button_Delete and Campus  8/5/10 DD
                                        --%>

                                        <telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True"
                                            AllowPaging="True" AutoGenerateColumns="False" AllowSorting="True" AllowAutomaticInserts="True"
                                            GroupingEnabled="False" ClientSettings-Resizing-AllowColumnResize="true" GridLines="Horizontal">


                                            <ItemStyle HorizontalAlign="Left" Wrap="true" />
                                            <AlternatingItemStyle HorizontalAlign="Left" Wrap="true" />

                                            <ExportSettings ExportOnlyData="true">
                                                <Pdf FontType="Subset" PaperSize="Letter" PageWidth="1100" PageHeight="850" PageLeftMargin="10px"
                                                    PageRightMargin="10px" PageTopMargin="10px" PageBottomMargin="10px" />
                                                <Excel Format="Html" />
                                                <Csv ColumnDelimiter="Colon" RowDelimiter="NewLine" />
                                            </ExportSettings>



                                            <MasterTableView AutoGenerateColumns="False" PageSize="10" EditMode="InPlace" AllowCustomSorting="true" GridLines="Horizontal">
                                                <ItemStyle Wrap="true" />
                                                <RowIndicatorColumn>
                                                    <HeaderStyle Width="20px"></HeaderStyle>
                                                </RowIndicatorColumn>

                                                <ExpandCollapseColumn>
                                                    <HeaderStyle Width="20px"></HeaderStyle>
                                                </ExpandCollapseColumn>
                                                <Columns>


                                                    <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../Images/icon/icon_add.png" HeaderText="Add"
                                                        UniqueName="Button_Add" CommandName="PerformInsert" Visible="true">
                                                        <HeaderStyle Font-Bold="True" Width="50px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    </telerik:GridButtonColumn>


                                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderText="Edit" UniqueName="Edit" EditImageUrl="~\images\icon\icon_edit.png" CancelImageUrl="~\images\icon\icon_cancel.png" UpdateImageUrl="~\images\icon\icon_save.png" InsertImageUrl="~\images\icon\icon_save.png">
                                                        <HeaderStyle Font-Bold="True" Width="50px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    </telerik:GridEditCommandColumn>

                                                    <telerik:GridButtonColumn ButtonType="ImageButton" ImageUrl="../Images/icon/icon_delete.png" HeaderText="Delete"
                                                        UniqueName="Button_Delete" CommandName="Delete" Visible="true">
                                                        <HeaderStyle Font-Bold="True" Width="50px" HorizontalAlign="Center" />
                                                        <ItemStyle HorizontalAlign="Center" Wrap="true" />
                                                    </telerik:GridButtonColumn>




                                                    <telerik:GridBoundColumn DataField="SettingId" Display="False"
                                                        HeaderText="SettingId" UniqueName="SettingId" Visible="true">
                                                    </telerik:GridBoundColumn>

                                                    <telerik:GridBoundColumn DataField="ValueId" Display="False"
                                                        HeaderText="ValueId" UniqueName="ValueId" Visible="true">
                                                    </telerik:GridBoundColumn>



                                                    <telerik:GridBoundColumn DataField="KeyName" HeaderText="Setting Name"
                                                        MaxLength="500" UniqueName="KeyName" ReadOnly="True"
                                                        FilterControlWidth="375px" AllowSorting="True" GroupByExpression="KeyName"
                                                        SortExpression="KeyName" HeaderStyle-Wrap="true" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains">
                                                        <HeaderStyle Font-Bold="True" />
                                                        <HeaderStyle Width="420px" />
                                                        <ItemStyle Wrap="true" Width="420px" />
                                                    </telerik:GridBoundColumn>



                                                    <telerik:GridTemplateColumn UniqueName="Value" HeaderText="Value"
                                                        AllowFiltering="False" ItemStyle-Wrap="true" FilterControlWidth="200px">

                                                        <HeaderStyle Width="300px" HorizontalAlign="Left" />
                                                        <ItemStyle Width="300px" Wrap="true" HorizontalAlign="Left" />

                                                        <HeaderTemplate>
                                                            <asp:Label ID="label1" Text="Value" runat="server"></asp:Label>
                                                        </HeaderTemplate>


                                                        <ItemTemplate>
                                                            <asp:Label ID="lblValue" runat="server" Width="220px" Text='<%# Eval("Value") %>'></asp:Label>
                                                        </ItemTemplate>

                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlValue" runat="server" Width="230px"></asp:DropDownList>
                                                            <asp:TextBox ID="txtValue" runat="server" Text='<%# Eval("Value") %>' Width="225"></asp:TextBox>
                                                        </EditItemTemplate>

                                                        <HeaderStyle Font-Bold="True" />
                                                    </telerik:GridTemplateColumn>



                                                    <telerik:GridTemplateColumn UniqueName="Campus" HeaderText="Campus"
                                                        AllowFiltering="True" ItemStyle-Wrap="true" FilterControlWidth="143px"
                                                        AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" DataField="Campus" Visible="true">


                                                        <HeaderStyle Width="170px" />
                                                        <ItemStyle Width="170px" />

                                                        <HeaderTemplate>
                                                            <asp:Label ID="label2" Text="Campus" runat="server"></asp:Label>
                                                        </HeaderTemplate>

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCampus" runat="server" Width="170px" Text='<%# Eval("Campus") %>'></asp:Label>
                                                        </ItemTemplate>

                                                        <EditItemTemplate>
                                                            <telerik:RadComboBox ID="ddlCampus" runat="server" Width="165px" NoWrap="true">
                                                            </telerik:RadComboBox>
                                                        </EditItemTemplate>

                                                        <HeaderStyle Font-Bold="True" />
                                                    </telerik:GridTemplateColumn>




                                                    <telerik:GridBoundColumn AllowSorting="False" DataField="Description"
                                                        HeaderText="Description" MaxLength="1000" UniqueName="Description"
                                                        FilterControlWidth="350px" ReadOnly="True" ItemStyle-HorizontalAlign="Left">

                                                        <HeaderStyle Font-Bold="True" Width="400px" />


                                                        <ItemStyle Width="400px" Wrap="true" />

                                                    </telerik:GridBoundColumn>


                                                    <telerik:GridBoundColumn DataField="CampusSpecific" Display="False"
                                                        HeaderText="CampusSpecific" UniqueName="CampusSpecific" Visible="False">
                                                    </telerik:GridBoundColumn>


                                                </Columns>

                                                <EditFormSettings>
                                                    <EditColumn UniqueName="EditCommandColumn1"></EditColumn>
                                                </EditFormSettings>
                                            </MasterTableView>
                                            <ClientSettings AllowColumnsReorder="True">

                                                <Selecting AllowRowSelect="True" />


                                                <Resizing AllowColumnResize="True"></Resizing>
                                            </ClientSettings>

                                        </telerik:RadGrid>
                                    </div>
                                    <input id="CampusSpecificDelete" type="hidden" runat="server" />
                                </div>
                            </asp:Panel>
                            <!--end table content-->

                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
    </div>
    <script type="text/javascript">

        //------------------------------------------------------------
        // Clear the session storage when the user changes a config
        // setting so that the changes to exclusion of menu items are
        // seen immediately without closing the browser first
        //------------------------------------------------------------
        function clearSessionStorage() {
            if (typeof (Storage) == "undefined") return;

            var sessionStorageKeys = new Array();

            for (var name in sessionStorage) {
                sessionStorageKeys.push(name);
            }

            for (var i = 0; i < sessionStorageKeys.length; i++) {
                var key = sessionStorageKeys[i];
                if (key != "currentModuleName" && key != "currentSubLevelName") {
                    sessionStorage.removeItem(key);
                }
            }
        }
    </script>
</asp:Content>


