﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReportParamsDateDDL.aspx.vb"
    Inherits="SY_ReportParamsDateDDL" MasterPageFile="~/NewSite.master" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="headinfo" ContentPlaceHolderID="additional_head" runat="server">
    <title>
        <%# PageTitle %>
    </title>
    <script type="text/javascript">
        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }
    </script>
    <script type="text/javascript" src="../Scripts/scrollsaver.min.js"></script>
</asp:Content>
<asp:Content ID="content1" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain2" runat="server">
    <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%"
        Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="250"
            Scrolling="Y">
            <%--<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<!-- begin leftcolumn -->
				<tr>
					<td class="listframe">--%>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop">
                        <asp:Label ID="lblPreferences" runat="server" CssClass="label" Text="Saved User Reports">Saved Report Selections</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfilters">
                            <asp:DataList ID="dlstPrefs" DataKeyField="PrefId" RepeatDirection="Vertical" runat="server">
                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Container.DataItem("PrefName")%>' runat="server" CssClass="itemstyle"
                                        CommandArgument='<%# Container.DataItem("PrefId")%>' ID="Linkbutton1" CausesValidation="False" />
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
        <!-- end leftcolumn -->
        <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both"
            orientation="horizontaltop" left="250px">
            <!-- begin rightcolumn -->
            <table id="table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" CausesValidation="false">
                        </asp:Button>
                        <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="false">
                        </asp:Button>
                        <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false">
                        </asp:Button>
                    </td>
                </tr>
            </table>
            <!-- end top menu (save,new,reset,delete,history)-->
            <!--begin right column-->
            <table class="maincontenttable" id="table5" cellspacing="0" cellpadding="0" width="80%"
                style="padding-left: 10px;" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="scrollrightreports">
                            <!-- begin table content-->
                            <asp:Panel ID="pnlPreference" CssClass="textbox" runat="server" Width="95%" Visible="true" Height="38px"
                                Style="padding-left: 2px">
                                <asp:Table ID="table7" CssClass="label" runat="server" Width="300px">
                                    <asp:TableRow Height="35px">
                                        <asp:TableCell Width="125px"  Wrap="false">
                                            <asp:Label ID="lblPrefName" runat="server" CssClass="labelbold">Preference name</asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="175px" >
                                            <asp:TextBox ID="txtPrefName" runat="server" CssClass="textbox" MaxLength="50" Width="175px"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                            <p>
                            </p>
                            <asp:Panel ID="pnl5" CssClass="textbox" runat="server" Visible="true" Width="95%">
                                <asp:Table ID="table8" CssClass="label" runat="server" Width="300px">
                                    <asp:TableRow>
                                        <asp:TableCell Width="125px" Wrap="false">
                                            <asp:Label ID="lbl4" CssClass="label" runat="server" Font-Bold="true">Required filter(s)</asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell Width="175px" Wrap="false">
                                            <asp:Label ID="lblRequiredFilters" CssClass="label" runat="server"></asp:Label>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                            <!-- end top panel -->
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contenttable">
                                <tr>
                                    <td class="lsreport">
                                        <!-- begin left side report params -->
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contenttable">
                                            <tr>
                                                <td colspan="3" class="lsheaderreport">
                                                    <asp:Label ID="lblSelectSort" runat="server" CssClass="label" Font-Bold="True">Step 1: Please select the fields to sort the report by:</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="lscontentreport1">
                                                    Available Sort Order
                                                </td>
                                                <td class="lscontentreport2">
                                                    &nbsp;
                                                </td>
                                                <td class="lscontentreport1">
                                                    Selected Sort Order
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="lscontentreport3">
                                                    <asp:ListBox ID="lbxSort" runat="server" Rows="4" CssClass="todocumentlist1"></asp:ListBox>
                                                </td>
                                                <td class="lscontentreport2">
                                                    <asp:Button ID="btnAdd" CssClass="buttons1" Text="Add >" runat="server" CausesValidation="False">
                                                    </asp:Button><br />
                                                    <asp:Button ID="btnRemove" CssClass="buttons1" Text="< Remove" runat="server" CausesValidation="False">
                                                    </asp:Button>
                                                </td>
                                                <td class="lscontentreport3">
                                                    <asp:ListBox ID="lbxSelSort" runat="server" Rows="4" CssClass="todocumentlist1">
                                                    </asp:ListBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="lsheaderreport">
                                                    <asp:Label ID="lblSelectFilters" runat="server" CssClass="label" Font-Bold="True">Step 2: Please select the filters to apply to the report:</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="lscontentreport1">
                                                    Filter
                                                </td>
                                                <td class="lscontentreport2">
                                                    &nbsp;
                                                </td>
                                                <td class="lscontentreport1">
                                                    Values
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="lscontentreport3">
                                                    <asp:ListBox ID="lbxFilterList" runat="server" Rows="4" CssClass="todocumentlist1"
                                                        AutoPostBack="True"></asp:ListBox>
                                                </td>
                                                <td class="lscontentreport2">
                                                    &nbsp;
                                                </td>
                                                <td class="lscontentreport3">
                                                    <asp:ListBox ID="lbxSelFilterList" runat="server" Rows="4" CssClass="todocumentlist1"
                                                        AutoPostBack="True" SelectionMode="Multiple"></asp:ListBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="lsfooterreport">
                                                    <asp:Panel ID="pnl4" CssClass="textbox" runat="server" Visible="false" Width="100%" Height="60px">
                                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td>
                                                                    <asp:Table ID="tblFilterOthers" CssClass="label" runat="server" Width="80%">
                                                                        <asp:TableRow>
                                                                            <asp:TableCell Width="20%"></asp:TableCell>
                                                                            <asp:TableCell Width="14%"></asp:TableCell>
                                                                            <asp:TableCell Width="20%"></asp:TableCell>
                                                                        </asp:TableRow>
                                                                    </asp:Table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- end left side report params -->
                                    </td>
                                    <td class="rsspacerreport">
                                    </td>
                                    <td class="rsreport">
                                        <!-- begin right side -->
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contenttable">
                                            <tr>
                                                <td>
                                                    <!-- begin right side top -->
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="contenttable">
                                                        <tr>
                                                            <td class="rtheaderreport">
                                                                <asp:Label ID="lblAdditionalInfo" runat="server" CssClass="label" Font-Bold="True">Step 3: Additional information to print on the report:</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="rtcontentreport">
                                                                <asp:CheckBox ID="chkRptFilters" runat="server" Text="Selected Filter criteria" CssClass="label">
                                                                </asp:CheckBox><br />
                                                                <asp:CheckBox ID="chkRptSort" runat="server" Text="Selected Sort Order" CssClass="label">
                                                                </asp:CheckBox><br />
                                                                <asp:CheckBox ID="chkRptDescrip" runat="server" Text="Report Description" CssClass="label">
                                                                </asp:CheckBox><br />
                                                                <asp:CheckBox ID="chkRptInstructions" runat="server" Text="Report Instructions" CssClass="label">
                                                                </asp:CheckBox><br />
                                                                <asp:CheckBox ID="chkRptNotes" runat="server" Text="Report Notes" CssClass="label">
                                                                </asp:CheckBox><br />
                                                                <asp:CheckBox ID="chkRptStudentGroup" runat="server" Text="Student Group" CssClass="label">
                                                                </asp:CheckBox><br />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <!-- end right side top -->
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <!-- begin right side bottom -->
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td colspan="2" align="left" valign="top">
                                                                <asp:Label ID="lblStep4" runat="server" CssClass="label" Font-Bold="True">Step 4: </asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="left">
                                                                <asp:Button ID="btnSearch" Text="View Report" runat="server" Width="150px"></asp:Button><br />
                                                                <asp:Button ID="btnFriendlyPrint" Visible="False" Text=" Printer Friendly Report"
                                                                    Width="100px" runat="server"></asp:Button>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="spacertables">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="rbdropdownreport">
                                                                <asp:DropDownList ID="ddlExportTo" runat="server" CssClass="dropdownlist">
                                                                    <asp:ListItem Value="pdf">Adobe Acrobat</asp:ListItem>
                                                                    <asp:ListItem Value="xls">Microsoft Excel</asp:ListItem>
                                                                    <asp:ListItem Value="doc">Microsoft Word</asp:ListItem>
                                                                    <asp:ListItem Value="xls1">Microsoft Excel (Data) </asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="rbbutton">
                                                                <asp:Button ID="btnExport" Text="Export" runat="server" Width="80px"></asp:Button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <!-- end right side bottom -->
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- end right side -->
                                    </td>
                                </tr>
                            </table>
                            <!-- start validation panel-->
                        </div>
                        <asp:LinkButton ID="lbtPrompt" runat="server" Style="border-style: none" Visible="false"></asp:LinkButton>
                        
                        <asp:HiddenField ID="txtRESID" runat="server" />
                        <asp:HiddenField ID="txtPrefId" runat="server" />
                        <asp:HiddenField ID="txtAllCampGrps" runat="server" />
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>
