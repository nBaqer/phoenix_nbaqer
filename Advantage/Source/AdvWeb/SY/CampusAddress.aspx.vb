Imports System.Diagnostics
Imports FAME.AdvantageV1.DataAccess
Imports FAME.Common
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports Advantage.Business.Objects
Imports System.IO
Imports System.Collections
Imports Advantage.Business.Logic.Layer
Imports NHibernate.Mapping
Imports FAME.Advantage.DataAccess.LINQ

Partial Class CampusAddress
    Inherits BasePage
    Protected WithEvents btnReset As Button
    Protected WithEvents chkStatus As CheckBox
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected strDefaultCountry As String
    Protected strPortalSetting As String
    Protected strDefaultIsRemoteServer As String

    Protected mContext As HttpContext

    Protected dtResFlds As DataTable

    Private dataUserProvider As UserStateDataProvider

    Protected MyAdvAppSettings As AdvAppSettings


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        dataUserProvider = New UserStateDataProvider(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        txtCampDescrip.Attributes.Add("onChange", "DoTitleCase('txtCampDescrip');")
        txtAddress1.Attributes.Add("onChange", "DoTitleCase('txtAddress1');")
        txtAddress2.Attributes.Add("onChange", "DoTitleCase('txtAddress2');")
        txtCity.Attributes.Add("onChange", "DoTitleCase('txtCity');")
        ''Added by saraswathi lakshmanan to fix issue 13365
        ''Added on June 16 2009
        ''Invoice address added
        txtInvAddress1.Attributes.Add("onChange", "DoTitleCase('txtInvAddress1');")
        txtInvAddress2.Attributes.Add("onChange", "DoTitleCase('txtInvAddress2');")
        txtInvCity.Attributes.Add("onChange", "DoTitleCase('txtInvCity');")

        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim sw As New StringWriter
        Dim sdfControls As New SDFComponent

        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'campusId = AdvantageSession.UserState.CampusId.ToString
        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        mContext = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString
        strDefaultIsRemoteServer = "no"

        ''added by saraswathi Lakshmanan to hide time clock informtion when the attendance type is byClass
        ''Modified on may 28 2009
        '  If SingletonAppSettings.AppSettings("TrackSapAttendance").ToString.ToLower = "byday" Then
        ShoworHideControlsbasedonByDayorByClass(True)
        'Else
        'ShoworHideControlsbasedonByDayorByClass(False)
        'End If


        Try
            If Not Page.IsPostBack Then
                BuildParentCampus()
                ToggleCampusBranch()

                mContext = HttpContext.Current
                txtResourceId.Text = CInt(mContext.Items("ResourceId"))
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                'Set the text box to a new guid
                txtCampusId.Text = Guid.NewGuid.ToString

                ds = objListGen.SummaryListGenerator(userId, campusId)

                PopulateDataList(ds)

                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()

                'If SingletonAppSettings.AppSettings("CorporateName") = "" Then
                '    'GetInputMaskValue(linkbtn)
                '    chkIsCorporate.Visible = True
                'Else
                ''Modified by saraswathi on june 15 2009
                ''To fix issue 13365
                chkIsCorporate.Visible = False
                ' End If

                '   txtIsRemoteServer.Text = strDefaultIsRemoteServer
                lblRemoteServerUsrNm.Visible = False
                lblRemoteServerPwd.Visible = False
                txtRemoteServerUsrNm.Visible = False
                txtRemoteServerPwd.Visible = False

                ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
                lblRemoteServerUsrNmFL.Visible = False
                lblRemoteServerPwdFL.Visible = False
                txtRemoteServerUsrNmFL.Visible = False
                txtRemoteServerPwdFL.Visible = False
                '''''''''''''''''''''''''''''''
                Try
                    ddlCountryId.SelectedValue = strDefaultCountry
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlCountryId.SelectedIndex = 0
                End Try
                ''Added

                Try
                    ddlInvCountryID.SelectedValue = strDefaultCountry
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlInvCountryID.SelectedIndex = 0
                End Try

                'txtFSEOGMatchType.Items.Insert(0, New ListItem(String.Empty, String.Empty))
                'txtFSEOGMatchType.Items.Add(New ListItem("Scholarship", "S"))
                'txtFSEOGMatchType.Items.Add(New ListItem("Cash Map", "C"))
                'txtFSEOGMatchType.SelectedIndex = 0
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            End If

            'GetInputMaskValue()
            'Check If any UDF exists for this resource
            Dim intSdfExists As Integer = sdfControls.GetSDFExists(ResourceId, ModuleId)
            If intSdfExists >= 1 Then
                pnlUDFHeader.Visible = True
            Else
                pnlUDFHeader.Visible = False
            End If

            If Trim(txtCampCode.Text) <> "" Then
                sdfControls.GenerateControlsEditSingleCell(pnlSDF, ResourceId, txtCampusId.Text, ModuleId)
            Else
                sdfControls.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)
            End If

            Dim olg As New DataListGenerator
            Dim dset As DataSet

            If Request.QueryString("redirect") = "save" Then
                dset = olg.SummaryListGenerator(userId, campusId)
                ReLoadPageContent(Request.QueryString("dispcamp"), dset)
                ViewState("MODE") = "EDIT"
                CommonWebUtilities.RestoreItemValues(dlstCmpAddr, txtCampusId.Text)
            End If

            'Show the 1098T section if user is support
            If Session("UserName").ToString.ToLower = "support" Then
                tbl1098T.Visible = True
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub



    Private Sub BuildParentCampus(Optional excludeList As List(Of Guid) = Nothing)
        Dim connectionString = MyAdvAppSettings.AppSettings("connectionString")
        Dim campusDA = New CampusDA(connectionString)

        Dim campusIds = campusDA.GetParentCampusesByUserId(Guid.Parse(userId)).ToList()
        If (excludeList IsNot Nothing AndAlso excludeList.Count > 0) Then
            campusIds = campusIds.Where(Function(x) Not excludeList.Contains(x.CampusId)).ToList()
        End If

        With ddlParentCampus
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            .DataSource = campusIds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))

            .SelectedIndex = 0

        End With

    End Sub

    'Private Function GetInputMaskValue() As String
    '    'Dim facInputMasks As New InputMasksFacade
    '    ''   Dim correctFormat As Boolean
    '    'Dim strMask As String
    '    'Dim zipMask As String
    '    ''  Dim errorMessage As String
    '    'Dim strPhoneReq As String
    '    ''    Dim strZipReq As String
    '    'Dim strFaxReq As String
    '    'Dim objCommon As New CommonUtilities
    '    ''  Dim ssnMask As String


    '    ''Get The Input Mask for Phone/Fax and Zip
    '    'strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

    '    ''Replace The Mask Character from # to 9 as Masked Edit TextBox 
    '    ''accepts only certain characters as mask characters
    '    'txtPhone1.Mask = Replace(strMask, "#", "9")
    '    'txtPhone2.Mask = Replace(strMask, "#", "9")
    '    'txtPhone3.Mask = Replace(strMask, "#", "9")
    '    'txtFax.Mask = Replace(strMask, "#", "9")
    '    ' ''Added by saraswathi 
    '    'txtInvPhone1.Mask = Replace(strMask, "#", "9")
    '    ''txtInvPhone2.Mask = Replace(strMask, "#", "9")
    '    ''txtInvPhone3.Mask = Replace(strMask, "#", "9")
    '    'txtInvFax.Mask = Replace(strMask, "#", "9")

    '    ''Get The Format Of the input masks and display it next to caption
    '    ''labels
    '    'lblPhone1.ToolTip = strMask
    '    'lblPhone2.ToolTip = strMask
    '    'lblPhone3.ToolTip = strMask
    '    'lblFax.ToolTip = strMask
    '    ' ''Added by saraswathi
    '    'lblInvPhone1.ToolTip = strMask
    '    ''lblInvPhone2.ToolTip = strMask
    '    ''lblInvPhone3.ToolTip = strMask
    '    'lblInvFax.ToolTip = strMask

    '    ''Get The RequiredField Value
    '    ''  Dim strSSNReq As String

    '    'strPhoneReq = objCommon.SetRequiredColorMask("Phone")
    '    'strFaxReq = objCommon.SetRequiredColorMask("Fax")

    '    ''If The Field Is Required Field Then Color The Masked
    '    ''Edit Control

    '    'If strPhoneReq = "Yes" Then
    '    '    txtPhone1.BackColor = Color.FromName("#ffff99")
    '    '    txtPhone2.BackColor = Color.FromName("#ffff99")
    '    '    txtPhone3.BackColor = Color.FromName("#ffff99")
    '    'End If
    '    'If strFaxReq = "Yes" Then
    '    '    txtFax.BackColor = Color.FromName("#ffff99")
    '    'End If
    '    ''zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
    '    ''txtZip.Mask = Replace(zipMask, "#", "9")
    '    ''lblZip.ToolTip = zipMask
    '    '' ''Added
    '    ''txtInvZip.Mask = Replace(zipMask, "#", "9")
    '    ''lblInvZip.ToolTip = zipMask
    'End Function

    Private Function ValidateFieldsWithInputMasks() As String

        'DE8990
        Dim errorMessage As String = ""

        If Not txtZip.Text = "" Then
            If txtZip.Text.Length < 5 Then
                errorMessage &= "Zip code must be 5 digits in length" & vbLf
            End If
        End If
        If Not txtPhone1.Text = "" Then
            If txtPhone1.Text.Length < 10 Then
                errorMessage &= "Phone numbers must include the area code and phone number and be 10 digits in length" & vbLf
            End If
        End If
        If Not txtPhone2.Text = "" Then
            If txtPhone2.Text.Length < 10 Then
                errorMessage &= "Phone numbers must include the area code and phone number and be 10 digits in length" & vbLf
            End If
        End If
        If Not txtPhone3.Text = "" Then
            If txtPhone3.Text.Length < 10 Then
                errorMessage &= "Phone numbers must include the area code and phone number and be 10 digits in length" & vbLf
            End If
        End If
        If Not txtFax.Text = "" Then
            If txtFax.Text.Length < 10 Then
                errorMessage &= "Phone numbers must include the area code and phone number and be 10 digits in length" & vbLf
            End If
        End If
        If Not txtInvZip.Text = "" Then
            If txtInvZip.Text.Length < 5 Then
                errorMessage &= "Invoice Address Zip code must be 5 digits in length" & vbLf
            End If
        End If
        If Not txtInvPhone1.Text = "" Then
            If txtInvPhone1.Text.Length < 10 Then
                errorMessage &= "Invoice Address Phone number must include the area code and phone number and be 10 digits in length" & vbLf
            End If
        End If
        If Not txtInvFax.Text = "" Then
            If txtInvFax.Text.Length < 10 Then
                errorMessage &= "Invoice Address Fax number must include the area code and phone number and be 10 digits in length" & vbLf
            End If
        End If

        Return errorMessage

        'Dim facInputMasks As New InputMasksFacade
        'Dim correctFormat As Boolean
        'Dim strMask As String
        'Dim zipMask As String
        'Dim errorMessage As String = ""
        ''   Dim ssnMask As String

        'strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        'zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)



        ''Validate the phone field format. If the field is empty we should not apply the mask
        ''agaist it.
        'If txtPhone1.Text <> "" Then
        '    correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtPhone1.Text)
        '    If correctFormat = False Then
        '        errorMessage &= "Incorrect format for phone1 field." & vbCr
        '    End If
        'End If

        'If txtPhone2.Text <> "" Then
        '    correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtPhone2.Text)
        '    If correctFormat = False Then
        '        errorMessage &= "Incorrect format for phone2 field." & vbCr
        '    End If
        'End If

        'If txtPhone3.Text <> "" Then
        '    correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtPhone3.Text)
        '    If correctFormat = False Then
        '        errorMessage &= "Incorrect format for phone3 field." & vbCr
        '    End If
        'End If

        'If txtFax.Text <> "" Then
        '    correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtFax.Text)
        '    If correctFormat = False Then
        '        errorMessage &= "Incorrect format for fax field." & vbCr
        '    End If
        'End If

        ''Validate the zip field format. If the field is empty we should not apply the mask
        ''against it.
        'If txtZip.Text <> "" Then
        '    correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtZip.Text)
        '    If correctFormat = False Then
        '        errorMessage &= "Incorrect format for zip field."
        '    End If
        'End If
        ' ''Added

        'If txtInvZip.Text <> "" Then
        '    correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtInvZip.Text)
        '    If correctFormat = False Then
        '        errorMessage &= "Incorrect format for Invoice Address zip field."
        '    End If
        'End If
        'If txtInvPhone1.Text <> "" Then
        '    correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtInvPhone1.Text)
        '    If correctFormat = False Then
        '        errorMessage &= "Incorrect format for Invoice Address phone1 field." & vbCr
        '    End If
        'End If

        ''If txtInvPhone2.Text <> "" Then
        ''    correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtInvPhone2.Text)
        ''    If correctFormat = False Then
        ''        errorMessage &= "Incorrect format for Invoice Address phone2 field." & vbCr
        ''    End If
        ''End If

        ''If txtInvPhone3.Text <> "" Then
        ''    correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtInvPhone3.Text)
        ''    If correctFormat = False Then
        ''        errorMessage &= "Incorrect format for Invoice Address phone3 field." & vbCr
        ''    End If
        ''End If

        'If txtInvFax.Text <> "" Then
        '    correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtInvFax.Text)
        '    If correctFormat = False Then
        '        errorMessage &= "Incorrect format for Invoice Address fax field." & vbCr
        '    End If
        'End If

        'Return errorMessage
    End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsave.Click
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim sw As New StringWriter
        Dim msg As String
        Dim errorMessage As String
        Dim facade As New CampusGroupsFacade
        Dim description As String
        Dim code As String
        Dim sStatusId As String

        Try
            ''Added by saraswathi on may 28 2009
            ''The text in password field is set before saving. since, after saving, it is pointing to the previous value set

            txtRemoteServerPwd.Attributes.Add("Value", txtRemoteServerPwd.Text)
            ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
            txtRemoteServerPwdFL.Attributes.Add("Value", txtRemoteServerPwdFL.Text)
            ''''''''''''''
            code = txtCampCode.Text
            description = txtCampDescrip.Text & " CG"
            sStatusId = ddlStatusId.SelectedItem.Value.ToString

            ''Added by Saraswathi lakshmanan on june 16 2009
            If chkUseCampusAddress.Checked = True Then
                txtInvAddress1.Text = txtAddress1.Text
                txtInvAddress2.Text = txtAddress2.Text
                txtInvCity.Text = txtCity.Text
                ddlInvStateID.SelectedIndex = ddlStateId.SelectedIndex
                txtInvZip.Text = txtZip.Text
                ddlInvCountryID.SelectedIndex = ddlCountryId.SelectedIndex
                txtInvPhone1.Text = txtPhone1.Text
                'txtInvPhone2.Text = txtPhone2.Text
                'txtInvPhone3.Text = txtPhone3.Text
                txtInvFax.Text = txtFax.Text
            End If

            errorMessage = ValidateFieldsWithInputMasks()
            If errorMessage <> "" Then
                DisplayErrorMessage(errorMessage)
                Exit Sub
            End If

            If Not String.IsNullOrEmpty(txtOPEID.Text) Then
                If txtOPEID.Text.Length < 6 Or txtOPEID.Text.Length > 8 Then
                    DisplayErrorMessage("OPEID must be between 6 and 8 characters.")
                    Exit Sub
                ElseIf Not Regex.IsMatch(txtOPEID.Text, "^\d+$") Then
                    DisplayErrorMessage("OPEID must be numeric.")
                    Exit Sub
                End If
            End If

            'The Token must be in the format of a GUID
            If Not String.IsNullOrEmpty(txtToken1098TService.Text) Then
                Try
                    Dim tokenGUID As Guid
                    tokenGUID = New Guid(txtToken1098TService.Text)
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    Dim errMsg As String
                    errMsg = "1098T Token must be in the format of a GUID." + vbCr + "Example: 51FEDCDC-9066-42E2-B098-A07E9BB77335"
                    DisplayErrorMessage(errMsg)
                    Exit Sub
                End Try
            End If

            'Kiss Code must only contain alphanumeric characters or a hyphen
            Dim isValid As Boolean = True
            Dim c As Char

            For i As Integer = 0 To txtSchoolCodeKissSchoolId.Text.Count - 1
                c = txtSchoolCodeKissSchoolId.Text.Chars(i)
                If Not (Char.IsLetterOrDigit(c) Or c.ToString.Contains("_")) Then
                    DisplayErrorMessage("The only special character allowed for 1098T Kiss Code is the underscore _ character")
                    Exit Sub
                End If
            Next

            'If a Token is entered in the 1098T section then KISS School code is required
            If Not String.IsNullOrEmpty(txtToken1098TService.Text) Then
                If String.IsNullOrEmpty(txtSchoolCodeKissSchoolId.Text) Then
                    DisplayErrorMessage("You must enter a Kiss School Id if you enter a Token under the 1098T section")
                    Exit Sub
                End If
            End If


            Dim isBranch = ddlCampusIsBranch.SelectedValue.Equals("1")
            If isBranch AndAlso String.IsNullOrEmpty(ddlParentCampus.SelectedValue) Then
                DisplayErrorMessage("You must select a parent when the campus is marked as a branch")
                Exit Sub
            End If

            Dim parentCampusId As Guid? = Nothing

            If isBranch Then
                parentCampusId = Guid.Parse(ddlParentCampus.SelectedValue)
            End If

            If ViewState("MODE") = "NEW" Then
                objCommon.PagePK = txtCampusId.Text
                msg = objCommon.DoInsertCampus(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), False, False, False)

                If msg <> "" Then
                    DisplayErrorMessage(msg)
                Else
                    'Insert the newly added campus into the 'All' campus group
                    facade.InsertCampusIntoAllCampusGroup(txtCampusId.Text, code, sStatusId, description, Session("UserName"))
                End If
                'Set the Primary Key value, so it can be used later by the DoUpdate method
                'objCommon.PagePK = txtCategoryId.Text
                txtRowIds.Text = objCommon.PagePK
                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstCmpAddr.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"
            ElseIf ViewState("MODE") = "EDIT" Then
                objCommon.DoUpdateCampus(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), False, False, False)

                If tblPortalCustomer.Visible = True Then

                    Dim facadeObj As New StudentPortalFacade

                    facadeObj.UpdatePortalSettings(txtCampusId.Text,
                                                   chkSourceCategoryAll.Checked, ddlSourceCategory.SelectedValue,
                                                   chkSourceTypeAll.Checked, ddlSourceType.SelectedValue,
                                                   chkAdmRepAll.Checked, ddlAdmRep.SelectedValue,
                                                   chkLeadStatusAll.Checked, ddlLeadStatus.SelectedValue,
                                                   chkPhoneType1All.Checked, ddlPhoneType1.SelectedValue,
                                                   chkPhoneType2All.Checked, ddlPhoneType2.SelectedValue,
                                                   chkAddTypeAll.Checked, ddlAddType.SelectedValue,
                                                   chkGenderAll.Checked, ddlGender.SelectedValue,
                                                   chkPortalCountryAll.Checked, ddlPortalCountry.SelectedValue,
                                                   chkPortalContactEmailAll.Checked, txtPortalContactEmail.Text,
                                                   chkEmailSubjectAll.Checked, txtEmailSubject.Text,
                                                   chkEmailBodyAll.Checked, txtEmailBody.Text)
                End If

                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstCmpAddr.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Response.Write(objCommon.strText)
            End If

            'update campus branch information
            facade.UpdateCampusBranch(txtCampusId.Text, isBranch, parentCampusId)

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked
            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            'Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                SDFControl.DeleteSDFValue(txtCampusId.Text)
                SDFID = SDFControl.GetAllLabels(pnlSDF)
                SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtCampusId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try
            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Advice to client that smust reload the campus lsit from server.
            AdvantageSession.RefreshListCampus = True
            'Try
            '    Master.BuildCampusList()

            'Catch ex As Exception
            '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)


            'End Try

            CommonWebUtilities.RestoreItemValues(dlstCmpAddr, txtCampusId.Text)

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub


    'Private Sub GenerateResponse(ByRef userState As User)
    '    Try
    '        Select Case userState.IsLoginSuccessful
    '            Case Is = False
    '                'lblMessage.Visible = True
    '                'lblMessage.Text = userState.ReturnMessage
    '            Case Else
    '                AdvantageSession.UserState = userState
    '                AdvantageSession.UserName = userState.UserName
    '                Response.Redirect(AdvantageSession.UserState.RedirectURL, False)
    '        End Select
    '    Catch ex As Exception
    '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Throw New Exception(ex.Message)
    '    End Try
    'End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnnew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New StringReader(CStr(ViewState("ds")))
        Try
            ClearRHS()
            txtZip.Text = ""
            txtInvZip.Text = ""
            txtCmsId.Text = ""
            txtPhone1.Text = ""
            txtPhone2.Text = ""
            txtPhone3.Text = ""
            txtFax.Text = ""
            ''Added by saraswathi
            txtInvPhone1.Text = ""
            'txtInvPhone2.Text = ""
            'txtInvPhone3.Text = ""
            txtInvFax.Text = ""

            txtRemoteServerPwd.Text = ""
            txtRemoteServerPwd.Attributes.Add("Value", "")
            ds.ReadXml(sr)
            PopulateDataList(ds)
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtCampusId.Text = Guid.NewGuid.ToString
            ' txtIsRemoteServer.Text = strDefaultIsRemoteServer
            Try
                ddlCountryId.SelectedValue = strDefaultCountry
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                ddlCountryId.SelectedIndex = 0
            End Try
            ''Added
            Try
                ddlInvCountryID.SelectedValue = strDefaultCountry
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                ddlInvCountryID.SelectedIndex = 0
            End Try

            tblPortalCustomer.Visible = False

            lblRemoteServerUsrNm.Visible = False
            lblRemoteServerPwd.Visible = False
            txtRemoteServerUsrNm.Visible = False
            txtRemoteServerPwd.Visible = False

            ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
            lblRemoteServerUsrNmFL.Visible = False
            lblRemoteServerPwdFL.Visible = False
            txtRemoteServerUsrNmFL.Visible = False
            txtRemoteServerPwdFL.Visible = False
            ''''''''''''''''
            'lblSourceFolderLoc.Visible = True
            'lblTargetFolderLoc.Visible = True
            'txtSourceFolderLoc.Visible = True
            'txtTargetFolderLoc.Visible = True

            '
            ToggleCampusBranch()
            DefaultParentCampusDropDown()

            txtInvAddress1.Enabled = True
            txtInvAddress2.Enabled = True
            txtInvCity.Enabled = True
            ddlInvStateID.Enabled = True
            txtInvZip.Enabled = True
            ddlInvCountryID.Enabled = True
            txtInvPhone1.Enabled = True
            'txtInvPhone2.Enabled = True
            'txtInvPhone3.Enabled = True
            txtInvFax.Enabled = True
            ddlStatusId.Enabled = True
            Dim sdfControls As New SDFComponent
            sdfControls.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)

            'CommonWebUtilities.RestoreItemValues(dlstCmpAddr, Guid.Empty.ToString)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btndelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim sw As New StringWriter
        Dim msg As String
        Dim facade As New CampusGroupsFacade

        'We do not want the user to delete the campus that he/she is logged in to.
        'We will compare the cmpid in the query string with the guid stored in txtCampusId.
        'If they are the same then we will inform the user that he/she cannot delete the campus that
        'he/she is logged in to.
        If Master.CurrentCampusId = txtCampusId.Text Then
            DisplayErrorMessage("You cannot delete the campus that you are logged into.")
        Else
            Try
                msg = facade.DeleteCampusFrmCampGrps(txtCampusId.Text)
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                    Exit Sub
                Else
                    msg = objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                    If msg <> "" Then
                        DisplayErrorMessage(msg)
                        Exit Sub
                    End If
                    ClearRHS()
                    ds = objListGen.SummaryListGenerator(userId, campusId)
                    dlstCmpAddr.SelectedIndex = -1
                    PopulateDataList(ds)
                    ds.WriteXml(sw)
                    ViewState("ds") = sw.ToString()
                    objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                    ViewState("MODE") = "NEW"
                    'Set the text box to a new guid
                    txtCampusId.Text = Guid.NewGuid.ToString
                    'Header1.EnableHistoryButton(False)

                    '
                    ToggleCampusBranch()
                    DefaultParentCampusDropDown()

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                    'SDF Code Starts Here
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim sdfControl As New SDFComponent
                    sdfControl.DeleteSDFValue(txtCampusId.Text)
                    sdfControl.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    'SDF Code Ends Here
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                End If
                ' txtIsRemoteServer.Text = strDefaultIsRemoteServer
                Try
                    ddlCountryId.SelectedValue = strDefaultCountry
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlCountryId.SelectedIndex = 0
                End Try
                Try
                    ddlInvCountryID.SelectedValue = strDefaultCountry
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlInvCountryID.SelectedIndex = 0
                End Try

                Try
                    'Advice to client that smust reload the campus lsit from server.
                    AdvantageSession.RefreshListCampus = True
                    'Master.BuildCampusList()

                    Response.Redirect("../SY/CampusAddress.aspx?resid=" & ResourceId & "&mod=AR&cmpid=" + campusId.ToString + "&desc=Campus" + "&redirect=delete" + "&dispcamp=" + txtCampusId.Text, +False)
                    Session("flagBuildMenu") = False
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)


                End Try
                CommonWebUtilities.RestoreItemValues(dlstCmpAddr, Guid.Empty.ToString)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End If

    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlrhs.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub dlstCmpAddr_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs) Handles dlstCmpAddr.ItemCommand

        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim sr As New StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities

        Dim stuPortal As New StudentPortalFacade

        txtRowIds.Text = e.CommandArgument.ToString
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.SetHiddenControlForAudit()

        'Try
        strGUID = dlstCmpAddr.DataKeys(e.Item.ItemIndex).ToString()
        objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGUID)
        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
        ViewState("MODE") = "EDIT"
        selIndex = e.Item.ItemIndex
        dlstCmpAddr.SelectedIndex = selIndex
        ds.ReadXml(sr)
        PopulateDataList(ds)
        'Header1.EnableHistoryButton(True)
        'Header1.ObjectID = strGUID
        ''Added by saraswathi to show the remote password as encrypted
        Dim pwd As String = ""
        If strGUID <> "" Then
            Dim UserSecFacade As New UserSecurityFacade
            pwd = UserSecFacade.GetPasswordforCampus(strGUID)
        End If
        txtRemoteServerPwd.Attributes.Add("Value", pwd)

        ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
        Dim PwdFL As String = ""
        If strGUID <> "" Then
            Dim UserSecFacade As New UserSecurityFacade
            PwdFL = UserSecFacade.GetPasswordforCampusFL(strGUID)
        End If
        txtRemoteServerPwdFL.Attributes.Add("Value", PwdFL)
        '''''''''''''''''''''''''''''''
        Dim connectionString = MyAdvAppSettings.AppSettings("connectionString")
        Dim campusDA = New CampusDA(connectionString)

        Dim campusData = campusDA.GetCampusById(Guid.Parse(strGUID))

        BuildParentCampus(New List(Of Guid) From {Guid.Parse(strGUID)})


        'set branch and parent data
        If (Not campusData Is Nothing) Then
            ddlCampusIsBranch.SelectedValue = If(campusData.IsBranch, "1", "0")

            If (ddlParentCampus.Items.FindByValue(campusData.ParentCampusId.ToString) IsNot Nothing) Then '
                ddlParentCampus.SelectedValue = campusData.ParentCampusId.ToString
            Else
                DefaultParentCampusDropDown()
            End If

        End If

        ToggleCampusBranch()


        If chkIsRemoteServer.Checked = True Then
            lblRemoteServerUsrNm.Visible = True
            lblRemoteServerPwd.Visible = True
            txtRemoteServerUsrNm.Visible = True
            txtRemoteServerPwd.Visible = True
            'lblSourceFolderLoc.Visible = True
            'lblTargetFolderLoc.Visible = True
            'txtSourceFolderLoc.Visible = True
            'txtTargetFolderLoc.Visible = True
        Else
            lblRemoteServerUsrNm.Visible = False
            lblRemoteServerPwd.Visible = False
            txtRemoteServerUsrNm.Visible = False
            txtRemoteServerPwd.Visible = False
            'lblSourceFolderLoc.Visible = False
            'lblTargetFolderLoc.Visible = False
            'txtSourceFolderLoc.Visible = False
            'txtTargetFolderLoc.Visible = False
        End If

        ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
        If chkIsRemoteServerFL.Checked = True Then
            lblRemoteServerUsrNmFL.Visible = True
            lblRemoteServerPwdFL.Visible = True
            txtRemoteServerUsrNmFL.Visible = True
            txtRemoteServerPwdFL.Visible = True
        Else
            lblRemoteServerUsrNmFL.Visible = False
            lblRemoteServerPwdFL.Visible = False
            txtRemoteServerUsrNmFL.Visible = False
            txtRemoteServerPwdFL.Visible = False
        End If
        '''''''''''''''''''''

        'If chkIsRemoteServerIL.Checked = True Then
        '    lblRemoteServerUserNameIL.Visible = True
        '    lblRemoteServerPasswordIL.Visible = True
        '    'txtRemoteServerUserNameIL.Visible = True
        '    'txtRemoteServerPasswordIL.Visible = True
        'Else
        '    lblRemoteServerUserNameIL.Visible = False
        '    lblRemoteServerPasswordIL.Visible = False
        '    'txtRemoteServerUserNameIL.Visible = False
        '    'txtRemoteServerPasswordIL.Visible = False

        'End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEditSingleCell(pnlSDF, ResourceId, txtCampusId.Text, ModuleId)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        CommonWebUtilities.RestoreItemValues(dlstCmpAddr, strGUID)

        If stuPortal.GetPortalSetting(txtCampusId.Text) = "True" Then
            txtPortalContactEmail.Text = ""
            txtEmailSubject.Text = ""
            txtEmailBody.Text = ""
            BuildSrcCategoryDDL()
            BuildUsersDDL()
            BuildDDLs()
            GetPortalSettingValueByCampus(txtCampusId.Text)
            tblPortalCustomer.Visible = True
        Else
            tblPortalCustomer.Visible = False
        End If

        ''Added by saraswathi lakshmanan
        ''On June 18 2009
        If chkUseCampusAddress.Checked = True Then
            txtInvAddress1.Enabled = False
            txtInvAddress2.Enabled = False
            txtInvCity.Enabled = False
            ddlInvStateID.Enabled = False
            txtInvZip.Enabled = False
            ddlInvCountryID.Enabled = False
            txtInvPhone1.Enabled = False
            'txtInvPhone2.Enabled = False
            'txtInvPhone3.Enabled = False
            txtInvFax.Enabled = False
        Else
            txtInvAddress1.Enabled = True
            txtInvAddress2.Enabled = True
            txtInvCity.Enabled = True
            ddlInvStateID.Enabled = True
            txtInvZip.Enabled = True
            ddlInvCountryID.Enabled = True
            txtInvPhone1.Enabled = True
            'txtInvPhone2.Enabled = True
            'txtInvPhone3.Enabled = True
            txtInvFax.Enabled = True

        End If

        Dim campusId As Guid = CampusObjects.GetQueryParameterValueGuidCampusId(Request)
        If campusId.ToString = e.CommandArgument.ToString Then
            ddlStatusId.Enabled = False
        Else
            ddlStatusId.Enabled = True
        End If
        CommonWebUtilities.RestoreItemValues(dlstCmpAddr, strGUID)

        ' Dim rcb As Telerik.Web.UI.RadComboBox = Master.FindControl("CampusSelector")

        'If rcb.SelectedValue.ToString = e.CommandArgument.ToString Then
        '    ddlStatusId.Enabled = False
        'Else
        '    ddlStatusId.Enabled = True
        'End If
        'CommonWebUtilities.RestoreItemValues(dlstCmpAddr, strGUID)
    End Sub

    Private Sub chkStatus_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkStatus.CheckedChanged
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        'Dim ds2 As New DataSet
        'Dim sw As New StringWriter
        Dim objCommon As New CommonUtilities
        'Dim dv2 As New DataView
        ' Dim sStatusId As String
        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was refactored by AVP on 6/21/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radstatus.SelectedItem.Text.ToLower = "active") Or (radstatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radstatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        Dim dv As New DataView(ds.Tables(0), sDataFilter, "CampDescrip", DataViewRowState.CurrentRows)

        With dlstCmpAddr
            .DataSource = dv
            .DataBind()
        End With

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radstatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim objCommon As New CommonUtilities

        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtCampusId.Text = Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
            Dim SDFControls As New SDFComponent
            SDFControls.GenerateControlsNewSingleCell(pnlSDF, ResourceId, ModuleId)


        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        ''added by saraswathi Lakshmanan to hide time clock informtion when the attendance type is byClass
        ''Modified on may 28 2009
        ' If SingletonAppSettings.AppSettings("TrackSapAttendance").ToString.ToLower = "byday" Then
        ShoworHideControlsbasedonByDayorByClass(True)
        'Else
        '    ShoworHideControlsbasedonByDayorByClass(False)
        'End If
    End Sub

    Protected Sub chkIsRemoteServer_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkIsRemoteServer.CheckedChanged
        If chkIsRemoteServer.Checked = True Then
            lblRemoteServerUsrNm.Visible = True
            lblRemoteServerPwd.Visible = True
            txtRemoteServerUsrNm.Visible = True
            txtRemoteServerPwd.Visible = True
            'lblSourceFolderLoc.Visible = True
            'lblTargetFolderLoc.Visible = True
            'txtSourceFolderLoc.Visible = True
            'txtTargetFolderLoc.Visible = True

        Else
            lblRemoteServerUsrNm.Visible = False
            lblRemoteServerPwd.Visible = False
            txtRemoteServerUsrNm.Visible = False
            txtRemoteServerPwd.Visible = False
            'lblSourceFolderLoc.Visible = False
            'lblTargetFolderLoc.Visible = False
            'txtSourceFolderLoc.Visible = False
            'txtTargetFolderLoc.Visible = False
        End If
    End Sub

    Private Sub ShoworHideControlsbasedonByDayorByClass(ByVal show As Boolean)
        If show = True Then
            lbltcinfo.Visible = True
            'lblRemoteServerUsrNm.Visible = True
            'lblRemoteServerPwd.Visible = True
            'txtRemoteServerUsrNm.Visible = True
            'txtRemoteServerPwd.Visible = True
            lblIsRemoteServer1.Visible = True
            chkIsRemoteServer.Visible = True
            lblTCSourcePath.Visible = True
            lblTCTargetPath.Visible = True
            txtTCSourcePath.Visible = True
            txtTCTargetPath.Visible = True
            tcinfo.Visible = True

        Else
            lbltcinfo.Visible = False
            lblRemoteServerUsrNm.Visible = False
            lblRemoteServerPwd.Visible = False
            txtRemoteServerUsrNm.Visible = False
            txtRemoteServerPwd.Visible = False
            lblIsRemoteServer1.Visible = False
            chkIsRemoteServer.Visible = False
            lblTCSourcePath.Visible = False
            lblTCTargetPath.Visible = False
            txtTCSourcePath.Visible = False
            txtTCTargetPath.Visible = False
            tcinfo.Visible = False
        End If
    End Sub

    Protected Sub chkUseCampusAddress_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkUseCampusAddress.CheckedChanged
        If chkUseCampusAddress.Checked = True Then
            txtInvAddress1.Text = txtAddress1.Text
            txtInvAddress2.Text = txtAddress2.Text
            txtInvCity.Text = txtCity.Text
            ddlInvStateID.SelectedIndex = ddlStateId.SelectedIndex
            txtInvZip.Text = txtZip.Text
            ddlInvCountryID.SelectedIndex = ddlCountryId.SelectedIndex
            txtInvPhone1.Text = txtPhone1.Text
            'txtInvPhone2.Text = txtPhone2.Text
            'txtInvPhone3.Text = txtPhone3.Text
            txtInvFax.Text = txtFax.Text

            txtInvAddress1.Enabled = False
            txtInvAddress2.Enabled = False
            txtInvCity.Enabled = False
            ddlInvStateID.Enabled = False
            txtInvZip.Enabled = False
            ddlInvCountryID.Enabled = False
            txtInvPhone1.Enabled = False
            'txtInvPhone2.Enabled = False
            'txtInvPhone3.Enabled = False
            txtInvFax.Enabled = False
        Else

            txtInvAddress1.Enabled = True
            txtInvAddress2.Enabled = True
            txtInvCity.Enabled = True
            ddlInvStateID.Enabled = True
            txtInvZip.Enabled = True
            ddlInvCountryID.Enabled = True
            txtInvPhone1.Enabled = True
            'txtInvPhone2.Enabled = True
            'txtInvPhone3.Enabled = True
            txtInvFax.Enabled = True

            txtInvAddress1.Text = ""
            txtInvAddress2.Text = ""
            txtInvCity.Text = ""
            ddlInvStateID.SelectedIndex = 0
            txtInvZip.Text = ""
            ddlInvCountryID.SelectedIndex = 0
            txtInvPhone1.Text = ""
            'txtInvPhone2.Text = ""
            'txtInvPhone3.Text = ""
            txtInvFax.Text = ""

        End If
    End Sub

    ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
    Protected Sub chkIsRemoteServerFL_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkIsRemoteServerFL.CheckedChanged
        If chkIsRemoteServerFL.Checked = True Then
            lblRemoteServerUsrNmFL.Visible = True
            lblRemoteServerPwdFL.Visible = True
            txtRemoteServerUsrNmFL.Visible = True
            txtRemoteServerPwdFL.Visible = True
        Else
            lblRemoteServerUsrNmFL.Visible = False
            lblRemoteServerPwdFL.Visible = False
            txtRemoteServerUsrNmFL.Visible = False
            txtRemoteServerPwdFL.Visible = False

        End If
    End Sub

    Private Sub ReLoadPageContent(ByVal campid As String, viewstateds As DataSet)

        Dim sw As New StringWriter
        viewstateds.WriteXml(sw)
        ViewState("ds") = sw.ToString()

        Dim selIndex As Integer
        Dim ds1 As New DataSet
        Dim sr As New StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        txtRowIds.Text = campid.ToString
        Master.PageObjectId = campid
        Master.PageResourceId = ResourceId
        Master.SetHiddenControlForAudit()

        'Try
        strGUID = campid.ToString()
        'strGUID = dlstCmpAddr.DataKeys(e.Item.ItemIndex).ToString()
        objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGUID)
        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
        ViewState("MODE") = "EDIT"
        'selIndex = e.Item.ItemIndex
        dlstCmpAddr.SelectedIndex = selIndex
        ds1.ReadXml(sr)
        PopulateDataList(ds1)

        'Header1.EnableHistoryButton(True)
        'Header1.ObjectID = strGUID
        ''Added by saraswathi to show the remote password as encrypted
        Dim pwd As String = ""
        If strGUID <> "" Then
            Dim UserSecFacade As New UserSecurityFacade
            pwd = UserSecFacade.GetPasswordforCampus(strGUID)
        End If
        txtRemoteServerPwd.Attributes.Add("Value", pwd)

        ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
        Dim PwdFL As String = ""
        If strGUID <> "" Then
            Dim UserSecFacade As New UserSecurityFacade
            PwdFL = UserSecFacade.GetPasswordforCampusFL(strGUID)
        End If
        txtRemoteServerPwdFL.Attributes.Add("Value", PwdFL)
        '''''''''''''''''''''''''''''''

        If chkIsRemoteServer.Checked = True Then
            lblRemoteServerUsrNm.Visible = True
            lblRemoteServerPwd.Visible = True
            txtRemoteServerUsrNm.Visible = True
            txtRemoteServerPwd.Visible = True
            'lblSourceFolderLoc.Visible = True
            'lblTargetFolderLoc.Visible = True
            'txtSourceFolderLoc.Visible = True
            'txtTargetFolderLoc.Visible = True
        Else
            lblRemoteServerUsrNm.Visible = False
            lblRemoteServerPwd.Visible = False
            txtRemoteServerUsrNm.Visible = False
            txtRemoteServerPwd.Visible = False
            'lblSourceFolderLoc.Visible = False
            'lblTargetFolderLoc.Visible = False
            'txtSourceFolderLoc.Visible = False
            'txtTargetFolderLoc.Visible = False
        End If

        ''Added by Kamalesh Ahuja on May 27 2010 to resolve mantis issue id 18652
        If chkIsRemoteServerFL.Checked = True Then
            lblRemoteServerUsrNmFL.Visible = True
            lblRemoteServerPwdFL.Visible = True
            txtRemoteServerUsrNmFL.Visible = True
            txtRemoteServerPwdFL.Visible = True
        Else
            lblRemoteServerUsrNmFL.Visible = False
            lblRemoteServerPwdFL.Visible = False
            txtRemoteServerUsrNmFL.Visible = False
            txtRemoteServerPwdFL.Visible = False
        End If
        '''''''''''''''''''''

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEditSingleCell(pnlSDF, ResourceId, txtCampusId.Text, ModuleId)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ''Added by saraswathi lakshmanan
        ''On June 18 2009
        If chkUseCampusAddress.Checked = True Then
            txtInvAddress1.Enabled = False
            txtInvAddress2.Enabled = False
            txtInvCity.Enabled = False
            ddlInvStateID.Enabled = False
            txtInvZip.Enabled = False
            ddlInvCountryID.Enabled = False
            txtInvPhone1.Enabled = False
            'txtInvPhone2.Enabled = False
            'txtInvPhone3.Enabled = False
            txtInvFax.Enabled = False
        Else
            txtInvAddress1.Enabled = True
            txtInvAddress2.Enabled = True
            txtInvCity.Enabled = True
            ddlInvStateID.Enabled = True
            txtInvZip.Enabled = True
            ddlInvCountryID.Enabled = True
            txtInvPhone1.Enabled = True
            'txtInvPhone2.Enabled = True
            'txtInvPhone3.Enabled = True
            txtInvFax.Enabled = True

        End If

        'Dim rcb As Telerik.Web.UI.RadComboBox = Master.FindControl("CampusSelector")

        Dim campusId As Guid = CampusObjects.GetQueryParameterValueGuidCampusId(Request)
        If campusId.ToString() = campid.ToString() Then
            AdvantageSession.EnableCampusComboBox = False
        Else
            AdvantageSession.EnableCampusComboBox = True
        End If

        'If rcb.SelectedValue.ToString = campid.ToString Then
        '    ddlStatusId.Enabled = False
        'Else
        '    ddlStatusId.Enabled = True
        'End If

    End Sub

    'Protected Sub chkIsRemoteServerIL_CheckedChanged(sender As Object, e As EventArgs) Handles chkIsRemoteServerIL.CheckedChanged
    '    If chkIsRemoteServerIL.Checked = True Then
    '        lblRemoteServerUserNameIL.Visible = True
    '        lblRemoteServerPasswordIL.Visible = True
    '        'txtRemoteServerUserNameIL.Visible = True
    '        'txtRemoteServerPasswordIL.Visible = True
    '    Else
    '        lblRemoteServerUserNameIL.Visible = False
    '        lblRemoteServerPasswordIL.Visible = False
    '        'txtRemoteServerUserNameIL.Visible = False
    '        'txtRemoteServerPasswordIL.Visible = False

    '    End If
    'End Sub

    Private Sub BuildSrcCategoryDDL()

        Dim SrcCategories As New LeadFacade

        ddlSourceCategory.Items.Clear()

        With ddlSourceCategory
            .DataTextField = "SourceCatagoryDescrip"
            .DataValueField = "SourceCatagoryId"
            .DataSource = SrcCategories.GetAllSourceCategoryByCampus(txtCampusId.Text, String.Empty)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSourceType
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildUsersDDL()

        Dim leadAdmissionReps As New LeadDB

        ddlAdmRep.Items.Clear()

        With ddlAdmRep
            .DataTextField = "FullName"
            .DataValueField = "UserId"
            .DataSource = leadAdmissionReps.GetAllAdmissionRepsByCampus(txtCampusId.Text, String.Empty)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildSourceTypeDDL(ByVal SourceID As String, Optional ByVal boolDisplayInActive As Boolean = False)

        Dim SourceType As New LeadFacade

        ddlSourceType.Items.Clear()

        With ddlSourceType
            .DataTextField = "SourceTypeDescrip"
            .DataValueField = "SourceTypeId"
            .DataSource = SourceType.GetAllSourceTypeByCampus(ddlSourceCategory.SelectedValue, txtCampusId.Text, "", boolDisplayInActive)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildDDLs()

        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        ddlList.Add(New AdvantageDDLDefinition(ddlLeadStatus, AdvantageDropDownListName.LeadStatus, txtCampusId.Text, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneType1, AdvantageDropDownListName.Phone_Types, txtCampusId.Text, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneType2, AdvantageDropDownListName.Phone_Types, txtCampusId.Text, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlAddType, AdvantageDropDownListName.Address_Types, txtCampusId.Text, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlGender, AdvantageDropDownListName.Genders, txtCampusId.Text, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlPortalCountry, AdvantageDropDownListName.Countries, txtCampusId.Text, True, True, String.Empty))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub

    Protected Sub ddlSourceCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSourceCategory.SelectedIndexChanged

        If (ddlSourceCategory.SelectedItem.Text <> "Select") Then

            BuildSourceTypeDDL(ddlSourceCategory.SelectedValue, True)
        Else
            ddlSourceType.Items.Clear()
            ddlSourceType.Items.Insert(0, New ListItem("Select", ""))
            ddlSourceType.SelectedIndex = 0
        End If

        ddlSourceType.Focus()
    End Sub
    Private Sub GetPortalSettingValueByCampus(ByVal CampusId As String)

        With New StudentPortalFacade

            Dim dataObj As DataSet

            dataObj = .GetPortalValuesByCampus(CampusId)

            If dataObj.Tables(0).Rows.Count > 0 Then

                BuildDDLs()

                chkSourceCategoryAll.Checked = dataObj.Tables(0).Rows(0)("SourceCategoryAll").ToString()
                If (dataObj.Tables(0).Rows(0)("SourceCatagoryDescrip").ToString() <> "") Then
                    BuildSrcCategoryDDL()
                    ddlSourceCategory.Items.Insert(0, New ListItem("Select", ""))
                    ddlSourceCategory.SelectedItem.Text = dataObj.Tables(0).Rows(0)("SourceCatagoryDescrip").ToString()
                    ddlSourceCategory.SelectedValue = dataObj.Tables(0).Rows(0)("SourceCategoryId").ToString()
                End If

                chkSourceTypeAll.Checked = dataObj.Tables(0).Rows(0)("SourceTypeAll").ToString()
                If (dataObj.Tables(0).Rows(0)("SourceTypeDescrip").ToString() <> "") Then
                    BuildSourceTypeDDL(dataObj.Tables(0).Rows(0)("SourceCategoryId").ToString())
                    ddlSourceType.SelectedItem.Text = dataObj.Tables(0).Rows(0)("SourceTypeDescrip").ToString()
                    ddlSourceType.SelectedValue = dataObj.Tables(0).Rows(0)("SourceTypeId").ToString()
                End If

                chkAdmRepAll.Checked = dataObj.Tables(0).Rows(0)("AdmRepAll").ToString()
                If (dataObj.Tables(0).Rows(0)("FullName").ToString() <> "") Then
                    BuildUsersDDL()
                    ddlAdmRep.SelectedItem.Text = dataObj.Tables(0).Rows(0)("FullName").ToString()
                    ddlAdmRep.SelectedValue = dataObj.Tables(0).Rows(0)("AdmRepId").ToString()
                End If

                chkLeadStatusAll.Checked = dataObj.Tables(0).Rows(0)("LeadStatusAll").ToString()
                If (dataObj.Tables(0).Rows(0)("LeadStatus").ToString() <> "") Then
                    ddlLeadStatus.SelectedItem.Text = dataObj.Tables(0).Rows(0)("LeadStatus").ToString()
                    ddlLeadStatus.SelectedValue = dataObj.Tables(0).Rows(0)("LeadStatusId").ToString()
                End If

                chkPhoneType1All.Checked = dataObj.Tables(0).Rows(0)("PhoneType1All").ToString()
                If (dataObj.Tables(0).Rows(0)("PhoneType1Descrip").ToString() <> "") Then
                    ddlPhoneType1.SelectedItem.Text = dataObj.Tables(0).Rows(0)("PhoneType1Descrip").ToString()
                    ddlPhoneType1.SelectedValue = dataObj.Tables(0).Rows(0)("PhoneType1Id").ToString()
                End If

                chkPhoneType2All.Checked = dataObj.Tables(0).Rows(0)("PhoneType2All").ToString()
                If (dataObj.Tables(0).Rows(0)("PhoneType2Descrip").ToString() <> "") Then
                    ddlPhoneType2.SelectedItem.Text = dataObj.Tables(0).Rows(0)("PhoneType2Descrip").ToString()
                    ddlPhoneType2.SelectedValue = dataObj.Tables(0).Rows(0)("PhoneType2Id").ToString()
                End If

                chkAddTypeAll.Checked = dataObj.Tables(0).Rows(0)("AddTypeAll").ToString()
                If (dataObj.Tables(0).Rows(0)("AddressDescrip").ToString() <> "") Then
                    ddlAddType.SelectedItem.Text = dataObj.Tables(0).Rows(0)("AddressDescrip").ToString()
                    ddlAddType.SelectedValue = dataObj.Tables(0).Rows(0)("AddTypeId").ToString()
                End If

                chkGenderAll.Checked = dataObj.Tables(0).Rows(0)("GenderAll").ToString()
                If (dataObj.Tables(0).Rows(0)("GenderDescrip").ToString() <> "") Then
                    ddlGender.SelectedItem.Text = dataObj.Tables(0).Rows(0)("GenderDescrip").ToString()
                    ddlGender.SelectedValue = dataObj.Tables(0).Rows(0)("GenderId").ToString()
                End If

                chkPortalCountryAll.Checked = dataObj.Tables(0).Rows(0)("PortalCountryAll").ToString()
                If (dataObj.Tables(0).Rows(0)("CountryDescrip").ToString() <> "") Then
                    ddlPortalCountry.SelectedItem.Text = dataObj.Tables(0).Rows(0)("CountryDescrip").ToString()
                    ddlPortalCountry.SelectedValue = dataObj.Tables(0).Rows(0)("PortalCountryId").ToString()
                End If

                chkPortalContactEmailAll.Checked = dataObj.Tables(0).Rows(0)("PortalContactEmailAll").ToString()
                If (dataObj.Tables(0).Rows(0)("PortalContactEmail").ToString() <> "") Then
                    txtPortalContactEmail.Text = dataObj.Tables(0).Rows(0)("PortalContactEmail").ToString()
                End If

                chkEmailSubjectAll.Checked = dataObj.Tables(0).Rows(0)("EmailSubjectAll").ToString()
                If (dataObj.Tables(0).Rows(0)("EmailSubject").ToString() <> "") Then
                    txtEmailSubject.Text = dataObj.Tables(0).Rows(0)("EmailSubject").ToString()
                End If

                chkEmailBodyAll.Checked = dataObj.Tables(0).Rows(0)("EmailBodyAll").ToString()
                If (dataObj.Tables(0).Rows(0)("EmailBody").ToString() <> "") Then
                    txtEmailBody.Text = dataObj.Tables(0).Rows(0)("EmailBody").ToString()
                End If

            End If
        End With
    End Sub

    Protected Sub chkIsRemoteServerIL_CheckedChanged(sender As Object, e As EventArgs) Handles chkIsRemoteServerIL.CheckedChanged
        If chkIsRemoteServerIL.Checked = True Then
            lblRemoteServerUserNameIL.Visible = True
            lblRemoteServerPasswordIL.Visible = True
            txtRemoteServerUserNameIL.Visible = True
            txtRemoteServerPasswordIL.Visible = True
        Else
            lblRemoteServerUserNameIL.Visible = False
            lblRemoteServerPasswordIL.Visible = False
            txtRemoteServerUserNameIL.Visible = False
            txtRemoteServerPasswordIL.Visible = False

        End If
    End Sub
    Protected Sub ddlCampusIsBranch_Changed(sender As Object, e As EventArgs) Handles ddlCampusIsBranch.SelectedIndexChanged
        ToggleCampusBranch()
    End Sub

    Private Sub DefaultParentCampusDropDown()
        ddlParentCampus.SelectedIndex = 0
    End Sub

    Private Sub ToggleCampusBranch()
        Dim campusIsBranch = ddlCampusIsBranch.SelectedValue.Equals("1")
        If (campusIsBranch) Then
            parentCampusContainer.Visible = True
        Else
            parentCampusContainer.Visible = False
            DefaultParentCampusDropDown()
        End If

    End Sub
End Class
