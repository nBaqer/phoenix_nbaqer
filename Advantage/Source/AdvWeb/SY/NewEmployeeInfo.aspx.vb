﻿Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections
Imports BL = Advantage.Business.Logic.Layer
Partial Class NewEmployeeInfo
    Inherits BasePage

    Protected WithEvents btnhistory As Button
    Protected state As AdvantageSessionState
    Protected WithEvents chkForeign As CheckBox
    Protected empId As String
    Protected strVID As String
    Protected m_Context As HttpContext
    Protected dtInstResFlds As DataTable
    Protected bSchoolRequiresDOB As Boolean
    Protected resourceId As Integer
    Protected moduleid As String
    Protected sdfcontrols As New SDFComponent
    Protected strDefaultCountry As String
    Protected userName As String
    Private pObj As New UserPagePermissionInfo

    Private mruProvider As BL.MRURoutines
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()


        mruProvider = New BL.MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))




    End Sub

#End Region
    Private campusId As String
    Protected userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim advantageUserState As User = AdvantageSession.UserState
        campusid = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        '   if we have an employeeInfo in the session then use it, else create a new one
        'If CommonWebUtilities.IsValidGuid(empId) Then
        '    If state("employeeInfo") Is Nothing Then
        '        state("employeeInfo") = (New EmployeesFacade).GetInfo(empId)
        '        state("empId") = CType(state("employeeInfo"), FAME.AdvantageV1.Common.EmployeeInfo).EmpId
        '    Else
        '        '   if this is not the same employee that we have in state... goto the backend
        '        If Not CType(state("employeeInfo"), FAME.AdvantageV1.Common.EmployeeInfo).EmpId = empId Then
        '            state("employeeInfo") = (New EmployeesFacade).GetInfo(empId)
        '            state("empId") = CType(state("employeeInfo"), FAME.AdvantageV1.Common.EmployeeInfo).EmpId
        '        End If
        '    End If
        'Else
        '    state("employeeInfo") = New Fame.AdvantageV1.Common.EmployeeInfo
        '    state("empId") = CType(state("employeeInfo"), FAME.AdvantageV1.Common.EmployeeInfo).EmpId
        '    empId = state("empId")
        'End If

        Session("SEARCH") = 0
        Session("EmployerID") = Nothing
        Dim objCommon As New CommonUtilities
        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString


        If Not Page.IsPostBack Then
            'objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW", pObj)

            m_Context = HttpContext.Current
            dtInstResFlds = CType(m_Context.Items("InstResFldsReq"), DataTable)

            For Each row As DataRow In dtInstResFlds.Rows
                If row("TblFldsId") = 471 And row("FldId") = 222 Then
                    'the dob (birthdate) field in a customer required field - set an indicator for the btnSave routine and turn off client side validation
                    bSchoolRequiresDOB = True


                End If

            Next



            ViewState("MODE") = "NEW"

            '   initialize Date Range Validator. 
            '   Birthdates must be at least 10 years ago and maximum 80 years ago            
            txtBirthDate.MinDate = CType(Date.Now.Subtract(New TimeSpan(29220, 0, 0, 0, 0)).ToShortDateString, Date)
            txtBirthDate.MaxDate = CType(Date.Now.Subtract(New TimeSpan(3652, 0, 0, 0, 0)).ToShortDateString, Date)
        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT", pObj)
        End If

        If Not Page.IsPostBack Then

            '   Build dropdownlists
            BuildDropDownLists()

            '   populate fields
            'Dim employeeInfo As FAME.AdvantageV1.Common.EmployeeInfo = (New EmployeesFacade).GetInfo(empId)
            'PopulateFields(employeeInfo)

            '   init buttons for edit

            txtEmpId.Text = Guid.NewGuid.ToString

            '   Set header info and save state
            'SetHeaderInfoAndSaveState(employeeInfo)

        Else
            ResetAgeField()
        End If
        ''Set the Delete Button so it prompts the user for confirmation when clicked
        'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        'Set the Birth Date button so that it generates a postback in the onblur event
        'txtBirthDate.Attributes.Add("onblur", "__doPostBack('txtBirthDate','')")
        GetInputMaskValue()

        'Make OtherState textbox visible
        If chkForeignZip.Checked = True Then
            lblOtherState.Visible = True
            txtOtherState.Visible = True
            ddlStateId.Enabled = False
        Else
            lblOtherState.Visible = False
            txtOtherState.Visible = False
            ddlStateId.Enabled = True
        End If

        'moduleid = HttpContext.Current.Request.Params("Mod").ToString
        moduleid = AdvantageSession.UserState.ModuleCode.ToString
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))


        'Check If any UDF exists for this resource
        Dim intSdfExists As Integer = sdfcontrols.GetSDFExists(resourceId, moduleid)
        If intSdfExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtFirstName.Text) <> "" Then
            sdfcontrols.GenerateControlsEdit(pnlSDF, resourceId, txtEmpId.Text, moduleid)
        Else
            sdfcontrols.GenerateControlsNew(pnlSDF, resourceId, moduleid)
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   instantiate component
        Dim empDB As New EmployeesFacade

        Dim errorMessage As String
        'Check If Mask is successful only if Foreign Is not checked
        If chkForeignZip.Checked = False Then
            errorMessage = ValidateFieldsWithInputMasks()
        Else
            errorMessage = ""
        End If

        'If bSchoolRequiresDOB = True Then
        '    If txtBirthDate.SelectedDate Is Nothing Then
        '        DisplayRADAlert(CallbackType.Postback, "Error1", "DOB is required", 350, 100, "Save Error")
        '        Exit Sub
        '    End If
        'End If

        If errorMessage = "" Then
            If txtBirthDate.SelectedDate.HasValue And bSchoolRequiresDOB = True Then
                If IsTextBoxValidDate(txtBirthDate.SelectedDate.Value.ToString) Then
                    'If Not (txtBirthDate.SelectedDate Is Nothing) Then
                    '   update Employee Info 
                    Dim result As String = empDB.UpdateInfo(BuildEmployeeInfo(txtEmpId.Text), AdvantageSession.UserState.UserName)
                    If result <> "" Then
                        '   Display Error Message
                        'DisplayErrorMessage(result)
                        DisplayRADAlert(CallbackType.Postback, "Error1", result, "Save Error")
                        Exit Sub
                    Else
                        cbxIsInDB.Checked = True

                        'txtEmpId.Text = empId
                    End If
                Else
                    Dim errMessage As String
                    If Not FAME.AdvantageV1.Common.EmployeeInfo.IsDOBValid(txtBirthDate.SelectedDate.Value) Then
                        errMessage = "Invalid Birth Date. " + vbCrLf + " Only employees older than " + AdvantageCommonValues.MinimumAcceptableEmployeeAge.ToString + " years are accepted."
                    Else
                        errMessage = "Invalid Birth Date"
                    End If
                    '   Display Error Message
                    'DisplayErrorMessage(errMessage)
                    DisplayRADAlert(CallbackType.Postback, "Error2", errMessage, "Save Error")
                    Exit Sub
                End If


            Else

                Dim result As String = empDB.UpdateInfo(BuildEmployeeInfo(txtEmpId.Text), AdvantageSession.UserState.UserName)
                If result <> "" Then
                    '   Display Error Message
                    'DisplayErrorMessage(result)
                    DisplayRADAlert(CallbackType.Postback, "Error1", result, "Save Error")
                    Exit Sub
                Else
                    cbxIsInDB.Checked = True

                    'txtEmpId.Text = empId
                End If



            End If
            strVID = BuildEmployeeStateObject(txtEmpId.Text)
            ' mruProvider.InsertMRU(3, txtEmpId.Text, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
        Else
            '   Display Error Message
            'DisplayErrorMessage(errorMessage)
            'DisplayRADAlert(CallbackType.Postback, "Error3", errorMessage, 350, 100, "Save Error")
            Exit Sub
        End If


        '   Set header info and save state
        'SetHeaderInfoAndSaveState(BuildEmployeeInfo(empId))


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim sdfid As ArrayList
        Dim sdfidValue As ArrayList
        '     Dim newArr As ArrayList
        Dim z As Integer
        Dim sdfControl As New SDFComponent
        Try
            sdfControl.DeleteSDFValue(txtEmpId.Text)
            sdfid = sdfControl.GetAllLabels(pnlSDF)
            sdfidValue = sdfControl.GetAllValues(pnlSDF)
            For z = 0 To sdfid.Count - 1
                sdfControl.InsertValues(txtEmpId.Text, Mid(sdfid(z).id, 5), sdfidValue(z))
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        If errorMessage = "" Then
            ClearControls()
        End If

        ' Master.BuildEmployeeMRU()
        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub
    Public Function BuildEmployeeStateObject(ByVal EmployeeId As String) As String
        Dim objStateInfo As New AdvantageStateInfo
        'Dim objGetStudentStatusBar As New AdvantageStateInfo
        'Dim facInputMasks As New InputMasksFacade
        Dim strEmployeeId As String

        strEmployeeId = EmployeeId

        'If EmployeeId.ToString.Trim = "" Then 'No Student was selected from MRU
        '    strEmployeeId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
        '                                                           3, _
        '                                                           AdvantageSession.UserState.CampusId.ToString)
        'Else
        '    strEmployeeId = EmployeeId
        'End If

        'objGetStudentStatusBar = mruProvider.BuildEmployeeStatusBar(strEmployeeId)
        'With objStateInfo
        '    .EmployeeId = objGetStudentStatusBar.EmployeeId
        '    .NameValue = objGetStudentStatusBar.NameValue
        '    .Address1 = objGetStudentStatusBar.Address1
        '    .Address2 = objGetStudentStatusBar.Address2
        '    .City = objGetStudentStatusBar.City
        '    .State = objGetStudentStatusBar.State
        '    .Zip = objGetStudentStatusBar.Zip
        'End With

        'Create a new guid to be associated with the employer pages
        Dim strVID As String = Guid.NewGuid.ToString

        'strLeadObjectPointer = strVID 'Set VID to Lead Object Pointer

        Session("EmployeeObjectPointer") = strVID ' This step is required, as master page loses values stored in class variables


        'Add an entry to AdvantageSessionState for this guid and object
        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        'Try
        '    Dim advResetUserState As New BO.User
        '    advResetUserState = AdvantageSession.UserState
        '    With advResetUserState
        '        .CampusId = New Guid(objStateInfo.CampusId.ToString)
        '    End With
        '    AdvantageSession.UserState = advResetUserState
        'Catch ex As Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    Throw New ArgumentException(ex.Message.ToString)
        'End Try

        mruProvider.InsertMRU(3, strEmployeeId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

        Return strVID

    End Function
    'Public Sub BuildEmployeeStateObject(ByVal EmployeeId As String, ByVal EmployeeName As String)
    '    Dim objStateInfo As New AdvantageStateInfo
    '    Dim objGetStudentStatusBar As New AdvantageStateInfo
    '    Dim strVID As String
    '    Dim facInputMasks As New InputMasksFacade
    '    Dim strEmployeeId As String = ""

    '    If EmployeeId.ToString.Trim = "" Then 'No Student was selected from MRU
    '        strEmployeeId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
    '                                                               3, _
    '                                                               AdvantageSession.UserState.CampusId.ToString)
    '    Else
    '        strEmployeeId = EmployeeId
    '    End If

    '    If Not strEmployeeId = "" Then
    '        objGetStudentStatusBar = mruProvider.BuildEmployeeStatusBar(strEmployeeId)
    '        With objStateInfo
    '            .EmployeeId = objGetStudentStatusBar.EmployeeId
    '            .NameValue = objGetStudentStatusBar.NameValue
    '            .Address1 = objGetStudentStatusBar.Address1
    '            .Address2 = objGetStudentStatusBar.Address2
    '            .City = objGetStudentStatusBar.City
    '            .State = objGetStudentStatusBar.State
    '            .Zip = objGetStudentStatusBar.Zip
    '        End With

    '        'Create a new guid to be associated with the Employee pages
    '        strVID = Guid.NewGuid.ToString

    '        strEmployeeObjectPointer = strVID
    '        Session("EmployeeObjectPointer") = strEmployeeObjectPointer 'Needed as master page doesn't retain variable values when refreshed

    '        'Add an entry to AdvantageSessionState for this guid and object
    '        'load Advantage state
    '        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
    '        state(strVID) = objStateInfo
    '        'save current State
    '        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

    '        If strEmployeeId.ToString.Trim = "" Then 'No Employer was selected from MRU
    '            Dim strRedirectURL As String = ChildResourceURL + "&VID=" + strVID
    '            Response.Redirect(strRedirectURL, True)
    '        Else
    '            Response.Clear()

    '            'Balaji 02/23/2012
    '            'The MRU list may come from different campuses, so make sure the QueryString always contains the 
    '            'lead or student's default campus
    '            'Reset AdvantageSession User State Object's CampusId Property
    '            '   Reason to reset user state: All Pages in Advantage are getting the campusid from the AdvantageSession.UserState Object
    '            '   So resetting the object here will impact all pages

    '            Try
    '                Dim advResetUserState As New BO.User
    '                advResetUserState = AdvantageSession.UserState
    '                With advResetUserState
    '                    .CampusId = New Guid(Request.QueryString("cmpid"))
    '                End With
    '                AdvantageSession.UserState = advResetUserState
    '                'UpdateMRUTable
    '                'Reason: We need to keep track of the last student record the user worked with
    '                'Scenario1 : User can log in and click on a student page without using MRU 
    '                'and we need to display the data of the last student the user worked with
    '                'If the user is a first time user, we will load the student who was last added to advantage
    '                mruProvider.UpdateEmployeeMRUList(strEmployeeId, AdvantageSession.UserState.UserId.ToString, _
    '                                          AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)
    '            Catch ex As Exception
     '            	Dim exTracker = new AdvApplicationInsightsInitializer()
    '            	exTracker.TrackExceptionWrapper(ex)

    '                Throw New ArgumentException(ex.Message.ToString)
    '            End Try
    '            'ReBuildEmployeeTabs(strVID)
    '            Dim strRedirectURL As String = BuildRedirectURLForEmployeePages(AdvantageSession.UserState.CampusId.ToString, strVID)
    '            'Response.Redirect(strRedirectURL, True)
    '            If strRedirectURL.Trim = "" Then
    '                ShowNotificationWhenUserHasNoAccessToEntityPage("employee")
    '                Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
    '                Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)
    '                grdMRU.ToolTip = "User has insufficient permissions to access employee pages"
    '                Exit Sub
    '            End If
    '            Try
    '                Response.Redirect(strRedirectURL, False)
    '            Catch ex As Exception
     '            	Dim exTracker = new AdvApplicationInsightsInitializer()
    '            	exTracker.TrackExceptionWrapper(ex)

    '            End Try
    '        End If
    '    Else
    '        RedirectToEmployeeSearchPage(AdvantageSession.UserState.CampusId.ToString)
    '    End If
    'End Sub
    Private Sub ResetAgeField()
        'lblAge.Enabled = False
        'txtAge.Visible = False
    End Sub
    'Private Sub ValidateDate()
    '    If IsTextBoxValidDate(txtBirthDate.SelectedDate.Value.ToString) Then
    '        If Not txtBirthDate.SelectedDate.ToString = Nothing Then
    '            If Date.Compare(Date.Parse(txtBirthDate.SelectedDate.ToString), Date.Now) < 0 Then
    '                txtAge.Text = Age(Date.Parse(txtBirthDate.SelectedDate.ToString)).ToString
    '                txtAge.Visible = True
    '            End If
    '        End If
    '    Else
    '        Dim errMessage As String
    '        If Not Fame.AdvantageV1.Common.EmployeeInfo.IsDOBValid(txtBirthDate.SelectedDate.ToString) Then
    '            errMessage = "Invalid Birth Date." + vbCrLf + "Only employees older than " + AdvantageCommonValues.MinimumAcceptableEmployeeAge.ToString + " years are accepted."
    '        Else
    '            errMessage = "Invalid Birth Date"
    '        End If
    '        '   Display Error Message
    '        'DisplayErrorMessage(errMessage)
    '        DisplayRADAlert(CallbackType.Postback, "Error4", errMessage, 350, 100, "Date Error")
    '        Exit Sub
    '    End If
    'End Sub
    Private Function Age(ByVal DOBDate As Date) As Integer
        If Now.DayOfYear < DOBDate.DayOfYear Then
            Return Now.Year - DOBDate.Year - 1
        Else
            Return Now.Year - DOBDate.Year
        End If
    End Function

    Private Sub PopulateFields(ByVal employeeInfo As Fame.AdvantageV1.Common.EmployeeInfo)
        '   bind Status

        Dim facInputMasks As New InputMasksFacade
        Dim zipMask As String
        Dim ssnMask As String

        'Get the mask for phone numbers and zip
        'facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        If Not (employeeInfo.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = employeeInfo.StatusId

        With employeeInfo

            '   IsInDB
            cbxIsInDB.Checked = .IsInDB

            '   bind LastName
            txtLastName.Text = .LastName

            '   bind FirstName
            txtFirstName.Text = .FirstName

            '   bind MI
            txtMI.Text = .MI

            'bind Zip checkbox


            '   bind prefix
            If Not (.PrefixId = Guid.Empty.ToString) Then ddlPrefixId.SelectedValue = .PrefixId

            '   bind suffix
            If Not (.SuffixId = Guid.Empty.ToString) Then ddlSuffixId.SelectedValue = .SuffixId

            '   bind SSN
            txtSSN.Text = .SSN
            If txtSSN.Text <> "" Then
                txtSSN.Text = facInputMasks.ApplyMask(ssnMask, txtSSN.Text)
            End If

            '   bind ID
            txtID.Text = .ID

            'ForeignZip
            chkForeignZip.Checked = .ForeignZip

            If chkForeignZip.Checked = True Then
                chkForeignZip.Checked = True
                txtOtherState.Visible = True
                ddlStateId.Enabled = False
                lblOtherState.Visible = True
            Else
                txtOtherState.Visible = False
                ddlStateId.Enabled = True
                chkForeignZip.Checked = False
                lblOtherState.Visible = False
            End If

            '   bind BirthDate
            If Not (.BirthDate = Date.MaxValue) Then
                txtBirthDate.SelectedDate = .BirthDate
                '   Age
                txtAge.Text = Age(.BirthDate).ToString
            Else
                txtBirthDate.SelectedDate = Nothing
            End If

            '   bind Gender
            If Not (.GenderId = Guid.Empty.ToString) Then ddlGenderId.SelectedValue = .GenderId

            '   bind Race
            If Not (.RaceId = Guid.Empty.ToString) Then ddlRaceId.SelectedValue = .RaceId

            '   bind Marital Status
            If Not (.MaritalStatId = Guid.Empty.ToString) Then ddlMaritalStatId.SelectedValue = .MaritalStatId

            '   bind address1
            txtAddress1.Text = .Address1

            '   bind address2
            txtAddress2.Text = .Address2

            '   bind city
            txtcity.Text = .City

            '   bind StateId
            Try
                ddlStateId.SelectedValue = .StateId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                BuildStatesDDL()
            End Try

            '   bind Zip
            'Get Zip
            txtZip.Text = .Zip
            If txtZip.Text <> "" And chkForeignZip.Checked = False Then
                txtZip.Text = facInputMasks.ApplyMask(zipMask, txtZip.Text)
            End If

            '   bind Country
            Try
                ddlCountryId.SelectedValue = strDefaultCountry
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlCountryId.SelectedIndex = 0
            End Try

            'bind OtherState
            txtOtherState.Text = .OtherState



        End With
    End Sub
    Private Function BuildEmployeeInfo(ByVal empId As String) As Fame.AdvantageV1.Common.EmployeeInfo

        '   instantiate class
        Dim employeeInfo As New FAME.AdvantageV1.Common.EmployeeInfo(empId)
        Dim facInputMask As New InputMasksFacade
        '        Dim phoneMask As String
        Dim zipMask As String
        Dim ssnMask As String

        With employeeInfo
            '   IsInDB
            .IsInDB = cbxIsInDB.Checked

            '   get EmpId
            .EmpId = empId

            '   get Status
            .StatusId = ddlStatusId.SelectedValue

            '   get LastName
            .LastName = txtLastName.Text.Trim

            '   get FirstName
            .FirstName = txtFirstName.Text.Trim

            '   get MI
            .MI = txtMI.Text

            '   get prefix
            .PrefixId = ddlPrefixId.SelectedValue

            '   get suffix
            .SuffixId = ddlSuffixId.SelectedValue

            '   get SSN
            If txtSSN.Text <> "" Then
                ssnMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
                .SSN = facInputMask.RemoveMask(ssnMask, txtSSN.Text)
            Else
                .SSN = txtSSN.Text
            End If


            '   bind ID
            .ID = txtID.Text.Trim

            '   get BirthDate
            If Not (txtBirthDate.SelectedDate.ToString = Nothing) Then
                .BirthDate = Date.Parse(txtBirthDate.SelectedDate)
            Else
                .BirthDate = Date.MaxValue
            End If

            '   get Gender
            .GenderId = ddlGenderId.SelectedValue

            '   get Race
            .RaceId = ddlRaceId.SelectedValue

            '   get Marital Status
            .MaritalStatId = ddlMaritalStatId.SelectedValue

            '   get address1
            .Address1 = txtAddress1.Text

            '   get address2
            .Address2 = txtAddress2.Text

            '   get city
            .City = txtcity.Text

            '   get StateId
            .StateId = ddlStateId.SelectedValue

            '   get Zip
            If txtZip.Text <> "" And chkForeignZip.Checked = False Then
                zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
                .Zip = facInputMask.RemoveMask(zipMask, txtZip.Text)
            Else
                .Zip = txtZip.Text
            End If


            '   get Country
            .CountryId = ddlCountryId.SelectedValue

            .ForeignZip = chkForeignZip.Checked

            'get otherstate value
            .OtherState = txtOtherState.Text

            .CampusId = campusId

        End With

        '   return data
        Return employeeInfo

    End Function
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildPrefixesDDL()
        BuildSuffixesDDL()
        BuildRacesDDL()
        BuildGendersDDL()
        BuildMaritalStatusesDDL()
        BuildStatesDDL()
        BuildCountriesDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    Private Sub BuildPrefixesDDL()
        '   bind the prefix DDL
        Dim prefixes As New PrefixesFacade

        With ddlPrefixId
            .DataTextField = "PrefixDescrip"
            .DataValueField = "PrefixId"
            .DataSource = prefixes.GetAllPrefixes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSuffixesDDL()
        '   bind the suffix DDL
        Dim suffixes As New SuffixesFacade

        With ddlSuffixId
            .DataTextField = "SuffixDescrip"
            .DataValueField = "SuffixId"
            .DataSource = suffixes.GetAllSuffixes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildRacesDDL()
        '   bind the race DDL
        Dim races As New RacesFacade

        With ddlRaceId
            .DataTextField = "RaceDescrip"
            .DataValueField = "RaceId"
            .DataSource = races.GetAllRaces()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildGendersDDL()
        '   bind the Gender DDL
        Dim genders As New GendersFacade

        With ddlGenderId
            .DataTextField = "GenderDescrip"
            .DataValueField = "GenderId"
            .DataSource = genders.GetAllGenders()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildMaritalStatusesDDL()
        '   bind the MaritalStatus DDL
        Dim maritalStatuses As New MaritalStatusesFacade

        With ddlMaritalStatId
            .DataTextField = "MaritalStatusDescrip"
            .DataValueField = "MaritalStatId"
            .DataSource = maritalStatuses.GetAllMaritalStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildStatesDDL()
        '   bind the State DDL
        Dim states As New StatesFacade

        With ddlStateId
            .DataTextField = "StateDescrip"
            .DataValueField = "StateId"
            .DataSource = states.GetAllStates()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .Items.Insert(1, New ListItem("Other States Not Listed", "5451720D-583D-4364-802A-BF500EDA1234"))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildCountriesDDL()
        '   bind Countries ddl
        Dim countries As New PrefixesFacade

        '   build Employer Countries ddl
        With ddlCountryId
            .DataTextField = "CountryDescrip"
            .DataValueField = "CountryId"
            .DataSource = countries.GetAllCountries()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            '.SelectedIndex = 0
        End With
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)

    '    '   Set error condition
    '    Customvalidator1.ErrorMessage = errorMessage
    '    Customvalidator1.IsValid = False

    '    If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
    '        '   Display error in message box in the client
    '        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    '    End If

    'End Sub

    Private Sub SetHeaderInfoAndSaveState(ByVal employeeInfo As Fame.AdvantageV1.Common.EmployeeInfo)

        ''   save information in state
        'state("empFirstName") = employeeInfo.FirstName
        'state("empLastName") = employeeInfo.LastName

        '   set values for the header
        If CommonWebUtilities.IsValidGuid(strVID) Then
            DirectCast(state(strVID), AdvantageStateInfo).NameCaption = "Employee"
            DirectCast(state(strVID), AdvantageStateInfo).NameValue = employeeInfo.LastName + ", " + employeeInfo.FirstName
        End If

        'Session("NameCaption") = "Employee"
        'Session("NameValue") = employeeInfo.LastName + ", " + employeeInfo.FirstName
        'Session("IdCaption") = ""
        'Session("IdValue") = ""

    End Sub
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'state("employeeInfo") = BuildEmployeeInfo(empId)
        ''save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(txtBirthDate)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        'BIndToolTip()
    End Sub
    Private Sub InitButtonsForLoad()
        'btnNew.Enabled = True
        'btnDelete.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        'btnDelete.Enabled = False
    End Sub
    'Private Sub InitButtonsForEdit()

    '    If pObj.HasFull Or pObj.HasEdit Then
    '        btnSave.Enabled = True
    '    Else
    '        btnSave.Enabled = False
    '    End If

    '    If pObj.HasFull Or pObj.HasDelete Then
    '        btnDelete.Enabled = True
    '    Else
    '        btnDelete.Enabled = False
    '    End If

    '    If pObj.HasFull Or pObj.HasAdd Then
    '        btnNew.Enabled = True
    '    Else
    '        btnNew.Enabled = False
    '    End If
    '    'btnNew.Enabled = True
    '    'btnDelete.Enabled = True

    '    'Set the Delete Button so it prompts the user for confirmation when clicked
    '    btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

    'End Sub
    Private Function IsTextBoxValidDate(ByVal birthDate As String) As Boolean
        If birthDate = "" Then Return True
        Try
            Return Fame.AdvantageV1.Common.EmployeeInfo.IsDOBValid(Date.Parse(birthDate))
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function

    'Private Sub txtBirthDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBirthDate.TextChanged
    '    'ValidateDate()
    'End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        If CommonWebUtilities.IsValidGuid(txtEmpId.Text) Then

            'update EmployeeEmergencyContact Info 
            Dim result As String = (New EmployeesFacade).DeleteEmployee(txtEmpId.Text)
            If Not result = "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error5", result, "Delete Error")
            Else
                state("employeeInfo") = New FAME.AdvantageV1.Common.EmployeeInfo
                state("empId") = CType(state("employeeInfo"), FAME.AdvantageV1.Common.EmployeeInfo).EmpId
                empId = CType(state("empId"), String)

                '   Set header info and save state
                SetHeaderInfoAndSaveState(CType(state("employeeInfo"), FAME.AdvantageV1.Common.EmployeeInfo))

                '   populate fields
                PopulateFields(CType(state("employeeInfo"), FAME.AdvantageV1.Common.EmployeeInfo))

                '   initialize buttons
                InitButtonsForLoad()

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim sdfControl As New SDFComponent
                sdfControl.DeleteSDFValue(txtEmpId.Text)
                sdfControl.GenerateControlsNew(pnlSDF, resourceId, moduleid)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            End If

        End If
    End Sub
    Private Sub GetInputMaskValue()
        Dim facInputMasks As New InputMasksFacade
        '        Dim correctFormat As Boolean
        '   Dim strMask As String
        Dim zipMask As String
        'Dim errorMessage As String
        'Dim strPhoneReq As String
        '   Dim strFaxReq As String
        Dim ssnMask As String
        Dim objCommon As New CommonUtilities

        'Get The Input Mask for Phone/Fax and Zip
        'Apply Mask Only If Its Local Zip
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        txtSSN.Mask = Replace(ssnMask, "#", "9")
        lblSSN.ToolTip = ""


        If chkForeignZip.Checked = False Then
            zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
            txtZip.Mask = Replace(zipMask, "#", "9")
            lblZip.ToolTip = zipMask
        Else
            txtZip.Mask = ""
            lblZip.ToolTip = ""
        End If


        'Get The RequiredField Value
        Dim ssnReq As String
        objCommon.SetRequiredColorMask("Zip")
        ssnReq = objCommon.SetRequiredColorMask("SSN")
    End Sub

    Private Function ValidateFieldsWithInputMasks() As String
        Dim facInputMasks As New InputMasksFacade
        Dim correctFormat As Boolean
        Dim zipMask As String
        Dim errorMessage As String = ""
        Dim ssnMask As String

        facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        'Validate the phone field format. If the field is empty we should not apply the mask
        'agaist it.

        'Validate the zip field format. If the field is empty we should not apply the mask
        'against it.
        If txtSSN.Text <> "" Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(ssnMask, txtSSN.Text)
            If correctFormat = False Then
                errorMessage = "Incorrect format for ssn field."
            End If
        End If


        If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtZip.Text)
            If correctFormat = False Then
                errorMessage &= "Incorrect format for zip field."
            End If
        End If

        Return errorMessage

    End Function

    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignZip.CheckedChanged
        'Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Page, txtAddress1)
    End Sub
    Private Sub txtcity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtcity.TextChanged
        If chkForeignZip.Checked = True Then
            'Dim objCommon As New CommonWebUtilities
            CommonWebUtilities.SetFocus(Page, txtOtherState)
        End If
    End Sub

    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOtherState.TextChanged
        'Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Page, txtZip)
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ClearControls()
    End Sub
    Private Sub ClearControls()
        ClearDropDown()
        txtEmpId.Text = Guid.NewGuid.ToString

        cbxIsInDB.Checked = False

        '   bind LastName
        txtLastName.Text = ""

        '   bind FirstName
        txtFirstName.Text = ""

        '   bind MI
        txtMI.Text = ""

        'bind SSN
        txtSSN.Text = ""

        txtID.Text = ""

        chkForeignZip.Checked = False

        If chkForeignZip.Checked = True Then
            chkForeignZip.Checked = True
            txtOtherState.Visible = True
            ddlStateId.Enabled = False
            lblOtherState.Visible = True
        Else
            txtOtherState.Visible = False
            ddlStateId.Enabled = True
            chkForeignZip.Checked = False
            lblOtherState.Visible = False
        End If

        txtBirthDate.SelectedDate = Nothing

        txtAddress1.Text = ""

        '   bind address2
        txtAddress2.Text = ""

        '   bind city
        txtcity.Text = ""

        '   bind Zip
        'Get Zip
        txtZip.Text = ""


        'bind OtherState
        txtOtherState.Text = ""

        'init buttons for load
        InitButtonsForLoad()

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, resourceId, moduleid)
    End Sub
    Private Sub ClearDropDown()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(DropDownList) Then
                    If CType(ctl, DropDownList).Items.Count > 0 Then
                        CType(ctl, DropDownList).SelectedIndex = 0
                    End If

                End If
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub





End Class