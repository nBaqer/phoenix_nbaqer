<%@ Reference Control="~/UserControls/Header.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="StudentContactPhones" CodeFile="StudentContactPhones.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Student Contact Phones</title>
	<%--	<LINK href="../css/localhost.css" type="text/css" rel="stylesheet">--%>
        <LINK href="../css/systememail.css" type="text/css" rel="stylesheet">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript">
		    
		</script>
        <script src="../Kendo/js/jquery.min.js"></script>
        <script type='text/javascript'>
            
            disableEnter = function (evt) {
                var evt = (evt) ? evt : ((event) ? event : null);
                var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
                if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
            };

            document.onkeypress = disableEnter;
        </script>
	</HEAD>
	<body runat="server" leftMargin="0" topMargin="0" ID="Body1" NAME="Body1">
		<form id="Form1" method="post" runat="server">
		    <telerik:RadScriptManager runat="server"></telerik:RadScriptManager>
			<!-- beging header -->
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td><IMG src="../images/advantage_contact_phones.jpg"></td>
					<td class="topemail"><A class="close" onclick="top.close()" href=#>X Close</A></td>
				</tr>
			</table>
			<!-- end header -->
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%" height="100%" border="0">
				<TR>
					<TD valign="top" style="background-color: #E9EDF2;">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table2">
							<tr>
								<td  class="listframe">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width="15%" nowrap align="left"><asp:Label ID="lblShow" Runat="server"><b class="label">Show</b></asp:Label></td>
											<td width="85%" nowrap>
												<asp:radiobuttonlist id="radStatus" CssClass="Label" AutoPostBack="true" Runat="Server" RepeatDirection="Horizontal">
													<asp:ListItem Text="Active" Selected="True" />
													<asp:ListItem Text="Inactive" />
													<asp:ListItem Text="All" />
												</asp:radiobuttonlist>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="listframebottom"><div class="scrollleftpopup"><asp:datalist id="dlstStudentContactPhones" runat="server">
											<SelectedItemStyle CssClass="SelectedItem"></SelectedItemStyle>
											<ItemStyle CssClass="NonSelectedItem"></ItemStyle>
											<ItemTemplate>
												<asp:ImageButton id="imgInActive" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' ImageUrl="../images/inactive.gif" CommandArgument='<%# Container.DataItem("StudentContactPhoneId")%>' CausesValidation="False">
												</asp:ImageButton>
												<asp:ImageButton id="imgActive" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' ImageUrl="../images/active.gif" CommandArgument='<%# Container.DataItem("StudentContactPhoneId")%>' CausesValidation="False">
												</asp:ImageButton>
												<asp:LinkButton id="Linkbutton1" CssClass="NonSelectedItem" CausesValidation="False" Runat="server" CommandArgument='<%# Container.DataItem("StudentContactPhoneId")%>' text='<%# Container.DataItem("Phone")%>'>
												</asp:LinkButton>
											</ItemTemplate>
										</asp:datalist></div>
								</td>
							</tr>
						</table>
					</TD>
					<td class="leftside">
						<table cellSpacing="0" cellPadding="0" width="9" border="0" ID="Table3">
						</table>
					</td>
					<td class="DetailsFrameTop">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<tr>
								<td class="MenuFrame" align="right">
									<asp:button id="btnSave" runat="server" Text="Save" CssClass="save" UseSubmitBehavior="false"></asp:button>
									<asp:button id="btnNew" runat="server" Text="New" CssClass="new"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"></asp:button>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table5">
							<tr>
								<td class="detailsframe">
									<DIV class="scrollrightpopup">
									<asp:panel id="pnlRHS" Runat="server">
											<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="60%" align="center">
												<TR>
													<TD class="twocolumnlabelcell" noWrap>
														<asp:label id="lblStudentContactNameCaption" runat="server" cssclass="Label">Contact Name</asp:label></TD>
													<TD class="contentcell4">
														<asp:label id="lblStudentContactName" runat="server" cssclass="label">Student Contact Name</asp:label></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell" noWrap></td>
													<TD class="twocolumncontentcell">
														<asp:CheckBox id="chkForeignPhone" tabIndex="11" Runat="server" AutoPostBack="true" CssClass="CheckboxInternational"
															Text="International"></asp:CheckBox></td>
												</tr>
												<tr>
													<TD class="twocolumnlabelcell" noWrap>
														<asp:label id="lblPhone" runat="server" cssclass="Label">Phone</asp:label></TD>
													<TD class="twocolumncontentcell">
														<telerik:RadMaskedTextBox runat="server" ID="txtPhone" Mask="(###) ###-####" Width="300px" autocomplete="off"  />
                                                    </TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblExt" Runat="server" CssClass="Label">Extension</asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:textbox id="txtExt" Runat="server" CssClass="TextBox" Columns="3"></asp:textbox></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblBestTime" Runat="server" CssClass="Label">Best Time</asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:textbox id="txtBestTime" Runat="server" CssClass="TextBox" Columns="9"></asp:textbox></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell" noWrap>
														<asp:label id="lblPhoneTypeID" Runat="server" CssClass="Label">Type</asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:dropdownlist id="ddlPhoneTypeID" Runat="server" cssclass="DropDownListFF"></asp:dropdownlist></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell" noWrap>
														<asp:label id="lblStatusId" runat="server" cssclass="Label">Status</asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:dropdownlist id="ddlStatusId" Runat="server" cssclass="DropDownListFF"></asp:dropdownlist></TD>
												</TR>
											</TABLE>
											<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD class="twocolumnlabelcell" noWrap>
														<asp:textbox id="txtStudentContactPhoneId" Runat="server" Visible="false"></asp:textbox>
														<asp:checkbox id="chkIsInDB" runat="server" Visible="false" checked="False"></asp:checkbox></TD>
													<TD class="twocolumncontentcell">
														<asp:textbox id="txtRowIds" style="VISIBILITY: hidden" runat="server" CssClass="Label" width="10%"></asp:textbox>
														<asp:textbox id="txtResourceId" style="VISIBILITY: hidden" runat="server" CssClass="Label" Width="10%"></asp:textbox></TD>
												</TR>
												<TR>
													<TD>
														<asp:textbox id="txtStudentContactId" Runat="server" Visible="false"></asp:textbox></TD>
													<asp:textbox id="txtModUser" runat="server" Visible="False">ModUser</asp:textbox>
													<asp:textbox id="txtModDate" runat="server" Visible="False">ModDate</asp:textbox></TR>
											</TABLE>
										</asp:panel>
									</DIV>
								</td>
							</tr>
						</table>
					</td>
				</TR>
			</TABLE>
			<asp:textbox id="txtPrgVerId" Runat="server" Visible="true"></asp:textbox><asp:textbox id="txtCampusId" Runat="server" Visible="true" MaxLength="128"></asp:textbox>
			<!-- start validation panel--><asp:panel id="Panel1" runat="server"></asp:panel><asp:customvalidator id="Customvalidator1" runat="server" Display="None" ErrorMessage="CustomValidator"></asp:customvalidator><asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:validationsummary>
			<%--<div id="footer">Copyright  FAME 2005 - 2012. All rights reserved.</div>--%>
		</form>
	</body>
</HTML>

