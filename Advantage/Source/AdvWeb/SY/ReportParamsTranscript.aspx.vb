﻿
Imports System.Data
Imports System.Web.UI
Imports System.Xml
Imports Telerik.Web.UI.com.hisoftware.api2
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.DataAccess
Imports Microsoft.VisualBasic
Imports FAME.Advantage.Reporting
Imports AdvWeb.VBCode.ComponentClasses

Namespace AdvWeb.SY
    Partial Class SyReportParamsTranscript
        Inherits BasePage
        Private pObj As New UserPagePermissionInfo
        Protected WithEvents PnlSavedPrefs As Panel
        Protected WithEvents Btnhistory As Button
        Protected WithEvents LblSkillGroups As Label
        Protected WithEvents LblStudentSkills As Label
        Protected WithEvents Lbl3 As Label
        Protected WithEvents Panel2 As Panel
        Protected WithEvents TblRequiredFields As Table
        'Protected WithEvents txtPreferenceName As System.Web.UI.WebControls.TextBox
        Protected PageTitle As String = "Report Parameters - "
        Protected CampusId, UserId As String
        Protected ResourceId As Integer
        'This call is required by the Web Form Designer.

        <Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()

        End Sub

        Protected MyAdvAppSettings As AdvAppSettings

        Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
            'CODEGEN: This method call is required by the Web Form Designer Do not modify it using the code editor.
            InitializeComponent()
            'This register the buttons to permit postback with them inside the ajax panel.
            ScriptManager.GetCurrent(Page).RegisterPostBackControl(lbtsearch)
            ScriptManager.GetCurrent(Page).RegisterPostBackControl(btnsearch)
            ScriptManager.GetCurrent(Page).RegisterPostBackControl(btnexport)
        End Sub

        Protected Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim advantageUserState As Advantage.Business.Objects.User = AdvantageSession.UserState

            Dim objCommon As New FAME.Common.CommonUtilities
            Dim rptFac As New ReportFacade
            Dim rptInfo As ReportInfo

            'Set the Delete button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

            CampusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
            ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
            UserId = AdvantageSession.UserState.UserId.ToString

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(ResourceId, String), CampusId)
            'Check if this page still exists in the menu while switching campus
            Dim boolSwitchCampus As Boolean = False
            Dim previousCampusId As String

            If Not Session("PreviousCampus") Is Nothing Then
                previousCampusId = Session("PreviousCampus").ToString
                If previousCampusId.Trim <> Request.QueryString("cmpid").ToString.Trim Then
                    boolSwitchCampus = True 'User switched campus
                End If
                Session("PreviousCampus") = Request.QueryString("cmpid").ToString.Trim
                If Me.Master.IsSwitchedCampus = True Then
                    If pObj.HasNone = True Then
                        Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                        Exit Sub
                    Else
                        CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                    End If
                End If
            End If

            If pObj Is Nothing Then
                'Send the user to the standard error page
                Session("Error") = "You do not have the necessary permissions to access to this report."
                Response.Redirect("../ErrorPage.aspx")
            End If

            'Hidde buttons if majorminor. only export should be visible.
            Dim setting As String = GetSettingString("MajorsMinorsConcentrations")
            If setting.ToUpper() = "YES" Then
                btnsearch.Visible = False
                btnfriendlyprint.Visible = False
                ddlexportto.SelectedIndex = 0
                ddlexportto.Enabled = False
            End If

            ' make sure the Session variable for Reporting Agency is cleared
            Session("RptAgency") = Nothing

            If ResourceId = 238 Then
                btnfriendlyprint.Enabled = False
            End If

            If Not Page.IsPostBack Then
                'If we are dealing with the Progress Report we want the required filters label to include
                'the term start cutoff
                If ResourceId = 537 Then
                    lblrequiredfilters.Text = lblrequiredfilters.Text & ", Term Start Cutoff or Term"
                End If

                'to enable the Suppress Header for Single and MultipleTranscripts
                If ResourceId = 238 Or ResourceId = 245 Then
                    chkrptsuppressheader.Enabled = True
                    chktermmodule.Visible = True
                    'DE8390 3/7/2013 Janet Robinson
                    chkShowOffTrans.Enabled = True
                    chkShowOffTrans.Visible = True
                Else
                    chkShowOffTrans.Enabled = False
                    chkShowOffTrans.Visible = False
                End If
                '--code added by Priyanka on date 8th of May 2009
                If ResourceId = 582 Or ResourceId = 602 Then
                    lblrequiredfilters.Visible = False
                    requiredfldvalstudent.Visible = False
                    requiredfldvalstuenroll.Visible = False
                End If

                If ResourceId = 537 Then
                    lbltermstartcutoff.Visible = True
                    txttermstart.Visible = True
                    rdolstchkunits.Visible = True
                    pnlrow.Visible = True
                    chkstudentsign.Visible = True
                    chkschoolsign.Visible = True
                    chktermmodule.Visible = True
                    chktotalcost.Visible = True
                    chkcurrentbalance.Visible = True

                End If
                '--
                pnl4.Visible = True     'Required Filters.
                pnl5.Visible = True     'Required Filters'labels.

                'Bind the DataList on the lhs to any preferences saved for this report.
                BindDataListFromDB()

                ViewState("resid") = Request.Params("resid")

                'Get the report info and store each piece in view state
                rptInfo = rptFac.GetReportInfo(CType(ViewState("resid"), String))
                ViewState("SQLID") = rptInfo.SqlId
                ViewState("OBJID") = rptInfo.ObjId
                ViewState("RESURL") = rptInfo.Url
                ViewState("Resource") = rptInfo.Resource

                'Populate all the filter sections.
                BuildAllSections()

                'Set the mode to NEW
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                'Set page title.
                PageTitle &= CType(ViewState("Resource"), String)
                'Page.DataBind()


            Else
                'Rebuild the dynamic table and dynamic validators each time there is a postback.
                BuildFilterOther()
            End If
        End Sub
        Private Sub BuildAllSections()
            Dim dt2 As DataTable
            Dim dtMasks As DataTable
            Dim dtOperators As DataTable
            Dim objRptParams As New RptParamsFacade
            Dim objCommon As New FAME.Common.CommonUtilities
            Dim dr As DataRow
            Dim ds As DataSet
            Dim numRows As Integer
            Dim rValues As Integer

            'Get the parameters for this report
            ds = objRptParams.GetRptParams

            'Set status of buttons and checkboxes.
            If ds.Tables.Count = 2 Then
                Dim row As DataRow = ds.Tables("RptProps").Rows(0)
                If Not row.IsNull("AllowParams") Then
                    If Not row("AllowParams") Then
                        'Report has no params, then disable Save, New and Delete buttons.
                        btnadd.Enabled = False
                        btnremove.Enabled = False
                        btnSave.Enabled = False
                        btnNew.Enabled = False
                        btnDelete.Enabled = False
                        'Filters checkbox.
                        chkrptfilters.Enabled = False
                        'Sort checkbox.
                        chkrptsort.Enabled = False
                    End If
                End If
                'Filter Criteria checkbox.
                If Not row.IsNull("AllowFilters") Then chkrptfilters.Enabled = CType(row("AllowFilters"), Boolean)
                'Sort Order checkbox.
                If Not row.IsNull("AllowSortOrder") Then chkrptsort.Enabled = CType(row("AllowSortOrder"), Boolean)
                'Selected Filter Criteria checkbox.
                If Not row.IsNull("SelectFilters") Then chkrptfilters.Checked = CType(row("SelectFilters"), Boolean)
                'Selected Sort Order checkbox.
                If Not row.IsNull("SelectSortOrder") Then chkrptsort.Checked = CType(row("SelectSortOrder"), Boolean)
                'Description checkbox.
                If Not row.IsNull("AllowDescrip") Then chkrptdescrip.Enabled = CType(row("AllowDescrip"), Boolean)
                'Instructions checkbox.
                If Not row.IsNull("AllowInstruct") Then chkrptinstructions.Enabled = CType(row("AllowInstruct"), Boolean)
                'Notes checkbox.
                If Not row.IsNull("AllowNotes") Then chkrptnotes.Enabled = CType(row("AllowNotes"), Boolean)
                'Code Added By Vijay Ramteke on May, 11 2009
                'Date Issue checkbox.
                If Not row.IsNull("AllowDateIssue") Then chkrptdateissue.Enabled = CType(row("AllowDateIssue"), Boolean)
                'Code Added By Vijay Ramteke on May, 11 2009

                'Code Added By Kamalesh Ahuja on August, 12 2010 to resolve mantis issue id 19506
                If Not row.IsNull("ShowLegalDisclaimer") Then chkshowlegaldisc.Enabled = CType(row("ShowLegalDisclaimer"), Boolean)
                '''''''''''''''''''''''''''

                ''Code Added By Vijay Ramteke On Settember 28, 2010 For Mantis Id 17685
                If Not row.IsNull("ShowTransferCampus") Then chkrptiswttdc.Enabled = CType(row("ShowTransferCampus"), Boolean)
                If Not row.IsNull("ShowTransferProgram") Then chkrptiswttdp.Enabled = CType(row("ShowTransferProgram"), Boolean)
                If Not row.IsNull("ShowLDA") Then chkrptdonotshowlda.Enabled = CType(row("ShowLDA"), Boolean)


                If Not row.IsNull("ShowUseSignLineForAttnd") Then chkUseSignatureForAttendance.Enabled = CType(row("ShowUseSignLineForAttnd"), Boolean)

                If Not row.IsNull("ShowTermProgressDescription") Then chkshowTermProgressDescription.Enabled = CType(row("ShowTermProgressDescription"), Boolean)

                Dim transcriptTypeString As String = CType(MyAdvAppSettings.AppSettings("TranscriptType", CampusId).ToLower, String)
                If transcriptTypeString = "traditional_numeric" Then
                    chkrptdateissue.Enabled = False
                End If
                If ResourceId = 238 Or ResourceId = 245 Then ' 238 = Transcript(individual Student)
                    If transcriptTypeString = "traditional_b" Then
                        chkrptshowclassdates.Enabled = True
                        chkUseSignatureForAttendance.Checked = True
                        'DE8390 3/7/2013 Janet Robinson
                        chkShowOffTrans.Checked = True
                        chkShowOffTrans.Enabled = True
                        chkShowOffTrans.Visible = True
                    Else
                        chkrptshowclassdates.Enabled = False
                        chkUseSignatureForAttendance.Enabled = False
                        'DE8390 3/7/2013 Janet Robinson
                        chkShowOffTrans.Visible = False
                        chkShowOffTrans.Enabled = False
                    End If
                End If


            End If

            'Get the input masks
            dtMasks = objCommon.GetInstInputMasks.Copy
            dtMasks.TableName = "InputMasks"
            ds.Tables.Add(dtMasks)

            With ds.Tables("InputMasks")
                .PrimaryKey = New DataColumn() { .Columns("FldId")}
            End With

            'Get the comparison operators
            dtOperators = objCommon.GetComparisonOps.Copy
            dtOperators.TableName = "Operators"
            ds.Tables.Add(dtOperators)

            'Add a dt to store the possible values for each parameter that belongs to the
            'FilterList section. For example, we want to be able to store the possible values
            'for Program or Status.
            ds.Tables.Add("FilterListValues")

            With ds.Tables("FilterListValues")
                .Columns.Add("RptParamId", GetType(Integer))
                .Columns.Add("ValueFld", GetType(String))
                .Columns.Add("DisplayFld", GetType(String))

                .Columns.Add("FldName", GetType(String))
                .Columns.Add("CampgrpId", GetType(String))
            End With

            'Add a dt to store the values selected for each parameter that belongs to the
            'FilterList section. For example, if the user selects Active and Attending for
            'Status we want to store these items.
            ds.Tables.Add("FilterListSelections")
            With ds.Tables("FilterListSelections")
                .Columns.Add("RptParamId", GetType(Integer))
                .Columns.Add("FldValue", GetType(String))

                .Columns.Add("FldName", GetType(String))
                .PrimaryKey = New DataColumn() { .Columns("RptParamId"), .Columns("FldValue")}
            End With

            'that it belongs to.
            For Each dr In ds.Tables("RptParams").Rows
                If dr("SortSec") = "True" Then
                    'Add the field info to the lbxSort listbox
                    lbxsort.Items.Add(New ListItem(CType(dr("Caption"), String), CType(dr("RptParamId"), String)))
                End If

                If dr("FilterListSec") = "True" Then
                    'Add the field info to the lbxFilterList listbox 
                    lbxfilterlist.Items.Add(New ListItem(CType(dr("Caption"), String), CType(dr("RptParamId"), String)))
                    'Get the list of possible values for this item
                    dt2 = objRptParams.GetFilterListValues(dr("DDLId"), , UserId.ToString)

                    numRows = dt2.Rows.Count

                    For rValues = 0 To numRows - 1
                        'Create a new row for the FilterListValues dt
                        Dim dRow1 As DataRow = ds.Tables("FilterListValues").NewRow
                        dRow1("RptParamId") = dr("RptParamId")
                        dRow1("ValueFld") = dt2.Rows(rValues)(0)
                        dRow1("DisplayFld") = dt2.Rows(rValues)(1)

                        dRow1("FldName") = dr("FldName")
                        dRow1("CampGrpId") = dt2.Rows(rValues)(2)
                        'Add the new row to the FilterListValues dt
                        ds.Tables("FilterListValues").Rows.Add(dRow1)
                    Next

                    If dr("Required") = "True" Then
                        'Add a hidden textbox and a compare validator if this field is marked as 'Required'.
                        'This textbox need to be updated everytime it is selected or deselected.
                        Dim txt As New TextBox
                        Dim compVal As New CompareValidator

                        txt.ID = "txt" & dr("RptParamId").ToString()
                        txt.CssClass = "totheme2"
                        txt.Text = Guid.Empty.ToString
                        txt.Width = Unit.Pixel(0)

                        compVal.ID = "compVal" & dr("RptParamId").ToString()
                        compVal.ControlToValidate = txt.ID
                        compVal.[Operator] = ValidationCompareOperator.NotEqual
                        compVal.ValueToCompare = Guid.Empty.ToString
                        compVal.Type = ValidationDataType.String
                        compVal.ErrorMessage = CType((dr("Caption") & " is a required filter."), String)
                        compVal.Display = ValidatorDisplay.None

                        pnlRequiredFieldValidators.Controls.Add(txt)
                        pnlRequiredFieldValidators.Controls.Add(compVal)
                    End If
                    If dr("RptParamId") = 1173 And dr("Required") = "False" Then
                        'Add a hidden textbox and a compare validator if this field is marked as 'Required'.
                        'This textbox need to be updated everytime it is selected or deselected.
                        Dim txt As New TextBox
                        'Dim compVal As New CompareValidator

                        txt.ID = "txt" & dr("RptParamId").ToString()
                        txt.CssClass = "totheme2"
                        txt.Text = Guid.Empty.ToString
                        txt.Width = Unit.Pixel(150)

                        pnlRequiredFieldValidators.Controls.Add(txt)
                    End If
                End If

                'Special case - Modified by Michelle Rodriguez on 11/18/2004.
                'We do not need to create a row for the StuEnrollId field.
                'Since this is a special page for the Student Transcript, there are several textboxs to hold 
                'the student name, program version, StuEnrollId, academic year and term. 
                'Only the student name and program version are visible. 
                'Internally, we only need to StuEnrollId to compute the report.
                'In case we might need other filters, I have left this section of code below. However, we need
                'to exclude the StuEnrollId parameter from it because there is already an invisible textbox 
                'that will hold the value depending on the user selection thru the Student Search pop-up.
                'StuEnrollId is RptParamId=127 and FldId=636
                If dr("FilterOtherSec") = "True" And Not (dr("RptParamId") = 127 And dr("FldId") = 636) Then
                    pnl4.Visible = True
                    'Add a row to the tblOthers table. This is a server control. 
                    '       
                    Dim r As New TableRow
                    Dim c0 As New TableCell
                    Dim c1 As New TableCell
                    Dim c2 As New TableCell
                    Dim c3 As TableCell
                    Dim c4 As TableCell
                    Dim c5 As New TableCell
                    Dim lbl As New Label
                    Dim ddl As New DropDownList
                    Dim txt As New TextBox
                    Dim txt2 As New TextBox
                    Dim fldType As String = dr("FldType").ToString
                    Dim regExpVal As RegularExpressionValidator
                    Dim reqFldVal As RequiredFieldValidator

                    txt.ID = "txt" & dr("RptParamId").ToString()
                    txt.CssClass = "textbox"
                    txt.Width = Unit.Pixel(120)
                    txt.ClientIDMode = ClientIDMode.Static


                    txt2.ID = "txt2" & dr("RptParamId").ToString()
                    txt2.CssClass = "textbox"           '"ToTheme2"
                    txt2.Width = Unit.Pixel(120)
                    txt2.Visible = False
                    txt2.ClientIDMode = ClientIDMode.Static

                    lbl.Text = CType(dr("Caption"), String)
                    lbl.CssClass = "label"
                    c0.Controls.Add(lbl)

                    ddl.ID = "ddl" & dr("RptParamId").ToString()
                    'ddl.DataSource = ds.Tables("Operators")
                    'Populate dropdownlist with operators relevant to the diferent data types.
                    If (ViewState("resid") = 590 And dr("FldId") = 1047) Then
                        'The report Attendance history Single Student need a date range; therefore, the operators drowdownlist can only contain one value.
                        'Between operator
                        ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                        txt2.Visible = True
                        'code added by Vijay Ramteke on date 7th of May
                    ElseIf (ViewState("resid") = 238 And dr("Fldid") = 65) Then
                        'Between operator
                        ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                        txt2.Visible = True
                        '--
                    ElseIf (ViewState("resid") = 582 And (dr("Fldid") = 1116 Or dr("Fldid") = 1107)) Then
                        'The report Individual SAP Report need a date range; therefore, the operators drowdownlist can only contain one value.
                        'Between operator
                        ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                        '--
                        'code added by Priyanka on date 13th of May
                        '--
                    ElseIf (ViewState("resid") = 602 And dr("Fldid") = 654) Then
                        'The report Dropped Courses for single student need a Drop date range; therefore, the operators drowdownlist can only contain one value.
                        'Between operator
                        ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                        '--
                    ElseIf (ViewState("resid") = 590 And dr("FldId") = 1105) Then
                        dtOperators = createStartDayTable()
                        dtOperators.TableName = "Operators1"
                        ds.Tables.Add(dtOperators)
                        ddl.DataSource = ds.Tables("Operators1")
                        txt.Visible = False

                    Else
                        Select Case dr("FldType")
                            Case "Datetime"
                                ddl.DataSource = New DataView(ds.Tables("Operators"), "AllowDateTime = 1", "", DataViewRowState.OriginalRows)

                            Case "Varchar", "Char"
                                ddl.DataSource = New DataView(ds.Tables("Operators"), "AllowString = 1", "", DataViewRowState.OriginalRows)

                            Case "Smallint", "Int", "TinyInt", "Float", "Decimal"
                                ddl.DataSource = New DataView(ds.Tables("Operators"), "AllowNumber = 1", "", DataViewRowState.OriginalRows)

                            Case Else
                                ddl.DataSource = ds.Tables("Operators")
                        End Select
                    End If

                    ddl.DataTextField = "CompOpText"
                    ddl.DataValueField = "CompOpId"
                    ddl.DataBind()
                    ddl.CssClass = "dropdownlist"
                    ddl.AutoPostBack = True
                    c1.Controls.Add(ddl)
                    c2.Controls.Add(txt)
                    c5.Controls.Add(txt2)

                    r.Cells.Add(c0)
                    r.Cells.Add(c1)
                    r.Cells.Add(c2)
                    r.Cells.Add(c5)

                    If fldType = "Datetime" Then
                        'Add a regular expression validator for Datetime fields.
                        'We can only accept dates with two slashes.
                        regExpVal = New RegularExpressionValidator
                        regExpVal.ID = "regExpVal" & dr("RptParamId").ToString()
                        regExpVal.ControlToValidate = txt.ID
                        regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                        'regExpVal.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                        regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                        '& vbCr & "The character required to separate values is the semicolon (;)."
                        regExpVal.Display = ValidatorDisplay.None
                        c3 = New TableCell
                        c3.Controls.Add(regExpVal)
                        regExpVal = New RegularExpressionValidator
                        regExpVal.ID = "regExpVal2" & dr("RptParamId").ToString()
                        regExpVal.ControlToValidate = txt2.ID
                        regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                        'regExpVal.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                        regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                        '& vbCr & "The character required to separate values is the semicolon (;)."
                        regExpVal.Display = ValidatorDisplay.None
                        c3.Controls.Add(regExpVal)
                        r.Cells.Add(c3)
                    End If

                    If dr("Required") = "True" Then
                        'Add a required field validator if this field is marked as 'Required'.
                        reqFldVal = New RequiredFieldValidator
                        reqFldVal.ID = "reqFldVal" & dr("RptParamId").ToString()
                        reqFldVal.ControlToValidate = txt.ID
                        reqFldVal.ErrorMessage = CType((dr("Caption") & " is a required filter."), String)
                        reqFldVal.Display = ValidatorDisplay.None
                        c4 = New TableCell
                        c4.Controls.Add(reqFldVal)
                        r.Cells.Add(c4)
                    End If

                    tblfilterothers.Rows.Add(r)
                End If

                ' Show Required Filters panel.
                If dr("Required") = "True" Then
                    pnl5.Visible = True
                    If lblrequiredfilters.Text = "" Then
                        lblrequiredfilters.Text = CType(dr("Caption"), String)
                    Else
                        lblrequiredfilters.Text &= CType((", " & dr("Caption")), String)
                    End If
                End If
            Next

            'Add a 'None' item so that users can deselect an item without having to
            'select another one.
            If lbxfilterlist.Items.Count > 0 Then
                lbxfilterlist.Items.Insert(0, New ListItem("None", CType(0, String)))
            End If

            'Add the ds DataSet to session so we can reuse it later on.
            Session("WorkingDS") = ds

            'Troy: DE13698: Central State Beauty - Reports - SAP report and  Dropped Students by Date Range report/ Missing required fields
            If ResourceId = 582 Then
                pnl5.Visible = True
                lblrequiredfilters.Visible = True
                lblrequiredfilters.Text = "Campus Group and Program Version OR Student and Enrollment"
            End If

            If ResourceId = 602 Then
                pnl5.Visible = True
                lblrequiredfilters.Visible = True
                lblrequiredfilters.Text = "Campus Group OR Student and Enrollment"
            End If
        End Sub

        Private Sub BuildFilterOther()
            Dim resID As Integer
            Dim dt, dt2 As DataTable
            Dim dr As DataRow
            Dim objRptParams As New RptParams
            'Dim objCommon As New FAME.common.CommonUtilities

            resID = CInt(ViewState("resid"))
            'Get the parameters for this report that belong to the FilterOther section.
            dt = objRptParams.GetRptParamsForFilterOther(resID)
            'Get the comparison operators
            dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("Operators")
            'Get the input masks

            'Loop through the rows in the dt and add each parameter. 
            For Each dr In dt.Rows
                'Special case - Modified by Michelle Rodriguez on 11/18/2004.
                'We do not need to create a row for the StuEnrollId field.
                'Since this is a special page for the Student Transcript, there are several textboxs to hold 
                'the student name, program version, StuEnrollId, academic year and term. 
                'Only the student name and program version are visible. 
                'Internally, we only need to StuEnrollId to compute the report.
                'In case we might need other filters, I have left this section of code below. However, we need
                'to exclude the StuEnrollId parameter from it because there is already an invisible textbox 
                'that will hold the value depending on the user selection thru the Student Search pop-up.
                'StuEnrollId is RptParamId=127 and FldId=636
                If Not (dr("RptParamId") = 127 And dr("FldId") = 636) Then
                    'Add a row to the tblOthers table. This is a server control.
                    Dim r As New TableRow
                    Dim c0 As New TableCell
                    Dim c1 As New TableCell
                    Dim c2 As New TableCell
                    Dim c3 As TableCell
                    Dim c4 As TableCell
                    Dim c5 As New TableCell
                    Dim lbl As New Label
                    Dim ddl As New DropDownList
                    Dim txt As New TextBox
                    Dim txt2 As New TextBox
                    Dim fldType As String = dr("FldType").ToString
                    Dim regExpVal As RegularExpressionValidator
                    Dim reqFldVal As RequiredFieldValidator

                    txt.ID = "txt" & dr("RptParamId").ToString()
                    txt.CssClass = "textbox"
                    txt.Width = Unit.Pixel(120)
                    txt.ClientIDMode = ClientIDMode.Static


                    txt2.ID = "txt2" & dr("RptParamId").ToString()
                    txt2.CssClass = "textbox"           '"ToTheme2"
                    txt2.Width = Unit.Pixel(120)
                    txt2.Visible = False
                    txt2.ClientIDMode = ClientIDMode.Static


                    lbl.Text = CType(dr("Caption"), String)
                    lbl.CssClass = "label"
                    c0.Controls.Add(lbl)

                    ddl.ID = "ddl" & dr("RptParamId").ToString()
                    'ddl.DataSource = dt2
                    'Populate dropdownlist with operators relevant to the diferent data types.
                    If (ViewState("resid") = 590 And dr("FldId") = 1047) Or (ViewState("resid") = 238 And dr("FldId") = 65) Then
                        'The Attendance History report need a date range; therefore, the operators drowdownlist can only contain one value.
                        'Between operator
                        ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                        txt2.Visible = True
                        'ElseIf (ViewState("resid") = 590 And dr("FldId") = 1105) Then
                        '    ddl.DataSource = dt2
                        '    txt2.Visible = False
                        '    txt.Visible = False
                    ElseIf (ViewState("resid") = 582 And (dr("Fldid") = 1116 Or dr("Fldid") = 1107)) Then
                        'The report Individual SAP Report need a date range; therefore, the operators drowdownlist can only contain one value.
                        'Between operator
                        ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                        txt2.Visible = True
                    ElseIf (ViewState("resid") = 602 And dr("Fldid") = 654) Then
                        'The report Dropped Courses for single student need a Drop date range; therefore, the operators drowdownlist can only contain one value.
                        'Between operator
                        ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                        txt2.Visible = True
                    ElseIf (ViewState("resid") = 590 And dr("FldId") = 1105) Then
                        'dtOperators = createStartDayTable()
                        'dtOperators.TableName = "Operators1"
                        'ds.Tables.Add(dtOperators)
                        ddl.DataSource = createStartDayTable()
                        txt.Visible = False
                        txt2.Visible = False
                    Else

                        Select Case dr("FldType")
                            Case "Datetime"
                                ddl.DataSource = New DataView(dt2, "AllowDateTime = 1", "", DataViewRowState.OriginalRows)

                            Case "Varchar", "Char"
                                ddl.DataSource = New DataView(dt2, "AllowString = 1", "", DataViewRowState.OriginalRows)

                            Case "Smallint", "Int", "TinyInt", "Float", "Decimal"
                                ddl.DataSource = New DataView(dt2, "AllowNumber = 1", "", DataViewRowState.OriginalRows)

                            Case Else
                                ddl.DataSource = dt2
                        End Select
                    End If
                    ddl.DataTextField = "CompOpText"
                    ddl.DataValueField = "CompOpId"
                    ddl.DataBind()
                    ddl.AutoPostBack = True
                    ddl.CssClass = "dropdownlist"
                    c1.Controls.Add(ddl)
                    c2.Controls.Add(txt)
                    c5.Controls.Add(txt2)

                    r.Cells.Add(c0)
                    r.Cells.Add(c1)
                    r.Cells.Add(c2)
                    r.Cells.Add(c5)

                    If fldType = "Datetime" Then
                        'Add a regular expression validator for Datetime fields.
                        'We can only accept dates with two slashes.
                        regExpVal = New RegularExpressionValidator
                        regExpVal.ID = "regExpVal" & dr("RptParamId").ToString()
                        regExpVal.ControlToValidate = txt.ID
                        regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                        'regExpVal.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                        regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                        '& vbCr & "The character required to separate values is the semicolon (;)."
                        regExpVal.Display = ValidatorDisplay.None
                        c3 = New TableCell
                        c3.Controls.Add(regExpVal)
                        '
                        regExpVal = New RegularExpressionValidator
                        regExpVal.ID = "regExpVal2" & dr("RptParamId").ToString()
                        regExpVal.ControlToValidate = txt2.ID
                        regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                        'regExpVal.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                        regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                        '& vbCr & "The character required to separate values is the semicolon (;)."
                        regExpVal.Display = ValidatorDisplay.None
                        c3.Controls.Add(regExpVal)
                        r.Cells.Add(c3)
                    End If

                    If dr("Required") = "True" Then
                        'Add a required field validator if this field is marked as 'Required'.
                        reqFldVal = New RequiredFieldValidator
                        reqFldVal.ID = "reqFldVal" & dr("RptParamId").ToString()
                        reqFldVal.ControlToValidate = txt.ID
                        reqFldVal.ErrorMessage = CType((dr("Caption") & " is a required filter."), String)
                        reqFldVal.Display = ValidatorDisplay.None
                        c4 = New TableCell
                        c4.Controls.Add(reqFldVal)
                        r.Cells.Add(c4)
                    End If

                    tblfilterothers.Rows.Add(r)
                End If
            Next

            'Add a hidden textbox and a compare validator for fields marked as 'Required'.
            'This textbox need to be updated everytime it is selected or deselected.
            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
            Dim rows() As DataRow = dt.Select("FilterListSec = 1 And Required = 1")

            For Each dr In rows
                Dim txt As New TextBox
                Dim compVal As New CompareValidator

                txt.ID = "txt" & dr("RptParamId").ToString()
                txt.CssClass = "totheme2"
                txt.Text = Guid.Empty.ToString
                txt.Width = Unit.Pixel(0)

                compVal.ID = "compVal" & dr("RptParamId").ToString()
                compVal.ControlToValidate = txt.ID
                compVal.[Operator] = ValidationCompareOperator.NotEqual
                compVal.ValueToCompare = Guid.Empty.ToString
                compVal.Type = ValidationDataType.String
                compVal.ErrorMessage = CType((dr("Caption") & " is a required filter."), String)
                compVal.Display = ValidatorDisplay.None

                pnlRequiredFieldValidators.Controls.Add(txt)
                pnlRequiredFieldValidators.Controls.Add(compVal)
            Next
        End Sub

        Private Sub BindDataListFromDB()
            Dim dt As DataTable
            Dim objRptParams As New RptParamsFacade

            dt = objRptParams.GetSavedPrefsList

            With dlstprefs
                .DataSource = dt
                .DataBind()
            End With

            'Cache the dt so we can reuse it when the links are clicked.
            'There is no need to have to go to the database to repopulate the datalist
            'when a different preference is selected.
            Cache("Prefs") = dt
        End Sub

        Private Sub BindDataListFromCache()
            Dim dt As DataTable
            dt = DirectCast(Cache("Prefs"), DataTable)
            With dlstprefs
                .DataSource = dt
                .DataBind()
            End With
        End Sub

        Private Sub dlstPrefs_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstprefs.ItemCommand
            Dim prefId As String
            Dim dt, dt2 As DataTable
            Dim dr, dr2 As DataRow
            Dim objRptParams As New RptParamsFacade
            Dim ds As DataSet
            Dim rptParamId As Integer
            Dim dRows As DataRow()
            Dim iCounter As Integer
            Dim objCommon As New FAME.Common.CommonUtilities
            Dim ctlTextBox As Control
            Dim ctlDDL As Control
            Dim found As Boolean

            dlstprefs.SelectedIndex = e.Item.ItemIndex

            If Not IsNothing(Cache("Prefs")) Then
                BindDataListFromCache()
            Else
                BindDataListFromDB()
            End If

            'Clear the relevant sections
            Clear()

            'There is no need to bring the preference name from the database.
            txtpreferencename.Text = DirectCast(e.CommandSource, LinkButton).Text         'e.CommandArgument



            'Get the DataSet containing the preferences
            prefId = dlstprefs.DataKeys(e.Item.ItemIndex).ToString()
            ds = objRptParams.GetUserRptPrefs(prefId)

            'Set txtPrefId to the preference id of the selected item
            txtprefid.Value = prefId
            'This section deals with displaying the Sort Preferences.
            dt = ds.Tables("SortPrefs")

            'Get the datatable from session that contains the details of each rptparam
            dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
            With dt2
                .PrimaryKey = New DataColumn() { .Columns("RptParamId")}
            End With

            'We need to get the captions for the rows in the SortPrefs dt
            For Each dr In dt.Rows
                dr2 = dt2.Rows.Find(dr("RptParamId"))
                dr("Caption") = dr2("Caption")
            Next

            With lbxselsort
                .DataSource = dt
                .DataTextField = "Caption"
                .DataValueField = "RptParamId"
                .DataBind()
            End With

            'When the page is first loaded we populate the lbxSort control with all the
            'sort parameters for the report. When a preference is selected we need to
            'make certain that the lbxSort control only displays the sort parameters that
            'are not already selected.
            If lbxsort.Items.Count > 0 Then
                lbxsort.Items.Clear()
            End If

            For Each dr In dt2.Rows
                If dr("SortSec") = True Then
                    found = False
                    For iCounter = 0 To lbxselsort.Items.Count - 1
                        If lbxselsort.Items(iCounter).Value = dr("RptParamId") Then
                            found = True
                        End If
                    Next
                    If found = False Then
                        lbxsort.Items.Add(New ListItem(CType(dr("Caption"), String), CType(dr("RptParamId"), String)))
                    End If
                End If
            Next

            'This section deals with storing and selecting the relevant FilterList prefs.
            'Set to empty GUID all hidden textboxes in pnlRequiredFieldValidators panel.
            For Each ctrl As Control In pnlRequiredFieldValidators.Controls
                Dim tBox As TextBox = TryCast(ctrl, TextBox)
                If (tBox IsNot Nothing) Then
                    tBox.Text = Guid.Empty.ToString
                End If
            Next

            dt = ds.Tables("FilterListPrefs")
            dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")

            For Each dr In dt.Rows
                dr2 = dt2.NewRow
                dr2("RptParamId") = dr("RptParamId")
                dr2("FldValue") = dr("FldValue")
                dt2.Rows.Add(dr2)
                '
                ctlTextBox = pnlRequiredFieldValidators.FindControl("txt" & dr("RptParamId").ToString)
                If Not (ctlTextBox Is Nothing) Then
                    If DirectCast(ctlTextBox, TextBox).Text = Guid.Empty.ToString Then
                        DirectCast(ctlTextBox, TextBox).Text = CType(dr("FldValue"), String)
                    Else
                        DirectCast(ctlTextBox, TextBox).Text &= CType(("," & dr("FldValue")), String)
                    End If
                End If
            Next

            'If an item is selected in the lbxFilterList listbox then we need to select
            'the values, if any, that the user had selected for that item.
            If lbxfilterlist.SelectedIndex <> -1 Then
                rptParamId = CType(lbxfilterlist.SelectedItem.Value, Integer)
                'Search the FilterListSelections dt to see if the user had selected any
                'values for this item.
                dRows = dt2.Select("RptParamId = " & rptParamId)
                If dRows.Length > 0 Then
                    For Each dr In dRows
                        'Loop through and select relevant item in the lbxSelFilterList control.
                        For iCounter = 0 To lbxselfilterlist.Items.Count - 1
                            If lbxselfilterlist.Items(iCounter).Value.ToString = dr("FldValue").ToString() Then
                                lbxselfilterlist.Items(iCounter).Selected = True
                            End If
                        Next
                    Next
                End If

            End If

            'This section deals with the FilterOther prefs.
            dt = ds.Tables("FilterOtherPrefs")
            If dt.Rows.Count > 0 Then
                pnl4.Visible = True
                'For each row we need to select the appropriate entry in the ddl and enter the appropriate
                'value in the textbox.
                For Each dr In dt.Rows
                    If dr("RptParamId") = 127 Then
                        'Special case.
                        'Need to get StudentName and PrgVerDescription associated to StuEnrollId.
                        txtstuenrollmentid.Value = CType(dr("OpValue"), String)
                        If CommonWebUtilities.IsValidGuid(txtstuenrollmentid.Value) Then
                            Dim rptUtils As New ReportCommonUtilsFacade
                            Dim stuDt As DataTable
                            stuDt = rptUtils.GetStuNameAndPrgVersion(txtstuenrollmentid.Value)
                            If stuDt.Rows.Count > 0 Then
                                txtStudentId.Text = CType(stuDt.Rows(0)("StudentName"), String)
                                txtstuenrollment.Text = CType(stuDt.Rows(0)("PrgVerDescrip"), String)

                            End If
                        End If
                    Else
                        '
                        ctlDDL = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("ddl" & dr("RptParamId").ToString())
                        ''Added by saraswathi to avaoid error page when using between in saving the report
                        ''Added on july 10 2009
                        If Not ctlDDL Is Nothing Then
                            objCommon.SelValInDDL(DirectCast(ctlDDL, DropDownList), dr("OpId").ToString())
                            ctlTextBox = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt" & dr("RptParamId").ToString())
                            If dr("OpId").ToString() = AdvantageCommonValues.BetweenOperatorValue.ToString Then
                                'Special case: When the operator is Between
                                Dim strSplit() As String = dr("OpValue").ToString.Split(CType(";", Char))
                                If strSplit.GetLength(0) > 1 Then
                                    DirectCast(ctlTextBox, TextBox).Text = strSplit(0)
                                    Dim ctlTextBox2 As Control = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt2" & dr("RptParamId").ToString())
                                    DirectCast(ctlTextBox2, TextBox).Text = strSplit(1)
                                ElseIf strSplit.GetLength(0) = 1 Then
                                    DirectCast(ctlTextBox, TextBox).Text = strSplit(0)
                                End If
                            Else
                                DirectCast(ctlTextBox, TextBox).Text = CType(dr("OpValue"), String)
                            End If
                        End If
                    End If
                Next

            End If

            'Set Mode to EDIT
            'SetMode("EDIT")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"

            '   set Style to Selected Item
            CommonWebUtilities.SetStyleToSelectedItem(dlstprefs, txtprefid.Value, ViewState)
        End Sub

        Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnadd.Click
            'The code in this sub should only execute if an item is selected
            'in the lbxSort listbox.
            If lbxsort.SelectedIndex <> -1 Then
                lbxselsort.Items.Add(New ListItem(lbxsort.SelectedItem.Text, lbxsort.SelectedItem.Value))
                lbxsort.Items.RemoveAt(lbxsort.SelectedIndex)
                If lbxsort.Items.Count > 0 Then
                    lbxsort.SelectedIndex = lbxsort.SelectedIndex + 1
                End If
            End If
        End Sub

        Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnremove.Click
            'The code in this sub should only execute if an item is selected in 
            'the lbxSelSort listbox.
            If lbxselsort.SelectedIndex <> -1 Then
                lbxsort.Items.Add(New ListItem(lbxselsort.SelectedItem.Text, lbxselsort.SelectedItem.Value))
                lbxselsort.Items.RemoveAt(lbxselsort.SelectedIndex)
            End If
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnSave.Click
            SavePreferences()
        End Sub

        Private Function ValidateFilterOthers() As Boolean
            Dim selOpId As Integer
            Dim rCounter As Integer
            Dim tbxText As String
            Dim tbxText2 As String
            Dim strItem As String
            Dim arrValues() As String
            Dim selIndex As String
            Dim errorMessage As String = ""
            Dim opName As String
            Dim fieldName As String
            Dim fieldId As Integer
            Dim fldMask As String
            Dim fldLen As Integer
            Dim dType As String
            Dim rptParamId As Integer
            Dim objcommon As New FAME.Common.CommonUtilities

            'We need to loop through the entries in the tblFilterOthers table.
            For rCounter = 1 To tblfilterothers.Rows.Count - 1
                tbxText = DirectCast(tblfilterothers.Rows(rCounter).Cells(2).Controls(0), TextBox).Text
                tbxText2 = DirectCast(tblfilterothers.Rows(rCounter).Cells(3).Controls(0), TextBox).Text
                'Ignore rows where nothing is entered into the textbox.
                If tbxText <> "" Then
                    arrValues = tbxText.Split(CType(";", Char))
                    selIndex = CType(DirectCast(tblfilterothers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedIndex, String)
                    opName = DirectCast(tblfilterothers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text
                    fieldName = DirectCast(tblfilterothers.Rows(rCounter).Cells(0).Controls(0), Label).Text
                    rptParamId = CType(DirectCast(tblfilterothers.Rows(rCounter).Cells(2).Controls(0), TextBox).ID.Substring(3), Integer)
                    fieldId = GetFldId(rptParamId)
                    fldMask = GetInputMask(fieldId)
                    fldLen = tbxText.Length

                    If selIndex >= 0 Then   'Make sure an entry is selected in the ddl
                        selOpId = CType(DirectCast(tblfilterothers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
                        If IsSingleValOp(selOpId) And arrValues.Length > 1 Then  'single value operator
                            'There should only be one entry in the textbox. There should be
                            'no semicolons (;) which is used to separate multiple values such as
                            'when using the BETWEEN or the IN LIST operators.
                            errorMessage &= "Operator " & opName & " cannot be used with multiple values" & vbCr
                        ElseIf opName = "Is Null" Then
                            'When the operator selected is "Is Null" then there is no point going
                            'any further since Is Null should not have a value specified.
                            errorMessage &= "You cannot specify a value when using the Is Null operator" & vbCr
                        ElseIf opName = "Between" And (tbxText = "" Or tbxText2 = "") Then
                            'Between must have exactly two values specified to be compared.
                            errorMessage &= "You must specify exactly two values when using the Between operator." & vbCr
                        Else
                            'It is okay to proceed with validating the entries.
                            'Loop through the entries in the arrValues array and validate.
                            For Each strItem In arrValues
                                'Verify the datatype of each entry in the arrValues array. We need to get the FldType from
                                'the RptParams dt stored in session.
                                dType = GetFldType(rptParamId)
                                If Not IsValidDataType(strItem, dType) Then
                                    errorMessage &= "Incorrect datatype entered for " & fieldName & vbCr
                                Else
                                    'Verify the input mask if there is one.
                                    If fldMask <> "" Then
                                        If Not objcommon.ValidateInputMask(fldMask, strItem) Then
                                            errorMessage &= "Incorrect format entered for " & fieldName & vbCr
                                        Else
                                            'Verify the field length if the field type is
                                            'Char or Varchar.
                                            If dType = "Varchar" Or dType = "Char" Then
                                                If fldLen > GetFldLen(rptParamId) Then
                                                    errorMessage &= fieldName & " cannot be more than " & GetFldLen(rptParamId).ToString & " characters" & vbCr
                                                End If
                                            End If
                                        End If  'Validate input mask
                                    End If  'Input mask exists
                                End If  'Valid data type
                            Next    'Loop through the items in the arrValues array
                        End If
                    End If  'Entry is selected in the ddl
                End If  'Textbox is not empty
            Next    'loop through the entries in the tblFilterOThers table
            If errorMessage = "" Then
                Return True
            Else
                'lblErrorMessage.Text = "Please review the following errors:" & errorMessage
                DisplayErrorMessage(errorMessage)
                Return False
            End If
        End Function

        Private Function IsSingleValOp(ByVal opId As Integer) As Boolean
            Select Case opId
                Case 5, 6
                    Return False
                Case Else
                    Return True
            End Select
        End Function

        Private Function IsValidDataType(ByVal sVal As String, ByVal dataType As String) As Boolean
            Dim iResult As Integer
            'Dim sResult As String
            Dim dResult As Decimal
            Dim fResult As Double
            Dim dtmResult As DateTime
            Dim cResult As Char
            Dim bResult As Byte

            Select Case dataType
                Case "Int"
                    Return Integer.TryParse(sVal, iResult)
                Case "Float"
                    Return Double.TryParse(sVal, fResult)
                Case "Money"
                    Return Decimal.TryParse(sVal, dResult)
                Case "Char"
                    Return Char.TryParse(sVal, cResult)
                Case "Datetime"
                    Return DateTime.TryParse(sVal, dtmResult)
                Case "Varchar"
                    If IsNothing(sVal) Then
                        Return False
                    End If
                    If sVal.GetType().IsValueType Then
                        Return True
                    End If
                    Return False
                Case "TinyInt"
                    Return Byte.TryParse(sVal, bResult)
                Case Else
                    Throw New Exception("Invalid Field Type: " & dataType)
            End Select
        End Function

        Private Function GetFldType(ByVal rptParamId As Integer) As String
            Dim dt As DataTable
            Dim drRows() As DataRow
            Dim fldType As String

            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
            drRows = dt.Select("RptParamId = " & rptParamId)
            fldType = CType(drRows(0)("FldType"), String)
            Return fldType
        End Function

        Private Function GetFldId(ByVal rptParamId As Integer) As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            Dim fldId As Integer

            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
            With dt
                .PrimaryKey = New DataColumn() { .Columns("RptParamId")}
            End With
            dr = dt.Rows.Find(rptParamId)
            fldId = CType(dr("FldId"), Integer)
            Return fldId
        End Function

        Private Function GetFldName(ByVal rptParamId As Integer) As String
            Dim dt As DataTable
            Dim drRows() As DataRow
            Dim fldName As String

            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
            drRows = dt.Select("RptParamId = " & rptParamId)
            fldName = CType(drRows(0)("FldName"), String)
            Return fldName
        End Function

        Private Function GetTblName(ByVal rptParamId As Integer) As String
            Dim dt As DataTable
            Dim drRows() As DataRow
            Dim tblName As String

            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
            drRows = dt.Select("RptParamId = " & rptParamId)
            tblName = CType(drRows(0)("TblName"), String)
            Return tblName
        End Function

        Private Function GetInputMask(ByVal fldId As Integer) As String
            Dim dt As DataTable
            Dim dr As DataRow
            Dim sMask As String

            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("InputMasks")
            dr = dt.Rows.Find(fldId)
            If dr Is Nothing Then
                sMask = ""
            Else
                sMask = CType(dr("Mask"), String)
            End If
            Return sMask
        End Function

        Private Function GetFldLen(ByVal rptParamId As Integer) As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            Dim fldLen As Integer

            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
            dr = dt.Rows.Find(rptParamId)
            fldLen = CType(dr("FldLen"), Integer)
            Return fldLen
        End Function

        Private Sub lbxFilterList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles lbxfilterlist.SelectedIndexChanged
            'When an item is selected we want to display the list of possible values for the item.
            'We can retrieve the list of values for this item from the FilterListValues stored
            'in the ds that is in session.
            Dim ds As DataSet
            Dim dt As DataTable
            Dim dr As DataRow
            Dim aRows As DataRow()
            Dim lbxRows As Integer
            Dim arrRows As Integer
            'Variables Added By Vijay Ramteke on May 13, 2009
            Dim fldName As String
            Dim cmpGrpSelectedRows As DataRow()
            Dim allRows As DataRow()
            'Variables Added By Vijay Ramteke on May 13, 2009
            ds = DirectCast(Session("WorkingDS"), DataSet)
            ViewState("boolShowErr") = True
            dt = ds.Tables("FilterListValues")
            'New Code Added By Vijay Ramteke on May 13, 2009
            allRows = dt.Select("FldName='CampGrpId' and DisplayFld='All'")
            If allRows.Length = 0 Then
            Else
            End If
            'New Code Added By Vijay Ramteke on May 13, 2009
            'If 'None' item was selected, we need to empty the FilterListSelections dt in session.
            'Otherwise, we want to get a list of all the rows in dt that have the corresponding RptParamId
            'that is selected in the lbxFilterList listbox.

            If lbxfilterlist.SelectedItem.Value = 0 Then
                'Clear the items from the lbxSelFilterList lisbox if there are any.
                If lbxselfilterlist.Items.Count > 0 Then
                    lbxselfilterlist.Items.Clear()
                End If
                'If the FilterListSelections dt in session is not empty then we need to empty it.
                dt = ds.Tables("FilterListSelections")
                If dt.Rows.Count > 0 Then
                    dt.Rows.Clear() 'Empty dt table.
                End If
                'Set to empty GUID all textboxes in panel pnlRequiredFieldValidators.
                For Each ctrl As Control In pnlRequiredFieldValidators.Controls
                    Dim box As TextBox = TryCast(ctrl, TextBox)
                    If (box IsNot Nothing) Then
                        box.Text = Guid.Empty.ToString
                    End If
                Next

            Else
                'We want to get a list of all the rows in dt that have the corresponding RptParamId
                'that is selected in the lbxFilterList listbox.
                aRows = dt.Select("RptParamId = " & lbxfilterlist.SelectedItem.Value)
                fldName = aRows(0)(3).ToString
                If fldName.ToLower <> "campgrpid" Then
                    cmpGrpSelectedRows = ds.Tables("FilterListSelections").Select("FldName='CampGrpId'")
                    If cmpGrpSelectedRows.Length > 0 Then
                    Else
                        If Not ResourceId = 238 And Not ResourceId = 537 Then
                            lbxfilterlist.Items.FindByValue(lbxfilterlist.SelectedValue).Selected = False
                            lbxfilterlist.Items.FindByText("Campus Group").Selected = True
                        End If
                        If lbxselfilterlist.Items.Count = 0 Then
                            aRows = dt.Select("RptParamId = " & lbxfilterlist.SelectedItem.Value)
                            For Each dr In aRows
                                lbxselfilterlist.Items.Add(New ListItem(CType(dr("DisplayFld"), String), CType(dr("ValueFld"), String)))
                            Next
                            lbxselfilterlist.Items.Insert(0, "None")
                        End If
                        If Not ResourceId = 238 And Not ResourceId = 537 Then
                            DisplayErrorMessage("Please first select the campus group")
                            Exit Sub
                        End If
                    End If
                End If
                'Clear the items from the lbxSelFilterList lisbox if there are any.
                If lbxselfilterlist.Items.Count > 0 Then
                    lbxselfilterlist.Items.Clear()
                End If
                'Add the rows from aRows to the lbxSelFilterList listbox.
                For Each dr In aRows
                    lbxselfilterlist.Items.Add(New ListItem(CType(dr("DisplayFld"), String), CType(dr("ValueFld"), String)))
                Next
                'Add a 'None' item so that users can deselect an item without having to select another one.
                lbxselfilterlist.Items.Insert(0, "None")

                'If the FilterListSelections dt in session is not empty then we need
                'to see if it has any entries for the item selected here. If it does then
                'we need to select the relevant entries in the lbxSelFilterList listbox.
                dt = ds.Tables("FilterListSelections")

                If dt.Rows.Count > 0 Then
                    aRows = dt.Select("RptParamId = " & lbxfilterlist.SelectedItem.Value)
                    If aRows.Length > 0 Then
                        'Loop through the items in the lbxSelFilterList listbox and
                        'select the relevant items.
                        For arrRows = 0 To aRows.Length - 1
                            For lbxRows = 0 To lbxselfilterlist.Items.Count - 1
                                If lbxselfilterlist.Items(lbxRows).Value.ToString = aRows(arrRows)("FldValue").ToString() Then
                                    lbxselfilterlist.Items(lbxRows).Selected = True
                                End If
                            Next
                        Next
                    End If 'search did not return 0 rows
                End If 'dt is not empty
            End If ''None' item was selected
        End Sub

        Private Sub lbxSelFilterList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As EventArgs) Handles lbxselfilterlist.SelectedIndexChanged

            '' New Code Added By Vijay Ramteke On November 04, 2010 For Rally Id DE1218
            If ResourceId = 602 Then
                If txtStudentId.Text <> "" And txtstuenrollment.Text <> "" Then
                    DisplayErrorMessage("You need to clear the Student Enrollment in order to use the filters.")
                    For j As Integer = 0 To lbxfilterlist.Items.Count - 1
                        lbxfilterlist.Items(j).Selected = False
                    Next
                    Exit Sub
                End If
            End If

            '' New Code Added By Vijay Ramteke On November 04, 2010 For Rally Id DE1218
            'When this event is fired it is important to remember that it could be that
            'the user has selected OR deselected a value. Also, this lisbox supports multiple
            'selections so we cannot simply get the item selected or unselected.
            'Each time this event fires we have to examine each item in the listbox. If the
            'item is not selected then if it exists in the dt we will have to remove it. If
            'the item is selected but not exists in the dt then we will have to add it.
            Dim i As Integer
            Dim ds As DataSet
            Dim dt As DataTable
            Dim dt2 As DataTable
            Dim dr2 As DataRow()
            Dim dr As DataRow
            Dim paramId As Integer
            Dim selValue As String
            Dim temp As String
            Dim ctlTextBox As Control
            Dim rows() As DataRow
            'Parameter Added By Vijay Ramteke on May 13,2009
            Dim fldName As String
            '    Dim selText As String
            'Parameter Added By Vijay Ramteke on May 13,2009

            ds = DirectCast(Session("WorkingDS"), DataSet)
            dt = ds.Tables("FilterListSelections")
            dt2 = ds.Tables("RptParams")
            paramId = CType(lbxfilterlist.SelectedItem.Value, Integer)
            fldName = CType(dt2.Select("RptParamId = " & paramId)(0)("FldName"), String)
            If fldName.ToLower = "campgrpid" Then
                dr2 = dt.Select("FldName <> 'CampGrpId'")
                For idx As Integer = 0 To dr2.Length - 1
                    dt.Rows.Remove(dr2(idx))
                Next
            End If
            For i = 0 To lbxselfilterlist.Items.Count - 1
                selValue = lbxselfilterlist.Items(i).Value
                If lbxselfilterlist.Items(i).Selected = False And i > 0 Then
                    If ItemExistsInFilterListSelections(i) Then
                        'We need to remove the item from the dt
                        Dim objCriteria() As Object = {paramId, selValue}
                        dr = dt.Rows.Find(objCriteria)
                        dt.Rows.Remove(dr)
                        'If field is 'Required', set hidden textbox to empty.
                        rows = dt2.Select("RptParamId = " & paramId & " AND Required = 1")
                        If rows.GetLength(0) = 1 Then
                            ctlTextBox = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt" & paramId)
                            If DirectCast(ctlTextBox, TextBox).Text.IndexOf(",", StringComparison.Ordinal) = -1 Then
                                'Only one entry.
                                DirectCast(ctlTextBox, TextBox).Text = Guid.Empty.ToString
                            Else
                                temp = DirectCast(ctlTextBox, TextBox).Text
                                temp = temp.Replace(selValue, ",")
                                If temp.StartsWith(",,") Then
                                    DirectCast(ctlTextBox, TextBox).Text = temp.Substring(2)
                                ElseIf temp.EndsWith(",,") Then
                                    DirectCast(ctlTextBox, TextBox).Text = temp.Substring(0, temp.Length - 2)
                                Else
                                    DirectCast(ctlTextBox, TextBox).Text = temp.Replace(",,,", ",")
                                End If
                            End If
                        End If
                    End If
                Else
                    If Not ItemExistsInFilterListSelections(i) And i > 0 Then
                        'We need to add the item to the dt
                        dr = dt.NewRow
                        dr("RptParamId") = paramId      'lbxFilterList.SelectedItem.Value
                        dr("FldValue") = selValue       'lbxSelFilterList.Items(i).Value
                        dr("FldName") = fldName
                        dt.Rows.Add(dr)
                    End If
                End If

            Next
            Session("WorkingDS") = ds
        End Sub

        Private Function ItemExistsInFilterListSelections(ByVal itemIndex As Integer) As Boolean
            '     Dim i As Integer
            Dim ds As DataSet
            Dim dt As DataTable
            Dim dr As DataRow
            Dim paramId As Integer
            Dim selValue As String

            paramId = CType(lbxfilterlist.SelectedItem.Value, Integer)
            selValue = lbxselfilterlist.Items(itemIndex).Value

            ds = DirectCast(Session("WorkingDS"), DataSet)
            dt = ds.Tables("FilterListSelections")

            If dt.Rows.Count = 0 Then
                Return False
            Else
                Dim objCriteria() As Object = {paramId, selValue}
                dr = dt.Rows.Find(objCriteria)
                If dr Is Nothing Then
                    Return False
                Else
                    Return True
                End If
            End If
        End Function

        Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnNew.Click
            Dim objCommon As New FAME.Common.CommonUtilities
            Dim dt As DataTable
            Dim dr As DataRow

            Try
                Clear()
                'SetMode("NEW")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                'Place the prefid guid into the txtPrefId textbox so it can be reused
                'if we need to do an update later on.
                txtprefid.Value = ""

                'We have to rebind the lbxSort control to the report parameters that are 
                'stored in session
                lbxsort.Items.Clear()
                dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
                For Each dr In dt.Rows
                    If dr("SortSec") = True Then
                        lbxsort.Items.Add(New ListItem(CType(dr("Caption"), String), CType(dr("RptParamId"), String)))
                    End If
                Next

                'Make sure no item is selected in the Preferences datalist.
                dlstprefs.SelectedIndex = -1

                'Rebind the Preferences datalist from the database.
                BindDataListFromDB()

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End Sub

        Private Sub Clear()
            Dim dt As DataTable
            Dim trCounter As Integer
            Dim ctrlDDL As Control
            Dim objCommon As New FAME.Common.CommonUtilities

            'Clear the txtPrefName textbox control.
            txtpreferencename.Text = ""
            'Clear the lbxSelSort listbox control
            lbxselsort.Items.Clear()
            'Make sure no item is selected in the lbxSort listbox.
            lbxsort.SelectedIndex = -1
            'Clear the entries from the FilterListSelections dt
            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")
            If dt.Rows.Count > 0 Then
                dt.Clear()
            End If
            'Make sure no entry is selected in the lbxSelFilterList listbox
            lbxselfilterlist.SelectedIndex = -1
            'Get operators table
            Dim dt2 As DataTable = DirectCast(Session("WorkingDS"), DataSet).Tables("Operators")
            'Clear the textboxes in the FilterOther section and set dropdownlists to 'Equal To'.
            ClearStuEnrollmentFields()
            For trCounter = 1 To tblfilterothers.Rows.Count - 1 'The first row is empty
                DirectCast(tblfilterothers.Rows(trCounter).Cells(2).Controls(0), TextBox).Text = ""
                DirectCast(tblfilterothers.Rows(trCounter).Cells(3).Controls(0), TextBox).Text = ""
                ctrlDDL = DirectCast(tblfilterothers.Rows(trCounter).Cells(1).Controls(0), DropDownList)
                objCommon.SelValInDDL(DirectCast(ctrlDDL, DropDownList), dt2.Rows(0)("CompOpId").ToString)
            Next
            'Set to empty GUID all hidden textboxes in pnlRequiredFieldValidators panel.
            For Each ctrl As Control In pnlRequiredFieldValidators.Controls
                Dim box As TextBox = TryCast(ctrl, TextBox)
                If (box IsNot Nothing) Then
                    box.Text = Guid.Empty.ToString
                End If
            Next
        End Sub

        Private Sub ClearStuEnrollmentFields()
            txtStudentId.Text = ""
            txtstuenrollmentid.Value = ""
            txtstuenrollment.Text = ""
            txttermid.Value = ""
            txtterm.Value = ""
            txtacademicyearid.Value = ""
            txtacademicyear.Value = ""
        End Sub

        Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnDelete.Click
            Dim objRptParams As New RptParamsFacade
            Dim objCommon As New FAME.Common.CommonUtilities

            'Delete all the preference info from the datatbase
            objRptParams.DeleteUserPrefInfo(txtprefid.Value)
            'Clear the rhs of the screen
            Clear()
            'Rebind the datalist from the database
            BindDataListFromDB()
            'Set mode to NEW
            'SetMode("NEW")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Place the prefid guid into the txtPrefId textbox so it can be reused
            'if we need to do an update later on.
            txtprefid.Value = ""
            'Make sure no item is selected in the datalist
            dlstprefs.SelectedIndex = -1
        End Sub

        Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles btnsearch.Click
            Validate()
            ExecuteReport(False)
        End Sub

        Private Function BuildOrderBy(ByRef strOClause As String) As String
            Dim i As Integer
            Dim oClause As String = ""
            Dim tempStringOClause As String = ""
            Dim fldName As String
            Dim tblName As String
            Dim fldCaption As String

            'Only execute if at least one sort param has been specified
            If lbxselsort.Items.Count > 0 Then
                For i = 0 To lbxselsort.Items.Count - 1
                    tblName = GetTblName(CType(lbxselsort.Items(i).Value, Integer))
                    fldName = GetFldName(CType(lbxselsort.Items(i).Value, Integer))
                    fldCaption = lbxselsort.Items(i).Text
                    If i < lbxselsort.Items.Count - 1 Then
                        oClause &= tblName & "." & fldName & ","
                        tempStringOClause &= fldCaption & ", "
                    Else
                        oClause &= tblName & "." & fldName
                        tempStringOClause &= fldCaption
                    End If
                Next
            End If

            'oClause = "ORDER BY " & oClause
            strOClause = tempStringOClause
            Return oClause
        End Function

        Private Function BuildFilterListClause(ByRef strWClause As String) As String
            Dim dt As DataTable
            Dim dt2 As DataTable
            Dim i, j, k As Integer
            Dim drRows() As DataRow
            Dim rows() As DataRow
            Dim fldName As String
            Dim tblName As String
            Dim fldType As String
            Dim fldCaption As String
            Dim [operator] As String
            Dim filterValue As String
            Dim tempWClause As String = ""
            Dim tempStringWClause As String = ""
            Dim stringWClause As String = ""
            Dim strTempOperator As String
            Dim wClause As String = ""

            'We need to loop through the items in the lbxFilterList control and see
            'if the item has any entries in the FilterListSelections dt stored in session.
            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")
            'We need to get the DisplayFld of all selected items.
            dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListValues")

            If dt.Rows.Count > 0 Then
                For i = 0 To lbxfilterlist.Items.Count - 1
                    drRows = dt.Select("RptParamId = " & lbxfilterlist.Items(i).Value)
                    If drRows.Length > 0 Then 'Match found

                        tblName = GetTblName(CType(lbxfilterlist.Items(i).Value, Integer))
                        fldName = GetFldName(CType(lbxfilterlist.Items(i).Value, Integer))
                        fldType = GetFldType(CType(lbxfilterlist.Items(i).Value, Integer))
                        fldCaption = lbxfilterlist.Items(i).Text

                        If drRows.Length > 1 Then
                            [operator] = " IN "
                            strTempOperator = " In List "
                        Else
                            [operator] = " = "
                            strTempOperator = " Equal To "
                        End If

                        For j = 0 To drRows.Length - 1
                            filterValue = BuildFilterValue(drRows(j)("FldValue").ToString(), fldType)
                            If j < drRows.Length - 1 Then
                                tempWClause &= filterValue & ","
                            Else
                                tempWClause &= filterValue
                            End If
                            'Get the DisplayFld
                            rows = dt2.Select("RptParamId = " & drRows(j)("RptParamId").ToString() & " AND " &
                                              "ValueFld = '" & drRows(j)("FldValue").ToString() & "'")
                            If rows.GetLength(0) = 1 Then
                                If j < drRows.Length - 1 Then
                                    tempStringWClause &= CType(("'" & rows(0)("DisplayFld") & "', "), String)
                                Else
                                    If strTempOperator = " Equal To " Then
                                        tempStringWClause &= CType(rows(0)("DisplayFld"), String)
                                    Else
                                        tempStringWClause &= CType(("'" & rows(0)("DisplayFld") & "'"), String)
                                    End If
                                End If
                            End If
                        Next

                        If k > 0 Then
                            wClause &= " AND "
                            stringWClause &= " AND "
                        End If

                        If [operator] = " = " Then
                            wClause &= tblName & "." & fldName & [operator] & tempWClause
                            stringWClause &= fldCaption & strTempOperator & tempStringWClause
                        Else
                            wClause &= tblName & "." & fldName & [operator] & "(" & tempWClause & ")"
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                        End If

                        k += 1 'Match found so we need to increment the count of matches so far
                    End If
                    tempWClause = ""
                    tempStringWClause = ""
                Next
            End If

            strWClause = stringWClause
            Return wClause
        End Function

        Private Function BuildFilterOtherClause(ByRef strWClause As String, ByRef strErrorMsg As String) As String
            Dim fldName As String
            Dim tblName As String
            Dim fldType As String
            Dim fldCaption As String
            Dim sqlOperator As String
            Dim rCounter As Integer
            Dim opId As Integer
            Dim rptParamId As Integer
            Dim tbxText As String
            Dim tbxText2 As String
            Dim arrValues() As String
            Dim selText As String
            Dim wClause As String = ""
            Dim tempWClause As String = ""
            Dim inList As String = ""
            Dim strItem As String
            Dim position As Integer
            Dim errMsg As String = ""
            Dim tempFilterValue As String = ""
            Dim tempFilterValue2 As String
            'The following variables will hold the string representation of the Where Clause,
            'which will be shown in the report as part of the title.        
            Dim tempStringWClause As String = ""
            Dim stringWClause As String = ""
            'Special case.
            If ResourceId = 582 Then
                If txtStudentId.Text = "" And txtstuenrollment.Text = "" And lbxfilterlist.SelectedIndex = -1 Then
                    errMsg = "Required fields Campus Group and Program Version OR Student and Enrollment are missing."
                ElseIf txtStudentId.Text <> "" And txtstuenrollment.Text <> "" Then
                    tempStringWClause = "Student " & txtStudentId.Text & " AND " &
                                        "Enrollment " & txtstuenrollment.Text & ""
                    tempWClause = "arStuEnrollments.StuEnrollId = '" & txtstuenrollmentid.Value & "'"
                    wClause &= tempWClause & " AND "
                    stringWClause &= tempStringWClause & " AND "
                ElseIf lbxfilterlist.SelectedIndex <> -1 Then

                Else
                    errMsg = "Required fields Student and Enrollment are missing."
                End If
            ElseIf ResourceId = 238 Then 'Transcript Report (single Student)
                If txtStudentId.Text <> "" And txtstuenrollment.Text <> "" Then
                    tempStringWClause = "Student " & txtStudentId.Text & " AND " &
                                        "Enrollment " & txtstuenrollment.Text & ""
                    If (txtstuenrollmentid.Value = Guid.Empty.ToString()) Then
                        tempWClause = "arStuEnrollments.StudentId = '" & txtstudentguidid.Value & "'"
                    Else
                        tempWClause = "arStuEnrollments.StuEnrollId = '" & txtstuenrollmentid.Value & "'"
                    End If
                    wClause &= tempWClause & " AND "
                    stringWClause &= tempStringWClause & " AND "
                Else
                    errMsg = "Required fields Student and Enrollment are missing."
                End If

            ElseIf ResourceId = 602 Then
                '' Do Nothing
                '' New Code By Vijay Ramteke On November 04, 2010 For Rally Id DE1218
            Else
                '--
                'Special case.
                If txtStudentId.Text <> "" And txtstuenrollment.Text <> "" Then
                    tempStringWClause = "Student " & txtStudentId.Text & " AND " &
                                        "Enrollment " & txtstuenrollment.Text & ""
                    tempWClause = "arStuEnrollments.StuEnrollId = '" & txtstuenrollmentid.Value & "'"
                    wClause &= tempWClause & " AND "
                    stringWClause &= tempStringWClause & " AND "
                Else
                    errMsg = "Required fields Student and Enrollment are missing."
                End If
                '--
            End If
            '--
            'Special case for Progress Report
            If ResourceId = 537 Then
                If txttermstart.Text <> "" And txttermstart.Text <> "" Then
                    tempStringWClause = "Term Start Cutoff " & txttermstart.Text & ""
                    tempWClause = "arTerm.StartDate <= '" & txttermstart.Text & "'"
                    wClause &= tempWClause & " AND "
                    stringWClause &= tempStringWClause & " AND "
                End If
            End If

            rCounter = 1
            'We need to loop through the entries in the tblFilterOthers table.
            Do While (rCounter <= tblfilterothers.Rows.Count - 1) And errMsg = ""
                tbxText = DirectCast(tblfilterothers.Rows(rCounter).Cells(2).Controls(0), TextBox).Text
                tbxText2 = DirectCast(tblfilterothers.Rows(rCounter).Cells(3).Controls(0), TextBox).Text
                fldCaption = DirectCast(tblfilterothers.Rows(rCounter).Cells(0).Controls(0), Label).Text
                arrValues = tbxText.Split(CType(";", Char))
                opId = CType(DirectCast(tblfilterothers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
                sqlOperator = GetSQLOperator(opId)
                rptParamId = CType(DirectCast(tblfilterothers.Rows(rCounter).Cells(2).Controls(0), TextBox).ID.Substring(3), Integer)
                tblName = GetTblName(rptParamId)
                fldName = GetFldName(rptParamId)
                fldType = GetFldType(rptParamId)
                selText = DirectCast(tblfilterothers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text
                'Ignore rows where nothing is entered into the textbox.
                If tbxText <> "" Then

                    'Modified by Michelle R. Rodriguez on 11/22/2004.
                    'FilterValue needs to be validated to exclude invalid dates and replace single quotes with double quotes.
                    'Also, the number of operands is validated.

                    If IsSingleValOp(opId) And selText = "Like" Then
                        If arrValues.GetLength(0) = 1 Then
                            tempFilterValue = BuildFilterValue(arrValues(0), fldType)
                            If tempFilterValue <> "" Then
                                If tblName <> "syDerived" Then
                                    tempWClause = tblName & "." & fldName & " " & sqlOperator & "'%" & arrValues(0) & "%'"
                                Else
                                    tempWClause = fldName & sqlOperator & "'%" & arrValues(0) & "%'"
                                End If
                                tempStringWClause = fldCaption & " " & selText & " " & arrValues(0)
                            Else
                                errMsg = "Invalid " & fldCaption & " value to filter for."
                            End If
                        Else
                            'More than one input for a single operator.
                            errMsg = "Too many values to filter " & fldCaption & " for."
                        End If

                    ElseIf IsSingleValOp(opId) And selText <> "Empty" And selText <> "Is Null" Then
                        ' Modified by Michelle Rodriguez on 09/26/2004 and 10/04/2004.
                        ' Needed to include the time for filters that involved Datetime fields:
                        ' - The " = " operator will behave like a " BETWEEN " and the filter value
                        '   will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
                        ' - The " <> " operator will behave like a " NOT BETWEEN " and the filter value
                        '   will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
                        ' - Also, the filter value for the " > " operator will be of format: mm/dd/yyyy 11:59:59 PM
                        ' All other cases remain the same.
                        If arrValues.GetLength(0) = 1 Then
                            tempFilterValue = BuildFilterValue(arrValues(0), fldType, sqlOperator)
                            If tempFilterValue <> "" Then
                                tempStringWClause = fldCaption & " " & selText & " " & arrValues(0)
                                If tblName <> "syDerived" Then
                                    If fldType = "Datetime" Then
                                        If sqlOperator = " = " Then
                                            sqlOperator = " BETWEEN "
                                        ElseIf sqlOperator = " <> " Then
                                            sqlOperator = " NOT BETWEEN "
                                        End If
                                    End If
                                    tempWClause = tblName & "." & fldName & sqlOperator & tempFilterValue
                                Else
                                    If fldType = "Datetime" Then
                                        If sqlOperator = " = " Then
                                            sqlOperator = " BETWEEN "
                                        ElseIf sqlOperator = " <> " Then
                                            sqlOperator = " NOT BETWEEN "
                                        End If
                                    End If
                                    tempWClause = fldName & sqlOperator & tempFilterValue
                                End If
                            Else
                                errMsg = "Invalid " & fldCaption & " value to filter for."
                            End If
                        Else
                            'More than one input for a single operator.
                            errMsg = "Too many values to filter " & fldCaption & " for."
                        End If

                    ElseIf selText = "Between" Then
                        'If arrValues.GetLength(0) = 2 Then
                        'Only two values are accepted as valid input.
                        ''Modified By Saraswathi

                        If ResourceId = 590 Then

                            tempFilterValue = BuildFilterValue(tbxText, fldType)
                            tempFilterValue2 = BuildFilterValue(tbxText2, fldType)
                            'Vijay Ramteke on May, 5th 2009
                        ElseIf ResourceId = 238 Then
                            tempFilterValue = BuildFilterValue(tbxText, fldType)
                            tempFilterValue2 = BuildFilterValue(tbxText2, fldType)
                            'Vijay Ramteke on May, 5th 2009

                            'Code added by Priyanka on date 07th of May 2009
                            '--
                        ElseIf ResourceId = 582 Or ResourceId = 602 Then
                            tempFilterValue = BuildFilterValue(tbxText, fldType)
                            tempFilterValue2 = BuildFilterValue(tbxText2, fldType)
                        Else

                            tempFilterValue = BuildFilterValue(arrValues(0), fldType)
                            tempFilterValue2 = BuildFilterValue(arrValues(1), fldType)

                        End If
                        If tempFilterValue <> "" And tempFilterValue2 <> "" Then
                            If ResourceId = 590 Then
                                If tblName <> "syDerived" Then
                                    tempWClause = tblName & "." & fldName & sqlOperator & tempFilterValue & " AND " & Left(tempFilterValue2, tempFilterValue2.Length - 1) & " 11:59:59 PM'"
                                Else
                                    tempWClause = fldName & sqlOperator & tempFilterValue & " AND " & Left(tempFilterValue2, tempFilterValue2.Length - 1) & " 11:59:59 PM'"
                                End If
                                tempStringWClause = fldCaption & " " & selText & " (" & tbxText & " AND " & tbxText2 & ")"
                                'code added by Vijay Ramteke on date 7th May 2009
                            ElseIf ResourceId = 238 Then
                                tempWClause = "arClassSections.StartDate>= " & tempFilterValue & " AND arClassSections.EndDate<=" & tempFilterValue2
                                tempStringWClause = fldCaption & " " & selText & " (" & tbxText & " AND " & tbxText2 & ")"
                                'code added by Vijay Ramteke on date 7th May 2009

                                'code added by Priyanka on date 7th of May 2009
                                '--
                            ElseIf ResourceId = 582 Then
                                If tblName <> "syDerived" Then
                                    tempWClause = tblName & "." & fldName & sqlOperator & tempFilterValue & " AND " & Left(tempFilterValue2, tempFilterValue2.Length - 1) & " 11:59:59 PM'"
                                Else
                                    tempWClause = fldName & sqlOperator & tempFilterValue & " AND " & Left(tempFilterValue2, tempFilterValue2.Length - 1) & " 11:59:59 PM'"
                                End If
                                tempStringWClause = fldCaption & " " & selText & " (" & tbxText & " AND " & tbxText2 & ")"
                                '--code added by Priyanka on date 14th of May 2009
                            ElseIf ResourceId = 602 Then
                                tempWClause = "((arStuEnrollments.LDA Between " & tempFilterValue & " AND " & tempFilterValue2 & ") OR (arStuEnrollments.DateDetermined Between " & tempFilterValue & " AND " & tempFilterValue2 & "))"
                                tempStringWClause = fldCaption & " " & selText & " (" & tbxText & " AND " & tbxText2 & ")"

                            Else
                                If tblName <> "syDerived" Then
                                    tempWClause = tblName & "." & fldName & sqlOperator & tempFilterValue & " AND " & tempFilterValue2
                                Else
                                    tempWClause = fldName & sqlOperator & tempFilterValue & " AND " & tempFilterValue2
                                End If
                                tempStringWClause = fldCaption & " " & selText & " (" & arrValues(0) & " AND " & arrValues(1) & ")"
                            End If

                        Else
                            errMsg = "Too few values to filter " & fldCaption & " for." & vbCr & "You must specify exactly two values when using the Between operator."
                            'errMsg = "Invalid " & fldCaption & " value to filter for."
                        End If

                    ElseIf selText = "In List" Then
                        If arrValues.GetLength(0) > 1 Then
                            'More than one value in input.
                            For Each strItem In arrValues
                                If strItem <> "" Then
                                    tempFilterValue = BuildFilterValue(strItem, fldType)
                                    If tempFilterValue <> "" Then
                                        inList &= tempFilterValue & ","
                                    Else
                                        Exit For
                                    End If
                                End If
                            Next
                            If tempFilterValue <> "" Then
                                'Remove the last comma.
                                inList = inList.Substring(0, inList.Length - 1)

                                If tblName <> "syDerived" Then
                                    tempWClause = tblName & "." & fldName & sqlOperator & "(" & inList & ")"
                                Else
                                    tempWClause = fldName & sqlOperator & "(" & inList & ")"
                                End If
                                tempStringWClause = fldCaption & " " & selText & " (" & inList & ")"
                            Else
                                errMsg = "Invalid " & fldCaption & " value to filter for."
                            End If
                        Else
                            'Too few operands or invalid separation character.
                            errMsg = "Invalid separation character or too few values to filter " & fldCaption & " for." & vbCrLf &
                                     "The character required to separate values is the semicolon (;)."
                        End If
                    End If

                    wClause &= tempWClause & " AND "
                    stringWClause &= tempStringWClause & " AND "
                    inList = ""

                Else
                    'Textbox is empty
                    If tbxText2 <> "" And selText = "Between" Then
                        'Too few operands.
                        errMsg = "Too few values to filter " & fldCaption & " for." & vbCr & "You must specify exactly two values when using the Between operator."
                    End If

                End If

                'For the Is Null and Empty operator the textbox should be empty
                If IsSingleValOp(opId) And selText = "Empty" Then
                    If tblName <> "syDerived" Then
                        tempWClause = tblName & "." & fldName & sqlOperator
                    Else
                        tempWClause = fldName & sqlOperator
                    End If
                    tempStringWClause = fldCaption & " Is " & selText
                    wClause &= tempWClause & " AND "
                    stringWClause &= tempStringWClause & " AND "
                End If

                If IsSingleValOp(opId) And selText = "Is Null" Then
                    If tblName <> "syDerived" Then
                        tempWClause = tblName & "." & fldName & sqlOperator
                    Else
                        tempWClause = fldName & sqlOperator
                    End If
                    tempStringWClause = fldCaption & selText
                    wClause &= tempWClause & " AND "
                    stringWClause &= tempStringWClause & " AND "
                End If

                If ResourceId = 590 And tblName.ToString.ToLower = "arStudentClockAttendance".ToLower And fldName.ToString.ToLower = "startday" Then
                    Dim startDay As Integer
                    If tblfilterothers.Rows.Count > 1 Then
                        startDay = CType(DirectCast(tblfilterothers.Rows(1).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
                    Else
                        startDay = 1
                    End If
                    ' tempWClause = fldCaption & "=" & noofDays
                    tempStringWClause = "Start Day=" & startDay & " AND "
                    'wClause &= tempWClause & " AND "
                    stringWClause &= tempStringWClause
                End If

                rCounter += 1
            Loop    'loop through the entries in the tblFilterOThers table


            If errMsg = "" Then
                'No error was found.
                'If the operator was the In List then we will have an extra AND
                'so we need to remove it.
                'Modified by Michelle Rodriguez 10/25/2004.
                'If operator was the Empty and the user typed something in the textbox next to the operators' dropdown,
                'there is an extra AND at position 0 (zero) and we need to remove it.
                If wClause <> "" Then
                    'Remove last occurrence of " AND".
                    position = wClause.LastIndexOf(" AND", StringComparison.Ordinal)
                    If position > 0 Then
                        wClause = wClause.Substring(0, position + 1)
                    End If
                    'Remove occurrence of " AND " at position 0 (zero).
                    position = wClause.IndexOf(" AND ", StringComparison.Ordinal)
                    If position = 0 Then
                        wClause = wClause.Substring(5)
                    End If
                End If
                'Need to do the same with the string reprentation of the Where Clause.
                'Remove last occurrence of " AND" and the first occurrence of " AND " from the stringWhereClause.
                If stringWClause <> "" Then
                    position = stringWClause.LastIndexOf(" AND ", StringComparison.Ordinal)
                    If position > 0 Then
                        stringWClause = stringWClause.Substring(0, position + 1)
                    End If
                    position = stringWClause.IndexOf(" AND ", StringComparison.Ordinal)
                    If position = 0 Then
                        stringWClause = stringWClause.Substring(0, position + 1).Trim
                    End If
                End If
            Else
                'If an error was found, clear variables.
                'StrWClause = ""
                wClause = ""
            End If

            strErrorMsg = errMsg
            strWClause = stringWClause
            Return wClause
        End Function

        Private Function BuildFilterValue(ByVal filterValue As String, ByVal dataType As String,
                                          Optional ByVal sqlOperator As String = "") As String
            ' Modified by Michelle Rodriguez on 09/26/2004 and 10/04/2004.
            ' Needed to include the time for filters that involved Datetime fields:
            ' - The " = " operator will behave like a " BETWEEN " and the filter value
            '   will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
            ' - The " <> " operator will behave like a " NOT BETWEEN " and the filter value
            '   will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
            ' - Also, the filter value for the " > " operator will be of format: mm/dd/yyyy 11:59:59 PM
            ' All other cases remain the same.       

            Dim returnValue As String
            Dim validValue As String

            validValue = ValidateValue(filterValue, dataType)
            If validValue <> "" Then
                Select Case dataType
                    Case "Uniqueidentifier", "Char", "Varchar"
                        returnValue = "'" & filterValue & "'"
                    Case "Datetime"
                        If sqlOperator = " = " Or sqlOperator = " <> " Then
                            returnValue = "'" & filterValue & " 12:00:00 AM' AND '" & filterValue & " 11:59:59 PM'"
                        ElseIf sqlOperator = " > " Then
                            returnValue = "'" & filterValue & " 11:59:59 PM'"
                        Else
                            returnValue = "'" & filterValue & "'"
                        End If
                    Case Else
                        Return filterValue
                End Select
            Else
                returnValue = validValue
            End If

            Return returnValue
        End Function

        'Method added by Michelle R. Rodriguez on 11/22/2004, as part of the error handler for the FilterOthers section.
        Private Function ValidateValue(ByVal filterValue As String, ByVal dataType As String) As String
            Dim returnValue As String

            Select Case dataType
                Case "Uniqueidentifier", "Char", "Varchar"
                    If filterValue.IndexOf("'", StringComparison.Ordinal) <> -1 Then
                        'Replace single quotes by double quotes.
                        returnValue = filterValue.Replace("'", "''")
                    Else
                        'Return FilterValue as it was.
                        returnValue = filterValue
                    End If
                Case "Datetime"
                    Try
                        Dim dDate As DateTime = Date.Parse(filterValue)
                        'SLQ Server does not accept year < 1753. Therefore, a limit has to be imposed.
                        If dDate < FAME.AdvantageV1.Common.AdvantageCommonValues.MinDate Or
                           dDate > FAME.AdvantageV1.Common.AdvantageCommonValues.MaxDate Then
                            returnValue = ""
                        Else
                            returnValue = dDate.ToShortDateString
                        End If
                    Catch
                        'Invalid date.
                        'Might be ArgumentException, FormatException or OverflowException
                        returnValue = ""
                    End Try
                Case Else
                    Return filterValue
            End Select
            Return returnValue
        End Function

        Private Function GetSQLOperator(ByVal opId As Integer) As String
            Select Case opId
                Case 1
                    Return " = "
                Case 2
                    Return " <> "
                Case 3
                    Return " > "
                Case 4
                    Return " < "
                Case 5
                    Return " IN "
                Case 6
                    Return " BETWEEN "
                Case 7
                    Return " LIKE "
                Case 8
                    Return " = '' "
                Case 9
                    Return " IS NULL "
                Case Else
                    Throw New Exception("The following operator is not recognized:" & opId)
            End Select
        End Function


        ' This method is going to build the dataset for the report. Then the report may be displayed
        ' in web form, which may contain a Crystal Report Viewer or a DataGrid, 
        ' or may be exported to any of the available formats.
        Private Function BuildReportDataSet(isExport As Boolean) As String
            Dim ds As DataSet
            Dim objName As String
            Dim facAssembly As Reflection.Assembly
            Dim rptParamInfo As ReportParamInfo = New ReportParamInfo()
            rptParamInfo.ExportType = ddlexportto.SelectedValue.ToString ' This is used only to majorminor report.
            Dim rptfac As New ReportFacade
            Dim strErrMsg As String = GetParamInfo(rptParamInfo)
            'A Error happened....
            If (strErrMsg <> String.Empty) Then
                Session("RptInfo") = Nothing
                Session("ReportDS") = Nothing
                Return strErrMsg
            End If

            'Create Reports................................
            ' ASk if we are in MajorMinor Schools and if it show report
            Dim setting As String = GetSettingString("MajorsMinorsConcentrations")
            If (ResourceId = 238 Or ResourceId = 245) AndAlso (setting.ToUpper() = "YES") AndAlso isExport Then
                'Get Policy for this school in exam.
                rptParamInfo.SettingRetakeTestPolicy = GetSettingString("SettingRetakeTestPolicy")
                If (IsNothing(rptParamInfo.SettingRetakeTestPolicy)) Then
                    rptParamInfo.SettingRetakeTestPolicy = "TAKEBEST"
                End If
                ' Create 3.5 Reports Schools with MajorMinor
                BuildMajorMinorReport(rptParamInfo)
                Return strErrMsg
            End If
            ' Actual Report 3.4.....
            If rptParamInfo.SqlId > 0 Then
                ds = rptfac.GetReportDataSet(rptParamInfo)
            Else
                'Get the name of the object associated with the OBJID.
                objName = rptfac.GetOBJName(rptParamInfo.ObjId)
                'Use reflection to create an instance of the object from the name.
                facAssembly = Reflection.Assembly.LoadFrom(Server.MapPath("..") + "\bin\BusinessFacade.dll")
                Dim objBaseRptFac As BaseReportFacade
                objBaseRptFac = DirectCast(facAssembly.CreateInstance("FAME.AdvantageV1.BusinessFacade." + objName), BaseReportFacade)
                ds = objBaseRptFac.GetReportDataSet(rptParamInfo)
            End If

            'If the SQLID is not 0 then we need to create the class that generates DataSet for simple report.
            'Else we need to use OBJID to get the name of the object and create object using reflection.
            If ResourceId = 582 Then
                If ds Is Nothing Or ds.Tables.Count = 0 Then
                    Session("ReportDS") = Nothing
                    Session("RptInfo") = Nothing
                    Return "There is no data matching selection."
                End If
            End If
            If ds.Tables.Count <> 0 Then
                If ResourceId = 537 Then
                    If ds.Tables(3).Rows.Count > 0 Then
                        Session("ReportDS") = ds
                        Session("RptInfo") = rptParamInfo
                    Else
                        Session("ReportDS") = Nothing
                        Session("RptInfo") = Nothing
                    End If
                Else
                    If ds.Tables(0).Rows.Count > 0 Then
                        Session("ReportDS") = ds
                        Session("RptInfo") = rptParamInfo
                    Else
                        Session("ReportDS") = Nothing
                        Session("RptInfo") = Nothing
                    End If

                End If
            End If
            If (strErrMsg = String.Empty) Then
                'Since no error was found, proceed to display report in Export Viewer page.
                If Not (Session("ReportDS") Is Nothing) Then

                    '   Make sure to clear any previously loaded report docuemnt objects
                    Session("RptObject") = Nothing


                    If isExport Then
                        LoadReport(ddlexportto.SelectedValue.ToString, ds, rptParamInfo)
                    Else
                        '   Setup the properties of the new window
                        Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium
                        Dim name As String
                        Dim url As String
                        name = "ReportViewer"
                        url = "../SY/" + name + ".aspx"
                        CommonWebUtilities.OpenClientChildWindow(Page, url, name, winSettings)

                    End If

                Else
                    'Alert user of no data matching selection.
                    strErrMsg = "There is no data matching selection."
                    'DisplayErrorMessage(strErrMsg)
                End If
            End If
            Return strErrMsg
        End Function

        Private Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnexport.Click
            ExecuteReport(True)
        End Sub

        Private Function GetValidateErrMsg() As String
            Dim rtn As String = String.Empty
            Dim count As Integer = Page.Validators.Count
            Dim index As Integer
            For index = 0 To count - 1
                If (Not Page.Validators(index).IsValid) Then
                    rtn = rtn + Page.Validators(index).ErrorMessage & vbLf
                End If
            Next
            Return rtn
        End Function

        Private Sub lbtSearch_Click(ByVal sender As System.Object, ByVal e As EventArgs) Handles lbtsearch.Click
            '   setup the properties of the new window
            Const winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=500px"
            Const name As String = "StudentPopupSearch"
            Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/sy/" + name + ".aspx?resid=293&mod=AR&cmpid=" + CampusId

            '   build list of parameters to be passed to the new window
            Dim parametersArray(7) As String

            '   set StudentName
            parametersArray(0) = txtStudentId.ClientID

            '   set StuEnrollment
            parametersArray(1) = txtstuenrollmentid.ClientID
            parametersArray(2) = txtstuenrollment.ClientID

            ''   set Terms
            parametersArray(3) = txttermid.ClientID
            parametersArray(4) = txtterm.ClientID

            '   set AcademicYears
            parametersArray(5) = txtacademicyearid.ClientID
            parametersArray(6) = txtacademicyear.ClientID

            parametersArray(7) = txtstudentguidid.ClientID
            '   open new window and pass parameters
            CommonWebUtilities.OpenWindowAndPassParameters(Page, url, name, winSettings, parametersArray)
        End Sub

        Private Sub DisplayErrorMessage(ByVal errorMessage As String)
            '   Set error condition
            Customvalidator1.ErrorMessage = errorMessage
            Customvalidator1.IsValid = False

            If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
                '   Display error in message box in the client
                CommonWebUtilities.DisplayErrorInClientMessageBox(Page, errorMessage)
            End If
        End Sub
        Private Sub btnFriendlyPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnfriendlyprint.Click
            ExecuteReport(False)
        End Sub

        Private Sub lbtPrompt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtprompt.Click
            Dim objRptParams As New RptParamsFacade
            Dim i As Integer
            Dim ds As DataSet
            Dim dt As DataTable
            Dim dr As DataRow
            Dim opId As Integer
            Dim fldValue As String
            Dim rptParamId As Integer
            Dim rowCounter As Integer
            Dim objCommon As New FAME.Common.CommonUtilities
            Dim errMsg As String

            If txtpreferencename.Text = "" Then
                'Alert user that a Preference name is required.
                errMsg = "Preference Name is required."
                'Display Error Message.
                DisplayErrorMessage(errMsg)
            Else
                'Before adding or updating we record we need to validate the entries
                'in the filter others section.
                If ValidateFilterOthers() Then
                    'If mode is NEW then we need to do an insert.
                    If ViewState("MODE") = "NEW" Then
                        Dim prefId As String
                        'Generate a guid to be used as the prefid
                        prefId = Guid.NewGuid.ToString
                        'Save the basic user preference information
                        objRptParams.AddUserPrefInfo(prefId, CType(Session("UserName"), String), CInt(ViewState("resid")), txtpreferencename.Text)
                        'Save each sort preference in the lbxSort listbox. Use the order in the
                        'listbox to generate the sequence.
                        For i = 0 To lbxselsort.Items.Count - 1
                            objRptParams.AddUserSortPrefs(prefId, CType(lbxselsort.Items(i).Value, Integer), i + 1)
                        Next

                        'If the FilterListSelections dt is not empty then we need to save the
                        'FilterList preferences
                        ds = DirectCast(Session("WorkingDS"), DataSet)
                        dt = ds.Tables("FilterListSelections")

                        For Each dr In dt.Rows
                            objRptParams.AddUserFilterListPrefs(prefId, CType(dr("RptParamId"), Integer), dr("FldValue").ToString())
                        Next

                        'Special case - Modified by Michelle R. Rodriguez on 11/18/2004
                        'txtStuEnrollmentId is a hidden textbox that contains the StuEnrollId corresponding to the 
                        'Student and Program selected thru the Student Search pop-up.
                        If txtstuenrollmentid.Value <> "" Then
                            rptParamId = 127
                            opId = 1 ' Equal To operator
                            fldValue = txtstuenrollmentid.Value
                            objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                        End If

                        'We need to examine each row in the tblFilterOthers table control. If the
                        'user entered data in the textbox then we should save the info for that row.
                        For rowCounter = 1 To tblfilterothers.Rows.Count - 1 'The first row is empty
                            'If there is an entry in the textbox then get the values from the row
                            If DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                                rptParamId = CType(DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3), Integer)
                                opId = CType(DirectCast(tblfilterothers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
                                fldValue = DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                If DirectCast(tblfilterothers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text <> "" Then
                                    'Special case: The Between operator uses two values located on two different textboxes
                                    fldValue &= ";" & DirectCast(tblfilterothers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                                End If
                                objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                                '
                            ElseIf DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text = "" And
                                   (DirectCast(tblfilterothers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Empty" Or
                                    DirectCast(tblfilterothers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Is Null") Then
                                'Empty or Is Null
                                fldValue = ""
                                opId = CType(DirectCast(tblfilterothers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
                                rptParamId = CType(DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3), Integer)
                                objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                            End If
                        Next

                        'We need to rebind the DataList. We cannot use the cache here since we need
                        'the latest info including the record just added. If the cache was used we
                        'would not see the new record in the datalist.
                        BindDataListFromDB()

                        'Change mode to Edit
                        'SetMode("EDIT")
                        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                        ViewState("MODE") = "EDIT"
                        'Place the prefid guid into the txtPrefId textbox so it can be reused
                        'if we need to do an update later on.
                        txtprefid.Value = prefId
                        '   set Style to Selected Item
                        CommonWebUtilities.SetStyleToSelectedItem(dlstprefs, txtprefid.Value, ViewState)

                    Else
                        'Update the basic user preference information
                        objRptParams.UpdateUserPrefInfo(txtprefid.Value, txtpreferencename.Text)
                        'Delete any previous sort info and then add the ones in the lbxSelSort listbox.
                        objRptParams.DeleteUserSortPrefs(txtprefid.Value)
                        'Save each sort preference in the lbxSort listbox. Use the order in the
                        'listbox to generate the sequence.
                        For i = 0 To lbxselsort.Items.Count - 1
                            objRptParams.AddUserSortPrefs(txtprefid.Value, CType(lbxselsort.Items(i).Value, Integer), i + 1)
                        Next
                        'Delete any previous filter list info and then add the ones in the FilterListSelections dt.
                        objRptParams.DeleteUserFilterListPrefs(txtprefid.Value)
                        'Save each filter list preference in the FilterListSelections dt.
                        ds = DirectCast(Session("WorkingDS"), DataSet)
                        dt = ds.Tables("FilterListSelections")

                        For Each dr In dt.Rows
                            objRptParams.AddUserFilterListPrefs(txtprefid.Value, CType(dr("RptParamId"), Integer), dr("FldValue").ToString())
                        Next
                        'Delete any previous filter other info and then add the ones now specified
                        objRptParams.DeleteUserFilterOtherPrefs(txtprefid.Value)

                        'Special case - Modified by Michelle R. Rodriguez on 11/19/2004
                        'txtStuEnrollmentId is a hidden textbox that contains the StuEnrollId corresponding to the 
                        'Student and Program selected thru the Student Search pop-up.
                        If txtstuenrollmentid.Value <> "" Then
                            rptParamId = 127
                            opId = 1 ' Equal To operator
                            fldValue = txtstuenrollmentid.Value
                            objRptParams.AddUserFilterOtherPrefs(txtprefid.Value, rptParamId, opId, fldValue)
                        End If
                        '
                        For rowCounter = 1 To tblfilterothers.Rows.Count - 1 'The first row is empty
                            'If there is an entry in the textbox then get the values from the row
                            If DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                                fldValue = DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                If DirectCast(tblfilterothers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text <> "" Then
                                    'Special case: The Between operator uses two values located on two different textboxes
                                    fldValue &= ";" & DirectCast(tblfilterothers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                                End If
                                opId = CType(DirectCast(tblfilterothers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
                                rptParamId = CType(DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3), Integer)
                                objRptParams.AddUserFilterOtherPrefs(txtprefid.Value, rptParamId, opId, fldValue)
                                '
                            ElseIf DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text = "" And
                                   (DirectCast(tblfilterothers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Empty" Or
                                    DirectCast(tblfilterothers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Is Null") Then
                                'Empty or Is Null
                                fldValue = ""
                                opId = CType(DirectCast(tblfilterothers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
                                rptParamId = CType(DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3), Integer)
                                objRptParams.AddUserFilterOtherPrefs(txtprefid.Value, rptParamId, opId, fldValue)
                            End If
                        Next

                        'We need to rebind the DataList in case txtPrefName had changed. 
                        BindDataListFromDB()
                        '   set Style to Selected Item
                        CommonWebUtilities.SetStyleToSelectedItem(dlstprefs, txtprefid.Value, ViewState)
                    End If  'Test if mode is NEW or EDIT
                End If 'ValidateFilterOThers returns True
            End If 'txtPreferenceName.Text is empty
        End Sub

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add buttons and listboxes
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(btnexport)
            controlsToIgnore.Add(btnfriendlyprint)
            controlsToIgnore.Add(btnsearch)
            controlsToIgnore.Add(btnadd)
            controlsToIgnore.Add(btnremove)
            controlsToIgnore.Add(lbxfilterlist)
            controlsToIgnore.Add(lbxselfilterlist)

            'Dim tbl As Table = DirectCast(FindControl("tblFilterOthers"), Table)
            Dim tbl As Table = Nothing
            If Not tblfilterothers Is Nothing Then
                tbl = tblfilterothers
            End If

            If Not (tbl Is Nothing) Then
                For i As Integer = 1 To tbl.Rows.Count - 1
                    Dim ddl As DropDownList = DirectCast(tbl.Rows(i).Cells(1).Controls(0), DropDownList)
                    ''Added label control to find the text of the label, When the text is number of days ,
                    '' then it should not show the textboxes when the operator value is 6, i.e between
                    Dim lbl As Label = DirectCast(tbl.Rows(i).Cells(0).Controls(0), Label)
                    If Not (ddl Is Nothing) Then
                        'add Operators ddl
                        controlsToIgnore.Add(ddl)

                        Dim rptParamId As Integer
                        rptParamId = Integer.Parse(ddl.ID.Substring(3))
                        Dim ctrl As Control = FindControlRecursive("txt2" & rptParamId)
                        ''Added for timeclock report
                        ''Avoid adding two textboxes when the ddl value is 6. i.e. Between range for dates used in other reports
                        ''in time clock report 6 is the number of days

                        If Not (ctrl Is Nothing) Then
                            ''Modified by saraswathi Lakshmanan
                            If ddl.SelectedValue = AdvantageCommonValues.BetweenOperatorValue Then
                                If (ViewState("resid") = 590 And lbl.Text <> "Month Range") Then
                                    ctrl.Visible = False
                                Else
                                    ctrl.Visible = True
                                End If
                            Else
                                ctrl.Visible = False
                            End If
                        End If
                    End If
                Next
            End If
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
            BindToolTip()
        End Sub
        Private Sub SavePreferences()
            Dim objRptParams As New RptParamsFacade
            Dim i As Integer
            Dim ds As DataSet
            Dim dt As DataTable
            Dim dr As DataRow
            Dim opId As Integer
            Dim fldValue As String
            Dim rptParamId As Integer
            Dim rowCounter As Integer '', rowcounter1
            ' Dim cellCounter As Integer
            Dim objCommon As New FAME.Common.CommonUtilities
            Dim errMsg As String

            If txtpreferencename.Text = "" Then
                'Alert user that a Preference name is required.
                errMsg = "Preference Name is required."
                'Display Error Message.
                DisplayErrorMessage(errMsg)
            Else
                'Before adding or updating we record we need to validate the entries
                'in the filter others section.
                If ValidateFilterOthers() Then
                    'If mode is NEW then we need to do an insert.
                    If ViewState("MODE") = "NEW" Then
                        Dim prefId As String
                        'Generate a guid to be used as the prefid
                        prefId = Guid.NewGuid.ToString
                        'Save the basic user preference information
                        objRptParams.AddUserPrefInfo(prefId, CType(Session("UserName"), String), CInt(ViewState("resid")), txtpreferencename.Text)

                        'Save each sort preference in the lbxSort listbox. Use the order in the
                        'listbox to generate the sequence.
                        For i = 0 To lbxselsort.Items.Count - 1
                            objRptParams.AddUserSortPrefs(prefId, CType(lbxselsort.Items(i).Value, Integer), i + 1)
                        Next

                        'If the FilterListSelections dt is not empty then we need to save the
                        'FilterList preferences
                        ds = DirectCast(Session("WorkingDS"), DataSet)
                        dt = ds.Tables("FilterListSelections")
                        For Each dr In dt.Rows
                            objRptParams.AddUserFilterListPrefs(prefId, CType(dr("RptParamId"), Integer), dr("FldValue").ToString())
                        Next

                        'We need to examine each row in the tblFilterOthers table control. If the
                        'user entered data in the textbox then we should save the info for that row.
                        For rowCounter = 1 To tblfilterothers.Rows.Count - 1 'The first row is empty
                            'If there is an entry in the textbox then get the values from the row
                            If DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                                ''If operatorId is 6- then it is between value. so two text boxes will be there, they are aded with a ; seperated
                                opId = CType(DirectCast(tblfilterothers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
                                If opId = 6 Then
                                    fldValue = DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                    Dim fldval1 As String
                                    fldval1 = DirectCast(tblfilterothers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                                    fldValue = fldValue + ";" + fldval1
                                    Dim str As String = DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID
                                    ''Modified by Saraswathi  On July 10 2009
                                    '  rptParamId = Integer.Parse(str.Substring(str.Length - 3, 3))
                                    rptParamId = Integer.Parse(str.Substring(3))
                                    objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                                Else
                                    fldValue = DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                    Dim str As String = DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID
                                    ''Modified by Saraswathi  On July 10 2009
                                    '  rptParamId = Integer.Parse(str.Substring(str.Length - 3, 3))
                                    rptParamId = Integer.Parse(str.Substring(3))
                                    objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                                End If

                            End If
                        Next

                        If txtstuenrollment.Text <> "" Then
                            opId = 1
                            fldValue = txtstuenrollmentid.Value
                            rptParamId = 127
                            objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                        End If
                        'Next

                        'We need to rebind the DataList. We cannot use the cache here since we need
                        'the latest info including the record just added. If the cache was used we
                        'would not see the new record in the datalist.
                        BindDataListFromDB()
                        'Change mode to Edit
                        'SetMode("EDIT")
                        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                        ViewState("MODE") = "EDIT"
                        'Place the prefid guid into the txtPrefId textbox so it can be reused
                        'if we need to do an update later on.
                        txtprefid.Value = prefId
                    Else
                        'Update the basic user preference information
                        objRptParams.UpdateUserPrefInfo(txtprefid.Value, txtpreferencename.Text)

                        'Delete any previous sort info and then add the ones in the lbxSelSort listbox.
                        objRptParams.DeleteUserSortPrefs(txtprefid.Value)

                        'Save each sort preference in the lbxSort listbox. Use the order in the
                        'listbox to generate the sequence.
                        For i = 0 To lbxselsort.Items.Count - 1
                            objRptParams.AddUserSortPrefs(txtprefid.Value, CType(lbxselsort.Items(i).Value, Integer), i + 1)
                        Next

                        'Delete any previous filter list info and then add the ones in the FilterListSelections dt.
                        objRptParams.DeleteUserFilterListPrefs(txtprefid.Value)

                        'Save each filter list preference in the FilterListSelections dt.
                        ds = DirectCast(Session("WorkingDS"), DataSet)
                        dt = ds.Tables("FilterListSelections")
                        For Each dr In dt.Rows
                            objRptParams.AddUserFilterListPrefs(txtprefid.Value, CType(dr("RptParamId"), Integer), dr("FldValue").ToString())
                        Next

                        'Delete any previous filter other info and then add the ones now specified
                        objRptParams.DeleteUserFilterOtherPrefs(txtprefid.Value)
                        For rowCounter = 1 To tblfilterothers.Rows.Count - 1 'The first row is empty
                            'If there is an entry in the textbox then get the values from the row
                            If DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                                opId = CType(DirectCast(tblfilterothers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
                                ''Modified by Saraswathi  On July 10 2009
                                If opId = 6 Then
                                    fldValue = DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                    Dim fldval1 As String
                                    fldval1 = DirectCast(tblfilterothers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                                    fldValue = fldValue + ";" + fldval1
                                    Dim str As String = DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID
                                    ''Modified by Saraswathi  On July 10 2009
                                    '  rptParamId = Integer.Parse(str.Substring(str.Length - 3, 3))
                                    rptParamId = Integer.Parse(str.Substring(3))
                                    objRptParams.AddUserFilterOtherPrefs(txtprefid.Value, rptParamId, opId, fldValue)
                                Else
                                    fldValue = DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                    Dim str As String = DirectCast(tblfilterothers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID
                                    ''Modified by Saraswathi  On July 10 2009
                                    '  rptParamId = Integer.Parse(str.Substring(str.Length - 3, 3))
                                    rptParamId = Integer.Parse(str.Substring(3))
                                    objRptParams.AddUserFilterOtherPrefs(txtprefid.Value, rptParamId, opId, fldValue)
                                End If

                            End If
                        Next

                        'For rowcounter1 = 1 To tblStudentSearch.Rows.Count - 1 'The first row is empty
                        'If there is an entry in the textbox then get the values from the row
                        If txtStudentId.Text <> "" Then
                            opId = 1
                            fldValue = txtstuenrollmentid.Value
                            rptParamId = 127
                            objRptParams.AddUserFilterOtherPrefs(txtprefid.Value, rptParamId, opId, fldValue)
                        End If
                        'If txtStuEnrollment.Text <> "" Then
                        '    opId = 1
                        '    fldValue = txtStuEnrollmentId.Value
                        '    rptParamId = 127
                        '    objRptParams.AddUserFilterOtherPrefs(txtPrefId.Value, rptParamId, opId, fldValue)
                        'End If
                        'Next

                    End If  'Test if mode is NEW or EDIT
                End If 'ValidateFilterOThers returns True
                'CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Value, ViewState, Header1)
            End If 'txtPreferenceName.Text is empty
        End Sub

        Protected Sub rdolstChkUnits_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdolstchkunits.SelectedIndexChanged
            If rdolstchkunits.SelectedValue = "False" Then
                pnlrow.Enabled = True
            Else
                pnlrow.Enabled = False
            End If
        End Sub

        ''Added by Saraswathi Lakshmanan
        ''For Attendance history timeClock reports
        ''To show the Start Day to print on the report

        Private Function createStartDayTable() As DataTable
            Dim dt As New DataTable
            Dim dr As DataRow
            dt.Columns.Add("CompOpText", Type.GetType("System.String"))
            dt.Columns.Add("CompOpId", Type.GetType("System.Int32"))
            Dim i As Integer
            For i = 1 To 7
                dr = dt.NewRow
                If i = 1 Then
                    dr("CompOpText") = "Monday"
                ElseIf i = 2 Then
                    dr("CompOpText") = "Tuesday"
                ElseIf i = 3 Then
                    dr("CompOpText") = "Wednesday"
                ElseIf i = 4 Then
                    dr("CompOpText") = "Thursday"
                ElseIf i = 5 Then
                    dr("CompOpText") = "Friday"
                ElseIf i = 6 Then
                    dr("CompOpText") = "Saturday"
                ElseIf i = 7 Then
                    dr("CompOpText") = "Sunday"
                End If
                dr("CompOpId") = i
                dt.Rows.Add(dr)
            Next i
            Return dt

        End Function



        Private Function getCampusId(ByVal filterList As String) As String
            Dim campId As String
            Dim campIdx1, campIdx2, campIdx3 As Integer
            campIdx1 = filterList.ToLower.IndexOf("campusid", StringComparison.Ordinal)
            campIdx2 = filterList.Substring(campIdx1).IndexOf("'", StringComparison.Ordinal)
            campIdx3 = filterList.Substring(campIdx1 + campIdx2 + 1).IndexOf("'", StringComparison.Ordinal)
            campId = filterList.Substring(campIdx1 + campIdx2 + 1, campIdx3)
            Return campId

        End Function


        Private Sub LoadReport(format As String, ds As DataSet, rptParamInfo As Object)
            Dim rptXport As New RptExport
            Dim objReport As CrystalDecisions.CrystalReports.Engine.ReportDocument

            If Session("RptInfo") IsNot Nothing Then
                rptParamInfo = DirectCast(Session("RptInfo"), ReportParamInfo)
            End If

            PageTitle &= CType(rptParamInfo.RptTitle, String)

            If Session("RptObject") IsNot Nothing Then
                objReport = DirectCast(Session("RptObject"), CrystalDecisions.CrystalReports.Engine.ReportDocument)
            Else
                objReport = (New AdvantageReportFactory).CreateAdvantageReport(ds, CType(rptParamInfo, ReportParamInfo))
                Session("RptObject") = objReport
            End If
            Dim contentType = ""
            rptXport.ExportToStream(Context, objReport, format, rptParamInfo.RptTitle.ToString)

        End Sub

        Private Sub ExecuteReport(isExport As Boolean)
            If Page.IsValid Then
                If (ResourceId = 238) And (txtstuenrollmentid.Value <> "") Then
                    Dim facade As New RegFacade
                    If facade.IsStudentHold(txtstuenrollmentid.Value) = "1" Then
                        DisplayErrorMessage("The report cannot be displayed as the student transcript is put on hold")
                        Exit Sub
                    End If
                End If
                ''''''''''''''''''
                Dim strErrMsg As String = BuildReportDataSet(isExport)
                'If strErrMsg = "" Then
                '    'Since no error was found, proceed to display report in Export Viewer page.
                '    If Not (Session("ReportDS") Is Nothing) Then

                '        '   Make sure to clear any previously loaded report docuemnt objects
                '        Session("RptObject") = Nothing

                '        '   Setup the properties of the new window
                '        Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium
                '        Dim name As String
                '        Dim url As String
                '        If isExport Then
                '            name = "ReportExportViewer"
                '            url = "../SY/" + name + ".aspx?ExportTo=" & ddlexportto.SelectedValue.ToString
                '        Else
                '            name = "ReportViewer"
                '            url = "../SY/" + name + ".aspx"
                '        End If

                '        CommonWebUtilities.OpenClientChildWindow(Page, url, name, winSettings)
                '    Else
                '        'Alert user of no data matching selection.
                '        strErrMsg = "There is no data matching selection."
                '        DisplayErrorMessage(strErrMsg)
                '    End If
                'Else
                'Alert user of error.
                '   DisplayErrorMessage(strErrMsg)
                'End If
                If strErrMsg <> String.Empty Then
                    DisplayErrorMessage(strErrMsg)
                End If
            Else
                If (ViewState("boolShowErr") IsNot Nothing) Then
                    Dim strErrMsg As String = GetValidateErrMsg()
                    DisplayErrorMessage(strErrMsg)
                End If
                ViewState("boolShowErr") = True
            End If
        End Sub

        Private Function GetParamInfo(ByRef rptParamInfo As ReportParamInfo) As String
            Dim orderByClause As String = ""
            Dim stringOrderByClause As String = ""
            Dim filterListWhereClause As String
            Dim stringWhereClause As String = ""
            Dim filterOtherWhereClause As String
            Dim stringFilterOtherWClause As String = ""
            Dim strErrMsg As String = ""
            Dim rptfac As New ReportFacade

            'Build the Order by clause if there are any entries in the lbxSelSort control.
            If lbxselsort.Items.Count > 0 Then
                orderByClause = BuildOrderBy(stringOrderByClause)
            End If

            'Build the filter list part of the where clause.
            filterListWhereClause = BuildFilterListClause(stringWhereClause)
            If ResourceId = 537 Then
                If txttermstart.Text = "" And filterListWhereClause = "" Then
                    Return "Required fields Term Start Cutoff or Term is missing."
                ElseIf txttermstart.Text <> "" And filterListWhereClause <> "" Then
                    Return "Please select either the Term Start Cutoff filter or the Term filter and not both."
                ElseIf txttermstart.Text = "" And filterListWhereClause <> "" Then
                    If DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections").Rows.Count > 1 Then
                        Return "Please select a single term"
                    Else
                        'we will use the orderby clause to store the term selected
                        orderByClause = " and " & filterListWhereClause
                    End If

                ElseIf txttermstart.Text <> "" And filterListWhereClause = "" Then
                    rptParamInfo.CutoffDate = txttermstart.Text

                End If
            End If
            ' Else
            'errMsg = "Required fields Term Start Cutoff is missing."
            'If resourceId = 537 Then filterListWhereClause = chkUnits.Checked & "," & chkStudentSign.Checked & "," & chkSchoolSign.Checked
            Dim groupfilter As String = String.Empty
            If chk499.Checked Then groupfilter = 499 & "|"
            If chk500.Checked Then groupfilter = groupfilter & 500 & "|"
            If chk501.Checked Then groupfilter = groupfilter & 501 & "|"
            If chk502.Checked Then groupfilter = groupfilter & 502 & "|"
            If chk503.Checked Then groupfilter = groupfilter & 503 & "|"
            If chk533.Checked Then groupfilter = groupfilter & 533 & "|"
            If chk544.Checked Then groupfilter = groupfilter & 544

            If ResourceId = 537 Then filterListWhereClause = rdolstchkunits.SelectedValue.ToString & "," & chkstudentsign.Checked & "," & chkschoolsign.Checked & "," & chktermmodule.Checked & "," & groupfilter

            'Build the filter other part of the where clause.
            'If an error is found in the user input, we cannot proceed to display the report.
            filterOtherWhereClause = BuildFilterOtherClause(stringFilterOtherWClause, strErrMsg)

            If ResourceId = 582 Then
                If filterOtherWhereClause = "" And filterListWhereClause = "" Then
                    Return "Required fields Campus Group and Program Version OR Student and Enrollment are missing."
                End If
            End If
            'This case is Transcript single student
            If ResourceId = 238 Then
                rptParamInfo.StuEnrollId = txtstuenrollmentid.Value ' This set this to the enrollment all to Guid.Empty if multiple enrollment.
                If chkrptshowclassdates.Checked And chktermmodule.Checked Then
                    Return "Please select either the Show Class Dates or the Show Term/Module checkbox and not both"
                End If
            End If

            ''New Code Added By Vijay Ramteke On November 04, 2010 For Rally Id DE1218
            If ResourceId = 602 Then
                If txtStudentId.Text <> "" And txtstuenrollment.Text = "All Enrollments" Then
                    'filterListWhereClause = ""
                    filterOtherWhereClause = ""
                    filterListWhereClause = "arStuEnrollments.StudentId = '" & txtstudentguidid.Value & "'"
                ElseIf txtStudentId.Text <> "" And txtstuenrollment.Text <> "" And txtstuenrollment.Text <> "All Enrollments" Then
                    'filterListWhereClause = ""
                    filterOtherWhereClause = ""
                    filterListWhereClause = "arStuEnrollments.StuEnrollId = '" & txtstuenrollmentid.Value & "'"
                    rptParamInfo.StuEnrollId = txtstuenrollmentid.Value
                ElseIf Not filterListWhereClause.ToLower.Contains("sycampgrps.campgrpid") Then
                    strErrMsg = "Select Campus Group or the Student and Enrollment"
                End If
            End If
            'If  error is found, return....
            If strErrMsg <> "" Then Return strErrMsg
            'Build the report Info.
            rptParamInfo.ResId = ResourceId
            If CInt(ViewState("SQLID")) > 0 Then
                rptParamInfo.SqlId = CInt(ViewState("SQLID"))
                rptParamInfo.ObjId = 0
            Else
                rptParamInfo.SqlId = 0
                rptParamInfo.ObjId = CInt(ViewState("OBJID"))
            End If
            rptParamInfo.StudentId = txtstudentguidid.Value
            rptParamInfo.OrderBy = orderByClause
            rptParamInfo.OrderByString = stringOrderByClause
            rptParamInfo.FilterList = filterListWhereClause
            rptParamInfo.FilterListString = stringWhereClause
            rptParamInfo.FilterOther = filterOtherWhereClause
            rptParamInfo.FilterOtherString = stringFilterOtherWClause
            rptParamInfo.ShowFilters = chkrptfilters.Checked
            rptParamInfo.ShowSortBy = chkrptsort.Checked
            rptParamInfo.ShowRptDescription = chkrptdescrip.Checked
            rptParamInfo.ShowRptInstructions = chkrptinstructions.Checked
            rptParamInfo.ShowRptNotes = chkrptnotes.Checked
            rptParamInfo.ShowRptDateIssue = chkrptdateissue.Checked
            rptParamInfo.SuppressHeader = chkrptsuppressheader.Checked
            rptParamInfo.ShowRptClassDates = chkrptshowclassdates.Checked
            rptParamInfo.ShowTermOrModule = chktermmodule.Checked
            rptParamInfo.ShowCurrentBalance = chkcurrentbalance.Checked
            rptParamInfo.ShowTotalCost = chktotalcost.Checked
            rptParamInfo.ShowUseSignLineForAttnd = chkUseSignatureForAttendance.Checked
            rptParamInfo.ShowLegalDisclaimer = chkshowlegaldisc.Checked
            rptParamInfo.ShowTermProgressDescription = chkshowTermProgressDescription.Checked
            rptParamInfo.ShowTransferCampus = chkrptiswttdc.Checked
            rptParamInfo.ShowTransferProgram = chkrptiswttdp.Checked
            rptParamInfo.ShowLDA = chkrptdonotshowlda.Checked

            'DE8390 3/7/2013 Janet Robinson
            If ResourceId = 238 Then
                rptParamInfo.ShowOfficialTranscript = chkShowOffTrans.Checked
            End If

            rptParamInfo.RptTitle = CType(ViewState("Resource"), String)
            rptParamInfo.CampusId = HttpContext.Current.Request("cmpid").ToString
            If Not (Session("OfficialSchoolLogo") Is Nothing) Then
                rptParamInfo.SchoolLogo = DirectCast(Session("OfficialSchoolLogo"), System.Byte())
            Else
                rptParamInfo.SchoolLogo = rptfac.GetSchoolLogo(True).Image
                Session("OfficialSchoolLogo") = rptParamInfo.SchoolLogo
            End If

            If rptParamInfo.FilterList.ToLower.IndexOf("campusid in", StringComparison.Ordinal) <> -1 Then
                strErrMsg = "Please select a single campus"
            End If
            If rptParamInfo.FilterList.ToLower.IndexOf("campusid", StringComparison.Ordinal) <> -1 Then
                rptParamInfo.CampusId = getCampusId(rptParamInfo.FilterList)
            Else
                rptParamInfo.CampusId = HttpContext.Current.Request("cmpid").ToString
            End If
            Return strErrMsg
        End Function


        ''' <summary>
        ''' Get directly the setting.
        ''' </summary>
        ''' <param name="settingName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function GetSettingString(settingName As String) As String
            Dim advAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                advAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                advAppSettings = New AdvAppSettings
            End If
            If Not IsNothing(advAppSettings.AppSettings(settingName)) Then
                Dim result As String = advAppSettings.AppSettings(settingName).ToString()
                Return result
            End If
            Return Nothing
        End Function


        ''' <summary>
        ''' This function enter all parameters to Report for MajorMinor.
        ''' </summary>
        ''' <param name="rptParamInfo">A filled rptParamInfo.</param>
        ''' <remarks></remarks>
        Private Sub BuildMajorMinorReport(ByRef rptParamInfo As ReportParamInfo)
            'Provisional disclaimer stay here
            Const disclaimer As String = "This transcript is official only when signed by the Registrar and embossed with the School's raised seal. Federal Law prohibits the release of this document to any person or institution without the written consent of the student."
            'Create the object to hold parameters to Report.
            Dim listp = New List(Of ReportParameters)
            Dim param As ReportParameters = New ReportParameters With {.Name = "StudentId", .Label = "StudentId", .Value = rptParamInfo.StudentId}
            listp.Add(param)
            param = New ReportParameters With {.Name = "Imagecode", .Label = "Imagecode", .Value = "RSTRAN1"} ' Image Code for This report...
            listp.Add(param)
            param = New ReportParameters With {.Name = "EnrollmentId", .Label = "EnrollmentId", .Value = rptParamInfo.StuEnrollId}
            listp.Add(param)
            If (rptParamInfo.StartDate.ToString = "1/1/0001 12:00:00 AM") Then
                rptParamInfo.StartDate = New DateTime(1900, 1, 1)
            End If
            param = New ReportParameters With {.Name = "FilterStartDate", .Label = "FilterStartDate", .Value = rptParamInfo.StartDate.ToString("yyyy-MM-dd")}
            listp.Add(param)
            If (rptParamInfo.EndDate.ToString = "1/1/0001 12:00:00 AM") Then
                Dim now As DateTime = DateTime.Now
                rptParamInfo.EndDate = now
            End If
            param = New ReportParameters With {.Name = "FilterEndDate", .Label = "FilterEndDate", .Value = rptParamInfo.EndDate.ToString("yyyy-MM-dd")}
            listp.Add(param)
            param = New ReportParameters With {.Name = "IsOfficialTranscript", .Label = "IsOfficialTranscript", .Value = rptParamInfo.ShowOfficialTranscript.ToString()}
            listp.Add(param)
            param = New ReportParameters With {.Name = "IsShowedSignatureLine", .Label = "IsShowedSignatureLine", .Value = (Not (rptParamInfo.ShowUseSignLineForAttnd)).ToString()}
            listp.Add(param)
            If rptParamInfo.ShowLegalDisclaimer Then
                param = New ReportParameters With {.Name = "LegalDisclaimer", .Label = "LegalDisclaimer", .Value = disclaimer}
            Else
                param = New ReportParameters With {.Name = "LegalDisclaimer", .Label = "LegalDisclaimer", .Value = Nothing}
            End If
            listp.Add(param)
            param = New ReportParameters With {.Name = "SignatureLegalDisclaimer", .Label = "SignatureLegalDisclaimer", .Value = "Signature of Registrar"}
            listp.Add(param)
            param = New ReportParameters With {.Name = "IsHeaderShowed", .Label = "IsHeaderShowed", .Value = (Not (rptParamInfo.SuppressHeader)).ToString()}
            listp.Add(param)

            param = New ReportParameters With {.Name = "CGPASetting", .Label = "CGPASetting", .Value = rptParamInfo.SettingRetakeTestPolicy}
            listp.Add(param)

            param = New ReportParameters With {.Name = "ShowTermProgressDescription", .Label = "ShowTermProgressDescription", .Value = rptParamInfo.ShowTermProgressDescription.ToString()}
            listp.Add(param)

            'Cleanup ListFilterString
            Dim params As String
            If (Not String.IsNullOrEmpty(rptParamInfo.FilterListString)) AndAlso (Not String.IsNullOrWhiteSpace(rptParamInfo.FilterListString)) Then
                Dim indexlow As Int32 = rptParamInfo.FilterListString.IndexOf("(", StringComparison.Ordinal)
                Dim indexHigh As Int32 = rptParamInfo.FilterListString.LastIndexOf(")", StringComparison.Ordinal)
                If indexHigh < 0 Then
                    indexlow = rptParamInfo.FilterListString.ToLower().IndexOf("to", StringComparison.Ordinal)
                    params = rptParamInfo.FilterListString.Substring(indexlow + 2).Trim()
                Else
                    params = rptParamInfo.FilterListString.Substring(indexlow + 1, (indexHigh - indexlow) - 1)
                    params = params.Replace("'", String.Empty).Replace(", ", ",")
                End If

                rptParamInfo.FilterListString = params

            End If
            If rptParamInfo.FilterListString = "" Then
                param = New ReportParameters With {.Name = "FilterTermDescription", .Label = "FilterTermDescription", .Value = Nothing}
            Else
                param = New ReportParameters With {.Name = "FilterTermDescription", .Label = "FilterTermDescription", .Value = rptParamInfo.FilterListString}
            End If
            'param = New ReportParameters With {.Name = "FilterTermDescription", .Label = "FilterTermDescription", .Value = rptParamInfo.FilterListString}
            listp.Add(param)
            'Select report path and name....
            Dim strReportPathAndName As String = ConfigurationManager.AppSettings("Reports.ReportsFolder") + "TranscriptSingleStudent/TranscriptIndividualStudent"
            ' Invoque the report....
            Dim report As ReportGenerator2 = New ReportGenerator2()
            Dim getReportAsBytes As [Byte]() = report.RenderReport2("pdf", UserId, "", "", strReportPathAndName, listp)
            ExportReport(rptParamInfo.ExportType, getReportAsBytes)
        End Sub

        Private Sub ExportReport(ByVal exportFormat As String, ByVal getReportAsBytes As [Byte]())
            Dim strExtension, strMimeType As String
            Select Case exportFormat.ToLower
                Case Is = "pdf"
                    strExtension = "pdf"
                    strMimeType = "application/pdf"
                    Exit Select
                Case "excel"
                    strExtension = "xls"
                    strMimeType = "application/vnd.excel"
                    Exit Select
                Case "word"
                    strExtension = "doc"
                    strMimeType = "application/vnd.ms-word"
                Case "csv"
                    strExtension = "csv"
                    strMimeType = "text/csv"
                Case Else
                    Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
            End Select
            Session("SSRS_FileExtension") = strExtension
            Session("SSRS_MimeType") = strMimeType
            Session("SSRS_ReportOutput") = getReportAsBytes
            'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
            Const script As String = "window.open('../SY/DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');"
            ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
        End Sub

    End Class
End Namespace