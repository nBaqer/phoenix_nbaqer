﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AfaIntegration.aspx.cs" Inherits="SY_AfaIntegration" MasterPageFile="~/NewSite.master" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>AFA Integration</title>
    <link href="../css/SY/AfaIntegration.css" rel="stylesheet" />
    <script src="../Scripts/Fame.Advantage.API.Client.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <div class="page-loading"></div>
    <br />
    <div id="dvAfaIntegration"  class="tabsForm" style="float: left">
        <div>
            <label id ="lblTitle" class="label-bold-title">AFA Integration</label>
        </div>
        <br />
        <div class="clearfix bottomSpace">
            <button type="button" id="btnSyncDemographics" class="k-button">Sync All Demographics</button>
        </div>
        <div class="clearfix bottomSpace">
            <%--<div class="formRow">
                <div style="margin-right: 20px; box-shadow: 0 1px 2px 1px rgba(0,0,0,.08), 0 3px 6px rgba(0,0,0,.08); border: 1px solid rgba(20,53,80,0.14);">
                    <div id="gvDemographics"></div>
                    <div style="padding: 5px; border-color: #a3d0e4; background-color: #d9ecf5;">
                        <button id="printUsersBtn" type="button" class="k-button" data-bind="enabled: canPrint">Print</button>
                    </div>
                </div>
            </div>--%>
        </div>
        <div class="clearfix bottomSpace">
            <button type="button" id="btnSyncProgramVersion" class="k-button">Sync All Program Versions</button>
        </div>
        <div class="clearfix bottomSpace">
            <button type="button" id="btnSyncEnrollments" class="k-button">Sync All Enrollments</button>
        </div>
        <div class="clearfix bottomSpace">
            <button type="button" id="btnSyncPaymentPeriod" class="k-button">Sync All Payment Periods</button>
        </div>
        <div class="clearfix bottomSpace">
            <button type="button" id="btnSyncPaymentPeriodAttendance" class="k-button">Sync All PaymentPeriod Attendance</button>
        </div>
        <div class="clearfix bottomSpace">
            <button type="button" id="btnSyncAwards" class="k-button">Sync All Awards</button>
        </div>
        <div class="clearfix bottomSpace">
            <button type="button" id="btnSyncDisbursements" class="k-button">Sync All Disbursements</button>
        </div>
        <div class="clearfix bottomSpace">
            <%--<button type="button" id="btnSyncRefunds" class="k-button">Sync All Refunds</button>--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            var afaIntegration = new Api.ViewModels.System.AfaIntegration();
            //$("lblTitle").text = afaIntegration.getAfaSessionKey();
        });
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>
