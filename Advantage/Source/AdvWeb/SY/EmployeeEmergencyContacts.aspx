﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="EmployeeEmergencyContacts.aspx.vb" Inherits="EmployeeEmergencyContacts" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<%--<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>

            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop2">
                        <br />
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="15%" nowrap align="left">
                                                <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                            </td>
                                            <td width="85%" nowrap>
                                                <asp:RadioButtonList ID="radStatus" CssClass="label" AutoPostBack="true" runat="Server"
                                                    RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Active" Selected="True" />
                                                    <asp:ListItem Text="Inactive" />
                                                    <asp:ListItem Text="All" />
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="listframebottom">
                                    <div class="scrollleftfilters">
                                        <asp:DataList ID="dlstEmployeeEmergencyContacts" DataKeyField="EmployeeEmergencyContactId"
                                            runat="server">
                                            <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                            <ItemStyle CssClass="itemstyle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgInActive" runat="server" CausesValidation="False" Visible='<%# not Ctype(Container.DataItem("StatusSelect"), boolean) %>'
                                                    ImageUrl="../images/Inactive.gif" />
                                                <asp:ImageButton ID="imgActive" runat="server" CausesValidation="False" Visible='<%# Ctype(Container.DataItem("StatusSelect"), boolean) %>'
                                                    ImageUrl="../images/Active.gif" />
                                                <asp:LinkButton ID="Linkbutton1" runat="server" CssClass="itemstyle" CausesValidation="False"
                                                    Text='<%# Container.DataItem("EmployeeEmergencyContactName") + " (" + Container.DataItem("Relation")+ ")" %>'
                                                    CommandArgument='<%# Container.DataItem("EmployeeEmergencyContactId")%>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->
                            <table width="60%" align="center">
                                <asp:TextBox ID="txtEmployeeEmergencyContactId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox
                                    ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="lblEmployeeEmergencyContactName" runat="server" CssClass="label">Name</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtEmpId" runat="server" Visible="False"></asp:TextBox><asp:TextBox
                                            ID="txtEmployeeEmergencyContactName" runat="server" CssClass="textbox"
                                            MaxLength="50" TabIndex="1"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="revName" runat="server" ControlToValidate="txtEmployeeEmergencyContactName"
                                            Display="None" ErrorMessage="Invalid Name" ValidationExpression=".{0,50}"></asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="txtEmployeeEmergencyContactName"
                                            Display="None" ErrorMessage="Name can not be blank">Name can not be blank</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:DropDownList ID="ddlStatusId" runat="server" TabIndex="2" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell">
                                        <asp:Label ID="lblRelation" CssClass="label" runat="server">Relation</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:DropDownList ID="ddlRelation" TabIndex="3" runat="server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                        <asp:CompareValidator ID="cvRelationship" runat="server" ErrorMessage="Must Select a Relation"
                                            Display="None" ControlToValidate="ddlRelation" Operator="GreaterThan" ValueToCompare="0">Must Select a Relation</asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables"></td>
                                </tr>
                                <tr>
                                    <td class="columnheader" colspan="2" align="left">Phones
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables"></td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblWorkPhone" CssClass="label" runat="server">Work</asp:Label>
                                    </td>
                                    <td class="contentcell4" style="line-height: 30px;">
                                        <%-- <ew:MaskedTextBox ID="txtWorkPhone" runat="server" CssClass="textbox" TabIndex="5"
                                        Width="200px">
                                    </ew:MaskedTextBox>--%>

                                        <telerik:RadMaskedTextBox ID="txtWorkPhone" TabIndex="5" runat="server" CssClass="textbox"
                                            DisplayFormatPosition="Right" Mask="(###)-###-####" DisplayMask="(###)-###-####"
                                            DisplayPromptChar="" Width="200px">
                                        </telerik:RadMaskedTextBox>
                                        <asp:CheckBox ID="chkForeignWorkPhone" TabIndex="4" AutoPostBack="true" runat="server"
                                            Text="International" CssClass="checkboxinternational"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblPassword" CssClass="label" runat="server">Home</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <%-- <ew:MaskedTextBox ID="txtHomePhone" runat="server" TabIndex="7" CssClass="textbox"
                                        Width="200px">
                                    </ew:MaskedTextBox>--%>
                                        <telerik:RadMaskedTextBox ID="txtHomePhone" TabIndex="7" runat="server" CssClass="textbox"
                                            DisplayFormatPosition="Right" Mask="(###)-###-####" DisplayMask="(###)-###-####"
                                            DisplayPromptChar="" Width="200px">
                                        </telerik:RadMaskedTextBox>
                                        <asp:CheckBox ID="chkForeignHomePhone" TabIndex="6" AutoPostBack="true" runat="server"
                                            Text="International" CssClass="checkboxinternational"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblCell" runat="server" CssClass="label">Cell</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <%--<ew:MaskedTextBox ID="txtCellPhone" runat="server" TabIndex="9" CssClass="textbox"
                                        Width="200px">
                                    </ew:MaskedTextBox>--%>
                                        <telerik:RadMaskedTextBox ID="txtCellPhone" TabIndex="9" runat="server" CssClass="textbox"
                                            DisplayFormatPosition="Right" Mask="(###)-###-####" DisplayMask="(###)-###-####"
                                            DisplayPromptChar="" Width="200px">
                                        </telerik:RadMaskedTextBox>
                                        <asp:CheckBox ID="chkForeignCellPhone" TabIndex="8" AutoPostBack="true" runat="server"
                                            Text="International" CssClass="checkboxinternational"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblBeeper" runat="server" CssClass="label">Beeper</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtBeeper" runat="server" TabIndex="10" CssClass="textbox"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" Display="None"
                                            ErrorMessage="Invalid Beeper Number" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}(\sx\d(\d)?(\d)?(\d)?(\d)?)?"
                                            ControlToValidate="txtBeeper" Enabled="False"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables"></td>
                                </tr>
                                <tr>
                                    <td class="columnheader" colspan="2" align="left">E-mails
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables"></td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblWork" runat="server" CssClass="label">Work</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtWorkEmail" TabIndex="11" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="None"
                                            ErrorMessage="Invalid Work Email Address" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ControlToValidate="txtWorkEmail"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblHome" runat="server" CssClass="label">Home</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtHomeEmail" TabIndex="12" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="None"
                                            ErrorMessage="Invalid Home Email Address" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ControlToValidate="txtHomeEmail"></asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                            <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td class="spacertables"></td>
                                    </tr>
                                    <tr>
                                        <td class="contentcellheader" nowrap colspan="6">
                                            <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables"></td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell2" colspan="6">
                                            <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:TextBox ID="txtRowIds" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="txtResourceId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

