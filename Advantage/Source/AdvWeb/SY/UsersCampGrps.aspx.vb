Imports System.Xml
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data

Partial Class UsersCampGrps
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            'Get the selected UserId from the querystring
            Dim userID As String

            userID = XmlConvert.ToGuid(Request.Params("UID")).ToString
            ' userID = AdvantageSession.UserState.UserId.ToString
            'Call code to build the list boxes
            BuildListBoxes(userID)

        End If
    End Sub

    Private Sub BuildListBoxes(ByVal userId As String)

        BuildRolesList()
    End Sub

    Private Sub BuildAvailableList(ByVal userId As String, ByVal roleId As String)
        Dim facUserSecurit As New UserSecurityFacade
        Dim dt As New DataTable

        dt = facUserSecurit.GetUserAvailableRolesCampusGroups(userId, roleId)

        With lbxCampusGroups
            .DataSource = dt
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataBind()
        End With
    End Sub

    Private Sub BuildSelectedList(ByVal userId As String, ByVal roleId As String)
        Dim facUserSecurit As New UserSecurityFacade
        Dim dt As New DataTable

        dt = facUserSecurit.GetUserAssignedRolesCampusGroups(userId, roleId)

        With lbxSelected
            .DataSource = dt
            .DataTextField = "Description"
            .DataValueField = "CampGrpId"
            .DataBind()
        End With

        'Place the DataTable in session
        Session("SelectedDT") = dt

    End Sub

    Private Sub BuildRolesList()
        Dim facUserSecurit As New UserSecurityFacade
        Dim dt As New DataTable

        dt = facUserSecurit.GetAllRoles()

        With lbxRoles
            .DataSource = dt
            .DataTextField = "Role"
            .DataValueField = "RoleId"
            .DataBind()
        End With
    End Sub

    Private Sub lbxCampusGroups_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxCampusGroups.SelectedIndexChanged
        'Enable the Add button if an item is selected
        If lbxCampusGroups.SelectedIndex <> -1 Then
            btnAdd.Enabled = True
        End If

        'No item should be selected in the selected lbx
        If lbxSelected.SelectedIndex <> -1 Then
            lbxSelected.SelectedIndex = -1
        End If

        'Disable the remove button
        btnRemove.Enabled = False

    End Sub

    Private Sub lbxRoles_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxRoles.SelectedIndexChanged
        'When a role is selected we want to populate the campus groups lbx with campus groups
        'that use that role that have not already been assigned for the selected user and selected role.
        'We also want to populate the selected lbx with the campus groups that have already been assigned
        'for that role.
        Dim userID As String

        userID = XmlConvert.ToGuid(Request.Params("UID")).ToString
        'userID = AdvantageSession.UserState.UserId.ToString
        BuildAvailableList(userID, lbxRoles.SelectedValue)
        BuildSelectedList(userID, lbxRoles.SelectedValue)


        'Make certain the remove button is disabled
        btnRemove.Enabled = False


    End Sub

    Private Sub lbxSelected_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxSelected.SelectedIndexChanged
        'Enable the remove button if an item is selected
        If lbxSelected.SelectedIndex <> -1 Then
            btnRemove.Enabled = True
        End If

        'No item should be selected in the campus groups lbx
        If lbxCampusGroups.SelectedIndex <> -1 Then
            lbxCampusGroups.SelectedIndex = -1
        End If

        'Disable the add button
        btnAdd.Enabled = False
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'Add the selected item in the campus groups lbx to the selected lbx
        lbxSelected.Items.Add(New ListItem(lbxCampusGroups.SelectedItem.Text, lbxCampusGroups.SelectedItem.Value))
        'Remove the selected item from the campus groups lbx
        lbxCampusGroups.Items.RemoveAt(lbxCampusGroups.SelectedIndex)
        'Disable the Add button
        btnAdd.Enabled = False
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        'Add the selected item in the selected lbx to the campus groups lbx
        lbxCampusGroups.Items.Add(New ListItem(lbxSelected.SelectedItem.Text, lbxSelected.SelectedItem.Value))
        'Remove the selected item from the selected lbx
        lbxSelected.Items.RemoveAt(lbxSelected.SelectedIndex)
        'Disable the remove button
        btnRemove.Enabled = False
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'If the Available Campus Groups and Selected Campus Groups list boxes are both empty
        'then the user should be alerted and the rest of the code should not execute
        If lbxCampusGroups.Items.Count = 0 And lbxSelected.Items.Count = 0 Then
            DisplayErrorMessage("Please make a selection to save")
        Else
            'There are no referential integrity issues involved so we will simply delete all the campus groups for
            'the user and selected role. We will then insert the selected campus groups in the selected lbx.
            Dim fac As New UserSecurityFacade
            Dim userID As String
            Dim iCounter As Integer

            'Get the selected UserId from the querystring
            userID = XmlConvert.ToGuid(Request.Params("UID")).ToString
            ' userID = AdvantageSession.UserState.UserId.ToString

            fac.DeleteUserRoleCampusGroups(userID, lbxRoles.SelectedValue)

            'Add each item in the lbxSelected control to the database
            If lbxSelected.Items.Count > 0 Then
                For iCounter = 0 To lbxSelected.Items.Count - 1
                    fac.AddUserRoleCampusGroup(userID, lbxRoles.SelectedValue, lbxSelected.Items(iCounter).Value, Session("UserName"))
                Next
                fac.InsertMRUsForFirstTimeUser(userID.ToString)
            End If

            'Added by Michelle R. Rodriguez on 01/17/2005.
            'Each time there is a save we want to update the rptAdmissionsRep and rptInstructors tables.
            'They will be used to populate the Filter Values listbox in the report params pages.
            Dim rptExcept As New ReportExceptionsFacade
            rptExcept.RefreshAdmissionsRep(HttpContext.Current.Request.Params("cmpid"))
            rptExcept.RefreshInstructor()

            ''Each time there is a save we want to check if the user has been assigned to a campus group that
            ''includes the default campus. If not, we want to display an alert informing the user and also let
            ''him/her know which campus groups include the default campus.
            'If fac.UserHasAccessToDefaultCampus(userID) Then
            '    'Do nothing
            'Else
            '    Dim msg As String
            '    Dim dt As DataTable
            '    Dim campusId As String
            '    Dim dr As DataRow
            '    Dim campusGrps As String

            '    campusId = fac.GetUserDefaultCampus(userID)
            '    dt = fac.GetCampusGroupsWithCampus(campusId)

            '    'If dt has rows then loop through them to add the campus groups that has the default campus
            '    If dt.Rows.Count > 0 Then
            '        For Each dr In dt.Rows
            '            campusGrps &= dr("CampGrpDescrip") & vbCr
            '        Next
            '    End If

            '    msg = "Your record was saved. However, the user has not been assigned access to his/her default campus. " & vbCr
            '    If campusGrps <> "" Then
            '        msg &= "In order to do this you must assign the user to a role for one of the following campus groups:" & vbCr
            '        msg &= vbTab & campusGrps
            '    End If


            '    DisplayErrorMessage(msg)
            'End If
        End If


    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
