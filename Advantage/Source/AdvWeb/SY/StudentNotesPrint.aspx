﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StudentNotesPrint.aspx.vb" Inherits="AdvWeb.SY.StudentNotesPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Student Notes</title>

    <link href="../css/LeadNotes.css" rel="stylesheet" />
    <script src="../Kendo/js/jquery.min.js"></script>
    <script src="../Kendo/js/kendo.all.min.js"></script>
    <script src="../Scripts/Advantage.Client.AD.js"></script>


</head>
<body>
    <form id="form1" runat="server">
        <article id="printInfoPage" style="width: 7.5in;">
            <div id="printWrapper">
                <div id="printHeaderRight">
                    <div id="printCampus">Campus</div>
                    <div id="printModule">ACADEMIC NOTES</div>
                    <div id="printDateTime">--/--/--</div>
                </div>

                <div class="clearfix"></div>
                <br />
                <label class="hrcol1">Student:</label>
                <input class="hrcol2" id="hrStudentFullName" readonly="readonly" />
                <label class="hrcol3">Program Version:</label>
                <input class="hrcol4" id="hrProgramVersion" readonly="readonly" />
                <div class="clearfix"></div>
                <label class="hrcol1">Status:</label>
                <input class="hrcol2" id="hrStudentStatus" readonly="readonly" />
                <label class="hrcol3">Start Date:</label>
                <input class="hrcol4" id="hrStartDate" readonly="readonly" />
                <div class="clearfix"></div>
                <label class="hrcol1">Student Id:</label>
                <input class="hrcol2" id="hrStudentId" readonly="readonly" />
                <div class="clearfix"></div>
                <label class="hrcol1">Grad Date:</label>
                <input class="hrcol2" id="hrGradDate" readonly="readonly" />
                <div class="clearfix"></div>
                <br />

                <div id="corpusReport" style="width: 7.5in; border: 2px solid gainsboro">
                    <table id="LeadNotes" class="LNStyle">
                    </table>
                </div>
            </div>
        </article>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            var moduleCode = "AR";
            var manager = new AD.LeadNotesPrint(moduleCode);
        });
    </script>
</body>
</html>
