
Imports AdvWeb.VBCode.ComponentClasses
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data
Imports FAME.AdvantageV1.Common

Partial Class ReportExportViewer
    Inherits Page

    Protected PageTitle As String
    Dim objReport As ReportDocument

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        LoadReport()
    End Sub

#End Region

    Private Sub LoadReport()
        Dim rptXport As New RptExport
        Dim ds As DataSet
        Dim rptParamInfo As Object

        Select Case Session("RptAgency")
            Case IPEDSCommon.AgencyName
                rptParamInfo = DirectCast(Session("RptInfo"), ReportParamInfoIPEDS)
            Case Else
                rptParamInfo = DirectCast(Session("RptInfo"), ReportParamInfo)
        End Select

        PageTitle &= CType(rptParamInfo.RptTitle, String)

        ds = DirectCast(Session("ReportDS"), DataSet)

        If Session("RptObject") IsNot Nothing Then
            Select Case Session("RptAgency")
                Case IPEDSCommon.AgencyName
                    objReport = DirectCast(Session("RptObject"), AdvantageReportIPEDS)
                Case Else
                    objReport = DirectCast(Session("RptObject"), ReportDocument)
            End Select
        Else
            Select Case Session("RptAgency")
                Case IPEDSCommon.AgencyName
                    objReport = New AdvantageReportIPEDS(ds, CType(rptParamInfo, ReportParamInfoIPEDS))
                Case Else
                    objReport = (New AdvantageReportFactory).CreateAdvantageReport(ds, CType(rptParamInfo, ReportParamInfo))
            End Select
            Session("RptObject") = objReport
        End If

        'rptXport.Export(Context, objReport, Request.QueryString("ExportTo"), rptParamInfo.RptTitle.ToString)

        Select Case Trim(Request.QueryString("ExportTo")).ToLower
            Case "pdf"
                rptXport.ExportToPdf(Context, objReport, Session.SessionID.ToString, rptParamInfo.RptTitle.ToString)
            Case "xls"
                rptXport.ExportToMsExcel(Context, objReport, Session.SessionID.ToString, rptParamInfo.RptTitle.ToString)
            Case "doc"
                rptXport.ExportToMsWord(Context, objReport, Session.SessionID.ToString, rptParamInfo.RptTitle.ToString)
            Case "xls1"
                rptXport.ExportToMsExcelData(Context, objReport, Session.SessionID.ToString, rptParamInfo.RptTitle.ToString)
        End Select

    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        Page.DataBind()
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Unload
        If (Not objReport Is Nothing) Then
            objReport.Close()
            objReport.Dispose()
        End If
    End Sub

End Class
