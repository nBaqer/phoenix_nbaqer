﻿<%@ Page Title="File Storage Settings" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" EnableEventValidation="false" CodeFile="FileStorageSettings.aspx.vb" Inherits="SY_FileStorageSettings" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../css/AdvantageValidator.css" rel="stylesheet" />
    <link href="../css/SY/StateBoardSettings.css" rel="stylesheet" />
    <link href="../css/AR/font-awesome.css" rel="stylesheet" />


    <script type="text/javascript" src="../Scripts/Storage/storageCache.js"></script>
    <script type="text/javascript" src="../Scripts/common-util.js"></script>
    <script src="../Scripts/Fame.Advantage.API.Client.js"></script>

    <link rel="stylesheet" type="text/css" href="../CSS/ImportLeads.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            var fileStorage = new Api.ViewModels.Maintenance.FileStorage();
            fileStorage.initialize();
        });
    </script>

    <style type="text/css">
        #dropReasonNaccasMappingGrid span.ddlAdvantageDropReason {
            width: 100%;
        }

        .k-multiselect:after {
            content: "\25BC";
            position: absolute;
            top: 30%;
            right: 25px;
            font-size: 12px;
        }

        #campusDetailsContent {
            padding: 15px;
        }

        .step1-margin > .form-row > .form-label > label {
            font-weight: 500 !important;
            font-size: 10pt !important;
            line-height: 18pt !important
        }

        .step1-margin > .form-row > .form-label {
            width: 50%;
            display: inline;
            float: left;
        }

        .step1-margin > .form-row > .form-input {
            width: 45%;
            display: inline;
        }

        .k-grid-content {
            overflow-y: hidden !important
        }

        #approvedNaccasProgramsGrid, #dropReasonNaccasMappingGrid, #campusDetailsContent {
            padding: 10px;
            background-color: white
        }

            #approvedNaccasProgramsGrid .k-grid-content {
                height: auto !important;
            }

        tbody:empty:before {
            content: ''
        }

        .k-grid-header {
            padding-right: 0px !important
        }

        @media only screen and ( max-width: 1440px) {
            .step1-margin > .form-row > .form-label {
                width: 70%;
            }

            .step1-margin > .form-row > .form-input {
                width: 25%;
            }
        }

        @media only screen and (min-width: 1441px) and ( max-width: 1600px) {
            .step1-margin > .form-row > .form-label {
                width: 60%;
            }

            .step1-margin > .form-row > .form-input {
                width: 25%;
            }
        }

        .switch-input {
            display: none;
        }

        .switch-label {
            position: relative;
            display: inline-block;
            min-width: 112px;
            cursor: pointer;
            font-weight: 500;
            text-align: left;
            margin: 16px;
        }

            .switch-label:before, .switch-label:after {
                content: "";
                position: absolute;
                margin: 0;
                outline: 0;
                top: 50%;
                -ms-transform: translate(0, -50%);
                -webkit-transform: translate(0, -50%);
                transform: translate(0, -50%);
                -webkit-transition: all 0.3s ease;
                transition: all 0.3s ease;
            }

            .switch-label:before {
                left: 1px;
                width: 34px;
                height: 14px;
                background-color: #9E9E9E;
                border-radius: 8px;
            }

            .switch-label:after {
                left: 0;
                width: 20px;
                height: 20px;
                background-color: #FAFAFA;
                border-radius: 50%;
                box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.14), 0 2px 2px 0 rgba(0, 0, 0, 0.098), 0 1px 5px 0 rgba(0, 0, 0, 0.084);
            }

        .switch-input:checked + .switch-label:before {
            background-color: rgba(63, 81, 181, 0.5);
        }

        .switch-input:checked + .switch-label:after {
            background-color: #3f51b5;
            -ms-transform: translate(80%, -50%);
            -webkit-transform: translate(80%, -50%);
            transform: translate(80%, -50%);
        }

        .switch-input:checked + .switch-label .toggle--on {
            display: inline-block;
        }

        .switch-input:checked + .switch-label .toggle--off {
            display: none;
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
    <%-- Get the Current Campus from the Server Side and put it into a hidden field that is accesible --%>
    <asp:HiddenField runat="server" ID="hdnModUser" ClientIDMode="Static" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <div class="boxContainer">
        <h3>
            <asp:Label ID="headerTitle" runat="server"></asp:Label>
        </h3>
        <div class="page-wrapper">
            <div id="mainConfigSection">

                <div class="formRow">
                    <div class="left leftLable" style="margin-right: 15px; width: 200px;">
                        Campus Group <span class="red-color">*</span>
                    </div>
                    <div class="inLineBlock left-margin">
                        <input id="ddlCampusGroup" class="fieldlabel1 inputCustom form-control" required />
                    </div>
                </div>

                <div class="formRow">
                    <div class="left leftLable" style="margin-right: 15px; width: 200px;">
                        File Storage Type <span class="red-color">*</span>
                    </div>
                    <div class="inLineBlock left-margin">
                        <input id="ddlFileStorageTypes" class="fieldlabel1 inputCustom" required />
                    </div>
                </div>
                <div class="formRow" id="inputPathContainer" hidden>
                    <div class="left leftLable" style="margin-right: 15px; width: 200px;">
                        <span id="pathMainLabel">File Path</span><span class="red-color">*</span>
                    </div>
                    <div class="inLineBlock left-margin">
                        <input id="inputPathMain" class="fieldlabel1 inputCustom k-textbox" data-validation="required-if-visible" />
                    </div>
                </div>
                <div class="formRow" id="inputCloudKeyContainer" hidden>
                    <div class="left leftLable" style="margin-right: 15px; width: 200px;">
                        <span id="cloudKeyMainLabel">Cloud storage key</span><span class="red-color">*</span>
                    </div>
                    <div class="inLineBlock left-margin">
                        <input id="inputCloudKeyMain" class="fieldlabel1 inputCustom k-textbox" data-validation="required-if-visible" />
                    </div>
                </div>
                <div id="inputUserCredentialsContainer" hidden>
                    <div class="formRow">
                        <div class="left leftLable" style="margin-right: 15px; width: 200px;">
                            <span id="userNameLabel">Username</span><span class="red-color">*</span>
                        </div>
                        <div class="inLineBlock left-margin">
                            <input id="inputUserNameMain" class="fieldlabel1 inputCustom k-textbox" data-validation="required-if-visible" />
                        </div>

                    </div>

                    <div class="formRow">
                        <div class="left leftLable" style="margin-right: 15px; width: 200px;">
                            <span id="passwordLabel">Password</span><span class="red-color">*</span>
                        </div>
                        <div class="inLineBlock left-margin">
                            <input id="inputPasswordMain" class="fieldlabel1 inputCustom k-textbox" data-validation="required-if-visible" />
                        </div>
                    </div>
                </div>


                <div class="formRow" id="reportTypeDDRow">
                    <div class="left leftLable" style="margin-right: 15px; width: 200px;">
                        Applies to all Features
                    </div>
                    <input type="checkbox" id="features-switch" class="switch-input" aria-label="AcrossAllFeatures Switch" checked="checked" />
                    <label for="features-switch" class="switch-label"></label>
                </div>

            </div>


            <%-- File Features section --%>
            <div id="fileFeaturesSection" class="section" style="width: 900px;">
                <h3 style="margin-top: 20px; margin-left: 20px;">Feature Configuration</h3>
                <div id="fileFeaturesGrid" style="margin: 20px;"></div>
            </div>

            <div class="formRow">
                <div class="buttonDiv">
                    <hr class="top-margin20px">
                    <div class="inLineBlock top-margin7px">

                        <button type="button" id="btnSave">
                            Save
                        </button>

                        <button type="button" id="btnDelete">
                            Delete
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
