﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AfaIntegration.aspx.cs" company="Fame INC">
//   2018
// </copyright>
// <summary>
//   Defines the SY_AfaIntegration type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

/// <summary>
/// The AFA integration.
/// </summary>
public partial class SY_AfaIntegration : BasePage
{
    /// <summary>
    /// The page_ load.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void Page_Load(object sender, EventArgs e)
    {
    }
}