﻿<%@ Page Title="State Board/Accrediting Agency Settings" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" EnableEventValidation="false" CodeFile="StateBoardSettings.aspx.vb" Inherits="SY_StateBoardSettings" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../css/AdvantageValidator.css" rel="stylesheet" />
    <%--<link href="../css/AR/StudentTermination.css" rel="stylesheet" />--%>
    <link href="../css/SY/StateBoardSettings.css" rel="stylesheet" />
    <link href="../css/AR/font-awesome.css" rel="stylesheet" />


    <script type="text/javascript" src="../Scripts/Storage/storageCache.js"></script>
    <script type="text/javascript" src="../Scripts/common-util.js"></script>
    <script src="../Scripts/Fame.Advantage.API.Client.js"></script>

    <link rel="stylesheet" type="text/css" href="../CSS/ImportLeads.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {
            var hdnHostName = $("#hdnHostName").val();
            var masterPageVm = new Api.ViewModels.Maintenance.StateBoardAgencyMaster();
            masterPageVm.initialize();
        });
    </script>

    <style type="text/css">
        #dropReasonNaccasMappingGrid span.ddlAdvantageDropReason {
            width: 100%;
        }

        .k-multiselect:after {
            content: "\25BC";
            position: absolute;
            top: 30%;
            right: 25px;
            font-size: 12px;
        }

        #campusDetailsContent {
            padding: 15px;
        }

        .step1-margin > .form-row > .form-label > label {
            font-weight: 500 !important;
            font-size: 10pt !important;
            line-height: 18pt !important
        }

        .step1-margin > .form-row > .form-label {
            width: 50%;
            display: inline;
            float: left;
        }

        .step1-margin > .form-row > .form-input {
            width: 45%;
            display: inline;
        }

        .k-grid-content {
            overflow-y: hidden !important
        }

        #approvedNaccasProgramsGrid, #dropReasonNaccasMappingGrid, #campusDetailsContent {
            padding: 10px;
            background-color: white
        }

        #approvedNaccasProgramsGrid .k-grid-content {
            height: auto !important;
        }

        tbody:empty:before {
            content: ''
        }

        .k-grid-header {
            padding-right: 0px !important
        }

        @media only screen and ( max-width: 1440px) {
            .step1-margin > .form-row > .form-label {
                width: 70%;
            }

            .step1-margin > .form-row > .form-input {
                width: 25%;
            }
        }

        @media only screen and (min-width: 1441px) and ( max-width: 1600px) {
            .step1-margin > .form-row > .form-label {
                width: 60%;
            }

            .step1-margin > .form-row > .form-input {
                width: 25%;
            }
        }
    </style>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
    <%-- Get the Current Campus from the Server Side and put it into a hidden field that is accesible --%>
    <asp:HiddenField runat="server" ID="hdnHostName" ClientIDMode="Static" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <div class="boxContainer">
        <h3>
            <asp:Label ID="headerTitle" runat="server"></asp:Label>
        </h3>
        <div class="page-wrapper">
           
            <%-- Shared dropdowns --%>
            <div id="sharedDropDowns">
                <div class="formRow">
                    <div class="left leftLable" style="margin-right: 15px; width: 200px;">
                        State board/accrediting agency <span class="red-color">*</span>
                    </div>
                    <div class="inLineBlock left-margin">
                        <input id="ddStateBoardStates" class="fieldlabel1 inputCustom" required />
                    </div>
                </div>
                <div class="formRow" id="reportTypeDDRow">
                    <div class="left leftLable" style="margin-right: 15px; width: 200px;">
                        Report type <span class="red-color">*</span>
                    </div>
                    <div class="inLineBlock left-margin">
                        <input id="ddStateBoardReports" class="fieldlabel1 inputCustom form-control" required />
                    </div>
                </div>

                <div class="formRow">
                    <div class="left leftLable" style="margin-right: 15px; width: 200px;">
                        Campus <span class="red-color">*</span>
                    </div>
                    <div class="inLineBlock left-margin">
                        <input id="ddCampus" class="fieldlabel1 inputCustom form-control" required />
                    </div>
                </div>

            </div>

            <%-- Indiana stateboard report section --%>
            <div id="indianaStateboardSection" class="section" style="display: none;">
                <div id="tabs" class="m-tabs">
                    <ul class="panelbar" id="panelBarDetailsStateBoard">
                        <li class="k-state-active" id="campusDetailsPanel">
                            <span>Campus Details</span>
                            <div class="step1-margin">
                                <br />

                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        Owner's name <span class="red-color">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="txtOwnerName" class="fieldlabel1 inputCustom form-control k-textbox" required />
                                    </div>
                                </div>

                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        School name <span class="red-color">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="txtSchoolName" class="fieldlabel1 inputCustom form-control k-textbox" required />
                                    </div>
                                </div>

                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        School address 1 <span class="red-color">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="txtSchoolAddress1" class="fieldlabel1 inputCustom form-control k-textbox" required />
                                    </div>
                                </div>

                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        School address 2 
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="txtSchoolAddress2" class="fieldlabel1 inputCustom form-control k-textbox" />
                                    </div>
                                </div>

                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        City <span class="red-color">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="txtCity" class="fieldlabel1 inputCustom form-control k-textbox" required />
                                    </div>
                                </div>

                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        Country <span class="red-color">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="ddCountry" class="fieldlabel1 inputCustom form-control" required />
                                    </div>
                                </div>

                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        State <span class="red-color">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="ddState" class="fieldlabel1 inputCustom form-control" required />
                                    </div>
                                </div>

                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        Zipcode <span class="red-color">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="txtZipCode" class="fieldlabel1 inputCustom form-control k-textbox" maxlength="5" required />
                                    </div>
                                </div>


                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        Owner's license number <span class="red-color">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="txtOwnerLicenseNumber" class="fieldlabel1 inputCustom form-control k-textbox" required />
                                    </div>
                                </div>

                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        Officer's name 1
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="txtOfficerName1" class="fieldlabel1 inputCustom form-control k-textbox" />
                                    </div>
                                </div>

                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        Officer's name 2
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="txtOfficerName2" class="fieldlabel1 inputCustom form-control k-textbox" />
                                    </div>
                                </div>

                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        Officer's name 3
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input id="txtOfficerName3" class="fieldlabel1 inputCustom form-control k-textbox" />
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="k-state-active" id="licensingDetailsPanel">
                            <span>State Board Licensing Agency Details</span>
                            <div class="step1-margin">
                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        Licensing agency <span style="color: red">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input type="text" id="ddlLicensingAgency" class="fieldlabel1 inputCustom form-control" maxlength="152" required />
                                    </div>
                                </div>
                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 155px;">
                                        Licensing agency address 1 <span style="color: red">*</span>
                                    </div>
                                    <div class="inLineBlock">
                                        <input type="text" id="licensingAgencyAddress1" class="fieldlabel1 inputCustom form-control k-textbox" maxlength="152" required />
                                    </div>
                                </div>
                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        Licensing agency address 2 
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input type="text" id="licensingAgencyAddress2" class="fieldlabel1 inputCustom form-control k-textbox" maxlength="152" />
                                    </div>
                                </div>
                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        Country <span style="color: red">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input type="text" id="licensingAgencyCountry" class="fieldlabel1 inputCustom form-control" maxlength="152" required />
                                    </div>
                                </div>
                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        City <span style="color: red">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input type="text" id="licensingAgencyCity" class="fieldlabel1 inputCustom form-control k-textbox" maxlength="152" required />
                                    </div>
                                </div>
                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        State <span style="color: red">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input type="text" id="licensingAgencyState" class="fieldlabel1 inputCustom form-control" maxlength="152" required />
                                    </div>
                                </div>
                                <div class="formRow">
                                    <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                                        Zipcode <span style="color: red">*</span>
                                    </div>
                                    <div class="inLineBlock left-margin">
                                        <input type="text" id="licensingAgencyZipCode" class="fieldlabel1 inputCustom form-control k-textbox" maxlength="152" data-validation="zip" required />
                                    </div>
                                </div>

                            </div>

                        </li>
                        <li class="k-state-active" id="mappingPanel">
                            <span>Campus Program Mapping to Licensing Agency Course Code</span>
                            <div class="step1-margin">
                                <div id="programCourseMappingGrid" style="height: 100%;"></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <%-- Naccas section --%>
            <div id="naccasSection" class="section" style="display: none;">
                <div id="tabs1" class="m-tabs">
                    <ul class="panelbar" id="panelBarDetailsNaccas">
                        <li class="k-state-active" id="mappingPanel1">
                            <span>Campus Details</span>
                            <div class="step1-margin" id="campusDetailsContent">
                                <div class="form-row">

                                    <div class="form-label">
                                        <label>Student allowed to graduate and still owe the school money <span class="red-color">*</span></label>
                                    </div>
                                    <div class="form-input">
                                        <input id="ddOweSchoolMoney" class="fieldlabel1 inputCustom form-control campusDetailInput" required />
                                    </div>
                                </div>
                                <br />
                                <div class="form-row">
                                    <div class="form-label">
                                        <label>Student allowed to graduate without completing all required tests, examinations, or subjects <span class="red-color">*</span></label>
                                    </div>
                                    <div class="form-input">
                                        <input id="ddAllowedToGraduate" class="fieldlabel1 inputCustom form-control campusDetailInput" required />
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="k-state-active" id="mappingPanel2">
                            <span>NACCAS approved program versions</span>
                            <div class="step1-margin">
                                <div id="approvedNaccasProgramsGrid"></div>
                            </div>
                        </li>
                        <li class="k-state-active" id="mappingPanel3">
                            <span>Advantage drop reason mapping to NACCAS drop reasons</span>
                            <div class="step1-margin">
                                <div id="dropReasonNaccasMappingGrid" style="height: auto !important"></div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="formRow">
                <div class="buttonDiv">
                    <hr class="top-margin20px">
                    <div class="inLineBlock top-margin7px">

                        <button type="button" id="btnSave">
                            Save
                        </button>
                        <button type="button" id="btnNew">
                            New
                        </button>
                        <button type="button" id="btnDelete" runat="server">
                            Delete
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
