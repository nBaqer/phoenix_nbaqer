﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" MasterPageFile="~/NewSite.master" CodeFile="NSLDSDataExport.aspx.vb" Inherits="NSLDSDataExport" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script type="text/javascript">


        function OnClientFileOpen(oExplorer, args) {
            var item = args.get_item();
            var fileExtension = item.get_extension();

            var fileDownloadMode = document.getElementById("chkbxDownoaldFile").checked;
            if ((fileDownloadMode == true) && (fileExtension == "jpg" || fileExtension == "gif" || fileExtension == "xls")) {// Download the file 
                // File is a image document, do not open a new window
                args.set_cancel(true);

                // Tell browser to open file directly
                var requestImage = "Handler.ashx?path=" + item.get_url();
                document.location = requestImage;
            }
        }
        function OnFileExplorerClientLoad(oExplorer, args) {

            var masterTable = oExplorer.get_grid().get_masterTableView();
            masterTable.sort("Name ASC");
        }
        //]]>
    </script>
    <style type="text/css">
        .loading {
            background-color: #fff;
            height: 100%;
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy2" runat="server">
    </asp:ScriptManagerProxy>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelContent" runat="server" Transparency="30">
        <div class="loading">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/loading3.gif" AlternateText="Exporting ...." />
        </div>
    </telerik:RadAjaxLoadingPanel>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnToExcel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadFileExplorer1" LoadingPanelID="RadAjaxLoadingPanelContent" />

                    <telerik:AjaxUpdatedControl ControlID="radNotification1" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">

            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <!-- begin rightcolumn -->
                    <td class="detailsframetop">
                        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="false"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="800px">
                                            <tr>
                                                <td class="contentcellheader" width="720px">
                                                    <asp:Label ID="lblgeneral" runat="server" Font-Bold="true" CssClass="label">
                                                Select Award Year, Campus, and Program to export data</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table width="60%" border="0" cellpadding="0" cellspacing="0" id="Table2" align="left">
                                            <tr>
                                                <td class="contentcell" align="right">
                                                    <asp:Label ID="lblAwardYear" runat="server" Text="Award Year" CssClass="label"></asp:Label></td>
                                                <td class="contentcell4">
                                                    <asp:DropDownList ID="ddlAwardYearId" runat="server" CssClass="dropdownlist" Width="230px"></asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" align="right">
                                                    <asp:Label ID="lblCampusId" runat="server" Text="Campus" CssClass="label"></asp:Label></td>
                                                <td class="contentcell4">
                                                    <asp:DropDownList ID="ddlCampuses" runat="server" Width="230px" AutoPostBack="true" CssClass="dropdownlist">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" align="right">
                                                    <asp:Label ID="lblProgram" runat="server" CssClass="label">Program</asp:Label></td>
                                                <td class="contentcell4">
                                                    <asp:DropDownList ID="ddlProgram" runat="server" Width="400px" CssClass="dropdownlist">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell"></td>
                                                <td class="contentcell4">

                                                    <asp:Button ID="btnToExcel" runat="server" Text="Export" Width="150px" />
                                                </td>
                                            </tr>
                                        </table>
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />
                                        <br />


                                        <asp:Panel runat="server" ID="pnlUploadMessage" BackColor="AliceBlue" Visible="false" Width="800px">
                                            <table width="800px" border="0" cellpadding="0" cellspacing="0" id="Table3" align="left" backcolor="AliceBlue">
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="Label2" runat="server" CssClass="labelbold">
                                                    The data was exported successfully. You can click on the following link and 
                                                    download the data exported recently <br /> or use the File Explorer to download the zip file.                                        
                                                        </asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:HyperLink ID="lnkDownloadYourFile" runat="server"></asp:HyperLink>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <br />

                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="800px">
                                            <tr>
                                                <td class="contentcellheader" width="800px">
                                                    <asp:Label ID="Label1" runat="server" Font-Bold="true" CssClass="labelbold">File Explorer: Browse to the folder and double click the zip file to save in your local machine</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table cellspacing="4">
                                            <tr>
                                                <td rowspan="2" style="vertical-align: top;">
                                                    <input type="checkbox" id="chkbxDownoaldFile" checked="checked" name="sdf" disabled="true" />Open images for direct download
                <br />
                                                    <telerik:RadFileExplorer runat="server" ID="RadFileExplorer1" Width="800px" Height="400px"
                                                        OnClientFileOpen="OnClientFileOpen"
                                                        TreePaneWidth="180px" AllowPaging="true" PageSize="10">
                                                        <Configuration ViewPaths="~/NSLDSDataExport/" />
                                                    </telerik:RadFileExplorer>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- begin footer -->

            <!-- end footer -->
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="CustomValidator"
                Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>

            <telerik:RadNotification runat="server" ID="RadNotification1"
                Text="This is a test" ShowCloseButton="true"
                Width="350px" Height="125px"
                TitleIcon=""
                Position="Center" Title="Message"
                EnableRoundedCorners="true"
                EnableShadow="true"
                Animation="Fade"
                AnimationDuration="1000"
                AutoCloseDelay="0"
                Style="padding-left: 120px; padding-top: 5px; word-spacing: 2pt;">
            </telerik:RadNotification>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <!--end validation panel-->
</asp:Content>
