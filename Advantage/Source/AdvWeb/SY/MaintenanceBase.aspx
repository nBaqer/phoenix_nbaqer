﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="MaintenanceBase.aspx.vb" Inherits="SY_MaintenanceBase" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script type="text/javascript">
        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }
        function RowSelected(sender, eventArgs) {

            var grid = sender;

            var MasterTable = grid.get_masterTableView();

            var row = MasterTable.get_dataItems()[eventArgs.get_itemIndexHierarchical()];

            var cell = MasterTable.getCellByColumnUniqueName(row, "Field1");

            document.getElementById('<%=(Master.FindControl("hdnObjectId")).ClientID %>').value = cell.innerHTML;
            document.getElementById('<%=(Master.FindControl("hdnPageResourceId")).ClientID %>').value = '<%=txtResourceId.Text%>';

            document.getElementById('btnAudit').removeAttribute("class");

        }

        function clearSessionStorage() {
            localStorage.clear();
            sessionStorage.clear();
        }
    </script>
    <style>
        .MaintenanceGrid_Web20 {
            border: 1px solid #4e75b3;
            background: #fff;
        }

        RadDock .rgDataDiv {
            height: auto !important;
        }

        .RadGrid_Web20 .rgEditRow {
            background-color: #fff !important;
        }

        .MyDeleteImageButton {
            content: "\e11b";
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
     <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrdPostScores">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrdPostScores" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Width="100%" Orientation="HorizontalTop">
            <asp:Panel ID="MainPanel" runat="server" Width="98%" Style="padding-left: 10px">
                <telerik:RadWindowManager ID="RadWindowManager1" runat="server" BackColor="#000066"
                    BorderColor="#E0E0E0" CssClass="Textbox" 
                    Modal="True">
                </telerik:RadWindowManager>
                <br />
                <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
                <p>
                </p>
                <input id="Hidden1" name="Hidden1" type="Hidden" runat="server" value="old value" />
                <div class="boxContainer">
                    <h3>
                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                    </h3>
                    <div style="">
                        <telerik:RadGrid ID="RadGrdPostScores" runat="server" AllowAutomaticDeletes="True"
                            Visible="true" AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True"
                            AutoGenerateColumns="False" AllowMultiRowEdit="false" AllowFilteringByColumn="true"
                            PageSize="8" CssClass="MaintenanceGrid_Web20">
                            <ClientSettings>
                                <%-- <Scrolling AllowScroll="true" UseStaticHeaders="true" SaveScrollPosition="true" />--%>
                            </ClientSettings>
                            <MasterTableView CommandItemDisplay="Top" HorizontalAlign="NotSet"
                                AutoGenerateColumns="false" EditMode="InPlace" InsertItemPageIndexAction="ShowItemOnFirstPage"
                                CommandItemSettings-AddNewRecordText="Add New Record" NoMasterRecordsText="No maintenance item to display"
                                DataKeyNames="Field2">
                                <CommandItemSettings AddNewRecordText="Add New Record" RefreshText="View All" ShowRefreshButton="false" />
                                <CommandItemTemplate>
                                    <div style="height: 20px;">
                                        <div style="float: left; vertical-align: top;">
                                            <div style="float: left; margin: 2px;">
                                                <asp:ImageButton ImageUrl="~\images\icon\icon_add.png" runat="server" ID="btn1" CommandName="InitInsert" CssClass="rgAdd" Text=" " />
                                                <asp:LinkButton runat="server" ID="linkbuttionInitInsert" CommandName="InitInsert"
                                                    Text="Add New Record" CssClass="vertical-align-top"></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
                                </CommandItemTemplate>
                                <HeaderStyle ForeColor="Black" />
                                <Columns>
                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn"
                                        HeaderStyle-Width="60px" EditImageUrl="~\images\icon\icon_edit.png" CancelImageUrl="~\images\icon\icon_cancel.png" UpdateImageUrl="~\images\icon\icon_save.png" InsertImageUrl="~\images\icon\icon_save.png">
                                        <HeaderStyle ForeColor="White" />
                                        <ItemStyle CssClass="MyEditImageButton" />
                                    </telerik:GridEditCommandColumn>
                                    <telerik:GridTemplateColumn UniqueName="Field3" AllowFiltering="false" HeaderStyle-Width="160px"
                                        >
                                        <HeaderStyle ForeColor="White" BackColor="#99CCFF" Font-Bold="true" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtEditDescription" runat="server" Text='<%# Eval("Field3") %>'
                                                Width="140px" MaxLength="50" CssClass="labelmaintenance"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvDesc" runat="server" ControlToValidate="txtEditDescription" Display="None" ErrorMessage="Description is Required"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderDescription" runat="server" Text='<%# Eval("Header3") %>'></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDisplayDescription" runat="server" Text='<%# Eval("Field3") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="Field2" AllowFiltering="false" HeaderStyle-Width="100px"
                                        >
                                        <HeaderStyle ForeColor="White" Font-Bold="true" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtEditCode" runat="server" Text='<%# Eval("Field2") %>' Width="85px" MaxLength="12"></asp:TextBox>
                                            <asp:TextBox ID="txtEditPKID" runat="server" Text='<%# Eval("Field1") %>' Visible="false"></asp:TextBox>
                                            <asp:TextBox ID="txtEditIsIPEDSVisible" runat="server" Text='<%# Eval("IsIPEDSVisible") %>'
                                                Visible="false"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtEditCode" Display="None" ErrorMessage="Code is Required"></asp:RequiredFieldValidator>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCode" runat="server" Text='<%# Eval("Header2") %>'></asp:Label>
                                            <asp:Label ID="txtHeadIsIPEDSVisible" runat="server" Text='<%# Eval("IsIPEDSVisible") %>'
                                                Visible="false"></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDisplayCode" runat="server" Text='<%# Eval("Field2") %>'></asp:Label>
                                            <asp:TextBox ID="txtPKID" runat="server" Text='<%# Eval("Field1") %>' Visible="false"></asp:TextBox>
                                            <asp:TextBox ID="txtIsIPEDSVisible" runat="server" Text='<%# Eval("IsIPEDSVisible") %>'
                                                Visible="false"></asp:TextBox>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="Field4" AutoPostBackOnFilter="true" AllowFiltering="true"
                                        CurrentFilterFunction="Contains" ShowFilterIcon="false" DataField="Status" FilterControlToolTip="Enter Active or Inactive or All; Hit Enter"
                                        HeaderStyle-Width="90px" >
                                        <HeaderStyle ForeColor="White" Font-Bold="true" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <FilterTemplate>
                                            <telerik:RadComboBox ID="RadComboBoxTitle" DataValueField="ContactTitle" Width="120px"
                                                 AppendDataBoundItems="true"
                                                SelectedValue='<%# TryCast(Container,GridItem).OwnerTableView.GetColumn("Field4").CurrentFilterValue %>'
                                                runat="server" OnClientSelectedIndexChanged="TitleIndexChanged">
                                                <Items>
                                                    <telerik:RadComboBoxItem Text="All" Value="" Selected="true" />
                                                    <telerik:RadComboBoxItem Text="Active" Value="Active" />
                                                    <telerik:RadComboBoxItem Text="Inactive" Value="Inactive" />
                                                </Items>
                                            </telerik:RadComboBox>
                                            <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
                                                <script type="text/javascript">
                                                    function TitleIndexChanged(sender, args) {
                                                        var tableView = $find("<%# TryCast(Container,GridItem).OwnerTableView.ClientID %>");
                                                        tableView.filter("Field4", args.get_item().get_value(), "EqualTo");
                                                    }
                                                </script>
                                            </telerik:RadScriptBlock>
                                        </FilterTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlEditStatus" runat="server" Width="90px" CssClass="dropdownlistmaintenance">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="cfvEditStatus" runat="server" ControlToValidate="ddlEditStatus"
                                                ValueToCompare="00000000-0000-0000-0000-000000000000" Operator="NotEqual"
                                                ErrorMessage="Status is Required" Display="None"></asp:CompareValidator>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderStatus" runat="server" Text='<%# Eval("Header4") %>'></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDisplayStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="Field5" AllowFiltering="false" HeaderStyle-Width="135px"
                                      >
                                        <HeaderStyle ForeColor="White" Font-Bold="true" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlCampusGroups" runat="server" Width="110px" CssClass="dropdownlistmaintenance">
                                            </asp:DropDownList>
                                            <asp:CompareValidator ID="cfvCampusGroups" runat="server" ControlToValidate="ddlCampusGroups"
                                                ValueToCompare="00000000-0000-0000-0000-000000000000" Operator="NotEqual"
                                                ErrorMessage="Campus Group is Required" Display="None"></asp:CompareValidator>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderCampusGroups" runat="server" Text='<%# Eval("Header5") %>'></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDisplayCampusGroups" runat="server" Text='<%# Eval("CampusGrpDescrip") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn UniqueName="Field6" AllowFiltering="false" HeaderStyle-Width="125px"
                                       >
                                        <HeaderStyle ForeColor="White" Font-Bold="true" />
                                        <ItemStyle HorizontalAlign="Left" />
                                        <EditItemTemplate>
                                            <asp:DropDownList ID="ddlIPEDS" runat="server" DataSource="<%# dtIPEDS %>" DataTextField="AgencyDescrip"
                                                DataValueField="RptAgencyFldValId" Width="115px" CssClass="dropdownlistmaintenance">
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblHeaderIPEDS" runat="server" Text='IPEDS'></asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDisplayIPEDS" runat="server" Text='<%# Eval("IPEDSFieldText") %>'></asp:Label>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" ConfirmDialogType="Classic"
                                        ConfirmText="Delete this item?" ConfirmTitle="Delete" Text="Delete" UniqueName="DeleteColumn"
                                        HeaderStyle-Width="30px" ImageUrl="~\images\icon\icon_delete.png">
                                        <ItemStyle CssClass="MyDeleteImageButton" HorizontalAlign="Center" />
                                    </telerik:GridButtonColumn>
                                    <telerik:GridBoundColumn UniqueName="Field1" DataField="Field1" Display="false" />
                                </Columns>
                                <EditFormSettings ColumnNumber="2" CaptionDataField="EmployeeID" CaptionFormatString="Edit properties of Product {0}">
                                    <EditColumn ButtonType="ImageButton" CancelText="Cancel edit " InsertText="Insert Order"
                                        UniqueName="EditCommandColumn1" UpdateText="Update record">
                                    </EditColumn>
                                    <FormTableStyle BackColor="White" CellPadding="2" CellSpacing="0" />
                                    <FormMainTableStyle BackColor="White" CellPadding="3" CellSpacing="0" GridLines="None" />
                                    <FormCaptionStyle CssClass="EditFormHeader" />
                                    <FormTableItemStyle Wrap="False"></FormTableItemStyle>
                                    <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
                                    <FormTableButtonRowStyle HorizontalAlign="Right" CssClass="EditFormButtonRow"></FormTableButtonRowStyle>
                                </EditFormSettings>
                            </MasterTableView>
                            <ClientSettings>
                                <Selecting AllowRowSelect="true" />
                                <ClientEvents OnRowSelected="RowSelected" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>

                </div>
                <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
                <asp:TextBox ID="txtCampusId" runat="server" Visible="false"></asp:TextBox>
                <asp:TextBox ID="txtPageName" runat="server" Visible="false"></asp:TextBox>
                <asp:TextBox ID="txtDescFilter" runat="server" Visible="false"></asp:TextBox>
                <input id="descfilter" runat="server" type="hidden" />
            </asp:Panel>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary" Visible="false">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>
