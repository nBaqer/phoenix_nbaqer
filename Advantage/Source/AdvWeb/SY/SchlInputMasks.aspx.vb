﻿Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class Template_Container_OneColumn
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected userId As String
    Protected campusId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim resourceId As Integer
        '   disable History button at all time
        'Header1.EnableHistoryButton(False)

        'pObj = Header1.UserPagePermission



        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            BindDataGrid()
        End If
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        headerTitle.Text = Header.Title

    End Sub

    Private Sub BindDataGrid()
        Dim facInputMasks As New InputMasksFacade
        Dim dt As New DataTable

        dt = facInputMasks.GetInputMasks
        dgInputMasks.DataSource = dt
        dgInputMasks.DataBind()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        Dim inputMaskId As Integer
        Dim mask As String
        Dim facInputMasks As New InputMasksFacade
        Dim strError As String = ""
        Dim numFound As Integer
        Dim objCommon As New CommonUtilities
        Dim originalZipMask As String

        originalZipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        iitems = dgInputMasks.Items

        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)

            inputMaskId = CInt(CType(iitem.FindControl("lblInputMaskId"), Label).Text)
            mask = CType(iitem.FindControl("txtMask"), TextBox).Text
            'Before we actually update the input mask we want to perform certain validations.
            '
            'SSN - There should be nine (9) digits (#) in the mask.
            '    - The only other valid characters are "(", ")" and "-"
            '
            'Phone/Fax - There should be ten (10) digits (#) in the mask.
            '          - The only other valid characters are "(", ")" and "-"
            '
            'Zip - There should be five (5) or ten (10) digits (#) in the mask
            '    - The only other valid character is "-"
            '    - If the format is currently five digits and there is existing data then
            '      we should not allow a change to ten digits. The db must first be updated
            '      with ten digits otherwise the input mask component would always fail.
            '      This means that we must check the db for the current length, if there any.
            '      If there is no record with a Zip (none international) then it is okay to make the change.

            Select Case inputMaskId
                Case 1 'SSN
                    numFound = objCommon.GetNumberOfOccurencesOfCharInString(mask, "#")
                    If numFound <> 9 Then
                        strError &= "SSN mask must contain nine (9) # symbols" & vbCr
                    End If
                    If objCommon.StringContainsInvalidCharacters(mask, "#()- ") Then
                        strError &= "SSN mask can only contain the following characters:" & vbCr
                        strError &= "(" & vbCr
                        strError &= ")" & vbCr
                        strError &= "#" & vbCr
                    End If

                Case 2 'Phone/Fax
                    numFound = objCommon.GetNumberOfOccurencesOfCharInString(mask, "#")
                    If numFound <> 10 Then
                        strError &= "Phone/Fax mask must contain ten (10) # symbols." & vbCr
                    End If
                    If objCommon.StringContainsInvalidCharacters(mask, "#()- ") Then
                        strError &= "Phone/Fax mask can only contain the following characters:" & vbCr
                        strError &= "(" & vbCr
                        strError &= ")" & vbCr
                        strError &= "#" & vbCr
                    End If

                Case 3 'Zip
                    numFound = objCommon.GetNumberOfOccurencesOfCharInString(mask, "#")
                    If numFound = 5 Or numFound = 10 Then
                        'OK - Do nothing
                    Else
                        strError &= "Zip mask must contain five (5) or ten (10) # symbols." & vbCr
                    End If
                    If objCommon.StringContainsInvalidCharacters(mask, "#()- ") Then
                        strError &= "Zip mask can only contain the following characters:" & vbCr
                        strError &= "(" & vbCr
                        strError &= ")" & vbCr
                        strError &= "#" & vbCr
                    End If

                    If originalZipMask.ToUpper <> mask.ToUpper Then
                        If Not facInputMasks.CanZipMaskBeChanged(mask) Then
                            strError &= "The zip mask cannot be changed at this time because there " & vbCr
                            strError &= "are existing records with a different number of digits. " & vbCr
                            strError &= "You must first update these records in the database before " & vbCr
                            strError &= "you can change the zip mask."
                        End If
                    End If


            End Select
        Next

        'If there are no errors we can go ahead and update the masks
        If strError <> "" Then
            DisplayErrorMessage(strError)

        Else

            '   Proceed saving.
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)

                Dim imaskInfo As New InputMaskInfo

                With imaskInfo
                    .IsInDB = True
                    .InputMaskId = CInt(CType(iitem.FindControl("lblInputMaskId"), Label).Text)
                    .Mask = CType(iitem.FindControl("txtMask"), TextBox).Text
                    .ModUser = CType(iitem.FindControl("txtModUser"), TextBox).Text
                    .ModDate = Date.Parse(CType(iitem.FindControl("txtModDate"), TextBox).Text)
                End With

                'inputMaskId = CInt(CType(iitem.FindControl("lblInputMaskId"), Label).Text)
                'mask = CType(iitem.FindControl("txtMask"), TextBox).Text

                'facInputMasks.UpdateInputMask(inputMaskId, mask)

                strError = facInputMasks.UpdateInputMaskInfo(imaskInfo, AdvantageSession.UserState.UserName)
                If strError <> "" Then
                    DisplayErrorMessage(strError)
                    Exit Sub
                End If
            Next

            '   rebind datagrid
            BindDataGrid()
        End If


    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class