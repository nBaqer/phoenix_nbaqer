<%@ Page Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="ReportParams1.aspx.vb" Inherits="ReportParams1" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="headinfo" ContentPlaceHolderID="additional_head" runat="server">
    <title>
        <%# PageTitle %>
    </title>

    <script type="text/javascript">
        function OldPageResized(sender) {
            window.$telerik.repaintChildren(sender);
        }
    </script>
    <script src="../js/EnterKey.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/scrollsaver.min.js"></script>
</asp:Content>
<asp:Content ID="studentaddresses_content1" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="studentaddresses_content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="studentaddresses_content2" ContentPlaceHolderID="contentmain2" runat="server">
    <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#fafafa" Width="250" Scrolling="Y">

            <table class="Table100">
                <tr>
                    <td class="listframetop">
                        <asp:Label ID="lblPreferences" runat="server" CssClass="label" Text="Saved User Reports">Saved Report Selections</asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfilters">
                            <asp:DataList ID="dlstPrefs" runat="server" Width="100%" DataKeyField="PrefId">
                                <SelectedItemStyle CssClass="selecteditemstyle" />
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# container.dataitem("prefname")%>' runat="server"
                                        CssClass="itemstyle" CommandArgument='<%# container.dataitem("prefid")%>'
                                        ID="linkbutton1" CausesValidation="false" />
                                </ItemTemplate>

                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
        <!-- end leftcolumn -->
        <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop" left="250px">
            <div class="contentPanelRight">
                <!-- begin rightcolumn -->
                <div class="titleReportParams">
                    <asp:Label runat="server" ID="lblReportName" CssClass="reportHeader"></asp:Label>
                </div>
                <div class="boxContainer full">
                    <asp:Panel ID="pnlPreference" runat="server" Width="95%" Visible="true" Height="100%">
                        <asp:Table ID="table7" CssClass="label" runat="server" Width="300px">
                            <asp:TableRow>
                                <asp:TableCell Width="125px" Wrap="false">
                                    <asp:Label ID="lblPrefName" runat="server" CssClass="labelbold">Preference name</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell Width="175px">
                                    <asp:TextBox ID="txtPrefName" runat="server" CssClass="textbox" MaxLength="50" Width="175px"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
                    <table id="table4" class="Table100">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" style="text-align: right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button>
                                <asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="boxContainer full">
                    <table class="maincontenttable" id="table5" style="padding-left: 10px; padding: 0; width: 80%; margin: 0; border: 0">
                        <tr>
                            <td class="detailsframe">
                                <div>

                                    <asp:Panel ID="pnl5" runat="server" Visible="true" Width="95%" Height="100%">
                                        <asp:Table ID="table8" CssClass="label" runat="server" Width="300px">
                                            <asp:TableRow>
                                                <asp:TableCell Width="125px" Wrap="false">
                                                    <asp:Label ID="lbl4" CssClass="label" runat="server" Font-Bold="true">Required filter(s)</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Width="175px" Wrap="false">
                                                    <asp:Label ID="lblRequiredFilters" CssClass="label" runat="server"></asp:Label>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:Panel>
                                    <table style="width: 100%; border: 0">
                                        <tr>
                                            <td style="width: 650px; vertical-align: top">
                                                <!-- pnlFilterValues goes here -->
                                                <asp:Panel ID="pnlFilterValues" runat="server" Visible="true" Height="100%">
                                                    <table class="contenttable, Table100">
                                                        <tr>
                                                            <td colspan="3" class="lsheaderreport" style="text-align: left; vertical-align: top">
                                                                <asp:Label ID="lblSelectSort" runat="server" CssClass="label" Font-Bold="True">Step 1: Please select the fields to sort the report by:</asp:Label></td>
                                                        </tr>

                                                        <tr>
                                                            <td class="font-blue">Available Sort Order</td>
                                                            <td class="lscontentreport2">&nbsp;</td>
                                                            <td class="font-blue">Selected Sort Order</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="lscontentreport3" style="width: 35%">
                                                                <asp:ListBox ID="lbxSort" runat="server" Rows="4" CssClass="todocumentlist1"></asp:ListBox></td>
                                                            <td class="lscontentreport2" style="width: 15%; text-align: center">
                                                                <div class="margin-b-5px">
                                                                    <asp:Button ID="btnAdd" Text="Add >" runat="server" CausesValidation="False"></asp:Button>
                                                                </div>
                                                                <asp:Button ID="btnRemove" Text="< Remove" runat="server" CausesValidation="False"></asp:Button>
                                                            </td>
                                                            <td class="lscontentreport3" style="width: 50%">
                                                                <asp:ListBox ID="lbxSelSort" runat="server" Rows="4" CssClass="todocumentlist1"></asp:ListBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" class="lsheaderreport" style="text-align: left">
                                                                <asp:Label ID="lblSelectFilters" runat="server" CssClass="label" Font-Bold="True">Step 2: Please select the filters to apply to the report:</asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblFilter" runat="server" Text="Filter" CssClass="font-blue"></asp:Label></td>
                                                            <td class="lscontentreport2">&nbsp;</td>
                                                            <td>
                                                                <asp:Label ID="lblValues" runat="server" Text="Values" CssClass="font-blue"></asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="lscontentreport3">
                                                                <asp:ListBox ID="lbxFilterList" runat="server" Rows="14" Height="200" CssClass="todocumentlist1" AutoPostBack="True"></asp:ListBox></td>
                                                            <td class="lscontentreport2">&nbsp;</td>
                                                            <td class="lscontentreport3">
                                                                <asp:ListBox ID="lbxSelFilterList" runat="server" Rows="14" Height="200" CssClass="todocumentlist1" AutoPostBack="True"
                                                                    SelectionMode="Multiple"></asp:ListBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="spacertables"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="3" class="lsfooterreport">
                                                                <asp:Panel ID="pnl4" CssClass="textbox" runat="server" Visible="False" Width="100%" Height="100%">
                                                                    <table class="Table100">
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Table ID="tblFilterOthers" CssClass="label" runat="server" Width="100%">
                                                                                    <asp:TableRow>
                                                                                        <asp:TableCell Width="40%"></asp:TableCell>
                                                                                        <asp:TableCell Width="14%"></asp:TableCell>
                                                                                        <asp:TableCell Width="14%"></asp:TableCell>
                                                                                    </asp:TableRow>
                                                                                </asp:Table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="spacertables"></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlReportingFilters" runat="server" Visible="false">
                                                    <table class="contenttable" style="border: 1px solid #ebebeb; width: 80%; text-align: left; padding: 0; margin: 0">
                                                        <tr>
                                                            <td class="contentcellheader" style="border-top: 0; border-right: 0; border-left: 0; width: 500px; text-align: left">
                                                                <asp:Label ID="lblReportingAgencies" runat="server" Font-Bold="true" CssClass="label">Step 1: Select Filter and Values</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="border-top: 0; border-right: 0; border-left: 0; width: 500px; text-align: left">
                                                                <asp:Panel ID="pnlFirstNameLastName" CssClass="textbox" runat="server" Visible="true" Width="100%" Height="100%">
                                                                    <table class="Table100">
                                                                        <tr>
                                                                            <td>
                                                                                <table style="width: 100%">
                                                                                    <tr>
                                                                                        <td style="white-space: nowrap">
                                                                                            <asp:Label ID="lblCampGrpId" runat="server" CssClass="label">Campus Group<span style="color: red">*</span></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:ListBox ID="ddlCampGrpId" runat="server"></asp:ListBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="white-space: nowrap">
                                                                                            <asp:Label ID="lblTermId" runat="server" CssClass="label" Text="Term"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:DropDownList ID="ddlTermId" runat="server"></asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="white-space: nowrap">
                                                                                            <asp:Label ID="lblPrgVerId" runat="server" CssClass="label" Text="Program Version"></asp:Label>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:DropDownList ID="ddlPrgVerId" runat="server"></asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="white-space: nowrap">
                                                                                            <asp:Label ID="lblFirstName" runat="server" Text="First Name" CssClass="label"></asp:Label>
                                                                                        </td>
                                                                                        <td style="white-space: nowrap">
                                                                                            <asp:TextBox ID="txtFirstName" runat="server" CssClass="textbox" Width="150px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="white-space: nowrap">
                                                                                            <asp:Label ID="lblLastName" runat="server" Text="Last Name" CssClass="label"></asp:Label>
                                                                                        </td>
                                                                                        <td style="white-space: nowrap">
                                                                                            <asp:TextBox ID="txtLastName" runat="server" CssClass="textbox" Width="150px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="white-space: nowrap">
                                                                                            <asp:Label ID="lblTermDates" runat="server" Text="Term Starts Between" CssClass="label"></asp:Label>
                                                                                        </td>
                                                                                        <td style="white-space: nowrap">
                                                                                            <asp:TextBox ID="txtTermStartDate" runat="server" CssClass="textbox" Width="100px"></asp:TextBox>
                                                                                            <asp:Label ID="Label4" runat="server" Text="and" CssClass="labeldate"></asp:Label>
                                                                                            <asp:TextBox ID="txtTermEndDate" runat="server" CssClass="textbox" Width="100px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="white-space: nowrap">
                                                                                            <asp:Label ID="lblStudentAttendance" runat="server" Text="Get Student Attendance Between" CssClass="label"></asp:Label>
                                                                                        </td>
                                                                                        <td style="white-space: nowrap">
                                                                                            <asp:TextBox ID="txtStartDate" runat="server" CssClass="textbox" Width="100px"></asp:TextBox>
                                                                                            <asp:Label ID="Label1" runat="server" Text="and" CssClass="labeldate"></asp:Label>
                                                                                            <asp:TextBox ID="txtEndDate" runat="server" CssClass="textbox" Width="100px"></asp:TextBox>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlDateBetween" CssClass="textbox" runat="server" Visible="false" Width="100%" Height="100%">
                                                    <table class="Table100">
                                                        <tr>
                                                            <td>
                                                                <table style="width: 100%">
                                                                    <tr>
                                                                        <td style="white-space: nowrap">
                                                                            <asp:Label ID="LblDateBetween" runat="server" Text="Date Between" CssClass="label"></asp:Label>
                                                                        </td>
                                                                        <td style="white-space: nowrap">
                                                                            <asp:TextBox ID="txtDateStart" runat="server" CssClass="textbox" Width="150px"></asp:TextBox>
                                                                        </td>
                                                                        <td style="white-space: nowrap">
                                                                            <asp:Label ID="Label2" runat="server" Text=" and " CssClass="label"></asp:Label>
                                                                        </td>
                                                                        <td style="white-space: nowrap">
                                                                            <asp:TextBox ID="txtDateEnd" runat="server" CssClass="textbox" Width="150px"></asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="Panel1" CssClass="textbox" Visible="false" runat="server" Width="100%" Height="100%">
                                                    <table class="Table100">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblTermStartCutOff" CssClass="label" runat="server">Term Start Cutoff</asp:Label></td>
                                                            <td>
                                                                <asp:TextBox ID="txtTermStart" runat="server" CssClass="textbox" Width="200px" Text='<%# Date.Today.ToShortDateString() %>'></asp:TextBox></td>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlSearch" CssClass="textbox" Visible="false" runat="server" Width="100%" Height="100%">
                                                    <asp:Table ID="tblStudentSearch" CssClass="label" runat="server" Width="100%">
                                                        <asp:TableRow>
                                                            <asp:TableCell Width="28%"></asp:TableCell>
                                                            <asp:TableCell Width="42%"></asp:TableCell>
                                                            <asp:TableCell Width="30%"></asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblStudent" CssClass="label" runat="server">Student</asp:Label>
                                                            </asp:TableCell>
                                                            <asp:TableCell Wrap="false">
                                                                <asp:TextBox ID="txtStudentId" CssClass="textbox" runat="server" Width="200px" ContentEditable="False"></asp:TextBox>

                                                                <asp:LinkButton ID="lbtSearch" runat="server" CausesValidation="False" Style="text-decoration: none">
																								<img style="border:0" src="../images/top_search.gif" alt="Search Student"></asp:LinkButton>
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="lblEnrollmentId" CssClass="label" runat="server" Visible="True">Enrollment</asp:Label>
                                                            </asp:TableCell>
                                                            <asp:TableCell Wrap="false">
                                                                <asp:TextBox ID="txtStuEnrollment" runat="server" CssClass="textbox" Width="200px" ContentEditable="False"></asp:TextBox>&nbsp;&nbsp;
																							<asp:Button ID="btnClear" Text="Clear" runat="server" CausesValidation="False" Width="50px"></asp:Button>
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:HiddenField ID="txtStuEnrollmentId" runat="server" />
                                                                <asp:HiddenField ID="txtTermId" runat="server" />
                                                                <asp:HiddenField ID="txtTerm" runat="server" />
                                                                <asp:HiddenField ID="txtAcademicYearId" runat="server" />
                                                                <asp:HiddenField ID="txtAcademicYear" runat="server" />
                                                                <asp:HiddenField ID="txtStudentIdentifier" runat="server" />
                                                                <asp:HiddenField ID="txtResId" runat="server" />
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:Panel>
                                                <p></p>
                                                <asp:Panel ID="pnlSign" CssClass="textbox" Visible="false" runat="server" Width="100%" BorderStyle="None" Height="100%">
                                                    <asp:Table ID="Table1" CssClass="label" runat="server" Width="100%">
                                                        <asp:TableRow>
                                                            <asp:TableCell Width="28%" Wrap="false">
                                                                <asp:CheckBox ID="chkStudentSign" runat="server" Text="Show Student signature line" CssClass="label" />
                                                            </asp:TableCell>
                                                            <asp:TableCell Width="42%" Wrap="false">
                                                                <asp:CheckBox ID="chkSchoolSign" runat="server" Text="Show School Official signature line" CssClass="label" />
                                                            </asp:TableCell>
                                                            <asp:TableCell Width="30%"></asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:Panel>
                                                <p></p>
                                                <asp:Panel ID="pnlTotalCost" CssClass="textbox" Visible="false" runat="server" Width="100%" Height="100%">
                                                    <asp:Table ID="Table2" CssClass="label" runat="server" Width="100%">
                                                        <asp:TableRow>
                                                            <asp:TableCell Width="28%">
                                                                <asp:CheckBox ID="ChkTotalCost" runat="server" Text="Show Total Cost" Checked="true" CssClass="label" />
                                                            </asp:TableCell>
                                                            <asp:TableCell Width="42%">
                                                                <asp:CheckBox ID="ChkCurrentBalance" runat="server" Text="Show Current Balance" Checked="true" CssClass="label" />
                                                            </asp:TableCell>
                                                            <asp:TableCell Width="30%"></asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlOfficialTitle" CssClass="textbox" Visible="false" runat="server" Width="100%" Height="100%" Style="border-style: none;">
                                                    <asp:Table ID="Table3" CssClass="label" runat="server" Width="100%">
                                                        <asp:TableRow>
                                                            <asp:TableCell Width="28%" Wrap="false">
                                                                <asp:CheckBox ID="chkOfficialTitle" runat="server" Text="Show Official Transcript" CssClass="label" />
                                                            </asp:TableCell>
                                                            <asp:TableCell Width="42%">
                                                            </asp:TableCell>
                                                            <asp:TableCell Width="30%"></asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:Panel>
                                                <asp:Panel ID="pnlWorkUnits" CssClass="textbox" Visible="false" runat="server" Width="100%" Height="100%" BorderStyle="None">
                                                    <asp:Table ID="Table6" CssClass="label" runat="server" Width="100%">
                                                        <asp:TableRow>
                                                            <asp:TableCell Width="50%">
                                                                <asp:RadioButtonList ID="rdolstChkUnits" runat="server" CssClass="label" AutoPostBack="true" RepeatColumns="2">
                                                                    <asp:ListItem Value="True" Selected="True">Show work units under each subject</asp:ListItem>
                                                                    <asp:ListItem Value="False">Do not show work units under each subject</asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:Panel>
                                                <p></p>
                                                <asp:Panel ID="pnlRow" runat="server" Enabled="false" Visible="false" Style="padding-left: 50px" Height="100%">

                                                    <table class="label">
                                                        <tr>
                                                            <td colspan="3">
                                                                <asp:Label ID="lblGrp" runat="server" Text="Group work units by type under each term"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chk501" runat="server" Text=" Exam" Checked="true" /></td>
                                                            <td>
                                                                <asp:CheckBox ID="chk544" runat="server" Text=" Externships" Checked="true" /></td>
                                                            <td>
                                                                <asp:CheckBox ID="chk502" runat="server" Text=" Final" Checked="true" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chk499" runat="server" Text=" Homework" Checked="true" /></td>
                                                            <td>
                                                                <asp:CheckBox ID="chk503" runat="server" Text=" Lab Hours" Checked="true" /></td>
                                                            <td>
                                                                <asp:CheckBox ID="chk500" runat="server" Text=" Lab Work" Checked="true" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chk533" runat="server" Text=" Practical Exams" Checked="true" /></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="pnl6" runat="server" Width="100%" CssClass="textbox" Height="100%">

                                                    <table id="Table9" style="padding: 2px; margin: 2px; width: 100%">
                                                        <tr>
                                                            <td style="padding-left: 10px; width: 140px">

                                                                <asp:Label ID="LblMinimumAttendance" CssClass="label" runat="server">Attendance</asp:Label>
                                                            </td>
                                                            <td style="width: 50px">
                                                                <asp:Label ID="Label5" CssClass="label" runat="server">Min(%)</asp:Label>
                                                            </td>
                                                            <td>
                                                                <telerik:RadNumericTextBox ID="txtMinimumAttendance" runat="server" CssClass="textbox" Width="100px" MinValue="0" MaxValue="999999" Type="Percent"></telerik:RadNumericTextBox>
                                                            </td>
                                                            <td style="width: 50px">

                                                                <asp:Label ID="lblMaxAttendance" CssClass="label" runat="server">Max(%)</asp:Label>
                                                            </td>
                                                            <td style="padding-right: 7px">
                                                                <telerik:RadNumericTextBox ID="txtMaxAttendance" runat="server" CssClass="textbox" Width="100px" MinValue="0" MaxValue="999999" Type="Percent"></telerik:RadNumericTextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                                <asp:Panel ID="pnl7" runat="server" Width="100%" CssClass="textbox" Visible="False" Height="100%">
                                                    <table class="Table100">
                                                        <tr>
                                                            <td style="width: 40%">
                                                                <asp:Label ID="lblNumberConsecutiveAbsentDays" CssClass="label" runat="server">Number of Consecutive Absent Days</asp:Label></td>
                                                            <td style="width: 30%">
                                                                <asp:DropDownList ID="ddlConsAbsCompOp" runat="server" CssClass="dropdownlist" AutoPostBack="true">
                                                                    <asp:ListItem Value="equalto" Selected="True">Equal To</asp:ListItem>
                                                                    <asp:ListItem Value="greaterthan">Greater Than</asp:ListItem>
                                                                    <asp:ListItem Value="lessthan">Less Than</asp:ListItem>
                                                                    <asp:ListItem Value="between">Between</asp:ListItem>
                                                                </asp:DropDownList></td>
                                                            <td style="width: 15%">
                                                                <asp:TextBox ID="txtNumberConsecutiveAbsentDays" runat="server" CssClass="textbox" Width="75px"></asp:TextBox></td>
                                                            <td style="width: 15%">
                                                                <asp:TextBox ID="txtNumberConsecutiveAbsentDays2" runat="server" CssClass="textbox" Width="75px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 40%">
                                                                <asp:Label ID="lblUsedScheduleDays" CssClass="label" runat="server">Use Scheduled Days</asp:Label></td>
                                                            <td style="width: 30%">
                                                                <asp:CheckBox ID="chkUsedScheduleDays" runat="server" /></td>
                                                            <td style="width: 15%"></td>
                                                            <td style="width: 15%"></td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                            <td style="width: 10px;">&nbsp;</td>
                                            <td style="width: 200px; text-align: right">
                                                <!-- Begin Check box special filters and Run Report Section-->
                                                <!-- begin right side -->
                                                <table class="Table100">
                                                    <tr>
                                                        <td>
                                                            <!-- begin right side top -->
                                                            <table class="Table100">
                                                                <tr>
                                                                    <td style="text-align: left">
                                                                        <asp:Label ID="lblAdditionalInfo" runat="server" CssClass="label" Font-Bold="True">Step 3: Additional information to print:</asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: left" class="contentcell">
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkRptFilters" runat="server" Text="Selected Filter criteria" CssClass="label"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkRptSort" runat="server" Text="Selected Sort Order" CssClass="label"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkRptDescrip" runat="server" Text="Report Description" CssClass="label"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkRptInstructions" runat="server" Text="Report Instructions" CssClass="label"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkRptNotes" runat="server" Text="Report Notes" CssClass="label"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkRptStudentGroup" runat="server" Text="Student Scheduling Group" CssClass="label"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkRptDateIssue" runat="server" Text="Date Issued" CssClass="label"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkRptSuppressHeader" runat="server" Text="Suppress Header" CssClass="label" Enabled="false"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkUseSignatureForAttendance" runat="server" Text="Suppress Signature Lines" Enabled="false" CssClass="label" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkShowOffTrans" runat="server" Text="Show Official Transcript" CssClass="label" Enabled="false" Visible="false" Checked="true"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ChkRptProgramVersion" runat="server" Text="Hide Program Version" CssClass="label"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ChkRptShowClassDates" runat="server" Text="Show Class Dates" CssClass="label" Enabled="false"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ChkRptGroupByClass" runat="server" Text="Group By Class" CssClass="label" Enabled="false"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ChkRptProjectionsExceed" runat="server" Text="Show Projections that Exceed Balance" CssClass="label" Enabled="false"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ChkRptShowGroupByEnrollment" runat="server" Text="Group By Enrollment Status" CssClass="label" Enabled="false"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ChkRptShowDisbNotBeenPaid" runat="server" Text="Show Disbursement Not Been Paid" CssClass="label" Enabled="false"></asp:CheckBox>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ChkTermModule" runat="server" Visible="False" Text="Show Term/Module" CssClass="label" Checked="True" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ChkShowLegalDisc" runat="server" Text="Show Legal Disclaimer" Enabled="false" CssClass="label" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkshowTermProgressDescription" runat="server" Text="Show Term Progress Description" Enabled="true" Checked="false" CssClass="label" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="ChkBaseCAOnRegClasses" runat="server" Text="Base Currently Attending on <br/> &nbsp;&nbsp;&nbsp;&nbsp; Registered Classes" Enabled="false" CssClass="label" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkRptUseStuCurrStatus" runat="server" Text="Use Student Current Status" Enabled="false" CssClass="label" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- end right side top -->
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <!-- begin right side bottom -->
                                                            <table class="Table100">
                                                                <tr>
                                                                    <td colspan="2" style="text-align: left; vertical-align: top">
                                                                        <asp:Label ID="lblStep4" runat="server" CssClass="label" Font-Bold="True">Step 4: </asp:Label></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="text-align: left">
                                                                        <asp:DropDownList ID="ddlExportTo" runat="server" AutoPostBack="true">
                                                                            <asp:ListItem Value="pdf">Adobe Acrobat</asp:ListItem>
                                                                            <asp:ListItem Value="xls">Microsoft Excel</asp:ListItem>
                                                                            <asp:ListItem Value="doc">Microsoft Word</asp:ListItem>
                                                                            <asp:ListItem Value="xls1">Microsoft Excel (Data) </asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="text-align: left">
                                                                        <div class="margin-b-5px">
                                                                            <asp:Button ID="btnExport" Text="Export" runat="server"></asp:Button>
                                                                        </div>
                                                                        <div class="margin-b-5px">
                                                                            <asp:Button ID="btnSearch" Text="View Report" runat="server" ></asp:Button>
                                                                        </div>
                                                                        <div class="margin-b-5px">
                                                                            <asp:Button ID="btnPerformDataDump" Text="View Report" runat="server" Visible="False"></asp:Button>
                                                                        </div>
                                                                        <div class="margin-b-5px">
                                                                            <asp:Button ID="btnExportToExcel" Text="Export to Excel" runat="server" Visible="False"></asp:Button>
                                                                        </div>
                                                                        <div class="margin-b-5px">
                                                                            <asp:Button ID="btnFriendlyPrint" Visible="False" Text="Printer Friendly Report" runat="server" Enabled="False"></asp:Button>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2" style="text-align: left">
                                                                        <br />
                                                                        <asp:Label ID="lblExportNote" CssClass="label" runat="server" Text=""></asp:Label></td>
                                                                </tr>
                                                            </table>
                                                            <!-- end right side bottom -->
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                    </table>
                                    <!-- start validation panel-->
                                    <asp:LinkButton ID="lbtPrompt" runat="server" Visible="false" CssClass="label" Style="border-style: none"></asp:LinkButton>
                                    <asp:HiddenField ID="txtPrefId" runat="server" />
                                    <asp:CustomValidator ID="Customvalidator1" runat="server" Display="None"
                                        ErrorMessage="CustomValidator"></asp:CustomValidator>
                                    <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
                                    <asp:ValidationSummary ID="Validationsummary1" runat="server" ShowSummary="False"
                                        ShowMessageBox="True"></asp:ValidationSummary>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>
