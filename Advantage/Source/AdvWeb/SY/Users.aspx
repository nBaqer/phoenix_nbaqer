﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" EnableEventValidation="false" CodeFile="Users.aspx.vb" Inherits="Users" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
   <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

        function openroles() {
            var campusid = "<%=HttpContext.Current.Request.Params("cmpid")%>";
            var uid = "<%=userId%>";
            theURL = "../SY/UsersCampGrps.aspx?resid=256&mod=SY&cmpid=" + campusid + "&UID=" + uid;
            winName = "UserRolesCampus";
            features = "status=no,resizable=no,width=900,height=470,scrollbars=no";
            theChild = window.open(theURL,winName,features);
        }
   </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    
 
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
     <%-- Add class ListFrameTop2 to the table below --%>

            <table border="0" cellpadding="0" cellspacing="0"  Width="350">
            <%--	<tr  class="listframetop3">
                    <td class="listframetop3">
                        Select a user
                    </td>
                </tr>--%>
                    <tr >
                    <td >
                        <%--Enter user name--%>
                    </td>
                </tr>
               
                    <tr>
                    <td align="center"  style="padding:25px 0 0 10px;">
    
                            <telerik:RadComboBox  ID="ddlUsername"  runat="server" DataTextField="FullName" DataValueField="UserID"
                        AutoPostBack="True"  AppendDataBoundItems="true" CausesValidation="False"   Filter="Contains" EmptyMessage="Select name from list or type to search"  Width="225px"  TabIndex="1" >
                          <ItemTemplate>
                         <img src='<%#  GetImageSrc(Eval("AccountActive")) %>' title='<%#  GetStatus(Eval("AccountActive")) %>' alt="" />
                             <asp:Label runat="server" ID="Label1" Text='<%# Eval("FullName")%>'></asp:Label>
         <telerik:RadToolTip ID="RadToolTip1" runat="server"  TargetControlID="Label1" Position="BottomRight" Text='<%# Eval("FullName")%>'>
          </telerik:RadToolTip>  
           </ItemTemplate>
                    

                        </telerik:RadComboBox> 
                      

             
                    </td>
                    <td align="left" style="padding:25px 0 0 0;" >
                         <telerik:RadButton ID="ThreeStateCheckBox" runat="server" ButtonType="ToggleButton"
                    ToggleType="CustomToggle" CausesValidation="False" AutoPostBack="True" BackColor="transparent" >
                    <ToggleStates>
                          <telerik:RadButtonToggleState Text="Active" Value="Active"  PrimaryIconCssClass="rbToggleCheckboxFilled" />
                        <telerik:RadButtonToggleState Text="All" Value="All" PrimaryIconCssClass="rbToggleCheckbox" />
                        <telerik:RadButtonToggleState Text="Inactive" Value="Inactive"  PrimaryIconCssClass="rbToggleCheckboxChecked" />
                      
                    </ToggleStates>
                </telerik:RadButton>	                        
                    </td>
                </tr>
                <tr>
                    <td >
                    <asp:ListBox ID="lbxUsers" Height="850"  Visible="False"    Width="350"  CssClass="listbox" BorderWidth="0" AutoPostBack="True" runat="server"></asp:ListBox>
                    
                    </td>
                </tr>
            </table>
    </telerik:RadPane>

    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
        <table class="tablecontent" >
            <!-- begin top menu (save,new,reset,delete,history)-->
            <tr>
                <td class="menuframe" align="right">
                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                        ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                            ID="btnDelete" runat="server" CssClass="delete hidden" Text="Delete" CausesValidation="False">
                        </asp:Button>
                </td>
            </tr>
        </table>
        <table class="maincontenttable" >
        <tr>
        <td class="detailsframe">
        <div id="rightpanel" style="padding:25px 0 0 25px;" runat="server">
            <!-- begin content table-->
            <table class="contenttable"  width="60%" align="center">
                <asp:TextBox ID="txtUserId" runat="server" Visible="False"></asp:TextBox>
                <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                <tr>
                    <td class="contentcell">
                        <asp:Label ID="lblFullName" runat="server" CssClass="label" Text="Name">Name <span style="color: red">*</span></asp:Label>
                    </td>
                    <td class="contentcell4">
                        <asp:TextBox ID="txtFullName" runat="server" CssClass="textbox"   ></asp:TextBox>
                        <asp:HiddenField ID="hdnFullName" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="contentcell">
                        <asp:Label ID="lblEmail_UserID" runat="server" CssClass="label" Text="Email">Email <span style="color: red">*</span></asp:Label>
                    </td>
                    <td class="contentcell4">
                        <asp:TextBox ID="txtEmail_UserID" runat="server" CssClass="textbox"   ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="contentcell">
                        <asp:Label ID="lblUserName" runat="server" CssClass="label" Text="User name">Display Name <span style="color: red">*</span></asp:Label>
                    </td>
                    <td class="contentcell4">
                        <asp:TextBox ID="txtUserName" runat="server" CssClass="textbox"   ></asp:TextBox>
                    </td>
                </tr>
                <tr >
                    <td class="contentcell">
                        <asp:Label ID="lblCampusId" runat="server" CssClass="label" Visible="False" Text="Default Campus"></asp:Label>
                    </td>
                    <td class="contentcell4">
                        <asp:DropDownList ID="ddlCampusId" runat="server" Visible="False" CssClass="dropdownlist"   >
                        </asp:DropDownList>
                        <asp:Label ID="lblCampusIdValue" runat="server" Visible="false"  CssClass="LabelLeft" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="contentcell">
                        <asp:Label ID="lblModuleId" runat="server" CssClass="label" Text="Default Module"> Default Module<span style="color: red">*</span></asp:Label>
                    </td>
                    <td class="contentcell4">
                        <asp:DropDownList ID="ddlModuleId" runat="server" CssClass=""   >
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="contentcell">
                    </td>
                    <td class="contentcell4user">
                        <asp:CheckBox ID="chkAccountActive" runat="server" CssClass="label" Text="Active Account">
                        </asp:CheckBox>
                    </td>
                </tr>
                <tr runat="server" id="trUnlockUser">
                    <td class="contentcell">
                    </td>
                    <td class="contentcell4user" runat="server">
                        <asp:CheckBox ID="chkAccountLocked" runat="server" CssClass="label" Text="Account Is Locked" >
                        </asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td class="contentcell">
                    </td>
                    <td class="contentcell4user">
                        <asp:CheckBox ID="chkIsDefaultAdminRep" runat="server" CssClass="label" Visible="false"
                            Text="Default Admin Rep"></asp:CheckBox>
                    </td>
                </tr>
                <tr>
                    <td class="contentcell">
                    </td>
                    <td class="contentcell4user">
                        <asp:LinkButton ID="lbResetPassword" runat="server" Text="Reset Password" OnClick="ResetPassword_Click" ></asp:LinkButton>						
                    </td>
                </tr>
                <tr>
                    <td class="contentcell">
                        <asp:Button ID="btnrefreshGrid" runat="server" Text="Refresh Grid" />
                    </td>
                    <td class="contentcell4user" style="text-align: left">
<%--                        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="label" NavigateUrl="../SY/UsersCampGrps.aspx?resid=256"
                            Target="_blank" Enabled="False">Assign user to roles/campus groups</asp:HyperLink>--%>
                        <asp:Button ID="btnRoleCampus" runat="server" Text="Assign user to roles/campus groups" CommandArgument="../SY/UsersCampGrps.aspx?resid=256"   />

                    </td>
                </tr>
                <tr>
                    <td class="contentcell">
                    </td>
                    <td class="contentcell4user" style="text-align: left">
                    </td>
                </tr>
            </table>

            <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" border="0" style="padding-right:20px" >
                <TR>
                    <TD style="padding-top: 20px">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="CampGrpId"
                            cellpadding="0" BorderWidth="1px" BorderColor="#E0E0E0" width="100%">
                            <HeaderStyle cssClass="datagridheader" Wrap="True"></HeaderStyle>
                            <RowStyle cssClass="datagriditemstyle"/>
                            <AlternatingRowStyle CssClass="datagridalternatingstyle" />
                            <Columns>
                                <asp:BoundField DataField="CampGrpDescrip" HeaderText="Campus Groups" SortExpression="CampGrpDescrip" HeaderStyle-CssClass="datagridheader" ItemStyle-CssClass="datagriditemstyle">
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="Campus" HeaderStyle-CssClass="datagridheader" ItemStyle-CssClass="datagriditemstyle">
                                    <ItemTemplate>
                                        <asp:BulletedList ID="BlCampus" DataTextField="CampDescrip" CausesValidation="False" BulletStyle="Square"  runat="server">
                                        </asp:BulletedList>
                                    </ItemTemplate>
                                    <ItemStyle CssClass="datagriditemstyle" />
                                    <HeaderStyle CssClass="datagridheader" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Roles" HeaderStyle-CssClass="datagridheader" ItemStyle-CssClass="datagriditemstyle">
                                    <ItemTemplate>
                                        <asp:BulletedList ID="BlRole"  DataTextField="Role" CausesValidation="False" BulletStyle="Square"  runat="server">
                                        </asp:BulletedList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </TD>
                </TR>
            </TABLE>
        <!-- end content table-->
        </div>
        </td>
        </tr>
        <tr>
         <td align="center">   <asp:LinkButton ID="HyperLink2" Visible="false" runat="server" CssClass="label" CausesValidation="False" Enabled="True">View roles assigned</asp:LinkButton>	</td>
        </tr>
            <asp:TextBox ID="txtPassword" runat="server" CssClass="textbox" Visible="false" TextMode="Password"></asp:TextBox>
            <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="textbox" Visible="false"	TextMode="Password"></asp:TextBox>


        </table>
    </telerik:RadPane>
    </telerik:RadSplitter>

    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>

    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="CustomValidator" Display="None">
        </asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            <asp:RequiredFieldValidator ID="rfvFullName" runat="server" Display="None" ErrorMessage="Name is required"
                ControlToValidate="txtFullName"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvEmail_UserID" runat="server" Display="None" ErrorMessage="Email is required"
                ControlToValidate="txtEmail_UserID"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvUserName" runat="server" Display="None" ErrorMessage="Display Name is required"
                ControlToValidate="txtUserName"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvPassword" runat="server" Display="None" ErrorMessage="Password is required"
                ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" Display="None"
                ErrorMessage="Confirm password is required" ControlToValidate="txtConfirmPassword"></asp:RequiredFieldValidator>
            <asp:RequiredFieldValidator ID="rfvModuleId" runat="server" Display="None" ErrorMessage="Default module is required"
                ControlToValidate="ddlModuleId" InitialValue=""></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="cmvPassword" runat="server" Display="None" ErrorMessage="Confirm password did not match password"
                ControlToValidate="txtConfirmPassword" ControlToCompare="txtPassword"></asp:CompareValidator>
            <asp:RegularExpressionValidator ID="regEmail_UserID" runat="server" Display="None" ErrorMessage="Invalid email/userid"
                ControlToValidate="txtEmail_UserID" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </asp:Panel>

        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
</asp:Content>