﻿Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports Advantage.Business.Logic.Layer
Imports FAME.Advantage.Common

Partial Class EmployeeHRInfo
    Inherits BasePage

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button

    ' This variable holds empId. The value assigned is for testing only
    Protected empId As String '= "EC633761-20D9-4A9E-AA18-3486B8F42AA8"
    Protected empHRInfoId As String
    Protected strVID As String
    Protected state As AdvantageSessionState
    Protected resourceId As Integer
    Protected moduleid As String
    Protected sdfcontrols As New SDFComponent
    Protected boolSwitchCampus As Boolean = False
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


#End Region

    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Protected userId As String

    Private mruProvider As MRURoutines
    Protected EmployeeId As String
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function getEmployeeFromStateObject(ByVal paramResourceId As Integer) As BO.EmployeeMRU

        Dim objStateInfo As New AdvantageStateInfo
        Dim objEmployeeState As New BO.EmployeeMRU


        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        MyBase.GlobalSearchHandler(3)

        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
        HttpContext.Current.Items("Language") = "En-US"

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            EmployeeId = Guid.Empty.ToString()
        Else
            EmployeeId = AdvantageSession.MasterEmployeeId
        End If


        txtEmpId.Text = EmployeeId


        Dim objGetStudentStatusBar As New AdvantageStateInfo

        mruProvider = New MRURoutines(Me.Context, GetAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)
        objGetStudentStatusBar = mruProvider.BuildEmployeeStatusBar(AdvantageSession.MasterEmployeeId)
        With objGetStudentStatusBar
            AdvantageSession.MasterName = objGetStudentStatusBar.NameValue
            AdvantageSession.MasterEmployeeName = objGetStudentStatusBar.NameValue
            AdvantageSession.MasterEmployeeAddress1 = objGetStudentStatusBar.Address1
            AdvantageSession.MasterEmployeeAddress2 = objGetStudentStatusBar.Address2
            AdvantageSession.MasterEmployeeCity = objGetStudentStatusBar.City
            AdvantageSession.MasterEmployeeState = objGetStudentStatusBar.State
            AdvantageSession.MasterEmployeeZip = objGetStudentStatusBar.Zip

        End With

        If Not String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            Master.ShowHideStatusBarControl(True)
            Master.PageObjectId = EmployeeId
            Master.PageResourceId = Request.QueryString("resid")
            Master.setHiddenControlForAudit()
        Else
            Master.ShowHideStatusBarControl(False)
        End If

        With objEmployeeState
            .EmployeeId = New Guid(EmployeeId)
            .Name = objStateInfo.NameValue
        End With

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            Return Nothing
        Else
            Return objEmployeeState
        End If


    End Function

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        ' Dim m_Context As HttpContext

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        campusId = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        '   if we have an employeeHRInfo in the session then use it, else create a new one


        Dim objEmployeeState As New BO.EmployeeMRU
        objEmployeeState = getEmployeeFromStateObject(55) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objEmployeeState Is Nothing Then
            MyBase.RedirectToEmployeeSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objEmployeeState
            empId = .EmployeeId.ToString

        End With


        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        If CommonWebUtilities.IsValidGuid(empId) Then
            If state("employeeHRInfo") Is Nothing Then
                state("employeeHRInfo") = (New EmployeesFacade).GetHRInfo(empId)
            Else
                '   if this is not the same employee that we have in state... goto the backend
                If Not CType(state("employeeHRInfo"), FAME.AdvantageV1.Common.EmployeeHRInfo).EmpId = empId Then
                    state("employeeHRInfo") = (New EmployeesFacade).GetHRInfo(empId)
                End If
            End If
        Else
            state("employeeHRInfo") = New FAME.AdvantageV1.Common.EmployeeHRInfo
        End If

        empHRInfoId = CType(state("employeeHRInfo"), FAME.AdvantageV1.Common.EmployeeHRInfo).EmpHRInfoId
        Master.PageObjectId = empHRInfoId
        Master.PageResourceId = Request.QueryString("resid")
        Master.setHiddenControlForAudit()
        'set header object to be used in the Audit History
        'Header1.ObjectID = empHRInfoId

        'pObj = Header1.UserPagePermission
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(3, AdvantageSession.MasterName)
            End If
            'objCommon.PageSetup(Form1, "NEW")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   initialize date range validator
            '   hire dates must be in the past and less than 50 years ago
            'RangeValidator1.MaximumValue = Date.Now.ToShortDateString
            'RangeValidator1.MinimumValue = Date.Now.Subtract(New TimeSpan(18262, 0, 0, 0, 0)).ToShortDateString

            '   build dropdownlists
            BuildDropDownLists()

            '   populate HireDate, Position and Department
            PopulateFields(state("employeeHRInfo"))

            '   Populate and bind ListBoxes
            PopulateAndBindListBoxes(empId)
            MyBase.uSearchEntityControlId.Value = ""

        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT")
        End If
        InitButtonsForEdit()
        'moduleid = HttpContext.Current.Request.Params("Mod").ToString
        moduleid = AdvantageSession.UserState.ModuleCode.ToString

        'Check If any UDF exists for this resource
        '  Dim intSDFExists As Integer = sdfcontrols.GetSDFExists(resourceId, moduleid)
        ' If intSDFExists >= 1 Then
        ' pnlUDFHeader.Visible = True
        ' Else
        pnlUDFHeader.Visible = False
        'End If


    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '   update Employee Contact Info 
        state("employeeHRInfo") = BuildEmployeeHRInfo(empId)
        Dim result As String = (New EmployeesFacade).UpdateHRInfo(state("employeeHRInfo"), AdvantageSession.UserState.UserName)
        If Not result = "" Then
            '   Display Error Message
            'DisplayErrorMessage(result)
            DisplayRADAlert(CallbackType.Postback, "Error1", result, "Save Error")
            Exit Sub
        Else
            '   populate HireDate, Position and Department
            state("employeeHRInfo") = (New EmployeesFacade).GetHRInfo(empId)
            state("empId") = CType(state("employeeHRInfo"), FAME.AdvantageV1.Common.EmployeeHRInfo).EmpId
            PopulateFields(state("employeeHRInfo"))
        End If

        '   Update Degrees and Certifications CheckBoxList Selections
        UpdateBackendAccordingToUserSelections(empId)

        '   Populate and bind ListBoxes
        PopulateAndBindListBoxes(empId)

        'Dim objCommon As New CommonUtilities
        txtEmpId.Text = empId
        'If ViewState("MODE") = "NEW" Then
        'objCommon.DoInsert(Form1)
        'Set the Primary Key value, so it can be used later by the DoUpdate method
        'txtEmpHRInfoId.Text = objCommon.PagePK
        'txtEmpId.Text = empId
        'Set Mode to EDIT since a record has been inserted into the db.
        'objCommon.SetBtnState(Form1, "EDIT")
        'ViewState("MODE") = "EDIT"

        'ElseIf viewstate("MODE") = "EDIT" Then
        'objCommon.DoUpdate(Form1)
        'Response.Write(objCommon.strText)

        '   Update Degrees and Certifications CheckBoxList Selections
        'UpdateBackendAccordingToUserSelections(empId)
        '

        '   Populate and bind ListBoxes
        'PopulateAndBindListBoxes(empId)
        'End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim SDFID As ArrayList
        Dim SDFIDValue As ArrayList
        '        Dim newArr As ArrayList
        Dim z As Integer
        Dim SDFControl As New SDFComponent
        Try
            SDFControl.DeleteSDFValue(txtEmpHRInfoId.Text)
            SDFID = SDFControl.GetAllLabels(pnlSDF)
            SDFIDValue = SDFControl.GetAllValues(pnlSDF)
            For z = 0 To SDFID.Count - 1
                SDFControl.InsertValues(txtEmpHRInfoId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


    End Sub
    Private Sub PopulateAndBindListBoxes(ByVal empId As String)
        '   Populate Degrees CheckBoxList
        '
        PopulateDegreesListBox()
        BindDegreesListBox(empId)

        '   Populate Certifications CheckBoxList
        '
        PopulateCertificationsListBox()
        BindCertificationsListBox(empId)

    End Sub
    Private Sub UpdateBackendAccordingToUserSelections(ByVal empId As String)
        '   Update Degrees CheckBoxList Selections
        UpdateEmployeeDegrees(empId)

        '   Update Degrees CheckBoxList Selections
        UpdateEmployeeCertifications(empId)
    End Sub
    Private Sub PopulateDegreesListBox()
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim degrees As New DegreesFacade
        ' Bind the Dataset to the CheckBoxList
        cblDegrees.DataTextField = "DegreeDescrip"
        cblDegrees.DataValueField = "DegreeId"
        cblDegrees.DataSource = degrees.GetAllDegrees()
        cblDegrees.DataBind()
    End Sub
    Private Sub PopulateCertificationsListBox()
        '
        '   Get Certifications data to bind the CheckBoxList
        '
        Dim certifications As New CertificationsFacade
        ' Bind the Dataset to the CheckBoxList
        cblCertifications.DataTextField = "CertificationDescrip"
        cblCertifications.DataValueField = "CertificationId"
        cblCertifications.DataSource = certifications.GetAllCertifications()
        cblCertifications.DataBind()
    End Sub
    Private Sub BindDegreesListBox(ByVal empId As String)
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim employees As New EmployeesFacade
        Dim ds As DataSet = employees.GetDegreesPerEmployee(empId)

        ' Select the items on the CheckBoxList
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim row As DataRow
            row = ds.Tables(0).Rows(i)
            Dim item As ListItem
            For Each item In cblDegrees.Items
                If item.Value.ToString = DirectCast(row("DegreeId"), Guid).ToString Then
                    item.Selected = True
                    Exit For
                End If
            Next
        Next
    End Sub
    Private Sub BindCertificationsListBox(ByVal empId As String)
        '
        '   Get Certifications data to bind the CheckBoxList
        '
        Dim employees As New EmployeesFacade
        Dim ds As DataSet = employees.GetCertificationsPerEmployee(empId)

        ' Select the items on the CheckBoxList
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim row As DataRow
            row = ds.Tables(0).Rows(i)
            Dim item As ListItem
            For Each item In cblCertifications.Items
                If item.Value.ToString = DirectCast(row("CertId"), Guid).ToString Then
                    item.Selected = True
                    Exit For
                End If
            Next
        Next
    End Sub
    ' Update EmpDegrees table
    Private Sub UpdateEmployeeDegrees(ByVal empId As String)

        '   create an array string with selected Degrees. The initial size of the array is the number of items in the cblDegrees
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), cblDegrees.Items.Count)

        Dim i As Integer = 0
        Dim item As ListItem
        For Each item In cblDegrees.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        '   resize the array
        If i > 0 Then
            ReDim Preserve selectedDegrees(i - 1)
        Else
            selectedDegrees = Nothing
        End If


        '   update selected Degrees (Roles)
        Dim employees As New EmployeesFacade
        If employees.UpdateEmployeeDegrees(empId, AdvantageSession.UserState.UserName, selectedDegrees) < 0 Then
            Customvalidator1.ErrorMessage = "There was a problem with Referential Integrity.<BR> You can not perform this operation."
            Customvalidator1.IsValid = False
        End If

    End Sub

    ' Update EmployeeInfoCertifications table
    Private Sub UpdateEmployeeCertifications(ByVal empId As String)
        '   create an array string with selected Certifications. The initial size of the array is the number of items in the cblCertifications
        Dim selectedCertifications() As String = Array.CreateInstance(GetType(String), cblCertifications.Items.Count)

        Dim i As Integer = 0
        Dim item As ListItem
        For Each item In cblCertifications.Items
            If item.Selected Then
                selectedCertifications.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        '   resize the array
        If i > 0 Then
            ReDim Preserve selectedCertifications(i - 1)
        Else
            selectedCertifications = Nothing
        End If


        '   update selected Certifications 
        Dim employees As New EmployeesFacade
        If employees.UpdateEmployeeCertifications(empId, AdvantageSession.UserState.UserName, selectedCertifications) <> "" Then
            Customvalidator1.ErrorMessage = "There was a problem with Referential Integrity.<BR> You can not perform this operation."
            Customvalidator1.IsValid = False
        End If
    End Sub
    Private Sub PopulateFields(ByVal empHRInfo As FAME.AdvantageV1.Common.EmployeeHRInfo)

        With empHRInfo
            '   IsInDB
            cbxIsInDB.Checked = .IsInDB

            '   EmpHRInfoId
            txtEmpHRInfoId.Text = .EmpHRInfoId

            '   HireDate
            If Not (.HireDate = Date.MaxValue) Then
                txtHireDate.SelectedDate = CType(.HireDate, Date).ToShortDateString
            Else
                txtHireDate.SelectedDate = Nothing
            End If

            '   bind Position
            'ddlPositionId.SelectedValue = .PositionId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPositionId, .PositionId, .Position)

            '   bind Department
            ddlDepartmentId.SelectedValue = .DepartmentId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDepartmentId, .DepartmentId, .Department)

            '   bind modUser
            txtModUser.Text = .ModUser

            '   bind ModDate
            txtModDate.Text = .ModDate.ToString
        End With

    End Sub
    Private Function BuildEmployeeHRInfo(ByVal empId As String) As FAME.AdvantageV1.Common.EmployeeHRInfo

        '   instantiate class
        Dim empHRInfo As New FAME.AdvantageV1.Common.EmployeeHRInfo(empId)

        With empHRInfo
            '   get IsInDB
            .IsInDB = cbxIsInDB.Checked

            '   get EmpHRInfoId
            .EmpHRInfoId = txtEmpHRInfoId.Text

            '   get HireDate
            If (txtHireDate.SelectedDate.ToString = Nothing) Then
                .HireDate = Date.MaxValue
            Else
                .HireDate = txtHireDate.SelectedDate
            End If

            '   get PositionId
            .PositionId = ddlPositionId.SelectedValue

            '   get HomePhone
            .DepartmentId = ddlDepartmentId.SelectedValue

            '   get EmpId
            .EmpId = empId

            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With

        '   return data
        Return empHRInfo

    End Function
    Private Sub BuildDropDownLists()
        'BuildPositionsDDL()
        'BuildDepartmentsDDL()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Dim x1 As PositionsDDLMetadata
        'Dim x2 As HRDepartmentsDDLMetadata

        'Positions DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlPositionId, AdvantageDropDownListName.Positions, campusId, True, True))

        'HRDepartments DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlDepartmentId, AdvantageDropDownListName.HRDepartments, campusId, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub
    'Private Sub BuildPositionsDDL()
    '    '   bind the Position DDL
    '    Dim positions As New PositionsFacade

    '    With ddlPositionId
    '        .DataTextField = "PositionDescrip"
    '        .DataValueField = "PositionId"
    '        .DataSource = positions.GetAllPositions()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With

    'End Sub
    'Private Sub BuildDepartmentsDDL()
    '    '   bind the Department DDL
    '    Dim departments As New EmployeesFacade

    '    With ddlDepartmentId
    '        .DataTextField = "HRDepartmentDescrip"
    '        .DataValueField = "HRDepartmentId"
    '        .DataSource = departments.GetAllHRDepartments()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)

    '    '   Set error condition
    '    Customvalidator1.ErrorMessage = errorMessage
    '    Customvalidator1.IsValid = False

    '    If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
    '        '   Display error in message box in the client
    '        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    '    End If

    'End Sub
    Private Sub InitButtonsForLoad()

        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        btnNew.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        'btnNew.Enabled = False
        'btnDelete.Enabled = True

        ''Set the Delete Button so it prompts the user for confirmation when clicked
        'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = False
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = False
        Else
            btnNew.Enabled = False
        End If
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'state("employeeHRInfo") = BuildEmployeeHRInfo(empId)
        'save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
        ''add to this list any button or link that should ignore the Confirm Exit Warning.
        'Dim controlsToIgnore As New ArrayList()
        ''add save button 
        'controlsToIgnore.Add(btnSave)
        'controlsToIgnore.Add(txtHireDate)
        ''Add javascript code to warn the user about non saved changes 
        'CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
