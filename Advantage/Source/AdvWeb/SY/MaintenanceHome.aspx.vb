﻿
Partial Class SY_MaintenanceHome
    Inherits BasePage

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            If Not Session("UserName") Is Nothing Then
                hdnUserName.Value = Session("UserName").ToString()
                hdnUserId.Value = Session("UserId").ToString()
            End If

            For Each tag As HtmlMeta In From tag1 In Page.Header.Controls.OfType(Of HtmlMeta)() Where (tag1.Name = "compat_mode")
                tag.Content = "IE-Edge"
            Next

            AdvantageSession.PageBreadCrumb = "Maintenance Home /"
        End If
    End Sub

End Class
