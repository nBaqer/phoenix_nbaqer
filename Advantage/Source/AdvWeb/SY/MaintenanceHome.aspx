﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NewSite.master" CodeFile="MaintenanceHome.aspx.vb" Inherits="SY_MaintenanceHome" EnableViewState="false" %>


<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="content4" ContentPlaceHolderID="additional_head" runat="server">
</asp:Content>
<asp:Content ID="content1" ContentPlaceHolderID="contentmain2" runat="server">
    <div id='frame'>
        <div>
            <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
                <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
                    <script src="../Scripts/DataSources/maintenanceModulesDataSource.js"></script>
                    <script src="../Scripts/common/maintenanceCommon.js"></script>
                    <script src="../Scripts/viewModels/maintenanceHome.js"></script>
                    <div id='content'>
                        <table border="0" id="searchDocsForm" cellpadding="0" cellspacing="3" width="100%" style="margin: 0px; position: relative; text-align: center;">
                            <tr>
                                <td>
                                    <input id="maintenanceModulesDropDownList" data-role="dropdownlist" data-value-field="Id" data-text-field="text" style="width: 200px" /></td>
                            </tr>
                        </table>
                        <div id="loading"></div>
                        <br />
                        <div id="reportData">
                            <table width="100%" id="maintenanceLinks" style="margin-left: 5px;">
                                <tr>
                                    <td>
                                        <table width="70%">
                                            <tr>
                                                <td colspan="3" id="AdmissionsLinks" valign="top"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="hr1">
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td colspan="3" id="AcademicsLinks" valign="top">
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="hr2">
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td id="StudentAccountsLinks" valign="top"></td>
                                                <td id="FinancialAidLinks" valign="top"></td>
                                                <td id="HumanResourcesLinks" valign="top"></td>
                                                <td id="PlacementLinks" valign="top"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td id="hr3">
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <table width="80%">
                                            <tr>
                                                
                                                <td id="SystemLinks" valign="top"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                        <table width="80%">
                                            <tr>
                                                <td colspan="3" id="ReportsLinks" valign="top"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>
                        </div>
                        <div id="errorDetails" style="display: none"></div>
                        <input type="hidden" id="hdnUserName" runat="server" class="hdnUserName" clientidmode="Static" />
                        <input type="hidden" id="hdnUserId" runat="server" class="hdnUserName" clientidmode="Static" />
                    </div>
                </telerik:RadPane>
            </telerik:RadSplitter>
        </div>
    </div>
</asp:Content>
