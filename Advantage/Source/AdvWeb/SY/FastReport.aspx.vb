﻿
Imports Fame.Advantage.Common
Imports Fame.AdvantageV1
Imports Microsoft.Reporting.WebForms

Namespace AdvWeb.SY
    Partial Class SyFastReport
        Inherits BasePage

        Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

            Try
                'ReportViewer1.Reset()
                Dim setting = AdvAppSettings.GetAppSettings()
                Dim reportServer = setting.AppSettings("ReportServerInstance").ToString()
                ReportViewer1.ServerReport.ReportServerUrl = New Uri(reportServer)
                If (IsPostBack) Then

                    Dim parameter As String = Request("__EVENTARGUMENT")
                    Dim control As String = Request("__EVENTTARGET")

                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "IsPostBack", "var isPostBack = true;", True)

                    If (control = "GoAndChangeReport") Then
                        ReportViewer1.Visible = True
                        RunReportServer(setting, parameter)
                    End If
                Else



                    ' Analysis if the call to the page is only for one report
                    Dim report As String = Request.QueryString("desc")
                    Dim item = BusinessFacade.ReportFacade.GetReportsItem(report)
                    If (item.Type = 2) Then
                        'Mark that the report is running from server
                        Dim script = String.Format("var runningReport = '{0}';", item.Description)
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "RunningReport", script, True)

                        'It is a report. Execute Immediate the Report
                        RunReportServer(setting, item.Path)

                    End If
                End If
            Catch ex As UriFormatException
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                Dim message = String.Format("The Fast Report Service is not configured.{0} Please enter the Report Server Address at [ReportServerInstance] setting", Environment.NewLine)
                CommonWebUtilities.DisplayErrorInMessageBox(Page, message)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                CommonWebUtilities.DisplayErrorInMessageBox(Page, ex.Message)
            End Try


        End Sub

        Private Sub RunReportServer(setting As AdvAppSettings, parameter As String)

            Dim conn = setting.AppSettings("ConnectionString").ToString()
            ReportViewer1.ServerReport.ReportPath = parameter
            Dim param = New ReportParameter()
            param.Name = "ConnectionString"
            param.Values.Add(conn)
            param.Visible = False
            If parameter.Contains("Absent Today Report") Or parameter.Contains("In School Today Report") Then
                Dim param1 = New ReportParameter()
                param1.Name = "CampusId"
                param1.Values.Add(Master.CurrentCampusId)
                param1.Visible = False
                Dim parameters() As ReportParameter = {param, param1}
                ReportViewer1.ServerReport.SetParameters(parameters)
            Else
                Dim reportParameters As ReportParameterInfoCollection = ReportViewer1.ServerReport.GetParameters()
                If (Not reportParameters Is Nothing) Then
                    If (reportParameters.Any(Function(x) x.Name = "CampusId")) Then
                        Dim param1 = New ReportParameter()
                        param1.Name = "CampusId"
                        param1.Values.Add(Master.CurrentCampusId)
                        param1.Visible = False
                        Dim parameters() As ReportParameter = {param, param1}
                        ReportViewer1.ServerReport.SetParameters(parameters)
                    Else
                        Dim parameters() As ReportParameter = {param}
                        ReportViewer1.ServerReport.SetParameters(parameters)
                    End If
                Else

                    Dim parameters() As ReportParameter = {param}
                    ReportViewer1.ServerReport.SetParameters(parameters)
                End If
            End If
            ReportViewer1.ServerReport.Refresh()
        End Sub
    End Class
End Namespace