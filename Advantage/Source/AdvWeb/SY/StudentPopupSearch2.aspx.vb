' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' StudentPopupSearch2.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================

Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports BO = Advantage.Business.Objects

Partial Class StudentPopupSearch2
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblStudentID As System.Web.UI.WebControls.Label
    Protected WithEvents txtStudentID As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlStatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents btnNew As System.Web.UI.WebControls.Button
    Protected WithEvents btnDelete As System.Web.UI.WebControls.Button
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents StudentLNameLinkButton As System.Web.UI.WebControls.LinkButton

   
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Private campusDesc As String = String.Empty
    Private resourceId As Integer
    '' Added by Vijay Ramteke on April 21, 2010 For Mantis Id 18185
    Private modName As String
    '' Added by Vijay Ramteke on April 21, 2010 For Mantis Id 18185
    ''Added by Saraswathi lakshmanan on August 2009
    ''For mantis case 17126
    Dim isSystemAdministrator As Boolean = False
    Private MaxFieldLengths(5) As Integer
    ' DE7569 5/1/2012 Janet Robinson
    Dim userId As String = String.Empty
    Dim userName As String = String.Empty

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState


        '  Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade

        ' resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'campusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ' DE7569 5/1/2012 Janet Robinson
        userName = AdvantageSession.UserState.UserName

        resourceId = CInt(Request.QueryString("resid"))
        campusId = Request.QueryString("cmpid")

        'Added By Vijay Ramteke on April 21, 2010
        modName = Request.QueryString("mod")
        'Added By Vijay Ramteke on April 21, 2010

        'userId = Request.QueryString("userid")
        ''Added by saraswathi lakshmanan to show the InSchool enrollment for non Sa users.
        ''Added to fix iisue mantis 14829  -modified on June 10 2009
        ''resourceid=85- Post payments 94- post refunds
        If resourceId = 85 Or resourceId = 94 Then
            'pObj = fac.GetUserResourcePermissions(userId, 357, campusId)
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, 357, campusId)
        Else
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        End If


        Dim StudentSearchFacade As New StudentSearchFacade
        isSystemAdministrator = StudentSearchFacade.GetIsRoleSystemAdministarator(userId)
        ViewState("isSystemAdministrator") = isSystemAdministrator

        If Not Page.IsPostBack Then
            '   build dropdownlists
            BuildDropdownlists()
            BindDataListMRU()
            If campusDesc = String.Empty Then
                lbHdr.Text = "Recently Accessed Students"
            Else
                lbHdr.Text = "Recently Accessed Students from " & campusDesc
            End If
        End If

    End Sub
    Private Sub BuildDropdownlists()
        BuildStatusDDL()
        BuildPrgVerDDL()
        BuildTermsDDL()
        BuildAcademicYearsDDL()
    End Sub
    Private Sub BuildStatusDDL()
        'Bind the Status DropDownList
        Dim ddlStatusId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStatusId"), DropDownList)
        Dim statuses As New StatusCodeFacade
        With ddlStatusId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            ''Modified by saraswathi lakshmanan on june 10 2009
            ''Added to fix issue 14829
            ''For post payment and post refunds page, only the inschool enrollments are shown for non Sa users
            ''Commented by Saraswathi lakshmanan
            ''August 11 2009
            ' ''If (resourceId = 85 Or resourceId = 94) And Session("UserName") <> "sa" Then
            ' ''    .DataSource = statuses.GetAllStatusCodesForStudentsWithInSchoolEnrollment(campusId)
            ' ''Else
            ' ''    .DataSource = statuses.GetAllStatusCodesForStudents(campusId)
            ' ''End If
            .DataSource = statuses.GetAllStatusCodesForStudents(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
        'With ddlStatusId
        '    .DataTextField = "Status"
        '    .DataValueField = "StatusId"
        '    .DataSource = (New StatusesFacade).GetAllStatuses()
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        '    .SelectedIndex = 0
        'End With
    End Sub
    Private Sub BuildPrgVerDDL()
        Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStudentPrgVerId"), DropDownList)
        'Bind the Programs DrowDownList
        With ddlStudentPrgVerId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = (New StudentSearchFacade).GetAllEnrollmentsUsedByStudents()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildTermsDDL()
        '   bind the Terms DDL
        Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
        Dim terms As New StudentsAccountsFacade

        With ddlTermId
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = terms.GetAllTerms(True)
            .DataBind()
            '.Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
            If .Items.Count > 0 Then .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildTermsDDL(ByVal stuEnrollId As String, ByVal campusid As String)
        '   bind the Terms DDL
        Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
        Dim terms As New StudentSearchFacade

        With ddlTermId
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = terms.getAllStudentTerm(stuEnrollId, campusid)
            .SelectedIndex = -1
            .DataBind()
            '.Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
            If .Items.Count > 0 Then .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildAcademicYearsDDL()
        '   bind the AcademicYearss DDL
        Dim ddlAcademicYearId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlAcademicYearId"), DropDownList)
        Dim academicYears As New StudentsAccountsFacade

        With ddlAcademicYearId
            .DataTextField = "AcademicYearDescrip"
            .DataValueField = "AcademicYearId"
            .DataSource = academicYears.GetAllAcademicYears()
            .DataBind()
            .SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYearId.Items)
        End With

    End Sub
    Private Sub BindDataList()
        Dim txtLastName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtLastName"), TextBox)
        Dim txtFirstName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtFirstName"), TextBox)
        Dim txtEnrollment As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtEnrollment"), TextBox)
        Dim ddlStatusId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStatusId"), DropDownList)
        Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStudentPrgVerId"), DropDownList)
        Dim txtSSN As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtSSN"), TextBox)
        Dim txtStudentNumber As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtStudentNumber"), TextBox)
        '   Bind datalist
        ''Added by saraswathi lakshmanan to show the InSchool enrollment for non Sa users.
        ''Added to fix iisue mantis 14829  -modified on June 10 2009
        ''resourceid=85- Post payments 94- post refunds
        If (resourceId = 85 Or resourceId = 94) And (AdvantageSession.UserState.UserName <> "sa") And (ViewState("isSystemAdministrator") = False) Then
            dgrdTransactionSearch.DataSource = New DataView((New StudentSearchFacade).GetStudentSearchDSWithStudentNumberforCurrentEnrollment(txtLastName.Text, txtFirstName.Text, txtSSN.Text, txtEnrollment.Text, ddlStatusId.SelectedValue, ddlStudentPrgVerId.SelectedValue, campusId, txtStudentNumber.Text).Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
            dgrdTransactionSearch.DataBind()

        Else
            dgrdTransactionSearch.DataSource = New DataView((New StudentSearchFacade).GetStudentSearchDSWithStudentNumber(txtLastName.Text, txtFirstName.Text, txtSSN.Text, txtEnrollment.Text, ddlStatusId.SelectedValue, ddlStudentPrgVerId.SelectedValue, campusId, txtStudentNumber.Text).Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
            dgrdTransactionSearch.DataBind()

        End If

    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim txtLastName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtLastName"), TextBox)
        Dim txtFirstName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtFirstName"), TextBox)
        Dim txtEnrollment As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtEnrollment"), TextBox)
        Dim ddlStatusId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStatusId"), DropDownList)
        Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStudentPrgVerId"), DropDownList)
        Dim txtSSN As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtSSN"), TextBox)
        Dim txtStudentNumber As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtStudentNumber"), TextBox)

        If txtLastName.Text = "" And txtFirstName.Text = "" And txtEnrollment.Text = "" And ddlStatusId.SelectedValue = Guid.Empty.ToString And ddlStudentPrgVerId.SelectedValue = Guid.Empty.ToString And txtSSN.Text = "" And txtStudentNumber.Text = "" Then
            'DisplayErrorMessage("Please enter a value to search")
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EM", "alert('Please enter a value to search');", True)
            Exit Sub
        End If

        '   bind datalist
        BindDataList()

    End Sub
    Private Sub dgrdTransactionSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTransactionSearch.ItemCommand
        Dim parametersArray(6) As String '= {e.CommandArgument, (New StudentSearchFacade).GetStudentNameByID(e.CommandArgument)}

        '   set StudentName
        parametersArray(0) = CType(e.Item.FindControl("lnkLastName"), LinkButton).Text + ", " + CType(e.Item.FindControl("lblFirstName1"), Label).Text

        '   set StuEnrollment
        Dim ddlEnrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)
        parametersArray(1) = ddlEnrollments.SelectedItem.Value
        parametersArray(2) = ddlEnrollments.SelectedItem.Text

        '   set Terms
        Dim ddlTerms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)
        parametersArray(3) = ddlTerms.SelectedItem.Value
        parametersArray(4) = ddlTerms.SelectedItem.Text

        '   set AcademicYears
        Dim ddlAcademicYears As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)
        parametersArray(5) = ddlAcademicYears.SelectedItem.Value
        parametersArray(6) = ddlAcademicYears.SelectedItem.Text

        Session("StuEnrollment") = ddlEnrollments.SelectedItem.Text
        Session("AcademicYear") = ddlAcademicYears.SelectedItem.Text
        Session("Term") = ddlTerms.SelectedItem.Text

        '' ''   send parameters back to the parent page
        ''CommonWebUtilities.PassDataBackToParentPageAndFireClickEvent(Page, parametersArray)
        PassDataBackToParentPageAndFireClickEvent(Page, parametersArray)
        '' ''CommonWebUtilities.PassDataBackToParentPage(Page, parametersArray)
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub

    Private Sub dgrdTransactionSearch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdTransactionSearch.ItemDataBound

        '   process only rows in the items section
        Select Case e.Item.ItemType

            Case ListItemType.Item, ListItemType.AlternatingItem


                '   Register Max field length for Last Name
                RegisterMaxFieldLength(0, e.Item.DataItem("LastName"))

                '   Register Max field length for First Name
                RegisterMaxFieldLength(1, e.Item.DataItem("FirstName"))

                '   Register Max field length for SSN
                RegisterMaxFieldLength(2, e.Item.DataItem("SSN"))




                '   get Enrollments dropdownlist
                Dim enrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)

                '   populate  Enrollments ddl
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim drows() As DataRow = dr.Row.GetChildRows("StudentsStudentEnrollments")
                For i As Integer = 0 To drows.Length - 1
                    enrollments.Items.Add(New ListItem(drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment"), CType(drows(i)("StuEnrollId"), Guid).ToString))

                    '   Register Max field length for Enrollments. Add some slack because it is a dropdownlist
                    RegisterMaxFieldLength(3, drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment") + "XXXX")
                Next

                ''   first Enrollment is selected
                'enrollments.SelectedIndex = 0                

                'select latest enrollment
                If Not drows.Length = 0 Then
                    'sort terms by date
                    Dim idx(drows.Length - 1, 1) As Double
                    For i As Integer = 0 To drows.Length - 1
                        idx(i, 0) = i
                        If Not drows(i)("StartDate") Is System.DBNull.Value Then
                            idx(i, 1) = CType(drows(i)("StartDate"), Date).Ticks
                        Else
                            idx(i, 1) = 0
                        End If
                    Next

                    'sort the terms by Date
                    If drows.Length > 1 Then
                        For j As Integer = 1 To drows.Length - 1
                            For k As Integer = j To drows.Length - 1
                                If idx(j, 1) > idx(j - 1, 1) Then
                                    Dim t0 As Double = idx(j - 1, 0)
                                    Dim t1 As Double = idx(j - 1, 1)
                                    idx(j - 1, 0) = idx(j, 0)
                                    idx(j - 1, 1) = idx(j, 1)
                                    idx(j, 0) = t0
                                    idx(j, 1) = t1
                                End If
                            Next
                        Next
                    End If

                    '   latest Enrollment is selected
                    enrollments.SelectedIndex = idx(0, 0)
                Else
                    enrollments.SelectedIndex = 0
                    CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                End If




                ''   get Terms dropdownlist
                'Dim terms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)

                ''   populate Terms ddl
                'For i As Integer = 0 To ddlTermId.Items.Count - 1
                '    terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                '    '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                '    RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                'Next

                '   first Term is selected
                'ddlTermId.SelectedIndex = 0

                '   get Terms dropdownlist
                Dim terms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)

                'get all student terms
                ' Dim row As DataRow
                Dim trows() As DataRow = dr.Row.GetChildRows("StudentStudentTerms")


                'if the student has no Terms ... populate all terms
                If Not trows.Length = 0 Then
                    'sort terms by date
                    Dim idx(trows.Length - 1, 1) As Double
                    For i As Integer = 0 To trows.Length - 1
                        idx(i, 0) = i
                        idx(i, 1) = CType(trows(i)("StartDate"), Date).Ticks
                    Next

                    'sort the terms by Date
                    If trows.Length > 1 Then
                        For j As Integer = 1 To trows.Length - 1
                            For k As Integer = j To trows.Length - 1
                                If idx(j, 1) > idx(j - 1, 1) Then
                                    Dim t0 As Double = idx(j - 1, 0)
                                    Dim t1 As Double = idx(j - 1, 1)
                                    idx(j - 1, 0) = idx(j, 0)
                                    idx(j - 1, 1) = idx(j, 1)
                                    idx(j, 0) = t0
                                    idx(j, 1) = t1
                                End If
                            Next
                        Next
                    End If
                    Dim cnt As Integer = 0
                    For i As Integer = 0 To trows.Length - 1
                        'populate only student terms
                        If trows(idx(i, 0))("StuEnrollId").ToString = enrollments.SelectedValue Then
                            terms.Items.Add(New ListItem(trows(idx(i, 0)).GetParentRow("TermsStudentTerms")("TermDescrip"), CType(trows(idx(i, 0))("TermId"), Guid).ToString))

                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                            RegisterMaxFieldLength(4, terms.Items(cnt).Text + "XXXX")
                            cnt = cnt + 1
                        End If
                    Next

                    '   select latest term for the student
                    If Not dr.Row("CurrentTerm") Is System.DBNull.Value Then
                        terms.SelectedValue = CType(dr.Row("CurrentTerm"), Guid).ToString
                    Else
                        terms.SelectedIndex = 0
                    End If
                    If terms.Items.Count = 0 Then
                        BuildTermsDDL(enrollments.SelectedValue.ToString(), campusId)
                        Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                        For i As Integer = 0 To ddlTermId.Items.Count - 1
                            terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                            RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                        Next
                        If ddlTermId.Items.Count = 0 Then
                            CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                        End If
                        '   first Enrollment is selected
                        terms.SelectedIndex = 0
                    End If
                Else
                    '   populate Terms ddl
                    BuildTermsDDL(enrollments.SelectedValue.ToString(), campusId)
                    Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                    For i As Integer = 0 To ddlTermId.Items.Count - 1
                        terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                        '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                        RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                    Next
                    If ddlTermId.Items.Count = 0 Then
                        CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                    End If
                    '   first Enrollment is selected
                    terms.SelectedIndex = 0
                End If

                '   get AcademicYear dropdownlist
                Dim academicYear As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)
                Dim ddlAcademicYearId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlAcademicYearId"), DropDownList)
                '   populate AcademicYear ddl
                For Each item As ListItem In ddlAcademicYearId.Items
                    Dim newItem As New ListItem(item.Text, item.Value)
                    newItem.Selected = item.Selected
                    academicYear.Items.Add(newItem)

                    '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                    RegisterMaxFieldLength(5, item.Text + "XXXX")
                Next

            Case ListItemType.Footer

                If SumOfMaxFieldLengths() > 0 Then
                    '   calculate the width percentage of each cell in the footer
                    Dim cells As ControlCollection = e.Item.Controls
                    For i As Integer = 0 To cells.Count - 1
                        Dim cell As TableCell = CType(cells(i), TableCell)
                        '   add the width attribute to each cell only if there are records
                        cell.Attributes.Add("width", CType(MaxFieldLengths(i) * 100.0 / SumOfMaxFieldLengths(), Integer).ToString + "%")
                    Next
                End If
        End Select
    End Sub
    Private Sub RegisterMaxFieldLength(ByVal idx As Integer, ByVal txt As Object)
        If Not txt.GetType.ToString = "System.DBNull" Then
            Dim text As String = CType(txt, String)
            If text.Length > MaxFieldLengths(idx) Then
                MaxFieldLengths(idx) = text.Length
            End If
        End If
    End Sub
    Private Function SumOfMaxFieldLengths() As Integer
        SumOfMaxFieldLengths = 0
        For i As Integer = 0 To MaxFieldLengths.Length - 1
            SumOfMaxFieldLengths += MaxFieldLengths(i)
        Next
    End Function
    Public Sub ddqty_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        'Dim drop As DropDownList = CType(sender, DropDownList)
        'Response.Write(drop.SelectedItem.Value)
        ''Dim ddlTermId As DropDownList = CType(dgrdTransactionSearch.Items(dgrdTransactionSearch.Items).FindControl("ddlTermId"), DropDownList)
        ''ddlTermId.Items.Clear()
        'Dim cell As TableCell = CType(drop.Parent, TableCell)
        'Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
        'Dim i As Integer = item.ItemIndex
        'Dim content As String = item.Cells(0).Text


        Dim ddllist As DropDownList = CType(sender, DropDownList)
        Dim cell As TableCell = CType(ddllist.Parent, TableCell)
        Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
        Dim content As String = item.Cells(0).Text
        Dim ddlType As DropDownList = CType(item.Cells(0).FindControl("ddlEnrollmentsId"), DropDownList)
        Dim ddlItem As DropDownList = CType(item.Cells(1).FindControl("ddlTermsId"), DropDownList)

        ddlItem.Items.Clear()
        With ddlItem
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = (New StudentSearchFacade).getStudentTerm(ddlType.SelectedValue, campusId)
            .DataBind()
            '.SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYearId.Items)
        End With
        If ddlItem.Items.Count = 0 Then
            With ddlItem
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataSource = (New StudentSearchFacade).getAllStudentTerm(ddlType.SelectedValue, campusId)
                .DataBind()
            End With
        End If
        If ddlItem.Items.Count = 0 Then
            CType(item.FindControl("lnkLastName"), LinkButton).Enabled = False
        Else
            CType(item.FindControl("lnkLastName"), LinkButton).Enabled = True
        End If
    End Sub


    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub


    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub

    Private Sub BindDataListMRU()
        Dim resourceId As Integer
        Dim fac As New ResourcesRelationsFacade
        Dim fac2 As New MRUFacade
        Dim mruTypeId As Integer
        Dim mruType As String = "Students"
        Dim modName As String
        Dim procMod As Boolean
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        Dim dsMRU As New DataSet
        If Not resourceId = 264 Then
            modName = HttpContext.Current.Request.Params("mod")
            Select Case modName.ToUpper
                Case "AD"
                    mruType = "Leads"
                    procMod = True
                Case "AR", "SA", "FA", "FC"
                    mruType = "Students"
                    procMod = True
                Case "PL"
                    mruTypeId = fac2.GetResourceMRUTypeId(resourceId)
                    Select Case mruTypeId
                        Case 1
                            mruType = "Students"
                            procMod = True
                        Case 2
                            mruType = "Employers"
                            procMod = True
                    End Select
                Case "HR"
                    mruType = "Employees"
                    procMod = True
            End Select
            'If procMod = True Then
            '    Dim objMRUFac As New MRUFacade
            '    dsMRU = objMRUFac.LoadMRU(mruType, Session("UserId").ToString(), HttpContext.Current.Request.Params("cmpid"))
            'End If
        Else 'Home Page. For this we will use the module to determine what MRU should be displayed
            modName = HttpContext.Current.Request.Params("mod")
            Select Case modName.ToUpper
                Case "AD"
                    mruType = "Leads"
                    procMod = True
                Case "AR", "SA", "FA", "PL", "FC"
                    mruType = "Students"
                    procMod = True
                Case "HR"
                    mruType = "Employees"
                    procMod = True
            End Select
        End If
        If procMod = True Then
          ' US3054 4/17/2012 Janet Robinson switch MRU return to match left MRU bar for Students
            If mruType = "Students" Then
                Dim objMRUFac As New MRUFacade
                dsMRU = objMRUFac.LoadMRUForStudentSearch(userId, HttpContext.Current.Request.Params("cmpid"))
                If Not IsNothing(dsMRU) Then
                    '  campusDesc = dsMRU.Tables(0).Rows(0).Item("CampusDescrip")
                    Dim dr As DataRow()
                    dr = dsMRU.Tables(0).Select("CampusID='" + HttpContext.Current.Request.Params("cmpid") + "'")
                    If dr.Length > 0 Then
                        For Each row In dr
                            campusDesc = row("CampusDescrip")
                            Exit For
                        Next

                    End If
                End If
            End If
        End If
        If dsMRU.Tables.Count > 0 Then
            If dsMRU.Tables("MRUList").Rows.Count > 0 Then
                If (resourceId = 85 Or resourceId = 94) And (userName <> "sa") And (ViewState("isSystemAdministrator") = False) Then
                    dgrdTransactionMRU.DataSource = New DataView((New StudentSearchFacade).GetStudentSearchDSWithStudentNumberforCurrentEnrollmentMRU(dsMRU, campusId).Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
                    dgrdTransactionMRU.DataBind()
                Else
                    dgrdTransactionMRU.DataSource = New DataView((New StudentSearchFacade).GetStudentSearchDSWithStudentNumberMRU(dsMRU, campusId).Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
                    dgrdTransactionMRU.DataBind()
                End If
            End If
        End If
    End Sub

    Protected Sub dgrdTransactionMRU_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTransactionMRU.ItemCommand
        Dim parametersArray(6) As String '= {e.CommandArgument, (New StudentSearchFacade).GetStudentNameByID(e.CommandArgument)}

        '   set StudentName
        parametersArray(0) = CType(e.Item.FindControl("lnkLastName"), LinkButton).Text + ", " + CType(e.Item.FindControl("lblFirstName1"), Label).Text

        '   set StuEnrollment
        Dim ddlEnrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)
        parametersArray(1) = ddlEnrollments.SelectedItem.Value
        parametersArray(2) = ddlEnrollments.SelectedItem.Text

        '   set Terms
        Dim ddlTerms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)
        parametersArray(3) = ddlTerms.SelectedItem.Value
        parametersArray(4) = ddlTerms.SelectedItem.Text

        '   set AcademicYears
        Dim ddlAcademicYears As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)
        parametersArray(5) = ddlAcademicYears.SelectedItem.Value
        parametersArray(6) = ddlAcademicYears.SelectedItem.Text

        Session("StuEnrollment") = ddlEnrollments.SelectedItem.Text
        Session("AcademicYear") = ddlAcademicYears.SelectedItem.Text
        Session("Term") = ddlTerms.SelectedItem.Text

        '   send parameters back to the parent page
        'CommonWebUtilities.PassDataBackToParentPageAndFireClickEvent(Page, parametersArray)
        PassDataBackToParentPageAndFireClickEvent(Page, parametersArray)
        'CommonWebUtilities.PassDataBackToParentPage(Page, parametersArray)
    End Sub

    Protected Sub dgrdTransactionMRU_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdTransactionMRU.ItemDataBound

        '   process only rows in the items section
        Select Case e.Item.ItemType

            Case ListItemType.Item, ListItemType.AlternatingItem


                '   Register Max field length for Last Name
                RegisterMaxFieldLength(0, e.Item.DataItem("LastName"))

                '   Register Max field length for First Name
                RegisterMaxFieldLength(1, e.Item.DataItem("FirstName"))

                '   Register Max field length for SSN
                RegisterMaxFieldLength(2, e.Item.DataItem("SSN"))




                '   get Enrollments dropdownlist
                Dim enrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)

                '   populate  Enrollments ddl
                Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim drows() As DataRow = dr.Row.GetChildRows("StudentsStudentEnrollments")
                For i As Integer = 0 To drows.Length - 1
                    enrollments.Items.Add(New ListItem(drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment"), CType(drows(i)("StuEnrollId"), Guid).ToString))

                    '   Register Max field length for Enrollments. Add some slack because it is a dropdownlist
                    RegisterMaxFieldLength(3, drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment") + "XXXX")
                Next

                ''   first Enrollment is selected
                'enrollments.SelectedIndex = 0                

                'select latest enrollment
                If Not drows.Length = 0 Then
                    'sort terms by date
                    Dim idx(drows.Length - 1, 1) As Double
                    For i As Integer = 0 To drows.Length - 1
                        idx(i, 0) = i
                        If Not drows(i)("StartDate") Is System.DBNull.Value Then
                            idx(i, 1) = CType(drows(i)("StartDate"), Date).Ticks
                        Else
                            idx(i, 1) = 0
                        End If
                    Next

                    'sort the terms by Date
                    If drows.Length > 1 Then
                        For j As Integer = 1 To drows.Length - 1
                            For k As Integer = j To drows.Length - 1
                                If idx(j, 1) > idx(j - 1, 1) Then
                                    Dim t0 As Double = idx(j - 1, 0)
                                    Dim t1 As Double = idx(j - 1, 1)
                                    idx(j - 1, 0) = idx(j, 0)
                                    idx(j - 1, 1) = idx(j, 1)
                                    idx(j, 0) = t0
                                    idx(j, 1) = t1
                                End If
                            Next
                        Next
                    End If

                    '   latest Enrollment is selected
                    enrollments.SelectedIndex = idx(0, 0)
                Else
                    enrollments.SelectedIndex = 0
                    CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                End If




                ''   get Terms dropdownlist
                'Dim terms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)

                ''   populate Terms ddl
                'For i As Integer = 0 To ddlTermId.Items.Count - 1
                '    terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                '    '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                '    RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                'Next

                '   first Term is selected
                'ddlTermId.SelectedIndex = 0

                '   get Terms dropdownlist
                Dim terms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)

                'get all student terms
                '   Dim row As DataRow
                Dim trows() As DataRow = dr.Row.GetChildRows("StudentStudentTerms")


                'if the student has no Terms ... populate all terms
                If Not trows.Length = 0 Then
                    'sort terms by date
                    Dim idx(trows.Length - 1, 1) As Double
                    For i As Integer = 0 To trows.Length - 1
                        idx(i, 0) = i
                        idx(i, 1) = CType(trows(i)("StartDate"), Date).Ticks
                    Next

                    'sort the terms by Date
                    If trows.Length > 1 Then
                        For j As Integer = 1 To trows.Length - 1
                            For k As Integer = j To trows.Length - 1
                                If idx(j, 1) > idx(j - 1, 1) Then
                                    Dim t0 As Double = idx(j - 1, 0)
                                    Dim t1 As Double = idx(j - 1, 1)
                                    idx(j - 1, 0) = idx(j, 0)
                                    idx(j - 1, 1) = idx(j, 1)
                                    idx(j, 0) = t0
                                    idx(j, 1) = t1
                                End If
                            Next
                        Next
                    End If
                    Dim cnt As Integer = 0
                    For i As Integer = 0 To trows.Length - 1
                        'populate only student terms
                        If trows(idx(i, 0))("StuEnrollId").ToString = enrollments.SelectedValue Then
                            terms.Items.Add(New ListItem(trows(idx(i, 0)).GetParentRow("TermsStudentTerms")("TermDescrip"), CType(trows(idx(i, 0))("TermId"), Guid).ToString))

                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                            RegisterMaxFieldLength(4, terms.Items(cnt).Text + "XXXX")
                            cnt = cnt + 1
                        End If
                    Next

                    '   select latest term for the student
                    If Not dr.Row("CurrentTerm") Is System.DBNull.Value Then
                        terms.SelectedValue = CType(dr.Row("CurrentTerm"), Guid).ToString
                    Else
                        terms.SelectedIndex = 0
                    End If
                    If terms.Items.Count = 0 Then
                        BuildTermsDDL(enrollments.SelectedValue.ToString(), campusId)
                        Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                        For i As Integer = 0 To ddlTermId.Items.Count - 1
                            terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                            RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                        Next
                        If ddlTermId.Items.Count = 0 Then
                            CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                        End If
                        '   first Enrollment is selected
                        terms.SelectedIndex = 0
                    End If
                Else
                    '   populate Terms ddl
                    BuildTermsDDL(enrollments.SelectedValue.ToString(), campusId)
                    Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                    For i As Integer = 0 To ddlTermId.Items.Count - 1
                        terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                        '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                        RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                    Next
                    If ddlTermId.Items.Count = 0 Then
                        CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                    End If
                    '   first Enrollment is selected
                    terms.SelectedIndex = 0
                End If

                '   get AcademicYear dropdownlist
                Dim academicYear As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)
                Dim ddlAcademicYearId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlAcademicYearId"), DropDownList)
                '   populate AcademicYear ddl
                For Each item As ListItem In ddlAcademicYearId.Items
                    Dim newItem As New ListItem(item.Text, item.Value)
                    newItem.Selected = item.Selected
                    academicYear.Items.Add(newItem)

                    '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                    RegisterMaxFieldLength(5, item.Text + "XXXX")
                Next

            Case ListItemType.Footer

                If SumOfMaxFieldLengths() > 0 Then
                    '   calculate the width percentage of each cell in the footer
                    Dim cells As ControlCollection = e.Item.Controls
                    For i As Integer = 0 To cells.Count - 1
                        Dim cell As TableCell = CType(cells(i), TableCell)
                        '   add the width attribute to each cell only if there are records
                        cell.Attributes.Add("width", CType(MaxFieldLengths(i) * 100.0 / SumOfMaxFieldLengths(), Integer).ToString + "%")
                    Next
                End If
        End Select
    End Sub
    Public Sub ddqtyMRU_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddllist As DropDownList = CType(sender, DropDownList)
        Dim cell As TableCell = CType(ddllist.Parent, TableCell)
        Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
        Dim content As String = item.Cells(0).Text
        Dim ddlType As DropDownList = CType(item.Cells(0).FindControl("ddlEnrollmentsId"), DropDownList)
        Dim ddlItem As DropDownList = CType(item.Cells(1).FindControl("ddlTermsId"), DropDownList)

        ddlItem.Items.Clear()
        With ddlItem
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = (New StudentSearchFacade).getStudentTerm(ddlType.SelectedValue, campusId)
            .DataBind()
        End With
        If ddlItem.Items.Count = 0 Then
            With ddlItem
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataSource = (New StudentSearchFacade).getAllStudentTerm(ddlType.SelectedValue, campusId)
                .DataBind()
            End With
        End If
        If ddlItem.Items.Count = 0 Then
            CType(item.FindControl("lnkLastName"), LinkButton).Enabled = False
        Else
            CType(item.FindControl("lnkLastName"), LinkButton).Enabled = True
        End If
    End Sub
    Private Sub PassDataBackToParentPageAndFireClickEvent(ByVal page As Page, ByVal parametersArray() As String)
        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            '   this is the beginning of the javascript code  
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=PassData();function PassData(){"

            '   this is the middle of the javascript
            '   build the parameter list to be returned to the parent page
            Dim scriptMiddle As String = ""
            Dim i As Integer
            For i = 0 To parametersArray.Length - 1
                scriptMiddle += "window.opener.document.getElementById('" + page.Request.QueryString.Keys(i + 3).ToString + "').value='" + ReplaceSpecialCharactersInJavascriptMessage(parametersArray(i).Trim) + "';"
            Next

            '   fire click event on parent window
            'scriptMiddle += "window.opener.document.getElementById('" + page.Request.QueryString.Keys(i + 1).ToString + "').fireEvent('onClick');"

            '   this is the end of the javascript code
            Dim scriptEnd As String = "window.close();}</script>"

            '   Register the javascript code

            'page.RegisterStartupScript("PassDataBackToParentPage", scriptBegin + scriptMiddle + scriptEnd)
            ScriptManager.RegisterStartupScript(page, page.GetType(), "PassDataBackToParentPage", scriptBegin + scriptMiddle + scriptEnd, False)
        End If
    End Sub
    Private Function ReplaceSpecialCharactersInJavascriptMessage(ByVal s As String) As String
        '   replace \ by \\, ' by \', " by "", CR by \r, LF by \n
        Return s.Trim().Replace("\", "\\").Replace("'", "\'").Replace("""", "\""").Replace(vbCr, "\r").Replace(vbLf, "\n")
    End Function

    Protected Sub rpbAdvanceOption_ItemClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadPanelBarEventArgs) Handles rpbAdvanceOption.ItemClick
        Dim txtLast As TextBox
        txtLast = CType(rpbAdvanceOption.Items(0).Controls(0).FindControl("txtLastName"), TextBox)
        txtLast.Focus()
    End Sub
End Class
