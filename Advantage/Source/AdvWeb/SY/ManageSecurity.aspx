﻿<%@ Page Title="Manage Security" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ManageSecurity.aspx.vb" Inherits="SY_ManageSecurity" ClientIDMode="AutoID" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }
    </script>
    <script type="text/javascript">
        function SetCheckboxes() {
            var xmlDoc = new ActiveXObject("Msxml2.DOMDocument.3.0")
            xmlDoc.async = "false";
            var chkbx = window.document.getElementById(window.event.srcElement.id);
            var idx = chkbx.id.substring(chkbx.id.length - 1);
            var status = chkbx.checked;
            var txt = window.document.getElementById("clientXml");
            xmlDoc.loadXML(txt.innerHTML);
            xmlDoc.setProperty("SelectionLanguage", "XPath");
            currNode = xmlDoc.selectSingleNode("//" + chkbx.id.substring(0, chkbx.id.length - 1) + '1');
            if (idx == 1) {
                NavigateXmlDoc(currNode, status, 1);
                NavigateXmlDoc(currNode, status, 2);
                NavigateXmlDoc(currNode, status, 3);
                NavigateXmlDoc(currNode, status, 4);
                NavigateXmlDoc(currNode, status, 5);
            }
            else {
                NavigateXmlDoc(currNode, status, idx);
                CheckFull(chkbx);
            }
            return true;
        }
        function NavigateXmlDoc(node, status, idx) {
            var chkName = node.nodeName.substring(0, node.nodeName.length - 1) + idx;
            var chkbx = window.document.getElementById(chkName);
            chkbx.checked = status;

            if (node.hasChildNodes()) {
                var children = node.childNodes
                for (var i = 0; i < children.length; i++) {
                    NavigateXmlDoc(children.item(i), status, idx)
                }
            }
        }
        function CheckFull(chkbx) {
            var idx = chkbx.id.substring(chkbx.id.length - 1);
            if (idx != 1) {
                var chkName = chkbx.id.substring(0, chkbx.id.length - 1);
                var allChk = true;
                for (var j = 2; j < 6; j++) {
                    var chkbx = window.document.getElementById(chkName + j.toString());
                    allChk = allChk && chkbx.checked;
                }
                var chkFull = window.document.getElementById(chkName + '1');
                chkFull.checked = allChk;
            }
        }
    </script>
    <style type="text/css">
        .rgRow:hover, .RadGrid_Material .rgAltRow:hover, .RadGrid_Material .rgEditRow:hover {
            color: black !important;
        }

        .loading {
            background-color: #fff;
            height: 100%;
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelContent" runat="server" Transparency="30">
        <div class="loading">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/loading3.gif" AlternateText="loading" />
        </div>
    </telerik:RadAjaxLoadingPanel>

    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnExportToPdf">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radGrid1" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnExportToExcel">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radGrid1" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnBuildList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlGroups">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddlModules" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlModules">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddlTypes" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3>
                                <asp:Label ID="headerTitle" runat="server"></asp:Label>
                            </h3>
                            <!-- begin content table-->
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                <tr>
                                    <td class="contentcell">

                                        <table class="contenttable" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="contentcellheader">
                                                    <asp:Label ID="lblgeneral" runat="server" Font-Bold="true" CssClass="label">Select Role, Module, and Menu Type to build list</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table id="AutoNumber6" border="0">
                                            <tr>
                                                <td width="70px" align="right">
                                                    <asp:Label ID="lblGroups" runat="server" CssClass="labelonerow">Role:</asp:Label>
                                                </td>
                                                <td width="150px">
                                                    <asp:DropDownList ID="ddlGroups" runat="server" CssClass="dropdownlist" OnSelectedIndexChanged="ddlGroups_SelectedIndexChanged"
                                                        AutoPostBack="True" Width="160px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="70px" align="right">
                                                    <asp:Label ID="lblModules" runat="server" CssClass="labelonerow">Module:</asp:Label>
                                                </td>
                                                <td width="150px">
                                                    <asp:DropDownList ID="ddlModules" runat="server" CssClass="dropdownlist" Width="160px" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="70px" align="right">
                                                    <asp:Label ID="lblTypes" runat="server" CssClass="labelonerow">Menu Type:</asp:Label>
                                                </td>
                                                <td width="150px">
                                                    <asp:DropDownList ID="ddlTypes" runat="server" CssClass="dropdownlist" Width="160px" Enabled="true">
                                                        <asp:ListItem Value="3">Common Tasks</asp:ListItem>
                                                        <asp:ListItem Value="4">Maintenance</asp:ListItem>
                                                        <asp:ListItem Value="5">Reports</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td width="150px" style="text-align: left; background-color: transparent; vertical-align: top;">
                                                    <div style="margin-top: 5px; margin-bottom: 5px;margin-left:10px;">
                                                        <asp:Button ID="btnBuildList" runat="server" Text="Build List"
                                                            OnClick="btnBuildList_Click" OnCommand="btnBuildList_Command"></asp:Button>
                                                    </div>
                                                </td>
                                            </tr>

                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td class="contentcellheadernoborderleft" nowrap width="420px">
                                                    <asp:Label ID="Label1" runat="server" Font-Bold="true" CssClass="label">Apply permissions for the pages</asp:Label></td>
                                                <td class="contentcellheadernobordercenter" nowrap width="150px">
                                                    <div style="margin: 5px;">
                                                        <asp:Button ID="btnExportToPdf" runat="server" Text="Export to PDF" OnClick="btnExportToPdf_Click" />
                                                    </div>
                                                    <td class="contentcellheadernoborderright" nowrap width="150px">
                                                        <div style="margin: 5px;">

                                                            <asp:Button ID="btnExportToExcel" runat="server" Text="Export to Excel" OnClick="btnExportToExcel_Click" />
                                                        </div>
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <telerik:RadGrid ID="RadGrid1" runat="server"
                                            AutoGenerateColumns="False" AllowSorting="false" Height="500px"
                                            AllowMultiRowSelection="True" GridLines="None" Visible="false" HeaderStyle-BackColor="#1565c0">
                                            <ClientSettings>
                                                <Scrolling AllowScroll="false" />
                                            </ClientSettings>
                                            <ItemStyle HorizontalAlign="Left" />
                                            <AlternatingItemStyle HorizontalAlign="Left" />
                                            <MasterTableView DataKeyNames="ChildResourceId" TableLayout="fixed">
                                                <Columns>
                                                    <telerik:GridTemplateColumn Visible="false">
                                                        <ItemTemplate>
                                                            <asp:TextBox runat="server" ID="txtParentResourceId" Text='<%# Bind("ParentResourceId") %>'></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="txtResourceTypeId" Text='<%# Bind("ResourceTypeId") %>'></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="txtChildResourceId" Text='<%# Bind("ChildResourceId") %>'></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="txtChildResource" Text='<%# Bind("ChildResource") %>'></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="txtAccessLevel" Text='<%# Bind("AccessLevel") %>'></asp:TextBox>
                                                            <asp:TextBox runat="server" ID="txtTabId" Text='<%# Bind("TabId") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderStyle-Width="520px" ItemStyle-Width="520px">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblResource" runat="server" Text="Page Name" CssClass="labelbold"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="lblParentModule" Text='<%# Bind("ParentResource") %>' CssClass="label" Style="font-weight: bold; padding-left: 10px;"></asp:Label>
                                                            <asp:Label runat="server" ID="txtParentResource" Text='<%# Bind("ParentResource") %>' CssClass="label" Style="font-weight: bold; padding-left: 20px; color: White;"></asp:Label>
                                                            <asp:Label runat="server" ID="lblChildResourceDesc" Text='<%# Bind("ChildResource") %>' CssClass="label" Style="padding-left: 35px;"></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn HeaderStyle-Width="330px" ItemStyle-Width="330px">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblPermission" runat="server" Text="Set Permission" CssClass="labelbold"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlPermissionsParent" runat="server" Width="160px" AutoPostBack="true" CssClass="dropdownlist" OnSelectedIndexChanged="ddlPermissionsParent_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">Select Permissions</asp:ListItem>
                                                                <asp:ListItem Value="15">Full Access</asp:ListItem>
                                                                <asp:ListItem Value="12">Add and Edit Only</asp:ListItem>
                                                                <asp:ListItem Value="10">Edit and Delete Only</asp:ListItem>
                                                                <asp:ListItem Value="6">Add and Delete Only</asp:ListItem>
                                                                <asp:ListItem Value="4">Add Only</asp:ListItem>
                                                                <asp:ListItem Value="8">Edit Only</asp:ListItem>
                                                                <asp:ListItem Value="1">Display Only</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:DropDownList ID="ddlPermissions" runat="server" AutoPostBack="true" Width="160px" CssClass="dropdownlist" OnSelectedIndexChanged="ddlPermissions_SelectedIndexChanged">
                                                                <asp:ListItem Value="0">Select Permissions</asp:ListItem>
                                                                <asp:ListItem Value="15">Full Access</asp:ListItem>
                                                                <asp:ListItem Value="12">Add and Edit Only</asp:ListItem>
                                                                <asp:ListItem Value="10">Edit and Delete Only</asp:ListItem>
                                                                <asp:ListItem Value="6">Add and Delete Only</asp:ListItem>
                                                                <asp:ListItem Value="4">Add Only</asp:ListItem>
                                                                <asp:ListItem Value="8">Edit Only</asp:ListItem>
                                                                <asp:ListItem Value="1">Display Only</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:DropDownList ID="ddlChild" runat="server" AutoPostBack="false" Width="160px" CssClass="dropdownlist">
                                                                <asp:ListItem Value="0">Select Permissions</asp:ListItem>
                                                                <asp:ListItem Value="15">Full Access</asp:ListItem>
                                                                <asp:ListItem Value="12">Add and Edit Only</asp:ListItem>
                                                                <asp:ListItem Value="10">Edit and Delete Only</asp:ListItem>
                                                                <asp:ListItem Value="6">Add and Delete Only</asp:ListItem>
                                                                <asp:ListItem Value="4">Add Only</asp:ListItem>
                                                                <asp:ListItem Value="8">Edit Only</asp:ListItem>
                                                                <asp:ListItem Value="1">Display Only</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                </Columns>
                                            </MasterTableView>
                                            <ClientSettings>
                                                <Scrolling AllowScroll="True" />
                                                <Selecting AllowRowSelect="True"></Selecting>
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                        <asp:Repeater ID="rptNavigationNodes" runat="server" OnItemDataBound="rptNavigationNodes_ItemDataBound" Visible="false">
                                            <HeaderTemplate>
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                    <tr>
                                                        <td width="50%" style="border: 0" class="datagridheaderstyle">&nbsp;
                                                        </td>
                                                        <td style="width: 10%; border: 0; text-align: center" class="datagridheaderstyle">Full
                                                        </td>
                                                        <td style="width: 10%; border: 0; text-align: center" class="datagridheaderstyle">Edit
                                                        </td>
                                                        <td style="width: 10%; border: 0; text-align: center" class="datagridheaderstyle">Add
                                                        </td>
                                                        <td style="width: 10%; border: 0; text-align: center" class="datagridheaderstyle">Delete
                                                        </td>
                                                        <td style="width: 10%; border: 0; text-align: center" class="datagridheaderstyle">Display
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                    <tr>
                                                        <td width="50%">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblIndentation" runat="server" Text="label"></asp:Label>
                                                                    </td>
                                                                    <td style="border: 0; padding: 0" class="datagriditemstyle" font-bold="True">
                                                                        <asp:Label ID="lblNavigationNode" runat="server"></asp:Label>
                                                                        <asp:Label ID="lblResourceId" runat="server" Visible="false"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 10%; border: 0; text-align: center; padding: 0" class="securitychkboxstyle">
                                                            <asp:CheckBox ID="CheckBox1" runat="server" CssClass="label" />
                                                        </td>
                                                        <td style="width: 10%; border: 0; text-align: center; padding: 0" class="securitychkboxstyle">
                                                            <asp:CheckBox ID="CheckBox2" runat="server" CssClass="label" />
                                                        </td>
                                                        <td style="width: 10%; border: 0; text-align: center; padding: 0" class="securitychkboxstyle">
                                                            <asp:CheckBox ID="CheckBox3" runat="server" CssClass="label" />
                                                        </td>
                                                        <td style="width: 10%; border: 0; text-align: center; padding: 0" class="securitychkboxstyle">
                                                            <asp:CheckBox ID="CheckBox4" runat="server" CssClass="label" />
                                                        </td>
                                                        <td style="width: 10%; border: 0; text-align: center; padding: 0" class="securitychkboxstyle">
                                                            <asp:CheckBox ID="CheckBox5" runat="server" CssClass="label" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

            <xml id="clientXml" runat="server"></xml>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
</asp:Content>


