<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SetupSDF.aspx.vb" Inherits="SetupSDF" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>Create User Defined Field</title>
    <script type="text/javascript">
        function OpenResponsibility() {
            window.open("../AD/Responsibility.aspx", "histWin", "width=500,height=150,resizable,scrollbars")
        }
        function scrollwindow() {
            window.document.Form1.scrollposition.value = window.document.getElementById("scrollright").scrollTop;
        }
        function scrollbody() {
            window.document.getElementById("scrollright").scrollTop = "<%=Session("ScrollValue")%>";
        }
    </script>
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="../AuditHist.js"></script>
    <script type="text/javascript"> 
        if (sessionStorage.captionRequirementdb !== undefined) {
            sessionStorage.removeItem("captionRequirementdb");
        }
        if (sessionStorage.getItem("PriorWorkSdfFields") !== undefined) {
            sessionStorage.removeItem("PriorWorkSdfFields");
        }
        if (sessionStorage.getItem("PriorWorkSdfFieldsStudent") !== undefined) {
            sessionStorage.removeItem("PriorWorkSdfFieldsStudent");
        }
        if (sessionStorage.getItem("PriorEduSdfFields") !== undefined) {
            sessionStorage.removeItem("PriorEduSdfFields");
        }
        if (sessionStorage.getItem("PriorEduSdfFieldsStudent") !== undefined) {
            sessionStorage.removeItem("PriorEduSdfFieldsStudent");
        }
    </script>
    <style>
        .rbl label
        {
            margin-right: 5px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstEmployerContact">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="pnlPagebutton" />
                    <telerik:AjaxUpdatedControl ControlID="pnlPagebutton" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                    <telerik:AjaxUpdatedControl ControlID="pnlPagebutton" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                    <telerik:AjaxUpdatedControl ControlID="pnlPagebutton" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                    <telerik:AjaxUpdatedControl ControlID="pnlPagebutton" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="radStatus">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="pnlPagebutton" />
                    <telerik:AjaxUpdatedControl ControlID="radStatus" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlDTypeId">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PnlFldProperty" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Radiobuttonlist1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="Radiobuttonlist1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlList" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlList" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <!-- begin leftcolumn -->
                        <td class="listframetop">
                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td class="listframetop">
                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr>
                                                <td nowrap align="left" width="15%">
                                                    <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                                <td nowrap width="85%">
                                                    <asp:RadioButtonList ID="radStatus" runat="Server" RepeatDirection="Horizontal" AutoPostBack="true" CssClass="label">
                                                        <asp:ListItem Text="Active" Selected="True" />
                                                        <asp:ListItem Text="Inactive" />
                                                        <asp:ListItem Text="All" />
                                                    </asp:RadioButtonList></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="listframebottom">
                                        <div class="scrollleftfilters">
                                            <asp:DataList ID="dlstEmployerContact" runat="server" DataKeyField="SDFId" Width="100%">
                                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server" CommandArgument='<%# Container.DataItem("SDFId")%>' Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>' CausesValidation="False"></asp:ImageButton>
                                                    <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" CommandArgument='<%# Container.DataItem("SDFId")%>' Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>' CausesValidation="False"></asp:ImageButton>

                                                    <asp:LinkButton Text='<%# Container.DataItem("SDFDescrip")%>' runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("SDFId")%>' ID="Linkbutton2" CausesValidation="False" />
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>

            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop">
                <asp:Panel ID="pnlPagebutton" runat="server">
                    <table cellspacing="0" cellpadding="0" width="98%" border="0">
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnSave" runat="server" Enabled="true" CssClass="save" Text="Save"></asp:Button>
                                <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button></td>
                        </tr>
                    </table>
                </asp:Panel>
                <!-- end top menu (save,new,reset,delete,history)-->
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <asp:Panel ID="pnlRHS" runat="server">
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <!--begin content table-->
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" nowrap colspan="6">
                                                <asp:Label ID="lblFieldProperties" runat="server" CssClass="Label" Font-Bold="true">Field Properties</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                    </table>

                                    <asp:Panel ID="PnlFldProperty" runat="server">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">

                                            <tr>
                                                <td class="contentcell" nowrap>
                                                    <asp:Label ID="lblSDFDescrip" runat="server" Text="Field Name" CssClass="Label"></asp:Label>
                                                </td>
                                                <td class="contentcell">
                                                    <asp:TextBox ID="txtSDFDescrip" TabIndex="1" runat="server" CssClass="textbox" Width="235px"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap>
                                                    <asp:Label ID="lblStatusId" runat="server" Text="Status" CssClass="Label"></asp:Label></td>
                                                <td class="contentcell">
                                                    <asp:DropDownList ID="ddlStatusId" TabIndex="2" runat="server" Width="230px"></asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap>
                                                    <asp:Label ID="lblDTypeId" runat="server" Text="Data Type" CssClass="Label"></asp:Label></td>
                                                <td class="contentcell">
                                                    <asp:DropDownList ID="ddlDTypeId" TabIndex="3" runat="server" Width="230px" AutoPostBack="true"></asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap>
                                                    <asp:Label ID="lblDecimals" runat="server" Text="Decimals" CssClass="Label"></asp:Label></td>
                                                <td class="contentcell">
                                                    <asp:TextBox ID="txtDecimals" runat="server" CssClass="textbox" Width="100px"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap>
                                                    <asp:Label ID="lblLen" runat="server" Text="Length" CssClass="Label"></asp:Label></td>
                                                <td class="contentcell">
                                                    <asp:TextBox ID="txtLen" runat="server" CssClass="textbox" Width="100px" TabIndex="4" MaxLength="5"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap>
                                                    <asp:Label ID="lblHelpText" runat="server" Text="Comments" CssClass="Label"></asp:Label></td>
                                                <td class="contentcell" colspan="5">
                                                    <asp:TextBox ID="txtHelpText" TabIndex="5" runat="server" MaxLength="300"
                                                        TextMode="MultiLine" Rows="3" Columns="80" CssClass="textbox" Width="300px">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" nowrap>
                                                    <asp:Label ID="lblIsRequired" runat="server" Text="Required?" CssClass="Label"></asp:Label></td>
                                                <td class="contentcell">
                                                    <asp:CheckBox ID="chkIsRequired" runat="server" CssClass="checkbox" TabIndex="6"></asp:CheckBox></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlRadioBtn" runat="server">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" colspan="6">
                                                    <asp:Label ID="Label1" runat="server" CssClass="Label" Font-Bold="true">Select Validation for the above mentioned field</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcelltwo" colspan="6">
                                                    <asp:RadioButtonList ID="Radiobuttonlist1" runat="server" AutoPostBack="True"
                                                        RepeatDirection="Horizontal" RepeatLayout="Flow" TextAlign="Right" CssClass="rbl">
                                                        <asp:ListItem Value="None" Selected="True" >None</asp:ListItem>
                                                        <asp:ListItem Value="List">List</asp:ListItem>
                                                        <asp:ListItem Value="Range">Range</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlListVal" runat="server" Visible="False">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap>
                                                    <asp:Label ID="lblList" runat="server" Font-Bold="true" CssClass="Label">Add items for the list</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcelltwo">
                                                    <asp:Panel ID="pnlList" runat="server" CssClass="PanelTheme" align="center" Width="99%" Visible="true">
                                                        <table class="contenttable" cellspacing="0" cellpadding="4" width="100%" align="center" border="0">
                                                            <tr>
                                                                <td width="15%">
                                                                    <asp:Label ID="lblItem" runat="server" CssClass="label" Width="75px">Item</asp:Label></td>
                                                                <td nowrap width="65%">
                                                                    <asp:TextBox ID="txtListItem" runat="server" Width="325px"></asp:TextBox></td>
                                                                <td nowrap width="15%">
                                                                    <asp:Button ID="btnAdd" runat="server" Text="Add" CausesValidation="False" Width="75px"></asp:Button></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" width="15%">
                                                                    <asp:Label ID="Label3" runat="server" CssClass="label" Width="75px">View Items</asp:Label></td>
                                                                <td width="65%">
                                                                    <asp:ListBox ID="lbxList" runat="server" Rows="7" Width="325px"></asp:ListBox></td>
                                                                <td valign="top" width="15%">
                                                                    <asp:Button ID="btnRemove" runat="server" Text="Remove" CausesValidation="False" Width="75px"></asp:Button></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlRange" runat="server" Visible="False" ScrollBars="None">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap>
                                                    <asp:Label ID="Label2" runat="server" Font-Bold="true" CssClass="Label">Specify the range for the field</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcelltwo">
                                                    <asp:Panel ID="pnlListRange" runat="server" CssClass="PanelTheme" Width="99%" Visible="true">
                                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tr>
                                                                <td class="spacertables"></td>
                                                            </tr>
                                                            <tr>
                                                                <td nowrap>
                                                                    <asp:Label ID="lblMinVal" runat="server" CssClass="Label" Width="80px">Between</asp:Label>
                                                                    <asp:TextBox ID="txtMinVal" runat="server" Width="150px"></asp:TextBox>
                                                                    <asp:Label ID="lblMaxVal" runat="server" CssClass="Label" Width="50px">&nbsp;&nbsp;&nbsp;and</asp:Label>
                                                                    <asp:TextBox ID="txtMaxVal" runat="server" Width="150px"></asp:TextBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="spacertables"></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="6">
                                                    <asp:Label ID="lblSDF" runat="server" Font-Bold="true" CssClass="Label">Select Validation for the above field</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcelltwo" colspan="6">
                                                    <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false"></asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:TextBox ID="txtStudentId" CssClass="Label" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtSDFId" CssClass="Label" runat="server" Visible="false"></asp:TextBox>
                                    <asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                                    <asp:TextBox ID="txtResourceID" CssClass="Label" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtRowIds" CssClass="Label" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtModDate" runat="server" Visible="False" />
                                    <input type="hidden" name="scrollposition" id="scrollposition" />
                                    <!--end content tables-->
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server" CssClass="ValidationSummary">
            <asp:CustomValidator ID="CustomValidator1" runat="server" CssClass="ValidationSummary" ErrorMessage="CustomValidator"
                Display="None"></asp:CustomValidator>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        </asp:Panel>
    </div>
</asp:Content>

