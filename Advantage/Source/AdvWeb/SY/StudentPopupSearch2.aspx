<%--<%@ Reference Page="~/SA/AcademicYears.aspx" %>--%>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="StudentPopupSearch2" CodeFile="StudentPopupSearch2.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Student Search</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
    <meta content="JavaScript" name="vs_defaultClientScript">
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
    <link href="../CSS/systememail.css" type="text/css" rel="stylesheet">
    <script language="javascript" type="text/javascript">
        function mngRequestStarted(ajaxManager, eventArgs) {
            if (eventArgs.EventTarget == "dgrdTransactionSearch") {
                eventArgs.EnableAjax = false;
            }
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server" defaultfocus="txtLastName">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <img src="../images/student_search.jpg">
            </td>
            <td class="topemail">
                <a class="close" onclick="top.close()" href="#">X Close</a>
            </td>
        </tr>
    </table>
    <telerik:RadScriptManager ID="rsmMain" runat="server" AsyncPostBackTimeout="360000">
    </telerik:RadScriptManager>
    <telerik:RadAjaxManager ID="ramSearch" runat="server" EnableAJAX="true">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rpbAdvanceOption">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rpbAdvanceOption" LoadingPanelID="ralpSearch" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rpbAdvanceOption" LoadingPanelID="ralpSearch" />
                    <telerik:AjaxUpdatedControl ControlID="dgrdTransactionSearch" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
        <ClientEvents OnRequestStart="mngRequestStarted" />
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="ralpSearch" runat="server" >
    </telerik:RadAjaxLoadingPanel>
    <div class="scrollleftright" style="width: 95%;HEIGHT: expression(document.body.clientHeight - 80 + 'px')">
        <table cellspacing="0" cellpadding="0" width="99%" border="0">
            <tr>
                <td class="detailsframe" style="width: 99%">
                    <table runat="server" id="tblMain" cellspacing="0" cellpadding="0" width="99%" border="0">
                        <tr id="trMRUH" runat="server">
                            <td colspan="6" class="labelbold" style="text-align: center; padding: 4px 6px 4px 6px">
                                   <asp:Label ID="lbHdr" runat="server" class="labelbold"></asp:Label>
                            </td>
                        </tr>
                        <tr id="trMRUGrid" runat="server">
                            <td colspan="6">
                                <div class="scrollleftright" style="height: 150px; width: 99%">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td align="center">
                                                <asp:DataGrid ID="dgrdTransactionMRU" Width="100%" runat="server" BorderStyle="Solid"
                                                    AutoGenerateColumns="False" BorderColor="#E0E0E0" BorderWidth="1px" ShowFooter="True">
                                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                                    <AlternatingItemStyle HorizontalAlign="Left" CssClass="datagridalternatingstyle">
                                                    </AlternatingItemStyle>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="datagriditemstyle"></ItemStyle>
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="datagridheaderstyle"></HeaderStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Last Name">
                                                            <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lnkLastName" runat="server" Text='<%# Container.DataItem("LastName") %>'
                                                                    CommandArgument='<%# Container.DataItem("StudentId") %>' CommandName="StudentSearch"
                                                                    CausesValidation="False">
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="First Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFirstName1" Text='<%# Container.DataItem("FirstName") %>' runat="server"  cssClass="label">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="SSN">
                                                            <ItemStyle Wrap="False"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="label1" Text='<%# Container.DataItem("SSN") %>' runat="server"  cssClass="label">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Enrollments">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlEnrollmentsId" runat="server" CssClass="dropdownlistff" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddqtyMRU_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Terms">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlTermsId" runat="server" CssClass="dropdownlistff">
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Academic Years">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddlAcademicYearsId" runat="server" CssClass="dropdownlistff">
                                                                </asp:DropDownList>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" height="10px">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="detailsframe">
                                <telerik:RadPanelBar runat="server" ID="rpbAdvanceOption" Width="100%" >
                                    <Items>
                                        <telerik:RadPanelItem Expanded="false" Text="Advanced Options" runat="server">
                                            <Items>
                                                <telerik:RadPanelItem Value="AdvanceOption" runat="server">
                                                    <ItemTemplate>
                                                        <table width="90%" align="center" border="0">
                                                            <tr>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="6" class="labelbold" style="text-align: center; padding: 16px 6px 16px 6px">
                                                                    Please enter your search criteria, and then press the 'Search' button.
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="stdpopcell" style="width: 10%">
                                                                    <asp:Label ID="lblLastName" runat="Server" CssClass="label">Last Name</asp:Label>
                                                                </td>
                                                                <td class="stdpopcell" style="width: 20%">
                                                                    <asp:TextBox ID="txtLastName" runat="Server" CssClass="textbox" TabIndex="1"></asp:TextBox>
                                                                </td>
                                                                <td class="stdpopcell" style="width: 10%">
                                                                    <asp:Label ID="lblProgram" runat="Server" CssClass="label">Program</asp:Label>
                                                                </td>
                                                                <td class="stdpopcell" style="width: 30%">
                                                                    <asp:DropDownList ID="ddlStudentPrgVerId" runat="Server" CssClass="dropdownlistff"
                                                                        TabIndex="3">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td class="stdpopcell" style="width: 10%">
                                                                    <asp:Label ID="lblSSN" runat="Server" CssClass="label">SSN</asp:Label>
                                                                </td>
                                                                <td class="stdpopcell" style="width: 20%">
                                                                    <asp:TextBox ID="txtSSN" runat="Server" CssClass="textbox" TabIndex="5"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="stdpopcell" style="width: 10%">
                                                                    <asp:Label ID="lblFirstName" runat="Server" CssClass="label">First Name</asp:Label>
                                                                </td>
                                                                <td class="stdpopcell" style="width: 20%">
                                                                    <asp:TextBox ID="txtFirstName" runat="Server" CssClass="textbox" TabIndex="2"></asp:TextBox>
                                                                </td>
                                                                <td class="stdpopcell" style="width: 10%">
                                                                    <asp:Label ID="lblStatus" runat="Server" CssClass="label">Status</asp:Label>
                                                                </td>
                                                                <td class="stdpopcell" style="width: 30%">
                                                                    <asp:DropDownList ID="ddlStatusId" runat="Server" CssClass="dropdownlistff" TabIndex="4">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td class="stdpopcell" style="width: 10%">
                                                                    <asp:Label ID="lblEnrollment" runat="Server" CssClass="label">Enrollment #</asp:Label>
                                                                </td>
                                                                <td class="stdpopcell" style="width: 20%">
                                                                    <asp:TextBox ID="txtEnrollment" runat="Server" CssClass="textbox" TabIndex="6"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="stdpopcell" style="width: 10%">
                                                                    <asp:Label ID="lblStudentNumber" runat="Server" CssClass="label">Student ID</asp:Label>
                                                                </td>
                                                                <td class="stdpopcell" style="width: 20%">
                                                                    <asp:TextBox ID="txtStudentNumber" runat="Server" CssClass="textbox" TabIndex="2"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlTermId" runat="server" Visible="False">
                                                                    </asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlAcademicYearId" runat="server" Visible="False">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center" colspan="8">
                                                                    <asp:Button ID="btnSearch" runat="Server" CssClass="button" Width="60px" Text="Search"
                                                                        OnClick="btnSearch_Click"></asp:Button>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </telerik:RadPanelItem>
                                            </Items>
                                        </telerik:RadPanelItem>
                                    </Items>
                                </telerik:RadPanelBar>
                            </td>
                        </tr>
                        <tr>
                            <td class="detailsframe">
                                <table width="99%">
                                    <tr valign="top">
                                        <td colspan="8" align="center">
                                            <asp:DataGrid ID="dgrdTransactionSearch" Width="99%" runat="server" BorderStyle="Solid"
                                                AutoGenerateColumns="False" BorderColor="#E0E0E0" BorderWidth="1px" ShowFooter="True">
                                                <EditItemStyle Wrap="False"></EditItemStyle>
                                                <AlternatingItemStyle HorizontalAlign="Left" CssClass="datagridalternatingstyle">
                                                </AlternatingItemStyle>
                                                <ItemStyle HorizontalAlign="Left" CssClass="datagriditemstyle"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Left" CssClass="datagridheaderstyle"></HeaderStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Last Name">
                                                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkLastName" runat="server" Text='<%# Container.DataItem("LastName") %>'
                                                                CommandArgument='<%# Container.DataItem("StudentId") %>' CommandName="StudentSearch"
                                                                CausesValidation="False">
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="First Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFirstName1" Text='<%# Container.DataItem("FirstName") %>' runat="server">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="SSN">
                                                        <ItemStyle Wrap="False"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="label1" Text='<%# Container.DataItem("SSN") %>' runat="server"  cssClass="label">
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Enrollments">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlEnrollmentsId" runat="server" CssClass="dropdownlistff" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddqty_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Terms">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlTermsId" runat="server" CssClass="dropdownlistff">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Academic Years">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlAcademicYearsId" runat="server" CssClass="dropdownlistff">
                                                            </asp:DropDownList>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <%--<div id="footer" style="width:100%;margin:auto;color:white;font-size:x-small; text-align:center">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>--%>
    </form>
</body>
</html>
