﻿<%@ Page Title="9010 Data Export" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="DataExport9010.aspx.vb" Inherits="DataExport9010" %>

<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <link href="../css/AR/font-awesome.css" rel="stylesheet" />
    <link href="../css/SY/DataExport9010.css" rel="stylesheet" />
    <script src="../Scripts/Fame.Advantage.API.Client.js"></script>

    <title>90/10 Data Export</title>

    <script>
        $(document).ready(function () {
            var module9010 = new Api.ViewModels.DataExport9010();
            window.module9010 = module9010;
            module9010.init();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain2" runat="Server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <Scripts>
            <asp:ScriptReference Path="../Scripts/Advantage.Client.SY.js"></asp:ScriptReference>
        </Scripts>
    </asp:ScriptManagerProxy>

    <div id="tabsForm">
        <h4>90/10 Data Export</h4>

        <div class="formRow">
            <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                Campus<span class="red-color">*</span>
            </div>
            <div class="inLineBlock left-margin">
                <telerik:RadListBox ID="RadListBoxCampuses" ShowCheckAll="true" runat="server" Width="360px" Height="80px"
                    AutoPostBackOnTransfer="True" SelectionMode="Multiple" CheckBoxes="True">
                </telerik:RadListBox>
                <asp:CustomValidator ID="CustomValidator2" runat="server" ErrorMessage="Please select campus(es)."
                    ControlToValidate="RadListBoxCampuses" ValidateEmptyText="true" EnableViewState="False"
                    ClientValidationFunction="module9010.validate_RadListBoxCampuses" Display="None">
                </asp:CustomValidator>
            </div>
        </div>

        <div class="formRow">
            <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                Fiscal Year Start <span class="red-color">*</span>
            </div>
            <div class="inLineBlock left-margin">
                <telerik:RadDatePicker ID="fiscalYearStart" TabIndex="9" runat="server"
                    Width="140px" MaxLength="12">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidator2" runat="server" Display="None" ErrorMessage="Fiscal Year Start is required."
                    ControlToValidate="fiscalYearStart">Fiscal Year Start is required.</asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="formRow">
            <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                Fiscal Year End <span class="red-color">*</span>
            </div>
            <div class="inLineBlock left-margin">
                <telerik:RadDatePicker ID="fiscalYearEnd" TabIndex="9" runat="server"
                    Width="140px" MaxLength="12">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator
                    ID="RequiredFieldValidator1" runat="server" Display="None" ErrorMessage="Fiscal Year End is required."
                    ControlToValidate="fiscalYearEnd">Fiscal Year End is required.</asp:RequiredFieldValidator>
            </div>
        </div>

        <div class="formRow">
            <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                Activities conducted for education and training:<span class="red-color">*</span>
            </div>
            <div class="inLineBlock left-margin">
                <asp:TextBox ID="activitiesConductedAmount" TabIndex="9" runat="server"
                    Width="140px" CssClass="textbox" MaxLength="12"></asp:TextBox>
                <span class="label">&nbsp;<%=MyAdvAppSettings.AppSettings("Currency")%></span>
                <asp:RequiredFieldValidator
                    ID="AmountReqFieldValidator" runat="server" Display="None" ErrorMessage="Activites conducted is required."
                    ControlToValidate="activitiesConductedAmount">Amount is Required</asp:RequiredFieldValidator>
            </div>
        </div>
        <br />
        <div class="formRow">
            <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                Program(s)<span class="red-color">*</span>
            </div>
            <div class="inLineBlock left-margin">
                <telerik:RadListBox ID="RadListBoxPrograms" ShowCheckAll="true" runat="server" Width="360px" Height="150px"
                    AutoPostBackOnTransfer="False" SelectionMode="Multiple" CheckBoxes="True">
                </telerik:RadListBox>
                <asp:CustomValidator ID="radListBoxProgramsValidator" runat="server" ErrorMessage="Please select a program(s)"
                    ControlToValidate="RadListBoxPrograms" ValidateEmptyText="true" EnableViewState="False"
                    ClientValidationFunction="module9010.validate_RadListBoxPrograms" Display="None">
                </asp:CustomValidator>
            </div>
        </div>

        <div class="formRow">
            <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                Group(s)<span class="red-color">*</span>
            </div>
            <div class="inLineBlock left-margin">
                <telerik:RadListBox ID="RadListBoxGroups" ShowCheckAll="true" runat="server" Width="360px" Height="150px"
                    AutoPostBackOnTransfer="False" SelectionMode="Multiple" CheckBoxes="True">
                </telerik:RadListBox>

                <asp:CustomValidator ID="radListBoxGroupsValidator" runat="server" ErrorMessage="Please select a group(s)"
                    ControlToValidate="RadListBoxGroups" ValidateEmptyText="true" EnableViewState="False"
                    ClientValidationFunction="module9010.validate_RadListBoxGroups" Display="None">
                </asp:CustomValidator>
            </div>
        </div>

        <div class="formRow">
            <FAME:StudentSearch ID="StudSearch1" runat="server" OnClientClick="return false;" Style="margin-left: 70px;" OnTransferToParent="TransferToParent" />
        </div>

        <div class="formRow">
            <div class="left leftLable" style="margin-right: 15px; width: 150px;">
                <br />
            </div>
            <div class="inLineBlock left-margin">
                <asp:Button ID="generateExportBtn" OnClientClick="return false;" runat="server" CssClass="rfdDecorated" Text="Export" Width="150px" />
            </div>
        </div>

        <%-- Hidden field to store student search enrollment id to send to api call --%>
        <asp:HiddenField ID="hiddenStuEnrollmentId" runat="server" Value="" ClientIDMode="Static" />

        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" ErrorMessage="CustomValidator"
            Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>

        <telerik:RadNotification runat="server" ID="RadNotification1"
            Text="This is a test" ShowCloseButton="true"
            Width="350px" Height="125px"
            TitleIcon=""
            Position="Center" Title="Message"
            EnableRoundedCorners="true"
            EnableShadow="true"
            Animation="Fade"
            AnimationDuration="1000"
            
            AutoCloseDelay="0"
            Style="padding-left: 120px; padding-top: 5px; word-spacing: 2pt;">
        </telerik:RadNotification>
    </div>

    <telerik:RadWindowManager runat="server" ID="RadWindowManager1"></telerik:RadWindowManager>
</asp:Content>
