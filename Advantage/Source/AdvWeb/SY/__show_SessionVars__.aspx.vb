﻿
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.IO

Partial Class SY__show_SessionVars__
    Inherits BasePage

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not IsPostBack Then

            If Request("resid") IsNot Nothing And Session("UserName") IsNot Nothing Then
                Dim rId As String = Request("resid").ToString()
                Dim user As String = Session("UserName").ToString()

                If rId <> "12345" Or user.ToLower() <> "support" Then
                    Throw New Exception("You are not authorized to view this page")
                Else
                    Dim sb As New StringBuilder
                    sb.Append("<table cellpadding=3 cellspacing=3 border=1>")
                    sb.Append("<tr><td><b>Session Key</b></td><td><b>Session Value</b></td><td><b>Size in Bytes</b></td></tr>")

                    Dim totalSessionBytes As Long = 0
                    Dim sessionCount As Long = 0
                    Dim b As New BinaryFormatter()
                    Dim m As MemoryStream
                    For Each obj As Object In Session
                        m = New MemoryStream()
                        b.Serialize(m, obj)
                        totalSessionBytes += m.Length
                        sb.Append("<tr>")
                        sb.Append("<td>" & obj.ToString() & "</td>")
                        If Session(obj.ToString()) IsNot Nothing Then
                            sb.Append("<td>" & Session(obj.ToString()).ToString() & "</td>")
                        Else
                            sb.Append("<td>Value not found</td>")
                        End If

                        sb.Append("<td>" & m.Length.ToString() & "</td>")
                        sb.Append("</tr>")
                        sessionCount = sessionCount + 1
                    Next

                    sb.Append("</table><br>")

                    lblSessions.Text = sb.ToString() & "<br>" & "<b>Total size:</b> " & totalSessionBytes.ToString() & " bytes <br>"
                    lblSessions.Text += "<b>Number of Items:</b> " & sessionCount.ToString()

                End If
            Else
                Throw New Exception("You are not authorized to view this page")
            End If
        End If

    End Sub

End Class
