﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EditStatusHistory.aspx.vb" Inherits="AdvWeb.Sy.EditStatusHistory" MasterPageFile="~/NewSite.master" %>

<%@ Register TagPrefix="FAME" TagName="StudentSearch" Src="../UserControls/SearchControls/StudentSearchControl.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <title>Edit Status History</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <div class="page-loading"></div>
    <script id="errorTemplateEditStatus" type="text/x-kendo-template">
        <div class="errorPopup">
            <h3>#= title #</h3>
            <img width="32px" height="32px" src=#= src #   />
            <p>#= message #</p>
            <div class="action">
                <input type="button" value="OK" class="k-button k-button-error" />
            </div>
        </div>
    </script>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">



            <table class="tablecontent" id="tableForm">
                <tr>
                    <td style="vertical-align: top; width: 100%">
                        <asp:Panel ID="pnlRHS" runat="server" Visible='<%# AdvantageSession.UserState.IsUserSupport%>'>
                            <!--begin right column-->
                            <table class="tablecontent" id="Table5" style="width: 98%;">
                                <tr>
                                    <td class="DetailsFrame" style="vertical-align: top; text-align: center;">
                                        <div class="boxContainer">
                                            <h3>
                                                <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                            </h3>
                                            <!-- begin table content-->
                                            <table class="contenttable" style="vertical-align: middle" width="100%">
                                                <tr>
                                                    <td class="contentcell4">

                                                        <div id="StudSearch" style="margin-bottom: -50px;">
                                                            <FAME:StudentSearch ID="StudSearch1" runat="server" ShowTerm="false" ShowAcaYr="false" OnTransferToParent="TransferToParent" />
                                                            <asp:Button runat="server" ID="btnUpdateSearchControl" CssClass="hidden" OnClick="btnUpdateSearchControl_OnClick" />
                                                        </div>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: middle; text-align: center; padding-top: 50px;">
                                                        <div>
                                                            <asp:Panel runat="server" ID="pnAddStatus" HorizontalAlign="Left" Visible="False" CssClass="left">
                                                                <div id="addStatus" style="line-height: 25px;">
                                                                    <span><b style="vertical-align: middle;">Status&nbsp;</b>
                                                                        <a href="javascript:void(0);" onclick="javascript:showAddStatusHistory();">
                                                                            <img src="../images/icon/Add.GIF" style="vertical-align: middle;" /></a>
                                                                    </span>
                                                                </div>
                                                            </asp:Panel>
                                                            <div class="print align_right right">
                                                                <input type="button" id="btnPrint" value="Print" class="k-button" />
                                                            </div>
                                                        </div>
                                                        <div class="clear"></div>
                                                        <div class="grid" style="margin-top: 5px;">
                                                            <div id="kendoStatusHistoryGrid"></div>
                                                        </div>
                                                        <div id="editAddForm" style="display: none;">
                                                            <div class="loading-status"></div>
                                                            <div class="row">
                                                                <div class="column col-left">Effective Date <span class="red">*</span></div>
                                                                <div class="column col-right">
                                                                    <input type="text" id="txtEffectiveDate" style="width: 70%;" />
                                                                </div>
                                                            </div>
                                                            <div class="row hidden" id="previousStatusRow">
                                                                <div class="column col-left">Old Status <span class="red">*</span></div>
                                                                <div class="column col-right">
                                                                    <select id="ddlPreviousStatus" style="width: 70%;" disabled="disabled"></select>
                                                                </div>

                                                            </div>
                                                            <div class="row">
                                                                <div class="column col-left">New Status <span class="red">*</span></div>
                                                                <div class="column col-right">
                                                                    <select id="ddlStatus" style="width: 70%;"></select>
                                                                </div>

                                                            </div>
                                                            <div class="row">
                                                                <div class="column col-left">Requested By <span class="red">*</span></div>
                                                                <div class="column col-right">
                                                                    <select id="ddlRequestedBy" style="width: 70%;"></select>
                                                                </div>

                                                            </div>
                                                            <div class="row">
                                                                <div class="column col-left">Case # <span class="red">*</span></div>
                                                                <div class="column col-right">
                                                                    <input type="text" id="txtCaseNumber" class="k-textbox fieldlabel1" style="width: 70%;" />
                                                                </div>

                                                            </div>
                                                            <div class="row">
                                                                <div class="column col-left">Date of Change</div>
                                                                <div class="column col-right">
                                                                    <input type="text" id="txtChangeDate" style="width: 70%;" disabled="disabled" />
                                                                </div>
                                                            </div>
                                                            <div id="rowReason" class="row">
                                                                <div class="column col-left">Reason <span class="red">*</span></div>
                                                                <div class="column col-right">
                                                                    <select id="ddlReason" style="width: 70%;"></select>
                                                                    <input type="text" id="txtReason" style="width: 70%;" class="k-textbox fieldlabel1" maxlength="50" />
                                                                </div>

                                                            </div>
                                                            <div id="rowEndDate" class="row">
                                                                <div class="column col-left">End Date <span class="red">*</span></div>
                                                                <div class="column col-right">
                                                                    <input type="text" id="txtEndDate" style="width: 70%;" />
                                                                </div>
                                                            </div>
                                                            <div class="row"></div>

                                                            <div class="row">
                                                                <div class="column col-left"></div>
                                                                <div class="align_right">
                                                                    <input type="button" id="btnSaveStatusChange" value="Save" class="k-button" />
                                                                    <input type="button" id="btnCloseForm" value="Cancel" class="k-button" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="deleteForm" style="display: none">
                                                            <div class="loading-status"></div>
                                                            <p>Selecting Delete will remove the status from the Status Change History. This step cannot be undone.</p>
                                                            <p class="red">
                                                                Removing an LOA, Probation, Suspension or Drop from the status history will also remove the recorded instances for those pages.  Client must confirm this is their request, and confirmation should be documented in Case # before you delete the change.
                                                            </p>
                                                            <div>
                                                                <div class="row">
                                                                    <div class="column col-left">Requested By <span class="red">*</span></div>
                                                                    <div class="column col-right">
                                                                        <select id="ddlRequestedByDelete" style="width: 70%;"></select>
                                                                    </div>

                                                                </div>
                                                                <div class="row">
                                                                    <div class="column col-left">Case # <span class="red">*</span></div>
                                                                    <div class="column col-right">
                                                                        <input type="text" id="txtCaseNumberDelete" class="k-textbox fieldlabel1" style="width: 70%;" />
                                                                    </div>

                                                                </div>
                                                                <div class="row">
                                                                    <div class="column col-left">Date of Change</div>
                                                                    <div class="column col-right">
                                                                        <input type="text" id="txtChangeDateDelete" style="width: 70%;" disabled="disabled" />
                                                                    </div>
                                                                </div>
                                                                <div id="rowReasonDelete" class="row">
                                                                    <div class="column col-left">Reason <span class="red">*</span></div>
                                                                    <div class="column col-right">
                                                                        <select id="ddlReasonDelete" style="width: 70%;"></select>
                                                                    </div>

                                                                </div>
                                                                <div class="row"></div>

                                                                <div class="row">
                                                                    <div class="column col-left"></div>
                                                                    <div class="align_right">
                                                                        <input type="button" id="btnDeleteStatusChange" value="Delete" class="k-button" />
                                                                        <input type="button" id="btnCloseDeleteForm" value="Cancel" class="k-button" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="confirmAttendanceActionForm" style="display: none;">
                                                            <h4>There are grades and attendance posted for the selected enrolment.</h4>
                                                            <p class="red">Changing to this status will permanently remove all the attendance and grades associated with this enrollment.</p>
                                                            <p>Before proceeding, confirm you have the following:</p>
                                                            <div>
                                                                <input id="chkDatabaseBackup" type="checkbox" /><label>A current backup of the Advantage database.</label>
                                                                <br />
                                                                <br />
                                                                <input id="chkClientConfirmation" type="checkbox" /><label>Confirmation from the client that this enrollment should not have any grades or attendance posted.</label>
                                                            </div>
                                                            <br />
                                                            <br />
                                                            <div class="align_right">
                                                                <input type="button" id="btnContinueDeletingAttendance" value="Continue" class="k-button" />
                                                                <input type="button" id="btnCancelDeletingAttendance" value="Cancel" class="k-button" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--end table content-->
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td></td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <div id="kNotification"></div>
            <div id="confirmAction" style="display: none">
                <div>
                    <p class="msg"></p>
                </div>
                <div class="center">
                    <button id="btnConfirmNo" class="k-button" type="button">Modify</button>
                    <button id="btnConfirmYes" class="k-button" type="button">Save</button>
                </div>
            </div>
            <asp:HiddenField ID="hfEnrollmentId" runat="server" />
            <asp:HiddenField ID="hfCampusId" runat="server" />
            <asp:HiddenField ID="hfUserId" runat="server" />
            <asp:HiddenField ID="hfAddWindowIsOpen" runat="server" />
            <div id="footer" runat="server">
                <asp:Label ID="lblFooterText" runat="server"></asp:Label>
            </div>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <script src="../Scripts/viewModels/SY/statusChange.js?r=<%= CInt(Int((9 * Rnd()) + 1))%>" type="text/javascript"></script>
    <script type="text/javascript">
        var statusChangeHistory = undefined;

        $(document).ready(function () {
            $('#btnPrint').hide();
        });

        function showAddStatusHistory() {
            if (statusChangeHistory != undefined) {
                statusChangeHistory.ShowAddEditStatusHistory('insert');
            }
        }

        function deleteRecord(e) {
            if (statusChangeHistory != undefined) {
                statusChangeHistory.DeleteStatusHistory(e);
            }
        }

        function bindKendoControls() {
            var studentEnrollmentId = $('#<%= hfEnrollmentId.ClientID%>').val();
            var campusId = $('#<%= hfCampusId.ClientID%>').val();
            var userId = $('#<%= hfUserId.ClientID%>').val();

            statusChangeHistory = new StatusChangeHistory(studentEnrollmentId, campusId, userId);
            statusChangeHistory.BindGrid();
            statusChangeHistory.btnUpdateSearchControlId = '#<%= btnUpdateSearchControl.ClientID%>';
            $('#btnPrint').click(function () {
                statusChangeHistory.Print();
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

