Imports System.Diagnostics
Imports FAME.Common
Imports System.Xml
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports Advantage.Business.Objects
Imports System.Collections

Partial Class StatusCodes
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents chkStatus As CheckBox
    Protected WithEvents btnhistory As Button
    'Protected WithEvents cblProbationTypeId As System.Web.UI.WebControls.RadioButtonList


    Private Sub Page_Init(sender As Object, e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private CurrentlyAttendingDefaultMessage As String = "Default Currently Attending Status (Applies to new enrollments)"
    Private CurrentlyAttendingOverrideMessage As String = "Override Current Currently Attending Default Status?"
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim objCommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        'Dim sw As New StringWriter
        'Dim ds2 As New DataSet
        Dim facade As New StatusCodeFacade
        Dim mContext As HttpContext

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim advantageUserState As User = AdvantageSession.UserState
        ResourceId = HttpContext.Current.Request.Params("resid")
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        mContext = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        'Get Default StatusCodes
        ViewState("DefaultStatusCodes") = (New StatusCodeFacade).GetDefaultLeadStatusCode().ToString

        Try
            If Not Page.IsPostBack Then
                '   disable history button first time
                'Header1.EnableHistoryButton(False)

                ds = facade.InitialLoadPage()
                BindDropDownLists(ds)
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                'Set the text box to a new guid
                txtStatusCodeId.Text = Guid.NewGuid.ToString

                'By Default Status Level has been set to Lead in the "Filter" section
                ViewState("StatusLevel") = 1
                PopulateDataList("Build")
                ' AddDefaultLabelToStatusCodes()
                EnableDisableDefaultCheckBox()
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        headerTitle.Text = Header.Title
    End Sub
    Private Sub BindDropDownLists(ds As DataSet)

        'Bind the 2 ActivityStatus dropdown lists
        BindStatusLevelDDLs(ds)
        'Bind the Activities dropdownlist
        BindStatusDDLs(ds)

        BindCampusGroupDDL(ds)

    End Sub
    Private Sub BindStatusLevelDDLs(ds As DataSet)
        With ddlStatusLevel
            .DataSource = ds
            .DataMember = "StatusLevelDT"
            .DataValueField = "StatusLevelId"
            .DataTextField = "StatusLevelDescrip"
            .DataBind()
        End With
        With ddlStatusLevelId
            .DataSource = ds
            .DataMember = "StatusLevelDT"
            .DataValueField = "StatusLevelId"
            .DataTextField = "StatusLevelDescrip"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BindStatusDDLs(ds As DataSet)
        With ddlStatus
            .DataSource = ds
            .DataMember = "StatusDT"
            .DataValueField = "StatusId"
            .DataTextField = "Status"
            .DataBind()
            .Items.Insert(0, New ListItem("All", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BindCampusGroupDDL(ds As DataSet)
        With ddlCampusGroup
            .DataSource = ds
            .DataMember = "CampGrpDT"
            .DataValueField = "CampGrpId"
            .DataTextField = "CampGrpDescrip"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub btnBuildList_Click(sender As Object, e As EventArgs) Handles btnBuildList.Click

        Dim objcommon As New CommonUtilities
        ViewState("StatusLevel") = ddlStatusLevel.SelectedItem.Value.ToString
        If Not ddlStatus.SelectedItem.Text = "Select" Then
            ViewState("Status") = ddlStatus.SelectedItem.Value.ToString
        Else
            ViewState("Status") = ""
        End If

        If Not ddlCampusGroup.SelectedItem.Text = "Select" Then
            ViewState("CampusGroup") = ddlCampusGroup.SelectedItem.Value.ToString
        Else
            ViewState("CampusGroup") = ""
        End If

        ViewState("ItemSelected") = dlstStatusCode.SelectedIndex.ToString
        PopulateDataList("Build")
        'AddDefaultLabelToStatusCodes()
        EnableDisableDefaultCheckBox()
        ClearRhs()
        objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
        'Set the text box to a new guid
        txtStatusCodeId.Text = Guid.NewGuid.ToString
    End Sub
    Private Sub PopulateDataList(CallingFunction As String, Optional ByVal GuidToFind As String = "", Optional ByVal ds As DataSet = Nothing)
        Dim dv As New DataView
        Dim ds2 As New DataSet

        If ds Is Nothing Then
            Dim facade As New StatusCodeFacade

            ViewState("StatusLevel") = ddlStatusLevel.SelectedValue
            ViewState("Status") = ddlStatus.SelectedValue
            ViewState("CampusGroup") = ddlCampusGroup.SelectedValue
            ds2 = facade.GetStatusCodesByStatusAndDefault(ViewState("StatusLevel"), ViewState("Status"), ViewState("CampusGroup")) 'facade.PopulateDataList(ViewState("StatusLevel"), ViewState("Status"), ViewState("CampusGroup"))
        End If

        With dlstStatusCode
            Select Case CallingFunction
                Case "Delete"
                    dlstStatusCode.SelectedIndex = -1
                Case "New"
                    Dim myDRV As DataRowView
                    Dim i As Integer
                    Dim GuidInDV As String
                    For Each myDRV In dv
                        GuidInDV = myDRV(0).ToString
                        If GuidToFind.ToString = GuidInDV Then
                            Exit For
                        End If
                        i += 1
                    Next
                    dlstStatusCode.SelectedIndex = i
                    ViewState("ItemSelected") = i

                Case "Sort"
                    If CInt(ViewState("ItemSelected")) > -1 Then
                        Dim myDRV As DataRowView
                        Dim i As Integer
                        Dim GuidInDV As String
                        For Each myDRV In dv
                            GuidInDV = myDRV(0).ToString
                            If GuidToFind.ToString = GuidInDV Then
                                Exit For
                            End If
                            i += 1
                        Next
                        dlstStatusCode.SelectedIndex = i
                        ViewState("ItemSelected") = i
                    Else
                        dlstStatusCode.SelectedIndex = -1
                        ViewState("ItemSelected") = -1
                    End If
                Case "Edit"
                    dlstStatusCode.SelectedIndex = CInt(ViewState("ItemSelected"))
                Case "Item"
                    dlstStatusCode.SelectedIndex = CInt(ViewState("ItemSelected"))
                Case "Build"
                    dlstStatusCode.SelectedIndex = -1
                Case "Add"
                    dlstStatusCode.SelectedIndex = -1
                Case Else
                    dlstStatusCode.SelectedIndex = -1
            End Select
            .DataSource = ds2
            .DataBind()
        End With
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnsave.Click
        Dim objCommon As New CommonUtilities
        Dim facade As New StatusCodeFacade
        Dim result As String

        Try
            'code added by Balaji on 06/16/2009 to fix issue 14542
            'If the system status is externship, validate to see if there are any status mapped to Externship
            If ddlSysStatusId.SelectedValue = "22" Then
                Dim strStatusCodeId = ""
                strStatusCodeId = (New ExternshipAttendanceFacade).GetExternshipStatusDescrip(campusId, txtStatusCodeId.Text)
                If Not strStatusCodeId = "" Then
                    Dim strMessage As String
                    strMessage = "Save failed, as only one status can be mapped to externship status." & vbCrLf
                    strMessage &= "Status code - " & strStatusCodeId & " has already been mapped to externship status."
                    DisplayErrorMessage(strMessage)
                    Return
                End If
            End If

            If ViewState("MODE") = "NEW" Then

                'Populate course object with the info.
                'Move the values in the controls over to the object
                Dim obj As StatusCodeInfo = PopulateStatusCodeObject()

                'Insert CampusGroup into syCampGrps Table
                result = facade.InsertStatusCode(obj, CType(Session("UserName"), String))

                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                    Exit Sub
                Else
                    If ddlStatusLevelId.SelectedValue = 2 And MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                        pnlRegent.Visible = True
                        Dim strGetRegentDegree As String = (New regentFacade).updateMaintenanceData("syStatusCodes", txtStatusCodeId.Text, ddlAcadStatus.SelectedValue, "MapId", "StatusCodeId")
                    End If
                    GetStatusCodeDetails(txtStatusCodeId.Text)
                End If
                'Populate the datalist
                PopulateDataList("Build")

                'Set Mode to Edit
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"
                'savedguid = txtStatusCodeId.Text
            Else
                'Populate course object with the info.
                'Move the values in the controls over to the object
                Dim obj As StatusCodeInfo = PopulateStatusCodeObject()
                'Insert CampusGroup into syCampGrps Table
                result = facade.UpdateStatusCode(obj, CType(Session("UserName"), String))

                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                Else
                    If ddlStatusLevelId.SelectedValue = 2 And MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                        pnlRegent.Visible = True
                        Dim strGetRegentDegree As String = (New regentFacade).updateMaintenanceData("syStatusCodes", txtStatusCodeId.Text, ddlAcadStatus.SelectedValue, "MapId", "StatusCodeId")
                    End If
                    GetStatusCodeDetails(txtStatusCodeId.Text)
                End If



                'Populate the datalist
                PopulateDataList("Build")
            End If

            ViewState("DefaultStatusCodes") = (New StatusCodeFacade).CheckIfDefaultStatusCodeExistForCampusGroup(ddlCampGrpId.SelectedValue).ToString
            ' AddDefaultLabelToStatusCodes()
            EnableDisableDefaultCheckBox()

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstStatusCode, txtStatusCodeId.Text, ViewState, Header1)

            CommonWebUtilities.RestoreItemValues(dlstStatusCode, txtStatusCodeId.Text.ToString)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    'Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    Dim objCommon As New CommonUtilities
    '    Dim ds As New DataSet
    '    Dim objListGen As New DataListGenerator
    '    Dim sw As New System.IO.StringWriter
    '    Dim rptExcept As New ReportExceptionsFacade
    '    Try
    '        If ViewState("MODE") = "NEW" Then
    '            objCommon.DoInsert(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
    '            rptExcept.RefreshLeadStatus()
    '            'Set the Primary Key value, so it can be used later by the DoUpdate method
    '            txtStatusCodeId.Text = objCommon.PagePK
    '            ds = objListGen.SummaryListGenerator()
    '            PopulateDataList(ds, txtStatusCodeId.Text, "New")
    '            ds.WriteXml(sw)
    '            ViewState("ds") = sw.ToString()
    '            'Set Mode to EDIT since a record has been inserted into the db.
    '            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
    '            ViewState("MODE") = "EDIT"

    '        ElseIf viewstate("MODE") = "EDIT" Then
    '            objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
    '            rptExcept.RefreshLeadStatus()
    '            ds = objListGen.SummaryListGenerator()
    '            PopulateDataList(ds, ViewState("SelectedIndex"), "Edit")
    '            ds.WriteXml(sw)
    '            ViewState("ds") = sw.ToString()
    '        End If
    '    Catch ex As System.Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        'Redirect to error page.
    '        If ex.InnerException Is Nothing Then
    '            Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
    '        Else
    '            Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
    '        End If
    '        Response.Redirect("../ErrorPage.aspx")
    '    End Try

    'End Sub
    Private Function PopulateStatusCodeObject() As StatusCodeInfo
        Dim obj As New StatusCodeInfo

        obj.StatusCodeId = XmlConvert.ToGuid(txtStatusCodeId.Text)
        obj.Code = txtStatusCode.Text
        obj.Descrip = txtStatusCodeDescrip.Text
        'Obj.StatusLevelId = (ddlStatusLevel.SelectedItem.Value).ToString
        ViewState("StatusLevel") = ddlStatusLevelId.SelectedItem.Value.ToString
        ViewState("Status") = ddlStatusId.SelectedItem.Value.ToString
        obj.StatusLevelId = CType((ddlStatusLevel.SelectedItem.Value), Integer)
        obj.SystemStatus = CType((ddlSysStatusId.SelectedItem.Value), Integer)
        obj.StatusId = XmlConvert.ToGuid(ddlStatusId.SelectedItem.Value)
        obj.CampusGroup = XmlConvert.ToGuid(ddlCampGrpId.SelectedItem.Value)
        If txtModUser.Text <> "" Then
            obj.ModUser = txtModUser.Text
        End If
        If txtModDate.Text = "" Then
            obj.ModDate = Date.Now
        Else
            obj.ModDate = CType(txtModDate.Text, Date)
        End If

        obj.IsInUse = Not ddlStatusLevelId.Enabled

        ' This Code will replace the IsDefault Lead Status to work with Is Default Status check box
        If checkboxIsDefaultStatus.Visible And checkboxIsDefaultStatus.Enabled Then
            If obj.StatusLevelId = 2 And obj.SystemStatus = 9 Then
                Dim defaultExist As Boolean
                Dim isDefaultStatus As Boolean = IsCurrentStatusCodeDefault(txtStatusCodeId.Text)
                Dim selectedCampusGuid As Guid
                If string.IsNullOrEmpty(ddlCampGrpId.SelectedValue) = False Then
                    selectedCampusGuid = XmlConvert.ToGuid(ddlCampGrpId.SelectedValue)
                End If
                defaultExist = CheckIfDefaulExist(obj.StatusLevelId, obj.SystemStatus, selectedCampusGuid)
                If defaultExist = False Then
                    obj.IsDefaultLeadStatus = True
                Else If isDefaultStatus and checkboxIsDefaultStatus.Checked = False Then
                    obj.IsDefaultLeadStatus = True
                Else
                    obj.IsDefaultLeadStatus = checkboxIsDefaultStatus.Checked
                End If
            Else 
                obj.IsDefaultLeadStatus = checkboxIsDefaultStatus.Checked
            End If
        ElseIf chkIsDefaultLeadStatus.Visible And chkIsDefaultLeadStatus.Enabled Then
            obj.IsDefaultLeadStatus = chkIsDefaultLeadStatus.Checked
        Else
            obj.IsDefaultLeadStatus = False
        End If

        Return obj
    End Function
    Private Sub DisplayErrorMessage(errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub

    Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnnew.Click
        Dim objcommon As New CommonUtilities

        Try
            ClearRhs()

            'Populate the datalist
            PopulateDataList("Build")
            'enable status level dropdownlist
            ddlStatusLevelId.Enabled = True
            'Set the text box to a new guid
            txtStatusCodeId.Text = Guid.NewGuid.ToString
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            'cblProbationTypeId.Visible = False
            '   set Style to Selected Item
            '   AddDefaultLabelToStatusCodes()
            EnableDisableDefaultCheckBox()
            ' CommonWebUtilities.SetStyleToSelectedItem(dlstStatusCode, System.Guid.Empty.ToString, ViewState, Header1)

            CommonWebUtilities.RestoreItemValues(dlstStatusCode, Guid.Empty.ToString)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btndelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        'Dim ds As New DataSet
        'Dim sw As New StringWriter
        Dim facade As New StatusCodeFacade
        Try
            Dim result As String = facade.DeleteStatusCode(txtStatusCodeId.Text)
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)

            Else
                ClearRhs()
                PopulateDataList("Build")
                'Set the text box to a new guid
                txtStatusCodeId.Text = Guid.NewGuid.ToString
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                'cblProbationTypeId.Visible = False
            End If

            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                BuildSession()
            End If

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub
    Private Sub BuildSession()
        With ddlAcadStatus
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("ACADSTATUS", txtStatusCodeId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub dlstStatusCode_ItemCommand(source As Object, e As DataListCommandEventArgs) Handles dlstStatusCode.ItemCommand
        Dim selIndex As Integer
        'Dim ds As New DataSet
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Try
            strGUID = dlstStatusCode.DataKeys(e.Item.ItemIndex).ToString()

            Master.PageObjectId = CType(e.CommandArgument, String)
            Master.PageResourceId = CType(ResourceId, Integer)
            Master.SetHiddenControlForAudit()
            GetStatusCodeDetails(strGUID)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = e.Item.ItemIndex
            dlstStatusCode.SelectedIndex = selIndex

            PopulateDataList("Build")

            'Dim lnkStatusCode As LinkButton = CType(e.Item.FindControl("LinkButton1"), LinkButton)
            ViewState("DefaultStatusCodes") = (New StatusCodeFacade).CheckIfDefaultStatusCodeExistForCampusGroup(ddlCampGrpId.SelectedValue)
            EnableDisableDefaultCheckBox()
            CommonWebUtilities.RestoreItemValues(dlstStatusCode, e.CommandArgument.ToString)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstAgencySp_ItemCommand " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstAgencySp_ItemCommand " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub GetStatusCodeDetails(statusCodeID As String)
        Dim facade As New StatusCodeFacade
        BindStatusCodeInfoExist(facade.GetStatusCodeDetails(statusCodeID))
    End Sub


    Private Sub BindStatusCodeInfoExist(statusCodeInfo As StatusCodeInfo)

        'Bind The StudentInfo Data From The Database
        With statusCodeInfo
            txtStatusCodeId.Text = .StatusCodeId.ToString
            ddlCampGrpId.SelectedValue = .CampusGroup.ToString
            ddlStatusId.SelectedValue = .StatusId.ToString
            txtStatusCode.Text = .Code
            txtStatusCodeDescrip.Text = .Descrip
            ddlStatusLevelId.SelectedValue = .StatusLevelId.ToString
            ViewState("StatusLevelId") = .StatusLevelId.ToString
            If ViewState("StatusLevelId") = 2 Then
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    pnlRegent.Visible = True
                    Try
                        ddlAcadStatus.SelectedValue = (New regentFacade).getAllMaintenanceDataByPrimaryKey("syStatusCodes", "StatusCodeId", txtStatusCodeId.Text)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        ddlAcadStatus.SelectedIndex = 0
                    End Try
                End If

            End If
            PopulateSysStatusDDL(CType(ViewState("StatusLevelId"), String))
            ddlSysStatusId.SelectedValue = .SystemStatus.ToString
            txtModDate.Text = .ModDate.ToString
            txtModUser.Text = .ModUser
            ddlStatusLevelId.Enabled = Not .IsInUse
            chkIsDefaultLeadStatus.Checked = .IsDefaultLeadStatus
            checkboxIsDefaultStatus.Checked = .IsDefaultLeadStatus
        End With
    End Sub
    Private Sub chkStatus_CheckedChanged(sender As Object, e As EventArgs) Handles chkStatus.CheckedChanged
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        'Dim ds2 As New DataSet
        'Dim sw As New StringWriter
        Dim objCommon As New CommonUtilities
        'Dim dv2 As New DataView
        '        Dim sStatusId As String

        Try
            ClearRhs()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds, "-1", "Check")

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub ClearRhs()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlrhs.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    If CType(ctl, DropDownList).Items.Count > 0 Then
                        CType(ctl, DropDownList).SelectedIndex = 0
                    End If

                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub PopulateDataList(ds As DataSet, guidToFind As String, callingFunction As String)
        Dim ds2 As DataSet
        Dim dv2 As New DataView
        Dim objListGen As New DataListGenerator
        Dim sStatusId As String

        ds2 = objListGen.StatusIdGenerator()
        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (chkStatus.Checked = True) Then 'Show Active Only

            Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays active records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "StatusCodeDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        ElseIf (chkStatus.Checked = False) Then

            Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays inactive records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "StatusCodeDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        End If
        With dlstStatusCode
            Select Case callingFunction
                Case "Delete"
                    dlstStatusCode.SelectedIndex = -1
                Case "New"
                    Dim myDrv As DataRowView
                    Dim i As Integer
                    Dim guidInDv As String
                    For Each myDrv In dv2
                        guidInDv = myDrv(0).ToString
                        If guidToFind.ToString = guidInDv Then
                            Exit For
                        End If
                        i += 1
                    Next
                    dlstStatusCode.SelectedIndex = i
                Case "Edit"
                    dlstStatusCode.SelectedIndex = CInt(ViewState("SelectedIndex"))
                Case "Item"
                    dlstStatusCode.SelectedIndex = CInt(ViewState("SelectedIndex"))
                Case "Check"
                    dlstStatusCode.SelectedIndex = -1
                Case "Add"
                    dlstStatusCode.SelectedIndex = -1
            End Select
            .DataSource = dv2
            .DataBind()
        End With
    End Sub

    Private Sub ddlStatusLevelId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatusLevelId.SelectedIndexChanged
        Dim facade As New StatusCodeFacade
        'Put user code to initialize the page here
        ViewState("StatusLevelId") = ddlStatusLevelId.SelectedItem.Value.ToString()
        If (ViewState("StatusLevelId") <> "") Then
            PopulateSysStatusDDL(CType(ViewState("StatusLevelId"), String))
        End If
        If ddlStatusLevelId.SelectedIndex >= 1 Then
            If ViewState("StatusLevelId") = 2 Then
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    pnlRegent.Visible = True
                    pnlRegentAwardChild.Visible = True
                    ddlAcadStatus.Visible = True
                    BuildSession()
                End If
            End If
            If (ViewState("StatusLevelId") <> "" And ViewState("StatusLevelId") = 1) And Not ddlCampGrpId.SelectedValue = "" Then
                chkIsDefaultLeadStatus.Visible = True
                ViewState("DefaultStatusCodes") = facade.CheckIfDefaultStatusCodeExistForCampusGroup(ddlCampGrpId.SelectedValue)
                If Not ViewState("DefaultStatusCodes") = "" And Not ViewState("DefaultStatusCodes") = Guid.Empty.ToString Then
                    'If the selected item was a default status then enable 
                    If Trim(CType(ViewState("DefaultStatusCodes"), String)) = Trim(txtStatusCodeId.Text) Then
                        chkIsDefaultLeadStatus.Enabled = True
                    Else
                        chkIsDefaultLeadStatus.Enabled = False
                    End If
                Else
                    'If no lead status has been set for campus group and only if New Lead is selected
                    If ddlSysStatusId.SelectedValue = "1" Then
                        chkIsDefaultLeadStatus.Enabled = True
                    Else
                        chkIsDefaultLeadStatus.Enabled = False
                    End If
                End If
            Else
                chkIsDefaultLeadStatus.Visible = False
            End If
        End If
    End Sub
    Private Sub PopulateSysStatusDDL(statusLevelId As String)
        '
        '   Get Degrees data to bind the CheckBoxList
        '
        Dim facade As New StatusCodeFacade

        ' Bind the Dataset to the CheckBoxList
        With ddlSysStatusId
            .DataSource = facade.GetSysStatuses(statusLevelId, False)
            .DataTextField = "SysStatusDescrip"
            .DataValueField = "SysStatusId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles MyBase.PreRender

        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add buttons 
        controlsToIgnore.Add(btnsave)
        controlsToIgnore.Add(btnBuildList)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BindToolTip()
    End Sub


    Protected Sub ddlCampGrpId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCampGrpId.SelectedIndexChanged
        Dim facade As New StatusCodeFacade
        If (ViewState("StatusLevelId") <> "" And ViewState("StatusLevelId") = 1) And Not ddlCampGrpId.SelectedValue = "" Then
            chkIsDefaultLeadStatus.Visible = True
            ViewState("DefaultStatusCodes") = facade.CheckIfDefaultStatusCodeExistForCampusGroup(ddlCampGrpId.SelectedValue)
            If Not ViewState("DefaultStatusCodes") = "" And Not ViewState("DefaultStatusCodes") = Guid.Empty.ToString Then
                'If the selected item was a default status then enable 
                If Trim(CType(ViewState("DefaultStatusCodes"), String)) = Trim(txtStatusCodeId.Text) Then
                    chkIsDefaultLeadStatus.Enabled = True
                Else
                    chkIsDefaultLeadStatus.Enabled = False
                End If
            Else
                'If no lead status has been set for campus group and only if New Lead is selected
                If ddlSysStatusId.SelectedValue = 1 Then
                    chkIsDefaultLeadStatus.Enabled = True
                Else
                    chkIsDefaultLeadStatus.Enabled = False
                End If
            End If
        Else
            chkIsDefaultLeadStatus.Visible = False
        End If
        EnableDisableDefaultCheckBox()
    End Sub
    Protected Sub ddlStatusLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlStatusLevel.SelectedIndexChanged
    End Sub

    Private Function CheckIfDefaulExist(statusLevel As Integer, systemStatus As Integer, campusGroup As Guid) As Boolean
        Dim facade As New StatusCodeFacade

        Return facade.DoesDefaultStatusExistForCampusGroup(statusLevel, systemStatus, campusGroup)
    End Function

    Private Function IsCurrentStatusCodeDefault(statusCodeId As String) As Boolean
        Dim statusCode As StatusCodeInfo
        Dim facade As New StatusCodeFacade
        statusCode = facade.GetStatusCodeDetails(statusCodeId)
        Return statusCode.IsDefaultLeadStatus
    End Function

    Private Sub EnableDisableDefaultCheckBox()
        'Disable the checkbox, if there is any country that is already marked as default
        'there can be only one country
        If ddlStatusLevelId.SelectedItem.Value.ToString = "1" Then
            chkIsDefaultLeadStatus.Visible = True
        Else
            chkIsDefaultLeadStatus.Visible = False
        End If

        If Not ViewState("DefaultStatusCodes") = "" And Not ViewState("DefaultStatusCodes") = Guid.Empty.ToString Then
            If Trim(CType(ViewState("DefaultStatusCodes"), String)) = Trim(txtStatusCodeId.Text) Then
                chkIsDefaultLeadStatus.Enabled = True
            Else
                chkIsDefaultLeadStatus.Enabled = False
            End If
        Else
            Try
                If ddlSysStatusId.SelectedValue = 1 Then
                    chkIsDefaultLeadStatus.Enabled = True
                Else
                    chkIsDefaultLeadStatus.Enabled = False
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                chkIsDefaultLeadStatus.Enabled = False
            End Try
        End If


        'Section for Student Enrollments (Currently attending)
        Dim enabledDefaultStatusCheckBox As Boolean
        Dim selectedStatusLevel As Integer
        Dim selectedSysStatus As Integer

        If String.IsNullOrEmpty(ddlSysStatusId.SelectedValue) = False Then
            selectedSysStatus = Convert.ToInt32(ddlSysStatusId.SelectedValue)
        End If

        If String.IsNullOrEmpty(ddlStatusLevelId.SelectedValue) = False Then
            selectedStatusLevel = Convert.ToInt32(ddlStatusLevelId.SelectedValue)
        End If

        Dim statusCodeId As String = txtStatusCodeId.Text

        If selectedStatusLevel = "2" And selectedSysStatus = 9 Then
            enabledDefaultStatusCheckBox = True
            Dim defaultExist As Boolean

            Dim selectedCampusGuid As Guid
            If string.IsNullOrEmpty(ddlCampGrpId.SelectedValue) = False Then
                selectedCampusGuid = XmlConvert.ToGuid(ddlCampGrpId.SelectedValue)
            End If

            defaultExist = CheckIfDefaulExist(selectedStatusLevel, selectedSysStatus, selectedCampusGuid)

            If defaultExist Then
                Dim isDefaultStatus As Boolean = IsCurrentStatusCodeDefault(statusCodeId)
                If (isDefaultStatus = True) Then
                    'if current selected is default status show defaul message
                    checkboxIsDefaultStatus.Text = CurrentlyAttendingDefaultMessage
                Else
                    'else if not then show override message
                    checkboxIsDefaultStatus.Text = CurrentlyAttendingOverrideMessage
                End If
            Else
                'If default does not exist then show default message to set current attending status as default
                checkboxIsDefaultStatus.Text = CurrentlyAttendingDefaultMessage
                
            End If

        Else
            enabledDefaultStatusCheckBox = False
        End If

        checkboxIsDefaultStatus.Visible = enabledDefaultStatusCheckBox
        checkboxIsDefaultStatus.Enabled = enabledDefaultStatusCheckBox
    End Sub


    Protected Sub ddlSysStatusId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSysStatusId.SelectedIndexChanged
        If Not ViewState("DefaultStatusCodes") = "" And Not ViewState("DefaultStatusCodes") = Guid.Empty.ToString Then
            If Trim(CType(ViewState("DefaultStatusCodes"), String)) = Trim(txtStatusCodeId.Text) Then
                chkIsDefaultLeadStatus.Enabled = True
            Else
                chkIsDefaultLeadStatus.Enabled = False
            End If
        End If
        If ddlSysStatusId.SelectedIndex >= 1 Then
            If ddlSysStatusId.SelectedValue <> "" And ddlSysStatusId.SelectedValue = 1 Then
                chkIsDefaultLeadStatus.Enabled = True
            Else
                chkIsDefaultLeadStatus.Checked = False
                chkIsDefaultLeadStatus.Enabled = False
            End If
        End If
        EnableDisableDefaultCheckBox()
    End Sub
    Protected Sub ddlCampusGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCampusGroup.SelectedIndexChanged

        EnableDisableDefaultCheckBox()
    End Sub
    

End Class
