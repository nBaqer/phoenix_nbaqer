﻿
Imports System.Web.UI
Imports System.Diagnostics
Imports System.ComponentModel.Design
Imports Fame.AdvantageV1.BusinessFacade
Imports Microsoft.VisualBasic
Imports System.Reflection
Imports System.Data.SqlClient
Imports Advantage.Business.Objects
Imports System.Web.Razor.Parser
Imports Fame.AdvantageV1.Common.RegentAdvantageIntegration
Imports Telerik.Web.UI
Imports Fame.Advantage.DataAccess.LINQ
Imports Fame.Parameters.Info
Imports Fame.Parameters.Interfaces
Imports Fame.Parameters.Collections
Imports Fame.Advantage.Reporting.Info
Imports System.IO
Imports System.Xml.Serialization
Imports Fame.Advantage.Common.LINQ.Entities
'Imports Telerik.Web.UI.Upload
Imports Fame.Advantage.Common
Imports System.Data
Imports Fame.AdvantageV1.Common
Imports System.Web
Imports System.Runtime.CompilerServices
Imports Telerik.OpenAccess
'Imports System.Windows.Forms.VisualStylesRadGridReportSettings

Public Module StringExtensions

    <Extension()>
    Public Function IsNotNullOrEmpty(ByVal s As String) As Boolean
        Return s IsNot Nothing AndAlso s.Trim.Length > 0
    End Function
    <Extension()>
    Public Function ContainsAny(ByVal s As String, ByVal ParamArray values As String()) As Boolean
        If s.IsNotNullOrEmpty AndAlso values.Length > 0 Then
            For Each value As String In values
                If s.Contains(value) Then Return True
            Next
        End If

        Return False
    End Function
End Module
Partial Class ParamReport
    Inherits BasePage
    'Private boolCancelOperation As Boolean = False
    Protected resourceId As Integer
    Protected CampusId As String
    Dim userId As String
    Private pObj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings

#Region "Properties"
    Private _resourceId As Integer
    Protected boolShowErr As Boolean = False

    Public Shared connectionString As String
    Public SqlConnection As SqlConnection

    Public Property ResId() As Integer
        Get
            Return _resourceId
        End Get
        Set(ByVal Value As Integer)
            _resourceId = Value
        End Set
    End Property
    Public ReadOnly Property Report() As Fame.Advantage.Reporting.Info.ReportInfo
        Get
            Return GetReport()
        End Get
    End Property

#End Region
#Region "Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString
            SqlConnection = New SqlConnection(connectionString)
            Dim m_Context As HttpContext

            Dim advantageUserState As New User()
            advantageUserState = AdvantageSession.UserState

            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            CampusId = AdvantageSession.UserState.CampusId.ToString
            userId = AdvantageSession.UserState.UserId.ToString

            Session("UserId") = userId

            txtResId.Value = resourceId.ToString

            m_Context = HttpContext.Current
            Try
                m_Context.Items("Language") = "En-US"
                m_Context.Items("ResourceId") = resourceId
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)


            End Try


            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, CampusId)
            If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If

            'Grab the ResourceId from the Query string
            ResId = Request.QueryString("resid")
            AddReportControlsToTabs()
            SetPostBackForTabs()

            Dim strReportName, strReportDescrip, strHTML As String
            strReportName = Report.FriendlyName
            strReportDescrip = Report.ReportDescription
            If Not IsPostBack Then
                CreateReportExportTypes()
                Session(ResId.ToString) = Nothing

                Select Case ResId
                    'IPEDS
                    Case 412, 413, 414, 415, 416, 417, 421, 452, 458, 641, 642, 643, 644, 645, 659, 660, 667, 668, 669, 670, 671, 672, 673, 674, 782, 784, 785, 786, 836, 877, 846
                        strHTML = "<div id=""ReportTitle"" Class=""RptTitle"" title=""" & strReportDescrip & """>" & strReportDescrip & "</div>"
                        radPanelReportDisplayOption.Visible = True
                    Case 238 'Transcript report
                        radPanelReportDisplayOption.Visible = False
                        strHTML = "<div id=""ReportTitle"" Class=""RptTitle"" title=""" & strReportDescrip & """>" & strReportName & "</div>"
                    Case Else 'other reports
                        strHTML = "<div id=""ReportTitle"" Class=""RptTitle"" title=""" & strReportDescrip & """>" & strReportName & "</div>"
                        radPanelReportDisplayOption.Visible = True
                End Select

                RptDescrip.Text = strHTML

            End If

            Page.Title = strReportName
            'Set data source for panel grids
            BindDataSource(ResId)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Session("Error") = ex.Message
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub RadScriptManager_AsyncPostBackError(ByVal sender As Object, ByVal e As AsyncPostBackErrorEventArgs)
        Session("Error") = e.Exception.Message
        Response.Redirect("../ErrorPage.aspx")
    End Sub

    Protected Sub RadGridReportSettings_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs)
        'Sets the tooltip description for the preference item description
        If TypeOf e.Item Is GridDataItem Then

            Dim myDataItem As GridDataItem = CType(e.Item, GridDataItem)
            Dim prefDescripTooltipText As String = myDataItem("PrefDescrip").Text
            Dim myDataButton As LinkButton = CType(myDataItem("PrefNameLink").Controls(0), LinkButton)

            If prefDescripTooltipText <> "&nbsp;" Then
                If prefDescripTooltipText.Length > 200 Then
                    prefDescripTooltipText = prefDescripTooltipText.Substring(0, 200)
                    myDataButton.ToolTip = prefDescripTooltipText & "..."
                Else
                    myDataButton.ToolTip = prefDescripTooltipText
                End If
            Else
                myDataButton.ToolTip = "No description available"
            End If

        End If

        'Set the width and style of edit textboxes
        If TypeOf e.Item Is GridEditableItem And e.Item.IsInEditMode Then
            Dim myEditItem As GridEditableItem = CType(e.Item, GridEditableItem)
            Dim txtPrefName, txtPrefDescrip As TextBox ', txtDescripNotice

            txtPrefName = CType(myEditItem("PrefName").Controls(0), TextBox)
            txtPrefDescrip = CType(myEditItem("PrefDescrip").Controls(0), TextBox)

            txtPrefName.Width = Unit.Pixel(290)
            txtPrefName.CssClass = "EditName"

            'Set-up the description edit box
            With txtPrefDescrip
                .Width = Unit.Pixel(290)
                .Height = Unit.Pixel(65)
                .TextMode = TextBoxMode.MultiLine
                .CssClass = "EditDescrip"
            End With

        End If

    End Sub

    Protected Sub RadGridReportSettings_OnItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs)
        'Here, we're going to check if the CommandName for the control was triggered, if so, we will
        'do something with the GUID, then display the user pref name and description where the report
        'name and description would be.
        Try
            If e.CommandName = "LoadPrefs" Or e.CommandName = "EditSettings" Then
                If TypeOf e.Item Is GridDataItem Then
                    Dim myDataItem As GridDataItem = CType(e.Item, GridDataItem)
                    Dim myDataButton As LinkButton = CType(myDataItem("PrefNameLink").Controls(0), LinkButton)

                    Dim strGUID, strPrefName, strPrefDescrip, strHTML, strXML As String

                    strGUID = myDataItem.GetDataKeyValue("UserSettingId").ToString()
                    strPrefName = myDataButton.Text
                    strPrefDescrip = myDataItem("PrefDescrip").Text

                    If strPrefDescrip = "&nbsp;" Then strPrefDescrip = "No description available."

                    strHTML = "<div id=""ReportTitle"" Class=""RptTitle"" title=""" & strPrefDescrip & """>" & strPrefName & "</div>"
                    'strHTML = strHTML & "<div id=""ReportDescrip"" Class=""RptDescrip"">" & strPrefDescrip & "</div>"

                    RptDescrip.Text = strHTML

                    'Now, we need to populate the save settings textboxes
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("txtSettingName"), TextBox).Text = strPrefName
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("txtSettingDescription"), TextBox).Text = strPrefDescrip

                    'Hide saved successfully message 
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblSuccessful"), Label).Visible = False
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheck"), ImageButton).Visible = False

                    'Hide the view successfully message
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewSuccess"), Label).Visible = False
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheckView"), ImageButton).Visible = False

                    'Hide the View Error
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewExportError"), Label).Visible = False

                    'Get the XML from the database
                    strXML = GetStoredXML(strGUID)

                    'Place the values back into the user controls
                    'Call GetSavedSettings(strXML)

                    Dim objReportInfo As Fame.Advantage.Reporting.Info.ReportInfo = Report
                    Dim UserReportSettings As ReportUserSettingsInfo = GetSavedReportSettings(strXML)
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblPageNumberStyle"), RadioButtonList).SelectedValue = UserReportSettings.PagingOption
                    'DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblHeaderStyle"), RadioButtonList).SelectedValue = UserReportSettings.HeadingOption
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblDisplayDate"), RadioButtonList).SelectedValue = UserReportSettings.ReportDateOption
                    'If Not UserReportSettings Is Nothing Then
                    For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                        Dim MyParamSet As ParameterSetInfo = tabview.ParamSet
                        Dim myControl As Control = Page.Controls(0).FindControl("ContentMain2").FindControl(tabview.UserControlName)
                        For Each ctrl As Control In myControl.Controls
                            'Dim myType As Type = ctrl.GetType
                            If TypeOf ctrl Is RadPanelBar Then
                                Dim myPanelBar As RadPanelBar = DirectCast(ctrl, RadPanelBar)

                                For Each item As RadPanelItem In myPanelBar.Items
                                    For Each section As ParameterSectionInfo In MyParamSet.ParameterSectionCollection

                                        Dim myPanelItem As RadPanelItem = myPanelBar.FindItemByValue(section.SectionName)
                                        'Dim PanelName As String = myPanelItem.Value

                                        If section.SectionName & " Root" = item.Value Then
                                            For Each detailitem As ParameterDetailItemInfo In section.ParameterDetailItemCollection
                                                Dim itemCtrl As Control = myPanelItem.FindControl(detailitem.ItemName)
                                                If TypeOf itemCtrl Is IParamItemControl Then
                                                    If TypeOf itemCtrl Is IFilterControl Then
                                                        Dim itemUserControl As IFilterControl = DirectCast(itemCtrl, IFilterControl)
                                                        'Dim MySetting As New ParamItemUserSettingsInfo
                                                        If Not UserReportSettings Is Nothing Then
                                                            If Not UserReportSettings.FilterSettings Is Nothing AndAlso UserReportSettings.FilterSettings.Count > 0 Then
                                                                For Each Setting As ParamItemUserSettingsInfo In UserReportSettings.FilterSettings
                                                                    If Setting.ItemName = detailitem.ItemName Then
                                                                        'MySetting = Setting
                                                                        'ItemUserControl.ClearControls()
                                                                        itemUserControl.SavedSettings = Setting
                                                                        itemUserControl.LoadSavedReportSettings()
                                                                    End If
                                                                Next
                                                            Else
                                                                itemUserControl.LoadSavedReportSettings()
                                                            End If
                                                        Else
                                                            itemUserControl.LoadSavedReportSettings()
                                                        End If
                                                        'ItemUserControl.SavedSettings = MySetting
                                                        'ItemUserControl.LoadSavedReportSettings()
                                                    ElseIf TypeOf itemCtrl Is ISortControl Then
                                                        Dim itemUserControl As ISortControl = DirectCast(itemCtrl, ISortControl)
                                                        Dim userSorts As List(Of SortItemInfo)
                                                        userSorts = UserReportSettings.SortSettings
                                                        If Not userSorts Is Nothing Then
                                                            itemUserControl.SavedSettings = userSorts
                                                            itemUserControl.LoadSavedReportSettings()
                                                        End If
                                                    ElseIf TypeOf itemCtrl Is ICustomControl Then
                                                        Dim itemUserControl As ICustomControl = DirectCast(itemCtrl, ICustomControl)
                                                        For Each customOption As ParamItemUserSettingsInfo In UserReportSettings.CustomOptions
                                                            If customOption.ItemName = detailitem.ItemName Then
                                                                itemUserControl.SavedSettings = customOption
                                                                itemUserControl.LoadSavedReportSettings()
                                                            End If
                                                        Next
                                                    Else
                                                        Throw New Exception("Unknown Control Type")
                                                    End If
                                                End If
                                            Next
                                        End If
                                    Next
                                Next
                            End If
                        Next
                    Next
                    'End If

                    'We need to modify the ModDate so the most recently selected items appear 
                    'at the top of the list
                    Dim comModDate As SqlCommand = New SqlCommand("UPDATE syReportUserSettings SET ModDate = CONVERT(smalldatetime, GETDATE()) WHERE UserSettingId = '" & strGUID & "'", SqlConnection)

                    comModDate.CommandType = CommandType.Text

                    If SqlConnection.State = ConnectionState.Open Then SqlConnection.Close()

                    SqlConnection.Open()
                    Try

                        comModDate.ExecuteReader()

                    Catch ex As Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(ex)

                    Finally
                        If SqlConnection.State = ConnectionState.Open Then SqlConnection.Close()
                    End Try
                End If
                If e.CommandName = "LoadPrefs" Then
                    RadRptTabStrip.SelectedIndex = Report.StartingTabIndex
                    RadRptMultiPage.SelectedIndex = Report.StartingTabIndex
                End If

                If e.CommandName = "EditSettings" Then
                    RadRptTabStrip.SelectedIndex = 4
                    RadRptMultiPage.SelectedIndex = 4
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("txtSettingName"), TextBox).Focus()
                End If

            ElseIf e.CommandName = "Delete" Then
                'DeleteSelectedSetting()      
                Dim strGuid As String
                Dim myDataItem As GridDataItem = TryCast(e.Item, GridDataItem)
                If (myDataItem IsNot Nothing) Then
                    strGuid = myDataItem.GetDataKeyValue("UserSettingId").ToString()
                    DeleteReportSetting(strGuid)
                    Response.Redirect(Page.Request.Url.ToString)
                End If
            Else

                ShowSelectionSummary()
                'Clear the save msg
                DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblSuccessful"), Label).Visible = False
                DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheck"), ImageButton).Visible = False
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Public Sub DeleteReportSetting(ByVal strGuid As String)
        Dim rptUtils As New ReportCommonUtilsFacade
        rptUtils.DeleteReportSetting(strGuid)
    End Sub

    Protected Function GetStoredXML(ByVal strGUID As String) As String
        'This function will return the stored XML from syReportUserSettings
        Dim strXml As String = String.Empty
        Dim drFindSettingXml As SqlDataReader
        Dim comFindSettingXml As SqlCommand = New SqlCommand("SELECT PrefData FROM syReportUserSettings WHERE UserSettingId = '" & strGUID & "'", SqlConnection)
        comFindSettingXml.CommandType = CommandType.Text

        If SqlConnection.State = ConnectionState.Open Then SqlConnection.Close()

        SqlConnection.Open()
        drFindSettingXml = comFindSettingXml.ExecuteReader
        Try
            While drFindSettingXml.Read()
                strXml = drFindSettingXml(0).ToString
            End While
            Return strXml
        Finally
            drFindSettingXml.Dispose()
            comFindSettingXml.Dispose()
            If SqlConnection.State = ConnectionState.Open Then SqlConnection.Close()
        End Try

    End Function

    Protected Function GetSavedReportSettings(ByVal strXML As String) As ReportUserSettingsInfo
        Dim objStringReader As New StringReader(strXML)
        'Dim myReportSettings As New ReportUserSettingsInfo()
        Try
            'Deserialize
            Dim serializer As New XmlSerializer(GetType(ReportUserSettingsInfo))
            Dim myReportSettings As ReportUserSettingsInfo = TryCast(serializer.Deserialize(objStringReader), ReportUserSettingsInfo)
            Return myReportSettings
        Finally
            objStringReader.Close()
        End Try
    End Function

    Protected Sub RadGridReportSettings_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        'Rebind the datagrid
        BindDataSource(ResId)
    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs)
        Response.Redirect(Page.Request.Url.ToString)
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Dig through the controls and get the values to write to the syReportUserSettings database

        Dim status As String
        Dim blnSetting As Boolean
        Dim strSettingName, strSettingDescrip As String
        Dim txtSetName, txtSetDescrip As TextBox

        'Clear the save button in the event the save is not successful
        DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblSuccessful"), Label).Visible = False
        DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheck"), ImageButton).Visible = False

        'Hide the view successfully message
        DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewSuccess"), Label).Visible = False
        DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheckView"), ImageButton).Visible = False

        'Hide the View Error
        DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewExportError"), Label).Visible = False

        txtSetName = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("txtSettingName"), TextBox)
        txtSetDescrip = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("txtSettingDescription"), TextBox)

        strSettingName = txtSetName.Text
        strSettingDescrip = txtSetDescrip.Text
        'Check for apostrophies since T-SQL cannot handle them on insert or update
        strSettingName = Replace(strSettingName, "'", "''")
        strSettingDescrip = Replace(strSettingDescrip, "'", "''")

        'First we need to determine if this preference already exists, if so -TRUE, we need to update, else, insert.
        status = "Select"
        blnSetting = SelectInsertUpdateTableReport(ResId, strSettingName, String.Empty, String.Empty, status)

        'Write this info into the database
        Dim strXML As String = GetControlStates()

        'If already exists, update. If not, then insert
        If blnSetting = True Then
            status = "Update"
            blnSetting = SelectInsertUpdateTableReport(ResId, strSettingName, strSettingDescrip, strXML, status)
        Else
            status = "Insert"
            blnSetting = SelectInsertUpdateTableReport(ResId, strSettingName, strSettingDescrip, strXML, status)
        End If

        If blnSetting = True Then
            'If the save is successful, display the message
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblSuccessful"), Label).Visible = True
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheck"), ImageButton).Visible = True

        End If
        'Calls the summary info
        Call ShowSelectionSummary()


    End Sub
    ''' <summary>
    '''  Tatiana -This Function Fix the DE10071 Unable to save. 
    ''' </summary>
    ''' <param name="resid"></param>
    ''' <param name="strSettingName"></param>
    ''' <param name="strSettingDescrip"></param>
    ''' <param name="strXML"></param>
    ''' <param name="status"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function SelectInsertUpdateTableReport(ResId As Integer, strSettingName As String, ByVal strSettingDescrip As String, strXML As String, status As String) As Boolean

        Dim query As String = "Select NEWID()"
        Dim blnSetting As Boolean = False
        Try
            If SqlConnection.State = ConnectionState.Open Then SqlConnection.Close()
            SqlConnection.Open()

            If strSettingDescrip.ContainsAny(strSettingDescrip) = False Then
                strSettingDescrip = String.Empty
            End If

            Select Case status
                Case "Insert"

                    Dim cmdID As New SqlCommand(query, SqlConnection)
                    ' FIND the ID from SQL statement newID
                    Dim newid As Guid = cmdID.ExecuteScalar()

                    Dim todaysdate As String = String.Format("{0:yyyy-MM-dd HH:mm:ss }", DateTime.Now)
                    Dim myCommand As New SqlCommand("INSERT INTO syReportUserSettings  (UserSettingId, UserId, ResourceId, PrefName, PrefDescrip, PrefData, ModDate) VALUES(@UserSettingId, @UserId, @ResourceId, @PrefName, @PrefDescrip, @PrefData, @ModDate)", SqlConnection)
                    myCommand.Parameters.AddWithValue("@UserSettingId", newid)
                    myCommand.Parameters.AddWithValue("@UserId", Session("UserName"))
                    myCommand.Parameters.AddWithValue("@ResourceId", ResId)
                    myCommand.Parameters.AddWithValue("@PrefName", strSettingName)
                    myCommand.Parameters.AddWithValue("@PrefDescrip", strSettingDescrip)
                    myCommand.Parameters.AddWithValue("@PrefData", strXML)
                    myCommand.Parameters.AddWithValue("@ModDate", todaysdate)
                    myCommand.CommandType = CommandType.Text
                    myCommand.ExecuteReader()

                    blnSetting = True

                Case "Update"
                    Dim todaysdate As String = String.Format("{0:yyyy-MM-dd HH:mm:ss }", DateTime.Now)
                    Dim myCommand As New SqlCommand("UPDATE syReportUserSettings SET UserId = @UserId," &
                                                    " PrefName = @PrefName, PrefDescrip = @PrefDescrip," &
                                                   " PrefData= @PrefData, ModDate =@ModDate WHERE PrefName = @PrefName" &
                                                   " AND UserId= @UserId AND ResourceId= @ResourceId", SqlConnection)
                    myCommand.Parameters.AddWithValue("@UserId", Session("UserName"))
                    myCommand.Parameters.AddWithValue("@ResourceId", Report.ResourceId)
                    myCommand.Parameters.AddWithValue("@PrefName", strSettingName)
                    myCommand.Parameters.AddWithValue("@PrefDescrip", strSettingDescrip)
                    myCommand.Parameters.AddWithValue("@PrefData", strXML)
                    myCommand.Parameters.AddWithValue("@ModDate", todaysdate)
                    myCommand.CommandType = CommandType.Text
                    myCommand.ExecuteReader()

                    blnSetting = True

                Case "Select"
                    Dim drFindSettingName As SqlDataReader
                    Dim comFindSettingName As SqlCommand = New SqlCommand("SELECT PrefName FROM syReportUserSettings WHERE PrefName = '" & strSettingName & "' AND UserId = '" & Session("UserName").ToString & "' AND ResourceId = " & Report.ResourceId, SqlConnection)

                    comFindSettingName.CommandType = CommandType.Text
                    drFindSettingName = comFindSettingName.ExecuteReader

                    While drFindSettingName.Read()
                        'The setting already exists
                        blnSetting = True
                    End While

            End Select
            Return blnSetting
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw

        Finally
            If SqlConnection.State = ConnectionState.Open Then SqlConnection.Close()
        End Try
    End Function


    Protected Sub ReportDisplayOptions_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            ShowSelectionSummary()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Protected Sub BtnView_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try

            'Hide the View Error
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewExportError"), Label).Visible = False
            'Hide the view successfully message
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewSuccess"), Label).Visible = False
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheckView"), ImageButton).Visible = False
            'Hide the saved successfully message
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblSuccessful"), Label).Visible = False
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheck"), ImageButton).Visible = False
            ' DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblExportProgress"), Label).Text = "Loading..."

            ViewReport()

            'DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("pnlExportProgress").FindControl("lblExportProgress"), Label).Visible = False

            'Calls the summary info
            Call ShowSelectionSummary()



        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Dim errMsg As Label = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewExportError"), Label)
            errMsg.Visible = True
            errMsg.Text = ex.Message & vbCrLf & "exeption" & ex.ToString() & vbCrLf & "inner:" & ex.InnerException.ToString()
        End Try
    End Sub

    Protected Sub RadRptTabStrip_TabClick(ByVal sender As Object, ByVal e As RadTabStripEventArgs)
        If e.Tab.Text.Trim = "Run Report" Then
            ShowSelectionSummary()
            'Hide the save is successful message
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblSuccessful"), Label).Visible = False
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheck"), ImageButton).Visible = False

            'Hide the view successfully message
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewSuccess"), Label).Visible = False
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheckView"), ImageButton).Visible = False

            'Hide the View Error
            DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewExportError"), Label).Visible = False

            'Show the new chars remaining textbox and hide the input box.
            'Dim txtRemChars As New TextBox
            Dim strTxt As String = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("txtSettingDescription"), TextBox).Text
            Dim intLen As Integer = Len(strTxt)
            Dim intCharsRemaining As Integer = (500 - intLen)

            If intCharsRemaining <> 500 Then
                DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("txtLimitLabel"), RadTextBox).Text = intCharsRemaining
            Else
                DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("txtLimitLabel"), RadTextBox).Text = "500"
            End If

            'US3158 - FOR Completions - CIP Data - Deteail & summary Report we need to check theCIP present if not prompt the mesage
            If ResId = 784 Or ResId = 786 Then
                DisplayWarningForIPEDSFallCompletionCIPAndByLvl()
            End If

        End If

    End Sub
#End Region
#Region "Methods"
    Private Sub ViewReport()
        GetUserSelections()
        BuildReport(Report)
    End Sub

    Private Sub CreateReportExportTypes()
        Try
            Dim exportComboBox As RadComboBox = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("RadComboBoxView"), RadComboBox)
            exportComboBox.Items.Clear()

            Dim intResId As Integer
            intResId = CInt(Request.QueryString("resid"))

            If intResId = 795 Then
                Dim item1 As New RadComboBoxItem
                item1.ImageUrl = "~\Images\icon\export_pdf.png"
                item1.Text = "PDF"
                item1.Value = "0"
                exportComboBox.Items.Add(item1)


                Exit Sub
            End If

            If intResId = 816 Then
                exportComboBox.Items.Clear()
                Dim item1 As New RadComboBoxItem
                item1.ImageUrl = "~\Images\icon\export_pdf.png"
                item1.Text = "PDF"
                item1.Value = "0"
                exportComboBox.Items.Add(item1)
                Dim item2 As New RadComboBoxItem
                item2.ImageUrl = "~\Images\icon\export_xls.png"
                item2.Text = "Excel"
                item2.Value = "1"
                exportComboBox.Items.Add(item2)
                Exit Sub
            End If



            Select Case Report.AllowedExportFormat
                Case Fame.Advantage.Reporting.Info.ReportInfo.AllowedExportType.PDFOnly
                    Dim item1 As New RadComboBoxItem
                    item1.ImageUrl = "~\Images\icon\export_pdf.png"
                    item1.Text = "PDF"
                    item1.Value = "0"
                    exportComboBox.Items.Add(item1)
                Case Fame.Advantage.Reporting.Info.ReportInfo.AllowedExportType.ExcelOnly
                    Dim item1 As New RadComboBoxItem
                    item1.ImageUrl = "~\Images\icon\export_xls.png"
                    item1.Text = "Excel"
                    item1.Value = "1"
                    exportComboBox.Items.Add(item1)
                Case Fame.Advantage.Reporting.Info.ReportInfo.AllowedExportType.CSVOnly
                    Dim item1 As New RadComboBoxItem
                    item1.ImageUrl = "~\Images\icon\export_xls.png"
                    item1.Text = "Data Only (.csv)"
                    item1.Value = "2"
                    exportComboBox.Items.Add(item1)
                Case Fame.Advantage.Reporting.Info.ReportInfo.AllowedExportType.PDFCSV
                    Dim item1 As New RadComboBoxItem
                    Dim item2 As New RadComboBoxItem
                    item1.ImageUrl = "~\Images\icon\export_pdf.png"
                    item2.ImageUrl = "~\Images\icon\export_xls.png"
                    item1.Text = "PDF"
                    item2.Text = "Data Only (.csv)"
                    item1.Value = "0"
                    item2.Value = "2"
                    exportComboBox.Items.Add(item1)
                    exportComboBox.Items.Add(item2)
                Case Fame.Advantage.Reporting.Info.ReportInfo.AllowedExportType.PDFExcel
                    Dim item1 As New RadComboBoxItem
                    Dim item2 As New RadComboBoxItem
                    item1.ImageUrl = "~\Images\icon\export_pdf.png"
                    item2.ImageUrl = "~\Images\icon\export_xls.png"
                    item1.Text = "PDF"
                    item2.Text = "Excel"
                    item1.Value = "0"
                    item2.Value = "1"
                    exportComboBox.Items.Add(item1)
                    exportComboBox.Items.Add(item2)
                Case Fame.Advantage.Reporting.Info.ReportInfo.AllowedExportType.PDFExcelWord
                    Dim item1 As New RadComboBoxItem
                    Dim item2 As New RadComboBoxItem
                    Dim item3 As New RadComboBoxItem
                    item1.ImageUrl = "~\Images\icon\export_pdf.png"
                    item2.ImageUrl = "~\Images\icon\export_xls.png"
                    item3.ImageUrl = "~\Images\icon\export_word.png"
                    item1.Text = "PDF"
                    item2.Text = "Excel"
                    item3.Text = "Word"
                    item1.Value = "0"
                    item2.Value = "1"
                    item3.Value = "3"
                    exportComboBox.Items.Add(item1)
                    exportComboBox.Items.Add(item2)
                    exportComboBox.Items.Add(item3)

                Case Fame.Advantage.Reporting.Info.ReportInfo.AllowedExportType.All
                    Dim item1 As New RadComboBoxItem
                    Dim item2 As New RadComboBoxItem
                    Dim item3 As New RadComboBoxItem
                    item1.ImageUrl = "~\Images\icon\export_pdf.png"
                    item2.ImageUrl = "~\Images\icon\export_xls.png"
                    item3.ImageUrl = "~\Images\icon\export_xls.png"
                    item1.Text = "PDF"
                    item2.Text = "Excel"
                    item3.Text = "Data Only (.csv)"
                    item1.Value = "0"
                    item2.Value = "1"
                    item3.Value = "2"
                    exportComboBox.Items.Add(item1)
                    exportComboBox.Items.Add(item2)
                    exportComboBox.Items.Add(item3)
                Case Else
                    Throw New Exception("unknown allowed export types.")
            End Select

            If intResId = 238 Or intResId = 606 Or intResId = 631 Or intResId = 819 Then
                Dim item2 As New RadComboBoxItem
                item2.ImageUrl = "~\Images\icon\export_xls.png"
                item2.Text = "Excel"
                item2.Value = "1"
                exportComboBox.Items.Add(item2)
                Dim item1 As New RadComboBoxItem
                item1.ImageUrl = "~\Images\word_icon.gif"
                item1.Text = "Word"
                item1.Value = "3"
                exportComboBox.Items.Add(item1)
                Exit Sub
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Private Sub BuildReport(ByVal Rpt As Fame.Advantage.Reporting.Info.ReportInfo)
        Dim ty As Type
        Dim mm As MethodInfo
        Dim dAobj As Object
        Dim reportAssembly As Assembly
        Dim getReportAsBytes As [Byte]()
        'Dim strFileFormat As String = ""
        Try

            If ResId = 784 Or ResId = 786 Then
                DisplayWarningForIPEDSFallCompletionCIPAndByLvl()
            End If

            If Session("RptType_" & Report.ReportName) Is Nothing Then
                reportAssembly = Assembly.LoadFrom(Server.MapPath(Rpt.AssemblyFilePath))
                ty = reportAssembly.GetType(Rpt.ReportClass, True)
                Session("RptType_" & Report.ReportName) = ty
            Else
                ty = CType(Session("RptType_" & Report.ReportName), Type)
            End If
            mm = ty.GetMethod(Rpt.CreationMethod)

            'Set the environment
            Rpt.Environment = ConfigurationManager.AppSettings("Reports.ReportsFolder").ToString.Trim

            dAobj = Activator.CreateInstance(ty)
            Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder")
            Dim params As Object()
            Dim AcademicType As String = String.Empty
            Dim AcademicTypesArray() As String = {"Credits-ClockHours", "Credits", "ClockHours"}
            If ResId = 238 Then ' Transcrip report

                'Validate when ShowMultipleEnrollments is true the Type 
                AcademicType = ValidateAcademicType()
                If Not (AcademicTypesArray.Contains(AcademicType)) Then
                    'Popup Window Error message ' The AcademicType should be same for all enrollments
                    DisplayWarningForAcademicType(AcademicType)
                    Return
                End If
                params = {Rpt _
                         , connectionString _
                         , AdvantageSession.UserState.UserId.ToString _
                         , strReportPath
                         }
            Else

                params = {Rpt _
                         , connectionString _
                         , AdvantageSession.UserState.UserId.ToString _
                         , MyAdvAppSettings.AppSettings("GradesFormat", Trim(Request.Params("cmpid").ToString)).ToString.ToLower _
                         , MyAdvAppSettings.AppSettings("GPAMethod").ToString.ToLower _
                         , MyAdvAppSettings.AppSettings("SchoolName").ToString _
                         , MyAdvAppSettings.AppSettings("TrackSapAttendance", Trim(Request.Params("cmpid").ToString)).ToString _
                         , MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString _
                         , MyAdvAppSettings.AppSettings("studentidentifier").ToString _
                         , MyAdvAppSettings.AppSettings("DisplayAttendanceUnitForProgressReportByClass").ToString _
                         , strReportPath
                         }
            End If

            Dim baseReportSize As Integer
            baseReportSize = GetBaseReportSize()
            Try
                getReportAsBytes = mm.Invoke(dAobj, params)
                'Will check that there is data in the report
                If (getReportAsBytes Is Nothing) OrElse (getReportAsBytes.Length() <= baseReportSize) Then
                    'Hide the view successfully message
                    'Show the view successfully message
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewSuccess"), Label).Visible = False
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheckView"), ImageButton).Visible = False
                    'Hide the View Error
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewExportError"), Label).Visible = True
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewExportError"), Label).Text = "No data matches filter selection."
                    Exit Sub
                Else
                    'Show the view successfully message
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewSuccess"), Label).Visible = True
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("ImgbtnCheckView"), ImageButton).Visible = True
                    'Hide the View Error
                    DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("lblViewExportError"), Label).Visible = False
                End If
                ExportReport(Rpt, getReportAsBytes)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                Throw ex
            End Try
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub


    Private Sub GetUserSelections()
        Try
            Dim exportDropDown As RadComboBox = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("RadComboBoxView"), RadComboBox)
            Report.ExportFormat = CInt(exportDropDown.SelectedItem.Value)
            Report.PagingOption = CInt(DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblPageNumberStyle"), RadioButtonList).SelectedValue)
            Report.HeadingOption = 0 'CInt(DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblHeaderStyle"), RadioButtonList).SelectedValue)
            Report.ReportDateOption = CInt(DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblDisplayDate"), RadioButtonList).SelectedValue)
            Dim customSettings As New List(Of ParamItemUserSettingsInfo)
            For Each tabview As ReportTabInfo In Report.ReportTabCollection
                Select Case tabview.ParamSet.SetType
                    Case "Filter"
                        Dim FiltersSettings As New MasterDictionary
                        Dim myParamSet As ParameterSetInfo = tabview.ParamSet
                        Dim myControl As Control = Page.Controls(0).FindControl("ContentMain2").FindControl(tabview.UserControlName)
                        For Each ctrl As Control In myControl.Controls
                            ' Dim myType As Type = ctrl.GetType
                            If TypeOf ctrl Is RadPanelBar Then
                                Dim myPanelBar As RadPanelBar = DirectCast(ctrl, RadPanelBar)

                                For Each item As RadPanelItem In myPanelBar.Items
                                    For Each section As ParameterSectionInfo In myParamSet.ParameterSectionCollection

                                        Dim myPanelItem As RadPanelItem = myPanelBar.FindItemByValue(section.SectionName)
                                        'Dim PanelName As String = myPanelItem.Value

                                        If section.SectionName & " Root" = item.Value Then
                                            For Each detailitem As ParameterDetailItemInfo In section.ParameterDetailItemCollection

                                                Dim itemCtrl As Control = myPanelItem.FindControl(detailitem.ItemName)


                                                If TypeOf itemCtrl Is IParamItemControl Then
                                                    If TypeOf itemCtrl Is IFilterControl Then
                                                        Dim itemUserControl As IFilterControl = DirectCast(itemCtrl, IFilterControl)
                                                        Dim userSelections As DetailDictionary
                                                        userSelections = itemUserControl.GetReturnValues
                                                        If Not userSelections Is Nothing AndAlso userSelections.Count > 0 Then
                                                            FiltersSettings.Add(detailitem.ItemName, userSelections)
                                                        End If
                                                    End If


                                                End If
                                            Next
                                        End If
                                    Next
                                Next
                            End If
                        Next
                        Report.Filters = FiltersSettings
                    Case "Sort"
                        Dim sortSettings As New List(Of SortItemInfo)
                        Dim myParamSet As ParameterSetInfo = tabview.ParamSet
                        Dim myControl As Control = Page.Controls(0).FindControl("ContentMain2").FindControl(tabview.UserControlName)
                        For Each ctrl As Control In myControl.Controls
                            'Dim myType As Type = ctrl.GetType
                            If TypeOf ctrl Is RadPanelBar Then
                                Dim myPanelBar As RadPanelBar = DirectCast(ctrl, RadPanelBar)

                                For Each item As RadPanelItem In myPanelBar.Items
                                    For Each section As ParameterSectionInfo In myParamSet.ParameterSectionCollection

                                        Dim myPanelItem As RadPanelItem = myPanelBar.FindItemByValue(section.SectionName)
                                        'Dim PanelName As String = myPanelItem.Value

                                        If section.SectionName & " Root" = item.Value Then
                                            For Each detailitem As ParameterDetailItemInfo In section.ParameterDetailItemCollection

                                                Dim itemCtrl As Control = myPanelItem.FindControl(detailitem.ItemName)
                                                If TypeOf itemCtrl Is IParamItemControl Then
                                                    If TypeOf itemCtrl Is ISortControl Then
                                                        Dim itemUserControl As ISortControl = DirectCast(itemCtrl, ISortControl)
                                                        Dim userSorts As List(Of SortItemInfo)
                                                        userSorts = itemUserControl.GetControlSettings

                                                        If Not userSorts Is Nothing Then
                                                            sortSettings = userSorts
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        End If
                                    Next
                                Next
                            End If
                        Next
                        Report.Sorts = sortSettings
                    Case "Custom"

                        Dim myParamSet As ParameterSetInfo = tabview.ParamSet
                        Dim myControl As Control = Page.Controls(0).FindControl("ContentMain2").FindControl(tabview.UserControlName)
                        For Each ctrl As Control In myControl.Controls
                            'Dim myType As Type = ctrl.GetType
                            If TypeOf ctrl Is RadPanelBar Then
                                Dim myPanelBar As RadPanelBar = DirectCast(ctrl, RadPanelBar)

                                For Each item As RadPanelItem In myPanelBar.Items
                                    For Each section As ParameterSectionInfo In myParamSet.ParameterSectionCollection

                                        Dim myPanelItem As RadPanelItem = myPanelBar.FindItemByValue(section.SectionName)
                                        'Dim PanelName As String = myPanelItem.Value

                                        If section.SectionName & " Root" = item.Value Then
                                            For Each detailitem As ParameterDetailItemInfo In section.ParameterDetailItemCollection

                                                Dim itemCtrl As Control = myPanelItem.FindControl(detailitem.ItemName)
                                                If TypeOf itemCtrl Is IParamItemControl Then
                                                    If TypeOf itemCtrl Is ICustomControl Then
                                                        Dim itemUserControl As ICustomControl = DirectCast(itemCtrl, ICustomControl)
                                                        Dim userSelections As ParamItemUserSettingsInfo
                                                        userSelections = itemUserControl.GetControlSettings
                                                        If Not userSelections Is Nothing Then
                                                            customSettings.Add(userSelections)
                                                        End If
                                                    ElseIf TypeOf itemCtrl Is ISearchControl Then
                                                        Dim itemUserControl As ISearchControl = DirectCast(itemCtrl, ISearchControl)
                                                        Dim userSelections As ParamItemUserSettingsInfo
                                                        userSelections = itemUserControl.GetControlSettings
                                                        If Not userSelections Is Nothing Then
                                                            customSettings.Add(userSelections)
                                                        End If

                                                    End If
                                                End If
                                            Next
                                        End If
                                    Next
                                Next
                            End If
                        Next

                    Case Else
                        Throw New Exception("Unknown Type")
                End Select
            Next
            Report.CustomOptions = customSettings
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub

    Private Function ValidateAcademicType() As String
        Dim AcademicType As String = String.Empty
        Dim StuEnrollIdList As New StringBuilder()
        Dim FiltersSettings As New MasterDictionary
        FiltersSettings = Report.Filters
        For Each item As DictionaryEntry In FiltersSettings
            ' Return item.Name 
            If (item.Key = "StudentEnrollSearch") Then
                Dim userSelections As DetailDictionary
                userSelections = DirectCast(item.Value, DetailDictionary)
                For Each item1 As DictionaryEntry In userSelections
                    If (item1.Key = "StudentEnrollSearch") Then
                        Dim ParamValue As ParamValueDictionary
                        ParamValue = DirectCast(item1.Value, ParamValueDictionary)
                        If ParamValue.Name = "StudentEnrollmentId" Then
                            For Each item3 As ModDictionary In ParamValue.Values
                                For Each item4 As DictionaryEntry In item3
                                    If (item4.Key = "StudentEnrollSearch") Then
                                        For Each StuEnrollId As Guid In item4.Value
                                            StuEnrollIdList.Append(StuEnrollId.ToString() + ",")
                                        Next
                                        StuEnrollIdList.Remove(StuEnrollIdList.Length - 1, 1)
                                        AcademicType = GetAcademicType(StuEnrollIdList.ToString())
                                    End If
                                Next
                            Next
                        End If
                    End If
                Next
            End If
        Next
        Return AcademicType
    End Function
    Private Function GetAcademicType(ByVal StuEnrollIdList As String) As String
        Dim AcademicType As String = ""
        Dim rptUtils As New ReportCommonUtilsFacade

        AcademicType = rptUtils.GetAcademicType(StuEnrollIdList)

        Return AcademicType
    End Function
    Protected Sub BindDataSource(ByVal intResourceId As Integer)
        'This will assign the SQL to the datasource and bind that datasource to the radgrid
        SqlDataSource1.ConnectionString = connectionString
        SqlDataSource1.SelectCommand = "SELECT UserSettingId, PrefName, PrefDescrip FROM syReportUserSettings WHERE ResourceId = '" & intResourceId & "' " _
        & "ORDER BY ModDate DESC"
        SqlDataSource1.DataBind()
        'We need this because the NeedDataSource is not firing on rebind when save setting button is clicked from within View & Save
        DirectCast(RadRptPanelBarRadGrid.FindItemByValue("PrefGrid").FindControl("RadGridReportSettings"), RadGrid).DataSource = Nothing
        DirectCast(RadRptPanelBarRadGrid.FindItemByValue("PrefGrid").FindControl("RadGridReportSettings"), RadGrid).DataSourceID = "SqlDataSource1"
    End Sub

    Private Sub SetPostBackForTabs()
        For Each tab As RadTab In RadRptTabStrip.GetAllTabs
            If Not tab.Text = "Run Report" Then
                tab.PostBack = False
            End If
        Next
    End Sub

    Private Sub ShowSelectionSummary()

        Dim ctrlPlcOption As PlaceHolder = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("PanelOptionWarning").FindControl("PlcOptionSummary"), PlaceHolder)
        Dim ctrlPlcWarning As PlaceHolder = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("PanelOptionWarning").FindControl("PlcOptionWarning"), PlaceHolder)
        Dim objReportInfo As Fame.Advantage.Reporting.Info.ReportInfo = Report
        Dim filterCount As Integer
        Try
            Dim filterTree As New RadTreeView
            filterTree.ID = "QuickFilterMode" 'UserSelections.ItemName & "_DisplaySelected"
            Dim filterParentNode As New RadTreeNode
            filterParentNode.Text = "Current Filter Mode" 'UserSelections.FriendlyName & " Filters"
            filterParentNode.Value = 1 'CStr(UserSelections.DetailId)
            filterParentNode.CssClass = "TreeParentNode"
            Dim filterChildNode As New RadTreeNode
            If CBool(Session("QuickFilterMode")) = False Then
                filterChildNode.Text = "Advanced Filters On"
                filterParentNode.Nodes.Add(filterChildNode)
            Else
                filterChildNode.Text = "Quick Filters On"
                filterParentNode.Nodes.Add(filterChildNode)
            End If
            filterTree.Nodes.Add(filterParentNode)
            Dim divOpenx As New Literal
            Dim divclosex As New Literal
            divOpenx.Text = "<div>"
            divclosex.Text = "</div>"
            If objReportInfo.ShowFilterMode = True Then
                ctrlPlcOption.Controls.Add(divOpenx)
                ctrlPlcOption.Controls.Add(filterTree)
                ctrlPlcOption.Controls.Add(divclosex)
            End If
            For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                Dim myParamSet As ParameterSetInfo = tabview.ParamSet
                Dim myControl As Control = Page.Controls(0).FindControl("ContentMain2").FindControl(tabview.UserControlName)
                For Each ctrl As Control In myControl.Controls
                    'Dim myType As Type = ctrl.GetType
                    If TypeOf ctrl Is RadPanelBar Then
                        Dim myPanelBar As RadPanelBar = DirectCast(ctrl, RadPanelBar)

                        For Each item As RadPanelItem In myPanelBar.Items
                            For Each section As ParameterSectionInfo In myParamSet.ParameterSectionCollection

                                Dim myPanelItem As RadPanelItem = myPanelBar.FindItemByValue(section.SectionName)
                                'Dim PanelName As String = myPanelItem.Value

                                If section.SectionName & " Root" = item.Value Then
                                    For Each detailitem As ParameterDetailItemInfo In section.ParameterDetailItemCollection

                                        Dim itemCtrl As Control = myPanelItem.FindControl(detailitem.ItemName)
                                        If TypeOf itemCtrl Is IParamItemControl Then
                                            Dim displayTree As New RadTreeView
                                            If TypeOf itemCtrl Is IDateControl Then
                                                Dim itemUserControl As IDateControl = DirectCast(itemCtrl, IDateControl)
                                                Dim userSelections As ParamItemUserSettingsInfo
                                                userSelections = itemUserControl.GetControlSettings
                                                If Not userSelections Is Nothing Then

                                                    displayTree.ID = userSelections.ItemName & "_DisplaySelected"
                                                    'Dim myDateControl As IDateControl = DirectCast(ItemCtrl, IDateControl)
                                                    'Dim ctrlMode As IDateControl.ModeType = myDateControl.Mode
                                                    Dim parentNode As New RadTreeNode
                                                    parentNode.Value = CStr(userSelections.DetailId)
                                                    parentNode.CssClass = "TreeParentNode"
                                                    parentNode.Text = userSelections.FriendlyName & " Filters"
                                                    For Each setting As ControlSettingInfo In userSelections.ControlSettingsCollection
                                                        Dim childNode As New RadTreeNode
                                                        For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                                            Dim strModifier As String = String.Empty
                                                            Dim strChildTxt As String
                                                            Dim strDate As String = String.Empty

                                                            If IsDate(itemValue.DisplayText) = True Then
                                                                strDate = itemValue.DisplayText
                                                            Else
                                                                If itemValue.DisplayText = "Greater Than Or Equal To" Then
                                                                    strModifier = " Greater Than Or Equal To "
                                                                ElseIf itemValue.DisplayText = "Less Than Or Equal To" Then
                                                                    strModifier = " Less Than Or Equal To "
                                                                ElseIf itemValue.DisplayText = "Greater Than" Then
                                                                    strModifier = " Greater Than "
                                                                ElseIf itemValue.DisplayText = "Less Than" Then
                                                                    strModifier = " Less Than "
                                                                End If
                                                            End If

                                                            Dim strParsedDate As String 'strMonth, strDay, strYear,
                                                            If strDate <> String.Empty Then
                                                                'Convert the date to a more human readable format
                                                                Dim myDate = CDate(strDate)
                                                                strParsedDate = myDate.ToString("d")

                                                                strChildTxt = strModifier & strParsedDate
                                                                'strDate = String.Empty

                                                                childNode.Text = strChildTxt
                                                                childNode.Value = strChildTxt

                                                                parentNode.Nodes.Add(childNode)
                                                            End If
                                                        Next

                                                    Next
                                                    If parentNode.Nodes.Count > 0 Then
                                                        displayTree.Nodes.Add(parentNode)
                                                        filterCount += 1
                                                    End If
                                                End If
                                            ElseIf TypeOf itemCtrl Is ISortControl Then
                                                Dim itemUserControl As ISortControl = DirectCast(itemCtrl, ISortControl)
                                                Dim userSorts As List(Of SortItemInfo)
                                                userSorts = itemUserControl.GetControlSettings
                                                displayTree.ID = "Sorts"
                                                Dim parentNode As New RadTreeNode
                                                parentNode.Text = "Sort Order"
                                                parentNode.CssClass = "TreeParentNode"
                                                If Not userSorts Is Nothing Then
                                                    For Each sort As SortItemInfo In userSorts
                                                        Dim childNode As New RadTreeNode
                                                        Dim direction As String
                                                        If sort.IsDecending = True Then
                                                            direction = "Descending"
                                                        Else
                                                            direction = "Ascending"
                                                        End If
                                                        childNode.Text = sort.DisplayName & " - " & direction

                                                        parentNode.Nodes.Add(childNode)
                                                    Next
                                                    displayTree.Nodes.Add(parentNode)
                                                End If
                                            ElseIf TypeOf itemCtrl Is ICustomControl Then
                                                Dim itemUserControl As ICustomControl = DirectCast(itemCtrl, ICustomControl)
                                                If Not itemUserControl.GetDisplayData Is Nothing Then

                                                    displayTree = DirectCast(itemUserControl.GetDisplayData, RadTreeView)
                                                End If
                                            ElseIf TypeOf itemCtrl Is ISearchControl Then
                                                Dim itemUserControl As ISearchControl = DirectCast(itemCtrl, ISearchControl)
                                                Dim userSelections As ParamItemUserSettingsInfo
                                                userSelections = itemUserControl.GetControlSettings

                                                If Not userSelections Is Nothing Then
                                                    For Each setting As ControlSettingInfo In userSelections.ControlSettingsCollection
                                                        displayTree.ID = userSelections.ItemName & "_DisplaySelected"
                                                        Dim parentNode As New RadTreeNode
                                                        parentNode.Text = userSelections.FriendlyName & " Filters"
                                                        parentNode.Value = CStr(userSelections.DetailId)
                                                        parentNode.CssClass = "TreeParentNode"

                                                        For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                                            Dim childNode As New RadTreeNode

                                                            childNode.Text = itemValue.DisplayText
                                                            childNode.Value = itemValue.KeyData
                                                            parentNode.Nodes.Add(childNode)
                                                        Next
                                                        If parentNode.Nodes.Count > 0 Then
                                                            displayTree.Nodes.Add(parentNode)
                                                        End If
                                                        filterCount += 1
                                                    Next
                                                End If
                                            ElseIf TypeOf itemCtrl Is IDataListControl Then
                                                Dim itemUserControl As IDataListControl = DirectCast(itemCtrl, IDataListControl)
                                                Dim userSelections As ParamItemUserSettingsInfo
                                                userSelections = itemUserControl.GetControlSettings

                                                If Not userSelections Is Nothing Then
                                                    For Each setting As ControlSettingInfo In userSelections.ControlSettingsCollection
                                                        If setting.ControlName = "RadListBox2" And setting.ControlValueCollection.Count > 0 Then
                                                            displayTree.ID = userSelections.ItemName & "_DisplaySelected"
                                                            Dim parentNode As New RadTreeNode
                                                            parentNode.Text = userSelections.FriendlyName & " Filters"
                                                            parentNode.Value = CStr(userSelections.DetailId)
                                                            parentNode.CssClass = "TreeParentNode"

                                                            For Each itemValue As ControlValueInfo In setting.ControlValueCollection
                                                                Dim childNode As New RadTreeNode

                                                                childNode.Text = itemValue.DisplayText
                                                                childNode.Value = itemValue.KeyData
                                                                parentNode.Nodes.Add(childNode)
                                                            Next
                                                            displayTree.Nodes.Add(parentNode)
                                                            filterCount += 1
                                                        End If
                                                    Next
                                                End If
                                            Else
                                                Throw New Exception("Unknown Control Type")
                                            End If
                                            If displayTree.Nodes.Count > 0 Then
                                                Dim openDiv As New Literal
                                                Dim closeDiv As New Literal
                                                openDiv.Text = "<div>"
                                                closeDiv.Text = "</div>"

                                                ctrlPlcOption.Controls.Add(openDiv)
                                                ctrlPlcOption.Controls.Add(displayTree)
                                                ctrlPlcOption.Controls.Add(closeDiv)
                                            End If
                                        End If
                                    Next
                                End If
                            Next
                        Next
                    End If
                Next
            Next

            'This will flag the user when the filters selected is less that the requirement.
            Const filterRequirement As Integer = 3
            Dim strWarning As String
            If filterCount < filterRequirement AndAlso CBool(Session("QuickFilterMode")) = False AndAlso CBool(Session("SearchHasValues")) = False Then
                strWarning = "You have less than " & filterRequirement &
                " filters selected for this report. This can potentially cause " _
                & "performance issues on your Advantage system. Please consider " _
                & "adding more filter options for this report."
            Else
                strWarning = ""
            End If

            Dim litWarning As New Literal

            litWarning.Text = strWarning
            If objReportInfo.ShowPerformanceMsg = True Then
                ctrlPlcWarning.Controls.Add(litWarning)
            End If


            'This is for the Report Display Options Tree. These are static.
            If radPanelReportDisplayOption.Visible = True Then   'do not show w
                Dim displaytree2 As New RadTreeView
                Dim rptDisplayOptionsNode As New RadTreeNode
                rptDisplayOptionsNode.Text = "Report Display Options"
                rptDisplayOptionsNode.CssClass = "TreeParentNode"

                Dim pageNode As New RadTreeNode
                'Dim HeadingNode As New RadTreeNode
                Dim reportDateNode As New RadTreeNode
                pageNode.Text = "Show " & DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblPageNumberStyle"), RadioButtonList).SelectedItem.Text
                'HeadingNode.Text = "Show " & DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblHeaderStyle"), RadioButtonList).SelectedItem.Text
                reportDateNode.Text = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblDisplayDate"), RadioButtonList).SelectedItem.Text
                rptDisplayOptionsNode.Nodes.Add(pageNode)
                'RptDisplayOptionsNode.Nodes.Add(HeadingNode)
                rptDisplayOptionsNode.Nodes.Add(reportDateNode)
                displaytree2.Nodes.Add(rptDisplayOptionsNode)

                Dim divOpen As New Literal
                Dim divclose As New Literal
                divOpen.Text = "<div>"
                divclose.Text = "</div>"

                ctrlPlcOption.Controls.Add(divOpen)
                ctrlPlcOption.Controls.Add(displaytree2)
                ctrlPlcOption.Controls.Add(divclose)
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Private Function GetControlStates() As String

        Dim objReportInfo As Fame.Advantage.Reporting.Info.ReportInfo = Report
        Dim userReportSettings As New ReportUserSettingsInfo
        Try
            With userReportSettings
                .UserId = New Guid(CStr(AdvantageSession.UserState.UserId.ToString))
                .ReportId = objReportInfo.ReportId
                .ResourceId = objReportInfo.ResourceId
                .PrefName = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("txtSettingName"), TextBox).Text
                .PrefDescription = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("txtSettingDescription"), TextBox).Text
                .PagingOption = CType(DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblPageNumberStyle"), RadioButtonList).SelectedValue, ReportUserSettingsInfo.PagingStyle)
                .HeadingOption = 0 'CInt(DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblHeaderStyle"), RadioButtonList).SelectedValue)
                .ReportDateOption = CType(DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportOptions").FindControl("rblDisplayDate"), RadioButtonList).SelectedValue, ReportUserSettingsInfo.ReportDateStyle)

            End With

            Dim filterSettings As New List(Of ParamItemUserSettingsInfo)
            Dim sortSettings As New List(Of SortItemInfo)
            Dim customSettings As New List(Of ParamItemUserSettingsInfo)

            For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                Dim myParamSet As ParameterSetInfo = tabview.ParamSet
                Dim myControl As Control = Page.Controls(0).FindControl("ContentMain2").FindControl(tabview.UserControlName)
                For Each ctrl As Control In myControl.Controls
                    'Dim myType As Type = ctrl.GetType
                    If TypeOf ctrl Is RadPanelBar Then
                        Dim myPanelBar As RadPanelBar = DirectCast(ctrl, RadPanelBar)

                        For Each item As RadPanelItem In myPanelBar.Items
                            For Each section As ParameterSectionInfo In myParamSet.ParameterSectionCollection

                                Dim myPanelItem As RadPanelItem = myPanelBar.FindItemByValue(section.SectionName)
                                'Dim PanelName As String = myPanelItem.Value

                                If section.SectionName & " Root" = item.Value Then
                                    For Each detailitem As ParameterDetailItemInfo In section.ParameterDetailItemCollection

                                        Dim itemCtrl As Control = myPanelItem.FindControl(detailitem.ItemName)
                                        If TypeOf itemCtrl Is IParamItemControl Then
                                            If TypeOf itemCtrl Is IFilterControl Then
                                                Dim itemUserControl As IFilterControl = DirectCast(itemCtrl, IFilterControl)
                                                Dim userSelections As ParamItemUserSettingsInfo
                                                userSelections = itemUserControl.GetControlSettings
                                                If Not userSelections Is Nothing Then
                                                    filterSettings.Add(userSelections)
                                                End If
                                            ElseIf TypeOf itemCtrl Is ISortControl Then
                                                Dim itemUserControl As ISortControl = DirectCast(itemCtrl, ISortControl)
                                                Dim userSorts As List(Of SortItemInfo)
                                                userSorts = itemUserControl.GetControlSettings

                                                If Not userSorts Is Nothing Then
                                                    sortSettings = userSorts
                                                End If
                                            ElseIf TypeOf itemCtrl Is ICustomControl Then
                                                Dim itemUserControl As ICustomControl = DirectCast(itemCtrl, ICustomControl)
                                                Dim userSelections As ParamItemUserSettingsInfo
                                                userSelections = itemUserControl.GetControlSettings
                                                If Not itemUserControl.GetControlSettings Is Nothing Then
                                                    customSettings.Add(userSelections)
                                                End If
                                            Else
                                                Throw New Exception("Unknown Control Type")
                                            End If
                                        End If
                                    Next
                                End If
                            Next
                        Next
                    End If
                Next
            Next
            userReportSettings.FilterSettings = filterSettings
            userReportSettings.SortSettings = sortSettings
            userReportSettings.CustomOptions = customSettings


            'Serialize 
            Dim stringWriter As New StringWriter()
            Dim serializer As New XmlSerializer(GetType(ReportUserSettingsInfo))
            Dim strXml As String
            serializer.Serialize(stringWriter, userReportSettings)
            strXml = stringWriter.ToString()

            Return strXml

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try

    End Function


    Private Function GetReport() As Fame.Advantage.Reporting.Info.ReportInfo
        Dim objReportInfo As Fame.Advantage.Reporting.Info.ReportInfo

        Try
            objReportInfo = CType(Session(ResId.ToString), Fame.Advantage.Reporting.Info.ReportInfo)
            If objReportInfo Is Nothing Then
                objReportInfo = GetReportByResourceID(ResId)
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
        Return objReportInfo
    End Function

    Private Function GetReportByResourceID(ByVal ResID As Integer) As Fame.Advantage.Reporting.Info.ReportInfo
        Dim db As AdvantageDataContext = New AdvantageDataContext(connectionString)
        Try
            Dim q = (From Report In db.syReports
                     Where Report.ResourceId.Equals(ResID)
                     Select New Fame.Advantage.Reporting.Info.ReportInfo With
                     {.ReportId = Report.ReportId,
                     .ResourceId = Report.ResourceId,
                     .ReportName = Report.ReportName,
                     .FriendlyName = ((From R In db.syResources Where R.ResourceID.Equals(Report.ResourceId) Select R.Resource).Single),
                     .ReportDescription = Report.ReportDescription,
                     .RDLName = Report.RDLName,
                     .AssemblyFilePath = Report.AssemblyFilePath,
                     .ReportClass = Report.ReportClass,
                     .CreationMethod = Report.CreationMethod,
                     .WebServiceUrl = Report.WebServiceUrl,
                     .RecordLimit = Report.RecordLimit,
                      .AllowedExportFormat = Report.AllowedExportTypes,
                      .ReportTabLayout = Report.ReportTabLayout,
                      .ShowFilterMode = Report.ShowFilterMode,
                      .ShowPerformanceMsg = Report.ShowPerformanceMsg,
                    .ReportTabCollection =
                     (From ReportTab In db.syReportTabs
                      Where ReportTab.ReportId = Report.ReportId
                      Select New ReportTabInfo With
                      {.ReportId = ReportTab.ReportId,
                      .SetId = ReportTab.SetId,
                      .PageViewId = ReportTab.PageViewId,
                      .UserControlName = ReportTab.UserControlName,
                      .ControllerClass = ReportTab.ControllerClass,
                      .DisplayName = ReportTab.DisplayName,
                      .ParameterSetLookUp = ReportTab.ParameterSetLookUp,
                      .TabName = ReportTab.TabName})}).Single

            Return q
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Debug.WriteLine("GetReportByResourceID Exception Message: {0} ", ex.Message)
            Throw
        End Try
    End Function

    Public Function GetParamSet(ByVal SetId As Integer) As ParameterSetInfo

        Dim pSet As ParameterSetInfo
        Try
            Dim da As New ParamDA(connectionString)
            pSet = da.GetParamSetBySetId(SetId)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Debug.WriteLine("GetParamSet Exception Message: {0} ", ex.Message)
            Throw
        End Try
        Return pSet
    End Function

    Private Sub AddReportControlsToTabs()
        Dim objReportInfo As Fame.Advantage.Reporting.Info.ReportInfo
        objReportInfo = Report
        Try

            If objReportInfo.ReportTabLayout = Fame.Advantage.Reporting.Info.ReportInfo.LayoutStyle.FullLayout Then
                objReportInfo.StartingTabIndex = 0
                Dim FilterTab As RadTab = RadRptTabStrip.FindTabByText("Filtering")
                Dim GroupTab As RadTab = RadRptTabStrip.FindTabByText("Grouping")
                Dim SortTab As RadTab = RadRptTabStrip.FindTabByText("Sorting")
                Dim CustomTab As RadTab = RadRptTabStrip.FindTabByText("Options")
                FilterTab.Visible = True
                GroupTab.Visible = True
                SortTab.Visible = True
                CustomTab.Visible = True
                If Not Page.IsPostBack Then
                    FilterTab.Selected = True
                    RadRptTabStrip.SelectedIndex = objReportInfo.StartingTabIndex
                    RadRptMultiPage.SelectedIndex = objReportInfo.StartingTabIndex
                End If
            ElseIf objReportInfo.ReportTabLayout = Fame.Advantage.Reporting.Info.ReportInfo.LayoutStyle.OptionsOnly Then
                objReportInfo.StartingTabIndex = 3
                Dim FilterTab As RadTab = RadRptTabStrip.FindTabByText("Filtering")
                Dim GroupTab As RadTab = RadRptTabStrip.FindTabByText("Grouping")
                Dim SortTab As RadTab = RadRptTabStrip.FindTabByText("Sorting")
                Dim CustomTab As RadTab = RadRptTabStrip.FindTabByText("Options")
                FilterTab.Visible = False
                GroupTab.Visible = False
                SortTab.Visible = False
                CustomTab.Visible = True
                If Not Page.IsPostBack Then
                    CustomTab.Selected = True
                    RadRptTabStrip.SelectedIndex = objReportInfo.StartingTabIndex
                    RadRptMultiPage.SelectedIndex = objReportInfo.StartingTabIndex
                End If
            Else
                Throw New Exception("Unkown Report Layout Type")
            End If

            For Each tabview As ReportTabInfo In objReportInfo.ReportTabCollection
                'Add ParamSet to ReportTabInfo
                tabview.ParamSet = GetParamSet(tabview.SetId)

                Dim userctrl As UserControl
                Dim myParamSetControl As IParamSetControl
                userctrl = CType(LoadControl("~/UserControls/ParamControls/" & tabview.ControllerClass), UserControl)
                userctrl.ID = tabview.UserControlName


                myParamSetControl = DirectCast(userctrl, IParamSetControl)
                myParamSetControl.DisplayName = tabview.DisplayName
                myParamSetControl.ParameterSetLookup = tabview.ParameterSetLookUp
                myParamSetControl.SetId = tabview.SetId
                myParamSetControl.SqlConn = connectionString
                'Add ParamSet to Control
                myParamSetControl.ParamSet = tabview.ParamSet
                'Add the User Control to the Proper PageView

                RadRptMultiPage.FindPageViewByID(tabview.PageViewId).Controls.Add(myParamSetControl)

                'Enable the Tab after User Control has been added and change Tooltip
                Dim objTab As RadTab = RadRptTabStrip.FindTabByText(tabview.TabName)
                objTab.Enabled = True

                Select Case tabview.TabName
                    Case "Filtering"
                        objTab.ToolTip = "Select filters for the report"
                    Case "Grouping"
                        objTab.ToolTip = "Select grouping for the report"
                    Case "Sorting"
                        objTab.ToolTip = "Select sort order for the report"
                    Case "Options"
                        objTab.ToolTip = "Select specific options for the report"
                    Case Else

                End Select
            Next
            Session(ResId.ToString) = objReportInfo
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Debug.WriteLine("AddReportControlsToTabs Exception Message: {0} ", ex.Message)
            Throw
        End Try
    End Sub

    Private Sub ExportReport(ByVal RptInfo As Fame.Advantage.Reporting.Info.ReportInfo, ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case RptInfo.ExportFormat
            Case Fame.Advantage.Reporting.Info.ReportInfo.ExportType.PDF
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case Fame.Advantage.Reporting.Info.ReportInfo.ExportType.Excel
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
            Case Fame.Advantage.Reporting.Info.ReportInfo.ExportType.CSV
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Fame.Advantage.Reporting.Info.ReportInfo.ExportType.Word
                strExtension = "doc"
                strMimeType = "application/vnd.ms-word"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select

        HttpContext.Current.Response.Clear()
        Response.ContentType = "application/octet-stream"
        If strExtension = "csv" Then
            HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=MyReport.") + strExtension)
        ElseIf strExtension = "xls" Then
            HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=MyReport.") + strExtension)
        ElseIf strExtension = "doc" Then
            HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=MyReport.") + strExtension)
        ElseIf strExtension = "pdf" Then
            HttpContext.Current.Response.AddHeader("content-disposition", ("attachment; filename=MyReport.") + strExtension)
        End If

        SetCookies(RptInfo)

        Dim stream As New MemoryStream(getReportAsBytes)
        stream.CopyTo(HttpContext.Current.Response.OutputStream)
    End Sub

    Private Sub SetCookies(ByVal RptInfo As Fame.Advantage.Reporting.Info.ReportInfo)

        'If NACCAS report, set cookie LastReportDownloaded to the ResourceId of report type Resource Id
        If (RptInfo.ResourceId = 846) Then
            For Each item As ParamItemUserSettingsInfo In RptInfo.CustomOptions
                For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection
                    For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                        Dim strKeyName As String = ""
                        Dim strKeyValue As String = ""
                        Dim strDisplayText As String = ""
                        Select Case drillItem.ControlName.Trim.ToString.ToLower
                            Case Is = "radcomboreportname"
                                strKeyName = "radcomboreporttype"
                                strKeyValue = controlValueItem.KeyData.ToString.Trim.ToLower
                                HttpContext.Current.Response.SetCookie(New HttpCookie("LastReportDownloaded", strKeyValue))
                        End Select
                    Next
                Next
            Next
        End If

    End Sub

    Private Sub DisplayWarningForIPEDSFallCompletionCIPAndByLvl()
        Dim dsProgMissingCip As DataSet
        'Dim IPEDSFacade As New IPEDSFacade
        Dim str As String
        Dim sCampusid As String = ""
        Dim sProgid As String = ""
        Dim iRow As Int16

        For Each tabview As ReportTabInfo In Report.ReportTabCollection
            Select Case tabview.ParamSet.SetType
                Case "Custom"
                    Dim customSettings As New List(Of ParamItemUserSettingsInfo)
                    Dim myParamSet As ParameterSetInfo = tabview.ParamSet
                    Dim myControl As Control = Page.Controls(0).FindControl("ContentMain2").FindControl(tabview.UserControlName)
                    For Each ctrl As Control In myControl.Controls
                        'Dim myType As Type = ctrl.GetType
                        If TypeOf ctrl Is RadPanelBar Then
                            Dim myPanelBar As RadPanelBar = DirectCast(ctrl, RadPanelBar)

                            For Each item As RadPanelItem In myPanelBar.Items
                                For Each section As ParameterSectionInfo In myParamSet.ParameterSectionCollection

                                    Dim myPanelItem As RadPanelItem = myPanelBar.FindItemByValue(section.SectionName)
                                    'Dim PanelName As String = myPanelItem.Value

                                    If section.SectionName & " Root" = item.Value Then
                                        For Each detailitem As ParameterDetailItemInfo In section.ParameterDetailItemCollection

                                            Dim itemCtrl As Control = myPanelItem.FindControl(detailitem.ItemName)
                                            If TypeOf itemCtrl Is IParamItemControl Then
                                                If TypeOf itemCtrl Is ICustomControl Then
                                                    Dim itemUserControl As ICustomControl = DirectCast(itemCtrl, ICustomControl)
                                                    Dim userSelections As ParamItemUserSettingsInfo
                                                    userSelections = itemUserControl.GetControlSettings
                                                    If Not userSelections Is Nothing Then
                                                        customSettings.Add(userSelections)
                                                    End If
                                                End If
                                            End If
                                        Next
                                    End If
                                Next
                            Next
                        End If
                    Next
                    For Each item As ParamItemUserSettingsInfo In customSettings   'Report.CustomOptions
                        For Each drillItem As ControlSettingInfo In item.ControlSettingsCollection
                            For Each controlValueItem As ControlValueInfo In drillItem.ControlValueCollection
                                Select Case drillItem.ControlName.Trim.ToString.ToLower
                                    Case Is = "radcombocampus"
                                        sCampusid = controlValueItem.KeyData.ToString.Trim.ToLower.ToString
                                    Case Is = "radlistboxprogram2"
                                        sProgid += controlValueItem.KeyData.ToString.Trim.ToLower & ","
                                End Select
                            Next
                        Next
                    Next
            End Select
        Next
        If Not sProgid.Trim = "" Then
            If InStrRev(sProgid, ",") >= 1 Then
                sProgid = Mid(sProgid.ToString.Trim, 1, (InStrRev(sProgid, ",") - 1))
            End If
        End If
        Dim bCIP As Boolean
        If ResId = 784 Then
            bCIP = True
        Else
            bCIP = False
        End If
        dsProgMissingCip = IPEDSFacade.GetProgramMisingCIPCodebyProgAndCampus(sCampusid, sProgid, bCIP)
        If dsProgMissingCip.Tables(0).Rows.Count > 0 Then
            If bCIP Then
                str = "CIP CODE missing in the following Program(s) " & "<br />" & "<br />"
            Else
                str = "Credential Level missing / not mapped to Agency IPEDS Credential Level in the following Program(s) " & "<br />" & "<br />"
            End If
            For iRow = 0 To dsProgMissingCip.Tables(0).Rows.Count - 1
                Dim drRow As DataRow = dsProgMissingCip.Tables(0).Rows(iRow)
                str += drRow("ProgDescrip") & ", "
            Next
            If InStrRev(str, ",") >= 1 Then
                str = Mid(str.ToString.Trim, 1, (InStrRev(str, ",") - 1))
            End If
            If bCIP Then
                str += "<br />" & "<br />" & "Please fix CIP Code before running this report!!!"
            Else
                str += "<br />" & "<br />" & "Please fix Credential Level before running this report, report will not include missing Credential level program(s)"
            End If
            Dim ctrlPlcWarning As PlaceHolder = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("PanelOptionWarning").FindControl("PlcOptionWarning"), PlaceHolder)
            Dim ctrlLitWarning As New Literal
            ctrlLitWarning.Text = str
            'Add the newly created literal controls to the placeholders
            ctrlPlcWarning.Controls.Add(ctrlLitWarning)
        End If

    End Sub

    Private Sub DisplayWarningForAcademicType(ByVal pMessage As String)
        Dim ctrlPlcWarning As PlaceHolder = DirectCast(RadPanelBar_ViewSave.FindItemByValue("ReportViewSave").FindControl("PanelOptionWarning").FindControl("PlcOptionWarning"), PlaceHolder)
        Dim ctrlLitWarning As New Literal
        ctrlPlcWarning.Controls.Clear()
        ctrlLitWarning.Text = "<br />" & "<br />" & pMessage & "<br />"
        ctrlPlcWarning.Controls.Add(ctrlLitWarning)

    End Sub

    Private Function GetBaseReportSize() As Integer
        'Set base size of the report based on report type
        Dim baseSize = 0
        Select Case Report.ReportName
            Case "GradCompletionReport"
                baseSize = (CInt(BaseReportSize.GradCompletionReport))
            Case Else
                baseSize = 0
        End Select
        Return baseSize
    End Function
#End Region

End Class

