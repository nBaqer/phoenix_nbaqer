﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="EmployeeSearch.aspx.vb" Inherits="EmployeeSearch" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">


    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table cellspacing="0" class="listframetop2" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframenofilterblue">Search Employee
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleft" style="height: expression(document.body.clientHeight - 194 + 'px')">
                            <table cellspacing="0" cellpadding="2" align="center" width="100%">
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblLastName" runat="Server" CssClass="label">Last Name</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:TextBox ID="txtLastName" runat="Server" CssClass="textbox"></asp:TextBox>
                                        <asp:TextBox ID="txtEmployeeId" runat="server" Visible="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblFirstName" runat="Server" CssClass="label">First Name</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:TextBox ID="txtFirstName" runat="Server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblSSN" runat="Server" CssClass="label">SSN</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:TextBox ID="txtSSN" runat="Server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblStatus" runat="Server" CssClass="label">Status</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlStatusId" runat="Server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch"></td>
                                    <td class="employersearch2" align="left">
                                        <table cellspacing="0" cellpadding="2" align="left" width="100%">
                                            <tr>
                                                <td width="50%" align="left">
                                                    <%-- <asp:Button ID="btnSearch" runat="Server" Text="Search" CssClass="button"></asp:Button>--%>
                                                    <telerik:RadButton ID="btnSearch" runat="server" Text="Search"></telerik:RadButton>
                                                    &nbsp;
                                                <telerik:RadButton ID="btnReset" runat="server" Text="Reset "></telerik:RadButton>
                                                </td>
                                                <td width="50%" align="left">
                                                    <%--<input id="Reset1" type="reset" value="Reset" name="Reset1" runat="server" class="button" />--%>
                                               
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>


        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgrdTransactionSearch" Width="100%" CellPadding="0" BorderWidth="1px"
                                            BorderStyle="Solid" BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True"
                                            HeaderStyle-Wrap="true" EditItemStyle-Wrap="false" GridLines="Horizontal" runat="server" DataKeyField="EmpId">
                                            <EditItemStyle Wrap="False"></EditItemStyle>
                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                            <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Last Name">
                                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="Linkbutton1" runat="server" Text='<%# Container.DataItem("LastName") %>'
                                                            CssClass="label" CausesValidation="False" CommandName="StudentSearch" CommandArgument='<%# Container.DataItem("EmpId") %>'> </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="First Name">
                                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDgFirstName" Text='<%# Container.DataItem("FirstName") %>' CssClass="label"
                                                            runat="server"> </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="SSN">
                                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="label1" Text='<%# Container.DataItem("SSN") %>' CssClass="label" runat="server"> </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                            <asp:TextBox ID="txtStudentDocId" runat="server" Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtEmpId" runat="server" Visible="false"></asp:TextBox>
                            <!-- end content table-->

                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadNotification runat="server" ID="RadNotification1"
                Text="This is a test" ShowCloseButton="true"
                Width="400px" Height="125px"
                TitleIcon=""
                Position="Center" Title="Message"
                EnableRoundedCorners="true"
                EnableShadow="true"
                Animation="Fade"
                AnimationDuration="1000"
                Style="padding-left: 120px; padding-top: 5px; word-spacing: 2pt;">
            </telerik:RadNotification>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

