﻿Imports Fame.common
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Web.WebPages

Partial Class Roles
    Inherits BasePage
    Dim pobj As New UserPagePermissionInfo
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblFullName As System.Web.UI.WebControls.Label
    Protected WithEvents txtFullName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEmail As System.Web.UI.WebControls.Label
    Protected WithEvents txtEmail As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblUserName As System.Web.UI.WebControls.Label
    Protected WithEvents txtUserName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblPassword As System.Web.UI.WebControls.Label
    Protected WithEvents txtPassword As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblConfirmPassword As System.Web.UI.WebControls.Label
    Protected WithEvents txtConfirmPassword As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblCampusId As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCampusId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblCampGrpId As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCampGrpId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents rfvCamGrp As System.Web.UI.WebControls.RequiredFieldValidator

    Dim userid As String
    Dim username As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
        Dim resourceId As Integer
        Dim campusId As String
        Dim objCommon As New CommonUtilities

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId

        '   disable History button at all time
        'Header1.EnableHistoryButton(False)
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        userid = advantageUserState.UserId.ToString
        username = advantageUserState.UserName


        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        'Check if this page still exists in the menu while switching campus
       If Me.Master.IsSwitchedCampus = True Then
            If pobj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            'Header1.EnableHistoryButton(False)
            ' load the form with its default values
            ResetForm()
            ' display the roles defined in the system on the left pane
            BindRoles()
            ' Bind the task to the form
            Dim RoleId As String = Request.Params("roleid")
            If Not RoleId Is Nothing Then
                BindRole(RoleId)
            End If
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pobj)
        Else
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pobj)
        End If
        headerTitle.Text = Header.Title
    End Sub

    ''' <summary>
    ''' Binds all roles to the datalist
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub BindRoles()
        Try
            If radStatus.SelectedIndex = 0 Then
                dlRoles.DataSource = RolesFacade.GetRoles(True, False)
            ElseIf radStatus.SelectedIndex = 1 Then
                dlRoles.DataSource = RolesFacade.GetRoles(False, True)
            Else
                dlRoles.DataSource = RolesFacade.GetRoles(True, True)
            End If
            dlRoles.DataBind()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub


    ''' <summary>
    ''' Fills up the form with the role corresponding to roleId
    ''' </summary>
    ''' <param name="roleId"></param>
    ''' <remarks></remarks>
    Private Sub BindRole(ByVal roleId As String)
        Dim info As RoleInfo = RolesFacade.GetRoleInfo(roleId)
        txtRoleId.Text = info.RoleId
        chkIsInDB.Checked = info.IsInDB
        txtModUser.Text = info.ModUser
        txtModDate.Text = info.ModDate
        txtCode.Text = info.Code
        txtDescrip.Text = info.Description
        If info.StatusId <> "" Then
            ddlStatusId.SelectedValue = info.StatusId
        End If
        If info.AdvantageRoleId <> "" Then
            ddlSysRoleId.SelectedValue = info.AdvantageRoleId
        Else
            ddlSysRoleId.SelectedIndex = -1
        End If

        BuildModulesLB(lbSysModules)
        BindValidModules(roleId)
        BindUsersInRole(roleId)
    End Sub

    ''' <summary>
    ''' Displays the valid modules for this role
    ''' </summary>
    ''' <param name="roleId"></param>
    ''' <remarks></remarks>
    Private Sub BindValidModules(ByVal roleId As String)
        Try
            lbModules.DataSource = RolesFacade.GetValidModules(roleId)
            lbModules.DataTextField = "ModuleName"
            lbModules.DataValueField = "ModuleId"
            lbModules.DataBind()

            ' Remove all tasks that are in lnNextTasks from lbSysTasks
            Dim lb As ListItem
            For Each lb In lbModules.Items
                Dim lb2 As ListItem = lbSysModules.Items.FindByValue(lb.Value)
                If Not lb2 Is Nothing Then lbSysModules.Items.Remove(lb2)
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

    ''' <summary>
    ''' Displays the users that are in the role
    ''' </summary>
    ''' <param name="roleId"></param>
    ''' <remarks></remarks>
    Private Sub BindUsersInRole(ByVal roleId As String)
        lbUsers.DataSource = RolesFacade.GetUsersInRole(roleId)
        lbUsers.DataTextField = "FullName"
        lbUsers.DataValueField = "UserId"
        lbUsers.DataBind()
    End Sub

    ''' <summary>
    ''' Display an error message via javascript
    ''' </summary>
    ''' <param name="errorMessage"></param>
    ''' <remarks></remarks>
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False
        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client            
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub

    ''' <summary>
    ''' Event handler for when the user clicks on the left datalist containing all the users.
    ''' In response, this handler should populate the info for a task
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlRoles_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlRoles.ItemCommand
        ' get the selected resultid and store it in the viewstate object
        Dim roleid As String = e.CommandArgument.ToString
        ViewState("roleid") = roleid
        dlRoles.SelectedIndex = e.Item.ItemIndex

        ' do some error checking
        If roleid Is Nothing Or roleid = "" Then
            Me.DisplayErrorMessage("There was a problem the information for this role.")
            Return
        End If

        ' show the controls that allow the user to set the task to inactive/active
        'ddlActive.Visible = True
        'lblActivePrompt.Visible = True

        ' bind the result to the form
        BindRole(roleid)

        ' Advantage specific stuff
        Dim objCommon As New CommonUtilities

        CommonWebUtilities.RestoreItemValues(dlRoles, txtRoleId.Text)

        'objCommon.SetBtnState(Form1, "EDIT", Header1.UserPagePermission)
        'CommonWebUtilities.SetStyleToSelectedItem(dlRoles, roleid, ViewState, Header1)
    End Sub

    ''' <summary>
    ''' Resets the form pulling data from the db
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ResetForm()
        ' clear out the form        
        txtCode.Text = ""
        Me.txtDescrip.Text = ""
        lbModules.Items.Clear()
        lbSysModules.Items.Clear()

        BuildStatusDDL()
        BuildAdvantageRolesDDL()
        BuildModulesLB(lbSysModules)

        'TMCommon.BuildStatusDDL(ddlActive)
        'ddlActive.SelectedIndex = 0
        'ddlActive.Visible = False
        'lblActivePrompt.Visible = False
    End Sub

    ''' <summary>
    ''' Checks the form's contents for validity.  If any control is
    ''' not valid, an alert is made and the function returns false.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ValidateForm() As Boolean
        ' Check the task drop down
        If Me.txtCode.Text.Length = 0 Then
            DisplayErrorMessage("Code is required.")
            Return False
        End If

        If txtDescrip.Text.Length = 0 Then
            DisplayErrorMessage("Description is required.")
            Return False
        End If

        Return True
    End Function

#Region "Button Handlers"
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            ' check that the user has entered in all the right values
            If Not ValidateForm() Then Return

            Dim roleid As String = ViewState("roleid")
            Dim info As New RoleInfo
            ' determine if this is an update or a new record by looking
            ' at the ViewState which is set by passing a parameter to the page.
            If roleid Is Nothing Or roleid = "" Then
                info.IsInDB = False
                ViewState("roleid") = info.RoleId
                roleid = info.RoleId ' set the roleid so the valid modules are saved correctly
            Else
                info.IsInDB = True
                info.RoleId = roleid
            End If

            info.Code = txtCode.Text
            info.Description = txtDescrip.Text
            info.StatusId = ddlStatusId.SelectedValue
            info.AdvantageRoleId = ddlSysRoleId.SelectedValue
            info.ModUser = txtModUser.Text
            Try
                info.ModDate = Date.Parse(txtModDate.Text)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                info.ModDate = Date.Now
            End Try
            Dim result As String = RolesFacade.UpdateRoleInfo(info, username)
            If Not result = "" Then
                DisplayErrorMessage(result) 'Display Error Message
            Else
                'Update the valid modules for this role
                RolesFacade.DeleteValidModules(roleid)
                Dim opOk As Boolean = True
                Dim lb As ListItem
                For Each lb In lbModules.Items
                    If Not RolesFacade.AddValidModule(roleid, lb.Value, userid) Then
                        opOk = False
                    End If
                Next

                If opOk Then
                    CommonWebUtilities.DisplayInfoInMessageBox(Me.Page,"Role was saved successfully")
                Else
                    DisplayErrorMessage("Errors occurred while saving the role")
                End If
            End If

            ' redisplay the roles
            BindRoles()

            ' Advantage specific stuff
            Dim objCommon As New CommonUtilities
            'objCommon.SetBtnState(Form1, "EDIT", Header1.UserPagePermission)
            'CommonWebUtilities.SetStyleToSelectedItem(dlRoles, roleid, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlRoles, info.RoleId)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayErrorMessage(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' User wants to delete
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If txtRoleId.Text = Guid.Empty.ToString Then Return

        'Delete User info 
        Dim result As String = RolesFacade.DeleteRoleInfo(txtRoleId.Text, Date.Parse(txtModDate.Text))
        If result <> "" Then
            DisplayErrorMessage(result) 'Display Error Message
        Else
            BindRoles()
            ResetForm()
        End If

        CommonWebUtilities.RestoreItemValues(dlRoles, Guid.Empty.ToString)
    End Sub

    ''' <summary>
    ''' User clicked the new button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ViewState("roleid") = Nothing
        ResetForm()

        ' Advantage specific stuff
        Dim objCommon As New CommonUtilities
        'objCommon.SetBtnState(Form1, "NEW", Header1.UserPagePermission)
        'CommonWebUtilities.SetStyleToSelectedItem(dlRoles, Guid.Empty.ToString, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlRoles, Guid.Empty.ToString)

    End Sub
#End Region

#Region "Build ddl and lb methods"
    Private Sub BuildStatusDDL()
        Dim statuses As New StatusesFacade
        ddlStatusId.DataTextField = "Status"
        ddlStatusId.DataValueField = "StatusId"
        ddlStatusId.DataSource = statuses.GetAllStatuses()
        ddlStatusId.DataBind()
    End Sub
    Private Sub BuildAdvantageRolesDDL()
        ddlSysRoleId.DataTextField = "Descrip"
        ddlSysRoleId.DataValueField = "SysRoleId"
        ddlSysRoleId.DataSource = RolesFacade.GetAdvantageRoles()
        ddlSysRoleId.DataBind()
        ddlSysRoleId.Items.Insert(0, New ListItem("Select", ""))
        ddlSysRoleId.SelectedIndex = 0
    End Sub
    Private Sub BuildModulesLB(ByVal lb As ListBox)
        Dim facade As New RolesFacade
        lb.DataSource = facade.GetAllResourcesModules()
        lb.DataTextField = "ModuleName"
        lb.DataValueField = "ModuleId"
        lb.DataBind()
    End Sub
#End Region

    ''' <summary>
    ''' Change the left hand side view between active, inactive and all
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        Dim objCommon As New CommonUtilities

        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pobj)
        ResetForm()
        BindRoles()
    End Sub

    ''' <summary>
    ''' Move an item from the SysModules to the Modules listbox
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAddModule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddModule.Click
        Dim i As Integer = lbSysModules.SelectedIndex
        If i = -1 Then Return

        Dim ModuleId As String = lbSysModules.SelectedItem.Value
        Dim Name As String = lbSysModules.SelectedItem.Text
        lbSysModules.Items.RemoveAt(i)
        Me.lbModules.Items.Add(New ListItem(Name, ModuleId))
    End Sub
    ''' <summary>
    ''' Move an item from the Modules to the sysModules listbox
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelModule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelModule.Click
        Dim i As Integer = lbModules.SelectedIndex
        If i = -1 Then Return

        Dim ModuleId As String = lbModules.SelectedItem.Value
        Dim Name As String = lbModules.SelectedItem.Text
        lbModules.Items.RemoveAt(i)
        lbSysModules.Items.Add(New ListItem(Name, ModuleId))
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub

End Class