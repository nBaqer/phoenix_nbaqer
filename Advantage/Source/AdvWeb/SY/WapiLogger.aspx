﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="WapiLogger.aspx.vb" Inherits="AdvWeb.SY.SyWapiLogger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../Scripts/Advantage.Client.SY.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <style scoped>
        .columnX {
            float: left;
            margin-bottom: 10px;
            margin-top: 10px;
            margin-left: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .columCenter {
            float: left;
            vertical-align: middle;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .columnY {
            float: left;
            margin-bottom: 10px;
            margin-left: 10px;
            margin-top: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            border: #1565c0  solid 1px;
            padding:10px;
        }

        .image-type {
            width: 150px;
            float: left;
            margin-bottom: 15px;
            margin-left: 10px;
            margin-top: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .button-type {
            margin-left: 200px;
            text-align: center;
            vertical-align: middle;
            /*margin-bottom: 15px;
                    margin-top: 20px;*/
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .colum1 {
            width: 55%;
            float: left;
            text-align: center;
            vertical-align: middle;
            margin-bottom: 15px;
            margin-top: 20px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }



        .k-panel {
            margin: 5px;
        }
    </style>

    <div class="boxContainer" id="KendoContainerLogger" style="height: 100%; overflow: scroll">
        <h3>
            <asp:Label ID="headerTitle" runat="server"></asp:Label>
        </h3>
        <section id="FilterPanel" class="k-panel" style="width: 99%; height: 65px; margin: 5px; -moz-min-width: 1000px; -ms-min-width: 1000px; -o-min-width: 1000px; -webkit-min-width: 1000px; min-width: 1000px">
            <div class="columnX">
                <div>Operation:</div>
                <input id="cbOperationType" style="width: 250px" />
            </div>
            <div class="columnX">
                <div>Initial Date:</div>
                <input id="cbInitialDay" />

            </div>
            <div class="columnX">
                <div>End Date:</div>
                <input id="cbEndDay" />
            </div>
            <div class="columnX">
                <div>Log Information:</div>
                <div id="cbLogType"></div>

            </div>
            <div class="columnX">
                <button id="btnLoggerSearch" class="k-button" type="button" style="padding:8px;">Search Logger</button>
            </div>
            <div style="clear: both"></div>
        </section>
        <!-- Grid -->
        <article id="GridLogger" style="width: 99%; max-height: 650px; margin: 5px;">
            <div id="WapiLoggerGrid" style="height: 520px"></div>
        </article>

        <footer id="ActionLogger" class="k-panel" style="width: 99%; height: 95px; margin: 5px; -moz-min-width: 1000px; -ms-min-width: 1000px; -o-min-width: 1000px; -webkit-min-width: 1000px; min-width: 1000px">
            <div class="columnY">
                <table>
                    <tr>
                        <td>
                            <img id="WapiImageServiceStatus1" src="../images/geargray.jpg" alt="Service does not installed" style="width: 48px; vertical-align: central" />
                        </td>
                        <td>
                            <button id="WapiServiceStartButton" class="k-button" type="button" style="margin-right: 10px">Start Service</button>

                            <button id="WapiServiceStopButton" class="k-button" type="button">Stop Service</button>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: left">
                            <span id="WapiTextServiceStatus1">Initializing Page...</span>
                        </td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div class="columnY">
                <table>
                    <tr>
                        <td colspan="3">
                            <span>Select Operation:</span>
                            <div id="cbWapiActionOperationType" style="width: 100%"></div>
                            <br />
                        </td>

                    </tr>
                    <tr>

                        <td colspan="3" style="text-align: right">
                            <button id="WapiActionInitOnDemandOperation" class="k-button" type="button">Init On Demand</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="columnY">
                <table>
                    <tr>
                        <td style="width: 135px">
                            <span>Page Refresh Interval:</span>
                            <div id="cbWapiActionRefreshInterval" style="width: 100%"></div>
                        </td>
                        <td style="vertical-align: bottom">
                            <button id="WapiActionGoConfiguration" class="k-button" type="button">Go to WAPI Configuration Page</button>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <button id="WapiActionClearLogger" class="k-button" type="button" style="width: 100%">Clear logger</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="clear: both"></div>
        </footer>
        <!-- Windows Control to Show On Demand waiting Operation -->
        <div id="WindowsWapiLogOnDemandOperation" style="display: none;">
            <table style="width: 100%">
                <tr>
                    <td>
                        <img alt="Waiting for On demand Operation" src="../images/Gearstar.gif" /></td>
                    <td style="text-align: center">
                        <button id="btnLogCancelOnDemand" class="k-button" type="button" style="width: 140px; height: 50px">Cancel On Demand</button></td>
                </tr>

            </table>
            <%--  <div class="image-type">
                
            </div>
            <div class="button-type">
                
            </div>--%>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {

            var wapilogger = new Wapi.WapiLogger();

        });

    </script>
    <script src="../Kendo/js/jszip.min.js"></script>

</asp:Content>

