﻿Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports BL = Advantage.Business.Logic.Layer

Partial Class SchlReqFldsForResources
    Inherits BasePage

#Region "Declare all variables to be used in this page"
    'Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    'Protected WithEvents txtResourcId As System.Web.UI.WebControls.TextBox
    'Protected strError As String
    'Protected WithEvents Panel2 As System.Web.UI.WebControls.Panel

    'Private pObj As New UserPagePermissionInfo
    'Protected userId As String
    'Protected campusId As String
    'Private mruProvider As BL.NavigationRoutines
    'Dim lstSetupRequiredFields As List(Of BO.SetUpRequiredFieldResources)
#End Region
    
#Region "Step 0: Web Form Designer Generated Code "
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    
#End Region

    '#Region "Step 1: Populate Module Dropdown, set permissions, populate Academics page by Default"

    '    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
    '        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    '    End Sub
    '    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
    '        InitializeComponent()
    '        mruProvider = New BL.NavigationRoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    '    End Sub
    '    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '        Dim resourceId As Integer

    '        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
    '       campusid = Master.CurrentCampusId 
    '        userId = AdvantageSession.UserState.UserId.ToString


    '        'btnBuildList.Attributes.Add("onclick", "SetHiddenText();return true;")

    '        'Disable History button at all time
    '        'Header1.EnableHistoryButton(False)

    '        'pObj = Header1.UserPagePermission

    '        Dim advantageUserState As New BO.User()
    '        advantageUserState = AdvantageSession.UserState

    '        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
    '        'Check if this page still exists in the menu while switching campus
    '        Dim boolPageStillPartofMenu As Boolean = False

    '            If  Master.IsSwitchedCampus = True Then
    '                boolPageStillPartofMenu = Master.CheckIfPageIsPartofMenuWhileSwitchingCampus(234, 3) 'Page Type:3 - Common Tasks
    '                If boolPageStillPartofMenu = False Or pObj.HasNone = True Then
    '                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
    '                    Exit Sub
    '                Else
    '                   CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
    '                End If
    '            End If

    '        If Not Page.IsPostBack Then
    '            Dim facResRels As New ResourcesRelationsFacade
    '            Dim facUserSecurit As New UserSecurityFacade
    '            'Dim dsResRels As New DataSet

    '            With ddlModules
    '                .DataSource = facResRels.GetModules
    '                .DataTextField = "Resource"
    '                .DataValueField = "ResourceId"
    '                .DataBind()
    '            End With

    '            divContent.Visible = False

    '            'Set the Session("ResourcesRelations") to nothing. This is very important to fix the following issue.
    '            'If the user leaves the page and then returns to it without closing the browser the
    '            'Session("ResourcesRelations") variable will still be populated and so the first test in the else
    '            'clause below will pass and there will be an attempt to build the attendance table when
    '            'a term is selected. This will cause an error.
    '            Session("ResourcesRelations") = Nothing

    '            'We want to load datalist when page loads
    '            'by default it will take first module from the list
    '            ddlModules.SelectedIndex = 0
    '            BuildRequiredFieldsList()

    '        Else
    '            If Not Session("ResourcesRelations") Is Nothing Then
    '                If HiddenText.Text <> "YES" Then
    '                    divContent.Visible = True
    '                Else
    '                    HiddenText.Text = ""
    '                End If

    '            Else
    '                If HiddenText.Text = "YES" Then
    '                    HiddenText.Text = ""
    '                End If

    '            End If
    '        End If


    '        'Added by Balaji on May 14 2012
    '        'US 3091 
    '        'Set Permission
    '        SetPermission()
    '    End Sub
    '    Private Sub SetPermission()
    '        'New and Delete buttons will always be disabled
    '        'as giving permissions to users the delete permission
    '        'will affect the stability of the application
    '        btnNew.Enabled = False
    '        btnDelete.Enabled = False

    '        'If user has full or edit permission 
    '        'enable Save Button
    '        If pObj.HasFull Or pObj.HasEdit = True Then
    '            btnSave.Enabled = True
    '        Else
    '            btnSave.Enabled = False
    '        End If
    '    End Sub
    '    Protected Sub ddlModules_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlModules.SelectedIndexChanged
    '        'US3091
    '        'Added by Balaji on May 14 2012
    '        BuildRequiredFieldsList()
    '    End Sub
    '    Private Sub BuildRequiredFieldsList()
    '        Try
    '            lstSetupRequiredFields = mruProvider.BuildSetUpRequiredFields().FindAll(Function(s) s.ModuleResourceId.Equals(CInt(ddlModules.SelectedValue)))
    '            If Not lstSetupRequiredFields Is Nothing Or lstSetupRequiredFields.Count >= 1 Then
    '                dlstSetupRequiredFields.DataSource = lstSetupRequiredFields
    '                dlstSetupRequiredFields.DataBind()
    '            End If
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '        End Try
    '    End Sub

    '#End Region

    '#Region "Step 2: Populate Right section by selecting an page in Left section"

    '    Protected Sub dlstSetupRequiredFields_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstSetupRequiredFields.ItemCommand
    '        Dim intResourceId As String = CInt(e.CommandArgument)
    '        BuildReqFieldsDataGrid(intResourceId)
    '        lblCaption.Text = e.CommandName.ToString
    '        txtResourceId.Text = e.CommandArgument.ToString
    '        CommonWebUtilities.RestoreItemValues(dlstSetupRequiredFields, intResourceId)
    '    End Sub
    '    Private Sub BuildReqFieldsDataGrid(ByVal resourceId As Integer)
    '        Dim fac As New ResourcesRelationsFacade
    '        Dim dt As New DataTable
    '        Dim dtResReqFlds As DataTable

    '        dtResReqFlds = fac.GetSchoolRequiredFldsForResource(resourceId)
    '        'dtResReqFlds.PrimaryKey = New DataColumn() {dtResReqFlds.Columns("TblFldsId")}
    '        Session("ResReqFlds") = dtResReqFlds

    '        dt = fac.GetFieldsForPage(resourceId)
    '        dgPageFields.DataSource = dt
    '        dgPageFields.DataBind()

    '        If dt.Rows.Count >= 1 Then
    '            divContent.Visible = True
    '            dgPageFields.Visible = True
    '        End If
    '    End Sub
    '    Private Sub dgPageFields_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPageFields.ItemDataBound
    '        'We want to check the checkboxes if the field has been set as required by the school for this resource
    '        Dim chk As CheckBox
    '        Dim fldId As Integer
    '        Dim dt As New DataTable

    '        Select Case e.Item.ItemType
    '            Case ListItemType.Item, ListItemType.AlternatingItem
    '                chk = CType(e.Item.FindControl("chkReq"), CheckBox)
    '                fldId = CInt(CType(e.Item.FindControl("lblFldId"), Label).Text)
    '                'If this fldid exists in the dt stored in Session("ResReqFlds") then we should
    '                'check the checkbox
    '                dt = Session("ResReqFlds")
    '                If CommonUtilities.TblFldsIdExistsInDT(dt, fldId) Then
    '                    chk.Checked = True
    '                Else
    '                    chk.Checked = False
    '                End If
    '        End Select

    '    End Sub

    '#End Region

    '#Region "Step 3 - Save Changes made to required fields to database"

    '    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '        Dim iitems As DataGridItemCollection
    '        Dim iitem As DataGridItem
    '        Dim i As Integer
    '        Dim fldId As Integer
    '        Dim req As Integer
    '        Dim facResRel As New ResourcesRelationsFacade
    '        Dim dt As New DataTable
    '        Dim recId As String

    '        ' Save the datagrid items in a collection.
    '        iitems = dgPageFields.Items

    '        'Loop thru the collection
    '        For i = 0 To iitems.Count - 1
    '            iitem = iitems.Item(i)

    '            req = CInt(CType(iitem.FindControl("chkReq"), CheckBox).Checked())
    '            fldId = CInt(CType(iitem.FindControl("lblFldId"), Label).Text())
    '            dt = Session("ResReqFlds")

    '            'If the item is not checked but exists in the session variable that stores the
    '            'required fields for the resource then we will need to delete it from the database
    '            If req = 0 And CommonUtilities.TblFldsIdExistsInDT(dt, fldId) Then
    '                facResRel.DeleteResourceRequiredField(CInt(txtResourceId.Text), fldId)
    '            End If

    '            'If the item is checked but does not exists in the session variable that stores the
    '            'required fields for the resource then we will need to insert it into the database
    '            If req <> 0 And Not CommonUtilities.TblFldsIdExistsInDT(dt, fldId) Then
    '                recId = Guid.NewGuid.ToString
    '                facResRel.AddResourceRequiredField(recId, CInt(txtResourceId.Text), fldId, AdvantageSession.UserState.UserName)
    '            End If
    '        Next

    '        'If we get to this point we need to refresh the DataGrid
    '        BuildReqFieldsDataGrid(CInt(txtResourceId.Text))

    '    End Sub

    '#End Region

    '#Region "Step 4 - Any Error Message Routines"

    '    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
    '        '   Set error condition
    '        Customvalidator1.ErrorMessage = errorMessage
    '        Customvalidator1.IsValid = False

    '        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
    '            '   Display error in message box in the client
    '            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    '        End If
    '    End Sub
    '    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    '        'add to this list any button or link that should ignore the Confirm Exit Warning.
    '        Dim controlsToIgnore As New ArrayList()
    '        'add save button 
    '        controlsToIgnore.Add(btnSave)
    '        controlsToIgnore.Add(btnBuildList)
    '        'Add javascript code to warn the user about non saved changes 
    '        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    '        BindToolTip()
    '    End Sub

    '#End Region


End Class
