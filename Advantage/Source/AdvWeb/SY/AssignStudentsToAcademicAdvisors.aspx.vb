' ===============================================================================
' FAME AdvantageV1
'
' AssignStudentsToAcademicAdvisors.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.

Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class AssignStudentsToAcademicAdvisors
    Inherits BasePage
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblAvailableRoles As System.Web.UI.WebControls.Label
    Protected WithEvents lblSelectedRoles As System.Web.UI.WebControls.Label
    Protected WithEvents lbxAvailableRoles As System.Web.UI.WebControls.ListBox
    Protected WithEvents lbxSelectedRoles As System.Web.UI.WebControls.ListBox
    Protected campusId As String
    Protected userName As String
    Protected strDefaultCountry As String
    Protected ResourceId, userId As String
    Protected ModuleId As String
    Dim m_Context As HttpContext
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        InitButtonsForLoad()
        If Not Page.IsPostBack Then
            'objCommon.PageSetup(Form1, "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            'ViewState("MODE") = "NEW"

            If CommonWebUtilities.IsValidGuid(Request.QueryString("cmpid")) Then
                ViewState("CampusId") = Request.QueryString("cmpid")
            End If

            '   BuildDropDownLists
            BuildDropdownLists()

            '   Populate and bind ListBoxes
            'PopulateListBoxes(ddlAcademicAdvisorId.SelectedValue, ddlAdvisorTypeId.SelectedValue)


        Else
            'objCommon.PageSetup(Form1, "EDIT")
        End If

    End Sub
    Private Sub BuildDropdownLists()
        '   Build Academic Advisors DDL
        BuildAcademicAdvisorsDDL(ddlAdvisorTypeId.SelectedValue)
    End Sub
    Private Sub BuildAcademicAdvisorsDDL(ByVal advisorTypeId As Integer)

        If advisorTypeId = 0 Then
            ddlAcademicAdvisorId.Items.Clear()
            ddlAcademicAdvisorId.Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            ddlAcademicAdvisorId.SelectedIndex = 0

        Else
            With ddlAcademicAdvisorId
                .DataTextField = "AcademicAdvisorName"
                .DataValueField = "AcademicAdvisorId"
                .DataSource = (New AcademicAdvisorsFacade).GetAcademicAdvisorsPerCampus(ViewState("CampusId"), advisorTypeId)
                .DataBind()

                '   if there are no Academic Advisors create an empty entry
                'If .Items.Count = 0 Then
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                'End If
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '   Update SelectedStudents table from ListBox Selections
        Dim result As String = (New AcademicAdvisorsFacade).UpdateListOfStudentsAssignedToAnAcademicAdvisor(ViewState("CampusId"), ddlAcademicAdvisorId.SelectedValue, ddlAdvisorTypeId.SelectedValue, Session("UserName"), GetSelectedStudentsFromListBox())

        '   Populate and bind ListBoxes
        PopulateListBoxes(ddlAcademicAdvisorId.SelectedValue, ddlAdvisorTypeId.SelectedValue)

    End Sub
    Private Sub PopulateListBoxes(ByVal AdvisorId As String, ByVal AdvisorTypeId As Integer)
        '   Populate Available Students ListBox
        PopulateAvailableStudentsListBox(AdvisorId, AdvisorTypeId)

        '   Populate Selected Students ListBox
        PopulateSelectedStudentsListBox(AdvisorId, AdvisorTypeId)

    End Sub

    Private Sub PopulateSelectedStudentsListBox(ByVal AdvisorId As String, ByVal AdvisorTypeId As Integer)

        '   Bind the SelectedStudents ListBox
        lbxSelectedStudents.DataSource = (New AcademicAdvisorsFacade).GetStudentsAssignedToASpecificAcademicAdvisor(AdvisorId, AdvisorTypeId, ViewState("CampusId"))
        lbxSelectedStudents.DataTextField = "StudentName"
        lbxSelectedStudents.DataValueField = "StuEnrollId"
        lbxSelectedStudents.DataBind()

    End Sub
    Private Sub PopulateAvailableStudentsListBox(ByVal AdvisorId As String, ByVal AdvisorTypeId As Integer)

        '   Bind the SelectedStudents ListBox
        lbxAvailableStudents.DataSource = (New AcademicAdvisorsFacade).GetStudentsNotAssignedToAnyAcademicAdvisor(ViewState("CampusId"), AdvisorTypeId)
        lbxAvailableStudents.DataTextField = "StudentName"
        lbxAvailableStudents.DataValueField = "StuEnrollId"
        lbxAvailableStudents.DataBind()

    End Sub
    ' Update SelectedStudents table From ListBox selections
    Private Function GetSelectedStudentsFromListBox() As String()

        '   create an array string with selected Students (Students)
        Dim selectedStudents(lbxSelectedStudents.Items.Count - 1) As String

        Dim i As Integer = 0
        Dim item As ListItem
        For Each item In lbxSelectedStudents.Items
            selectedStudents.SetValue(item.Value.ToString, i)
            i += 1
        Next

        '   return array with selected students
        Return selectedStudents

    End Function
    '
    '   Move items from Available to Selected
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        '   check that there are items selected
        If Not AreThereSelectedItems(lbxAvailableStudents) Then
            DisplayErrorMessage("At least one item must be selected")
            Exit Sub
        End If

        '   Loop while there are selected items
        Dim thereAreSelectedItems As Boolean = True
        While thereAreSelectedItems
            Dim item As ListItem
            thereAreSelectedItems = False

            '   Loop throught out the listbox
            For Each item In lbxAvailableStudents.Items

                '   If the item is selected move it to the other listbox
                If item.Selected Then
                    lbxSelectedStudents.Items.Add(New ListItem(item.Text.ToString, item.Value.ToString))
                    lbxAvailableStudents.Items.Remove(item)
                    thereAreSelectedItems = True
                    Exit For
                End If
            Next

        End While

    End Sub
    '
    '   Move items from Selected to Available
    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click

        '   check that there are items selected
        If Not AreThereSelectedItems(lbxSelectedStudents) Then
            DisplayErrorMessage("At least one item must be selected")
            Exit Sub
        End If

        '   Loop while there are selected items
        Dim thereAreSelectedItems As Boolean = True
        While thereAreSelectedItems
            Dim item As ListItem
            thereAreSelectedItems = False

            '   Loop throught out the listbox
            For Each item In lbxSelectedStudents.Items

                '   If the item is selected move it to the other listbox
                If item.Selected Then
                    lbxAvailableStudents.Items.Add(New ListItem(item.Text.ToString, item.Value.ToString))
                    lbxSelectedStudents.Items.Remove(item)
                    thereAreSelectedItems = True
                    Exit For
                End If
            Next
        End While

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Function AreThereSelectedItems(ByVal listbox As ListBox) As Boolean
        For Each item As ListItem In listbox.Items
            If item.Selected Then Return True
        Next
        Return False
    End Function

    Private Sub ddlAcademicAdvisorId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAcademicAdvisorId.SelectedIndexChanged
        '   Populate and bind ListBoxes
        PopulateListBoxes(ddlAcademicAdvisorId.SelectedValue, ddlAdvisorTypeId.SelectedValue)
    End Sub
    Private Sub InitButtonsForLoad()
        btnNew.Enabled = False
        btnDelete.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnDelete.Enabled = False
    End Sub

    Private Sub ddlAdvisorTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAdvisorTypeId.SelectedIndexChanged
        BuildAcademicAdvisorsDDL(ddlAdvisorTypeId.SelectedValue)
        lbxAvailableStudents.Items.Clear()
        lbxSelectedStudents.Items.Clear()
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnRemove)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
