﻿Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports Telerik.Web.UI

Partial Class StudentPageNavigation
    Inherits BasePage
#Region " Web Form Designer Generated Code "
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Private rolesResourcesAccessRightsDS As DataSet
    Private clientIds As New NameValueCollection()
    Private clientXmlDoc As New System.Xml.XmlDocument()
    Protected intSchoolOption As Integer = 0
    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim m_Context As HttpContext
        m_Context = HttpContext.Current
        txtResourceId.Text = CInt(m_Context.Items("ResourceId"))

        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim ds2 As New DataSet
        'Dim sStatusId As String

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

      If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        InitButtonsForLoad()
        If Not IsPostBack Then
            BindGrid()
        End If
        headerTitle.Text = Header.Title
    End Sub
    Private Sub BindGrid()
        intSchoolOption = CommonWebUtilities.SchoolSelectedOptions(MyAdvAppSettings.AppSettings("SchedulingMethod"))
        RadGrid1.Visible = True
        ViewState("getPages") = (New GradesFacade).AddPagesToModules(intSchoolOption)
        RadGrid1.DataSource = ViewState("getPages")
        RadGrid1.DataBind()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ''Every Page should be associated to atleast one module
        ''DE7652 QA: Setup student page navigation should probably not force all the pages to be associated to the a module(s).
        Dim ErrorMessage As String = ""

        For Each row As GridDataItem In RadGrid1.Items
            If row.ItemType = GridItemType.Item Or _
          row.ItemType = GridItemType.AlternatingItem Then
                If CType(row.Controls(0).FindControl("chkAR"), RadButton).Checked = False And CType(row.Controls(0).FindControl("chkFAC"), RadButton).Checked = False And CType(row.Controls(0).FindControl("chkFA"), RadButton).Checked = False And CType(row.Controls(0).FindControl("chkSA"), RadButton).Checked = False And CType(row.Controls(0).FindControl("chkPL"), RadButton).Checked = False Then
                    Dim resource As String = CType(row.Controls(0).FindControl("lblChildResourceDesc"), Label).Text
                    If Not resource = "" Then
                        ErrorMessage += resource + "; "
                    End If
                End If
            End If
        Next
        If Not ErrorMessage = "" Then

            '    DisplayErrorMessage(" The following page(s) has to be added to  any one of the modules." + ErrorMessage)
            RadNotification1.Show()
            RadNotification1.Text = " The following page(s) has to be added to  any one of the modules." + ErrorMessage
            Exit Sub
        End If
        ''"The page has to be added to any one of the modules"
        AddPermissionsToPages()
        intSchoolOption = CommonWebUtilities.SchoolSelectedOptions(MyAdvAppSettings.AppSettings("SchedulingMethod"))
        RadGrid1.DataSource = (New GradesFacade).AddPagesToModules(intSchoolOption)
        RadGrid1.Rebind()

        'Rebuild student tabs
        '  Master.ReBuildStudentTabsForStudentPageNavigation(Master.CurrentCampusId)

        RadNotification1.Show()
        RadNotification1.Text = "The selected student pages were modified successfully"
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub
    Protected Sub RadGrid1_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid1.ItemDataBound
        If e.Item.ItemType = GridItemType.Item Or _
            e.Item.ItemType = GridItemType.AlternatingItem Then
            Dim Resource As String = CType(e.Item.Controls(0).FindControl("lblChildResourceDesc"), Label).Text
            Dim ResourceId As String = CType(e.Item.Controls(0).FindControl("txtChildResourceId"), TextBox).Text
            Dim ParentResourceId As String = CType(e.Item.Controls(0).FindControl("txtParentResourceId"), TextBox).Text
            Dim ResourceTypeId As Integer = CInt(CType(e.Item.Controls(0).FindControl("txtResourceTypeId"), TextBox).Text)
            Dim TabId As Integer = 0
            Try
                TabId = CInt(CType(e.Item.Controls(0).FindControl("txtTabId"), TextBox).Text)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                TabId = 0
            End Try


            'Set the sub menus
            Select Case ResourceTypeId 'ResourceId
                Case Is = 1 '"26"
                    CType(e.Item.Controls(0).FindControl("lblParentModule"), Label).Visible = False
                Case Is = 2 '"693", "132", "160", "129", "71", "190"
                    CType(e.Item.Controls(0).FindControl("txtParentResource"), Label).Text = Resource
                    CType(e.Item.Controls(0).FindControl("lblChildResourceDesc"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("lblParentModule"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("txtParentResource"), Label).CssClass = "label"
                    CType(e.Item.Controls(0).FindControl("chkAR"), RadButton).Visible = False
                    CType(e.Item.Controls(0).FindControl("chkFAC"), RadButton).Visible = False
                    CType(e.Item.Controls(0).FindControl("chkFA"), RadButton).Visible = False
                    CType(e.Item.Controls(0).FindControl("chkSA"), RadButton).Visible = False
                    CType(e.Item.Controls(0).FindControl("chkPL"), RadButton).Visible = False
                    e.Item.ForeColor = Drawing.Color.White
                    e.Item.BackColor = Drawing.Color.FromArgb(21, 101, 192)
            End Select


            'Do not repeat sub menus
            Select Case ResourceTypeId 'ParentResourceId
                Case Is = 3 And TabId = 394 '"693", "132", "160", "129", "71", "190" 
                    CType(e.Item.Controls(0).FindControl("txtParentResource"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("lblParentModule"), Label).Text = ""
                    'CType(e.Item.Controls(0).FindControl("ddlChild"), RadComboBox).Visible = True

                    If CInt(CType(e.Item.Controls(0).FindControl("txtIsInAR"), TextBox).Text) = 1 Then
                        CType(e.Item.Controls(0).FindControl("chkAR"), RadButton).Checked = True
                        ''DE7652 QA: Setup student page navigation should probably not force all the pages to be associated to the a module(s).
                        If ResourceId = 203 Then
                            CType(e.Item.Controls(0).FindControl("chkAR"), RadButton).Enabled = False
                            CType(e.Item.Controls(0).FindControl("chkAR"), RadButton).ToolTip = Resource & " page was shipped with Academics module and cannot be edited"
                        End If


                        'If CInt(CType(e.Item.Controls(0).FindControl("txtisPageShippedWithAR"), TextBox).Text) = 1 Then 'Page was shipped along with AR module, so cannot be removed
                        '    CType(e.Item.Controls(0).FindControl("chkAR"), RadButton).Enabled = False
                        '    CType(e.Item.Controls(0).FindControl("chkAR"), RadButton).ToolTip = Resource & " page was shipped with Academics module and cannot be edited"
                        'End If
                    End If
                    If CInt(CType(e.Item.Controls(0).FindControl("txtIsInFAC"), TextBox).Text) = 1 Then
                        CType(e.Item.Controls(0).FindControl("chkFAC"), RadButton).Checked = True
                        If ResourceId = 203 Then
                            CType(e.Item.Controls(0).FindControl("chkFAC"), RadButton).Enabled = False
                            CType(e.Item.Controls(0).FindControl("chkFAC"), RadButton).ToolTip = Resource & " page was shipped with Faculty module and cannot be edited"
                        End If
                        'If CInt(CType(e.Item.Controls(0).FindControl("txtisPageShippedWithFAC"), TextBox).Text) = 1 Then 'Page was shipped along with AR module, so cannot be removed
                        '    CType(e.Item.Controls(0).FindControl("chkFAC"), RadButton).Enabled = False
                        '    CType(e.Item.Controls(0).FindControl("chkFAC"), RadButton).ToolTip = Resource & " page was shipped with Faculty module and cannot be edited"
                        'End If
                    End If
                    If CInt(CType(e.Item.Controls(0).FindControl("txtIsInFA"), TextBox).Text) = 1 Then
                        CType(e.Item.Controls(0).FindControl("chkFA"), RadButton).Checked = True
                        If ResourceId = 175 Then
                            CType(e.Item.Controls(0).FindControl("chkFA"), RadButton).Enabled = False
                            CType(e.Item.Controls(0).FindControl("chkFA"), RadButton).ToolTip = Resource & " page was shipped with Financial Aid module and cannot be edited"
                        End If
                        'If CInt(CType(e.Item.Controls(0).FindControl("txtisPageShippedWithFA"), TextBox).Text) = 1 Then 'Page was shipped along with AR module, so cannot be removed
                        '    CType(e.Item.Controls(0).FindControl("chkFA"), RadButton).Enabled = False
                        '    CType(e.Item.Controls(0).FindControl("chkFA"), RadButton).ToolTip = Resource & " page was shipped with Financial Aid module and cannot be edited"
                        'End If
                    End If
                    If CInt(CType(e.Item.Controls(0).FindControl("txtIsInSA"), TextBox).Text) = 1 Then
                        CType(e.Item.Controls(0).FindControl("chkSA"), RadButton).Checked = True
                        If ResourceId = 116 Then
                            CType(e.Item.Controls(0).FindControl("chkSA"), RadButton).Enabled = False
                            CType(e.Item.Controls(0).FindControl("chkSA"), RadButton).ToolTip = Resource & " page was shipped with Student Accounts module and cannot be edited"
                        End If
                        'If CInt(CType(e.Item.Controls(0).FindControl("txtisPageShippedWithSA"), TextBox).Text) = 1 Then 'Page was shipped along with AR module, so cannot be removed
                        '    CType(e.Item.Controls(0).FindControl("chkSA"), RadButton).Enabled = False
                        '    CType(e.Item.Controls(0).FindControl("chkSA"), RadButton).ToolTip = Resource & " page was shipped with Student Accounts module and cannot be edited"
                        'End If
                    End If
                    If CInt(CType(e.Item.Controls(0).FindControl("txtIsInPL"), TextBox).Text) = 1 Then
                        CType(e.Item.Controls(0).FindControl("chkPL"), RadButton).Checked = True
                        If ResourceId = 203 Then
                            CType(e.Item.Controls(0).FindControl("chkPL"), RadButton).Enabled = False
                            CType(e.Item.Controls(0).FindControl("chkPL"), RadButton).ToolTip = Resource & " page was shipped with Placement module and cannot be edited"
                        End If
                        'If CInt(CType(e.Item.Controls(0).FindControl("txtisPageShippedWithPL"), TextBox).Text) = 1 Then 'Page was shipped along with AR module, so cannot be removed
                        '    CType(e.Item.Controls(0).FindControl("chkPL"), RadButton).Enabled = False
                        '    CType(e.Item.Controls(0).FindControl("chkPL"), RadButton).ToolTip = Resource & " page was shipped with Placement module and cannot be edited"
                        'End If
                    End If
            End Select

            Dim AccessLevel As Integer
            Try
                AccessLevel = CInt(CType(e.Item.Controls(0).FindControl("txtAccessLevel"), TextBox).Text)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                AccessLevel = 0
            End Try
        End If
    End Sub
    Private Function GetListofPageLevelPermissions() As DataSet
        Dim tbl As New DataTable("StudentPageNavigation")
        Dim strModUser As String = ""
        Dim strModDate As DateTime = Date.Now
        Dim intParentResourceId, intChildResourceId, intModuleResourceId, intResourceTypeId As Integer
        With tbl
            'Define table schema
            .Columns.Add("ModuleResourceId", GetType(Integer))
            .Columns.Add("SubMenuResourceId", GetType(Integer))
            .Columns.Add("PageResourceId", GetType(Integer))
            .Columns.Add("ModUser", GetType(String))
            .Columns.Add("CheckboxState", GetType(Boolean))
        End With

        'Disable Paging, other wise RadGrid1.MasterTableView.Items will only return the pagesize
        'For example, if page size is set to 10, then RadGrid1.MasterTableView.Items will only return 10, even if we have 100 records in grid

        'RadGrid1.AllowPaging = False
        'RadGrid1.Rebind()


        'GetSelectedItems method brings in a collection of rows that were selected
        For Each dataItem As GridDataItem In RadGrid1.MasterTableView.Items
            'BR: If the rules to the datatable using LoadDataRow Method
            'Benefits:LoadDataRow method helps get control RowState for the new DataRow
            'First Parameter: Array of values, the items in the array correspond to columns in the table. 
            'Second Parameter: AcceptChanges, enables to control the value of the RowState property of the new DataRow.
            '                  Passing a value of False for this parameter causes the new row to have a RowState of Added

            Try
                intParentResourceId = CInt(CType(dataItem.FindControl("txtParentResourceId"), TextBox).Text)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)


            End Try
            intChildResourceId = 0
            Try
                intChildResourceId = CInt(CType(dataItem.FindControl("txtChildResourceId"), TextBox).Text)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                intChildResourceId = 0
            End Try

            intResourceTypeId = CInt(CType(dataItem.FindControl("txtResourceTypeId"), TextBox).Text)

            strModUser = Session("UserName")
            Try
                Dim boolAcademics As Boolean = CType(dataItem.FindControl("chkAR"), RadButton).Checked
                Dim boolFAC As Boolean = CType(dataItem.FindControl("chkFAC"), RadButton).Checked
                Dim boolFA As Boolean = CType(dataItem.FindControl("chkFA"), RadButton).Checked
                Dim boolPL As Boolean = CType(dataItem.FindControl("chkPL"), RadButton).Checked
                Dim boolSA As Boolean = CType(dataItem.FindControl("chkSA"), RadButton).Checked

                'If boolAcademics = True AndAlso CType(dataItem.FindControl("chkAR"), CheckBox).Enabled = True Then
                '    intModuleResourceId = 26
                '    tbl.LoadDataRow(New Object() {intModuleResourceId, intParentResourceId, intChildResourceId, strModUser}, False)
                'End If
                'If boolFAC = True AndAlso CType(dataItem.FindControl("chkFAC"), CheckBox).Enabled = True Then
                '    intModuleResourceId = 300
                '    tbl.LoadDataRow(New Object() {intModuleResourceId, intParentResourceId, intChildResourceId, strModUser}, False)
                'End If
                'If boolFA = True AndAlso CType(dataItem.FindControl("chkFA"), CheckBox).Enabled = True Then
                '    intModuleResourceId = 191
                '    tbl.LoadDataRow(New Object() {intModuleResourceId, intParentResourceId, intChildResourceId, strModUser}, False)
                'End If
                'If boolPL = True AndAlso CType(dataItem.FindControl("chkPL"), CheckBox).Enabled = True Then
                '    intModuleResourceId = 193
                '    tbl.LoadDataRow(New Object() {intModuleResourceId, intParentResourceId, intChildResourceId, strModUser}, False)
                'End If
                'If boolSA = True AndAlso CType(dataItem.FindControl("chkSA"), CheckBox).Enabled = True Then
                '    intModuleResourceId = 194
                '    tbl.LoadDataRow(New Object() {intModuleResourceId, intParentResourceId, intChildResourceId, strModUser}, False)
                'End If
                If CType(dataItem.FindControl("chkAR"), RadButton).Enabled = True And intResourceTypeId = 3 Then
                    intModuleResourceId = 26
                    tbl.LoadDataRow(New Object() {intModuleResourceId, intParentResourceId, intChildResourceId, strModUser, boolAcademics}, False)
                End If
                If CType(dataItem.FindControl("chkFAC"), RadButton).Enabled = True And intResourceTypeId = 3 Then
                    intModuleResourceId = 300
                    tbl.LoadDataRow(New Object() {intModuleResourceId, intParentResourceId, intChildResourceId, strModUser, boolFAC}, False)
                End If
                If CType(dataItem.FindControl("chkFA"), RadButton).Enabled = True And intResourceTypeId = 3 Then
                    intModuleResourceId = 191
                    tbl.LoadDataRow(New Object() {intModuleResourceId, intParentResourceId, intChildResourceId, strModUser, boolFA}, False)
                End If
                If CType(dataItem.FindControl("chkPL"), RadButton).Enabled = True And intResourceTypeId = 3 Then
                    intModuleResourceId = 193
                    tbl.LoadDataRow(New Object() {intModuleResourceId, intParentResourceId, intChildResourceId, strModUser, boolPL}, False)
                End If
                If CType(dataItem.FindControl("chkSA"), RadButton).Enabled = True And intResourceTypeId = 3 Then
                    intModuleResourceId = 194
                    tbl.LoadDataRow(New Object() {intModuleResourceId, intParentResourceId, intChildResourceId, strModUser, boolSA}, False)
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        Next
        'Enable Paging again
        'RadGrid1.AllowPaging = True
        'RadGrid1.Rebind()
        Dim ds As DataSet = New DataSet()
        ds.Tables.Add(tbl)
        Return ds
    End Function
    Private Sub AddPermissionsToPages() 'This Procedure contains the update logic
        'Get the contents of datatable and pass it as xml document
        Dim dsPermissions As New DataSet
        Dim grdFacade As New SetDictationTestRulesFacade
        dsPermissions = GetListofPageLevelPermissions()
        If Not dsPermissions Is Nothing Then
            For Each lcol As DataColumn In dsPermissions.Tables(0).Columns
                lcol.ColumnMapping = System.Data.MappingType.Attribute
            Next
            Dim strXML As String = dsPermissions.GetXml
            grdFacade.AddStudentPagesToOtherModules(strXML)
        End If
    End Sub
    Protected Sub ddlPermissions_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl1 As DropDownList = DirectCast(sender, DropDownList)
        Dim item As GridDataItem = DirectCast(ddl1.NamingContainer, GridDataItem)
        Dim keyvalue As String = item.GetDataKeyValue("ChildResourceId").ToString()

        Dim ddlEditAccess As DropDownList = CType(item.FindControl("ddlPermissions"), DropDownList)
        For Each grdItem As GridDataItem In RadGrid1.Items
            Dim ResourceId As String = CType(grdItem.FindControl("txtParentResourceId"), TextBox).Text
            If ResourceId = keyvalue Then
                Dim ddlCurrent As DropDownList = CType(grdItem.FindControl("ddlPermissions"), DropDownList)
                ddlCurrent.SelectedValue = ddlEditAccess.SelectedValue
                Dim ddlChildDDL As RadComboBox = CType(grdItem.FindControl("ddlChild"), RadComboBox)
                ddlChildDDL.SelectedValue = ddlEditAccess.SelectedValue
            End If
        Next
    End Sub
    Protected Sub ddlPermissionsParent_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl1 As DropDownList = DirectCast(sender, DropDownList)
        Dim item As GridDataItem = DirectCast(ddl1.NamingContainer, GridDataItem)
        Dim keyvalue As String = item.GetDataKeyValue("ChildResourceId").ToString()
        Dim ddlEditAccess As DropDownList = CType(item.FindControl("ddlPermissionsParent"), DropDownList)
        For Each grdItem As GridDataItem In RadGrid1.Items
            Dim ResourceId As String = CType(grdItem.FindControl("txtParentResourceId"), TextBox).Text
            Dim ddlCurrent As DropDownList = CType(grdItem.FindControl("ddlPermissions"), DropDownList)
            Dim ddlChildDDL As RadComboBox = CType(grdItem.FindControl("ddlChild"), RadComboBox)
            ddlCurrent.SelectedValue = ddlEditAccess.SelectedValue
            ddlEditAccess.SelectedValue = ddlEditAccess.SelectedValue
            ddlChildDDL.SelectedValue = ddlEditAccess.SelectedValue
        Next
    End Sub
    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToPdf.Click
        'Dim getReportAsBytes As [Byte]()
        'getReportAsBytes = (New Logic.ManageSecurityReportGenerator).RenderReport("pdf", Session("User"), ddlGroups.SelectedValue.ToString, _
        '                                                                          ddlGroups.SelectedItem.Text, ddlModules.SelectedValue.ToString, _
        '                                                                          ddlModules.SelectedItem.Text, CType(Master.FindControl("UserSplitButton"), RadButton).Text, ddlTypes.SelectedItem.Text)
        'ExportReport("pdf", getReportAsBytes)
    End Sub
    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
        'Dim getReportAsBytes As [Byte]()
        'getReportAsBytes = (New Logic.ManageSecurityReportGenerator).RenderReport("excel", Session("User"), ddlGroups.SelectedValue.ToString, _
        '                                                                          ddlGroups.SelectedItem.Text, ddlModules.SelectedValue.ToString, _
        '                                                                          ddlModules.SelectedItem.Text, CType(Master.FindControl("UserSplitButton"), RadButton).Text, ddlTypes.SelectedItem.Text)
        'ExportReport("excel", getReportAsBytes)
    End Sub
    Private Sub ExportReport(ByVal ExportFormat As String, ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case ExportFormat.ToLower
            Case Is = "pdf"
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case "excel"
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
                'Case "WORD"
                '    strExtension = "doc"
                '    strMimeType = "application/vnd.ms-word"
            Case "csv"
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select
        Session("SSRS_FileExtension") = strExtension
        Session("SSRS_MimeType") = strMimeType
        Session("SSRS_ReportOutput") = getReportAsBytes
        'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        Dim script As String = String.Format("window.open('DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString())
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    End Sub
#End Region

    Protected Sub RadGrid1_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        'Paging to next page pulls a blank grid
        'Need this code in this event
        RadGrid1.DataSource = ViewState("getPages")
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        btnNew.Enabled = False
        btndelete.Enabled = False
    End Sub
End Class
