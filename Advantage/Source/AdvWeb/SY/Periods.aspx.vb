Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class Periods
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo
    Protected ResourceId As String
    Protected ModuleId As String
    Protected campusId, userId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As System.Web.UI.WebControls.Label
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblPeriodDate As System.Web.UI.WebControls.Label
    Protected WithEvents CalButton As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtPeriodDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents DateReqFieldValidator As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents drvTransactionDate As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents A1 As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents dlstHolidays As System.Web.UI.WebControls.DataList
    Protected WithEvents txtHolidayId As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblHolidayCode As System.Web.UI.WebControls.Label
    Protected WithEvents txtHolidayCode As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblHolidayDate As System.Web.UI.WebControls.Label
    Protected WithEvents txtHolidayDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblHolidayDescrip As System.Web.UI.WebControls.Label
    Protected WithEvents txtHolidayDescrip As System.Web.UI.WebControls.TextBox

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        ViewState("IsUserSa") = advantageUserState.IsUserSA

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not IsPostBack Then
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

            'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            BindDataList()

            '   bind an empty new PeriodInfo
            BindPeriodData(New PeriodInfo)

            '   initialize buttons
            InitButtonsForLoad()
            ' Header1.EnableHistoryButton(False)
        Else
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            InitButtonsForEdit()
        End If
        headerTitle.Text = Header.Title

    End Sub
    Private Sub BindDataList()

        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radstatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)

        '   bind Periods datalist
        If isUserSa Then
            dlstPeriods.DataSource = New DataView((New PeriodFacade).GetAllPeriods().Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        Else
            dlstPeriods.DataSource = New DataView((New PeriodFacade).GetAllPeriods(campusId).Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        End If
        dlstPeriods.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
        BuildTimeIntervalsDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

    End Sub
    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildTimeIntervalsDDL()
        '   bind the TimeIntervals DDL
        Dim ds As DataSet = (New StudentsAccountsFacade).GetAllTimeIntervals()

        With ddlStartTimeId
            .DataTextField = "TimeIntervalDescrip"
            .DataTextFormatString = "{0:hh:mm tt}"
            .DataValueField = "TimeIntervalId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

        With ddlEndTimeId
            .DataTextField = "TimeIntervalDescrip"
            .DataTextFormatString = "{0:hh:mm tt}"
            .DataValueField = "TimeIntervalId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub dlstPeriods_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstPeriods.ItemCommand

        '   get the PeriodId from the backend and display it
        GetPeriodId(e.CommandArgument)

        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.SetHiddenControlForAudit()
        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstPeriods, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()

        CommonWebUtilities.RestoreItemValues(dlstPeriods, e.CommandArgument)

    End Sub
    Private Sub BindPeriodData(ByVal Period As PeriodInfo)
        With Period
            chkIsInDB.Checked = .IsInDB
            txtPeriodId.Text = .PeriodId
            txtPeriodCode.Text = .Code
            If Not (Period.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = Period.StatusId
            txtPeriodDescrip.Text = .Description
            If Not (Period.CampGrpId = Guid.Empty.ToString) Then ddlCampGrpId.SelectedValue = Period.CampGrpId Else ddlCampGrpId.SelectedIndex = 0
            ConvertIntegerToCheckBoxListValues(cblMeetDays, .MeetDays)
            ddlStartTimeId.SelectedValue = .StartTimeId
            ddlEndTimeId.SelectedValue = .EndTimeId
            'cbxStartTimeAndEndTimeAreFixed.Checked = .StartTimeAndEndTimeAreFixed
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If Not IsDataValidForSave() Then
            DisplayErrorMessage(GetErrorMessageForSave())
            Exit Sub
        End If

        '   instantiate component
        Dim result As String
        With New PeriodFacade
            '   update Period Info 
            result = .UpdatePeriodInfo(BuildPeriodInfo(txtPeriodId.Text), Session("UserName"))
        End With

        '   bind datalist
        BindDataList()

        '   set Style to Selected Item
        ' CommonWebUtilities.SetStyleToSelectedItem(dlstPeriods, txtPeriodId.Text, ViewState, Header1)

        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            '   get the PeriodId from the backend and display it
            GetPeriodId(txtPeriodId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new PeriodInfo
            'BindPeriodData(New PeriodInfo)

            '   initialize buttons
            'InitButtonsForLoad()
            InitButtonsForEdit()

        End If
        CommonWebUtilities.RestoreItemValues(dlstPeriods, txtPeriodId.Text)
    End Sub
    Private Function BuildPeriodInfo(ByVal PeriodId As String) As PeriodInfo
        'instantiate class
        Dim PeriodInfo As New PeriodInfo

        With PeriodInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get PeriodId
            .PeriodId = PeriodId

            'get Code
            .Code = txtPeriodCode.Text.Trim

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'get Period's name
            .Description = txtPeriodDescrip.Text.Trim

            'get Campus Group
            .CampGrpId = ddlCampGrpId.SelectedValue

            'get All Day ?
            .MeetDays = ConvertCheckBoxListValuesToInteger(cblMeetDays)

            'get StartTimeId
            .StartTimeId = ddlStartTimeId.SelectedValue

            'get EndTimeId
            .EndTimeId = ddlEndTimeId.SelectedValue

            'get StartTimeAndEndTimeAreFixed
            '.StartTimeAndEndTimeAreFixed = cbxStartTimeAndEndTimeAreFixed.Checked

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With
        'return data
        Return PeriodInfo
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        '   bind an empty new PeriodInfo
        BindPeriodData(New PeriodInfo)

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstPeriods, Guid.Empty.ToString, ViewState, Header1)

        'initialize buttons
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlstPeriods, Guid.Empty.ToString)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        If Not (txtPeriodId.Text = Guid.Empty.ToString) Then

            'update Period Info 
            Dim result As String = (New PeriodFacade).DeletePeriodInfo(txtPeriodId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   bind datalist
                BindDataList()

                '   bind an empty new PeriodInfo
                BindPeriodData(New PeriodInfo)

                '   initialize buttons
                InitButtonsForLoad()
            End If
            CommonWebUtilities.RestoreItemValues(dlstPeriods, Guid.Empty.ToString)
        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        btndelete.Enabled = False

        'btnNew.Enabled = False
        'btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()

        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        'btnNew.Enabled = True
        'btnDelete.Enabled = True

        ''Set the Delete Button so it prompts the user for confirmation when clicked
        'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

    End Sub
    Private Sub GetPeriodId(ByVal PeriodId As String)

        '   bind Period properties
        BindPeriodData((New PeriodFacade).GetPeriodInfo(PeriodId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        InitButtonsForLoad()
        BindPeriodData(New PeriodInfo)
        '   bind datalist
        BindDataList()
    End Sub
    Private Function IsDataValidForSave() As Boolean

        If ConvertCheckBoxListValuesToInteger(cblMeetDays) = 0 Then Return False

        Dim anyTimeIsNull = ddlStartTimeId.SelectedValue = Guid.Empty.ToString Or ddlEndTimeId.SelectedValue = Guid.Empty.ToString
        If anyTimeIsNull Then Return False

        If Not anyTimeIsNull Then
            If Date.Parse(ddlStartTimeId.SelectedItem.Text).CompareTo(Date.Parse(ddlEndTimeId.SelectedItem.Text)) >= 0 Then Return False
        End If

        Return True
    End Function
    Private Function GetErrorMessageForSave() As String
        Dim errorMessage As String = ""

        If ConvertCheckBoxListValuesToInteger(cblMeetDays) = 0 Then
            errorMessage += "No day selected. You must select at least one day." + vbCrLf
        End If

        Dim anyTimeIsNull = ddlStartTimeId.SelectedValue = Guid.Empty.ToString Or ddlEndTimeId.SelectedValue = Guid.Empty.ToString
        If anyTimeIsNull Then
            errorMessage += "You must select a 'Start Time' and 'End Time'." + vbCrLf
        End If

        If Not anyTimeIsNull Then
            If Date.Parse(ddlStartTimeId.SelectedItem.Text).CompareTo(Date.Parse(ddlEndTimeId.SelectedItem.Text)) >= 0 Then
                errorMessage += "'Start Time' must be earlier than 'End Time'." + vbCrLf
            End If
        End If

        Return errorMessage
    End Function
    Private Function ConvertCheckBoxListValuesToInteger(ByVal cbl As CheckBoxList) As Integer
        Dim sum As Integer = 0
        For i As Integer = 0 To cbl.Items.Count - 1
            If cbl.Items(i).Selected Then sum += PowerOfTwo(i)
        Next

        Return sum

    End Function
    Private Sub ConvertIntegerToCheckBoxListValues(ByVal cbl As CheckBoxList, ByVal cblValue As Integer)
        For i As Integer = cbl.Items.Count - 1 To 0 Step -1
            Dim l As Boolean = cblValue >= PowerOfTwo(i)
            cbl.Items(i).Selected = l
            If l Then cblValue -= PowerOfTwo(i)
        Next
    End Sub
    Private Function PowerOfTwo(ByVal power As Integer) As Integer
        PowerOfTwo = 1
        If power = 0 Then Return PowerOfTwo
        For i As Integer = 1 To power
            PowerOfTwo = PowerOfTwo * 2
        Next
        Return PowerOfTwo
    End Function
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

    Protected Sub dlstHolidays_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstHolidays.ItemCommand

    End Sub
End Class
