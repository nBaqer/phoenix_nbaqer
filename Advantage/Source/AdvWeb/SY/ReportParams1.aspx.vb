
Imports System.Web.UI
Imports System.Diagnostics
Imports FAME.Common
Imports System.IO
Imports Microsoft.VisualBasic
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports FAME.Advantage.Reporting
Imports System.Data
Imports FAME.AdvantageV1.DataAccess
Imports System.Reflection
Imports CrystalDecisions.CrystalReports.Engine
Imports AdvWeb.VBCode.ComponentClasses

Partial Class ReportParams1
    Inherits BasePage
    Protected WithEvents PnlSavedPrefs As Panel
    Protected WithEvents Btnhistory As Button
    Protected WithEvents LblSkillGroups As Label
    Protected WithEvents LblStudentSkills As Label
    Protected WithEvents Lbl3 As Label
    Protected WithEvents Panel2 As Panel
    Protected WithEvents TblRequiredFields As Table
    Protected WithEvents PnlHideSort As Panel
    Protected WithEvents PnlHideFilter As Panel
    Protected PageTitle As String = "Report Parameters - "
    Protected BoolShowErr As Boolean = False
    Protected ResourceId As Integer
    Protected CampusId As String
    Dim userId As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim objCommon As New CommonUtilities
        Dim rptFac As New ReportFacade
        Dim rptInfo As ReportInfo
        'Dim fac As New UserSecurityFacade

        'Dim testStr As String = CType(Page.Master.FindControl("CampusSelector"), Telerik.Web.UI.RadComboBox).SelectedValue
        'Set the Delete button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")


        Dim mContext As HttpContext

        Dim advantageUserState As User = AdvantageSession.UserState

        'Dim majorMinor As String = GetSettingString("MajorsMinorsConcentrations").ToUpper()
        'If majorMinor = "YES" Then
        '    btnSearch.Visible = False
        '    ddlExportTo.SelectedIndex = 0
        '    ddlExportTo.Enabled = False
        'End If

        'Header1.EnableHistoryButton(False)

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        CampusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        Session("UserId") = userId

        txtResId.Value = ResourceId.ToString

        mContext = HttpContext.Current
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try


        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(ResourceId, String), CampusId)
        'Check if this page still exists in the menu while switching campus
        'Dim boolSwitchCampus As Boolean = False
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        'txtPrefName.BackColor = Color.FromName("#ffff99")

        ResourceId = CType(txtResId.Value.Trim, Integer)
        'Dim contentMain As ContentPlaceHolder = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder)
        If ResourceId = 247 Or ResourceId = 249 Or
                ResourceId = 245 Or ResourceId = 274 Or ResourceId = 267 Or
                ResourceId = 202 Or ResourceId = 485 Or ResourceId = 489 Or
                ResourceId = 490 Or ResourceId = 554 Or ResourceId = 555 Or
                ResourceId = 556 Or ResourceId = 560 Or ResourceId = 557 Or ResourceId = 550 Then
            btnSearch.Attributes.Add("onclick", "if(confirm('This report may take several minutes to produce results. Do you want to proceed?')){}else{return false}")
            btnExport.Attributes.Add("onclick", "if(confirm('This report may take several minutes to produce results. Do you want to proceed?')){}else{return false}")
            btnFriendlyPrint.Attributes.Add("onclick", "if(confirm('This report may take several minutes to produce results. Do you want to proceed?')){}else{return false}")
            'Added By Vijay Ramteke on 09-April-2009
        ElseIf ResourceId = 583 Then
            btnPerformDataDump.Attributes.Add("onclick", "if(confirm('This report may take several minutes to produce results. Do you want to proceed?')){}else{return false}")
        End If

        If ResourceId = 489 Or ResourceId = 490 Or ResourceId = 238 Then
            btnFriendlyPrint.Enabled = False
        End If

        If pObj Is Nothing Then
            'Send the user to the standard error page
            Session("Error") = "You do not have the necessary permissions to access to this report."
            Response.Redirect("../ErrorPage.aspx")
        End If

        ' make sure the Session variable for Reporting Agency is cleared
        Session("RptAgency") = Nothing

        If ResourceId = 554 Or ResourceId = 555 Then
            pnl6.Visible = True
            LblMinimumAttendance.Visible = True
            txtMinimumAttendance.Visible = True
        End If



        If ResourceId = 633 Then
            pnl7.Visible = True
            lblNumberConsecutiveAbsentDays.Visible = True
            ddlConsAbsCompOp.Visible = True
            txtNumberConsecutiveAbsentDays.Visible = True
            If Not IsPostBack Then
                txtNumberConsecutiveAbsentDays2.Visible = False
            End If
            lblUsedScheduleDays.Visible = True
            chkUsedScheduleDays.Visible = True
        End If

        If Not Page.IsPostBack Then

            If ResourceId = 554 Then
                pnl6.Visible = True
                LblMinimumAttendance.Visible = True
                txtMinimumAttendance.Visible = True
                txtMaxAttendance.Visible = True
            ElseIf ResourceId = 560 Then
                pnl6.Visible = True
                LblMinimumAttendance.Visible = True
                txtMinimumAttendance.Visible = True
                txtMaxAttendance.Visible = False
                lblMaxAttendance.Visible = False
                LblMinimumAttendance.Text = "Minimum Score (%)"
            Else
                pnl6.Visible = False
                LblMinimumAttendance.Visible = False
                txtMinimumAttendance.Visible = False
                lblMaxAttendance.Visible = False
                txtMaxAttendance.Visible = False
            End If

            If ResourceId = 550 Then
                lblTermStartCutOff.Visible = True
                txtTermStart.Visible = True
                Panel1.Visible = True
                rdolstChkUnits.Visible = True
                pnlRow.Visible = True
                chkStudentSign.Visible = True
                chkSchoolSign.Visible = True
                pnlSign.Visible = True
                ChkTermModule.Visible = True
                pnlTotalCost.Visible = True
                ChkTotalCost.Visible = True
                ChkCurrentBalance.Visible = True
            End If

            If ResourceId = 629 Then
                lblTermStartCutOff.Visible = False
                txtTermStart.Visible = False
                'Panel1.Visible = False
                txtTermStart.Text = Date.Now.ToShortDateString()
                rdolstChkUnits.Visible = True
                pnlRow.Visible = True

                chkStudentSign.Visible = True
                chkSchoolSign.Visible = True
                'pnlSign.Visible = False

                ChkTermModule.Visible = True
                ChkTermModule.Enabled = False
                ChkTermModule.Checked = False
                chkStudentSign.Text = "Show Registrar Official signature line"
                chkOfficialTitle.Visible = True
                lblGrp.Visible = False
                rdolstChkUnits.Items(0).Text = "Group work units by subject"
                rdolstChkUnits.Items(1).Text = "Group work units by type"
                tblStudentSearch.Visible = True

                pnlSearch.Visible = True
                pnlSign.Visible = True
                pnlRow.Visible = True
                pnlRow.Visible = True
                pnlWorkUnits.Visible = True
                pnlOfficialTitle.Visible = True
            End If
            'to enable the Suppress Header for Single and MultipleTranscripts
            If ResourceId = 238 Or ResourceId = 245 Then
                chkRptSuppressHeader.Enabled = True
                ChkTermModule.Visible = True
                'DE8390 3/7/2013 Janet Robinson
                chkShowOffTrans.Enabled = True
                chkShowOffTrans.Visible = True
            Else
                chkShowOffTrans.Enabled = False
                chkShowOffTrans.Visible = False
            End If

            'to enable the Group By Class in Multiple Progress Reports
            If ResourceId = 550 And MyAdvAppSettings.AppSettings("EnableClassGroupingOnProgressreport").ToLower = "yes" Then
                ChkRptGroupByClass.Enabled = True
            End If
            pnl4.Visible = False
            pnl5.Visible = False
            'Bind the DataList on the lhs to any preferences saved for this report.
            BindDataListFromDB()

            ViewState("resid") = Request.Params("resid")

            rptInfo = rptFac.GetReportInfo(CType(ViewState("resid"), String))
            ViewState("SQLID") = rptInfo.SqlId
            ViewState("OBJID") = rptInfo.ObjId
            ViewState("RESURL") = rptInfo.Url
            ViewState("Resource") = rptInfo.Resource

            If MyAdvAppSettings.AppSettings("ShowStudentSchedulingGroup").ToLower = "yes" Then
                chkRptStudentGroup.Visible = True
            Else
                chkRptStudentGroup.Visible = False
            End If

            'Populate all the filter sections.
            BuildAllSections()


            'Set the mode to NEW
            'SetMode("NEW")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            'Set page title.
            PageTitle &= CType(ViewState("Resource"), String)

            lblReportName.Text = PageTitle

            'testStr = CType(Page.Master.FindControl("CampusSelector"), Telerik.Web.UI.RadComboBox).SelectedValue
            'Page.DataBind()
            'Dim testStr As String= CType(Page.Master.FindControl("CampusSelector"), Telerik.Web.UI.RadComboBox).SelectedValue
            If ResourceId = 550 Or ResourceId = 629 Then
                lblRequiredFilters.Text = lblRequiredFilters.Text & ", Term Start Cutoff or Term"
                ''Added by Saraswathi lakshmanan on oct 2009
                ''For lead Conversion Report
            ElseIf ResourceId = 353 Then
                lblRequiredFilters.Text = lblRequiredFilters.Text & ", Expected Start Date or Term"
            End If

            If ResourceId = 580 Or ResourceId = 600 Then
                pnlFirstNameLastName.Visible = True
                'pnlDateBetween.Visible = True
                With ddlTermId
                    .DataSource = (New TermsFacade).GetAllTermsByUser(False, CampusId, CType(HttpContext.Current.Session("UserName"), String), userId)
                    .DataTextField = "TermDescrip"
                    .DataValueField = "TermId"
                    .DataBind()
                    .Items.Insert(0, New ListItem("Select", ""))
                    .SelectedIndex = 0
                End With
                With ddlPrgVerId
                    .DataSource = (New ProgVerFacade).GetProgVersionsByUser(CampusId, CType(HttpContext.Current.Session("UserName"), String), userId)
                    '.DataTextField = "PrgVerDescrip"
                    ''Modified by saraswathi to show the programversionShiftDescription
                    ''Changed on may 19 2009
                    .DataTextField = "PrgVerShiftDescrip"
                    .DataValueField = "PrgVerid"
                    .DataBind()
                    .Items.Insert(0, New ListItem("Select", ""))
                    .SelectedIndex = 0
                End With
                With ddlCampGrpId
                    If HttpContext.Current.Session("UserName") = "sa" Then
                        .DataSource = (New CampusGroupsFacade).GetAllCampusGroups()
                    Else
                        .DataSource = (New CampusGroupsFacade).GetAllCampusGroupsByUser(userId)
                    End If

                    .DataTextField = "CampGrpDescrip"
                    .DataValueField = "CampGrpId"
                    .DataBind()
                    .Items.Insert(0, New ListItem("Select", ""))
                    .SelectedIndex = 0
                End With
                lblFilter.Visible = False
                lblValues.Visible = False
                lbxFilterList.Visible = False
                lbxSelFilterList.Visible = False
                pnlFilterValues.Visible = False
                pnlReportingFilters.Visible = True
                lblAdditionalInfo.Text = "Step 2: Additional information to print on the report:"
                lblStep4.Text = "Step 3:"
                pnl5.Visible = True
                lblRequiredFilters.Text = "Campus Group"
            End If

        Else
            'Rebuild the dynamic table and dynamic validators each time there is a postback.
            If Not (ResourceId = 580 Or ResourceId = 600) Then
                BuildFilterOther()
            End If

        End If
    End Sub

    Private Sub BuildAllSections()
        Dim dt2 As DataTable
        Dim dtMasks As DataTable
        Dim dtOperators As DataTable
        Dim objRptParams As New RptParamsFacade
        Dim objCommon As New CommonUtilities
        Dim dr As DataRow
        Dim ds As DataSet
        Dim numRows As Integer
        'Dim numCols As Integer
        Dim rValues As Integer

        'Get the parameters for this report
        ds = objRptParams.GetRptParams

        'Set status of buttons and checkboxes.
        If ds.Tables.Count = 2 Then
            Dim row As DataRow = ds.Tables("RptProps").Rows(0)
            If Not row.IsNull("AllowParams") Then
                If Not row("AllowParams") Then
                    'Report has no params, then disable Save, New and Delete buttons.
                    btnAdd.Enabled = False
                    btnRemove.Enabled = False
                    btnsave.Enabled = False
                    btnnew.Enabled = False
                    btndelete.Enabled = False
                    'Filters checkbox.
                    chkRptFilters.Enabled = False
                    'Sort checkbox.
                    chkRptSort.Enabled = False
                End If
            End If
            'Filter Criteria checkbox.
            If Not row.IsNull("AllowFilters") Then chkRptFilters.Enabled = CType(row("AllowFilters"), Boolean)
            'Sort Order checkbox.
            If Not row.IsNull("AllowSortOrder") Then chkRptSort.Enabled = CType(row("AllowSortOrder"), Boolean)
            'Selected Filter Criteria checkbox.
            If Not row.IsNull("SelectFilters") Then chkRptFilters.Checked = CType(row("SelectFilters"), Boolean)
            'Selected Sort Order checkbox.
            If Not row.IsNull("SelectSortOrder") Then chkRptSort.Checked = CType(row("SelectSortOrder"), Boolean)
            'Description checkbox.
            If Not row.IsNull("AllowDescrip") Then chkRptDescrip.Enabled = CType(row("AllowDescrip"), Boolean)
            'Instructions checkbox.
            If Not row.IsNull("AllowInstruct") Then chkRptInstructions.Enabled = CType(row("AllowInstruct"), Boolean)
            'Notes checkbox.
            If Not row.IsNull("AllowNotes") Then chkRptNotes.Enabled = CType(row("AllowNotes"), Boolean)
            'Student Group checkbox.
            If Not row.IsNull("AllowStudentGroup") Then chkRptStudentGroup.Enabled = CType(row("AllowStudentGroup"), Boolean)
            'Date Issue checkbox.
            If Not row.IsNull("AllowDateIssue") Then chkRptDateIssue.Enabled = CType(row("AllowDateIssue"), Boolean)

            If Not row.IsNull("AllowProgramGroup") Then ChkRptProgramVersion.Enabled = CType(row("AllowProgramGroup"), Boolean)

            If Not row.IsNull("ShowProjExceedsBal") Then ChkRptProjectionsExceed.Enabled = CType(row("ShowProjExceedsBal"), Boolean)

            If Not row.IsNull("ShowEnrollmentGroup") Then ChkRptShowGroupByEnrollment.Enabled = CType(row("ShowEnrollmentGroup"), Boolean)
            If Not row.IsNull("ShowDisbNotBeenPaid") Then ChkRptShowDisbNotBeenPaid.Enabled = CType(row("ShowDisbNotBeenPaid"), Boolean)

            If Not row.IsNull("ShowLegalDisclaimer") Then ChkShowLegalDisc.Enabled = CType(row("ShowLegalDisclaimer"), Boolean)
            If Not row.IsNull("ShowTermProgressDescription") Then chkshowTermProgressDescription.Enabled = CType(row("ShowTermProgressDescription"), Boolean)
            If Not row.IsNull("BaseCAOnRegClasses") Then ChkBaseCAOnRegClasses.Enabled = CType(row("BaseCAOnRegClasses"), Boolean)
            If Not row.IsNull("ShowUseStuCurrStatus") Then chkRptUseStuCurrStatus.Enabled = CType(row("ShowUseStuCurrStatus"), Boolean)
            If Not row.IsNull("ShowUseSignLineForAttnd") Then chkUseSignatureForAttendance.Enabled = CType(row("ShowUseSignLineForAttnd"), Boolean)

            Dim transcriptTypeString As String = CType(MyAdvAppSettings.AppSettings("TranscriptType").ToLower, String)
            If transcriptTypeString = "traditional_numeric" Then
                chkRptDateIssue.Enabled = False
            End If
            If CInt(txtResId.Value.Trim) = 238 Or CInt(txtResId.Value.Trim) = 245 Then
                If transcriptTypeString = "traditional_b" Then
                    ChkRptShowClassDates.Enabled = True
                    chkUseSignatureForAttendance.Checked = True
                    chkShowOffTrans.Checked = True
                    chkShowOffTrans.Enabled = True
                    chkShowOffTrans.Visible = True
                Else
                    ChkRptShowClassDates.Enabled = False
                    chkUseSignatureForAttendance.Enabled = False
                    'DE8390 3/7/2013 Janet Robinson
                    chkShowOffTrans.Visible = False
                    chkShowOffTrans.Enabled = False
                End If
            End If
        End If

        'Get the input masks
        dtMasks = objCommon.GetInstInputMasks.Copy
        dtMasks.TableName = "InputMasks"
        ds.Tables.Add(dtMasks)

        With ds.Tables("InputMasks")
            .PrimaryKey = New DataColumn() { .Columns("FldId")}
        End With
        ''Added by Saraswathi Jan 16 2009
        ''If condition inserted by saraswathi to have a dropdown list containing the number of days from 1 to 7

        'Get the comparison operators
        dtOperators = objCommon.GetComparisonOps.Copy
        dtOperators.TableName = "Operators"
        ds.Tables.Add(dtOperators)
        'End If

        If ViewState("resid") = 275 Or ViewState("resid") = 245 Then
            'Student Account Balance Report & Transcript Multiple Student
            'Remove the following items from the operator dropdown
            '5 - InList or 7 - Like or 8 - Empty or 9 - IsNull
            'Added by Balaji on 02/02/2009
            For Each drOpr As DataRow In ds.Tables("Operators").Rows
                If CType(drOpr("CompOpId"), Integer) = 5 Or
                    CType(drOpr("CompOpId"), Integer) = 7 Or
                    CType(drOpr("CompOpId"), Integer) = 8 Or
                    CType(drOpr("CompOpId"), Integer) = 9 Then
                    drOpr.Delete()
                End If
            Next
            ds.AcceptChanges()
        End If

        If ViewState("resid") = 606 Then
            'Student GPA Report
            'Remove all but Between Operator
            'Added by Dennis on 8/18/2009
            For Each drOpr As DataRow In ds.Tables("Operators").Rows
                If CType(drOpr("CompOpId"), Integer) = 1 Or
                    CType(drOpr("CompOpId"), Integer) = 2 Or
                    CType(drOpr("CompOpId"), Integer) = 3 Or
                    CType(drOpr("CompOpId"), Integer) = 4 Or
                    CType(drOpr("CompOpId"), Integer) = 5 Or
                    CType(drOpr("CompOpId"), Integer) = 7 Or
                    CType(drOpr("CompOpId"), Integer) = 8 Or
                    CType(drOpr("CompOpId"), Integer) = 9 Then
                    drOpr.Delete()
                End If
            Next
            ds.AcceptChanges()
        End If

        'Add a dt to store the possible values for each parameter that belongs to the
        'FilterList section. For example, we want to be able to store the possible values
        'for Program or Status.
        ds.Tables.Add("FilterListValues")

        With ds.Tables("FilterListValues")
            .Columns.Add("RptParamId", GetType(Integer))
            .Columns.Add("ValueFld", GetType(String))
            .Columns.Add("DisplayFld", GetType(String))
            .Columns.Add("FldName", GetType(String))
            .Columns.Add("CampgrpId", GetType(String))
        End With


        'Add a dt to store the values selected for each parameter that belongs to the
        'FilterList section. For example, if the user selects Active and Attending for
        'Status we want to store these items.
        ds.Tables.Add("FilterListSelections")
        With ds.Tables("FilterListSelections")
            .Columns.Add("RptParamId", GetType(Integer))
            .Columns.Add("FldValue", GetType(String))
            .Columns.Add("FldName", GetType(String))
            .PrimaryKey = New DataColumn() { .Columns("RptParamId"), .Columns("FldValue")}
        End With

        '' New Code Added By Vijay Ramteke On August 11, 2010 For Mantis Id 18560
        Dim ir As Integer = 0
        '' New Code Added By Vijay Ramteke On August 11, 2010 For Mantis Id 18560

        'that it belongs to.
        For Each dr In ds.Tables("RptParams").Rows
            If dr("SortSec") = "True" Then
                'Add the field info to the lbxSort listbox
                lbxSort.Items.Add(New ListItem(CType(dr("Caption"), String), CType(dr("RptParamId"), String)))
            End If


            If dr("FilterListSec") = "True" Then
                'Add the field info to the lbxFilterList listbox.
                'Troy:4/30/2008:
                'We want to display the student group filter ONLY when the user is requesting
                lbxFilterList.Items.Add(New ListItem(CType(dr("Caption"), String), CType(dr("RptParamId"), String)))

                '' New Code Added By Vijay Ramteke On August 11, 2010 For Mantis Id 18560
                ir = ir + 1
                If dr("Caption") = "Student Group" And (ResourceId = 247 Or ResourceId = 249) And Not MyAdvAppSettings.AppSettings("ShowStudentSchedulingGroup").ToLower = "yes" Then
                    ViewState("ItemRemove") = ir
                    ''New Code Added By Vijay Ramteke On September 08, 2010 For Mantis Id 19639
                ElseIf dr("Caption") = "Student Group" And (ResourceId = 247 Or ResourceId = 249) And MyAdvAppSettings.AppSettings("ShowStudentSchedulingGroup").ToLower = "yes" Then
                    Dim hasStudentGroupForScheduling As Boolean
                    hasStudentGroupForScheduling = objRptParams.HasStudentGroupForScheduling()
                    If hasStudentGroupForScheduling = False Then
                        ViewState("ItemRemove") = ir
                    End If
                    ''New Code Added By Vijay Ramteke On September 08, 2010 For Mantis Id 19639
                End If
                '' New Code Added By Vijay Ramteke On August 11, 2010 For Mantis Id 18560

                'Get the list of possible values for this item
                dt2 = objRptParams.GetFilterListValues(dr("DDLId"), True, userId)


                numRows = dt2.Rows.Count
                'numCols = dt2.Columns.Count

                For rValues = 0 To numRows - 1
                    'Create a new row for the FilterListValues dt
                    Dim dRow1 As DataRow = ds.Tables("FilterListValues").NewRow
                    dRow1("RptParamId") = dr("RptParamId")
                    dRow1("ValueFld") = dt2.Rows(rValues)(0)
                    dRow1("DisplayFld") = dt2.Rows(rValues)(1)
                    dRow1("FldName") = dr("FldName")
                    dRow1("CampGrpId") = dt2.Rows(rValues)(2)
                    'Add the new row to the FilterListValues dt
                    ds.Tables("FilterListValues").Rows.Add(dRow1)
                Next

                '' New Code Added By Vijay Ramteke On August 11, 2010 For Mantis Id 18560
                If dr("Caption") = "Student Group" And (ResourceId = 247 Or ResourceId = 249) And Not MyAdvAppSettings.AppSettings("ShowStudentSchedulingGroup").ToLower = "yes" Then
                    lbxFilterList.Items.RemoveAt(DirectCast(ViewState("ItemRemove"), Integer) - 1)
                    ''New Code Added By Vijay Ramteke On September 08, 2010 For Mantis Id 19639
                ElseIf dr("Caption") = "Student Group" And (ResourceId = 247 Or ResourceId = 249) And MyAdvAppSettings.AppSettings("ShowStudentSchedulingGroup").ToLower = "yes" Then
                    Dim hasStudentGroupForScheduling As Boolean
                    hasStudentGroupForScheduling = objRptParams.HasStudentGroupForScheduling()
                    If hasStudentGroupForScheduling = False Then
                        lbxFilterList.Items.RemoveAt(DirectCast(ViewState("ItemRemove"), Integer) - 1)
                        chkRptStudentGroup.Enabled = False
                    End If
                    ''New Code Added By Vijay Ramteke On September 08, 2010 For Mantis Id 19639
                End If
                '' New Code Added By Vijay Ramteke On August 11, 2010 For Mantis Id 18560

                If dr("Required") = "True" Then
                    'Add a hidden textbox and a compare validator if this field is marked as 'Required'.
                    'This textbox need to be updated everytime it is selected or deselected.
                    Dim txt As New TextBox
                    Dim compVal As New CompareValidator

                    txt.ID = "txt" & dr("RptParamId").ToString()
                    txt.CssClass = "ToTheme2"
                    txt.Text = Guid.Empty.ToString

                    txt.Visible = False

                    compVal.ID = "compVal" & dr("RptParamId").ToString()
                    compVal.ControlToValidate = txt.ID
                    compVal.[Operator] = ValidationCompareOperator.NotEqual
                    compVal.ValueToCompare = Guid.Empty.ToString
                    compVal.Type = ValidationDataType.String
                    compVal.ErrorMessage = CType((dr("Caption") & " is a required filter."), String)
                    compVal.Display = ValidatorDisplay.None
                    If Not (ResourceId = 580 Or ResourceId = 600 Or ResourceId = 629) Then
                        compVal.ErrorMessage = CType((dr("Caption") & " is a required filter."), String)
                        compVal.Display = ValidatorDisplay.None
                    End If

                    pnlRequiredFieldValidators.Controls.Add(txt)
                    If Not (ResourceId = 580 Or ResourceId = 600 Or ResourceId = 629) Then
                        pnlRequiredFieldValidators.Controls.Add(compVal)
                    End If

                End If
                'End If

            End If
            '
            If dr("FilterOtherSec") = "True" Then
                pnl4.Visible = True
                'Add a row to the tblOthers table. This is a server control.
                Dim r As New TableRow
                Dim c0 As New TableCell
                Dim c1 As New TableCell
                Dim c2 As New TableCell
                Dim c3 As TableCell
                Dim c4 As TableCell
                Dim c5 As New TableCell
                Dim lbl As New Label
                Dim ddl As New DropDownList
                Dim txt As New TextBox
                Dim txt2 As New TextBox
                Dim fldType As String = dr("FldType").ToString
                Dim regExpVal As RegularExpressionValidator
                Dim reqFldVal As RequiredFieldValidator

                txt.ID = "txt" & dr("RptParamId").ToString()
                txt.CssClass = "textbox"        '"ToTheme2"
                txt.Width = Unit.Pixel(120)
                txt.ClientIDMode = ClientIDMode.Static
                If ViewState("resid") = 384 And dr("FldId") = 952 Then
                    ''txt.Text = Date.Now.Year
                End If
                If (ViewState("resid") = 551 Or ViewState("resid") = 555 Or ViewState("resid") = 556) And dr("FldId") = 65 Then
                    'If (ViewState("resid") = 551 Or ViewState("resid") = 554 Or ViewState("resid") = 555 Or ViewState("resid") = 556) And dr("FldId") = 65 Then
                    txt.Text = Date.Now.ToShortDateString
                End If

                txt2.ID = "txt2" & dr("RptParamId").ToString()
                txt2.CssClass = "textbox"        '"ToTheme2"
                txt2.Width = Unit.Pixel(120)
                txt2.Visible = False
                txt2.ClientIDMode = ClientIDMode.Static
                'txt.Width.Pixel(5)

                lbl.Text = CType(dr("Caption"), String)
                lbl.CssClass = "label"    '"ToTheme"
                c0.Controls.Add(lbl)

                ddl.ID = "ddl" & dr("RptParamId").ToString()
                ''ResourceId=329 No Show Students,495-SAP Check Results,229-Student Listing
                ''236-StudentListing For Dropped Courses
                ''335-Students not Scheduled
                ''330- Students with low GPA
                ''366- Summary of Absences
                ''245- Transcript of Multiple Students
                ''275- Student Account Balance
                ''513- Student with balance in multiple Enrollments
                ''516- Students with Credit Balance



                If (ViewState("resid") = 247 And dr("RptParamId") = 105) Or
                        (ViewState("resid") = 249 And dr("RptParamId") = 114) Or
                       (ViewState("resid") = 352 And dr("RptParamId") = 304) Or
                       (ViewState("resid") = 386 And dr("FldId") = 825) Or
                        (ViewState("resid") = 485 And dr("FldId") = 617) Or
                        (ViewState("resid") = 489 And dr("FldId") = 660) Or
                        (ViewState("resid") = 490 And dr("FldId") = 660) Or
                        (ViewState("resid") = 518 And dr("FldId") <> 65) Or
                        (ViewState("resid") = 551 And dr("FldId") = 65) Or
                        (ViewState("resid") = 555 And dr("FldId") = 65) Or
                        (ViewState("resid") = 557 And dr("FldId") = 65) Or
                        (ViewState("resid") = 556 And dr("RptParamId") = 1109) Or
                        (ViewState("resid") = 262 And dr("FldId") = 960) Or
                        (ViewState("resid") = 274 And dr("FldId") = 960) Or
                        (ViewState("resid") = 267 And dr("FldId") = 960) Or
                        (ViewState("resid") = 494 And dr("FldId") = 960) Or
                        (ViewState("resid") = 514 And dr("FldId") = 960) Or
                        (ViewState("resid") = 495 And dr("FldId") = 960) Or
                        (ViewState("resid") = 329 And dr("FldId") = 960) Or
                        (ViewState("resid") = 229 And dr("FldId") = 960) Or
                        (ViewState("resid") = 335 And dr("FldId") = 960) Or
                        (ViewState("resid") = 330 And dr("FldId") = 960) Or
                        (ViewState("resid") = 366 And dr("FldId") = 960) Or
                        (ViewState("resid") = 245 And dr("FldId") = 960) Or
                        (ViewState("resid") = 275 And dr("FldId") = 960) Or
                        (ViewState("resid") = 513 And dr("FldId") = 960) Or
                        (ViewState("resid") = 516 And dr("FldId") = 960) Then ' Or _
                    '(ViewState("resid") = 554 And (dr("FldId") = 65) Or dr("FldId") = 1071) Then

                    'Special cases: Student Aging Detail, 
                    '               Student Aging Summary,
                    '               WTD/MTD Admission Rep Performance, 
                    '               YTD Admission Rep Performance,
                    '               90/10 Report,
                    '               Projected Cash Flow,
                    '               Weekly Admissions Report,
                    '               Deferred Revenue Detail,
                    '               Deferred Revenue Summary
                    '               Sign-In Sheet
                    'These reports need a reference date; therefore, the operators drowdownlist can only contain one value.
                    'Equal To (=) operator
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
                ElseIf (ViewState("resid") = 554 And (dr("FldId") = 65) Or dr("FldId") = 1071) Then  'US3768
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                    'txt2.Text = Date.Now.ToShortDateString
                    'code added by priyanka on date 27th April 2009
                ElseIf (ViewState("resid") = 236 And dr("FldId") = 885) Or
                    (ViewState("resid") = 353 And dr("FldID") = 619) Or
                     (ViewState("resid") = 353 And dr("FldID") = 3) Or
         (ViewState("resid") = 351 And dr("RptParamId") = 303) Then ''Added By Saraswathi lakshmanan on oct 20 2009 to show between operator for Date range
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                ElseIf (ViewState("resid") = 245 And dr("FldId") = 960) Then
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
                    'New code Added By Vijay Ramteke on May, 11 2009
                ElseIf (ViewState("resid") = 245 And dr("FldId") = 65) Or (ViewState("resid") = 560 And dr("FldId") = 65) Or (ViewState("resid") = 325 And dr("FldId") = 889) Then
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                ElseIf ((ViewState("resid") = 229 And dr("RptParamId") = 1184) Or (ViewState("resid") = 229 And dr("TblName").ToString.ToLower = "systudentstatuschanges" And dr("FldName").ToString.ToLower = "studentstatuschangeid")) Then
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                    ''90/10 revenue ratio -- between operator added to the report
                    ''Added by Saraswathi lakshmanan on July 13 2009

                ElseIf (ViewState("resid") = 310 And dr("RptParamId") = 273) Then
                    'Special case: Expiring Financial Aid and Payment Plans
                    'These reports need a reference date; therefore, the operators drowdownlist can only contain EQUAL TO and BETWEEN.
                    'Equal To (=) and Between operators
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.EqualToOperatorValue & " OR CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                    ''Added by saraswathi, since, it was delelted
                    ''Added by Vijay Ramteke on 09-April-2009
                    ''   ElseIf (ViewState("resid") = 583 And dr("RptParamId") = 1129) Then
                    ''Modified by Saraswathi Lakshmanan the Rptparamid is replaced with FldId
                    '' Changes Made by Kamalesh Ahuja on June 21st for revenue Ratio Report
                ElseIf (ViewState("resid") = 384 And dr("FldId") = 952) Or (ViewState("resid") = 632 And dr("FldId") = 952) Or (ViewState("resid") = 636 And dr("FldId") = 952) Then
                    ddl.DataSource = New DataView(ds.Tables("Operators"), " CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                    ''''
                ElseIf (ViewState("resid") = 583 And dr("Fldid") = 842) Then
                    'Special case: Expiring Financial Aid and Payment Plans
                    'These reports need a reference date; therefore, the operators drowdownlist can only contain EQUAL TO and BETWEEN.
                    'Equal To (=) and Between operators
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                    btnSearch.Visible = False
                    ddlExportTo.Enabled = False
                    btnExport.Enabled = False
                    btnPerformDataDump.Visible = True
                    'btnExportToExcel.Visible = True
                    'btnExportToExcel.Enabled = False
                    ''Added by Vijay Ramteke on 09-April-2009
                    ''Added by Vijay Ramteke on JAN 22, 2010
                ElseIf (ViewState("resid") = 618 And dr("Fldid") = 842) Then
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                    ''Added by Vijay Ramteke on JAN 22, 2010
                ElseIf (ViewState("resid") = 325 And dr("RptParamId") = 272) Or
                        (ViewState("resid") = 262 And dr("FldId") = 65) Or
                        (ViewState("resid") = 274 And dr("FldId") = 65) Or
                        (ViewState("resid") = 267 And dr("FldId") = 65) Or
                        (ViewState("resid") = 515 And dr("FldId") = 3) Or
                        (ViewState("resid") = 560 And dr("FldId") = 65) Or
                        (ViewState("resid") = 556 And dr("RptParamId") = 1108) Or
    (ViewState("resid") = 591 And dr("FldId") = 65) Or
                            (ViewState("resid") = 589 And dr("FldId") = 1047) Or
                        (ViewState("resid") = 518 And dr("FldId") = 65) Or
                         (ViewState("resid") = 245 And dr("FldId") = 65) Or (ViewState("resid") = 560 And dr("FldId") = 65) Or (ViewState("resid") = 325 And dr("FldId") = 889) Then


                    'Special case: No Attendance Posted, Class Roster, Multiple Student Class Schedule, Class Schedule Summary,
                    'DroppedCoursesSummaryReport
                    'These reports need a date range; therefore, the operators drowdownlist can only contain one value.
                    'Between operator
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                ElseIf (ViewState("resid") = 560 And dr("RptParamId") = 960) Then
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)

                    ''Added for timeclock punches report
                    ''Addded by Saraswathi since, it was deleted
                ElseIf (ViewState("resid") = 586 And dr("FldId") = 1051) Or
                  (ViewState("resid") = 591 And dr("FldId") = 30) Then

                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
                    ''Added by Saraswathi Since, it was deleted
                ElseIf (ViewState("resid") = 586 And dr("FldId") = 1100) Or
    (ViewState("resid") = 591 And dr("FldId") = 1100) Then
                    dtOperators = createNoOfDaysTable()
                    dtOperators.TableName = "Operators1"
                    ds.Tables.Add(dtOperators)
                    ddl.DataSource = ds.Tables("Operators1")
                    txt.Visible = False
                    ''For Attendance History report- TimeClock
                ElseIf (ViewState("resid") = 589 And dr("FldId") = 1105) Then
                    dtOperators = createStartDayTable()
                    dtOperators.TableName = "Operators1"
                    ds.Tables.Add(dtOperators)
                    ddl.DataSource = ds.Tables("Operators1")
                    txt.Visible = False
                    ''Added for weekly Attendance report
                    ''Added for weekly Attendance report
                ElseIf (ViewState("resid") = 285 And dr("FldId") = 340) Then
                    'call  to get teh datasouce from the DB.
                    dtOperators = objRptParams.GetFilterListValues(dr("DDLId"), False, userId)
                    dtOperators.TableName = "Operators1"
                    ds.Tables.Add(dtOperators.Copy())
                    ddl.DataSource = ds.Tables("Operators1")
                    txt.Visible = False
                    ''Aid Received by Date
                ElseIf (ViewState("resid") = 588) Then
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
                    'Added for Student Listing enhancement by DD 
                ElseIf (ViewState("resid") = 229 And dr("FldId") = 1128) Then
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                    ''Added By Vijay Ramteke On July 10, 2010 For Mantis Id 06156
                ElseIf (ViewState("resid") = 634 And dr("FldName").ToString.ToLower = "enddate") Then
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                    ''Added By Vijay Ramteke On July 10, 2010 For Mantis Id 06156
                    ''Added By Vijay Ramteke On September 09, 2010 For Mantis Id 17901
                ElseIf (ViewState("resid") = 649 And dr("FldName").ToString.ToLower = "startdate") Then
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
                    ''Added By Vijay Ramteke On September 09, 2010 For Mantis Id 17901
                    ''Added By Vijay Ramteke On September 24, 2010 For Mantis Id 19129
                ElseIf (ViewState("resid") = 650 And dr("FldName").ToString.ToLower = "transdate") Then
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                    txt2.Visible = True
                    ''Added By Vijay Ramteke On September 24, 2010 For Mantis Id 19129
                Else
                    'Populate dropdownlist with operators relevant to the diferent data types.
                    Select Case dr("FldType")
                        Case "Datetime"
                            ddl.DataSource = New DataView(ds.Tables("Operators"), "AllowDateTime = 1", "", DataViewRowState.OriginalRows)

                        Case "Varchar", "Char"
                            ddl.DataSource = New DataView(ds.Tables("Operators"), "AllowString = 1", "", DataViewRowState.OriginalRows)

                        Case "Smallint", "Int", "TinyInt", "Float", "Decimal"
                            ddl.DataSource = New DataView(ds.Tables("Operators"), "AllowNumber = 1", "", DataViewRowState.OriginalRows)

                        Case Else
                            ddl.DataSource = ds.Tables("Operators")
                    End Select
                End If


                ddl.DataTextField = "CompOpText"
                ddl.DataValueField = "CompOpId"
                ddl.DataBind()
                ddl.CssClass = "dropdownlist"       '"ToTheme"
                ddl.AutoPostBack = True
                ddl.Width = Unit.Pixel(100)

                'If ViewState("resid") = 580 And (dr("RptParamId") = 1127 Or dr("RptParamId") = 1128) Then
                'Else
                c1.Controls.Add(ddl)
                'End If
                c2.Controls.Add(txt)
                c5.Controls.Add(txt2)


                r.Cells.Add(c0)
                'If ViewState("resid") = 580 And (dr("RptParamId") = 1127 Or dr("RptParamId") = 1128) Then
                'Else
                r.Cells.Add(c1)
                'End If
                r.Cells.Add(c2)
                r.Cells.Add(c5)

                If fldType = "Datetime" And ResourceId <> 245 Then
                    'Add a regular expression validator for Datetime fields.
                    'We can only accept dates with two slashes.
                    regExpVal = New RegularExpressionValidator
                    regExpVal.ID = "regExpVal" & dr("RptParamId").ToString()
                    regExpVal.ControlToValidate = txt.ID
                    regExpVal.ValidationExpression = "^\d{1,2}\/\d{1,2}\/\d{4}$"
                    'regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                    'regExpVal.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                    regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                    '& vbCr & "The character required to separate values is the semicolon (;)."
                    regExpVal.Display = ValidatorDisplay.None
                    c3 = New TableCell
                    c3.Controls.Add(regExpVal)
                    '
                    regExpVal = New RegularExpressionValidator
                    regExpVal.ID = "regExpVal2" & dr("RptParamId").ToString()
                    regExpVal.ControlToValidate = txt2.ID
                    regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                    'regExpVal.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                    regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                    '& vbCr & "The character required to separate values is the semicolon (;)."
                    regExpVal.Display = ValidatorDisplay.None
                    c3.Controls.Add(regExpVal)
                    r.Cells.Add(c3)
                End If

                If dr("Required") = "True" And ResourceId <> 629 Then
                    'Add a required field validator if this field is marked as 'Required'.
                    reqFldVal = New RequiredFieldValidator
                    reqFldVal.ID = "reqFldVal" & dr("RptParamId").ToString()
                    reqFldVal.ControlToValidate = txt.ID
                    reqFldVal.ErrorMessage = CType((dr("Caption") & " is a required filter."), String)
                    reqFldVal.Display = ValidatorDisplay.None
                    c4 = New TableCell
                    c4.Controls.Add(reqFldVal)
                    r.Cells.Add(c4)
                End If

                tblFilterOthers.Rows.Add(r)

                'If (ViewState("resid") = 229) And (dr("Caption") = "Enrollment Status Date Range") Then
                '    'Student Listing Report - For Enrollment Status Date Range Field
                '    'Select Between and lock the control
                '    'Added by Dennis on 8/27/2009
                '    Dim strDdlId As String = ddl.ID.ToString
                '    If ddl.ID = strDdlId Then
                '        ddl.SelectedIndex = 5
                '        ddl.Enabled = False
                '    End If
                'End If


            End If

            ' Show Required Filters panel.
            If dr("Required") = "True" And ResourceId <> 629 Then
                pnl5.Visible = True
                If lblRequiredFilters.Text = "" Then
                    lblRequiredFilters.Text = CType(dr("Caption"), String)
                Else
                    lblRequiredFilters.Text &= CType((", " & dr("Caption")), String)
                End If
            End If
        Next

        '' New Code Added By Vijay Ramteke On August 04, 2010 For Mantis Id 19447
        If ResourceId = 633 Then
            If lblRequiredFilters.Text = "" Then
                lblRequiredFilters.Text = "Consecutive Absent Days"
            Else
                lblRequiredFilters.Text &= ", " & "Consecutive Absent Days"
            End If
        End If
        '' New Code Added By Vijay Ramteke On August 04, 2010 For Mantis Id 19447

        'Add a 'None' item so that users can deselect an item without having to
        'select another one.
        If ResourceId = 554 Then
            lblRequiredFilters.Text = lblRequiredFilters.Text & ", Maximum Attendance (%)"
        End If
        If lbxFilterList.Items.Count > 0 Then
            lbxFilterList.Items.Insert(0, New ListItem("None", 0))
        End If

        'Troy: DE13274:SP 10 Reg Defect - Reports, Grade Book Results report missing field.
        If ResourceId = 560 Then
            If lblRequiredFilters.Text = "" Then
                lblRequiredFilters.Text = "Minimum Score (%)"
            Else
                lblRequiredFilters.Text &= ", " & "Minimum Score (%)"
            End If
        End If


        ''Added by saraswathi lakshmanan on may 07 2009
        ''Get the cmpGrpId which is mapped to 'All'
        ''Added to fix the issue 16041
        Dim dtCampGrpIdforAll As DataTable
        dtCampGrpIdforAll = objCommon.GetCampGrpIdforAll.Copy
        dtCampGrpIdforAll.TableName = "CmpGrpIdAll"
        ds.Tables.Add(dtCampGrpIdforAll)
        'Add the ds DataSet to session so we can reuse it later on.
        Session("WorkingDS") = ds
    End Sub

    Private Sub BuildFilterOther()
        Dim resID As Integer
        Dim dt, dt2 As DataTable
        Dim dtOperators As DataTable
        Dim ds As New DataSet
        Dim dtMasks As DataTable
        Dim dr As DataRow
        '     Dim dr1 As DataRow
        Dim objRptParams As New RptParams
        Dim objCommon As New CommonUtilities

        resID = CInt(ViewState("resid"))
        'Get the parameters for this report that belong to the FilterOther section.
        dt = objRptParams.GetRptParamsForFilterOther(resID)
        'Get the comparison operators
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("Operators")
        'Get the input masks
        dtMasks = DirectCast(Session("WorkingDS"), DataSet).Tables("InputMasks")

        If resID = 586 Then
            Dim dtParamView As New DataView(dt) 'dt should be the name of your datatable

            dtParamView.Sort = "FldType Asc" 'Put the column names in here

            dt = dtParamView.ToTable()
        End If


        'Loop through the rows in the dt and add each parameter. 
        For Each dr In dt.Rows
            'Add a row to the tblOthers table. This is a server control.
            Dim r As New TableRow
            Dim c0 As New TableCell
            Dim c1 As New TableCell
            Dim c2 As New TableCell
            Dim c3 As TableCell
            Dim c4 As TableCell
            Dim c5 As New TableCell
            Dim lbl As New Label
            Dim ddl As New DropDownList
            Dim txt As New TextBox
            Dim txt2 As New TextBox
            Dim fldType As String = dr("FldType").ToString
            Dim regExpVal As RegularExpressionValidator
            Dim reqFldVal As RequiredFieldValidator

            txt.ID = "txt" & dr("RptParamId").ToString()
            txt.CssClass = "textbox"           '"ToTheme2"
            txt.Width = Unit.Pixel(120)
            txt.ClientIDMode = ClientIDMode.Static
            If ViewState("resid") = 384 And dr("FldId") = 952 Then
                'txt.Text = Date.Now.Year
            End If

            txt2.ID = "txt2" & dr("RptParamId").ToString()
            txt2.CssClass = "textbox"           '"ToTheme2"
            txt2.Width = Unit.Pixel(120)
            txt2.Visible = False
            txt2.ClientIDMode = ClientIDMode.Static

            'txt.Width.Pixel(5)

            lbl.Text = CType(dr("Caption"), String)
            lbl.CssClass = "label"         '"ToTheme"
            c0.Controls.Add(lbl)

            ddl.ID = "ddl" & dr("RptParamId").ToString()

            If (ViewState("resid") = 247 And dr("RptParamId") = 105) Or
                    (ViewState("resid") = 249 And dr("RptParamId") = 114) Or
                     (ViewState("resid") = 352 And dr("RptParamId") = 304) Or
                    (ViewState("resid") = 386 And dr("FldId") = 825) Or
                    (ViewState("resid") = 485 And dr("FldId") = 617) Or
                    (ViewState("resid") = 489 And dr("FldId") = 660) Or
                    (ViewState("resid") = 490 And dr("FldId") = 660) Or
                    (ViewState("resid") = 262 And dr("FldId") = 960) Or
                    (ViewState("resid") = 267 And dr("FldId") = 960) Or
                    (ViewState("resid") = 274 And dr("FldId") = 960) Or
                    (ViewState("resid") = 275 And dr("FldId") = 960) Or
                    (ViewState("resid") = 494 And dr("FldId") = 960) Or
                    (ViewState("resid") = 513 And dr("FldId") = 960) Or
                    (ViewState("resid") = 514 And dr("FldId") = 960) Or
                    (ViewState("resid") = 516 And dr("FldId") = 960) Or
                    (ViewState("resid") = 591 And dr("FldId") = 30) Or
                    (ViewState("resid") = 247 And dr("RptParamId") = 105) Or
                       (ViewState("resid") = 249 And dr("RptParamId") = 114) Or
                      (ViewState("resid") = 352 And dr("RptParamId") = 304) Or
                      (ViewState("resid") = 386 And dr("FldId") = 825) Or
                       (ViewState("resid") = 485 And dr("FldId") = 617) Or
                       (ViewState("resid") = 489 And dr("FldId") = 660) Or
                       (ViewState("resid") = 490 And dr("FldId") = 660) Or
                       (ViewState("resid") = 518 And dr("FldId") <> 65) Or
                       (ViewState("resid") = 551 And dr("FldId") = 65) Or
                       (ViewState("resid") = 555 And dr("FldId") = 65) Or
                       (ViewState("resid") = 557 And dr("FldId") = 65) Or
                       (ViewState("resid") = 556 And dr("RptParamId") = 1109) Or
                       (ViewState("resid") = 262 And dr("FldId") = 960) Or
                       (ViewState("resid") = 274 And dr("FldId") = 960) Or
                       (ViewState("resid") = 267 And dr("FldId") = 960) Or
                       (ViewState("resid") = 494 And dr("FldId") = 960) Or
                       (ViewState("resid") = 514 And dr("FldId") = 960) Or
                       (ViewState("resid") = 495 And dr("FldId") = 960) Or
                       (ViewState("resid") = 329 And dr("FldId") = 960) Or
                       (ViewState("resid") = 229 And dr("FldId") = 960) Or
                       (ViewState("resid") = 335 And dr("FldId") = 960) Or
                       (ViewState("resid") = 330 And dr("FldId") = 960) Or
                       (ViewState("resid") = 366 And dr("FldId") = 960) Or
                       (ViewState("resid") = 245 And dr("FldId") = 960) Or
                       (ViewState("resid") = 275 And dr("FldId") = 960) Or
                       (ViewState("resid") = 513 And dr("FldId") = 960) Or
                       (ViewState("resid") = 516 And dr("FldId") = 960) Then  ' Or _
                '(ViewState("resid") = 554 And (dr("FldId") = 65) Or dr("FldId") = 1071) Then

                'Special cases: Student Aging Detail, 
                '               Student Aging Summary,
                '               WTD/MTD Admission Rep Performance, 
                '               YTD Admission Rep Performance,
                '               90/10 Report,
                '               Projected Cash Flow,
                '               Weekly Admissions Report,
                '               Deferred Revenue Detail,
                '               Deferred Revenue Summary.
                'These reports need a reference date; therefore, the operators drowdownlist can only contain one value.
                'Equal To (=) operator
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
                ''90/10 revenue ratio -- between operator added to the report
                ''Added by Saraswathi lakshmanan on July 13 2009
            ElseIf (ViewState("resid") = 310 And dr("RptParamId") = 273) Then
                'Special case: Expiring Financial Aid and Payment Plans
                'These reports need a reference date; therefore, the operators drowdownlist can only contain EQUAL TO and BETWEEN.
                'Equal To (=) and Between operators
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.EqualToOperatorValue & " OR CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                txt2.Visible = True
            ElseIf (ViewState("resid") = 649 And dr("FldName").ToString.ToLower = "startdate") Then
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
                ''Added by Saraswathi Lakshmanan on Oct 2009
                ''For Lead ConversionReports
                '' Changes Made by Kamalesh Ahuja on June 21st for revenue Ratio Report
            ElseIf (ViewState("resid") = 384 And dr("FldId") = 952) Or (ViewState("resid") = 632 And dr("FldId") = 952) Or (ViewState("resid") = 636 And dr("FldId") = 952) Or (ViewState("resid") = 247 And dr("RptParamId") = 105) Or
                      (ViewState("resid") = 249 And dr("RptParamId") = 114) Or
                     (ViewState("resid") = 352 And dr("RptParamId") = 304) Or
                     (ViewState("resid") = 386 And dr("FldId") = 825) Or
                      (ViewState("resid") = 485 And dr("FldId") = 617) Or
                      (ViewState("resid") = 489 And dr("FldId") = 660) Or
                      (ViewState("resid") = 490 And dr("FldId") = 660) Or
                      (ViewState("resid") = 518 And dr("FldId") <> 65) Or
                      (ViewState("resid") = 551 And dr("FldId") = 65) Or
                      (ViewState("resid") = 555 And dr("FldId") = 65) Or
                      (ViewState("resid") = 557 And dr("FldId") = 65) Or
                      (ViewState("resid") = 556 And dr("RptParamId") = 1109) Or
                      (ViewState("resid") = 262 And dr("FldId") = 960) Or
                      (ViewState("resid") = 274 And dr("FldId") = 960) Or
                      (ViewState("resid") = 267 And dr("FldId") = 960) Or
                      (ViewState("resid") = 494 And dr("FldId") = 960) Or
                      (ViewState("resid") = 514 And dr("FldId") = 960) Or
                      (ViewState("resid") = 495 And dr("FldId") = 960) Or
                      (ViewState("resid") = 329 And dr("FldId") = 960) Or
                      (ViewState("resid") = 229 And dr("FldId") = 960) Or
                      (ViewState("resid") = 335 And dr("FldId") = 960) Or
                      (ViewState("resid") = 330 And dr("FldId") = 960) Or
                      (ViewState("resid") = 366 And dr("FldId") = 960) Or
                      (ViewState("resid") = 245 And dr("FldId") = 960) Or
                      (ViewState("resid") = 275 And dr("FldId") = 960) Or
                      (ViewState("resid") = 513 And dr("FldId") = 960) Or
                      (ViewState("resid") = 516 And dr("FldId") = 960) Or
                      (ViewState("resid") = 554 And (dr("FldId") = 65) Or dr("FldId") = 1071) Then
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                txt2.Visible = True
                '''''''''''''''''''

            ElseIf (ViewState("resid") = 322 And dr("RptParamId") = 272) Or
               (ViewState("resid") = 351 And dr("RptParamId") = 303) Or
                       (ViewState("resid") = 262 And dr("FldId") = 65) Or
                       (ViewState("resid") = 274 And dr("FldId") = 65) Or
                       (ViewState("resid") = 267 And dr("FldId") = 65) Or
                       (ViewState("resid") = 515 And dr("FldId") = 3) Or
                       (ViewState("resid") = 591 And dr("FldId") = 65) Or
                       (ViewState("resid") = 353 And dr("FldId") = 619) Or
                        (ViewState("resid") = 353 And dr("FldId") = 3) Or
                          (ViewState("resid") = 245 And dr("FldId") = 65) Or
                            (ViewState("resid") = 560 And dr("FldId") = 65) Or
                             (ViewState("resid") = 325 And dr("FldId") = 889) Or
                                 (ViewState("resid") = 229 And dr("FldId") = 1118) Or
                       (ViewState("resid") = 589 And dr("FldId") = 1047) Then


                'Special case: No Attendance Posted, Class Roster, Multiple Student Class Schedule, Class Schedule Summary,
                'DroppedCourseSummaryReport.
                'These reports need a date range; therefore, the operators drowdownlist can only contain one value.
                'Between operator
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                txt2.Visible = True
            ElseIf (ViewState("resid") = 618 And dr("Fldid") = 842) Then
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                txt2.Visible = True
            ElseIf (ViewState("resid") = 650 And dr("FldName").ToString.ToLower = "transdate") Then
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                txt2.Visible = True
                ''Added for timeclock punches report
            ElseIf (ViewState("resid") = 634 And dr("FldName").ToString.ToLower = "enddate") Then
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                txt2.Visible = True

            ElseIf (ViewState("resid") = 586 And dr("FldId") = 1051) Or
            (ViewState("resid") = 591 And dr("FldId") = 30) Then

                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
            ElseIf (ViewState("resid") = 586 And dr("FldId") = 1100) Then
                dtOperators = createNoOfDaysTable()
                dtOperators.TableName = "Operators1"
                ds.Tables.Add(dtOperators)
                ddl.DataSource = ds.Tables("Operators1")
                txt.Visible = False



            ElseIf (ViewState("resid") = 591 And dr("FldId") = 1100) Or
            (ViewState("resid") = 589 And dr("FldId") = 1105) Then
                'ddl.DataSource = dt2
                ddl.DataSource = createStartDayTable()
                txt2.Visible = False
                txt.Visible = False
                ''Added for weekly Attendance Report
            ElseIf (ViewState("resid") = 285 And dr("FldId") = 340) Then
                'call  to get teh datasouce from the DB.
                dtOperators = objRptParams.GetFilterListValues(53, False, userId)
                dtOperators.TableName = "Operators1"
                ds.Tables.Add(dtOperators.Copy())
                ddl.DataSource = ds.Tables("Operators1")
                txt.Visible = False
                ''Aid Received by Date
            ElseIf (ViewState("resid") = 588) Then
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
            ElseIf (ViewState("resid") = 229 And dr("FldId") = 1128) Then
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)

                txt2.Visible = True
            ElseIf ((ViewState("resid") = 229 And dr("RptParamId") = 1184) Or (ViewState("resid") = 229 And dr("TblName").ToString.ToLower = "systudentstatuschanges" And dr("FldName").ToString.ToLower = "studentstatuschangeid")) Then
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)

                txt2.Visible = True
            ElseIf (ViewState("resid") = 583 And dr("Fldid") = 842) Then
                'Special case: Expiring Financial Aid and Payment Plans
                'These reports need a reference date; therefore, the operators drowdownlist can only contain EQUAL TO and BETWEEN.
                'Equal To (=) and Between operators
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                txt2.Visible = True
                btnSearch.Visible = False
                ddlExportTo.Enabled = False
                btnExport.Enabled = False
                btnPerformDataDump.Visible = True
                'btnExportToExcel.Visible = True
                'btnExportToExcel.Enabled = False
                ''Added by Vijay Ramteke on 09-April-2009
                ''Added by Vijay Ramteke on JAN 22, 2010
            Else
                'Populate dropdownlist with operators relevant to the diferent data types
                Select Case dr("FldType")
                    Case "Datetime"
                        ddl.DataSource = New DataView(dt2, "AllowDateTime = 1", "", DataViewRowState.OriginalRows)

                    Case "Varchar", "Char"
                        ddl.DataSource = New DataView(dt2, "AllowString = 1", "", DataViewRowState.OriginalRows)

                    Case "Smallint", "Int", "TinyInt", "Float", "Decimal"
                        ddl.DataSource = New DataView(dt2, "AllowNumber = 1", "", DataViewRowState.OriginalRows)

                    Case Else
                        ddl.DataSource = dt2
                End Select
            End If
            ddl.DataTextField = "CompOpText"
            ddl.DataValueField = "CompOpId"
            ddl.DataBind()
            ddl.CssClass = "dropdownlist"        '"ToTheme"
            ddl.Width = Unit.Pixel(100)
            ddl.AutoPostBack = True
            c1.Controls.Add(ddl)
            c2.Controls.Add(txt)
            c5.Controls.Add(txt2)

            r.Cells.Add(c0)
            r.Cells.Add(c1)
            r.Cells.Add(c2)
            r.Cells.Add(c5)

            If fldType = "Datetime" And ResourceId <> 245 Then
                'Add a regular expression validator for Datetime fields.
                'We can only accept dates with two slashes.
                regExpVal = New RegularExpressionValidator
                regExpVal.ID = "regExpVal" & dr("RptParamId").ToString()
                regExpVal.ControlToValidate = txt.ID
                regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                'regExpVal.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                '& vbCr & "The character required to separate values is the semicolon (;)."
                regExpVal.Display = ValidatorDisplay.None
                c3 = New TableCell
                c3.Controls.Add(regExpVal)

                regExpVal = New RegularExpressionValidator
                regExpVal.ID = "regExpVal2" & dr("RptParamId").ToString()
                regExpVal.ControlToValidate = txt2.ID
                regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                'regExpVal.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                '& vbCr & "The character required to separate values is the semicolon (;)."
                regExpVal.Display = ValidatorDisplay.None
                c3.Controls.Add(regExpVal)
                r.Cells.Add(c3)
            End If

            If dr("Required") = "True" And ResourceId <> 629 Then
                'Add a required field validator if this field is marked as 'Required'.
                reqFldVal = New RequiredFieldValidator
                reqFldVal.ID = "reqFldVal" & dr("RptParamId").ToString()
                reqFldVal.ControlToValidate = txt.ID
                reqFldVal.ErrorMessage = CType((dr("Caption") & " is a required filter."), String)
                reqFldVal.Display = ValidatorDisplay.None
                c4 = New TableCell
                c4.Controls.Add(reqFldVal)
                r.Cells.Add(c4)
            End If


            tblFilterOthers.Rows.Add(r)
        Next

        'Add a hidden textbox and a compare validator for fields marked as 'Required'.
        'This textbox need to be updated everytime it is selected or deselected.
        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        Dim rows() As DataRow = dt.Select("FilterListSec = 1 And Required = 1")


        For Each dr In rows
            Dim txt As New TextBox
            Dim compVal As New CompareValidator

            txt.ID = "txt" & dr("RptParamId").ToString()
            txt.CssClass = "ToTheme2"
            txt.Text = Guid.Empty.ToString
            txt.Visible = False

            compVal.ID = "compVal" & dr("RptParamId").ToString()
            compVal.ControlToValidate = txt.ID
            compVal.[Operator] = ValidationCompareOperator.NotEqual
            compVal.ValueToCompare = Guid.Empty.ToString
            compVal.Type = ValidationDataType.String
            compVal.ErrorMessage = CType((" - " & dr("Caption") & " is a required filter"), String)
            compVal.Display = ValidatorDisplay.None
            pnlRequiredFieldValidators.Controls.Add(txt)
            If Not resID = 629 Then
                pnlRequiredFieldValidators.Controls.Add(compVal)
            End If
        Next

    End Sub

    Private Sub BindDataListFromDB()
        Dim dt As DataTable
        Dim objRptParams As New RptParamsFacade

        dt = objRptParams.GetSavedPrefsList

        With dlstPrefs
            .DataSource = dt

            .DataBind()
        End With

        'Cache the dt so we can reuse it when the links are clicked.
        'There is no need to have to go to the database to repopulate the datalist
        'when a different preference is selected.
        Cache("Prefs") = dt
    End Sub

    Private Sub BindDataListFromCache()
        Dim dt As DataTable
        dt = DirectCast(Cache("Prefs"), DataTable)
        With dlstPrefs
            .DataSource = dt
            .DataBind()
        End With
    End Sub

    Private Sub dlstPrefs_ItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs) Handles dlstPrefs.ItemCommand
        Dim dt, dt2 As DataTable
        Dim dr, dr2 As DataRow
        Dim ds As DataSet
        Dim dRows As DataRow()
        Dim found As Boolean
        Dim prefId As String
        Dim rptParamId As Integer
        Dim iCounter As Integer
        Dim objRptParams As New RptParamsFacade
        Dim objCommon As New CommonUtilities
        Dim ctlTextBox As Control
        Dim ctlDDL As Control

        dlstPrefs.SelectedIndex = e.Item.ItemIndex

        If Not IsNothing(Cache("Prefs")) Then
            BindDataListFromCache()
        Else
            BindDataListFromDB()
        End If

        'Clear the relevant sections
        Clear()

        'There is no need to bring the preference name from the database.
        txtPrefName.Text = DirectCast(e.CommandSource, LinkButton).Text

        'Get the DataSet containing the preferences
        prefId = dlstPrefs.DataKeys(e.Item.ItemIndex).ToString()

        'Set Mode to EDIT
        'SetMode("EDIT")
        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
        ViewState("MODE") = "EDIT"

        If CInt(ViewState("resid")) = 580 Then
            ds = objRptParams.GetUserRptPrefsForDailyCompletedHours(prefId)
            If ds.Tables(0).Rows.Count >= 1 Then
                For Each drCompHours As DataRow In ds.Tables(0).Rows
                    ddlCampGrpId.SelectedValue = drCompHours("CampGrpId").ToString
                    ddlTermId.SelectedValue = drCompHours("TermId").ToString
                    ddlPrgVerId.SelectedValue = drCompHours("PrgVerId").ToString
                    txtFirstName.Text = drCompHours("FirstName").ToString
                    txtLastName.Text = drCompHours("LastName").ToString
                    If CDate(drCompHours("TermStartDate").ToString) = "01/01/1900" Then
                        txtTermStartDate.Text = ""
                    Else
                        txtTermStartDate.Text = drCompHours("TermStartDate").ToString
                    End If
                    If CDate(drCompHours("TermEndDate").ToString) = "01/01/1900" Then
                        txtTermEndDate.Text = ""
                    Else
                        txtTermEndDate.Text = drCompHours("TermEndDate").ToString
                    End If
                    If CDate(drCompHours("attendancestartdate").ToString) = "01/01/1900" Then
                        txtStartDate.Text = ""
                    Else
                        txtStartDate.Text = drCompHours("attendancestartdate").ToString
                    End If
                    If CDate(drCompHours("attendanceenddate").ToString) = "01/01/1900" Then
                        txtEndDate.Text = ""
                    Else
                        txtEndDate.Text = drCompHours("attendanceenddate").ToString
                    End If
                    txtPrefId.Value = prefId
                Next
            End If
            Exit Sub
        End If

        ds = objRptParams.GetUserRptPrefs(prefId)

        'Set txtPrefId to the preference id of the selected item
        txtPrefId.Value = prefId
        'This section deals with displaying the Sort Preferences.
        dt = ds.Tables("SortPrefs")

        'Get the datatable from session that contains the details of each rptparam
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        With dt2
            .PrimaryKey = New DataColumn() { .Columns("RptParamId")}
        End With

        'We need to get the captions for the rows in the SortPrefs dt
        For Each dr In dt.Rows
            dr2 = dt2.Rows.Find(dr("RptParamId"))
            dr("Caption") = dr2("Caption")
        Next

        With lbxSelSort
            .DataSource = dt
            .DataTextField = "Caption"
            .DataValueField = "RptParamId"
            .DataBind()
        End With

        'When the page is first loaded we populate the lbxSort control with all the
        'sort parameters for the report. When a preference is selected we need to
        'make certain that the lbxSort control only displays the sort parameters that
        'are not already selected.
        If lbxSort.Items.Count > 0 Then
            lbxSort.Items.Clear()
        End If

        For Each dr In dt2.Rows
            If dr("SortSec") = True Then
                found = False
                For iCounter = 0 To lbxSelSort.Items.Count - 1
                    If lbxSelSort.Items(iCounter).Value = dr("RptParamId") Then
                        found = True
                    End If
                Next
                If Not found Then
                    lbxSort.Items.Add(New ListItem(dr("Caption"), dr("RptParamId")))
                End If
            End If
        Next

        'This section deals with storing and selecting the relevant FilterList prefs.
        'Set to empty GUID all hidden textboxes in pnlRequiredFieldValidators panel.
        For Each ctrl As Control In pnlRequiredFieldValidators.Controls
            If TypeOf ctrl Is TextBox Then
                DirectCast(ctrl, TextBox).Text = Guid.Empty.ToString
            End If
        Next

        dt = ds.Tables("FilterListPrefs")
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")

        For Each dr In dt.Rows
            dr2 = dt2.NewRow
            dr2("RptParamId") = dr("RptParamId")
            dr2("FldValue") = dr("FldValue")
            dt2.Rows.Add(dr2)
            '
            ctlTextBox = pnlRequiredFieldValidators.FindControl("txt" & dr("RptParamId").ToString)
            If Not (ctlTextBox Is Nothing) Then
                If DirectCast(ctlTextBox, TextBox).Text = Guid.Empty.ToString Then
                    DirectCast(ctlTextBox, TextBox).Text = CType(dr("FldValue"), String)
                Else
                    DirectCast(ctlTextBox, TextBox).Text &= CType(("," & dr("FldValue")), String)
                End If
            End If
        Next

        'If an item is selected in the lbxFilterList listbox then we need to select
        'the values, if any, that the user had selected for that item.
        If lbxFilterList.SelectedIndex <> -1 Then
            rptParamId = CType(lbxFilterList.SelectedItem.Value, Integer)
            'Search the FilterListSelections dt to see if the user had selected any
            'values for this item.
            dRows = dt2.Select("RptParamId = " & rptParamId)
            If dRows.Length > 0 Then
                For Each dr In dRows
                    'Loop through and select relevant item in the lbxSelFilterList control.
                    For iCounter = 0 To lbxSelFilterList.Items.Count - 1
                        If lbxSelFilterList.Items(iCounter).Value.ToString = dr("FldValue").ToString() Then
                            lbxSelFilterList.Items(iCounter).Selected = True
                        End If
                    Next
                Next
            End If
        End If

        'This section deals with the FilterOther prefs.
        dt = ds.Tables("FilterOtherPrefs")
        If dt.Rows.Count > 0 Then
            pnl4.Visible = True
            'For each row we need to select the appropriate entry in the ddl and enter the appropriate
            'value in the textbox.
            For Each dr In dt.Rows
                ctlDDL = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("ddl" & dr("RptParamId").ToString())
                ''Added by saraswathi to avaoid error page when using between in saving the report
                ''Added on july 10 2009
                If Not ctlDDL Is Nothing Then
                    objCommon.SelValInDDL(DirectCast(ctlDDL, DropDownList), dr("OpId").ToString())
                    ctlTextBox = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt" & dr("RptParamId").ToString())
                    ''Added for timeclock report
                    ''Avoid adding two textboxes when the ddl value is 6. i.e. Between range for dates used in other reports
                    ''in time clock report 6 is the number of days
                    If dr("OpId").ToString() = AdvantageCommonValues.BetweenOperatorValue.ToString Then
                        Dim strSplit() As String = dr("OpValue").ToString.Split(";")
                        If strSplit.GetLength(0) > 1 Then
                            DirectCast(ctlTextBox, TextBox).Text = strSplit(0)
                            Dim ctlTextBox2 As Control = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt2" & dr("RptParamId").ToString())
                            DirectCast(ctlTextBox2, TextBox).Text = strSplit(1)
                        ElseIf strSplit.GetLength(0) = 1 Then
                            DirectCast(ctlTextBox, TextBox).Text = strSplit(0)
                        End If
                    Else
                        DirectCast(ctlTextBox, TextBox).Text = dr("OpValue")
                    End If
                End If
            Next
        End If



        '   set Style to Selected Item
        CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Value, ViewState)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        'The code in this sub should only execute if an item is selected
        'in the lbxSort listbox.
        If lbxSort.SelectedIndex <> -1 Then
            lbxSelSort.Items.Add(New ListItem(lbxSort.SelectedItem.Text, lbxSort.SelectedItem.Value))
            lbxSort.Items.RemoveAt(lbxSort.SelectedIndex)
            If lbxSort.Items.Count > 0 Then
                lbxSort.SelectedIndex = lbxSort.SelectedIndex + 1
            End If
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemove.Click
        'The code in this sub should only execute if an item is selected in 
        'the lbxSelSort listbox.
        If lbxSelSort.SelectedIndex <> -1 Then
            lbxSort.Items.Add(New ListItem(lbxSelSort.SelectedItem.Text, lbxSelSort.SelectedItem.Value))
            lbxSelSort.Items.RemoveAt(lbxSelSort.SelectedIndex)
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsave.Click
        SavePreferences()
    End Sub

    Private Function ValidateFilterOthers() As Boolean
        Dim selOpId As Integer
        Dim rCounter As Integer
        Dim tbxText As String
        Dim tbxText2 As String
        Dim strItem As String
        Dim arrValues() As String
        Dim selIndex As String
        Dim errorMessage As String = ""
        Dim opName As String
        Dim fieldName As String
        Dim fieldId As Integer
        Dim fldMask As String
        Dim fldLen As Integer
        Dim dType As String
        Dim rptParamId As Integer
        Dim objcommon As New CommonUtilities

        'We need to loop through the entries in the tblFilterOthers table.
        For rCounter = 1 To tblFilterOthers.Rows.Count - 1
            tbxText = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).Text
            tbxText2 = DirectCast(tblFilterOthers.Rows(rCounter).Cells(3).Controls(0), TextBox).Text
            'Ignore rows where nothing is entered into the textbox.
            If tbxText <> "" Then
                arrValues = tbxText.Split(";")
                selIndex = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedIndex
                opName = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text
                fieldName = DirectCast(tblFilterOthers.Rows(rCounter).Cells(0).Controls(0), Label).Text
                rptParamId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                fieldId = GetFldId(rptParamId)
                fldMask = GetInputMask(fieldId)
                fldLen = tbxText.Length

                If selIndex >= 0 Then   'Make sure an entry is selected in the ddl
                    selOpId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                    If IsSingleValOp(selOpId) And arrValues.Length > 1 Then  'single value operator
                        'There should only be one entry in the textbox. There should be
                        'no semicolons (;) which is used to separate multiple values such as
                        'when using the BETWEEN or the IN LIST operators.
                        errorMessage &= "Operator " & opName & " cannot be used with multiple values." & vbCr
                    ElseIf opName = "Is Null" Then
                        'When the operator selected is "Is Null" then there is no point going
                        'any further since Is Null should not have a value specified.
                        errorMessage &= "You cannot specify a value when using the Is Null operator." & vbCr
                    ElseIf opName = "Between" And (tbxText = "" Or tbxText2 = "") Then
                        'Between must have exactly two values specified to be compared.
                        errorMessage &= "You must specify exactly two values when using the Between operator." & vbCr
                    Else
                        'It is okay to proceed with validating the entries.
                        'Loop through the entries in the arrValues array and validate.
                        For Each strItem In arrValues
                            'Verify the datatype of each entry in the arrValues array. We need to get the FldType from
                            'the RptParams dt stored in session.
                            dType = GetFldType(rptParamId)
                            If Not IsValidDataType(strItem, dType) Then
                                errorMessage &= "Incorrect datatype entered for " & fieldName & vbCr
                            Else
                                'Verify the input mask if there is one.
                                If fldMask <> "" Then
                                    If Not objcommon.ValidateInputMask(fldMask, strItem) Then
                                        errorMessage &= "Incorrect format entered for " & fieldName & vbCr
                                    Else
                                        'Verify the field length if the field type is
                                        'Char or Varchar.
                                        If dType = "Varchar" Or dType = "Char" Then
                                            If fldLen > GetFldLen(rptParamId) Then
                                                errorMessage &= fieldName & " cannot be more than " & GetFldLen(rptParamId).ToString & " characters" & vbCr
                                            End If
                                        End If
                                    End If  'Validate input mask
                                End If  'Input mask exists
                            End If  'Valid data type
                        Next    'Loop through the items in the arrValues array
                    End If
                End If  'Entry is selected in the ddl
            End If  'Textbox is not empty
        Next    'loop through the entries in the tblFilterOThers table
        If errorMessage = "" Then
            Return True
        Else
            'lblErrorMessage.Text = "Please review the following errors:" & errorMessage
            DisplayErrorMessage(errorMessage)
            Return False
        End If
    End Function

    Private Function IsSingleValOp(ByVal opId As Integer) As Boolean
        Select Case opId
            Case 5, 6
                Return False
            Case Else
                Return True
        End Select
    End Function

    Private Function IsValidDataType(ByVal sVal As String, ByVal dataType As String) As Boolean
        Dim iResult As Integer
        'Dim sResult As String
        Dim dResult As Decimal
        Dim fResult As Double
        Dim dtmResult As DateTime
        Dim cResult As Char
        Dim bResult As Byte

        Select Case dataType
            Case "Int"
                Return Integer.TryParse(sVal, iResult)
            Case "Float"
                Return Double.TryParse(sVal, fResult)
            Case "Money"
                Return Decimal.TryParse(sVal, dResult)
            Case "Char"
                Return Char.TryParse(sVal, cResult)
            Case "Datetime"
                Return DateTime.TryParse(sVal, dtmResult)
            Case "Varchar"
                If IsNothing(sVal) Then
                    Return False
                End If
                If sVal.GetType().IsValueType Then
                    Return True
                End If
                Return False
            Case "TinyInt"
                Return Byte.TryParse(sVal, bResult)
            Case Else
                Throw New Exception("Invalid Field Type: " & dataType)
        End Select
    End Function

    Private Function GetFldType(ByVal rptParamId As Integer) As String
        Dim dt As DataTable
        Dim drRows() As DataRow
        Dim fldType As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        drRows = dt.Select("RptParamId = " & rptParamId)
        fldType = CType(drRows(0)("FldType"), String)
        Return fldType
    End Function

    Private Function GetFldId(ByVal rptParamId As Integer) As Integer
        Dim dt As DataTable
        Dim dr As DataRow
        Dim fldId As Integer

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        With dt
            .PrimaryKey = New DataColumn() { .Columns("RptParamId")}
        End With
        dr = dt.Rows.Find(rptParamId)
        fldId = CType(dr("FldId"), Integer)
        Return fldId
    End Function

    Private Function GetFldName(ByVal rptParamId As Integer) As String
        Dim dt As DataTable
        Dim drRows() As DataRow
        Dim fldName As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        drRows = dt.Select("RptParamId = " & rptParamId)
        fldName = CType(drRows(0)("FldName"), String)
        Return fldName
    End Function

    Private Function GetTblName(ByVal rptParamId As Integer) As String
        Dim dt As DataTable
        Dim drRows() As DataRow
        Dim tblName As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        drRows = dt.Select("RptParamId = " & rptParamId)
        tblName = CType(drRows(0)("TblName"), String)
        Return tblName
    End Function

    Private Function GetInputMask(ByVal fldId As Integer) As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim sMask As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("InputMasks")
        dr = dt.Rows.Find(fldId)
        If dr Is Nothing Then
            sMask = ""
        Else
            sMask = CType(dr("Mask"), String)
        End If
        Return sMask
    End Function

    Private Function GetFldLen(ByVal rptParamId As Integer) As Integer
        Dim dt As DataTable
        Dim dr As DataRow
        Dim fldLen As Integer

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        dr = dt.Rows.Find(rptParamId)
        fldLen = CType(dr("FldLen"), Integer)
        Return fldLen
    End Function

    Private Sub lbxFilterList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles lbxFilterList.SelectedIndexChanged
        'When an item is selected we want to display the list of possible values for the item.
        'We can retrieve the list of values for this item from the FilterListValues stored

        'in the ds that is in session.
        If ResourceId = 629 Then
            If txtStudentId.Text <> "" And txtStuEnrollment.Text <> "" Then
                DisplayErrorMessage("You need to clear the Student Enrollment in order to use the filters.")
                For i As Integer = 0 To lbxFilterList.Items.Count - 1
                    lbxFilterList.Items(i).Selected = False
                Next
                Exit Sub
            End If
        End If
        Dim ds As DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim aRows As DataRow()
        Dim lbxRows As Integer
        Dim arrRows As Integer
        Dim allRows As DataRow()
        Dim cmpGrpSelectedRows As DataRow()
        Dim allCampgrpId As String
        Dim fldName As String
        Dim dsCampGroups As DataSet
        Dim objRptParams As New RptParamsFacade

        ViewState("boolShowErr") = True
        ds = DirectCast(Session("WorkingDS"), DataSet)
        dt = ds.Tables("FilterListValues")
        allRows = dt.Select("FldName='CampGrpId' and DisplayFld='All'")
        If allRows.Length = 0 Then
            ''Modified by saraswathi lakshmanan on May 07 2009
            ''If the selected user does not have permission to all the campus groups,  but has permission to the campus group assigned, then, the other filter lists are not populating the values assigned to all campus groups and assigned to a particular campus group
            ''They are listing only the other lists mapped to the selected campus, they are not listing the ones mapped to all.
            allCampgrpId = ds.Tables("CmpGrpIdAll").Rows(0)("CampGrpId").ToString
        Else
            allCampgrpId = allRows(0)("CampGrpId").ToString
        End If

        If lbxFilterList.SelectedItem.Text.ToLower.Contains("term") And (ResourceId = 606 Or ResourceId = 262) Then
            lbxSelFilterList.SelectionMode = ListSelectionMode.Single
        Else
            lbxSelFilterList.SelectionMode = ListSelectionMode.Multiple
        End If
        'If 'None' item was selected, we need to empty the FilterListSelections dt in session.
        'Otherwise, we want to get a list of all the rows in dt that have the corresponding RptParamId
        'that is selected in the lbxFilterList listbox.

        If lbxFilterList.SelectedItem.Value = 0 Then
            'Clear the items from the lbxSelFilterList lisbox if there are any.
            If lbxSelFilterList.Items.Count > 0 Then
                lbxSelFilterList.Items.Clear()
            End If
            'If the FilterListSelections dt in session is not empty then we need to empty it.
            dt = ds.Tables("FilterListSelections")
            If dt.Rows.Count > 0 Then
                dt.Rows.Clear() 'Empty dt table.
            End If
            'Set to empty GUID all textboxes in panel pnlRequiredFieldValidators.
            For Each ctrl As Control In pnlRequiredFieldValidators.Controls
                Dim textBox As TextBox = TryCast(ctrl, TextBox)
                If (textBox IsNot Nothing) Then
                    textBox.Text = Guid.Empty.ToString
                End If
            Next

        Else
            'We want to get a list of all the rows in dt that have the corresponding RptParamId
            'that is selected in the lbxFilterList listbox.
            aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value)
            If aRows.Length > 0 Then
                fldName = aRows(0)(3).ToString
                If fldName.ToLower <> "campgrpid" Then
                    cmpGrpSelectedRows = ds.Tables("FilterListSelections").Select("FldName='CampGrpId'")
                    If cmpGrpSelectedRows.Length > 0 Then
                        Dim campgrpParamid As Integer = CInt(cmpGrpSelectedRows(0)(0))
                        Dim ctlTextBox As Control = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt" & campgrpParamid)
                        Dim strCampGrpId As String = DirectCast(ctlTextBox, TextBox).Text
                        Dim strValue As String = " CampGrpId in('" & strCampGrpId.Replace(",", "','") & "') "
                        ''Added by Saraswathi lakshmanan to find the campus groups, the student has rights to
                        ''All the campus groups, where the selected campus group's Campus is available is found.
                        dsCampGroups = objRptParams.GetCampusgroups(strValue)

                        If ((strCampGrpId = allCampgrpId And Session("UserName") = "sa") Or (allCampgrpId = "" And Session("UserName") = "sa")) Then

                            'If strCampGrpId = allCampgrpId Then
                            aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value)
                        Else
                            ''Added by saraswathi lakshmanan
                            ''Added on May 11 2009
                            ''Fix for mantis case: 14745
                            ''When a campus group is selected, The campus available in htat campus group is found. 
                            ''And  the other campus groups which holds the same campus from the selected group is found.
                            ''Thus the entities whose, campus groups mapped to the selected campus groups are listed.
                            ''EG: Miami Campusgroup has Miami campus and Orlando Campus group has orlando campus.
                            ''Florida campus Group has MI and Or campus. Tampa Campus group has Tampa Campus
                            ''All campus group has MI, Or, Ta Campuses
                            ''When we select Florida Campus Group, Anything mapped to Florida, Miami, Orlando and All are listed 

                            If dsCampGroups.Tables.Count > 0 Then
                                If dsCampGroups.Tables(0).Rows.Count > 0 Then
                                    Dim strCmpGroupIds As String = ""
                                    Dim i As Integer
                                    For i = 0 To dsCampGroups.Tables(0).Rows.Count - 1
                                        strCmpGroupIds = strCmpGroupIds + "'" + dsCampGroups.Tables(0).Rows(i)(0).ToString + "',"
                                    Next
                                    ''Remove the last comma
                                    strCmpGroupIds = strCmpGroupIds.Remove(strCmpGroupIds.Length - 1, 1)
                                    If fldName.ToLower <> "campusid" Then
                                        aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value & " AND ( CampGrpId in(" + strCmpGroupIds + "))")
                                    Else
                                        aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value & " AND ( CampGrpId in('" + strCampGrpId.Replace(",", "','") + "'))")
                                    End If
                                Else
                                    DisplayErrorMessage("Please first select the campus group")
                                End If
                            Else
                                DisplayErrorMessage("Please first select the campus group")
                            End If
                        End If
                    Else
                        lbxFilterList.Items.FindByValue(lbxFilterList.SelectedValue).Selected = False
                        lbxFilterList.Items.FindByText("Campus Group").Selected = True
                        If lbxSelFilterList.Items.Count = 0 Then
                            aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value)
                            For Each dr In aRows
                                lbxSelFilterList.Items.Add(New ListItem(CType(dr("DisplayFld"), String), CType(dr("ValueFld"), String)))
                            Next
                            lbxSelFilterList.Items.Insert(0, "None")
                        End If
                        DisplayErrorMessage("Please first select the campus group")
                        Exit Sub
                    End If
                End If
            End If
            'Clear the items from the lbxSelFilterList lisbox if there are any.
            If lbxSelFilterList.Items.Count > 0 Then
                lbxSelFilterList.Items.Clear()
            End If

            For i As Integer = 0 To aRows.Length - 1
                Dim drRow As DataRow = aRows(i)
                If i = 0 Then
                    lbxSelFilterList.Items.Add(New ListItem(CType(drRow("DisplayFld"), String), CType(drRow("ValueFld"), String)))
                Else
                    If aRows(i)("ValueFld").ToString <> aRows(i - 1)("ValueFld").ToString Then
                        lbxSelFilterList.Items.Add(New ListItem(CType(drRow("DisplayFld"), String), CType(drRow("ValueFld"), String)))
                    End If
                End If
            Next
            'Add All program
            Dim major As String = GetSettingString("MajorsMinorsConcentrations")
            If (lbxFilterList.SelectedItem.Text.ToLower().Contains("program") And major.ToUpper() = "YES") Then
                lbxSelFilterList.Items.Insert(0, New ListItem("All Programs", Guid.Empty.ToString()))
            End If


            'Add a 'None' item so that users can deselect an item without having to
            'select another one.
            lbxSelFilterList.Items.Insert(0, "None")

            'If the FilterListSelections dt in session is not empty then we need
            'to see if it has any entries for the item selected here. If it does then
            'we need to select the relevant entries in the lbxSelFilterList listbox.
            dt = ds.Tables("FilterListSelections")
            If dt.Rows.Count > 0 Then
                aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value)
                If aRows.Length > 0 Then
                    'Loop through the items in the lbxSelFilterList listbox and
                    'select the relevant items.
                    For arrRows = 0 To aRows.Length - 1
                        For lbxRows = 0 To lbxSelFilterList.Items.Count - 1
                            If lbxSelFilterList.Items(lbxRows).Value.ToString = aRows(arrRows)("FldValue").ToString() Then
                                lbxSelFilterList.Items(lbxRows).Selected = True
                            End If
                        Next
                    Next
                End If 'search did not return 0 rows
            End If 'dt is not empty
        End If
    End Sub

    Private Sub lbxSelFilterList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles lbxSelFilterList.SelectedIndexChanged
        'When this event is fired it is important to remember that it could be that
        'the user has selected OR deselected a value. Also, this lisbox supports multiple
        'selections so we cannot simply get the item selected or unselected.
        'Each time this event fires we have to examine each item in the listbox. If the
        'item is not selected then if it exists in the dt we will have to remove it. If
        'the item is selected but not exists in the dt then we will have to add it.
        Dim i As Integer
        Dim ds As DataSet
        Dim dt As DataTable
        Dim dt2 As DataTable
        Dim dr As DataRow
        Dim dr2 As DataRow()
        Dim fldName As String
        Dim paramId As Integer
        Dim selValue As String
        Dim temp As String
        Dim ctlTextBox As Control
        Dim rows() As DataRow

        ds = DirectCast(Session("WorkingDS"), DataSet)
        dt = ds.Tables("FilterListSelections")
        dt2 = ds.Tables("RptParams")
        paramId = CType(lbxFilterList.SelectedItem.Value, Integer)
        fldName = CType(dt2.Select("RptParamId = " & paramId)(0)("FldName"), String)
        If fldName.ToLower = "campgrpid" Then
            dr2 = dt.Select("FldName <> 'CampGrpId'")
            For idx As Integer = 0 To dr2.Length - 1
                ctlTextBox = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl(CType(("txt" & dr2(idx)(0)), String))
                If Not ctlTextBox Is Nothing Then DirectCast(ctlTextBox, TextBox).Text = Guid.Empty.ToString
                dt.Rows.Remove(dr2(idx))

            Next
        End If

        'Detect if is All Programs and if it change the selected item to all and continues.
        Const allProgram As String = "All Programs"
        Dim itemAll As ListItem = lbxSelFilterList.Items.FindByText(allProgram)
        If (Not IsNothing(itemAll) AndAlso itemAll.Selected) Then
            lbxSelFilterList.ClearSelection()
            lbxSelFilterList.SelectionMode = ListSelectionMode.Multiple
            For Each item As ListItem In lbxSelFilterList.Items
                If (item.Text.ToLower() = "none") Or (item.Text = allProgram) Then
                    Continue For
                End If
                item.Selected = True
            Next
        End If

        For i = 0 To lbxSelFilterList.Items.Count - 1
            selValue = lbxSelFilterList.Items(i).Value

            If lbxSelFilterList.Items(i).Selected = False And i > 0 Then
                If ItemExistsInFilterListSelections(i) Then
                    'We need to remove the item from the dt
                    Dim objCriteria() As Object = {paramId, selValue}
                    dr = dt.Rows.Find(objCriteria)
                    dt.Rows.Remove(dr)
                    'If field is 'Required', set hidden textbox to empty.
                    rows = dt2.Select("RptParamId = " & paramId & " AND Required = 1")
                    If rows.GetLength(0) = 1 Then
                        ctlTextBox = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt" & paramId)
                        If DirectCast(ctlTextBox, TextBox).Text.IndexOf(",", StringComparison.Ordinal) = -1 Then
                            'Only one entry.
                            DirectCast(ctlTextBox, TextBox).Text = Guid.Empty.ToString
                        Else
                            temp = DirectCast(ctlTextBox, TextBox).Text
                            temp = temp.Replace(selValue, ",")
                            If temp.StartsWith(",,") Then
                                DirectCast(ctlTextBox, TextBox).Text = temp.Substring(2)
                            ElseIf temp.EndsWith(",,") Then
                                DirectCast(ctlTextBox, TextBox).Text = temp.Substring(0, temp.Length - 2)
                            Else
                                DirectCast(ctlTextBox, TextBox).Text = temp.Replace(",,,", ",")
                            End If
                        End If
                    End If
                End If
            Else
                If Not ItemExistsInFilterListSelections(i) And i > 0 Then
                    'We need to add the item to the dt
                    If lbxSelFilterList.Items(i).Text = "All" Then
                        lbxSelFilterList.ClearSelection()
                        lbxSelFilterList.SelectionMode = ListSelectionMode.Single
                        lbxSelFilterList.SelectedValue = selValue
                    ElseIf lbxFilterList.SelectedItem.Text.ToLower.Contains("term") And ResourceId = 606 Then
                        lbxSelFilterList.ClearSelection()
                        lbxSelFilterList.SelectionMode = ListSelectionMode.Single
                        lbxSelFilterList.SelectedValue = selValue
                    Else
                        lbxSelFilterList.SelectionMode = ListSelectionMode.Multiple
                    End If


                    dr = dt.NewRow
                    dr("RptParamId") = paramId      'lbxFilterList.SelectedItem.Value
                    dr("FldValue") = selValue       'lbxSelFilterList.Items(i).Value
                    dr("FldName") = fldName
                    dt.Rows.Add(dr)
                    'If field is 'Required', set hidden textbox to newly selected value.
                    rows = dt2.Select("RptParamId = " & paramId & " AND Required = 1")
                    If rows.GetLength(0) = 1 Then
                        ctlTextBox = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt" & paramId)
                        If DirectCast(ctlTextBox, TextBox).Text = Guid.Empty.ToString Then
                            DirectCast(ctlTextBox, TextBox).Text = selValue
                        Else
                            DirectCast(ctlTextBox, TextBox).Text &= "," & selValue
                        End If
                    End If
                End If
            End If
        Next
        Session("WorkingDS") = ds
    End Sub

    Private Function ItemExistsInFilterListSelections(ByVal itemIndex As Integer) As Boolean
        ' Dim i As Integer
        Dim ds As DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim paramId As Integer
        Dim selValue As String

        paramId = CType(lbxFilterList.SelectedItem.Value, Integer)
        selValue = lbxSelFilterList.Items(itemIndex).Value

        ds = DirectCast(Session("WorkingDS"), DataSet)
        dt = ds.Tables("FilterListSelections")

        If dt.Rows.Count = 0 Then
            Return False
        Else
            Dim objCriteria() As Object = {paramId, selValue}
            dr = dt.Rows.Find(objCriteria)
            If dr Is Nothing Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnnew.Click
        Dim objCommon As New CommonUtilities
        Dim dt As DataTable
        Dim dr As DataRow

        Try
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            'Place the prefid guid into the txtPrefId textbox so it can be reused
            'if we need to do an update later on.
            txtPrefId.Value = ""
            'Make sure no item is selected in the Preferences datalist.
            dlstPrefs.SelectedIndex = -1
            If CInt(ViewState("resid")) = 580 Then
                DailyCompletedHoursClear()
                Exit Sub
            Else
                'Clear the rhs of the screen
                Clear()
            End If


            'We have to rebind the lbxSort control to the report parameters that are stored in session
            lbxSort.Items.Clear()
            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
            For Each dr In dt.Rows
                If dr("SortSec") = True Then
                    lbxSort.Items.Add(New ListItem(CType(dr("Caption"), String), CType(dr("RptParamId"), String)))
                End If
            Next



            'Rebind the Preferences datalist from the database.
            BindDataListFromDB()

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub Clear()
        Dim dt As DataTable
        Dim trCounter As Integer
        Dim ctrlDDL As Control
        Dim objCommon As New CommonUtilities

        'Clear the txtPrefName textbox control.
        txtPrefName.Text = ""
        'Clear the lbxSelSort listbox control
        lbxSelSort.Items.Clear()
        'Make sure no item is selected in the lbxSort listbox.
        lbxSort.SelectedIndex = -1
        'Clear the entries from the FilterListSelections dt
        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")
        If dt.Rows.Count > 0 Then
            dt.Clear()
        End If
        'Make sure no entry is selected in the lbxSelFilterList listbox
        lbxSelFilterList.SelectedIndex = -1
        'Get operators table
        Dim dt2 As DataTable = DirectCast(Session("WorkingDS"), DataSet).Tables("Operators")
        'Clear the textboxes in the FilterOther section and set dropdownlists to 'Equal To'.
        For trCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
            DirectCast(tblFilterOthers.Rows(trCounter).Cells(2).Controls(0), TextBox).Text = ""
            DirectCast(tblFilterOthers.Rows(trCounter).Cells(3).Controls(0), TextBox).Text = ""
            ctrlDDL = DirectCast(tblFilterOthers.Rows(trCounter).Cells(1).Controls(0), DropDownList)
            objCommon.SelValInDDL(DirectCast(ctrlDDL, DropDownList), dt2.Rows(0)("CompOpId").ToString)
        Next
        'Set to empty GUID all hidden textboxes in pnlRequiredFieldValidators panel.
        For Each ctrl As Control In pnlRequiredFieldValidators.Controls
            Dim textBox As TextBox = TryCast(ctrl, TextBox)
            If (textBox IsNot Nothing) Then
                textBox.Text = Guid.Empty.ToString
            End If
        Next
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btndelete.Click
        Dim objRptParams As New RptParamsFacade
        Dim objCommon As New CommonUtilities

        'Delete all the preference info from the datatbase
        If CInt(ViewState("resid")) = 580 Then
            objRptParams.DeleteUserRptPrefsForDailyCompletedHours(txtPrefId.Value)
            DailyCompletedHoursClear()
        Else
            objRptParams.DeleteUserPrefInfo(txtPrefId.Value)
            'Clear the rhs of the screen
            Clear()
        End If


        'Set mode to NEW
        'SetMode("NEW")
        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
        ViewState("MODE") = "NEW"
        'Place the prefid guid into the txtPrefId textbox so it can be reused
        'if we need to do an update later on.
        txtPrefId.Value = ""
        'Make sure no item is selected in the datalist.
        dlstPrefs.SelectedIndex = -1
        'Rebind the datalist from the database.
        BindDataListFromDB()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        Dim rptParamInfo As New ReportParamInfo
        If Page.IsValid Then
            If (ResourceId = 629) And (txtStuEnrollmentId.Value <> "" And Not txtStuEnrollmentId.Value.ToLower.Contains("stuenrollmentid")) Then
                Dim facade As New RegFacade
                If Not facade.IsStudentTranscriptHold(txtStuEnrollmentId.Value) = "0" Then
                    DisplayErrorMessage("The report cannot be displayed as the student transcript is put on hold")
                    Exit Sub
                End If
            End If

            Dim strErrMsg As String = BuildReportDataSet(rptParamInfo)

            If strErrMsg = "" Then
                'Since no error was found, proceed to display report in Report Viewer page.
                If Not (Session("ReportDS") Is Nothing) Then

                    '   Make sure to clear any previously loaded report docuemnt objects
                    Session("RptObject") = Nothing

                    '   Setup the properties of the new window
                    Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsMedium
                    Dim name As String = "ReportViewer"
                    Dim url As String = "../SY/" + name + ".aspx"
                    CommonWebUtilities.OpenClientChildWindow(Page, url, name, winSettings)

                Else
                    'Alert user of no data matching selection.
                    If Not ResourceId = 580 Or ResourceId = 600 Then
                        strErrMsg = "There is no data matching selection."
                        DisplayErrorMessage(strErrMsg)
                    End If
                End If
            Else
                'Alert user of error.
                DisplayErrorMessage(strErrMsg)
            End If
        End If
    End Sub

    Private Function GetValidateErrMsg() As String
        Dim rtn As String = String.Empty
        Dim count As Integer = Page.Validators.Count
        Dim index As Integer
        For index = 0 To count - 1
            If (Not Page.Validators(index).IsValid) Then
                rtn = rtn + Page.Validators(index).ErrorMessage & vbLf
            End If
        Next
        Return rtn
    End Function

    Private Function BuildOrderBy(ByRef strOClause As String) As String
        Dim i As Integer
        Dim oClause As String = ""
        Dim tempStringOClause As String = ""
        'Dim dt As DataTable
        Dim fldName As String
        Dim tblName As String
        Dim fldCaption As String

        'Only execute if at least one sort param has been specified
        If lbxSelSort.Items.Count > 0 Then
            For i = 0 To lbxSelSort.Items.Count - 1
                tblName = GetTblName(CType(lbxSelSort.Items(i).Value, Integer))
                fldName = GetFldName(CType(lbxSelSort.Items(i).Value, Integer))
                fldCaption = lbxSelSort.Items(i).Text
                If i < lbxSelSort.Items.Count - 1 Then
                    If tblName <> "syDerived" Then
                        oClause &= tblName & "." & fldName & ","
                    Else
                        oClause &= fldName & ","
                    End If
                    tempStringOClause &= fldCaption & ", "
                Else
                    If tblName <> "syDerived" Then
                        oClause &= tblName & "." & fldName
                    Else
                        oClause &= fldName
                    End If
                    tempStringOClause &= fldCaption
                End If
            Next
        End If

        'oClause = "ORDER BY " & oClause
        strOClause = tempStringOClause
        Return oClause
    End Function

    Private Function BuildFilterListClause(ByRef strWClause As String, Optional ByRef strPrgVerClause As String = "") As String
        Dim dt As DataTable
        Dim dt2 As DataTable
        Dim i, j, k As Integer
        Dim drRows() As DataRow
        Dim rows() As DataRow
        Dim fldName As String
        Dim tblName As String
        Dim fldType As String
        Dim fldCaption As String
        Dim [Operator] As String
        Dim filterValue As String
        Dim tempWClause As String = ""
        Dim tempStringWClause As String = ""
        Dim stringWClause As String = ""
        Dim strTempOperator As String
        Dim wClause As String = ""
        Dim allCampGrp As String = ""

        'We need to loop through the items in the lbxFilterList control and see
        'if the item has any entries in the FilterListSelections dt stored in session.
        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")
        'We need to get the DisplayFld of all selected items.
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListValues")

        If dt.Rows.Count > 0 Then
            For i = 0 To lbxFilterList.Items.Count - 1
                drRows = dt.Select("RptParamId = " & lbxFilterList.Items(i).Value)
                If drRows.Length > 0 Then 'Match found

                    tblName = GetTblName(CType(lbxFilterList.Items(i).Value, Integer))
                    fldName = GetFldName(CType(lbxFilterList.Items(i).Value, Integer))
                    fldType = GetFldType(CType(lbxFilterList.Items(i).Value, Integer))
                    fldCaption = lbxFilterList.Items(i).Text

                    If drRows.Length > 1 Then
                        [Operator] = " IN "
                        strTempOperator = " In List "
                    Else
                        [Operator] = " = "
                        strTempOperator = " Equal To "
                    End If

                    For j = 0 To drRows.Length - 1
                        filterValue = BuildFilterValue(drRows(j)("FldValue").ToString(), fldType)
                        If tblName.ToString.ToLower = "arstuenrollments" And fldName.ToString.ToLower = "prgverid" Then
                            strPrgVerClause = filterValue
                        End If
                        If fldName.ToString.ToLower = "prgverid" Then
                            strPrgVerClause = filterValue
                        End If
                        If j < drRows.Length - 1 Then
                            tempWClause &= filterValue & ","
                        Else
                            tempWClause &= filterValue
                        End If
                        'Get the DisplayFld
                        rows = dt2.Select("RptParamId = " & drRows(j)("RptParamId").ToString() & " AND " &
                                            "ValueFld = '" & drRows(j)("FldValue").ToString() & "'")
                        If rows.GetLength(0) > 0 Then
                            If j < drRows.Length - 1 Then
                                tempStringWClause &= CType(("'" & rows(0)("DisplayFld") & "', "), String)
                            Else
                                If strTempOperator = " Equal To " Then
                                    tempStringWClause &= CType(rows(0)("DisplayFld"), String)
                                Else
                                    tempStringWClause &= CType(("'" & rows(0)("DisplayFld") & "'"), String)
                                End If
                            End If
                        End If
                    Next

                    If k > 0 Then
                        wClause &= " AND "
                        stringWClause &= " AND "
                    End If

                    If [Operator] = " = " Then
                        ' wClause &= tblName & "." & fldName & [Operator] & tempWClause
                        'stringWClause &= fldCaption & strTempOperator & tempStringWClause
                        'Modified by balaji on 11/20/2006 validation for all campus group removed
                        If fldName.ToLower = "campgrpid" Then
                            [Operator] = " in "
                            'allCampGrp = "or " & tblName & "." & fldName & "=(SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')"
                            Const strCampGrp As String = " (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in "

                            wClause &= tblName & "." & fldName & [Operator] & strCampGrp & "(" & tempWClause & ")" & allCampGrp & ")"

                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                        Else
                            [Operator] = " = "
                            wClause &= tblName & "." & fldName & [Operator] & tempWClause
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                        End If
                    Else
                        If fldName.ToLower = "campgrpid" Then
                            'Modified by balaji on 11/20/2006 validation for all campus group removed
                            Dim strCampGrp As String = " (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in "
                            wClause &= tblName & "." & fldName & [Operator] & strCampGrp & "(" & tempWClause & "))"
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                        Else
                            wClause &= tblName & "." & fldName & [Operator] & "(" & tempWClause & ")"
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                        End If
                    End If
                    k += 1 'Match found so we need to increment the count of matches so far
                End If
                tempWClause = ""
                tempStringWClause = ""
            Next
        Else
            'There is no item form lbxFilterList control 
            'that has entries in the FilterListSelections dt stored in session.
        End If
        strWClause = stringWClause
        Return wClause
    End Function

    Private Function BuildFilterOtherClause(ByRef strWClause As String, ByRef strErrorMsg As String, Optional ByRef strClassDate As String = "", Optional ByRef cohortStartDate As String = "", Optional ByRef strFirstName As String = "", Optional ByRef strLastName As String = "") As String
        Dim fldName As String
        Dim tblName As String
        'Dim fldValue As String = ""
        Dim fldType As String
        Dim fldCaption As String
        Dim sqlOperator As String
        Dim rCounter As Integer
        Dim opId As Integer
        Dim rptParamId As Integer
        'Dim filterValue As String = ""
        Dim tbxText As String
        Dim tbxText2 As String
        Dim arrValues() As String
        Dim selText As String
        Dim wClause As String = ""
        Dim tempWClause As String = ""
        Dim inList As String = ""
        Dim strItem As String
        Dim position As Integer
        Dim errMsg As String = ""
        Dim tempFilterValue As String = ""
        Dim tempFilterValue2 As String
        'The following variables will hold the string representation of the Where Clause,
        'which will be shown in the report as part of the title.        
        Dim tempStringWClause As String = ""
        Dim stringWClause As String = ""

        rCounter = 1
        'We need to loop through the entries in the tblFilterOthers table.
        Do While (rCounter <= tblFilterOthers.Rows.Count - 1) And errMsg = ""
            tbxText = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).Text
            tbxText2 = DirectCast(tblFilterOthers.Rows(rCounter).Cells(3).Controls(0), TextBox).Text
            fldCaption = DirectCast(tblFilterOthers.Rows(rCounter).Cells(0).Controls(0), Label).Text
            arrValues = tbxText.Split(CType(";", Char))
            opId = CType(DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
            rptParamId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
            fldName = GetFldName(rptParamId)
            if  ResourceId = 285 And fldName.ToString.ToLower = "academicyearid"
                sqlOperator = " = "
                tbxText2 = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text
                else    
                    sqlOperator = GetSQLOperator(opId)
            End If
            tblName = GetTblName(rptParamId)
            fldType = GetFldType(rptParamId)
            selText = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text
            'Ignore rows where nothing is entered into the textbox.
            If tbxText <> "" Then

                'Modified by Michelle R. Rodriguez on 11/19/2004 and 11/22/2004.
                'FilterValue needs to be validated to exclude invalid dates and replace single quotes with double quotes.
                'Also, the number of operands is validated.

                Try
                    If ResourceId = 560 And tblName.ToString.ToLower = "arstuenrollments" And fldName.ToString.ToLower = "cohortstartdate" Then
                        If arrValues.GetLength(0) = 1 Then
                            cohortStartDate = sqlOperator & "('" & tbxText & "')"
                            tempWClause = fldCaption & " " & selText & " " & arrValues(0)
                            tempStringWClause = fldCaption & " " & selText & " " & arrValues(0)
                            Exit Try
                        End If
                    End If
                    'Time Clock Punch Report - Resource 586
                    If ResourceId = 586 And tblName.ToString.ToLower = "arStudentTimeClockPunches".ToLower Then
                        If arrValues.GetLength(0) = 1 Then
                            Dim noofDays As Integer
                            If tblFilterOthers.Rows.Count > 1 Then
                                noofDays = DirectCast(tblFilterOthers.Rows(2).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                            Else
                                noofDays = 1
                            End If

                            tempWClause = "From Date" & " ;NoOfDays= " & noofDays & " ;FromDate=" & arrValues(0)
                            tempStringWClause = "From Date" & " " & noofDays & " " & arrValues(0)

                            'tempWClause = fldCaption & " ;NoOfDays= " & noofDays & " ;FromDate=" & arrValues(0)
                            'tempStringWClause = fldCaption & " " & noofDays & " " & arrValues(0)
                            Exit Try
                        End If
                    End If
                    ''Added for Weekly Attendance Report
                    If ResourceId = 588 And tblName.ToString.ToLower = "arStudentClockAttendance".ToLower Then
                        If arrValues.GetLength(0) = 1 Then
                            tempWClause = fldCaption & ";" & arrValues(0)
                            tempStringWClause = fldCaption & ";" & arrValues(0)
                            Exit Try
                        End If
                    End If
                    If  ResourceId = 285 And fldName.ToString.ToLower = "academicyearid" then   
                        tempStringWClause = "AcademicYearDescrip" & " = " & selText & " " 
                    ElseIf IsSingleValOp(opId) And selText = "Like" Then
                        If arrValues.GetLength(0) = 1 Then
                            tempFilterValue = BuildFilterValue(arrValues(0), fldType)
                            If tempFilterValue <> "" Then
                                If tblName <> "syDerived" Then
                                    tempWClause = tblName & "." & fldName & " " & sqlOperator & "'%" & arrValues(0) & "%'"
                                Else
                                    tempWClause = fldName & sqlOperator & "'%" & arrValues(0) & "%'"
                                End If
                                tempStringWClause = fldCaption & " " & selText & " " & arrValues(0)
                            Else
                                errMsg = "Invalid " & fldCaption & " value to filter for."
                            End If
                        Else
                            'More than one input for a single operator.
                            errMsg = "Too many values to filter " & fldCaption & " for."
                        End If

                    ElseIf IsSingleValOp(opId) And selText <> "Empty" And selText <> "Is Null" Then
                        'Modified by Michelle Rodriguez on 09/26/2004 and 10/04/2004.
                        'Needed to include the time for filters that involved Datetime fields:
                        'The " = " operator will behave like a " BETWEEN " and the filter value
                        'will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
                        'The " <> " operator will behave like a " NOT BETWEEN " and the filter value
                        'will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
                        'Also, the filter value for the " > " operator will be of format: mm/dd/yyyy 11:59:59 PM
                        'All other cases remain the same.
                        If arrValues.GetLength(0) = 1 Then
                            tempFilterValue = BuildFilterValue(arrValues(0), fldType, sqlOperator)
                            If tempFilterValue <> "" Then
                                If ResourceId = 485 Then
                                    'special case
                                    tempStringWClause = fldCaption & " " & arrValues(0)
                                Else
                                    ' Changed by Uvarajan
                                    If (ResourceId = 554 Or ResourceId = 555) And fldCaption = "Minimum Attendance" Then
                                        tempStringWClause = ""
                                    ElseIf ResourceId = 591 And fldCaption = "Report Start Date" Then
                                        tempStringWClause = fldCaption & "=" & arrValues(0) & ";"
                                    Else
                                        tempStringWClause = fldCaption & " " & selText & " " & arrValues(0)
                                    End If
                                End If
                                If tblName <> "syDerived" Then
                                    If fldType = "Datetime" Then
                                        If sqlOperator = " = " Then
                                            sqlOperator = " BETWEEN "
                                        ElseIf sqlOperator = " <> " Then
                                            sqlOperator = " NOT BETWEEN "
                                        End If
                                    End If
                                    ' Changed by Uvarajan
                                    If (ResourceId = 554 Or ResourceId = 555) And fldCaption = "Minimum Attendance" Then
                                        tempWClause = ""
                                    ElseIf ResourceId = 591 And fldCaption = "Report Start Date" Then
                                        tempWClause = ""
                                    Else
                                        tempWClause = tblName & "." & fldName & sqlOperator & tempFilterValue
                                    End If
                                Else
                                    If fldType = "Datetime" Then
                                        If sqlOperator = " = " Then
                                            sqlOperator = " BETWEEN "
                                        ElseIf sqlOperator = " <> " Then
                                            sqlOperator = " NOT BETWEEN "
                                        End If
                                    End If
                                    ' Changed by Uvarajan
                                    If (ResourceId = 554 Or ResourceId = 555) And fldCaption = "Minimum Attendance" Then
                                        tempWClause = ""

                                    Else
                                        tempWClause = fldName & sqlOperator & tempFilterValue
                                    End If

                                End If
                            Else
                                errMsg = "Invalid " & fldCaption & " value to filter for."
                            End If
                        Else
                            'More than one input for a single operator.
                            errMsg = "Too many values to filter " & fldCaption & " for."
                        End If

                    ElseIf selText = "Between" Then
                        'If arrValues.GetLength(0) = 2 Then
                        'Only two values are accepted as valid input.
                        tempFilterValue = BuildFilterValue(tbxText, fldType)
                        tempFilterValue2 = BuildFilterValue(tbxText2, fldType)
                        If tempFilterValue <> "" And tempFilterValue2 <> "" Then
                            If ResourceId = 275 And fldName.ToString.ToLower.Trim = "balance" Then
                                tempWClause = fldName & sqlOperator & tempFilterValue & " AND " & tempFilterValue2
                            Else
                                If tblName <> "syDerived" Then
                                    'Code Added By Vijay Ramteke May 13,2009
                                    If tblName = "arClassSections" Then
                                        If ResourceId = 554 Then

                                            If Convert.ToDateTime(tempFilterValue2.Replace("'", "")) >= Convert.ToDateTime(tempFilterValue.Replace("'", "")) Then
                                                tempFilterValue2 = BuildFilterValue(tbxText2, fldType, " > ")
                                                'tempWClause = "arClassSections.StartDate" & sqlOperator & tempFilterValue & " AND " & tempFilterValue2
                                                tempWClause = "atclssectattendance.MeetDate" & sqlOperator & tempFilterValue & " AND " & tempFilterValue2
                                            Else
                                                errMsg = "To date should be greater than or equal to from date"
                                            End If

                                        Else
                                            tempWClause = "arClassSections.StartDate>= " & tempFilterValue & " AND arClassSections.EndDate<=" & tempFilterValue2
                                        End If
                                    Else
                                        tempWClause = tblName & "." & fldName & sqlOperator & tempFilterValue & " AND " & Left(tempFilterValue2, tempFilterValue2.Length - 1) & " 11:59:59 PM'"
                                    End If
                                    'tempWClause = tblName & "." & fldName & sqlOperator & tempFilterValue & " AND " & Left(tempFilterValue2, tempFilterValue2.Length - 1) & " 11:59:59 PM'"
                                Else
                                    If ResourceId = 636 Or ResourceId = 632 Or ResourceId = 384 Then
                                        Dim dTo As Date = Convert.ToDateTime(tempFilterValue2.Replace("'", ""))
                                        Dim dFrom As Date = Convert.ToDateTime(tempFilterValue.Replace("'", ""))

                                        If DateTime.Compare(dTo, dFrom) > 0 Then
                                            tempWClause = fldName & sqlOperator & tempFilterValue & " AND " & tempFilterValue2
                                        Else
                                            errMsg = "To date should be greater than from date"
                                        End If
                                        'Added by vijay Ramteke on 09-April-2009
                                    ElseIf ResourceId = 583 And fldName.ToString.ToLower.Trim = "expecteddate" Then
                                        Dim dTo As Date = Convert.ToDateTime(tempFilterValue.Replace("'", ""))
                                        Dim dFrom As Date = Convert.ToDateTime(tempFilterValue2.Replace("'", ""))
                                        Dim dCom As Date = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"))
                                        If DateTime.Compare(dTo, dCom) >= 0 Then
                                            If DateTime.Compare(dFrom, dTo) >= 0 Then
                                                tempWClause = fldName & sqlOperator & tempFilterValue & " AND " & tempFilterValue2
                                            Else
                                                errMsg = "To date should be greater than from date"
                                            End If
                                            'tempWClause = fldName & sqlOperator & tempFilterValue & " AND " & tempFilterValue2
                                        Else
                                            errMsg = "Please enter date greater than equal to current date."
                                        End If
                                    Else
                                        tempWClause = fldName & sqlOperator & tempFilterValue & " AND " & Left(tempFilterValue2, tempFilterValue2.Length - 1) & " 11:59:59 PM'"
                                    End If
                                    'Added by vijay Ramteke on 09-April-2009
                                    'Commented by Vijay Ramteke on 09-April-2009
                                    'tempWClause = fldName & sqlOperator & tempFilterValue & " AND " & Left(tempFilterValue2, tempFilterValue2.Length - 1) & " 11:59:59 PM'"
                                End If
                            End If
                            tempStringWClause = fldCaption & " " & selText & " (" & tbxText & " AND " & tbxText2 & ")"
                            If ResourceId = 560 Then
                                strClassDate = " StartDate >=" & tempFilterValue & " AND " & "EndDate <=" & tempFilterValue2
                                tempWClause = tempStringWClause
                            End If
                        Else
                            errMsg = "Too few values to filter " & fldCaption & " for." & vbCr & "You must specify exactly two values when using the Between operator."
                            'errMsg = "Invalid " & fldCaption & " value to filter for."
                        End If
                        'Else
                        'Too few operands or invalid separation character.
                        'errMsg = "Invalid separation character or invalid number of values to filter " & fldCaption & " for." & vbCrLf & _
                        '            "The character required to separate values is the semicolon (;)."
                        'End If

                    ElseIf selText = "In List" Then
                        If arrValues.GetLength(0) > 1 Then
                            'More than one value in input.
                            For Each strItem In arrValues
                                If strItem <> "" Then
                                    tempFilterValue = BuildFilterValue(strItem, fldType)
                                    If tempFilterValue <> "" Then
                                        inList &= tempFilterValue & ","
                                    Else
                                        Exit For
                                    End If
                                End If
                            Next
                            If tempFilterValue <> "" Then
                                'Remove the last comma.
                                inList = inList.Substring(0, inList.Length - 1)

                                If tblName <> "syDerived" Then
                                    tempWClause = tblName & "." & fldName & sqlOperator & "(" & inList & ")"
                                Else
                                    tempWClause = fldName & sqlOperator & "(" & inList & ")"
                                End If
                                tempStringWClause = fldCaption & " " & selText & " (" & inList & ")"
                            Else
                                errMsg = "Invalid " & fldCaption & " value to filter for."
                            End If
                        Else
                            'Too few operands or invalid separation character.
                            errMsg = "Invalid separation character or too few values to filter " & fldCaption & " for." & vbCrLf &
                                        "The character required to separate values is the semicolon (;)."
                        End If
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try

                ' Changed by Uvarajan
                If ResourceId = 554 Or ResourceId = 555 Then
                    wClause &= tempWClause
                Else
                    wClause &= tempWClause & " AND "
                End If

                ' Changed by Uvarajan
                If ResourceId = 554 Or ResourceId = 555 Then
                    stringWClause &= tempStringWClause
                Else
                    stringWClause &= tempStringWClause & " AND "
                End If


                inList = ""
            Else
                'Textbox is empty
                If tbxText2 <> "" And selText = "Between" Then
                    'Too few operands.
                    errMsg = "Too few values to filter " & fldCaption & " for." & vbCr & "You must specify exactly two values when using the Between operator."
                End If
            End If



            'For the Is Null and Empty operator the textbox should be empty.
            'If textbox is not empty, its contents are ignored.
            If  ResourceId = 285 And fldName.ToString.ToLower = "academicyearid" then
               ' do nothing 
            ElseIf IsSingleValOp(opId) And selText = "Empty" Then
                If tblName <> "syDerived" Then
                    tempWClause = tblName & "." & fldName & sqlOperator
                Else
                    tempWClause = fldName & sqlOperator
                End If
                tempStringWClause = fldCaption & " Is " & selText
                wClause &= tempWClause & " AND "
                stringWClause &= tempStringWClause & " AND "
            End If

            If  ResourceId = 285 And fldName.ToString.ToLower = "academicyearid" then
                ' do nothing 
                ElseIf IsSingleValOp(opId) And selText = "Is Null" Then
                    If tblName <> "syDerived" Then
                        tempWClause = tblName & "." & fldName & sqlOperator
                    Else
                        tempWClause = fldName & sqlOperator
                    End If
                    tempStringWClause = fldCaption & " " & selText
                    wClause &= tempWClause & " AND "
                    stringWClause &= tempStringWClause & " AND "
            End If

            'Added by Saraswathi Since, deleted
            If ResourceId = 285 And tblName.ToString.ToLower = "saAcademicYears".ToLower And fldName.ToString.ToLower = "academicyearid" Then
                If tblFilterOthers.Rows.Count > 1 Then
                    If not (DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text.Equals("Select")) then 
                        tempWClause  = "  C.AcademicYearId = (SELECT AcademicYearId FROM saAcademicYears WHERE AcademicYearCode = '" &  DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value &"')"
                        tempStringWClause = " Award Year Equal To " &  DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text
                        wClause &= tempWClause & " AND "
                        stringWClause &= tempStringWClause & " AND "
                    End If
                'Else
                '    startDay = 1
                End If
                'stringWClause &= tempStringWClause
            End If
            If ResourceId = 589 And tblName.ToString.ToLower = "arStudentClockAttendance".ToLower And fldName.ToString.ToLower = "startday" Then
                Dim startDay As Integer
                If tblFilterOthers.Rows.Count > 1 Then
                    startDay = CType(DirectCast(tblFilterOthers.Rows(1).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
                Else
                    startDay = 1
                End If
                ' tempWClause = fldCaption & "=" & noofDays
                tempStringWClause = "Start Day" & "=" & startDay & " AND "
                'wClause &= tempWClause & " AND "
                stringWClause &= tempStringWClause
            End If

            If ResourceId = 591 And tblName.ToString.ToLower = "arStuEnrollments".ToLower And fldName.ToString.ToLower = "noofdays" Then
                Dim noofDays As Integer
                If tblFilterOthers.Rows.Count > 1 Then
                    noofDays = CType(DirectCast(tblFilterOthers.Rows(1).Cells(1).Controls(0), DropDownList).SelectedItem.Value, Integer)
                Else
                    noofDays = 1
                End If

                ' tempWClause = fldCaption & "=" & noofDays
                tempStringWClause = fldCaption & "=" & noofDays
                'wClause &= tempWClause & " AND "
                stringWClause &= tempStringWClause & ";"

            End If

            rCounter += 1
        Loop 'loop through the entries in the tblFilterOThers table

        If ResourceId = 580 Or ResourceId = 600 Then
            If Not txtFirstName.Text.ToString.Trim = "" Then
                strFirstName = txtFirstName.Text.ToString.Trim
                stringWClause &= strFirstName
            End If
            If Not txtLastName.Text.ToString.Trim = "" Then
                strLastName = txtLastName.Text.ToString.Trim
                If Not strFirstName.ToString.Trim = "" Then
                    stringWClause &= " AND "
                End If
                stringWClause &= txtLastName.Text.ToString.Trim
            End If
        End If

        If errMsg = "" Then
            'No error was found.
            'If the operator was the In List then we will have an extra AND
            'so we need to remove it.
            'Modified by Michelle Rodriguez 10/25/2004.
            'If operator was the Empty and the user typed something in the textbox next to the operators' dropdown,
            'there is an extra AND at position 0 (zero) and we need to remove it.
            If wClause <> "" Then
                'Remove last occurrence of " AND".

                ' Changed by Uvarajan
                If ResourceId = 554 Or ResourceId = 555 Then
                Else
                    position = wClause.LastIndexOf(" AND", StringComparison.Ordinal)
                End If

                If position > 0 Then
                    wClause = wClause.Substring(0, position + 1)
                End If
                'Remove occurrence of " AND " at position 0 (zero).
                position = wClause.IndexOf(" AND ", StringComparison.Ordinal)
                If position = 0 Then
                    wClause = wClause.Substring(5)
                End If
            End If
            'Need to do the same with the string reprentation of the Where Clause.
            'Remove last occurrence of " AND" and the first occurrence of " AND " from the stringWhereClause.
            If stringWClause <> "" Then
                position = stringWClause.LastIndexOf(" AND ", StringComparison.Ordinal)
                If position > 0 Then
                    stringWClause = stringWClause.Substring(0, position + 1)
                End If
                position = stringWClause.IndexOf(" AND ", StringComparison.Ordinal)
                If position = 0 Then
                    stringWClause = stringWClause.Substring(0, position + 1)
                End If
            End If
        Else
            'If an error was found, clear variables.
            wClause = ""
        End If

        strErrorMsg = errMsg
        strWClause = stringWClause
        Return wClause
    End Function

    Private Function BuildFilterValue(ByVal filterValue As String, ByVal dataType As String,
                                        Optional ByVal sqlOperator As String = "") As String

        Dim returnValue As String
        Dim validValue As String

        validValue = ValidateValue(filterValue, dataType)
        If validValue <> "" Then
            Select Case dataType
                Case "Uniqueidentifier", "Char", "Varchar"
                    returnValue = "'" & validValue & "'"
                Case "Datetime"
                    If sqlOperator = " = " Or sqlOperator = " <> " Then
                        ' Changed by Uvarajan
                        If ResourceId = 554 Or ResourceId = 555 Or ResourceId = 556 Or ResourceId = 557 Then
                            returnValue = "'01/01/1900 12:00:00 AM' AND '" & validValue & " 11:59:59 PM'"
                            ''''' Code changes by Kamalesh Ahuja on 11th oct 2010 to resolve mantis issue id 19762
                        ElseIf ResourceId = 229 Then
                            returnValue = "'" & validValue & "' AND '" & validValue & " 11:59:59 PM'"
                            ''''''''
                        Else
                            returnValue = "'" & validValue & " 12:00:00 AM' AND '" & validValue & " 11:59:59 PM'"
                        End If
                    ElseIf sqlOperator = " > " Then
                        returnValue = "'" & validValue & " 11:59:59 PM'"
                    Else
                        'returnValue = "'" & validValue & " 11:59:59 PM'"
                        returnValue = "'" & validValue & "'"
                    End If
                Case Else
                    Return filterValue
            End Select
        Else
            returnValue = validValue
        End If

        Return returnValue
    End Function

    'Method added by Michelle R. Rodriguez on 11/19/2004, as part of the error handler for the FilterOthers section.
    Private Function ValidateValue(ByVal filterValue As String, ByVal dataType As String) As String
        Dim returnValue As String

        Select Case dataType
            Case "Uniqueidentifier", "Char", "Varchar"
                If filterValue.IndexOf("'", StringComparison.Ordinal) <> -1 Then
                    'Replace single quotes by double quotes.
                    returnValue = filterValue.Replace("'", "''")
                Else
                    'Return FilterValue as it was.
                    returnValue = filterValue
                End If

            Case "Datetime"
                Try
                    Dim dDate As DateTime = Date.Parse(filterValue)
                    'SLQ Server does not accept year < 1753. Therefore, a limit has to be imposed.
                    If dDate < AdvantageCommonValues.MinDate Or
                            dDate > AdvantageCommonValues.MaxDate Then
                        returnValue = ""
                    Else
                        returnValue = dDate.ToShortDateString
                    End If
                Catch
                    'Invalid date.
                    'Might be ArgumentException, FormatException or OverflowException
                    returnValue = ""
                End Try

            Case "Int"

                Dim x As Integer
                If Integer.TryParse(filterValue, x) Then
                    returnValue = x.ToString()
                Else
                    'Invalid integer.
                    'Might be ArgumentException, FormatException or OverflowException
                    returnValue = String.Empty
                End If
                'Try
                '    Dim x As Integer = Integer.Parse(FilterValue)
                '    returnValue = x
                'Catch

                '    returnValue = ""
                'End Try

            Case Else
                Return filterValue
        End Select

        Return returnValue
    End Function

    Private Function GetSQLOperator(ByVal opId As Integer) As String
        Select Case opId
            Case 1
                Return " = "
            Case 2
                Return " <> "
            Case 3
                Return " > "
            Case 4
                Return " < "
            Case 5
                Return " IN "
            Case 6
                Return " BETWEEN "
            Case 7
                Return " LIKE "
            Case 8
                Return " = '' "
            Case 9
                Return " IS NULL "
            Case Else
                Throw New Exception("The following operator is not recognized:" & opId)
        End Select
    End Function


    ' This method is going to build the dataset for the report. Then the report may be displayed
    ' in web form, which may contain a Crystal Report Viewer or a DataGrid, 
    ' or may be exported to any of the available formats.
    Private Function BuildReportDataSet(ByRef rptParamInfo As ReportParamInfo, ByRef Optional ds As DataSet = Nothing) As String

        Dim orderByClause As String = ""
        Dim stringOrderByClause As String = ""
        Dim filterListWhereClause As String
        Dim stringWhereClause As String = ""
        Dim filterOtherWhereClause As String
        Dim stringFilterOtherWClause As String = ""
        Dim strErrMsg As String = ""
        Dim rptfac As New ReportFacade

        If (ds Is Nothing) Then
            ds = New DataSet
        End If

        'Dim rptParamInfo As New FAME.AdvantageV1.Common.ReportParamInfo
        Dim objName As String
        Dim facAssembly As Assembly

        'Build the Order By clause if there are any entries in the lbxSelSort control.
        If lbxSelSort.Items.Count > 0 Then
            orderByClause = BuildOrderBy(stringOrderByClause)
        End If

        'This is for the StudentGPAReport. Dennis 8/19/09
        If ResourceId = 606 Then
            Dim i As Integer
            Dim fldCaption As String
            If lbxSelSort.Items.Count > 0 Then
                For i = 0 To lbxSelSort.Items.Count - 1
                    fldCaption = lbxSelSort.Items(i).Text
                    Select Case fldCaption
                        Case Is = "Cumulative GPA/Average"
                            rptParamInfo.SORT_CumGPA = True
                        Case Is = "Student Last Name"
                            rptParamInfo.SORT_LastName = True
                        Case Is = "Term GPA/Average"
                            rptParamInfo.SORT_TermGPA = True
                    End Select
                Next
            End If
        End If
        Dim dt As DataTable
        Dim j As Integer
        Dim drRows() As DataRow
        Dim fldCaption2 As String
        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")

        If dt.Rows.Count > 0 Then
            For j = 0 To lbxFilterList.Items.Count - 1
                fldCaption2 = lbxFilterList.Items(j).Text
                ' if student group filter
                If fldCaption2.Equals("student group", StringComparison.InvariantCultureIgnoreCase) Then
                    drRows = dt.Select("RptParamId = " & lbxFilterList.Items(j).Value)
                    If drRows.Length > 0 Then 'Match found
                        rptParamInfo.FiltersByStudentGroup = True
                    End If
                End If
            Next
        End If

        'making FiltersByStudentGroup as true if orderby for leadgrp is true for only studentlisting report.. 
        'if it works and the apprach is approved then can remove the check to include in rest of other 8 crsytal report
        If ResourceId = 229 Then
            If orderByClause.Contains("adLeadByLeadGroups.LeadGrpId") Then
                rptParamInfo.FiltersByStudentGroup = True
            End If
        End If

        Dim strPrgVerValue As String = ""
        Dim strClsDate As String = ""
        Dim strFirstName As String = ""
        Dim strLastName As String = ""

        'Build the filter list part of the Where clause.
        filterListWhereClause = BuildFilterListClause(stringWhereClause, strPrgVerValue)

        ''''' Code changes by Kamalesh ahuja to resolve mantis issue id 19762 '''
        Dim tempdt As DataTable
        If ResourceId = 229 And ChkBaseCAOnRegClasses.Checked = True Then
            'The user must specify enrollment status date range for this option
            If DirectCast(tblFilterOthers.Rows(1).Cells(2).Controls(0), TextBox).Text = "" Or DirectCast(tblFilterOthers.Rows(1).Cells(3).Controls(0), TextBox).Text = "" Then
                Dim strMsg As String
                strMsg = "You must enter an Enrollment Status Date Range start and end value when you check Base Currently Attending on Registered Classes"
                Return strMsg
            End If

            tempdt = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")

            Dim tempDv = New DataView(tempdt)  ''tempds.Tables(0).DefaultView()
            tempDv.RowFilter = "FldName = 'StatusCodeId'"
            tempDv.RowStateFilter = DataViewRowState.CurrentRows

            If tempDv.Count > 1 Then
                Return "Only Currently Attending enrollment status can be selected with the Checkbox Base Currently Attending on Register Classes"
            ElseIf tempDv.Count = 1 Then
                If rptfac.GetCurrentlyAttendingStatusId(CType(tempDv(0)("FldValue"), String)) Then
                Else
                    Return "Only Currently Attending enrollment status can be selected with the Checkbox Base Currently Attending on Register Classes"
                End If
            End If

        End If
        ''''''''''''''''

        Dim groupfilter As String = String.Empty
        If chk499.Checked Then groupfilter = 499 & "|"
        If chk500.Checked Then groupfilter = groupfilter & 500 & "|"
        If chk501.Checked Then groupfilter = groupfilter & 501 & "|"
        If chk502.Checked Then groupfilter = groupfilter & 502 & "|"
        If chk503.Checked Then groupfilter = groupfilter & 503 & "|"
        If chk533.Checked Then groupfilter = groupfilter & 533 & "|"
        If chk544.Checked Then groupfilter = groupfilter & 544

        'Build the filter other part of the Where clause.
        'If an error is found in the user input, we cannot proceed to display the report.
        Dim cohortStartDate As String = ""

        filterOtherWhereClause = BuildFilterOtherClause(stringFilterOtherWClause, strErrMsg, strClsDate, cohortStartDate, strFirstName, strLastName)
        If ResourceId = 550 Or ResourceId = 629 Then
            If txtTermStart.Text = "" And filterListWhereClause.ToLower.IndexOf("arterm", StringComparison.Ordinal) = -1 Then
                strErrMsg = "Required fields Term Start Cutoff or Term is missing."
            ElseIf filterListWhereClause.ToLower.IndexOf("arterm", StringComparison.Ordinal) = -1 And ChkRptGroupByClass.Checked Then
                Return "Select a term to use the Group by Class option"
            ElseIf txtTermStart.Text <> "" And filterListWhereClause.ToLower.IndexOf("arterm", StringComparison.Ordinal) >= 0 Then
                Return "Please select either the Term Start Cutoff filter or the Term filter and not both."
            ElseIf txtTermStart.Text = "" And filterListWhereClause.ToLower.IndexOf("arterm", StringComparison.Ordinal) >= 0 Then
                If filterListWhereClause.ToLower.IndexOf("arterm.termid in", StringComparison.Ordinal) >= 0 Then
                    strErrMsg = "Please select a single term"
                Else
                    strErrMsg = ""
                End If

            ElseIf txtTermStart.Text <> "" And filterListWhereClause.ToLower.IndexOf("arterm", StringComparison.Ordinal) = -1 Then
                strErrMsg = ""
            End If
        ElseIf ResourceId = 353 Then
            If filterOtherWhereClause.ToLower.Contains("adleads.expectedstart") = False And filterListWhereClause.ToLower.Contains("arterm") = False Then
                strErrMsg = "Required fields Expected Start Date or Term is missing."
            ElseIf filterOtherWhereClause.ToLower.Contains("adleads.expectedstart") = True And filterListWhereClause.ToLower.Contains("arterm") = True Then
                strErrMsg = "Please select either Expected Start Date filter or the Term filter and not both."
            End If
        End If
        'If resourceId = 493 Then
        '    If filterListWhereClause.IndexOf("AND SyTypeofRequirement.TypeOfReq IN", StringComparison.Ordinal) <> -1 Then
        '        strErrMsg = "Please select a single type of requirement."
        '    End If

        'End If
        If ResourceId = 629 Then

            If txtStudentId.Text <> "" And txtStuEnrollment.Text <> "" Then
                filterOtherWhereClause = ""
                filterListWhereClause = "arStuEnrollments.StuEnrollId = '" & txtStuEnrollmentId.Value & "'"
            ElseIf Not (filterListWhereClause.ToLower.Contains("sycampgrps.campgrpid") And filterListWhereClause.ToLower.Contains("arstuenrollments.prgverid")) Then
                strErrMsg = "Select Campus Group and Program Version or the Student and Enrollment"
            End If
        End If

        If ResourceId = 550 Or ResourceId = 629 Then filterOtherWhereClause = filterOtherWhereClause & rdolstChkUnits.SelectedValue.ToString & "," & chkStudentSign.Checked & "," & chkSchoolSign.Checked & "," & ChkTermModule.Checked & "," & groupfilter

        If ResourceId = 606 Then
            Dim intCounter As Integer = 1
            Dim beginRange, endRange As Decimal
            Dim strCaption As String
            Do While (intCounter <= tblFilterOthers.Rows.Count - 1)
                strCaption = DirectCast(tblFilterOthers.Rows(intCounter).Cells(0).Controls(0), Label).Text
                beginRange = CType(IIf(DirectCast(tblFilterOthers.Rows(intCounter).Cells(2).Controls(0), TextBox).Text <> "", DirectCast(tblFilterOthers.Rows(intCounter).Cells(2).Controls(0), TextBox).Text, 0), Decimal)
                endRange = CType(IIf(DirectCast(tblFilterOthers.Rows(intCounter).Cells(3).Controls(0), TextBox).Text <> "", DirectCast(tblFilterOthers.Rows(intCounter).Cells(3).Controls(0), TextBox).Text, 0), Decimal)
                Select Case strCaption
                    Case Is = "Cumulative GPA/Avg Range"
                        rptParamInfo.CumGPARangeBegin = beginRange
                        rptParamInfo.CumGPARangeEnd = endRange
                    Case Is = "Term Credits Range"
                        rptParamInfo.TermCreditsRangeBegin = beginRange
                        rptParamInfo.TermCreditsRangeEnd = endRange
                    Case Is = "Term GPA/Avg Range"
                        rptParamInfo.TermGPARangeBegin = beginRange
                        rptParamInfo.TermGPARangeEnd = endRange
                End Select
                intCounter += 1
            Loop
        End If

        If ResourceId = 554 Or ResourceId = 555 Then
            If txtMinimumAttendance.Text <> "" And txtMaxAttendance.Text <> "" Then
                stringFilterOtherWClause = txtMinimumAttendance.Text & " And " & txtMaxAttendance.Text
            ElseIf txtMinimumAttendance.Text <> "" Then
                stringFilterOtherWClause = txtMinimumAttendance.Text
            ElseIf txtMaxAttendance.Text <> "" Then
                stringFilterOtherWClause = txtMaxAttendance.Text
            ElseIf txtMaxAttendance.Text = "" Then
                strErrMsg = "Required field Maximum Attendance is missing."
            ElseIf txtMinimumAttendance.Text = "" Then
                strErrMsg = "Required field Maximum Attendance is missing."
            Else

                strErrMsg = "Required field Minimum Attendance is missing."
            End If
        End If

        'Added By  Hepsiba
        If ResourceId = 560 Then
            If txtMinimumAttendance.Text <> "" Then
                stringFilterOtherWClause = txtMinimumAttendance.Text
            Else
                strErrMsg = "Required field Minimum Score is missing."
            End If
        End If

        '' New Code Added By Vijay Ramteke On July 08, 2010 For Mantis Id 11856
        If ResourceId = 633 Then
            If ddlConsAbsCompOp.SelectedValue = "between" Then
                If txtNumberConsecutiveAbsentDays.Text <> "" And txtNumberConsecutiveAbsentDays2.Text <> "" Then
                    Try
                        Dim ncad1 As Integer
                        Dim ncad2 As Integer
                        Try
                            ncad1 = CType(txtNumberConsecutiveAbsentDays.Text, Integer)
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            Return "Number of Consecutive Absent Days is only numeric type."
                        End Try
                        Try
                            ncad2 = CType(txtNumberConsecutiveAbsentDays2.Text, Integer)
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            Return "Number of Consecutive Absent Days is only numeric type."
                        End Try
                        If ncad1 > 0 And ncad2 > 0 Then
                            rptParamInfo.NumberOfConsecutiveAbsentDays = txtNumberConsecutiveAbsentDays.Text
                            rptParamInfo.NumberOfConsecutiveAbsentDays2 = txtNumberConsecutiveAbsentDays2.Text
                        Else
                            strErrMsg = "Number of Consecutive Absent Days must be greater than 0."
                        End If
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        strErrMsg = "Number of Consecutive Absent Days is only numeric type."
                    End Try
                Else
                    strErrMsg = "Required fields Number of Consecutive Absent Days is missing."
                End If
            Else
                If txtNumberConsecutiveAbsentDays.Text <> "" Then
                    Try
                        If CType(txtNumberConsecutiveAbsentDays.Text, Integer) > 0 Then
                            rptParamInfo.NumberOfConsecutiveAbsentDays = txtNumberConsecutiveAbsentDays.Text
                            rptParamInfo.NumberOfConsecutiveAbsentDays2 = "0"
                        Else
                            strErrMsg = "Number of Consecutive Absent Days must be greater than 0."
                        End If
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        strErrMsg = "Number of Consecutive Absent Days is only numeric type."
                    End Try
                Else
                    strErrMsg = "Required fields Number of Consecutive Absent Days is missing."
                End If
            End If

        End If

        rptParamInfo.UsedScheduledDays = chkUsedScheduleDays.Checked
        rptParamInfo.CompOperator = ddlConsAbsCompOp.SelectedValue

        'If no error is found, proceed to build report dataset.
        If strErrMsg = "" Then
            rptParamInfo.ResId = ResourceId
            If CInt(ViewState("SQLID")) > 0 Then
                rptParamInfo.SqlId = CInt(ViewState("SQLID"))
                rptParamInfo.ObjId = 0
            Else
                rptParamInfo.SqlId = 0
                rptParamInfo.ObjId = CInt(ViewState("OBJID"))
            End If
            rptParamInfo.OrderBy = orderByClause
            rptParamInfo.OrderByString = stringOrderByClause
            rptParamInfo.FilterList = filterListWhereClause
            rptParamInfo.FilterListString = stringWhereClause
            rptParamInfo.FilterOther = filterOtherWhereClause
            rptParamInfo.FilterOtherString = stringFilterOtherWClause
            rptParamInfo.ShowFilters = chkRptFilters.Checked
            rptParamInfo.ShowSortBy = chkRptSort.Checked
            rptParamInfo.ShowRptDescription = chkRptDescrip.Checked
            rptParamInfo.ShowRptInstructions = chkRptInstructions.Checked
            rptParamInfo.ShowRptNotes = chkRptNotes.Checked
            rptParamInfo.ShowRptStudentGroup = chkRptStudentGroup.Checked
            rptParamInfo.ShowRptDateIssue = chkRptDateIssue.Checked
            rptParamInfo.SuppressHeader = chkRptSuppressHeader.Checked
            rptParamInfo.GroupByClass = ChkRptGroupByClass.Checked
            rptParamInfo.HideRptProgramVersion = ChkRptProgramVersion.Checked
            '' Code Added by Kamalesh Ahuja on 29 Oct 09 '''
            rptParamInfo.ShowRptClassDates = CType(ChkRptShowClassDates.Checked.ToString, Boolean)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '' Code Added by Kamalesh Ahuja on 23 Jan 2010 to resolve Mantis Issue Id 17903 '''
            rptParamInfo.ShowRptProjExceedBal = CType(ChkRptProjectionsExceed.Checked.ToString, Boolean)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '' Code Added by Vijay Ramteke on March 05, 2010 to resolve Mantis Issue Id 18567 '''
            rptParamInfo.ShowRptGroupByEnrollment = ChkRptShowGroupByEnrollment.Checked
            rptParamInfo.ShowRptDisbNotBeenPaid = ChkRptShowDisbNotBeenPaid.Checked
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '' Code Added by Kamalesh ahuja on 31st May 2010 to resolve mantis issue id 18868
            rptParamInfo.ShowLegalDisclaimer = ChkShowLegalDisc.Checked
            '''''''''''''''''
            rptParamInfo.ShowTermProgressDescription = chkshowTermProgressDescription.Checked
            '' Code Added by Kamalesh ahuja on 08 Oct 2010 to resolve mantis issue id 19762
            rptParamInfo.BaseCAOnRegClasses = ChkBaseCAOnRegClasses.Checked
            '''''''''''''''''

            '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178
            rptParamInfo.ShowUseStuCurrStatus = chkRptUseStuCurrStatus.Checked
            rptParamInfo.ShowUseSignLineForAttnd = chkUseSignatureForAttendance.Checked
            '' Code Added By Vijay Ramteke On October 27, 2010 For Rally Id US1178

            'rptParamInfo.CampusId = HttpContext.Current.Request("cmpid").ToString

            If rptParamInfo.FilterList.ToLower.IndexOf("campusid in", StringComparison.Ordinal) <> -1 Then
                strErrMsg = "Please select a single campus"
                If ResourceId = 554 Then
                    Return "Please select a single campus"
                End If
            End If
            If rptParamInfo.FilterList.ToLower.IndexOf("campusid", StringComparison.Ordinal) <> -1 Then
                rptParamInfo.CampusId = getCampusId(rptParamInfo.FilterList)
            Else
                rptParamInfo.CampusId = AdvantageSession.UserState.CampusId.ToString 'HttpContext.Current.Request("cmpid").ToString
            End If

            rptParamInfo.ShowTermOrModule = ChkTermModule.Checked
            rptParamInfo.ShowCurrentBalance = ChkCurrentBalance.Checked
            rptParamInfo.ShowTotalCost = ChkTotalCost.Checked
            If ResourceId = 629 Then rptParamInfo.ShowRptDescription = chkOfficialTitle.Checked
            If ViewState("Resource").Equals("Revenue Ratio") Then
                rptParamInfo.RptTitle = CType((MyAdvAppSettings.AppSettings("RevenueRatio") & " Rule"), String)
            Else
                rptParamInfo.RptTitle = CType(ViewState("Resource"), String)
            End If
            If ResourceId = 245 Then
                'Special case: Multiple Student Transcript
                If ChkRptShowClassDates.Checked And ChkTermModule.Checked Then
                    Return "Please select either the Show Class Dates or the Show Term/Module checkbox and not both"
                End If
                If Not (Session("OfficialSchoolLogo") Is Nothing) Then
                    rptParamInfo.SchoolLogo = DirectCast(Session("OfficialSchoolLogo"), Byte())
                Else
                    rptParamInfo.SchoolLogo = rptfac.GetSchoolLogo(True).Image
                    Session("OfficialSchoolLogo") = rptParamInfo.SchoolLogo
                End If
                'DE8390 3/7/2013 Janet Robinson
                rptParamInfo.ShowOfficialTranscript = chkShowOffTrans.Checked
            Else
                If Not (Session("SchoolLogo") Is Nothing) Then
                    rptParamInfo.SchoolLogo = DirectCast(Session("SchoolLogo"), Byte())
                Else
                    rptParamInfo.SchoolLogo = rptfac.GetSchoolLogo.Image
                    Session("SchoolLogo") = rptParamInfo.SchoolLogo
                End If
            End If

            'added by Uvarajan
            If ResourceId = 560 Then
                Dim arr() As String
                arr = rptParamInfo.FilterList.Split(CType("'", Char))
                rptParamInfo.CampGrpId = arr(1)
                rptParamInfo.PrgVerId = strPrgVerValue
                rptParamInfo.CohortStartDate = cohortStartDate
                rptParamInfo.Classdate() = strClsDate
            End If
            If ResourceId = 580 Or ResourceId = 600 Then
                lbxFilterList.Visible = False
                lbxSelFilterList.Visible = False

                Dim strFilter As String = ""
                Dim strCampGrpId As String = ""
                Dim strCampDescrip As String = ""
                If ddlCampGrpId.SelectedIndex = 0 Then
                    DisplayErrorMessage("Please select a campus group")
                    Return "No Campus Group"
                End If
                If ddlCampGrpId.SelectedIndex >= 1 Then
                    For Each item As ListItem In ddlCampGrpId.Items
                        If item.Selected = True Then
                            strCampGrpId &= item.Value & "','"
                            strCampDescrip &= item.Text & ","
                            strCampGrpId = Mid(strCampGrpId, 1, InStrRev(strCampGrpId, "'") - 2)
                            strCampDescrip = Mid(strCampDescrip, 1, InStrRev(strCampDescrip, ",") - 1)
                        End If
                    Next
                    rptParamInfo.CampGrpId = strCampGrpId
                End If
                If Not strCampDescrip.ToString.Trim = "" Then
                    strFilter = "Campus Group Equal To (" & strCampDescrip & ")"
                End If
                If ddlTermId.SelectedIndex >= 1 Then
                    strFilter &= " and Term Equal To (" & ddlTermId.SelectedItem.Text & ")"
                End If
                If ddlPrgVerId.SelectedIndex >= 1 Then
                    strFilter &= " and Program Version Equal to (" & ddlPrgVerId.SelectedItem.Text & ")"
                End If
                If Not txtStartDate.Text.ToString.Trim = "" And Not txtEndDate.Text.ToString.Trim = "" Then
                    strFilter &= " and Date Attended between " & txtStartDate.Text.ToString.Trim & " and " & txtEndDate.Text.ToString.Trim
                End If
                If Not txtTermStartDate.Text.ToString.Trim = "" And Not txtTermEndDate.Text.ToString.Trim = "" Then
                    strFilter &= " and Term Starts between " & txtTermStartDate.Text.ToString.Trim & " and " & txtTermEndDate.Text.ToString.Trim
                End If
                rptParamInfo.FilterOtherString = strFilter.ToString
                rptParamInfo.FirstName = strFirstName
                rptParamInfo.LastName = strLastName
                If ddlTermId.SelectedIndex >= 1 Then
                    rptParamInfo.TermId = ddlTermId.SelectedValue
                    rptParamInfo.TermDescrip = ddlTermId.SelectedItem.Text
                Else
                    rptParamInfo.TermId = ""
                End If
                If ddlPrgVerId.SelectedIndex >= 1 Then
                    rptParamInfo.PrgVerId = ddlPrgVerId.SelectedValue
                    rptParamInfo.PrgVerDescrip = strFilter.ToString
                Else
                    rptParamInfo.PrgVerId = ""
                    rptParamInfo.PrgVerDescrip = strFilter.ToString
                End If
                If txtStartDate.Text.ToString.Trim = "" Then
                    rptParamInfo.StartDate = CType("01/01/1900", Date)
                Else
                    Try
                        rptParamInfo.StartDate = CDate(txtStartDate.Text)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisplayErrorMessage("Please check the attendance start date")
                    End Try
                End If
                If txtEndDate.Text.ToString.Trim = "" Then
                    rptParamInfo.EndDate = CType("01/01/1900", Date)
                Else
                    Try
                        rptParamInfo.EndDate = CDate(txtEndDate.Text)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisplayErrorMessage("Please check the attendance end date")
                    End Try
                End If
                If txtTermStartDate.Text.ToString.Trim = "" Then
                    rptParamInfo.TermStartDate = CType("01/01/1900", Date)
                Else
                    Try
                        rptParamInfo.TermStartDate = CDate(txtTermStartDate.Text)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisplayErrorMessage("Please check the term start date")
                    End Try
                End If
                If txtTermEndDate.Text.ToString.Trim = "" Then
                    rptParamInfo.TermEndDate = CType("01/01/1900", Date)
                Else
                    Try
                        rptParamInfo.TermEndDate = CDate(txtTermEndDate.Text)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisplayErrorMessage("Please check the term end date")
                    End Try
                End If
            End If
            'multiple student attendance
            If ResourceId = 557 Then
                Dim arr() As String
                arr = rptParamInfo.FilterOther.Split(CType("'", Char))
                rptParamInfo.FilterOtherString = arr(3)
            End If

            If ResourceId = 550 Or ResourceId = 629 Then
                rptParamInfo.CutoffDate = txtTermStart.Text
            End If

            'If the SQLID is not 0 then we need to create the class that generates DataSet for simple report.
            'Else we need to use OBJID to get the name of the object and create object using reflection.
            If rptParamInfo.SqlId > 0 Then
                ds = rptfac.GetReportDataSet(rptParamInfo)
            Else
                'Get the name of the object associated with the OBJID.
                objName = rptfac.GetOBJName(rptParamInfo.ObjId)
                'Use reflection to create an instance of the object from the name.
                facAssembly = Assembly.LoadFrom(Server.MapPath("..") + "\bin\BusinessFacade.dll")
                Dim objBaseRptFac As BaseReportFacade
                objBaseRptFac = DirectCast(facAssembly.CreateInstance("FAME.AdvantageV1.BusinessFacade." + objName), BaseReportFacade)
                ds = objBaseRptFac.GetReportDataSet(rptParamInfo)
            End If

            'Add the Report Params into the dataset so we can display it in the report is requested
            If ResourceId = 606 Then
                'Sorting
                If rptParamInfo.ShowSortBy = True And ds.Tables.Count = 6 Then
                    ds.Tables("ReportParams").Rows(0).Item("ShowSortBy") = True
                    If rptParamInfo.OrderByString <> "" Then
                        ds.Tables("ReportParams").Rows(0).Item("OrderByString") = "Sort Order: " & stringOrderByClause
                    End If
                End If
                'Filtering
                If rptParamInfo.ShowFilters = True And ds.Tables.Count = 6 Then
                    ds.Tables("ReportParams").Rows(0).Item("ShowFilters") = True
                    Dim strfilterListWhereClause As String
                    Dim strFilterOther As String = String.Empty
                    If rptParamInfo.FilterListString <> "" Then
                        strfilterListWhereClause = rptParamInfo.FilterListString
                        ds.Tables("ReportParams").Rows(0).Item("FilterByString") = "Filter Criteria: " & strfilterListWhereClause
                        'Else
                        '    strfilterListWhereClause = ""
                    End If
                    'Filter Other
                    Const tolerance As Double = 0.01
                    If Math.Abs(rptParamInfo.TermGPARangeBegin - 0.0) > tolerance Then
                        strFilterOther = "Additional Filter Criteria: Term GPA/Avg Range Between (" & rptParamInfo.TermGPARangeBegin & " and " & rptParamInfo.TermGPARangeEnd & ")"
                    End If

                    If Math.Abs(rptParamInfo.TermCreditsRangeBegin - 0.0) > tolerance Then
                        If strFilterOther <> "" Then
                            strFilterOther = strFilterOther & " and Term Credits Range Between (" & rptParamInfo.TermCreditsRangeBegin & " and " & rptParamInfo.TermCreditsRangeEnd & ")"
                        Else
                            strFilterOther = "Additional Filter Criteria: Term Credits Range Between (" & rptParamInfo.TermCreditsRangeBegin & " and " & rptParamInfo.TermCreditsRangeEnd & ")"
                        End If
                    End If

                    If Math.Abs(rptParamInfo.CumGPARangeBegin - 0.0) > tolerance Then
                        If strFilterOther <> "" Then
                            strFilterOther = strFilterOther & " and Cum GPA/Avg Range Between (" & rptParamInfo.CumGPARangeBegin & " and " & rptParamInfo.CumGPARangeEnd & ")"
                        Else
                            strFilterOther = "Additional Filter Criteria: Cum GPA/Avg Range Between (" & rptParamInfo.CumGPARangeBegin & " and " & rptParamInfo.CumGPARangeEnd & ")"
                        End If
                    End If

                    If strFilterOther <> "" Then
                        ds.Tables("ReportParams").Rows(0).Item("FilterOtherString") = strFilterOther
                    End If
                End If
            End If

            ''Code changed by kamalesh Ahuja on 03 June 2010 to resolve mantis issue Id 17802 ''
            ''If resourceId = 580 And ds.Tables(0).Rows.Count = 0 Then
            If ResourceId = 580 And ds.Tables.Count = 0 Then
                Return "There is no data matching selection."
            End If

            If ds.Tables.Count <> 0 Then
                If ResourceId = 550 Or ResourceId = 629 Then
                    If ds.Tables(3).Rows.Count > 0 Then
                        Session("ReportDS") = ds
                        Session("RptInfo") = rptParamInfo
                    Else
                        Session("ReportDS") = Nothing
                        Session("RptInfo") = Nothing
                    End If
                ElseIf ResourceId = 606 And ds.Tables.Count = 6 Then
                    'Student GPA Report DD 9/10/09
                    If ds.Tables(1).Rows.Count > 0 And ds.Tables("EnrollmentSummary").Rows.Count > 0 Then
                        Session("ReportDS") = ds
                        Session("RptInfo") = rptParamInfo
                    Else
                        Session("ReportDS") = Nothing
                        Session("RptInfo") = Nothing
                    End If
                Else
                    If ds.Tables(0).Rows.Count > 0 Then
                        Session("ReportDS") = ds
                        Session("RptInfo") = rptParamInfo
                    Else
                        Session("ReportDS") = Nothing
                        Session("RptInfo") = Nothing
                    End If

                End If
            Else
                Session("RptInfo") = Nothing
                Session("ReportDS") = Nothing
            End If
        Else
            'Errors where found while building WHERE or/and ORDER BY clause(s).
            Session("RptInfo") = Nothing
            Session("ReportDS") = Nothing
        End If 'strErrMsg = ""l
        Return strErrMsg
    End Function

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click
        'Dim t As New System.Diagnostics.Stopwatch
        't.Start()
        Dim rptParamInfo As New ReportParamInfo
        Dim ds As New DataSet
        If Page.IsValid Then
            '' Code Added By Vijay Ramteke On June 19, 2010 to For Mantis Id 18141
            If (ResourceId = 629) And (txtStuEnrollmentId.Value <> "" And Not txtStuEnrollmentId.Value.ToLower.Contains("stuenrollmentid")) Then
                Dim facade As New RegFacade
                If Not facade.IsStudentTranscriptHold(txtStuEnrollmentId.Value) = "0" Then
                    DisplayErrorMessage("The report cannot be exported as the student transcript is put on hold")
                    Exit Sub
                End If
            End If
            Dim strErrMsg As String

            ' ASk if we are in MajorMinor Schools and if report is Student Transcript ---------------------------------
            ' This report is SSRS type report
            Dim setting As String = GetSettingString("MajorsMinorsConcentrations")
            If setting.ToUpper() = "YES" AndAlso ResourceId = 245 Then
                'Create Specif paramReport for this report
                strErrMsg = CompleteReportParamInfo(rptParamInfo)
                ' Create Reports Schools with MajorMinor
                BuildMajorMinorReport(rptParamInfo)
                If Not String.IsNullOrEmpty(strErrMsg) Then
                    DisplayErrorMessage(strErrMsg)
                End If
                Return
            End If
            '  End Transcript SSRS Report -------------------------------------------------------------------------------

            ' Cristal Report no major minor schools and others
            strErrMsg = BuildReportDataSet(rptParamInfo, ds)
            If strErrMsg = "" Then

                'Since no error was found, proceed to display report in Export Viewer page.
                If Not (Session("ReportDS") Is Nothing) Then
                    '   Make sure to clear any previously loaded report docuemnt objects
                    Session("RptObject") = Nothing
                    '   Setup the properties of the new window
                    'Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsMedium + ",menubar=yes"
                    'Const name As String = "ReportExportViewer"
                    'Dim url As String = "../SY/" + name + ".aspx?ExportTo=" & ddlExportTo.SelectedValue.ToString

                    'CommonWebUtilities.OpenClientChildWindow(Page, url, name, winSettings)

                    Dim format = ddlExportTo.SelectedValue.ToString()
                    If (format IsNot Nothing) Then
                        LoadReport(format, ds, rptParamInfo)
                    End If

                Else
                    'Alert user of no data matching selection.
                    strErrMsg = "There is no data matching selection."
                    DisplayErrorMessage(strErrMsg)
                End If
            Else
                'Alert user of error.
                DisplayErrorMessage(strErrMsg)
            End If
        Else

            If (ViewState("boolShowErr") IsNot Nothing) Then
                'Dim strErrMsg As String = getErrString()
                Dim strErrMsg As String = GetValidateErrMsg()
                DisplayErrorMessage(strErrMsg)
            End If
            ViewState("boolShowErr") = True
        End If

        't.Stop()
        ' DisplayErrorMessage(t.Elapsed.Duration.TotalSeconds)
    End Sub
    Private Sub LoadReport(format As String, ds As DataSet, rptParamInfo As Object)
        Dim rptXport As New RptExport
        Dim objReport As ReportDocument

        Select Case Session("RptAgency")
            Case IPEDSCommon.AgencyName
                rptParamInfo = DirectCast(Session("RptInfo"), ReportParamInfoIPEDS)
            Case Else
                rptParamInfo = DirectCast(Session("RptInfo"), ReportParamInfo)
        End Select

        PageTitle &= CType(rptParamInfo.RptTitle, String)


        If Session("RptObject") IsNot Nothing Then
            Select Case Session("RptAgency")
                Case IPEDSCommon.AgencyName
                    objReport = DirectCast(Session("RptObject"), AdvantageReportIPEDS)
                Case Else
                    objReport = DirectCast(Session("RptObject"), ReportDocument)
            End Select
        Else
            Select Case Session("RptAgency")
                Case IPEDSCommon.AgencyName
                    objReport = New AdvantageReportIPEDS(ds, CType(rptParamInfo, ReportParamInfoIPEDS))
                Case Else
                    objReport = (New AdvantageReportFactory).CreateAdvantageReport(ds, CType(rptParamInfo, ReportParamInfo))
            End Select
            Session("RptObject") = objReport
        End If
        Dim contentType = ""
        rptXport.ExportToStream(Context, objReport, format, rptParamInfo.RptTitle.ToString)
        'Dim path = rptXport.ExportToDisk(Context, objReport, Session.SessionID.ToString(), format, contentType, rptParamInfo.RptTitle.ToString)

    End Sub

    Private Function CompleteReportParamInfo(ByRef rptParamInfo As ReportParamInfo) As String

        Dim dt As DataTable = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")

        rptParamInfo.CampusId = GetListFromDataSet("CampusId", dt)
        If (String.IsNullOrEmpty(rptParamInfo.CampusId)) Then
            Return "You must select a CampusId"
        End If

        rptParamInfo.PrgVerId = GetListFromDataSet("PrgVerId", dt)
        If (String.IsNullOrEmpty(rptParamInfo.PrgVerId)) Then
            Return "You must select a Program Version"
        End If

        'Get Policy for this school in exam.
        rptParamInfo.SettingRetakeTestPolicy = GetSettingString("SettingRetakeTestPolicy")
        If (IsNothing(rptParamInfo.SettingRetakeTestPolicy)) Then
            rptParamInfo.SettingRetakeTestPolicy = "TAKEBEST"
        End If

        rptParamInfo.TermId = GetListFromDataSet("TermId", dt)
        rptParamInfo.StatusCodeId = GetListFromDataSet("StatusCodeId", dt)
        rptParamInfo.SuppressHeader = chkRptSuppressHeader.Checked
        rptParamInfo.ShowOfficialTranscript = chkShowOffTrans.Checked
        rptParamInfo.ShowUseSignLineForAttnd = chkUseSignatureForAttendance.Checked
        rptParamInfo.ShowLegalDisclaimer = ChkShowLegalDisc.Checked
        rptParamInfo.ShowTermProgressDescription = chkshowTermProgressDescription.Checked
        rptParamInfo.ExportType = ddlExportTo.SelectedValue.ToString.ToLower()

        'Term Dates
        If (Not String.IsNullOrEmpty(txtTermStartDate.Text)) Then
            If (Date.TryParse(txtTermStart.Text, rptParamInfo.TermStartDate) = False) Then
                Return "The Start Date is not correct"
            End If
        Else
            rptParamInfo.TermStartDate = New DateTime(1900, 1, 1)
        End If

        If (Not String.IsNullOrEmpty(txtTermEndDate.Text)) Then
            If (Date.TryParse(txtTermEndDate.Text, rptParamInfo.TermEndDate) = False) Then
                Return "The End Date is not correct"
            End If
        Else
            rptParamInfo.TermEndDate = Now
        End If

        'Filter ot Graduation Date
        If (Not String.IsNullOrEmpty(txtNumberConsecutiveAbsentDays.Text)) Then
            rptParamInfo.ExpectedGraduationOperation = ddlConsAbsCompOp.SelectedValue.ToUpper()
        Else
            rptParamInfo.ExpectedGraduationOperation = Nothing
        End If

        'Expected Graduation date
        If (Not String.IsNullOrEmpty(txtNumberConsecutiveAbsentDays.Text)) Then
            If (Date.TryParse(txtNumberConsecutiveAbsentDays.Text, rptParamInfo.StartDate) = False) Then
                Return "The Start Date is not correct"
            End If
        Else
            rptParamInfo.StartDate = New DateTime(1900, 1, 1)
        End If

        If (Not String.IsNullOrEmpty(txtNumberConsecutiveAbsentDays2.Text)) Then
            If (Date.TryParse(txtNumberConsecutiveAbsentDays2.Text, rptParamInfo.EndDate) = False) Then
                Return "The End Date is not correct"
            End If
        Else
            rptParamInfo.EndDate = Now
        End If

        Return String.Empty
    End Function



    Private Function GetListFromDataSet(fieldIdentifier As String, dt As DataTable) As String

        Dim list = From row In dt.AsEnumerable()
                   Where row.Field(Of String)("FldName") = fieldIdentifier
                   Select row.Field(Of String)("FldValue")
        If (Not IsNothing(list) AndAlso list.Any()) Then
            Dim tmp As String = list.Aggregate(String.Empty, Function(current, s) current + "," + s)
            tmp = tmp.TrimStart(CType(",", Char))
            Return tmp
        Else
            Return String.Empty
        End If
    End Function

    Private Sub BuildMajorMinorReport(ByRef rptParamInfo As ReportParamInfo)
        'Provisional disclaimer stay here
        Const disclaimer As String = "This transcript is official only when signed by the Registrar and embossed with the School's raised seal. Federal Law prohibits the release of this document to any person or institution without the written consent of the student."
        'Create the object to hold parameters to Report.
        Dim listp = New List(Of ReportParameters)
        Dim param As ReportParameters
        param = New ReportParameters With {.Name = "ProgramVersionList", .Label = "ProgramVersionList", .Value = rptParamInfo.PrgVerId}
        listp.Add(param)
        param = New ReportParameters With {.Name = "Imagecode", .Label = "Imagecode", .Value = "RSTRAN1"} ' Image Code for This report...
        listp.Add(param)
        param = New ReportParameters With {.Name = "CampusList", .Label = "CampusList", .Value = rptParamInfo.CampusId}
        listp.Add(param)
        param = New ReportParameters With {.Name = "EnrollmentStatusList", .Label = "EnrollmentStatusList", .Value = rptParamInfo.StatusCodeId}
        listp.Add(param)
        param = New ReportParameters With {.Name = "FilterStartDate", .Label = "FilterStartDate", .Value = rptParamInfo.TermStartDate.ToString("yyyy-MM-dd")}
        listp.Add(param)
        param = New ReportParameters With {.Name = "FilterEndDate", .Label = "FilterEndDate", .Value = rptParamInfo.TermEndDate.ToString("yyyy-MM-dd")}
        listp.Add(param)
        param = New ReportParameters With {.Name = "ExpectedGraduationDateStart", .Label = "ExpectedGraduationDateStart", .Value = rptParamInfo.StartDate.ToString("yyyy-MM-dd")}
        listp.Add(param)
        param = New ReportParameters With {.Name = "ExpectedGraduationDateEnd", .Label = "ExpectedGraduationDateEnd", .Value = rptParamInfo.EndDate.ToString("yyyy-MM-dd")}
        listp.Add(param)
        param = New ReportParameters With {.Name = "ExpectedGraduationOperation", .Label = "ExpectedGraduationOperation", .Value = rptParamInfo.ExpectedGraduationOperation}
        listp.Add(param)

        param = New ReportParameters With {.Name = "IsOfficialTranscript", .Label = "IsOfficialTranscript", .Value = rptParamInfo.ShowOfficialTranscript.ToString()}
        listp.Add(param)
        param = New ReportParameters With {.Name = "IsShowedSignatureLine", .Label = "IsShowedSignatureLine", .Value = (Not (rptParamInfo.ShowUseSignLineForAttnd)).ToString()}
        listp.Add(param)
        If rptParamInfo.ShowLegalDisclaimer Then
            param = New ReportParameters With {.Name = "LegalDisclaimer", .Label = "LegalDisclaimer", .Value = disclaimer}
        Else
            param = New ReportParameters With {.Name = "LegalDisclaimer", .Label = "LegalDisclaimer", .Value = Nothing}
        End If
        listp.Add(param)
        param = New ReportParameters With {.Name = "SignatureLegalDisclaimer", .Label = "SignatureLegalDisclaimer", .Value = "Signature of Registrar"}
        listp.Add(param)
        param = New ReportParameters With {.Name = "IsHeaderShowed", .Label = "IsHeaderShowed", .Value = (Not (rptParamInfo.SuppressHeader)).ToString()}
        listp.Add(param)
        param = New ReportParameters With {.Name = "CGPASetting", .Label = "CGPASetting", .Value = rptParamInfo.SettingRetakeTestPolicy}
        listp.Add(param)
        param = New ReportParameters With {.Name = "ShowTermProgressDescription", .Label = "ShowTermProgressDescription", .Value = rptParamInfo.ShowTermProgressDescription.ToString()}
        listp.Add(param)
        'Cleanup ListFilterString
        Dim params As String
        If (Not String.IsNullOrEmpty(rptParamInfo.FilterListString)) AndAlso (Not String.IsNullOrWhiteSpace(rptParamInfo.FilterListString)) Then
            Dim indexlow As Int32 = rptParamInfo.FilterListString.IndexOf("(", StringComparison.Ordinal)
            Dim indexHigh As Int32 = rptParamInfo.FilterListString.LastIndexOf(")", StringComparison.Ordinal)
            If indexHigh < 0 Then
                indexlow = rptParamInfo.FilterListString.ToLower().IndexOf("to", StringComparison.Ordinal)
                params = rptParamInfo.FilterListString.Substring(indexlow + 2).Trim()
            Else
                params = rptParamInfo.FilterListString.Substring(indexlow + 1, (indexHigh - indexlow) - 1)
                params = params.Replace("'", String.Empty).Replace(", ", ",")
            End If

            rptParamInfo.FilterListString = params

        End If
        param = New ReportParameters With {.Name = "FilterTermDescription", .Label = "FilterTermDescription", .Value = rptParamInfo.FilterListString}
        listp.Add(param)
        'Select report path and name....
        Dim strReportPathAndName As String = ConfigurationManager.AppSettings("Reports.ReportsFolder") + "TranscriptSingleStudent/TrascriptMultipleStudent"
        ' Invoque the report....
        Dim report As ReportGenerator2 = New ReportGenerator2()
        Dim getReportAsBytes As [Byte]() = report.RenderReport2(rptParamInfo.ExportType, userId, "", "", strReportPathAndName, listp)
        ExportReport(rptParamInfo.ExportType, getReportAsBytes)
    End Sub

    Private Sub ExportReport(ByVal exportFormat As String, ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case exportFormat.ToLower
            Case Is = "pdf"
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case "excel"
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
            Case "word"
                strExtension = "doc"
                strMimeType = "application/vnd.ms-word"
            Case "csv"
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select
        Session("SSRS_FileExtension") = strExtension
        Session("SSRS_MimeType") = strMimeType
        Session("SSRS_ReportOutput") = getReportAsBytes
        'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        Const script As String = "window.open('../SY/DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');"
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        'If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInClientMessageBox(Me.Page, errorMessage)
        'End If
    End Sub
    Private Sub btnFriendlyPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnFriendlyPrint.Click
        Dim rptParamInfo As New ReportParamInfo
        If Page.IsValid Then
            '' Code Added By Vijay Ramteke On June 19, 2010 to For Mantis Id 18141
            If (ResourceId = 629) And (txtStuEnrollmentId.Value <> "" And Not txtStuEnrollmentId.Value.ToLower.Contains("stuenrollmentid")) Then
                Dim facade As New RegFacade
                If Not facade.IsStudentTranscriptHold(txtStuEnrollmentId.Value) = "0" Then
                    DisplayErrorMessage("The report cannot be displayed as the student transcript is put on hold")
                    Exit Sub
                End If
            End If
            '' Code Added By Vijay Ramteke On June 19, 2010 to For Mantis Id 18141
            Dim strErrMsg As String = BuildReportDataSet(rptParamInfo)
            If strErrMsg = "" Then
                'Since no error was found, proceed to display report in Export Viewer page.
                If Not (Session("ReportDS") Is Nothing) Then

                    '   Make sure to clear any previously loaded report docuemnt objects
                    Session("RptObject") = Nothing

                    '   Setup the properties of the new window
                    Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsMedium + ",menubar=yes"
                    Dim name As String = "ReportFriendlyPrint"
                    Dim url As String = "../SY/" + name + ".aspx"
                    CommonWebUtilities.OpenClientChildWindow(Page, url, name, winSettings)
                Else
                    'Alert user of no data matching selection.
                    strErrMsg = "There is no data matching selection."
                    DisplayErrorMessage(strErrMsg)
                End If
            Else
                'Alert user of error.
                DisplayErrorMessage(strErrMsg)
            End If
        Else
            If (ViewState("boolShowErr") IsNot Nothing) Then
                Dim strErrMsg As String = GetValidateErrMsg()
                DisplayErrorMessage(strErrMsg)
            End If
            ViewState("boolShowErr") = True
        End If
    End Sub

    Protected Sub lbtPrompt_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtPrompt.Click
        Dim objRptParams As New RptParamsFacade
        Dim i As Integer
        Dim ds As DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim opId As Integer
        Dim fldValue As String
        Dim rptParamId As Integer
        Dim rowCounter As Integer
        Dim objCommon As New CommonUtilities
        Dim errMsg As String

        If txtPrefName.Text = "" Then
            'Alert user that a Preference name is required.
            errMsg = "Preference Name is required."
            'Display Error Message.
            DisplayErrorMessage(errMsg)
        Else
            'Before adding or updating we record we need to validate the entries
            'in the filter others section.
            If ValidateFilterOthers() Then
                'If mode is NEW then we need to do an insert.
                If ViewState("MODE") = "NEW" Then
                    Dim prefId As String
                    'Generate a guid to be used as the prefid.
                    prefId = Guid.NewGuid.ToString
                    'Save the basic user preference information.
                    objRptParams.AddUserPrefInfo(prefId, Session("UserName"), CInt(ViewState("resid")), txtPrefName.Text)

                    'Save each sort preference in the lbxSort listbox. Use the order in the
                    'listbox to generate the sequence.
                    For i = 0 To lbxSelSort.Items.Count - 1
                        objRptParams.AddUserSortPrefs(prefId, lbxSelSort.Items(i).Value, i + 1)
                    Next

                    'If the FilterListSelections dt is not empty then we need to save the
                    'FilterList preferences.
                    ds = DirectCast(Session("WorkingDS"), DataSet)
                    dt = ds.Tables("FilterListSelections")
                    For Each dr In dt.Rows
                        objRptParams.AddUserFilterListPrefs(prefId, dr("RptParamId"), dr("FldValue").ToString())
                    Next

                    'We need to examine each row in the tblFilterOthers table control. If the
                    'user entered data in the textbox then we should save the info for that row.
                    For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
                        'If there is an entry in the textbox then get the values from the row.
                        If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                            fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                            If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text <> "" Then
                                'Special case: The Between operator uses two values located on two different textboxes
                                fldValue &= ";" & DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                            End If
                            rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                            objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                            '
                        ElseIf DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text = "" And
                               (DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Empty" Or
                               DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Is Null") Then
                            'Empty or Is Null
                            fldValue = ""
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                            rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                            objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                        End If
                    Next

                    'We need to rebind the DataList. We cannot use the cache here since we need
                    'the latest info including the record just added. If the cache was used we
                    'would not see the new record in the datalist.
                    BindDataListFromDB()
                    'Change mode to Edit
                    'SetMode("EDIT")
                    objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                    ViewState("MODE") = "EDIT"
                    'Place the prefid guid into the txtPrefId textbox so it can be reused
                    'if we need to do an update later on.
                    txtPrefId.Value = prefId
                    '   set Style to Selected Item
                    CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Value, ViewState)

                Else
                    'Update the basic user preference information
                    objRptParams.UpdateUserPrefInfo(txtPrefId.Value, txtPrefName.Text)

                    'Delete any previous sort info and then add the ones in the lbxSelSort listbox.
                    objRptParams.DeleteUserSortPrefs(txtPrefId.Value)

                    'Save each sort preference in the lbxSort listbox. Use the order in the
                    'listbox to generate the sequence.
                    For i = 0 To lbxSelSort.Items.Count - 1
                        objRptParams.AddUserSortPrefs(txtPrefId.Value, lbxSelSort.Items(i).Value, i + 1)
                    Next

                    'Delete any previous filter list info and then add the ones in the FilterListSelections dt.
                    objRptParams.DeleteUserFilterListPrefs(txtPrefId.Value)

                    'Save each filter list preference in the FilterListSelections dt.
                    ds = DirectCast(Session("WorkingDS"), DataSet)
                    dt = ds.Tables("FilterListSelections")
                    For Each dr In dt.Rows
                        objRptParams.AddUserFilterListPrefs(txtPrefId.Value, dr("RptParamId"), dr("FldValue").ToString())
                    Next

                    'Delete any previous filter other info and then add the ones now specified.
                    objRptParams.DeleteUserFilterOtherPrefs(txtPrefId.Value)

                    For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty.
                        'If there is an entry in the textbox then get the values from the row.
                        If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                            fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                            If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text <> "" Then
                                'Special case: The Between operator uses two values located on two different textboxes
                                fldValue &= ";" & DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                            End If
                            rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                            objRptParams.AddUserFilterOtherPrefs(txtPrefId.Value, rptParamId, opId, fldValue)
                            '
                        ElseIf DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text = "" And
                                (DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Empty" Or
                                DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Is Null") Then
                            'Special case: Empty or Is Null operators
                            fldValue = ""
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                            rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                            objRptParams.AddUserFilterOtherPrefs(txtPrefId.Value, rptParamId, opId, fldValue)
                        End If
                    Next

                    'We need to rebind the DataList in case txtPrefName had changed.
                    BindDataListFromDB()
                    '   set Style to Selected Item
                    CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Value, ViewState)
                End If  'Test if mode is NEW or EDIT
            End If 'ValidateFilterOThers returns True
        End If 'txtPrefName.Text is empty
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender

        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add buttons and listboxes
        controlsToIgnore.Add(btnsave)
        controlsToIgnore.Add(btnExport)
        controlsToIgnore.Add(btnFriendlyPrint)
        controlsToIgnore.Add(btnSearch)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnRemove)
        controlsToIgnore.Add(lbxFilterList)
        controlsToIgnore.Add(lbxSelFilterList)

        'Dim tbl As Table = DirectCast(FindControl("ContentMain2_tblFilterOthers"), Table)
        Dim tbl As System.Web.UI.WebControls.Table = Nothing
        If Not tblFilterOthers Is Nothing Then
            tbl = tblFilterOthers
        End If

        'Add javascript code to warn the user about non saved changes 
        ''Modifeid By Saraswathi lakshmanan , since, codes deleted 
        If Not (tbl Is Nothing) Then
            For i As Integer = 1 To tbl.Rows.Count - 1
                Dim ddl As DropDownList = DirectCast(tbl.Rows(i).Cells(1).Controls(0), DropDownList)
                ''Added label control to find the text of the label, When the text is number of days ,
                '' then it should not show the textboxes when the operator value is 6, i.e between
                Dim lbl As Label = DirectCast(tbl.Rows(i).Cells(0).Controls(0), Label)
                If Not (ddl Is Nothing) Then

                    'add Operators ddl
                    controlsToIgnore.Add(ddl)

                    Dim rptParamId As Integer
                    rptParamId = Integer.Parse(ddl.ID.Substring(3))
                    Dim ctrl As Control = MyBase.FindControlRecursive("txt2" & rptParamId)
                    'Dim test As Control = MyBase.FindControlRecursive("txt2" & rptParamId)
                    ''Added for timeclock report
                    ''Avoid adding two textboxes when the ddl value is 6. i.e. Between range for dates used in other reports
                    ''in time clock report 6 is the number of days

                    If Not (ctrl Is Nothing) Then
                        ''Modified by saraswathi Lakshmanan
                        If ddl.SelectedValue = AdvantageCommonValues.BetweenOperatorValue Then
                            If ViewState("resid") = 586 Or ViewState("resid") = 588 Or (ViewState("resid") = 591 And lbl.Text <> "Start Date Range") Or (ViewState("resid") = 589 And lbl.Text <> "Month Range") Then
                                ctrl.Visible = False
                            Else
                                ctrl.Visible = True
                            End If
                        Else
                            ctrl.Visible = False
                        End If

                    End If
                End If
            Next
        End If
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub SavePreferences()
        Dim objRptParams As New RptParamsFacade
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim opId As Integer
        Dim fldValue As String
        Dim rptParamId As Integer
        Dim rowCounter As Integer
        '  Dim cellCounter As Integer
        Dim objCommon As New CommonUtilities
        Dim errMsg As String

        If txtPrefName.Text = "" Then
            'Alert user that a Preference name is required.
            'lblPrefName.Text = lblPrefName.Text & "<font color=""red"">*</font>"
            errMsg = "Preference Name is required."
            'Display Error Message.
            DisplayErrorMessage(errMsg)
        Else
            'Before adding or updating we record we need to validate the entries
            'in the filter others section.
            If ValidateFilterOthers() Then
                'If mode is NEW then we need to do an insert.
                If ViewState("MODE") = "NEW" Then
                    Dim prefId As String
                    'Generate a guid to be used as the prefid
                    prefId = Guid.NewGuid.ToString
                    'Save the basic user preference information
                    objRptParams.AddUserPrefInfo(prefId, Session("UserName"), CInt(ViewState("resid")), txtPrefName.Text)

                    'Save each sort preference in the lbxSort listbox. Use the order in the
                    'listbox to generate the sequence.
                    For i = 0 To lbxSelSort.Items.Count - 1
                        objRptParams.AddUserSortPrefs(prefId, lbxSelSort.Items(i).Value, i + 1)
                    Next

                    'If the FilterListSelections dt is not empty then we need to save the
                    'FilterList preferences
                    ds = DirectCast(Session("WorkingDS"), DataSet)
                    dt = ds.Tables("FilterListSelections")
                    For Each dr In dt.Rows
                        objRptParams.AddUserFilterListPrefs(prefId, dr("RptParamId"), dr("FldValue").ToString())
                    Next

                    'We need to examine each row in the tblFilterOthers table control. If the
                    'user entered data in the textbox then we should save the info for that row.
                    For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
                        'If there is an entry in the textbox then get the values from the row
                        If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                            ''If operatorId is 6- then it is between value. so two text boxes will be there, they are aded with a ; seperated
                            If opId = 6 Then
                                fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                Dim fldval1 As String
                                fldval1 = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                                fldValue = fldValue + ";" + fldval1
                                rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                                objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                            Else
                                fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                                objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                            End If

                        End If
                    Next

                    If CInt(ViewState("resid")) = 580 Then
                        Dim dtTermStartDate, dtTermEndDate, dtStartDate, dtEndDate As Date

                        If txtTermStartDate.Text = "" Then
                            dtTermStartDate = "1/1/1900"
                        Else
                            dtTermStartDate = CDate(txtTermStartDate.Text)
                        End If
                        If txtTermEndDate.Text = "" Then
                            dtTermEndDate = "1/1/1900"
                        Else
                            dtTermEndDate = CDate(txtTermEndDate.Text)
                        End If
                        If txtStartDate.Text = "" Then
                            dtStartDate = "1/1/1900"
                        Else
                            dtStartDate = CDate(txtStartDate.Text)
                        End If
                        If txtEndDate.Text = "" Then
                            dtEndDate = "1/1/1900"
                        Else
                            dtEndDate = CDate(txtEndDate.Text)
                        End If

                        objRptParams.AddUserPrefForDailyCompletedHours(prefId, ddlCampGrpId.SelectedValue, ddlTermId.SelectedValue,
                                                                       ddlPrgVerId.SelectedValue, txtFirstName.Text, txtLastName.Text,
                                                                       dtTermStartDate, dtTermEndDate, dtStartDate,
                                                                       dtEndDate)
                    End If

                    'We need to rebind the DataList. We cannot use the cache here since we need
                    'the latest info including the record just added. If the cache was used we
                    'would not see the new record in the datalist.
                    BindDataListFromDB()
                    'Change mode to Edit
                    'SetMode("EDIT")
                    objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                    ViewState("MODE") = "EDIT"
                    'Place the prefid guid into the txtPrefId textbox so it can be reused
                    'if we need to do an update later on.
                    txtPrefId.Value = prefId
                Else
                    'Update the basic user preference information
                    objRptParams.UpdateUserPrefInfo(txtPrefId.Value, txtPrefName.Text)

                    'Delete any previous sort info and then add the ones in the lbxSelSort listbox.
                    objRptParams.DeleteUserSortPrefs(txtPrefId.Value)

                    'Save each sort preference in the lbxSort listbox. Use the order in the
                    'listbox to generate the sequence.
                    For i = 0 To lbxSelSort.Items.Count - 1
                        objRptParams.AddUserSortPrefs(txtPrefId.Value, lbxSelSort.Items(i).Value, i + 1)
                    Next

                    'Delete any previous filter list info and then add the ones in the FilterListSelections dt.
                    objRptParams.DeleteUserFilterListPrefs(txtPrefId.Value)

                    'Save each filter list preference in the FilterListSelections dt.
                    ds = DirectCast(Session("WorkingDS"), DataSet)
                    dt = ds.Tables("FilterListSelections")
                    For Each dr In dt.Rows
                        objRptParams.AddUserFilterListPrefs(txtPrefId.Value, dr("RptParamId"), dr("FldValue").ToString())
                    Next

                    'Delete any previous filter other info and then add the ones now specified
                    objRptParams.DeleteUserFilterOtherPrefs(txtPrefId.Value)
                    For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
                        'If there is an entry in the textbox then get the values from the row
                        If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                            ''If operatorId is 6- then it is between value. so two text boxes will be there, they are aded with a ; seperated
                            ''modified on july 10 2009
                            If opId = 6 Then
                                fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                Dim fldval1 As String
                                fldval1 = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                                fldValue = fldValue + ";" + fldval1
                                rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                                objRptParams.AddUserFilterOtherPrefs(txtPrefId.Value, rptParamId, opId, fldValue)
                            Else
                                fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                                objRptParams.AddUserFilterOtherPrefs(txtPrefId.Value, rptParamId, opId, fldValue)
                            End If

                        End If
                    Next
                    If CInt(ViewState("resid")) = 580 Then
                        Dim dtTermStartDate, dtTermEndDate, dtStartDate, dtEndDate As Date

                        If txtTermStartDate.Text = "" Then
                            dtTermStartDate = "1/1/1900"
                        Else
                            dtTermStartDate = CDate(txtTermStartDate.Text)
                        End If
                        If txtTermEndDate.Text = "" Then
                            dtTermEndDate = "1/1/1900"
                        Else
                            dtTermEndDate = CDate(txtTermEndDate.Text)
                        End If
                        If txtStartDate.Text = "" Then
                            dtStartDate = "1/1/1900"
                        Else
                            dtStartDate = CDate(txtStartDate.Text)
                        End If
                        If txtEndDate.Text = "" Then
                            dtEndDate = "1/1/1900"
                        Else
                            dtEndDate = CDate(txtEndDate.Text)
                        End If
                        objRptParams.UpdateUserPrefForDailyCompletedHours(txtPrefId.Value, ddlCampGrpId.SelectedValue, ddlTermId.SelectedValue,
                                                                       ddlPrgVerId.SelectedValue, txtFirstName.Text, txtLastName.Text,
                                                                      dtTermStartDate, dtTermEndDate, dtStartDate,
                                                                       dtEndDate)
                    End If
                End If  'Test if mode is NEW or EDIT
            End If 'ValidateFilterOThers returns True
            'CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Value, ViewState, Header1)
        End If 'txtPrefName.Text is empty
    End Sub
    'Private Function RemoveDuplicateRows(ByVal dTable As DataTable, ByVal colName As String) As DataTable
    '    Dim hTable As New Hashtable()
    '    Dim duplicateList As New ArrayList()
    '    'Add list of all the unique item value to hashtable, which stores combination of key, value pair. 
    '    'And add duplicate item value in arraylist. 
    '    For Each drow As DataRow In dTable.Rows
    '        If hTable.Contains(drow(colName)) Then
    '            duplicateList.Add(drow)
    '        Else
    '            hTable.Add(drow(colName), String.Empty)
    '        End If
    '    Next
    '    'Removing a list of duplicate items from datatable. 
    '    For Each dRow As DataRow In duplicateList
    '        dTable.Rows.Remove(dRow)
    '    Next
    '    'Datatable which contains unique records will be return as output. 
    '    Return dTable
    'End Function
    Protected Sub ddlTermId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlTermId.SelectedIndexChanged
        Session("TermId") = ddlTermId.SelectedValue
        Session("TermDescrip") = ddlTermId.SelectedItem.Text
    End Sub

    Private Function createNoOfDaysTable() As DataTable
        Dim dt As New DataTable
        Dim dr As DataRow
        dt.Columns.Add("CompOpText", Type.GetType("System.String"))
        dt.Columns.Add("CompOpId", Type.GetType("System.Int32"))
        Dim i As Integer
        For i = 1 To 7
            dr = dt.NewRow
            dr("CompOpText") = i.ToString
            dr("CompOpId") = i
            dt.Rows.Add(dr)
        Next i
        Return dt

    End Function

    ''Added by Saraswathi Lakshmanan
    ''For Attendance history timeClock reports
    ''To show the Start Day to print on the report
    ''Added by Saraswathi Since, Code deleted
    Private Function createStartDayTable() As DataTable
        Dim dt As New DataTable
        Dim dr As DataRow
        dt.Columns.Add("CompOpText", Type.GetType("System.String"))
        dt.Columns.Add("CompOpId", Type.GetType("System.Int32"))
        Dim i As Integer
        For i = 1 To 7
            dr = dt.NewRow
            If i = 1 Then
                dr("CompOpText") = "Monday"
            ElseIf i = 2 Then
                dr("CompOpText") = "Tuesday"
            ElseIf i = 3 Then
                dr("CompOpText") = "Wednesday"
            ElseIf i = 4 Then
                dr("CompOpText") = "Thursday"
            ElseIf i = 5 Then
                dr("CompOpText") = "Friday"
            ElseIf i = 6 Then
                dr("CompOpText") = "Saturday"
            ElseIf i = 7 Then
                dr("CompOpText") = "Sunday"
            End If
            dr("CompOpId") = i
            dt.Rows.Add(dr)
        Next i
        Return dt

    End Function


    Protected Sub rdolstChkUnits_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles rdolstChkUnits.SelectedIndexChanged
        If rdolstChkUnits.SelectedValue = "False" Then
            pnlRow.Enabled = True
        Else
            pnlRow.Enabled = False
        End If
    End Sub

    Protected Sub btnPerformDataDump_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPerformDataDump.Click
        Dim rptParamInfo As New ReportParamInfo

        If Page.IsValid Then

            Dim strErrMsg As String = BuildReportDataSet(rptParamInfo)

            If strErrMsg = "" Then
                Dim ds As DataSet
                ds = DirectCast(Session("ReportDS"), DataSet)
                If Not ds Is Nothing Then
                    If (ds.Tables(0).Rows.Count > 0) Then
                        ' Code Commited By Vijay Ramteke on 04-April-2009

                        strErrMsg = "Data dumped successfully."
                        'btnExportToExcel.Enabled = True

                        ' Code Commited By Vijay Ramteke on 04-April-2009
                        ' New Code By Vijay Ramteke on 04-April-2009
                        ' Export to Excel
                        'first let's clean up the response.object

                        Response.Clear()
                        Response.Charset = ""
                        Response.AddHeader("content-disposition", "attachment;filename=Projected cash flow- By week.xls")
                        'set the response mime type for excel
                        Response.ContentType = "application/vnd.ms-excel"
                        Dim strStyle As String = "<style>.text { mso-number-format:000\-00\-0000; } </style>"
                        'create a string writer
                        Dim stringWrite As New StringWriter
                        'create an htmltextwriter which uses the stringwriter
                        Dim htmlWrite As New HtmlTextWriter(stringWrite)
                        stringWrite.WriteLine(strStyle)
                        'instantiate a datagrid
                        Dim dg As New DataGrid
                        'set the datagrid datasource to the dataset passed in
                        dg.DataSource = ds.Tables(0)
                        'bind the datagrid
                        dg.DataBind()
                        For intTemp As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            dg.Items(intTemp).Cells(2).Attributes.Add("class", "text")
                        Next
                        'tell the datagrid to render itself to our htmltextwriter
                        dg.RenderControl(htmlWrite)
                        'all that's left is to output the html
                        Response.Write(stringWrite.ToString)
                        Response.End()
                        ' New Code By Vijay Ramteke on 04-April-2009
                    Else
                        'code commented by Priyanka on date 19th of May 2009
                        'strErrMsg = "There is no data matching selection. Data dump failed"
                        '-code added by Priyanka on date 19th of May 2009
                        strErrMsg = "There is no data matching selection."
                        '-
                    End If
                Else
                    'code commented by Priyanka on date 19th of May 2009
                    'strErrMsg = "There is no data matching selection. Data dump failed"
                    '-code added by Priyanka on date 19th of May 2009
                    strErrMsg = "There is no data matching selection."
                    '-
                End If

                'ds.Dispose()
                DisplayErrorMessage(strErrMsg)
            Else
                'Alert user of error.
                DisplayErrorMessage(strErrMsg)
            End If
        Else

            If (ViewState("boolShowErr") IsNot Nothing) Then
                'Dim strErrMsg As String = getErrString()
                Dim strErrMsg As String = GetValidateErrMsg()
                DisplayErrorMessage(strErrMsg)
            End If
            ViewState("boolShowErr") = True
        End If
    End Sub

    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToExcel.Click
        ''Since no error was found, proceed to display report in Report Viewer page.
        'If Not (Session("ReportDS") Is Nothing) Then
        '    Dim ds As New DataSet
        '    ds = DirectCast(Session("ReportDS"), DataSet)

        '    ' Export to Excel
        '    'first let's clean up the response.object

        '    Response.Clear()
        '    Response.Charset = ""
        '    'set the response mime type for excel
        '    Response.ContentType = "application/vnd.ms-excel"
        '    'create a string writer
        '    Dim stringWrite As New System.IO.StringWriter
        '    'create an htmltextwriter which uses the stringwriter
        '    Dim htmlWrite As New System.Web.UI.HtmlTextWriter(stringWrite)
        '    'instantiate a datagrid
        '    Dim dg As New DataGrid
        '    'set the datagrid datasource to the dataset passed in
        '    dg.DataSource = ds.Tables(0)
        '    'bind the datagrid
        '    dg.DataBind()
        '    'tell the datagrid to render itself to our htmltextwriter
        '    dg.RenderControl(htmlWrite)
        '    'all that's left is to output the html
        '    Response.Write(stringWrite.ToString)
        '    Response.End()

        'End If
        ''Alert user if .
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        End If
    '    Next

    'End Sub
    Private Sub DailyCompletedHoursClear()
        ddlCampGrpId.SelectedValue = ""
        ddlTermId.SelectedValue = ""
        ddlPrgVerId.SelectedValue = ""
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtTermStartDate.Text = ""
        txtTermEndDate.Text = ""
        txtStartDate.Text = ""
        txtEndDate.Text = ""
        txtPrefName.Text = ""
    End Sub
    'Private Function getPrgVerId(ByVal strWhere As String) As String
    '    Dim prgVerId As String
    '    If strWhere.ToLower.Contains("arstuenrollments.prgverid in ") Then
    '        If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in ")).ToLower.IndexOf(" and ") >= 0 Then
    '            prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in ("), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in (")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.prgverid in", "").Replace(")", "").Replace("(", "")
    '        Else
    '            prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid in (")).Replace("arstuenrollments.prgverid in", "").Replace(")", "").Replace("(", "")
    '        End If
    '    Else
    '        If strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).ToLower.IndexOf(" and ") >= 0 Then
    '            prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = "), strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).ToLower.IndexOf(" and ")).Replace("arstuenrollments.prgverid =", "").Replace(")", "").Replace("(", "")
    '        Else
    '            prgVerId = strWhere.Substring(strWhere.ToLower.IndexOf("arstuenrollments.prgverid = ")).Replace("arstuenrollments.prgverid =", "").Replace(")", "").Replace("(", "")
    '        End If
    '    End If
    '    prgVerId = prgVerId.Replace("arStuEnrollments.PrgVerId IN", "")
    '    prgVerId = prgVerId.Replace(" ", "").Replace("'", "")
    '    Return prgVerId
    'End Function
    Private Sub lbtSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lbtSearch.Click
        Dim filterListWhereClause As String = BuildFilterListClause("", "")
        If filterListWhereClause = "" Then

            '   setup the properties of the new window
            Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=500px"
            Dim name As String = "StudentPopupSearch"
            Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/sy/" + name + ".aspx?resid=294&mod=SA&cmpid=" + CampusId
            'Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/sy/" + name + ".aspx"

            '   build list of parameters to be passed to the new window
            Dim parametersArray(8) As String

            '   set StudentName
            parametersArray(0) = txtStudentId.ClientID

            '   set StuEnrollment
            parametersArray(1) = txtStuEnrollmentId.ClientID
            parametersArray(2) = txtStuEnrollment.ClientID

            ''   set Terms
            parametersArray(3) = txtTermId.ClientID
            parametersArray(4) = txtTerm.ClientID

            '   set AcademicYears
            parametersArray(5) = txtAcademicYearId.ClientID
            parametersArray(6) = txtAcademicYear.ClientID
            parametersArray(7) = txtStudentIdentifier.ClientID
            parametersArray(8) = "ParentId=629"
            '   open new window and pass parameters
            CommonWebUtilities.OpenWindowAndPassParameters(Page, url, name, winSettings, parametersArray)
        Else
            DisplayErrorMessage("You need to set the filter to None in order to select a Student Enrollment.")

        End If
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnClear.Click
        txtStudentId.Text = ""
        txtStuEnrollment.Text = ""
        txtStuEnrollmentId.Value = ""

    End Sub
    Protected Sub ddlConsAbsCompOp_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlConsAbsCompOp.SelectedIndexChanged
        If ddlConsAbsCompOp.SelectedValue = "between" Then
            txtNumberConsecutiveAbsentDays.Text = ""
            txtNumberConsecutiveAbsentDays2.Text = ""
            txtNumberConsecutiveAbsentDays2.Visible = True
        Else
            txtNumberConsecutiveAbsentDays.Text = ""
            txtNumberConsecutiveAbsentDays2.Text = ""
            txtNumberConsecutiveAbsentDays2.Visible = False
        End If
    End Sub
    Private Function getCampusId(filterList As String) As String
        Dim campId As String
        Dim campIdx1, campIdx2, campIdx3 As Integer
        campIdx1 = filterList.ToLower.IndexOf("campusid", StringComparison.Ordinal)
        campIdx2 = filterList.Substring(campIdx1).IndexOf("'", StringComparison.Ordinal)
        campIdx3 = filterList.Substring(campIdx1 + campIdx2 + 1).IndexOf("'", StringComparison.Ordinal)
        campId = filterList.Substring(campIdx1 + campIdx2 + 1, campIdx3)
        Return campId

    End Function

    Protected Sub ddlExportTo_TextChanged(sender As Object, e As EventArgs) Handles ddlExportTo.TextChanged
        Select Case ddlExportTo.SelectedValue
            Case "doc"
                lblExportNote.Text = "Exporting large amounts of data to this format is not recommended."
            Case Else
                lblExportNote.Text = ""
        End Select
    End Sub

    ''' <summary>
    ''' Get directly the setting.
    ''' </summary>
    ''' <param name="settingName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetSettingString(settingName As String) As String
        Dim advAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            advAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            advAppSettings = New AdvAppSettings
        End If
        If Not IsNothing(advAppSettings.AppSettings(settingName)) Then
            Dim result As String = advAppSettings.AppSettings(settingName).ToString()
            Return result
        End If
        Return Nothing
    End Function

End Class
