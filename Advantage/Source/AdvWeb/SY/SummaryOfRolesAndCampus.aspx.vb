Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade

Partial Class SY_SummaryOfRolesAndCampus
    Inherits System.Web.UI.Page
    Protected facade As New UserSecurityFacade
    Protected UserId As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            UserId = Request("UID")
            lblUserName.Text = Request("UName")
            BindGridView(UserId)
        End If
    End Sub
    Private Sub BindGridView(ByVal UserId As String)
        Dim dt As DataTable = facade.GetCampusGroupsByUser(UserId)
        For Each dr As DataRow In dt.Rows
            dr("CampGrpDescrip") = dr("CampGrpDescrip") & " (" & dr("Status") & ")"
        Next
        GridView1.DataSource = dt
        GridView1.DataBind()
    End Sub
    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim blCampus As BulletedList = CType(e.Row.FindControl("BlCampus"), BulletedList)
            Dim blRole As BulletedList = CType(e.Row.FindControl("BlRole"), BulletedList)

            Dim ds, ds1 As New DataSet
            ds = facade.GetCampusAndRolesByCampusGroups(UserId, GridView1.DataKeys(e.Row.RowIndex).Value.ToString)

            blCampus.DataSource = ds
            blCampus.DataBind()


            ds1 = facade.GetRolesByCampusGroups(UserId, GridView1.DataKeys(e.Row.RowIndex).Value.ToString)
            blRole.DataSource = ds1
            blRole.DataBind()
        End If
    End Sub
End Class
