<%@ Reference Control="~/UserControls/CrystalRptViewer.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="AdvWeb.SY.ReportViewer" CodeFile="ReportViewer.aspx.vb" %>
<%@ Register TagPrefix="fame" TagName="CrystalRptViewer" Src="~/UserControls/CrystalRptViewer.ascx" %>

<html>
	<head>
		<title>
			<%# PageTitle %>
		</title>
		 <link rel="stylesheet" type="text/css" href="../css/localhost_lowercase.css" />
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body runat="server">
		<form id="Form1" method="post" runat="server">
			<table  cellpadding="0" class="maincontenttable">
				<tr>
					<td width="100%" align="left" class="reportdetailsframe">
						<div class="rptscroll">
							<fame:CrystalRptViewer id="crv" runat="server"></fame:CrystalRptViewer>
						</div>
					</td>
				</tr>							
			</table>
		</form>
	</body>
</html>
