<%@ page title="" language="vb" MasterPageFile="~/NewSite.master" autoeventwireup="false" codefile="StatusCodes.aspx.vb" inherits="StatusCodes" %>
<%@ mastertype virtualpath="~/NewSite.master"%> 
<asp:content id="content1" contentplaceholderid="additional_head" runat="server">
<script language="javascript" src="../js/checkall.js" type="text/javascript"/>
<script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstStatusCode">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstStatusCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstStatusCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstStatusCode" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table cellspacing="0" cellpadding="2" width="100%" align="center" border="0">
                                <tr>
                                    <td class="contentcell" style="background: transparent;">
                                        <asp:Label ID="lblStatusLevel" runat="server" CssClass="label">Status for</asp:Label></td>
                                    <td class="contentcell4" style="background: transparent;" align="left">
                                        <asp:DropDownList ID="ddlStatusLevel" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td class="contentcell" style="background: transparent;">
                                        <asp:Label ID="lblStatus" runat="server" CssClass="label">Status</asp:Label></td>
                                    <td class="contentcell4" style="background: transparent;" align="left">
                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td class="contentcell" style="background: transparent;">
                                        <asp:Label ID="lblCampusGroup" runat="server" CssClass="label">Campus Group</asp:Label></td>
                                    <td class="contentcell4" style="background: transparent;" align="left">
                                        <asp:DropDownList ID="ddlCampusGroup" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td class="contentcell" style="background: transparent;"></td>
                                    <td class="contentcell4" style="background: transparent;" align="left">
                                        <asp:Button ID="btnBuildList" runat="server" CausesValidation="False"
                                            Text="Build List"></asp:Button></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstStatusCode" runat="server" CssClass="label" Width="100%" DataKeyField="StatusCodeId">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server" CausesValidation="False" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'></asp:ImageButton>
                                        <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" CausesValidation="False" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'></asp:ImageButton>
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                        <asp:LinkButton Text='<%# Container.DataItem("StatusCodeDescrip")%>' runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("StatusCodeId")%>' ID="Linkbutton1" CausesValidation="False" />
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="98%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width:98%;" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->

                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table width="60%" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusCode" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtStatusCode" runat="server" CssClass="textbox" MaxLength="128" Width="180px"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusCodeDescrip" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtStatusCodeDescrip" runat="server" CssClass="textbox" MaxLength="128"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusLevelId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusLevelId" runat="server" CssClass="dropdownlist" AutoPostBack="True"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblSysStatusId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlSysStatusId" runat="server" CssClass="dropdownlist" AutoPostBack="true"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" AutoPostBack="true"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="twocolumncontentcelluser">
                                                <asp:CheckBox ID="chkIsDefaultLeadStatus" runat="server" CssClass="label" Visible="false" Text="Default Lead Status"></asp:CheckBox>
                                                <asp:CheckBox ID="checkboxIsDefaultStatus" runat="server" CssClass="label" Visible="False"></asp:CheckBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="twocolumncontentcelldate">
                                                <!-- <asp:radiobuttonlist id=cblProbationTypeId tabIndex=12 runat="server" cssClass="textbox" AutoPostBack="True" RepeatColumns="1" Visible="False">
                                                            <asp:ListItem Value="0" Selected="True">Academic Probation</asp:ListItem>
                                                            <asp:ListItem Value="1">Disciplinary Probation</asp:ListItem>
                                                        </asp:radiobuttonlist> -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="2">
                                                <asp:TextBox ID="txtStatusCodeId" runat="server" CssClass="label" MaxLength="128" Visible="false"></asp:TextBox></td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnlRegent" runat="server" Visible="false">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center" style="border: 1px solid #A3C7E2;">
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="6" style="border-top: 0px; border-right: 0px; border-left: 0px">
                                                    <asp:Label ID="Label2" runat="server" Font-Bold="true" CssClass="label">Map To Regent</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellcitizenship" colspan="6" style="padding-top: 12px">
                                                    <asp:Panel ID="pnlRegentAwardChild" TabIndex="12" runat="server" Width="80%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="35%" nowrap class="contentcell">
                                                                    <asp:Label ID="lblregentterm" runat="server" CssClass="label">Academic Status<span style="color: red">*</span></asp:Label></td>
                                                                <td width="65%" nowrap class="contentcell4">
                                                                    <asp:DropDownList ID="ddlAcadStatus" runat="server" CssClass="textbox" TabIndex="6"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                                <!--end table content-->
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:TextBox ID="txtModUser" runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtModDate" runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtDefaultStatusCodeId" runat="server" Visible="False"></asp:TextBox>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server" Width="50px"></asp:Panel>
        &nbsp;&nbsp;
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    </div>
</asp:Content>


