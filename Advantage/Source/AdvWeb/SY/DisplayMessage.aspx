<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DisplayMessage.aspx.vb" Inherits="DisplayMessage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Message</title>
    <script language="javascript">
     function yesclick(){
       opener.document.form1.CampusSpecificDelete.value = "yes";
       opener.document.form1.submit();
         window.close();
     }       
    //function noclick(){
    //    opener.document.form1.CampusSpecificDelete.value = "no";
    //    window.close();  
    //}       
    function cancelclick(){
       // opener.document.form1.CampusSpecificDelete.value = "cancel";
        //opener.document.form1.submit();
        window.close();
    }       
    </script>
    <style>
        .LabelBold
            {
	            font: normal 11px verdana;
	            font-weight: bold;
	            color: #000066;
	            background-color: transparent;
	            width: 100%;
	            padding: 2px;
	            vertical-align: bottom;
            }
       .buttons
            {
	            margin: 4px;
	            vertical-align: middle;
	            font: normal 10px verdana;
	            text-align:center;
	            color: #000066;
            }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <p></p><p></p><p></p><p></p>
        <table width="80%">
            <tr>
                <td nowrap>
                    <asp:Label ID="lblBold" runat="Server" CssClass="LabelBold" Width="50px"></asp:Label>            
                </td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="Label1" runat="Server" Visible="False"><%=Request.QueryString("Message1")%></asp:Label>            
                </td>
            </tr>
        </table>
        <p></p>
        <table width="50%">
            <tr>
                <td><asp:Button ID="btnyes" Text="Continue" runat="server"  width="75px" Visible="False" /></td>
                <%--<td><asp:Button ID="btnno" Text="No" runat="server"  width="75px" /></td>--%>
                <td><asp:Button ID="btncancel" Text="Close" runat="server"  width="75px" Visible="false" /></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
