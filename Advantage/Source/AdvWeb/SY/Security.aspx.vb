﻿'Partial Class Security
'    Inherits System.Web.UI.Page

'#Region " Web Form Designer Generated Code "

'    'This call is required by the Web Form Designer.
'    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

'    End Sub
'    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
'    Protected WithEvents rfvModule As System.Web.UI.WebControls.RequiredFieldValidator
'    Protected WithEvents rfvType As System.Web.UI.WebControls.RequiredFieldValidator
'    Protected WithEvents rfvGroup As System.Web.UI.WebControls.RequiredFieldValidator
'    Protected WithEvents lblInstructions As System.Web.UI.WebControls.Label


'    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
'        'CODEGEN: This method call is required by the Web Form Designer
'        'Do not modify it using the code editor.
'        InitializeComponent()
'    End Sub

'#End Region

'    Protected moduleName As String
'    Protected intRows As Integer
'    Protected strError As String
'    Protected WithEvents chk1 As CheckBox
'    Protected WithEvents chk2 As CheckBox
'    Protected WithEvents chk3 As CheckBox
'    Protected WithEvents chk4 As CheckBox
'    Protected WithEvents chk5 As CheckBox
'    Protected WithEvents chk6 As CheckBox

'    Private pObj As New UserPagePermissionInfo

'    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        btnBuildList.Attributes.Add("onclick", "SetHiddenText();return true;")

'        '   disable History button at all time
'        Header1.EnableHistoryButton(False)

'        pObj = Header1.UserPagePermission

'        If Not Page.IsPostBack Then
'            Dim facResRels As New ResourcesRelationsFacade
'            Dim facUserSecurit As New UserSecurityFacade
'            Dim dsResRels As New DataSet

'            divContent.Visible = False

'            With ddlModules
'                .DataSource = facResRels.GetModules
'                .DataTextField = "Resource"
'                .DataValueField = "ResourceId"
'                .DataBind()
'                .Items.Insert(0, New ListItem("Select One", ""))
'            End With

'            With ddlTypes
'                .DataSource = facResRels.GetResourceTypes
'                .DataTextField = "ResourceType"
'                .DataValueField = "ResourceTypeId"
'                .DataBind()
'                .Items.Insert(0, New ListItem("Select One", ""))
'            End With

'            With ddlGroups
'                .DataSource = facUserSecurit.GetAllRoles
'                .DataTextField = "Role"
'                .DataValueField = "RoleId"
'                .DataBind()
'                .Items.Insert(0, New ListItem("Select One", ""))
'            End With

'            'Set the Session("ResourcesRelations") to nothing. This is very important to fix the following issue.
'            'If the user leaves the page and then returns to it without closing the browser the
'            'Session("ResourcesRelations") variable will still be populated and so the first test in the else
'            'clause below will pass and there will be an attempt to build the attendance table when
'            'a term is selected. This will cause an error.
'            Session("ResourcesRelations") = Nothing

'        Else
'            moduleName = ddlModules.SelectedItem.Text

'            If Not Session("ResourcesRelations") Is Nothing Then
'                If HiddenText.Text <> "YES" Then
'                    divContent.Visible = True
'                    DisplayHierarchy()
'                Else
'                    HiddenText.Text = ""
'                End If

'            Else
'                If HiddenText.Text = "YES" Then
'                    HiddenText.Text = ""
'                End If

'            End If

'        End If


'    End Sub
'    Private Sub btnBuildList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
'        Dim facResRels As New ResourcesRelationsFacade
'        Dim dsResRels As New DataSet

'        ValidateLHS()
'        If strError <> "" Then
'            DisplayErrorMessage(strError)
'            divContent.Visible = False
'            btnSave.Enabled = False
'        Else
'            'Enable the Save button if the user has permission
'            If pObj.HasFull Then
'                btnSave.Enabled = True
'            Else
'                btnSave.Enabled = False
'            End If


'            dsResRels = facResRels.GetResourceRelations
'            Session("ResourcesRelations") = dsResRels
'            divContent.Visible = True
'            DisplayHierarchy()
'            ClearAll()
'            CheckAuthorizedResoucesLevels()
'        End If

'    End Sub

'    Public Sub chkChanged(ByVal obj As Object, ByVal e As EventArgs)
'        Dim ctl As Control
'        Dim ctl0 As Control
'        Dim ctl5 As Control
'        Dim ctl4 As Control
'        Dim ctl3 As Control
'        Dim ctl2 As Control
'        Dim ctl1 As Control
'        Dim intColNum, intCount As Integer
'        Dim chk As CheckBox

'        ctl = tblModuleResources.FindControl("lblError")
'        CType(ctl, Label).Text = ""

'        intRows = tblModuleResources.Rows.Count


'        Select Case obj.id.ToString().Remove(0, 1)
'            Case "5"
'                intColNum = 1
'            Case "4"
'                intColNum = 2
'            Case "3"
'                intColNum = 3
'            Case "2"
'                intColNum = 4
'            Case "1"
'                intColNum = 5
'        End Select

'        ctl5 = tblModuleResources.FindControl("L5")
'        ctl4 = tblModuleResources.FindControl("L4")
'        ctl3 = tblModuleResources.FindControl("L3")
'        ctl2 = tblModuleResources.FindControl("L2")
'        ctl1 = tblModuleResources.FindControl("L1")

'        For intCount = 2 To intRows - 1
'            If tblModuleResources.Rows(intCount).Cells(intColNum).Controls.Count = 1 Then
'                chk = CType(tblModuleResources.Rows(intCount).Cells(intColNum).Controls(0), CheckBox)
'                chk.Checked = obj.Checked
'            End If
'        Next

'    End Sub

'    Private Sub SubModuleChkChanged(ByVal obj As Object, ByVal e As EventArgs)
'        Dim resourceId As String
'        Dim ds As New DataSet
'        Dim subModRows() As DataRow
'        Dim drRow2 As DataRow
'        Dim level As String
'        Dim strControl As String
'        Dim ctl As Control

'        'Get the resource id and level associated with the checkbox checked
'        resourceId = obj.id.ToString().Remove(0, 7)
'        level = obj.id.ToString().Substring(4, 1)

'        'The resource is a submodule so we can get its children
'        ds = Session("ResourcesRelations")
'        subModRows = ds.Tables("ResourceRelations").Select("ParentId = " & CInt(resourceId))

'        'For each row we want to locate the relevant checkbox.
'        For Each drRow2 In subModRows
'            'Build string that represents the checkbox we are interested in
'            strControl = "chkR" & level & "ID" & drRow2("ResourceId").ToString()
'            ctl = tblModuleResources.FindControl(strControl)

'            If DirectCast(obj, CheckBox).Checked = True Then
'                DirectCast(ctl, CheckBox).Checked = True
'            Else
'                DirectCast(ctl, CheckBox).Checked = False
'            End If
'        Next


'    End Sub

'    Public Sub DisplayHierarchy()
'        'We want to first bring the direct children of the resource (module) that is selected.
'        'We will only bring children of the resource type selected. If a child is a submodule then we will
'        'also bring its children. If we are dealing with reports then we will disable the checkboxes except
'        'for the display one. This is because the other buttons are not relevant when it comes to reports.
'        Dim ds As New DataSet
'        Dim resourceID As Integer
'        Dim resourceType As Integer
'        Dim childRows() As DataRow
'        Dim subModRows() As DataRow
'        Dim drRow As DataRow
'        Dim drRow2 As DataRow

'        ValidateLHS()

'        If strError <> "" Then
'            DisplayErrorMessage(strError)
'            divContent.Visible = False
'            btnSave.Enabled = False
'        Else
'            resourceID = ddlModules.SelectedItem.Value
'            resourceType = ddlTypes.SelectedItem.Value

'            'Commented out by Troy on 4/1/2005. User still needs permission to preferences
'            'If we are dealing with reports we need to disable the module level checkboxes
'            'If resourceType = 5 Then
'            '    SetModuleLevelCheckBoxesForReports()
'            'Else
'            '    SetModuleLevelCheckBoxesForNotReports()
'            'End If

'            ds = Session("ResourcesRelations")
'            childRows = ds.Tables("ResourceRelations").Select("ParentId = " & resourceID & " AND ResourceTypeId = " & resourceType & " OR ParentId = " & resourceID & " AND ResourceTypeId = 2 AND ChildTypeId = " & resourceType, "Sequence")
'            For Each drRow In childRows
'                Dim r As New TableRow
'                Dim c1 As New TableCell
'                Dim c2 As New TableCell
'                Dim c3 As New TableCell
'                Dim c4 As New TableCell
'                Dim c5 As New TableCell
'                Dim c6 As New TableCell
'                Dim c7 As New TableCell

'                c2.HorizontalAlign = 2
'                c3.HorizontalAlign = 2
'                c4.HorizontalAlign = 2
'                c5.HorizontalAlign = 2
'                c6.HorizontalAlign = 2
'                c7.HorizontalAlign = 2

'                Dim lbl As New Label
'                chk1 = New CheckBox
'                chk2 = New CheckBox
'                chk3 = New CheckBox
'                chk4 = New CheckBox
'                chk5 = New CheckBox
'                chk6 = New CheckBox

'                lbl.Text = "&nbsp;&nbsp;&nbsp;&nbsp;" & drRow("Resource").ToString()
'                c1.Controls.Add(lbl)
'                r.Cells.Add(c1)

'                chk1.ID = "chkR" & "5" & "ID" & drRow("ResourceID").ToString()
'                chk1.CssClass = "Label"
'                'If resourceType = 5 Then
'                '    chk1.Enabled = False
'                'End If
'                c2.Controls.Add(chk1)
'                r.Cells.Add(c2)

'                chk2.ID = "chkR" & "4" & "ID" & drRow("ResourceID").ToString()
'                chk2.AutoPostBack = False
'                chk2.CssClass = "Label"
'                'If resourceType = 5 Then
'                '    chk2.Enabled = False
'                'End If
'                c3.Controls.Add(chk2)
'                r.Cells.Add(c3)

'                chk3.ID = "chkR" & "3" & "ID" & drRow("ResourceID").ToString()
'                chk3.AutoPostBack = False
'                chk3.CssClass = "Label"
'                'If resourceType = 5 Then
'                '    chk3.Enabled = False
'                'End If
'                c4.Controls.Add(chk3)
'                r.Cells.Add(c4)

'                chk4.ID = "chkR" & "2" & "ID" & drRow("ResourceID").ToString()
'                chk4.AutoPostBack = False
'                chk4.CssClass = "Label"
'                'If resourceType = 5 Then
'                '    chk4.Enabled = False
'                'End If
'                c5.Controls.Add(chk4)
'                r.Cells.Add(c5)

'                chk5.ID = "chkR" & "1" & "ID" & drRow("ResourceID").ToString()
'                chk5.AutoPostBack = False
'                chk5.CssClass = "Label"
'                c6.Controls.Add(chk5)
'                r.Cells.Add(c6)

'                tblModuleResources.Rows.Add(r)
'                'If we are dealing with a submodule then we need to add rows for its children. We also want
'                'the checkboxes to cause postbacks so we can check/uncheck the children.
'                If drRow("ResourceTypeId") = 2 Then
'                    'Set the checkboxes for the submodule to cause post back and dynamically add event handler
'                    HandleSubModuleCheckBoxes()

'                    'Get the children of the submodule
'                    subModRows = ds.Tables("ResourceRelations").Select("ParentId = " & drRow("ResourceID"))
'                    'Now loop through each row in the dataview and add a row to the table
'                    For Each drRow2 In subModRows
'                        Dim r0 As New TableRow
'                        Dim c10 As New TableCell
'                        Dim c20 As New TableCell
'                        Dim c30 As New TableCell
'                        Dim c40 As New TableCell
'                        Dim c50 As New TableCell
'                        Dim c60 As New TableCell
'                        Dim c70 As New TableCell

'                        c20.HorizontalAlign = 2
'                        c30.HorizontalAlign = 2
'                        c40.HorizontalAlign = 2
'                        c50.HorizontalAlign = 2
'                        c60.HorizontalAlign = 2
'                        c70.HorizontalAlign = 2

'                        Dim lbl0 As New Label
'                        Dim chk10 As New CheckBox
'                        Dim chk20 As New CheckBox
'                        Dim chk30 As New CheckBox
'                        Dim chk40 As New CheckBox
'                        Dim chk50 As New CheckBox
'                        Dim chk60 As New CheckBox

'                        lbl0.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" & drRow2("Resource").ToString()
'                        c10.Controls.Add(lbl0)
'                        r0.Cells.Add(c10)

'                        chk10.ID = "chkR" & "5" & "ID" & drRow2("ResourceID").ToString()
'                        chk10.AutoPostBack = False
'                        chk10.CssClass = "Label"
'                        c20.Controls.Add(chk10)
'                        r0.Cells.Add(c20)

'                        chk20.ID = "chkR" & "4" & "ID" & drRow2("ResourceID").ToString()
'                        chk20.AutoPostBack = False
'                        chk20.CssClass = "Label"
'                        c30.Controls.Add(chk20)
'                        r0.Cells.Add(c30)

'                        chk30.ID = "chkR" & "3" & "ID" & drRow2("ResourceID").ToString()
'                        chk30.AutoPostBack = False
'                        chk30.CssClass = "Label"
'                        c40.Controls.Add(chk30)
'                        r0.Cells.Add(c40)

'                        chk40.ID = "chkR" & "2" & "ID" & drRow2("ResourceID").ToString()
'                        chk40.AutoPostBack = False
'                        chk40.CssClass = "Label"
'                        c50.Controls.Add(chk40)
'                        r0.Cells.Add(c50)

'                        chk50.ID = "chkR" & "1" & "ID" & drRow2("ResourceID").ToString()
'                        chk50.AutoPostBack = False
'                        chk50.CssClass = "Label"
'                        c60.Controls.Add(chk50)
'                        r0.Cells.Add(c60)

'                        tblModuleResources.Rows.Add(r0)
'                    Next

'                End If

'            Next

'        End If


'    End Sub

'    Public Sub CheckAuthorizedResoucesLevels()
'        Dim facUserSecurity As New UserSecurityFacade
'        Dim dt As DataTable
'        Dim aRows() As DataRow
'        Dim RID, LVL As String
'        Dim intCount1, intCount2 As Integer

'        dt = facUserSecurity.GetRoleAccessLevels(ddlGroups.SelectedItem.Value)

'        'Loop through the table of checkboxes and check each if the role being
'        'examined has an entry in the RolesAccess DataTable in the dt DataTable.
'        'We start at row index position 6 to exclude the rows without checkboxes
'        For intCount1 = 3 To tblModuleResources.Rows.Count - 1
'            For intCount2 = 1 To 5
'                If tblModuleResources.Rows(intCount1).Cells(intCount2).Controls.Count = 1 Then
'                    Dim chkID As String
'                    chkID = CType(tblModuleResources.Rows(intCount1).Cells(intCount2).Controls(0), CheckBox).ID.ToString
'                    RID = CType(tblModuleResources.Rows(intCount1).Cells(intCount2).Controls(0), CheckBox).ID.Remove(0, 7)
'                    LVL = CType(tblModuleResources.Rows(intCount1).Cells(intCount2).Controls(0), CheckBox).ID.Substring(4, 1)
'                    aRows = dt.Select("ResourceID = " & RID & " AND AccessLevel = " & LVL)
'                    If aRows.Length > 0 Then
'                        Dim chk As CheckBox = CType(tblModuleResources.Rows(intCount1).Cells(intCount2).Controls(0), CheckBox)
'                        chk.Checked = True
'                    End If
'                End If
'            Next

'        Next
'    End Sub

'    Sub ClearAll()
'        Dim intCount, intCount2 As Integer
'        Dim chk As CheckBox
'        For intCount = 1 To tblModuleResources.Rows.Count - 1
'            For intCount2 = 1 To 5
'                If tblModuleResources.Rows(intCount).Cells(intCount2).Controls.Count = 1 Then
'                    chk = CType(tblModuleResources.Rows(intCount).Cells(intCount2).Controls(0), CheckBox)
'                    chk.Checked = False
'                End If
'            Next

'        Next
'    End Sub

'    Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        'If the Set All button is clicked then at least one of the module level
'        'checkboxes must be checked
'        Dim ctlL5 As Control
'        Dim ctlL4 As Control
'        Dim ctlL3 As Control
'        Dim ctlL2 As Control
'        Dim ctlL1 As Control
'        Dim ctlError As Control
'        Dim intCount1, intCount2 As Integer
'        Dim RID, LVL As Integer
'        Dim facUserSecurity As New UserSecurityFacade

'        ctlL5 = tblModuleResources.FindControl("L5")
'        ctlL4 = tblModuleResources.FindControl("L4")
'        ctlL3 = tblModuleResources.FindControl("L3")
'        ctlL2 = tblModuleResources.FindControl("L2")
'        ctlL1 = tblModuleResources.FindControl("L1")
'        ctlError = tblModuleResources.FindControl("lblError")

'        If (sender.ID = "btnSetAll" And CType(ctlL5, CheckBox).Checked = False And CType(ctlL4, CheckBox).Checked = False And CType(ctlL3, CheckBox).Checked = False And CType(ctlL2, CheckBox).Checked = False And CType(ctlL1, CheckBox).Checked = False) Then
'            strError = "You must select at least one module level option to use the Set All button."
'            CType(ctlError, Label).Text = strError
'        Else
'            'First clear the error label display
'            CType(ctlError, Label).Text = ""


'            'No errors so we can go ahead with the save operation
'            'We enclose this section in a Try Block so we can rollback the Delete operations
'            'if any of the Insert statements fail
'            Try

'                'Loop through the table and find each checkbox that is checked
'                'We start at row index position 2 to exclude the rows without checkboxes
'                For intCount1 = 3 To tblModuleResources.Rows.Count - 1
'                    For intCount2 = 1 To 5
'                        If tblModuleResources.Rows(intCount1).Cells(intCount2).Controls.Count = 1 Then
'                            Dim strID As String
'                            strID = CType(tblModuleResources.Rows(intCount1).Cells(intCount2).Controls(0), CheckBox).ID.ToString
'                            RID = CType(tblModuleResources.Rows(intCount1).Cells(intCount2).Controls(0), CheckBox).ID.Remove(0, 7)
'                            LVL = CType(tblModuleResources.Rows(intCount1).Cells(intCount2).Controls(0), CheckBox).ID.Substring(4, 1)
'                            'First delete any access that this role has to the resource id. We do not want
'                            'to do this for each cell in the row. We only need to delete once so we will do
'                            'it for the first cell only
'                            If intCount2 = 1 Then
'                                facUserSecurity.DeleteRoleResourceAccessLevels(ddlGroups.SelectedItem.Value, RID)
'                            End If

'                            'If the checkbox is checked then we need to add an entry for this role and access level to the resource
'                            If CType(tblModuleResources.Rows(intCount1).Cells(intCount2).Controls(0), CheckBox).Checked = True Then
'                                facUserSecurity.AddRoleResourceAccessLevel(ddlGroups.SelectedItem.Value, RID, LVL, Session("UserName"))
'                            End If
'                        End If
'                    Next
'                Next


'                'Commit the transaction
'                'ContextUtil.SetComplete()
'            Catch ex As System.Exception
 '            	Dim exTracker = new AdvApplicationInsightsInitializer()
'            	exTracker.TrackExceptionWrapper(ex)

'                'ContextUtil.SetAbort()
'                If ex.InnerException Is Nothing Then
'                    Throw New System.Exception(ex.Message)
'                Else
'                    Throw New System.Exception(ex.Message & "   Error Text:" & ex.InnerException.Message)
'                End If

'            End Try
'        End If

'    End Sub

'    Sub InsertRow(ByVal RoleId As String, ByVal ModId As Integer, ByVal RID As Integer, ByVal ALevel As Integer)

'    End Sub

'    Public Sub HandleSubModuleCheckBoxes()
'        chk1.AutoPostBack = True
'        AddHandler chk1.CheckedChanged, AddressOf SubModuleChkChanged

'        chk2.AutoPostBack = True
'        AddHandler chk2.CheckedChanged, AddressOf SubModuleChkChanged

'        chk3.AutoPostBack = True
'        AddHandler chk3.CheckedChanged, AddressOf SubModuleChkChanged

'        chk4.AutoPostBack = True
'        AddHandler chk4.CheckedChanged, AddressOf SubModuleChkChanged

'        chk5.AutoPostBack = True
'        AddHandler chk5.CheckedChanged, AddressOf SubModuleChkChanged

'        chk6.AutoPostBack = True
'        AddHandler chk6.CheckedChanged, AddressOf SubModuleChkChanged
'    End Sub

'    Private Sub SetModuleLevelCheckBoxesForReports()
'        Dim ctl As Control

'        ctl = tblModuleResources.FindControl("L5")
'        DirectCast(ctl, CheckBox).Enabled = False

'        ctl = tblModuleResources.FindControl("L4")
'        DirectCast(ctl, CheckBox).Enabled = False

'        ctl = tblModuleResources.FindControl("L3")
'        DirectCast(ctl, CheckBox).Enabled = False

'        ctl = tblModuleResources.FindControl("L2")
'        DirectCast(ctl, CheckBox).Enabled = False

'    End Sub

'    Private Sub SetModuleLevelCheckBoxesForNotReports()
'        Dim ctl As Control

'        ctl = tblModuleResources.FindControl("L5")
'        DirectCast(ctl, CheckBox).Enabled = True

'        ctl = tblModuleResources.FindControl("L4")
'        DirectCast(ctl, CheckBox).Enabled = True

'        ctl = tblModuleResources.FindControl("L3")
'        DirectCast(ctl, CheckBox).Enabled = True

'        ctl = tblModuleResources.FindControl("L2")
'        DirectCast(ctl, CheckBox).Enabled = True

'    End Sub

'    Private Sub ValidateLHS()
'        'Ensure that a module is selected
'        If ddlModules.SelectedValue = "" Then
'            strError = "Module is a required field" & vbCr
'        End If
'        'Ensure that a page type is selected
'        If ddlTypes.SelectedValue = "" Then
'            strError &= "Type is a required field" & vbCr
'        End If
'        'Ensure that a role/group is selected
'        If ddlGroups.SelectedValue = "" Then
'            strError &= "Group is a required field" & vbCr
'        End If

'    End Sub

'    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

'        '   Set error condition
'        Customvalidator1.ErrorMessage = errorMessage
'        Customvalidator1.IsValid = False

'        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
'            '   Display error in message box in the client
'            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
'        End If

'    End Sub

''    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender 
'        'add to this list any button or link that should ignore the Confirm Exit Warning.
'        Dim controlsToIgnore As New ArrayList() 
'        'add save button 
'        controlsToIgnore.Add(btnSave)
'       'Add javascript code to warn the user about non saved changes 
'        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
'    End Sub
'End Class

