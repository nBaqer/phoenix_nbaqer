Imports System.Data
Imports System.Diagnostics
Imports System.Xml
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI

Namespace AdvWeb.SY

' ===============================================================================
' FAME.AdvantageV1.DataAccess
'
' StudentPopupSearch.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
' ===============================================================================
    Partial Class StudentPopupSearch
        Inherits Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents LblStudentID As Label
        Protected WithEvents TxtStudentID As TextBox
        Protected WithEvents DDLStatus As DropDownList
        Protected WithEvents BtnSave As Button
        Protected WithEvents BtnNew As Button
        Protected WithEvents BtnDelete As Button
        Protected WithEvents Btnhistory As Button
        Protected WithEvents StudentLNameLinkButton As LinkButton


        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        'Private pObj As New UserPagePermissionInfo
        Private campusId As String
        Private campusDesc As String = String.Empty
        Private resourceId As Integer
        Private parentId As Integer
        Dim isSystemAdministrator As Boolean = False
        Private ReadOnly maxFieldLengths(5) As Integer
        ' DE7569 5/1/2012 Janet Robinson
        Dim userId As String = String.Empty
        Dim userName As String = String.Empty

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            'Dim fac As New UserSecurityFacade
            Dim advantageUserState As User = AdvantageSession.UserState

            ViewState("IsUserSa") = advantageUserState.IsUserSA

            ' DE7569 5/1/2012 Janet Robinson
            userId = AdvantageSession.UserState.UserId.ToString
            userName = AdvantageSession.UserState.UserName

            resourceId = CInt(Request.QueryString("resid"))
            campusId = Request.QueryString("cmpid")
            If Not Request.QueryString("ParentId") Is Nothing Then parentId = CInt(Request.QueryString("ParentId").Replace("=", ""))
            If (resourceId = 340) Or (resourceId = 84) Then
                SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, "293", campusId) 'fac.GetUserResourcePermissions(userId, 293, campusId)
            Else
                SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId) 'fac.GetUserResourcePermissions(userId, resourceId, campusId)
            End If

            Dim studentSearchFacade As New StudentSearchFacade
            

            If Not Page.IsPostBack Then
                '   build dropdownlists
                BuildDropdownlists()
                BindDataListMRU()
                If campusDesc = String.Empty Then
                    lbHdr.Text = "Recently Accessed Students"
                Else
                    lbHdr.Text = "Recently Accessed Students from " & campusDesc
                End If
            End If

        End Sub
        Private Sub BuildDropdownlists()
            BuildStatusDDL()
            BuildPrgVerDDL()
            BuildTermsDDL()
            BuildAcademicYearsDDL()
        End Sub

        Private Sub BuildStatusDDL()
            Dim ddlStatusId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStatusId"), DropDownList)
            'Bind the Status DropDownList
            Dim statuses As New StatusCodeFacade
            With ddlStatusId
                .DataTextField = "StatusCodeDescrip"
                .DataValueField = "StatusCodeId"
                .DataSource = statuses.GetAllStatusCodesForStudents(campusId)
                .DataBind()
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                .SelectedIndex = 0
            End With
        End Sub
        'Private Sub BuildPrgVerDDL()

        '    'Bind the Programs DrowDownList
        '    With ddlStudentPrgVerId
        '        .DataTextField = "PrgVerDescrip"
        '        .DataValueField = "PrgVerId"
        '        .DataSource = (New StudentSearchFacade).GetAllEnrollmentsUsedByStudents()
        '        .DataBind()
        '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        '        .SelectedIndex = 0
        '    End With

        'End Sub
        Private Sub BuildPrgVerDDL()
            Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStudentPrgVerId"), DropDownList)
            'Bind the Programs DrowDownList
            With ddlStudentPrgVerId
                .DataTextField = "PrgVerDescrip"
                .DataValueField = "PrgVerId"
                .DataSource = (New StudentSearchFacade).GetAllEnrollmentsUsedByStudents(XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
                .DataBind()
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                .SelectedIndex = 0
            End With

        End Sub

        'Private Sub BuildTermsDDL()
        '    '   bind the Terms DDL
        '    Dim terms As New StudentsAccountsFacade

        '    With ddlTermId
        '        .DataTextField = "TermDescrip"
        '        .DataValueField = "TermId"
        '        .DataSource = terms.GetAllTerms(True)
        '        .DataBind()
        '        '.Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
        '        .SelectedIndex = 0
        '    End With

        'End Sub
        Private Sub BuildTermsDDL()
            '   bind the Terms DDL
            Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
            Dim terms As New StudentsAccountsFacade

            With ddlTermId
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataSource = terms.GetAllTerms(True, XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString)
                .DataBind()
                '.Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

        End Sub
        Private Sub BuildTermsDDL(ByVal stuEnrollId As String, ByVal campusidx As String)
            '   bind the Terms DDL
            Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
            Dim terms As New StudentSearchFacade

            With ddlTermId
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                .DataSource = terms.GetAllStudentTerm(stuEnrollId, campusidx)
                .SelectedIndex = -1
                .DataBind()
                '.Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
                If .Items.Count > 0 Then .SelectedIndex = 0
            End With

        End Sub



        Private Sub BuildAcademicYearsDDL()
            '   bind the AcademicYearss DDL
            Dim ddlAcademicYearId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlAcademicYearId"), DropDownList)
            Dim academicYears As New StudentsAccountsFacade

            With ddlAcademicYearId
                .DataTextField = "AcademicYearDescrip"
                .DataValueField = "AcademicYearId"
                .DataSource = academicYears.GetAllAcademicYears()
                .DataBind()
                .SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYearId.Items)
            End With
        End Sub
        Private Sub BindDataList()
            '   Bind datalist
            ''Added by saraswathi lakshmanan to show the InSchool enrollment for non Sa users.
            ''Added to fix iisue mantis 14829  -modified on June 10 2009
            ''resourceid=340 recordtransafer Grades, 84- PostCharges
            Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)
            Dim txtLastName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtLastName"), TextBox)
            Dim txtFirstName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtFirstName"), TextBox)
            Dim txtEnrollment As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtEnrollment"), TextBox)
            Dim ddlStatusId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStatusId"), DropDownList)
            Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStudentPrgVerId"), DropDownList)
            Dim txtSSN As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtSSN"), TextBox)
            If (resourceId = 340 Or resourceId = 84) And Not isUserSa Then
                dgrdTransactionSearch.DataSource = New DataView((New StudentSearchFacade).GetStudentSearchDSForInSchoolEnrollment(txtLastName.Text, txtFirstName.Text, txtSSN.Text, txtEnrollment.Text, ddlStatusId.SelectedValue, ddlStudentPrgVerId.SelectedValue, campusId).Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
            Else
                dgrdTransactionSearch.DataSource = New DataView((New StudentSearchFacade).GetStudentSearchDS(txtLastName.Text, txtFirstName.Text, txtSSN.Text, txtEnrollment.Text, ddlStatusId.SelectedValue, ddlStudentPrgVerId.SelectedValue, campusId).Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
            End If

            dgrdTransactionSearch.DataBind()

        End Sub
        Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim txtLastName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtLastName"), TextBox)
            Dim txtFirstName As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtFirstName"), TextBox)
            Dim txtEnrollment As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtEnrollment"), TextBox)
            Dim ddlStatusId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStatusId"), DropDownList)
            Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlStudentPrgVerId"), DropDownList)
            Dim txtSSN As TextBox = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("txtSSN"), TextBox)
            If txtLastName.Text = "" And txtFirstName.Text = "" And txtEnrollment.Text = "" And ddlStatusId.SelectedValue = Guid.Empty.ToString And ddlStudentPrgVerId.SelectedValue = Guid.Empty.ToString And txtSSN.Text = "" Then
                DisplayErrorMessage("Please enter a value to search")
                Exit Sub
            End If

            If Len(txtSSN.Text.Trim) > 0 Then
                'remove spaces, dashes from the search
                Dim ssn As String = txtSSN.Text.Trim
                Dim arrChar() As Char = {"-"c, " "c}
                Do While ssn.IndexOfAny(arrChar) >= 0
                    ssn = ssn.Remove(ssn.IndexOfAny(arrChar), 1)
                Loop

                'we only allow searchs using a full SSN
                If Not ssn.Length = 9 Then
                    DisplayErrorMessage("Only full SSN searches are permitted. Please, enter a full SSN.")
                    Exit Sub
                End If

            End If

            '   bind datalist
            BindDataList()

        End Sub
        Private Sub dgrdTransactionSearch_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgrdTransactionSearch.ItemCommand
            Dim parametersArray(7) As String '= {e.CommandArgument, (New StudentSearchFacade).GetStudentNameByID(e.CommandArgument)}

            '   set StudentName
            parametersArray(0) = CType(e.Item.FindControl("lnkLastName"), LinkButton).Text + " " + CType(e.Item.FindControl("lblFirstName1"), Label).Text

            '   set StuEnrollment
            Dim ddlEnrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)
            parametersArray(1) = ddlEnrollments.SelectedItem.Value
            parametersArray(2) = ddlEnrollments.SelectedItem.Text

            '   set Terms
            If Not parentId = 629 Then
                Dim ddlTerms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)
                parametersArray(3) = ddlTerms.SelectedItem.Value
                parametersArray(4) = ddlTerms.SelectedItem.Text
                Session("Term") = ddlTerms.SelectedItem.Text
            End If

            '   set AcademicYears
            Dim ddlAcademicYears As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)
            parametersArray(5) = ddlAcademicYears.SelectedItem.Value
            parametersArray(6) = ddlAcademicYears.SelectedItem.Text

            parametersArray(7) = CType(e.Item.FindControl("lnkLastName"), LinkButton).CommandArgument

            Session("StuEnrollment") = ddlEnrollments.SelectedItem.Text
            Session("AcademicYear") = ddlAcademicYears.SelectedItem.Text

            'parametersArray(7) = txtStudentID.Text


            '   send parameters back to the parent page
            'CommonWebUtilities.PassDataBackToParentPage(Page, parametersArray)
            PassDataBackToParentPage(Page, parametersArray)
        End Sub
        Private Sub DisplayErrorMessage(ByVal errorMessage As String)

            'Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)

        End Sub

        Private Sub dgrdTransactionSearch_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdTransactionSearch.ItemDataBound

            '   process only rows in the items section
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem
                    '   Register Max field length for Last Name
                    RegisterMaxFieldLength(0, e.Item.DataItem("LastName"))

                    '   Register Max field length for First Name
                    RegisterMaxFieldLength(1, e.Item.DataItem("FirstName"))

                    '   Register Max field length for SSN
                    RegisterMaxFieldLength(2, e.Item.DataItem("SSN"))

                    '   get Enrollments dropdownlist
                    Dim enrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)

                    '   populate  Enrollments ddl
                    Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                    Dim drows() As DataRow = dr.Row.GetChildRows("StudentsStudentEnrollments")
                    For i As Integer = 0 To drows.Length - 1
                        enrollments.Items.Add(New ListItem(CType(drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment"), String), CType(drows(i)("StuEnrollId"), Guid).ToString))
                        '   Register Max field length for Enrollments. Add some slack because it is a dropdownlist
                        RegisterMaxFieldLength(3, drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment") + "XXXX")
                    Next

                    'Add All Enrollment if the switch MajorMinor is true and exists more thatn one enrollment
                    Dim majorMinorActive = GetSettingString("MajorsMinorsConcentrations")
                    If majorMinorActive.ToUpper() = "YES" AndAlso enrollments.Items.Count > 1 Then
                        Dim item As ListItem = New ListItem()
                        item.Value = Guid.Empty.ToString()
                        item.Text = "All Enrollments"
                        enrollments.Items.Add(item)
                    End If

                    'select latest enrollment
                    If Not drows.Length = 0 Then
                        'sort terms by date
                        Dim idx(drows.Length - 1, 1) As Double
                        For i As Integer = 0 To drows.Length - 1
                            idx(i, 0) = i
                            If Not drows(i)("StartDate") Is DBNull.Value Then
                                idx(i, 1) = CType(drows(i)("StartDate"), Date).Ticks
                            Else
                                idx(i, 1) = 0
                            End If
                        Next

                        'sort the terms by Date
                        If drows.Length > 1 Then
                            For j As Integer = 1 To drows.Length - 1
                                For k = j To drows.Length - 1
                                    If idx(j, 1) > idx(j - 1, 1) Then
                                        Dim t0 As Double = idx(j - 1, 0)
                                        Dim t1 As Double = idx(j - 1, 1)
                                        idx(j - 1, 0) = idx(j, 0)
                                        idx(j - 1, 1) = idx(j, 1)
                                        idx(j, 0) = t0
                                        idx(j, 1) = t1
                                    End If
                                Next
                            Next
                        End If

                        '   latest Enrollment is selected
                        enrollments.SelectedIndex = CType(idx(0, 0), Integer)
                    Else
                        enrollments.SelectedIndex = 0
                        CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                    End If

                    '   get Terms dropdownlist
                    Dim terms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)

                    'get all student terms
                    Dim trows() As DataRow = dr.Row.GetChildRows("StudentStudentTerms")


                    'if the student has no Terms ... populate all terms
                    If Not trows.Length = 0 Then
                        'sort terms by date
                        Dim idx(trows.Length - 1, 1) As Double
                        For i As Integer = 0 To trows.Length - 1
                            idx(i, 0) = i
                            If Not trows(i)("StartDate") Is DBNull.Value Then
                                idx(i, 1) = CType(trows(i)("StartDate"), Date).Ticks
                            End If
                        Next

                        'sort the terms by Date
                        If trows.Length > 1 Then
                            For j As Integer = 1 To trows.Length - 1
                                For k = j To trows.Length - 1
                                    If idx(j, 1) > idx(j - 1, 1) Then
                                        Dim t0 As Double = idx(j - 1, 0)
                                        Dim t1 As Double = idx(j - 1, 1)
                                        idx(j - 1, 0) = idx(j, 0)
                                        idx(j - 1, 1) = idx(j, 1)
                                        idx(j, 0) = t0
                                        idx(j, 1) = t1
                                    End If
                                Next
                            Next
                        End If
                        Dim cnt As Integer = 0
                        For i As Integer = 0 To trows.Length - 1
                            If trows(CType(idx(i, 0), Integer))("StuEnrollId").ToString = enrollments.SelectedValue Then
                                'populate only student terms
                                terms.Items.Add(New ListItem(CType(trows(CType(idx(i, 0), Integer)).GetParentRow("TermsStudentTerms")("TermDescrip"), String), CType(trows(CType(idx(i, 0), Integer))("TermId"), Guid).ToString))

                                '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                                RegisterMaxFieldLength(4, terms.Items(cnt).Text + "XXXX")
                                cnt = cnt + 1
                            End If
                        Next

                        '   select latest term for the student
                        If Not dr.Row("CurrentTerm") Is DBNull.Value Then
                            terms.SelectedValue = CType(dr.Row("CurrentTerm"), Guid).ToString
                        Else
                            terms.SelectedIndex = 0
                        End If
                        Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                        If terms.Items.Count = 0 Then
                            BuildTermsDDL(enrollments.SelectedValue.ToString(), campusId)
                            For i As Integer = 0 To ddlTermId.Items.Count - 1
                                terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                                '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                                RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                            Next
                            If ddlTermId.Items.Count = 0 Then
                                CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                            End If
                            '   first Enrollment is selected
                            terms.SelectedIndex = 0
                        End If
                    Else
                        Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                        '   populate Terms ddl
                        BuildTermsDDL(enrollments.SelectedValue.ToString(), campusId)
                        For i As Integer = 0 To ddlTermId.Items.Count - 1
                            terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                            RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                        Next

                        If ddlTermId.Items.Count = 0 Then
                            CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                        End If
                        '   first Enrollment is selected
                        terms.SelectedIndex = 0

                    End If

                    '   get AcademicYear dropdownlist
                    Dim academicYear As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)
                    Dim ddlAcademicYearId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlAcademicYearId"), DropDownList)
                    '   populate AcademicYear ddl
                    For Each item As ListItem In ddlAcademicYearId.Items
                        Dim newItem As New ListItem(item.Text, item.Value)
                        newItem.Selected = item.Selected
                        academicYear.Items.Add(newItem)

                        '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                        RegisterMaxFieldLength(5, item.Text + "XXXX")
                    Next

                Case ListItemType.Footer
                    If SumOfMaxFieldLengths() > 0 Then
                        '   calculate the width percentage of each cell in the footer
                        Dim cells As ControlCollection = e.Item.Controls
                        For i As Integer = 0 To cells.Count - 1
                            Dim cell As TableCell = CType(cells(i), TableCell)
                            '   add the width attribute to each cell only if there are records
                            cell.Attributes.Add("width", CType(maxFieldLengths(i) * 100.0 / SumOfMaxFieldLengths(), Integer).ToString + "%")
                        Next
                    End If
                Case ListItemType.Header
                    If parentId = 629 Then e.Item.Controls(4).Visible = False
            End Select
        End Sub

        Private Sub RegisterMaxFieldLength(ByVal idx As Integer, ByVal txt As Object)
            If Not txt.GetType.ToString = "System.DBNull" Then
                Dim text As String = CType(txt, String)
                If text.Length > maxFieldLengths(idx) Then
                    maxFieldLengths(idx) = text.Length
                End If
            End If
        End Sub
        Private Function SumOfMaxFieldLengths() As Integer
            SumOfMaxFieldLengths = 0
            For i As Integer = 0 To maxFieldLengths.Length - 1
                SumOfMaxFieldLengths += maxFieldLengths(i)
            Next
        End Function
        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
            ' 'add to this list any button or link that should ignore the Confirm Exit Warning.
            ' Dim controlsToIgnore As New ArrayList() 
            ' 'add save button 
            ' controlsToIgnore.Add(btnSave)
            ''Add javascript code to warn the user about non saved changes 
            ' CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
            BIndToolTip()
        End Sub
        Public Sub ddqty_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)

            'Dim drop As DropDownList = CType(sender, DropDownList)
            'Response.Write(drop.SelectedItem.Value)
            ''Dim ddlTermId As DropDownList = CType(dgrdTransactionSearch.Items(dgrdTransactionSearch.Items).FindControl("ddlTermId"), DropDownList)
            ''ddlTermId.Items.Clear()
            'Dim cell As TableCell = CType(drop.Parent, TableCell)
            'Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
            'Dim i As Integer = item.ItemIndex
            'Dim content As String = item.Cells(0).Text


            Dim ddllist As DropDownList = CType(sender, DropDownList)
            Dim cell As TableCell = CType(ddllist.Parent, TableCell)
            Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
            'Dim content As String = item.Cells(0).Text
            Dim ddlType As DropDownList = CType(item.Cells(0).FindControl("ddlEnrollmentsId"), DropDownList)
            Dim ddlItem As DropDownList = CType(item.Cells(1).FindControl("ddlTermsId"), DropDownList)

            'Fill in advance search combobox Terms for normal case and AllEnrollments
            ddlItem.Items.Clear()
            Dim enrollmentId As String = ddlType.SelectedValue
            With ddlItem
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                If (enrollmentId = Guid.Empty.ToString()) Then
                    .Items.Add(New ListItem(" All Term", Guid.Empty.ToString()))
                Else
                    .DataSource = (New StudentSearchFacade).GetStudentTerm(ddlType.SelectedValue, campusId)
                End If
                .DataBind()
            End With
        
            'With ddlItem
            '    .DataTextField = "TermDescrip"
            '    .DataValueField = "TermId"
            '    .DataSource = (New StudentSearchFacade).GetStudentTerm(ddlType.SelectedValue, campusId)
            '    .DataBind()
            '    '.SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYearId.Items)
            'End With
            If ddlItem.Items.Count = 0 Then
                With ddlItem
                    .DataTextField = "TermDescrip"
                    .DataValueField = "TermId"
                    .DataSource = (New StudentSearchFacade).GetAllStudentTerm(ddlType.SelectedValue, campusId)
                    .DataBind()
                End With
            End If
            If ddlItem.Items.Count = 0 Then
                CType(item.FindControl("lnkLastName"), LinkButton).Enabled = False
            Else
                CType(item.FindControl("lnkLastName"), LinkButton).Enabled = True
            End If
        End Sub

        ''Added by Saraswathi lakshmanan on August 24 2009
        ''To find the list controls and add a tool tip to those items in the control
        ''list controls include drop down list, list box, group checkbox, etc.
        Public Sub BIndToolTip()
            Dim i As Integer
            Dim ctl As Control
            For Each ctl In Page.Form.Controls
                If TypeOf ctl Is ListControl Then
                    For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                        DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                    Next
                End If
                If TypeOf ctl Is Panel Then
                    BindToolTipForControlsInsideaPanel(CType(ctl, Panel))
                End If
                If TypeOf ctl Is DataGrid Then
                    BindToolTipForControlsInsideaGrid(CType(ctl, DataGrid))
                End If
            Next
        End Sub
        Public Sub BindToolTipForControlsInsideaPanel(ByVal ctrlpanel As Panel)
            Dim ctrl As Control
            Dim j As Integer
            For Each ctrl In ctrlpanel.Controls
                If TypeOf ctrl Is ListControl Then
                    For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                        DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                    Next
                ElseIf TypeOf ctrl Is Panel Then
                    BindToolTipForControlsInsideaPanel(CType(ctrl, Panel))
                ElseIf TypeOf ctrl Is DataGrid Then
                    BindToolTipForControlsInsideaGrid(CType(ctrl, DataGrid))
                End If
            Next

        End Sub

        Public Sub BindToolTipForControlsInsideaGrid(ByVal ctrlGrid As DataGrid)
            Dim j As Integer
            Dim itm As DataGridItem
            Dim ctrl As Control
            Dim ctrl1 As Control

            For Each itm In ctrlGrid.Items
                For Each ctrl In itm.Controls
                    For Each ctrl1 In ctrl.Controls
                        If TypeOf ctrl1 Is ListControl Then
                            For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                                DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                            Next
                        End If
                    Next
                Next
            Next
        End Sub
        Private Sub dgrdTransactionSearchMRU_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgrdTransactionSearchMRU.ItemCommand
            Dim parametersArray(7) As String '= {e.CommandArgument, (New StudentSearchFacade).GetStudentNameByID(e.CommandArgument)}

            '   set StudentName
            parametersArray(0) = CType(e.Item.FindControl("lnkLastName"), LinkButton).Text + " " + CType(e.Item.FindControl("lblFirstName1"), Label).Text

            '   set StuEnrollment
            Dim ddlEnrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)
            parametersArray(1) = ddlEnrollments.SelectedItem.Value
            parametersArray(2) = ddlEnrollments.SelectedItem.Text

            '   set Terms
            Dim ddlTerms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)
            parametersArray(3) = ddlTerms.SelectedItem.Value
            parametersArray(4) = ddlTerms.SelectedItem.Text

            '   set AcademicYears
            Dim ddlAcademicYears As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)
            parametersArray(5) = ddlAcademicYears.SelectedItem.Value
            parametersArray(6) = ddlAcademicYears.SelectedItem.Text

            'parametersArray(7) = CType(e.Item.FindControl("lnkLastName"), LinkButton).CommandArgument

            parametersArray(7) = e.CommandArgument.ToString()


            Session("StuEnrollment") = ddlEnrollments.SelectedItem.Text
            Session("AcademicYear") = ddlAcademicYears.SelectedItem.Text
            Session("Term") = ddlTerms.SelectedItem.Text

            '   send parameters back to the parent page
            PassDataBackToParentPage(Page, parametersArray)
        End Sub

        Private Sub PassDataBackToParentPage(ByVal pagex As Page, ByVal parametersArray() As String)
            If pagex.Request.Browser.EcmaScriptVersion.Major >= 1 Then
                '   this is the beginning of the javascript code  
                Const scriptBegin As String = "<script type='text/javascript'>window.Onload=PassData();function PassData(){"

                '   this is the middle of the javascript
                '   build the parameter list to be returned to the parent page
                Dim scriptMiddle As String = ""
                'Dim cntParam As Integer = parametersArray.Length
                If parentId = 629 Then
                    For i As Integer = 0 To 2
                        ''Thic Condition added by Saraswathi Since it gave error
                        ''When the query string didnot have eneough argements
                        ''Modified by Saraswathi on Jan 8 2009

                        If pagex.Request.QueryString.Keys.Count + 1 > parametersArray.Length + 3 Then

                            If pagex.Request.QueryString.Keys(i + 3).ToString = "ddlShiftId" Then
                                scriptMiddle += "window.opener.document.getElementById('ddlShiftId').SelectedValue='" + ReplaceSpecialCharactersInJavascriptMessage(parametersArray(8).Trim) + "';"
                            End If
                            scriptMiddle += "window.opener.document.getElementById('" + pagex.Request.QueryString.Keys(i + 3).ToString + "').value='" + ReplaceSpecialCharactersInJavascriptMessage(parametersArray(i).Trim) + "';"
                        End If
                    Next

                Else
                    For i As Integer = 0 To parametersArray.Length - 1
                        ''Thic Condition added by Saraswathi Since it gave error
                        ''When the query string didnot have eneough argements
                        ''Modified by Saraswathi on Jan 8 2009

                        If pagex.Request.QueryString.Keys.Count + 1 > parametersArray.Length + 3 Then

                            If pagex.Request.QueryString.Keys(i + 3).ToString = "ddlShiftId" Then
                                scriptMiddle += "window.opener.document.getElementById('ddlShiftId').SelectedValue='" + ReplaceSpecialCharactersInJavascriptMessage(parametersArray(8).Trim) + "';"
                            End If
                            scriptMiddle += "window.opener.document.getElementById('" + pagex.Request.QueryString.Keys(i + 3).ToString + "').value='" + ReplaceSpecialCharactersInJavascriptMessage(parametersArray(i).Trim) + "';"
                        End If
                    Next
                End If




                '   this is the end of the javascript code
                Const scriptEnd As String = "window.close();}</script>"

                '   Register the javascript code
                ScriptManager.RegisterStartupScript(pagex, pagex.GetType(), "PassDataBackToParentPage", scriptBegin + scriptMiddle + scriptEnd, False)
            End If
        End Sub
        Private Sub BindDataListMRU()
            Dim resourceIdx As Integer
            'Dim fac As New ResourcesRelationsFacade
            Dim fac2 As New MRUFacade
            Dim mruTypeId As Integer
            Dim mruType As String = "Students"
            Dim modName As String
            Dim procMod As Boolean
            resourceIdx = CInt(HttpContext.Current.Request.Params("resid"))
            Dim dsMRU As New DataSet
            If Not resourceIdx = 264 Then
                modName = HttpContext.Current.Request.Params("mod")
                Select Case modName.ToUpper
                    Case "AD"
                        mruType = "Leads"
                        procMod = True
                    Case "AR", "SA", "FA", "FC"
                        mruType = "Students"
                        procMod = True
                    Case "PL"
                        mruTypeId = fac2.GetResourceMRUTypeId(resourceIdx)
                        Select Case mruTypeId
                            Case 1
                                mruType = "Students"
                                procMod = True
                            Case 2
                                mruType = "Employers"
                                procMod = True
                        End Select
                    Case "HR"
                        mruType = "Employees"
                        procMod = True
                End Select

            Else 'Home Page. For this we will use the module to determine what MRU should be displayed
                modName = HttpContext.Current.Request.Params("mod")
                Select Case modName.ToUpper
                    Case "AD"
                        mruType = "Leads"
                        procMod = True
                    Case "AR", "SA", "FA", "PL", "FC"
                        mruType = "Students"
                        procMod = True
                    Case "HR"
                        mruType = "Employees"
                        procMod = True
                End Select
            End If
            If procMod = True Then
                ' US3054 4/17/2012 Janet Robinson switch MRU return to match left MRU bar for Students
                If mruType = "Students" Then
                    Dim objMRUFac As New MRUFacade
                    dsMRU = objMRUFac.LoadMRUForStudentSearch(userId, HttpContext.Current.Request.Params("cmpid"))
                    If Not IsNothing(dsMRU) Then

                        Dim dr As DataRow()
                        dr = dsMRU.Tables(0).Select("CampusID='" + HttpContext.Current.Request.Params("cmpid") + "'")
                        If dr.Length > 0 Then
                            For Each row In dr
                                campusDesc = CType(row("CampusDescrip"), String)
                                Exit For
                            Next

                        End If


                    End If
                End If
            End If
            If dsMRU.Tables.Count > 0 Then
                If dsMRU.Tables("MRUList").Rows.Count > 0 Then
                    Dim isUserSa = CType(ViewState("IsUserSa"), Boolean)
                    If (resourceIdx = 340 Or resourceIdx = 84) And Not isUserSa Then
                        dgrdTransactionSearchMRU.DataSource = New DataView((New StudentSearchFacade).GetStudentSearchDSForInSchoolEnrollmentMRU(dsMRU, campusId).Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
                    Else
                        dgrdTransactionSearchMRU.DataSource = New DataView((New StudentSearchFacade).GetStudentSearchDSMRU(dsMRU, campusId).Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
                    End If
                    dgrdTransactionSearchMRU.DataBind()
                End If
            End If
        End Sub

        Private Sub dgrdTransactionSearchMRU_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdTransactionSearchMRU.ItemDataBound
            '   process only rows in the items section
            Select Case e.Item.ItemType
                Case ListItemType.Item, ListItemType.AlternatingItem
                    '   Register Max field length for Last Name

                    RegisterMaxFieldLength(0, e.Item.DataItem("LastName"))

                    '   Register Max field length for First Name
                    RegisterMaxFieldLength(1, e.Item.DataItem("FirstName"))

                    '   Register Max field length for SSN
                    RegisterMaxFieldLength(2, e.Item.DataItem("SSN"))

                    '   get Enrollments dropdownlist
                    Dim enrollments As DropDownList = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)

                    '   populate  Enrollments ddl
                    Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
                    Dim drows() As DataRow = dr.Row.GetChildRows("StudentsStudentEnrollments")
                    For i As Integer = 0 To drows.Length - 1
                        Dim li As ListItem = New ListItem(CType(drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment"), String), CType(drows(i)("StuEnrollId"), Guid).ToString)
                        enrollments.Items.Add(li)

                        '   Register Max field length for Enrollments. Add some slack because it is a dropdownlist
                        RegisterMaxFieldLength(3, drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment") + "XXXX")
                    Next

                    Dim majorMinorActive = GetSettingString("MajorsMinorsConcentrations")
                    If (drows.Length > 1 AndAlso majorMinorActive.ToUpper() = "YES") Then
                        Dim ae As ListItem = New ListItem("All Enrollments", Guid.Empty.ToString())
                        enrollments.Items.Add(ae)

                    End If
                    'select latest enrollment
                    If Not drows.Length = 0 Then
                        'sort terms by date
                        Dim idx(drows.Length - 1, 1) As Double
                        For i As Integer = 0 To drows.Length - 1
                            idx(i, 0) = i
                            If Not drows(i)("StartDate") Is DBNull.Value Then
                                idx(i, 1) = CType(drows(i)("StartDate"), Date).Ticks
                            Else
                                idx(i, 1) = 0
                            End If
                        Next

                        'sort the terms by Date
                        If drows.Length > 1 Then
                            For j As Integer = 1 To drows.Length - 1
                                For k = j To drows.Length - 1
                                    If idx(j, 1) > idx(j - 1, 1) Then
                                        Dim t0 As Double = idx(j - 1, 0)
                                        Dim t1 As Double = idx(j - 1, 1)
                                        idx(j - 1, 0) = idx(j, 0)
                                        idx(j - 1, 1) = idx(j, 1)
                                        idx(j, 0) = t0
                                        idx(j, 1) = t1
                                    End If
                                Next
                            Next
                        End If

                        '   latest Enrollment is selected
                        enrollments.SelectedIndex = CType(idx(0, 0), Integer)
                    Else
                        enrollments.SelectedIndex = 0
                        CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                    End If

                    '   get Terms dropdownlist
                    Dim terms As DropDownList = CType(e.Item.FindControl("ddlTermsId"), DropDownList)
                    ' If parentId = 629 Then terms.Visible = False
                    'get all student terms
                    Dim trows() As DataRow = dr.Row.GetChildRows("StudentStudentTerms")


                    'if the student has no Terms ... populate all terms
                    If Not trows.Length = 0 Then
                        'sort terms by date
                        Dim idx(trows.Length - 1, 1) As Double
                        For i As Integer = 0 To trows.Length - 1
                            idx(i, 0) = i
                            If Not trows(i)("StartDate") Is DBNull.Value Then
                                idx(i, 1) = CType(trows(i)("StartDate"), Date).Ticks
                            End If
                        Next

                        'sort the terms by Date
                        If trows.Length > 1 Then
                            For j As Integer = 1 To trows.Length - 1
                                For k = j To trows.Length - 1
                                    If idx(j, 1) > idx(j - 1, 1) Then
                                        Dim t0 As Double = idx(j - 1, 0)
                                        Dim t1 As Double = idx(j - 1, 1)
                                        idx(j - 1, 0) = idx(j, 0)
                                        idx(j - 1, 1) = idx(j, 1)
                                        idx(j, 0) = t0
                                        idx(j, 1) = t1
                                    End If
                                Next
                            Next
                        End If
                        Dim cnt As Integer = 0
                        For i As Integer = 0 To trows.Length - 1
                            If trows(CType(idx(i, 0), Integer))("StuEnrollId").ToString = enrollments.SelectedValue Then
                                'populate only student terms
                                terms.Items.Add(New ListItem(CType(trows(CType(idx(i, 0), Integer)).GetParentRow("TermsStudentTerms")("TermDescrip"), String), CType(trows(CType(idx(i, 0), Integer))("TermId"), Guid).ToString))

                                '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                                RegisterMaxFieldLength(4, terms.Items(cnt).Text + "XXXX")
                                cnt = cnt + 1
                            End If
                        Next

                        '   select latest term for the student
                        If Not dr.Row("CurrentTerm") Is DBNull.Value Then
                            terms.SelectedValue = CType(dr.Row("CurrentTerm"), Guid).ToString
                        Else
                            terms.SelectedIndex = 0
                        End If
                        Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                        If terms.Items.Count = 0 Then
                            BuildTermsDDL(enrollments.SelectedValue.ToString(), campusId)
                            For i As Integer = 0 To ddlTermId.Items.Count - 1
                                terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                                '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                                RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                            Next
                            If ddlTermId.Items.Count = 0 Then
                                CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                            End If
                            '   first Enrollment is selected
                            terms.SelectedIndex = 0
                        End If
                    Else
                        Dim ddlTermId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlTermId"), DropDownList)
                        '   populate Terms ddl
                        BuildTermsDDL(enrollments.SelectedValue.ToString(), campusId)
                        For i As Integer = 0 To ddlTermId.Items.Count - 1
                            terms.Items.Add(New ListItem(ddlTermId.Items(i).Text, ddlTermId.Items(i).Value))

                            '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                            RegisterMaxFieldLength(4, ddlTermId.Items(i).Text + "XXXX")
                        Next

                        If ddlTermId.Items.Count = 0 Then
                            CType(e.Item.FindControl("lnkLastName"), LinkButton).Enabled = False
                        End If
                        '   first Enrollment is selected
                        terms.SelectedIndex = 0

                    End If

                    '   get AcademicYear dropdownlist
                    Dim academicYear As DropDownList = CType(e.Item.FindControl("ddlAcademicYearsId"), DropDownList)
                    Dim ddlAcademicYearId As DropDownList = DirectCast(rpbAdvanceOption.FindItemByValue("AdvanceOption").FindControl("ddlAcademicYearId"), DropDownList)
                    '   populate AcademicYear ddl
                    For Each item As ListItem In ddlAcademicYearId.Items
                        Dim newItem As New ListItem(item.Text, item.Value)
                        newItem.Selected = item.Selected
                        academicYear.Items.Add(newItem)

                        '   Register Max field length for Terms. Add some slack because it is a dropdownlist
                        RegisterMaxFieldLength(5, item.Text + "XXXX")
                    Next

                Case ListItemType.Footer
                    If SumOfMaxFieldLengths() > 0 Then
                        '   calculate the width percentage of each cell in the footer
                        Dim cells As ControlCollection = e.Item.Controls
                        For i As Integer = 0 To cells.Count - 1
                            Dim cell As TableCell = CType(cells(i), TableCell)
                            '   add the width attribute to each cell only if there are records
                            cell.Attributes.Add("width", CType(maxFieldLengths(i) * 100.0 / SumOfMaxFieldLengths(), Integer).ToString + "%")
                        Next
                    End If
                Case ListItemType.Header
                    If parentId = 629 Then e.Item.Controls(4).Visible = False

            End Select
        End Sub
        Public Sub ddqtyMRU_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            Dim ddllist As DropDownList = CType(sender, DropDownList)
            Dim cell As TableCell = CType(ddllist.Parent, TableCell)
            Dim item As DataGridItem = CType(cell.Parent, DataGridItem)
            'Dim content As String = item.Cells(0).Text
            Dim ddlType As DropDownList = CType(item.Cells(0).FindControl("ddlEnrollmentsId"), DropDownList)
            Dim ddlItem As DropDownList = CType(item.Cells(1).FindControl("ddlTermsId"), DropDownList)

            ddlItem.Items.Clear()
            With ddlItem
                .DataTextField = "TermDescrip"
                .DataValueField = "TermId"
                If (ddlType.SelectedValue = Guid.Empty.ToString()) Then
                    .Items.Add(New ListItem(" All Term", Guid.Empty.ToString()))
                Else
                    .DataSource = (New StudentSearchFacade).GetStudentTerm(ddlType.SelectedValue, campusId)
                End If
                .DataBind()
                '.SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYearId.Items)
            End With
            If ddlItem.Items.Count = 0 Then
                With ddlItem
                    .DataTextField = "TermDescrip"
                    .DataValueField = "TermId"
                    .DataSource = (New StudentSearchFacade).GetAllStudentTerm(ddlType.SelectedValue, campusId)
                    .DataBind()
                End With
            End If
            If ddlItem.Items.Count = 0 Then
                CType(item.FindControl("lnkLastName"), LinkButton).Enabled = False
            Else
                CType(item.FindControl("lnkLastName"), LinkButton).Enabled = True
            End If
        End Sub
        Private Function ReplaceSpecialCharactersInJavascriptMessage(ByVal s As String) As String
            '   replace \ by \\, ' by \', " by "", CR by \r, LF by \n
            Return s.Trim().Replace("\", "\\").Replace("'", "\'").Replace("""", "\""").Replace(vbCr, "\r").Replace(vbLf, "\n")
        End Function

        Protected Sub rpbAdvanceOption_ItemClick(ByVal sender As Object, ByVal e As RadPanelBarEventArgs) Handles rpbAdvanceOption.ItemClick
            Dim txtLast As TextBox
            txtLast = CType(rpbAdvanceOption.Items(0).Controls(0).FindControl("txtLastName"), TextBox)
            txtLast.Focus()
        End Sub

        ''' <summary>
        ''' Get a setting as String
        ''' </summary>
        ''' <param name="settingName">The name of the setting</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function GetSettingString(settingName As String) As String
            Dim advAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                advAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                advAppSettings = New AdvAppSettings
            End If
            Dim result As String = advAppSettings.AppSettings(settingName).ToString()
            Return result
        End Function

    End Class
End NameSpace