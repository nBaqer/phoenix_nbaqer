
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class StudentIdSetUp
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblStudentIdFormat As System.Web.UI.WebControls.Label
    Protected WithEvents lblSelection As System.Web.UI.WebControls.Label
    Protected WithEvents lblResult As System.Web.UI.WebControls.Label
    Protected WithEvents lblStudLastName As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim advantageUserState As New BO.User()
        Dim m_Context As HttpContext
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        InitButtonsForLoad()
        If Not Page.IsPostBack Then
            'If pObj.HasFull Then
            '    btnsave.Enabled = True
            'Else
            '    btnsave.Enabled = False
            'End If
            BuildDDL()
            pnlEnrollFormat.Visible = False
            pnlSeqNumber.Visible = False

            Dim getStudentFormat As New StudentMasterFacade
            Dim StudInfo As StudentMasterInfo = getStudentFormat.GetStudentFormat()
            Dim getFormat As String
            'Dim intYearNumber As Integer
            'Dim intMonthNumber As Integer
            'Dim intDateNumber As Integer
            'Dim intLNameNumber As Integer
            'Dim intFNameNumber As Integer
            'Dim intSeqNumber As Integer
            'Dim intStartSeqNumber As Integer

            With StudInfo
                getFormat = .FormatType
                txtSeqNumber.Text = .SeqStartNumber
                txtYear.SelectedValue = .YearNumber
                txtMonth.SelectedValue = .MonthNumber
                txtDate.SelectedValue = .DateNumber
                txtLName.Text = .LNameNumber
                txtFName.Text = .FNameNumber
                txtSeq.Text = .SeqNumber
            End With
            If Not getFormat = "" Then
                If getFormat = 2 Then
                    radFormat.SelectedIndex = 1
                    pnlSeqNumber.Visible = True
                    pnlEnrollFormat.Visible = False
                    txtYear.SelectedValue = 0
                    txtMonth.SelectedValue = 0
                    txtDate.SelectedValue = 0
                    txtLName.Text = ""
                    txtFName.Text = ""
                    txtSeq.Text = ""
                ElseIf getFormat = 3 Then
                    radFormat.SelectedIndex = 2
                    pnlEnrollFormat.Visible = True
                    pnlSeqNumber.Visible = False
                ElseIf getFormat = 4 Then
                    radFormat.SelectedIndex = 3
                    pnlSeqNumber.Visible = False
                    pnlEnrollFormat.Visible = False
                Else
                    radFormat.SelectedIndex = 0
                    pnlSeqNumber.Visible = False
                    pnlEnrollFormat.Visible = False
                End If
            End If
        End If
    End Sub
    Private Sub radFormat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radFormat.SelectedIndexChanged

        Select Case radFormat.SelectedValue
            Case 1
                pnlEnrollFormat.Visible = False
                pnlSeqNumber.Visible = False
            Case 2
                pnlEnrollFormat.Visible = False
                pnlSeqNumber.Visible = True
            Case 3
                pnlEnrollFormat.Visible = True
                pnlSeqNumber.Visible = False
            Case Else
                pnlEnrollFormat.Visible = False
                pnlSeqNumber.Visible = False
        End Select
    End Sub
    Private Sub BuildDDL()
        Dim CampGrp As New CampusGroupsFacade
        With ddlCampusId
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            .DataSource = CampGrp.GetAllCampusEnrollment()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click

        Dim getStudent As New StudentMasterFacade
        Dim strStartingSeqNumber As Integer
        Dim strSeqNumber As Integer
        Dim strYearNumber As Integer
        Dim strMonthNumber As Integer
        Dim strDateNumber As Integer
        Dim strLNameNumber As Integer
        Dim strFNameNumber As Integer

        Dim strFormatType As String = radFormat.SelectedValue 'radFormat.SelectedItem.Text
        If txtYear.SelectedValue = 0 Then
            strYearNumber = 0
        Else
            strYearNumber = CInt(txtYear.SelectedValue)
        End If
        If txtMonth.SelectedValue = 0 Then
            strMonthNumber = 0
        Else
            strMonthNumber = CInt(txtMonth.SelectedValue)
        End If
        If txtDate.SelectedValue = 0 Then
            strDateNumber = 0
        Else
            strDateNumber = CInt(txtDate.SelectedValue)
        End If
        If txtLName.Text = "" Then
            strLNameNumber = 0
        Else
            strLNameNumber = CInt(txtLName.Text)
        End If
        If txtFName.Text = "" Then
            strFNameNumber = 0
        Else
            strFNameNumber = CInt(txtFName.Text)
        End If

        If txtSeq.Text = "" Then
            strSeqNumber = 0
        Else
            strSeqNumber = CInt(txtSeq.Text)
        End If
        If txtSeqNumber.Text = "" Then
            strStartingSeqNumber = 0
        Else
            strStartingSeqNumber = CInt(txtSeqNumber.Text)
        End If

        Dim StCount As Integer
        If strFormatType = 2 Then '"Generate Sequential Number(Only)"
            If strStartingSeqNumber = 0 Then
                DisplayErrorMessage("Please enter the Sequence starting number")
                Exit Sub
            End If
            strYearNumber = 0
            strMonthNumber = 0
            strDateNumber = 0
            strLNameNumber = 0
            strFNameNumber = 0
            strSeqNumber = 0
        ElseIf strFormatType = 3 Then
            StCount = strYearNumber + strMonthNumber + strDateNumber + strLNameNumber + strFNameNumber + strSeqNumber
            'If StCount > 10 Then
            '    DisplayErrorMessage("The combination for student id should not exceed 10 characters ")
            '    Exit Sub
            'End If
        End If

        Dim result As Integer
        result = getStudent.AddStudentIdFormat(strFormatType, strYearNumber, strMonthNumber, strDateNumber, strLNameNumber, strFNameNumber, strSeqNumber, strStartingSeqNumber, Session("UserName"))

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        btnnew.Enabled = False
        btndelete.Enabled = False
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
