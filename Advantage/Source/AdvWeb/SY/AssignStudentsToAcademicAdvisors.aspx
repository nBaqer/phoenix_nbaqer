<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="AssignStudentsToAcademicAdvisors.aspx.vb" Inherits="AssignStudentsToAcademicAdvisors" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table3">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table5">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="False"></asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button></td>

                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                            align="center">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <asp:Panel ID="pnlRHS" runat="server">
                                            <table cellspacing="0" cellpadding="0" width="40%" align="center" border="0">
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblAdvisorType" CssClass="labelbold" runat="server">Advisor Type</asp:Label></td>
                                                    <td class="contentcell4">
                                                        <asp:DropDownList ID="ddlAdvisorTypeId" CssClass="dropdownlist" runat="server" AutoPostBack="True">
                                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                                            <asp:ListItem Value="1">Academic Records</asp:ListItem>
                                                            <asp:ListItem Value="2">Financial Aid</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:CompareValidator ID="cvAdvisorTypeId" runat="server" ControlToValidate="ddlAdvisorTypeId" ValueToCompare="0"
                                                            Operator="NotEqual" Display="None" ErrorMessage="Must Select an Advisor Type">Must Select an Advisor Type</asp:CompareValidator></td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell">
                                                        <asp:Label ID="lblAcademicAdvisor" runat="server" CssClass="labelbold">Advisor</asp:Label></td>
                                                    <td class="contentcell4" style="padding-bottom: 20px">
                                                        <asp:DropDownList ID="ddlAcademicAdvisorId" runat="server" CssClass="dropdownlist" AutoPostBack="True"></asp:DropDownList>
                                                        <asp:CompareValidator ID="cvAcademicAdvisor" runat="server" ControlToValidate="ddlAcademicAdvisorId" ValueToCompare="00000000-0000-0000-0000-000000000000"
                                                            Operator="NotEqual" Display="None" ErrorMessage="Must Select an Advisor">Must Select an Advisor</asp:CompareValidator></td>
                                                </tr>
                                            </table>
                                            <table cellspacing="0" cellpadding="0" width="100%" border="0" align="center">
                                                <tr>
                                                    <td class="threecolumnheader1">
                                                        <asp:Label ID="lblAvailableStudents" runat="server" CssClass="labelbold" Font-Bold="True">Available Students</asp:Label></td>
                                                    <td class="threecolumnspacer1" nowrap></td>
                                                    <td class="threecolumnheader1">
                                                        <asp:Label ID="lblSelectedStudents" runat="server" CssClass="labelbold" Font-Bold="True">SelectedStudents</asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="threecolumncontent">
                                                        <asp:ListBox ID="lbxAvailableStudents" runat="server" CssClass="listboxes" SelectionMode="Multiple"
                                                            Rows="13" Width="100%"></asp:ListBox></td>
                                                    <td class="threecolumnbuttons">
                                                        <telerik:RadButton ID="btnAdd" runat="server" Text="Add >>" CssClass="buttons1" CausesValidation="False"
                                                            Width="100px">
                                                        </telerik:RadButton>
                                                        <br />
                                                        <telerik:RadButton ID="btnRemove" runat="server" Text="Remove <<" CssClass="buttons1" CausesValidation="False"
                                                            Width="100px">
                                                        </telerik:RadButton>
                                                        <br />
                                                    </td>
                                                    <td class="threecolumncontent">
                                                        <asp:ListBox ID="lbxSelectedStudents" runat="server" CssClass="listboxes" SelectionMode="Multiple"
                                                            Rows="13" Width="100%"></asp:ListBox></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>
