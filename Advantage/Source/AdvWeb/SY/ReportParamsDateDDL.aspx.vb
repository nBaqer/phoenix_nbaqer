
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Data
Imports FAME.AdvantageV1.DataAccess

Partial Class SY_ReportParamsDateDDL
    Inherits BasePage
    Protected WithEvents pnlSavedPrefs As System.Web.UI.WebControls.Panel
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblSkillGroups As System.Web.UI.WebControls.Label
    Protected WithEvents lblStudentSkills As System.Web.UI.WebControls.Label
    Protected WithEvents lbl3 As System.Web.UI.WebControls.Label
    Protected WithEvents Panel2 As System.Web.UI.WebControls.Panel
    Protected WithEvents tblRequiredFields As System.Web.UI.WebControls.Table
    Protected WithEvents pnlHideSort As System.Web.UI.WebControls.Panel
    Protected WithEvents pnlHideFilter As System.Web.UI.WebControls.Panel
    Protected PageTitle As String = "Report Parameters - "
    Protected resourceId As Integer
    Protected CampusId As String
    Dim userId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim objCommon As New FAME.common.CommonUtilities
        Dim rptFac As New ReportFacade
        Dim rptInfo As New ReportInfo
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade

        'Dim testStr As String = CType(Page.Master.FindControl("CampusSelector"), Telerik.Web.UI.RadComboBox).SelectedValue
        'Set the Delete button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        CampusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        Session("UserId") = userId

        txtRESID.Value = resourceId.ToString

        m_Context = HttpContext.Current
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = resourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, CampusId)

    If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        'Special cases: Very long reports. 
        'Set buttons so they prompt users for confirmation when resource is any of these: 
        '       SA -> Deferred Revenue Detail (resourceid = 489)
        '       SA -> Deferred Revenue Summary (resourceid = 490)
        If resourceId = 489 Or resourceId = 490 Or resourceId = 561 Then
            btnSearch.Attributes.Add("onclick", "if(confirm('This report may take several minutes to produce results. Do you want to proceed?')){}else{return false}")
            btnExport.Attributes.Add("onclick", "if(confirm('This report may take several minutes to produce results. Do you want to proceed?')){}else{return false}")
            btnFriendlyPrint.Attributes.Add("onclick", "if(confirm('This report may take several minutes to produce results. Do you still want to proceed?')){}else{return false}")
            btnFriendlyPrint.Enabled = False
        End If

        If pObj Is Nothing Then
            'Send the user to the standard error page
            Session("Error") = "You do not have the necessary permissions to access to this report."
            Response.Redirect("../ErrorPage.aspx")
        End If

        ' make sure the Session variable for Reporting Agency is cleared
        Session("RptAgency") = Nothing

        If Not Page.IsPostBack Then
            pnl4.Visible = False
            pnl5.Visible = False
            'Bind the DataList on the lhs to any preferences saved for this report.
            BindDataListFromDB()

            ViewState("resid") = Request.Params("resid")

            'Get the report info and store each piece in view state
            rptInfo = rptFac.GetReportInfo(CInt(ViewState("resid")))
            ViewState("SQLID") = rptInfo.SqlId
            ViewState("OBJID") = rptInfo.ObjId
            ViewState("RESURL") = rptInfo.Url
            ViewState("Resource") = rptInfo.Resource

            'Populate all the filter sections.
            BuildAllSections()

            'Set the mode to NEW
            'SetMode("NEW")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            'Set page title.
            PageTitle &= ViewState("Resource")
            'testStr = CType(Page.Master.FindControl("CampusSelector"), Telerik.Web.UI.RadComboBox).SelectedValue
            'Page.DataBind()

        Else
            'Rebuild the dynamic table and dynamic validators each time there is a postback.
            BuildFilterOther()
        End If
        'btnSave.Enabled = False
    End Sub

    Private Sub BuildAllSections()
        Dim dt2 As DataTable
        Dim dtMasks As New DataTable
        Dim dtOperators As New DataTable
        Dim objRptParams As New RptParamsFacade
        Dim objCommon As New Fame.common.CommonUtilities
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim numRows As Integer
        Dim numCols As Integer
        Dim rValues As Integer

        'Get the parameters for this report
        ds = objRptParams.GetRptParams

        'Set status of buttons and checkboxes.
        If ds.Tables.Count = 2 Then
            Dim row As DataRow = ds.Tables("RptProps").Rows(0)
            If Not row.IsNull("AllowParams") Then
                If Not row("AllowParams") Then
                    'Report has no params, then disable Save, New and Delete buttons.
                    btnAdd.Enabled = False
                    btnRemove.Enabled = False
                    btnSave.Enabled = False
                    btnNew.Enabled = False
                    btnDelete.Enabled = False
                    'Filters checkbox.
                    chkRptFilters.Enabled = False
                    'Sort checkbox.
                    chkRptSort.Enabled = False
                End If
            End If
            'Filter Criteria checkbox.
            If Not row.IsNull("AllowFilters") Then chkRptFilters.Enabled = row("AllowFilters")
            'Sort Order checkbox.
            If Not row.IsNull("AllowSortOrder") Then chkRptSort.Enabled = row("AllowSortOrder")
            'Selected Filter Criteria checkbox.
            If Not row.IsNull("SelectFilters") Then chkRptFilters.Checked = row("SelectFilters")
            'Selected Sort Order checkbox.
            If Not row.IsNull("SelectSortOrder") Then chkRptSort.Checked = row("SelectSortOrder")
            'Description checkbox.
            If Not row.IsNull("AllowDescrip") Then chkRptDescrip.Enabled = row("AllowDescrip")
            'Instructions checkbox.
            If Not row.IsNull("AllowInstruct") Then chkRptInstructions.Enabled = row("AllowInstruct")
            'Notes checkbox.
            If Not row.IsNull("AllowNotes") Then chkRptNotes.Enabled = row("AllowNotes")
            'Student Group checkbox.
            ''Added by Saraswathi lakshmanan on Sept 23 2009 to show Student Group Checkbox
            If Not row.IsNull("AllowStudentGroup") Then chkRptStudentGroup.Enabled = row("AllowStudentGroup")
        End If

        'Get the input masks
        dtMasks = objCommon.GetInstInputMasks.Copy
        dtMasks.TableName = "InputMasks"
        ds.Tables.Add(dtMasks)

        With ds.Tables("InputMasks")
            .PrimaryKey = New DataColumn() {.Columns("FldId")}
        End With

        'Get the comparison operators
        dtOperators = objCommon.GetComparisonOps.Copy
        dtOperators.TableName = "Operators"
        ds.Tables.Add(dtOperators)

        'Add a dt to store the possible values for each parameter that belongs to the
        'FilterList section. For example, we want to be able to store the possible values
        'for Program or Status.
        ds.Tables.Add("FilterListValues")

        With ds.Tables("FilterListValues")
            '.Columns.Add("RptParamId", GetType(Integer))
            '.Columns.Add("ValueFld", GetType(String))
            '.Columns.Add("DisplayFld", GetType(String))
            ''Modified by Saraswathi on Oct 06 2009

            .Columns.Add("RptParamId", GetType(Integer))
            .Columns.Add("ValueFld", GetType(String))
            .Columns.Add("DisplayFld", GetType(String))
            .Columns.Add("FldName", GetType(String))
            .Columns.Add("CampgrpId", GetType(String))

        End With

        'Add a dt to store the values selected for each parameter that belongs to the
        'FilterList section. For example, if the user selects Active and Attending for
        'Status we want to store these items.
        ds.Tables.Add("FilterListSelections")
        With ds.Tables("FilterListSelections")
            .Columns.Add("RptParamId", GetType(Integer))
            .Columns.Add("FldValue", GetType(String))
            ''Added by Saraswathi lakshmanan on Oct 06 2009

            .Columns.Add("FldName", GetType(String))
            .PrimaryKey = New DataColumn() {.Columns("RptParamId"), .Columns("FldValue")}
        End With

        'that it belongs to.
        For Each dr In ds.Tables("RptParams").Rows
            If dr("SortSec") = "True" Then
                'Add the field info to the lbxSort listbox
                lbxSort.Items.Add(New ListItem(dr("Caption"), dr("RptParamId")))
            End If

            If dr("FilterListSec") = "True" Then
                'Add the field info to the lbxFilterList listbox 
                lbxFilterList.Items.Add(New ListItem(dr("Caption"), dr("RptParamId")))
                'Get the list of possible values for this item
                dt2 = objRptParams.GetFilterListValues(dr("DDLId"), True, userId)

                numRows = dt2.Rows.Count
                numCols = dt2.Columns.Count

                For rValues = 0 To numRows - 1
                    'Create a new row for the FilterListValues dt
                    Dim dRow1 As DataRow = ds.Tables("FilterListValues").NewRow
                    'dRow1("RptParamId") = dr("RptParamId")
                    'dRow1("ValueFld") = dt2.Rows(rValues)(0)
                    'dRow1("DisplayFld") = dt2.Rows(rValues)(1)
                    ''Modified by Saraswathi lakshmanan on Oct 06 2009
                    dRow1("RptParamId") = dr("RptParamId")
                    dRow1("ValueFld") = dt2.Rows(rValues)(0)
                    dRow1("DisplayFld") = dt2.Rows(rValues)(1)
                    dRow1("FldName") = dr("FldName")
                    dRow1("CampGrpId") = dt2.Rows(rValues)(2)
                    'Add the new row to the FilterListValues dt
                    ds.Tables("FilterListValues").Rows.Add(dRow1)
                Next

                If dr("Required") = "True" Then
                    'Add a hidden textbox and a compare validator if this field is marked as 'Required'.
                    'This textbox need to be updated everytime it is selected or deselected.
                    Dim txt As New TextBox
                    Dim compVal As New CompareValidator

                    txt.ID = "txt" & dr("RptParamId").ToString()
                    txt.CssClass = "ToTheme2"
                    txt.Text = System.Guid.Empty.ToString
                    txt.Width = Unit.Pixel(0)
                    txt.Height = Unit.Pixel(0)
                    txt.BorderWidth = Unit.Pixel(0)

                    compVal.ID = "compVal" & dr("RptParamId").ToString()
                    compVal.ControlToValidate = txt.ID
                    compVal.[Operator] = ValidationCompareOperator.NotEqual
                    compVal.ValueToCompare = System.Guid.Empty.ToString
                    compVal.Type = ValidationDataType.String
                    compVal.ErrorMessage = dr("Caption") & " is a required filter."
                    compVal.Display = ValidatorDisplay.None

                    pnlRequiredFieldValidators.Controls.Add(txt)
                    pnlRequiredFieldValidators.Controls.Add(compVal)
                End If
            End If
            '
            If dr("FilterOtherSec") = "True" Then
                pnl4.Visible = True
                'Add a row to the tblOthers table. This is a server control.
                Dim r As New TableRow
                Dim c0 As New TableCell
                Dim c1 As New TableCell
                Dim c2 As New TableCell
                Dim c5 As New TableCell
                Dim lbl As New Label
                Dim ddl As New DropDownList
                Dim ddlDate As New DropDownList

                Dim txt As New TextBox

                Dim fldType As String = dr("FldType").ToString

                lbl.Text = dr("Caption")
                lbl.CssClass = "label"
                c0.Controls.Add(lbl)

                If (ViewState("resid") = 489 And dr("FldID") = 379) Or (ViewState("resid") = 561 And dr("FldID") = 379) Then

                    txt.ID = "txt" & dr("RptParamId").ToString()
                    txt.CssClass = "textbox"        '"ToTheme2"
                    txt.Width = Unit.Pixel(120)
                    If ViewState("resid") <> 489 Then 'DE8922
                        txt.Visible = False
                    End If
                    txt.ClientIDMode = UI.ClientIDMode.Static

                    ddl.ID = "ddl" & dr("RptParamId").ToString()
                    'Equal To (=) operator
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)


                ElseIf (ViewState("resid") = 489 And dr("FldId") = 660) Or _
                        (ViewState("resid") = 490 And dr("FldId") = 660) Or _
                        (ViewState("resid") = 561 And dr("FldId") = 660) Then


                    ddlDate.ID = "ddlDate" & dr("RptParamId").ToString()
                    ddlDate.DataTextField = "DefRevenueDateDescrip"
                    ddlDate.DataValueField = "DefRevenueDate"
                    ddlDate.DataSource = (New StudentsAccountsFacade).GetDeferredRevenueDates()
                    ddlDate.DataBind()
                    ddlDate.SelectedIndex = 0
                    ddlDate.CssClass = "dropdownlist"
                    ddlDate.Width = Unit.Pixel(100)

                    ddl.ID = "ddl" & dr("RptParamId").ToString()
                    'Special cases: Deferred Revenue Detail,
                    '               Deferred Revenue Summary
                    'These reports need a reference date; therefore, the operators drowdownlist can only contain one value.
                    'Equal To (=) operator
                    ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId = " & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
                End If


                ddl.DataTextField = "CompOpText"
                ddl.DataValueField = "CompOpId"
                ddl.DataBind()
                ddl.CssClass = "dropdownlist"
                ddl.AutoPostBack = True
                ddl.Width = Unit.Pixel(100)

                c1.Controls.Add(ddl)
                If (ViewState("resid") = 489 And dr("FldID") = 379) Or (ViewState("resid") = 561 And dr("FldID") = 379) Then
                    c2.Controls.Add(txt)
                Else
                    c2.Controls.Add(ddlDate)
                End If




                r.Cells.Add(c0)
                r.Cells.Add(c1)
                r.Cells.Add(c2)

                tblFilterOthers.Rows.Add(r)
            End If

            ' Show Required Filters panel.
            If dr("Required") = "True" Then
                pnl5.Visible = True
                If lblRequiredFilters.Text = "" Then
                    lblRequiredFilters.Text = dr("Caption")
                Else
                    lblRequiredFilters.Text &= ", " & dr("Caption")
                End If
            End If
        Next

        'Add a 'None' item so that users can deselect an item without having to
        'select another one.
        If lbxFilterList.Items.Count > 0 Then
            lbxFilterList.Items.Insert(0, New ListItem("None", 0))
        End If

        ''Added by saraswathi lakshmanan on may 07 2009
        ''Get the cmpGrpId which is mapped to 'All'
        ''Added to fix the issue 16041
        ''Added by Saraswathi To fix issue 19068
        Dim dtCampGrpIdforAll As DataTable
        dtCampGrpIdforAll = objCommon.GetCampGrpIdforAll.Copy
        dtCampGrpIdforAll.TableName = "CmpGrpIdAll"
        ds.Tables.Add(dtCampGrpIdforAll)
        'Add the ds DataSet to session so we can reuse it later on.
        Session("WorkingDS") = ds
    End Sub

    Private Sub BuildFilterOther()
        Dim resID As Integer
        Dim dt, dt2 As DataTable
        Dim dtMasks As DataTable
        Dim dr As DataRow
        Dim objRptParams As New RptParams
        Dim objCommon As New Fame.common.CommonUtilities

        resID = CInt(ViewState("resid"))
        'Get the parameters for this report that belong to the FilterOther section.
        dt = objRptParams.GetRptParamsForFilterOther(resID)
        'Get the comparison operators
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("Operators")
        'Get the input masks
        dtMasks = DirectCast(Session("WorkingDS"), DataSet).Tables("InputMasks")

        'Loop through the rows in the dt and add each parameter. 
        For Each dr In dt.Rows
            'Add a row to the tblOthers table. This is a server control.
            Dim r As New TableRow
            Dim c0 As New TableCell
            Dim c1 As New TableCell
            Dim c2 As New TableCell
            Dim c5 As New TableCell
            Dim lbl As New Label
            Dim ddl As New DropDownList
            Dim txt As New TextBox
            Dim txt2 As New TextBox
            Dim ddlDate As New DropDownList

            ddl.ID = "ddl" & dr("RptParamId").ToString()

            lbl.Text = dr("Caption")
            lbl.CssClass = "label"
            c0.Controls.Add(lbl)

            If (ViewState("resid") = 489 And dr("FldID") = 379) Or (ViewState("resid") = 561 And dr("FldID") = 379) Then

                txt.ID = "txt" & dr("RptParamId").ToString()
                txt.CssClass = "textbox"        '"ToTheme2"
                txt.Width = Unit.Pixel(120)
                txt.ClientIDMode = UI.ClientIDMode.Static
                ddl.ID = "ddl" & dr("RptParamId").ToString()
                'Equal To (=) operator
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)


            ElseIf (ViewState("resid") = 489 And dr("FldId") = 660) Or _
                (ViewState("resid") = 490 And dr("FldId") = 660) Or _
                (ViewState("resid") = 561 And dr("FldId") = 660) Then

                ddlDate.ID = "ddlDate" & dr("RptParamId").ToString()
                'ddlDate.DataTextField = "EndOfPeriodDate"
                'ddlDate.DataValueField = "EndOfPeriodDate"
                'ddlDate.DataSource = (New StudentsAccountsFacade).GetEndOfMonthDatesForPostingDeferredRevenue()
                ddlDate.DataTextField = "DefRevenueDateDescrip"
                ddlDate.DataValueField = "DefRevenueDate"
                ddlDate.DataSource = (New StudentsAccountsFacade).GetDeferredRevenueDates()
                ddlDate.DataBind()
                ddlDate.SelectedIndex = 0
                ddlDate.CssClass = "dropdownlist"
                ddlDate.Width = Unit.Pixel(100)

                'Special cases: Deferred Revenue Detail,
                '               Deferred Revenue Summary.
                'These reports need a reference date; therefore, the operators drowdownlist can only contain one value.
                'Equal To (=) operator
                ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
            End If

            ddl.DataTextField = "CompOpText"
            ddl.DataValueField = "CompOpId"
            ddl.DataBind()
            ddl.CssClass = "dropdownlist"
            ddl.Width = Unit.Pixel(100)
            ddl.AutoPostBack = True

            c1.Controls.Add(ddl)
            If (ViewState("resid") = 489 And dr("FldID") = 379) Or (ViewState("resid") = 561 And dr("FldID") = 379) Then
                c2.Controls.Add(txt)
            Else
                c2.Controls.Add(ddlDate)
            End If

            r.Cells.Add(c0)
            r.Cells.Add(c1)
            r.Cells.Add(c2)

            tblFilterOthers.Rows.Add(r)
        Next

        'Add a hidden textbox and a compare validator for fields marked as 'Required'.
        'This textbox need to be updated everytime it is selected or deselected.
        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        Dim rows() As DataRow = dt.Select("FilterListSec = 1 And Required = 1")

        For Each dr In rows
            Dim txt As New TextBox
            Dim compVal As New CompareValidator

            txt.ID = "txt" & dr("RptParamId").ToString()
            txt.CssClass = "textbox"
            txt.Text = System.Guid.Empty.ToString
            txt.Width = Unit.Pixel(0)
            txt.Height = Unit.Pixel(0)
            txt.BorderWidth = Unit.Pixel(0)

            compVal.ID = "compVal" & dr("RptParamId").ToString()
            compVal.ControlToValidate = txt.ID
            compVal.[Operator] = ValidationCompareOperator.NotEqual
            compVal.ValueToCompare = System.Guid.Empty.ToString
            compVal.Type = ValidationDataType.String
            compVal.ErrorMessage = dr("Caption") & " is a required filter."
            compVal.Display = ValidatorDisplay.None

            pnlRequiredFieldValidators.Controls.Add(txt)
            pnlRequiredFieldValidators.Controls.Add(compVal)
        Next
    End Sub

    Private Sub BindDataListFromDB()
        Dim dt As DataTable
        Dim objRptParams As New RptParamsFacade

        dt = objRptParams.GetSavedPrefsList

        With dlstPrefs
            .DataSource = dt
            .DataBind()
        End With

        'Cache the dt so we can reuse it when the links are clicked.
        'There is no need to have to go to the database to repopulate the datalist
        'when a different preference is selected.
        Cache("Prefs") = dt
    End Sub

    Private Sub BindDataListFromCache()
        Dim dt As DataTable
        dt = DirectCast(Cache("Prefs"), DataTable)
        With dlstPrefs
            .DataSource = dt
            .DataBind()
        End With
    End Sub


    Private Sub dlstPrefs_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstPrefs.ItemCommand
        Dim dt, dt2 As DataTable
        Dim dr, dr2 As DataRow
        Dim ds As New DataSet
        Dim dRows As DataRow()
        Dim found As Boolean
        Dim prefId As String
        Dim rptParamId As Integer
        Dim iCounter As Integer
        Dim objRptParams As New RptParamsFacade
        Dim objCommon As New Fame.common.CommonUtilities
        Dim ctlTextBox As Control
        Dim ctlDDL As Control

        dlstPrefs.SelectedIndex = e.Item.ItemIndex

        If Not IsNothing(Cache("Prefs")) Then
            BindDataListFromCache()
        Else
            BindDataListFromDB()
        End If

        'Clear the relevant sections
        Clear()

        'There is no need to bring the preference name from the database.
        txtPrefName.Text = DirectCast(e.CommandSource, LinkButton).Text

        'Get the DataSet containing the preferences
        prefId = dlstPrefs.DataKeys(e.Item.ItemIndex).ToString()
        ds = objRptParams.GetUserRptPrefs(prefId)

        'Set txtPrefId to the preference id of the selected item
        txtPrefId.Value = prefId
        'This section deals with displaying the Sort Preferences.
        dt = ds.Tables("SortPrefs")

        'Get the datatable from session that contains the details of each rptparam
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        With dt2
            .PrimaryKey = New DataColumn() {.Columns("RptParamId")}
        End With

        'We need to get the captions for the rows in the SortPrefs dt
        For Each dr In dt.Rows
            dr2 = dt2.Rows.Find(dr("RptParamId"))
            dr("Caption") = dr2("Caption")
        Next

        With lbxSelSort
            .DataSource = dt
            .DataTextField = "Caption"
            .DataValueField = "RptParamId"
            .DataBind()
        End With

        'When the page is first loaded we populate the lbxSort control with all the
        'sort parameters for the report. When a preference is selected we need to
        'make certain that the lbxSort control only displays the sort parameters that
        'are not already selected.
        If lbxSort.Items.Count > 0 Then
            lbxSort.Items.Clear()
        End If

        For Each dr In dt2.Rows
            If dr("SortSec") = True Then
                found = False
                For iCounter = 0 To lbxSelSort.Items.Count - 1
                    If lbxSelSort.Items(iCounter).Value = dr("RptParamId") Then
                        found = True
                    End If
                Next
                If Not found Then
                    lbxSort.Items.Add(New ListItem(dr("Caption"), dr("RptParamId")))
                End If
            End If
        Next

        'This section deals with storing and selecting the relevant FilterList prefs.
        'Set to empty GUID all hidden textboxes in pnlRequiredFieldValidators panel.
        For Each ctrl As Control In pnlRequiredFieldValidators.Controls
            If TypeOf ctrl Is TextBox Then
                DirectCast(ctrl, TextBox).Text = System.Guid.Empty.ToString
            End If
        Next

        dt = ds.Tables("FilterListPrefs")
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")

        For Each dr In dt.Rows
            dr2 = dt2.NewRow
            dr2("RptParamId") = dr("RptParamId")
            dr2("FldValue") = dr("FldValue")
            dt2.Rows.Add(dr2)
            '
            ctlTextBox = pnlRequiredFieldValidators.FindControl("txt" & dr("RptParamId").ToString)
            If Not (ctlTextBox Is Nothing) Then
                If DirectCast(ctlTextBox, TextBox).Text = System.Guid.Empty.ToString Then
                    DirectCast(ctlTextBox, TextBox).Text = dr("FldValue")
                Else
                    DirectCast(ctlTextBox, TextBox).Text &= "," & dr("FldValue")
                End If
            End If
        Next

        'If an item is selected in the lbxFilterList listbox then we need to select
        'the values, if any, that the user had selected for that item.
        If lbxFilterList.SelectedIndex <> -1 Then
            rptParamId = lbxFilterList.SelectedItem.Value
            'Search the FilterListSelections dt to see if the user had selected any
            'values for this item.
            dRows = dt2.Select("RptParamId = " & rptParamId)
            If dRows.Length > 0 Then
                For Each dr In dRows
                    'Loop through and select relevant item in the lbxSelFilterList control.
                    For iCounter = 0 To lbxSelFilterList.Items.Count - 1
                        If lbxSelFilterList.Items(iCounter).Value.ToString = dr("FldValue").ToString() Then
                            lbxSelFilterList.Items(iCounter).Selected = True
                        End If
                    Next
                Next
            End If
        End If

        'This section deals with the FilterOther prefs.
        dt = ds.Tables("FilterOtherPrefs")
        If dt.Rows.Count > 0 Then
            pnl4.Visible = True
            'For each row we need to select the appropriate entry in the ddl and enter the appropriate
            'value in the textbox.
            For Each dr4 As DataRow In dt.Rows
                ctlDDL = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("ddl" & dr4("RptParamId").ToString())
                objCommon.SelValInDDL(DirectCast(ctlDDL, DropDownList), dr4("OpId").ToString())
                ctlTextBox = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt" & dr4("RptParamId").ToString())
                If Not (ctlTextBox Is Nothing) Then
                    If dr4("OpId").ToString() = AdvantageCommonValues.BetweenOperatorValue.ToString Then
                        Dim strSplit() As String = dr4("OpValue").ToString.Split(";")
                        If strSplit.GetLength(0) > 0 Then
                            DirectCast(ctlTextBox, TextBox).Text = strSplit(0)
                            Dim ctlTextBox2 As Control = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt2" & dr4("RptParamId").ToString())
                            DirectCast(ctlTextBox2, TextBox).Text = strSplit(1)
                        End If
                    Else
                        DirectCast(ctlTextBox, TextBox).Text = dr4("OpValue")
                    End If
                Else
                    'Special case: DDLDate
                    ctlDDL = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("ddlDate" & dr4("RptParamId").ToString())
                    objCommon.SelValInDDL(DirectCast(ctlDDL, DropDownList), dr4("OpValue"))
                End If
            Next
        End If

        'Set Mode to EDIT
        'SetMode("EDIT")
        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
        ViewState("MODE") = "EDIT"

        '   set Style to Selected Item
        CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Value, ViewState)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'The code in this sub should only execute if an item is selected
        'in the lbxSort listbox.
        If lbxSort.SelectedIndex <> -1 Then
            lbxSelSort.Items.Add(New ListItem(lbxSort.SelectedItem.Text, lbxSort.SelectedItem.Value))
            lbxSort.Items.RemoveAt(lbxSort.SelectedIndex)
            If lbxSort.Items.Count > 0 Then
                lbxSort.SelectedIndex = lbxSort.SelectedIndex + 1
            End If
        End If
    End Sub
    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        'The code in this sub should only execute if an item is selected in 
        'the lbxSelSort listbox.
        If lbxSelSort.SelectedIndex <> -1 Then
            lbxSort.Items.Add(New ListItem(lbxSelSort.SelectedItem.Text, lbxSelSort.SelectedItem.Value))
            lbxSelSort.Items.RemoveAt(lbxSelSort.SelectedIndex)
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim strPromptMsg As String
        'strPromptMsg = "Please type name for Report Selection: "
        'If txtPrefName.Text = "" Then
        '    'Prompt user for Report Selection name.
        '    CommonWebUtilities.PromptTextBox(Me.Page, strPromptMsg, "Form1", Me.txtPrefName.ClientID)
        'Else
        '    'Prompt user for new name for current Report Selection.
        '    CommonWebUtilities.PromptDefaultValueTextBox(Me.Page, strPromptMsg, "Form1", Me.txtPrefName.ClientID)
        'End If
        SavePreferences()
    End Sub

    Private Function ValidateFilterOthers() As Boolean
        Dim selOpId As Integer
        Dim rCounter As Integer
        'im tbxText As String
        'Dim tbxText2 As String
        'Dim strItem As String
        'Dim arrValues() As String
        Dim selIndex As String
        Dim errorMessage As String = ""
        Dim opName As String
        Dim fieldName As String
        Dim fieldId As Integer
        Dim fldMask As String
        'Dim fldLen As Integer
        'Dim dType As String
        Dim rptParamId As Integer
        Dim objcommon As New Fame.common.CommonUtilities
        'We need to loop through the entries in the tblFilterOthers table.
        For rCounter = 1 To tblFilterOthers.Rows.Count - 1
            'tbxText = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).Text
            'tbxText2 = DirectCast(tblFilterOthers.Rows(rCounter).Cells(3).Controls(0), TextBox).Text
            'Ignore rows where nothing is entered into the textbox.
            'If tbxText <> "" Then
            'arrValues = tbxText.Split(";")
            selIndex = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedIndex
            opName = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text
            fieldName = DirectCast(tblFilterOthers.Rows(rCounter).Cells(0).Controls(0), Label).Text
            Dim StrDate As String = "ddlDate"
            Dim str As String = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), DropDownList).ID
            rptParamId = str.Substring(StrDate.Length, (str.Length - StrDate.Length))
            fieldId = GetFldId(rptParamId)
            fldMask = GetInputMask(fieldId)
            'fldLen = tbxText.Length

            If selIndex >= 0 Then   'Make sure an entry is selected in the ddl
                selOpId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                'If IsSingleValOp(selOpId) And arrValues.Length > 1 Then  'single value operator
                '    'There should only be one entry in the textbox. There should be
                '    'no semicolons (;) which is used to separate multiple values such as
                '    'when using the BETWEEN or the IN LIST operators.
                '    errorMessage &= "Operator " & opName & " cannot be used with multiple values." & vbCr
                'ElseIf opName = "Is Null" Then
                '    'When the operator selected is "Is Null" then there is no point going
                '    'any further since Is Null should not have a value specified.
                '    errorMessage &= "You cannot specify a value when using the Is Null operator." & vbCr
                '    'ElseIf opName = "Between" And (tbxText = "" Or tbxText2 = "") Then
                '    'Between must have exactly two values specified to be compared.
                '    ' errorMessage &= "You must specify exactly two values when using the Between operator." & vbCr
                'Else
                '    'It is okay to proceed with validating the entries.
                '    'Loop through the entries in the arrValues array and validate.
                '    For Each strItem In arrValues
                '        'Verify the datatype of each entry in the arrValues array. We need to get the FldType from
                '        'the RptParams dt stored in session.
                '        dType = GetFldType(rptParamId)
                '        If Not IsValidDataType(strItem, dType) Then
                '            errorMessage &= "Incorrect datatype entered for " & fieldName & vbCr
                '        Else
                '            'Verify the input mask if there is one.
                '            If fldMask <> "" Then
                '                If Not objcommon.ValidateInputMask(fldMask, strItem) Then
                '                    errorMessage &= "Incorrect format entered for " & fieldName & vbCr
                '                Else
                '                    'Verify the field length if the field type is
                '                    'Char or Varchar.
                '                    If dType = "Varchar" Or dType = "Char" Then
                '                        If fldLen > GetFldLen(rptParamId) Then
                '                            errorMessage &= fieldName & " cannot be more than " & GetFldLen(rptParamId).ToString & " characters" & vbCr
                '                        End If
                '                    End If
                '                End If  'Validate input mask
                '            End If  'Input mask exists
                '        End If  'Valid data type
                '    Next    'Loop through the items in the arrValues array
                'End If
            End If  'Entry is selected in the ddl
            'End If  'Textbox is not empty
        Next    'loop through the entries in the tblFilterOThers table
        If errorMessage = "" Then
            Return True
        Else
            'lblErrorMessage.Text = "Please review the following errors:" & errorMessage
            DisplayErrorMessage(errorMessage)
            Return False
        End If
    End Function
    Private Function ValidateRequiredFilters() As String
        Dim rCounter As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dtReq As DataTable
        Dim dr() As DataRow
        Dim rows() As DataRow
        Dim paramId As Integer
        Dim rptParamId As Integer
        Dim filterName As String
        Dim errorMessage As String = ""
        Dim tbxText As String
        Dim IsFilterOther As Boolean

        ds = DirectCast(Session("WorkingDS"), DataSet)
        dt = ds.Tables("FilterListSelections")
        dtReq = ds.Tables("RptParams")
        rows = dtReq.Select("Required = 1")

        'Loop through all Required filters from RptParams.
        For Each r As DataRow In rows
            paramId = r.Item("RptParamId")
            filterName = r.Item("Caption")
            IsFilterOther = r.Item("FilterOtherSec")
            'Check if RptParamId is in FilterListSelection.
            dr = dt.Select("RptParamId = " & paramId)
            'When RptParamId is NOT in FilterListSelection, build string stating that field is required.
            If dr.GetLength(0) = 0 Then
                If Not IsFilterOther Then
                    'Check if RptParamId is in lbxFilterList listbox.
                    For rCounter = 0 To lbxFilterList.Items.Count - 1
                        rptParamId = lbxFilterList.Items(rCounter).Value
                        If paramId = rptParamId Then
                            'errorMessage &= "<br>" & filterName & " is a required filter"
                            errorMessage &= vbCrLf & filterName & " is a required filter."
                        End If
                    Next
                Else
                    'Check if RptParamId is in tblFilterOthers table.
                    If (tblFilterOthers.Rows.Count - 1) = 0 Then
                        errorMessage &= "<br>" & filterName & " is a required filter"
                    Else
                        'We need to loop through the entries in the tblFilterOthers table.
                        For rCounter = 1 To tblFilterOthers.Rows.Count - 1
                            tbxText = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).Text
                            rptParamId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                            If paramId = rptParamId And tbxText = "" Then
                                'errorMessage &= "<br>" & filterName & " is a required filter"
                                errorMessage &= vbCrLf & filterName & " is a required filter."
                            End If
                        Next
                    End If
                End If
            End If
        Next
        ''If errorMessage = "" Then
        ''    Return True
        ''Else
        ''    lblErrorMessage.Text = "Please review the following errors:" & errorMessage
        ''    Return False
        ''End If
        Return errorMessage
    End Function

    Private Function IsSingleValOp(ByVal opId As Integer) As Boolean
        Select Case opId
            Case 5, 6
                Return False
            Case Else
                Return True
        End Select
    End Function

    Private Function HasSingleEntry(ByVal sVal As String) As Boolean
        'Search the string to see if it contains a semicolon.
        If sVal.IndexOf(";") >= 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function IsValidDataType(ByVal sVal As String, ByVal dataType As String) As Boolean
        Dim iResult As Integer
        Dim sResult As String
        Dim dResult As Decimal
        Dim fResult As Double
        Dim dtmResult As DateTime
        Dim cResult As Char

        Select Case dataType
            Case "Int"
                Try
                    iResult = Convert.ToInt32(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "Float"
                Try
                    fResult = Convert.ToDouble(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "Money"
                Try
                    dResult = Convert.ToDecimal(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "Char"
                Try
                    cResult = Convert.ToChar(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "Datetime"
                Try
                    dtmResult = Convert.ToDateTime(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "Varchar"
                Try
                    sResult = Convert.ToString(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "TinyInt"
                Try
                    iResult = Convert.ToInt32(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case Else
                Throw New System.Exception("Invalid Field Type: " & dataType)
        End Select
    End Function

    Private Function GetFldType(ByVal rptParamId As Integer) As String
        Dim dt As DataTable
        Dim drRows() As DataRow
        Dim fldType As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")

        drRows = dt.Select("RptParamId = " & rptParamId)
        fldType = drRows(0)("FldType")
        Return fldType
    End Function

    Private Function GetFldId(ByVal rptParamId As Integer) As Integer
        Dim dt As DataTable
        Dim dr As DataRow
        Dim fldId As Integer

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        With dt
            .PrimaryKey = New DataColumn() {.Columns("RptParamId")}
        End With
        dr = dt.Rows.Find(rptParamId)
        fldId = dr("FldId")
        Return fldId
    End Function

    Private Function GetFldName(ByVal rptParamId As Integer) As String
        Dim dt As DataTable
        Dim drRows() As DataRow
        Dim fldName As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        drRows = dt.Select("RptParamId = " & rptParamId)
        fldName = drRows(0)("FldName")
        Return fldName
    End Function

    Private Function GetTblName(ByVal rptParamId As Integer) As String
        Dim dt As DataTable
        Dim drRows() As DataRow
        Dim tblName As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        drRows = dt.Select("RptParamId = " & rptParamId)
        tblName = drRows(0)("TblName")
        Return tblName
    End Function

    Private Function GetInputMask(ByVal fldId As Integer) As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim sMask As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("InputMasks")
        dr = dt.Rows.Find(fldId)
        If dr Is Nothing Then
            sMask = ""
        Else
            sMask = dr("Mask")
        End If
        Return sMask
    End Function

    Private Function GetFldLen(ByVal rptParamId As Integer) As Integer
        Dim dt As DataTable
        Dim dr As DataRow
        Dim fldLen As Integer

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        dr = dt.Rows.Find(rptParamId)
        fldLen = dr("FldLen")
        Return fldLen
    End Function

    Private Sub lbxFilterList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxFilterList.SelectedIndexChanged
        ' ''When an item is selected we want to display the list of possible values for the item.
        ' ''We can retrieve the list of values for this item from the FilterListValues stored
        ' ''in the ds that is in session.
        ''Dim ds As New DataSet
        ''Dim dt As DataTable
        ''Dim dr As DataRow
        ''Dim aRows As DataRow()
        ''Dim lbxRows As Integer
        ''Dim arrRows As Integer

        ''ds = DirectCast(Session("WorkingDS"), DataSet)
        ''dt = ds.Tables("FilterListValues")

        ' ''If 'None' item was selected, we need to empty the FilterListSelections dt in session.
        ' ''Otherwise, we want to get a list of all the rows in dt that have the corresponding RptParamId
        ' ''that is selected in the lbxFilterList listbox.

        ''If lbxFilterList.SelectedItem.Value = 0 Then
        ''    'Clear the items from the lbxSelFilterList lisbox if there are any.
        ''    If lbxSelFilterList.Items.Count > 0 Then
        ''        lbxSelFilterList.Items.Clear()
        ''    End If
        ''    'If the FilterListSelections dt in session is not empty then we need to empty it.
        ''    dt = ds.Tables("FilterListSelections")
        ''    If dt.Rows.Count > 0 Then
        ''        dt.Rows.Clear() 'Empty dt table.
        ''    End If
        ''    'Set to empty GUID all textboxes in panel pnlRequiredFieldValidators.
        ''    For Each ctrl As Control In pnlRequiredFieldValidators.Controls
        ''        If TypeOf ctrl Is TextBox Then
        ''            DirectCast(ctrl, TextBox).Text = System.Guid.Empty.ToString
        ''        End If
        ''    Next

        ''Else
        ''    'We want to get a list of all the rows in dt that have the corresponding RptParamId
        ''    'that is selected in the lbxFilterList listbox.
        ''    aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value)

        ''    'Clear the items from the lbxSelFilterList lisbox if there are any.
        ''    If lbxSelFilterList.Items.Count > 0 Then
        ''        lbxSelFilterList.Items.Clear()
        ''    End If
        ''    'Add the rows from aRows to the lbxSelFilterList listbox.
        ''    For Each dr In aRows
        ''        lbxSelFilterList.Items.Add(New ListItem(dr("DisplayFld"), dr("ValueFld")))
        ''    Next
        ''    'Add a 'None' item so that users can deselect an item without having to
        ''    'select another one.
        ''    lbxSelFilterList.Items.Insert(0, "None")

        ''    'If the FilterListSelections dt in session is not empty then we need
        ''    'to see if it has any entries for the item selected here. If it does then
        ''    'we need to select the relevant entries in the lbxSelFilterList listbox.
        ''    dt = ds.Tables("FilterListSelections")
        ''    If dt.Rows.Count > 0 Then
        ''        aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value)
        ''        If aRows.Length > 0 Then
        ''            'Loop through the items in the lbxSelFilterList listbox and
        ''            'select the relevant items.
        ''            For arrRows = 0 To aRows.Length - 1
        ''                For lbxRows = 0 To lbxSelFilterList.Items.Count - 1
        ''                    If lbxSelFilterList.Items(lbxRows).Value.ToString = aRows(arrRows)("FldValue").ToString() Then
        ''                        lbxSelFilterList.Items(lbxRows).Selected = True
        ''                    End If
        ''                Next
        ''            Next
        ''        End If 'search did not return 0 rows
        ''    End If 'dt is not empty
        ''End If

        ''Modified by Saraswathi lakshmanan on oct 06 2009
        'When an item is selected we want to display the list of possible values for the item.
        'We can retrieve the list of values for this item from the FilterListValues stored
        'in the ds that is in session.
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim aRows As DataRow()
        Dim lbxRows As Integer
        Dim arrRows As Integer
        Dim allRows As DataRow()
        Dim cmpGrpSelectedRows As DataRow()
        Dim allCampgrpId As String
        Dim fldName As String
        Dim dsCampGroups As DataSet
        Dim objRptParams As New RptParamsFacade

        ViewState("boolShowErr") = True
        ds = DirectCast(Session("WorkingDS"), DataSet)
        dt = ds.Tables("FilterListValues")
        allRows = dt.Select("FldName='CampGrpId' and DisplayFld='All'")
        If allRows.Length = 0 Then
            ''Modified by saraswathi lakshmanan on May 07 2009
            ''If the selected user does not have permission to all the campus groups,  but has permission to the campus group assigned, then, the other filter lists are not populating the values assigned to all campus groups and assigned to a particular campus group
            ''They are listing only the other lists mapped to the selected campus, they are not listing the ones mapped to all.
            ''Added to fix issue 16041

            allCampgrpId = ds.Tables("CmpGrpIdAll").Rows(0)("CampGrpId").ToString
        Else
            allCampgrpId = allRows(0)("CampGrpId").ToString
        End If



        'If 'None' item was selected, we need to empty the FilterListSelections dt in session.
        'Otherwise, we want to get a list of all the rows in dt that have the corresponding RptParamId
        'that is selected in the lbxFilterList listbox.

        If lbxFilterList.SelectedItem.Value = 0 Then
            'Clear the items from the lbxSelFilterList lisbox if there are any.
            If lbxSelFilterList.Items.Count > 0 Then
                lbxSelFilterList.Items.Clear()
            End If
            'If the FilterListSelections dt in session is not empty then we need to empty it.
            dt = ds.Tables("FilterListSelections")
            If dt.Rows.Count > 0 Then
                dt.Rows.Clear() 'Empty dt table.
            End If
            'Set to empty GUID all textboxes in panel pnlRequiredFieldValidators.
            For Each ctrl As Control In pnlRequiredFieldValidators.Controls
                If TypeOf ctrl Is TextBox Then
                    DirectCast(ctrl, TextBox).Text = System.Guid.Empty.ToString
                End If
            Next

        Else

            'Only allow for single selection for Student GPA Report
            If resourceId = 606 And lbxFilterList.SelectedItem.Text = "Term" Then
                lbxSelFilterList.SelectionMode = ListSelectionMode.Single
            End If


            'We want to get a list of all the rows in dt that have the corresponding RptParamId
            'that is selected in the lbxFilterList listbox.
            aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value)
            If aRows.Length > 0 Then
                fldName = aRows(0)(3).ToString
                If fldName.ToLower <> "campgrpid" Then
                    cmpGrpSelectedRows = ds.Tables("FilterListSelections").Select("FldName='CampGrpId'")
                    If cmpGrpSelectedRows.Length > 0 Then


                        Dim campgrpParamid As Integer = CInt(cmpGrpSelectedRows(0)(0))
                        Dim ctlTextBox As Control = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt" & campgrpParamid)
                        Dim strCampGrpId As String = DirectCast(ctlTextBox, TextBox).Text
                        Dim strValue As String = " CampGrpId in('" & strCampGrpId.Replace(",", "','") & "') "
                        ''Added by Saraswathi lakshmanan to find the campus groups, the student has rights to
                        ''All the campus groups, where the selected campus group's Campus is available is found.
                        dsCampGroups = objRptParams.GetCampusgroups(strValue)

                        If ((strCampGrpId = allCampgrpId And Session("UserName") = "sa") Or (allCampgrpId = "" And Session("UserName") = "sa")) Then

                            'If strCampGrpId = allCampgrpId Then
                            aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value)
                        Else
                            ''Added by saraswathi lakshmanan
                            ''Added on May 11 2009
                            ''Fix for mantis case: 14745
                            ''When a campus group is selected, The campus available in htat campus group is found. 
                            ''And  the other campus groups which holds the same campus from the selected group is found.
                            ''Thus the entities whose, campus groups mapped to the selected campus groups are listed.
                            ''EG: Miami Campusgroup has Miami campus and Orlando Campus group has orlando campus.
                            ''Florida campus Group has MI and Or campus. Tampa Campus group has Tampa Campus
                            ''All campus group has MI, Or, Ta Campuses
                            ''When we select Florida Campus Group, Anything mapped to Florida, Miami, Orlando and All are listed 

                            If dsCampGroups.Tables.Count > 0 Then
                                If dsCampGroups.Tables(0).Rows.Count > 0 Then
                                    Dim strCmpGroupIDS As String = ""
                                    Dim i As Integer
                                    For i = 0 To dsCampGroups.Tables(0).Rows.Count - 1
                                        strCmpGroupIDS = strCmpGroupIDS + "'" + dsCampGroups.Tables(0).Rows(i)(0).ToString + "',"
                                    Next
                                    ''Remove the last comma
                                    strCmpGroupIDS = strCmpGroupIDS.Remove(strCmpGroupIDS.Length - 1, 1)

                                    aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value & " AND ( CampGrpId in(" + strCmpGroupIDS + "))")
                                Else
                                    DisplayErrorMessage("Please first select the campus group")
                                End If
                            Else
                                DisplayErrorMessage("Please first select the campus group")
                            End If


                        End If


                    Else
                        lbxFilterList.Items.FindByValue(lbxFilterList.SelectedValue).Selected = False
                        lbxFilterList.Items.FindByText("Campus Group").Selected = True
                        If lbxSelFilterList.Items.Count = 0 Then
                            aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value)
                            For Each dr In aRows
                                lbxSelFilterList.Items.Add(New ListItem(dr("DisplayFld"), dr("ValueFld")))
                            Next
                            lbxSelFilterList.Items.Insert(0, "None")
                        End If
                        DisplayErrorMessage("Please first select the campus group")
                        Exit Sub
                    End If
                End If
            End If
            'Clear the items from the lbxSelFilterList lisbox if there are any.
            If lbxSelFilterList.Items.Count > 0 Then
                lbxSelFilterList.Items.Clear()
            End If
            'Add the rows from aRows to the lbxSelFilterList listbox.

            'For Each dr In aRows
            '    lbxSelFilterList.Items.Add(New ListItem(dr("DisplayFld"), dr("ValueFld")))
            'Next

            For i As Integer = 0 To aRows.Length - 1
                Dim drRow As DataRow = aRows(i)
                If i = 0 Then
                    lbxSelFilterList.Items.Add(New ListItem(drRow("DisplayFld"), drRow("ValueFld")))
                Else
                    If aRows(i)("ValueFld").ToString <> aRows(i - 1)("ValueFld").ToString Then
                        lbxSelFilterList.Items.Add(New ListItem(drRow("DisplayFld"), drRow("ValueFld")))
                    End If
                End If

            Next


            'Add a 'None' item so that users can deselect an item without having to
            'select another one.
            lbxSelFilterList.Items.Insert(0, "None")

            'If the FilterListSelections dt in session is not empty then we need
            'to see if it has any entries for the item selected here. If it does then
            'we need to select the relevant entries in the lbxSelFilterList listbox.
            dt = ds.Tables("FilterListSelections")
            If dt.Rows.Count > 0 Then
                aRows = dt.Select("RptParamId = " & lbxFilterList.SelectedItem.Value)
                If aRows.Length > 0 Then
                    'Loop through the items in the lbxSelFilterList listbox and
                    'select the relevant items.
                    For arrRows = 0 To aRows.Length - 1
                        For lbxRows = 0 To lbxSelFilterList.Items.Count - 1
                            If lbxSelFilterList.Items(lbxRows).Value.ToString = aRows(arrRows)("FldValue").ToString() Then
                                lbxSelFilterList.Items(lbxRows).Selected = True
                            End If
                        Next
                    Next
                End If 'search did not return 0 rows
            End If 'dt is not empty
        End If


    End Sub

    Private Sub lbxSelFilterList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxSelFilterList.SelectedIndexChanged
        ' ''When this event is fired it is important to remember that it could be that
        ' ''the user has selected OR deselected a value. Also, this lisbox supports multiple
        ' ''selections so we cannot simply get the item selected or unselected.
        ' ''Each time this event fires we have to examine each item in the listbox. If the
        ' ''item is not selected then if it exists in the dt we will have to remove it. If
        ' ''the item is selected but not exists in the dt then we will have to add it.
        ''Dim i As Integer
        ''Dim ds As New DataSet
        ''Dim dt As DataTable
        ''Dim dt2 As DataTable
        ''Dim dr As DataRow
        ''Dim dr2 As DataRow
        ''Dim paramId As Integer
        ''Dim selValue As String
        ''Dim temp As String
        ''Dim ctlTextBox As Control
        ''Dim rows() As DataRow

        ''ds = DirectCast(Session("WorkingDS"), DataSet)
        ''dt = ds.Tables("FilterListSelections")
        ''dt2 = ds.Tables("RptParams")
        ''paramId = lbxFilterList.SelectedItem.Value

        ''For i = 0 To lbxSelFilterList.Items.Count - 1
        ''    selValue = lbxSelFilterList.Items(i).Value
        ''    If lbxSelFilterList.Items(i).Selected = False And i > 0 Then
        ''        If ItemExistsInFilterListSelections(i) Then
        ''            'We need to remove the item from the dt
        ''            Dim objCriteria() As Object = {paramId, selValue}
        ''            dr = dt.Rows.Find(objCriteria)
        ''            dt.Rows.Remove(dr)
        ''            'If field is 'Required', set hidden textbox to empty.
        ''            rows = dt2.Select("RptParamId = " & paramId & " AND Required = 1")
        ''            If rows.GetLength(0) = 1 Then
        ''                ctlTextBox = Form1.FindControl("txt" & paramId)
        ''                If DirectCast(ctlTextBox, TextBox).Text.IndexOf(",") = -1 Then
        ''                    'Only one entry.
        ''                    DirectCast(ctlTextBox, TextBox).Text = System.Guid.Empty.ToString
        ''                Else
        ''                    temp = DirectCast(ctlTextBox, TextBox).Text
        ''                    temp = temp.Replace(selValue, ",")
        ''                    If temp.StartsWith(",,") Then
        ''                        DirectCast(ctlTextBox, TextBox).Text = temp.Substring(2)
        ''                    ElseIf temp.EndsWith(",,") Then
        ''                        DirectCast(ctlTextBox, TextBox).Text = temp.Substring(0, temp.Length - 2)
        ''                    Else
        ''                        DirectCast(ctlTextBox, TextBox).Text = temp.Replace(",,,", ",")
        ''                    End If
        ''                End If
        ''            End If
        ''        End If
        ''    Else
        ''        If Not ItemExistsInFilterListSelections(i) And i > 0 Then
        ''            'We need to add the item to the dt
        ''            If lbxSelFilterList.Items(i).Text = "All" Then
        ''                lbxSelFilterList.ClearSelection()
        ''                lbxSelFilterList.SelectionMode = ListSelectionMode.Single
        ''                lbxSelFilterList.SelectedValue = selValue
        ''                txtAllCampGrps.Text = "All"
        ''            Else
        ''                lbxSelFilterList.SelectionMode = ListSelectionMode.Multiple
        ''                txtAllCampGrps.Text = ""
        ''            End If
        ''            'We need to add the item to the dt
        ''            dr = dt.NewRow
        ''            dr("RptParamId") = paramId      'lbxFilterList.SelectedItem.Value
        ''            dr("FldValue") = selValue       'lbxSelFilterList.Items(i).Value
        ''            dt.Rows.Add(dr)
        ''            'If field is 'Required', set hidden textbox to newly selected value.
        ''            rows = dt2.Select("RptParamId = " & paramId & " AND Required = 1")
        ''            If rows.GetLength(0) = 1 Then
        ''                ctlTextBox = Form1.FindControl("txt" & paramId)
        ''                If DirectCast(ctlTextBox, TextBox).Text = System.Guid.Empty.ToString Then
        ''                    DirectCast(ctlTextBox, TextBox).Text = selValue
        ''                Else
        ''                    DirectCast(ctlTextBox, TextBox).Text &= "," & selValue
        ''                End If
        ''            End If
        ''        End If
        ''    End If
        ''Next
        ''Session("WorkingDS") = ds

        ''Modified by Saraswathi lakshmanan on Oct 6 2009
        ''Campus group should be selected before selcting any othert filter 

        'When this event is fired it is important to remember that it could be that
        'the user has selected OR deselected a value. Also, this lisbox supports multiple
        'selections so we cannot simply get the item selected or unselected.
        'Each time this event fires we have to examine each item in the listbox. If the
        'item is not selected then if it exists in the dt we will have to remove it. If
        'the item is selected but not exists in the dt then we will have to add it.
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dt2 As DataTable
        Dim dr As DataRow
        Dim dr2 As DataRow()
        Dim fldName As String
        Dim paramId As Integer
        Dim selValue, selText As String
        Dim temp As String
        Dim ctlTextBox As Control
        Dim rows() As DataRow

        ds = DirectCast(Session("WorkingDS"), DataSet)
        dt = ds.Tables("FilterListSelections")
        dt2 = ds.Tables("RptParams")
        paramId = lbxFilterList.SelectedItem.Value
        fldName = dt2.Select("RptParamId = " & paramId)(0)("FldName")
        If fldName.ToLower = "campgrpid" Then
            dr2 = dt.Select("FldName <> 'CampGrpId'")
            For idx As Integer = 0 To dr2.Length - 1
                dt.Rows.Remove(dr2(idx))
            Next
        End If
        For i = 0 To lbxSelFilterList.Items.Count - 1
            selValue = lbxSelFilterList.Items(i).Value
            selText = lbxSelFilterList.Items(i).Text

            If lbxSelFilterList.Items(i).Selected = False And i > 0 Then
                If ItemExistsInFilterListSelections(i) Then
                    'We need to remove the item from the dt
                    Dim objCriteria() As Object = {paramId, selValue}
                    dr = dt.Rows.Find(objCriteria)
                    dt.Rows.Remove(dr)
                    'If field is 'Required', set hidden textbox to empty.
                    rows = dt2.Select("RptParamId = " & paramId & " AND Required = 1")
                    If rows.GetLength(0) = 1 Then
                        ctlTextBox = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt" & paramId)
                        If DirectCast(ctlTextBox, TextBox).Text.IndexOf(",") = -1 Then
                            'Only one entry.
                            DirectCast(ctlTextBox, TextBox).Text = System.Guid.Empty.ToString
                        Else
                            temp = DirectCast(ctlTextBox, TextBox).Text
                            temp = temp.Replace(selValue, ",")
                            If temp.StartsWith(",,") Then
                                DirectCast(ctlTextBox, TextBox).Text = temp.Substring(2)
                            ElseIf temp.EndsWith(",,") Then
                                DirectCast(ctlTextBox, TextBox).Text = temp.Substring(0, temp.Length - 2)
                            Else
                                DirectCast(ctlTextBox, TextBox).Text = temp.Replace(",,,", ",")
                            End If
                        End If
                    End If
                End If
            Else
                If Not ItemExistsInFilterListSelections(i) And i > 0 Then
                    'We need to add the item to the dt
                    If lbxSelFilterList.Items(i).Text = "All" Then
                        lbxSelFilterList.ClearSelection()
                        lbxSelFilterList.SelectionMode = ListSelectionMode.Single
                        lbxSelFilterList.SelectedValue = selValue
                    Else
                        lbxSelFilterList.SelectionMode = ListSelectionMode.Multiple
                    End If


                    dr = dt.NewRow
                    dr("RptParamId") = paramId      'lbxFilterList.SelectedItem.Value
                    dr("FldValue") = selValue       'lbxSelFilterList.Items(i).Value
                    dr("FldName") = fldName
                    dt.Rows.Add(dr)
                    'If field is 'Required', set hidden textbox to newly selected value.
                    rows = dt2.Select("RptParamId = " & paramId & " AND Required = 1")
                    If rows.GetLength(0) = 1 Then
                        ctlTextBox = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("txt" & paramId)
                        If DirectCast(ctlTextBox, TextBox).Text = System.Guid.Empty.ToString Then
                            DirectCast(ctlTextBox, TextBox).Text = selValue
                        Else
                            DirectCast(ctlTextBox, TextBox).Text &= "," & selValue
                        End If
                    End If
                End If
                ''Only allow for single selection for Student GPA Report
                'If resourceId = 606 And lbxFilterList.SelectedItem.Text = "Term" Then
                '    lbxSelFilterList.SelectionMode = ListSelectionMode.Single
                'End If
            End If
        Next



        Session("WorkingDS") = ds



    End Sub

    Private Function ItemExistsInFilterListSelections(ByVal itemIndex As Integer) As Boolean
        '  Dim i As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim paramId As Integer
        Dim selValue As String

        paramId = lbxFilterList.SelectedItem.Value
        selValue = lbxSelFilterList.Items(itemIndex).Value

        ds = DirectCast(Session("WorkingDS"), DataSet)
        dt = ds.Tables("FilterListSelections")

        If dt.Rows.Count = 0 Then
            Return False
        Else
            Dim objCriteria() As Object = {paramId, selValue}
            dr = dt.Rows.Find(objCriteria)
            If dr Is Nothing Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objCommon As New Fame.common.CommonUtilities
        Dim dt As DataTable
        Dim dr As DataRow

        Try
            Clear()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            'Place the prefid guid into the txtPrefId textbox so it can be reused
            'if we need to do an update later on.
            txtPrefId.Value = ""

            'We have to rebind the lbxSort control to the report parameters that are stored in session
            lbxSort.Items.Clear()
            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
            For Each dr In dt.Rows
                If dr("SortSec") = True Then
                    lbxSort.Items.Add(New ListItem(dr("Caption"), dr("RptParamId")))
                End If
            Next

            'Make sure no item is selected in the Preferences datalist.
            dlstPrefs.SelectedIndex = -1

            'Rebind the Preferences datalist from the database.
            BindDataListFromDB()

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub Clear()
        Dim dt As DataTable
        Dim trCounter As Integer
        Dim ctrlDDL As Control
        Dim objCommon As New Fame.common.CommonUtilities

        'Clear the txtPrefName textbox control.
        txtPrefName.Text = ""
        'Clear the lbxSelSort listbox control
        lbxSelSort.Items.Clear()
        'Make sure no item is selected in the lbxSort listbox.
        lbxSort.SelectedIndex = -1
        'Clear the entries from the FilterListSelections dt
        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")
        If dt.Rows.Count > 0 Then
            dt.Clear()
        End If
        'Make sure no entry is selected in the lbxSelFilterList listbox
        lbxSelFilterList.SelectedIndex = -1
        'Get operators table
        Dim dt2 As DataTable = DirectCast(Session("WorkingDS"), DataSet).Tables("Operators")
        'In the FilterOther section, set dropdownlists to index 0.
        For trCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
            ctrlDDL = DirectCast(tblFilterOthers.Rows(trCounter).Cells(1).Controls(0), DropDownList)
            objCommon.SelValInDDL(DirectCast(ctrlDDL, DropDownList), dt2.Rows(0)("CompOpId").ToString)
            'DirectCast(tblFilterOthers.Rows(trCounter).Cells(2).Controls(0), DropDownList).SelectedIndex = 0
            If (ViewState("resid") = 489 And trCounter > 1) Then 'error out during new button
                Dim txt As TextBox = CType(tblFilterOthers.Rows(trCounter).Cells(2).Controls(0), TextBox)
                txt.Text = ""
            Else
                Dim ddl As DropDownList = CType(tblFilterOthers.Rows(trCounter).Cells(2).Controls(0), DropDownList)
                If ddl.Items.Count > 0 Then ddl.SelectedIndex = 0
            End If
        Next
        'Set to empty GUID all hidden textboxes in pnlRequiredFieldValidators panel.
        For Each ctrl As Control In pnlRequiredFieldValidators.Controls
            If TypeOf ctrl Is TextBox Then
                DirectCast(ctrl, TextBox).Text = System.Guid.Empty.ToString
            End If
        Next
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objRptParams As New RptParamsFacade
        Dim objCommon As New Fame.common.CommonUtilities

        'Delete all the preference info from the datatbase
        objRptParams.DeleteUserPrefInfo(txtPrefId.Value)
        'Clear the rhs of the screen
        Clear()
        'Set mode to NEW
        'SetMode("NEW")
        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
        ViewState("MODE") = "NEW"
        'Place the prefid guid into the txtPrefId textbox so it can be reused
        'if we need to do an update later on.
        txtPrefId.Value = ""
        'Make sure no item is selected in the datalist.
        dlstPrefs.SelectedIndex = -1
        'Rebind the datalist from the database.
        BindDataListFromDB()
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        If Page.IsValid Then

            Dim ds As New DataSet
            ''Added by saraswathi Lakshmanan on march 16 2009
            ''Required filter doesn't check for mandatory fields.
            ''issue 15412 & 15413
            Dim datefield As String

            Dim dr As DataRow
            ds = Session("WorkingDS")
            ''----------
            ''''''''''''

            For Each dr In ds.Tables("RptParams").Rows
                If dr("FilterOtherSec") = True Then
                    If dr("Required") = True Then
                        If (ViewState("resid") = 489 And dr("FldId") = 660) Or _
                                      (ViewState("resid") = 561 And dr("FldId") = 660) Then
                            datefield = DirectCast(tblFilterOthers.Rows(1).Cells(2).Controls(0), DropDownList).SelectedValue
                            If datefield = "" Then
                                DisplayErrorMessage(dr("Caption") + " is a required filter")
                                Exit Sub
                            End If


                        End If
                    End If
                End If
            Next

            '''''''''''''
            '--------------

            Dim strErrMsg As String = BuildReportDataSet()

            If strErrMsg = "" Then
                'Since no error was found, proceed to display report in Report Viewer page.
                If Not (Session("ReportDS") Is Nothing) Then

                    '   Make sure to clear any previously loaded report docuemnt objects
                    Session("RptObject") = Nothing

                    '   Setup the properties of the new window
                    Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium
                    Dim name As String = "ReportViewer"
                    Dim url As String = "../SY/" + name + ".aspx"
                    CommonWebUtilities.OpenClientChildWindow(Page, url, name, winSettings)
                Else
                    'Alert user of no data matching selection.
                    strErrMsg = "There is no data matching selection."
                    DisplayErrorMessage(strErrMsg)
                End If
            Else
                'Alert user of error.
                DisplayErrorMessage(strErrMsg)
            End If
        Else

            If (ViewState("boolShowErr") IsNot Nothing) Then
                Dim strErrMsg As String = GetValidateErrMsg()
                DisplayErrorMessage(strErrMsg)
            End If
            ViewState("boolShowErr") = True
        End If
    End Sub

    Private Function getErrString() As String
        Dim rtn As String = String.Empty
        Dim compCtrl As CompareValidator
        Dim reqCtrl As RequiredFieldValidator
        For Each ctrl As Control In pnlRequiredFieldValidators.Controls
            If TypeOf ctrl Is CompareValidator Then
                compCtrl = DirectCast(ctrl, CompareValidator)

                If Not compCtrl.IsValid Then
                    rtn = rtn + compCtrl.ErrorMessage & vbLf
                End If
            ElseIf TypeOf ctrl Is RequiredFieldValidator Then
                reqCtrl = DirectCast(ctrl, RequiredFieldValidator)

                If Not reqCtrl.IsValid Then
                    rtn = rtn + reqCtrl.ErrorMessage & vbLf
                End If
            End If
        Next
        Return rtn
    End Function
    Private Function GetValidateErrMsg() As String
        Dim rtn As String = String.Empty
        Dim count As Integer = Page.Validators.Count
        Dim index As Integer
        For index = 0 To count - 1
            If (Not Page.Validators(index).IsValid) Then
                rtn = rtn + Page.Validators(index).ErrorMessage & vbLf
            End If
        Next
        Return rtn
    End Function
    Private Function BuildOrderBy(ByRef StrOClause As String) As String
        Dim i As Integer
        Dim oClause As String = ""
        Dim tempStringOClause As String = ""
        'Dim dt As DataTable
        Dim fldName As String = ""
        Dim tblName As String = ""
        Dim fldCaption As String = ""

        'Only execute if at least one sort param has been specified
        If lbxSelSort.Items.Count > 0 Then
            For i = 0 To lbxSelSort.Items.Count - 1
                tblName = GetTblName(lbxSelSort.Items(i).Value)
                fldName = GetFldName(lbxSelSort.Items(i).Value)
                fldCaption = lbxSelSort.Items(i).Text
                If i < lbxSelSort.Items.Count - 1 Then
                    If tblName <> "syDerived" Then
                        oClause &= tblName & "." & fldName & ","
                    Else
                        oClause &= fldName & ","
                    End If
                    tempStringOClause &= fldCaption & ", "
                Else
                    If tblName <> "syDerived" Then
                        oClause &= tblName & "." & fldName
                    Else
                        oClause &= fldName
                    End If
                    tempStringOClause &= fldCaption
                End If
            Next
        End If

        'oClause = "ORDER BY " & oClause
        StrOClause = tempStringOClause
        Return oClause
    End Function

    Private Function BuildFilterListClause(ByRef StrWClause As String) As String
        Dim dt As DataTable
        Dim dt2 As DataTable
        Dim i, j, k As Integer
        Dim drRows() As DataRow
        Dim rows() As DataRow
        Dim fldName As String = ""
        Dim tblName As String = ""
        Dim fldType As String = ""
        Dim fldCaption As String = ""
        Dim [Operator] As String = ""
        Dim filterValue As String = ""
        Dim tempWClause As String = ""
        Dim tempStringWClause As String = ""
        Dim stringWClause As String = ""
        Dim strTempOperator As String = ""
        Dim wClause As String = ""

        'We need to loop through the items in the lbxFilterList control and see
        'if the item has any entries in the FilterListSelections dt stored in session.
        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")
        'We need to get the DisplayFld of all selected items.
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListValues")

        If dt.Rows.Count > 0 Then
            For i = 0 To lbxFilterList.Items.Count - 1
                drRows = dt.Select("RptParamId = " & lbxFilterList.Items(i).Value)
                If drRows.Length > 0 Then 'Match found

                    tblName = GetTblName(lbxFilterList.Items(i).Value)
                    fldName = GetFldName(lbxFilterList.Items(i).Value)
                    fldType = GetFldType(lbxFilterList.Items(i).Value)
                    fldCaption = lbxFilterList.Items(i).Text

                    If drRows.Length > 1 Then
                        [Operator] = " IN "
                        strTempOperator = " In List "
                    Else
                        [Operator] = " = "
                        strTempOperator = " Equal To "
                    End If

                    For j = 0 To drRows.Length - 1
                        filterValue = BuildFilterValue(drRows(j)("FldValue").ToString(), fldType)
                        If j < drRows.Length - 1 Then
                            tempWClause &= filterValue & ","
                        Else
                            tempWClause &= filterValue
                        End If
                        'Get the DisplayFld
                        rows = dt2.Select("RptParamId = " & drRows(j)("RptParamId").ToString() & " AND " & _
                                            "ValueFld = '" & drRows(j)("FldValue").ToString() & "'")
                        If rows.GetLength(0) = 1 Then
                            If j < drRows.Length - 1 Then
                                tempStringWClause &= "'" & rows(0)("DisplayFld") & "', "
                            Else
                                If strTempOperator = " Equal To " Then
                                    tempStringWClause &= rows(0)("DisplayFld")
                                Else
                                    tempStringWClause &= "'" & rows(0)("DisplayFld") & "'"
                                End If
                            End If
                        End If
                    Next

                    If k > 0 Then
                        wClause &= " AND "
                        stringWClause &= " AND "
                    End If

                    'If [Operator] = " = " Then
                    '    wClause &= tblName & "." & fldName & [Operator] & tempWClause
                    '    stringWClause &= fldCaption & strTempOperator & tempStringWClause
                    'Else
                    '    wClause &= tblName & "." & fldName & [Operator] & "(" & tempWClause & ")"
                    '    stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                    'End If

                    If [Operator] = " = " Then
                        ' wClause &= tblName & "." & fldName & [Operator] & tempWClause
                        'stringWClause &= fldCaption & strTempOperator & tempStringWClause
                        If fldName.ToLower = "campgrpid" Then
                            'If Not txtAllCampGrps.Text.ToLower = "all" Then
                            Dim strCampGrp As String
                            [Operator] = " in "
                            'strCampGrp = " (select Distinct t1.CampGrpId from syCmpGrpCmps t1,syCmpGrpCmps t2 where t1.CampusId = t2.CampusId and t2.CampGrpId in "
                            strCampGrp = " (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in "
                            wClause &= tblName & "." & fldName & [Operator] & strCampGrp & "(" & tempWClause & "))"
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                            'End If
                        Else
                            [Operator] = " = "
                            wClause &= tblName & "." & fldName & [Operator] & tempWClause
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                        End If
                    Else
                        If fldName.ToLower = "campgrpid" Then
                            'If Not txtAllCampGrps.Text.ToLower = "all" Then
                            Dim strCampGrp As String
                            strCampGrp = " (select Distinct t1.CampGrpId from syCmpGrpCmps t1 where t1.CampGrpId in "
                            'strCampGrp = " (select Distinct t1.CampGrpId from syCmpGrpCmps t1,syCmpGrpCmps t2 where t1.CampusId = t2.CampusId and t2.CampGrpId in "
                            wClause &= tblName & "." & fldName & [Operator] & strCampGrp & "(" & tempWClause & "))"
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                            'End If
                        Else
                            wClause &= tblName & "." & fldName & [Operator] & "(" & tempWClause & ")"
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                        End If
                    End If


                    k += 1 'Match found so we need to increment the count of matches so far
                End If
                tempWClause = ""
                tempStringWClause = ""
            Next
        Else
            'There is no item form lbxFilterList control 
            'that has entries in the FilterListSelections dt stored in session.
        End If

        StrWClause = stringWClause
        Return wClause
    End Function

    Private Function BuildFilterOtherClause(ByRef StrWClause As String, ByRef StrErrorMsg As String) As String
        Dim fldName As String = ""
        Dim tblName As String = ""
        Dim fldValue As String = ""
        Dim fldType As String = ""
        Dim fldCaption As String = ""
        Dim sqlOperator As String = ""
        Dim rCounter As Integer
        Dim opId As Integer
        Dim rptParamId As Integer
        Dim filterValue As String = ""
        Dim tbxText As String = ""
        Dim tbxText2 As String = ""
        Dim selText As String = ""
        Dim wClause As String = ""
        Dim tempWClause As String = ""
        Dim inList As String = ""
        Dim strItem As String = ""
        Dim position As Integer
        Dim errMsg As String = ""
        Dim tempFilterValue As String = ""
        Dim tempFilterValue2 As String = ""
        'The following variables will hold the string representation of the Where Clause,
        'which will be shown in the report as part of the title.        
        Dim tempStringWClause As String = ""
        Dim stringWClause As String = ""

        rCounter = 1
        'We need to loop through the entries in the tblFilterOthers table.
        Do While (rCounter <= tblFilterOthers.Rows.Count - 1) And errMsg = ""


            If (ViewState("resid") = 489 And TypeOf tblFilterOthers.Rows(rCounter).Cells(2).Controls(0) Is TextBox) Or (ViewState("resid") = 561 And TypeOf tblFilterOthers.Rows(rCounter).Cells(2).Controls(0) Is TextBox) Then
                tbxText = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).Text
            Else
                tbxText = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), DropDownList).SelectedValue
            End If

            fldCaption = DirectCast(tblFilterOthers.Rows(rCounter).Cells(0).Controls(0), Label).Text

            opId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
            sqlOperator = GetSQLOperator(opId)

            ' rptParamId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), DropDownList).ID.Substring(7)  'ddlDate
            If (ViewState("resid") = 489 And TypeOf tblFilterOthers.Rows(rCounter).Cells(2).Controls(0) Is TextBox) Or (ViewState("resid") = 561 And TypeOf tblFilterOthers.Rows(rCounter).Cells(2).Controls(0) Is TextBox) Then
                rptParamId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)  'txtDate
            Else
                rptParamId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), DropDownList).ID.Substring(7)  'ddlDate
            End If
            tblName = GetTblName(rptParamId)
            fldName = GetFldName(rptParamId)
            fldType = GetFldType(rptParamId)

            selText = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text
            If tbxText <> "" Then
                If (ViewState("resid") = 489 And TypeOf tblFilterOthers.Rows(rCounter).Cells(2).Controls(0) Is TextBox) Or (ViewState("resid") = 561 And TypeOf tblFilterOthers.Rows(rCounter).Cells(2).Controls(0) Is TextBox) Then
                    If IsDate(tbxText) = False Then
                        errMsg = "Invalid " & fldCaption & " value to filter for."
                    Else
                        Dim MOnthendDAte As Date
                        If Not DirectCast(tblFilterOthers.Rows(1).Cells(2).Controls(0), DropDownList).SelectedValue = "" Then
                            MOnthendDAte = CDate(DirectCast(tblFilterOthers.Rows(1).Cells(2).Controls(0), DropDownList).SelectedValue)
                            If tbxText > MOnthendDAte Then
                                errMsg = "Transaction Starting from Date should be less than the Month Ending Value."
                            Else


                                tempWClause = " TransactionDate: " + tbxText
                                tempStringWClause = " TransactionDate: " + tbxText
                                wClause &= tempWClause & " AND "
                                stringWClause &= tempStringWClause & " AND "
                                inList = ""
                            End If
                        End If
                    End If

                Else
                    tempFilterValue = BuildFilterValue(tbxText, fldType, sqlOperator)
                    If tempFilterValue <> "" Then
                        tempStringWClause = Convert.ToDateTime(tbxText).Month & "/01/" & Convert.ToDateTime(tbxText).Year & " - " & Convert.ToDateTime(tbxText).ToShortDateString
                        If fldType = "Datetime" Then
                            If sqlOperator = " = " Then
                                sqlOperator = " BETWEEN "
                            End If
                        End If
                        tempWClause = fldName & sqlOperator & tempFilterValue
                    Else
                        errMsg = "Invalid " & fldCaption & " value to filter for."
                    End If
                    wClause &= tempWClause & " AND "
                    stringWClause &= tempStringWClause & " AND "
                    inList = ""
                End If
            End If

            rCounter += 1
        Loop 'loop through the entries in the tblFilterOThers table

        If errMsg = "" Then
            'No error was found.
            'If the operator was the In List then we will have an extra AND
            'so we need to remove it.
            'Modified by Michelle Rodriguez 10/25/2004.
            'If operator was the Empty and the user typed something in the textbox next to the operators' dropdown,
            'there is an extra AND at position 0 (zero) and we need to remove it.
            If wClause <> "" Then
                'Remove last occurrence of " AND".
                position = wClause.LastIndexOf(" AND")
                If position > 0 Then
                    wClause = wClause.Substring(0, position + 1)
                End If
                'Remove occurrence of " AND " at position 0 (zero).
                position = wClause.IndexOf(" AND ")
                If position = 0 Then
                    wClause = wClause.Substring(5)
                End If
            End If
            'Need to do the same with the string reprentation of the Where Clause.
            'Remove last occurrence of " AND" and the first occurrence of " AND " from the stringWhereClause.
            If stringWClause <> "" Then
                position = stringWClause.LastIndexOf(" AND ")
                If position > 0 Then
                    stringWClause = stringWClause.Substring(0, position + 1)
                End If
                position = stringWClause.IndexOf(" AND ")
                If position = 0 Then
                    stringWClause = stringWClause.Substring(0, position + 1)
                End If
            End If
        Else
            'If an error was found, clear variables.
            StrWClause = ""
            wClause = ""
        End If

        StrErrorMsg = errMsg
        StrWClause = stringWClause
        Return wClause
    End Function

    Private Function BuildFilterValue(ByVal filterValue As String, ByVal dataType As String, _
                                        Optional ByVal sqlOperator As String = "") As String
        ' Modified by Michelle Rodriguez on 09/26/2004 and 10/04/2004.
        ' Needed to include the time for filters that involved Datetime fields:
        ' - The " = " operator will behave like a " BETWEEN " and the filter value
        '   will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
        ' - The " <> " operator will behave like a " NOT BETWEEN " and the filter value
        '   will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
        ' - Also, the filter value for the " > " operator will be of format: mm/dd/yyyy 11:59:59 PM
        ' All other cases remain the same.       

        Dim returnValue As String
        Dim validValue As String

        validValue = ValidateValue(filterValue, dataType)
        If validValue <> "" Then
            Select Case dataType
                Case "Uniqueidentifier", "Char", "Varchar"
                    returnValue = "'" & validValue & "'"
                Case "Datetime"
                    If sqlOperator = " = " Or sqlOperator = " <> " Then
                        returnValue = "'" & validValue & " 12:00:00 AM' AND '" & validValue & " 11:59:59 PM'"
                    ElseIf sqlOperator = " > " Then
                        returnValue = "'" & validValue & " 11:59:59 PM'"
                    Else
                        returnValue = "'" & validValue & "'"
                    End If
                Case Else
                    Return filterValue
            End Select
        Else
            returnValue = validValue
        End If

        Return returnValue
    End Function

    'Method added by Michelle R. Rodriguez on 11/19/2004, as part of the error handler for the FilterOthers section.
    Private Function ValidateValue(ByVal FilterValue As String, ByVal dataType As String) As String
        Dim returnValue As String = ""

        Select Case dataType
            Case "Uniqueidentifier", "Char", "Varchar"
                If FilterValue.IndexOf("'") <> -1 Then
                    'Replace single quotes by double quotes.
                    returnValue = FilterValue.Replace("'", "''")
                Else
                    'Return FilterValue as it was.
                    returnValue = FilterValue
                End If

            Case "Datetime"
                Try
                    Dim dDate As DateTime = Date.Parse(FilterValue)
                    'SLQ Server does not accept year < 1753. Therefore, a limit has to be imposed.
                    If dDate < Fame.AdvantageV1.Common.AdvantageCommonValues.MinDate Or _
                            dDate > Fame.AdvantageV1.Common.AdvantageCommonValues.MaxDate Then
                        returnValue = ""
                    Else
                        returnValue = dDate.ToShortDateString
                    End If
                Catch
                    'Invalid date.
                    'Might be ArgumentException, FormatException or OverflowException
                    returnValue = ""
                End Try

            Case "Int"
                Try
                    Dim x As Integer = Integer.Parse(FilterValue)
                    returnValue = x
                Catch
                    'Invalid integer.
                    'Might be ArgumentException, FormatException or OverflowException
                    returnValue = ""
                End Try

            Case Else
                Return FilterValue
        End Select

        Return returnValue
    End Function

    Private Function GetSQLOperator(ByVal opId As Integer) As String
        Select Case opId
            Case 1
                Return " = "
            Case 2
                Return " <> "
            Case 3
                Return " > "
            Case 4
                Return " < "
            Case 5
                Return " IN "
            Case 6
                Return " BETWEEN "
            Case 7
                Return " LIKE "
            Case 8
                Return " = '' "
            Case 9
                Return " IS NULL "
            Case Else
                Throw New System.Exception("The following operator is not recognized:" & opId)
        End Select
    End Function


    ' This method is going to build the dataset for the report. Then the report may be displayed
    ' in web form, which may contain a Crystal Report Viewer or a DataGrid, 
    ' or may be exported to any of the available formats.
    Private Function BuildReportDataSet() As String
        Dim orderByClause As String = ""
        Dim stringOrderByClause As String = ""
        Dim filterListWhereClause As String = ""
        Dim stringWhereClause As String = ""
        Dim filterOtherWhereClause As String = ""
        Dim stringFilterOtherWClause As String = ""
        Dim strErrMsg As String = ""
        Dim rptfac As New ReportFacade
        Dim ds As New DataSet
        Dim rptParamInfo As New ReportParamInfo
        Dim objName As String
        Dim facAssembly As System.Reflection.Assembly

        'Build the Order By clause if there are any entries in the lbxSelSort control.
        If lbxSelSort.Items.Count > 0 Then
            orderByClause = BuildOrderBy(stringOrderByClause)
        End If

        'Build the filter list part of the Where clause.
        filterListWhereClause = BuildFilterListClause(stringWhereClause)

        'Build the filter other part of the Where clause.
        'If an error is found in the user input, we cannot proceed to display the report.
        filterOtherWhereClause = BuildFilterOtherClause(stringFilterOtherWClause, strErrMsg)

        'If no error is found, proceed to build report dataset.
        If strErrMsg = "" Then
            rptParamInfo.ResId = resourceId
            If CInt(ViewState("SQLID")) > 0 Then
                rptParamInfo.SqlId = CInt(ViewState("SQLID"))
                rptParamInfo.ObjId = 0
            Else
                rptParamInfo.SqlId = 0
                rptParamInfo.ObjId = CInt(ViewState("OBJID"))
            End If
            rptParamInfo.OrderBy = orderByClause
            rptParamInfo.OrderByString = stringOrderByClause
            rptParamInfo.FilterList = filterListWhereClause
            rptParamInfo.FilterListString = stringWhereClause
            rptParamInfo.FilterOther = filterOtherWhereClause
            rptParamInfo.FilterOtherString = stringFilterOtherWClause
            rptParamInfo.ShowFilters = chkRptFilters.Checked
            rptParamInfo.ShowSortBy = chkRptSort.Checked
            rptParamInfo.ShowRptDescription = chkRptDescrip.Checked
            rptParamInfo.ShowRptInstructions = chkRptInstructions.Checked
            rptParamInfo.ShowRptNotes = chkRptNotes.Checked
            ''Added by Saraswathi lakshmanan on Sept 23 2009 to show Student Group Checkbox
            rptParamInfo.ShowRptStudentGroup = chkRptStudentGroup.Checked

            If ViewState("Resource").Equals("Revenue Ratio") Then
                rptParamInfo.RptTitle = MyAdvAppSettings.AppSettings("RevenueRatio") & " Rule"
            Else
                rptParamInfo.RptTitle = ViewState("Resource")
            End If

            If Not (Session("SchoolLogo") Is Nothing) Then
                rptParamInfo.SchoolLogo = DirectCast(Session("SchoolLogo"), System.Byte())
            Else
                rptParamInfo.SchoolLogo = rptfac.GetSchoolLogo.Image
                Session("SchoolLogo") = rptParamInfo.SchoolLogo
            End If

            'If the SQLID is not 0 then we need to create the class that generates DataSet for simple report.
            'Else we need to use OBJID to get the name of the object and create object using reflection.
            If rptParamInfo.SqlId > 0 Then
                ds = rptfac.GetReportDataSet(rptParamInfo)
            Else
                'Get the name of the object associated with the OBJID.
                objName = rptfac.GetOBJName(rptParamInfo.ObjId)
                'Use reflection to create an instance of the object from the name.
                facAssembly = Reflection.Assembly.LoadFrom(Server.MapPath("..") + "\bin\BusinessFacade.dll")
                Dim objBaseRptFac As BaseReportFacade
                objBaseRptFac = DirectCast(facAssembly.CreateInstance("FAME.AdvantageV1.BusinessFacade." + objName), BaseReportFacade)
                ds = objBaseRptFac.GetReportDataSet(rptParamInfo)
            End If
            '
            If ds.Tables.Count <> 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Session("RptInfo") = rptParamInfo
                    Session("ReportDS") = ds
                Else
                    Session("RptInfo") = Nothing
                    Session("ReportDS") = Nothing
                    strErrMsg = "No records to display"
                End If
            End If
        Else
            'Errors where found while building WHERE or/and ORDER BY clause(s).
            Session("RptInfo") = Nothing
            Session("ReportDS") = Nothing

        End If 'strErrMsg = ""

        Return strErrMsg
    End Function

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        If Page.IsValid Then


            Dim ds As New DataSet
            ''Added by saraswathi Lakshmanan on march 16 2009
            ''Required filter doesn't check for mandatory fields.
            ''issue 15412 & 15413
            Dim datefield As String

            Dim dr As DataRow
            ds = Session("WorkingDS")
            ''----------
            ''''''''''''

            For Each dr In ds.Tables("RptParams").Rows
                If dr("FilterOtherSec") = True Then
                    If dr("Required") = True Then
                        If (ViewState("resid") = 489 And dr("FldId") = 660) Or _
                                      (ViewState("resid") = 561 And dr("FldId") = 660) Then
                            datefield = DirectCast(tblFilterOthers.Rows(1).Cells(2).Controls(0), DropDownList).SelectedValue
                            If datefield = "" Then
                                DisplayErrorMessage(dr("Caption") + " is a required filter")
                                Exit Sub
                            End If


                        End If
                    End If
                End If
            Next

            '''''''''''''
            '--------------

            Dim strErrMsg As String = BuildReportDataSet()

            If strErrMsg = "" Then
                'Since no error was found, proceed to display report in Export Viewer page.
                If Not (Session("ReportDS") Is Nothing) Then

                    '   Make sure to clear any previously loaded report docuemnt objects
                    Session("RptObject") = Nothing

                    '   Setup the properties of the new window
                    Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium + ",menubar=yes"
                    Dim name As String = "ReportExportViewer"
                    Dim url As String = "../SY/" + name + ".aspx?ExportTo=" & ddlExportTo.SelectedValue.ToString
                    CommonWebUtilities.OpenClientChildWindow(Page, url, name, winSettings)

                Else
                    'Alert user of no data matching selection.
                    strErrMsg = "There is no data matching selection."
                    DisplayErrorMessage(strErrMsg)
                End If
            Else
                'Alert user of error.
                DisplayErrorMessage(strErrMsg)
            End If
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False
        Customvalidator1.Visible = True

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInClientMessageBox(Me.Page, errorMessage)
        End If
    End Sub
    Private Sub btnFriendlyPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFriendlyPrint.Click
        If Page.IsValid Then
            Dim strErrMsg As String = BuildReportDataSet()

            If strErrMsg = "" Then
                'Since no error was found, proceed to display report in Export Viewer page.
                If Not (Session("ReportDS") Is Nothing) Then

                    '   Make sure to clear any previously loaded report docuemnt objects
                    Session("RptObject") = Nothing

                    '   Setup the properties of the new window
                    Dim winSettings As String = Fame.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium + ",menubar=yes"
                    Dim name As String = "ReportFriendlyPrint"
                    Dim url As String = "../SY/" + name + ".aspx"
                    CommonWebUtilities.OpenClientChildWindow(Page, url, name, winSettings)

                Else
                    'Alert user of no data matching selection.
                    strErrMsg = "There is no data matching selection."
                    DisplayErrorMessage(strErrMsg)
                End If
            Else
                'Alert user of error.
                DisplayErrorMessage(strErrMsg)
            End If
        Else

            If (ViewState("boolShowErr") IsNot Nothing) Then
                Dim strErrMsg As String = GetValidateErrMsg()
                DisplayErrorMessage(strErrMsg)
            End If
            ViewState("boolShowErr") = True
        End If
    End Sub

    Protected Sub lbtPrompt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtPrompt.Click
        Dim objRptParams As New RptParamsFacade
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim opId As Integer
        Dim fldValue As String
        Dim rptParamId As Integer
        Dim rowCounter As Integer
        Dim objCommon As New Fame.common.CommonUtilities
        Dim errMsg As String

        If txtPrefName.Text = "" Then
            'Alert user that a Preference name is required.
            errMsg = "Preference Name is required."
            'Display Error Message.
            DisplayErrorMessage(errMsg)
        Else
            'Before adding or updating we record we need to validate the entries
            'in the filter others section.
            'If ValidateFilterOthers() Then
            'If mode is NEW then we need to do an insert.
            If ViewState("MODE") = "NEW" Then
                Dim prefId As String
                'Generate a guid to be used as the prefid.
                prefId = Guid.NewGuid.ToString
                'Save the basic user preference information.
                objRptParams.AddUserPrefInfo(prefId, Session("UserName"), CInt(ViewState("resid")), txtPrefName.Text)

                'Save each sort preference in the lbxSort listbox. Use the order in the
                'listbox to generate the sequence.
                For i = 0 To lbxSelSort.Items.Count - 1
                    objRptParams.AddUserSortPrefs(prefId, lbxSelSort.Items(i).Value, i + 1)
                Next

                'If the FilterListSelections dt is not empty then we need to save the
                'FilterList preferences.
                ds = DirectCast(Session("WorkingDS"), DataSet)
                dt = ds.Tables("FilterListSelections")
                For Each dr In dt.Rows
                    objRptParams.AddUserFilterListPrefs(prefId, dr("RptParamId"), dr("FldValue").ToString())
                Next

                'We need to examine each row in the tblFilterOthers table control.
                For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
                    opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                    fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), DropDownList).SelectedValue
                    rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), DropDownList).ID.Substring(7)    'ddlDate
                    objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                Next

                'We need to rebind the DataList. We cannot use the cache here since we need
                'the latest info including the record just added. If the cache was used we
                'would not see the new record in the datalist.
                BindDataListFromDB()
                'Change mode to Edit
                'SetMode("EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"
                'Place the prefid guid into the txtPrefId textbox so it can be reused
                'if we need to do an update later on.
                txtPrefId.Value = prefId
                '   set Style to Selected Item
                CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Value, ViewState)

            Else
                'Update the basic user preference information
                objRptParams.UpdateUserPrefInfo(txtPrefId.Value, txtPrefName.Text)

                'Delete any previous sort info and then add the ones in the lbxSelSort listbox.
                objRptParams.DeleteUserSortPrefs(txtPrefId.Value)

                'Save each sort preference in the lbxSort listbox. Use the order in the
                'listbox to generate the sequence.
                For i = 0 To lbxSelSort.Items.Count - 1
                    objRptParams.AddUserSortPrefs(txtPrefId.Value, lbxSelSort.Items(i).Value, i + 1)
                Next

                'Delete any previous filter list info and then add the ones in the FilterListSelections dt.
                objRptParams.DeleteUserFilterListPrefs(txtPrefId.Value)

                'Save each filter list preference in the FilterListSelections dt.
                ds = DirectCast(Session("WorkingDS"), DataSet)
                dt = ds.Tables("FilterListSelections")
                For Each dr In dt.Rows
                    objRptParams.AddUserFilterListPrefs(txtPrefId.Value, dr("RptParamId"), dr("FldValue").ToString())
                Next

                'Delete any previous filter other info and then add the ones now specified.
                objRptParams.DeleteUserFilterOtherPrefs(txtPrefId.Value)

                For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty.
                    'If there is an entry in the textbox then get the values from the row.
                    opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                    fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), DropDownList).SelectedValue
                    rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), DropDownList).ID.Substring(7)    'ddlDate
                    objRptParams.AddUserFilterOtherPrefs(txtPrefId.Value, rptParamId, opId, fldValue)
                Next

                'We need to rebind the DataList in case txtPrefName had changed.
                BindDataListFromDB()
                '   set Style to Selected Item
                CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Value, ViewState)
            End If  'Test if mode is NEW or EDIT
            'End If 'ValidateFilterOThers returns True
        End If 'txtPrefName.Text is empty
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add buttons and listboxes
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnExport)
        controlsToIgnore.Add(btnFriendlyPrint)
        controlsToIgnore.Add(btnSearch)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnRemove)
        controlsToIgnore.Add(lbxFilterList)
        controlsToIgnore.Add(lbxSelFilterList)

        'Dim tbl As Table = DirectCast(FindControl("tblFilterOthers"), Table)
        Dim tbl As Table = Nothing
        If Not tblFilterOthers Is Nothing Then
            tbl = DirectCast(tblFilterOthers, Table)
        End If

        If Not (tbl Is Nothing) Then
            For i As Integer = 1 To tbl.Rows.Count - 1
                Dim ddl As DropDownList = DirectCast(tbl.Rows(i).Cells(1).Controls(0), DropDownList)

                If Not (ddl Is Nothing) Then

                    'add Operators ddl
                    controlsToIgnore.Add(ddl)

                    Dim rptParamId As Integer
                    rptParamId = Integer.Parse(ddl.ID.Substring(3))
                    Dim ctrl As Control = MyBase.FindControlRecursive("txt2" & rptParamId)
                    If Not (ctrl Is Nothing) Then
                        If ddl.SelectedValue = AdvantageCommonValues.BetweenOperatorValue Then
                            ctrl.Visible = True
                        Else
                            ctrl.Visible = False
                        End If
                    End If
                End If
            Next
        End If
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    Private Sub SavePreferences()
        Dim objRptParams As New RptParamsFacade
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim opId As Integer
        Dim fldValue As String
        Dim rptParamId As Integer
        Dim rowCounter As Integer ', rowcounter1
        '  Dim cellCounter As Integer
        Dim objCommon As New Fame.common.CommonUtilities
        Dim errMsg As String

        If txtPrefName.Text = "" Then
            'Alert user that a Preference name is required.
            errMsg = "Preference Name is required."
            'Display Error Message.
            DisplayErrorMessage(errMsg)
        Else
            'Before adding or updating we record we need to validate the entries
            'in the filter others section.
            If ValidateFilterOthers() Then
                'If mode is NEW then we need to do an insert.
                If ViewState("MODE") = "NEW" Then
                    Dim prefId As String
                    'Generate a guid to be used as the prefid
                    prefId = Guid.NewGuid.ToString
                    'Save the basic user preference information
                    objRptParams.AddUserPrefInfo(prefId, Session("UserName"), CInt(ViewState("resid")), txtPrefName.Text)

                    'Save each sort preference in the lbxSort listbox. Use the order in the
                    'listbox to generate the sequence.
                    For i = 0 To lbxSelSort.Items.Count - 1
                        objRptParams.AddUserSortPrefs(prefId, lbxSelSort.Items(i).Value, i + 1)
                    Next

                    'If the FilterListSelections dt is not empty then we need to save the
                    'FilterList preferences
                    ds = DirectCast(Session("WorkingDS"), DataSet)
                    dt = ds.Tables("FilterListSelections")
                    For Each dr In dt.Rows
                        objRptParams.AddUserFilterListPrefs(prefId, dr("RptParamId"), dr("FldValue").ToString())
                    Next

                    'We need to examine each row in the tblFilterOthers table control. If the
                    'user entered data in the textbox then we should save the info for that row.
                    For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
                        'If there is an entry in the textbox then get the values from the row
                        'If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), DropDownList).SelectedIndex Then
                        opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                        fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), DropDownList).SelectedValue
                        Dim str As String = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), DropDownList).ID
                        rptParamId = str.Substring(str.Length - 3, 3)
                        objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                        'End If
                    Next


                    'We need to rebind the DataList. We cannot use the cache here since we need
                    'the latest info including the record just added. If the cache was used we
                    'would not see the new record in the datalist.
                    BindDataListFromDB()
                    'Change mode to Edit
                    'SetMode("EDIT")
                    objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                    ViewState("MODE") = "EDIT"
                    'Place the prefid guid into the txtPrefId textbox so it can be reused
                    'if we need to do an update later on.
                    txtPrefId.Value = prefId
                Else
                    'Update the basic user preference information
                    objRptParams.UpdateUserPrefInfo(txtPrefId.Value, txtPrefName.Text)

                    'Delete any previous sort info and then add the ones in the lbxSelSort listbox.
                    objRptParams.DeleteUserSortPrefs(txtPrefId.Value)

                    'Save each sort preference in the lbxSort listbox. Use the order in the
                    'listbox to generate the sequence.
                    For i = 0 To lbxSelSort.Items.Count - 1
                        objRptParams.AddUserSortPrefs(txtPrefId.Value, lbxSelSort.Items(i).Value, i + 1)
                    Next

                    'Delete any previous filter list info and then add the ones in the FilterListSelections dt.
                    objRptParams.DeleteUserFilterListPrefs(txtPrefId.Value)

                    'Save each filter list preference in the FilterListSelections dt.
                    ds = DirectCast(Session("WorkingDS"), DataSet)
                    dt = ds.Tables("FilterListSelections")
                    For Each dr In dt.Rows
                        objRptParams.AddUserFilterListPrefs(txtPrefId.Value, dr("RptParamId"), dr("FldValue").ToString())
                    Next

                    'Delete any previous filter other info and then add the ones now specified
                    objRptParams.DeleteUserFilterOtherPrefs(txtPrefId.Value)
                    For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
                        'If there is an entry in the textbox then get the values from the row
                        'If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                        opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                        fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), DropDownList).SelectedValue
                        Dim str As String = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), DropDownList).ID
                        rptParamId = str.Substring(str.Length - 3, 3)
                        objRptParams.AddUserFilterOtherPrefs(txtPrefId.Value, rptParamId, opId, fldValue)
                        'End If
                    Next

                    'For rowcounter1 = 1 To tblStudentSearch.Rows.Count - 1 'The first row is empty
                    '    'If there is an entry in the textbox then get the values from the row
                    '    If txtStudentId.Text <> "" Then
                    '        opId = 1
                    '        fldValue = txtStudentIdentifier.Text
                    '        rptParamId = 182
                    '        objRptParams.AddUserFilterOtherPrefs(txtPrefId.Text, rptParamId, opId, fldValue)
                    '    End If
                    '    If txtStuEnrollment.Text <> "" Then
                    '        opId = 1
                    '        fldValue = txtStuEnrollmentId.Text
                    '        rptParamId = 182
                    '        objRptParams.AddUserFilterOtherPrefs(txtPrefId.Text, rptParamId, opId, fldValue)
                    '    End If
                    'Next

                End If  'Test if mode is NEW or EDIT
            End If 'ValidateFilterOThers returns True
            CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Value, ViewState)
        End If 'txtPrefName.Text is empty
    End Sub



End Class
