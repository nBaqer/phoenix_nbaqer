﻿<%@ Page Title="View System Data Dictionary" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SchlDataDictionary.aspx.vb" Inherits="SchlDataDictionary" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script language="javascript" src="../js/CheckAll.js" type="text/javascript"/>
   <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>


            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop2">Select a field</td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleft">
                            <asp:ListBox ID="lbxFields" Height="850px" Width="90%" CssClass="listbox" AutoPostBack="True" runat="server"></asp:ListBox>
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="98%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width:98%;" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <!-- begin content table-->
                            <h3>
                                <asp:Label ID="headerTitle" runat="server"></asp:Label>
                            </h3>
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                <tbody>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblColName" CssClass="label" runat="server" Text="Field Name"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtFldName" runat="server" CssClass="textbox" Enabled="False"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblType" CssClass="label" runat="server" Text="Type"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtFldType" runat="server" CssClass="textbox" Enabled="False"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="label1" CssClass="label" runat="server" Text="Length"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtFldLen" runat="server" CssClass="textbox" Enabled="False"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="label3" CssClass="label" runat="server" Text="Caption"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtCaption" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">&nbsp;
                                        </td>
                                        <td class="contentcell4">
                                            <asp:CheckBox ID="chkSchoolReq" runat="server" CssClass="checkboxinternational" Text="This is a required column"></asp:CheckBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:TextBox ID="txtCaptionIsInDB" CssClass="label" runat="server" Text="FALSE" Visible="False"></asp:TextBox>
                                        </td>
                                        <td class="contentcell4">&nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

