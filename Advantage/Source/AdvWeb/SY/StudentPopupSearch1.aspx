<%--<%@ Reference Page="~/SA/AcademicYears.aspx" %>--%>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="StudentPopupSearch1" CodeFile="StudentPopupSearch1.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Student Search</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link href="../css/localhost.css" type="text/css" rel="stylesheet" />
        <link href="../CSS/systememail.css" type="text/css" rel="stylesheet" />
		<script language="javascript" type="text/javascript">
		    function mngRequestStarted(ajaxManager, eventArgs) {
		        if (eventArgs.EventTarget == "dgrdTransactionSearch") {
		            eventArgs.EnableAjax = false;
		        }
		    }
     </script>
	</HEAD>
 
	<body>
         
<form id="Form1" method="post" runat="server"  defaultfocus="txtLastName">
<%--<fame:header id="Header1" visible="False" runat="server"></fame:header>--%>
<telerik:RadScriptManager ID="rsmMain" runat="server" AsyncPostBackTimeout="360000">
</telerik:RadScriptManager>
<telerik:RadAjaxManager ID="ramSearch" runat="server" EnableAJAX="true">
<AjaxSettings>
<telerik:AjaxSetting AjaxControlID="rpbAdvanceOption">
<UpdatedControls>
<telerik:AjaxUpdatedControl ControlID="rpbAdvanceOption" LoadingPanelID="ralpSearch"  />
</UpdatedControls>
</telerik:AjaxSetting>
<telerik:AjaxSetting AjaxControlID="btnSearch">
<UpdatedControls>
<telerik:AjaxUpdatedControl ControlID="rpbAdvanceOption" LoadingPanelID="ralpSearch"  />
<telerik:AjaxUpdatedControl ControlID="dgrdTransactionSearch" />
</UpdatedControls>
</telerik:AjaxSetting>
</AjaxSettings>
<ClientEvents OnRequestStart="mngRequestStarted" />
</telerik:RadAjaxManager>
<telerik:RadAjaxLoadingPanel ID="ralpSearch" runat="server" ></telerik:RadAjaxLoadingPanel>
<table cellspacing="0" cellpadding="0" width="100%" border="0">
    <tr>
    <td>
    <img src="../images/student_search.jpg"></td>
    <td class="topemail"><a class="close" onclick="top.close()" href=#>X Close</a></td>
    </tr>
</table>

<div class="scrollleftright" style="WIDTH: 95%;HEIGHT: expression(document.body.clientHeight - 80 + 'px')">
<table cellSpacing="0" cellPadding="0" width="99%" border="0">
<tr>
<td class="DetailsFrame" style="width:99%">
<table runat="server" id="tblMain" cellSpacing="0" cellPadding="0" width="99%" border="0">
<tr id="trMRUH" runat="server">
<td colspan="6" class="labelbold" style="text-align: center; padding: 4px 6px 4px 6px">
    <asp:Label ID="lbHdr" runat="server" class="labelbold"></asp:Label>   
</td>
</tr>
<tr id="trMRUGrid" runat="server">
<td colspan="6">
    <div class="scrollleftright" style="height:150px;width:99%">
    <table width="100%" cellpadding="0" cellspacing="0">
    <tr>
    <td align="center">
    <asp:datagrid id="dgrdTransactionSearchMRU" Width="99%" runat="server" BorderStyle="Solid" AutoGenerateColumns="False" BorderColor="#E0E0E0" BorderWidth="1px" ShowFooter="True">
    <EditItemStyle Wrap="False"></EditItemStyle>
    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
    <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
    <Columns>
    <asp:TemplateColumn HeaderText="Last Name">
    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
    <ItemStyle HorizontalAlign="Left"></ItemStyle>
    <ItemTemplate>
    <asp:LinkButton id="lnkLastName" runat="server" Text='<%# Container.DataItem("LastName") %>' CausesValidation="False" CommandName="StudentSearch" CommandArgument='<%# Container.DataItem("StudentId") %>'></asp:LinkButton></ItemTemplate>
    </asp:TemplateColumn>
    <asp:TemplateColumn HeaderText="First Name">
    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
    <ItemStyle HorizontalAlign="Left"></ItemStyle>
    <ItemTemplate>
    <asp:Label id="lblFirstName1" Text='<%# Container.DataItem("FirstName") %>' runat="server"></asp:Label>
    </ItemTemplate>
    </asp:TemplateColumn>
    <asp:TemplateColumn HeaderText="SSN">
    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
    <ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
    <ItemTemplate>
    <asp:Label id="Label1" Text='<%# Container.DataItem("SSN") %>' runat="server"  cssClass="label"></asp:Label>
    <asp:Label id="label2" Visible="False" Text='<%# Container.DataItem("StudentId") %>' runat="server" cssClass="label"></asp:Label>
    </ItemTemplate>
    </asp:TemplateColumn>
    <asp:TemplateColumn HeaderText="Enrollments">
    <ItemTemplate>
    <asp:DropDownList id="ddlEnrollmentsId" runat="server" CssClass="dropdownlistff"  AutoPostBack="true"  OnSelectedIndexChanged="ddqtyMRU_SelectedIndexChanged"></asp:DropDownList>
    </ItemTemplate>
    </asp:TemplateColumn>
    <asp:TemplateColumn HeaderText="Terms">
    <ItemTemplate>
    <asp:DropDownList id="ddlTermsId" runat="server" CssClass="dropdownlistff"></asp:DropDownList></ItemTemplate>
    </asp:TemplateColumn>
    <asp:TemplateColumn HeaderText="Academic Years">
    <ItemTemplate>
    <asp:DropDownList id="ddlAcademicYearsId" runat="server" CssClass="dropdownlistff"></asp:DropDownList></ItemTemplate>
    </asp:TemplateColumn>
    </Columns>
    </asp:datagrid>
    </td>
    </tr>
    </table>
    </div>
</td>
</tr>
<tr>
<td colspan="6" height="10px">&nbsp;</td>
</tr>
<tr>
<td class="detailsframe" style="width:99%">
    <telerik:RadPanelBar  runat="server" ID="rpbAdvanceOption" Width="99%" >
    <Items>
    <telerik:RadPanelItem  Expanded="false" Text="Advanced Options" runat="server">
    <Items>
    <telerik:RadPanelItem  Value="AdvanceOption" runat="server">
    <ItemTemplate>
    <table width="80%" align="center" border="0">
    <tr height="20px">
    <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
    <td colspan="6">
    <span class="labelBold">Please enter your search criteria, and then press the 'Search' button.</span>
    </td>
    </tr>
    <tr height="5px">
    <td colspan="6px"></td>
    </tr>
    <tr>
    <td><asp:label id="lblLastName" runat="Server" cssClass="label">Last Name</asp:label></td>
    <td><asp:textbox id="txtLastName" runat="Server" cssClass="textbox" Width="115px" tabIndex="1"></asp:textbox></td>
    <td><asp:label id="lblProgram" runat="Server" cssClass="label">Program</asp:label></td>
    <td><asp:dropdownlist id="ddlStudentPrgVerId" runat="Server" cssClass="dropdownlistff" Width="200px" tabIndex="3"></asp:dropdownlist></td>
    <td><asp:label id="lblSSN" runat="Server" cssClass="label">SSN</asp:label></td>
    <td><asp:textbox id="txtSSN" runat="Server" cssClass="textbox" Width="80px" tabIndex="5"></asp:textbox></td>
    </tr>
    <tr>
    <td><asp:label id="lblFirstName" runat="Server" cssClass="label">First Name</asp:label></td>
    <td><asp:textbox id="txtFirstName" runat="Server" cssClass="textbox" Width="115px" tabIndex="2"></asp:textbox></td>
    <td><asp:label id="lblStatus" runat="Server" cssClass="label">Status</asp:label></td>
    <td><asp:dropdownlist id="ddlStatusId" runat="Server" cssClass="dropdownlistff" tabIndex="4"></asp:dropdownlist></td>
    <td><asp:label id="lblEnrollment" runat="Server" cssClass="label">Enrollment #</asp:label></td>
    <td><asp:textbox id="txtEnrollment" runat="Server" cssClass="textbox" Width="80px" tabIndex="6"></asp:textbox></td>
    </tr>
    <tr>
    <td></td>
    <td></td>
    <td>
    <asp:dropdownlist id="ddlTermId" runat="server" Visible="False"></asp:dropdownlist>
    <asp:dropdownlist id="ddlAcademicYearId" runat="server" Visible="False"></asp:dropdownlist>
    </td>
    <td></td>
    <td></td>
    <td></td>
    </tr>
    <tr>
    <td colspan="8" align="center"><asp:button id="btnSearch" runat="Server" CssClass="button" width="60px" Text="Search" OnClick="btnSearch_Click"></asp:button></td>
    </tr>
    </table>
    </ItemTemplate>
    </telerik:RadPanelItem>
    </Items>
    </telerik:RadPanelItem>
    </Items>
    </telerik:RadPanelBar>
</td>
</tr>
<tr>
<td class="detailsframe" style="width:99%">
    <table width="99%">
    <tr vAlign="top">
    <td colspan="8" align="center">
    <asp:datagrid id="dgrdTransactionSearch" Width="99%" runat="server" BorderStyle="Solid" AutoGenerateColumns="False" BorderColor="#E0E0E0" BorderWidth="1px" ShowFooter="True">
    <EditItemStyle Wrap="False"></EditItemStyle>
    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
    <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
    <Columns>
    <asp:TemplateColumn HeaderText="Last Name">
    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
    <ItemStyle HorizontalAlign="Left"></ItemStyle>
    <ItemTemplate>
    <asp:LinkButton id="lnkLastName" runat="server" Text='<%# Container.DataItem("LastName") %>' CausesValidation="False" CommandName="StudentSearch" CommandArgument='<%# Container.DataItem("StudentId") %>'></asp:LinkButton>
    </ItemTemplate>
    </asp:TemplateColumn>
    <asp:TemplateColumn HeaderText="First Name">
    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
    <ItemStyle HorizontalAlign="Left"></ItemStyle>
    <ItemTemplate>
    <asp:Label id="lblFirstName1" Text='<%# Container.DataItem("FirstName") %>' runat="server"></asp:Label>
    </ItemTemplate>
    </asp:TemplateColumn>
    <asp:TemplateColumn HeaderText="SSN">
    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
    <ItemStyle Wrap="False" HorizontalAlign="Left"></ItemStyle>
    <ItemTemplate>
    <asp:Label id="Label1" Text='<%# Container.DataItem("SSN") %>' runat="server"></asp:Label>
    <asp:Label id="label2" Visible="False" Text='<%# Container.DataItem("StudentId") %>' runat="server"></asp:Label>
    </ItemTemplate>
    </asp:TemplateColumn>
    <asp:TemplateColumn HeaderText="Enrollments">
    <ItemTemplate>
    <asp:DropDownList id="ddlEnrollmentsId" runat="server" CssClass="dropdownlist"  AutoPostBack="true"  OnSelectedIndexChanged="ddqtyMRU_SelectedIndexChanged"></asp:DropDownList>
    </ItemTemplate>
    </asp:TemplateColumn>
    <asp:TemplateColumn HeaderText="Terms">
    <ItemTemplate>
    <asp:DropDownList id="ddlTermsId" runat="server" CssClass="dropdownlist"></asp:DropDownList>
    </ItemTemplate>
    </asp:TemplateColumn>
    <asp:TemplateColumn HeaderText="Academic Years">
    <ItemTemplate>
    <asp:DropDownList id="ddlAcademicYearsId" runat="server" CssClass="dropdownlist"></asp:DropDownList>
    </ItemTemplate>
    </asp:TemplateColumn>
    </Columns>
    </asp:datagrid>
    </td>
    </tr>
    </table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>

<%--<div id="footer" style="width:100%;margin:auto;color:white;font-size:x-small; text-align:center">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>--%>
</form>
         
	</body>
       
</HTML>