﻿Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports BO = Advantage.Business.Objects
Imports System.Web
Imports System.Web.UI.WebControls
Imports Telerik.Web.UI

Namespace AdvWeb.Sy
    Partial Class EditStatusHistory
        Inherits BasePage
        Private _campusId, _userId As String
        Private _resourceId As Integer
        Private _userPagePermissionInfo As New UserPagePermissionInfo

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            _resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            _campusId = Master.CurrentCampusId
            _userId = AdvantageSession.UserState.UserId.ToString

            Dim advantageUserState As New BO.User()
            advantageUserState = AdvantageSession.UserState

            _userPagePermissionInfo = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, _resourceId.ToString(), _campusId)

            If Not Page.IsPostBack Then
                Dim commonUtilities As New CommonUtilities
                commonUtilities.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW")

                ViewState("MODE") = "NEW"
            End If
            headerTitle.Text = Header.Title
        End Sub

        Protected Sub TransferToParent(ByVal enrollId As String, ByVal fullName As String, ByVal termId As String, _
                                   ByVal termdDescrip As String, ByVal academicYearId As String, ByVal academicYearDescrip As String, _
                                   ByVal hourType As String)


            hfEnrollmentId.Value = enrollId
            hfCampusId.Value = _campusId
            hfUserId.Value = _userId
            pnAddStatus.Visible = True
            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add("bindKendoControls();")
        End Sub

        Protected Sub btnUpdateSearchControl_OnClick(sender As Object, e As EventArgs)
            hfEnrollmentId.Value = StudSearch1.UpdateSearchControl()
            RadAjaxManager.GetCurrent(Page).ResponseScripts.Add("bindKendoControls();")
        End Sub
    End Class
End Namespace
