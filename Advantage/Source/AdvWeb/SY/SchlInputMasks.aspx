﻿<%@ Page Title="Setup Input Masks" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SchlInputMasks.aspx.vb" Inherits="Template_Container_OneColumn" %>
<%@ MasterType  virtualPath="~/NewSite.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Width="100%" Orientation="HorizontalTop">

            <table width="98%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                            Enabled="False"></asp:Button></td>
                </tr>
            </table>
            <!-- end top menu (save,new,reset,delete,history)-->
            <!--begin right column-->
            <table style="width: 98%;" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                align="center">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3>
                                <asp:Label ID="headerTitle" runat="server"></asp:Label>
                            </h3>

                            <asp:DataGrid ID="dgInputMasks" runat="server" CellPadding="5" align="center" BorderStyle="Solid"
                                AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true" EditItemStyle-Wrap="false"
                                GridLines="Horizontal" Width="50%">
                                <EditItemStyle Wrap="False"></EditItemStyle>
                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                <FooterStyle CssClass="label"></FooterStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="Item">
                                        <ItemTemplate>
                                            <asp:Label ID="lblInputMaskId" runat="server" Text='<%# Container.DataItem("InputMaskId")  %>'
                                                Visible="False" />
                                            <asp:Label ID="lblItem" runat="server" Text='<%# Container.DataItem("Item")  %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Mask">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtMask" runat="server" CssClass="textbox" Text='<%# Container.DataItem("Mask")  %>' />
                                            <asp:TextBox ID="txtModUser" runat="server" CssClass="textbox" Text='<%# Container.DataItem("ModUser")  %>'
                                                Width="0px" Visible="false" />
                                            <asp:TextBox ID="txtModDate" runat="server" CssClass="textbox" Text='<%# Ctype(Container.DataItem("ModDate"),Date).ToString  %>'
                                                Width="0px" Visible="false" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>



                        </div>
                    </td>
                </tr>
            </table>

            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>

