<%@ Page Language="vb" AutoEventWireup="false" Inherits="StudentContactAddresses" CodeFile="StudentContactAddresses.aspx.vb" %>
<%@ Reference Page="~/AD/Countries.aspx" %>
<%@ Reference Control="~/UserControls/Header.ascx" %>
<%@ Register TagPrefix="fame" TagName="footer" Src="~/UserControls/Footer.ascx" %>
<%@ Register TagPrefix="fame" TagName="header" Src="~/UserControls/Header.ascx" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Student Contact Addresses</title>
<%--		<LINK href="../CSS/localhost.css" type="text/css" rel="stylesheet">--%>
		<LINK href="../CSS/systememail.css" type="text/css" rel="stylesheet">
<%--        <link rel="stylesheet" type="text/css" href="../css/popup.css" />--%>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
	</HEAD>
	<body runat="server" leftMargin="0" topMargin="0" ID="Body1" NAME="Body1">
		<form id="Form1" method="post" runat="server">
			<!-- beging header -->
			<table cellSpacing="0" cellPadding="0" width="100%"  border="0">
				<tr>
					<td><IMG src="../images/advantage_contact_add.jpg"></td>
					<td class="topemail"><A class="close" onclick="top.close()" href=#>X Close</A></td>
				</tr>
			</table>
			<!-- end header -->
			<TABLE id="Table1" cellSpacing="0" cellPadding="0" width="100%"  height="100%" border="0">
				<TR>
					<TD valign="top" style="background-color: #E9EDF2;">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table2">
							<tr>
								<td class="listframe">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width="15%" nowrap align="left"><asp:Label ID="lblShow" Runat="server"><b class="label">Show</b></asp:Label></td>
											<td width="85%" nowrap>
												<asp:radiobuttonlist id="radStatus" CssClass="Label" AutoPostBack="true" Runat="Server" RepeatDirection="Horizontal">
													<asp:ListItem Text="Active" Selected="True" />
													<asp:ListItem Text="Inactive" />
													<asp:ListItem Text="All" />
												</asp:radiobuttonlist>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="listframebottom"><div class="scrollleftpopup"><asp:datalist id="dlstStudentContactAddresses" runat="server">
											<SelectedItemStyle CssClass="SelectedItem"></SelectedItemStyle>
											<ItemStyle CssClass="NonSelectedItem"></ItemStyle>
											<ItemTemplate>
												<asp:ImageButton id="imgInActive" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' ImageUrl="../images/Inactive.gif" CommandArgument='<%# Container.DataItem("StudentContactAddressId")%>' CausesValidation="False">
												</asp:ImageButton>
												<asp:ImageButton id="imgActive" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' ImageUrl="../images/Active.gif" CommandArgument='<%# Container.DataItem("StudentContactAddressId")%>' CausesValidation="False">
												</asp:ImageButton>
												<asp:LinkButton id="Linkbutton1" CssClass="NonSelectedItem" CausesValidation="False" Runat="server" CommandArgument='<%# Container.DataItem("StudentContactAddressId")%>' text='<%# Container.DataItem("Address1")%>'>
												</asp:LinkButton>
											</ItemTemplate>
										</asp:datalist></div>
								</td>
							</tr>
						</table>
					</TD>
					<td class="leftside">
						<table cellSpacing="0" cellPadding="0" width="9" border="0" ID="Table3">
						</table>
					</td>
					<td class="DetailsFrameTop">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<tr>
								<td class="MenuFrame" align="right">
									<asp:button id="btnSave" runat="server" Text="Save" CssClass="save"></asp:button>
									<asp:button id="btnNew" runat="server" Text="New" CssClass="new"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"></asp:button>
								</td>
							</tr>
						</table>
						<table width="100%"  border="0" cellpadding="0" cellspacing="0" ID="Table5">
							<tr>
								<td>
									<DIV>
									<TABLE width="60%" align="center">
											<asp:textbox id="txtStudentContactAddressId" runat="server" Visible="False"></asp:textbox>
											<asp:checkbox id="chkIsInDB" runat="server" Visible="False"></asp:checkbox>
											<asp:textbox id="txtStudentContactId" runat="server" Visible="False"></asp:textbox>
											<asp:textbox id="txtModUser" runat="server" Visible="False">ModUser</asp:textbox>
											<asp:textbox id="txtModDate" runat="server" Visible="False">ModDate</asp:textbox>
											<TR>
												<TD class="contentcell" noWrap>
													<asp:label id="lblStudentContactNameCaption" runat="server" cssclass="Label">Contact Name</asp:label></TD>
												<TD class="contentcell4">
													<asp:label id="lblStudentContactName" runat="server" cssclass="Label">Student Contact Name</asp:label></TD>
											</TR>
											<TR>
												<TD class="contentcell">
												</TD>
												<TD class="contentcell4">
													<asp:CheckBox id="chkForeignZip" tabIndex="1" Runat="server" AutoPostBack="true" text="International"
														cssClass="CheckBoxInternational"></asp:CheckBox>
												</TD>
											</TR>
											<tr>
												<td class="contentcell">
													<asp:Label id="lblAddress1" runat="server" CssClass="Label">Address 1</asp:Label>
												</td>
												<td class="contentcell4">
													<asp:textbox id="txtAddress1" runat="server" CssClass="textbox" TabIndex="2"></asp:textbox>
													<asp:RequiredFieldValidator id="rfvAddress1" runat="server" ErrorMessage="Address 1 can not be blank" Display="None"
														ControlToValidate="txtAddress1">Address 1 can not be blank</asp:RequiredFieldValidator></td>
											</tr>
											<TR>
												<TD class="contentcell" noWrap>
													<asp:Label id="lblAddress2" runat="server" CssClass="Label">Address 2</asp:Label></TD>
												<TD class="contentcell4">
													<asp:textbox id="txtAddress2" runat="server" CssClass="textbox" TabIndex="3"></asp:textbox></TD>
											</TR>
											<TR>
												<TD class="contentcell" noWrap>
													<asp:Label id="lblCity" runat="server" CssClass="Label"> City</asp:Label></TD>
												<TD class="contentcell4">
													<asp:textbox id="txtCity" runat="server" CssClass="textbox" TabIndex="4"></asp:textbox>
													<asp:RequiredFieldValidator id="rfvCity" runat="server" ErrorMessage="City can not be blank" Display="None"
														ControlToValidate="txtCity">City can not be blank</asp:RequiredFieldValidator></TD>
											</TR>
											<TR>
												<TD class="contentcell" noWrap>
													<asp:Label id="lblStateId" runat="server" CssClass="Label">State</asp:Label></TD>
												<TD class="contentcell4">
													<asp:DropDownList id="ddlStateId" runat="server" CssClass="DropDownListFF" TabIndex="5"></asp:DropDownList></TD>
											</TR>
											<TR>
												<TD class="contentcell" noWrap>
													<asp:label id="lblOtherState" Runat="server" CssClass="Label" Visible="False">Other State</asp:label></TD>
												<TD class="contentcell4" noWrap>
													<asp:textbox id="txtOtherState" Runat="server" CssClass="Textbox" Visible="False" AutoPostBack="true"
														TabIndex="6"></asp:textbox></TD>
											</TR>
											<TR>
												<TD class="contentcell" noWrap>
													<asp:Label id="lblZip" runat="server" CssClass="Label">Zip</asp:Label></TD>
												<TD class="contentcell4">
													<ew:maskedtextbox id="txtZip" runat="server" cssclass="TextBox" tabindex="7"></ew:maskedtextbox>&nbsp;
												</TD>
											</TR>
											<TR>
												<TD class="contentcell" noWrap>
													<asp:Label id="lblCountryId" runat="server" CssClass="Label">Country</asp:Label></TD>
												<TD class="contentcell4">
													<asp:DropDownList id="ddlCountryId" runat="server" cssclass="DropDownListFF" TabIndex="8"></asp:DropDownList></TD>
											</TR>
											<TR>
												<TD class="contentcell" noWrap>
													<asp:Label id="lblRelation" runat="server" CssClass="Label">Address Type</asp:Label></TD>
												<TD class="contentcell4">
													<asp:DropDownList id="ddlAddressTypeId" runat="server" cssclass="DropDownListFF" TabIndex="9"></asp:DropDownList></TD>
											</TR>
											<TR>
												<TD class="contentcell" noWrap><asp:label id="lblStatusId" CssClass="label" Runat="server">Status</asp:label></TD>
												<TD class="contentcell4"><asp:dropdownlist id="ddlStatusId" runat="server" cssclass="DropDownListFF" TabIndex="10"></asp:dropdownlist></TD>
											</TR>
										</TABLE>
									</DIV>
								</td>
							</tr>
						</table>
					</td>
				</TR>
			</TABLE>
			<asp:textbox id="txtPrgVerId" Runat="server" Visible="true"></asp:textbox><asp:textbox id="txtCampusId" Runat="server" Visible="true" MaxLength="128"></asp:textbox>
			<!-- start validation panel--><asp:panel id="Panel1" runat="server"></asp:panel><asp:customvalidator id="Customvalidator1" runat="server" Display="None" ErrorMessage="CustomValidator"></asp:customvalidator><asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:validationsummary>

		</form>
	</body>
</HTML>


