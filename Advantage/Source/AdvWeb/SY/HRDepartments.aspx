<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="HRDepartments.aspx.vb" Inherits="HRDepartments" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

    function OldPageResized(sender, args) {
        $telerik.repaintChildren(sender);
    }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstHRDepartments">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstHRDepartments" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstHRDepartments" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstHRDepartments" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstHRDepartments" runat="server" DataKeyField="HRDepartmentId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" runat="server" CausesValidation="False" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' CommandArgument='<%# Container.DataItem("HRDepartmentId")%>' ImageUrl="../images/Inactive.gif"></asp:ImageButton>
                                        <asp:ImageButton ID="imgActive" runat="server" CausesValidation="False" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' CommandArgument='<%# Container.DataItem("HRDepartmentId")%>' ImageUrl="../images/Active.gif"></asp:ImageButton>
                                        <asp:LinkButton ID="Linkbutton1" CssClass="itemstyle" CausesValidation="False" runat="server" CommandArgument='<%# Container.DataItem("HRDepartmentId")%>' Text='<%# Container.DataItem("HRDepartmentDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <table width="100%" align="center">
                                        <asp:TextBox ID="txtHRDepartmentId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                        <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                        <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                        <tr>
                                            <td class="twocolumnlabelcell" style="width: 30%">
                                                <asp:Label ID="lblHRDepartmentCode" runat="server" CssClass="label">Code</asp:Label></td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtHRDepartmentCode" runat="server" CssClass="textbox" MaxLength="12"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="revCode" runat="server" ControlToValidate="txtHRDepartmentCode" Display="None"
                                                    ErrorMessage="too many characters" ValidationExpression=".{0,12}"></asp:RegularExpressionValidator></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblStatusId" CssClass="label" runat="server">Status</asp:Label></td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblHRDepartmentDescrip" CssClass="label" runat="server">Description</asp:Label></td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtHRDepartmentDescrip" CssClass="textbox" MaxLength="50" runat="server" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="label" runat="server">Campus Group</asp:Label></td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList>
                                                <asp:CompareValidator ID="CampusGroupCompareValidator" runat="server" ErrorMessage="Must Select a Campus Group"
                                                    Display="None" ControlToValidate="ddlCampGrpId" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Campus Group</asp:CompareValidator>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--end table content-->
                                    <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="label" Width="10%"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="label" Width="10%"></asp:TextBox>
                                </div>
                                <!--end table content-->
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" ErrorMessage="CustomValidator"
            Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True"
            ShowSummary="False"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>

</asp:Content>


