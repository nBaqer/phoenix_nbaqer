﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentContacts.aspx.vb" Inherits="StudentContacts" EnableEventValidation="false" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <title>Student Contacts</title>
   <script type="text/javascript">
   </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
  <%--  <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstStudentContacts">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="OldContentSplitter"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstStudentContacts"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="OldContentSplitter"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstStudentContacts"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="OldContentSplitter"  />
                     <telerik:AjaxUpdatedControl ControlID="dlstStudentContacts"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings> 
    </telerik:RadAjaxManagerProxy>--%>


    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
     <%-- Add class ListFrameTop2 to the table below --%>   
     
           
                    <%--LEFT PANE CONTENT HERE--%>


<table cellSpacing="0" cellPadding="0" width="100%">
							<tr>
								<td class="listframetop">
									<table cellSpacing="0" cellPadding="0" width="100%" border="0">
										<tr>
											<td noWrap align="left" width="15%"><asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
											<td noWrap width="85%"><asp:radiobuttonlist id="radStatus" Runat="Server" RepeatDirection="Horizontal" AutoPostBack="true" CssClass="label">
													<asp:ListItem Text="Active" Selected="True" />
													<asp:ListItem Text="Inactive" />
													<asp:ListItem Text="All" />
												</asp:radiobuttonlist>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="listframebottom">
									<div class="scrollleftfilters"><asp:datalist id="dlstStudentContacts" runat="server">
											<SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
											<ItemStyle CssClass="itemstyle"></ItemStyle>
											<ItemTemplate>
												<asp:ImageButton id=imgInActive runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' ImageUrl="../images/Inactive.gif" CommandArgument='<%# Container.DataItem("StudentContactId")%>' CausesValidation="False">
												</asp:ImageButton>
												<asp:ImageButton id=imgActive runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' ImageUrl="../images/Active.gif" CommandArgument='<%# Container.DataItem("StudentContactId")%>' CausesValidation="False">
												</asp:ImageButton>
												<asp:LinkButton id=Linkbutton1 CssClass="itemstyle" CausesValidation="False" Runat="server" CommandArgument='<%# Container.DataItem("StudentContactId")%>' text='<%# Container.DataItem("ContactName")%>'>
												</asp:LinkButton>
												<asp:Label id=lblRelationShip runat="server" CssClass="itemstyle" Text='<%# "(" + Container.DataItem("Relation")+ ")" %>'>
												</asp:Label>
											</ItemTemplate>
										</asp:datalist></div>
								</td>
							</tr>
						</table>
    </telerik:RadPane>
    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                        ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                                    </asp:Button>
                                    </td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                       <tr>
                         <td class="detailsframe">
                          <div class="scrollright2">
                            <!-- begin content table-->

                            <%-- MAIN CONTENT HERE--%>
              
					        <!-- begin table content-->
					        <TABLE width="90%" align="center">
                                           
							        <asp:textbox id="txtStudentContactId" runat="server" Visible="False"></asp:textbox>
                                    <asp:checkbox id="chkIsInDB" runat="server" Visible="False"></asp:checkbox><asp:textbox id="txtStudentId" runat="server" Visible="False"></asp:textbox>
                                    <asp:textbox id="txtModUser" runat="server" Visible="False">ModUser</asp:textbox>
                                    <asp:textbox id="txtModDate" runat="server" Visible="False">ModDate</asp:textbox>
                                    <asp:textbox id="txtRowIds" style="VISIBILITY: hidden" runat="server" CssClass="label" width="10%"></asp:textbox>
                                    <asp:textbox id="txtResourceId" style="VISIBILITY: hidden" runat="server" CssClass="label" Width="10%"></asp:textbox>
                                    <asp:CompareValidator id="cvRelationShip" runat="server" ErrorMessage="You must select a Relationship"
								        ControlToValidate="ddlRelation" ValueToCompare="0" Operator="GreaterThan" Display="None"></asp:CompareValidator>
                                <TR>
							        <TD class="contentcell"><asp:label id="lblFirstName" runat="server" CssClass="label">First Name</asp:label></TD>
							        <TD class="contentcell4"><asp:textbox id="txtFirstName" runat="server" CssClass="textboxreq"></asp:textbox></TD>
							        <TD class="emptycell"></TD>
							        <TD class="contentcell"><asp:label id="lblRelation" runat="server" CssClass="label">Relationship</asp:label><font color="red">*</font></TD>
							        <TD class="contentcell4"><asp:dropdownlist id="ddlRelation" runat="server" CssClass="dropdownlist" ></asp:dropdownlist>
							        <asp:comparevalidator id="RelationCompareValidator" runat="server" ErrorMessage="Must Select a Relationship"
												        Display="None" ControlToValidate="ddlRelation" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Relationship</asp:comparevalidator></TD>
						        </TR>
						        <TR>
							        <TD class="contentcell" noWrap><asp:label id="lblMI" runat="server" CssClass="label">Middle Name</asp:label></TD>
							        <TD class="contentcell4"><asp:textbox id="txtMI" runat="server" CssClass="textbox"></asp:textbox></TD>
							        <TD class="emptycell"></TD>
							        <TD class="contentcell" noWrap><asp:label id="lblPrefixId" runat="server" CssClass="label">Prefix</asp:label></TD>
							        <TD class="contentcell4"><asp:dropdownlist id="ddlPrefixId" runat="server" CssClass="dropdownlist"></asp:dropdownlist></TD>
						        </TR>
						        <TR>
							        <TD class="contentcell" noWrap><asp:label id="lblLastName" runat="server" CssClass="label">Last Name</asp:label></TD>
							        <TD class="contentcell4"><asp:textbox id="txtLastName" runat="server" CssClass="textboxreq"></asp:textbox></TD>
							        <TD class="emptycell"></TD>
							        <TD class="contentcell" noWrap><asp:label id="lblSuffixId" runat="server" CssClass="label">Suffix</asp:label></TD>
							        <TD class="contentcell4"><asp:dropdownlist id="ddlSuffixId" runat="server" CssClass="dropdownlist"></asp:dropdownlist></TD>
						        </TR>
						        <TR>
							        <TD class="contentcell" noWrap><asp:label id="lblStatusId" Runat="server" CssClass="label">Status</asp:label></TD>
							        <TD class="contentcell4"><asp:dropdownlist id="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:dropdownlist></TD>
							        <TD class="emptycell"></TD>
							        <TD class="contentcell" noWrap>
                                        <asp:label id="lblEmailAddress" runat="server" CssClass="label">Email Address</asp:label></TD>
							        <TD class="contentcell4">
                                        <asp:textbox id="txtStudentContactHead" runat="server" Visible="False"></asp:textbox><asp:textbox id="txtEmailAddress" runat="server" CssClass="textbox"></asp:textbox></TD>
						        </TR>
						        <TR>
							        <TD class="contentcell" noWrap></TD>
							        <TD class="contentcell4" colSpan="4">
                                        <asp:regularexpressionvalidator id="revEmail" runat="server" ControlToValidate="txtEmailAddress" Display="None"
									        ErrorMessage="Invalid Email Address" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator></TD>
						        </TR>
						        <TR>
							        <TD class="contentcell" noWrap><asp:label id="lblComments" runat="server" CssClass="label">Comments</asp:label></TD>
							        <TD class="contentcell4" colSpan="4">
                                        <asp:textbox id="txtComments" runat="server" CssClass="tocommentsnowrap" TextMode="MultiLine"
									        Rows="6"></asp:textbox></TD>
						        </TR>
						        <TR>
							        <TD class="contentcell" noWrap></TD>
							        <TD colSpan="2">
								        <asp:LinkButton id="lbtSetUpAddresses" runat="server" CssClass="label" CausesValidation="False">Set Up Addresses for this Contact</asp:LinkButton></TD>
							        <TD colspan="4">
								        <asp:LinkButton id="lbtSetupPhones" runat="server" CssClass="label" CausesValidation="False">Set Up Phones for this Contact</asp:LinkButton>
							        </TD>
						        </TR>
					        </TABLE>

					        <asp:Panel ID="pnlUDFHeader" Runat="server" Visible="False">
						        <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%">
							        <TR>
								        <TD class="spacertables"></TD>
							        </TR>
							        <TR>
								        <TD class="contentcellheader" noWrap colSpan="6">
									        <asp:label id="lblSDF" Runat="server" cssclass="label" Font-Bold="true">School Defined Fields</asp:label></TD>
							        </TR>
						        </TABLE>
						        <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%">
							        <TR>
								        <TD class="spacertables"></TD>
							        </TR>
							        <TR>
								        <TD width="100%">
									        <asp:panel id="pnlSDF" tabIndex="12" Runat="server" Width="100%" enableviewstate="false"></asp:panel></TD>
							        </TR>
						        </TABLE>
					        </asp:Panel>
										        <!--end table content-->
						</div>
						 </td>
					   </tr>
					</table>
        <!-- end content table-->
       

    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

