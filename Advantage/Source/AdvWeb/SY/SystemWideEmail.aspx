<%@ Page Language="vb" AutoEventWireup="false" Inherits="SystemWideEmail" CodeFile="SystemWideEmail.aspx.vb" %>
<%@ Register TagPrefix="fame" TagName="EmailSelector" Src="~/UserControls/EmailSelector.ascx" %>
<%@ Register TagPrefix="fame" TagName="EmailEditor" Src="~/UserControls/EmailEditor.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>System Wide Email</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="../css/systememail.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body leftMargin="0" topMargin="0">
		<form id="Form1" method="post" runat="server">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" >
           
				<tr style="height:40px;background-color:#1976d2;text-align:center;">
					<td><asp:label ID="lblHeader" runat="server" Text="Mail" style=" font-family:Verdana; font-size:16px; color:White; text-align:center; font-weight:bolder;"></asp:label></td>
				</tr>
                <tr style="height:5px;">
					<td></td>
				</tr>
			</table>
			<!-- end header -->
			
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<!-- begin leftcolumn -->
					<td class="listframemail">
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td><fame:emailselector id="EmailSelector1" runat="server"></fame:emailselector>
								</td>
							</tr>
						</table>
					</td>
					<!-- end leftcolumn -->
					<!-- begin rightcolumn -->
					<td class="leftside">
						<table cellSpacing="0" cellPadding="0" width="9" border="0">
						</table>
					</td>
					<td class="DetailsFrameTopemail">
						<!-- end top menu (save,new,reset,delete,history)-->
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td class="detailsframe"><fame:emaileditor id="EmailEditor1" runat="server"></fame:emaileditor>
								</td>
							</tr>
						</table>
					</td>
					<!-- end rightcolumn -->
				</tr>
			</table>
			<!-- begin panel required field validators -->
			<asp:panel id="pnlRequiredFieldValidators" runat="server" CssClass="ValidationSummary">
				<asp:ValidationSummary id="ValidationSummary1" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
				<asp:CustomValidator id="CustomValidator1" Runat="server"></asp:CustomValidator>
			</asp:panel>
			<!-- end panel required field validators -->
			
		</form>
         <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
	</body>
</HTML>

