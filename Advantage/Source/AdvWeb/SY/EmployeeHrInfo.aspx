﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="EmployeeHRInfo.aspx.vb" Inherits="EmployeeHRInfo" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%;border:none">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->
                                        <asp:Panel ID="pnlRHS" runat="server">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                                <asp:CheckBox ID="cbxIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                                <asp:TextBox ID="txtEmpHRInfoId" runat="server" Visible="False"></asp:TextBox>
                                                <asp:TextBox ID="txtEmpId" runat="server" Visible="False"></asp:TextBox>
                                                <tr>
                                                    <td class="contentcell" style="width: 20%">
                                                        <asp:Label ID="lblHireDate" CssClass="label" runat="server">Hire Date</asp:Label>
                                                    </td>
                                                    <td class="contentcell4" style="text-align: left; width: 80%">

                                                        <telerik:RadDatePicker ID="txtHireDate" MinDate="1/1/1945" runat="server" Width="200px"></telerik:RadDatePicker>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell" style="width: 20%">
                                                        <asp:Label ID="lblPositionId" CssClass="label" runat="server">Position</asp:Label>
                                                    </td>
                                                    <td class="contentcell4" style="width: 80%">
                                                        <asp:DropDownList ID="ddlPositionId" runat="server" CssClass="dropdownlist" Width="200px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell" style="width: 20%">
                                                        <asp:Label ID="lblDepartmentId" CssClass="label" runat="server">Department</asp:Label>
                                                    </td>
                                                    <td class="contentcell4" style="width: 80%">
                                                        <asp:DropDownList ID="ddlDepartmentId" runat="server" CssClass="dropdownlist" Width="200px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell" style="width: 20%">
                                                        <asp:Label ID="lblDegreesId" CssClass="label" runat="server">Degree(s)</asp:Label>
                                                    </td>
                                                    <td class="contentcell4" style="width: 80%">
                                                        <asp:CheckBoxList ID="cblDegrees" runat="server" CssClass="textbox" RepeatColumns="3" Width="200px">
                                                        </asp:CheckBoxList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell" style="width: 20%">
                                                        <asp:Label ID="lblCertificationsId" runat="server" CssClass="label">Certification(s)</asp:Label>
                                                    </td>
                                                    <td class="contentcell4" style="width: 80%">
                                                        <asp:CheckBoxList ID="cblCertifications" runat="server" CssClass="textbox" RepeatColumns="3">
                                                        </asp:CheckBoxList>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="spacertables"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcellheader" nowrap colspan="6">
                                                            <asp:Label ID="lblSDF" runat="server" Font-Bold="true" CssClass="label">School Defined Fields</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="spacertables"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell2" colspan="6">
                                                            <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </asp:Panel>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

