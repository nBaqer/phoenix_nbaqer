﻿<%@ Page Title="WAPI CONFIGURATION MANAGER" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="WapiConfig.aspx.vb" Inherits="AdvWeb.SY.SyWapiConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <%--  <link href="../Kendo/styles/kendo.common.min.css" rel="stylesheet" />
    <link href="../Kendo/styles/kendo.blueopal.min.css" rel="stylesheet" />--%>
    <script src="../Scripts/Advantage.Client.SY.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <div id="WapiConfigContainer" style="height: 100%">
        <style type="text/css">
            .k-list-container .k-list .k-item {
                padding-right: 25px;
            }
        </style>
        <script id="WapiOperationSettingGrid_rowTemplate" type="text/x-kendo-tmpl">
			<tr data-uid="#= uid #" >
				<td style="visibility: hidden; width:1px">#: id #  </td>
				 <td>  
					 <p>
						<a class="k-button k-button-icontext k-grid-edit"  style="width:80px" href="\#"><span class="k-icon k-edit"></span>Edit</a>
					</p>
					<p>
						<a class="k-button k-button-icontext k-grid-delete"  style="width:80px" href="\#"><span class="k-icon k-delete"></span>Delete</a>
					</p>
					<%--<p>
						<a class="k-button k-button-icontext k-grid-refresh" style="width:80px"  href="\#"><span class="k-icon k-i-refresh"></span>Execute</a>
					</p>--%>
				   </td> 
				<td style="vertical-align: top" >
					<span class=description> #: CodeOperation# </span>
					<span class=description><b>Last Execution: </b> #= kendo.toString(kendo.parseDate(DateLastExecution), "g") #</span>
					<span class=description><b>Active: </b>#: IsActiveOperation # </span>
				</td>
				
				<td style="vertical-align: top">
						<span class="description"><b>Company Code: </b>#: CodeExtCompany#</span>	           
						<span class="description"><b>Description:  </b>#: DescripExtCompany#</span>
						<span class="description"><b>Operation Mode: </b>#: CodeExtOperationMode# </span>
						<span class="description"><b>Description:  </b>#: DescripExtOperationMode#</span>
				 </td>
				 <td style="vertical-align: top">
						<span class="description"><b>Consumer Key : </b>#: ConsumerKey#</span>	   
						<span class="description"><b>Private Key : </b>#: PrivateKey#</span>
                        <span class="description"><b>User Name : </b>#: UserName#</span>
						<span class="description"><b>External URL : </b>#: ExternalUrl#</span>		   
				 </td>
				 <td style="vertical-align: top">
						 <span class="description"><b>Proxy Service: </b>#: CodeWapiServ# </span>	   
						 <span class="description"><b>Description: </b>#: CodeWapiServ# </span>	
						 <span class="description"><b>Second Proxy Service: </b>#: SecondCodeWapiServ# </span>	
						 <span class="description"><b>Description: </b>#: SecondDescWapiServ# </span>
				  </td>	   
				  <td style="vertical-align: top">
						<span class="description"><b>Schedule Time: (Seconds) : </b>#: OperationSecInterval#</span>	   
						<span class="description"><b>On Demand Poll(Seconds) : </b>#: PollSecOnDemandOperation#</span>	   
						<span class="description"><b>Flag On Demand Status: </b> #: FlagOnDemandOperation# </span>
						<span class="description"><b>Flag Refresh Configuration: </b>  #: FlagRefreshConfig# </span>
				  </td>

			   </tr>
        </script>
        <script id="WapiOperationSettingGrid_altrowTemplate" type="text/x-kendo-tmpl">
				<tr class="k-alt"  data-uid="#= uid #" style="vertical-align: top">
				   <td style="visibility: hidden; width:1px">#: id #  
				   </td> 
				   <td>  
					 <p>
						<a class="k-button k-button-icontext k-grid-edit"  style="width:80px" href="\#"><span class="k-icon k-edit"></span>Edit</a>
					</p>
					<p>
						<a class="k-button k-button-icontext k-grid-delete"  style="width:80px" href="\#"><span class="k-icon k-delete"></span>Delete</a>
					</p>
				<%--	<p>
						<a class="k-button k-button-icontext k-grid-refresh" style="width:80px"  href="\#"><span class="k-icon k-i-refresh"></span>Execute</a>
					</p>--%>
				   </td> 
				  <td style="vertical-align: top" >
					<span class=description> #: CodeOperation# </span>
					<span class=description><b>Last Execution: </b> #= kendo.toString(kendo.parseDate(DateLastExecution), "g") # </span>
					<span class=description><b>Active: </b>#: IsActiveOperation # </span>

				</td>
				  <td style="vertical-align: top">
						<span class="description"><b>Company Code: </b>#: CodeExtCompany#</span>	           
						<span class="description"><b>Description:  </b>#: DescripExtCompany#</span>
						<span class="description"><b>Operation Mode: </b>#: CodeExtOperationMode# </span>
						<span class="description"><b>Description:  </b>#: DescripExtOperationMode#</span>
				   </td>
				 <td style="vertical-align: top">
						<span class="description"><b>Consumer Key : </b>#: ConsumerKey#</span>	   
						<span class="description"><b>Private Key : </b>#: PrivateKey#</span>
                        <span class="description"><b>User Name : </b>#: UserName#</span>
						<span class="description"><b>External URL : </b>#: ExternalUrl#</span>   
				 </td>
				 <td style="vertical-align: top">
						 <span class="description"><b>Proxy Service: </b>#: CodeWapiServ# </span>	   
						 <span class="description"><b>Description: </b>#: CodeWapiServ# </span>	
						 <span class="description"><b>Second Proxy Service: </b>#: SecondCodeWapiServ# </span>	
						 <span class="description"><b>Description: </b>#: SecondDescWapiServ# </span>  
				  </td>
				   <td style="vertical-align: top">
						<span class="description"><b>Schedule Time (Seconds) : </b>#: OperationSecInterval#</span>	   
						<span class="description"><b>On Demand Poll(Seconds) : </b>#: PollSecOnDemandOperation#</span>	   
						<span class="description"><b>Flag On Demand Status: </b> #: FlagOnDemandOperation# </span>
						<span class="description"><b>Flag Refresh Configuration: </b>  #: FlagRefreshConfig# </span>
				  </td>
			   </tr>
        </script>

        <!-- INIT TAB -->
        <div class="boxContainer" style="height: 100%; overflow: auto;">
            <h3>
                <asp:Label ID="headerTitle" runat="server"></asp:Label>
            </h3>
            <div id="wapiconfigtabstrip">
                <ul>
                    <li class="k-state-active">Windows Service </li>
                    <li>WAPI External Operation</li>
                    <li>WAPI Companies Code</li>
                    <li>WAPI Allowed Services</li>
                    <li>WAPI Proxy Security</li>
                    <li>Mobility Application</li>
                </ul>
                <!-- TAP: Windows Service -->
                <div>
                    <div style="text-align: left">
                        <br />
                        <table style="width: 100%">
                            <tr>
                                <td colspan="2">Advantage Windows Service Status Report:</td>

                            </tr>
                            <tr>
                                <td style="width: 60px">
                                    <img id="WapiImageServiceStatus" src="../images/geargray.jpg" alt="Service does not installed" style="width: 48px; vertical-align: central" />
                                </td>
                                <td style="text-align: left"><span id="WapiTextServiceStatus">Initializing Page...</span></td>
                                <td style="text-align: right">
                                    <button id="WapiGoLoggerPageButton" class="k-button" type="button" style="margin-right: 10px">Go to Logger Page</button>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <button id="WapiConfigStartButton" class="k-button" type="button" style="margin-right: 10px">Start Service</button>
                        <button id="WapiConfigStopButton" class="k-button" type="button">Stop Service</button>
                    </div>
                    <br />
                </div>
                <!-- TAB:  External Operation Settings -->
                <div>
                    <div id="example" style="-ms-align-content: center; -webkit-align-content: center; align-content: center; width: 100%; margin: 0 10px 0 10px; overflow: auto">
                        <br />
                        <div>
                            Advantage WAPI Allowed Operation Settings
                        </div>
                        <br />

                        <table id="WapiOperationSettingGrid" style="vertical-align: top">
                            <colgroup>
                                <col style="visibility: hidden; width: 1px" />
                                <col class="button_Column" />
                                <col class="settingRow" />
                                <col />
                                <col />
                                <col />
                                <col />

                            </colgroup>
                            <thead>
                                <tr>
                                    <th id="th1" style="visibility: hidden; width: 1px"></th>
                                    <th id="th2">Actions</th>
                                    <th id="th4">Operation</th>
                                    <th id="th5">Operation Details</th>
                                    <th id="th6">External Security</th>
                                    <th id="th7">Proxy Services</th>
                                    <th id="th8">On Demand Settings</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="8"></td>
                                </tr>
                            </tbody>
                        </table>
                        <br />
                        <button id="btnWapiSettingAddNew" class="k-button" type="button">Add New External Operation</button>
                    </div>

                </div>
                <!-- TAB:  Companies keys -->
                <div>

                    <div id="AdvantageWapiCompaniesManagement" style="-ms-align-content: center; -webkit-align-content: center; align-content: center; width: 90%; margin: 0 10px 0 10px; overflow: auto">
                        <br />
                        <div>
                            Advantage WAPI Companies Management
                        </div>

                        <div id="CompaniesContainer">
                            <div id="WapiCompaniesSettingGrid"></div>
                        </div>

                        <br />
                        <button id="btnWapiCompaniesSynchro" class="k-button" type="button">Synchronize Companies</button>
                    </div>


                </div>
                <!-- TAB:  Advantage WAPIAllowed Services Management  -->
                <div>
                    <div>
                        Advantage WAPI Allowed Services Manager
                    </div>
                    <div id="AllowedContainer">
                        <div id="WapiAllowedGrid"></div>
                    </div>
                </div>
                <!-- TAB:  Advantage WAPI Companies Security Management -->
                <div>
                    <div id="Div1" style="-ms-align-content: center; -webkit-align-content: center; align-content: center; width: 100%; margin: 0 10px 0 10px; overflow: auto">
                        <div>
                            <br />
                            Advantage WAPI Security Management
                        </div>
                        <br />
                        <div>
                            <button id="btnCreate" class="k-button" type="button">Create Key</button>
                            <button id="btnDelete" class="k-button" type="button">Delete Key</button>
                            <button id="btnShow" class="k-button" type="button">Show Key</button>
                        </div>
                        <br />
                        <p>Public Key:</p>

                        <div id="textAreaSecurity" class="k-textbox" style="width: 600px; height: 50px; overflow: auto"></div>
                        <br />
                        <button id="btnSelectAll" class="k-button" type="button">Select</button>

                    </div>
                    <br />
                </div>
                <!-- TAB: Mobility Application -->
                <div>
                    <div class="mobilityButtonPanel">
                        <label><b>Configuration Items</b></label>
                        <label id="klassAppHelp" style="float: right; text-align: right;" title="Help">
                            <img alt="Help" src="../images/HelpIcon.png"></label>
                    </div>
                    <div class="mobilityTablePanel">
                        <div id="klassAppGrid"></div>
                    </div>
                    <div>
                        <div class="mobilityButtonPanel">

                            <button id="klassBtnConfiguration" type="button" class="k-button">Send Configuration</button>
                            <button id="klassBtnInitialize" type="button" class="k-button">Initialize Configuration</button>
                            <button id="klassBtnResetKlassApp" type="button" class="k-button buttonDanger" style="background-color: orangered">Delete All KlassApp Data</button>
                        </div>
                    </div>

                </div>

            </div>
            <style scoped>
                /* Removes sprite styles from font icons */
                .k-icon {
                    font-family: WebComponentsIcons !important;
                }

                .k-button .k-image {
                    height: 16px;
                }

                .button_Column {
                    width: 90px;
                }

                .settingRow {
                    width: 150px;
                }

                .details {
                    width: 350px;
                    text-align: left;
                    vertical-align: top;
                }

                .description {
                    display: block;
                    padding-top: 5px;
                }

                .description1 {
                    padding-top: 5px;
                }

                td.photo {
                    text-align: center;
                }

                .k-grid-header .k-header {
                    padding: 5px 10px;
                }

                .k-grid td {
                    background: -moz-linear-gradient(top, rgba(0,0,0,0.05) 0, rgba(0,0,0,0.15) 100%);
                    background: -webkit-gradient(linear, left top, left bottom, color-stop(0,rgba(0,0,0,0.05)), color-stop(100%,rgba(0,0,0,0.15)));
                    background: -webkit-linear-gradient(top, rgba(0,0,0,0.05) 0,rgba(0,0,0,0.15) 100%);
                    background: -o-linear-gradient(top, rgba(0,0,0,0.05) 0,rgba(0,0,0,0.15) 100%);
                    background: -ms-linear-gradient(top, rgba(0,0,0,0.05) 0,rgba(0,0,0,0.15) 100%);
                    background: linear-gradient(to bottom, rgba(0,0,0,0.05) 0,rgba(0,0,0,0.15) 100%);
                    padding: 5px;
                }



                .k-grid .k-alt td {
                    background: -moz-linear-gradient(top, rgba(0,0,0,0.2) 0, rgba(0,0,0,0.1) 100%);
                    background: -webkit-gradient(linear, left top, left bottom, color-stop(0,rgba(0,0,0,0.2)), color-stop(100%,rgba(0,0,0,0.1)));
                    background: -webkit-linear-gradient(top, rgba(0,0,0,0.2) 0,rgba(0,0,0,0.1) 100%);
                    background: -o-linear-gradient(top, rgba(0,0,0,0.2) 0,rgba(0,0,0,0.1) 100%);
                    background: -ms-linear-gradient(top, rgba(0,0,0,0.2) 0,rgba(0,0,0,0.1) 100%);
                    background: linear-gradient(to bottom, rgba(0,0,0,0.2) 0,rgba(0,0,0,0.1) 100%);
                }

                .k-grid-content {
                    max-height: 500px;
                }

                .divColumn {
                    float: left;
                    margin-bottom: 15px;
                    margin-top: 10px;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .divWidth10 {
                    width: 10%;
                }

                .divWidth15 {
                    width: 15%;
                }

                .divWidth20 {
                    width: 20%;
                }

                .divWidth30 {
                    width: 30%;
                }

                .divWidth35 {
                    width: 35%;
                }

                .divWidth60 {
                    width: 60%;
                }

                .divWidth65 {
                    width: 65%;
                }

                .controlpaddingWidth {
                    width: 98%;
                }


                .columnControl-type {
                    width: 30%;
                    float: left;
                    margin-bottom: 15px;
                    margin-top: 10px;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .columnLabel-type {
                    width: 20%;
                    float: left;
                    margin-bottom: 15px;
                    margin-top: 10px;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .image-type {
                    width: 40%;
                    float: left;
                    margin-bottom: 15px;
                    margin-left: 10px;
                    margin-top: 10px;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                .button-type {
                    width: 55%;
                    float: left;
                    text-align: center;
                    vertical-align: middle;
                    margin-bottom: 15px;
                    margin-top: 20px;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }


                .column80-type {
                    width: 80%;
                    float: left;
                    margin-bottom: 15px;
                    margin-top: 10px;
                    -webkit-box-sizing: border-box;
                    -moz-box-sizing: border-box;
                    box-sizing: border-box;
                }

                textarea.k-textbox {
                    white-space: normal;
                }

                .mobilityTablePanel {
                    width: 100%;
                    margin: 5px;
                }

                .mobilityButtonPanel {
                    width: 100%;
                    margin: 5px;
                }

                .buttonDanger {
                    background-color: orangered;
                    color: black;
                }

                    .buttonDanger:hover {
                        background-color: darkred;
                        color: white;
                    }

                    .buttonDanger:focus {
                        background-color: crimson;
                        color: white;
                    }

                #klassAppHelp {
                    float: right;
                    text-align: right;
                    cursor: default;
                }

                    #klassAppHelp:hover {
                        float: right;
                        text-align: right;
                        cursor: pointer;
                    }
            </style>
        </div>
        <!-- Windows Control to Show On Demand waiting Operation -->
        <div id="WindowsWapiOnDemandOperation" style="display:none;">
            <div class="image-type">
                <img alt="Waiting for On demand Operation" src="../images/Gearstar.gif" />
            </div>
            <div class="button-type">
                <button id="btnCancelOnDemand" class="k-button" type="button" style="width: 150px; height: 60px">Cancel On Demand</button>
            </div>
        </div>

        <!-- Windows Control to Edit Operation Settings -->
        <div id="WindowsWapiOperationSettingEditGrid">
            <div class="k-header">Operation:</div>
            <div class="columnLabel-type">Operation Name:</div>
            <div class="columnControl-type">
                <input id="tbOperationSettingName" name="tbOperationSettingName" class="k-textbox" required />
            </div>
            <div class="columnLabel-type">Last Executed:</div>
            <div class="columnControl-type">
                <input id="dtLastExecutedDateTime" class="k-datetimepicker" />
            </div>
            <div style="width: 100%; clear: both"></div>

            <div class="columnLabel-type">Operation Active: </div>
            <div class="columnControl-type">
                <input id="cbWapiActiveOperation" type="checkbox" />
            </div>

            <div class="k-header" style="width: 100%; clear: both">External Operation Security and Localization:</div>

            <div class="columnLabel-type">Company Code:</div>
            <div class="columnControl-type">
                <input id="windowsWapiOperationCompany" class="k-dropdown" />
            </div>
            <div class="columnLabel-type">Mode:</div>
            <div class="columnControl-type">
                <input id="windowsWapiMode" class="k-dropdown" style="width: 100%" />
            </div>

            <div class="columnLabel-type">Consumer Key:</div>
            <div class="columnControl-type">
                <input id="tbwindowwapiConsumerKey" class="k-textbox" type="text" />
            </div>
            <div class="columnLabel-type">Private Key:</div>
            <div class="columnControl-type">
                <span class="k-textbox">
                    <input id="tbwindowwapiPrivateKey" type="text" style="width: 100%" /></span>
            </div>
            <div class="columnLabel-type">User Name:</div>
            <div class="columnControl-type">
                <span class="k-textbox">
                    <input id="tbwindowwapiUserName" type="text" style="width: 100%" /></span>
            </div>
            <div id="externalUrl" style="clear: both">
                <div class="columnLabel-type">External URL:</div>
                <div class="column80-type">
                    <input id="tbExternalUrl" class="k-textbox" type="text" style="width: 100%" />
                </div>
            </div>

            <div class="k-header" style="width: 100%; clear: both">WAPI Service</div>
            <div class="divWidth20 divColumn">Code WAPI:</div>
            <div class="divColumn divWidth30">
                <input id="ddCodeWapiService" class="k-dropdown controlpaddingWidth" />
            </div>
            <div class="divWidth20 divColumn">Second Code:</div>
            <div class="divColumn divWidth30">
                <input id="ddSecondWapiService" class="k-dropdown controlpaddingWidth" />
            </div>


            <div class="k-header" style="width: 100%; clear: both">On Demand Settings (seconds)</div>
            <div class="columnLabel-type">Operation Poll:</div>
            <div class="columnControl-type">
                <input id="windowsServicePollInterval" name="windowsServicePollInterval" class="k-textbox" required data-validation="rangeNumber" data-min="60" data-max="99999999" data-name="Service Interval" />
            </div>
            <div class="columnLabel-type">Configuration Poll:</div>
            <div class="columnControl-type">
                <input id="windowRefreshConfigurationInterval" name="windowRefreshConfigurationInterval" class="k-textbox" style="width: 100%" required />
            </div>
            <div style="clear: both; text-align: center">
                <button type="button" id="windowbtnEditSave" class="k-button" style="width: 80px">Save</button>
            </div>

        </div>

        <!-- Windows Control to enter a new External Operation -->
        <div id="WindowWapiAddNewExternalOperation">
            <div id="WapiWindowTabstripNewOperation">
                <ul>
                    <li id="wapitab1" class="k-state-active">Begin configuration
                    </li>
                    <li id="wapitab2">External Service
                    </li>
                    <li id="wapitab3"></li>
                    Advantage Service Layer 
                </ul>
                <div id="NewTab1" style="height: 360px">
                    <div style="height: 320px">
                        <div class="k-header">Part 1: External Operation Definition:</div>
                        <div class="columnLabel-type">Operation Name:</div>
                        <div class="columnControl-type">
                            <input id="tbNewOperationSettingName" class="k-textbox" required />
                        </div>
                        <div class="columnLabel-type">Operation Active:</div>
                        <div class="columnControl-type">
                            <input id="cbNewWapiActiveOperation" type="checkbox" />
                        </div>

                        <div style="width: 100%; clear: both"></div>

                        <div class="columnLabel-type">Operation Module(Mode):</div>
                        <div class="columnControl-type">
                            <input id="ddNewCodeModeOperation" class="k-dropdown" />
                        </div>
                        <div class="columnLabel-type">Company Code:</div>
                        <div class="columnControl-type">
                            <input id="ddNewCodeCompany" class="k-dropdown" style="width: 100%" />
                        </div>
                    </div>
                    <div style="width: 100%; clear: both"></div>
                    <div style="text-align: right">
                        <button id="btnWapiWizardNext1" class="k-button" type="button">Next</button>
                    </div>


                </div>

                <div style="height: 360px">
                    <div style="height: 320px">
                        <div class="k-header">Part 2: External Operation, Security and URL:</div>
                        <div class="columnLabel-type">Consumer Key:</div>
                        <div class="columnControl-type">
                            <input id="tbNewwindowwapiConsumerKey" class="k-textbox" />
                        </div>
                        <div class="columnLabel-type">Private Key:</div>
                        <div class="columnControl-type">
                            <input id="tbwNewindowwapiPrivateKey" class="k-textbox" style="width: 100%" />
                        </div>
                        <div class="columnLabel-type">User Name:</div>
                        <div class="columnControl-type">
                            <input id="tbwNewindowwapiUserName" class="k-textbox" style="width: 100%" />
                        </div>
                        <div id="NewexternalUrl" style="clear: both">
                            <div class="columnLabel-type">External URL:</div>
                            <div class="column80-type">
                                <input id="tbNewExternalUrl" class="k-textbox" style="width: 100%" />
                            </div>
                        </div>

                    </div>
                    <div style="width: 100%; clear: both"></div>
                    <div class="columnLabel-type">
                        <button id="btnWapiWizardPrevius1" class="k-button" type="button">Previous</button>
                    </div>
                    <div class="column80-type" style="text-align: right">
                        <button id="btnWapiWizardNext2" class="k-button" type="button">Next</button>
                    </div>
                    <div style="width: 100%; clear: both"></div>
                </div>
                <div style="height: 360px">
                    <div style="height: 320px">
                        <div class="k-header" style="width: 100%; clear: both">Part 3: Advantage Service Usage and On demand settings</div>
                        <div class="divColumn divWidth15">Code WAPI:</div>
                        <div class="divColumn divWidth35">
                            <input id="ddNewCodeWapiService" class="k-dropdown controlpaddingWidth" />
                        </div>
                        <div class="divColumn divWidth15">Second Code:</div>
                        <div class="divColumn divWidth35">
                            <input id="ddNewSecondWapiService" class="k-dropdown controlpaddingWidth" />
                        </div>


                        <div class="k-header" style="width: 100%; clear: both">On Demand Settings (seconds)</div>
                        <div class="divColumn divWidth15">Operation Poll:</div>
                        <div class="divColumn divWidth35">
                            <input id="NewwindowsServicePollInterval" class="k-textbox" required />
                        </div>
                        <div class="divColumn divWidth15">On-Demand Time Out:</div>
                        <div class="divColumn divWidth35">
                            <input id="NewWindowOnDemandTimeOut" class="k-textbox" style="width: 100%" required />
                        </div>
                        <div style="clear: both; text-align: center"></div>

                    </div>
                    <div style="width: 100%; clear: both"></div>
                    <div class="columnLabel-type">
                        <button id="btnWapiWizardPrevius2" class="k-button" type="button">Previous</button>
                    </div>
                    <div class="column80-type" style="text-align: right">
                        <button id="btnWapiWizardFinish" class="k-button" type="button">Finish</button>
                    </div>
                    <div style="width: 100%; clear: both"></div>
                </div>

            </div>



        </div>

    </div>
    <%-- ReSharper disable UnusedLocals --%>
    <script src="../Kendo/js/jszip.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            var wapimanager = new Wapi.WapiConfig();
            var mobile = new SY.Mobile();

        });
    </script>
    <%-- ReSharper restore UnusedLocals --%>
</asp:Content>

