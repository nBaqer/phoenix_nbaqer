﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class SchlDataDictionary
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents txtIsInDB As System.Web.UI.WebControls.TextBox

    Private pObj As New UserPagePermissionInfo
    Protected userId As String
    Protected campusId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString

        '   disable History button at all time
        'Header1.EnableHistoryButton(False)


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            'When the page first loads we want to populate lbxFields control with the captions of the fields
            'in the data dictionary.
            Dim facDataDict As New DataDictionaryFacade
            Dim dt As New DataTable

            dt = facDataDict.GetFields

            With lbxFields
                .DataSource = dt
                .DataTextField = "FldName"
                .DataValueField = "FldId"
                .DataBind()
            End With
        End If

        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        btnNew.Enabled = False
        btnDelete.Enabled = False
        headerTitle.Text = Header.Title
    End Sub

    Private Sub lbxFields_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxFields.SelectedIndexChanged
        Dim facDataDict As New DataDictionaryFacade
        Dim dt As New DataTable

        dt = facDataDict.GetFieldProperties(lbxFields.SelectedItem.Value)

        txtFldName.Text = dt.Rows(0)("FldName")
        txtFldType.Text = dt.Rows(0)("FldType")
        txtFldLen.Text = dt.Rows(0)("FldLen").ToString()
        If Not dt.Rows(0).IsNull("Caption") Then
            txtCaption.Text = dt.Rows(0)("Caption")
            txtCaptionIsInDB.Text = "TRUE"
        Else
            txtCaption.Text = ""
            txtCaptionIsInDB.Text = "FALSE"
        End If


        'If the field is set as being data dictionary required by the school then we need to check
        'the checkbox
        If facDataDict.IsFieldDataDictionaryRequired(lbxFields.SelectedItem.Value) Then
            chkSchoolReq.Checked = True
        Else
            chkSchoolReq.Checked = False
        End If

        'Enable the Save button if the user has full permission
        If pObj.HasFull Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objDataDicFac As New DataDictionaryFacade

        objDataDicFac.UpdateFieldProperties(lbxFields.SelectedItem.Value, chkSchoolReq.Checked, txtCaption.Text, txtCaptionIsInDB.Text)

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
