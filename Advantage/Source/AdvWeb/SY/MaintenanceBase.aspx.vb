﻿Imports System.Data
Imports System.Data.SqlClient
Imports Telerik.Web.UI
Imports CO = FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common

Partial Class SY_MaintenanceBase
    Inherits BasePage
    Public pageName As String
    Dim conn As New SqlConnection
    Public dtCampusGroup As DataTable
    Public dtIPEDS As DataTable
    Dim ddlIPEDS As DropDownList
    Dim ddlCampusGroups As DropDownList
    Dim ddlEditStatus As DropDownList
    Dim pObj As UserPagePermissionInfo = New UserPagePermissionInfo
    Dim cmpCallReportingAgencies As New ReportingAgencies
    Dim radGridDS As New DataSet
    Dim facade As New MaintenanceBaseFacade
    Protected campusId, userId As String
    Protected rowsCount As Integer = 0
    Protected IPEDSCount As Integer = 0

    Private Sub BuildGridData(ByVal ResourceId As Integer, ByVal StatusId As String)
        radGridDS = facade.BuildRadGrid(ResourceId, StatusId)
        rowsCount = radGridDS.Tables("RadGridDS").Rows.Count
        RadGrdPostScores.DataSource = radGridDS.Tables("RadGridDS")
        RadGrdPostScores.DataBind()
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not Page.IsPostBack Then
            'Get the value from query string
            txtResourceId.Text = Request.QueryString("resid")
            txtCampusId.Text = Request.QueryString("cmpid")
            txtPageName.Text = Request.QueryString("desc")
            'CType(MyBase.FindControlRecursive("btnAudit"), Button).Visible = True
            Dim btn = CType(FindControlRecursive("btnAudit"), LinkButton)
            btn.Attributes.Remove("class")

            btn.Style.Add("visibility", "hidden")

            'Set Security
            Try
                pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, txtResourceId.Text.ToString.Trim, txtCampusId.Text.ToString.Trim)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                Session.Abandon()
                Response.Redirect(FormsAuthentication.LoginUrl)
            End Try


            'Build the grid
            BuildGridData(CType(txtResourceId.Text, Integer), "")

            'ReBind
            RadGrdPostScores.Rebind()

            RadGrdPostScores.MasterTableView.NoMasterRecordsText = "No maintenance item exists for " & txtPageName.Text.ToLower.ToString.Trim
            RadGrdPostScores.MasterTableView.CommandItemSettings.AddNewRecordText = "Add new item"


            campusId = Master.CurrentCampusId
            If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If
            headerTitle.Text = Header.Title
        End If
    End Sub
    Protected Sub RadGrdPostScores_DeleteCommand(ByVal sender As Object, ByVal e As GridCommandEventArgs) Handles RadGrdPostScores.DeleteCommand
        Dim deletedItem = DirectCast(e.Item, GridDataItem)
        Dim field1_PKID As String
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, txtResourceId.Text.ToString.Trim, txtCampusId.Text.ToString.Trim)
        If pObj.HasDelete = True Then
            RadGrdPostScores.AllowAutomaticDeletes = True
        ElseIf pObj.HasDelete = False Then 'if user doesn't have permission
            'Dim cell As TableCell = CType(e.Item, GridDataItem)("DeleteColumn")
            'Dim lnk As ImageButton = CType(cell.Controls(0), ImageButton)
            Dim scriptstring = "radalert('User does not have permission to delete record', 280, 100);"
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
            Exit Sub
        End If

        field1_PKID = TryCast(deletedItem("field2").Controls(3), TextBox).Text
        conn.ConnectionString = GetConnectionStringFromAdvAppSetting("ConnectionString")
        conn.Open()

        Dim cmd1 As New SqlCommand
        cmd1.Connection = conn
        cmd1.CommandType = CommandType.StoredProcedure
        cmd1.CommandText = "USP_Maintenance_Delete"
        cmd1.Parameters.Add("@pResourceId", SqlDbType.Int).Value = CInt(txtResourceId.Text)
        cmd1.Parameters.Add("@field1", SqlDbType.VarChar).Value = field1_PKID

        Try
            cmd1.ExecuteNonQuery()
            conn.Close()
            e.Canceled = True
            RadGrdPostScores.Rebind()
            Me.ClearClientLocalandSessionStorage()
        Catch ex As SqlException
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'US3522 6/5/2013 Janet Robinson trap referential integrity error and display pretty msg
            If ex.ErrorCode = -2146232060 Then
                Dim radmgr1 = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                radmgr1.ResponseScripts.Add("radalert('The entry you are trying to delete is Active/In Use and cannot be deleted', 300, 100);")
                Exit Sub
            Else
                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Delete: " & ex.Message & " "
                Else
                    Session("Error") = "Error in Delete: " & ex.Message & " " & ex.InnerException.Message
                End If
                Application("ErrorPage") = Request.Url.ToString
                Response.Redirect("../ErrorPage.aspx")
            End If
        Finally
            conn.Close()
        End Try
    End Sub
    Protected Sub RadGrdPostScores_InsertCommand(sender As Object, e As GridCommandEventArgs) Handles RadGrdPostScores.InsertCommand

        Dim insertedItem = DirectCast(e.Item, GridDataInsertItem)
        Dim field1_PKID As String = Guid.NewGuid.ToString()
        Dim field2_Code As String
        Dim field3_Descrip As String
        Dim field4_StatusId As String
        Dim field5_CampGrpId As String
        Dim intIPEDSValue As Integer
        Dim strModUser As String
        Dim dtModDate As Date

        field2_Code = TryCast(insertedItem("field2").Controls(1), TextBox).Text
        field3_Descrip = TryCast(insertedItem("field3").Controls(1), TextBox).Text
        field4_StatusId = TryCast(insertedItem("field4").Controls(1), DropDownList).SelectedValue
        field5_CampGrpId = TryCast(insertedItem("field5").Controls(1), DropDownList).SelectedValue
        Dim ipeds = TryCast(insertedItem("field6").Controls(1), DropDownList).SelectedValue
        If Not Integer.TryParse(ipeds, intIPEDSValue) Then
            intIPEDSValue = 0
        End If

        strModUser = AdvantageSession.UserName
        dtModDate = Date.Now

        conn.ConnectionString = GetConnectionStringFromAdvAppSetting("ConnectionString")
        conn.Open()

        Dim cmd1 As New SqlCommand
        cmd1.Connection = conn
        cmd1.CommandType = CommandType.StoredProcedure
        cmd1.CommandText = "USP_Maintenance_Insert"
        cmd1.Parameters.Add("@pResourceId", SqlDbType.Int).Value = txtResourceId.Text
        cmd1.Parameters.Add("@field1", SqlDbType.VarChar).Value = field1_PKID
        cmd1.Parameters.Add("@field2", SqlDbType.VarChar).Value = field2_Code
        cmd1.Parameters.Add("@field3", SqlDbType.VarChar).Value = Replace(field3_Descrip, "'", "''")  'field3_Descrip
        cmd1.Parameters.Add("@field4", SqlDbType.VarChar).Value = field4_StatusId
        cmd1.Parameters.Add("@field5", SqlDbType.VarChar).Value = field5_CampGrpId
        cmd1.Parameters.Add("@field6", SqlDbType.Int).Value = intIPEDSValue
        cmd1.Parameters.Add("@username", SqlDbType.VarChar).Value = strModUser
        cmd1.Parameters.Add("@createddate", SqlDbType.VarChar).Value = dtModDate

        Try
            cmd1.ExecuteNonQuery()
            conn.Close()
            e.Canceled = True
            RadGrdPostScores.MasterTableView.IsItemInserted = False
            RadGrdPostScores.Rebind()
            Dim strIPEDSValue As New ArrayList
            strIPEDSValue.Add(intIPEDSValue.ToString)
            cmpCallReportingAgencies.InsertValues(field1_PKID.ToString, strIPEDSValue)
            Me.ClearClientLocalandSessionStorage()
        Catch ex As SqlException
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            If ex.ErrorCode = -2146232060 Then 'duplicate entry
                Dim radmgr1 As RadAjaxManager = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                radmgr1.ResponseScripts.Add("radalert('Duplicate record - try again', 300, 100);")
                Exit Sub
            Else
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Insert: " & ex.Message & " "
                Else
                    Session("Error") = "Error in Insert: " & ex.Message & " " & ex.InnerException.Message
                End If
                Application("ErrorPage") = Request.Url.ToString
                Response.Redirect("../ErrorPage.aspx")
            End If
        Finally
            conn.Close()
        End Try
    End Sub
    Protected Sub RadGrdPostScores_ItemCommand(ByVal sender As Object, ByVal e As GridCommandEventArgs) Handles RadGrdPostScores.ItemCommand
        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked
            e.Canceled = True
            Dim newValues As ListDictionary = New ListDictionary()
            Dim gridEditFormItem As GridCommandItem = DirectCast(e.Item, GridCommandItem)
            newValues("Field2") = ""
            newValues("Field3") = ""
            newValues("Field4") = ""
            newValues("Field5") = "All"
            newValues("Field6") = ""
            e.Item.OwnerTableView.InsertItem(newValues)
        End If
    End Sub
    Protected Sub RadGrdPostScores_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrdPostScores.ItemDataBound
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, txtResourceId.Text.ToString.Trim, txtCampusId.Text.ToString.Trim)

        'RadGrdPostScores.Height = Unit.Pixel(intHeight)
        If pObj.HasAdd = True Then
            RadGrdPostScores.AllowAutomaticInserts = True
        ElseIf pObj.HasAdd = False Then
            If TypeOf e.Item Is GridCommandItem Then
                Dim cmditm = DirectCast(e.Item, GridCommandItem)
                '  Dim lnkBtn1 As LinkButton = DirectCast(cmditm.FindControl("linkbuttionInitInsert"), LinkButton)
                Dim btn1 = DirectCast(cmditm.FindControl("btn1"), Button)
                Try
                    btn1.Enabled = False
                    btn1.Visible = False
                    btn1.ToolTip = "User does not have permission to add new " & txtPageName.Text & " records"
                    Dim lnkbtn1 = DirectCast(cmditm.FindControl("linkbuttionInitInsert"), LinkButton)
                    lnkbtn1.Enabled = False
                    lnkbtn1.ToolTip = "User does not have permission to add new " & txtPageName.Text & " records"
                    lnkbtn1.Visible = False
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                End Try
            End If
        End If

        If pObj.HasEdit = True Then
            RadGrdPostScores.AllowAutomaticUpdates = True
        ElseIf pObj.HasEdit = False And pObj.HasAdd = False Then

            If TypeOf e.Item Is GridDataItem Then
                '  If TypeOf e.Item Is GridDataItem And e.Item.IsInEditMode And e.Item.OwnerTableView.IsItemInserted = False Then

                Dim cell As TableCell = CType(e.Item, GridDataItem)("EditCommandColumn")
                Dim lnk As ImageButton = CType(cell.Controls(0), ImageButton)
                lnk.Visible = False
                cell.ToolTip = "User does not have the permission to edit " & txtPageName.Text & " records"
            End If

        ElseIf pObj.HasEdit = False And pObj.HasAdd = True Then
            If TypeOf e.Item Is GridDataItem And e.Item.OwnerTableView.IsItemInserted = False Then
                '  If TypeOf e.Item Is GridDataItem And e.Item.IsInEditMode And e.Item.OwnerTableView.IsItemInserted = False Then

                Dim cell As TableCell = CType(e.Item, GridDataItem)("EditCommandColumn")
                Dim lnk As ImageButton = CType(cell.Controls(0), ImageButton)
                lnk.Visible = False
                cell.ToolTip = "User does not have the permission to edit " & txtPageName.Text & " records"
            End If
        End If
        If pObj.HasDelete = False Then
            If TypeOf e.Item Is GridDataItem Then
                Dim cell As TableCell = CType(e.Item, GridDataItem)("DeleteColumn")
                Dim lnk As ImageButton = CType(cell.Controls(0), ImageButton)
                lnk.Visible = False
                cell.ToolTip = "User does not have the permission to delete " & txtPageName.Text & " records"
            End If

        End If

        If (TypeOf e.Item Is GridDataItem) AndAlso e.Item.IsInEditMode Then
            Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
            BuildDDLsInsideGrid(gridEditFormItem)

            If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                'Item Array 2 - IPEDS
                'At times IPEDS may not be applicable for some pages - so suppress the error

                Dim IpedsValue As String
                IpedsValue = TryCast(e.Item.FindControl("txtEditIsIPEDSVisible"), TextBox).Text

                Try

                    '  If DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(14) = "1" Then
                    If IpedsValue = "1" Then
                        Try
                            ddlIPEDS.SelectedValue = DirectCast((gridEditFormItem.DataItem), DataRowView).Row.ItemArray(12)
                            ddlIPEDS.SelectedItem.Text = DirectCast((gridEditFormItem.DataItem), DataRowView).Row.ItemArray(13).ToString()
                        Catch ex As Exception
                            Dim exTracker = New AdvApplicationInsightsInitializer()
                            exTracker.TrackExceptionWrapper(ex)

                            ddlIPEDS.SelectedIndex = 0
                        End Try
                    End If
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlIPEDS.SelectedIndex = 0
                End Try

                ddlCampusGroups.SelectedValue = DirectCast((gridEditFormItem.DataItem), DataRowView).Row.ItemArray(9).ToString()
                Dim StatusValue As String
                StatusValue = UCase(DirectCast((gridEditFormItem.DataItem), DataRowView).Row.ItemArray(8).ToString())
                Dim ddl As DropDownList = TryCast(e.Item.FindControl("ddlEditStatus"), DropDownList)
                ddl.SelectedValue = StatusValue

            End If

            ' adSourceCatagory table needs 50 for code length all other 12 chars
            Dim txtBox As TextBox
            txtBox = TryCast(e.Item.FindControl("txtEditCode"), TextBox)
            If txtResourceId.Text = 212 Then
                txtBox.MaxLength = 50
            ElseIf txtResourceId.Text = 788 Then  'Credential Level Page
                txtBox.MaxLength = 2
            End If

        End If

        'Try
        'The following code is to hide the IPEDS column from the grid based on whether the page
        'has IPEDS functionality in it or not
        If (TypeOf e.Item Is GridDataItem) Then

            If Not CType(e.Item.FindControl("txtIsIPEDSVisible"), TextBox) Is Nothing Then
                If CType(e.Item.FindControl("txtIsIPEDSVisible"), TextBox).Text.ToString.Trim = "0" Then
                    RadGrdPostScores.MasterTableView.GetColumn("Field6").Visible = False
                    RadGrdPostScores.MasterTableView.GetColumn("Field5").HeaderStyle.Width = New Unit("250px")
                End If
            End If
        End If
        'Catch ex As Exception
        '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        'End Try

        If (TypeOf e.Item Is GridHeaderItem) Then
            DirectCast(e.Item.FindControl("lblHeaderCode"), Label).Text = radGridDS.Tables("Headers").Rows(0)("Header2")
            DirectCast(e.Item.FindControl("lblHeaderCode"), Label).Visible = True
            DirectCast(e.Item.FindControl("lblHeaderDescription"), Label).Text = radGridDS.Tables("Headers").Rows(0)("Header3")
            DirectCast(e.Item.FindControl("lblHeaderStatus"), Label).Text = radGridDS.Tables("Headers").Rows(0)("Header4")
            If Not IsNothing(radGridDS.Tables("Headers").Rows(0)("Header5")) Then
                DirectCast(e.Item.FindControl("lblHeaderCampusGroups"), Label).Text = radGridDS.Tables("Headers").Rows(0)("Header5")
            End If
            DirectCast(e.Item.FindControl("lblHeaderIPEDS"), Label).Text = "IPEDS"

            If rowsCount = 0 Then
                If IPEDSCount > 0 Then
                    DirectCast(e.Item.FindControl("lblHeaderIPEDS"), Label).Visible = True
                Else
                    DirectCast(e.Item.FindControl("lblHeaderIPEDS"), Label).Visible = False
                    RadGrdPostScores.MasterTableView.GetColumn("Field6").Visible = False
                    RadGrdPostScores.MasterTableView.GetColumn("Field5").HeaderStyle.Width = New Unit("250px")
                End If
            End If
        End If


        If ((pObj.HasAdd = True) And (pObj.HasDelete = False And pObj.HasDisplay = False And pObj.HasEdit = False And pObj.HasFull = False And pObj.HasNone = False)) Then
            For Each item As GridDataItem In RadGrdPostScores.Items
                If TypeOf item Is GridDataItem Then
                    Dim editcell As TableCell = CType(item, GridDataItem)("EditCommandColumn")
                    Dim editlnk As ImageButton = CType(editcell.Controls(0), ImageButton)
                    editlnk.Visible = False
                    editcell.ToolTip = "User does not have the permission to edit " & txtPageName.Text & " records"
                    Dim deletecell As TableCell = CType(item, GridDataItem)("DeleteColumn")
                    Dim deletelnk As ImageButton = CType(deletecell.Controls(0), ImageButton)
                    deletelnk.Visible = False
                    deletecell.ToolTip = "User does not have the permission to delete " & txtPageName.Text & " records"
                End If
            Next
        End If

        If ((pObj.HasAdd = True And pObj.HasDelete = True) And (pObj.HasDisplay = False And pObj.HasEdit = False And pObj.HasFull = False And pObj.HasNone = False)) Then
            For Each item As GridDataItem In RadGrdPostScores.Items
                If TypeOf item Is GridDataItem Then
                    Dim editcell As TableCell = CType(item, GridDataItem)("EditCommandColumn")
                    Dim editlnk As ImageButton = CType(editcell.Controls(0), ImageButton)
                    editlnk.Visible = False
                    editcell.ToolTip = "User does not have the permission to edit " & txtPageName.Text & " records"
                End If
            Next
        End If




    End Sub

    Private Sub BuildDDLsInsideGrid(ByVal gridEditFormItem As GridDataItem)
        dtIPEDS = BuildIPEDS()
        ddlIPEDS = DirectCast(gridEditFormItem("Field6").FindControl("ddlIPEDS"), DropDownList)
        ddlIPEDS.DataSource = dtIPEDS
        ddlIPEDS.DataTextField = "AgencyDescrip"
        ddlIPEDS.DataValueField = "RptAgencyFldValId"
        ddlIPEDS.DataBind()
        ddlIPEDS.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
        IPEDSCount = dtIPEDS.Rows.Count

        ddlCampusGroups = DirectCast(gridEditFormItem("Field5").FindControl("ddlCampusGroups"), DropDownList)
        ddlCampusGroups.DataSource = BuildCampusGroups()
        ddlCampusGroups.DataTextField = "CampGrpDescrip"
        ddlCampusGroups.DataValueField = "CampGrpId"
        ddlCampusGroups.DataBind()
        ddlCampusGroups.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))

        ddlEditStatus = DirectCast(gridEditFormItem("Field4").FindControl("ddlEditStatus"), DropDownList)
        ddlEditStatus.Items.Insert(0, New ListItem("Select", "00000000-0000-0000-0000-000000000000"))
        ddlEditStatus.Items.Insert(1, New ListItem("Active", "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965"))
        ddlEditStatus.Items.Insert(2, New ListItem("Inactive", "1AF592A6-8790-48EC-9916-5412C25EF49F"))
    End Sub
    Protected Sub RadGrdPostScores_ItemInserted(ByVal sender As Object, ByVal e As GridInsertedEventArgs) Handles RadGrdPostScores.ItemInserted
    End Sub
    Protected Sub RadGrdPostScores_NeedDataSource(ByVal sender As Object, ByVal e As GridNeedDataSourceEventArgs) Handles RadGrdPostScores.NeedDataSource
        radGridDS = facade.BuildRadGrid(CType(txtResourceId.Text, Integer), "")
        rowsCount = radGridDS.Tables("RadGridDS").Rows.Count
        RadGrdPostScores.DataSource = radGridDS.Tables("RadGridDS")

    End Sub
    Private Function BuildIPEDS() As DataTable
        conn.ConnectionString = GetConnectionStringFromAdvAppSetting("ConnectionString")
        Dim queryString = "USP_IPEDS_BuildList"
        Dim adapter = New SqlDataAdapter(queryString, conn)

        Dim campgrps As DataSet = New DataSet

        Dim cmd1 As New SqlCommand
        cmd1.Connection = conn
        cmd1.CommandType = CommandType.StoredProcedure
        cmd1.CommandText = "USP_IPEDS_BuildList"
        cmd1.Parameters.Add("@pResourceId", SqlDbType.Int).Value = txtResourceId.Text   'By Default Agency Sponsors will be Displayed
        adapter.SelectCommand = cmd1
        adapter.Fill(campgrps, "IPEDS")
        Dim dt As DataTable = campgrps.Tables(0)
        Return dt
    End Function
    Private Function BuildCampusGroups() As DataTable
        conn.ConnectionString = GetConnectionStringFromAdvAppSetting("ConnectionString")
        Dim queryString As String = "SELECT Distinct CampGrpId,CampGrpDescrip from syCampGrps order by CampGrpDescrip"
        Dim adapter = New SqlDataAdapter(queryString, conn)

        Dim campgrps = New DataSet
        adapter.Fill(campgrps, "CampusGroups")
        Dim dt As DataTable = campgrps.Tables(0)
        Return dt
    End Function

    Protected Sub RadGrdPostScores_UpdateCommand(ByVal sender As Object, ByVal e As GridCommandEventArgs) Handles RadGrdPostScores.UpdateCommand
        Dim editedItem = DirectCast(e.Item, GridEditableItem)
        Dim field1_PKID As String
        Dim field2Code As String
        Dim field3Descrip As String
        Dim field4StatusId As String
        Dim field5CampGrpId As String
        Dim intIpedsValue As Integer
        Dim strModUser As String
        Dim dtModDate As Date

        Try
            field1_PKID = TryCast(editedItem("field2").Controls(3), TextBox).Text
            field2Code = TryCast(editedItem("field2").Controls(1), TextBox).Text
            field3Descrip = TryCast(editedItem("field3").Controls(1), TextBox).Text
            field4StatusId = TryCast(editedItem("field4").Controls(1), DropDownList).SelectedValue
            field5CampGrpId = TryCast(editedItem("field5").Controls(1), DropDownList).SelectedValue
            intIpedsValue = 0
            If TryCast(editedItem("field2").Controls(5), TextBox).Text.ToString.Trim = "1" Then
                If TryCast(editedItem("field6").Controls(1), DropDownList).SelectedValue <> Guid.Empty.ToString Then
                    intIpedsValue = TryCast(editedItem("field6").Controls(1), DropDownList).SelectedValue
                End If
                'Else
                '    intIPEDSValue = 0 'if page is not needed for IPEDS
            End If
            '  strModUser = "sa"
            strModUser = AdvantageSession.UserName
            dtModDate = Date.Now

            conn.ConnectionString = GetConnectionStringFromAdvAppSetting("ConnectionString")
            conn.Open()

            'Dim adapter As New SqlDataAdapter()
            Dim cmd1 As New SqlCommand
            cmd1.Connection = conn
            cmd1.CommandType = CommandType.StoredProcedure
            cmd1.CommandText = "USP_Maintenance_Update"
            cmd1.Parameters.Add("@pResourceId", SqlDbType.Int).Value = txtResourceId.Text
            cmd1.Parameters.Add("@field1", SqlDbType.VarChar).Value = field1_PKID
            cmd1.Parameters.Add("@field2", SqlDbType.VarChar).Value = field2Code
            cmd1.Parameters.Add("@field3", SqlDbType.VarChar).Value = Replace(field3Descrip, "'", "''")
            cmd1.Parameters.Add("@field4", SqlDbType.VarChar).Value = field4StatusId
            cmd1.Parameters.Add("@field5", SqlDbType.VarChar).Value = field5CampGrpId
            cmd1.Parameters.Add("@field6", SqlDbType.VarChar).Value = intIpedsValue
            cmd1.Parameters.Add("@username", SqlDbType.VarChar).Value = strModUser
            cmd1.Parameters.Add("@createddate", SqlDbType.VarChar).Value = dtModDate


            cmd1.ExecuteNonQuery()
            conn.Close()
            e.Canceled = True
            RadGrdPostScores.MasterTableView.ClearEditItems()
            RadGrdPostScores.Rebind()
            Dim strIpedsValue As New ArrayList
            strIpedsValue.Add(intIpedsValue.ToString)
            cmpCallReportingAgencies.InsertValues(field1_PKID.ToString, strIpedsValue)
            Me.ClearClientLocalandSessionStorage()
        Catch ex As SqlException
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.ErrorCode = -2146232060 Then 'duplicate entry
                Dim radmgr1 = CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager)
                radmgr1.ResponseScripts.Add("radalert('Duplicate record - try again', 300, 100); ")
                RadGrdPostScores.MasterTableView.ClearEditItems()
                RadGrdPostScores.Rebind()
                Exit Sub
            Else
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Insert: " & ex.Message & " "
                Else
                    Session("Error") = "Error in Insert: " & ex.Message & " " & ex.InnerException.Message
                End If
                Application("ErrorPage") = Request.Url.ToString
                Response.Redirect("../ErrorPage.aspx")
            End If
        Finally
            conn.Close()
        End Try
    End Sub

    Private Sub ClearClientLocalandSessionStorage()
        Dim scriptName As String = "clearClientLocalandSessionStorage"
        Dim cstype As Type = Me.GetType()
        ScriptManager.RegisterStartupScript(Page, cstype, scriptName, "clearSessionStorage();", True)
    End Sub

#Region "Filter Routines"
    'Protected Sub RadGrdPostScores_ColumnCreating(ByVal sender As Object, ByVal e As GridColumnCreatingEventArgs) Handles RadGrdPostScores.ColumnCreating
    '    If (e.ColumnType = GetType(MyCustomFilteringColumnCS).Name) Then
    '        e.Column = New MyCustomFilteringColumnCS(29)
    '    End If
    'End Sub
    'Protected Sub clrFilters_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    For Each column As GridColumn In RadGrdPostScores.MasterTableView.Columns
    '        column.CurrentFilterFunction = GridKnownFunction.NoFilter
    '        column.CurrentFilterValue = String.Empty
    '    Next
    '    RadGrdPostScores.MasterTableView.FilterExpression = String.Empty
    '    RadGrdPostScores.MasterTableView.Rebind()
    'End Sub
#End Region
    Protected Sub RadGrdPostScores_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles RadGrdPostScores.PreRender
        If Not RadGrdPostScores.MasterTableView.FilterExpression Is String.Empty Then
            RefreshGrid()
        End If
    End Sub

    Private Sub RefreshGrid()
        Dim strFilter As String = RadGrdPostScores.MasterTableView.FilterExpression.ToString
        If Not strFilter.ToString.Trim = "" Then
            If strFilter.Substring(35, 2).ToString.ToLower = "ac" Then
                strFilter = "active"
            ElseIf strFilter.Substring(35, 2).ToString.ToLower = "in" Then
                strFilter = "inactive"
            Else
                strFilter = ""
            End If
        Else
            strFilter = ""
        End If

        radGridDS = facade.BuildRadGrid(CType(txtResourceId.Text, Integer), strFilter)
        rowsCount = radGridDS.Tables("RadGridDS").Rows.Count
        RadGrdPostScores.DataSource = radGridDS.Tables("RadGridDS")
        RadGrdPostScores.Rebind()
    End Sub
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub

End Class
