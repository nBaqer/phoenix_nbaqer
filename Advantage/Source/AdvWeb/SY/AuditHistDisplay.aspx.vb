
Imports System.Diagnostics
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports Telerik.Web.UI

Partial Class AuditHistDisplay
    Inherits Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Dim dt As New DataTable("AuditHistDT")
    Dim tableRowIds As String
    Dim resourceId As Integer
    Protected Sub dgHist_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles dgHist.NeedDataSource

        Dim facAuditDisplay As New AuditHistoryFacade
        '            Dim dt As DataTable

        tableRowIDS = Request.QueryString("TableRowIDS")
        resourceId = CType(Request.QueryString("resid"), Integer)
        'BindDG("Default")
        If resourceId = 203 Then
            'The student master is now really pointing to the adLeads table
            resourceId = 170
            'The initial RowId passed in is the StudentId so we have to get the LeadId for that StudentId
            tableRowIds = facAuditDisplay.GetLeadIdFromStudentId(tableRowIds)
        End If
        dt = facAuditDisplay.GetAuditHist(resourceId, tableRowIDS)
        dgHist.DataSource = dt

    End Sub
End Class
