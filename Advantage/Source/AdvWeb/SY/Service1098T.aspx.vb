﻿
Imports FAME.Advantage.Common
Imports FAME.Advantage.Reporting.Logic

Namespace AdvWeb.SY

    Partial Class Service1098T
        Inherits Page
        Protected Sub btnExceptions_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExceptions.Click
        Dim getReportAsBytes As [Byte]()
        Dim intTabId As Integer = 0

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        intTabId = 0
        
        Dim intSchoolOptions As Integer = CommonWebUtilities.SchoolSelectedOptions(myAdvAppSettings.AppSettings("SchedulingMethod").ToString)
        Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder") '"/Advantage Reports/" + SingletonAppSettings.AppSettings("SSRS Deployment Environment").ToString.Trim
        'jguirado refactoring to eliminate the control from page master : 
        ' CType(Master.FindControl("UserSplitButton"), RadButton).Text, _
        'was change by get the session value
        getReportAsBytes = (New ReportGeneratorFor1098T).RenderReport("pdf", "support","89B675EE-546A-4C99-8427-305FED6DBF06", _
                                                                                  "Academic Advisor", "26", _
                                                                                  "26", AdvantageSession.UserName, _
                                                                                  0, _
                                                                                  strReportPath, _
                                                                                  intTabId,
                                                                                  0, _
                                                                                  AdvantageSession.UserState.CampusId.ToString, intSchoolOptions)
        ExportReport("pdf", getReportAsBytes)
    End Sub
        Private Sub ExportReport(ByVal ExportFormat As String, ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case ExportFormat.ToLower
            Case Is = "pdf"
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case "excel"
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
                'Case "WORD"
                '    strExtension = "doc"
                '    strMimeType = "application/vnd.ms-word"
            Case "csv"
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select
        Session("SSRS_FileExtension") = strExtension
        Session("SSRS_MimeType") = strMimeType
        Session("SSRS_ReportOutput") = getReportAsBytes
        'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        Dim script As String = String.Format("window.open('DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString())
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    End Sub
    End Class

End Namespace