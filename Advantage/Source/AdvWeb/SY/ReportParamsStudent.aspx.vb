﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports FAME.AdvantageV1.DataAccess
Imports AdvWeb.VBCode.ComponentClasses

Partial Class ReportParamsStudent
    Inherits BasePage
    Protected WithEvents pnlSavedPrefs As System.Web.UI.WebControls.Panel
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblSkillGroups As System.Web.UI.WebControls.Label
    Protected WithEvents lblStudentSkills As System.Web.UI.WebControls.Label
    Protected WithEvents lbl3 As System.Web.UI.WebControls.Label
    Protected WithEvents Panel2 As System.Web.UI.WebControls.Panel
    Protected WithEvents tblRequiredFields As System.Web.UI.WebControls.Table
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected PageTitle As String = "Report Parameters - "
    Protected campusId, userId As String
    Protected resourceId As Integer
    ' US2729 12/23/2011 Janet Robinson 
    Protected RptParam1 As Integer
    Protected RptParam2 As Integer
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Private pObj As New UserPagePermissionInfo
    Dim strCampGrpId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objCommon As New FAME.Common.CommonUtilities
        Dim rptFac As New ReportFacade
        Dim rptInfo As New ReportInfo
        ' Dim m_Context As HttpContext

        Dim fac As New UserSecurityFacade

        'Set the Delete button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        'resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'campusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString

        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString

        'This will appease the request in RptParamsDB.vb for the UserId. Remember, we are using DataAccess project from Clock Hour.
        'Moving forward, we need to use AdvantageSession.UserState.UserId.ToString to grab the UserId
        HttpContext.Current.Session("UserId") = userId


        Dim advantageUserState As New BO.User()

        advantageUserState = AdvantageSession.UserState


        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        ' make sure the Session variable for Reporting Agency is cleared
        Session("RptAgency") = Nothing
        If pObj Is Nothing Then
            'Send the user to the standard error page
            Session("Error") = "You do not have the necessary permissions to access to this report."
            Response.Redirect("../ErrorPage.aspx")
        End If

        If Not Page.IsPostBack Then

            pnl4.Visible = True     'Required Filters.
            pnl5.Visible = True     'Required Filters'labels.

            'Bind the DataList on the lhs to any preferences saved for this report.
            BindDataListFromDB()

            ViewState("resid") = Request.Params("resid")

            'Get the report info and store each piece in view state.
            rptInfo = rptFac.GetReportInfo(CInt(ViewState("resid")))
            ViewState("SQLID") = rptInfo.SqlId
            ViewState("OBJID") = rptInfo.ObjId
            ViewState("RESURL") = rptInfo.Url
            ViewState("Resource") = rptInfo.Resource

            'Populate all the filter sections.
            BuildAllSections()

            'Set the mode to NEW
            'SetMode("NEW")
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"

            If CInt(ViewState("resid")) = 615 Then
                requiredFldValStudent.Enabled = False
                requiredFldValStuEnroll.Enabled = False

                Dim strreqLbl As String = " [Campus Group]           Or          [Student and Enrollment]"
                lblRequiredFilters.Text = strreqLbl
                lblSpeed.Visible = True
                ddlSpeedOperator.Visible = True
                txtSpeedVal1.Visible = True
                lblAccuracy.Visible = True
                ddlAccuracyOperator.Visible = True
                txtAccuracyVal1.Visible = True
            End If

            'Set page title.
            PageTitle &= ViewState("Resource")
            'Page.DataBind()



        Else
            'Rebuild the dynamic table and dynamic validators each time there is a postback.
            BuildFilterOther()
        End If
        'btnSave.Enabled = False
    End Sub

    Private Sub BuildAllSections()
        Dim dt2 As DataTable
        Dim dtMasks As New DataTable
        Dim dtOperators As New DataTable
        Dim objRptParams As New RptParamsFacade
        Dim objCommon As New FAME.Common.CommonUtilities
        Dim dr As DataRow
        Dim ds As New DataSet
        Dim numRows As Integer
        Dim numCols As Integer
        Dim rValues As Integer

        'Get the parameters for this report
        ds = objRptParams.GetRptParams

        'Set status of buttons and checkboxes.
        If ds.Tables.Count = 2 Then
            Dim row As DataRow = ds.Tables("RptProps").Rows(0)
            If Not row.IsNull("AllowParams") Then
                If Not row("AllowParams") Then
                    'Report has no params, then disable Save, New and Delete buttons.
                    btnadd.Enabled = False
                    btnremove.Enabled = False
                    btnSave.Enabled = False
                    btnNew.Enabled = False
                    btnDelete.Enabled = False
                    'Filters checkbox.
                    chkRptFilters.Enabled = False
                    'Sort checkbox.
                    chkRptSort.Enabled = False
                End If
            End If
            'Filter Criteria checkbox.
            If Not row.IsNull("AllowFilters") Then chkRptFilters.Enabled = row("AllowFilters")
            'Sort Order checkbox.
            If Not row.IsNull("AllowSortOrder") Then chkRptSort.Enabled = row("AllowSortOrder")
            'Selected Filter Criteria checkbox.
            If Not row.IsNull("SelectFilters") Then chkRptFilters.Checked = row("SelectFilters")
            'Selected Sort Order checkbox.
            If Not row.IsNull("SelectSortOrder") Then chkRptSort.Checked = row("SelectSortOrder")
            'Description checkbox.
            If Not row.IsNull("AllowDescrip") Then chkRptDescrip.Enabled = row("AllowDescrip")
            'Instructions checkbox.
            If Not row.IsNull("AllowInstruct") Then chkRptInstructions.Enabled = row("AllowInstruct")
            'Notes checkbox.
            If Not row.IsNull("AllowNotes") Then chkRptNotes.Enabled = row("AllowNotes")
            'Added By Vijay Ramteke on Feb 08, 2010 For Mantis Id 17900
            'Cost checkbox.
            If Not row.IsNull("ShowCosts") Then chkRptCosts.Enabled = row("ShowCosts")
            'Expected Funding checkbox.
            If Not row.IsNull("ShowExpectedFunding") Then chkRptExpectedFunding.Enabled = row("ShowExpectedFunding")
            'Category Breakdown checkbox.
            If Not row.IsNull("ShowCategoryBreakdown") Then chkRptCategoryBreakdown.Enabled = row("ShowCategoryBreakdown")
            'Added By Vijay Ramteke on Feb 08, 2010 For Mantis Id 17900
        End If

        'Get the input masks
        dtMasks = objCommon.GetInstInputMasks.Copy
        dtMasks.TableName = "InputMasks"
        ds.Tables.Add(dtMasks)

        With ds.Tables("InputMasks")
            .PrimaryKey = New DataColumn() { .Columns("FldId")}
        End With

        'Get the comparison operators
        dtOperators = objCommon.GetComparisonOps.Copy
        dtOperators.TableName = "Operators"
        ds.Tables.Add(dtOperators)

        'Add a dt to store the possible values for each parameter that belongs to the
        'FilterList section. For example, we want to be able to store the possible values
        'for Program or Status.
        ds.Tables.Add("FilterListValues")

        With ds.Tables("FilterListValues")
            .Columns.Add("RptParamId", GetType(Integer))
            .Columns.Add("ValueFld", GetType(String))
            .Columns.Add("DisplayFld", GetType(String))
        End With

        'Add a dt to store the values selected for each parameter that belongs to the
        'FilterList section. For example, if the user selects Active and Attending for
        'Status we want to store these items.
        ds.Tables.Add("FilterListSelections")
        With ds.Tables("FilterListSelections")
            .Columns.Add("RptParamId", GetType(Integer))
            .Columns.Add("FldValue", GetType(String))
            .PrimaryKey = New DataColumn() { .Columns("RptParamId"), .Columns("FldValue")}
        End With

        'that it belongs to.
        For Each dr In ds.Tables("RptParams").Rows
            If dr("SortSec") = "True" Then
                'Add the field info to the lbxSort listbox
                lbxsort.Items.Add(New ListItem(dr("Caption"), dr("RptParamId")))
            End If
            If dr("FilterListSec") = "True" Then
                'Add the field info to the lbxFilterList listbox 
                lbxfilterlist.Items.Add(New ListItem(dr("Caption"), dr("RptParamId")))
                'Get the list of possible values for this item

                If resourceId = 615 And dr("DDLId") = 132 Then
                    dt2 = (New GradesFacade).GetTestType()
                Else
                    dt2 = objRptParams.GetFilterListValues(dr("DDLId"), True, userId)
                End If

                numRows = dt2.Rows.Count
                numCols = dt2.Columns.Count

                For rValues = 0 To numRows - 1
                    'Create a new row for the FilterListValues dt
                    Dim dRow1 As DataRow = ds.Tables("FilterListValues").NewRow
                    dRow1("RptParamId") = dr("RptParamId")
                    dRow1("ValueFld") = dt2.Rows(rValues)(0)
                    dRow1("DisplayFld") = dt2.Rows(rValues)(1)
                    'Add the new row to the FilterListValues dt
                    ds.Tables("FilterListValues").Rows.Add(dRow1)
                Next

                If dr("Required") = "True" Then
                    'Add a hidden textbox and a compare validator if this field is marked as 'Required'.
                    'This textbox need to be updated everytime it is selected or deselected.
                    Dim txt As New TextBox
                    Dim compVal As New CompareValidator

                    txt.ID = "txt" & dr("RptParamId").ToString()
                    txt.CssClass = "ToTheme2"
                    txt.Text = System.Guid.Empty.ToString
                    txt.Width = Unit.Pixel(0)

                    compVal.ID = "compVal" & dr("RptParamId").ToString()
                    compVal.ControlToValidate = txt.ID
                    compVal.[Operator] = ValidationCompareOperator.NotEqual
                    compVal.ValueToCompare = System.Guid.Empty.ToString
                    compVal.Type = ValidationDataType.String
                    compVal.ErrorMessage = dr("Caption") & " is a required filter."
                    compVal.Display = ValidatorDisplay.None

                    pnlRequiredFieldValidators.Controls.Add(txt)
                    pnlRequiredFieldValidators.Controls.Add(compVal)
                End If
            End If

            'Special case - Modified by Michelle Rodriguez on 01/14/2005.
            'We do not need to create a row for the StudentId and StuEnrollId fields.
            'Since this is a special page for the Student Ledger, there are several textboxes to hold 
            'the student name, program version, StuEnrollId, academic year, term and StudentId. 
            'Only the student name and program version are visible. 
            'Internally, we need either the StuEnrollId or the StudentId to compute the report.
            'In case we might need other filters, I have left this section of code below. However, we need
            'to exclude StudentId and StuEnrollId parameters from it because there are invisible textboxes 
            'that will hold the value depending on the user selection thru the Student Search pop-up.
            'ResourceId = 277 (Student Ledger report) 
            '       StudentId is RptParamId=181 and FldId=288
            '       StuEnrollId is RptParamId=182 and FldId=636
            'ResourceId = 295 (Single Student Class Schedule)
            '       StuEnrollId is RptParamId=219 and FldId=636
            ' US2729 12/23/2011 Janet Robinson logic to capture RptParam as I have noticed that it may not be the same across 
            '               school databases and code further down needs it
            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
            If (resourceId = 779 And dr("FldId") = 288) Then
                RptParam1 = dr("RptParamId")
            End If
            If (resourceId = 779 And dr("FldId") = 636) Then
                RptParam2 = dr("RptParamId")
            End If
            ' US2729 12/23/2011 Janet Robinson added resourceid 771
            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
            If dr("FilterOtherSec") = "True" And
                    Not (resourceId = 277 And dr("RptParamId") = 181 And dr("FldId") = 288) And
                    Not (resourceId = 277 And dr("RptParamId") = 182 And dr("FldId") = 636) And
                    Not (resourceId = 295 And dr("RptParamId") = 219 And dr("FldId") = 636) And
                    Not (resourceId = 779 And dr("FldId") = 288) And
                    Not (resourceId = 779 And dr("FldId") = 636) Then
                pnl4.Visible = True
                'Add a row to the tblOthers table. This is a server control.
                Dim r As New TableRow
                Dim c0 As New TableCell
                Dim c1 As New TableCell
                Dim c2 As New TableCell
                Dim c3 As TableCell
                Dim c4 As TableCell
                Dim c5 As New TableCell
                Dim lbl As New Label
                Dim ddl As New DropDownList
                Dim txt As New TextBox
                Dim txt2 As New TextBox
                Dim fldType As String = dr("FldType").ToString
                Dim regExpVal As RegularExpressionValidator
                Dim reqFldVal As RequiredFieldValidator

                txt.ID = "txt" & dr("RptParamId").ToString()
                txt.CssClass = "TextBox"
                txt.Width = Unit.Pixel(120)
                txt.ClientIDMode = UI.ClientIDMode.Static


                txt2.ID = "txt2" & dr("RptParamId").ToString()
                txt2.CssClass = "textbox"           '"ToTheme2"
                txt2.Width = Unit.Pixel(120)
                txt2.Visible = False
                txt2.ClientIDMode = UI.ClientIDMode.Static

                lbl.Text = dr("Caption")
                lbl.CssClass = "Label"
                c0.Controls.Add(lbl)

                ddl.ID = "ddl" & dr("RptParamId").ToString()
                'ddl.DataSource = ds.Tables("Operators")
                'Populate dropdownlist with operators relevant to the diferent data types.
                Select Case dr("FldType")
                    Case "Datetime"
                        ''New Code Added By Vijay Ramteke On July 22, 2010
                        ''If (resourceId = 295 And dr("FldId") = 65) Or (resourceId = 277 And dr("FldId") = 379) Then
                        If (resourceId = 295 And dr("FldId") = 65) Or (resourceId = 277 And dr("FldId") = 379) Or (resourceId = 637 And dr("FldId") = 952) Then
                            ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                            txt2.Visible = True
                            ' US2729 12/23/2011 Janet Robinson added resourceid 771
                            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                        ElseIf (resourceId = 779 And dr("FldId") = 65) Then
                            ddl.DataSource = New DataView(ds.Tables("Operators"), "CompOpId=" & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
                            txt2.Visible = False
                        Else
                            ''New Code Added By Vijay Ramteke On July 22, 2010
                            ddl.DataSource = New DataView(ds.Tables("Operators"), "AllowDateTime = 1", "", DataViewRowState.OriginalRows)
                        End If

                    Case "Varchar", "Char"
                        ddl.DataSource = New DataView(ds.Tables("Operators"), "AllowString = 1", "", DataViewRowState.OriginalRows)

                    Case "Smallint", "Int", "TinyInt", "Float", "Decimal"
                        ddl.DataSource = New DataView(ds.Tables("Operators"), "AllowNumber = 1", "", DataViewRowState.OriginalRows)

                    Case Else
                        ddl.DataSource = ds.Tables("Operators")
                End Select
                ddl.DataTextField = "CompOpText"
                ddl.DataValueField = "CompOpId"
                ddl.DataBind()
                ddl.CssClass = "dropdownlist"
                ddl.AutoPostBack = True
                c1.Controls.Add(ddl)
                c2.Controls.Add(txt)
                c5.Controls.Add(txt2)

                r.Cells.Add(c0)
                r.Cells.Add(c1)
                r.Cells.Add(c2)
                r.Cells.Add(c5)

                If fldType = "Datetime" Then
                    'Add a regular expression validator for Datetime fields.
                    'We can only accept dates with two slashes.
                    regExpVal = New RegularExpressionValidator
                    regExpVal.ID = "regExpVal" & dr("RptParamId").ToString()
                    regExpVal.ControlToValidate = txt.ID
                    regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                    'regExpVal.ValidationExpression = "(\d{1,2}/\d{1,2}/(\d{2,2}|\d{4,4}))(;(\d{1,2}/\d{1,2}/(\d{2,2}|\d{4,4})))*"
                    regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                    '& vbCr & "The character required to separate values is the semicolon (;)."
                    regExpVal.Display = ValidatorDisplay.None
                    c3 = New TableCell
                    c3.Controls.Add(regExpVal)
                    '
                    regExpVal = New RegularExpressionValidator
                    regExpVal.ID = "regExpVal2" & dr("RptParamId").ToString()
                    regExpVal.ControlToValidate = txt2.ID
                    regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                    'regExpVal.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                    regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                    '& vbCr & "The character required to separate values is the semicolon (;)."
                    regExpVal.Display = ValidatorDisplay.None
                    c3.Controls.Add(regExpVal)
                    r.Cells.Add(c3)
                End If

                If dr("Required") = "True" Then
                    'Add a required field validator if this field is marked as 'Required'.
                    reqFldVal = New RequiredFieldValidator
                    reqFldVal.ID = "reqFldVal" & dr("RptParamId").ToString()
                    reqFldVal.ControlToValidate = txt.ID
                    reqFldVal.ErrorMessage = dr("Caption") & " is a required filter."
                    reqFldVal.Display = ValidatorDisplay.None
                    c4 = New TableCell
                    c4.Controls.Add(reqFldVal)
                    r.Cells.Add(c4)
                End If

                tblFilterOthers.Rows.Add(r)
            End If

            ' Show Required Filters panel.
            If dr("Required") = "True" Then
                pnl5.Visible = True
                If lblRequiredFilters.Text = "" Then
                    lblRequiredFilters.Text = dr("Caption")
                Else
                    lblRequiredFilters.Text &= ", " & dr("Caption")
                End If
            End If
        Next

        'Add a 'None' item so that users can deselect an item without having to
        'select another one.
        If lbxfilterlist.Items.Count > 0 Then 'And Not resourceId = 615 Then
            lbxfilterlist.Items.Insert(0, New ListItem("None", 0))
        End If

        'Add the ds DataSet to session so we can reuse it later on.
        Session("WorkingDS") = ds
    End Sub

    Private Sub BuildFilterOther()
        Dim resID As Integer
        Dim dt, dt2 As DataTable
        Dim dtMasks As DataTable
        Dim dr As DataRow
        Dim objRptParams As New RptParams
        Dim objCommon As New FAME.Common.CommonUtilities

        resID = CInt(ViewState("resid"))
        'Get the parameters for this report that belong to the FilterOther section.
        dt = objRptParams.GetRptParamsForFilterOther(resID)
        'Get the comparison operators
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("Operators")
        'Get the input masks
        dtMasks = DirectCast(Session("WorkingDS"), DataSet).Tables("InputMasks")

        'Loop through the rows in the dt and add each parameter. 
        For Each dr In dt.Rows
            'Special case - Modified by Michelle Rodriguez on 01/14/2005.
            'We do not need to create a row for the StudentId and StuEnrollId fields.
            'Since this is a special page for the Student Ledger, there are several textboxes to hold 
            'the student name, program version, StuEnrollId, academic year, term and StudentId. 
            'Only the student name and program version are visible. 
            'Internally, we need either the StuEnrollId or the StudentId to compute the report.
            'In case we might need other filters, I have left this section of code below. However, we need
            'to exclude StudentId and StuEnrollId parameters from it because there are invisible textboxes 
            'that will hold the value depending on the user selection thru the Student Search pop-up.
            'ResourceId = 277 (Student Ledger report) 
            '       StudentId is RptParamId=181 and FldId=288
            '       StuEnrollId is RptParamId=182 and FldId=636
            'ResourceId = 295 (Single Student Class Schedule)
            '       StuEnrollId is RptParamId=219 and FldId=636
            ' US2729 12/23/2011 Janet Robinson added resourceid 771
            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
            If Not (resourceId = 277 And dr("RptParamId") = 181 And dr("FldId") = 288) And
                    Not (resourceId = 277 And dr("RptParamId") = 182 And dr("FldId") = 636) And
                    Not (resourceId = 295 And dr("RptParamId") = 219 And dr("FldId") = 636) And
                    Not (resourceId = 779 And dr("FldId") = 288) And
                    Not (resourceId = 779 And dr("FldId") = 636) Then
                'Add a row to the tblOthers table. This is a server control.
                Dim r As New TableRow
                Dim c0 As New TableCell
                Dim c1 As New TableCell
                Dim c2 As New TableCell
                Dim c3 As TableCell
                Dim c4 As TableCell
                Dim c5 As New TableCell
                Dim lbl As New Label
                Dim ddl As New DropDownList
                Dim txt As New TextBox
                Dim txt2 As New TextBox
                Dim fldType As String = dr("FldType").ToString
                Dim regExpVal As RegularExpressionValidator
                Dim reqFldVal As RequiredFieldValidator

                txt.ID = "txt" & dr("RptParamId").ToString()
                txt.CssClass = "TextBox"
                txt.Width = Unit.Pixel(120)
                txt.ClientIDMode = UI.ClientIDMode.Static



                txt2.ID = "txt2" & dr("RptParamId").ToString()
                txt2.CssClass = "textbox"           '"ToTheme2"
                txt2.Width = Unit.Pixel(120)
                txt2.Visible = False
                txt2.ClientIDMode = UI.ClientIDMode.Static

                lbl.Text = dr("Caption")
                lbl.CssClass = "Label"
                c0.Controls.Add(lbl)

                ddl.ID = "ddl" & dr("RptParamId").ToString()

                'Populate dropdownlist with operators relevant to the diferent data types.
                Select Case dr("FldType")
                    Case "Datetime"

                        If (resourceId = 295 And dr("FldId") = 65) Or (resourceId = 277 And dr("FldId") = 379) Or (resourceId = 637 And dr("FldId") = 952) Then
                            ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.BetweenOperatorValue, "", DataViewRowState.OriginalRows)
                            txt2.Visible = True
                            ' US2729 12/23/2011 Janet Robinson added resourceid 771
                            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                        ElseIf resourceId = 779 And dr("FldId") = 65 Then
                            ddl.DataSource = New DataView(dt2, "CompOpId=" & AdvantageCommonValues.EqualToOperatorValue, "", DataViewRowState.OriginalRows)
                            txt2.Visible = False
                        Else
                            ddl.DataSource = New DataView(dt2, "AllowDateTime = 1", "", DataViewRowState.OriginalRows)
                        End If
                    Case "Varchar", "Char"
                        ddl.DataSource = New DataView(dt2, "AllowString = 1", "", DataViewRowState.OriginalRows)
                    Case "Smallint", "Int", "TinyInt", "Float", "Decimal"
                        ddl.DataSource = New DataView(dt2, "AllowNumber = 1", "", DataViewRowState.OriginalRows)
                    Case Else
                        ddl.DataSource = dt2
                End Select
                ddl.DataTextField = "CompOpText"
                ddl.DataValueField = "CompOpId"
                ddl.DataBind()
                'ddl.Width = Unit.Pixel(90)
                ddl.CssClass = "dropdownlist"
                ddl.AutoPostBack = True
                c1.Controls.Add(ddl)
                c2.Controls.Add(txt)
                c5.Controls.Add(txt2)

                r.Cells.Add(c0)
                r.Cells.Add(c1)
                r.Cells.Add(c2)
                r.Cells.Add(c5)

                If fldType = "Datetime" Then
                    'Add a regular expression validator for Datetime fields.
                    'We can only accept dates with two slashes.
                    regExpVal = New RegularExpressionValidator
                    regExpVal.ID = "regExpVal" & dr("RptParamId").ToString()
                    regExpVal.ControlToValidate = txt.ID
                    'regExpVal.ValidationExpression = "(\d{1,2}/\d{1,2}/(\d{2,2}|\d{4,4}))(;(\d{1,2}/\d{1,2}/(\d{2,2}|\d{4,4})))*"
                    regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                    regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                    '& vbCr & "The character required to separate values is the semicolon (;)."
                    regExpVal.Display = ValidatorDisplay.None
                    c3 = New TableCell
                    c3.Controls.Add(regExpVal)
                    '
                    regExpVal = New RegularExpressionValidator
                    regExpVal.ID = "regExpVal2" & dr("RptParamId").ToString()
                    regExpVal.ControlToValidate = txt2.ID
                    regExpVal.ValidationExpression = "((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2}))"        '(;((\d{1,2}/\d{1,2}/\d{4})|(\d{1,2}/\d{1,2}/\d{2})))*
                    'regExpVal.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                    regExpVal.ErrorMessage = "Invalid date format. Dates are expected as month/day/year. Month, day and year must be numeric values. Year must have at least two digits."
                    '& vbCr & "The character required to separate values is the semicolon (;)."
                    regExpVal.Display = ValidatorDisplay.None
                    c3.Controls.Add(regExpVal)
                    r.Cells.Add(c3)
                End If

                If dr("Required") = "True" Then
                    'Add a required field validator if this field is marked as 'Required'.
                    reqFldVal = New RequiredFieldValidator
                    reqFldVal.ID = "reqFldVal" & dr("RptParamId").ToString()
                    reqFldVal.ControlToValidate = txt.ID
                    reqFldVal.ErrorMessage = dr("Caption") & " is a required filter."
                    reqFldVal.Display = ValidatorDisplay.None
                    c4 = New TableCell
                    c4.Controls.Add(reqFldVal)
                    r.Cells.Add(c4)
                End If

                tblFilterOthers.Rows.Add(r)
            End If
        Next

        'Add a hidden textbox and a compare validator for fields marked as 'Required'.
        'This textbox need to be updated everytime it is selected or deselected.
        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        Dim rows() As DataRow = dt.Select("FilterListSec = 1 And Required = 1")

        For Each dr In rows
            Dim txt As New TextBox
            Dim compVal As New CompareValidator

            txt.ID = "txt" & dr("RptParamId").ToString()
            txt.CssClass = "ToTheme2"
            txt.Text = System.Guid.Empty.ToString
            txt.Width = Unit.Pixel(0)

            compVal.ID = "compVal" & dr("RptParamId").ToString()
            compVal.ControlToValidate = txt.ID
            compVal.[Operator] = ValidationCompareOperator.NotEqual
            compVal.ValueToCompare = System.Guid.Empty.ToString
            compVal.Type = ValidationDataType.String
            compVal.ErrorMessage = dr("Caption") & " is a required filter."
            compVal.Display = ValidatorDisplay.None

            pnlRequiredFieldValidators.Controls.Add(txt)
            pnlRequiredFieldValidators.Controls.Add(compVal)
        Next
    End Sub

    Private Sub BindDataListFromDB()
        Dim dt As DataTable
        Dim objRptParams As New RptParamsFacade

        dt = objRptParams.GetSavedPrefsList

        With dlstPrefs
            .DataSource = dt
            .DataBind()
        End With

        'Cache the dt so we can reuse it when the links are clicked.
        'There is no need to have to go to the database to repopulate the datalist
        'when a different preference is selected.
        Cache("Prefs") = dt
    End Sub

    Private Sub BindDataListFromCache()
        Dim dt As DataTable
        dt = DirectCast(Cache("Prefs"), DataTable)
        With dlstPrefs
            .DataSource = dt
            .DataBind()
        End With
    End Sub

    Private Sub dlstPrefs_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstPrefs.ItemCommand
        Dim prefId As String
        Dim dt, dt2 As DataTable
        Dim dr, dr2 As DataRow
        Dim objRptParams As New RptParamsFacade
        Dim ds As New DataSet
        Dim rptParamId As Integer
        Dim dRows As DataRow()
        Dim iCounter As Integer
        Dim objCommon As New FAME.Common.CommonUtilities
        Dim ctlTextBox As Control
        Dim ctlDDL As Control
        Dim found As Boolean

        dlstPrefs.SelectedIndex = e.Item.ItemIndex

        If Not IsNothing(Cache("Prefs")) Then
            BindDataListFromCache()
        Else
            BindDataListFromDB()
        End If

        'Clear the relevant sections
        Clear()

        'There is no need to bring the preference name from the database.
        txtPrefName.Text = DirectCast(e.CommandSource, LinkButton).Text     'e.CommandArgument

        'Get the DataSet containing the preferences
        prefId = dlstPrefs.DataKeys(e.Item.ItemIndex).ToString()
        ds = objRptParams.GetUserRptPrefs(prefId)

        'Set txtPrefId to the preference id of the selected item
        txtPrefId.Text = prefId
        'This section deals with displaying the Sort Preferences.
        dt = ds.Tables("SortPrefs")

        'Get the datatable from session that contains the details of each rptparam
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        With dt2
            .PrimaryKey = New DataColumn() { .Columns("RptParamId")}
        End With

        'We need to get the captions for the rows in the SortPrefs dt
        For Each dr In dt.Rows
            dr2 = dt2.Rows.Find(dr("RptParamId"))
            dr("Caption") = dr2("Caption")
        Next

        With lbxselsort
            .DataSource = dt
            .DataTextField = "Caption"
            .DataValueField = "RptParamId"
            .DataBind()
        End With

        'When the page is first loaded we populate the lbxSort control with all the
        'sort parameters for the report. When a preference is selected we need to
        'make certain that the lbxSort control only displays the sort parameters that
        'are not already selected.
        If lbxsort.Items.Count > 0 Then
            lbxsort.Items.Clear()
        End If

        For Each dr In dt2.Rows
            If dr("SortSec") = True Then
                found = False
                For iCounter = 0 To lbxselsort.Items.Count - 1
                    If lbxselsort.Items(iCounter).Value = dr("RptParamId") Then
                        found = True
                    End If
                Next
                If found = False Then
                    lbxsort.Items.Add(New ListItem(dr("Caption"), dr("RptParamId")))
                End If
            End If

        Next

        'This section deals with storing and selecting the relevant FilterList prefs.
        'Set to empty GUID all hidden textboxes in pnlRequiredFieldValidators panel.
        For Each ctrl As Control In pnlRequiredFieldValidators.Controls
            If TypeOf ctrl Is TextBox Then
                DirectCast(ctrl, TextBox).Text = System.Guid.Empty.ToString
            End If
        Next

        dt = ds.Tables("FilterListPrefs")
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")

        For Each dr In dt.Rows
            dr2 = dt2.NewRow
            dr2("RptParamId") = dr("RptParamId")
            dr2("FldValue") = dr("FldValue")
            dt2.Rows.Add(dr2)
            '
            ctlTextBox = pnlRequiredFieldValidators.FindControl("txt" & dr("RptParamId").ToString)
            If Not (ctlTextBox Is Nothing) Then
                If DirectCast(ctlTextBox, TextBox).Text = System.Guid.Empty.ToString Then
                    DirectCast(ctlTextBox, TextBox).Text = dr("FldValue")
                Else
                    DirectCast(ctlTextBox, TextBox).Text &= "," & dr("FldValue")
                End If
            End If
        Next

        'If an item is selected in the lbxFilterList listbox then we need to select
        'the values, if any, that the user had selected for that item.
        If lbxfilterlist.SelectedIndex <> -1 Then
            rptParamId = lbxfilterlist.SelectedItem.Value
            'Search the FilterListSelections dt to see if the user had selected any
            'values for this item.
            dRows = dt2.Select("RptParamId = " & rptParamId)
            If dRows.Length > 0 Then
                For Each dr In dRows
                    'Loop through and select relevant item in the lbxSelFilterList control.
                    For iCounter = 0 To lbxselfilterlist.Items.Count - 1
                        If lbxselfilterlist.Items(iCounter).Value.ToString = dr("FldValue").ToString() Then
                            lbxselfilterlist.Items(iCounter).Selected = True
                        End If
                    Next
                Next
            End If

        End If

        'This section deals with the FilterOther prefs.
        dt = ds.Tables("FilterOtherPrefs")
        If dt.Rows.Count > 0 Then
            pnl4.Visible = True
            'For each row we need to select the appropriate entry in the ddl and enter the appropriate
            'value in the textbox.
            For Each dr In dt.Rows

                ' US2729 12/23/2011 Janet Robinson added resourceid 771
                ' DE7194 2/14/2012 Janet Robinson 
                'If dr("RptParamId") = 181 Then
                If dr("RptParamId") = 181 OrElse dr("FldId") = 288 Then
                    'Special case: Need to get StudentName associated to StudentId.
                    txtstudentidentifier.Value = dr("OpValue")
                    txtstuenrollmentid.Value = System.Guid.Empty.ToString
                    Dim rptUtils As New ReportCommonUtilsFacade
                    Dim stuDT As DataTable
                    stuDT = rptUtils.GetStudentName(txtstudentidentifier.Value)
                    If stuDT.Rows.Count > 0 Then
                        txtStudentId.Text = stuDT.Rows(0)("StudentName")
                        txtStuEnrollment.Text = "All Enrollments"
                    End If
                    '
                    ' US2729 12/23/2011 Janet Robinson added resourceid 771
                    ' DE7194 2/14/2012 Janet Robinson 
                ElseIf dr("RptParamId") = 182 OrElse dr("FldId") = 636 Then
                    'Special case: Need to get StudentName and PrgVerDescription associated to StuEnrollId.
                    txtstuenrollmentid.Value = dr("OpValue")
                    Dim rptUtils As New ReportCommonUtilsFacade
                    Dim stuDT As DataTable
                    stuDT = rptUtils.GetStuNameAndPrgVersion(txtstuenrollmentid.Value)
                    If stuDT.Rows.Count > 0 Then
                        txtStudentId.Text = stuDT.Rows(0)("StudentName")
                        txtStuEnrollment.Text = stuDT.Rows(0)("PrgVerDescrip")
                    End If
                    '
                Else
                    '
                    ctlDDL = Master.FindControl("ddl" & dr("RptParamId").ToString())
                    ''Added by saraswathi to avaoid error page when using between in saving the report
                    ''Added on july 10 2009
                    If Not ctlDDL Is Nothing Then
                        objCommon.SelValInDDL(DirectCast(ctlDDL, DropDownList), dr("OpId").ToString())
                        ctlTextBox = Master.FindControl("txt" & dr("RptParamId").ToString())
                        If dr("OpId").ToString() = AdvantageCommonValues.BetweenOperatorValue.ToString Then
                            'Special case: When the operator is Between
                            Dim strSplit() As String = dr("OpValue").ToString.Split(";")
                            If strSplit.GetLength(0) > 1 Then
                                DirectCast(ctlTextBox, TextBox).Text = strSplit(0)
                                Dim ctlTextBox2 As Control = Master.FindControl("txt2" & dr("RptParamId").ToString())
                                DirectCast(ctlTextBox2, TextBox).Text = strSplit(1)
                            ElseIf strSplit.GetLength(0) = 1 Then
                                DirectCast(ctlTextBox, TextBox).Text = strSplit(0)
                            End If
                        Else
                            DirectCast(ctlTextBox, TextBox).Text = dr("OpValue")
                        End If
                    End If
                    'ctlTextBox = Form1.FindControl("txt" & dr("RptParamId").ToString())
                    'ctlDDL = Form1.FindControl("ddl" & dr("RptParamId").ToString())
                    'DirectCast(ctlTextBox, TextBox).Text = dr("OpValue")
                    'objCommon.SelValInDDL(DirectCast(ctlDDL, DropDownList), dr("OpId").ToString())
                End If
            Next

        End If

        'Set Mode to EDIT
        'SetMode("EDIT")
        'objCommon.SetBtnState(Form1, "EDIT", pObj)
        objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
        ViewState("MODE") = "EDIT"

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Text, ViewState, Header1)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnadd.Click
        'The code in this sub should only execute if an item is selected
        'in the lbxSort listbox.
        If lbxsort.SelectedIndex <> -1 Then
            lbxselsort.Items.Add(New ListItem(lbxsort.SelectedItem.Text, lbxsort.SelectedItem.Value))
            lbxsort.Items.RemoveAt(lbxsort.SelectedIndex)
            If lbxsort.Items.Count > 0 Then
                lbxsort.SelectedIndex = lbxsort.SelectedIndex + 1
            End If
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnremove.Click
        'The code in this sub should only execute if an item is selected in 
        'the lbxSelSort listbox.
        If lbxselsort.SelectedIndex <> -1 Then
            lbxsort.Items.Add(New ListItem(lbxselsort.SelectedItem.Text, lbxselsort.SelectedItem.Value))
            lbxselsort.Items.RemoveAt(lbxselsort.SelectedIndex)
        End If
    End Sub

    'Private Sub SetMode(ByVal strMode As String)
    '    Select Case strMode
    '        Case "NEW"
    '            btnNew.Enabled = False
    '            btnDelete.Enabled = False
    '            Viewstate("MODE") = "NEW"
    '            txtPrefId.Text = ""
    '        Case "EDIT"
    '            btnNew.Enabled = True
    '            btnDelete.Enabled = True
    '            viewstate("MODE") = "EDIT"
    '    End Select
    'End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim strPromptMsg As String = "Please type name for Report Selection: "
        'If txtPrefName.Text = "" Then
        '    'Prompt user for Report Selection name.
        '    CommonWebUtilities.PromptTextBox(Me.Page, strPromptMsg, "Form1", Me.txtPrefName.ClientID)
        'Else
        '    'Prompt user for new name for current Report Selection.
        '    CommonWebUtilities.PromptDefaultValueTextBox(Me.Page, strPromptMsg, "Form1", Me.txtPrefName.ClientID)
        'End If
        SavePreferences()
    End Sub

    Private Function ValidateFilterOthers() As Boolean
        Dim selOpId As Integer
        Dim rCounter As Integer
        Dim tbxText As String
        Dim tbxText2 As String
        Dim strItem As String
        Dim arrValues() As String
        Dim selIndex As String
        Dim errorMessage As String = ""
        Dim opName As String
        Dim fieldName As String
        Dim fieldId As Integer
        Dim fldMask As String
        Dim fldLen As Integer
        Dim dType As String
        Dim rptParamId As Integer
        Dim objcommon As New FAME.Common.CommonUtilities

        'We need to loop through the entries in the tblFilterOthers table.
        For rCounter = 1 To tblFilterOthers.Rows.Count - 1
            tbxText = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).Text
            tbxText2 = DirectCast(tblFilterOthers.Rows(rCounter).Cells(3).Controls(0), TextBox).Text
            'Ignore rows where nothing is entered into the textbox.
            If tbxText <> "" Then
                arrValues = tbxText.Split(";")
                selIndex = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedIndex
                opName = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text
                fieldName = DirectCast(tblFilterOthers.Rows(rCounter).Cells(0).Controls(0), Label).Text
                rptParamId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                fieldId = GetFldId(rptParamId)
                fldMask = GetInputMask(fieldId)
                fldLen = tbxText.Length

                If selIndex >= 0 Then   'Make sure an entry is selected in the ddl
                    selOpId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                    If IsSingleValOp(selOpId) And arrValues.Length > 1 Then  'single value operator
                        'There should only be one entry in the textbox. There should be
                        'no semicolons (;) which is used to separate multiple values such as
                        'when using the BETWEEN or the IN LIST operators.
                        errorMessage &= "Operator " & opName & " cannot be used with multiple values" & vbCr
                    ElseIf opName = "Is Null" Then
                        'When the operator selected is "Is Null" then there is no point going
                        'any further since Is Null should not have a value specified.
                        errorMessage &= "You cannot specify a value when using the Is Null operator" & vbCr
                    ElseIf opName = "Between" And (tbxText = "" Or tbxText2 = "") Then
                        'Between must have exactly two values specified to be compared.
                        errorMessage &= "You must specify exactly two values when using the Between operator." & vbCr
                    Else
                        'It is okay to proceed with validating the entries.
                        'Loop through the entries in the arrValues array and validate.
                        For Each strItem In arrValues
                            'Verify the datatype of each entry in the arrValues array. We need to get the FldType from
                            'the RptParams dt stored in session.
                            dType = GetFldType(rptParamId)
                            If Not IsValidDataType(strItem, dType) Then
                                errorMessage &= "Incorrect datatype entered for " & fieldName & vbCr
                            Else
                                'Verify the input mask if there is one.
                                If fldMask <> "" Then
                                    If Not objcommon.ValidateInputMask(fldMask, strItem) Then
                                        errorMessage &= "Incorrect format entered for " & fieldName & vbCr
                                    Else
                                        'Verify the field length if the field type is
                                        'Char or Varchar.
                                        If dType = "Varchar" Or dType = "Char" Then
                                            If fldLen > GetFldLen(rptParamId) Then
                                                errorMessage &= fieldName & " cannot be more than " & GetFldLen(rptParamId).ToString & " characters" & vbCr
                                            End If
                                        End If
                                    End If  'Validate input mask
                                End If  'Input mask exists
                            End If  'Valid data type
                        Next    'Loop through the items in the arrValues array
                    End If
                End If  'Entry is selected in the ddl
            End If  'Textbox is not empty
        Next    'loop through the entries in the tblFilterOThers table
        If errorMessage = "" Then
            Return True
        Else
            'lblErrorMessage.Text = "Please review the following errors:" & errorMessage
            DisplayErrorMessage(errorMessage)
            Return False
        End If
    End Function

    Private Function ValidateRequiredFilters() As String
        Dim errorMessage As String = ""
        Dim filterName As String

        If txtStuEnrollment.Text = "" Then
            filterName = lblStudent.Text
            errorMessage &= vbCrLf & filterName & " is a required filter."
            filterName = lblEnrollmentId.Text
            errorMessage &= vbCrLf & filterName & " is a required filter."
        End If
        Return errorMessage
    End Function

    Private Function IsSingleValOp(ByVal opId As Integer) As Boolean
        Select Case opId
            Case 5, 6
                Return False
            Case Else
                Return True
        End Select
    End Function

    Private Function HasSingleEntry(ByVal sVal As String) As Boolean
        'Search the string to see if it contains a semicolon.
        If sVal.IndexOf(";") >= 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Function IsValidDataType(ByVal sVal As String, ByVal dataType As String) As Boolean
        Dim iResult As Integer
        Dim sResult As String
        Dim dResult As Decimal
        Dim fResult As Double
        Dim dtmResult As DateTime
        Dim cResult As Char

        Select Case dataType
            Case "Int"
                Try
                    iResult = Convert.ToInt32(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "Float"
                Try
                    fResult = Convert.ToDouble(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "Money"
                Try
                    dResult = Convert.ToDecimal(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "Char"
                Try
                    cResult = Convert.ToChar(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "Datetime"
                Try
                    dtmResult = Convert.ToDateTime(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "Varchar"
                Try
                    sResult = Convert.ToString(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case "TinyInt"
                Try
                    iResult = Convert.ToInt32(sVal)
                    Return True
                Catch
                    'The conversion could not take place so invalid datatype
                    Return False
                End Try
            Case Else
                Throw New System.Exception("Invalid Field Type: " & dataType)
        End Select
    End Function

    Private Function GetFldType(ByVal rptParamId As Integer) As String
        Dim dt As DataTable
        Dim drRows() As DataRow
        Dim fldType As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        drRows = dt.Select("RptParamId = " & rptParamId)
        fldType = drRows(0)("FldType")
        Return fldType
    End Function

    Private Function GetFldId(ByVal rptParamId As Integer) As Integer
        Dim dt As DataTable
        Dim dr As DataRow
        Dim fldId As Integer

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        With dt
            .PrimaryKey = New DataColumn() { .Columns("RptParamId")}
        End With
        dr = dt.Rows.Find(rptParamId)
        fldId = dr("FldId")
        Return fldId
    End Function

    Private Function GetFldName(ByVal rptParamId As Integer) As String
        Dim dt As DataTable
        Dim drRows() As DataRow
        Dim fldName As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        drRows = dt.Select("RptParamId = " & rptParamId)
        fldName = drRows(0)("FldName")
        Return fldName
    End Function

    Private Function GetTblName(ByVal rptParamId As Integer) As String
        Dim dt As DataTable
        Dim drRows() As DataRow
        Dim tblName As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        drRows = dt.Select("RptParamId = " & rptParamId)
        tblName = drRows(0)("TblName")
        Return tblName
    End Function

    Private Function GetInputMask(ByVal fldId As Integer) As String
        Dim dt As DataTable
        Dim dr As DataRow
        Dim sMask As String

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("InputMasks")
        dr = dt.Rows.Find(fldId)
        If dr Is Nothing Then
            sMask = ""
        Else
            sMask = dr("Mask")
        End If
        Return sMask
    End Function

    Private Function GetFldLen(ByVal rptParamId As Integer) As Integer
        Dim dt As DataTable
        Dim dr As DataRow
        Dim fldLen As Integer

        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
        dr = dt.Rows.Find(rptParamId)
        fldLen = dr("FldLen")
        Return fldLen
    End Function

    Private Sub lbxFilterList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxfilterlist.SelectedIndexChanged
        'When an item is selected we want to display the list of possible values for the item.
        'We can retrieve the list of values for this item from the FilterListValues stored
        'in the ds that is in session.
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim aRows As DataRow()
        Dim lbxRows As Integer
        Dim arrRows As Integer

        ds = DirectCast(Session("WorkingDS"), DataSet)
        dt = ds.Tables("FilterListValues")

        'If 'None' item was selected, we need to empty the FilterListSelections dt in session.
        'Otherwise, we want to get a list of all the rows in dt that have the corresponding RptParamId
        'that is selected in the lbxFilterList listbox.

        If lbxfilterlist.SelectedItem.Value = 0 Then
            'Clear the items from the lbxSelFilterList listbox if there are any.
            If lbxselfilterlist.Items.Count > 0 Then
                lbxselfilterlist.Items.Clear()
            End If
            'If the FilterListSelections dt in session is not empty then we need to empty it.
            dt = ds.Tables("FilterListSelections")
            If dt.Rows.Count > 0 Then
                dt.Rows.Clear() 'Empty dt table.
            End If
            'Set to empty GUID all textboxes in panel pnlRequiredFieldValidators.
            For Each ctrl As Control In pnlRequiredFieldValidators.Controls
                If TypeOf ctrl Is TextBox Then
                    DirectCast(ctrl, TextBox).Text = System.Guid.Empty.ToString
                End If
            Next
            ViewState("CampGrpId_Filter") = ""
        Else

            If resourceId = 615 And Not txtStudentId.Text = "" Then
                Dim strErrorMessage As String = ""
                strErrorMessage = "The selected report filters combination is incorrect. Only one of the following report filters combination is allowed " & vbCrLf
                strErrorMessage &= vbCrLf
                strErrorMessage &= "                                            Student and Enrollment Filter  " & vbCrLf
                strErrorMessage &= vbCrLf
                strErrorMessage &= "                                                            OR               " & vbCrLf
                strErrorMessage &= vbCrLf
                strErrorMessage &= "                                           " & lbxfilterlist.SelectedItem.Text & " Filter           " & vbCrLf
                DisplayErrorMessage(strErrorMessage)
                Exit Sub
            End If

            If resourceId = 615 And Not lbxfilterlist.SelectedItem.Text = "Campus Group" And txtStudentId.Text = "" Then
                If ViewState("CampGrpId_Filter") = "" Then
                    DisplayErrorMessage("Campus Group is required. Please select campus group before selecting other filters ")
                    Exit Sub
                End If
            End If

            'We want to get a list of all the rows in dt that have the corresponding RptParamId
            'that is selected in the lbxFilterList listbox.
            aRows = dt.Select("RptParamId = " & lbxfilterlist.SelectedItem.Value)

            'Clear the items from the lbxSelFilterList lisbox if there are any.
            If lbxselfilterlist.Items.Count > 0 Then
                lbxselfilterlist.Items.Clear()
            End If
            'Add the rows from aRows to the lbxSelFilterList listbox.
            For Each dr In aRows
                lbxselfilterlist.Items.Add(New ListItem(dr("DisplayFld"), dr("ValueFld")))
            Next
            'Add a 'None' item so that users can deselect an item without having to
            'select another one.
            lbxselfilterlist.Items.Insert(0, "None")

            'If the FilterListSelections dt in session is not empty then we need
            'to see if it has any entries for the item selected here. If it does then
            'we need to select the relevant entries in the lbxSelFilterList listbox.
            dt = ds.Tables("FilterListSelections")

            If dt.Rows.Count > 0 Then
                aRows = dt.Select("RptParamId = " & lbxfilterlist.SelectedItem.Value)
                If aRows.Length > 0 Then
                    'Loop through the items in the lbxSelFilterList listbox and
                    'select the relevant items.
                    For arrRows = 0 To aRows.Length - 1
                        For lbxRows = 0 To lbxselfilterlist.Items.Count - 1
                            If lbxselfilterlist.Items(lbxRows).Value.ToString = aRows(arrRows)("FldValue").ToString() Then
                                lbxselfilterlist.Items(lbxRows).Selected = True
                            End If
                        Next
                    Next
                End If 'search did not return 0 rows
            End If 'dt is not empty
        End If ''None' item was selected
    End Sub

    Private Sub lbxSelFilterList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxselfilterlist.SelectedIndexChanged
        'When this event is fired it is important to remember that it could be that
        'the user has selected OR deselected a value. Also, this lisbox supports multiple
        'selections so we cannot simply get the item selected or unselected.
        'Each time this event fires we have to examine each item in the listbox. If the
        'item is not selected then if it exists in the dt we will have to remove it. If
        'the item is selected but not exists in the dt then we will have to add it.
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dt2 As DataTable
        Dim dr As DataRow
        Dim paramId As Integer
        Dim selValue As String
        Dim temp As String
        Dim ctlTextBox As Control
        Dim row() As DataRow

        'If resourceId = 615 And paramId = 1201 Then
        '    ViewState("CampGrpId_Filter") = ""
        'End If

        ds = DirectCast(Session("WorkingDS"), DataSet)
        dt = ds.Tables("FilterListSelections")
        dt2 = ds.Tables("RptParams")
        paramId = lbxfilterlist.SelectedItem.Value

        For i = 0 To lbxselfilterlist.Items.Count - 1
            selValue = lbxselfilterlist.Items(i).Value
            If lbxselfilterlist.Items(i).Selected = False And i > 0 Then
                If ItemExistsInFilterListSelections(i) Then
                    'We need to remove the item from the dt
                    Dim objCriteria() As Object = {paramId, selValue}
                    dr = dt.Rows.Find(objCriteria)
                    dt.Rows.Remove(dr)
                    'If field is 'Required', set hidden textbox to empty.
                    row = dt2.Select("RptParamId = " & paramId & " AND Required = 1")
                    If row.GetLength(0) = 1 Then
                        ctlTextBox = Master.FindControl("txt" & paramId)
                        If DirectCast(ctlTextBox, TextBox).Text.IndexOf(",") = -1 Then
                            'Only one entry.
                            DirectCast(ctlTextBox, TextBox).Text = System.Guid.Empty.ToString
                        Else
                            temp = DirectCast(ctlTextBox, TextBox).Text
                            temp = temp.Replace(selValue, ",")
                            If temp.StartsWith(",,") Then
                                DirectCast(ctlTextBox, TextBox).Text = temp.Substring(2)
                            ElseIf temp.EndsWith(",,") Then
                                DirectCast(ctlTextBox, TextBox).Text = temp.Substring(0, temp.Length - 2)
                            Else
                                DirectCast(ctlTextBox, TextBox).Text = temp.Replace(",,,", ",")
                            End If
                        End If
                    End If
                End If
            Else
                If Not ItemExistsInFilterListSelections(i) And i > 0 Then
                    'We need to add the item to the dt
                    If lbxselfilterlist.Items(i).Text = "All" Then
                        lbxselfilterlist.ClearSelection()
                        lbxselfilterlist.SelectionMode = ListSelectionMode.Single
                        lbxselfilterlist.SelectedValue = selValue
                        txtAllCampGrps.Text = "All"
                    Else
                        lbxselfilterlist.SelectionMode = ListSelectionMode.Multiple
                        txtAllCampGrps.Text = ""
                    End If
                    'We need to add the item to the dt
                    dr = dt.NewRow
                    dr("RptParamId") = paramId      'lbxFilterList.SelectedItem.Value
                    dr("FldValue") = selValue       'lbxSelFilterList.Items(i).Value

                    If resourceId = 615 And paramId = 1201 And selValue.Length >= 36 Then
                        ViewState("CampGrpId_Filter") = "Selected"
                    End If
                    dt.Rows.Add(dr)
                    'If field is 'Required', set hidden textbox to newly selected value.
                    row = dt2.Select("RptParamId = " & paramId & " AND Required = 1")
                    If row.GetLength(0) = 1 Then
                        ctlTextBox = Master.FindControl("txt" & paramId)
                        If DirectCast(ctlTextBox, TextBox).Text = System.Guid.Empty.ToString Then
                            DirectCast(ctlTextBox, TextBox).Text = selValue
                        Else
                            DirectCast(ctlTextBox, TextBox).Text &= "," & selValue
                        End If
                    End If
                End If
            End If
        Next

        Session("WorkingDS") = ds
    End Sub

    Private Function ItemExistsInFilterListSelections(ByVal itemIndex As Integer) As Boolean
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim paramId As Integer
        Dim selValue As String

        paramId = lbxfilterlist.SelectedItem.Value
        selValue = lbxselfilterlist.Items(itemIndex).Value

        ds = DirectCast(Session("WorkingDS"), DataSet)
        dt = ds.Tables("FilterListSelections")

        If dt.Rows.Count = 0 Then
            Return False
        Else
            Dim objCriteria() As Object = {paramId, selValue}
            dr = dt.Rows.Find(objCriteria)
            If dr Is Nothing Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objCommon As New FAME.Common.CommonUtilities
        Dim dt As DataTable
        Dim dr As DataRow

        Try
            Clear()
            'SetMode("NEW")
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"

            'Place the prefid guid into the txtPrefId textbox so it can be reused
            'if we need to do an update later on.
            txtPrefId.Text = ""

            'We have to rebind the lbxSort control to the report parameters that are 
            'stored in session
            lbxsort.Items.Clear()
            dt = DirectCast(Session("WorkingDS"), DataSet).Tables("RptParams")
            For Each dr In dt.Rows
                If dr("SortSec") = True Then
                    lbxsort.Items.Add(New ListItem(dr("Caption"), dr("RptParamId")))
                End If
            Next

            'Make sure no item is selected in the Preferences datalist.
            dlstPrefs.SelectedIndex = -1

            'Rebind the Preferences datalist from the database.
            BindDataListFromDB()

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub Clear()
        Dim dt As DataTable
        Dim trCounter As Integer
        Dim ctrlDDL As Control
        Dim objCommon As New FAME.Common.CommonUtilities

        'Clear the txtPrefName textbox control.
        txtPrefName.Text = ""
        'Clear the lbxSelSort listbox control
        lbxselsort.Items.Clear()
        'Make sure no item is selected in the lbxSort listbox.
        lbxsort.SelectedIndex = -1
        'Clear the entries from the FilterListSelections dt
        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")
        If dt.Rows.Count > 0 Then
            dt.Clear()
        End If
        'Make sure no entry is selected in the lbxSelFilterList listbox
        lbxselfilterlist.SelectedIndex = -1
        'Get operators table
        Dim dt2 As DataTable = DirectCast(Session("WorkingDS"), DataSet).Tables("Operators")
        'Clear the textboxes in the FilterOther section and set dropdownlists to 'Equal To'.
        ClearStuEnrollmentFields()
        For trCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
            DirectCast(tblFilterOthers.Rows(trCounter).Cells(2).Controls(0), TextBox).Text = ""
            DirectCast(tblFilterOthers.Rows(trCounter).Cells(3).Controls(0), TextBox).Text = ""
            ctrlDDL = DirectCast(tblFilterOthers.Rows(trCounter).Cells(1).Controls(0), DropDownList)
            objCommon.SelValInDDL(DirectCast(ctrlDDL, DropDownList), dt2.Rows(0)("CompOpId").ToString)
        Next
        'Set to empty GUID all hidden textboxes in pnlRequiredFieldValidators panel.
        For Each ctrl As Control In pnlRequiredFieldValidators.Controls
            If TypeOf ctrl Is TextBox Then
                DirectCast(ctrl, TextBox).Text = System.Guid.Empty.ToString
            End If
        Next
    End Sub

    Private Sub ClearStuEnrollmentFields()
        txtStudentId.Text = ""
        txtstuenrollmentid.Value = ""
        txtStuEnrollment.Text = ""
        txttermid.Value = ""
        txtterm.Value = ""
        txtacademicyearid.Value = ""
        txtacademicyear.Value = ""
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim objRptParams As New RptParamsFacade
        Dim objCommon As New FAME.Common.CommonUtilities

        'Delete all the preference info from the datatbase
        objRptParams.DeleteUserPrefInfo(txtPrefId.Text)
        'Clear the rhs of the screen
        Clear()
        'Rebind the datalist from the database
        BindDataListFromDB()
        'Set mode to NEW
        'SetMode("NEW")
        'objCommon.SetBtnState(Form1, "NEW", pObj)
        objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
        ViewState("MODE") = "NEW"
        'Place the prefid guid into the txtPrefId textbox so it can be reused
        'if we need to do an update later on.
        txtPrefId.Text = ""
        'Make sure no item is selected in the datalist
        dlstPrefs.SelectedIndex = -1
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim rptParamInfo As New ReportParamInfo
        Dim ds As New DataSet
        If Page.IsValid Then
            Dim strErrMsg As String = BuildReportDataSet(rptParamInfo, ds)
            If strErrMsg = "" Then
                'Since no error was found, proceed to display report in Export Viewer page.
                If Not (Session("ReportDS") Is Nothing) Then

                    '   Make sure to clear any previously loaded report docuemnt objects
                    Session("RptObject") = Nothing

                    '   Setup the properties of the new window
                    Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium
                    Dim name As String = "ReportViewer"
                    Dim url As String = "../SY/" + name + ".aspx"
                    CommonWebUtilities.OpenClientChildWindow(Page, url, name, winSettings)

                Else
                    'Alert user of no data matching selection.
                    strErrMsg = "There is no data matching selection."
                    DisplayErrorMessage(strErrMsg)
                End If
            Else
                'Alert user of error.
                DisplayErrorMessage(strErrMsg)
            End If
        End If
    End Sub

    Private Function BuildOrderBy(ByRef StrOClause As String) As String
        Dim i As Integer
        Dim oClause As String = ""
        Dim tempStringOClause As String = ""
        Dim fldName As String
        Dim tblName As String
        Dim fldCaption As String

        'Only execute if at least one sort param has been specified
        If lbxselsort.Items.Count > 0 Then
            For i = 0 To lbxselsort.Items.Count - 1
                tblName = GetTblName(lbxselsort.Items(i).Value)
                fldName = GetFldName(lbxselsort.Items(i).Value)
                fldCaption = lbxselsort.Items(i).Text
                If i < lbxselsort.Items.Count - 1 Then
                    oClause &= tblName & "." & fldName & ","
                    tempStringOClause &= fldCaption & ", "
                Else
                    oClause &= tblName & "." & fldName
                    tempStringOClause &= fldCaption
                End If
            Next
        End If

        'oClause = "ORDER BY " & oClause
        StrOClause = tempStringOClause
        Return oClause
    End Function
    Private Function BuildFilterListClauseWithParamInfo(ByRef StrWClause As String,
                                                        ByRef ParamInfo As ReportParamInfo,
                                                        Optional ByVal resourceId As Integer = 0) As String
        Dim dt As DataTable
        Dim dt2 As DataTable
        Dim i, j, k As Integer
        Dim drRows() As DataRow
        Dim rows() As DataRow
        Dim fldName As String
        Dim tblName As String
        Dim fldType As String
        Dim fldCaption As String
        Dim [Operator] As String
        Dim filterValue As String
        Dim tempWClause As String = ""
        Dim tempStringWClause As String = ""
        Dim stringWClause As String = ""
        Dim strTempOperator As String = ""
        Dim wClause As String = ""

        'We need to loop through the items in the lbxFilterList control and see
        'if the item has any entries in the FilterListSelections dt stored in session.
        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")
        'We need to get the DisplayFld of all selected items.
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListValues")

        If dt.Rows.Count > 0 Then
            For i = 0 To lbxfilterlist.Items.Count - 1
                drRows = dt.Select("RptParamId = " & lbxfilterlist.Items(i).Value)
                If drRows.Length > 0 Then 'Match found

                    tblName = GetTblName(lbxfilterlist.Items(i).Value)
                    fldName = GetFldName(lbxfilterlist.Items(i).Value)
                    fldType = GetFldType(lbxfilterlist.Items(i).Value)
                    fldCaption = lbxfilterlist.Items(i).Text

                    If drRows.Length > 1 Then
                        [Operator] = " IN "
                        strTempOperator = " In List "
                    Else
                        [Operator] = " = "
                        strTempOperator = " Equal To "
                    End If

                    Dim tblTerm As New DataTable("Terms")
                    Dim tblGrdCompTypes As New DataTable("GradeComponentTypes")
                    Dim tblCampGrps As New DataTable("CampusGroups")
                    Dim tblProgramVersion As New DataTable("ProgramVersion")
                    Dim tblStatusCodes As New DataTable("statuscodes")

                    Dim dsFilter As New DataSet
                    With tblTerm
                        .Columns.Add("termid", GetType(String))
                    End With

                    With tblGrdCompTypes
                        .Columns.Add("grdComponentTypeId", GetType(String))
                    End With

                    With tblCampGrps
                        .Columns.Add("campgrpid", GetType(String))
                    End With

                    With tblProgramVersion
                        .Columns.Add("prgverid", GetType(String))
                    End With
                    With tblStatusCodes
                        .Columns.Add("statuscodeid", GetType(String))
                    End With


                    For j = 0 To drRows.Length - 1
                        filterValue = BuildFilterValue(drRows(j)("FldValue").ToString(), fldType)
                        If j < drRows.Length - 1 Then
                            tempWClause &= filterValue & ","
                        Else
                            tempWClause &= filterValue
                        End If
                        'Get the DisplayFld
                        rows = dt2.Select("RptParamId = " & drRows(j)("RptParamId").ToString() & " AND " &
                                            "ValueFld = '" & drRows(j)("FldValue").ToString() & "'")
                        If rows.GetLength(0) = 1 Then
                            If j < drRows.Length - 1 Then
                                tempStringWClause &= "'" & rows(0)("DisplayFld") & "', "
                            Else
                                If strTempOperator = " Equal To " Then
                                    tempStringWClause &= rows(0)("DisplayFld")
                                Else
                                    tempStringWClause &= "'" & rows(0)("DisplayFld") & "'"
                                End If
                            End If
                        End If

                        If tblName.ToLower = "arterm" Then
                            Dim row As DataRow
                            row = tblTerm.NewRow()
                            row("TermId") = Replace(filterValue.ToString, "'", "")
                            tblTerm.Rows.Add(row)
                        End If

                        If tblName.ToLower = "argrdcomponenttypes" Then
                            Dim row As DataRow
                            row = tblGrdCompTypes.NewRow()
                            row("grdComponentTypeId") = Replace(filterValue.ToString, "'", "")
                            tblGrdCompTypes.Rows.Add(row)
                        End If

                        If tblName.ToLower = "sycampgrps" Then
                            Dim row As DataRow
                            row = tblCampGrps.NewRow()
                            row("campgrpid") = Replace(filterValue.ToString, "'", "")
                            tblCampGrps.Rows.Add(row)
                        End If
                        If tblName.ToLower = "programversion" Then
                            Dim row As DataRow
                            row = tblProgramVersion.NewRow()
                            row("prgverid") = Replace(filterValue.ToString, "'", "")
                            tblProgramVersion.Rows.Add(row)
                        End If

                        If tblName.ToLower = "systatuscodes" Then
                            Dim row As DataRow
                            row = tblStatusCodes.NewRow()
                            row("statuscodeid") = Replace(filterValue.ToString, "'", "")
                            tblStatusCodes.Rows.Add(row)
                        End If
                    Next

                    If tblName.ToLower = "arterm" Then
                        dsFilter.Tables.Add(tblTerm)
                        If Not dsFilter Is Nothing Then
                            For Each lcol As DataColumn In dsFilter.Tables(0).Columns
                                lcol.ColumnMapping = System.Data.MappingType.Attribute
                            Next
                            Dim strTermXML As String = dsFilter.GetXml
                            ParamInfo.TermId = strTermXML
                        End If
                    End If

                    dsFilter.Clear()

                    If tblName.ToLower = "argrdcomponenttypes" Then
                        dsFilter.Tables.Add(tblGrdCompTypes)
                        If Not dsFilter Is Nothing Then
                            For Each lcol As DataColumn In dsFilter.Tables(0).Columns
                                lcol.ColumnMapping = System.Data.MappingType.Attribute
                            Next
                            Dim strGrdCompTypesXML As String = dsFilter.GetXml
                            ParamInfo.TestType = strGrdCompTypesXML
                        End If
                    End If

                    dsFilter.Clear()

                    If tblName.ToLower = "sycampgrps" Then
                        dsFilter.Tables.Add(tblCampGrps)
                        If Not dsFilter Is Nothing Then
                            For Each lcol As DataColumn In dsFilter.Tables(0).Columns
                                lcol.ColumnMapping = System.Data.MappingType.Attribute
                            Next
                            Dim strGrdCompTypesXML As String = dsFilter.GetXml
                            ParamInfo.CampGrpId = strGrdCompTypesXML
                        End If
                    End If

                    dsFilter.Clear()

                    If tblName.ToLower = "arprgversions" Then
                        dsFilter.Tables.Add(tblProgramVersion)
                        If Not dsFilter Is Nothing Then
                            For Each lcol As DataColumn In dsFilter.Tables(0).Columns
                                lcol.ColumnMapping = System.Data.MappingType.Attribute
                            Next
                            Dim strGrdCompTypesXML As String = dsFilter.GetXml
                            ParamInfo.PrgVerId = strGrdCompTypesXML
                        End If
                    End If

                    If tblName.ToLower = "systatuscodes" Then
                        dsFilter.Tables.Add(tblStatusCodes)
                        If Not dsFilter Is Nothing Then
                            For Each lcol As DataColumn In dsFilter.Tables(0).Columns
                                lcol.ColumnMapping = System.Data.MappingType.Attribute
                            Next
                            Dim strGrdCompTypesXML As String = dsFilter.GetXml
                            ParamInfo.StatusCodeId = strGrdCompTypesXML
                        End If
                    End If

                    If ddlSpeedOperator.SelectedValue = "1" Then
                        ParamInfo.Speed = "Speed >=" & txtSpeedVal1.Text & " and Speed <=" & txtSpeedVal2.Text
                    ElseIf ddlSpeedOperator.SelectedValue = "2" Then
                        ParamInfo.Speed = "Speed =" & txtSpeedVal1.Text
                    ElseIf ddlSpeedOperator.SelectedValue = "3" Then
                        ParamInfo.Speed = "Speed >" & txtSpeedVal1.Text
                    ElseIf ddlSpeedOperator.SelectedValue = "4" Then
                        ParamInfo.Speed = "Speed >=" & txtSpeedVal1.Text
                    ElseIf ddlSpeedOperator.SelectedValue = "5" Then
                        ParamInfo.Speed = "Speed <" & txtSpeedVal1.Text
                    ElseIf ddlSpeedOperator.SelectedValue = "6" Then
                        ParamInfo.Speed = "Speed <=" & txtSpeedVal1.Text
                    Else
                        ParamInfo.Speed = ""
                    End If


                    If ddlAccuracyOperator.SelectedValue = "1" Then
                        ParamInfo.Accuracy = "Accuracy >=" & txtAccuracyVal1.Text & " and Accuracy <=" & txtAccuracyVal2.Text
                    ElseIf ddlAccuracyOperator.SelectedValue = "2" Then
                        ParamInfo.Accuracy = "Accuracy =" & txtAccuracyVal1.Text
                    ElseIf ddlAccuracyOperator.SelectedValue = "3" Then
                        ParamInfo.Accuracy = "Accuracy >" & txtAccuracyVal1.Text
                    ElseIf ddlAccuracyOperator.SelectedValue = "4" Then
                        ParamInfo.Accuracy = "Accuracy >=" & txtAccuracyVal1.Text
                    ElseIf ddlAccuracyOperator.SelectedValue = "5" Then
                        ParamInfo.Accuracy = "Accuracy <" & txtAccuracyVal1.Text
                    ElseIf ddlAccuracyOperator.SelectedValue = "6" Then
                        ParamInfo.Accuracy = "Accuracy <=" & txtAccuracyVal1.Text
                    Else
                        ParamInfo.Accuracy = ""
                    End If


                    If k > 0 Then
                        wClause &= " AND "
                        stringWClause &= " AND "
                    End If

                    'If [Operator] = " = " Then
                    '    wClause &= tblName & "." & fldName & [Operator] & tempWClause
                    '    stringWClause &= fldCaption & strTempOperator & tempStringWClause
                    'Else
                    '    wClause &= tblName & "." & fldName & [Operator] & "(" & tempWClause & ")"
                    '    stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                    'End If
                    If fldName.ToLower = "campgrpid" Then
                        strCampGrpId = tempWClause
                    End If

                    If [Operator] = " = " Then
                        ' wClause &= tblName & "." & fldName & [Operator] & tempWClause
                        'stringWClause &= fldCaption & strTempOperator & tempStringWClause
                        If fldName.ToLower = "campgrpid" Then
                            If Not txtAllCampGrps.Text.ToLower = "all" Then
                                Dim strCampGrp As String
                                [Operator] = " in "
                                strCampGrp = " (select Distinct t1.CampGrpId from syCmpGrpCmps t1,syCmpGrpCmps t2 where t1.CampusId = t2.CampusId and t2.CampGrpId in "
                                wClause &= tblName & "." & fldName & [Operator] & strCampGrp & "(" & tempWClause & "))"
                                stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                            End If
                        Else
                            [Operator] = " = "
                            wClause &= tblName & "." & fldName & [Operator] & tempWClause
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                        End If
                    Else
                        If fldName.ToLower = "campgrpid" Then
                            If Not txtAllCampGrps.Text.ToLower = "all" Then
                                Dim strCampGrp As String
                                strCampGrp = " (select Distinct t1.CampGrpId from syCmpGrpCmps t1,syCmpGrpCmps t2 where t1.CampusId = t2.CampusId and t2.CampGrpId in "
                                wClause &= tblName & "." & fldName & [Operator] & strCampGrp & "(" & tempWClause & "))"
                                stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                            End If
                        Else
                            wClause &= tblName & "." & fldName & [Operator] & "(" & tempWClause & ")"
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                        End If
                    End If

                    k += 1 'Match found so we need to increment the count of matches so far
                End If
                tempWClause = ""
                tempStringWClause = ""
            Next
        Else
            If ddlSpeedOperator.SelectedValue = "1" Then
                ParamInfo.Speed = "Speed >=" & txtSpeedVal1.Text & " and Speed <=" & txtSpeedVal2.Text
            ElseIf ddlSpeedOperator.SelectedValue = "2" Then
                ParamInfo.Speed = "Speed =" & txtSpeedVal1.Text
            ElseIf ddlSpeedOperator.SelectedValue = "3" Then
                ParamInfo.Speed = "Speed >" & txtSpeedVal1.Text
            ElseIf ddlSpeedOperator.SelectedValue = "4" Then
                ParamInfo.Speed = "Speed >=" & txtSpeedVal1.Text
            ElseIf ddlSpeedOperator.SelectedValue = "5" Then
                ParamInfo.Speed = "Speed <" & txtSpeedVal1.Text
            ElseIf ddlSpeedOperator.SelectedValue = "6" Then
                ParamInfo.Speed = "Speed <=" & txtSpeedVal1.Text
            Else
                ParamInfo.Speed = ""
            End If

            If ddlAccuracyOperator.SelectedValue = "1" Then
                ParamInfo.Accuracy = "Accuracy >=" & txtAccuracyVal1.Text & " and Accuracy <=" & txtAccuracyVal2.Text
            ElseIf ddlAccuracyOperator.SelectedValue = "2" Then
                ParamInfo.Accuracy = "Accuracy =" & txtAccuracyVal1.Text
            ElseIf ddlAccuracyOperator.SelectedValue = "3" Then
                ParamInfo.Accuracy = "Accuracy >" & txtAccuracyVal1.Text
            ElseIf ddlAccuracyOperator.SelectedValue = "4" Then
                ParamInfo.Accuracy = "Accuracy >=" & txtAccuracyVal1.Text
            ElseIf ddlAccuracyOperator.SelectedValue = "5" Then
                ParamInfo.Accuracy = "Accuracy <" & txtAccuracyVal1.Text
            ElseIf ddlAccuracyOperator.SelectedValue = "6" Then
                ParamInfo.Accuracy = "Accuracy <=" & txtAccuracyVal1.Text
            Else
                ParamInfo.Accuracy = ""
            End If
        End If

        StrWClause = stringWClause
        Return wClause
    End Function
    Private Function BuildFilterListClause(ByRef StrWClause As String) As String
        Dim dt As DataTable
        Dim dt2 As DataTable
        Dim i, j, k As Integer
        Dim drRows() As DataRow
        Dim rows() As DataRow
        Dim fldName As String
        Dim tblName As String
        Dim fldType As String
        Dim fldCaption As String
        Dim [Operator] As String
        Dim filterValue As String
        Dim tempWClause As String = ""
        Dim tempStringWClause As String = ""
        Dim stringWClause As String = ""
        Dim strTempOperator As String = ""
        Dim wClause As String = ""

        'We need to loop through the items in the lbxFilterList control and see
        'if the item has any entries in the FilterListSelections dt stored in session.
        dt = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListSelections")
        'We need to get the DisplayFld of all selected items.
        dt2 = DirectCast(Session("WorkingDS"), DataSet).Tables("FilterListValues")

        If dt.Rows.Count > 0 Then
            For i = 0 To lbxfilterlist.Items.Count - 1
                drRows = dt.Select("RptParamId = " & lbxfilterlist.Items(i).Value)
                If drRows.Length > 0 Then 'Match found

                    tblName = GetTblName(lbxfilterlist.Items(i).Value)
                    fldName = GetFldName(lbxfilterlist.Items(i).Value)
                    fldType = GetFldType(lbxfilterlist.Items(i).Value)
                    fldCaption = lbxfilterlist.Items(i).Text

                    If drRows.Length > 1 Then
                        [Operator] = " IN "
                        strTempOperator = " In List "
                    Else
                        [Operator] = " = "
                        strTempOperator = " Equal To "
                    End If

                    For j = 0 To drRows.Length - 1
                        filterValue = BuildFilterValue(drRows(j)("FldValue").ToString(), fldType)
                        If j < drRows.Length - 1 Then
                            tempWClause &= filterValue & ","
                        Else
                            tempWClause &= filterValue
                        End If
                        'Get the DisplayFld
                        rows = dt2.Select("RptParamId = " & drRows(j)("RptParamId").ToString() & " AND " &
                                            "ValueFld = '" & drRows(j)("FldValue").ToString() & "'")
                        If rows.GetLength(0) = 1 Then
                            If j < drRows.Length - 1 Then
                                tempStringWClause &= "'" & rows(0)("DisplayFld") & "', "
                            Else
                                If strTempOperator = " Equal To " Then
                                    tempStringWClause &= rows(0)("DisplayFld")
                                Else
                                    tempStringWClause &= "'" & rows(0)("DisplayFld") & "'"
                                End If
                            End If
                        End If
                    Next



                    If k > 0 Then
                        wClause &= " AND "
                        stringWClause &= " AND "
                    End If

                    'If [Operator] = " = " Then
                    '    wClause &= tblName & "." & fldName & [Operator] & tempWClause
                    '    stringWClause &= fldCaption & strTempOperator & tempStringWClause
                    'Else
                    '    wClause &= tblName & "." & fldName & [Operator] & "(" & tempWClause & ")"
                    '    stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                    'End If
                    If fldName.ToLower = "campgrpid" Then
                        strCampGrpId = tempWClause
                    End If

                    If [Operator] = " = " Then
                        ' wClause &= tblName & "." & fldName & [Operator] & tempWClause
                        'stringWClause &= fldCaption & strTempOperator & tempStringWClause
                        If fldName.ToLower = "campgrpid" Then
                            If Not txtAllCampGrps.Text.ToLower = "all" Then
                                Dim strCampGrp As String
                                [Operator] = " in "
                                strCampGrp = " (select Distinct t1.CampGrpId from syCmpGrpCmps t1,syCmpGrpCmps t2 where t1.CampusId = t2.CampusId and t2.CampGrpId in "
                                wClause &= tblName & "." & fldName & [Operator] & strCampGrp & "(" & tempWClause & "))"
                                stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                            End If
                        Else
                            [Operator] = " = "
                            wClause &= tblName & "." & fldName & [Operator] & tempWClause
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                        End If
                    Else
                        If fldName.ToLower = "campgrpid" Then
                            If Not txtAllCampGrps.Text.ToLower = "all" Then
                                Dim strCampGrp As String
                                strCampGrp = " (select Distinct t1.CampGrpId from syCmpGrpCmps t1,syCmpGrpCmps t2 where t1.CampusId = t2.CampusId and t2.CampGrpId in "
                                wClause &= tblName & "." & fldName & [Operator] & strCampGrp & "(" & tempWClause & "))"
                                stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                            End If
                        Else
                            wClause &= tblName & "." & fldName & [Operator] & "(" & tempWClause & ")"
                            stringWClause &= fldCaption & strTempOperator & "(" & tempStringWClause & ")"
                        End If
                    End If

                    k += 1 'Match found so we need to increment the count of matches so far
                End If
                tempWClause = ""
                tempStringWClause = ""
            Next
        End If

        StrWClause = stringWClause
        Return wClause
    End Function

    Private Function BuildFilterOtherClause(ByRef StrWClause As String, ByRef StrErrorMsg As String) As String
        Dim fldName As String = ""
        Dim tblName As String = ""
        Dim fldType As String = ""
        Dim fldCaption As String = ""
        Dim sqlOperator As String = ""
        Dim rCounter As Integer
        Dim opId As Integer
        Dim rptParamId As Integer
        Dim tbxText As String = ""
        Dim tbxText2 As String = ""
        Dim arrValues() As String
        Dim selText As String = ""
        Dim wClause As String = ""
        Dim tempWClause As String = ""
        Dim inList As String = ""
        Dim strItem As String = ""
        Dim position As Integer
        Dim errMsg As String = ""
        Dim tempFilterValue As String = ""
        Dim tempFilterValue2 As String = ""
        'The following variables will hold the string representation of the Where Clause,
        'which will be shown in the report as part of the title.        
        Dim tempStringWClause As String = ""
        Dim stringWClause As String = ""

        'Special case.       
        If txtStudentId.Text <> "" And txtStuEnrollment.Text <> "" And txtstudentidentifier.Value <> "" Then
            If txtstuenrollmentid.Value = System.Guid.Empty.ToString Then
                tempStringWClause = "Student " & txtStudentId.Text
                tempWClause = "arStudent.StudentId = '" & txtstudentidentifier.Value & "'"
            Else
                tempStringWClause = "Student " & txtStudentId.Text & " AND " &
                    "Enrollment " & txtStuEnrollment.Text & ""
                tempWClause = "arStuEnrollments.StuEnrollId = '" & txtstuenrollmentid.Value & "'"
            End If
            ' DE7194 2/14/2012 Janet Robinson Reverted change made for Weekly Attend. Single so that it is only for that report and not any others
            If resourceId = 779 Then
                wClause &= " AND " & tempWClause & ";"
                stringWClause &= " AND " & tempStringWClause & ";"
            Else
                wClause &= tempWClause & " AND "
                stringWClause &= tempStringWClause & " AND "
            End If
        Else
            If CInt(ViewState("resid")) <> 615 Then
                errMsg = "Required fields Student and Enrollment are missing."
            End If
        End If


        rCounter = 1
        'We need to loop through the entries in the tblFilterOthers table.
        Do While (rCounter <= tblFilterOthers.Rows.Count - 1) And errMsg = ""
            tbxText = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).Text
            tbxText2 = DirectCast(tblFilterOthers.Rows(rCounter).Cells(3).Controls(0), TextBox).Text
            fldCaption = DirectCast(tblFilterOthers.Rows(rCounter).Cells(0).Controls(0), Label).Text
            arrValues = tbxText.Split(";")
            opId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
            sqlOperator = GetSQLOperator(opId)
            rptParamId = DirectCast(tblFilterOthers.Rows(rCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
            tblName = GetTblName(rptParamId)
            fldName = GetFldName(rptParamId)
            fldType = GetFldType(rptParamId)
            selText = DirectCast(tblFilterOthers.Rows(rCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text
            'Ignore rows where nothing is entered into the textbox.



            If tbxText <> "" Then

                Try
                    ' US2729 12/23/2011 Janet Robinson added resourceid 771
                    ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                    'Added for Weekly Attendance Report
                    If resourceId = 779 And tblName.ToString.ToLower = "arStudentClockAttendance".ToLower Then
                        If arrValues.GetLength(0) = 1 Then
                            tempWClause = fldCaption & ";" & arrValues(0)
                            tempStringWClause = fldCaption & ";" & arrValues(0)
                            Exit Try
                        End If
                    End If

                    'Modified by Michelle R. Rodriguez on 11/22/2004.
                    'FilterValue needs to be validated to exclude invalid dates and replace single quotes with double quotes.
                    'Also, the number of operands is validated.

                    If IsSingleValOp(opId) And selText = "Like" Then
                        If arrValues.GetLength(0) = 1 Then
                            tempFilterValue = BuildFilterValue(arrValues(0), fldType)
                            If tempFilterValue <> "" Then
                                If tblName <> "syDerived" Then
                                    tempWClause = tblName & "." & fldName & " " & sqlOperator & "'%" & arrValues(0) & "%'"
                                Else
                                    tempWClause = fldName & sqlOperator & "'%" & arrValues(0) & "%'"
                                End If
                                tempStringWClause = fldCaption & " " & selText & " " & arrValues(0)
                            Else
                                errMsg = "Invalid " & fldCaption & " value to filter for."
                            End If
                        Else
                            'More than one input for a single operator.
                            errMsg = "Too many values to filter " & fldCaption & " for."
                        End If

                    ElseIf IsSingleValOp(opId) And selText <> "Empty" And selText <> "Is Null" Then
                        ' Modified by Michelle Rodriguez on 09/26/2004 and 10/04/2004.
                        ' Needed to include the time for filters that involved Datetime fields:
                        ' - The " = " operator will behave like a " BETWEEN " and the filter value
                        '   will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
                        ' - The " <> " operator will behave like a " NOT BETWEEN " and the filter value
                        '   will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
                        ' - Also, the filter value for the " > " operator will be of format: mm/dd/yyyy 11:59:59 PM
                        ' All other cases remain the same.
                        If arrValues.GetLength(0) = 1 Then
                            tempFilterValue = BuildFilterValue(arrValues(0), fldType, sqlOperator)
                            If tempFilterValue <> "" Then
                                tempStringWClause = fldCaption & " " & selText & " " & arrValues(0)
                                If tblName <> "syDerived" Then
                                    If fldType = "Datetime" Then
                                        If sqlOperator = " = " Then
                                            sqlOperator = " BETWEEN "
                                        ElseIf sqlOperator = " <> " Then
                                            sqlOperator = " NOT BETWEEN "
                                        End If
                                    End If
                                    tempWClause = tblName & "." & fldName & sqlOperator & tempFilterValue
                                Else
                                    If fldType = "Datetime" Then
                                        If sqlOperator = " = " Then
                                            sqlOperator = " BETWEEN "
                                        ElseIf sqlOperator = " <> " Then
                                            sqlOperator = " NOT BETWEEN "
                                        End If
                                    End If
                                    tempWClause = fldName & sqlOperator & tempFilterValue
                                End If
                            Else
                                errMsg = "Invalid " & fldCaption & " value to filter for."
                            End If
                        Else
                            'More than one input for a single operator.
                            errMsg = "Too many values to filter " & fldCaption & " for."
                        End If

                    ElseIf selText = "Between" Then
                        'If arrValues.GetLength(0) = 2 Then
                        'Only two values are accepted as valid input.
                        tempFilterValue = BuildFilterValue(tbxText, fldType)
                        tempFilterValue2 = BuildFilterValue(tbxText2, fldType)
                        If tempFilterValue <> "" And tempFilterValue2 <> "" Then
                            If tblName <> "syDerived" Then
                                tempWClause = tblName & "." & fldName & sqlOperator & tempFilterValue & " AND " & tempFilterValue2
                            Else

                                If resourceId = 637 Then
                                    Dim dTo As Date = Convert.ToDateTime(tempFilterValue2.Replace("'", ""))
                                    Dim dFrom As Date = Convert.ToDateTime(tempFilterValue.Replace("'", ""))

                                    If DateTime.Compare(dTo, dFrom) > 0 Then
                                        tempWClause = fldName & sqlOperator & tempFilterValue & " AND " & tempFilterValue2
                                    Else
                                        errMsg = "To date should be greater than from date"
                                    End If
                                Else
                                    tempWClause = fldName & sqlOperator & tempFilterValue & " AND " & tempFilterValue2

                                End If
                            End If
                            tempStringWClause = fldCaption & " " & selText & " (" & tbxText & " AND " & tbxText2 & ")"
                        Else
                            errMsg = "Too few values to filter " & fldCaption & " for." & vbCr & "You must specify exactly two values when using the Between operator."
                            'errMsg = "Invalid " & fldCaption & " value to filter for."
                        End If
                        'Else
                        '    'Too few operands or invalid separation character.
                        '    errMsg = "Invalid separation character or invalid number of values to filter " & fldCaption & " for." & vbCrLf & _
                        '                "The character required to separate values is the semicolon (;)."
                        'End If

                        ''If tblName <> "syDerived" Then
                        ''    tempWClause = tblName & "." & fldName & sqlOperator & BuildFilterValue(arrValues(0), fldType) & " AND " & BuildFilterValue(arrValues(1), fldType)
                        ''Else
                        ''    tempWClause = fldName & sqlOperator & BuildFilterValue(arrValues(0), fldType) & " AND " & BuildFilterValue(arrValues(1), fldType)
                        ''End If
                        ''tempStringWClause = fldCaption & " " & selText & " " & arrValues(0) & " and " & arrValues(1)

                    ElseIf selText = "In List" Then
                        If arrValues.GetLength(0) > 1 Then
                            'More than one value in input.
                            For Each strItem In arrValues
                                If strItem <> "" Then
                                    tempFilterValue = BuildFilterValue(strItem, fldType)
                                    If tempFilterValue <> "" Then
                                        inList &= tempFilterValue & ","
                                    Else
                                        Exit For
                                    End If
                                End If
                            Next
                            If tempFilterValue <> "" Then
                                'Remove the last comma.
                                inList = inList.Substring(0, inList.Length - 1)

                                If tblName <> "syDerived" Then
                                    tempWClause = tblName & "." & fldName & sqlOperator & "(" & inList & ")"
                                Else
                                    tempWClause = fldName & sqlOperator & "(" & inList & ")"
                                End If
                                tempStringWClause = fldCaption & " " & selText & " (" & inList & ")"
                            Else
                                errMsg = "Invalid " & fldCaption & " value to filter for."
                            End If
                        Else
                            'Too few operands or invalid separation character.
                            errMsg = "Invalid separation character or too few values to filter " & fldCaption & " for." & vbCrLf &
                                        "The character required to separate values is the semicolon (;)."
                        End If
                        ''For Each strItem In arrValues
                        ''    inList &= BuildFilterValue(strItem, fldType) & ","
                        ''Next
                        'remove the last comma
                        ''inList = inList.Substring(0, inList.Length - 1)

                        ''If tblName <> "syDerived" Then
                        ''    tempWClause = tblName & "." & fldName & sqlOperator & "(" & inList & ")"
                        ''Else
                        ''    tempWClause = fldName & sqlOperator & "(" & inList & ")"
                        ''End If
                        ''tempStringWClause = fldCaption & " " & selText & " (" & inList & ")"
                    End If

                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                End Try

                wClause &= tempWClause & " AND "
                stringWClause &= tempStringWClause & " AND "
                inList = ""

            Else
                'Textbox is empty
                If tbxText2 <> "" And selText = "Between" Then
                    'Too few operands.
                    errMsg = "Too few values to filter " & fldCaption & " for." & vbCr & "You must specify exactly two values when using the Between operator."
                End If
            End If



            'For the Is Null and Empty operator the textbox should be empty
            If IsSingleValOp(opId) And selText = "Empty" Then
                If tblName <> "syDerived" Then
                    tempWClause = tblName & "." & fldName & sqlOperator
                Else
                    tempWClause = fldName & sqlOperator
                End If
                tempStringWClause = fldCaption & " Is " & selText
                wClause &= tempWClause & " AND "
                stringWClause &= tempStringWClause & " AND "
            End If

            If IsSingleValOp(opId) And selText = "Is Null" Then
                If tblName <> "syDerived" Then
                    tempWClause = tblName & "." & fldName & sqlOperator
                Else
                    tempWClause = fldName & sqlOperator
                End If
                tempStringWClause = fldCaption & selText
                wClause &= tempWClause & " AND "
                stringWClause &= tempStringWClause & " AND "
            End If

            rCounter += 1
        Loop    'loop through the entries in the tblFilterOThers table


        If errMsg = "" Then
            'No error was found.
            'If the operator was the In List then we will have an extra AND
            'so we need to remove it.
            'Modified by Michelle Rodriguez 10/25/2004.
            'If operator was the Empty and the user typed something in the textbox next to the operators' dropdown,
            'there is an extra AND at position 0 (zero) and we need to remove it.
            If wClause <> "" Then
                'Remove last occurrence of " AND".
                position = wClause.LastIndexOf(" AND")
                If position > 0 Then
                    wClause = wClause.Substring(0, position + 1)
                End If
                'Remove occurrence of " AND " at position 0 (zero).
                position = wClause.IndexOf(" AND ")
                If position = 0 Then
                    wClause = wClause.Substring(5)
                End If
            End If
            'Need to do the same with the string reprentation of the Where Clause.
            'Remove last occurrence of " AND" and the first occurrence of " AND " from the stringWhereClause.
            If stringWClause <> "" Then
                position = stringWClause.LastIndexOf(" AND ")
                If position > 0 Then
                    stringWClause = stringWClause.Substring(0, position + 1)
                End If
                position = stringWClause.IndexOf(" AND ")
                If position = 0 Then
                    ' US2729 12/23/2011 Janet Robinson added resourceid 771
                    ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                    If resourceId = 779 Then
                        stringWClause = stringWClause.Substring(5)
                    Else
                        stringWClause = stringWClause.Substring(0, position + 1).Trim
                    End If

                End If
            End If
        Else
            'If an error was found, clear variables.
            StrWClause = ""
            wClause = ""
        End If

        StrErrorMsg = errMsg
        StrWClause = stringWClause
        Return wClause
    End Function

    Private Function BuildFilterValue(ByVal filterValue As String, ByVal dataType As String,
                                        Optional ByVal sqlOperator As String = "") As String
        ' Modified by Michelle Rodriguez on 09/26/2004 and 10/04/2004.
        ' Needed to include the time for filters that involved Datetime fields:
        ' - The " = " operator will behave like a " BETWEEN " and the filter value
        '   will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
        ' - The " <> " operator will behave like a " NOT BETWEEN " and the filter value
        '   will be of format: mm/dd/yyyy 12:00:00 AM AND mm/dd/yyyy 11:59:59 PM.
        ' - Also, the filter value for the " > " operator will be of format: mm/dd/yyyy 11:59:59 PM
        ' All other cases remain the same.       

        Dim returnValue As String
        Dim validValue As String

        validValue = ValidateValue(filterValue, dataType)
        If validValue <> "" Then
            Select Case dataType
                Case "Uniqueidentifier", "Char", "Varchar"
                    returnValue = "'" & filterValue & "'"
                Case "Datetime"
                    If sqlOperator = " = " Or sqlOperator = " <> " Then
                        returnValue = "'" & filterValue & " 12:00:00 AM' AND '" & filterValue & " 11:59:59 PM'"
                    ElseIf sqlOperator = " > " Then
                        returnValue = "'" & filterValue & " 11:59:59 PM'"
                    Else
                        returnValue = "'" & filterValue & "'"
                    End If
                Case Else
                    Return filterValue
            End Select
        Else
            returnValue = validValue
        End If

        Return returnValue
    End Function

    'Method added by Michelle R. Rodriguez on 11/22/2004, as part of the error handler for the FilterOthers section.
    Private Function ValidateValue(ByVal FilterValue As String, ByVal dataType As String) As String
        Dim returnValue As String

        Select Case dataType
            Case "Uniqueidentifier", "Char", "Varchar"
                If FilterValue.IndexOf("'") <> -1 Then
                    'Replace single quotes by double quotes.
                    returnValue = FilterValue.Replace("'", "''")
                Else
                    'Return FilterValue as it was.
                    returnValue = FilterValue
                End If
            Case "Datetime"
                Try
                    Dim dDate As DateTime = Date.Parse(FilterValue)
                    'SLQ Server does not accept year < 1753. Therefore, a limit has to be imposed.
                    If dDate < FAME.AdvantageV1.Common.AdvantageCommonValues.MinDate Or
                            dDate > FAME.AdvantageV1.Common.AdvantageCommonValues.MaxDate Then
                        returnValue = ""
                    Else
                        returnValue = dDate.ToShortDateString
                    End If
                Catch
                    'Invalid date.
                    'Might be ArgumentException, FormatException or OverflowException
                    returnValue = ""
                End Try
            Case Else
                Return FilterValue
        End Select
        Return returnValue
    End Function

    Private Function GetSQLOperator(ByVal opId As Integer) As String
        Select Case opId
            Case 1
                Return " = "
            Case 2
                Return " <> "
            Case 3
                Return " > "
            Case 4
                Return " < "
            Case 5
                Return " IN "
            Case 6
                Return " BETWEEN "
            Case 7
                Return " LIKE "
            Case 8
                Return " = '' "
            Case 9
                Return " IS NULL "
            Case Else
                Throw New System.Exception("The following operator is not recognized:" & opId)
        End Select
    End Function


    ' This method is going to build the dataset for the report. Then the report may be displayed
    ' in web form, which may contain a Crystal Report Viewer or a DataGrid, 
    ' or may be exported to any of the available formats.
    Private Function BuildReportDataSet(ByRef rptParamInfo As ReportParamInfo, ByRef Optional ds As DataSet = Nothing) As String
        Dim orderByClause As String = ""
        Dim stringOrderByClause As String = ""
        Dim filterListWhereClause As String = ""
        Dim stringWhereClause As String = ""
        Dim filterOtherWhereClause As String = ""
        Dim stringFilterOtherWClause As String = ""
        Dim strErrMsg As String = ""
        Dim rptfac As New ReportFacade

        Dim objName As String
        Dim facAssembly As System.Reflection.Assembly


        If (ds Is Nothing) Then
            ds = New DataSet
        End If


        'Build the Order by clause if there are any entries in the lbxSelSort control.
        If lbxselsort.Items.Count > 0 Then
            orderByClause = BuildOrderBy(stringOrderByClause)
        End If

        'Build the filter list part of the where clause.

        If resourceId = 615 Then
            filterListWhereClause = BuildFilterListClauseWithParamInfo(stringWhereClause, rptParamInfo)
        Else
            filterListWhereClause = BuildFilterListClause(stringWhereClause)
        End If


        'Build the filter other part of the where clause.
        'If an error is found in the user input, we cannot proceed to display the report.
        filterOtherWhereClause = BuildFilterOtherClause(stringFilterOtherWClause, strErrMsg)

        ViewState("StudentFilter") = filterOtherWhereClause

        If resourceId = 615 And Not txtStudentId.Text = "" Then ' if student was selected and resource id is 615 
            If Not filterListWhereClause = "" Or InStr(rptParamInfo.CampGrpId, "2ac97fc1-bb9b-450a-aa84-7c503c90a1eb") >= 1 Then 'If all Campus Group is selected
                Dim strErrorMessage As String = ""
                strErrorMessage = "The selected report filters combination is incorrect. Only one of the following report filters combination is allowed " & vbCrLf
                strErrorMessage &= vbCrLf
                strErrorMessage &= "                                            Student and Enrollment Filter  " & vbCrLf
                strErrorMessage &= vbCrLf
                strErrorMessage &= "                                                            OR               " & vbCrLf
                strErrorMessage &= vbCrLf
                strErrorMessage &= "                                            Campus Group Filter           " & vbCrLf
                Return strErrorMessage
                Exit Function
            End If
        ElseIf resourceId = 615 AndAlso txtStudentId.Text = "" AndAlso filterListWhereClause = "" Then
            Try
                If InStr(rptParamInfo.CampGrpId, "2ac97fc1-bb9b-450a-aa84-7c503c90a1eb") >= 1 Then ' if all campus group is selected do not validate
                    Exit Try
                End If
                Dim strErrorMessage As String = ""
                strErrorMessage = "Please select any one of the following report filters combination" & vbCrLf
                strErrorMessage &= vbCrLf
                strErrorMessage &= "                Student and Enrollment Filter  " & vbCrLf
                strErrorMessage &= vbCrLf
                strErrorMessage &= "                           OR               " & vbCrLf
                strErrorMessage &= vbCrLf
                strErrorMessage &= "                Campus Group Filter           " & vbCrLf
                Return strErrorMessage
                Exit Function
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try
        End If

        'If no error is found, proceed to build report dataset.
        If strErrMsg = "" Then
            rptParamInfo.ResId = resourceId
            If CInt(ViewState("SQLID")) > 0 Then
                rptParamInfo.SqlId = CInt(ViewState("SQLID"))
                rptParamInfo.ObjId = 0
            Else
                rptParamInfo.SqlId = 0
                rptParamInfo.ObjId = CInt(ViewState("OBJID"))
            End If
            rptParamInfo.OrderBy = orderByClause
            rptParamInfo.OrderByString = stringOrderByClause
            rptParamInfo.FilterList = filterListWhereClause
            rptParamInfo.FilterListString = stringWhereClause
            rptParamInfo.FilterOther = filterOtherWhereClause
            rptParamInfo.FilterOtherString = stringFilterOtherWClause
            rptParamInfo.ShowFilters = chkRptFilters.Checked
            rptParamInfo.ShowSortBy = chkRptSort.Checked
            rptParamInfo.ShowRptDescription = chkRptDescrip.Checked
            rptParamInfo.ShowRptInstructions = chkRptInstructions.Checked
            rptParamInfo.ShowRptNotes = chkRptNotes.Checked
            'Added by Vijay Ramteke on Feb 08, 2010
            rptParamInfo.ShowRptCosts = chkRptCosts.Checked
            rptParamInfo.ShowRptExpectedFunding = chkRptExpectedFunding.Checked
            rptParamInfo.ShowRptCategoryBreakdown = chkRptCategoryBreakdown.Checked
            'Added by Vijay Ramteke on Feb 08, 2010
            rptParamInfo.RptTitle = ViewState("Resource")
            If resourceId = 615 Then
                'rptParamInfo.F = rptParamInfo.FilterListString
                ''New Code Added By Kamalesh Ahuja On June 29, 2010 For Mantis Id 19264
                If Not txtstudentidentifier.Value + "" = "" And Not txtstudentidentifier.Value.ToLower = "studentid" Then
                    rptParamInfo.StudentId = txtstudentidentifier.Value
                Else
                    rptParamInfo.StudentId = Nothing
                End If
                ''New Code Added By Kamalesh Ahuja On June 29, 2010 For Mantis Id 19264
                ''New Code Added By Vijay Ramteke On July 01, 2010 For Mantis Id 19264
                If Not txtstuenrollmentid.Value = Guid.Empty.ToString And Not txtstuenrollmentid.Value.ToLower = "stuenrollmentid" Then
                    rptParamInfo.StuEnrollId = txtstuenrollmentid.Value
                Else
                    rptParamInfo.StuEnrollId = Nothing
                End If
                ''New Code Added By Vijay Ramteke On July 01, 2010 For Mantis Id 19264

            End If

            If resourceId = 637 Then
                ''New Code Added By Vijay Ramteke On July 29, 2010
                If Not txtstudentidentifier.Value + "" = "" And Not txtstudentidentifier.Value.ToLower = "studentid" Then
                    rptParamInfo.StudentId = txtstudentidentifier.Value
                Else
                    rptParamInfo.StudentId = Nothing
                End If
                ''New Code Added By Vijay Ramteke On July 22, 2010
                ''New Code Added By Vijay Ramteke On July 01, 2010 For Mantis Id 19264
                If Not txtstuenrollmentid.Value = Guid.Empty.ToString And Not txtstuenrollmentid.Value.ToLower = "stuenrollmentid" Then
                    rptParamInfo.StuEnrollId = txtstuenrollmentid.Value
                Else
                    rptParamInfo.StuEnrollId = Guid.Empty.ToString
                End If
                ''New Code Added By Vijay Ramteke On July 01, 2010 For Mantis Id 19264
            End If

            ' US2729 12/23/2011 Janet Robinson added resourceid 771
            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
            If resourceId = 779 Then
                If Not txtstudentidentifier.Value + "" = "" And Not txtstudentidentifier.Value.ToLower = "studentid" Then
                    rptParamInfo.StudentId = txtstudentidentifier.Value
                Else
                    rptParamInfo.StudentId = Nothing
                End If
                If Not txtstuenrollmentid.Value = Guid.Empty.ToString And Not txtstuenrollmentid.Value.ToLower = "stuenrollmentid" Then
                    rptParamInfo.StuEnrollId = txtstuenrollmentid.Value
                Else
                    rptParamInfo.StuEnrollId = Guid.Empty.ToString
                End If
            End If

            If Not resourceId = 615 Then
                rptParamInfo.CampGrpId = strCampGrpId
            End If
            rptParamInfo.CampusId = HttpContext.Current.Request("cmpid").ToString
            If Not (Session("SchoolLogo") Is Nothing) Then
                rptParamInfo.SchoolLogo = DirectCast(Session("SchoolLogo"), System.Byte())
            Else
                rptParamInfo.SchoolLogo = rptfac.GetSchoolLogo.Image
                Session("SchoolLogo") = rptParamInfo.SchoolLogo
            End If
            If rptParamInfo.FilterList.ToLower.IndexOf("campusid in") <> -1 Then
                strErrMsg = "Please select a single campus"
            End If
            If rptParamInfo.FilterList.ToLower.IndexOf("campusid") <> -1 Then
                rptParamInfo.CampusId = getCampusId(rptParamInfo.FilterList)
            Else
                rptParamInfo.CampusId = HttpContext.Current.Request("cmpid").ToString
            End If
            'If the SQLID is not 0 then we need to create the class that generates DataSet for simple report.
            'Else we need to use OBJID to get the name of the object and create object using reflection.
            If rptParamInfo.SqlId > 0 Then
                ds = rptfac.GetReportDataSet(rptParamInfo)
            Else
                'Get the name of the object associated with the OBJID.
                objName = rptfac.GetOBJName(rptParamInfo.ObjId)
                'Use reflection to create an instance of the object from the name.
                facAssembly = Reflection.Assembly.LoadFrom(Server.MapPath("..") + "\bin\BusinessFacade.dll")
                Dim objBaseRptFac As BaseReportFacade
                objBaseRptFac = DirectCast(facAssembly.CreateInstance("FAME.AdvantageV1.BusinessFacade." + objName), BaseReportFacade)
                ds = objBaseRptFac.GetReportDataSet(rptParamInfo)
            End If

            ' US2729 12/23/2011 Janet Robinson added resourceid 771
            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
            If resourceId = 779 Then
                Dim arrValues() As String
                arrValues = rptParamInfo.FilterOther.Split(";")
                rptParamInfo.FilterListString = "Single Student"
                rptParamInfo.FilterOtherString = arrValues(1).ToString & ";" & arrValues(2).ToString
            End If

            Try
                If ds.Tables.Count <> 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        Session("ReportDS") = ds
                        Session("RptInfo") = rptParamInfo
                    Else
                        Session("ReportDS") = Nothing
                        Session("RptInfo") = Nothing
                    End If
                End If
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                Return "No data matching selection"
                Exit Function
            End Try

        Else
            Session("RptInfo") = Nothing
            Session("ReportDS") = Nothing
        End If 'strErrMsg = ""

        Return strErrMsg
    End Function

    Private Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Dim rptParamInfo As New ReportParamInfo
        Dim ds As New DataSet
        If Page.IsValid Then
            Dim strErrMsg As String = BuildReportDataSet(rptParamInfo, ds)

            If strErrMsg = "" Then
                'Since no error was found, proceed to display report in Export Viewer page.
                If Not (Session("ReportDS") Is Nothing) Then


                    Dim format = ddlExportTo.SelectedValue.ToString()
                    If (format IsNot Nothing) Then
                        LoadReport(format, ds, rptParamInfo)
                    End If


                    '   Make sure to clear any previously loaded report docuemnt objects
                    Session("RptObject") = Nothing

                Else
                    'Alert user of no data matching selection.
                    strErrMsg = "There is no data matching selection."
                    DisplayErrorMessage(strErrMsg)
                End If
            Else
                'Alert user of error.
                DisplayErrorMessage(strErrMsg)
            End If
        End If
    End Sub


    Private Sub LoadReport(format As String, ds As DataSet, rptParamInfo As Object)
        Dim rptXport As New RptExport
        Dim objReport As CrystalDecisions.CrystalReports.Engine.ReportDocument

        If Session("RptInfo") IsNot Nothing Then
            rptParamInfo = DirectCast(Session("RptInfo"), ReportParamInfo)
        End If

        PageTitle &= CType(rptParamInfo.RptTitle, String)

        If Session("RptObject") IsNot Nothing Then
            objReport = DirectCast(Session("RptObject"), CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Else
            objReport = (New AdvantageReportFactory).CreateAdvantageReport(ds, CType(rptParamInfo, ReportParamInfo))
            Session("RptObject") = objReport
        End If
        Dim contentType = ""
        rptXport.ExportToStream(Context, objReport, format, rptParamInfo.RptTitle.ToString)

    End Sub

    Private Sub lbtSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbtSearch.Click

        '   setup the properties of the new window
        Dim winSettings As String = "toolbar=no, status=yes, resizable=yes,width=900px,height=500px"
        Dim name As String = "StudentPopupSearch1"
        Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/sy/" + name + ".aspx?resid=294&mod=SA&cmpid=" + campusId
        'Dim url As String = CommonWebUtilities.GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/sy/" + name + ".aspx"

        '   build list of parameters to be passed to the new window
        Dim parametersArray(7) As String

        '   set StudentName
        parametersArray(0) = txtStudentId.ClientID

        '   set StuEnrollment
        parametersArray(1) = txtstuenrollmentid.ClientID
        parametersArray(2) = txtStuEnrollment.ClientID

        ''   set Terms
        parametersArray(3) = txttermid.ClientID
        parametersArray(4) = txtterm.ClientID

        '   set AcademicYears
        parametersArray(5) = txtacademicyearid.ClientID
        parametersArray(6) = txtacademicyear.ClientID
        parametersArray(7) = txtstudentidentifier.ClientID

        '   open new window and pass parameters
        CommonWebUtilities.OpenWindowAndPassParameters(Page, url, name, winSettings, parametersArray)

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInClientMessageBox(Me.Page, errorMessage)
        End If
    End Sub

    Private Sub btnFriendlyPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFriendlyPrint.Click
        Dim rptParamInfo As New ReportParamInfo
        Dim ds As New DataSet
        If Page.IsValid Then
            Dim strErrMsg As String = BuildReportDataSet(rptParamInfo, ds)
            If strErrMsg = "" Then
                'Since no error was found, proceed to display report in Export Viewer page.
                If Not (Session("ReportDS") Is Nothing) Then

                    '   Make sure to clear any previously loaded report docuemnt objects
                    Session("RptObject") = Nothing

                    '   Setup the properties of the new window
                    Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium + ",menubar=yes"
                    Dim name As String = "ReportFriendlyPrint"
                    Dim url As String = "../SY/" + name + ".aspx"
                    CommonWebUtilities.OpenClientChildWindow(Page, url, name, winSettings)

                Else
                    'Alert user of no data matching selection.
                    strErrMsg = "There is no data matching selection."
                    DisplayErrorMessage(strErrMsg)
                End If
            Else
                'Alert user of error.
                DisplayErrorMessage(strErrMsg)
            End If
        End If
    End Sub

    Private Sub lbtPrompt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtPrompt.Click
        Dim objRptParams As New RptParamsFacade
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim opId As Integer
        Dim fldValue As String
        Dim rptParamId As Integer
        Dim rowCounter As Integer
        Dim objCommon As New FAME.Common.CommonUtilities
        Dim errMsg As String

        If txtPrefName.Text = "" Then
            'Alert user that a Preference name is required.
            errMsg = "Preference Name is required."
            'Display Error Message.
            DisplayErrorMessage(errMsg)
        Else
            'Before adding or updating we record we need to validate the entries
            'in the filter others section.
            If ValidateFilterOthers() Then
                'If mode is NEW then we need to do an insert.
                If ViewState("MODE") = "NEW" Then
                    Dim prefId As String
                    'Generate a guid to be used as the prefid
                    prefId = Guid.NewGuid.ToString
                    'Save the basic user preference information
                    objRptParams.AddUserPrefInfo(prefId, Session("UserName"), CInt(ViewState("resid")), txtPrefName.Text)
                    'Save each sort preference in the lbxSort listbox. Use the order in the
                    'listbox to generate the sequence.
                    For i = 0 To lbxselsort.Items.Count - 1
                        objRptParams.AddUserSortPrefs(prefId, lbxselsort.Items(i).Value, i + 1)
                    Next

                    'If the FilterListSelections dt is not empty then we need to save the
                    'FilterList preferences
                    ds = DirectCast(Session("WorkingDS"), DataSet)
                    dt = ds.Tables("FilterListSelections")

                    For Each dr In dt.Rows
                        objRptParams.AddUserFilterListPrefs(prefId, dr("RptParamId"), dr("FldValue").ToString())
                    Next

                    'Special case - Modified by Michelle R. Rodriguez on 01/14/2005
                    'txtStudentIdentifier and txtStuEnrollmentId are hidden textboxes that contain
                    'the StudentId and StuEnrollId corresponding to the Student and Program selected 
                    'thru the Student Search pop-up.
                    If txtStuEnrollment.Text <> "" And txtstudentidentifier.Value <> "" Then
                        If txtstuenrollmentid.Value = System.Guid.Empty.ToString Then
                            'All Enrollments.
                            ' US2729 12/23/2011 Janet Robinson added resourceid 771
                            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                            'rptParamId = 181
                            If resourceId = 277 Then rptParamId = 181
                            If resourceId = 779 Then rptParamId = RptParam1
                            opId = AdvantageCommonValues.EqualToOperatorValue ' Equal To operator
                            fldValue = txtstudentidentifier.Value
                            objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                        Else
                            'One enrollment.
                            ' US2729 12/23/2011 Janet Robinson added resourceid 771
                            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                            'rptParamId = 182
                            If resourceId = 277 Then rptParamId = 182
                            If resourceId = 779 Then rptParamId = RptParam2
                            opId = AdvantageCommonValues.EqualToOperatorValue ' Equal To operator
                            fldValue = txtstuenrollmentid.Value
                            objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                        End If
                    End If

                    'We need to examine each row in the tblFilterOthers table control. If the
                    'user entered data in the textbox then we should save the info for that row.
                    For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
                        'If there is an entry in the textbox then get the values from the row
                        If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                            fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                            If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text <> "" Then
                                'Special case: The Between operator uses two values located on two different textboxes
                                fldValue &= ";" & DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                            End If
                            rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                            objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                            '
                        ElseIf DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text = "" And
                               (DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Empty" Or
                               DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Is Null") Then
                            'Empty or Is Null
                            fldValue = ""
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                            rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                            objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                        End If
                    Next

                    'We need to rebind the DataList. We cannot use the cache here since we need
                    'the latest info including the record just added. If the cache was used we
                    'would not see the new record in the datalist.
                    BindDataListFromDB()

                    'Change mode to Edit
                    'SetMode("EDIT")
                    'objCommon.SetBtnState(Form1, "EDIT", pObj)
                    objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                    ViewState("MODE") = "EDIT"
                    'Place the prefid guid into the txtPrefId textbox so it can be reused
                    'if we need to do an update later on.
                    txtPrefId.Text = prefId
                    '   set Style to Selected Item
                    CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Text, ViewState)

                Else
                    'Update the basic user preference information
                    objRptParams.UpdateUserPrefInfo(txtPrefId.Text, txtPrefName.Text)
                    'Delete any previous sort info and then add the ones in the lbxSelSort listbox.
                    objRptParams.DeleteUserSortPrefs(txtPrefId.Text)
                    'Save each sort preference in the lbxSort listbox. Use the order in the
                    'listbox to generate the sequence.
                    For i = 0 To lbxselsort.Items.Count - 1
                        objRptParams.AddUserSortPrefs(txtPrefId.Text, lbxselsort.Items(i).Value, i + 1)
                    Next
                    'Delete any previous filter list info and then add the ones in the FilterListSelections dt.
                    objRptParams.DeleteUserFilterListPrefs(txtPrefId.Text)
                    'Save each filter list preference in the FilterListSelections dt.
                    ds = DirectCast(Session("WorkingDS"), DataSet)
                    dt = ds.Tables("FilterListSelections")

                    For Each dr In dt.Rows
                        objRptParams.AddUserFilterListPrefs(txtPrefId.Text, dr("RptParamId"), dr("FldValue").ToString())
                    Next
                    'Delete any previous filter other info and then add the ones now specified
                    objRptParams.DeleteUserFilterOtherPrefs(txtPrefId.Text)

                    'Special case - Modified by Michelle R. Rodriguez on 01/14/2005
                    'txtStudentIdentifier and txtStuEnrollmentId are hidden textboxes that contain
                    'the StudentId and StuEnrollId corresponding to the Student and Program selected 
                    'thru the Student Search pop-up.
                    If txtStuEnrollment.Text <> "" And txtstudentidentifier.Value <> "" Then
                        If txtstuenrollmentid.Value = System.Guid.Empty.ToString Then
                            'All Enrollments.
                            ' US2729 12/23/2011 Janet Robinson added resourceid 771
                            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                            'rptParamId = 181
                            If resourceId = 277 Then rptParamId = 181
                            If resourceId = 779 Then rptParamId = RptParam1
                            opId = AdvantageCommonValues.EqualToOperatorValue ' Equal To operator
                            fldValue = txtstudentidentifier.Value
                            objRptParams.AddUserFilterOtherPrefs(txtPrefId.Text, rptParamId, opId, fldValue)
                        Else
                            'One enrollment.
                            ' US2729 12/23/2011 Janet Robinson added resourceid 771
                            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                            'rptParamId = 182
                            If resourceId = 277 Then rptParamId = 182
                            If resourceId = 779 Then rptParamId = RptParam2
                            opId = AdvantageCommonValues.EqualToOperatorValue ' Equal To operator
                            fldValue = txtstuenrollmentid.Value
                            objRptParams.AddUserFilterOtherPrefs(txtPrefId.Text, rptParamId, opId, fldValue)
                        End If
                    End If
                    '
                    For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
                        'If there is an entry in the textbox then get the values from the row
                        If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                            fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                            If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text <> "" Then
                                'Special case: The Between operator uses two values located on two different textboxes
                                fldValue &= ";" & DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                            End If
                            rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                            objRptParams.AddUserFilterOtherPrefs(txtPrefId.Text, rptParamId, opId, fldValue)
                            '
                        ElseIf DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text = "" And
                               (DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Empty" Or
                               DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Text = "Is Null") Then
                            'Empty or Is Null
                            fldValue = ""
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value
                            rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                            objRptParams.AddUserFilterOtherPrefs(txtPrefId.Text, rptParamId, opId, fldValue)
                        End If
                    Next

                    'We need to rebind the DataList in case txtPrefName had changed. 
                    BindDataListFromDB()
                    '   set Style to Selected Item
                    CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Text, ViewState)
                End If  'Test if mode is NEW or EDIT
            End If 'ValidateFilterOThers returns True
        End If 'txtPrefName.Text is empty
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add buttons and listboxes
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnExport)
        controlsToIgnore.Add(btnFriendlyPrint)
        controlsToIgnore.Add(btnSearch)
        controlsToIgnore.Add(btnadd)
        controlsToIgnore.Add(btnremove)
        controlsToIgnore.Add(lbxfilterlist)
        controlsToIgnore.Add(lbxselfilterlist)

        'Dim tbl As Table = DirectCast(FindControl("tblFilterOthers"), Table)
        Dim tbl As Table = Nothing
        If Not tblFilterOthers Is Nothing Then
            tbl = DirectCast(tblFilterOthers, Table)
        End If

        If Not (tbl Is Nothing) Then
            For i As Integer = 1 To tbl.Rows.Count - 1
                Dim ddl As DropDownList = DirectCast(tbl.Rows(i).Cells(1).Controls(0), DropDownList)
                Dim lbl As Label = DirectCast(tbl.Rows(i).Cells(0).Controls(0), Label)

                If Not (ddl Is Nothing) Then

                    'add Operators ddl
                    controlsToIgnore.Add(ddl)

                    Dim rptParamId As Integer
                    rptParamId = Integer.Parse(ddl.ID.Substring(3))
                    Dim ctrl As Control = MyBase.FindControlRecursive("txt2" & rptParamId)
                    If Not (ctrl Is Nothing) Then
                        If ddl.SelectedValue = AdvantageCommonValues.BetweenOperatorValue Then
                            ' US2729 12/23/2011 Janet Robinson added resourceid 771
                            ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                            If ViewState("resid") = 779 Then
                                ctrl.Visible = False
                            Else
                                ctrl.Visible = True
                            End If
                            ctrl.Visible = True
                        Else
                            ctrl.Visible = False
                        End If
                    End If
                End If
            Next
        End If
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub SavePreferences()
        Dim objRptParams As New RptParamsFacade
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim dr As DataRow
        Dim opId As Integer
        Dim fldValue As String
        Dim rptParamId As Integer
        Dim rowCounter As Integer ', rowcounter1
        '   Dim cellCounter As Integer
        Dim objCommon As New FAME.Common.CommonUtilities
        Dim errMsg As String

        If txtPrefName.Text = "" Then
            'Alert user that a Preference name is required.
            errMsg = "Preference Name is required."
            'Display Error Message.
            DisplayErrorMessage(errMsg)
        Else
            'Before adding or updating we record we need to validate the entries
            'in the filter others section.
            If ValidateFilterOthers() Then
                'If mode is NEW then we need to do an insert.
                If ViewState("MODE") = "NEW" Then
                    Dim prefId As String
                    'Generate a guid to be used as the prefid
                    prefId = Guid.NewGuid.ToString
                    'Save the basic user preference information
                    objRptParams.AddUserPrefInfo(prefId, Session("UserName"), CInt(ViewState("resid")), txtPrefName.Text)

                    'Save each sort preference in the lbxSort listbox. Use the order in the
                    'listbox to generate the sequence.
                    For i = 0 To lbxselsort.Items.Count - 1
                        objRptParams.AddUserSortPrefs(prefId, lbxselsort.Items(i).Value, i + 1)
                    Next

                    'If the FilterListSelections dt is not empty then we need to save the
                    'FilterList preferences
                    ds = DirectCast(Session("WorkingDS"), DataSet)
                    dt = ds.Tables("FilterListSelections")
                    For Each dr In dt.Rows
                        objRptParams.AddUserFilterListPrefs(prefId, dr("RptParamId"), dr("FldValue").ToString())
                    Next

                    'We need to examine each row in the tblFilterOthers table control. If the
                    'user entered data in the textbox then we should save the info for that row.
                    For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
                        'If there is an entry in the textbox then get the values from the row
                        ''If operatorId is 6- then it is between value. so two text boxes will be there, they are aded with a ; seperated

                        If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value

                            ''If operatorId is 6- then it is between value. so two text boxes will be there, they are aded with a ; seperated
                            If opId = 6 Then
                                fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                Dim Fldval1 As String
                                Fldval1 = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                                fldValue = fldValue + ";" + Fldval1
                                rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                                objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                            Else
                                fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                                objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                            End If

                        End If
                    Next

                    ''Commented on July 10 2009
                    ''to avoid adding the same values more than once
                    ' For rowcounter1 = 1 To tblStudentSearch.Rows.Count - 1 'The first row is empty
                    'If there is an entry in the textbox then get the values from the row
                    'If txtStudentId.Text <> "" Then
                    '    opId = 1
                    '    fldValue = txtStudentIdentifier.Text
                    '    rptParamId = 181
                    '    objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                    'End If
                    If txtStuEnrollment.Text <> "" Then
                        opId = 1
                        fldValue = txtstuenrollmentid.Value
                        ' US2729 12/23/2011 Janet Robinson added resourceid 771
                        ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                        'rptParamId = 182
                        If resourceId = 277 Then rptParamId = 182
                        If resourceId = 779 Then rptParamId = RptParam2
                        objRptParams.AddUserFilterOtherPrefs(prefId, rptParamId, opId, fldValue)
                    End If
                    '  Next

                    'We need to rebind the DataList. We cannot use the cache here since we need
                    'the latest info including the record just added. If the cache was used we
                    'would not see the new record in the datalist.
                    BindDataListFromDB()
                    'Change mode to Edit
                    'SetMode("EDIT")
                    'objCommon.SetBtnState(Form1, "EDIT")
                    objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                    ViewState("MODE") = "EDIT"
                    'Place the prefid guid into the txtPrefId textbox so it can be reused
                    'if we need to do an update later on.
                    txtPrefId.Text = prefId
                Else
                    'Update the basic user preference information
                    objRptParams.UpdateUserPrefInfo(txtPrefId.Text, txtPrefName.Text)

                    'Delete any previous sort info and then add the ones in the lbxSelSort listbox.
                    objRptParams.DeleteUserSortPrefs(txtPrefId.Text)

                    'Save each sort preference in the lbxSort listbox. Use the order in the
                    'listbox to generate the sequence.
                    For i = 0 To lbxselsort.Items.Count - 1
                        objRptParams.AddUserSortPrefs(txtPrefId.Text, lbxselsort.Items(i).Value, i + 1)
                    Next

                    'Delete any previous filter list info and then add the ones in the FilterListSelections dt.
                    objRptParams.DeleteUserFilterListPrefs(txtPrefId.Text)

                    'Save each filter list preference in the FilterListSelections dt.
                    ds = DirectCast(Session("WorkingDS"), DataSet)
                    dt = ds.Tables("FilterListSelections")
                    For Each dr In dt.Rows
                        objRptParams.AddUserFilterListPrefs(txtPrefId.Text, dr("RptParamId"), dr("FldValue").ToString())
                    Next

                    'Delete any previous filter other info and then add the ones now specified
                    objRptParams.DeleteUserFilterOtherPrefs(txtPrefId.Text)
                    For rowCounter = 1 To tblFilterOthers.Rows.Count - 1 'The first row is empty
                        'If there is an entry in the textbox then get the values from the row
                        If DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text <> "" Then
                            opId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(1).Controls(0), DropDownList).SelectedItem.Value

                            ''If operatorId is 6- then it is between value. so two text boxes will be there, they are aded with a ; seperated
                            If opId = 6 Then
                                fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                Dim Fldval1 As String
                                Fldval1 = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(3).Controls(0), TextBox).Text
                                fldValue = fldValue + ";" + Fldval1
                                rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                                objRptParams.AddUserFilterOtherPrefs(txtPrefId.Text, rptParamId, opId, fldValue)
                            Else
                                fldValue = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).Text
                                rptParamId = DirectCast(tblFilterOthers.Rows(rowCounter).Cells(2).Controls(0), TextBox).ID.Substring(3)
                                objRptParams.AddUserFilterOtherPrefs(txtPrefId.Text, rptParamId, opId, fldValue)
                            End If

                        End If
                    Next

                    'For rowcounter1 = 1 To tblStudentSearch.Rows.Count - 1 'The first row is empty
                    '    'If there is an entry in the textbox then get the values from the row
                    '    If txtStudentId.Text <> "" Then
                    '        opId = 1
                    '        fldValue = txtStudentIdentifier.Text
                    '        rptParamId = 181
                    '        objRptParams.AddUserFilterOtherPrefs(txtPrefId.Text, rptParamId, opId, fldValue)
                    '    End If
                    ''Commented on July 10 2009
                    ''to avoid adding the same values more than once
                    If txtStuEnrollment.Text <> "" Then
                        opId = 1
                        fldValue = txtstuenrollmentid.Value
                        ' US2729 12/23/2011 Janet Robinson added resourceid 771
                        ' DE7165 2/9/2012 Janet Robinson resid s/b 779
                        'rptParamId = 182
                        If resourceId = 277 Then rptParamId = 182
                        If resourceId = 779 Then rptParamId = RptParam2
                        objRptParams.AddUserFilterOtherPrefs(txtPrefId.Text, rptParamId, opId, fldValue)
                    End If
                    ' Next

                End If  'Test if mode is NEW or EDIT
            End If 'ValidateFilterOThers returns True
            'CommonWebUtilities.SetStyleToSelectedItem(dlstPrefs, txtPrefId.Text, ViewState, Header1)
        End If 'txtPrefName.Text is empty
    End Sub
    Protected Sub ddlSpeedOperator_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSpeedOperator.SelectedIndexChanged
        If ddlSpeedOperator.SelectedValue = "1" Then
            lblBetweenAnd.Visible = True
            txtSpeedVal2.Visible = True
        Else
            lblBetweenAnd.Visible = False
            txtSpeedVal2.Visible = False
        End If
    End Sub

    Protected Sub ddlAccuracyOperator_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAccuracyOperator.SelectedIndexChanged
        If ddlAccuracyOperator.SelectedValue = "1" Then
            lblaccuracybetweenand.Visible = True
            txtAccuracyVal2.Visible = True
        Else
            lblaccuracybetweenand.Visible = False
            txtAccuracyVal2.Visible = False
        End If
    End Sub
    Private Function getCampusId(ByVal filterList As String) As String
        Dim campId As String = String.Empty
        Dim campIdx1, campIdx2, campIdx3 As Integer
        campIdx1 = filterList.ToLower.IndexOf("campusid")
        campIdx2 = filterList.Substring(campIdx1).IndexOf("'")
        campIdx3 = filterList.Substring(campIdx1 + campIdx2 + 1).IndexOf("'")
        campId = filterList.Substring(campIdx1 + campIdx2 + 1, campIdx3)
        Return campId

    End Function
End Class