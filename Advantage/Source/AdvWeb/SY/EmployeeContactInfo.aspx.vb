﻿Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common
Imports Advantage.Business.Logic.Layer

Partial Class EmployeeContactInfo
    Inherits BasePage

    Private state As AdvantageSessionState
    Private empId As String
    Private empContactInfoId As String
    Private errorMessage As String
    Private mruProvider As MRURoutines

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub



#End Region

    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Private resourceId As Integer
    Private EmployeeId As String
    Private boolSwitchCampus As Boolean = False

    Private Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
    Private Function getEmployeeFromStateObject(ByVal paramResourceId As Integer) As BO.EmployeeMRU

        Dim objStateInfo As New AdvantageStateInfo
        Dim objEmployeeState As New BO.EmployeeMRU


        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        MyBase.GlobalSearchHandler(3)

        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
        HttpContext.Current.Items("Language") = "En-US"

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            EmployeeId = Guid.Empty.ToString()
            AdvantageSession.MasterEmployeeId = EmployeeId
        Else
            EmployeeId = AdvantageSession.MasterEmployeeId

        End If


        txtEmpId.Text = EmployeeId


        Dim objGetStudentStatusBar As New AdvantageStateInfo

        mruProvider = New MRURoutines(Me.Context, GetAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)
        objGetStudentStatusBar = mruProvider.BuildEmployeeStatusBar(AdvantageSession.MasterEmployeeId)
        With objGetStudentStatusBar
            AdvantageSession.MasterName = objGetStudentStatusBar.NameValue
            AdvantageSession.MasterEmployeeName = objGetStudentStatusBar.NameValue
            AdvantageSession.MasterEmployeeAddress1 = objGetStudentStatusBar.Address1
            AdvantageSession.MasterEmployeeAddress2 = objGetStudentStatusBar.Address2
            AdvantageSession.MasterEmployeeCity = objGetStudentStatusBar.City
            AdvantageSession.MasterEmployeeState = objGetStudentStatusBar.State
            AdvantageSession.MasterEmployeeZip = objGetStudentStatusBar.Zip

        End With

        If Not String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            Master.ShowHideStatusBarControl(True)
            Master.PageObjectId = EmployeeId
            Master.PageResourceId = Request.QueryString("resid")
            Master.setHiddenControlForAudit()
        Else
            Master.ShowHideStatusBarControl(False)
        End If

        With objEmployeeState
            .EmployeeId = New Guid(EmployeeId)
            .Name = objStateInfo.NameValue
        End With

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            Return Nothing
        Else
            Return objEmployeeState
        End If


    End Function

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        campusId = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        Dim objEmployeeState As New BO.EmployeeMRU
        objEmployeeState = getEmployeeFromStateObject(69) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objEmployeeState Is Nothing Then
            MyBase.RedirectToEmployeeSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objEmployeeState
            empId = .EmployeeId.ToString

        End With

        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)

        '   if we have an employeeContactInfo in the session then use it, else create a new one
        If CommonWebUtilities.IsValidGuid(empId) Then
            If state("employeeContactInfo") Is Nothing Then
                state("employeeContactInfo") = (New EmployeesFacade).GetContactInfo(empId)

            Else
                '   if this is not the same employee that we have in state... goto the backend
                If Not CType(state("employeeContactInfo"), FAME.AdvantageV1.Common.EmployeeContactInfo).EmpId = empId Then
                    state("employeeContactInfo") = (New EmployeesFacade).GetContactInfo(empId)

                End If
            End If
        Else
            state("employeeContactInfo") = New FAME.AdvantageV1.Common.EmployeeContactInfo(empId)

        End If



        empContactInfoId = CType(state("employeeContactInfo"), FAME.AdvantageV1.Common.EmployeeContactInfo).EmpContactInfoId
        Master.PageObjectId = empContactInfoId
        Master.PageResourceId = resourceId
        Master.setHiddenControlForAudit()

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(3, AdvantageSession.MasterName)
            End If
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW")

            '   bind data
            BindData(state("employeeContactInfo"))
            MyBase.uSearchEntityControlId.Value = ""
        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT")
        End If

        InitButtonsForEdit()

        GetInputMaskValue()
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        state("employeeContactInfo") = BuildEmployeeContactInfo(empId)
        Dim result As String = (New EmployeesFacade).UpdateContactInfo(state("employeeContactInfo"), AdvantageSession.UserState.UserName)

        If errorMessage = "" Then
            If Not result = "" Then
                DisplayRADAlert(CallbackType.Postback, "Error2", errorMessage, "Save Error")
                Exit Sub
            Else
                state("employeeContactInfo") = (New EmployeesFacade).GetContactInfo(empId)
                state("empId") = CType(state("employeeContactInfo"), FAME.AdvantageV1.Common.EmployeeContactInfo).EmpId
                BindData(state("employeeContactInfo"))
            End If

        End If
    End Sub
    Private Sub BindData(ByVal employeeContactInfo As FAME.AdvantageV1.Common.EmployeeContactInfo)

        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        With employeeContactInfo
            '   IsInDB
            cbxIsInDB.Checked = .IsInDB

            '   EmpContactInfoId
            txtEmpContactInfoId.Text = .EmpContactInfoId

            'bind ForeignHomePhone
            chkForeignHomePhone.Checked = .ForeignHomePhone

            'bind ForeignWorkPhone
            chkForeignWorkPhone.Checked = .ForeignWorkPhone

            'bind ForeignCellPhone
            chkForeignCellPhone.Checked = .ForeignCellPhone

            '   bind WorkPhone
            txtWorkPhone.Text = .WorkPhone
            'If txtWorkPhone.Text <> "" And chkForeignWorkPhone.Checked = False And txtWorkPhone.Text.Length >= 1 Then
            '    txtWorkPhone.Text = facInputMasks.ApplyMask(strMask, txtWorkPhone.Text)
            'End If

            '   bind WorkPhone
            txtHomePhone.Text = .HomePhone
            'If txtHomePhone.Text <> "" And chkForeignHomePhone.Checked = False And txtHomePhone.Text.Length >= 1 Then
            '    txtHomePhone.Text = facInputMasks.ApplyMask(strMask, txtHomePhone.Text)
            'End If

            '   bind WorkPhone
            txtCellPhone.Text = .CellPhone
            'If txtCellPhone.Text <> "" And chkForeignCellPhone.Checked = False And txtCellPhone.Text.Length >= 1 Then
            '    txtCellPhone.Text = facInputMasks.ApplyMask(strMask, txtCellPhone.Text)
            'End If


            PhoneMask(chkForeignCellPhone.Checked, txtCellPhone)
            PhoneMask(chkForeignHomePhone.Checked, txtHomePhone)
            PhoneMask(chkForeignWorkPhone.Checked, txtWorkPhone)
            'bind(Beeper)
            txtBeeper.Text = .Beeper

            '   bind WorkEmail
            txtWorkEmail.Text = .WorkEmail

            '   bind HomeEmail
            txtHomeEmail.Text = .HomeEmail

            '   bind modUser
            txtModUser.Text = .ModUser

            '   bind ModDate
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Function BuildEmployeeContactInfo(ByVal empId As String) As FAME.AdvantageV1.Common.EmployeeContactInfo

        '   instantiate class
        Dim employeeContactInfo As New FAME.AdvantageV1.Common.EmployeeContactInfo(empId)

        With employeeContactInfo
            '   IsInDB
            .IsInDB = cbxIsInDB.Checked

            '   EmpContactInfoId
            .EmpContactInfoId = txtEmpContactInfoId.Text

            '   get WorkPhone
            .WorkPhone = txtWorkPhone.Text.Trim

            '   get HomePhone
            .HomePhone = txtHomePhone.Text.Trim

            '   get CellPhone
            .CellPhone = txtCellPhone.Text.Trim

            '   get Beeper
            .Beeper = txtBeeper.Text.Trim

            '   get WorkEmail
            .WorkEmail = txtWorkEmail.Text.Trim

            '   get HomeEmail
            .HomeEmail = txtHomeEmail.Text.Trim

            '   get Homephone
            'phoneMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
            'If txtHomePhone.Text <> "" And chkForeignHomePhone.Checked = False Then
            '    .HomePhone = facInputMask.RemoveMask(phoneMask, txtHomePhone.Text)
            'Else
            '    .HomePhone = txtHomePhone.Text
            'End If

            ''   get Workphone
            'If txtWorkPhone.Text <> "" And chkForeignWorkPhone.Checked = False Then
            '    .WorkPhone = facInputMask.RemoveMask(phoneMask, txtWorkPhone.Text)
            'Else
            '    .WorkPhone = txtWorkPhone.Text
            'End If

            ''   get Cellphone
            'If txtCellPhone.Text <> "" And chkForeignCellPhone.Checked = False Then
            '    .CellPhone = facInputMask.RemoveMask(phoneMask, txtCellPhone.Text)
            'Else
            '    .CellPhone = txtCellPhone.Text
            'End If

            '   get ForeignHomePhone
            .ForeignHomePhone = chkForeignHomePhone.Checked

            '   get ForeignCellPhone
            .ForeignCellPhone = chkForeignCellPhone.Checked

            '   get ForeignWorkPhone
            .ForeignWorkPhone = chkForeignWorkPhone.Checked


            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With

        '   return data
        Return employeeContactInfo

    End Function
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = False
        Else
            btnNew.Enabled = False
        End If
    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(chkForeignCellPhone)
        controlsToIgnore.Add(chkForeignWorkPhone)
        controlsToIgnore.Add(chkForeignHomePhone)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Private Sub GetInputMaskValue()
        Dim objCommon As New CommonUtilities

        Dim strWorkPhoneReq, strHomePhoneReq, strCellPhoneReq As String

        'Get The RequiredField Value
        strWorkPhoneReq = objCommon.SetRequiredColorMask("WorkPhone")
        strHomePhoneReq = objCommon.SetRequiredColorMask("HomePhone")
        strCellPhoneReq = objCommon.SetRequiredColorMask("CellPhone")
    End Sub
    Private Sub chkForeignCellPhone_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignCellPhone.CheckedChanged
        PhoneMask(chkForeignCellPhone.Checked, txtCellPhone)
        CommonWebUtilities.SetFocus(Me.Page, txtCellPhone)
    End Sub

    Private Sub chkForeignHomePhone_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignHomePhone.CheckedChanged
        PhoneMask(chkForeignHomePhone.Checked, txtHomePhone)
        CommonWebUtilities.SetFocus(Me.Page, txtHomePhone)
    End Sub

    Private Sub chkForeignWorkPhone_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignWorkPhone.CheckedChanged
        PhoneMask(chkForeignWorkPhone.Checked, txtWorkPhone)
        CommonWebUtilities.SetFocus(Me.Page, txtWorkPhone)
    End Sub

    Private Sub PhoneMask(ByVal bchecked As Boolean, ByVal txtPhone As RadMaskedTextBox)
        If bchecked = False Then
            txtPhone.Mask = "(###)-###-####"
            txtPhone.DisplayMask = "(###)-###-####"
            txtPhone.DisplayPromptChar = ""
        Else
            txtPhone.Text = ""
            txtPhone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtPhone.DisplayMask = ""
            txtPhone.DisplayPromptChar = ""
        End If
    End Sub
End Class
