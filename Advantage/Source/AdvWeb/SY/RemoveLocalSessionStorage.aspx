﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="RemoveLocalSessionStorage.aspx.vb" Inherits="SY_RemoveLocalSessionStorage" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content4" ContentPlaceHolderID="additional_head" runat="server">
</asp:Content>


<asp:Content ID="content2" ContentPlaceHolderID="contentmain2" runat="server">
    <script src="../Scripts/Storage/storageCache.js"></script>
    <script src="../Scripts/common-util.js"></script>
    <script type="text/javascript" src="../Scripts/viewModels/removeLocalSessionStorage.js"></script>
    <script type="text/javascript" src="../Scripts/common/kendoControlSetup.js"></script>
    <script id="infoTemplate" type="text/x-kendo-template">
        <div class="infoMessageTemplate">
            <img width="32px" height="32px" src=#= src #   />
            <h3>#= title #</h3>
            <p>#= message #</p>
        </div>
    </script>

            <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
                <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">

                    <br />
                    <div class="boxContainer" style="margin: 10px;height:500px;">
                        <h3>
                            <asp:Label ID="headerTitle" runat="server"></asp:Label>
                        </h3>
                           <br />
                                <button id="btnClear" class="k-button" type="button" style="width: 175px;">Remove Cache</button>
                                <button type="button" id="btnRefresh" class="k-button">Refresh</button>
                                <br />
                                <br />
                                <div id="totalCount"></div>
                       
                    </div>
                    <br />


                    <div id="errorDetails" style="display: none"></div>
                    <input type="hidden" id="hdnUserName" runat="server" class="hdnUserName" clientidmode="Static" />
                    <span id="kNotification"></span>

                </telerik:RadPane>
            </telerik:RadSplitter>


</asp:Content>


