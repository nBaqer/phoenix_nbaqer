
<%@ Page Language="vb" AutoEventWireup="false" Inherits="UsersCampGrps" CodeFile="UsersCampGrps.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Assign Users to Roles/Campus Groups</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<link rel="stylesheet" type="text/css" href="../css/systememail.css">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		
		<link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
		<LINK href="../css/systememail.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body id="Body1" leftMargin="0" topMargin="0" runat="server">
		<form id="Form1" method="post" runat="server">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><IMG src="../images/advantage_assign_users.jpg"></td>
					<td class="topemail"><A class="close" onclick="top.close()" href=#>X Close</A></td>
				</tr>
			</table>
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right"><asp:button id="btnSave" runat="server" Text="Save" CssClass="save"></asp:button><asp:button id="btnNew" runat="server" Text="New" CssClass="new" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Enabled="False"></asp:button></td>
							</tr>
						</table>
			
			<table class="maincontenttable" id="Table5" cellSpacing="0" cellPadding="0" width="100%"
				border="0">
				<TBODY>
					<tr>
						<td class="detailsFrame">
							<div class="scrollwholeusers">
								<!-- begin content table-->
								<table class="contenttable" cellSpacing="0" cellPadding="0" width="100%" align="center">
											<asp:label id="lblError" CssClass="label" ForeColor="red" visible="false" Runat="server"></asp:label>
											<tr>
												<td class="fourcolumnheader" noWrap><asp:label id="lblRoles" CssClass="label" Runat="server" Font-Bold="True">Roles</asp:label></td>
												<td class="fourcolumnheader" noWrap><asp:label id="lblAvailableCampGrps" CssClass="label" Runat="server" Font-Bold="True">Available Campus Groups</asp:label></td>
												<td class="fourcolumnheaderspacer" noWrap></td>
												<td class="fourcolumnheader" noWrap><asp:label id="lblStudentSkills" CssClass="label" Runat="server" Font-Bold="True">Selected Campus Groups</asp:label></td>
											</tr>
											<tr>
												<td class="fourcolumncontent" noWrap><asp:listbox id="lbxRoles" Runat="server" Rows="20" cssClass="tostudentskillsusersy" AutoPostback="True"></asp:listbox></td>
												<td class="fourcolumncontent" noWrap><asp:listbox id="lbxCampusGroups" Runat="server" Rows="20" cssClass="tostudentskillsusersy" AutoPostBack="True"></asp:listbox></td>
												<td class="fourcolumnbuttons" noWrap><asp:button id="btnAdd" Text="Add >" CssClass="buttons1" Runat="server" Enabled="False"></asp:button><br>
													<asp:button id="btnRemove" Text="< Remove" CssClass="buttons1" Runat="server" Enabled="False"></asp:button></td>
												<td class="fourcolumncontent" noWrap><asp:listbox id="lbxSelected" Runat="server" Rows="20" cssClass="tostudentskillsusersy" AutoPostBack="True"></asp:listbox></td>
											</tr>
										</table>
								<!-- end content table -->
		</DIV></TD></TR></TBODY></TABLE><asp:textbox id="txtSkillGrpId" visible="False" Runat="server"></asp:textbox><asp:button id="btncheck" Text="Check" Runat="server" Visible="false"></asp:button><input id="Hidden1" type="hidden" name="txtcheck" runat="server">
			<asp:textbox id="txtStudentId" Runat="server" Visible="False"></asp:textbox>
		<div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
		</FORM>
	</body>
</HTML>

