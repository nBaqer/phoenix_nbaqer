﻿Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports System.Collections
Imports System.Diagnostics
Imports System.Drawing
Imports System.IO
Imports System.Xml
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports Telerik.Web.UI
Imports FAME.Advantage.Reporting.Logic

Partial Class SY_ManageSecurity
    Inherits BasePage
#Region " Web Form Designer Generated Code "
    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Private rolesResourcesAccessRightsDS As DataSet
    Private clientIds As New NameValueCollection()
    Private clientXmlDoc As New XmlDocument()
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim mContext As HttpContext
        mContext = HttpContext.Current
        txtResourceId.Text = CType(mContext.Items("ResourceId"), String)

        Dim objCommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        'Dim ds As New DataSet
        'Dim sw As New StringWriter
        'Dim ds2 As New DataSet
        '  Dim sStatusId As String

        Dim advantageUserState As User = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        mContext = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        If Not IsPostBack Then
            BuilDDLGroups()
            BuildDropDownLists()
            ddlGroups.SelectedIndex = 1
            BuildDDLModules()
            'BuildDDLTypes()
            Try
                ddlModules.SelectedIndex = 1
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlModules.SelectedIndex = 0
            End Try

            'Build Page Dropdownlist
            BuildPageTypeDropDownList()
            Try
                ddlTypes.SelectedIndex = 1
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlTypes.SelectedIndex = 0
            End Try
            BindGrid()
        End If

        'Disable btnNew and btnDelete button as its not needed anymore
        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        btnNew.Enabled = False
        btnDelete.Enabled = False

        headerTitle.Text = Header.Title
    End Sub
    Private Sub BindGrid()
        RadGrid1.Visible = True

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        Dim intSchoolOptions As Integer = CommonWebUtilities.SchoolSelectedOptions(CType(myAdvAppSettings.AppSettings("SchedulingMethod"), String))
        RadGrid1.DataSource = (New GradesFacade).GetPages(ddlGroups.SelectedValue.ToString, CInt(ddlModules.SelectedValue), CInt(ddlTypes.SelectedValue), intSchoolOptions, campusId)
        RadGrid1.DataBind()
    End Sub
    Private Sub BuildDropDownLists()
        BuildDDLModules()
        ' BuildDDLTypes()
    End Sub
    Private Sub BuilDDLGroups()

        'build Groups DDL
        With ddlGroups
            .DataSource = (New UserSecurityFacade).GetAllRoles
            .DataTextField = "Role"
            .DataValueField = "RoleId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select One", ""))
        End With

    End Sub
    Private Sub BuildDDLModules()
        'build Modules DDL
        If Not ddlGroups.SelectedValue = "" Then
            ddlModules.Enabled = True
            With ddlModules
                .DataSource = (New ResourcesRelationsFacade).GetModulesByRoleId(ddlGroups.SelectedValue)
                .DataTextField = "Resource"
                .DataValueField = "ResourceId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select One", ""))
            End With
        Else
            ddlModules.Enabled = False
        End If
    End Sub
    Private Sub BuildDDLTypes()
        'build Types DDL
        With ddlTypes
            .DataSource = (New ResourcesRelationsFacade).GetResourceTypes
            .DataTextField = "ResourceType"
            .DataValueField = "ResourceTypeId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select One", ""))
        End With
    End Sub
    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuildList.Click
        Dim result As String = ValidateLHS()
        If Not result = "" Then
            DisplayErrorMessage(result)
            Exit Sub
        End If

        'Enable the Save button if the user has permission
        If pObj.HasFull Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        BindGrid()

    End Sub
    'Private Function BuildItemsNode(ByVal navXmlDoc As XmlDocument, ByVal selectedModule As String, ByVal resourceTypeId As String) As XmlNode

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    'these are all the nodes to be added to the selected Module
    '    Dim sb As New StringBuilder()
    '    'this is entity tabs parent node
    '    sb.Append("/Nodes/Node[Text=" + "'" + selectedModule + "']/Nodes/Node[ResourceTypeId=8 and ChildTypeId=" + resourceTypeId + "] ")
    '    sb.Append(" | ")
    '    ''these are entity tabs nodes
    '    'sb.Append("/Nodes/Node[Text=" + "'" + selectedModule + "']/Nodes/Node[ResourceTypeId=8]/Nodes/Node[ResourceTypeId=" + resourceTypeId + "] ")
    '    'sb.Append(" | ")
    '    'these are other nodes
    '    sb.Append("/Nodes/Node[Text=" + "'" + selectedModule + "']/Nodes/Node[ResourceTypeId=" + resourceTypeId + " or (ResourceTypeId=2 and ChildTypeId=" + resourceTypeId + ")] ")

    '    'create the nodelist
    '    Dim nodeList As XmlNodeList = navXmlDoc.SelectNodes(sb.ToString)

    '    'add all nodes to the root
    '    Dim xmlDoc As New XmlDocument()
    '    Dim root As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "Nodes", Nothing)

    '    'create the module nodes
    '    Dim moduleNodes As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "Nodes", Nothing)
    '    root.AppendChild(moduleNodes)

    '    'create the module node
    '    Dim moduleNode As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "Node", Nothing)
    '    moduleNodes.AppendChild(moduleNode)

    '    'create the Text node
    '    Dim moduleNodeText As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "Text", Nothing)
    '    moduleNodeText.InnerText = selectedModule
    '    moduleNode.AppendChild(moduleNodeText)

    '    For Each node As XmlNode In nodeList
    '        Dim iNode As XmlNode = xmlDoc.ImportNode(node, True)
    '        ''Modified by Saraswathi lakshmanan On June 15 2010
    '        ''To fix the issue 19081- Hide pages in Manage Security also
    '        ''19147: QA: Need not see the Manage Configuration Settings page on the Manage Security page. 
    '        If iNode.FirstChild.InnerText.Contains("Manage Configuration Settings") Then
    '        ElseIf (MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" And iNode.FirstChild.InnerText.Contains("Post Grade by Student (Instructor Level)")) Then
    '        ElseIf (MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" And iNode.FirstChild.InnerText.Contains("Setup Grade Book Weightings")) Then
    '        ElseIf (MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" And iNode.FirstChild.InnerText.Contains("Apply Grade Book Weights to Class Sections")) Then
    '        ElseIf (MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "instructorlevel" And iNode.FirstChild.InnerText.Contains("Post Grade by Student (Course Level)")) Then
    '        Else
    '            moduleNode.AppendChild(iNode)
    '        End If
    '    Next

    '    'return the node with all navigation items
    '    Return root

    'End Function

    Protected Sub rptNavigationNodes_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptNavigationNodes.ItemDataBound
        Select Case e.Item.ItemType

            Case ListItemType.Header

            Case ListItemType.Item, ListItemType.AlternatingItem

                Dim resourceTypeId As Integer = 0
                If Not IsNothing(e.Item.DataItem("ResourceTypeId")) Then
                    resourceTypeId = Integer.Parse(CType(e.Item.DataItem("ResourceTypeId"), XmlElement).InnerText)
                End If

                'setup text of navigation node with indentation
                Dim lblIndentation As Label = CType(e.Item.FindControl("lblIndentation"), Label)
                lblIndentation.Text = GetIndentation(e.Item.DataItem)

                Dim lbl As Label = CType(e.Item.FindControl("lblNavigationNode"), Label)
                lbl.Text = CType(e.Item.DataItem("Text"), XmlElement).InnerText

                'set ResourceId
                Dim lblResourceId As Label = CType(e.Item.FindControl("lblResourceId"), Label)
                If resourceTypeId = 3 Or resourceTypeId = 4 Or resourceTypeId = 5 Then
                    lblResourceId.Text = ExtractResourceIdFromURL(CType(e.Item.DataItem("Url"), XmlElement).InnerText)
                Else
                    lblResourceId.Text = "0"
                End If

                'bind check boxes only for resources that need permissions            
                If resourceTypeId = 3 Or resourceTypeId = 4 Or resourceTypeId = 5 Then
                    BindCheckBoxes(e, GetPermissionsForThisRoleOnThisResource(ddlGroups.SelectedValue, ExtractResourceIdFromURL(CType(e.Item.DataItem("Url"), XmlElement).InnerText)), resourceTypeId)
                Else
                    InsertJavascriptCode(e, resourceTypeId)
                End If

                'get client and parent ids
                Dim depth As String = GetNodeDepth(e.Item.DataItem)
                Dim clientId As String = CType(e.Item.FindControl("Checkbox1"), CheckBox).ClientID
                UpdateNodeList(depth, clientId)

                'add node to client xml document
                Dim clientNode As XmlNode = clientXmlDoc.CreateNode(XmlNodeType.Element, Nothing, clientId, Nothing)
                If clientXmlDoc.HasChildNodes Then
                    clientXmlDoc.SelectSingleNode("//" + GetParentNodeClientId(depth)).AppendChild(clientNode)
                Else
                    clientXmlDoc.AppendChild(clientNode)
                End If

        End Select
    End Sub

    Private Function GetIndentation(ByVal node As XmlNode) As String
        Dim indentation As New StringBuilder()
        Do While Not IsNothing(node.ParentNode)
            node = node.ParentNode
            indentation.Append("&nbsp;&nbsp;&nbsp;&nbsp;")
        Loop
        Return indentation.ToString
    End Function
    Private Function GetNodeDepth(ByVal node As XmlNode) As String
        Dim i As Integer = 0
        Do While Not IsNothing(node.ParentNode)
            node = node.ParentNode
            i += 1
        Loop
        Return i.ToString
    End Function
    Private Sub UpdateNodeList(ByVal depth As String, ByVal clientId As String)
        If clientIds.Item(depth) Is Nothing Then
            clientIds.Add(depth, clientId)
        Else
            clientIds.Item(depth) = clientId
        End If
    End Sub
    Private Function GetParentNodeClientId(ByVal depth As String) As String
        For i As Integer = 0 To clientIds.Count - 1
            If clientIds.Keys(i) = depth Then
                If i > 0 Then
                    Return clientIds.Item(i - 1).ToString()
                Else
                    Return ""
                End If
            End If
        Next
        Return String.Empty
    End Function

    Private Function GetPermissionsForThisRoleOnThisResource(ByVal roleId As String, ByVal resourceId As Integer) As List(Of Boolean)
        Dim permissionsList As List(Of Boolean)
        Dim rows As DataRow() = rolesResourcesAccessRightsDS.Tables("RlsResLvls").Select("RoleId='" + roleId + "' and ResourceId=" + resourceId.ToString)
        If rows.Length > 0 Then
            permissionsList = MapAccessRightsToPermissions(rows(0)("AccessLevel"))
        Else
            permissionsList = New List(Of Boolean)(5)
            For i As Integer = 0 To 4
                permissionsList.Add(False)
            Next
        End If

        'return permissions list
        Return permissionsList

    End Function

    Private Function ExtractResourceIdFromURL(ByVal url As String) As Integer
        Dim startPos As Integer = url.IndexOf("resid=") + 6
        Dim endPos As Integer = url.IndexOf("&", startPos)
        If endPos < 0 Then endPos = url.Length()
        Return Integer.Parse(url.Substring(startPos, endPos - startPos))
    End Function

    Private Sub BindCheckBoxes(ByVal e As RepeaterItemEventArgs, ByVal permissions As List(Of Boolean), ByVal resourceTypeId As Integer)
        Dim i As Integer = 1
        For Each permission As Boolean In permissions
            Dim chkbox As CheckBox = CType(e.Item.FindControl("Checkbox" + i.ToString), CheckBox)
            chkbox.Checked = permission
            If ddlTypes.SelectedValue = 5 And i < 5 Then chkbox.Enabled = False
            If ddlTypes.SelectedValue = 5 And i < 5 Then chkbox.Checked = False
            chkbox.Attributes.Add("OnClick", "SetCheckboxes();")
            i += 1
        Next
    End Sub

    Private Function MapAccessRightsToPermissions(ByVal accessRights As Integer) As List(Of Boolean)
        Dim permissionsList As New List(Of Boolean)(5)

        'this is "Full" permissions
        If accessRights = 15 Then
            permissionsList.Add(True)
        Else
            permissionsList.Add(False)
        End If

        'this is "Edit" permissions
        If accessRights - 8 >= 0 Then
            permissionsList.Add(True)
            accessRights -= 8
        Else
            permissionsList.Add(False)
        End If

        'this is "Add" permissions
        If accessRights - 4 >= 0 Then
            permissionsList.Add(True)
            accessRights -= 4
        Else
            permissionsList.Add(False)
        End If

        'this is "Delete" permissions
        If accessRights - 2 >= 0 Then
            permissionsList.Add(True)
            accessRights -= 2
        Else
            permissionsList.Add(False)
        End If

        'this is "Display" permissions
        If accessRights - 1 >= 0 Then
            permissionsList.Add(True)
        Else
            permissionsList.Add(False)
        End If

        'return permissionsList
        Return permissionsList

    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'recover ds from viewstate
        'rolesResourcesAccessRightsDS = Session("RolesResourcesAccessRightsDS")

        'update Roles Resource Permissions with user selections
        'UpdateRolesResourcesPermissionsWithUserSelections()

        'Dim result As String = ""
        'If rolesResourcesAccessRightsDS.HasChanges() Then
        '    result = (New ResourcesRelationsFacade).UpdateRolesResourcesPermissionsDS(rolesResourcesAccessRightsDS)
        'End If

        ''display errors
        'If Not result = "" Then
        '    DisplayErrorMessage(result)
        'Else
        '    btnBuildList_Click(Me, New EventArgs())
        'End If

        AddPermissionsToPages()
        Dim intSchoolOptions As Integer = CommonWebUtilities.SchoolSelectedOptions(MyAdvAppSettings.AppSettings("SchedulingMethod"))
        RadGrid1.DataSource = (New GradesFacade).GetPages(ddlGroups.SelectedValue, CInt(ddlModules.SelectedValue), CInt(ddlTypes.SelectedValue), intSchoolOptions, campusId)
        RadGrid1.Rebind()
    End Sub

    Private Sub UpdateRolesResourcesPermissionsWithUserSelections()
        For i As Integer = 0 To rptNavigationNodes.Items.Count - 1
            'get resourceId
            Dim resourceId As Integer = Integer.Parse(CType(rptNavigationNodes.Items(i).FindControl("lblResourceId"), Label).Text)

            'update only items with resources not
            If resourceId > 0 Then
                Dim accessLevel As Integer = GetAccessLevelValueFromCheckBoxes(rptNavigationNodes.Items(i))
                If RoleResourcePermissionsExists(ddlGroups.SelectedValue, resourceId) Then
                    'get the row for this roleId and Resource
                    Dim row As DataRow = GetRoleResourceRow(ddlGroups.SelectedValue, resourceId)
                    If accessLevel = 0 Then
                        'delete the row
                        row.Delete()
                    Else
                        If accessLevel <> CType(row("AccessLevel"), Integer) Then
                            'update row
                            UpdateRow(ddlGroups.SelectedValue, resourceId, accessLevel, row)
                        End If
                    End If
                Else
                    If accessLevel > 0 Then
                        'insert a new row
                        InsertRow(ddlGroups.SelectedValue, resourceId, accessLevel)
                    End If
                End If
            End If
        Next
    End Sub

    Private Function GetAccessLevelValueFromCheckBoxes(ByVal chkboxes As RepeaterItem) As Integer
        'if full is selected return 15
        If CType(chkboxes.FindControl("Checkbox1"), CheckBox).Checked Then Return 15

        Dim sum As Integer = 0
        If CType(chkboxes.FindControl("Checkbox2"), CheckBox).Checked Then sum += 8 'edit 
        If CType(chkboxes.FindControl("Checkbox3"), CheckBox).Checked Then sum += 4 'add 
        If CType(chkboxes.FindControl("Checkbox4"), CheckBox).Checked Then sum += 2 'delete
        If CType(chkboxes.FindControl("Checkbox5"), CheckBox).Checked Then sum += 1 'display

        'return sum
        Return sum

    End Function

    Private Function RoleResourcePermissionsExists(ByVal roleId As String, ByVal resourceId As Integer) As Boolean
        Dim rows As DataRow() = rolesResourcesAccessRightsDS.Tables("RlsResLvls").Select("RoleId='" + roleId + "' and ResourceId=" + resourceId.ToString)
        If rows.Length > 0 Then Return True Else Return False
    End Function

    Private Function GetRoleResourceRow(ByVal roleId As String, ByVal resourceId As Integer) As DataRow
        Dim rows As DataRow() = rolesResourcesAccessRightsDS.Tables("RlsResLvls").Select("RoleId='" + roleId + "' and ResourceId=" + resourceId.ToString)
        Return rows(0)
    End Function

    Private Sub InsertRow(ByVal roleId As String, ByVal resourceId As Integer, ByVal accessLevel As Integer)
        Dim table As DataTable = rolesResourcesAccessRightsDS.Tables("RlsResLvls")
        Dim row As DataRow = table.NewRow()
        row("RRLId") = New Guid(Guid.NewGuid.ToString)
        row("RoleId") = New Guid(roleId)
        row("ResourceId") = resourceId
        row("AccessLevel") = accessLevel
        row("ModUser") = "Insert 3/30" 'Session("UserName")
        row("ModDate") = Date.Now()
        table.Rows.Add(row)
    End Sub

    Private Sub UpdateRow(ByVal roleId As String, ByVal resourceId As Integer, ByVal accessLevel As Integer, ByVal row As DataRow)
        row("AccessLevel") = accessLevel
        row("ModUser") = "Update 3/30" 'Session("UserName")
    End Sub

    Private Sub InsertJavascriptCode(ByVal e As RepeaterItemEventArgs, ByVal resourceTypeId As Integer)
        For i As Integer = 1 To 5
            Dim chkbox As CheckBox = CType(e.Item.FindControl("Checkbox" + i.ToString), CheckBox)
            If (ddlTypes.SelectedValue = 5) And i < 5 Then chkbox.Enabled = False
            chkbox.Attributes.Add("OnClick", "SetCheckboxes();")
        Next
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub

    Protected Sub ddlGroups_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlGroups.SelectedIndexChanged
        BuildDropDownLists()
    End Sub

    Private Function ValidateLHS() As String
        Dim strError As String = ""

        'Ensure that a module is selected
        If ddlModules.SelectedValue = "" Then
            strError = "Module is a required field" & vbCr
        End If
        'Ensure that a page type is selected
        If ddlTypes.SelectedValue = "" Then
            strError &= "Type is a required field" & vbCr
        End If
        'Ensure that a role/group is selected
        If ddlGroups.SelectedValue = "" Then
            strError &= "Group is a required field" & vbCr
        End If
        Return strError
    End Function

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(ddlGroups)
        controlsToIgnore.Add(ddlModules)
        controlsToIgnore.Add(btnBuildList)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Protected Sub btnBuildList_Command(ByVal sender As Object, ByVal e As CommandEventArgs) Handles btnBuildList.Command
    End Sub
    Protected Sub RadGrid1_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrid1.ItemDataBound
        If e.Item.ItemType = GridItemType.Item Or _
            e.Item.ItemType = GridItemType.AlternatingItem Then
            Dim resource As String = CType(e.Item.Controls(0).FindControl("lblChildResourceDesc"), Label).Text
            Dim resourceTypeId = CInt(CType(e.Item.Controls(0).FindControl("txtResourceTypeId"), TextBox).Text)
            Dim tabId As Integer
            Try
                Dim texte As String = CType(e.Item.Controls(0).FindControl("txtTabId"), TextBox).Text
                tabId = If(String.IsNullOrEmpty(texte), 0, CInt(texte))
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                tabId = 0
            End Try

            'Set the sub menus
            Select Case resourceTypeId 'ResourceId
                Case Is = 1 '"26"
                    If InStr(resource, "popup") >= 1 Then
                        CType(e.Item.Controls(0).FindControl("lblParentModule"), Label).Text = ddlTypes.SelectedItem.Text
                    Else
                        CType(e.Item.Controls(0).FindControl("lblParentModule"), Label).Text = resource & " / " & ddlTypes.SelectedItem.Text
                    End If

                    CType(e.Item.Controls(0).FindControl("txtParentResource"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("lblChildResourceDesc"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("ddlPermissionsParent"), DropDownList).Visible = True
                    CType(e.Item.Controls(0).FindControl("ddlPermissions"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("ddlChild"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("lblParentModule"), Label).ForeColor = Color.White
                    e.Item.ForeColor = Color.White
                    e.Item.BackColor = Color.FromArgb(21, 101, 192)
                Case Is = 2 '"693", "132", "160", "129", "71", "190"
                    CType(e.Item.Controls(0).FindControl("txtParentResource"), Label).Text = Resource
                    CType(e.Item.Controls(0).FindControl("lblChildResourceDesc"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("lblParentModule"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("ddlPermissionsParent"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("ddlPermissions"), DropDownList).Visible = True
                    CType(e.Item.Controls(0).FindControl("ddlChild"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("txtParentResource"), Label).CssClass = "label"
                    e.Item.ForeColor = Color.White
                    e.Item.BackColor = Color.FromArgb(21, 101, 192)
                    CType(e.Item.Controls(0).FindControl("ddlPermissions"), DropDownList).Width = Unit.Pixel(160)
            End Select

            'Do not repeat sub menus
            Select Case resourceTypeId 'ParentResourceId
                Case Is = 3 And TabId = 0 '"693", "132", "160", "129", "71", "190" 
                    CType(e.Item.Controls(0).FindControl("txtParentResource"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("lblParentModule"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("ddlPermissionsParent"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("ddlPermissions"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("ddlChild"), DropDownList).Visible = True
                Case Is = 4 '"693", "132", "160", "129", "71", "190"
                    CType(e.Item.Controls(0).FindControl("txtParentResource"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("lblParentModule"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("ddlPermissionsParent"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("ddlPermissions"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("ddlChild"), DropDownList).Visible = True
                Case Is = 5 '"693", "132", "160", "129", "71", "190"
                    CType(e.Item.Controls(0).FindControl("txtParentResource"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("lblParentModule"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("ddlPermissionsParent"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("ddlPermissions"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("ddlChild"), DropDownList).Visible = True
                Case Is = 3 And (TabId = 394 Or TabId = 395 Or TabId = 396 Or TabId = 397)
                    CType(e.Item.Controls(0).FindControl("txtParentResource"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("lblParentModule"), Label).Text = ""
                    CType(e.Item.Controls(0).FindControl("ddlPermissionsParent"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("ddlPermissions"), DropDownList).Visible = False
                    CType(e.Item.Controls(0).FindControl("ddlChild"), DropDownList).Visible = True

            End Select

            Dim accessLevel As String
            Try
                Dim tb = CType(e.Item.Controls(0).FindControl("txtAccessLevel"), TextBox)
                accessLevel = If(String.IsNullOrEmpty(tb.Text), "0", tb.Text)
                ' accessLevel = CInt(CType(e.Item.Controls(0).FindControl("txtAccessLevel"), TextBox).Text)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                accessLevel = "0"
            End Try

            Try
                CType(e.Item.Controls(0).FindControl("ddlPermissionsParent"), DropDownList).SelectedValue = accessLevel
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'CType(e.Item.Controls(0).FindControl("ddlPermissions"), DropDownList).SelectedIndex = 0
            End Try

            Try
                CType(e.Item.Controls(0).FindControl("ddlPermissions"), DropDownList).SelectedValue = accessLevel
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'CType(e.Item.Controls(0).FindControl("ddlPermissions"), DropDownList).SelectedIndex = 0
            End Try

            Try
                CType(e.Item.Controls(0).FindControl("ddlChild"), DropDownList).SelectedValue = accessLevel
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'CType(e.Item.Controls(0).FindControl("ddlPermissions"), DropDownList).SelectedIndex = 0
            End Try
        End If
    End Sub
    Private Function GetListofPageLevelPermissions() As DataSet
        Dim tbl As New DataTable("SetupPageLevelPermissions")
        Dim strModUser As String
        'Dim strModDate As DateTime = Date.Now
        Dim RRLID As Guid
        Dim RoleId As Guid
        Dim AccessLevel As Integer
        Dim ResourceTypeId As Integer
        With tbl
            'Define table schema
            .Columns.Add("RRLID", GetType(Guid))
            .Columns.Add("RoleId", GetType(Guid))
            .Columns.Add("ResourceId", GetType(Integer))
            .Columns.Add("AccessLevel", GetType(Integer))
            .Columns.Add("ModUser", GetType(String))
        End With


        'GetSelectedItems method brings in a collection of rows that were selected
        For Each dataItem As GridDataItem In RadGrid1.MasterTableView.Items
            'BR: If the rules to the datatable using LoadDataRow Method
            'Benefits:LoadDataRow method helps get control RowState for the new DataRow
            'First Parameter: Array of values, the items in the array correspond to columns in the table. 
            'Second Parameter: AcceptChanges, enables to control the value of the RowState property of the new DataRow.
            '                  Passing a value of False for this parameter causes the new row to have a RowState of Added
            RRLID = Guid.NewGuid
            RoleId = New Guid(ddlGroups.SelectedValue) 'New Guid("BB0E90D6-4D77-4362-95FA-B2D34260A4A1")
            ResourceId = CType(dataItem.FindControl("txtChildResourceId"), TextBox).Text
            ResourceTypeId = CInt(CType(dataItem.FindControl("txtResourceTypeId"), TextBox).Text)
            If ResourceTypeId = 1 Then
                AccessLevel = CType(dataItem.FindControl("ddlPermissionsParent"), DropDownList).SelectedValue
            ElseIf ResourceTypeId = 2 Then
                AccessLevel = CType(dataItem.FindControl("ddlPermissions"), DropDownList).SelectedValue
            Else
                AccessLevel = CType(dataItem.FindControl("ddlChild"), DropDownList).SelectedValue
            End If
            strModUser = Session("UserName")
            'DE7748 - Module level access should not be saved 
            If Not String.IsNullOrEmpty(ResourceId) Then
                Dim foundRows() As DataRow = tbl.Select("ResourceId=" & ResourceId)
                If ResourceTypeId <> 1 And foundRows.Length = 0 Then
                    If ddlTypes.SelectedValue = "999" And ResourceTypeId = 2 Then
                        'If page is pop up and if the item is sub menu do nothing
                    Else
                        tbl.LoadDataRow(New Object() {RRLID, RoleId, ResourceId, AccessLevel, strModUser}, False)
                    End If

                End If
            End If
        Next
        Dim ds As DataSet = New DataSet()
        ds.Tables.Add(tbl)
        Return ds

    End Function
    Private Sub AddPermissionsToPages() 'This Procedure contains the update logic
        'Get the contents of datatable and pass it as xml document
        Dim dsPermissions As New DataSet
        Dim grdFacade As New SetDictationTestRulesFacade
        dsPermissions = GetListofPageLevelPermissions()
        If Not dsPermissions Is Nothing Then
            For Each lcol As DataColumn In dsPermissions.Tables(0).Columns
                lcol.ColumnMapping = MappingType.Attribute
            Next
            Dim strXML As String = dsPermissions.GetXml
            grdFacade.SetupPageLevelPermission(strXML)
        End If
    End Sub
    Protected Sub ddlPermissions_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl1 As DropDownList = DirectCast(sender, DropDownList)
        Dim item As GridDataItem = DirectCast(ddl1.NamingContainer, GridDataItem)
        Dim keyvalue As String = item.GetDataKeyValue("ChildResourceId").ToString()

        Dim ddlEditAccess As DropDownList = CType(item.FindControl("ddlPermissions"), DropDownList)
        For Each grdItem As GridDataItem In RadGrid1.Items
            Dim ResourceId As String = CType(grdItem.FindControl("txtParentResourceId"), TextBox).Text
            If ResourceId = keyvalue Then
                Dim ddlCurrent As DropDownList = CType(grdItem.FindControl("ddlPermissions"), DropDownList)
                ddlCurrent.SelectedValue = ddlEditAccess.SelectedValue
                Dim ddlChildDDL As DropDownList = CType(grdItem.FindControl("ddlChild"), DropDownList)
                ddlChildDDL.SelectedValue = ddlEditAccess.SelectedValue
            End If
        Next
    End Sub
    Protected Sub ddlPermissionsParent_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl1 As DropDownList = DirectCast(sender, DropDownList)
        Dim item As GridDataItem = DirectCast(ddl1.NamingContainer, GridDataItem)
        Dim keyvalue As String = item.GetDataKeyValue("ChildResourceId").ToString()
        Dim ddlEditAccess As DropDownList = CType(item.FindControl("ddlPermissionsParent"), DropDownList)
        For Each grdItem As GridDataItem In RadGrid1.Items
            Dim ResourceId As String = CType(grdItem.FindControl("txtParentResourceId"), TextBox).Text
            Dim ddlCurrent As DropDownList = CType(grdItem.FindControl("ddlPermissions"), DropDownList)
            Dim ddlChildDDL As DropDownList = CType(grdItem.FindControl("ddlChild"), DropDownList)
            ddlCurrent.SelectedValue = ddlEditAccess.SelectedValue
            ddlEditAccess.SelectedValue = ddlEditAccess.SelectedValue
            ddlChildDDL.SelectedValue = ddlEditAccess.SelectedValue
        Next
    End Sub
    Protected Sub btnExportToPdf_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToPdf.Click
        Dim getReportAsBytes As [Byte]()
        Dim intTabId As Integer = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If ddlTypes.SelectedValue = 0 Or ddlTypes.SelectedValue = 3 Or ddlTypes.SelectedValue = 4 Or ddlTypes.SelectedValue = 5 Then
            intTabId = 0
        Else
            intTabId = ddlTypes.SelectedValue 'Select value will be a entity value
        End If
        Dim intSchoolOptions As Integer = CommonWebUtilities.SchoolSelectedOptions(MyAdvAppSettings.AppSettings("SchedulingMethod"))
        Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder") '"/Advantage Reports/" + SingletonAppSettings.AppSettings("SSRS Deployment Environment").ToString.Trim
        'jguirado refactoring to eliminate the control from page master : 
        ' CType(Master.FindControl("UserSplitButton"), RadButton).Text, _
        'was change by get the session value
        getReportAsBytes = (New ManageSecurityReportGenerator).RenderReport("pdf", Session("User"), ddlGroups.SelectedValue.ToString, _
                                                                                  ddlGroups.SelectedItem.Text, ddlModules.SelectedValue.ToString, _
                                                                                  ddlModules.SelectedItem.Text, AdvantageSession.UserName, _
                                                                                  ddlTypes.SelectedItem.Text, _
                                                                                  strReportPath, _
                                                                                  intTabId,
                                                                                  CInt(ddlTypes.SelectedValue), _
                                                                                  AdvantageSession.UserState.CampusId.ToString, intSchoolOptions)
        ExportReport("pdf", getReportAsBytes)
    End Sub
    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToExcel.Click
        Dim getReportAsBytes As [Byte]()
        Dim intTabId As Integer = 0

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If ddlTypes.SelectedValue = 0 Or ddlTypes.SelectedValue = 3 Or ddlTypes.SelectedValue = 4 Or ddlTypes.SelectedValue = 5 Then
            intTabId = 0
        Else
            intTabId = ddlTypes.SelectedValue 'Select value will be a entity value
        End If
        Dim intSchoolOptions As Integer = CommonWebUtilities.SchoolSelectedOptions(MyAdvAppSettings.AppSettings("SchedulingMethod"))
        Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder") ' "/Advantage Reports/" + SingletonAppSettings.AppSettings("SSRS Deployment Environment").ToString.Trim
        'jguirado refactoring to eliminate the control from page master : 
        ' CType(Master.FindControl("UserSplitButton"), RadButton).Text, _
        'was change by get the session value
        getReportAsBytes = (New ManageSecurityReportGenerator).RenderReport("excel", Session("User"), ddlGroups.SelectedValue.ToString, _
                                                                                  ddlGroups.SelectedItem.Text, ddlModules.SelectedValue.ToString, _
                                                                                  ddlModules.SelectedItem.Text, AdvantageSession.UserName, _
                                                                                  ddlTypes.SelectedItem.Text, strReportPath, intTabId, _
                                                                                  CInt(ddlTypes.SelectedValue), AdvantageSession.UserState.CampusId.ToString, intSchoolOptions)
        ExportReport("excel", getReportAsBytes)
    End Sub
    Private Sub ExportReport(ByVal ExportFormat As String, ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case ExportFormat.ToLower
            Case Is = "pdf"
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case "excel"
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
                'Case "WORD"
                '    strExtension = "doc"
                '    strMimeType = "application/vnd.ms-word"
            Case "csv"
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select
        Session("SSRS_FileExtension") = strExtension
        Session("SSRS_MimeType") = strMimeType
        Session("SSRS_ReportOutput") = getReportAsBytes
        'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        Dim script As String = String.Format("window.open('DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString())
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    End Sub
#End Region

    Protected Sub ddlModules_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlModules.SelectedIndexChanged
        BuildPageTypeDropDownList()
    End Sub
    Private Sub BuildPageTypeDropDownList()
        ddlTypes.Items.Clear()
        With ddlTypes
            .Items.Add(New ListItem("Common Tasks", "3"))
            .Items.Add(New ListItem("Maintenance", "4"))
            .Items.Add(New ListItem("Reports", "5"))
            .Items.Add(New ListItem("Popup Pages", "999"))
        End With

        Select Case ddlModules.SelectedValue
            Case "26"
                ddlTypes.Items.Add(New ListItem("Manage Students", "394"))
                ddlTypes.Items.Add(New ListItem("IPEDS Reports", "689")) 'for IPEDS Page - DE7659
            Case "191", "194", "300"
                ddlTypes.Items.Add(New ListItem("Manage Students", "394"))
            Case "193" 'Add Student and Employer pages
                ddlTypes.Items.Add(New ListItem("Manage Students", "394"))
                ddlTypes.Items.Add(New ListItem("Manage Employers", "397"))
            Case "192"
                ddlTypes.Items.Add(New ListItem("Manage Employees", "396"))
            Case "189"
                ddlTypes.Items.Add(New ListItem("Manage Leads", "395"))
        End Select
    End Sub
End Class



