﻿Imports Fame.Common
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Advantage.Business.Logic.Layer
Imports System.Collections.Generic
Imports Advantage.Business.Objects

Partial Class StudentContacts
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected StudentId As String
    Protected resourceId As Integer
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected userId As String
    Protected modCode As String


    Protected state As AdvantageSessionState
    Protected LeadId As String




    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub

#Region "MRU Routines"
    Protected boolSwitchCampus As Boolean = False
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub


    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try
            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)



        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

#End Region
    Private Sub Page_Load1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        Dim userId As String
        Dim evenfac As New UserSecurityFacade
        '        Dim m_Context As HttpContext
        Dim sdfcontrols As New SDFComponent

        Page.Title = "Student Contacts"

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(213) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString
        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString()
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString



        pObj = evenfac.GetUserResourcePermissions(userId, resourceId, campusId)
        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   bind datalist
            BindDataList(StudentId)

            '   bind an empty new StudentContactInfo
            BindStudentContactData(New StudentContactInfo)

            '   disable History button the first time
            'header1.EnableHistoryButton(False)

            '   initialize buttons
            InitButtonsForLoad()

            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, studentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, studentId, resourceId.ToString)
            'End If
            MyBase.uSearchEntityControlId.Value = ""
        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
        End If

        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfcontrols.GetSDFExists(resourceId, ModuleId)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtFirstName.Text) <> "" Then
            sdfcontrols.GenerateControlsEdit(pnlSDF, resourceId, txtStudentContactId.Text, ModuleId)
        Else
            sdfcontrols.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
        End If
    End Sub
    'Private Sub OpenFERPAPopUP()


    '    '   open new window and pass parameters



    '    Dim sb As New StringBuilder
    '    '   append PrgVerId to the querystring
    '    sb.Append("&studentId=" + studentId)
    '    '   append Course Description to the querystring

    '    '   setup the properties of the new window
    '    Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
    '    Dim name As String = "FERPAPermission"
    '    Dim url As String = "../AR/" + name + ".aspx?resid=" + resourceId.ToString + "&mod=AR&cmpid=" + campusId + sb.ToString

    '    '   open new window and pass parameters
    '    CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    'End Sub



    Private Sub BindDataList(ByVal studentId As String)

        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind StudentContacts datalist
        dlstStudentContacts.DataSource = New DataView((New StudentContactsFacade).GetAllStudentContacts(studentId).Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstStudentContacts.DataBind()

    End Sub
    Private Sub BuildDropDownLists()
        'BuildRelationDDL()
        'BuildStatusDDL()
        'BuildPrefixDDL()
        'BuildSuffixDDL()

        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Dim x1 As StatusesDDLMetadata
        'Dim x2 As RelationsDDLMetadata
        'Dim x3 As PrefixesDDLMetadata
        'Dim x4 As SuffixesDDLMetadata

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing, True, True))

        'Relations DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlRelation, AdvantageDropDownListName.Relations, Nothing, True, True))

        'Prefixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlPrefixId, AdvantageDropDownListName.Prefixes, Nothing, True, True))

        'Suffixes DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlSuffixId, AdvantageDropDownListName.Suffixes, Nothing, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub
    'Private Sub BuildStatusDDL()
    '    '   bind the status DDL
    '    Dim statuses As New StatusesFacade

    '    With ddlStatusId
    '        .DataTextField = "Status"
    '        .DataValueField = "StatusId"
    '        .DataSource = statuses.GetAllStatuses()
    '        .DataBind()
    '    End With
    'End Sub
    'Private Sub BuildPrefixDDL()
    '    Dim Prefix As New PrefixesFacade
    '    With ddlPrefixId
    '        .DataTextField = "PrefixDescrip"
    '        .DataValueField = "PrefixId"
    '        .DataSource = Prefix.GetAllPrefixes()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildSuffixDDL()
    '    Dim Suffix As New SuffixesFacade
    '    With ddlSuffixId
    '        .DataTextField = "SuffixDescrip"
    '        .DataValueField = "SuffixId"
    '        .DataSource = Suffix.GetAllSuffixes()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildRelationDDL()
    '    Dim names() As String = System.Enum.GetNames(GetType(Relationship))
    '    Dim values() As Integer = System.Enum.GetValues(GetType(Relationship))

    '    For i As Integer = 0 To names.Length - 1
    '        ddlRelation.Items.Add(New ListItem(names(i), values(i)))
    '    Next

    '    ddlRelation.Items.Insert(0, New ListItem("Select", 0))
    '    ddlRelation.SelectedIndex = 0
    'End Sub
    Private Sub dlstStudentContacts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstStudentContacts.ItemCommand

        '   get the StudentContactId from the backend and display it
        GetStudentContactId(e.CommandArgument)
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = resourceId
        Master.setHiddenControlForAudit()
        '   save it in the session
        Session("StudentContactId") = e.CommandArgument

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentContacts, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEdit(pnlSDF, resourceId, e.CommandArgument, ModuleId)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        CommonWebUtilities.RestoreItemValues(dlstStudentContacts, e.CommandArgument)
    End Sub
    Private Sub BindStudentContactData(ByVal StudentContact As StudentContactInfo)
        With StudentContact
            chkIsInDB.Checked = .IsInDB
            txtStudentContactId.Text = .StudentContactId
            'ddlStatusId.SelectedValue = .StatusId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStatusId, .StatusId, .Status)
            txtStudentId.Text = .StudentId
            'ddlRelation.SelectedValue = .RelationId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRelation, .RelationId, .Relation)

            txtFirstName.Text = .FirstName
            txtMI.Text = .MI
            txtLastName.Text = .LastName
            'ddlPrefixId.SelectedValue = .PrefixId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrefixId, .PrefixId, .Prefix)
            'ddlSuffixId.SelectedValue = .SuffixId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSuffixId, .SuffixId, .Suffix)
            txtComments.Text = .Comments
            txtEmailAddress.Text = .Email
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   instantiate component
        Dim result As String
        With New StudentContactsFacade
            '   update StudentContact Info 
            result = .UpdateStudentContactInfo(BuildStudentContactInfo(txtStudentContactId.Text, StudentId), Session("UserName"))
        End With

        '   bind datalist
        BindDataList(StudentId)

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentContacts, txtStudentContactId.Text, ViewState, Header1)

        '   save it in the session
        Session("StudentContactId") = txtStudentContactId.Text

        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
            Exit Sub
        Else
            '   get the StudentContactId from the backend and display it
            GetStudentContactId(txtStudentContactId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then

            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new StudentContactInfo
            'BindStudentContactData(New StudentContactInfo)

            '   initialize buttons
            'InitButtonsForLoad()
            InitButtonsForEdit()


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try

                SDFControl.DeleteSDFValue(txtStudentContactId.Text)
                SDFID = SDFControl.GetAllLabels(pnlSDF)
                SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtStudentContactId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            CommonWebUtilities.RestoreItemValues(dlstStudentContacts, txtStudentContactId.Text)
        End If
    End Sub
    Private Function BuildStudentContactInfo(ByVal StudentContactId As String, ByVal StudentId As String) As StudentContactInfo
        'instantiate class
        Dim StudentContactInfo As New StudentContactInfo(StudentId)

        With StudentContactInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get StudentContactId
            .StudentContactId = StudentContactId

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'studentId
            '.StudentId = txtStudentId.Text

            'RelationId
            .RelationId = ddlRelation.SelectedValue

            'First Name
            .FirstName = txtFirstName.Text

            'MI
            .MI = txtMI.Text

            'LastName
            .LastName = txtLastName.Text

            'PrefixId
            .PrefixId = ddlPrefixId.SelectedValue

            'SuffixId
            .SuffixId = ddlSuffixId.SelectedValue

            'Comments
            .Comments = txtComments.Text

            'Email
            .Email = txtEmailAddress.Text

            'ModUser
            .ModUser = txtModUser.Text

            'ModDate
            .ModDate = Date.Parse(txtModDate.Text)

        End With
        'return data
        Return StudentContactInfo
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty new StudentContactInfo
        BindStudentContactData(New StudentContactInfo(StudentId))

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentContacts, Guid.Empty.ToString, ViewState, Header1)

        'initialize buttons
        InitButtonsForLoad()

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
        CommonWebUtilities.RestoreItemValues(dlstStudentContacts, Guid.Empty.ToString)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        If Not (txtStudentContactId.Text = Guid.Empty.ToString) Then

            'update StudentContact Info 
            Dim result As String = (New StudentContactsFacade).DeleteStudentContactInfo(txtStudentContactId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                Exit Sub
            Else
                '   bind datalist
                BindDataList(studentId)

                '   bind an empty new StudentContactInfo
                BindStudentContactData(New StudentContactInfo(studentId))

                '   initialize buttons
                InitButtonsForLoad()

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim SDFControl As New SDFComponent
                SDFControl.DeleteSDFValue(txtStudentContactId.Text)
                SDFControl.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End If
        End If
        CommonWebUtilities.RestoreItemValues(dlstStudentContacts, Guid.Empty.ToString)
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
        lbtSetupPhones.Enabled = False
        lbtSetUpAddresses.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        '   enable linkbutton to setup Addresses
        lbtSetUpAddresses.Enabled = True

        '   enable linkbutton to setup Phones
        lbtSetupPhones.Enabled = True

    End Sub
    Private Sub GetStudentContactId(ByVal StudentContactId As String)
        '   bind StudentContact properties
        BindStudentContactData((New StudentContactsFacade).GetStudentContactInfo(StudentContactId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        '   bind datalist
        BindDataList(studentId)

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, resourceId, moduleid)

    End Sub
    Private Sub lbtSetUpPhones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtSetupPhones.Click

        '   build querystring to be added to the URL
        Dim sb As New System.Text.StringBuilder("&StudentContactId=" + txtStudentContactId.Text)
        sb.Append("&StudentContact=" + Server.UrlEncode(txtLastName.Text + (", ") + txtFirstName.Text))

        '   setup the properties of the new window
        Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "StudentContactPhones"
        Dim url As String = "../SY/" + name + ".aspx?resid=288&mod=SY&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

    End Sub
    Private Sub lbtSetUpAddresses_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtSetUpAddresses.Click

        '   build querystring to be added to the URL
        Dim sb As New System.Text.StringBuilder("&StudentContactId=" + txtStudentContactId.Text)
        sb.Append("&StudentContact=" + Server.UrlEncode(txtLastName.Text + (", ") + txtFirstName.Text))

        '   setup the properties of the new window
        Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name As String = "StudentContactAddresses"
        Dim url As String = "../SY/" + name + ".aspx?resid=287&mod=SY&cmpid=" + campusId + sb.ToString

        '   open new window and pass parameters
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub


End Class
