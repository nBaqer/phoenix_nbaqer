<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="TimeIntervals.aspx.vb" Inherits="TimeIntervals" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstTimeIntervals">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstTimeIntervals" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstTimeIntervals" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstTimeIntervals" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnGenerateTimeRecords">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstTimeIntervals" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%"
            Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized"
            Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                    </td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server"
                                            RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstTimeIntervals" runat="server" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TimeIntervalDescrip", "{0:hh:mm tt}") %>' CommandArgument='<%# Container.DataItem("TimeIntervalId") %>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both"
                orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button>
                            </td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <asp:Panel ID="pnlIntervalData" runat="server" CssClass="label">
                                        <table width="60%" align="center">
                                            <asp:TextBox ID="txtTimeIntervalId" runat="server" Visible="False"></asp:TextBox>
                                            <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                            <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                            <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                            <tr>
                                                <td colspan="2">
                                                    <p style="border-right: #eee 1px solid; padding-right: 6px; border-top: #eee 1px solid; padding-left: 6px; padding-bottom: 6px; margin: 0px 3em 10px; font: bold 12px verdana; border-left: #eee 1px solid; color: #d17a08; padding-top: 6px; border-bottom: #eee 1px solid; text-align: center">
                                                        Use this section to setup individual records
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblStatusId" runat="server" CssClass="label">Status<span style="COLOR: red">*</span></asp:Label>
                                                </td>
                                                <td class="contentcell4">
                                                    <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblTimeIntervalDescrip" runat="server" CssClass="label">Time Interval Start<span style="COLOR: red">*</span></asp:Label>
                                                </td>
                                                <td class="contentcell4">
                                                    <asp:TextBox ID="txtTimeIntervalDescrip" runat="server" CssClass="textbox" MaxLength="50"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlCreateTimeInterval" runat="server" CssClass="label">
                                        <table id="Table1" width="60%" align="center">
                                            <asp:TextBox ID="Textbox4" runat="server" Visible="False"></asp:TextBox>
                                            <asp:CheckBox ID="Checkbox1" runat="server" Visible="False"></asp:CheckBox>
                                            <asp:TextBox ID="Textbox3" runat="server" Visible="False">ModUser</asp:TextBox>
                                            <asp:TextBox ID="Textbox2" runat="server" Visible="False">ModDate</asp:TextBox>
                                            <tr>
                                                <td style="height: 10px">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" colspan="2">
                                                    <asp:Label ID="lbltimereocrds" runat="server" Font-Bold="true" CssClass="label" Font-Size="12px">Generate Time Records</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <p style="border-right: #eee 1px solid; padding-right: 6px; border-top: #eee 1px solid; padding-left: 6px; padding-bottom: 6px; margin: 10px 3em; font: bold 12px verdana; border-left: #eee 1px solid; color: #d17a08; padding-top: 6px; border-bottom: #eee 1px solid; text-align: center">
                                                        Use this section to setup multiple records
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblStartTime" runat="server" CssClass="label">Start Time<span style="COLOR: red">*</span></asp:Label>
                                                </td>
                                                <td class="contentcell4">
                                                    <asp:TextBox ID="txtStartTime" runat="server" CssClass="textbox" MaxLength="50">07:00 AM</asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblEndTime" runat="server" CssClass="label">End Time<span style="COLOR: red">*</span></asp:Label>
                                                </td>
                                                <td class="contentcell4">
                                                    <asp:TextBox ID="txtEndTime" runat="server" CssClass="textbox" MaxLength="50">10:00 PM</asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">
                                                    <asp:Label ID="lblTimeInterval" runat="server" CssClass="label">Time Interval (minutes)<span style="COLOR: red">*</span></asp:Label>
                                                </td>
                                                <td class="contentcell4">
                                                    <asp:TextBox ID="txtTimeInterval" runat="server" CssClass="textbox" MaxLength="50">60</asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">&nbsp;
                                                </td>
                                                <td class="contentcell4" style="text-align: left">
                                                    <asp:CheckBox ID="cbxDeleteAllExistingRecords" runat="server" CssClass="checkbox"
                                                        Text="Delete All Existing Records"></asp:CheckBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell">&nbsp;
                                                </td>
                                                <td class="contentcell4" align="center">
                                                    <asp:Button ID="btnGenerateTimeRecords" runat="server" Text="Generate Time Records"
                                                       ></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:TextBox ID="txtRowIds" runat="server" CssClass="textbox" Visible="false"></asp:TextBox><asp:TextBox
                                        ID="txtResourceId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox>
                                </div>

                                <!--end table content-->
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
                runat="server">
            </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>
</asp:Content>
