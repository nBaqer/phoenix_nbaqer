' ===============================================================================
'
' FAME AdvantageV1
'
' HRDepartments.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class HRDepartments
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As System.Web.UI.WebControls.Label
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

      If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            If Not IsPostBack Then
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                'objCommon.PopulatePage(Form1)
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                '   build dropdownlists
                BuildDropDownLists()

                '   bind datalist
                BindDataList()

                '   bind an empty new HRDepartmentInfo
                BindHRDepartmentData(New HRDepartmentInfo)

                '   initialize buttons
                InitButtonsForLoad()

            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                InitButtonsForEdit()
            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub BindDataList()

        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = Nothing
            Case 1
                rowFilter = "Status=0"
                sortExpression = Nothing
            Case Else
                rowFilter = Nothing
                sortExpression = "Status desc"
        End Select

        '   bind HRDepartments datalist
        dlstHRDepartments.DataSource = New DataView((New EmployeesFacade).GetAllHRDepartments().Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstHRDepartments.DataBind()
    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampusGroupsDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the status DDL
        Dim statuses As New StatusesFacade

        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With
    End Sub
    Private Sub BuildCampusGroupsDDL()
        '   bind the CampusGroups DDL
        Dim campusGroups As New CampusGroupsFacade

        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub dlstHRDepartments_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstHRDepartments.ItemCommand

        '   get the HRDepartmentId from the backend and display it
        GetHRDepartmentId(e.CommandArgument)

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstHRDepartments, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()

        CommonWebUtilities.RestoreItemValues(dlstHRDepartments, e.CommandArgument.ToString)
    End Sub
    Private Sub BindHRDepartmentData(ByVal HRDepartment As HRDepartmentInfo)
        With HRDepartment
            chkIsInDB.Checked = .IsInDB
            txtHRDepartmentId.Text = .HRDepartmentId
            txtHRDepartmentCode.Text = .Code
            If Not (HRDepartment.StatusId = Guid.Empty.ToString) Then ddlStatusId.SelectedValue = HRDepartment.StatusId
            txtHRDepartmentDescrip.Text = .Description
            If Not (HRDepartment.CampGrpId = Guid.Empty.ToString) Then ddlCampGrpId.SelectedValue = HRDepartment.CampGrpId Else ddlCampGrpId.SelectedIndex = 0
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '   instantiate component
        Dim result As String
        With New EmployeesFacade
            '   update HRDepartment Info 
            result = .UpdateHRDepartmentInfo(BuildHRDepartmentInfo(txtHRDepartmentId.Text), Session("UserName"))
        End With

        '   bind datalist
        BindDataList()

        '   set Style to Selected Item
        CommonWebUtilities.SetStyleToSelectedItem(dlstHRDepartments, txtHRDepartmentId.Text, ViewState)

        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            '   get the HRDepartmentId from the backend and display it
            GetHRDepartmentId(txtHRDepartmentId.Text)
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then
            '   set the property IsInDB to true in order to avoid an error if the user
            '   hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            'note: in order to display a new page after "save".. uncomment next lines
            '   bind an empty new HRDepartmentInfo
            'BindHRDepartmentData(New HRDepartmentInfo)

            '   initialize buttons
            'InitButtonsForLoad()
            InitButtonsForEdit()
        End If
        CommonWebUtilities.RestoreItemValues(dlstHRDepartments, txtHRDepartmentId.Text)
    End Sub
    Private Function BuildHRDepartmentInfo(ByVal HRDepartmentId As String) As HRDepartmentInfo
        'instantiate class
        Dim HRDepartmentInfo As New HRDepartmentInfo

        With HRDepartmentInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get HRDepartmentId
            .HRDepartmentId = HRDepartmentId

            'get Code
            .Code = txtHRDepartmentCode.Text.Trim

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'get HRDepartment's name
            .Description = txtHRDepartmentDescrip.Text.Trim

            'get Campus Group
            .CampGrpId = ddlCampGrpId.SelectedValue

            'get ModUser
            .ModUser = txtModUser.Text

            'get ModDate
            .ModDate = Date.Parse(txtModDate.Text)
        End With

        'return data
        Return HRDepartmentInfo
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty new HRDepartmentInfo
        BindHRDepartmentData(New HRDepartmentInfo)

        'Reset Style in the Datalist
        CommonWebUtilities.SetStyleToSelectedItem(dlstHRDepartments, Guid.Empty.ToString, ViewState)

        'initialize buttons
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlstHRDepartments, Guid.Empty.ToString)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        If Not (txtHRDepartmentId.Text = Guid.Empty.ToString) Then

            'update HRDepartment Info 
            Dim result As String = (New EmployeesFacade).DeleteHRDepartmentInfo(txtHRDepartmentId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   bind datalist
                BindDataList()

                '   bind an empty new HRDepartmentInfo
                BindHRDepartmentData(New HRDepartmentInfo)

                '   initialize buttons
                InitButtonsForLoad()
            End If
            CommonWebUtilities.RestoreItemValues(dlstHRDepartments, Guid.Empty.ToString)
        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Sub GetHRDepartmentId(ByVal HRDepartmentId As String)

        '   bind HRDepartment properties
        BindHRDepartmentData((New EmployeesFacade).GetHRDepartmentInfo(HRDepartmentId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged

        BindHRDepartmentData(New HRDepartmentInfo)
        'initialize buttons
        InitButtonsForLoad()
        '   bind datalist
        BindDataList()
        'Header1.EnableHistoryButton(False)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Overloads Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Overloads Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Overloads Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub
End Class
