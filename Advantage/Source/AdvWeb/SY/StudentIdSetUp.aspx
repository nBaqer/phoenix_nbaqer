<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="StudentIdSetUp.aspx.vb" Inherits="StudentIdSetUp" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%"
            Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized"
            Style="overflow: auto;">
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both"
                orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"
                                Enabled="false"></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"
                                Enabled="false"></asp:Button>
                        </td>
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="scrollright2">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                        <tr>
                                            <td class="contentcellheader" nowrap colspan="6">
                                                <asp:Label ID="label2" runat="server" CssClass="tothemebold" Font-Bold="true">Please select a method for constructing Student ID</asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="100%" align="center">
                                        <tr>
                                            <td class="spacertables">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButtonList ID="radFormat" runat="Server" CssClass="label" RepeatDirection="Vertical"
                                                    AutoPostBack="True">
                                                    <asp:ListItem Text="Generate Manually - No Validation Applied" Value="1" />
                                                    <asp:ListItem Text="Generate Based On Sequential Numbers" Value="2" />
                                                    <asp:ListItem Text="Generate Based On a Specific Format" Value="3" />
                                                    <asp:ListItem Text="Generate Based On Enrollment Badge ID" Value="4" />
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnlEnrollFormat" runat="server">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="6">
                                                    <asp:Label ID="lblEnrollmentId" runat="server" CssClass="tothemebold" Font-Bold="true">Please select number of characters below</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables">
                                                </td>
                                            </tr>
                                        </table>
                                        <br>
                                        <table width="100%" align="center">
                                            <tr>
                                                <td nowrap>
                                                    <asp:DropDownList ID="txtYear" runat="server" CssClass="textbox" Width="40px">
                                                        <asp:ListItem Value="0">0</asp:ListItem>
                                                        <asp:ListItem Value="1">1</asp:ListItem>
                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                        <asp:ListItem Value="3">3</asp:ListItem>
                                                        <asp:ListItem Value="4">4</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lblYear" runat="server" CssClass="label">Numbers From Current Year</asp:Label>
                                                </td>
                                                <td nowrap>
                                                    <asp:TextBox ID="txtLName" runat="server" CssClass="textbox" Width="20px"></asp:TextBox>
                                                    <asp:Label ID="lblLName" runat="server" CssClass="label">Characters From Last Name</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap>
                                                    <asp:DropDownList ID="txtMonth" runat="server" CssClass="textbox" Width="40px">
                                                        <asp:ListItem Value="0">0</asp:ListItem>
                                                        <asp:ListItem Value="1">1</asp:ListItem>
                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lblMonth" runat="server" CssClass="label">Numbers From Current Month</asp:Label>
                                                </td>
                                                <td nowrap>
                                                    <asp:TextBox ID="txtFName" runat="server" CssClass="textbox" Width="20px"></asp:TextBox>
                                                    <asp:Label ID="lblFName" runat="server" CssClass="label">Characters From First Name</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap>
                                                    <asp:DropDownList ID="txtDate" runat="server" CssClass="textbox" Width="40px">
                                                        <asp:ListItem Value="0">0</asp:ListItem>
                                                        <asp:ListItem Value="1">1</asp:ListItem>
                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:Label ID="lblDate" runat="server" CssClass="label">Numbers From Current Date</asp:Label>
                                                </td>
                                                <td nowrap>
                                                    <asp:TextBox ID="txtSeq" runat="server" CssClass="textbox" Width="20px" Visible="False"></asp:TextBox>
                                                    <asp:Label ID="lblSeq" runat="server" CssClass="label" Visible="False">Characters From Sequential Number</asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <p>
                                        </p>
                                        <table width="60%" align="center" border="0">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblExample" runat="server" CssClass="labelbold">Example:</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td nowrap valign="top">
                                                    <asp:Label ID="lblStudFirstName" runat="server" CssClass="labelbold" Width="100px">Student Name : </asp:Label>&nbsp;
                                                    <asp:Label ID="label7" runat="server" CssClass="labelbold" Width="150px">John Smith</asp:Label>
                                            </tr>
                                        </table>
                                        <p>
                                        </p>
                                        <table width="70%" align="center">
                                            <tr>
                                                <td valign="top" width="20px">
                                                </td>
                                                <td nowrap>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:DropDownList ID="Textbox1" runat="server" CssClass="textbox" Enabled="False"
                                                        Width="40px">
                                                        <asp:ListItem Value="2">4</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:Label ID="label1" runat="server" CssClass="label" Width="150px">numbers from current year ,</asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:TextBox ID="Textbox2" runat="server" CssClass="textbox" Enabled="False" Width="20px">5</asp:TextBox>
                                                    <asp:Label ID="lblcharacter" runat="server" CssClass="label" Width="50px">characters from last name</asp:Label><br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:DropDownList ID="Dropdownlist1" runat="server" CssClass="textbox" Enabled="False"
                                                        Width="40px">
                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:Label ID="label3" runat="server" CssClass="label" Width="170px">numbers from current month ,</asp:Label>&nbsp;&nbsp;
                                                    <asp:TextBox ID="Textbox3" runat="server" CssClass="textbox" Enabled="False" Width="20px">3</asp:TextBox>
                                                    <asp:Label ID="label5" runat="server" CssClass="label" Width="50px">characters from first name</asp:Label><br>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <asp:DropDownList ID="Dropdownlist2" runat="server" CssClass="textbox" Enabled="False"
                                                        Width="40px">
                                                        <asp:ListItem Value="2">2</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:Label ID="label4" runat="server" CssClass="label" Width="50px">numbers from current day</asp:Label>&nbsp;
                                                    <br>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="45%" align="center">
                                            <tr height="10px">
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="20px">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </td>
                                                <td nowrap>
                                                    <asp:Label ID="lblValue" runat="server" CssClass="labelbold"><u>Student ID </u>:  20050516SMITHJOH<font color="red">95</font></asp:Label>&nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" align="center">
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                                <td>
                                                    <asp:Label ID="label6" runat="server" CssClass="label">The last 2 numbers shown in red are automatically generated by Advantage (and gets incremented) for each student</asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlSeqNumber" runat="server">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="6">
                                                    <asp:Label ID="lblSeqNumber" runat="server" CssClass="tothemebold" Font-Bold="true">Specify The Starting Number</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables">
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="100%" align="center" border="0">
                                            <tr>
                                                <td nowrap>
                                                    <asp:Label ID="lblSeqNumber1" runat="server" CssClass="label" Width="150px">Sequential Number Starts From</asp:Label>
                                                    <asp:TextBox ID="txtSeqNumber" runat="server" CssClass="textbox" Width="230px"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <table width="100%" align="center">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCampus" runat="server" Visible="False" CssClass="label">Campus</asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCampusId" runat="server" CssClass="dropdownlist" Width="230px"
                                                    Visible="false">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <!--end table content-->
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
                runat="server">
            </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
        <!--end validation panel-->
    </div>
</asp:Content>
