﻿<%@ Page Title="Manage Security" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" Inherits="BasePage" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="FAME.Advantage.Common" %>
<%@ Import Namespace="FAME.AdvantageV1.BusinessFacade" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="FAME.AdvantageV1.Common" %>
<%@ Import Namespace="Advantage.Business.Objects" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>

    <script type="text/javascript">
        function SetCheckboxes() {
            var xmlDoc = new ActiveXObject("Msxml2.DOMDocument.3.0")
            xmlDoc.async = "false";
            var chkbx = window.document.getElementById(window.event.srcElement.id);
            var idx = chkbx.id.substring(chkbx.id.length - 1);
            var status = chkbx.checked;
            var txt = window.document.getElementById("clientXml");
            xmlDoc.loadXML(txt.innerHTML);
            xmlDoc.setProperty("SelectionLanguage", "XPath");
            currNode = xmlDoc.selectSingleNode("//" + chkbx.id.substring(0, chkbx.id.length - 1) + '1');
            if (idx == 1) {
                NavigateXmlDoc(currNode, status, 1);
                NavigateXmlDoc(currNode, status, 2);
                NavigateXmlDoc(currNode, status, 3);
                NavigateXmlDoc(currNode, status, 4);
                NavigateXmlDoc(currNode, status, 5);
            }
            else {
                NavigateXmlDoc(currNode, status, idx);
                CheckFull(chkbx);
            }
            return true;
        }
        function NavigateXmlDoc(node, status, idx) {
            var chkName = node.nodeName.substring(0, node.nodeName.length - 1) + idx;
            var chkbx = window.document.getElementById(chkName);
            chkbx.checked = status;

            if (node.hasChildNodes()) {
                var children = node.childNodes
                for (var i = 0; i < children.length; i++) {
                    NavigateXmlDoc(children.item(i), status, idx)
                }
            }
        }
        function CheckFull(chkbx) {
            var idx = chkbx.id.substring(chkbx.id.length - 1);
            if (idx != 1) {
                var chkName = chkbx.id.substring(0, chkbx.id.length - 1);
                var allChk = true;
                for (var j = 2; j < 6; j++) {
                    var chkbx = window.document.getElementById(chkName + j.toString());
                    allChk = allChk && chkbx.checked;
                }
                var chkFull = window.document.getElementById(chkName + '1');
                chkFull.checked = allChk;
            }
        }
    </script>

    <script runat="server">
    
        Private rolesResourcesAccessRightsDS As DataSet
        Private clientIds As New NameValueCollection()
        Private clientXmlDoc As New XmlDocument()
        Protected campusId As String
    
        Private pObj As New UserPagePermissionInfo

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
            Dim resourceId As Integer
           
            '   disable History button at all time
            'Header1.EnableHistoryButton(False)
            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            campusId = Master.CurrentCampusId
           
            Dim advantageUserState As New User
            advantageUserState = AdvantageSession.UserState

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
            'pObj = Header1.UserPagePermission

 
            If Not IsPostBack Then
                BuilDDLGroups()
                BuildDropDownLists()
            End If
        End Sub
    
        Private Sub BuildDropDownLists()
            BuildDDLModules()
            BuildDDLTypes()
        End Sub
        Private Sub BuilDDLGroups()
 
            'build Groups DDL
            With ddlGroups
                .DataSource = (New UserSecurityFacade).GetAllRoles
                .DataTextField = "Role"
                .DataValueField = "RoleId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select One", ""))
            End With
        
        End Sub
    
        Private Sub BuildDDLModules()
            'build Modules DDL
            If Not ddlGroups.SelectedValue = "" Then
                ddlModules.Enabled = True
                With ddlModules
                    .DataSource = (New ResourcesRelationsFacade).GetModulesByRoleId(ddlGroups.SelectedValue)
                    .DataTextField = "Resource"
                    .DataValueField = "ResourceId"
                    .DataBind()
                    .Items.Insert(0, New ListItem("Select One", ""))
                End With
            Else
                ddlModules.Enabled = False
            End If
        End Sub
    
        Private Sub BuildDDLTypes()
            'build Types DDL
            With ddlTypes
                .DataSource = (New ResourcesRelationsFacade).GetResourceTypes
                .DataTextField = "ResourceType"
                .DataValueField = "ResourceTypeId"
                .DataBind()
                .Items.Insert(0, New ListItem("Select One", ""))
            End With
        End Sub

        Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As EventArgs)
            Dim result As String = ValidateLHS()
            If Not result = "" Then
                DisplayErrorMessage(result)
                Exit Sub
            End If

            'Enable the Save button if the user has permission
            If pObj.HasFull Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If

            'get Roles-Resources-Permissions dataset
            rolesResourcesAccessRightsDS = (New ResourcesRelationsFacade).GetRolesResourcesPermissionsDS()

            'save ds in viewstate
            Session("RolesResourcesAccessRightsDS") = rolesResourcesAccessRightsDS
        
            'bind repeater   
            'MERGEFIX
            'Dim navXmlDoc As System.Xml.XmlDocument = (New NavigationFacade).GetNavigationTree("", CommonWebUtilities.SchoolSelectedOptions(SingletonAppSettings.AppSettings("SchedulingMethod")))
           
            Dim MyAdvAppSettings As AdvAppSettings
           
           
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

  
           
            Dim navXmlDoc As XmlDocument = (New NavigationFacade).GetNavigationTree("", CommonWebUtilities.SchoolSelectedOptions(MyAdvAppSettings.AppSettings("SchedulingMethod")), vbNullString)
            rptNavigationNodes.DataSource = (CType(BuildItemsNode(navXmlDoc, ddlModules.SelectedItem.Text, ddlTypes.SelectedItem.Value), XmlNode).SelectNodes("//Node"))
            rptNavigationNodes.DataBind()
        
            'save clientXml in the client
            'clientXml.InnerHtml = clientXmlDoc.InnerXml
        
        End Sub
        Private Function BuildItemsNode(ByVal navXmlDoc As XmlDocument, ByVal selectedModule As String, ByVal resourceTypeId As String) As XmlNode

            'these are all the nodes to be added to the selected Module
            Dim sb As New StringBuilder()
            'this is entity tabs parent node
            sb.Append("/Nodes/Node[Text=" + "'" + selectedModule + "']/Nodes/Node[ResourceTypeId=8 and ChildTypeId=" + resourceTypeId + "] ")
            sb.Append(" | ")
            ''these are entity tabs nodes
            'sb.Append("/Nodes/Node[Text=" + "'" + selectedModule + "']/Nodes/Node[ResourceTypeId=8]/Nodes/Node[ResourceTypeId=" + resourceTypeId + "] ")
            'sb.Append(" | ")
            'these are other nodes
            sb.Append("/Nodes/Node[Text=" + "'" + selectedModule + "']/Nodes/Node[ResourceTypeId=" + resourceTypeId + " or (ResourceTypeId=2 and ChildTypeId=" + resourceTypeId + ")] ")
        
            'create the nodelist
            Dim nodeList As XmlNodeList = navXmlDoc.SelectNodes(sb.ToString)

            'add all nodes to the root
            Dim xmlDoc As New XmlDocument()
            Dim root As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "Nodes", Nothing)
        
            'create the module nodes
            Dim moduleNodes As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "Nodes", Nothing)
            root.AppendChild(moduleNodes)

            'create the module node
            Dim moduleNode As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "Node", Nothing)
            moduleNodes.AppendChild(moduleNode)
 
            'create the Text node
            Dim moduleNodeText As XmlNode = xmlDoc.CreateNode(XmlNodeType.Element, "Text", Nothing)
            moduleNodeText.InnerText = selectedModule
            moduleNode.AppendChild(moduleNodeText)
           
            Dim MyAdvAppSettings As AdvAppSettings
           
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

   
            For Each node As XmlNode In nodeList
                Dim iNode As XmlNode = xmlDoc.ImportNode(node, True)
                ''Modified by Saraswathi lakshmanan On June 15 2010
                ''To fix the issue 19081- Hide pages in Manage Security also
                ''19147: QA: Need not see the Manage Configuration Settings page on the Manage Security page. 
                If iNode.FirstChild.InnerText.Contains("Manage Configuration Settings") Then
                ElseIf (MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" And iNode.FirstChild.InnerText.Contains("Post Grade by Student (Instructor Level)")) Then
                ElseIf (MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" And iNode.FirstChild.InnerText.Contains("Setup Grade Book Weightings")) Then
                ElseIf (MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "courselevel" And iNode.FirstChild.InnerText.Contains("Apply Grade Book Weights to Class Sections")) Then
                ElseIf (MyAdvAppSettings.AppSettings("GradeBookWeightingLevel").ToString.ToLower = "instructorlevel" And iNode.FirstChild.InnerText.Contains("Post Grade by Student (Course Level)")) Then
                Else
                    moduleNode.AppendChild(iNode)
                End If
            Next
        
            'return the node with all navigation items
            Return root

        End Function

        Protected Sub rptNavigationNodes_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
            Select Case e.Item.ItemType
            
                Case ListItemType.Header
                
                Case ListItemType.Item, ListItemType.AlternatingItem

                    Dim resourceTypeId As Integer = 0
                    If Not IsNothing(e.Item.DataItem("ResourceTypeId")) Then
                        resourceTypeId = Integer.Parse(CType(e.Item.DataItem("ResourceTypeId"), XmlElement).InnerText)
                    End If
                
                    'setup text of navigation node with indentation
                    Dim lblIndentation As Label = CType(e.Item.FindControl("lblIndentation"), Label)
                    lblIndentation.Text = GetIndentation(e.Item.DataItem)
 
                    Dim lbl As Label = CType(e.Item.FindControl("lblNavigationNode"), Label)
                    lbl.Text = CType(e.Item.DataItem("Text"), XmlElement).InnerText

                    'set ResourceId
                    Dim lblResourceId As Label = CType(e.Item.FindControl("lblResourceId"), Label)
                    If resourceTypeId = 3 Or resourceTypeId = 4 Or resourceTypeId = 5 Then
                        lblResourceId.Text = ExtractResourceIdFromURL(CType(e.Item.DataItem("Url"), XmlElement).InnerText)
                    Else
                        lblResourceId.Text = "0"
                    End If
                
                    'bind checkboxes only for resources that need permissions            
                    If resourceTypeId = 3 Or resourceTypeId = 4 Or resourceTypeId = 5 Then
                        BindCheckBoxes(e, GetPermissionsForThisRoleOnThisResource(ddlGroups.SelectedValue, ExtractResourceIdFromURL(CType(e.Item.DataItem("Url"), XmlElement).InnerText)), resourceTypeId)
                    Else
                        InsertJavascriptCode(e, resourceTypeId)
                    End If
                
                    'get client and parent ids
                    Dim depth As String = GetNodeDepth(e.Item.DataItem)
                    Dim clientId As String = CType(e.Item.FindControl("Checkbox1"), CheckBox).ClientID
                    UpdateNodeList(depth, clientId)
                
                    'add node to client xml document
                    Dim clientNode As XmlNode = clientXmlDoc.CreateNode(XmlNodeType.Element, Nothing, clientId, Nothing)
                    If clientXmlDoc.HasChildNodes Then
                        clientXmlDoc.SelectSingleNode("//" + GetParentNodeClientId(depth)).AppendChild(clientNode)
                    Else
                        clientXmlDoc.AppendChild(clientNode)
                    End If
                
            End Select
        End Sub
 
        Private Function GetIndentation(ByVal node As XmlNode) As String
            Dim indentation As New StringBuilder()
            Do While Not IsNothing(node.ParentNode)
                node = node.ParentNode
                indentation.Append("&nbsp;&nbsp;&nbsp;&nbsp;")
            Loop
            Return indentation.ToString
        End Function
        Private Function GetNodeDepth(ByVal node As XmlNode) As String
            Dim i As Integer = 0
            Do While Not IsNothing(node.ParentNode)
                node = node.ParentNode
                i += 1
            Loop
            Return i.ToString
        End Function
        Private Sub UpdateNodeList(ByVal depth As String, ByVal clientId As String)
            If clientIds.Item(depth) Is Nothing Then
                clientIds.Add(depth, clientId)
            Else
                clientIds.Item(depth) = clientId
            End If
        End Sub
        Private Function GetParentNodeClientId(ByVal depth As String) As String
            For i As Integer = 0 To clientIds.Count - 1
                If clientIds.Keys(i) = depth Then
                    If i > 0 Then
                        Return clientIds.Item(i - 1).ToString()
                    Else
                        Return ""
                    End If
                End If
            Next
            
            Return String.Empty
        End Function
       
        Private Function GetPermissionsForThisRoleOnThisResource(ByVal roleId As String, ByVal resourceId As Integer) As List(Of Boolean)
            Dim permissionsList As List(Of Boolean)
            Dim rows As DataRow() = rolesResourcesAccessRightsDS.Tables("RlsResLvls").Select("RoleId='" + roleId + "' and ResourceId=" + resourceId.ToString)
            If rows.Length > 0 Then
                permissionsList = MapAccessRightsToPermissions(rows(0)("AccessLevel"))
            Else
                permissionsList = New List(Of Boolean)(5)
                For i As Integer = 0 To 4
                    permissionsList.Add(False)
                Next
            End If
        
            'return permissions list
            Return permissionsList
        
        End Function
       
        Private Function ExtractResourceIdFromURL(ByVal url As String) As Integer
            Dim startPos As Integer = url.IndexOf("resid=") + 6
            Dim endPos As Integer = url.IndexOf("&", startPos)
            If endPos < 0 Then endPos = url.Length()
            Return Integer.Parse(url.Substring(startPos, endPos - startPos))
        End Function
       
        Private Sub BindCheckBoxes(ByVal e As RepeaterItemEventArgs, ByVal permissions As List(Of Boolean), ByVal resourceTypeId As Integer)
            Dim i As Integer = 1
            For Each permission As Boolean In permissions
                Dim chkbox As CheckBox = CType(e.Item.FindControl("Checkbox" + i.ToString), CheckBox)
                chkbox.Checked = permission
                If ddlTypes.SelectedValue = 5 And i < 5 Then chkbox.Enabled = False
                If ddlTypes.SelectedValue = 5 And i < 5 Then chkbox.Checked = False
                chkbox.Attributes.Add("OnClick", "SetCheckboxes();")
                i += 1
            Next
        End Sub
       
        Private Function MapAccessRightsToPermissions(ByVal accessRights As Integer) As List(Of Boolean)
            Dim permissionsList As New List(Of Boolean)(5)
        
            'this is "Full" permissions
            If accessRights = 15 Then
                permissionsList.Add(True)
            Else
                permissionsList.Add(False)
            End If
        
            'this is "Edit" permissions
            If accessRights - 8 >= 0 Then
                permissionsList.Add(True)
                accessRights -= 8
            Else
                permissionsList.Add(False)
            End If

            'this is "Add" permissions
            If accessRights - 4 >= 0 Then
                permissionsList.Add(True)
                accessRights -= 4
            Else
                permissionsList.Add(False)
            End If
 
            'this is "Delete" permissions
            If accessRights - 2 >= 0 Then
                permissionsList.Add(True)
                accessRights -= 2
            Else
                permissionsList.Add(False)
            End If

            'this is "Display" permissions
            If accessRights - 1 >= 0 Then
                permissionsList.Add(True)
            Else
                permissionsList.Add(False)
            End If
        
            'return permissionsList
            Return permissionsList
        
        End Function

        Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs)
            'recover ds from viewstate
            rolesResourcesAccessRightsDS = Session("RolesResourcesAccessRightsDS")
 
            'update Roles Resource Permissions with user selections
            UpdateRolesResourcesPermissionsWithUserSelections()

            Dim result As String = ""
            If rolesResourcesAccessRightsDS.HasChanges() Then
                result = (New ResourcesRelationsFacade).UpdateRolesResourcesPermissionsDS(rolesResourcesAccessRightsDS)
            End If
  
            'display errors
            If Not result = "" Then
                DisplayErrorMessage(result)
            Else
                btnBuildList_Click(Me, New EventArgs())
            End If
        End Sub
       
        Private Sub UpdateRolesResourcesPermissionsWithUserSelections()
            For i As Integer = 0 To rptNavigationNodes.Items.Count - 1
                'get resourceId
                Dim resourceId As Integer = Integer.Parse(CType(rptNavigationNodes.Items(i).FindControl("lblResourceId"), Label).Text)
            
                'update only items with resources not
                If resourceId > 0 Then
                    Dim accessLevel As Integer = GetAccessLevelValueFromCheckBoxes(rptNavigationNodes.Items(i))
                    If RoleResourcePermissionsExists(ddlGroups.SelectedValue, resourceId) Then
                        'get the row for this roleId and Resource
                        Dim row As DataRow = GetRoleResourceRow(ddlGroups.SelectedValue, resourceId)
                        If accessLevel = 0 Then
                            'delete the row
                            row.Delete()
                        Else
                            If accessLevel <> CType(row("AccessLevel"), Integer) Then
                                'update row
                                UpdateRow(ddlGroups.SelectedValue, resourceId, accessLevel, row)
                            End If
                        End If
                    Else
                        If accessLevel > 0 Then
                            'insert a new row
                            InsertRow(ddlGroups.SelectedValue, resourceId, accessLevel)
                        End If
                    End If
                End If
            Next
        End Sub
       
        Private Function GetAccessLevelValueFromCheckBoxes(ByVal chkboxes As RepeaterItem) As Integer
            'if full is selected return 15
            If CType(chkboxes.FindControl("Checkbox1"), CheckBox).Checked Then Return 15
        
            Dim sum As Integer = 0
            If CType(chkboxes.FindControl("Checkbox2"), CheckBox).Checked Then sum += 8 'edit 
            If CType(chkboxes.FindControl("Checkbox3"), CheckBox).Checked Then sum += 4 'add 
            If CType(chkboxes.FindControl("Checkbox4"), CheckBox).Checked Then sum += 2 'delete
            If CType(chkboxes.FindControl("Checkbox5"), CheckBox).Checked Then sum += 1 'display
        
            'return sum
            Return sum
        
        End Function
       
        Private Function RoleResourcePermissionsExists(ByVal roleId As String, ByVal resourceId As Integer) As Boolean
            Dim rows As DataRow() = rolesResourcesAccessRightsDS.Tables("RlsResLvls").Select("RoleId='" + roleId + "' and ResourceId=" + resourceId.ToString)
            If rows.Length > 0 Then Return True Else Return False
        End Function
       
        Private Function GetRoleResourceRow(ByVal roleId As String, ByVal resourceId As Integer) As DataRow
            Dim rows As DataRow() = rolesResourcesAccessRightsDS.Tables("RlsResLvls").Select("RoleId='" + roleId + "' and ResourceId=" + resourceId.ToString)
            Return rows(0)
        End Function
       
        Private Sub InsertRow(ByVal roleId As String, ByVal resourceId As Integer, ByVal accessLevel As Integer)
            Dim table As DataTable = rolesResourcesAccessRightsDS.Tables("RlsResLvls")
            Dim row As DataRow = table.NewRow()
            row("RRLId") = New Guid(Guid.NewGuid.ToString)
            row("RoleId") = New Guid(roleId)
            row("ResourceId") = resourceId
            row("AccessLevel") = accessLevel
            row("ModUser") = "Insert 3/30" 'Session("UserName")
            row("ModDate") = Date.Now()
            table.Rows.Add(row)
        End Sub
       
        Private Sub UpdateRow(ByVal roleId As String, ByVal resourceId As Integer, ByVal accessLevel As Integer, ByVal row As DataRow)
            row("AccessLevel") = accessLevel
            row("ModUser") = "Update 3/30" 'Session("UserName")
        End Sub
       
        Private Sub InsertJavascriptCode(ByVal e As RepeaterItemEventArgs, ByVal resourceTypeId As Integer)
            For i As Integer = 1 To 5
                Dim chkbox As CheckBox = CType(e.Item.FindControl("Checkbox" + i.ToString), CheckBox)
                If (ddlTypes.SelectedValue = 5) And i < 5 Then chkbox.Enabled = False
                chkbox.Attributes.Add("OnClick", "SetCheckboxes();")
            Next
        End Sub
       
        Private Sub DisplayErrorMessage(ByVal errorMessage As String)

            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

        End Sub
       
        Protected Sub ddlGroups_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            BuildDropDownLists()
            'btnBuildList_Click(Me, New EventArgs())
        End Sub
       
        Private Function ValidateLHS() As String
            Dim strError As String = ""
        
            'Ensure that a module is selected
            If ddlModules.SelectedValue = "" Then
                strError = "Module is a required field" & vbCr
            End If
            'Ensure that a page type is selected
            If ddlTypes.SelectedValue = "" Then
                strError &= "Type is a required field" & vbCr
            End If
            'Ensure that a role/group is selected
            If ddlGroups.SelectedValue = "" Then
                strError &= "Group is a required field" & vbCr
            End If
            Return strError
        End Function

        Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs)
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(ddlGroups)
            controlsToIgnore.Add(ddlModules)
            controlsToIgnore.Add(btnBuildList)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        End Sub

        Protected Sub btnBuildList_Command(ByVal sender As Object, ByVal e As CommandEventArgs)

        End Sub
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>


            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table2">
                <tr>
                    <td class="listframetop">
                        <table id="AutoNumber6" width="100%">
                            <tr>
                                <td class="contentcell">
                                    <asp:Label ID="lblGroups" runat="server" Width="100%" CssClass="label">Roles</asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlGroups" runat="server" CssClass="label" OnSelectedIndexChanged="ddlGroups_SelectedIndexChanged"
                                        AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcell">
                                    <asp:Label ID="lblModules" runat="server" CssClass="label">Module</asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlModules" runat="server" CssClass="label">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcell">
                                    <asp:Label ID="lblTypes" runat="server" Width="100%" CssClass="label">Type</asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlTypes" runat="server" CssClass="label">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcell">&nbsp;
                                </td>
                                <td class="contentcell4" style="text-align: center">
                                    <asp:Button ID="btnBuildList" runat="server"  Text="Build List"
                                        OnClick="btnBuildList_Click" OnCommand="btnBuildList_Command"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleft" style="height: expression(document.body.clientHeight - 280 + 'px')">
                        </div>
                    </td>
                </tr>
            </table>



        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="scrollright2">
                            <!-- begin content table-->


                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                <tr>
                                    <td class="contentcell">
                                        <asp:Repeater ID="rptNavigationNodes" runat="server" OnItemDataBound="rptNavigationNodes_ItemDataBound">
                                            <HeaderTemplate>
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                    <tr>
                                                        <td width="50%" style="border: 0px" class="datagridheaderstyle">&nbsp;
                                                        </td>
                                                        <td style="width: 10%; border: 0px; text-align: center" class="datagridheaderstyle">Full
                                                        </td>
                                                        <td style="width: 10%; border: 0px; text-align: center" class="datagridheaderstyle">Edit
                                                        </td>
                                                        <td style="width: 10%; border: 0px; text-align: center" class="datagridheaderstyle">Add
                                                        </td>
                                                        <td style="width: 10%; border: 0px; text-align: center" class="datagridheaderstyle">Delete
                                                        </td>
                                                        <td style="width: 10%; border: 0px; text-align: center" class="datagridheaderstyle">Display
                                                        </td>
                                                    </tr>
                                                </table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                                    <tr>
                                                        <td width="50%">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblIndentation" runat="server" Text="label"></asp:Label>
                                                                    </td>
                                                                    <td style="border: 0px; padding: 0" class="datagriditemstyle" font-bold="True">
                                                                        <asp:Label ID="lblNavigationNode" runat="server"></asp:Label>
                                                                        <asp:Label ID="lblResourceId" runat="server" Visible="false"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td style="width: 10%; border: 0px; text-align: center; padding: 0" class="securitychkboxstyle">
                                                            <asp:CheckBox ID="CheckBox1" runat="server" CssClass="label" />
                                                        </td>
                                                        <td style="width: 10%; border: 0px; text-align: center; padding: 0" class="securitychkboxstyle">
                                                            <asp:CheckBox ID="CheckBox2" runat="server" CssClass="label" />
                                                        </td>
                                                        <td style="width: 10%; border: 0px; text-align: center; padding: 0" class="securitychkboxstyle">
                                                            <asp:CheckBox ID="CheckBox3" runat="server" CssClass="label" />
                                                        </td>
                                                        <td style="width: 10%; border: 0px; text-align: center; padding: 0" class="securitychkboxstyle">
                                                            <asp:CheckBox ID="CheckBox4" runat="server" CssClass="label" />
                                                        </td>
                                                        <td style="width: 10%; border: 0px; text-align: center; padding: 0" class="securitychkboxstyle">
                                                            <asp:CheckBox ID="CheckBox5" runat="server" CssClass="label" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                            </table>



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

            <xml id="clientXml" runat="server"></xml>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

