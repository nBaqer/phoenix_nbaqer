﻿
Imports FAME.Advantage.Common
Imports FluentNHibernate.Conventions.AcceptanceCriteria
Imports Advantage.Business.Logic.Layer
Imports Microsoft.Practices.ServiceLocation
Imports FAME.Advantage.Common.Services
Imports Advantage.Business.Objects
Imports FAME.Advantage.Api.Library.Models
Imports FAME.Advantage.Api.Library

Partial Class SY_UserImpersonation
    Inherits BasePage

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load

        '-------------------------------------------------------------------------------
        'The javascript view model will postback to change the user for the application
        '-------------------------------------------------------------------------------
        If Page.IsPostBack Then

            Dim needToRedirect As Boolean = False

            '----------------------------------------------------------------------------------
            ' Get the values from the hidden fields on the page
            '----------------------------------------------------------------------------------
            Dim isImpersonating As String = hdnImpIsImpersonatingLocal.Value.ToString()
            Dim impUserId As String = hdnImpUserId.Value.ToString()
            Dim impUserName As String = hdnImpUserName.Value.ToString()
            Dim impUserAndName As String = hdnImpUserAndName.Value.ToString()
            Dim nonImpersonateUserId As String = hdnNonImpersonatingUserId.Value.ToString()
            Dim userImpersonationLogId As String = hdnUserImpersonationLogId.Value.ToString()
            '----------------------------------------------------------------------------------



            '----------------------------------------------------------------------------------
            ' check if this page needs to redirect to the dashboard.
            ' If the isimpersonating flag is true, we need to redirect.  
            ' If not, then we will redirect if we detect that the user chose to stop impersonation
            '----------------------------------------------------------------------------------
            If AdvantageSession.IsImpersonating Is Nothing Then
                If isImpersonating IsNot Nothing Then
                    needToRedirect = True
                End If
            Else
                needToRedirect = AdvantageSession.IsImpersonating.ToUpper() <> isImpersonating.ToUpper() Or AdvantageSession.ImpersonatedUserId.ToUpper() <> impUserId.ToUpper()
            End If
            '----------------------------------------------------------------------------------


            '----------------------------------------------------------------------------------
            ' Add the values we got from hidden field into the Advantage Session properties
            '----------------------------------------------------------------------------------
            AdvantageSession.IsImpersonating = isImpersonating
            AdvantageSession.ImpersonatedUserId = impUserId
            AdvantageSession.ImpersonatedUserName = impUserName
            AdvantageSession.ImpersonatedUserAndName = impUserAndName
            AdvantageSession.NonImpersonateUserId = nonImpersonateUserId
            AdvantageSession.UserImpersonationLogId = userImpersonationLogId
            '----------------------------------------------------------------------------------


            '----------------------------------------------------------------------------------
            ' If we are impersonating, get the values for the impersonated user, and create a new
            ' user state object for that user.  Then set the userstate property in Advantage Session
            ' to this new user.  If we are not impersonating, set the user back to the support user
            ' if we are impersonating then get the token for the impersonated user and reset the token and save the old token
            ' if not then get the saved token of support and reset token in session with previous token
            '----------------------------------------------------------------------------------
            If Session("IsImpersonating") IsNot Nothing Then

                If Session("IsImpersonating").ToString().ToUpper() = "TRUE" Then


                    Dim strTenantAuthUserId = Session("ImpersonatedUserId").ToString()
                    Session("TenantUserId") = Session("ImpersonatedUserId").ToString()

                    Dim intTenantid = CInt(Session("TenantNameId"))
                    Session("TenantNameId") = intTenantid

                    Dim myAppSettings As New AdvAppSettings
                    Session("AdvAppSettings") = myAppSettings

                    Dim strCustomerDBConnectionString = getConnectionString(strTenantAuthUserId, intTenantid)
                    Dim dataUserProvider = New UserStateDataProvider(Context, strCustomerDBConnectionString)

                    Dim addUser As New User() With {
                            .UserId = New Guid(strTenantAuthUserId)
                            }

                    Dim advantageUserState = dataUserProvider.getUserLoginState(addUser)

                    'Added by Balaji on Mar 21 2012
                    'To keep track of old campus when user switches to new campus
                    AdvantageSession.UserState = advantageUserState 'Load user object in session
                    AdvantageSession.UserName = advantageUserState.UserName
                    AdvantageSession.PageTheme = PageTheme.Blue_Theme
                    Dim tokenResponse As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)
                    Session("OldTokenResponse") = tokenResponse
                    dim tokenReq = New FAME.Advantage.Api.Library.Token(tokenResponse.ApiUrl)
                    Dim service = ServiceLocator.Current.GetInstance(Of IMultiTenantService)()
                    Dim tenantName = service.GetTenantName(Guid.Parse(strTenantAuthUserId),intTenantid)
                    Dim token = new FAME.Advantage.Api.Library.Models.TokenResponse()
                    token  = tokenReq.GetTokenForImpersonation(tenantName,Guid.Parse(strTenantAuthUserId),tokenResponse.Token)
                    Session("AdvantageApiToken") = token
                Else
                    If Not String.IsNullOrEmpty(Session("OldTokenResponse").tostring())
                        Session("AdvantageApiToken") = Session("OldTokenResponse")
                    End If

                    Dim strTenantAuthUserId = Session("NonImpersonateUserId").ToString()
                    Session("TenantUserId") = Session("NonImpersonateUserId").ToString()

                    Dim intTenantid = CInt(Session("TenantNameId"))
                    Session("TenantNameId") = intTenantid

                    Dim myAppSettings As New AdvAppSettings
                    Session("AdvAppSettings") = myAppSettings

                    Dim strCustomerDBConnectionString = getConnectionString(strTenantAuthUserId, intTenantid)
                    Dim dataUserProvider = New UserStateDataProvider(Context, strCustomerDBConnectionString)

                    Dim addUser As New User() With {
                            .UserId = New Guid(strTenantAuthUserId)
                            }

                    Dim advantageUserState = dataUserProvider.getUserLoginState(addUser)

                    'Added by Balaji on Mar 21 2012
                    'To keep track of old campus when user switches to new campus
                    AdvantageSession.UserState = advantageUserState 'Load user object in session
                    AdvantageSession.UserName = advantageUserState.UserName
                    AdvantageSession.PageTheme = PageTheme.Blue_Theme
                End If
            End If

            AdvantageSession.MasterStudentId = String.Empty
            AdvantageSession.MasterEmployerId = String.Empty
            AdvantageSession.MasterEmployeeId = String.Empty
            AdvantageSession.MasterLeadId = String.Empty

            ' if we need to, redirect to dashboard
            If needToRedirect Then
                Response.Redirect("../dash.aspx?resid=264&mod=SY&desc=dashboard")
            End If
        Else
            '----------------------------------------------------------------------------------
            ' Not a postback, check if this user is authorized for this page.
            ' if the the user is not support and they are not impersonating, they should not be
            ' here, so redirect them to dashboard.
            '----------------------------------------------------------------------------------
            Dim userName As String = ""
            Dim isImpersonsating As String = ""
            If AdvantageSession.UserName IsNot Nothing Then
                userName = AdvantageSession.UserName.ToUpper()
            End If
            If AdvantageSession.IsImpersonating IsNot Nothing Then
                isImpersonsating = AdvantageSession.IsImpersonating.ToUpper()
            End If

            If userName <> "SUPPORT" And isImpersonsating <> "TRUE" Then
                Response.Redirect("../dash.aspx?resid=264&mod=SY&desc=dashboard")
            End If
            '----------------------------------------------------------------------------------

            ' set the hidden fields we need
            If AdvantageSession.UserImpersonationLogId IsNot Nothing Then
                hdnUserImpersonationLogId.Value = AdvantageSession.UserImpersonationLogId.ToString()
            End If

            If AdvantageSession.IsImpersonating IsNot Nothing Then
                hdnImpIsImpersonatingLocal.Value = AdvantageSession.IsImpersonating.ToString()
            End If
        End If



    End Sub

    Private Function getConnectionString(ByVal strTenantUserId As String, ByVal strTenantNameid As Integer) As String
        Dim service = ServiceLocator.Current.GetInstance(Of IMultiTenantService)()
        Return service.GetConnectionString(Guid.Parse(strTenantUserId), strTenantNameid)
    End Function

End Class
