﻿
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports FAME.Advantage.Common
Partial Class SY_AgencySchoolMappings
    Inherits BasePage
    Private ddlDS As DataSet
    Private previousField As String = ""
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim objCommon As New FAME.common.CommonUtilities
        Dim fac As New UserSecurityFacade

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        Dim m_Context As HttpContext
        m_Context = HttpContext.Current
        'txtResourceId.Text = ResourceId
        txtResourceId.Value = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        'pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Value, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        'Call InitButtonsForLoad
        InitButtonsForLoad()
        If Not IsPostBack Then
            'build ddl's 
            BuildDropDownLists()

            'save button must be disabled during first time load
            btnsave.Enabled = False

        End If
        headerTitle.Text = Header.Title
    End Sub

    Private Sub BuildDropDownLists()

        'instantiate a metadata object for Acreditation Agencies ddl
        Dim agenciesDDLMetadata As New AcreditationAgenciesDDLMetadata()

        'add all metadataddl's to a list
        Dim ddlList As New System.Collections.Generic.List(Of AdvantageDropDownListMetadata)
        ddlList.Add(agenciesDDLMetadata)

        'get ds with dropdownlists tables
        Dim ds As DataSet = (New DropDownListsFacade).GetDropDownLists(ddlList)

        'bind ddl's
        agenciesDDLMetadata.Bind(ddlAgencyId, ds)

        If MyAdvAppSettings.AppSettings("IPEDS").ToLower = "no" Then
            ddlAgencyId.Items.RemoveAt(ddlAgencyId.Items.IndexOf(ddlAgencyId.Items.FindByText("IPEDS")))
        End If

        If MyAdvAppSettings.AppSettings("NACCAS").ToLower = "no" Then
            ddlAgencyId.Items.RemoveAt(ddlAgencyId.Items.IndexOf(ddlAgencyId.Items.FindByText("NACCAS")))
        End If

        If MyAdvAppSettings.AppSettings("ACCSCT").ToLower = "no" Then
            ddlAgencyId.Items.RemoveAt(ddlAgencyId.Items.IndexOf(ddlAgencyId.Items.FindByText("ACCSCT")))
        End If

        If MyAdvAppSettings.AppSettings("ISIR").ToLower = "no" Then
            ddlAgencyId.Items.RemoveAt(ddlAgencyId.Items.IndexOf(ddlAgencyId.Items.FindByText("ISIR")))
        End If

        If MyAdvAppSettings.AppSettings("GainfulEmployment").ToLower = "no" Then
            ddlAgencyId.Items.RemoveAt(ddlAgencyId.Items.IndexOf(ddlAgencyId.Items.FindByText("Gainful Employment")))
        End If

        'Added by Saraswathi Lakshmanan on October 16 2008
        ''If all the Report Agency Id in the WebConfigAppsettings is set to No, then the FieldDDL is not populated
        If ddlAgencyId.Items.Count <> 0 Then
            'build ddl of the fields of the selected agency
            BuildFieldsDDL(ddlAgencyId.SelectedValue.ToString)
        End If



    End Sub

    Private Sub BuildFieldsDDL(ByVal ddlSelectedValue As String)

        '   bind Fields DDL
        With ddlFieldsId
            .DataTextField = "Text"
            .DataValueField = "Value"
            .DataSource = (New AcreditationAgenciesFacade).GetFieldsUsedByAcreditationAgency(ddlSelectedValue)
            .DataBind()
            .Items.Insert(0, New ListItem("Select All", "0"))
            .SelectedIndex = 0
        End With
        '  DE 5159 Janet Robinson 4/26/2011

        If (ddlAgencyId.SelectedItem.Text.ToUpper() = "IPEDS") Then
            If MyAdvAppSettings.AppSettings("IPEDS").ToLower = "yes" Then
                ddlFieldsId.Items.RemoveAt(ddlFieldsId.Items.IndexOf(ddlFieldsId.Items.FindByText("States")))
            End If
        End If

    End Sub

    Protected Sub ddlAgencyId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAgencyId.SelectedIndexChanged
        'get fields ddl
        BuildFieldsDDL(ddlAgencyId.SelectedValue)

        'reset repeater
        rptMappingValues.DataSource = Nothing
        rptMappingValues.DataBind()

        'disable save button
        btnSave.Enabled = False

    End Sub

    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click

        'validate the DDLs
        If ddlAgencyId.Items.Count > 0 Then
            Dim result As String = (New AcreditationAgenciesFacade).ValidateDDLValues(ddlAgencyId.SelectedValue, ddlFieldsId.SelectedValue)
            'display errors
            If Not result = "" Then
                DisplayErrorMessage(result)
                Exit Sub
            Else
                'bind the Mapping Values repeater
                ddlDS = (New AcreditationAgenciesFacade).GetDDLValuesDS(ddlAgencyId.SelectedValue, ddlFieldsId.SelectedValue)
                ViewState("MappingsOptionsDS") = ddlDS
                rptMappingValues.DataSource = ddlDS.Tables("DDLValues")
                rptMappingValues.DataBind()

                'enable save button
                btnSave.Enabled = True
            End If
        End If
    End Sub

    Private Function GetAgencyFieldId(ByVal s As String) As Integer
        'return only the AgencyFieldId
        Return Integer.Parse(s.Substring(0, s.IndexOf(";"c)))
    End Function

    Protected Sub rptMappingValues_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMappingValues.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem

                'bind Field
                CType(e.Item.FindControl("lblField"), Label).Text = e.Item.DataItem("Descrip")

                'bind School Field
                CType(e.Item.FindControl("lblSchoolField"), Label).Text = e.Item.DataItem("SchoolText")

                'bind School Value
                CType(e.Item.FindControl("lblSchoolValue"), Label).Text = e.Item.DataItem("SchoolValue")

                'bind AgencyFieldId
                CType(e.Item.FindControl("lblRptAgencyFldId"), Label).Text = e.Item.DataItem("RptAgencyFldId")

                'bind ddl
                Dim ddl As DropDownList = CType(e.Item.FindControl("ddlMappedValue"), DropDownList)
                ddl.Items.Clear()
                Dim drv As DataRowView = CType(e.Item.DataItem, DataRowView)
                Dim rs As DataRow() = drv.Row.GetParentRow("RptAgencyFieldsDDLValues").GetChildRows("RptAgencyFieldsRptAgencyFldValues")
                For i As Integer = 0 To rs.Length - 1
                    ddl.Items.Add(New ListItem(CType(rs(i)("AgencyDescrip"), String), CType(rs(i)("RptAgencyFldValId"), String)))
                Next
                ddl.Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                ddl.SelectedIndex = 0

                'set selected value to ddl
                Dim rows As DataRow() = drv.Row.GetParentRow("RptAgencyFieldsDDLValues").GetChildRows("RptAgencyFieldsRptAgencyFldValues")
                If rows.Length > 0 Then
                    For i As Integer = 0 To rows.Length - 1
                        Dim r As DataRow() = rows(i).GetChildRows("RptAgencyFldValuesRptAgencySchoolMappings")
                        If r.Length > 0 Then
                            For j As Integer = 0 To r.Length - 1
                                If CType(r(j)("SchoolDescripId"), Guid).ToString = e.Item.DataItem("SchoolValue") Then
                                    ddl.SelectedValue = CType(r(j)("RptAgencyFldValId"), Integer).ToString
                                    Exit For
                                End If
                            Next
                        End If
                    Next
                End If

                ' DE 1131 Janet Robinson 05/03/2011
                CType(e.Item.FindControl("lblMappedValueOrig"), Label).Text = ddl.SelectedValue
                CType(e.Item.FindControl("lblTblName"), Label).Text = GetUpdTableName(e.Item.DataItem("Descrip"))
                ' DE 1131 End

                'set visibility of mapped values
                If chkShowOnlySchoolValuesNotMapped.Checked And Not ddl.SelectedIndex = 0 Then
                    'set visibility of the row
                    e.Item.Visible = False
                Else
                    'set visibility of the row
                    e.Item.Visible = True
                End If
        End Select
    End Sub

    ' DE 1131 Janet Robinson 05/03/2011 new function
    Private Function GetUpdTableName(ByVal strDescrip As String) As String

        Dim strTblName As String = String.Empty

        Select Case strDescrip

            Case "Gender"
                strTblName = "adGenders"
            Case "Ethnic Code"
                strTblName = "adEthCodes"
            Case "Citizenship"
                strTblName = "adCitizenships"
            Case "Degree Cert. Seeking"
                strTblName = "adDegCertSeeking"
            Case "Housing Type"
                strTblName = "arHousing"
            Case "Drop Reason"
                strTblName = "arDropReasons"
            Case "Attendance Type"
                strTblName = "arAttendTypes"
            Case "Program Type"
                strTblName = "arProgTypes"
            Case "Fund Source"
                strTblName = "saFundSources"
            Case "Degrees"
                strTblName = "arDegrees"
            Case "Tuition Categories"
                strTblName = "saTuitionCategories"
            Case "Income Level"
                strTblName = "syFamilyIncome"
            Case "Credential Level"
                strTblName = "arProgCredential"

        End Select

        Return strTblName

    End Function

    Private Function UpdateMappingOptionsDatasetWithUserSelections(ByVal user As String) As DataSet

        'recover Mapping Options Dataset from the viewstate
        ddlDS = ViewState("MappingsOptionsDS")
        Dim dt As DataTable = ddlDS.Tables("RptAgencySchoolMappings")

        'update all mapped values in the dataset
        Dim rightNow As DateTime = Date.Now
        For i As Integer = 0 To rptMappingValues.Items.Count - 1
            Dim rptAgencyFldValId As String = CType(rptMappingValues.Items(i).FindControl("ddlMappedValue"), DropDownList).SelectedValue.ToString
            Dim schoolValue As String = CType(rptMappingValues.Items(i).FindControl("lblSchoolValue"), Label).Text
            Dim rptAgencyFldId As String = CType(rptMappingValues.Items(i).FindControl("lblRptAgencyFldId"), Label).Text

            ' DE 1131 Janet Robinson 05/03/2011
            Dim mappedValueOrig As String = CType(rptMappingValues.Items(i).FindControl("lblMappedValueOrig"), Label).Text
            Dim tblName As String = CType(rptMappingValues.Items(i).FindControl("lblTblName"), Label).Text

            Try
                If Not rptAgencyFldValId = mappedValueOrig Then

                    Select Case ddlAgencyId.SelectedItem.ToString()
                        Case "IPEDS"
                            If rptAgencyFldValId = Guid.Empty.ToString Then
                                Dim BuildIPEDsDB As New ReportingAgencyFacade
                                'ddl changed to select
                                Dim result As Integer
                                If rptAgencyFldId="39" Then
                                    result = BuildIPEDsDB.UpdateIPEDSValues(0, schoolValue, "adReqs")
                                Else
                                     result = BuildIPEDsDB.UpdateIPEDSValues(0, schoolValue, tblName)
                                End If
                            End If
                        Case "Gainful Employment"
                            Dim intRptAgencyFldValId As Integer
                            If rptAgencyFldValId = Guid.Empty.ToString Then
                                intRptAgencyFldValId = 0
                            Else
                                intRptAgencyFldValId = CType(rptAgencyFldValId, Integer)
                            End If

                            Dim BuildGainfulEmploymentDB As New ReportingAgencyFacade
                            'ddl changed to select
                            Dim result As Integer = BuildGainfulEmploymentDB.UpdateGainfulEmploymentValues(intRptAgencyFldValId, schoolValue, tblName)

                    End Select

                End If

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            ' DE 1131 End

            'delete existing values for this field
            If Not rptAgencyFldId = previousField Then
                DeleteAllValuesForThisField(rptAgencyFldId)
                previousField = rptAgencyFldId
            End If

            If Not rptAgencyFldValId = Guid.Empty.ToString Then
                If Not IsInTable(rptAgencyFldValId, schoolValue) Then
                    Dim row As DataRow = dt.NewRow
                    row("MappingId") = Guid.NewGuid
                    row("RptAgencyFldValId") = rptAgencyFldValId
                    row("SchoolDescripId") = New Guid(schoolValue)
                    row("ModUser") = user
                    row("ModDate") = rightNow
                    ' DE 1131 Janet Robinson 05/03/2011
                    row("MappingIDOrig") = mappedValueOrig
                    row("TblName") = tblName
                    ' DE 1131 End
                    dt.Rows.Add(row)
                End If
            End If
        Next

        'return dataset
        Return ddlDS

    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        'update mapped values only if they have changed
        ddlDS = UpdateMappingOptionsDatasetWithUserSelections(Session("UserName"))
        Dim result As String = ""
        If ddlDS.HasChanges() Then
            result = (New AcreditationAgenciesFacade).UpdateMappedValues(ddlDS)
        End If

        'display errors
        If Not result = "" Then
            DisplayErrorMessage(result)
        Else
            btnBuildList_Click(Me, New EventArgs())
        End If
    End Sub
    Private Function IsInTable(ByVal rptAgencyFldValId As String, ByVal schoolValue As String) As Boolean
        Dim dt As DataTable = ddlDS.Tables("RptAgencySchoolMappings")
        Dim rows As DataRow() = dt.Select("RptAgencyFldValId=" + rptAgencyFldValId + " AND SchoolDescripId='" + schoolValue + "'")
        If rows.Length > 0 Then Return True Else Return False
    End Function
    Private Sub DeleteAllValuesForThisField(ByVal rptAgencyFldId As String)
        Dim dt As DataTable = ddlDS.Tables("RptAgencyFields")
        Dim rows As DataRow() = dt.Select("RptAgencyFldId=" + rptAgencyFldId)
        If rows.Length > 0 Then
            Dim rs As DataRow() = rows(0).GetChildRows("RptAgencyFieldsRptAgencyFldValues")
            For i As Integer = 0 To rs.Length - 1
                Dim rts As DataRow() = rs(i).GetChildRows("RptAgencyFldValuesRptAgencySchoolMappings")
                If rts.Length > 0 Then
                    For j As Integer = 0 To rts.Length - 1
                        rts(j).Delete()
                    Next
                End If
            Next
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        btnnew.Enabled = False
        btndelete.Enabled = False

    End Sub
End Class
