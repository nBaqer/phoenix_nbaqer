
' ===============================================================================
'
' FAME AdvantageV1
'
' GetSchoolLogo.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2005 FAME Inc.
' All rights reserved.

Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade

Partial Class GetSchoolLogo

    Inherits Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim logo As AdvantageLogoImage = Nothing

        '   Get Logo from the backend
        If Not Request.Params("schoolId") Is Nothing Then
            If Not Integer.TryParse(Request.Params("schoolId"), 0) Then
               logo = (New ReportFacade).GetSchoolLogo(Convert.ToInt32(Request.Params("schoolId")))
            End If
            'If IsValidInteger(Request.Params("schoolId")) Then
            '    logo = (New ReportFacade).GetSchoolLogo(Convert.ToInt32(Request.Params("schoolId")))
            'End If
        Else
            logo = (New ReportFacade).GetSchoolLogo()
        End If

        If Not logo Is Nothing Then
            '   send image to the browser
            Response.Clear()
            Response.ContentType = logo.ContentType
            Response.AppendHeader("content-length", CType(logo.ImageLength, String))
            If (logo.Image IsNot Nothing) Then Response.BinaryWrite(logo.Image)
            Response.End()
        End If

    End Sub
    'Private Function IsValidInteger(ByVal obj As Object) As Boolean
    '    Try
    '        Dim i As Integer = CType(obj, Integer)
    '        Return True
    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Return False
    '    End Try
    'End Function
End Class
