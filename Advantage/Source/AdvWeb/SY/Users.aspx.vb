﻿Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common
Imports System.Net.Mail
Imports FAME.Advantage.Site.Lib.Infrastruct.Helpers
Imports Microsoft.Practices.ServiceLocation
Imports FAME.Advantage.Common.Services
Imports FAME.Advantage.Messages.SystemStuff.Maintenance.Smtp
Imports FluentNHibernate.Utils

Partial Class Users
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <Diagnostics.DebuggerStepThrough> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(sender As System.Object, e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region

    Protected WithEvents pnlRHS As Panel
    Protected WithEvents txtBankId As TextBox
    Protected WithEvents OpenLink As HtmlAnchor

    Private _userPagePermissionInfo As New UserPagePermissionInfo
    Protected userId As String
    Protected campusId As String
    Protected originallyActive As Boolean
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(sender As System.Object, e As EventArgs) Handles MyBase.Load
        MyAdvAppSettings = AdvAppSettings.GetAppSettings()
        campusId = Master.CurrentCampusId

        If MyAdvAppSettings.AppSettings("UsersPageEnabled").ToLower = "no" Then
            trUnlockUser.Style.Add("visibility", "hidden")

            Dim resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            Dim advantageUserState As User = AdvantageSession.UserState

            ' Disable History button at all time
            'Header1.EnableHistoryButton(False)

            _userPagePermissionInfo = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

            If Me.Master.IsSwitchedCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If

            'pObj = Header1.UserPagePermission

            'When the page first loads we want to populate the lbxUsers controls
            'with all the existing users
            Dim objCommon As New CommonUtilities
            Dim contentMain = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder)

            If Not Page.IsPostBack Then
                ViewState("Mode") = "New"
                trUnlockUser.Style.Add("visibility", "hidden")

                objCommon.SetBtnState(contentMain, "NEW", _userPagePermissionInfo)

                Session("ManageUsersSelectedUserID") = Nothing
                BuildUsersListBox(ThreeStateCheckBox.SelectedToggleState.Value)
                BuildModulesDDL()

                BindUserInfoData(New UserSecurityInfo)
                lbResetPassword.Visible = False
                originallyActive = False
            Else
                If Session("ShowLockControl") Then
                    trUnlockUser.Style.Add("visibility", "visible")
                End If

                objCommon.SetBtnState(contentMain, "EDIT", _userPagePermissionInfo)

                If ddlUsername.SelectedIndex = -1 And Not Session("ManageUsersSelectedUserID") Is Nothing Then
                    BuildUsersListBox(ThreeStateCheckBox.SelectedToggleState.Value)

                    BuildModulesDDL()

                    If Not ddlUsername.Items.FindItemByValue(Session("ManageUsersSelectedUserID")) Is Nothing Then
                        ddlUsername.Items.FindItemByValue(Session("ManageUsersSelectedUserID")).Selected = True
                    End If

                    Dim facUserSecurit As New UserSecurityFacade
                    Dim userSecurit As UserSecurityInfo = facUserSecurit.GetUserInfo(Session("ManageUsersSelectedUserID"))

                    InitButtonsForEdit(userSecurit)
                    BindUserInfoData(userSecurit)

                    'Dim sb As New StringBuilder(HyperLink1.NavigateUrl.Substring(0, HyperLink1.NavigateUrl.IndexOf("?resid=256") + 10))
                    ''   append CourseId to the querystring
                    'sb.Append("&UID=" + txtUserId.Text)
                    'sb.Append("&cmpid=" + HttpContext.Current.Request.Params("cmpid"))
                    'sb.Append("&mod=SY")
                    'HyperLink1.NavigateUrl = sb.ToString

                    Dim sb2 As New StringBuilder(btnRoleCampus.CommandArgument.Substring(0, btnRoleCampus.CommandArgument.IndexOf("?resid=256") + 10))
                    '   append CourseId to the querystring
                    sb2.Append("&UID=" + txtUserId.Text)
                    sb2.Append("&cmpid=" + HttpContext.Current.Request.Params("cmpid"))
                    sb2.Append("&mod=SY")
                    btnRoleCampus.CommandArgument = sb2.ToString

                    Session("ManageUsersSelectedUserID") = txtUserId.Text
                End If
                '   InitButtonsForEdit(userSecurit)
                Dim facUserSF As New UserSecurityFacade
                If Not Session("ManageUsersSelectedUserID") Is Nothing AndAlso Not facUserSF.GetUserInfo(Session("ManageUsersSelectedUserID")) Is Nothing Then
                    originallyActive = facUserSF.GetUserInfo(Session("ManageUsersSelectedUserID")).AccountActive
                End If
            End If
        Else
            Dim strSearchUrl = "~/SY/ManageUsers.aspx?resid=253&mod=SY&cmpid=" + campusId
            Response.Redirect(strSearchUrl, False)
        End If
    End Sub

    Private Sub BindUserInfoData(userSecurity As UserSecurityInfo)
        With userSecurity
            txtUserId.Text = .UserId
            txtUserName.ReadOnly = .IsAdvantageSuperUser
            chkIsInDB.Checked = .IsInDB
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate
            txtFullName.Text = .FullName
            hdnFullName.Value = .FullName
            txtEmail_UserID.Text = .Email
            txtUserName.Text = .UserName
            txtPassword.Attributes.Add("Value", .UserPassword)
            txtConfirmPassword.Attributes.Add("Value", .ConfirmUserPassword)
            ddlCampusId.SelectedValue = .CampusId
            chkAccountActive.Checked = .AccountActive
            userId = txtUserId.Text
            lbResetPassword.Enabled = chkAccountActive.Checked

            If .ModuleId = 0 Then
                ddlModuleId.SelectedIndex = 0
            Else
                ddlModuleId.SelectedValue = .ModuleId
            End If
        End With
    End Sub

    Private Sub lbxUsers_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles lbxUsers.SelectedIndexChanged
        'ddlCampusId.Enabled = False
        'ddlCampusId.Visible = True
        'lblCampusIdValue.Visible = False
        Dim facUserSecurit As New UserSecurityFacade
        Dim userSecurit As UserSecurityInfo = facUserSecurit.GetUserInfo(lbxUsers.SelectedItem.Value)
        BindUserInfoData(userSecurit)

        InitButtonsForEdit(userSecurit)

        'Add items to the querystring
        'Get original querystring
        'Dim str As String
        'str = HyperLink1.NavigateUrl.Substring(0, HyperLink1.NavigateUrl.IndexOf("?resid=256") + 9)
        'Dim sb As New StringBuilder(HyperLink1.NavigateUrl.Substring(0, HyperLink1.NavigateUrl.IndexOf("?resid=256") + 10))
        ''   append CourseId to the querystring
        'sb.Append("&UID=" + lbxUsers.SelectedItem.Value)
        'sb.Append("&cmpid=" + HttpContext.Current.Request.Params("cmpid"))
        'sb.Append("&mod=SY")
        'HyperLink1.NavigateUrl = sb.ToString

        Dim str2 As String
        str2 = btnRoleCampus.CommandArgument.Substring(0, btnRoleCampus.CommandArgument.IndexOf("?resid=256") + 9)
        Dim sb2 As New StringBuilder(btnRoleCampus.CommandArgument.Substring(0, btnRoleCampus.CommandArgument.IndexOf("?resid=256") + 10))
        '   append CourseId to the querystring
        sb2.Append("&UID=" + lbxUsers.SelectedItem.Value)
        sb2.Append("&cmpid=" + HttpContext.Current.Request.Params("cmpid"))
        sb2.Append("&mod=SY")
        btnRoleCampus.CommandArgument = sb2.ToString
        Session("ManageUsersSelectedUserID") = lbxUsers.SelectedItem.Value
    End Sub

    Private Sub BuildModulesDDL()
        Dim facRels As New ResourcesRelationsFacade
        Dim dt As New DataTable

        dt = facRels.GetDefaultModules

        With ddlModuleId
            .DataSource = dt
            .DataTextField = "Resource"
            .DataValueField = "ResourceId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildUsersListBox(Optional ByVal filterCondition As String = "")
        Dim facUserSecurit As New UserSecurityFacade
        Dim dt As DataTable
        Dim strusername As String = AdvantageSession.UserState.UserName.ToString.Trim
        Dim strcampusid As String = HttpContext.Current.Request.Params("cmpid").ToString

        dt = facUserSecurit.GetExistingUsers(strusername, strcampusid)

        Dim query As EnumerableRowCollection(Of DataRow)
        Dim boundTable As DataTable

        If filterCondition = "Active" Then

            query = From dtUser In dt.AsEnumerable()
                    Where dtUser.Field(Of Boolean)("AccountActive") = True Select dtUser
            boundTable = query.CopyToDataTable()
        ElseIf filterCondition = "Inactive" Then

            query = From dtUser In dt.AsEnumerable()
                    Where dtUser.Field(Of Boolean)("AccountActive") = False Select dtUser
            boundTable = query.CopyToDataTable()
        Else
            query = From dtUser In dt.AsEnumerable() Select dtUser
            boundTable = query.CopyToDataTable()
        End If

        With lbxUsers
            .DataSource = boundTable
            .DataTextField = "FullName"
            .DataValueField = "UserId"
            .DataBind()
        End With

        ddlUsername.Items.Clear()
        With ddlUsername
            .DataSource = boundTable
            .DataBind()
        End With

        ddlUsername.ClearSelection()
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As EventArgs) Handles btnSave.Click
        Dim result As String
        Dim userSecurit As UserSecurityInfo = BuildUserSecurityInfo()
        Dim sTempPassword As String = String.Empty
        Dim bInconsistantEmailFlag = False
        Dim sHoldInconsistantUserId As String = String.Empty

        If txtUserName.Text.ToUpper = "SUPPORT" Or txtUserName.Text.ToUpper = "SUPER" Then
            result = "Cannot Add/Edit this user's information"
        Else
            If Not Regex.IsMatch(txtEmail_UserID.Text.ToString.Trim, "^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$") Then
                DisplayAlert("The Email/User Name is not properly formatted. Please re-enter.")
                Return
            End If

            Dim facade = New UserSecurityFacade()

            If ViewState("Mode") = "New" Then
                'check to see if user is in the Tenant DB
                Dim membershipUsers = Membership.FindUsersByEmail(userSecurit.Email)
                If membershipUsers.Count > 0 Then
                    DisplayAlert("This user already exists in the database")
                    Return
                End If

                'check to see if the display name already exists in Advantage DB
                If facade.DisplayNameExists(txtUserName.Text) Then
                    DisplayAlert("The Display Name already exists in the database")
                    Return
                End If

                'add the new user to the databases
                sTempPassword = GetPassword()
                Dim membershipUser = Membership.CreateUser(userSecurit.Email, sTempPassword, userSecurit.Email, "What is your favorite color", "Blue", True, MembershipCreateStatus.Success)

                'we successfully added a new user to the Tenant DB so we need to update the TenantUsers table with the entry 
                Try
                    Dim newUserId = Guid.Parse(membershipUser.ProviderUserKey.ToString())
                    Dim tenantId = Integer.Parse(Session("TenantNameId").ToString())

                    Dim service = ServiceLocator.Current.GetInstance(Of IMultiTenantService)()
                    service.UpdateTenantAccess(newUserId, tenantId)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Membership.DeleteUser(txtEmail_UserID.Text)
                    Throw
                End Try
            Else
                'edit processing
                'check to see id a email change is being requested, and make sure it does not already exist in the Tenant DB
                If Not txtEmail_UserID.Text.ToString.Trim = Session("Email/UserName").ToString.Trim Then
                    Dim ds As MembershipUserCollection = Membership.FindUsersByEmail(txtEmail_UserID.Text)
                    If ds.Count > 0 Then
                        DisplayAlert("The email address of " + txtEmail_UserID.Text + " is already being used in the database." & vbCrLf &
                                     "User email addresses must be unique.")
                        Exit Sub
                    End If
                End If
                'check to see if the display name already exists in Advantage DB
                Dim bDoesDisplayNameExists As Boolean = facade.DisplayNameExists(txtUserName.Text, txtUserId.Text)
                If bDoesDisplayNameExists = True Then
                    DisplayAlert("The Display Name already exists in the database")
                    Exit Sub
                End If

                Dim memUser As MembershipUser
                memUser = Membership.GetUser(Session("Email/UserName").ToString)
                If memUser Is Nothing Then
                    'we have a record that has inconsistant email addresses in the aspnet_membership table and aspnet_users table
                    memUser = Membership.GetUser(New Guid(userSecurit.UserId))
                    bInconsistantEmailFlag = True
                    sHoldInconsistantUserId = userSecurit.UserId
                End If

                memUser.IsApproved = chkAccountActive.Checked
                memUser.Email = txtEmail_UserID.Text
                Membership.UpdateUser(memUser)

                If Not chkAccountLocked.Checked Then
                    memUser.UnlockUser()
                    Session("ShowLockControl") = False
                Else
                    trUnlockUser.Visible = True
                End If

                'if the user is requesting a username/email update
                If Session("Email/UserName") <> memUser.Email Then
                    Try
                        facade.ResetUsers_UserName(memUser.ProviderUserKey.ToString, txtEmail_UserID.Text)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)


                    End Try
                End If
            End If

            If userSecurit.IsInDB Then
                result = facade.CheckDuplicateUserOrEmail(userSecurit.UserName, userSecurit.Email, userSecurit.UserId)
            Else
                result = facade.CheckDuplicateUserOrEmail(userSecurit.UserName, userSecurit.Email)
            End If

            If result = True Then
                'result = "Username or Email already exists in the database."
                Dim userId As String = Membership.GetUser(userSecurit.Email.ToString).ProviderUserKey.ToString()
                Membership.DeleteUser(txtEmail_UserID.Text)
                result = facade.DeleteTenantUsers(userId)

                If result = Nothing Then
                    DisplayAlert("Username or Email already exists in the database")
                Else
                    DisplayAlert(result)
                End If
                Exit Sub
            Else
                With New UserSecurityFacade
                    'Update UserSecurityInfo 
                    Dim userid As String
                    If bInconsistantEmailFlag = True Then
                        userid = sHoldInconsistantUserId.ToString
                    Else
                        userid = Membership.GetUser(userSecurit.Email.ToString).ProviderUserKey.ToString()
                    End If
                    userSecurit.UserId = userid
                    result = .UpdateUserSecurityInfo(userSecurit, AdvantageSession.UserState.UserName, hdnFullName.Value)

                    'add dashboard user role for new user
                    If result = "" And ViewState("Mode") = "New" Then
                        'insert dashboard role for new user'
                        Dim ds As DataSet = .GetDashboardRoleID
                        Dim sRoleID As String = ds.Tables(0).Rows(0).Item(0).ToString
                        Dim sCampGrpID As String = ds.Tables(0).Rows(0).Item(1).ToString

                        .AddUserRoleCampusGroup(userid, sRoleID, sCampGrpID, AdvantageSession.UserState.UserId.ToString)

                        'insert a campusid for the new user
                        .InsertCampusIdforNewUser(userid, campusId)

                        txtUserId.Text = userid
                    End If
                End With
            End If
        End If
        ''''''''''''''

        If Not result = "" Then
            'Display Error Message
            DisplayAlert(result)
        Else
            Session("Email/UserName") = txtEmail_UserID.Text

            If chkAccountActive.Checked AndAlso (ViewState("Mode") = "New" OrElse (ViewState("Mode") = "Edit" AndAlso originallyActive = False)) Then
                SendNewUserInviteEmail(Session("Email/UserName").ToString, sTempPassword)
            End If

            'Get the UsersecurityInfo from the backend and display it
            GetUserId(txtUserId.Text)
            Session("ManageUsersSelectedUserID") = txtUserId.Text

            InitButtonsForEdit(userSecurit)

        End If

        Try
            Dim userKey = New Guid(userSecurit.UserId)
            Dim memUser As MembershipUser = Membership.GetUser(userKey)

            If memUser.IsLockedOut And memUser.IsApproved Then
                trUnlockUser.Style.Add("visibility", "visible")
                chkAccountLocked.Checked = True
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayAlert("There is a problem with this user. Please contact your support administrator ")
        End Try

        'If there are no errors bind a new entity and init buttons
        If Page.IsValid Then
            InitButtonsForEdit(userSecurit)

            'Add items to the querystring
            'Get original querystring
            If Not Session("ManageUsersSelectedUserID") Is Nothing Then
                'Dim sb As New StringBuilder(HyperLink1.NavigateUrl.Substring(0, HyperLink1.NavigateUrl.IndexOf("?resid=256") + 10))
                ''   append CourseId to the querystring
                ''   sb.Append("&UID=" + lbxUsers.SelectedItem.Value)
                'sb.Append("&UID=" + Session("ManageUsersSelectedUserID"))
                'sb.Append("&cmpid=" + HttpContext.Current.Request.Params("cmpid"))
                'sb.Append("&mod=SY")
                'HyperLink1.NavigateUrl = sb.ToString

                Dim sb2 As New StringBuilder(btnRoleCampus.CommandArgument.Substring(0, btnRoleCampus.CommandArgument.IndexOf("?resid=256") + 10))
                '   append CourseId to the querystring
                '   sb.Append("&UID=" + lbxUsers.SelectedItem.Value)
                sb2.Append("&UID=" + Session("ManageUsersSelectedUserID"))
                sb2.Append("&cmpid=" + HttpContext.Current.Request.Params("cmpid"))
                sb2.Append("&mod=SY")
                btnRoleCampus.CommandArgument = sb2.ToString


            End If

            '  Set the property IsInDB to true in order to avoid an error if the user
            '  hits "save" twice after adding a record.
            chkIsInDB.Checked = True

            BuildUsersListBox(ThreeStateCheckBox.SelectedToggleState.Value)

            ViewState("Mode") = "Edit"
            txtEmail_UserID.Enabled = False
            lbResetPassword.Visible = True
        End If
    End Sub

    Private Sub SendNewUserInviteEmail(userName As String, password As String)
        Dim link As String = MultiTenantHostHelper.GetPasswordResetURL(userName, password)

        Dim mailBody As String
        mailBody = "<p> Your User Name: " & userName & " has been added to Advantage </p>"
        mailBody += "<p> Click here to set up your password: " & link & "</p>"
        mailBody += "<p> If you have any questions or trouble logging on please contact a site administrator</p>"
        mailBody += "<p> Thanks, </p>"
        mailBody += "<p> Advantage Support </p>"

        Try
            Dim smtpObject = New smtp.SmtpSendMessageInputModel()
            Dim bodyBytes = Encoding.UTF8.GetBytes(mailBody)
            Dim body = Convert.ToBase64String(bodyBytes)
            With smtpObject

                .To = userName
                .Body64 = body
                .Command = 1
                .ContentType = "text/html"
                .Subject = "Advantage New User Added"
            End With

            Dim sMailReturnMsg = FAME.AdvantageV1.BusinessFacade.MSG.Mail.SendEmailService(smtpObject) '.SendEmailMessage(insMail)

            If Not sMailReturnMsg = String.Empty Then
                If sMailReturnMsg.Equals("Bad Request") Then
                    DisplayAlert("User successfully created. Your email settings are not configured. Please contact your system administrator. System Report: Failure sending email.")
                Else
                    DisplayAlert(sMailReturnMsg)
                End If
                Dim errorText = String.Format("Your email settings are not configured. Please contact your system administrator. System Report: {0}", sMailReturnMsg)
                DisplayAlert(errorText)
            Else
                CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, "Email sent!")
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

    Private Function BuildUserSecurityInfo() As UserSecurityInfo
        Dim userSecuritInfo As New UserSecurityInfo

        With userSecuritInfo
            'Get IsInDB
            .IsInDB = chkIsInDB.Checked

            'Get UserId
            .UserId = txtUserId.Text

            'Get FullName
            .FullName = txtFullName.Text

            'Get Email
            .Email = txtEmail_UserID.Text

            'Get UserName
            .UserName = txtUserName.Text

            'Get Password
            .UserPassword = txtPassword.Text

            'Get ConfirmPassword
            .ConfirmUserPassword = txtConfirmPassword.Text

            'Get AccountActive
            .AccountActive = chkAccountActive.Checked

            'Get ModUser
            .ModUser = AdvantageSession.UserState.UserName

            'Get ModDate
            If IsDate(txtModDate.Text) Then
                .ModDate = Date.Parse(txtModDate.Text)
            End If

            ''Get CampusId
            '.CampusId = ddlCampusId.SelectedItem.Value

            'Get ModuleId
            .ModuleId = ddlModuleId.SelectedItem.Value
        End With

        '   return data
        Return userSecuritInfo
    End Function

    Private Sub DisplayAlert(errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub
    Private Sub DisplayInfoMessage(infoMessage As String)
        'Display Information in message box in the client
        CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, infoMessage)
    End Sub
    Public Sub DisplayWarningMessage(infoMessage As String)
        'Display Warning in message box in the client
        CommonWebUtilities.DisplayWarningInMessageBox(Me.Page, infoMessage)
    End Sub
    Private Sub DisplayErrorMessage(errorMessage As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub InitButtonsForEdit(userSecurity As UserSecurityInfo)
        If _userPagePermissionInfo.HasFull Or _userPagePermissionInfo.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'If _userPagePermissionInfo.HasFull Or _userPagePermissionInfo.HasDelete Then
        '    ''Code changed by Kamalesh ahuja on 27 April 2010 to resolve mantis issues id 18047 ''
        '    If txtUserName.Text.ToUpper = "SUPER" Or txtUserName.Text.ToUpper = "SUPPORT" Then
        '        btnDelete.Enabled = False
        '    Else
        '        btnDelete.Enabled = True
        '    End If
        '    ''''''''''''''''
        'ElseIf userSecurity.IsAdvantageSuperUser Then
        '    ''Code changed by Kamalesh ahuja on 27 April 2010 to resolve mantis issues id 18047 ''
        '    If txtUserName.Text.ToUpper = "SUPER" Or txtUserName.Text.ToUpper = "SUPPORT" Then
        '        btnDelete.Enabled = False
        '    Else
        '        btnDelete.Enabled = True
        '    End If
        '    '''''''''''''
        '    'Set the Delete Button so it prompts the user for confirmation when clicked
        '    btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        'Else
        '    btnDelete.Enabled = False
        'End If

        If _userPagePermissionInfo.HasFull Or _userPagePermissionInfo.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        ''Code changed by Kamalesh ahuja on 27 April 2010 to resolve mantis issues id 18047 ''
        If txtUserName.Text.ToUpper = "SUPER" Or txtUserName.Text.ToUpper = "SUPPORT" Then
            'HyperLink1.Enabled = False
            btnRoleCampus.Enabled = False
        Else
            'HyperLink1.Enabled = True
            btnRoleCampus.Enabled = userSecurity.AccountActive
        End If
        ''HyperLink1.Enabled = True
        '''''''''''''''''''

        HyperLink2.Enabled = True

        ''Set the Delete Button so it prompts the user for confirmation when clicked
        'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

    End Sub

    Private Sub btnNew_Click(sender As System.Object, e As System.EventArgs) Handles btnNew.Click
        'If Session("username") = "sa" Then
        '    'ddlCampusId.Enabled = True
        '    'ddlCampusId.Visible = True
        '    'lblCampusIdValue.Visible = False
        'Else

        '    'ddlCampusId.SelectedValue = campusId
        '    ''lblCampusIdValue.Visible = True
        '    ''lblCampusIdValue.Text = ddlCampusId.SelectedItem.Text
        '    'ddlCampusId.Visible = False
        'End If
        Session("ManageUsersSelectedUserID") = Nothing
        Dim objCommon As New CommonWebUtilities

        'Bind an empty new BankInfo
        BindUserInfoData(New UserSecurityInfo)

        'Make sure that no item is selected in the lbxUsers control
        lbxUsers.SelectedIndex = -1
        ddlUsername.ClearSelection()

        'Initialize buttons
        InitButtonsForLoad()

        'Set the focus to the Name textbox
        CommonWebUtilities.SetFocus(Me.Page, txtFullName)
        GridView1.DataSource = Nothing
        GridView1.DataBind()

        'txtFullName.Enabled = True
        'rfvFullName.Enabled = True
        'txtEmail_UserID.Enabled = True
        'rfvEmail_UserID.Enabled = True
        'txtPassword.Enabled = True
        'rfvPassword.Enabled = True
        'txtConfirmPassword.Enabled = True
        'rfvConfirmPassword.Enabled = True
        ViewState("Mode") = "New"
        lbResetPassword.Visible = False

    End Sub

    Private Sub InitButtonsForLoad()
        If _userPagePermissionInfo.HasFull Or _userPagePermissionInfo.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If _userPagePermissionInfo.HasFull Or _userPagePermissionInfo.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
        'HyperLink1.Enabled = False
        btnRoleCampus.Enabled = False
        HyperLink2.Enabled = True
    End Sub

    Private Sub btnDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete.Click
        If (txtUserId.Text = Guid.Empty.ToString) Then
            Return
        End If

        Dim userId As String = Membership.GetUser(txtEmail_UserID.Text.ToString).ProviderUserKey.ToString()

        Dim result As String

        Dim gUser = New Guid(userId)

        '=================================================================
        ' Instantiate component and call method to delete the user
        ' If there is no error the return value should be 1.  If there is 
        ' an error code from the stored procedure, display the appropriate
        ' error code
        '=================================================================
        Dim facade As New UserSecurityFacade
        result = facade.DeleteAdvantageUser(gUser)

        If Not String.IsNullOrEmpty(result) Then
            DisplayAlert(result)

            Exit Sub
        Else

            BuildUsersListBox(ThreeStateCheckBox.SelectedToggleState.Value)

            'Bind an empty new UserSecurityInfo
            BindUserInfoData(New UserSecurityInfo)

            'Initialize buttons
            InitButtonsForLoad()

            'Set the focus to the Name textbox
            CommonWebUtilities.SetFocus(Me.Page, txtFullName)
        End If
    End Sub

    Private Sub GetUserId(userId As String)
        'Create a UserSecurityFacade Instance
        Dim saf As New UserSecurityFacade

        '   bind BankAcct properties
        BindUserInfoData(saf.GetUserInfo(userId))
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()

        'add save button 
        controlsToIgnore.Add(btnSave)

        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub

    Protected Sub HyperLink2_Click(sender As Object, e As System.EventArgs) Handles HyperLink2.Click
        'Dim url As String = "../SY/SummaryOfRolesAndCampus.aspx?resid=383" + "&UID=" + Session("UserId") + "&UName=" + txtFullName.Text
        If Not Session("ManageUsersSelectedUserID") Is Nothing And txtUserName.Text.Trim() <> "" Then
            Dim url As String = "../SY/SummaryOfRolesAndCampus.aspx?resid=383" + "&UID=" + Session("ManageUsersSelectedUserID").ToString() + "&UName=" + txtFullName.Text
            Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium
            Dim name = "RolesAssignedToUsers"
            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
        End If
    End Sub

    Protected Sub ddlUsername_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlUsername.SelectedIndexChanged
        If ddlUsername.SelectedIndex = -1 Then
            Return
        End If

        Dim facUserSecurit As New UserSecurityFacade
        Dim userSecurit As UserSecurityInfo = facUserSecurit.GetUserInfo(ddlUsername.SelectedItem.Value)

        BindUserInfoData(userSecurit)
        InitButtonsForEdit(userSecurit)

        'Add items to the querystring
        'Get original querystring
        'Dim sb As New StringBuilder(HyperLink1.NavigateUrl.Substring(0, HyperLink1.NavigateUrl.IndexOf("?resid=256") + 10))
        ''   append CourseId to the querystring
        'sb.Append("&UID=" + ddlUsername.SelectedItem.Value)
        'sb.Append("&cmpid=" + HttpContext.Current.Request.Params("cmpid"))
        'sb.Append("&mod=SY")
        'HyperLink1.NavigateUrl = sb.ToString

        Dim sb2 As New StringBuilder(btnRoleCampus.CommandArgument.Substring(0, btnRoleCampus.CommandArgument.IndexOf("?resid=256") + 10))
        '   append CourseId to the querystring
        sb2.Append("&UID=" + ddlUsername.SelectedItem.Value)
        sb2.Append("&cmpid=" + HttpContext.Current.Request.Params("cmpid"))
        sb2.Append("&mod=SY")
        btnRoleCampus.CommandArgument = sb2.ToString

        BindGridView(userId)
        Session("ManageUsersSelectedUserID") = ddlUsername.SelectedItem.Value

        ViewState("Mode") = "Edit"
        Session("Email/UserName") = userSecurit.Email

        Try
            Dim userKey = New Guid(userSecurit.UserId)
            Dim memUser As MembershipUser
            memUser = Membership.GetUser(userKey)
            If memUser.IsLockedOut Then
                trUnlockUser.Style.Add("visibility", "visible")
                chkAccountLocked.Checked = True
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayAlert("There is a problem with this user. Please contact your support administrator ")
        End Try

        lbResetPassword.Visible = True
    End Sub

    Protected Sub ThreeStateCheckBox_Command(sender As Object, e As System.Web.UI.WebControls.CommandEventArgs) Handles ThreeStateCheckBox.Command
        BuildUsersListBox(ThreeStateCheckBox.SelectedToggleState.Value)
        'Bind an empty new BankInfo
        BindUserInfoData(New UserSecurityInfo)
        'Make sure that no item is selected in the lbxUsers control
        lbxUsers.SelectedIndex = -1

        'Initialize buttons
        InitButtonsForLoad()
        ddlUsername.Text = ""
    End Sub

    Protected Function GetImageSrc(Status As Boolean) As String
        If Status = True Then
            Return "../images/AdvActive.png"
        Else
            Return "../images/AdvInactive.png"
        End If
    End Function

    Protected Function GetStatus(Status As Boolean) As String
        If Status = True Then
            Return "Active"
        Else
            Return "Inactive"
        End If
    End Function

    Private Sub BindGridView(UserId As String)
        Dim facade As New UserSecurityFacade
        Dim dt As DataTable = facade.GetCampusGroupsByUser(UserId)
        For Each dr As DataRow In dt.Rows
            dr("CampGrpDescrip") = dr("CampGrpDescrip") & " (" & dr("Status") & ")"
        Next
        GridView1.DataSource = dt
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        Dim facade As New UserSecurityFacade
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim blCampus = CType(e.Row.FindControl("BlCampus"), BulletedList)
            Dim blRole = CType(e.Row.FindControl("BlRole"), BulletedList)

            Dim ds, ds1 As DataSet
            ds = facade.GetCampusAndRolesByCampusGroups(userId, GridView1.DataKeys(e.Row.RowIndex).Value.ToString)

            blCampus.DataSource = ds
            blCampus.DataBind()


            ds1 = facade.GetRolesByCampusGroups(userId, GridView1.DataKeys(e.Row.RowIndex).Value.ToString)
            blRole.DataSource = ds1
            blRole.DataBind()
        End If
    End Sub

    Protected Sub btnrefreshGrid_Click(sender As Object, e As EventArgs) Handles btnrefreshGrid.Click
        userId = txtUserId.Text
        BindGridView(userId)
    End Sub

    Protected Sub resetPassword_Click(sender As Object, e As EventArgs)
        Dim userName = Session("Email/UserName").ToString()
        Dim memUser = GetMembershipUser(userName, txtUserId.Text)

        Dim newPassword = ResetPassword(memUser)
        SendPasswordResetEmail(userName, newPassword)
    End Sub

    Private Sub SendPasswordResetEmail(userName As String, newPassword As String)
        Try

            Dim smtpObject = New smtp.SmtpSendMessageInputModel()
            Dim bodyBytes = Encoding.UTF8.GetBytes(GetPasswordResetMailBody(userName, newPassword))
            Dim body = Convert.ToBase64String(bodyBytes)
            With smtpObject

                .To = userName
                .Body64 = body
                .Command = 1
                .ContentType = "text/html"
                .Subject = "Advantage Password Reset"
            End With

            Dim result As String = FAME.AdvantageV1.BusinessFacade.MSG.Mail.SendEmailService(smtpObject)

            If Not result = String.Empty Then
                DisplayAlert(result)
            End If

            DisplayAlert("The users password has been reset.")
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayAlert(ex.Message)
        End Try
    End Sub

    Private Shared Function GetPasswordResetMailBody(userName As String, newPassword As String) As String
        Dim passwordResetUrl As String = MultiTenantHostHelper.GetPasswordResetURL(userName, newPassword)

        Dim mailBody As New StringBuilder()
        mailBody.AppendFormat("<p> Username: {0} </p>", userName)
        mailBody.Append("<p> Your password has been reset by an Administrator</p>")
        mailBody.AppendFormat("<p> Click here to change your password: {0}</p>", passwordResetUrl)
        mailBody.Append("<p> If you have any questions or trouble logging on please contact a site administrator</p>")
        mailBody.Append("<p> Thanks,</p>")
        mailBody.Append("<p> Advantage Support </p>")

        Return mailBody.ToString()
    End Function

    Private Shared Function GetMembershipUser(userName As String, userID As String) As MembershipUser
        Dim membershipUser = Membership.GetUser(userName)

        ' Fallback to looking up the user by ID
        If membershipUser Is Nothing Then
            membershipUser = Membership.GetUser(New Guid(userID))
        End If

        Return membershipUser
    End Function

    Private Shared Function ResetPassword(memUser As MembershipUser) As String
        memUser.UnlockUser()
        Dim oldPassword As String = memUser.ResetPassword()
        Dim newPassword As String = GetPassword()

        'Change the user's old password to new password
        memUser.ChangePassword(oldPassword, newPassword)

        ' Call the service to reset the creationdates to trigger a password reset at next login.
        Dim service = ServiceLocator.Current.GetInstance(Of IMultiTenantService)()
        Dim userId As Guid = Guid.Parse(memUser.ProviderUserKey.ToString())
        service.ResetPasswordDates(userId)

        Return newPassword
    End Function

    Private Shared Function GetPassword() As String
        'new password that is 10 characters, 4 lowercase letters, 4 numbers, and 2 uppercase letters. 
        Dim sBuilder As New StringBuilder()

        With sBuilder
            .Append(RandomString(4, True))
            .Append(RandomNumber(1000, 9999))
            .Append(RandomString(2, False))
            .Append(RandomSymbol())
        End With

        Return sBuilder.ToString()
    End Function

    Private Shared Function RandomNumber(min As Integer, max As Integer) As Integer
        Dim random As New Random()
        Return random.Next(min, max)
    End Function

    Private Shared Function RandomString(size As Integer, lowerCase As Boolean) As String
        Dim builder As New StringBuilder()
        Dim random As New Random()
        Dim ch As Char
        Dim i As Integer
        For i = 0 To size - 1
            ch = Convert.ToChar(Convert.ToInt32((26 * random.NextDouble() + 65)))
            builder.Append(ch)
        Next i
        If lowerCase Then
            Return builder.ToString().ToLower()
        End If
        Return builder.ToString()
    End Function

    Private Shared Function RandomSymbol() As String
        Return "*"
    End Function

    Protected Sub chkAccountLocked_CheckedChanged(sender As Object, e As EventArgs) Handles chkAccountLocked.CheckedChanged
        Dim memUser As MembershipUser
        memUser = Membership.GetUser(Session("Email/UserName").ToString)
        memUser.UnlockUser()
    End Sub

    Protected Sub btnRoleCampus_Click(sender As Object, e As EventArgs) Handles btnRoleCampus.Click
        If (Not Session("ManageUsersSelectedUserID") Is Nothing) Then
            Dim url As String = btnRoleCampus.CommandArgument
            Dim script As String = String.Format("window.open('{0}');", url)

            Page.ClientScript.RegisterStartupScript(Me.GetType(), "UsersCampGrps" + UniqueID, script, True)
        Else
            DisplayAlert("Please choose or save user before assigning roles.")
        End If
    End Sub
End Class
