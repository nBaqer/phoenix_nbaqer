﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="AgencySchoolMappings.aspx.vb" Inherits="SY_AgencySchoolMappings" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="98%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button>
                            <asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <!-- end top menu (save,new,reset,delete,history)-->
                                    <table width="80%" cellpadding="0" cellspacing="0" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblAgency" runat="server" CssClass="label">Agency</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlAgencyId" runat="server" AutoPostBack="true" CssClass="dropdownlist">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblField" runat="server" CssClass="label">Field</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlFieldsId" runat="server" CssClass="dropdownlist">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">&nbsp;</td>
                                            <td class="contentcell4" style="text-align: left">
                                                <asp:CheckBox ID="chkShowOnlySchoolValuesNotMapped" runat="server" Text="Show only school values not mapped"
                                                    Checked="True" OnCheckedChanged="btnBuildList_Click" AutoPostBack="false" CssClass="checkbox"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">&nbsp;</td>
                                            <td class="contentcell4">
                                                <div style="margin: 10px;">
                                                    <asp:Button ID="btnBuildList" runat="server" Text="Build List" OnClick="btnBuildList_Click"></asp:Button>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <div>
                                        <table width="80%" cellpadding="0" cellspacing="0" align="center" class="tableborder">
                                            <asp:Repeater ID="rptMappingValues" runat="server" OnItemDataBound="rptMappingValues_ItemDataBound">
                                                <HeaderTemplate>
                                                    <tr>
                                                        <td style="width: 25%; border-bottom: 1px solid #A3C7E2; border-right: 1px solid #A3C7E2; padding: 4px; font: bold 11px verdana;">Field</td>
                                                        <td style="width: 35%;  border-bottom: 1px solid  #A3C7E2; border-right: 1px solid  #A3C7E2; padding: 4px; font: bold 11px verdana;" >School Value</td>
                                                        <td style="width: 40%; border-bottom: 1px solid  #A3C7E2; padding: 4px; font: bold 11px verdana;" >Agency Value</td>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="border-bottom: 1px solid #A3C7E2; border-right: 1px solid #A3C7E2; padding: 4px; font: bold 11px verdana; vertical-align: middle">
                                                            <asp:Label ID="lblField" runat="server" Text="Label" CssClass="label"></asp:Label>
                                                        </td>
                                                        <td style="border-bottom: 1px solid #A3C7E2; border-right: 1px solid #A3C7E2; padding: 4px; font: bold 11px verdana; vertical-align: middle">
                                                            <asp:Label ID="lblSchoolField" runat="server" Text="Label" CssClass="label"></asp:Label>
                                                        </td>
                                                        <td style="border-bottom: 1px solid #A3C7E2; padding: 4px; font: bold 11px verdana; vertical-align: middle">
                                                            <asp:Label ID="lblSchoolValue" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblRptAgencyFldId" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblMappedValueOrig" runat="server" Visible="false"></asp:Label>
                                                            <asp:Label ID="lblTblName" runat="server" Visible="false"></asp:Label>
                                                            <asp:DropDownList ID="ddlMappedValue" runat="server" Width="250px" CssClass="dropdownlist">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                    </div>
                                </div>
                            </asp:Panel>
                            <!--end table content-->
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        <%--<asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>--%>
        <asp:HiddenField ID="txtResourceId" runat="server"></asp:HiddenField>
        <!--end validation panel-->
    </div>

</asp:Content>




