
<%@ Page Language="vb" AutoEventWireup="false" Inherits="AuditHistDisplay" CodeFile="AuditHistDisplay.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML style="overflow-x: scroll" >
	<HEAD>
		<title>Audit History</title>
	</HEAD>
	<body style="padding: 0; margin:0; border:0; text-align: center;" >
  
		<form id="form1" runat="server">

         <telerik:RadScriptManager ID="ScriptManager1" runat="server" 
             EnableTheming="True">
             <Scripts>
                 <asp:ScriptReference Assembly="Telerik.Web.UI" 
                     Name="Telerik.Web.UI.Common.Core.js">
                 </asp:ScriptReference>
                 <asp:ScriptReference Assembly="Telerik.Web.UI" 
                     Name="Telerik.Web.UI.Common.jQuery.js">
                 </asp:ScriptReference>
                 <asp:ScriptReference Assembly="Telerik.Web.UI" 
                     Name="Telerik.Web.UI.Common.jQueryInclude.js">
                 </asp:ScriptReference>
             </Scripts>
         </telerik:RadScriptManager>
                 <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingMainPanel" runat="server"></telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="dgHist" >
                    <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="dgHist"  LoadingPanelID="RadAjaxLoadingMainPanel"/>
            </UpdatedControls>
        </telerik:AjaxSetting>
           </AjaxSettings>
        </telerik:RadAjaxManager>
            <div class="Table100" style="background-color: #154d7e; text-align: left">
                <img src="../images/advantage_history.jpg"/>
            </div>
            <br/>
	
         <div style="margin: 0 auto; width: 750px; height: 500px" >
               <telerik:radgrid id="dgHist" runat="server"  AllowSorting="True"  HorizontalAlign="Center" 
             AllowPaging="True" PageSize="10" 
            AutoGenerateColumns="False" Width="750px"    
             AllowFilteringByColumn="True" GridLines="Both">
					            <GroupingSettings CaseSensitive="false"/>	
            <MasterTableView AllowPaging="true" PageSize="10" PagerStyle-Mode="NumericPages" AllowFilteringByColumn="True" >
            
            
            <Columns>
            <telerik:GridBoundColumn DataField="Event" HeaderText="Action" SortExpression="Event"  AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"/>
            <telerik:GridBoundColumn  DataField="EventDate" HeaderText="Date Modified" SortExpression="EventDate"  AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" />
          <%--  <telerik:GridBoundColumn  DataField="AppName" HeaderText="Application" SortExpression="AppName"   AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"/>--%>
            <telerik:GridBoundColumn DataField="UserName" HeaderText="Modified By" SortExpression="UserName"   AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"/>
            <telerik:GridBoundColumn  DataField="Caption" HeaderText="Field" SortExpression="Caption"   AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"/>
            <telerik:GridBoundColumn  DataField="OldValue" HeaderText="Old Value" SortExpression="OldValue"   AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"/>
            <telerik:GridBoundColumn  DataField="NewValue" HeaderText="New Value" SortExpression="NewValue"   AutoPostBackOnFilter="true" CurrentFilterFunction="Contains"/>
            </Columns>
            </MasterTableView>
 
            </telerik:radgrid>
    </div>
           

 	
 
		</form>
       
 <div style=" font: normal 11px verdana;
    color: #fff;
    width: 100%;
    position: fixed;
    background-color: #154d7e;
   height:25px;
    bottom: 0;
    left: 0;
    vertical-align: bottom;
    margin-bottom: 0;
    text-align: center;
    padding-top: 4px;
    ">
     <asp:Label runat="server" style="width:250px;margin:auto;color:white;font-size:x-small;">
         Copyright &copy; 2005 - <%=Year(DateTime.Now).ToString%> FAME. All Rights Reserved.
     </asp:Label>
   
 </div>          
	</body>
</HTML>

