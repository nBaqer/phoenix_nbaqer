﻿
Imports FAME.Advantage.MultiTenantHost.Lib.Presenters

Partial Class SY_StateBoardSettings
    Inherits Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim presenter = New TenantPickerPresenter(User.Identity.Name)
        If presenter.IsSupportUser() Then
            btnDelete.Visible = True
        Else
            btnDelete.Visible = False
        End If

        headerTitle.Text = Header.Title
    End Sub
End Class
