﻿
Partial Class PasswordResetDefault
    Inherits System.Web.UI.Page
    Protected Sub ChangePasswordPushButton_Click(sender As Object, e As EventArgs)
        If Not Session("UserName") Is Nothing Then
            Dim user As MembershipUser = Membership.GetUser(Session("UserName").ToString())
            Dim currentPassword As String = CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("CurrentPassword"), TextBox).Text
            Dim newPassword As String = CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("NewPassword"), TextBox).Text
            Dim boolSuccess As Boolean = user.ChangePassword(currentPassword, newPassword)

            If boolSuccess Then
                Response.Redirect("http://dev2/Advantage/dev/3.2/dash.aspx")
            End If


        End If
    End Sub
End Class
