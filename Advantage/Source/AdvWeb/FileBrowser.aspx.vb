﻿

Partial Class FileBrowser
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session("File2Browse") = Request.QueryString("fileurl")
        If IsPostBack = False Then
            AttachmentBrowser()
        End If

    End Sub

    Public Shared Sub AttachmentBrowser()
        Try
            Dim path As String = HttpContext.Current.Session("File2Browse")
            HttpContext.Current.Session.Remove("File2Browse")

            Dim fi As New IO.FileInfo(path)

            If fi.Exists Then
                ' Open a file that is to be loaded into a byte array
                Dim oFile As System.IO.FileInfo
                oFile = New System.IO.FileInfo(path)

                Dim oFileStream As System.IO.FileStream = oFile.OpenRead()
                Dim lBytes As Long = oFileStream.Length

                If (lBytes > 0) Then
                    Dim fileData(lBytes - 1) As Byte

                    ' Read the file into a byte array
                    oFileStream.Read(fileData, 0, lBytes)
                    oFileStream.Close()

                    RederFile(fi.Extension, fileData)
                Else

                End If
            Else

            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        Finally

        End Try
    End Sub

    Public Shared Sub RederFile(ByVal inFileExt As String, ByVal fileData() As Byte)
        Try
            Dim FileExt As String = String.Empty
            FileExt = inFileExt.Substring(1)

            HttpContext.Current.Response.ClearContent()
            HttpContext.Current.Response.ClearHeaders()
            HttpContext.Current.Response.AddHeader("content-disposition", "filename=export" & inFileExt)
            HttpContext.Current.Response.ContentType = String.Format("application/{0}", FileExt)
            HttpContext.Current.Response.BinaryWrite(fileData)
            HttpContext.Current.Response.Flush()
            'HttpContext.Current.Response.Close()

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw ex
        End Try
    End Sub
End Class
