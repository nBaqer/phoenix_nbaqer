﻿Partial Class firstPage
    Inherits BasePage

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit

    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles MyBase.Load

        AdvantageSession.PageTheme = PageTheme.Blue_Theme
        Dim ctrl As Control = FindControlRecursive("TextBox1")
        If Not ctrl Is Nothing Then
            Label1.Text = "I have found " & ctrl.ID & "!"
        End If
        TextBox1.Text = [Enum].GetName(GetType(PageTheme), AdvantageSession.PageTheme)
    End Sub

    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        DisplayRADAlert(CallbackType.Postback, "Test Alert", "This is my message", "My Window Title")
    End Sub
    Protected Sub Button2_Click(sender As Object, e As System.EventArgs) Handles Button2.Click
        Dim key As String = "Test Alert"
        Dim msg As String = "This is my other message"
        Dim title As String = "Other Window Title"
        Literal1.Text = String.Format("Key: {0} <br/> Message: {1} <br /> Width: {2}px <br /> Height: {3}px <br /> Title: {4} <br />", key, msg, TextBox2.Text, TextBox3.Text, title)
        DisplayRADAlert(CallbackType.RadAjaxCallback, key, msg, title)
    End Sub


End Class
