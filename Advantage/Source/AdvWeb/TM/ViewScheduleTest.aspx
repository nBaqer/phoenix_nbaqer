﻿<%--<%@ Reference Control="~/UserControls/Header_1.ascx" %>--%>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewScheduleTest.aspx.vb" Inherits="TM_ViewScheduleTest" %>
<%--<%@ Register TagPrefix="fame" TagName="header" Src="~/UserControls/Header_1.ascx" %>--%>
<%@ Register TagPrefix="fame" TagName="footer" Src="../UserControls/Footer.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

</head>
<body>
    <form id="form1" runat="server">
    <div>
      <telerik:RadScriptManager runat="Server" ID="RadScriptManager1" />
            </div>
        <telerik:RadScheduler ID="RadScheduler1"    runat="server" 
        GroupBy="UserName" GroupingDirection="Horizontal"
            DataKeyField="UserTaskID" FirstDayOfWeek="Monday" 
        LastDayOfWeek="Sunday" DataSubjectField="Descrip"
            DataStartField="StartDate" DataEndField="EndDate" 
            SelectedView="DayView" 
            AdvancedForm-EnableCustomAttributeEditing="true"
            CustomAttributeNames = "TaskID,Message"

            OverflowBehavior="Scroll" Height="477px"
            >
  <AppointmentContextMenus>
       <telerik:RadSchedulerContextMenu runat="server" ID="ContextMenu1">
           <Items>
               <telerik:RadMenuItem Text="Edit" Value="CommandEdit" />
               <telerik:RadMenuItem Text="Delete" Value="CommandDelete" />
               </Items>
               </telerik:RadSchedulerContextMenu>
               </AppointmentContextMenus>





         <AppointmentTemplate>
                <%# Eval("Subject") %>
                <p style="font-style: italic;">
                    <%# Eval("Message")%></p>
            </AppointmentTemplate>


              <ResourceHeaderTemplate>
                <asp:Panel ID="ResourceImageWrapper" runat="server" CssClass="ResCustomClass">
                   <asp:Label ID="UserName" runat="server" ToolTip='<%# Eval("Text") %>'  Text='<%# Eval("Text") %>'></asp:Label>
                </asp:Panel>
            </ResourceHeaderTemplate>
               <InlineInsertTemplate>
            <div id="InlineInsertTemplate">
                <div>
                </div>
                <span >
                     <div>
                        <asp:Label ID="lbltask" Text="Task " runat="server" ></asp:Label>
                        </div>
                    <asp:DropDownList ID="Taskddl" runat="server"    DataTextField="Descrip" DataValueField="TaskId"
                        Height="20px" Width="200px"></asp:DropDownList>
                        <div>
                        <asp:Label ID="lblMessage" Text="Message "  runat="server" ></asp:Label>
                        </div>
                             <div>
                        <asp:TextBox ID="txtMessage"   runat="server" ></asp:TextBox>
                        </div>
                      <asp:LinkButton ID="InsertButton" runat="server" CommandName="Insert">Insert
                      
                    </asp:LinkButton>
                    <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"> Cancel
                     
                    </asp:LinkButton>
             
                    <asp:LinkButton ID="InsertMoreButton" runat="server" CommandName="More" CssClass="rsAdvancedEditLink">Advanced</asp:LinkButton>
            
                </span>
            </div>
        </InlineInsertTemplate>
          
      
        <AdvancedInsertTemplate >
      
     <div id="qsfexAdvEditWrapper">
                <div id="qsfexAdvEditInnerWrapper" class="technical">
                    <div class="qsfexAdvAppType">
                    </div>
                    <div class="qsfexAdvEditControlWrapper">

                        <asp:Label ID="lbltask" Text="Task " runat="server" ></asp:Label>
                        </div>
                        <div>
                               <asp:DropDownList ID="Taskddl" runat="server"    DataTextField="Descrip" DataValueField="TaskId"
                        Height="20px" Width="300px"></asp:DropDownList>
                        </div>
                             <div>
                        <asp:Label ID="lblMessage" Text="Message "  runat="server" ></asp:Label>
                        </div>
                             <div>
                        <asp:TextBox ID="txtMessage" Rows="5" Columns="50"  TextMode="MultiLine"  runat="server" ></asp:TextBox>
                        </div>
                           <div class="qsfexAdvEditControlWrapper">


                        <asp:Label ID="Label5" AssociatedControlID="StartInput" runat="server" CssClass="inline-label">Start time:</asp:Label>
                   <telerik:RadDateInput ID="StartInput"  SelectedDate='<%# Bind("Start") %>' runat="server">
                        </telerik:RadDateInput> 
                        
                        <br />
                    </div>
                    <div class="qsfexAdvEditControlWrapper">
                        <asp:Label ID="Label6" AssociatedControlID="EndInput" runat="server" CssClass="inline-label">End time:</asp:Label>
                        <telerik:RadDateInput ID="EndInput" SelectedDate='<%# Bind("End") %>' runat="server">
                        </telerik:RadDateInput><br />
                    </div>
                      <div class="qsfexAdvEditControlWrapper" style="text-align: left;">
                        <asp:LinkButton ID="InsertButton" Text="Insert" runat="server" CommandName="Insert">
                          </asp:LinkButton>
<asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" Text="Cancel" CommandName="Cancel"
Style="margin-left: 8px;">
                           </asp:LinkButton>
                    </div>

                        </div>
        </AdvancedInsertTemplate>

           <AdvancedEditTemplate>
            <div id="qsfexAdvEditWrapper">
                <div id="qsfexAdvEditInnerWrapper" >
                    <div class="qsfexAdvAppType">
                    </div>
                      <div class="qsfexAdvEditControlWrapper">

                        <asp:Label ID="lbltask" Text="Task " runat="server" ></asp:Label>
                        </div>
                        <div>
                               <asp:DropDownList ID="Taskddl" runat="server"   DataTextField="Descrip" DataValueField="TaskId"
                        Height="20px" Width="300px"> </asp:DropDownList>
                        </div>
                             <div>
                        <asp:Label ID="lblMessage" Text="Message "  runat="server" ></asp:Label>
                        </div>
                             <div>
                        <asp:TextBox ID="txtMessage"  Rows="5" Columns="50" TextMode="MultiLine"  runat="server" ></asp:TextBox>
                        </div>


                    <div class="qsfexAdvEditControlWrapper">
                        <asp:Label ID="Label2" AssociatedControlID="StartInput" runat="server" CssClass="inline-label">Start time:</asp:Label>
                        <telerik:RadDateInput ID="StartInput" SelectedDate='<%# Bind("Start") %>' runat="server">
                        </telerik:RadDateInput><br />
                    </div>
                    <div>
                      <telerik:RadTimePicker ID="RadTimePr1" runat="server" ZIndex="30001" />
                    </div>
                                       
                    <div class="qsfexAdvEditControlWrapper">
                        <asp:Label ID="Label3" AssociatedControlID="EndInput" runat="server" CssClass="inline-label">End time:</asp:Label>
                        <telerik:RadDateTimePicker ID="EndInput" SelectedDate='<%# Bind("End") %>'  ZIndex="200000" runat="server">
                        </telerik:RadDateTimePicker><br />
                    </div>
                               
                   <div>
                      <telerik:RadComboBox ID="RadComboBox1"   runat="server">
                        <ItemTemplate>
                        <div>
                           <telerik:RadTimePicker ID="RadTimePicker1"  runat="server" ZIndex="300001" />
                        </div>
                        </ItemTemplate>
                        </telerik:RadComboBox>
                    </div>

                    <div>

         
        </div>

      
                   <div class="qsfexAdvEditControlWrapper" style="text-align: right;">
                        <asp:LinkButton ID="UpdateButton" runat="server" CommandName="Update">
                       Update </asp:LinkButton>
<asp:LinkButton ID="UpdateCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"
Style="margin-right: 8px;">
                          Cancel</asp:LinkButton>
                    </div>
                </div>
            </div>
        </AdvancedEditTemplate>




            <ResourceTypes>
                <telerik:ResourceType KeyField="UserID" Name="UserName" TextField="UserName" ForeignKeyField="UserID"
                  />
            </ResourceTypes>


    </telerik:RadScheduler>
    </form>
</body>
</html>

