' ===============================================================================
' FilterUserTasks
'   Allows the user to filter ViewUserTasks
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports FAME.AdvantageV1.BusinessFacade.TM

Partial Class AdvantageApp_TM_FilterUserTasks
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TMCommon.AdvInit(Me)

        If Not Page.IsPostBack Then
            TMCommon.BuildPriorityDLL(ddlPriority)
            ' need to add an "All" priority filter
            ddlPriority.Items.Insert(0, (New ListItem("All", "")))
            ddlPriority.SelectedIndex = 0

            TMCommon.BuildTaskStatusDDL(ddlStatus)
            TMCommon.BuildAssignedToDDL(TMCommon.GetCurrentUserId(), TMCommon.GetCampusID(), ddlAssignedTo)
            Try
                ddlAssignedTo.SelectedValue = TMCommon.GetCurrentUserId()
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            TMCommon.BuildModulesDDL(TMCommon.GetCurrentUserId(), ddlModule)

            ' add javascript for the contact lookup handlers
            Dim sjs As String = TMCommon.GetContactLookupJS("txtContactName", "hfReType", "hfReId", True)
            lbReLookup.Attributes.Add("onclick", sjs)
            ibReLookup.Attributes.Add("onclick", sjs)

            ' put javascript handler on the ok button
            ' The ok button return a string with this format:
            '       staus|retype|reid|assignedtoid|entityid
            Dim js As New StringBuilder
            js.Append("window.returnValue=document.getElementById('ddlStatus').value + '|' + ")
            js.Append("document.getElementById('ddlPriority').value + '|' + ")
            js.Append("document.getElementById('hfReId').value + '|' + ")
            js.Append("document.getElementById('ddlAssignedTo').value + '|' + ")
            js.Append("document.getElementById('ddlEntity').value;" & vbCrLf)
            js.Append("window.close();")
            btnOK.Attributes.Add("onclick", js.ToString)

            ' put javascript handler on the cancel button
            btnCancel.Attributes.Add("onclick", "window.returnValue=''; window.close();")
        End If
    End Sub

    ''' <summary>
    ''' Show the entity for the selected module
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModule.SelectedIndexChanged
        If ddlModule.SelectedIndex <> 0 Then
            TMCommon.BuildModuleEntitiesDDL(ddlModule.SelectedValue, ddlEntity)
        End If
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        ddlPriority.SelectedIndex = 0
        TMCommon.BuildTaskStatusDDL(ddlStatus)
        Try
            ddlAssignedTo.SelectedValue = TMCommon.GetCurrentUserId()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        ddlModule.SelectedIndex = 0
        Me.txtContactName.Text = ""
        Me.txtRecipientId.Value = ""
        Me.hfReId.Value = ""
        Me.hfReType.Value = ""
        Me.ddlStatus.SelectedIndex = 0
        Me.ddlPriority.SelectedIndex = 0
        Me.ddlEntity.Items.Clear()        
    End Sub
End Class
