' ===============================================================================
' AddUserTask
' Add a new user task or edits an existing user task.
'   To call page in edit mode, pass the taskid as a parameters.  
'   Ex: AddTask.aspx?usertaskid=12345-123-234234
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data
Imports FAME.AdvantageV1.Common.TM
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports Telerik.Web.UI
Imports System.Drawing

Partial Class AdvantageApp_TM_AddUserTask
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        TMCommon.AdvInit(Me)

        If Not IsPostBack Then
            If Request.UrlReferrer.ToString().IndexOf("AddUserTask.aspx") = -1 Then
                ViewState("refurl") = Request.UrlReferrer.ToString()
            End If
            ' record what params were passed to the page            
            ViewState("usertaskid") = Request.Params("usertaskid")
            ' load the form with its default values
            ResetForm()
            ' add javascript for the contact lookup handlers
            Dim js As String = TMCommon.GetContactLookupJS("txtContact", "hfReType", "hfReId", False)
          
            ' add javascript for the cancel button
            js = "var s=confirm('Cancel this task assignment?'); document.getElementById('hfResult').value=s;"
            btnCancel.Attributes.Add("onclick", js)

            ' Bind the task to the form
            Dim UserTaskId As String = Request.Params("usertaskid")
            If Not UserTaskId Is Nothing Then
                lblHeader.Text = "Edit Task Assignment" ' change the header if we are editing a task assignment
                BindUserTask(UserTaskId)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Loads the form to its initial state
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetForm()
        hfReType.Value = ""
        hfReId.Value = ""

        txtStartDate.Text = Date.Now.ToString("MM/dd/yyyy")
        ddlStartHour.SelectedValue = "12"
        ddlEndHour.SelectedValue = "12"

        TMCommon.BuildModulesDDL(TMCommon.GetCurrentUserId(), ddlModules)
        TMCommon.BuildTasksForUserDDL(TMCommon.GetCurrentUserId(), TMCommon.GetCampusID(), Nothing, ddlTasks)
        TMCommon.BuildPriorityDLL(ddlPriority)
        TMCommon.BuildAssignedToDDL(TMCommon.GetCurrentUserId(), TMCommon.GetCampusID(), ddlAssignedTo)
        Try
            ddlAssignedTo.SelectedValue = TMCommon.GetCurrentUserId()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

    ''' <summary>
    ''' User has changed the module.  Only show the tasks avaialable for the selected module
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlModules_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModules.SelectedIndexChanged
        ' If ddlModules.SelectedIndex = 0 Or ddlModules.SelectedIndex = 1 Then
        If ddlModules.SelectedIndex = 0 Then
            TMCommon.BuildTasksForUserDDL(TMCommon.GetCurrentUserId(), TMCommon.GetCampusID(), Nothing, ddlTasks)
        Else
            TMCommon.BuildTasksForUserDDL(TMCommon.GetCurrentUserId(), TMCommon.GetCampusID(), ddlModules.SelectedValue, ddlTasks)
            EntityList.SelectedValue = MapEntityToModuleSelection(ddlModules.SelectedValue)
        End If
    End Sub
    Private Function MapEntityToModuleSelection(ByVal ModuleId As integer) As String 
        Select Case ModuleId
            Case 189  'Admissions - Lead
                Return "Lead"
            Case 26, 191, 194 'Academics, FA, SA - Student
                Return "Student"
            Case 192  'HR - Employees
                Return "Employees"
            Case 193 'Placement - Employer
                Return "Employer"
        End Select
    End Function
    Public Sub lookupEntity_OnClick(ByVal sender As Object, ByVal e As EventArgs) Handles lookupEntity.Click
        If String.IsNullOrEmpty(txtSearch.Text) Then
            TMCommon.DisplayErrorMessage(Page, "Please enter the First name or Last name of the Contact Lookup")
            txtSearch.BackColor = Color.Yellow
        Else
            txtSearch.BackColor = Color.White 
            Dim ds As DataSet = ContactSearchFacade.RecipientSearch(EntityList.SelectedValue, txtSearch.Text, TMCommon.GetCampusID())
            rgEntities.DataSource = ds
            rgEntities.DataBind()
            tr2.Visible = True
        End If




    End Sub

    Public Sub rgEntities_ItemCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles rgEntities.ItemCommand
        'need to put there here. The navigation paging controls will generate an ItemCommand, 
        'we don't want any trouble
        If e.CommandName = "Select" And TypeOf e.Item Is GridDataItem Then

            'requires that the referenced field be in the DataKeyValues in the grid
            Dim item As GridDataItem = (DirectCast(e.Item, GridDataItem))

            Dim recId As String = item.GetDataKeyValue("RecipientId").ToString()
            Dim whole As String = item.GetDataKeyValue("Name").ToString()
            Dim index =  whole.IndexOf("-", StringComparison.Ordinal)
            If  index > 0
                whole = whole.Remove(index, (whole.Length - index ))
            End If
            Dim name As String = whole 'item.GetDataKeyValue("Name").ToString()

            hfReType.Value = EntityList.SelectedValue
            hfReId.Value = recId
            txtContact.Text =  item.GetDataKeyValue("Name").ToString()
            lbEntity.Text = name


            tr2.Visible = False


        End If


    End Sub

    ''' <summary>
    ''' Binds a UserTaskInfo object to the form
    ''' </summary>
    ''' <param name="UserTaskId"></param>
    ''' <remarks></remarks>
    Protected Sub BindUserTask(ByVal UserTaskId As String)
        Try
            Dim utInfo As UserTaskInfo = UserTasksFacade.GetUserTaskInfo(UserTaskId)
            ddlModules.SelectedValue = utInfo.ModuleId
            ddlPriority.SelectedValue = utInfo.Priority
            EntityList.SelectedValue = utInfo.EntityName

            'BEN - 10/27/06 - Force the dropdown to have this task name in it
            ' so that change can be made.  This fixes the problem if the task
            ' has become inactive.
            ddlTasks.Items.Clear()
            ddlTasks.Items.Add("---Select---") ' dummy item so that task will save correctly
            ddlTasks.Items.Add(New ListItem(utInfo.TaskDescrip, utInfo.TaskId))
            ddlTasks.SelectedValue = utInfo.TaskId

            ddlAssignedTo.SelectedValue = utInfo.OwnerId
            lblAssignedBy.Text = utInfo.AssignedByName

            hfReType.Value = utInfo.EntityName
            hfReId.Value = utInfo.ReId
            txtContact.Text = utInfo.ReName
            lbEntity.Text = utInfo.EntityName

            Dim tmpDate As DateTime = CType(utInfo.StartDate, DateTime)
            If utInfo.StartDate > DateTime.MinValue Then
                txtStartDate.Text = tmpDate.ToShortDateString()
            End If
            ddlStartMin.SelectedValue = tmpDate.Minute.ToString()
            If tmpDate.ToShortTimeString().IndexOf("PM") > 0 Then
                ddlStartHour.SelectedValue = (tmpDate.Hour - 12).ToString()
                ddlStartAMPM.SelectedValue = "PM"
            Else
                ddlStartHour.SelectedValue = tmpDate.Hour.ToString()
                ddlStartAMPM.SelectedValue = "AM"
            End If

            tmpDate = CType(utInfo.EndDate, DateTime)
            If utInfo.EndDate > DateTime.MinValue Then
                txtEndDate.Text = tmpDate.ToShortDateString()
            End If
            ddlEndMin.SelectedValue = tmpDate.Minute.ToString()
            If tmpDate.ToShortTimeString().IndexOf("PM") > 0 Then
                ddlEndHour.SelectedValue = (tmpDate.Hour - 12).ToString()
                ddlEndAMPM.SelectedValue = "PM"
            Else
                ddlEndHour.SelectedValue = tmpDate.Hour.ToString()
                ddlEndAMPM.SelectedValue = "AM"
            End If

            txtMessage.Text = utInfo.Message
            lblCategory.Text = utInfo.CategoryDescrip
            'utInfo.WorkflowId 

            TMCommon.BuildAvialableResultsDDL(utInfo.TaskId, ddlResult)
            lblStatus.Text = utInfo.StatusString
            ddlResult.SelectedValue = utInfo.ResultId

            ' if the status is completed, the user cannot change
            ' the status or result again
            If utInfo.Status = TaskStatus.Completed Then
                ddlResult.Enabled = False
            End If

            ' enable the Cancel button it the owner is the current user
            If utInfo.OwnerId.ToUpper() = TMCommon.GetCurrentUserId().ToUpper() AndAlso _
               utInfo.Status = TaskStatus.Pending Then
                btnCancel.Visible = True
            End If
            If (ddlResult.SelectedValue <> "") Then
                btnOk.Enabled = False
            End If
            ' display the extra controls for editing the task
            SetEditMode()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    Protected Sub SetEditMode()
        ' because the task has already been defined, we cannot change the contact
        ' so disable the lookup button
        

        ' Show controls associated with editing a task
        ' I.e., show Status and Result prompts
        btnOk.Text = "Save Changes"
        ddlModules.Enabled = False ' user cannot change the module after it has been set
        ddlTasks.Enabled = False ' User cannot change the task after it has been set
        lblStatusPrompt.Visible = True
        lblStatus.Visible = True
        lblResult.Visible = True
        ddlResult.Visible = True
        lblAssignedByPrompt.Visible = True
        lblAssignedBy.Visible = True
        lblCategoryPrompt.Visible = True
        lblCategory.Visible = True
        txtContact.Enabled = False ' user cannot change the contact name after it has been set
    End Sub

    ''' <summary>
    ''' Checks the form's contents for validity.  If any control is
    ''' not valid, an alert is made and the function returns false.
    ''' 
    ''' If we are in edit mode (UserTaskId was passed to the page as a parameter).
    ''' and the status is set to "Complete" then the user must select a Result.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ValidateForm() As Boolean
        ' Check the task drop down
        If Me.ddlTasks.SelectedIndex = 0 Then
            TMCommon.Alert(Me, "Task must be selected.")
            Return False
        End If

        If Me.txtContact.Text.Length = 0 Then
            TMCommon.Alert(Me, "Contact must be entered.")
            Return False
        End If

        If Me.hfReId.Value.Length = 0 Then
            TMCommon.Alert(Me, "Invalid contact.  Please use the Lookup button to enter a valid contact.")
            Return False
        End If

        Dim dtTmp As DateTime
        Dim dtTmp2 As DateTime
        Dim tmpStr As String
        ' Check the start date
        ' 8/21/06: Ben - Start date is now required
        If txtStartDate.Text.Length = 0 Then
            TMCommon.Alert(Me, "Start date must be entered.")
            Return False
        End If
        If txtEndDate.Text.Length = 0 Then
            TMCommon.Alert(Me, "End date must be entered.")
            Return False
        End If

        Try
            tmpStr = String.Format("{0} {1}:{2} {3}", txtStartDate.Text, _
                    ddlStartHour.SelectedValue, ddlStartMin.SelectedValue, ddlStartAMPM.SelectedValue)
            If txtStartDate.Text.Length > 0 Then dtTmp = txtStartDate.Text
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.Alert(Me, "Invalid start date.")
            Return False
        End Try
        ' Check the end date
        Try
            tmpStr = String.Format("{0} {1}:{2} {3}", txtEndDate.Text, _
                    ddlEndHour.SelectedValue, ddlEndMin.SelectedValue, ddlEndAMPM.SelectedValue)
            If txtEndDate.Text.Length > 0 Then dtTmp = txtEndDate.Text
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.Alert(Me, "Invalid end date.")
            Return False
        End Try
        ' check that the start date is less than the end date
        Try
            dtTmp = DateTime.MinValue ' dtTmp will hold the startdate
            dtTmp2 = DateTime.MaxValue ' dtTmp2 will hold the enddate
            tmpStr = String.Format("{0} {1}:{2} {3}", txtStartDate.Text, _
                    ddlStartHour.SelectedValue, ddlStartMin.SelectedValue, ddlStartAMPM.SelectedValue)
            If txtStartDate.Text.Length > 0 Then dtTmp = tmpStr

            tmpStr = String.Format("{0} {1}:{2} {3}", txtEndDate.Text, _
                    ddlEndHour.SelectedValue, ddlEndMin.SelectedValue, ddlEndAMPM.SelectedValue)
            If txtEndDate.Text.Length > 0 Then dtTmp2 = tmpStr
            If dtTmp2 <= dtTmp Then
                TMCommon.Alert(Me, "End date must be greater than the start date.")
                Return False
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try

        Return True
    End Function

    ''' <summary>
    ''' Add or edit a usertask info to the database
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Try
            If Not ValidateForm() Then Return

            Dim refer As String = ViewState("refurl")
            Dim usertaskid As String = ViewState("usertaskid")

            Dim utInfo As New UserTaskInfo
            ' determine if this is an update or a new task by looking
            ' at the ViewState("usertaskid") which is set by passing a parameter to 
            ' the page.
            If usertaskid Is Nothing Or usertaskid = "" Then
                utInfo.IsInDB = False
            Else
                utInfo.IsInDB = True
                utInfo.UserTaskId = usertaskid
            End If
            utInfo.AssignedById = TMCommon.GetCurrentUserId()

            ' record the end date
            If txtEndDate.Text.Length > 0 Then
                utInfo.EndDate = String.Format("{0} {1}:{2} {3}", txtEndDate.Text, _
                    ddlEndHour.SelectedValue, ddlEndMin.SelectedValue, ddlEndAMPM.SelectedValue)
            Else
                utInfo.EndDate = Nothing
            End If

            ' record the start date
            If txtStartDate.Text.Length > 0 Then
                utInfo.StartDate = String.Format("{0} {1}:{2} {3}", txtStartDate.Text, _
                    ddlStartHour.SelectedValue, ddlStartMin.SelectedValue, ddlStartAMPM.SelectedValue)
            Else
                utInfo.StartDate = Nothing
            End If

            utInfo.Message = txtMessage.Text
            utInfo.OwnerId = ddlAssignedTo.SelectedValue
            utInfo.Priority = ddlPriority.SelectedValue
            utInfo.ReId = hfReId.Value
            ' if the user set the result without setting the status
            ' then we just set the status to completed.
            If ddlResult.SelectedValue <> "" Then
                utInfo.Status = TaskStatus.Completed
            End If
            utInfo.ResultId = ddlResult.SelectedValue
            utInfo.TaskId = ddlTasks.SelectedValue

            ' Update the task and check the result
            If Not UserTasksFacade.UpdateUserTask(utInfo, TMCommon.GetCurrentUserId()) Then
                TMCommon.Alert(Page, "The update/add operation failed")
                Return
            End If

            ' go back to the previous page or indicate that the task was created
            If refer Is Nothing Or refer = "" Then
                ResetForm()
                TMCommon.Alert(Page, "Task was added successfully")
            Else
                Response.Redirect(refer)
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' If called, user want to cancel the task
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If Not hfResult.Value Is Nothing AndAlso hfResult.Value = "true" Then
            Dim usertaskid As String = ViewState("usertaskid")
            Dim utInfo As UserTaskInfo = UserTasksFacade.GetUserTaskInfo(usertaskid)
            utInfo.Status = TaskStatus.Cancelled
            UserTasksFacade.UpdateUserTask(utInfo, TMCommon.GetCurrentUserId())

            Dim refer As String = ViewState("refurl")
            If refer Is Nothing Or refer = "" Then
                ResetForm()
            Else
                Response.Redirect(refer)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Discard any changes and just close the form and go back to the referrer
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Dim refer As String = ViewState("refurl")
        If refer Is Nothing Or refer = "" Then
            ResetForm()
        Else
            Response.Redirect(refer)
        End If
    End Sub

    ''' <summary>
    ''' Enable/Disable the buttons for Contact Lookup because we can only do lookups
    ''' after the user has chosen a task.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlTasks_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTasks.SelectedIndexChanged
        If ddlTasks.SelectedIndex > 0 Then
            

            Dim TaskId As String = ddlTasks.SelectedValue
            Dim info As TaskInfo = TasksFacade.GetTaskInfo(TaskId)

            ' check if the entity names of the newly seclected task is different
            ' If they are different, then we need to clear out the contact information
            ' and force them to lookup another one
            If info.EntityName <> hfReType.Value Then
                Me.hfReId.Value = ""
                ' set the retype so when a contact search is done, it knows which group to look at
                Me.hfReType.Value = info.EntityName
                Me.txtContact.Text = ""
                lbEntity.Text = info.EntityName
            End If
        Else
            ' invalid task selected, hide the buttons for selecting a contact
            ' and clear out the contact name
            
            hfReId.Value = ""
            hfReType.Value = ""
            txtContact.Text = "'"
            lbEntity.Text = ""
        End If
    End Sub

  
End Class
