<%@ Control Language="VB" ClassName="CalendarMenu2" %>
<%@ Import Namespace="FAME.AdvantageV1.BusinessFacade" %>


<style type="text/css">
    .style1
    {
        width: 100%;
    }
</style>


<script runat="server">    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Page.IsPostBack Then
            'build the predefined filters
            Dim filter As String = Request.Params("f")
            Dim boolFrontDesk As Boolean
            If Trim(Session("UserName")).ToLower = "sa" Then
                boolFrontDesk = True
                ShowLInkButtons(boolFrontDesk)
                BuildFiltersDDL(ddlFilters, boolFrontDesk)
            Else
                boolFrontDesk = (New RolesFacade).CheckIfUserIsMappedToFrontDesk(Trim(Session("UserId")))
                ShowLInkButtons(boolFrontDesk)
                BuildFiltersDDL(ddlFilters, boolFrontDesk)
            End If
            'If (filter Is Nothing Or filter = "" Or filter = "tasks assigned to me") Then
            '    ddlFilters.SelectedValue = "tasks assigned to me"
            'ElseIf (Not filter Is Nothing Or Not filter = "") And boolFrontDesk = True Then
            '    ddlFilters.SelectedValue = "tasks assigned to others by me"
            'End If
          '  Dim UserCount As Integer
                      
            ' Dim ViewSetting As String
            ' Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
            'ViewSetting = tm.GetUserTaskCalendarDefaultView(Session("UserId"))
           
            'For i As Integer = 0 To ddlFilters.Items.Count - 1
            '    If ddlFilters.Items(i).Text = ViewSetting Then
            '        ddlFilters.SelectedIndex = ddlFilters.Items(i).Index
            '    End If
            'Next
           
            
            
            '  lblDefView.Text = ViewSetting
            'UserCount = tm.GetUserTaskCalendarDefaultView_usersCount(Session("UserId"))
            'If UserCount > 0 Then
            '    lnkViewOtherUsers.Text = lnkViewOtherUsers.Text + "(" + UserCount.ToString + ")"
            'End If
        End If
    End Sub
    'Protected Sub ddlFilters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim url As String = Page.AppRelativeVirtualPath + "?f=" + ddlFilters.SelectedValue
    '    Response.Redirect(url)
    'End Sub
    Protected Sub BuildFiltersDDL(ByRef ddl As Telerik.Web.UI.RadComboBox, ByVal UserIsFrontDesk As Boolean)
        'Dim item As New RadComboBoxItem()
        'item.Text = "--Select Filter--"
        'item.Value = " "
        'ddl.Items.Add(item)
        
        Dim item1 As New RadComboBoxItem()
        item1.Text = "Tasks assigned to me"
        item1.Value = "tasks assigned to me"
        ddl.Items.Add(item1)
        
   
        If UserIsFrontDesk = True Then
            Dim item2 As New RadComboBoxItem()
            item2.Text = "Tasks assigned to others"
            item2.Value = "tasks assigned to others"
            ddl.Items.Add(item2)
        
            Dim item3 As New RadComboBoxItem()
            item3.Text = "Tasks assigned to others by me"
            item3.Value = "tasks assigned to others by me"
            ddl.Items.Add(item3)
            lnkViewOtherUsers.Visible = True
        End If
        
   
        
        
    End Sub
    Protected Sub ShowLInkButtons(ByVal boolFrontDesk As Boolean)
        'If boolFrontDesk = True Then
            
        '    lnkotherstasks.Visible = True
        '    lnkotherstasks.PostBackUrl = "Userlist_PopUp.aspx?TaskCategory=1"
            
        '    lnkotherstskbyme.Visible = True
        '    lnkotherstskbyme.PostBackUrl = "Userlist_PopUp.aspx?TaskCategory=2"
        'End If
        
    End Sub
        
    Protected Sub btnmytask_Click(sender As Object, e As System.EventArgs)
        Dim url As String = Page.AppRelativeVirtualPath + "?f=tasks assigned to me"
        Response.Redirect(url)
    End Sub

    Protected Sub ddlFilters_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        Dim url As String = Page.AppRelativeVirtualPath + "?f=" + ddlFilters.SelectedValue
        Response.Redirect(url)
    End Sub

</script>

<div style="margin: 0; padding: 0; background-color: #BFCCD9;" align="center">

    <table class="style1">
        <tr>
            <td>

    <asp:LinkButton ID="lnknewtask" Font-Names="arial,sans-serif" Font-Size="Small" PostBackUrl="AddUserTask.aspx" Text="New Task"  runat="server"></asp:LinkButton>
            </td>
            <td>
                <asp:Label ID="lblDefView" ForeColor="Blue" runat="server" Text=""></asp:Label>
                      
              <asp:LinkButton ID="btnmytask"  Text="My tasks"  runat="server" Visible="false" OnClick="btnmytask_Click"></asp:LinkButton></td>
            <td>
                <asp:Label ID="Label1" runat="server" Font-Names="arial,sans-serif" Font-Size="Small" Text="Search for Tasks"></asp:Label>
             <telerik:RadComboBox ID="ddlFilters" runat="server" Width="195px" EmptyMessage="--Select Filter--"  AutoPostBack="True" Visible="True" OnSelectedIndexChanged="ddlFilters_SelectedIndexChanged">
    </telerik:RadComboBox >
                 <asp:LinkButton ID="lnkotherstasks"  Text="Others tasks"  runat="server" Visible="false" ></asp:LinkButton></td>
            <td>
               <asp:LinkButton ID="lnkotherstskbyme" Text="Others tasks assigned by me"  runat="server" Visible="false"></asp:LinkButton>
                 <asp:LinkButton ID="lnkViewOtherUsers" Text="My Calendar Settings" Font-Names="arial,sans-serif" Font-Size="Small"  runat="server" Visible="false" PostBackUrl="Userlist_PopUp.aspx?TaskCategory=1"></asp:LinkButton></td>
        </tr>
    </table>
 
   

     </div>
