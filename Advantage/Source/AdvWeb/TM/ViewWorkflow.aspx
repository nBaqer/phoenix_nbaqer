<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewWorkflow.aspx.vb" Inherits="TM_ViewWorkflow" %>

<%@ Register TagName="SysHeader" TagPrefix="Header" Src="~/usercontrols/TMHeader.ascx" %>
<%@ Register TagName="TaskMenu" TagPrefix="Menu" Src="Menu_Tasks.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>View Workflow</title>
    <link href="../css/TM.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: White">
    <form id="form1" runat="server">
        <div>
            <table width="100%" summary="View Tasks" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left">
                        <Menu:TaskMenu ID="taskMenu" runat="server" />
                    </td>
                </tr>
                <tr>
                   <td style="padding: 10px">
                        <asp:Label ID="lblTasksMsgs" runat="server" CssClass="MsgLabel" Text="No workflows defined. "
                            Visible="False" /></td>
                </tr>
            </table>
            <div id="scrolldiv" runat="server" class="ScrolledDiv">
                <div class="Div">
                    <asp:Repeater ID="rptWorkflows" runat="server">
                        <HeaderTemplate>
                            <table width="100%" cellpadding="4" cellspacing="0" class="Repeater">
                                <tr class="RepeaterHeader">
                                    <td align="left" class="RepeaterHeaderLabel">
                                        Contact</td>
                                    <td align="left" class="RepeaterHeaderLabel">
                                        Previous Task</td>
                                    <td align="left" class="RepeaterHeaderLabel">
                                        Result</td>
                                    <td align="left" class="RepeaterHeaderLabel">
                                        Current Task</td>
                                    <td align="left" class="RepeaterHeaderLabel">
                                        Status</td>
                                    <td>
                                    </td>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr bgcolor="white" onmouseover="this.style.backgroundColor='lightblue'" onmouseout="this.style.backgroundColor='white'">
                                <td align="left" class="Label">
                                    <asp:Label ID="lblContact" runat="server" Text='<%# Container.DataItem("ReName") %>' />
                                </td>
                                <td align="left" class="Label">
                                    <asp:HyperLink ID="hlPrevTask" runat="server" Text='<%# Container.DataItem("PrevTaskDescrip") %>'
                                        NavigateUrl='<%# "AddUserTask.aspx?UserTaskId=" & Container.DataItem("PrevUserTaskId").ToString() %>' />
                                </td>
                                <td align="left" class="Label">
                                    <%#Container.DataItem("PrevTaskResultDescrip")%>
                                </td>
                                <td align="left" class="Label">
                                    <asp:HyperLink ID="hlCurTask" runat="server" Text='<%# Container.DataItem("TaskDescrip") %>'
                                        NavigateUrl='<%# "AddUserTask.aspx?UserTaskId=" & Container.DataItem("UserTaskId").ToString() %>' />
                                </td>
                                <td align="left" class="Label">
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Container.DataItem("Status") %>' />
                                </td>
                                <td align="left" class="Label">
                                    <asp:HyperLink ID="hlView" runat="server" Text="Graphical View" Target="_blank" NavigateUrl='<%# "ViewWorkflowPopup.aspx?taskid=" & Container.DataItem("PrevTaskId").ToString() %>' />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <SeparatorTemplate>
                            <tr class="RepeaterSeparator" />
                        </SeparatorTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <asp:HiddenField ID="hfResult" runat="server" />
            <asp:HiddenField ID="filterReType" runat="server" />
            <asp:HiddenField ID="filterReId" runat="server" />
            <asp:HiddenField ID="filterPriority" runat="server" />
            <asp:HiddenField ID="filterAssignedToId" runat="server" />
            <asp:HiddenField ID="filterStatus" runat="server" />
            <asp:HiddenField ID="filterEntityId" runat="server" />
            <asp:HiddenField ID="filterDueDateMin" runat="server" />
            <asp:HiddenField ID="filterDueDateMax" runat="server" />
        </div>
    </form>
</body>
</html>
