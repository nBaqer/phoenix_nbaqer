' ===============================================================================
' ViewCalendar_Weekly
' Displays a calendar in a weekly view
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data
Imports FAME.AdvantageV1.Common.TM
Imports FAME.AdvantageV1.BusinessFacade.TM

Partial Class TM_ViewCalendar
    Inherits System.Web.UI.Page

    Private tabIndex As Long = 0
    Private dSunday As DateTime = #1/1/1900#
    Private dMonday As DateTime = #1/1/1900#
    Private dTuesday As DateTime = #1/1/1900#
    Private dWednesday As DateTime = #1/1/1900#
    Private dThursday As DateTime = #1/1/1900#
    Private dFriday As DateTime = #1/1/1900#
    Private dSaturday As DateTime = #1/1/1900#

    Private calendarEvents As New ArrayList
    Private SubCalendars As New ArrayList

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim dDate As DateTime = Date.Today 'SelectedDate()
            Dim filter As String = ""

            Try
                filter = Request.QueryString("f")
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                filter = "task assigned to me"
            End Try

            If filter = "" Or filter = Nothing Then
                filter = "task assigned to me"
            End If

            If filter.ToString.ToLower = "tasks assigned to others" Then
                ' get all the tasks for this user and save it in the viewstate
                Dim ds As DataSet = UserTasksFacade.GetUserTasks(TMCommon.GetCampusID(), Nothing, Nothing, _
                                    Nothing, Nothing, TaskStatus.None, Nothing, Nothing, "others", TMCommon.GetCurrentUserId())
                ViewState("usertasks") = ds
                CalculateWeekDays(dDate)
                BindPage()
            Else
                Dim ds As DataSet = UserTasksFacade.GetUserTasks(TMCommon.GetCampusID(), Nothing, TMCommon.GetCurrentUserId(), _
                                    Nothing, Nothing, TaskStatus.None, Nothing, Nothing)
                ViewState("usertasks") = ds
                CalculateWeekDays(dDate)
                BindPage()
            End If

        End If
    End Sub

#Region "Helpers"
    ''' <summary>
    ''' Returns either "StartDate" or "EndDate" depending on the selection
    ''' made in Menu_Calendar->ddlFilter
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function GetFilter() As String
        If Session("filter") Is Nothing Or Session("filter") <> "2" Then
            Return "StartDate"
        End If
        Return "EndDate"
    End Function

    Private Sub BindPage()
        Dim oTime As DateTime = CType("12:00 AM", DateTime)
        Dim oRow As DataRow
        Dim i As Integer
        Dim NewModules As New ArrayList
        Dim dBegin, dEnd As Date
        Try
            With grdWeekView.Columns
                .Item(0).HeaderText = "Time:"
                lblWeekOf.Text = String.Format("Week {0} of {1}", DatePart(DateInterval.WeekOfYear, dMonday), dMonday.ToLongDateString)
                ViewState("WeekOf") = dMonday.ToShortDateString
                .Item(1).Visible = False
                .Item(2).HeaderText = dMonday.ToString("ddd") & " <br>" & dMonday.Day
                .Item(3).HeaderText = dTuesday.ToString("ddd") & " <br>" & dTuesday.Day
                .Item(4).HeaderText = dWednesday.ToString("ddd") & " <br>" & dWednesday.Day
                .Item(5).HeaderText = dThursday.ToString("ddd") & " <br>" & dThursday.Day
                .Item(6).HeaderText = dFriday.ToString("ddd") & " <br>" & dFriday.Day
                .Item(7).HeaderText = dSaturday.ToString("ddd") & " <br>" & dSaturday.Day
                .Item(8).HeaderText = dSunday.ToString("ddd") & " <br>" & dSunday.Day
                .Item(8).Visible = True
                dBegin = dMonday
                dEnd = dSunday
            End With

            ' See If we have any New Sub-Master Modules
            If NewModules.Count > 0 Then
                SubCalendars.AddRange(NewModules)
            End If

            'set up columns in the results table
            Dim oTable As DataTable = New DataTable("RESULTS")
            oTable.Columns.Add("EventTime", GetType(System.String))
            oTable.Columns.Add("Sunday", GetType(System.String))
            oTable.Columns.Add("Monday", GetType(System.String))
            oTable.Columns.Add("Tuesday", GetType(System.String))
            oTable.Columns.Add("Wednesday", GetType(System.String))
            oTable.Columns.Add("Thursday", GetType(System.String))
            oTable.Columns.Add("Friday", GetType(System.String))
            oTable.Columns.Add("Saturday", GetType(System.String))

            For i = 0 To 23
                oRow = oTable.NewRow()
                oRow("EventTime") = oTime.AddHours(i).ToShortTimeString
                oRow("Sunday") = GetItemForDate(calendarEvents, dSunday, CType(oRow("EventTime"), DateTime))
                oRow("Monday") = GetItemForDate(calendarEvents, dMonday, CType(oRow("EventTime"), DateTime))
                oRow("Tuesday") = GetItemForDate(calendarEvents, dTuesday, CType(oRow("EventTime"), DateTime))
                oRow("Wednesday") = GetItemForDate(calendarEvents, dWednesday, CType(oRow("EventTime"), DateTime))
                oRow("Thursday") = GetItemForDate(calendarEvents, dThursday, CType(oRow("EventTime"), DateTime))
                oRow("Friday") = GetItemForDate(calendarEvents, dFriday, CType(oRow("EventTime"), DateTime))
                oRow("Saturday") = GetItemForDate(calendarEvents, dSaturday, CType(oRow("EventTime"), DateTime))
                oTable.Rows.Add(oRow)
            Next

            grdWeekView.DataSource = oTable
            grdWeekView.DataBind()
        Catch
        End Try
    End Sub

    Function GetItemForDate(ByVal loadedEvents As ArrayList, ByVal dDate As DateTime, ByVal dTime As DateTime) As String
        Dim sTemp As String = ""
        'get selected date events
        Dim ds As DataSet = ViewState("usertasks")
        If ds Is Nothing Then Return ""

        Dim cellStr As String = "<div><a class=Label href='AddUserTask.aspx?UserTaskId={0}' title='{1}' />{2}</a></div>"


        ' set the real datetime object as a composite of dDate and dTime
        dDate = New DateTime(dDate.Year, dDate.Month, dDate.Day, dTime.Hour, dTime.Minute, dTime.Second)
        Dim sql As String = String.Format("{0}>='{1}' and {0}<'{2}'", GetFilter(), dDate.ToString(), dDate.AddMinutes(30).ToString())
        Dim drs As DataRow() = ds.Tables(0).Select(sql)
        Dim dr As DataRow
        For Each dr In drs
            ' 7/29/06 - Ben: Added a tooltip to each day's link
            Dim strStatus As String = ""
            If dr("Status") = TaskStatus.Cancelled Then
                strStatus = "Cancelled"
            ElseIf dr("Status") = TaskStatus.Completed Then
                strStatus = "Completed"
            ElseIf dr("Status") = TaskStatus.Pending Then
                strStatus = "Pending"
            End If
            Dim tooltip As String = String.Format("{0} with {2} ({1})", dr("TaskDescrip"), strStatus, dr("ReName"))
            cellStr = dr("OwnerName") & ":" + cellStr
            sTemp += String.Format(cellStr, dr(0).ToString, tooltip, dr(2).ToString)            
        Next

        Return sTemp

    End Function

    Sub CalculateWeekDays(ByVal dRequestDate As DateTime)
        Dim dWeekStart As Date
        Dim nDayOff As Integer

        'If (Culture.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Sunday) Then
        'nDayOff = 0
        'Else
        nDayOff = 1
        'End If

        Select Case dRequestDate.DayOfWeek
            Case DayOfWeek.Sunday
                'If (Culture.DateTimeFormat.FirstDayOfWeek = DayOfWeek.Sunday) Then
                'dWeekStart = dRequestDate
                'Else
                dWeekStart = dRequestDate.AddDays(-6)
                'End If
            Case DayOfWeek.Monday
                dWeekStart = dRequestDate.AddDays(-DayOfWeek.Monday + nDayOff)
            Case DayOfWeek.Tuesday
                dWeekStart = dRequestDate.AddDays(-DayOfWeek.Tuesday + nDayOff)
            Case DayOfWeek.Wednesday
                dWeekStart = dRequestDate.AddDays(-DayOfWeek.Wednesday + nDayOff)
            Case DayOfWeek.Thursday
                dWeekStart = dRequestDate.AddDays(-DayOfWeek.Thursday + nDayOff)
            Case DayOfWeek.Friday
                dWeekStart = dRequestDate.AddDays(-DayOfWeek.Friday + nDayOff)
            Case DayOfWeek.Saturday
                dWeekStart = dRequestDate.AddDays(-DayOfWeek.Saturday + nDayOff)
        End Select
        dMonday = dWeekStart
        dTuesday = dWeekStart.AddDays(1)
        dWednesday = dWeekStart.AddDays(2)
        dThursday = dWeekStart.AddDays(3)
        dFriday = dWeekStart.AddDays(4)
        dSaturday = dWeekStart.AddDays(5)
        dSunday = dWeekStart.AddDays(6)
        'End If
    End Sub
#End Region

#Region "Links and Buttons"
    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        Dim dDate As DateTime = CType(ViewState("WeekOf"), DateTime)
        CalculateWeekDays(dDate.AddDays(8))
        BindPage()
    End Sub

    Private Sub lnkPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrev.Click
        Dim dDate As DateTime = CType(ViewState("WeekOf"), DateTime)
        CalculateWeekDays(dDate.AddDays(-1))
        BindPage()
    End Sub
#End Region

End Class
