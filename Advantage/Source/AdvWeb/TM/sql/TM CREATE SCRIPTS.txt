/****** Object:  Table [dbo].[syModuleEntities]    Script Date: 02/23/2006 17:27:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[syModuleEntities](
	[EntityId] [uniqueidentifier] ROWGUIDCOL  NOT NULL CONSTRAINT [DF_syModuleEntities_EntityId]  DEFAULT (newid()),
	[ModuleId] [tinyint] NULL,
	[Descrip] [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
 CONSTRAINT [PK_syModuleEntities] PRIMARY KEY CLUSTERED 
(
	[EntityId] ASC
) ON [PRIMARY]
) ON [PRIMARY]
