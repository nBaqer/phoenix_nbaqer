<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewUserTaskHistory.aspx.vb"
    Inherits="TM_ViewUserTaskHistory" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<head runat="server">
    <title>View User Task History</title>
    <link href="../CSS/localhost.css" type="text/css" rel="stylesheet">
    <link href="../CSS/systememail.css" type="text/css" rel="stylesheet">
    <link href="../css/TM.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="pragma" content="NO-CACHE">
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <img src="../images/task_history.jpg"></td>
                <td class="topemail">
                    <a class="close" onclick="top.close()" href=#>X Close</a></td>
            </tr>
        </table>
        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="DetailsFrameTopemail">
                    <div class="scrollwholetask">
                        <asp:Repeater ID="rptHistory" runat="server">
                            <HeaderTemplate>
                                <table width="100%" cellpadding="4" cellspacing="0" class="Repeater">
                                    <tr class="RepeaterHeader">
                                        <td align="left" class="RepeaterHeaderLabel" style="width: 30%">
                                            Date started</td>
                                        <td align="left" class="RepeaterHeaderLabel" style="width: 30%">
                                            Task name</td>
                                        <td align="left" class="RepeaterHeaderLabel" style="width: 20%">
                                            Status</td>
                                        <td align="left" class="RepeaterHeaderLabel" style="width: 20%">
                                            Result</td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr bgcolor="white" onmouseover="this.style.backgroundColor='#e7edf3'" onmouseout="this.style.backgroundColor='white'">
                                    <td class="RepeateritemLabel">
                                        <%#Container.DataItem("StartDate")%>
                                    </td>
                                    <td class="RepeateritemLabel">
                                        <%#Container.DataItem("TaskDescrip")%>
                                    </td>
                                    <td class="RepeateritemLabel">
                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Container.DataItem("Status") %>' />
                                    </td>
                                    <td class="RepeateritemLabel">
                                        <asp:Label ID="Label1" runat="server" Text='<%# Container.DataItem("ResultDescrip") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <SeparatorTemplate>
                                <tr class="RepeaterSeparator" />
                            </SeparatorTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
        </table>
      <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
    </form>
</body>
</html>

