' ===============================================================================
' SetupResults
' Handles adding and editing results
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' 11/2/06 (BEN) - Added support for specifying a start gap of auto created tasks

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common.TM
Imports FAME.AdvantageV1.BusinessFacade
'Imports FAME.AdvantageV1.Common
Imports Fame.AdvantageV1.Common
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class AdvantageApp_TM_SetupResults
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim fac As New UserSecurityFacade
        Dim m_Context As HttpContext

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Session("UserId") = userId
        Session("UserName") = AdvantageSession.UserState.UserName.ToString
        Session("cmpid") = campusId
        Session("resid") = txtResourceId.Text.Trim
        TMCommon.AdvInit(Me)

        If Not Page.IsPostBack Then
            InitButtonsForLoad()
            'Header1.EnableHistoryButton(False)
            ' load the form with its default values
            ResetForm()
            ' display the results defined in the system on the left pane
            BindResults()
            ' Bind the task to the form
            Dim ResultId As String = Request.Params("resultid")
            If Not ResultId Is Nothing Then
                BindResult(ResultId)
            End If
        Else
            InitButtonsForEdit()
        End If
        headerTitle.Text = Header.Title
    End Sub

#Region "Advantage Permissions"
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnnew.Enabled = False
        End If

        btndelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If
    End Sub
#End Region

#Region "Bind Methods"
    ''' <summary>
    ''' Binds the results to the datalist on the left pane.
    ''' Depending on the state of the status selector, we show active, inactive or all results.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindResults()
        Try
            If radStatus.SelectedIndex = 0 Then
                dlResults.DataSource = ResultsFacade.GetResults(True, False)
            ElseIf radStatus.SelectedIndex = 1 Then
                dlResults.DataSource = ResultsFacade.GetResults(False, True)
            Else
                dlResults.DataSource = ResultsFacade.GetResults(True, True)
            End If
            dlResults.DataBind()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Fills the form with a Result given a ResultId
    ''' </summary>
    ''' <param name="ResultId"></param>
    ''' <remarks></remarks>
    Protected Sub BindResult(ByVal ResultId As String)
        Try
            ViewState("id") = ResultId ' save for general use
            Dim info As Fame.AdvantageV1.Common.TM.ResultInfo = ResultsFacade.GetResultInfo(ResultId)
            ddlActive.SelectedValue = info.Active.ToString()
            txtCode.Text = info.Code
            txtName.Text = info.Descrip
            ViewState("moddate") = info.ModDate ' need this to do a proper delete

            ' display the other action values
            Try
                ddlActions.SelectedValue = info.ResultActionId
                If Not info.ResultActionId Is Nothing AndAlso info.ResultActionId <> "" Then
                    TMCommon.BuildResultValuesDDL(ddlActions.SelectedValue, ddlActionValues)
                    ddlActionValues.SelectedValue = info.ResultActionValue
                Else
                    ddlActionValues.Items.Clear()
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            BindNextTasks(ResultId)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

#Region "Next Tasks"
    ''' <summary>
    ''' Binds the next tasks for this result
    ''' 11/1/06 - BEN: Changed lbNextTasks to a Repeater to support
    ''' entry of the start gap time
    ''' </summary>
    ''' <param name="ResultId"></param>
    ''' <remarks></remarks>
    Protected Sub BindNextTasks(ByVal ResultId As String)
        Try
            If ViewState("NextTasks") Is Nothing Then
                ViewState("NextTasks") = ResultTasksFacade.GetNextTasks(ResultId)
            End If
            rptNextTasks.DataSource = ViewState("NextTasks")
            rptNextTasks.DataBind()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Called for each item in the repeater.  Allows us to bind the list ov available tasks
    ''' shown on the header.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rptNextTasks_ItemDataBound(ByVal sender As System.Object, ByVal e As RepeaterItemEventArgs) Handles rptNextTasks.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then
                Dim ddlTasks As DropDownList = e.Item.FindControl("ddlTasks")
                ddlTasks.DataSource = TasksFacade.GetTasks(Nothing, Nothing, Nothing, True, False)
                ddlTasks.DataTextField = "Descrip"
                ddlTasks.DataValueField = "TaskId"
                ddlTasks.DataBind()
                ddlTasks.Items.Insert(0, "---Select---")
                ddlTasks.SelectedIndex = 0

                Dim ds As DataSet = ViewState("NextTasks")
                For Each dr As DataRow In ds.Tables(0).Rows
                    Dim taskId As String = dr("TaskId").ToString()
                    Dim li As ListItem = ddlTasks.Items.FindByValue(taskId)
                    If Not li Is Nothing Then ddlTasks.Items.Remove(li)
                Next
            End If
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim ddl As DropDownList = e.Item.FindControl("ddlEdStartGapUnit")
                ddl.SelectedValue = e.Item.DataItem("StartGapUnit")
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    ''' <summary>
    ''' Responds to the user Adding or Deleting an "Auto Created Task"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rptNextTasks_ItemCommand(ByVal sender As System.Object, ByVal e As RepeaterCommandEventArgs) Handles rptNextTasks.ItemCommand
        Try
            ' delete the selected row from the list of Auto Created Tasks
            If e.CommandName = "Del" Then
                Dim ds As DataSet = ViewState("NextTasks")
                If Not ds Is Nothing Then
                    Dim drs() As DataRow = ds.Tables(0).Select(String.Format("TaskId='{0}'", e.CommandArgument))
                    For Each dr As DataRow In drs
                        dr.Delete()
                    Next
                    ds.Tables(0).AcceptChanges()
                    ' refresh the list of auto created tasks
                    Dim resultid As String = ViewState("id")
                    BindNextTasks(resultid)
                End If
            ElseIf e.CommandName = "Add" Then
                Dim ds As DataSet = ViewState("NextTasks")
                If Not ds Is Nothing Then
                    Dim dr As DataRow = ds.Tables(0).NewRow()
                    Dim ddlTasks As DropDownList = CType(e.Item.FindControl("ddlTasks"), DropDownList)
                    ' check that the user selected a valid task and display an error message if not
                    If ddlTasks.SelectedIndex = 0 Then
                        TMCommon.DisplayErrorMessage(Me, "Please select a task for clicking Add")
                        Return
                    End If
                    dr("TaskId") = ddlTasks.SelectedValue
                    dr("TaskDescrip") = ddlTasks.SelectedItem.Text
                    dr("StartGap") = CType(e.Item.FindControl("txtStartGap"), TextBox).Text
                    dr("StartGapUnit") = CType(e.Item.FindControl("ddlStartGapUnit"), DropDownList).SelectedValue
                    ds.Tables(0).Rows.Add(dr)
                    ' refresh the list of auto created tasks
                    Dim resultid As String = ViewState("id")
                    BindNextTasks(resultid)
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub
#End Region
#End Region

    ''' <summary>
    ''' Event handler for when the user clicks on the left datalist containing all the users.
    ''' In response, this handler should populate the info for a task
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlResults_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlResults.ItemCommand
        ' get the selected resultid and store it in the viewstate object
        Dim resultid As String = e.CommandArgument.ToString
        dlResults.SelectedIndex = e.Item.ItemIndex

        ' do some error checking
        If resultid Is Nothing Or resultid = "" Then
            TMCommon.Alert(Me, "There was a problem displaying the details for this result.")
            Return
        End If

        ' bind the result to the form
        ResetForm()
        BindResult(resultid)

        ' Advantage specific stuff
        'CommonWebUtilities.SetStyleToSelectedItem(dlResults, resultid, ViewState, Header1)
        InitButtonsForEdit()
        CommonWebUtilities.RestoreItemValues(Me.dlResults, resultid)
    End Sub

    ''' <summary>
    ''' User wants a specific action (like update Lead Status) to happen
    ''' when this result is chosen.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlActions.SelectedIndexChanged
        If ddlActions.SelectedIndex = 0 Then
            ddlActionValues.Items.Clear()
        Else
            TMCommon.BuildResultValuesDDL(ddlActions.SelectedValue, ddlActionValues)
        End If
    End Sub

    ''' <summary>
    ''' Checks the form's contents for validity.  If any control is
    ''' not valid, an alert is made and the function returns false.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ValidateForm() As Boolean
        ' Check the task drop down
        If Me.txtCode.Text.Length = 0 Then
            TMCommon.Alert(Me, "Code is required.")
            Return False
        End If

        If txtCode.Text.Length > 12 Then
            TMCommon.DisplayErrorMessage(Me, "Code cannot be greater than 12 characters.")
            Return False
        End If

        If Me.txtName.Text.Length = 0 Then
            TMCommon.Alert(Me, "Description is required.")
            Return False
        End If

        If ddlActions.SelectedIndex > 0 AndAlso ddlActionValues.SelectedIndex = 0 Then
            TMCommon.Alert(Me, "Action Value is required when an Action has been selected.")
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' Adds or updates a Result
    ''' Each result is has one or more next tasks associated with it.  These
    ''' next tasks are stored in tmResultTasks.  It maps a tmResults.ResultId
    ''' to one or more TaskId(s).  
    ''' So, after adding or udpating the result, we have to look at the next tasks
    ''' which are store in lbNextTasks and update that too.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            ' check that the user has entered in all the right values
            If Not ValidateForm() Then Return

            Dim resultid As String = ViewState("id")
            Dim info As New FAME.AdvantageV1.Common.TM.ResultInfo
            ' determine if this is an update or a new task by looking
            ' at the ViewState("id") which is set by passing a parameter to the page.
            If resultid Is Nothing Or resultid = "" Then
                info.IsInDB = False
                ViewState("id") = info.ResultId
            Else
                info.IsInDB = True
                info.ResultId = resultid
            End If
            info.Active = CType(ddlActive.SelectedValue, Boolean)
            info.Code = txtCode.Text
            info.Descrip = txtName.Text
            info.ResultActionId = ddlActions.SelectedValue
            info.ResultActionValue = ddlActionValues.SelectedValue
            ' do the add or update 
            ResultsFacade.UpdateResult(info, TMCommon.GetCurrentUserId())

            ' Update the Actions that get auto-generated as a result of this task'
            ' See description of method for more info.
            ' start by clearing all next tasks for this result
            ResultTasksFacade.DeleteAllByResultId(info.ResultId)
            Dim opOk As Boolean = True
            Dim i As Integer = 0
            For Each ri As RepeaterItem In rptNextTasks.Items
                Dim rtInfo As New ResultTaskInfo
                rtInfo.ResultId = info.ResultId
                rtInfo.TaskId = CType(ri.FindControl("hfTaskId"), HiddenField).Value
                rtInfo.StartGap = CInt(CType(ri.FindControl("txtEdStartGap"), TextBox).Text)
                rtInfo.StartGapUnit = CType(ri.FindControl("ddlEdStartGapUnit"), DropDownList).SelectedValue
                rtInfo.TaskOrder = i
                i += 1
                If Not ResultTasksFacade.UpdateNextTask(rtInfo, TMCommon.GetCurrentUserId()) Then
                    opOk = False
                End If
            Next

            If opOk Then
                TMCommon.Alert(Page, "Result was saved successfully")
            Else
                TMCommon.Alert(Page, "Errors ocurred while saving the result")
            End If

            ' update the left pane with the new set of tasks
            BindResults()

            ' Advantage specific stuff
            'CommonWebUtilities.SetStyleToSelectedItem(dlResults, resultid, ViewState, Header1)
            InitButtonsForEdit()
            CommonWebUtilities.RestoreItemValues(Me.dlResults, resultid)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Resets the form to the intial state.  All controls that are databound are initialized
    ''' from their datasource.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetForm()
        ' clear out the form        
        txtCode.Text = ""
        txtName.Text = ""

        ViewState("NextTasks") = Nothing
        rptNextTasks.DataSource = Nothing
        rptNextTasks.DataBind()

        TMCommon.BuildResultActionsDDL(ddlActions)
        ddlActionValues.Items.Clear()

        TMCommon.BuildStatusDDL(ddlActive)
        ddlActive.SelectedValue = True
    End Sub


    ''' <summary>
    ''' Simply reset the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ViewState("id") = Nothing
        ViewState("moddate") = Nothing
        ResetForm()
        ' Advantage specific stuff
        ' CommonWebUtilities.SetStyleToSelectedItem(Me.dlResults, Guid.Empty.ToString, ViewState, Header1)
        InitButtonsForLoad()
        CommonWebUtilities.RestoreItemValues(Me.dlResults, Guid.Empty.ToString)
    End Sub

    ''' <summary>
    ''' Delete the result
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim id As String = ViewState("id")
            If Not id Is Nothing AndAlso id <> "" Then
                Dim modDate As DateTime = ViewState("moddate")
                Dim res As String = ResultsFacade.DeleteResult(id, modDate)
                If res <> "" Then
                    TMCommon.DisplayErrorMessage(Me, res)
                Else
                    ' since the result was deleted, delete all the dependent ResultTasks
                    ResultTasksFacade.DeleteAllByResultId(id)

                    ViewState("id") = Nothing
                    ResetForm()
                    ' update the left pane
                    BindResults()

                    ' Advantage specific stuff
                    'CommonWebUtilities.SetStyleToSelectedItem(Me.dlResults, Guid.Empty.ToString, ViewState, Header1)
                    InitButtonsForLoad()
                End If
                CommonWebUtilities.RestoreItemValues(Me.dlResults, Guid.Empty.ToString)
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Me, "Error.  Record could not be deleted.")
        End Try
    End Sub

    ''' <summary>
    ''' Display active, inactive or all based on the user checking the radio box option
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        BindResults()
        ResetForm()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
