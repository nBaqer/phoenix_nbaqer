﻿<%--<%@ Reference Control="~/UserControls/Header_1.ascx" %>--%>
<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewSchedule.aspx.vb" Inherits="TM_ViewSchedule" %>
<%--<%@ Register TagPrefix="fame" TagName="header" Src="~/UserControls/Header_1.ascx" %>--%>
<%@ Register TagPrefix="fame" TagName="footer" Src="../UserControls/Footer.ascx" %>
<%@ Register TagName="CalendarMenu" TagPrefix="Menu" Src="Menu_Calendar2.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <style type="text/css">
 .rsAdvancedModal
        {
            z-index:100000 !important;
        }
       </style>
       <style type="text/css" media="print">
    #div1
    {
       display: none;
    }
    </style>
        <style type="text/css">  
        #Label1Panel { display: inline !important; }
        
        .lblError {
            background: #CC0000;
            border: 1px solid darkred;
            color: white;
            font: bold 16px "Segoe UI", Arial, sans-serif;
            vertical-align: middle;
            display: none;
            height: 30px;
            line-height: 28px;
            position: absolute;
            padding-left: 5px;
            width: 743px;
            z-index: 1001;
            cursor: pointer;
            -ms-filter: progid:DXImageTransform.Microsoft.Alpha(style=0,opacity=90) !important;
            opacity: 0.9;
            -moz-opacity: 0.9;
            filter: alpha(opacity=90);
        }
            
</style>
</head>

<body>
    <form id="form1" runat="server">
        <div>
          <asp:Panel runat="server" ID="Testpanel1">
		<menu:CalendarMenu ID="sysMenu" runat="server" />
        </asp:Panel>
		</div>
        <br />

    <div>
    <telerik:RadSkinManager ID="RadSkinManager1" runat="server" >
    </telerik:RadSkinManager>
 <telerik:RadWindowManager runat="server" id="RadWindowManager1" CssClass="radConfirmWindow"  ></telerik:RadWindowManager>
      <telerik:RadScriptManager runat="Server" ID="RadScriptManager1" />
         <script type="text/javascript">
        function changeEndDate(sender, e)
        {    
            var endDatePickerID = sender.get_id().replace("StartInput", "EndInput");
            var endDatePicker = $find(endDatePickerID);

            var endTime = new Date(sender.get_selectedDate());
            endTime.setMinutes(endTime.getMinutes() + 60);

         //   if (endDatePicker.get_selectedDate() <= sender.get_selectedDate()) {
                endDatePicker.set_selectedDate(endTime);
         //   }

        }


        function changeStartDate(sender, e) {
            var startDatePickerID = sender.get_id().replace("EndInput", "StartInput");
            var startDatePicker = $find(endDatePickerID);

            var startTime = new Date(sender.get_selectedDate());
            startTime.setMinutes(startTime.getMinutes() - 60);

        //    if (startDatePicker.get_selectedDate() >= sender.get_selectedDate()) {
                startDatePicker.set_selectedDate(startTime);
        //    }

            }

                  function onAppointmentInserting(sender, args) {
                      var slot = args.get_targetSlot();
                      var start = slot.get_startTime();
                      var end = slot.get_endTime();

                      if (isRoomOccupied(sender, start, end, slot)) {
                          //   alert("Another appointment occurs in this time slot");
                          var r = top.confirm("Another appointment occurs in this time slot");
                          if (r == true) {
                              //  args.set_cancel(true);
                          }
                          else {
                              args.set_cancel(true);
                          }
                          //  args.set_cancel(true);
                      }
                      var today = new Date();
                      if (start < today || end < today) {
                          //   alert("Another appointment occurs in this time slot");
                          var r = top.confirm("This appointment occurs in the past");
                          if (r == true) {
                              //  args.set_cancel(true);
                          }
                          else {
                              args.set_cancel(true);
                          }
                          //  args.set_cancel(true);
                      }
            }

            function onAppointmentResizeEnd(sender, args) {
                var start = args.get_appointment().get_start();
                var end = args.get_targetSlot().get_endTime();


                warnIfOccupied(start, end, sender, args);
            }


            function onAppointmentMoveEnd(sender, args) {
                var start = args.get_targetSlot().get_startTime();
                var end = new Date(start.getTime() + args.get_appointment().get_duration());

                warnIfOccupied(start, end, sender, args);
            
            }

                      function warnIfinPast(start, end, sender, args) {
                var slot = args.get_targetSlot();
                var appointment = args.get_appointment();

                //                if (isUserOccupied(sender, start, end, appointment)) {
                //                    alert("Another appointment occurs in this time slot");
                //                  // args.set_cancel(true);
                //                }
                //                else
                var today = new Date();
                if (start < today || end < today ) {
                    //   alert("Another appointment occurs in this time slot");
                    var r = top.confirm("This appointment occurs in the past");
                    if (r == true) {
                        //  args.set_cancel(true);
                    }
                    else {
                        args.set_cancel(true);
                    }
                    //  args.set_cancel(true);
                }
                appointment.get_element().style.border = "";
            }


            function warnIfOccupied(start, end, sender, args) {
                var slot = args.get_targetSlot();
                var appointment = args.get_appointment();

//                if (isUserOccupied(sender, start, end, appointment)) {
//                    alert("Another appointment occurs in this time slot");
//                  // args.set_cancel(true);
//                }
//                else
                 if (isRoomOccupied(sender, start, end, slot, appointment)) {
                  //   alert("Another appointment occurs in this time slot");
                     var r = top.confirm("Another appointment occurs in this time slot");
                     if (r == true) {
                       //  args.set_cancel(true);
                     }
                     else {
                         args.set_cancel(true);
                     }
                  //  args.set_cancel(true);
                }
                 appointment.get_element().style.border = "";
                 warnIfinPast(start, end, sender, args);
            }

            function isUserOccupied(scheduler, start, end, appointment) {
                //get the "User" resource associated with the appointment
                var currentUser = appointment.get_resources().getResourcesByType("UserName").getResource(0);
                //get all appointments in this period which are associated with this resource
               // alert(appointment.get_resources().getResourcesByType("UserName").getResource(0).toString);
                var appointmentsForThisUser = getAppointmentsInRangeByResource(scheduler, start, end, currentUser, appointment);
                //if the list of appointments is not empty the user has other appointments besides the specified in this time period
              //  alert(appointmentsForThisUser.get_count());
                return appointmentsForThisUser.get_count() > 0;
            }

            function isRoomOccupied(scheduler, start, end, slot, appointment) {
                //get the "Room" resource associated with the time slot
                var currentRoom = slot.get_resource();
                //get all appointments for this "room" in the specified period
                var appointmentsForThisRoom = getAppointmentsInRangeByResource(scheduler, start, end, currentRoom, appointment);
                //if the list of appointments is not empty there are other appointments in this slot
                return appointmentsForThisRoom.get_count() > 0;
            }
            function getAppointmentsInRangeByResource(scheduler, start, end, resource, appointment) {
                //Get all appointments within the specified time period
                var result = scheduler.get_appointments().getAppointmentsInRange(start, end);

                //If specified remove the appointment from the appointment list
                if (appointment)
                    result.remove(appointment);

                //Filter the appointments based on the resource
                return result.findByResource(resource);
            }

            function confirmCallBackFn(arg) {
                radalert("Confirm returned the following result: " + arg);
            }



          </script>

<script type="text/javascript">
    Telerik.Web.UI.RadScheduler.prototype._onDateHeaderClick = function (e) {
        $telerik.cancelRawEvent(e);

        var date = new Date();
        // Date headers have an href in the format #yyyy-MM-dd
        var dateMatch = e.eventMapTarget.href.match(/#(\d{4}-\d{2}-\d{2})/);
        if (dateMatch && dateMatch.length == 2) {
            var parts = $telerik.$.map(dateMatch[1].split("-"), function (part) { return parseInt(part, 10); });
            date = new Date(parts[0], parts[1] - 1, parts[2]);
        }

        return this._switchToSelectedDay(date);
    };
</script>
    </div>
  <!--<div>
  <a href="javascript:void(window.print())" >print this page</a>
  </div> 
   <div style="font: normal 13px arial,sans-serif;margin-top: 10px;color: #b71c1c" align="left">
                        <asp:Literal ID="literalWarningMessage" Text="" runat="server"></asp:Literal>
                    </div>-->
  <div style="text-align:center;">
  <asp:Label ID="lblCAlView" runat="server" Text="" Font-Names="arial,sans-serif" Font-Size="Small" Font-Bold="true"></asp:Label>
    </div>
            <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="RadScheduler1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="RadScheduler1" LoadingPanelID="RadAjaxLoadingPanel1" />
                        <telerik:AjaxUpdatedControl ControlID="Label1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <asp:Label ID="Label1" runat="server" CssClass="lblError"></asp:Label>
    <telerik:RadScheduler ID="RadScheduler1"    runat="server" 
        GroupBy="UserName" GroupingDirection="Horizontal"
            DataKeyField="UserTaskID" FirstDayOfWeek="Sunday" 
        LastDayOfWeek="Saturday" DataSubjectField="Descrip"
            DataStartField="StartDate" DataEndField="EndDate" 
            SelectedView="DayView" 
            AdvancedForm-EnableCustomAttributeEditing="true"
            CustomAttributeNames = "TaskID,Message,Priority,AssignedById,AssignedByName"
            MinimumInlineFormHeight="140"
            MinimumInlineFormWidth="250"
           OverflowBehavior="Scroll" Height="477px"
           OnClientAppointmentMoveEnd="onAppointmentMoveEnd"
           OnClientAppointmentResizeEnd="onAppointmentResizeEnd"
           OnClientAppointmentInserting="onAppointmentInserting"
            >
            <AdvancedForm Modal="true"   />

  <AppointmentContextMenus>
       <telerik:RadSchedulerContextMenu runat="server" ID="ContextMenu1">
           <Items>
               <telerik:RadMenuItem Text="Edit" Value="CommandEdit" />
               <telerik:RadMenuItem Text="Delete" Value="CommandDelete" />
               </Items>
               </telerik:RadSchedulerContextMenu>
               </AppointmentContextMenus>
         <AppointmentTemplate>
         <div>
        <%# Eval("Subject") %> 
        <div style="<%# GetPriorityStyle(Eval("Priority"))%>" >
        Priority:<%# GetPriorityType(Eval("Priority"))%></div>
         </div>
                              <p style="font-style: italic;" >
                    <%# Eval("Message")%></p>
                      <p align="right">by:  <%# Eval("AssignedByName")%></p>
                   
            </AppointmentTemplate>


              <ResourceHeaderTemplate>
                <asp:Panel ID="ResourceImageWrapper" runat="server"  CssClass="ResCustomClass">
                    <div style="<%# HighlightLoggedinUser(Eval("Text"))%>">
                            <asp:Label ID="UserName" runat="server" ToolTip='<%# Eval("Text") %>'  Text='<%# Eval("Text") %>'></asp:Label>
                            </div>
                </asp:Panel>
            </ResourceHeaderTemplate>
               <InlineInsertTemplate>
            <div id="InlineInsertTemplate">
                        <span >
                             <div>
                        <asp:Label ID="lbltask" Text="Task " runat="server" ></asp:Label>
                        </div>
                       <telerik:RadComboBox ID="radtaskddl"  DataTextField="Descrip" DataValueField="TaskID" DropDownWidth="300"   runat="server" >
                     </telerik:RadComboBox>
                        <div>
                        <asp:Label ID="lblMessage" Text="Message "  runat="server" ></asp:Label>
                        </div>
                             <div>
                       <telerik:RadTextBox ID="txtMessage" TextMode="MultiLine" runat="server">
                       </telerik:RadTextBox>
                                 <br />
                        </div>
                    <div>  
                    <br />
                    <table>
                    <tr>
                    <td>
                       <asp:LinkButton ID="InsertButton" runat="server" CommandName="Insert">Insert
                      </asp:LinkButton>  
                    </td>
                    <td>
                         <asp:LinkButton ID="InsertCancelButton" runat="server" CausesValidation="False" CommandName="Cancel"> Cancel
                          </asp:LinkButton> 
                    </td>
                    <td>
                       <asp:LinkButton ID="InsertMoreButton" runat="server" CommandName="More" CssClass="rsAdvancedEditLink">Advanced</asp:LinkButton>
                      </td>

                    </tr>
                    </table>
                               
                    </div> 
                   
                </span>
            </div>
           
        </InlineInsertTemplate>
          
      
        <AdvancedInsertTemplate >
      
    <div class="rsAdvancedEdit rsAdvancedModal" style="position: relative;">
       <div class="rsModalBgTopLeft">
       </div>
       <div class="rsModalBgTopRight">
       </div>
       <div class="rsModalBgBottomLeft">
       </div>
       <div class="rsModalBgBottomRight">
       </div>
          <%-- Title bar --%>
          <div class="rsAdvTitle">
            <%--The rsAdvInnerTitleelement isused asa draghandle whenthe formis modal   --%>        
               
              <h1 class="rsAdvInnerTitle"> <%# Container.Appointment.Owner.Localization.AdvancedNewAppointment%></h1>
           <asp:LinkButton runat="server" ID="AdvancedEditCloseButton" CssClass="rsAdvEditClose"
               CommandName="Cancel" CausesValidation="false" ToolTip='<%# Container.Appointment.Owner.Localization.AdvancedClose %>'>
<%# Container.Appointment.Owner.Localization.AdvancedClose%>
           </asp:LinkButton>
       </div>
    <div class="rsAdvContentWrapper">
               <div style="margin:5px 0px 5px 0px;">
                 <div>
                 <asp:Label ID="lbltask" Text="Task " runat="server" ></asp:Label>
                 </div>
                 <div>
                  <telerik:RadComboBox ID="radtaskddl"  DataTextField="Descrip" DataValueField="TaskID"  ZIndex="200000"  Width="500"  runat="server" >
                     </telerik:RadComboBox>
                   
                 </div>
                 </div>
                   <div style="margin:5px 0px 5px 0px;">
                 <div>
                      <asp:Label ID="Label2" runat="server" >Start time:</asp:Label>
                  </div>
                   <div>
                         <telerik:RadDateTimePicker ID="StartInput" SelectedDate='<%# Bind("Start") %>'  ZIndex="200000" runat="server">
                        </telerik:RadDateTimePicker>            
                    </div>
                    </div>
                      <div style="margin:5px 0px 5px 0px;">
                   <div>
                        <asp:Label ID="Label3" runat="server" >End time:</asp:Label>  
                   </div>
                   <div>
                     <telerik:RadDateTimePicker ID="EndInput" SelectedDate='<%# Bind("End") %>'  ZIndex="200000" runat="server">
                        </telerik:RadDateTimePicker>
                                        
                   </div>
                   </div>
                     <div style="margin:5px 0px 5px 0px;">
                    <div>
                      <asp:Label ID="Label1" Text="Priority " runat="server" ></asp:Label>
                      </div>
                 <div>
                   
                      <telerik:RadComboBox ID="radpriorityddl" ZIndex="200000"   runat="server">
                                            </telerik:RadComboBox>
                 </div>
                 </div>
                   <div style="margin:5px 0px 5px 0px;">
                 <div>
                   <asp:Label ID="lblMessage" Text="Message "  runat="server" ></asp:Label>
                 </div>
                
                  <div>
                     <telerik:RadTextBox ID="txtMessage" TextMode="MultiLine"  runat="server" Width="300px" Height="75px">
                     </telerik:RadTextBox>
                   </div>
                     <div>
                       <br />
                   <br />

                                  <asp:Label runat="server" ID="lblerrmsg" ForeColor="Red"  Text=""></asp:Label>
                                   </div>
                  </div>

                       <asp:Panel runat="server" ID="ButtonsPanel" CssClass="rsAdvancedSubmitArea">
               <div class="rsAdvButtonWrapper">
                   <asp:LinkButton CommandName="Insert" runat="server" ID="InsertButton" CssClass="rsAdvEditSave">
<span><%# Container.Appointment.Owner.Localization.Save%></span>
                   </asp:LinkButton>
                   <asp:LinkButton runat="server" ID="InsertCancelButton" CssClass="rsAdvEditCancel" CommandName="Cancel"
                       CausesValidation="false">
<span><%# Container.Appointment.Owner.Localization.Cancel%></span>
                   </asp:LinkButton>
               </div>
           </asp:Panel>
          </div>
             </div>
        </AdvancedInsertTemplate>

           <AdvancedEditTemplate>
            <div id="qsfexAdvEditWrapper" class="rsAdvancedEdit rsAdvancedModal" style="position: relative">
              <div class="rsModalBgTopLeft">
       </div>
       <div class="rsModalBgTopRight">
       </div>
       <div class="rsModalBgBottomLeft">
       </div>
       <div class="rsModalBgBottomRight">
       </div>
       <%-- Title bar --%>
          <div class="rsAdvTitle">
            <%--The rsAdvInnerTitleelement isused asa draghandle whenthe formis modal   --%>        
               
              <h1 class="rsAdvInnerTitle"> <%# Container.Appointment.Owner.Localization.AdvancedEditAppointment %></h1>
                <asp:LinkButton runat="server" ID="LinkButton1" CssClass="rsAdvEditClose"
               CommandName="Cancel" CausesValidation="false" ToolTip='<%# Container.Appointment.Owner.Localization.AdvancedClose %>'>
<%# Container.Appointment.Owner.Localization.AdvancedClose%>
           </asp:LinkButton>
       </div>

               <div class="rsAdvContentWrapper">
               <div style="margin:5px 0px 5px 0px;">
                 <div>
                 <asp:Label ID="lbltask" Text="Task " runat="server" ></asp:Label>
                 </div>
                 <div>
                   <telerik:RadComboBox ID="radtaskddl"  DataTextField="Descrip" DataValueField="TaskID"  Width="500" ZIndex="200000"    runat="server" >
                   </telerik:RadComboBox>
                 </div>
                 </div>
                   <div style="margin:5px 0px 5px 0px;">
                 <div>
                      <asp:Label ID="Label2" runat="server" >Start time:</asp:Label>
                  </div>
                   <div>
                          <telerik:RadDateTimePicker ID="StartInput" SelectedDate='<%# Bind("Start") %>'  ZIndex="200000" runat="server">
                        </telerik:RadDateTimePicker>                         
                    </div>
                    </div>
                      <div style="margin:5px 0px 5px 0px;">
                   <div>
                        <asp:Label ID="Label3" runat="server" >End time:</asp:Label>  
                   </div>
                   <div>
                       <telerik:RadDateTimePicker ID="EndInput" SelectedDate='<%# Bind("End") %>'  ZIndex="200000" runat="server">
                        </telerik:RadDateTimePicker>
                   </div>
                   </div>
                     <div style="margin:5px 0px 5px 0px;">
                    <div>
                      <asp:Label ID="Label1" Text="Priority " runat="server" ></asp:Label>
                      </div>
                 <div>
                      <telerik:RadComboBox ID="radpriorityddl" ZIndex="200000"   runat="server">
                                            </telerik:RadComboBox>
                 </div>
                 </div>
                   <div style="margin:5px 0px 5px 0px;">
                 <div>
                   <asp:Label ID="lblMessage" Text="Message "  runat="server" ></asp:Label>
                 </div>
                
                  <div>
                     <telerik:RadTextBox ID="txtMessage" TextMode="MultiLine"  runat="server" Width="300px" Height="75px">
                     </telerik:RadTextBox>
                   </div>
                   <div>
                   <br />
                   <br />
                   <asp:Label runat="server" ID="lblerrmsg" Text=""  ForeColor="Red" ></asp:Label></div>
                                   </div>
                                   
                    <asp:Panel runat="server" ID="Panel1" CssClass="rsAdvancedSubmitArea">
               <div class="rsAdvButtonWrapper">
                   <asp:LinkButton CommandName="Update" runat="server" ID="UpdateButton"  CssClass="rsAdvEditSave">
<span><%# Container.Appointment.Owner.Localization.Save%></span>
                   </asp:LinkButton>
                   <asp:LinkButton runat="server" ID="UpdateCancelButton" CssClass="rsAdvEditCancel" CommandName="Cancel"
                       CausesValidation="false">
<span><%# Container.Appointment.Owner.Localization.Cancel%></span>
                   </asp:LinkButton>
               </div>
           </asp:Panel>
           </div>
           </div>
  </AdvancedEditTemplate>
            <ResourceTypes>
                <telerik:ResourceType KeyField="UserID" Name="UserName" TextField="FullName" ForeignKeyField="UserID"
                  />
            </ResourceTypes>


    </telerik:RadScheduler>

     <p>
            <telerik:RadButton ID="RadButton1" runat="server" Text="Print Calendar" OnClick="RadButton1_Click">
                          </telerik:RadButton>
        </p>
    </form>
</body>
</html>
