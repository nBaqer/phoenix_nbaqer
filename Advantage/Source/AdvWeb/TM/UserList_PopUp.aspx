﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserList_PopUp.aspx.vb" Inherits="TM_UserList_PopUp" %>

<%--<%@ Register TagPrefix="fame" TagName="fame" Src="~/UserControls/Header_1.ascx" %>--%>
<%@ Register TagPrefix="custom" Namespace="FilteringTemplateColumns" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <link rel="stylesheet" type="text/css" media="all" href="jscalendar/calendar-blue2.css"
        title="blue2" />
            <link href="../CSS/localhost.css" type="text/css" rel="stylesheet"/>

        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

   <style type="text/css"> 
label1 {font: normal 12px arial,sans-serif;} 
</style>

    </telerik:RadCodeBlock>
    <style type="text/css">
        .style1
        {
            width: 246px;
        }
        .style2
        {
            width: 148px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    <!-- content start -->
      <telerik:RadSkinManager ID="RadSkinManager1" runat="server" >
    </telerik:RadSkinManager>
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" />
         

       <div style="margin: 2px 0 20px;margin-left:5px;"  >
       <table>
       <tr>
       <td class="style2">
         <div style="font: normal 13px arial,sans-serif;margin-top: 12px;" align="left">
       <asp:Label ID="label3" runat="server"  Font-Bold="true" Text="Filter users" />
       </div>
       </td>
       </tr>
       <tr>
       <td align="left" class="style2">
       <div style="font: normal 13px arial,sans-serif;margin-top: 5px;" align="left">
            <asp:Label ID="comboLbl" runat="server"  Font-Names="arial,sans-serif" Font-Size="small" Text="User Roles " />
</div>
       </td>
       <td align="left">
     
                  <telerik:RadComboBox ID="ddlroles" runat="server"
                DataTextField="Role" DataValueField="RoleId" Width="250px" Height="150px"
                AppendDataBoundItems="true" CheckBoxes="True" EmptyMessage="--Select Filter--">
                     </telerik:RadComboBox>

       </td>
      
  
        <td align="left">
  
       <asp:Label ID="Label2" runat="server" Font-Names="arial,sans-serif" Font-Size="small" Visible="false"  Text="Campus" />
      
       </td>
       <td class="style1">
        <div align="left" > 
            <telerik:RadComboBox ID="ddlcampusDesc" runat="server" 
                DataTextField="CampDescrip" DataValueField="CampDescrip" Width="250px" Height="150px"
                AppendDataBoundItems="true" CheckBoxes="True" Visible="false"  EmptyMessage="--Select Filter--">
                           </telerik:RadComboBox>
            </div>
       </td>
       </tr>
       <tr>
       <td align="left" class="style2">

           <asp:Label ID="lblFullname" Font-Names="arial,sans-serif" Font-Size="small" runat="server" Text="User Name" />
       </td>
       <td align="left">
           <asp:TextBox ID="txtFullName" runat="server"></asp:TextBox>
       </td>
      
  
        <td>
            &nbsp;</td>
       <td align="left" class="style1">
           &nbsp;</td>
       </tr>
       <tr>
       <td class="style2" align="left">
           <asp:Button ID="btnFilter"  runat="server" Text="Filter" 
               Width="59px" />
           </td>
       <td>
              &nbsp;</td>
      
  
        <td>
            &nbsp;</td>
       <td class="style1">
           &nbsp;</td>
       </tr>
       </table>
        <div style="font: normal 13px arial,sans-serif;margin-top: 12px;" align="left">
            <asp:Label ID="label1" runat="server"  Font-Bold="true" Text="Select users to view their tasks (12 max)" />
        </div>
       </div>
               <div style="margin: 5px 0 20px;">
            <telerik:RadGrid ID="dgUsers" AllowPaging="true"  runat="server" 
                GridLines="None" Width="97%" AllowSorting="true" AllowRowSelect="true" AllowMultiRowSelection="true"  >
            <SortingSettings EnableSkinSortStyles="false"/>
                            <HeaderContextMenu EnableAutoScroll="True">
                            </HeaderContextMenu>
                            <GroupingSettings CaseSensitive="false" />
                            <MasterTableView DataKeyNames="UserID" AutoGenerateColumns="False" AllowMultiColumnSorting="false" AllowFilteringByColumn="False">
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </ExpandCollapseColumn>
                                <NoRecordsTemplate>
                                    <div style="height: 60px; cursor: pointer;">
                                        No Records Selected</div>
                                </NoRecordsTemplate>
                                <Columns>
               <telerik:GridButtonColumn
       UniqueName="SelectColumn"
       CommandName="Select"
       Text="Select" />
     <telerik:GridButtonColumn
       UniqueName="DeselectColumn"
       CommandName="Deselect"
       Text="Clear" />
                                                 <telerik:GridBoundColumn DataField="FullName" HeaderText="User Name" HeaderStyle-HorizontalAlign="Center" SortExpression="FullName"
                                        UniqueName="FullName" HeaderTooltip="Sort by user name" ShowSortIcon="true" CurrentFilterFunction="Contains" ShowFilterIcon="false"   AutoPostBackOnFilter="true">
                                    </telerik:GridBoundColumn>                                    
                                     <telerik:GridBoundColumn DataField="UserID" HeaderText="UserID" SortExpression="UserID"
                                        UniqueName="UserID" Visible="false">
                                    </telerik:GridBoundColumn>  
                                       <custom:MyCustomFilteringColumn DataField="CampDescrip" FilterControlWidth="180px" HeaderStyle-HorizontalAlign="Center" Visible="false"  HeaderText="Campus Name">
                        <headerstyle width="25%" />
                        <itemtemplate>
                          <%# Eval("CampDescrip")%>
                        </itemtemplate>
                    </custom:MyCustomFilteringColumn>
                                              
                                   
                                 </Columns>
                                <PagerStyle Mode="NumericPages"/>
                            </MasterTableView>
                            <ClientSettings AllowRowsDragDrop="True">
                                <Selecting AllowRowSelect="True" EnableDragToSelectRows="false" />        
                                                         </ClientSettings>
                            <PagerStyle Mode="NumericPages"/>
           
           
            </telerik:RadGrid>
        </div>
    </div>
    <div style="margin-top: 12px;margin-left:5px;" align="left">
    <asp:Label ID="lblDefaultSetting" CssClass="label" Font-Names="arial,sans-serif" Font-Size="Small"  runat="server" Text="Default Calendar View" />
               <telerik:RadComboBox ID="ddlFilters" runat="server" Width="195px" EmptyMessage="--Select Filter--"  >
                   <Items>
                       <telerik:RadComboBoxItem runat="server" Text="Tasks assigned to me" 
                           Value="Tasks assigned to me" />
                       <telerik:RadComboBoxItem runat="server" Text="Tasks assigned to others" 
                           Value="Tasks assigned to others" />
                       <telerik:RadComboBoxItem runat="server" Text="Tasks assigned to others by me" 
                           Value="Tasks assigned to others by me" />
                   </Items>
    </telerik:RadComboBox >
    </div>

      <div style="margin-top: 12px;margin-left:5px;" align="left"> 
   <asp:Button ID="btnOk"  Text="OK" runat="server" />  
         
      <asp:Button ID="btnClearSelections"  Text="Clear All Selections" runat="server" /> 
     </div>
        <div style="font: normal 13px arial,sans-serif;margin-top: 12px;color: #b71c1c" align="left">
          <asp:Literal ID="Literal1" runat="server" />
        </div>
    </form>
</body>
</html>
