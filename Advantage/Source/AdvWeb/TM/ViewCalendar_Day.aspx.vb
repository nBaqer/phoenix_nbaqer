' ===============================================================================
' ViewCalendar_Day
' Displays a calendar for the current day
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common.TM

Partial Class TM_ViewCalendar_Day
    Inherits System.Web.UI.Page

    Private tabIndex As Long = 0    
    Private calendarEvents As New ArrayList
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cmpId As String
        If Not IsPostBack Then
            Dim strToday As String = Request.Params("today")
            If strToday Is Nothing Then
                ViewState("DayOf") = Date.Today
            Else
                If strToday = "1" Then
                    ViewState("DayOf") = Date.Today
                Else
                    ViewState("DayOf") = Request.Params("date")
                End If
            End If
            cmpId = Session("campusid")

            '' get all the tasks for this user and save it in the viewstate
            'Dim ds As DataSet = UserTasksFacade.GetUserTasks(cmpId, Nothing, TMCommon.GetCurrentUserId(), _
            '                    Nothing, Nothing, TaskStatus.None, Nothing, Nothing)

            'ViewState("usertasks") = ds
            'BindPage()
            Dim Filter As String = ""
            Try
                Filter = Request.QueryString("f")
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Filter = "task assigned to me"
            End Try

            If Filter = "" Or Filter = Nothing Then
                Filter = "task assigned to me"
            End If

            If Filter.ToString.ToLower = "tasks assigned to others" Then
                ' get all the tasks for this user and save it in the viewstate
                Dim ds As DataSet = UserTasksFacade.GetUserTasks(cmpId, Nothing, Nothing, _
                                    Nothing, Nothing, TaskStatus.None, Nothing, Nothing, "others", TMCommon.GetCurrentUserId())
                ViewState("usertasks") = ds
                BindPage()
            Else
                Dim ds As DataSet = UserTasksFacade.GetUserTasks(cmpId, Nothing, TMCommon.GetCurrentUserId(), _
                                    Nothing, Nothing, TaskStatus.None, Nothing, Nothing)
                ViewState("usertasks") = ds
                BindPage()
            End If
            
        End If
    End Sub

#Region "Helpers"
    ''' <summary>
    ''' Returns either "StartDate" or "EndDate" depending on the selection
    ''' made in Menu_Calendar->ddlFilter
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function GetFilter() As String
        If Session("filter") Is Nothing Or Session("filter") <> "2" Then
            Return "StartDate"
        End If
        Return "EndDate"
    End Function

    Private Sub BindPage()
        Try
            Dim selDate As DateTime = ViewState("DayOf")
            Dim oTime As DateTime = CType("12:00 AM", DateTime)
            lblDay.Text = selDate.ToLongDateString()

            'set up columns in the results table
            Dim oTable As DataTable = New DataTable("RESULTS")
            oTable.Columns.Add("EventTime", GetType(System.String))
            oTable.Columns.Add("Events", GetType(System.String))

            Dim oRow As DataRow
            Dim i As Integer
            For i = 0 To 23
                oRow = oTable.NewRow()
                oRow("EventTime") = oTime.AddHours(i).ToShortTimeString
                oRow("Events") = GetItemForDate(calendarEvents, selDate, CType(oRow("EventTime"), DateTime))
                oTable.Rows.Add(oRow)
            Next

            dgDayView.DataSource = oTable
            dgDayView.DataBind()
        Catch
        End Try
    End Sub

    Function GetItemForDate(ByVal loadedEvents As ArrayList, ByVal dDate As DateTime, ByVal dTime As DateTime) As String
        Dim sTemp As String = ""        
        'get selected date events
        Dim ds As DataSet = ViewState("usertasks")
        If ds Is Nothing Then Return ""

        Dim strStatus As String = ""
        Dim cellStr As String = "<div><a class=Label href='AddUserTask.aspx?UserTaskId={0}' />{1} - {2}:{3} ({4})</a></div>"

        ' set the real datetime object as a composite of dDate and dTime
        dDate = New DateTime(dDate.Year, dDate.Month, dDate.Day, dTime.Hour, dTime.Minute, dTime.Second)
        Dim sql As String = String.Format("{0}>='{1}' and {0}<'{2}'", GetFilter(), dDate.ToString(), dDate.AddMinutes(30).ToString())
        Dim drs As DataRow() = ds.Tables(0).Select(sql)
        Dim dr As DataRow
        For Each dr In drs
            If dr("Status") = TaskStatus.Cancelled Then
                strStatus = "Cancelled"
            ElseIf dr("Status") = TaskStatus.Completed Then
                strStatus = "Completed"
            ElseIf dr("Status") = TaskStatus.Pending Then
                strStatus = "Pending"
            Else
                strStatus = ""
            End If
            cellStr = dr("OwnerName") & ":" + cellStr
            sTemp += String.Format(cellStr, dr("UserTaskId").ToString(), dr("TaskDescrip"), _
                        dr("EntityName"), dr("ReName"), strStatus)
        Next
        Return sTemp
    End Function
#End Region

#Region "Links and Buttons"
    Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        Try
            Dim dDate As DateTime = CType(ViewState("DayOf"), DateTime)
            ViewState("DayOf") = dDate.AddDays(1)
            BindPage()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.Alert(Me, "Failed to go to the next day.")
        End Try
    End Sub

    Private Sub lnkPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrev.Click
        Try
            Dim dDate As DateTime = CType(ViewState("DayOf"), DateTime)
            ViewState("DayOf") = dDate.AddDays(-1)
            BindPage()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.Alert(Me, "Failed to go to the previous day.")
        End Try
    End Sub
#End Region

End Class
