<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AddUserTask.aspx.vb" Inherits="AdvantageApp_TM_AddUserTask" %>

<%@ Register TagName="SysHeader" TagPrefix="Header" Src="~/usercontrols/TMHeader.ascx" %>
<%@ Register TagName="TaskMenu" TagPrefix="Menu" Src="Menu_Tasks.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>New Task Assignment</title>
    <link href="../css/TM.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" media="all" href="jscalendar/calendar-blue2.css"
        title="blue2" />

    <script type="text/javascript" src="jscalendar/calendar.js"></script>

    <script type="text/javascript" src="jscalendar/lang/calendar-en.js"></script>

    <script type="text/javascript" src="jscalendar/calendar-setup.js"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="../Scripts/Watermark/WaterMark.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(function () {
        $("[id*=txtSearch]").WaterMark();

    });
    </script>
</head>
<body>
    <form id="form1" runat="server">
         <telerik:RadScriptManager ID="RadScriptManager1" Runat="server" EnablePageMethods="true" AsyncPostBackTimeout="3600" >
             <Scripts>
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
            <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
        </Scripts>
       </telerik:RadScriptManager>
    <div class="MenuFramediv">&nbsp;</div>
        <div class="AlignedDiv">
            <table summary="Add User Task table" width="100%" class="tableborder" cellpadding="0" cellspacing="0">
                <tr>
                    <th colspan="2">
                        <h1><asp:Label ID="lblHeader" runat="server" Text="New Task Assignment" /></h1>
                    </th>
                </tr>
                <tr><td style="padding: 20px"><table summary="Add User Task table" width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="labelcell">
                        Module/Entity</td>
                    <td class="contentcell">
                        <asp:DropDownList ID="ddlModules" runat="server" CssClass="DropDownList" Width="40%" AutoPostBack="True" />
                        /
                        <asp:Label ID="lbEntity" runat="server" CssClass="Label" Text="{N/A}" /></td>
                </tr>
                <tr>
                    <td class="labelcell">
                        Task Name<font color="red">*</font></td>
                    <td class="contentcell">
                        <asp:DropDownList ID="ddlTasks" runat="server" CssClass="DropDownListReq" Width="70%" AutoPostBack="True" />
                    </td>
                </tr>
                <tr id="tr1" runat="server" visible="true">
                    <td class="labelcell">Contact Lookup<font style="color: Red">*</font></td>
                    <td class="contentcell">
                        <asp:DropDownList id="EntityList" AutoPostBack="True" CssClass="DropDownList" runat="server" Width="85px">
                                <asp:ListItem Selected="True" Value="Student"> Student </asp:ListItem>
                                <asp:ListItem Value="Lead"> Lead </asp:ListItem>
                                <asp:ListItem Value="Employers"> Employer </asp:ListItem>
                                <asp:ListItem Value="Employees">  Employee</asp:ListItem>
                                <asp:ListItem Value="Lender"> Lender </asp:ListItem>
                            </asp:DropDownList>  
                        <asp:TextBox ID="txtSearch" runat="server" Width="210px" ToolTip="Search by First Name or Last Name"  CssClass="textboxreq"/>&nbsp;&nbsp;
                        <asp:ImageButton runat="server" ID="lookupEntity" OnClick="lookupEntity_OnClick" ImageUrl="../Images/MSG/RecipLookup.gif"/>
                                    
                    </td>
                </tr>
                <tr id="tr2" runat="server" visible="false" height="150px">
                    <td>&nbsp;</td>
                    <td class="contentcell" >
                            <telerik:RadGrid ID="rgEntities" runat="server" Width="90%" AllowSorting="false" style="min-height:35px;" 
                                AutoGenerateColumns="False" 
                                AllowFilteringByColumn="false" GroupingSettings-CaseSensitive="false" AllowPaging="false" OnItemCommand="rgEntities_ItemCommand"
                                 >
                                <ClientSettings>
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="true" ScrollHeight="150px" ></Scrolling>
                                    <Selecting AllowRowSelect="True"></Selecting>
                            </ClientSettings>
                                            
                                <MasterTableView Width="100%"  AllowMultiColumnSorting="false" DataKeyNames="RecipientId, Name" >
                                    <Columns>
                                        <telerik:GridButtonColumn Text="Select" CommandName="Select" >
                                            <HeaderStyle Width="50px"></HeaderStyle> 
                                                        
                                        </telerik:GridButtonColumn>
                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="StatusCodeDescrip"
                                            HeaderText="RecipientId" DataField="RecipientId" display="false">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn AllowFiltering="true" SortExpression="Details" HeaderText="Name"
                                                DataField="Name">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                    </td>
                </tr>
                <tr>
                    <td class="labelcell">
                        Contact<font color="red">*</font></td>
                    <td class="contentcell">
                        <asp:TextBox ID="txtContact" runat="server" CssClass="textboxreq" Width="70%" ReadOnly="true" BackColor="#E7F3F8"  />
                       
                        <asp:HiddenField ID="hfReType" runat="server" />
                        <asp:HiddenField ID="hfReId" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="labelcell">
                        Start Date<font color="red">*</font></td>
                    <td class="contentcell">
                        <asp:TextBox ID="txtStartDate" runat="server" CssClass="textbox" Width="20%" />
                        <img src="../images/TM/PopUpCalendar.gif" id="ibStartDate" />&nbsp;
                        <asp:DropDownList ID="ddlStartHour" runat="server" CssClass="DropDownList" Width="50px">
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                        </asp:DropDownList>
                        <strong>:</strong>
                        <asp:DropDownList ID="ddlStartMin" runat="server" CssClass="DropDownList" Width="50px">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>45</asp:ListItem>
                        </asp:DropDownList>&nbsp;
                        <asp:DropDownList ID="ddlStartAMPM" runat="server" CssClass="DropDownList" Width="50px">
                            <asp:ListItem>AM</asp:ListItem>
                            <asp:ListItem>PM</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="labelcell">
                        End Date<font color="red">*</font></td>
                    <td class="contentcell">
                        <asp:TextBox ID="txtEndDate" runat="server" CssClass="textbox" Width="20%" />
                        <img src="../images/TM/PopUpCalendar.gif" id="ibEndDate" />&nbsp;
                        <asp:DropDownList ID="ddlEndHour" runat="server" CssClass="DropDownList" Width="50px">
                            <asp:ListItem>1</asp:ListItem>
                            <asp:ListItem>2</asp:ListItem>
                            <asp:ListItem>3</asp:ListItem>
                            <asp:ListItem>4</asp:ListItem>
                            <asp:ListItem>5</asp:ListItem>
                            <asp:ListItem>6</asp:ListItem>
                            <asp:ListItem>7</asp:ListItem>
                            <asp:ListItem>8</asp:ListItem>
                            <asp:ListItem>9</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                        </asp:DropDownList>
                        <strong>:</strong>
                        <asp:DropDownList ID="ddlEndMin" runat="server" CssClass="DropDownList" Width="50px">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>45</asp:ListItem>
                        </asp:DropDownList>&nbsp;
                        <asp:DropDownList ID="ddlEndAMPM" runat="server" CssClass="DropDownList" Width="50px">
                            <asp:ListItem>AM</asp:ListItem>
                            <asp:ListItem>PM</asp:ListItem>
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td class="labelcell">
                        Message</td>
                    <td class="contentcell">
                        <asp:TextBox ID="txtMessage" runat="server" CssClass="textboxmsg" Width="70%" Rows="3"
                            TextMode="MultiLine" /></td>
                </tr>
                <tr>
                    <td class="labelcell">
                        <asp:Label ID="lblCategoryPrompt" runat="server" CssClass="Label" Text="Category"
                            Visible="false" />
                    </td>
                    <td class="contentcell">
                        <asp:Label ID="lblCategory" runat="server" CssClass="Label" Width="70%" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td class="labelcell">
                        Priority</td>
                    <td class="contentcell">
                        <asp:DropDownList ID="ddlPriority" runat="server" Width="70%" CssClass="DropDownList" />
                    </td>
                </tr>
                <tr>
                    <td class="labelcell">
                        Assigned to</td>
                    <td class="contentcell">
                        <asp:DropDownList ID="ddlAssignedTo" runat="server" Width="70%" CssClass="DropDownList" />
                    </td>
                </tr>
                <tr>
                    <td class="labelcell">
                        <asp:Label ID="lblAssignedByPrompt" runat="server" CssClass="Label" Text="Created By"
                            Visible="False" />
                    </td>
                    <td class="contentcell">
                        <asp:Label ID="lblAssignedBy" runat="server" CssClass="Label" Width="70%" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td class="labelcell">
                        <asp:Label ID="lblStatusPrompt" runat="server" CssClass="Label" Text="Status" Visible="false" />
                    </td>
                    <td class="contentcell">
                        <asp:Label ID="lblStatus" runat="server" CssClass="Label"  Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td class="labelcell">
                        <asp:Label ID="lblResult" runat="server" CssClass="Label" Text="Result" Visible="false" />
                    </td>
                    <td class="contentcell">
                        <asp:DropDownList ID="ddlResult" runat="server" CssClass="DropDownList" Width="70%" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <asp:Button ID="btnOk" runat="server" Text="Add Task" Width="90px"  />
                        <asp:Button ID="btnClose" runat="server" Text="Close" Width="90px"  />
                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="90px" 
                            Visible="false" /></td>
                </tr>
                </table></td></tr>
            </table>
        </div>
        <asp:HiddenField ID="hfResult" runat="server" />
    </form>
    <!-- Popup calendar javascript setup -->

    <script type="text/javascript">
        Calendar.setup({
            inputField     :    "txtStartDate",   // id of the input field
            ifFormat       :    "%m/%d/%Y",
            button         :    "ibStartDate"   // trigger for the calendar (button ID)
        });
        Calendar.setup({
            inputField     :    "txtEndDate",
            ifFormat       :    "%m/%d/%Y",
            button         :    "ibEndDate"   // trigger for the calendar (button ID)
        });
    </script>

    <!-- End Popup calendar javascript setup -->
</body>
</html>
