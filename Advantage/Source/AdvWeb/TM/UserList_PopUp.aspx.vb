﻿Imports Telerik.Web.UI
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports System.Data

Partial Class TM_UserList_PopUp
    Inherits System.Web.UI.Page

    Dim UserID As String
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init

    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then

            If Request.UrlReferrer.ToString().IndexOf("UserList_PopUp.aspx") = -1 Then
                ViewState("refurl") = Request.UrlReferrer.ToString()
            End If


            UserID = TMCommon.GetCurrentUserId()
            Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
            LoadGrid()
            ddlroles.DataSource = tm.GetRoles_ForwhomtheUsercanAssignTasksTo(UserID)
            ddlroles.DataBind()
            ddlcampusDesc.DataSource = tm.GetUsersCampusList_ForwhomtheUsercanAssignTasksTo(UserID)
            ddlcampusDesc.DataBind()

            Dim ViewSetting As String
            ViewSetting = tm.GetUserTaskCalendarDefaultView(UserID)
            For i As Integer = 0 To ddlFilters.Items.Count - 1
                If ddlFilters.Items(i).Text = ViewSetting Then
                    ddlFilters.SelectedIndex = ddlFilters.Items(i).Index
                End If
            Next
           


            Dim ds As New DataSet
            Dim selectedItems As New ArrayList
            ds = tm.GetUserTaskCalendarDefaultView_users(TMCommon.GetCurrentUserId())
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        selectedItems.Add(ds.Tables(0).Rows(i)(0).ToString)
                    Next
                End If
            End If
            Session("selectedItems") = selectedItems
        End If


    End Sub

    Private Sub LoadGrid()

        Dim UserID As String
        Dim RolesID As String = ""
        Dim CampusDesc As String = ""
        Dim Fullname As String = ""

        '   UserID = "4fcf31eb-7b2a-42d6-918c-9dbccc3df3ab" '' TMCommon.GetCurrentUserId()
        UserID = TMCommon.GetCurrentUserId()
        Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
        If ddlroles.CheckedItems.Count > 0 Then
            For Each item As RadComboBoxItem In ddlroles.CheckedItems
                RolesID = RolesID + item.Value + ","
            Next
            RolesID = RolesID.Remove(RolesID.Length - 1, 1)
        End If
        If ddlcampusDesc.CheckedItems.Count > 0 Then
            For Each item As RadComboBoxItem In ddlcampusDesc.CheckedItems
                CampusDesc = CampusDesc + item.Value + ","
            Next
            CampusDesc = CampusDesc.Remove(CampusDesc.Length - 1, 1)
        End If
        If txtFullName.Text <> String.Empty Then
            Fullname = txtFullName.Text
        End If

        dgUsers.DataSource = tm.GetUsersList_ForwhomtheUsercanAssignTasksTo(UserID, RolesID, CampusDesc, Fullname)

    End Sub


    Protected Sub dgUsers_NeedDataSource(source As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles dgUsers.NeedDataSource
        LoadGrid()
    End Sub

    'Protected Sub ddlroles_SelectedIndexChanged(o As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles ddlroles.SelectedIndexChanged
    '    'If ddlroles.SelectedItem.Text <> "All" Then
    '    '    LoadGrid(ddlroles.SelectedItem.Value)
    '    '    dgUsers.DataBind()
    '    'End If
    '    '  Session("selectedItems") = Nothing

    'End Sub

    Protected Sub btnOk_Click(sender As Object, e As System.EventArgs) Handles btnOk.Click
        Dim params As String
        Dim errmessage As String = ""
        Dim selectedItems As New ArrayList
        UserID = TMCommon.GetCurrentUserId()
        If Not Session("selectedItems") Is Nothing Then
            selectedItems = CType(Session("selectedItems"), ArrayList)
  
            End If

            Dim err As String
            Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
            If selectedItems.Count = 0 Then
            '  errmessage = errmessage + " Please select at least one user "
                err = tm.DeleteUsersPreferenceList(UserID)
            If Not err = "" Then
                errmessage = errmessage + " User preference not saved "
            End If

        ElseIf selectedItems.Count > 0 And selectedItems.Count <= 12 Then
            ''Save user task -others list preference as Default View
            Dim Otheruserslist As New StringBuilder
            For i As Integer = 0 To selectedItems.Count - 1
                Otheruserslist.Append(selectedItems(i).ToString)
                Otheruserslist.Append(",")
            Next
            Otheruserslist.Remove(Otheruserslist.ToString.Length - 1, 1)



            err = tm.InsertUsersPreferenceList(UserID, Otheruserslist.ToString, ddlFilters.SelectedValue.ToString)
            If Not err = "" Then
                errmessage = errmessage + " User preference not saved "
            Else
                Dim refer As String = ViewState("refurl")
                If refer Is Nothing Or refer = "" Then

                Else
                    Dim QString As String()
                    QString = refer.Split("?")
                    params = Request.QueryString("TaskCategory")
                    If params = "1" Then
                        Response.Redirect(QString(0) + "?f=tasks assigned to others")
                    ElseIf params = "2" Then
                        Response.Redirect(QString(0) + "?f=tasks assigned to others by me")
                    Else
                        Response.Redirect(refer)
                    End If

                End If
            End If
        ElseIf selectedItems.Count > 12 Then
            errmessage = errmessage + " Please select not more than 12 Users "
        End If
        Literal1.Text = errmessage
    End Sub
    Protected Sub dgUsers_ItemCommand(ByVal source As Object, _
    ByVal e As GridCommandEventArgs) Handles dgUsers.ItemCommand
        Dim selectedItems As ArrayList
        If Session("selectedItems") Is Nothing Then
            selectedItems = New ArrayList
        Else
            selectedItems = CType(Session("selectedItems"), ArrayList)
        End If

        If e.CommandName = RadGrid.SelectCommandName _
           AndAlso TypeOf e.Item Is GridDataItem Then
            Dim dataItem As GridDataItem = CType(e.Item, GridDataItem)
            Dim UserID As String = dataItem.OwnerTableView.DataKeyValues(dataItem.ItemIndex)("UserID").ToString
            selectedItems.Add(userID)
            Session("selectedItems") = selectedItems
        End If
        If e.CommandName = RadGrid.DeselectCommandName _
           AndAlso TypeOf e.Item Is GridDataItem Then
            Dim dataItem As GridDataItem = CType(e.Item, GridDataItem)
            Dim UserID As String = dataItem.OwnerTableView.DataKeyValues(dataItem.ItemIndex)("UserID").ToString
            selectedItems.Remove(userID)
            Session("selectedItems") = selectedItems
        End If
    End Sub
    Protected Sub dgUsers_PreRender(ByVal sender As Object, _
        ByVal e As EventArgs) Handles dgUsers.PreRender
        If Not (Session("selectedItems") Is Nothing) Then
            Dim selectedItems As ArrayList = CType(Session("selectedItems"), ArrayList)
            Dim stackIndex As Int16
            For stackIndex = 0 To selectedItems.Count - 1
                Dim curItem As String = selectedItems(stackIndex).ToString
                For Each item As GridItem In dgUsers.MasterTableView.Items
                    If TypeOf item Is GridDataItem Then
                        Dim dataItem As GridDataItem = CType(item, GridDataItem)
                        If curItem.Equals(dataItem.OwnerTableView.DataKeyValues(dataItem.ItemIndex)("UserID").ToString()) Then
                            dataItem.Selected = True
                        End If
                    End If
                Next
            Next
        End If
    End Sub

    Protected Sub btnClearSelections_Click(sender As Object, e As System.EventArgs) Handles btnClearSelections.Click
        Session("selectedItems") = Nothing
     
            LoadGrid()


        dgUsers.DataBind()
    End Sub

    Protected Sub btnFilter_Click(sender As Object, e As System.EventArgs) Handles btnFilter.Click
        LoadGrid()
        dgUsers.DataBind()
    End Sub
End Class
