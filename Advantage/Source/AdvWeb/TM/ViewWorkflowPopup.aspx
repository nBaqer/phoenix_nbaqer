<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewWorkflowPopup.aspx.vb"
    Inherits="TM_ViewWorkflowPopup" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>View Workflow</title>
    <link href="../CSS/localhost.css" type="text/css" rel="stylesheet" />
    <link href="../CSS/systememail.css" type="text/css" rel="stylesheet" />
    <link href="../css/tm.css" rel="stylesheet" type="text/css" />
    <link href="../css/flowchart.css" type="text/css" rel="stylesheet" />
    <script type="text/javascript">
        // Node definitions
        var nr = 1;
        var nodeBag = {};
        var sel = 0;
        var attach_sel = 0;
        var selnode;

        // Node constructor
        function Node(data, childNodes) {
            this.data = data;
            this.id = nr++;
            nodeBag[this.id] = this;
            if (childNodes) {
                this.first = childNodes[0];
                for (var i = 0; i < childNodes.length; i++) {
                    childNodes[i].prev = childNodes[i - 1];
                    childNodes[i].next = childNodes[i + 1];
                    childNodes[i].parent = this;
                } // end for
            } // end if
        } // end node construcotr

        Node.prototype = {
            render: function (finalver) {
                var s = ''; var next = ''; var l = 1;
                if (this.first) {
                    l = 0; var node = this.first;
                    while (node) {
                        next += '<TD valign="top">' + node.render(finalver) + '</td>';
                        node = node.next; l++;
                    }
                }
                s += '\n<table cellspacing="0" cellpadding="0"><tr><td colspan=' + l + '>';
                s += '\n<table cellspacing="0" cellpadding="0" width="100%">';
                if (this.parent) {
                    s += '\n<tr><td width="50%" class="' + (!this.prev ? 'lineright' : 'leftabove') + '">&nbsp;</td>';
                    s += '<td width="50%" class="' + (!this.next ? 'nolines' : 'rightabove') + '">&nbsp;</td></tr>';
                }
                s += '\n<tr><td colspan="2" class="boxc">\n<table cellspacing="0" cellpadding="0">';
                s += '\n<tr><td align="center" class="' + (sel == this.id && !finalver ? 'boxChecked' : 'box') + '"'
                if (!finalver) s += ' onclick="unhover(this);choose(' + this.id + ');hover(this);" id="box' + this.id + '" onmouseover="hover(this)" onmouseout="unhover(this)"';
                s += '">';
                if (finalver && this.data.result) s += '{' + this.data.result + '}';
                s += '<div class="result">';
                s += this.data.result;
                s += '</div><div class="task">';
                s += (this.data.name || '');
                if (finalver && this.data.result) s += '</div></a>';
                s += '</td></tr></table>\n</td></tr>';
                if (this.first) s += '\n<tr><td width="50%" class="lineright">&nbsp;</td><td width="50%"></td></tr>';
                s += '</table></td></tr><tr>' + next + '</tr></table>';
                return s;
            }
        } //end node prototype definition

        function choose(s) {
            if (attach_sel) { selnode.doAttach(s); renderIt(); return; }
            if (sel) try { document.getElementById('box' + sel).className = 'box'; } catch (e) { unselect() }
            if (sel != s) {
                sel = s; selnode = nodeBag[sel];
                document.getElementById('box' + sel).className = 'boxChecked';
                var f = document.forms[0]; f.reset();
                for (var p in selnode.data) f[p].value = selnode.data[p];
                document.getElementById('props').style.display = document.getElementById('chartpart').style.display;
            } else {
                unselect();
            }
        }

        function renderIt() {
            var s = ''; var l = "0;"
            for (var i in nodeBag)
                if (!nodeBag[i].parent) {
                    if (l) s += '<HR width="70%">';
                    s += nodeBag[i].render();
                    l++;
                }
            document.getElementById('chart').innerHTML = s || '&nbsp;';
        }

        function hover(el) {
            el.oldclass = el.className;
            el.className = el.className + 'Hover';
        }

        function unhover(el) {
            el.className = el.oldclass;
        }
    </script>
</head>
<body onload="loadData();">
    <form id="form1" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="toppopupheader">
                <img src="../images/graphical_view.jpg" alt="work flow" />
            </td>
            <td class="topemail">
                <a class="close" onclick="top.close()" href="#">X Close</a>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <div class="label">
        Max # of levels to display:&nbsp;
        <asp:DropDownList ID="ddlMaxLevels" runat="server" class="dropdownlist" Width="45px"
            AutoPostBack="True" />
    </div>
    </form>
    <div class="workflowpopup">
        <asp:Label ID="lblData" runat="server" />
        <table width="100%" cellspacing="0" cellpadding="0" id="chartpart">
            <tr>
                <td id="chart" class="border">
                    &nbsp;
                </td>
            </tr>
        </table>
        <form onsubmit="return false">
        <table class="border" cellspacing="0" cellpadding="0" style="display: none" id="props">
            <tr>
                <td class="props" colspan="2">
                    Task Properties
                </td>
            </tr>
            <tr>
                <th class="props">
                    Name:
                </th>
                <td class="border">
                    <input class="props" size="30" type="text" name="name" value="" onblur="if (selnode) selnode.update();renderIt();" />
                </td>
            </tr>
            <tr>
                <th class="props">
                    Result:
                </th>
                <td class="border">
                    <input class="props" size="30" type="text" name="result" value="" onblur="if (selnode) selnode.update();renderIt();" />
                </td>
            </tr>
        </table>
        </form>
        <hr />
    </div>
    <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
</body>
</html>
