<%@ Control Language="VB" ClassName="CalendarMenu" %>
<%@ Import Namespace="FAME.AdvantageV1.BusinessFacade" %>
<link href="../css/TM.css" rel="stylesheet" type="text/css" />

<script runat="server">    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Page.IsPostBack Then
            'build the predefined filters
            Dim filter As String = Request.Params("f")
            Dim boolFrontDesk As Boolean
            If Trim(Session("UserName")).ToLower = "sa" Then
                boolFrontDesk = True
                BuildFiltersDDL(ddlFilters, boolFrontDesk)
            Else
                boolFrontDesk = (New RolesFacade).CheckIfUserIsMappedToFrontDesk(Trim(Session("UserId")))
                BuildFiltersDDL(ddlFilters, boolFrontDesk)
            End If
            If (filter Is Nothing Or filter = "" Or filter = "Tasks assigned to me") Then
                ddlFilters.SelectedValue = "Tasks assigned to me"
            ElseIf (Not filter Is Nothing Or Not filter = "") And boolFrontDesk = True Then
                ddlFilters.SelectedValue = "Tasks assigned to others"
            End If
        End If
    End Sub
    Protected Sub ddlFilters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim url As String = Page.AppRelativeVirtualPath + "?f=" + ddlFilters.SelectedValue
        Response.Redirect(url)
    End Sub
    Protected Sub BuildFiltersDDL(ByRef ddl As DropDownList, ByVal UserIsFrontDesk As Boolean)
        ddl.Items.Add(New ListItem("--Select Filter--", ""))
        ddl.Items.Add("Tasks assigned to me")
        If UserIsFrontDesk = True Then
            ddl.Items.Add("Tasks assigned to others")
        End If
    End Sub
</script>
<div style="margin: 0; padding: 0">
    <table width="100%" summary="SysMenu" cellpadding="0" cellspacing="0">
        <tr>
             <td class="MenuFrame"  nowrap>
             <div class="calendarmenuitems">
                    <asp:Menu ID="Menu16" runat="server" DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false"
                        Orientation="Horizontal">
                        <StaticMenuStyle />
                        <StaticMenuItemStyle CssClass="taskbuttons" />
                        <DynamicMenuStyle />
                        <DynamicMenuItemStyle />
                        <DynamicHoverStyle />
                        <Items>
                            <asp:MenuItem NavigateUrl="AddUserTask.aspx" Text="New Task" Value="New Task" />
                        </Items>
                    </asp:Menu>
                </div>
                <div class="calendarmenuitems">
                    <asp:Menu ID="Menu2" runat="server" DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false"
                        Orientation="Horizontal">
                        <StaticMenuStyle />
                        <StaticMenuItemStyle CssClass="taskbuttons" />
                        <DynamicMenuStyle />
                        <DynamicMenuItemStyle />
                        <DynamicHoverStyle />
                        <Items>
                            <asp:MenuItem NavigateUrl="ViewCalendar_Day.aspx?today=1" Text="Today" Value="Today" />
                        </Items>
                    </asp:Menu>
                </div>
                <div class="calendarmenuitems">
                    <asp:Menu ID="Menu3" runat="server" DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false"
                        Orientation="Horizontal">
                        <StaticMenuStyle />
                        <StaticMenuItemStyle CssClass="taskbuttons" />
                        <DynamicMenuStyle />
                        <DynamicMenuItemStyle />
                        <DynamicHoverStyle />
                        <Items>
                            <asp:MenuItem NavigateUrl="ViewCalendar_Day.aspx" Text="Daily View" Value="Daily View" />
                        </Items>
                    </asp:Menu>
                </div>
                <div class="calendarmenuitems">
                    <asp:Menu ID="Menu4" runat="server" DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false"
                        Orientation="Horizontal">
                        <StaticMenuStyle />
                        <StaticMenuItemStyle CssClass="taskbuttons" />
                        <DynamicMenuStyle />
                        <DynamicMenuItemStyle />
                        <DynamicHoverStyle />
                        <Items>
                            <asp:MenuItem NavigateUrl="ViewCalendar_Weekly.aspx" Text="Weekly View" Value="Weekly View" />
                        </Items>
                    </asp:Menu>
                </div>
                <div class="calendarmenuitems">
                    <asp:Menu ID="Menu5" runat="server" DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false"
                        Orientation="Horizontal">
                        <StaticMenuStyle />
                        <StaticMenuItemStyle CssClass="taskbuttons" />
                        <DynamicMenuStyle />
                        <DynamicMenuItemStyle />
                        <DynamicHoverStyle />
                        <Items>
                            <asp:MenuItem NavigateUrl="ViewCalendar_Month.aspx" Text="Monthly View" Value="Monthly View" />
                        </Items>
                    </asp:Menu>
                </div>
                <div style="float: left; width: 203px; margin: 10px 0 0 0; padding: 0; text-align: left">
                    <asp:DropDownList ID="ddlFilters" runat="server" Width="195px" AutoPostBack="True"
                        CssClass="dropdowntaskmenu" OnSelectedIndexChanged="ddlFilters_SelectedIndexChanged" Visible="True">
                    </asp:DropDownList>
                </div>
                <div class="taskbgrightdiv">&nbsp;</div>                                         
            </td>
        </tr>
    </table>
</div>
