<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SetupPermissions.aspx.vb" Inherits="AdvantageApp_TM_SetupPermissions" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlUsers">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlUsers" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlUsers" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlUsers" />
                </UpdatedControls>
            </telerik:AjaxSetting>

        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlUsers" runat="server" DataKeyField="UserId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" ImageUrl="~/images/Inactive.gif" runat="server"
                                            Visible='<%# (Not Ctype(Container.DataItem("AccountActive"), Boolean)).ToString %>'
                                            CommandArgument='<%# Container.DataItem("UserId")%>' CausesValidation="False" />
                                        <asp:ImageButton ID="imgActive" ImageUrl="~/images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("AccountActive"), Boolean).ToString %>'
                                            CommandArgument='<%# Container.DataItem("UserId")%>' CausesValidation="False" />
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("AccountActive")%>' />
                                        <asp:LinkButton Text='<%# Container.DataItem("FullName")%>' runat="server" CssClass="itemstyle"
                                            CommandArgument='<%# Container.DataItem("UserId")%>' ID="Linkbutton1" CausesValidation="False" />
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="98%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button Enabled="false" ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width:98%;" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <asp:Repeater ID="rptPermissions" runat="server">
                                        <HeaderTemplate>
                                            <table class="contenttable" width="250" cellpadding="0" cellspacing="0" align="center">
                                                <tr>
                                                    <td class="label" style="width: 100%; text-align: center; padding: 0 20px 10px 4em" colspan="2">
                                                        <i>Define the roles for which a user can assign tasks to.</i></td>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: right; width: 10%; padding-right: 10px">
                                                    <asp:HiddenField ID="hfCampGrpId" runat="server" Value='<%# Container.DataItem("CampGrpId") %>' />
                                                    <asp:Label ID="lblCampus" runat="server" Class="label" Text='<%#Container.DataItem("CampGrpDescrip")%>' />
                                                </td>
                                                <td style="text-align: left; width: 90%">
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="threecolumnheaderreg" nowrap>
                                                                <asp:Label ID="lblRoles" Class="label" runat="server" Font-Bold="True" Text="Roles" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="threecolumncontentreg" style="text-align: center; padding-bottom: 20px" nowrap>
                                                                <div style="overflow: auto; height: 100px; width: 450px">
                                                                    <asp:CheckBoxList ID="cblRoles" runat="server" CssClass="listboxes" Height="100px" Width="100%" TextAlign="Right" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <SeparatorTemplate>
                                        </SeparatorTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <!--end table content-->
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <asp:HiddenField ID="hfResult" runat="server" />
        <!-- end footer -->
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
            ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
                runat="server">
            </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    </div>
</asp:Content>


