' ===============================================================================
' ViewCalendar_Month
' View a calendar in a monthly view
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common.TM

Partial Class TM_ViewCalendary_Month
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            'Dim ds As DataSet = UserTasksFacade.GetUserTasks(TMCommon.GetCampusID(), Nothing, TMCommon.GetCurrentUserId(), _
            '                    Nothing, Nothing, TaskStatus.None, Nothing, Nothing)
            'ViewState("usertasks") = ds

            Dim filter As String
            Try
                filter = Request.QueryString("f")
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                filter = "task assigned to me"
            End Try

            If filter = "" Or filter = Nothing Then
                filter = "task assigned to me"
            End If

            If filter.ToString.ToLower = "tasks assigned to others" Then
                ' get all the tasks for this user and save it in the viewstate
                Dim ds As DataSet = UserTasksFacade.GetUserTasks(TMCommon.GetCampusID(), Nothing, Nothing, _
                                    Nothing, Nothing, TaskStatus.None, Nothing, Nothing, "others", TMCommon.GetCurrentUserId())
                ViewState("usertasks") = ds
            Else
                Dim ds As DataSet = UserTasksFacade.GetUserTasks(TMCommon.GetCampusID(), Nothing, TMCommon.GetCurrentUserId(), _
                                    Nothing, Nothing, TaskStatus.None, Nothing, Nothing)
                ViewState("usertasks") = ds
            End If
        End If
    End Sub

    ''' <summary>
    ''' Returns either "StartDate" or "EndDate" depending on the selection
    ''' made in Menu_Calendar->ddlFilter
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function GetFilter() As String
        If Session("filter") Is Nothing Or Session("filter") <> "2" Then
            Return "StartDate"
        End If
        Return "EndDate"
    End Function

    Protected Sub Calendar1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Calendar1.SelectionChanged
        Dim oCal As Calendar = sender
        Dim strDate As String = oCal.SelectedDate.ToShortDateString()
        Response.Redirect("ViewCalendar_Day.aspx?today=0&date=" + strDate)        
    End Sub

    ''' <summary>
    ''' This method gets called for each day in the calendar
    ''' Add the events for each day as a list of links
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub OnDayRender(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DayRenderEventArgs) Handles Calendar1.DayRender
        Dim ds As DataSet = ViewState("usertasks")

        If ds Is Nothing Then Return

        Dim lbl As New Label()
        ' add a carriage return
        lbl.Text = "<div></div>"
        e.Cell.Controls.Add(lbl)

        Dim cellStr As String = "<div><a href='AddUserTask.aspx?UserTaskId={0}' />{1}</a></div>"
        Dim sql As String = String.Format("{0}>='{1}' and {0}<'{2}'", GetFilter(), e.Day.Date.ToString(), e.Day.Date.AddDays(1).ToString())
        Dim tmpStr As String = ""
        Dim strStatus As String = ""
        Dim drs As DataRow() = ds.Tables(0).Select(sql)
        Dim dr As DataRow
        For Each dr In drs
            lbl = New Label()
            tmpStr = dr("TaskDescrip").ToString()
            If tmpStr.Length > 16 Then
                tmpStr = tmpStr.Substring(0, 16) + "..."
            End If
            cellStr = dr("OwnerName") & ":" + cellStr
            lbl.Text = String.Format(cellStr, dr(0).ToString, tmpStr)
            ' change the style here to make break the word
            lbl.Height = 10
            lbl.Width = e.Cell.Width

            If dr("Status") = TaskStatus.Cancelled Then
                strStatus = "Cancelled"
            ElseIf dr("Status") = TaskStatus.Completed Then
                strStatus = "Completed"
            ElseIf dr("Status") = TaskStatus.Pending Then
                strStatus = "Pending"
            Else
                strStatus = ""
            End If

            lbl.ToolTip = String.Format("{0} with {2} ({1})", dr("TaskDescrip"), strStatus, dr("ReName"))
            e.Cell.Controls.Add(lbl)
        Next
    End Sub

    Function GetPriorityString(ByVal priority As Integer) As String
        If priority = 0 Then
            Return "Normal"
        ElseIf priority = 1 Then
            Return "High"
        ElseIf priority = 2 Then
            Return "Low"
        End If
        Return "N/A"
    End Function

End Class
