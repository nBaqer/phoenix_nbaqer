' ===============================================================================
' ViewUserTaskHistory
'   Views the task history for a specfic contact
'   This page requires that the contactid is passed
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

' This page can take "reid" as a paramter.  "reid" can be a studentid, leadid or employerid
' For example, ViewUserTaskHistory.aspx?reid=132-23423-23423

Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common.TM

Partial Class TM_ViewUserTaskHistory
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim reid As String = Request.Params("reid")

            ' check if the reid paramter was passed and if not, return an error
            If reid Is Nothing Or reid = "" Then
                TMCommon.DisplayErrorMessage(Me, "The required paramter reid was not passed to the page.")
                Return
            End If

            rptHistory.DataSource = UserTasksFacade.GetUserTasks(TMCommon.GetCampusID(), Nothing, Nothing, reid, Nothing, TaskStatus.None, Nothing, Nothing)
            rptHistory.DataBind()

            Me.Title = "User Task History for " + UserTasksFacade.GetContactName(reid)
        End If
    End Sub

    ''' <summary>
    ''' Called for each item in the repeater.
    ''' Gives us the change to set the images and other text values correctly
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rptHistory_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptHistory.ItemDataBound
        Try
            ' set the status to the correct string
            Dim lb As Label = e.Item.FindControl("lblStatus")
            If Not lb Is Nothing Then
                If lb.Text = TaskStatus.Cancelled Then
                    lb.Text = "Cancelled"
                ElseIf lb.Text = TaskStatus.Completed Then
                    lb.Text = "Completed"
                ElseIf lb.Text = TaskStatus.Pending Then
                    lb.Text = "Pending"
                Else
                    lb.Text = "N/A"
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

End Class
