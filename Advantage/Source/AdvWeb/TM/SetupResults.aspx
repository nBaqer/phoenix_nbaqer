<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SetupResults.aspx.vb" Inherits="AdvantageApp_TM_SetupResults" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlResults">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlResults" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlResults" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlResults" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlResults" runat="server" DataKeyField="ResultId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" ImageUrl="~/images/Inactive.gif" runat="server" Visible='<%# (Not Ctype(Container.DataItem("Active"), Boolean)).ToString %>' CausesValidation="False" CommandArgument='<%# Container.DataItem("ResultId")%>' />
                                        <asp:ImageButton ID="imgActive" ImageUrl="~/images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Active"), Boolean).ToString %>' CausesValidation="False" CommandArgument='<%# Container.DataItem("ResultId")%>' />
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("Active")%>' />
                                        <asp:LinkButton Text='<%# Container.DataItem("Descrip")%>' runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("ResultId")%>' ID="Linkbutton1" CausesValidation="False" />
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="98%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->

                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" style="width:50%;" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCode" runat="server" Class="label">Code <span style="color: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCode" runat="server" CssClass="textbox" TabIndex="1" MaxLength="20"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblActivePrompt" runat="server" Class="label">Status <span style="color: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlActive" runat="server" class="dropdownlist" TabIndex="2" /></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblDescrip" runat="server" Class="label">Description <span style="color: red">*</span></asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="textbox" TabIndex="3" MaxLength="100"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblotheraction" runat="server" Class="label">Other action</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlActions" runat="server" class="dropdownlist" AutoPostBack="True" TabIndex="4" Width="200px"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblOtherActionValue" runat="server" Class="label">Other action value</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlActionValues" runat="server" class="dropdownlist" TabIndex="5"></asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <!--Start of Atuo Create Next Tasks -->
                                    <asp:Repeater ID="rptNextTasks" runat="server">
                                        <HeaderTemplate>
                                            <table class="contenttable" align="center" width="440px">
                                                <tr>
                                                    <td class="threecolumnheaderreg" colspan="5" align="center">
                                                        <asp:Label ID="lblAutoCreateTaskList" Class="label" runat="server" Font-Bold="True" Text="List of tasks that will automatically be created on this result" /></td>
                                                </tr>
                                                <tr>
                                                    <td width="10px" align="left">
                                                        <asp:ImageButton ID="btnAdd" runat="server" ImageUrl="~/images/TM/add.jpg" CommandName="Add" ToolTip="Add" /></td>
                                                    <td width="200px">
                                                        <asp:DropDownList ID="ddlTasks" runat="server" class="dropdownlist" Width="200px" /></td>
                                                    <td width="80px" align="right" nowrap>
                                                        <asp:Label Width="80px" ID="lbNumAdv" runat="server" Text="Due" Class="label" ToolTip="Due date of this task will be the time that the base task ended + the specified amount of time" /></td>
                                                    <td width="30px">
                                                        <asp:TextBox ID="txtStartGap" runat="server" CssClass="textbox" Text="0" Width="30px" /></td>
                                                    <td width="100px" class="Label">
                                                        <asp:DropDownList ID="ddlStartGapUnit" runat="server" class="dropdownlist" Width="100px">
                                                            <asp:ListItem Text="Day(s)" Value="day" Selected="True" />
                                                            <asp:ListItem Text="Hour(s)" Value="hour" />
                                                            <asp:ListItem Text="Minute(s)" Value="min" />
                                                        </asp:DropDownList>&nbsp;later</td>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td width="10px" align="left">
                                                    <asp:HiddenField ID="hfTaskId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.TaskId") %>' />
                                                    <asp:ImageButton ID="btnDelete" runat="server" ImageUrl="~/images/TM/delete.gif"
                                                        CommandName="Del" ToolTip="Delete" CommandArgument='<%# DataBinder.Eval(Container, "DataItem.TaskId") %>' /></td>
                                                <td width="200px" nowrap>
                                                    <asp:Label Width="200px" ID="lblTaskName" runat="server" Class="label" Text='<%# DataBinder.Eval(Container, "DataItem.TaskDescrip") %>' /></td>
                                                <td width="80px" class="Label" align="right" nowrap>Due</td>
                                                <td width="30px">
                                                    <asp:TextBox ID="txtEdStartGap" runat="server" CssClass="textbox" Width="30px" Text='<%# DataBinder.Eval(Container, "DataItem.StartGap") %>' /></td>
                                                <td width="100px" class="Label">
                                                    <asp:DropDownList ID="ddlEdStartGapUnit" runat="server" class="dropdownlist" Width="100px">
                                                        <asp:ListItem Text="Day(s)" Value="day" Selected="True" />
                                                        <asp:ListItem Text="Hour(s)" Value="hour" />
                                                        <asp:ListItem Text="Minute(s)" Value="min" />
                                                    </asp:DropDownList>&nbsp;later</td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <!-- End of Atuo Create Next Tasks -->
                                </div>
                                <!--end table content-->
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
            ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
                runat="server">
            </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    </div>

</asp:Content>


