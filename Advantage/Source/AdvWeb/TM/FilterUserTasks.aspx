<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FilterUserTasks.aspx.vb"
    Inherits="AdvantageApp_TM_FilterUserTasks" %>

<%@ Register TagName="SysHeader" TagPrefix="Header" Src="~/usercontrols/TMHeader.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head runat="server">
    <title>Filter Tasks</title>
    <meta http-equiv="pragma" content="NO-CACHE">
    <link href="../CSS/systememail.css" type="text/css" rel="stylesheet">
    <link href="../css/TM.css" rel="stylesheet" type="text/css" />
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <img src="../images/filter_tasks.jpg"></td>
                <td class="topemail">
                    <a class="close" onclick="top.close()" href=#>X Close</a></td>
            </tr>
        </table>
        <div style="padding: 20px; margin: 0; width: 100%">
            <table width="100%" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="twocolumnlabelcell"><span class="label">
                        Status</span></td>
                    <td class="twocolumncontentcell">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="DropDownList" /></td>
                </tr>
                <tr>
                    <td class="twocolumnlabelcell"><span class="label">
                        Contact</span></td>
                    <td class="twocolumncontentcell" style="text-align: left">
                        <asp:TextBox ID="txtContactName" runat="server" CssClass="textbox" Width="50%" />
                        <asp:ImageButton ID="ibReLookup" Width="14px" Height="14px" ImageUrl="../Images/MSG/RecipLookup.gif"
                            runat="server" />
                        <asp:LinkButton ID="lbReLookup" runat="server" Width="30%" CssClass="Label" Text="Lookup" />
                        <asp:HiddenField ID="hfReId" runat="server" />
                        <asp:HiddenField ID="hfReType" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="twocolumnlabelcell"><span class="label">
                        Priority</span></td>
                    <td class="twocolumncontentcell">
                        <asp:DropDownList ID="ddlPriority" runat="server" CssClass="DropDownList" /></td>
                </tr>
                <tr>
                    <td class="twocolumnlabelcell"><span class="label">
                        Assigned To</span></td>
                    <td class="twocolumncontentcell">
                        <asp:DropDownList ID="ddlAssignedTo" runat="server" CssClass="DropDownList" /></td>
                </tr>
                <tr>
                    <td class="twocolumnlabelcell"><span class="label">
                        Module</span></td>
                    <td class="twocolumncontentcell">
                        <asp:DropDownList ID="ddlModule" runat="server" CssClass="DropDownList" AutoPostBack="true" /></td>
                </tr>
                <tr>
                    <td class="twocolumnlabelcell"><span class="label">
                        Entity</span></td>
                    <td class="twocolumncontentcell">
                        <asp:DropDownList ID="ddlEntity" runat="server" CssClass="DropDownList" /></td>
                </tr>
                <tr>
                    <td class="twocolumnlabelcell">
                        &nbsp;</td>
                    <td class="twocolumncontentcell">
                        <asp:Button ID="btnOK" runat="server"  Text="Ok" Width="80px" />&nbsp;
                        <asp:Button ID="btnCancel" runat="server"  Text="Cancel" Width="80px" />
                        <asp:Button ID="btnReset" runat="server"  Text="Reset" Width="80px" /></td>
                </tr>
            </table>
            <!-- start Hidden fields -->
            <asp:HiddenField ID="txtRecipientId" runat="server" />
            <!-- end Hidden fields -->
        </div>
       <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
    </form>
</body>
</html>

