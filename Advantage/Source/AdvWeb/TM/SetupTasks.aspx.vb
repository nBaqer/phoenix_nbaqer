' ===============================================================================
' SetupTasks
' Task Manager code behind SetupTasks.aspx
' Handles adding and editing tasks
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common.TM
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class AdvantageApp_TM_SetupTasks
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")


        Dim m_Context As HttpContext

        Dim userId As String
        Dim resourceId As Integer
        Dim fac As New UserSecurityFacade

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = resourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = resourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Session("UserId") = userId
        Session("UserName") = AdvantageSession.UserState.UserName.ToString
        Session("cmpid") = campusId
        Session("resid") = txtResourceId.Text.Trim
        TMCommon.AdvInit(Me)


        If Not Page.IsPostBack Then
            InitButtonsForLoad()
            'Header1.EnableHistoryButton(False)

            ' load the form with its default values
            ResetForm()
            PopulateLHS()
            ' show the tasks in the left pane
            BindTasks()

            ' Bind the task to the form
            Dim TaskId As String = Request.Params("taskid")
            If Not TaskId Is Nothing Then
                BindTask(TaskId)
            End If
        Else
            InitButtonsForEdit()
        End If
        headerTitle.Text = Header.Title
    End Sub

#Region "Advantage Permissions"
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If
        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnnew.Enabled = False
        End If
        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If
        'btnnew.Enabled = True
        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If
    End Sub
#End Region

    ''' <summary>
    ''' Clears the form to its initial state.  DropDownLists are reset
    ''' with values from the database.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ResetForm()
        ' clear out the form
        txtCode.Text = ""
        txtName.Text = ""

        TMCommon.BuildCampusGroups(Me.ddlCampGroup)
        TMCommon.BuildCategoryDDL(TMCommon.GetCampusID(), ddlCategory)
        TMCommon.BuildModulesDDL(Nothing, ddlModule)
        'ddlModule.Items.RemoveAt(1) ' Mantis 7874: Remove "All" from the module drop(down)

        ddlEntity.Items.Clear()

        lbSysResults.Items.Clear()
        lbResults.Items.Clear()
        TMCommon.BuildAvialableResultsLB(lbSysResults)

        TMCommon.BuildStatusDDL(ddlActive)
        ddlActive.SelectedValue = True

        txtNewResult.Text = ""
        hlWorkflow.Visible = False
        ddlStatus.Items.Clear()
        ddlStatus.Visible = False
        lblStatus.Visible = False
    End Sub

    ''' <summary>
    ''' Displays the list of tasks
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindTasks()
        If radStatus.SelectedIndex = 0 Then
            dlTasks.DataSource = TasksFacade.GetTasks(Nothing, Nothing, Nothing, True, False)
        ElseIf radStatus.SelectedIndex = 1 Then
            dlTasks.DataSource = TasksFacade.GetTasks(Nothing, Nothing, Nothing, False, True)
        Else
            dlTasks.DataSource = TasksFacade.GetTasks(Nothing, Nothing, Nothing, True, True)
        End If
        dlTasks.DataBind()
    End Sub
    Protected Sub BindTasks(ByVal categoryId As String, ByVal moduleId As String)
        If radStatus.SelectedIndex = 0 Then
            dlTasks.DataSource = TasksFacade.GetFilterTasks(Nothing, categoryId, moduleId, True, False)
        ElseIf radStatus.SelectedIndex = 1 Then
            dlTasks.DataSource = TasksFacade.GetFilterTasks(Nothing, categoryId, moduleId, False, True)
        Else
            dlTasks.DataSource = TasksFacade.GetFilterTasks(Nothing, categoryId, moduleId, True, True)
        End If
        dlTasks.DataBind()
    End Sub

    ''' <summary>
    ''' Fills the forms with values queries from the database
    ''' using the supplied taskid
    ''' </summary>
    ''' <param name="TaskId"></param>
    ''' <remarks></remarks>
    Protected Sub BindTask(ByVal TaskId As String)
        Try
            ' display the available results
            TMCommon.BuildAvialableResultsLB(lbSysResults)

            Dim info As TaskInfo = TasksFacade.GetTaskInfo(TaskId)
            ddlActive.SelectedValue = info.Active.ToString()
            txtCode.Text = info.Code
            txtName.Text = info.Descrip
            ViewState("moddate") = info.ModDate ' need this to do a proper delete()

            ' select the module
            Try
                ddlModule.SelectedValue = info.ModuleId
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            ' select the entity
            Try
                If ddlModule.SelectedIndex <> 0 Then
                    TMCommon.BuildModuleEntitiesDDL(ddlModule.SelectedValue, ddlEntity)
                    ddlEntity.SelectedValue = info.ModuleEntityId
                    'If (info.ModuleEntityId = "1") Then
                    '    lblStatus.Text = "Link To Lead New Status"
                    'ElseIf info.ModuleEntityId = "2" Then
                    '    lblStatus.Text = "Link To Student Status"
                    'End If

                Else
                    ddlEntity.Items.Clear()
                    ddlStatus.Visible = False
                    lblStatus.Visible = False
                End If
                'select the Enrollment status
                If (ddlEntity.SelectedIndex <> 0 And (ddlEntity.SelectedValue = "2" Or ddlEntity.SelectedValue = "1")) Then
                    'TMCommon.BuildEntityStatusDDL(ddlEntity.SelectedValue, ddlStatus)
                    ddlStatus.Visible = True
                    lblStatus.Visible = True
                    TMCommon.BuildEntityStatusDDL(ddlEntity.SelectedValue, ddlStatus, campusId)

                    If (ddlEntity.SelectedValue = "1") Then
                        lblStatus.Text = "Link to Lead New Status"
                    ElseIf ddlEntity.SelectedValue = "2" Then
                        lblStatus.Text = "Link To Student Status"
                    End If

                    If (info.StatusId <> Nothing) Then ddlStatus.SelectedValue = info.StatusId()


                Else
                    ddlStatus.Items.Clear()
                    ddlStatus.Visible = False
                    lblStatus.Visible = False
                End If

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            ' select the campus group
            Try
                ddlCampGroup.SelectedValue = info.CampGroupId
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            ' select the category
            Try
                ddlCategory.SelectedValue = info.CategoryId
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            ' update the possible results
            BindResults(TaskId)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Event handler for when the user clicks on the left datalist containing all the users.
    ''' In response, this handler should populate the info for a task
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlTasks_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlTasks.ItemCommand
        ' get the selected userid and store it in the viewstate object
        Dim taskid As String = e.CommandArgument.ToString
        ViewState("id") = taskid
        dlTasks.SelectedIndex = e.Item.ItemIndex

        ' do some error checking
        If taskid Is Nothing Or taskid = "" Then
            TMCommon.Alert(Me, "There was a problem displaying the details for this task.")
            Return
        End If

        ' bind the task to the form
        BindTask(taskid)

        ' Advantage specific stuff
        'CommonWebUtilities.SetStyleToSelectedItem(dlTasks, taskid, ViewState, Header1)
        InitButtonsForEdit()
        CommonWebUtilities.RestoreItemValues(dlTasks, taskid)
    End Sub

    ''' <summary>
    ''' Fills the Results listbox with values from the database for 
    ''' the specific task.
    ''' Note: You must fill up lbSysResults with all possible results before
    ''' calling this method.
    ''' </summary>
    ''' <param name="TaskId"></param>
    ''' <remarks></remarks>
    Protected Sub BindResults(ByVal TaskId As String)
        Try
            ' Add all the results for the given taskid
            lbResults.DataSource = TaskResultsFacade.GetTaskResults(TaskId)
            lbResults.DataValueField = "ResultId"
            lbResults.DataTextField = "Descrip"
            lbResults.DataBind()

            ' now, we need to remove all the results that at in lbResults
            ' from the lbSysResults so that the user cannot add results twice.
            Dim lb As ListItem
            For Each lb In lbResults.Items
                Dim lb2 As ListItem = lbSysResults.Items.FindByValue(lb.Value)
                If Not lb2 Is Nothing Then lbSysResults.Items.Remove(lb2)
            Next

            hlWorkflow.Visible = True
            Dim url As String = String.Format("ViewWorkflowPopup.aspx?TaskId={0}", TaskId)
            hlWorkflow.NavigateUrl = url
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Build a list of entities for the selected module
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModule.SelectedIndexChanged
        If ddlModule.SelectedIndex <> 0 Then
            TMCommon.BuildModuleEntitiesDDL(ddlModule.SelectedValue, ddlEntity)
            lblStatus.Visible = False
            ddlStatus.Visible = False
            ddlStatus.Items.Clear()
        Else
            ddlEntity.Items.Clear()
            ddlStatus.Items.Clear()
            ddlStatus.Visible = False
            lblStatus.Visible = False
        End If
    End Sub

    ''' <summary>
    ''' Move an item from the SysResults to the Results listbox.
    ''' That is, the item is removed from possible results in SysResults
    ''' and is added to the Results listbox.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAddResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddResult.Click
        Dim i As Integer = lbSysResults.SelectedIndex
        If i = -1 Then Return

        Dim TaskId As String = lbSysResults.SelectedItem.Value
        Dim Name As String = lbSysResults.SelectedItem.Text
        lbSysResults.Items.RemoveAt(i)
        lbResults.Items.Add(New ListItem(Name, TaskId))
    End Sub

    ''' <summary>
    ''' Move an item from the Results to the SysResults listbox.
    ''' That is, the item is removed from possible Results and
    ''' is added to the SysResults listbox
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelResult.Click
        Dim i As Integer = lbResults.SelectedIndex
        If i = -1 Then Return

        Dim TaskId As String = lbResults.SelectedItem.Value
        Dim Name As String = lbResults.SelectedItem.Text
        lbResults.Items.RemoveAt(i)
        lbSysResults.Items.Add(New ListItem(Name, TaskId))
    End Sub


    ''' <summary>
    ''' Validates the form for completeness.
    ''' If a required field has not been entered, the function returns false.
    ''' It also displays an error message through javascript.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ValidateForm() As Boolean
        If txtCode.Text = "" Then
            TMCommon.DisplayErrorMessage(Me, "Code is a required field.")
            Return False
        End If
        ' Mantis 7685, if code>12 chars, then add fails
        If txtCode.Text.Length > 12 Then
            TMCommon.DisplayErrorMessage(Me, "Code cannot be greater than 12 characters.")
            Return False
        End If
        If txtName.Text = "" Then
            TMCommon.DisplayErrorMessage(Me, "Description is a required field.")
            Return False
        End If
        If ddlCampGroup.SelectedIndex = 0 Then
            TMCommon.DisplayErrorMessage(Me, "Campus Group is a required field.")
            Return False
        End If
        If ddlCategory.SelectedIndex = 0 Then
            TMCommon.DisplayErrorMessage(Me, "Category is a required field.")
            Return False
        End If
        If Me.ddlEntity.SelectedIndex <= 0 Then
            TMCommon.DisplayErrorMessage(Me, "Entity is a required field.")
            Return False
        End If
        Return True
    End Function

    ''' <summary>
    ''' User wants to save the task
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            ' verify that the proper data has been entered
            If Not ValidateForm() Then Return

            Dim taskid As String = ViewState("id")
            Dim info As New TaskInfo
            ' determine if this is an update or a new task by looking at the 
            ' ViewState("id") which is set by passing a parameter to the page.
            If taskid Is Nothing Or taskid = "" Then
                info.IsInDB = False
                ViewState("id") = info.TaskId
            Else
                info.IsInDB = True
                info.TaskId = taskid
            End If
            info.Active = CType(ddlActive.SelectedValue, Boolean)
            info.CategoryId = Me.ddlCategory.SelectedValue
            info.CampGroupId = ddlCampGroup.SelectedValue
            info.Code = txtCode.Text
            info.Descrip = txtName.Text
            If ddlEntity.SelectedIndex = 0 Then
                info.ModuleEntityId = ""
            Else
                info.ModuleEntityId = ddlEntity.SelectedValue
            End If
            If (ddlStatus.SelectedIndex = 0) Then
                info.StatusId = ""
            Else
                info.StatusId = ddlStatus.SelectedValue
            End If

            ' Update the task and check the result
            If Not TasksFacade.UpdateTask(info, TMCommon.GetCurrentUserId()) Then
                TMCommon.Alert(Page, "The update/add operation failed")
                Return
            End If

            ' Update the Task Results for this Task.  First, start by clearing out(all)
            ' tasks that used to belong to it and then add each result one at a time.
            TaskResultsFacade.DeleteTaskResultsForTask(info.TaskId)
            Dim lb As ListItem
            For Each lb In lbResults.Items
                Dim trInfo As New TaskResultInfo
                trInfo.TaskId = info.TaskId
                trInfo.ResultId = lb.Value
                ' TODO: check for errors
                TaskResultsFacade.UpdateTaskResult(trInfo, TMCommon.GetCurrentUserId())
            Next

            ' update the pane on the left hand side
            If (ddlCategoryId.SelectedValue.ToString = "" And ddlModuleId.SelectedValue.ToString = "") Then
                BindTasks()
            Else
                BindTasks(ddlCategoryId.SelectedValue.ToString, ddlModuleId.SelectedValue.ToString)
            End If
            TMCommon.Alert(Page, "Task was saved successfully")

            ' Advantage specific stuff
            'CommonWebUtilities.SetStyleToSelectedItem(dlTasks, taskid, ViewState, Header1)
            InitButtonsForEdit()

            CommonWebUtilities.RestoreItemValues(dlTasks, taskid)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Delete a task
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim taskid As String = ViewState("id")
            If Not taskid Is Nothing AndAlso taskid <> "" Then
                Dim modDate As DateTime = ViewState("moddate")
                Dim res As String = TasksFacade.DeleteTask(taskid, modDate)
                If res <> "" Then
                    TMCommon.DisplayErrorMessage(Me, res)
                Else
                    ViewState("id") = Nothing
                    ResetForm()
                    ' show the tasks in the left pane
                    If (ddlCategoryId.SelectedValue.ToString = "" And ddlModuleId.SelectedValue.ToString = "") Then
                        BindTasks()
                    Else
                        BindTasks(ddlCategoryId.SelectedValue.ToString, ddlModuleId.SelectedValue.ToString)
                    End If

                    ' Advantage specific stuff
                    ' CommonWebUtilities.SetStyleToSelectedItem(Me.dlTasks, Guid.Empty.ToString, ViewState, Header1)
                    InitButtonsForLoad()
                End If
                CommonWebUtilities.RestoreItemValues(dlTasks, Guid.Empty.ToString)
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Me, "Error.  Record could not be deleted.")
        End Try
    End Sub

    ''' <summary>
    ''' This just clears the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ViewState("id") = Nothing
        ViewState("moddate") = Nothing
        ResetForm()

        ' Advantage specific stuff
        ' CommonWebUtilities.SetStyleToSelectedItem(Me.dlTasks, Guid.Empty.ToString, ViewState, Header1)
        InitButtonsForLoad()
        CommonWebUtilities.RestoreItemValues(dlTasks, Guid.Empty.ToString)
    End Sub

    ''' <summary>
    ''' Add a new result to the database
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAddNewResult_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNewResult.Click
        Try
            ' make sure that the result description is not empty
            If txtNewResult.Text <> "" Then
                ' first add the result to the db
                Dim info As New FAME.AdvantageV1.Common.TM.ResultInfo
                info.Code = "[NONE]" ' assign a generic code
                info.Descrip = txtNewResult.Text
                If ResultsFacade.UpdateResult(info, TMCommon.GetCurrentUserId()) Then

                    lbResults.Items.Add(New ListItem(info.Descrip, info.ResultId))
                Else
                    TMCommon.DisplayErrorMessage(Me, "Failed to add the result to the database")
                End If
                txtNewResult.Text = ""
            Else
                TMCommon.Alert(Me, "Description for result cannot be empty")
            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Me, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Display active, inactive or all based on the user checking the radio box option
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        InitButtonsForLoad()
        ResetForm()
        If (ddlCategoryId.SelectedValue.ToString = "" And ddlModuleId.SelectedValue.ToString = "") Then
            BindTasks()
        Else
            BindTasks(ddlCategoryId.SelectedValue.ToString, ddlModuleId.SelectedValue.ToString)
        End If
    End Sub

    ''' <summary>
    ''' Allows the user to quickly add a category if one is not present
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnAddCategory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCategory.Click
        ' first checkf that the user entered a category
        If Me.txtCategory.Text = "" Then
            TMCommon.Alert(Me, "Please enter the category description in the textbox before clicking the Add button.")
            Return
        End If

        ' build the info class and add it to the db
        Dim info As New CategoryInfo
        info.Code = "[NONE]" ' assign a default code
        info.Descrip = txtCategory.Text
        'info.CampGroupId = TMCommon.GetCampusID()
        If CategoriesFacade.UpdateCategory(info, TMCommon.GetCurrentUserId()) Then
            ' category was added correctly
            ' clear the textbox and add it to the drop down list and make it the selected(one)
            txtCategory.Text = ""
            ddlCategory.Items.Add(New ListItem(info.Descrip, info.CategoryId))
            ddlCategory.SelectedValue = info.CategoryId
        Else
            TMCommon.DisplayErrorMessage(Me, "Failed to add category")
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

    Protected Sub ddlEntity_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEntity.SelectedIndexChanged
        'If (ddlEntity.SelectedIndex <> 0 And ddlEntity.SelectedValue = "2") Then
        If (ddlEntity.SelectedIndex <> 0 And (ddlEntity.SelectedValue = "2" Or ddlEntity.SelectedValue = "1")) Then
            ddlStatus.Visible = True
            lblStatus.Visible = True
            If (ddlEntity.SelectedValue = "1") Then
                lblStatus.Text = "Link to Lead New Status"
            ElseIf ddlEntity.SelectedValue = "2" Then
                lblStatus.Text = "Link To Student Status"
            End If
            TMCommon.BuildEntityStatusDDL(ddlEntity.SelectedValue, ddlStatus, campusId)
            'TMCommon.BuildEntityStatusDDL(ddlEntity.SelectedValue, ddlStatus, ddlCampGroup.SelectedValue)
            'TMCommon.BuildEntityStatusDDL(ddlEntity.SelectedValue, ddlStatus)
        Else
            ddlStatus.ClearSelection()
        End If
    End Sub
    Private Sub PopulateLHS()
        TMCommon.BuildCategoryDDL(TMCommon.GetCampusID(), ddlCategoryId)
        TMCommon.BuildModulesDDL(Nothing, ddlModuleId)
        'ddlModuleId.Items.RemoveAt(1)
    End Sub

    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        BindTasks(ddlCategoryId.SelectedValue.ToString, ddlModuleId.SelectedValue.ToString)
        ResetForm()
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

    Protected Sub ddlCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCategory.SelectedIndexChanged

    End Sub
End Class

