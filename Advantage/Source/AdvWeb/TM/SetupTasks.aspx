<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SetupTasks.aspx.vb" Inherits="AdvantageApp_TM_SetupTasks" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlTasks">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlTasks" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlTasks" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlTasks" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="15%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radStatus" CssClass="label" AutoPostBack="true" runat="Server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Active" Selected="True" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                            <p></p>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background: transparent;">
                                <tr>
                                    <td width="15%" nowrap align="left">
                                        <asp:Label ID="lblCategoryId" runat="server" CssClass="label">Category</asp:Label></td>
                                    <td class="contentcell4" width="85%" nowrap style="background: transparent;" align="left">
                                        <asp:DropDownList ID="ddlCategoryId" runat="server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%" nowrap align="left">
                                        <asp:Label ID="lblModuleId" runat="server" CssClass="label">Module</asp:Label></td>
                                    <td class="contentcell4" width="85%" style="background: transparent;" align="left">
                                        <asp:DropDownList ID="ddlModuleId" runat="server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%" nowrap align="left"></td>
                                    <td class="contentcell4" width="85%" style="background: transparent;" align="left">
                                        <asp:Button ID="btnBuildList" runat="server" Text="Build List" Width="130"></asp:Button>
                                    </td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlTasks" DataKeyField="TaskId" runat="server" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" ImageUrl="~/images/Inactive.gif" runat="server"
                                            Visible='<%# (Not Ctype(Container.DataItem("Active"), Boolean)).ToString %>' CommandArgument='<%# Container.DataItem("TaskId")%>' CausesValidation="False"></asp:ImageButton>
                                        <asp:ImageButton ID="imgActive" ImageUrl="~/images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Active"), Boolean).ToString %>' CommandArgument='<%# Container.DataItem("TaskId")%>' CausesValidation="False"></asp:ImageButton>
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("Active")%>'></asp:Label>
                                        <asp:LinkButton Text='<%# Container.DataItem("Descrip")%>' runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("TaskId")%>' ID="Linkbutton1" CausesValidation="False"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="98%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%;" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->

                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <!-- End of Atuo Create Next Tasks -->
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCode" runat="server" CssClass="label">Code <span style="color: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCode" runat="server" CssClass="textbox" TabIndex="1" MaxLength="20"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblActivePrompt" runat="server" CssClass="label">Status <span style="color: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlActive" runat="server" CssClass="dropdownlist" TabIndex="2" /></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblDescrip" runat="server" CssClass="label">Description <span style="color: red">*</span></asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtName" runat="server" CssClass="textbox" TabIndex="3" MaxLength="100"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGroup" runat="server" CssClass="label">Campus Groups <span style="color: 
red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGroup" runat="server" CssClass="dropdownlist"
                                                    TabIndex="4">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblcategory" runat="server" CssClass="label">Category<span style="color: red">*</span></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="dropdownlist"
                                                    TabIndex="5">
                                                </asp:DropDownList>
                                                <asp:Button ID="btnAddCategory" runat="server" Text="<- Add" />
                                                <asp:TextBox ID="txtCategory" runat="server" CssClass="textbox" TabIndex="6" MaxLength="50"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" align="left" valign="top" style="height: 27px">
                                                <asp:Label ID="LblModule" runat="server" CssClass="label">Module<span style="color: #ff0000">*</span></asp:Label></td>
                                            <td align="left" class="contentcell4" style="height: 27px">
                                                <asp:DropDownList ID="ddlModule" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                                    TabIndex="7">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" align="left" valign="top">
                                                <asp:Label ID="LblEntity" runat="server" CssClass="label">Entity<span style="color: #ff0000">*</span></asp:Label></td>
                                            <td align="left" class="contentcell4">
                                                <asp:DropDownList ID="ddlEntity" runat="server" CssClass="dropdownlist" TabIndex="8"
                                                    AutoPostBack="True">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" align="left" valign="top">
                                                <asp:Label ID="lblStatus" runat="server" CssClass="label" Visible="false">Link To Student Status</asp:Label></td>
                                            <td align="left" class="contentcell4">
                                                <asp:DropDownList ID="ddlStatus" runat="server" CssClass="dropdownlist" TabIndex="8"
                                                    AutoPostBack="false" Visible="false">
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                        <tr>
                                            <td class="threecolumnheaderreg">
                                                <asp:Label ID="lblAvailresults" CssClass="labelbold" runat="server" Font-Bold="True">Available Results</asp:Label></td>
                                            <td class="threecolumnspacerreg"></td>
                                            <td class="threecolumnheaderreg">
                                                <asp:Label ID="lblresults" CssClass="labelbold" runat="server" Font-Bold="True">Results</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="threecolumncontentreg" style="text-align: center">
                                                <asp:ListBox ID="lbSysResults" runat="server" CssClass="listboxes" Height="150px"
                                                    TabIndex="9"></asp:ListBox></td>
                                            <td class="threecolumnbuttonsreg">
                                                <div style="margin: 10px;">
                                                    <telerik:RadButton ID="btnAddResult" runat="server" Text="Add -->" TabIndex="10" Width="100px" />
                                                </div>
                                                <div style="margin: 10px;">
                                                    <telerik:RadButton ID="btnDelResult" runat="server" Text="<-- Remove"
                                                        TabIndex="11" Width="100px" />
                                                </div>

                                            </td>
                                            <td class="threecolumncontentreg" style="text-align: center">
                                                <asp:ListBox ID="lbResults" runat="server" CssClass="listboxes" Height="150px" TabIndex="12"></asp:ListBox></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" style="width: 100%; text-align: center; padding: 10px 0 10px 0">
                                                <asp:Label ID="Label1" runat="server" CssClass="label">Result not in the list? Add a result quickly 
here.</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <asp:Button ID="btnAddNewResult" runat="server" Text="Add New Result" Width="150px"
                                                    UseSubmitBehavior="False" />&nbsp
                                                    <asp:TextBox ID="txtNewResult" runat="server" CssClass="textbox" Width="170px" MaxLength="100" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="Label" align="center">
                                                <asp:HyperLink ID="hlWorkflow" runat="server" Text="Click here to get a graphical view of how this task relates to other tasks in the system." CssClass="label" Visible="false" Target="_blank" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
            ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators"
            runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    </div>
</asp:Content>


