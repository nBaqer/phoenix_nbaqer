﻿Imports Telerik.Web.UI
Imports Fame.AdvantageV1.Common.TM
Imports System.Data

Partial Class TM_ViewScheduleTest
    Inherits System.Web.UI.Page
    Dim DtTaskDetails As DataTable

    Protected Sub RadScheduler1_AppointmentCommand(ByVal sender As Object, ByVal e As Telerik.Web.UI.AppointmentCommandEventArgs) Handles RadScheduler1.AppointmentCommand
        If e.CommandName = "Insert" OrElse e.CommandName = "More" Then
            Dim taskddl As DropDownList = DirectCast(e.Container.FindControl("taskddl"), DropDownList)
            Dim Message As TextBox = DirectCast(e.Container.FindControl("txtMessage"), TextBox)
            Dim taskID As String
            taskID = taskddl.SelectedItem.Value
            e.Container.Appointment.Subject = taskddl.SelectedItem.Text
            e.Container.Appointment.Attributes.Add("TaskID", taskID)
            e.Container.Appointment.Attributes.Add("Message", Message.Text)

        ElseIf e.CommandName = "Update" Then
            Dim taskddl As DropDownList = DirectCast(e.Container.FindControl("taskddl"), DropDownList)
            Dim Message As TextBox = DirectCast(e.Container.FindControl("txtMessage"), TextBox)
            Dim taskID As String
            taskID = taskddl.SelectedItem.Value
            e.Container.Appointment.Subject = taskddl.SelectedItem.Text
            e.Container.Appointment.Attributes.Add("Message", Message.Text)
            e.Container.Appointment.Attributes.Add("TaskID", taskID)
            Dim startInput As RadDateInput = DirectCast(e.Container.FindControl("StartInput"), RadDateInput)
            e.Container.Appointment.Start = startInput.SelectedDate

            Dim endInput As RadDateInput = DirectCast(e.Container.FindControl("EndInput"), RadDateInput)
            e.Container.Appointment.End = endInput.SelectedDate
        End If
    End Sub

    Protected Sub RadScheduler1_AppointmentDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.SchedulerEventArgs) Handles RadScheduler1.AppointmentDataBound
        e.Appointment.ToolTip = e.Appointment.Subject + ": " + e.Appointment.Attributes("Message")

    End Sub

    Protected Sub RadScheduler1_AppointmentDelete(ByVal sender As Object, ByVal e As Telerik.Web.UI.SchedulerCancelEventArgs) Handles RadScheduler1.AppointmentDelete
        Try
            Dim tm As New Fame.AdvantageV1.BusinessFacade.TaskManager
            Dim tmFac As New Fame.AdvantageV1.BusinessFacade.TM.UserTasksFacade

            Dim usertaskid As String = ""

            Dim utInfo As New UserTaskInfo
            ' determine if this is an update or a new task by looking
            ' at the ViewState("usertaskid") which is set by passing a parameter to 
            ' the page.

            utInfo.IsInDB = True
            utInfo.AssignedById = "A11CB992-2538-49A6-A726-6FD8DAD050FC" ''TM.TMCommon.GetCurrentUserId()
            utInfo.UserTaskId = e.Appointment.ID.ToString
            utInfo.TaskId = e.Appointment.Attributes("TaskID")
            utInfo.StartDate = e.Appointment.Start
            utInfo.EndDate = e.Appointment.End
            utInfo.Message = e.Appointment.Attributes("Message")
            utInfo.OwnerId = tm.GetUserIdFromUserName(e.Appointment.Resources(0).Text)


            ' Update the task and check the result
            If Not Fame.AdvantageV1.BusinessFacade.TM.UserTasksFacade.DeleteUserTaks(utInfo.UserTaskId) Then
                'Alert(Page, "The update/add operation failed")
                Return
            Else
                bindscheduler()
            End If


        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            '   TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub



    Protected Sub RadScheduler1_AppointmentInsert(ByVal sender As Object, ByVal e As Telerik.Web.UI.SchedulerCancelEventArgs) Handles RadScheduler1.AppointmentInsert
        Try
            Dim tm As New Fame.AdvantageV1.BusinessFacade.TaskManager
            Dim tmFac As New Fame.AdvantageV1.BusinessFacade.TM.UserTasksFacade

            Dim usertaskid As String = ""

            Dim utInfo As New UserTaskInfo
            ' determine if this is an update or a new task by looking
            ' at the ViewState("usertaskid") which is set by passing a parameter to 
            ' the page.

            utInfo.IsInDB = False
            utInfo.AssignedById = "A11CB992-2538-49A6-A726-6FD8DAD050FC" ''TM.TMCommon.GetCurrentUserId()

            utInfo.TaskId = e.Appointment.Attributes("TaskID")
            utInfo.StartDate = e.Appointment.Start
            utInfo.EndDate = e.Appointment.End
            utInfo.Message = e.Appointment.Attributes("Message")
            utInfo.OwnerId = tm.GetUserIdFromUserName(e.Appointment.Resources(0).Text)


            ' Update the task and check the result
            If Not Fame.AdvantageV1.BusinessFacade.TM.UserTasksFacade.UpdateUserTask(utInfo, "A11CB992-2538-49A6-A726-6FD8DAD050FC") Then
                'Alert(Page, "The update/add operation failed")
                Return
            Else
                bindscheduler()
            End If


        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            '   TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try

    End Sub

    Protected Sub RadScheduler1_AppointmentUpdate(ByVal sender As Object, ByVal e As Telerik.Web.UI.AppointmentUpdateEventArgs) Handles RadScheduler1.AppointmentUpdate
        Try
            Dim tm As New Fame.AdvantageV1.BusinessFacade.TaskManager
            Dim tmFac As New Fame.AdvantageV1.BusinessFacade.TM.UserTasksFacade

            Dim usertaskid As String = ""

            Dim utInfo As New UserTaskInfo
            ' determine if this is an update or a new task by looking
            ' at the ViewState("usertaskid") which is set by passing a parameter to 
            ' the page.

            utInfo.IsInDB = True
            utInfo.AssignedById = "A11CB992-2538-49A6-A726-6FD8DAD050FC" ''TM.TMCommon.GetCurrentUserId()
            'utInfo.UserTaskId = e.Appointment.ID.ToString
            'utInfo.TaskId = e.Appointment.Attributes("TaskID")
            'utInfo.StartDate = e.Appointment.Start
            'utInfo.EndDate = e.Appointment.End
            'utInfo.Message = e.Appointment.Attributes("Message")
            'utInfo.OwnerId = tm.GetUserIdFromUserName(e.Appointment.Resources(0).Text)


            utInfo.UserTaskId = e.ModifiedAppointment.ID.ToString
            utInfo.TaskId = e.ModifiedAppointment.Attributes("TaskID")
            utInfo.StartDate = e.ModifiedAppointment.Start
            utInfo.EndDate = e.ModifiedAppointment.End
            utInfo.Message = e.ModifiedAppointment.Attributes("Message")
            utInfo.OwnerId = tm.GetUserIdFromUserName(e.ModifiedAppointment.Resources(0).Text)



            ' Update the task and check the result
            If Not Fame.AdvantageV1.BusinessFacade.TM.UserTasksFacade.UpdateUserTask(utInfo, "A11CB992-2538-49A6-A726-6FD8DAD050FC") Then
                'Alert(Page, "The update/add operation failed")
                Return
            Else
                bindscheduler()
            End If


        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            '   TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try

    End Sub





    Protected Sub RadScheduler1_FormCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.SchedulerFormCreatedEventArgs) Handles RadScheduler1.FormCreated
        Dim scheduler As RadScheduler = DirectCast(sender, RadScheduler)
        Dim tm As New Fame.AdvantageV1.BusinessFacade.TaskManager
        If e.Container.Mode = SchedulerFormMode.Insert Then
            Dim taskddl As DropDownList = DirectCast(e.Container.FindControl("taskddl"), DropDownList)
            taskddl.DataSource = tm.GetTaskDetails
            taskddl.DataBind()

        ElseIf e.Container.Mode = SchedulerFormMode.Edit Then
            Dim taskddl As DropDownList = DirectCast(e.Container.FindControl("taskddl"), DropDownList)
            taskddl.DataSource = tm.GetTaskDetails
            taskddl.DataBind()
            taskddl.SelectedItem.Text = e.Appointment.Subject
            taskddl.SelectedItem.Value = e.Appointment.ID.ToString

            Dim subjectBox As TextBox = DirectCast(e.Container.FindControl("txtMessage"), TextBox)
            subjectBox.Text = e.Appointment.Attributes("Message")
        ElseIf e.Container.Mode = SchedulerFormMode.AdvancedEdit Then
            Dim taskddl As DropDownList = DirectCast(e.Container.FindControl("taskddl"), DropDownList)
            taskddl.DataSource = tm.GetTaskDetails
            taskddl.DataBind()
            taskddl.SelectedItem.Text = e.Appointment.Subject
            taskddl.SelectedItem.Value = e.Appointment.Attributes("TaskID")

            Dim subjectBox As TextBox = DirectCast(e.Container.FindControl("txtMessage"), TextBox)
            subjectBox.Text = e.Appointment.Attributes("Message")

            Dim startInput As RadDateInput = DirectCast(e.Container.FindControl("StartInput"), RadDateInput)
            startInput.DateFormat = scheduler.AdvancedForm.DateFormat + " " + scheduler.AdvancedForm.TimeFormat
            startInput.SelectedDate = RadScheduler1.DisplayToUtc(e.Appointment.Start)

            Dim endInput As RadDateTimePicker = DirectCast(e.Container.FindControl("EndInput"), RadDateTimePicker)
            '  endInput.DateFormat = scheduler.AdvancedForm.DateFormat + " " + scheduler.AdvancedForm.TimeFormat
            endInput.SelectedDate = RadScheduler1.DisplayToUtc(e.Appointment.[End])
        ElseIf e.Container.Mode = SchedulerFormMode.AdvancedInsert Then
            Dim taskddl As DropDownList = DirectCast(e.Container.FindControl("taskddl"), DropDownList)
            taskddl.DataSource = tm.GetTaskDetails
            taskddl.DataBind()
            Dim startInput As RadDateInput = DirectCast(e.Container.FindControl("StartInput"), RadDateInput)
            startInput.DateFormat = scheduler.AdvancedForm.DateFormat + " " + scheduler.AdvancedForm.TimeFormat
            startInput.SelectedDate = RadScheduler1.DisplayToUtc(e.Appointment.Start)

            taskddl.SelectedItem.Text = e.Appointment.Subject
            taskddl.SelectedItem.Value = e.Appointment.Attributes("TaskID")

            Dim subjectBox As TextBox = DirectCast(e.Container.FindControl("txtMessage"), TextBox)
            subjectBox.Text = e.Appointment.Attributes("Message")

            Dim endInput As RadDateInput = DirectCast(e.Container.FindControl("EndInput"), RadDateInput)
            endInput.DateFormat = scheduler.AdvancedForm.DateFormat + " " + scheduler.AdvancedForm.TimeFormat
            endInput.SelectedDate = RadScheduler1.DisplayToUtc(e.Appointment.[End])

        End If
    End Sub


    'Protected Sub RadScheduler1_ResourceHeaderCreated(sender As Object, e As Telerik.Web.UI.ResourceHeaderCreatedEventArgs) Handles RadScheduler1.ResourceHeaderCreated
    '    Dim ResourceImageWrapper As Panel = TryCast(e.Container.FindControl("ResourceImageWrapper"), Panel)
    '    '  ResourceImageWrapper.CssClass = "Resource" + e.Container.Resource.Key.ToString()

    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cmpId As String

        If Not Page.IsPostBack Then

            cmpId = Session("campusid")
            Dim Filter As String = ""
            Try
                Filter = Request.QueryString("f")
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Filter = "task assigned to me"
            End Try

            If Filter = "" Or Filter = Nothing Then
                Filter = "task assigned to me"
            End If

            'If Filter.ToString.ToLower = "tasks assigned to others" Then
            '    ' get all the tasks for this user and save it in the viewstate
            '    Dim ds As DataSet = UserTasksFacade.GetUserTasks(cmpId, Nothing, Nothing, _
            '                        Nothing, Nothing, TaskStatus.None, Nothing, Nothing, "others", TM.TMCommon.GetCurrentUserId())
            '    ViewState("usertasks") = ds
            '    BindPage()
            'Else
            '    Dim ds As DataSet = UserTasksFacade.GetUserTasks(cmpId, Nothing, TM.TMCommon.GetCurrentUserId(), _
            '                        Nothing, Nothing, TaskStatus.None, Nothing, Nothing)
            '    ViewState("usertasks") = ds
            '    BindPage()
            'End If
            bindscheduler()
        End If

    End Sub

    Private Sub bindscheduler()
        Dim dt As DataTable
        Dim DtResourceUserNames As DataTable



        Dim tm As New Fame.AdvantageV1.BusinessFacade.TaskManager
        dt = tm.GetAdmissionRepSchedule


        DtResourceUserNames = tm.GetAdmissionRepUserDetails("A11CB992-2538-49A6-A726-6FD8DAD050FC")


        '   RadScheduler1.ResourceTypes.Add(rd)
        RadScheduler1.DataSource = dt
        RadScheduler1.ResourceTypes(0).DataSource = DtResourceUserNames
        RadScheduler1.DataBind()
    End Sub
End Class
