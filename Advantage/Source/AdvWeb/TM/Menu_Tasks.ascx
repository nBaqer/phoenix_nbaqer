<%@ Control Language="VB" ClassName="TaskMenu" %>
<%@ Import Namespace="FAME.AdvantageV1.BusinessFacade.TM" %>
<link href="../css/TM.css" rel="stylesheet" type="text/css" />

<script runat="server">    

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Page.IsPostBack Then
            ' build the predefined filters
            BuildFiltersDDL(ddlFilters)
            Dim filter As String = Request.Params("filter")
            Dim predef As String = Request.Params("predef")

            ' default to tasks that are assigned to the current user if
            ' no paramter has been passed
            If (filter Is Nothing Or filter = "") AndAlso (predef Is Nothing Or predef = "") Then
                ddlFilters.SelectedValue = "Tasks assigned to me"                                                                 
            End If
            If Not (predef Is Nothing Or predef = "") Then
                ddlFilters.SelectedValue = predef
            End If 
                        
            ' add javascript to popup the filter window
            Dim js As String = TMCommon.GetJavsScriptPopup("FilterUserTasks.aspx", 500, 350, "taskMenu_hfResult")
            Menu3.Attributes.Add("onclick", js)
        End If
    End Sub

    Protected Sub Menu3_MenuItemClick(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.MenuEventArgs)
        If hfResult.Value.Length = 0 Then Return
        ddlFilters.SelectedIndex = 0
        Dim url As String = "ViewUserTasks.aspx?filter=" + Server.HtmlEncode(hfResult.Value)
        Response.Redirect(url)
    End Sub

    Protected Sub ddlFilters_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim url As String = "ViewUserTasks.aspx?predef=" + ddlFilters.SelectedValue
        Response.Redirect(url)
    End Sub
    
    ''' <summary>
    ''' build some pre-defined filters that can be selected by the user
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Protected Sub BuildFiltersDDL(ByRef ddl As DropDownList)
        ddl.Items.Add(new ListItem("--Select Filter--",""))
        ddl.Items.Add("Tasks due today")
        ddl.Items.Add("Tasks due in 1 week")
        ddl.Items.Add("Overdue tasks")
        ddl.Items.Add("High priority tasks")
        ddl.Items.Add("Tasks assigned to me")
        ddl.Items.Add("Tasks I assigned to others")
        ddl.Items.Add("Completed in the last week")
    End Sub
        
</script>

<div style="padding: 0; margin: 0">
    <table summary="SysMenu" width="100%" align="left" cellpadding="0" cellspacing="0">
        <tr>
            <td class="MenuFrame" nowrap>
                <div class="calendarmenuitems">
                    <asp:Menu ID="Menu1" runat="server" DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false"
                        Orientation="Horizontal">
                        <StaticMenuStyle />
                        <StaticMenuItemStyle CssClass="taskbuttons" />
                        <DynamicMenuStyle />
                        <DynamicMenuItemStyle />
                        <DynamicHoverStyle />
                        <Items>
                            <asp:MenuItem NavigateUrl="AddUserTask.aspx" Text="New Task" Value="New Task" />
                        </Items>
                    </asp:Menu>
                </div>
                <div class="calendarmenuitems">
                    <asp:Menu ID="Menu2" runat="server" DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false"
                        Orientation="Horizontal">
                        <StaticMenuStyle />
                        <StaticMenuItemStyle CssClass="taskbuttons" />
                        <DynamicMenuStyle />
                        <DynamicMenuItemStyle />
                        <DynamicHoverStyle />
                        <Items>
                            <asp:MenuItem NavigateUrl="ViewUserTasks.aspx" Text="Reset Filter" Value="Reset Filter" />
                        </Items>
                    </asp:Menu>
                </div>
                <div class="calendarmenuitems">
                    <asp:Menu ID="Menu3" runat="server" DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false"
                        Orientation="Horizontal" OnMenuItemClick="Menu3_MenuItemClick">
                        <StaticMenuStyle />
                        <StaticMenuItemStyle CssClass="taskbuttons" />
                        <DynamicMenuStyle />
                        <DynamicMenuItemStyle />
                        <DynamicHoverStyle ForeColor="White" />
                        <Items>
                            <asp:MenuItem Text="Advanced Filter" Value="Adv Filter" />
                        </Items>
                    </asp:Menu>
                </div>
                <div style="float: left; width: 203px; margin: 5px 0 0 0; padding: 0; text-align: left">
                    <asp:DropDownList ID="ddlFilters" runat="server" Width="195px" Height="20px" AutoPostBack="True" Font-Size="Small"
                        CssClass="dropdowntaskmenu" OnSelectedIndexChanged="ddlFilters_SelectedIndexChanged" />
                </div>
                <div class="taskbgrightdiv">
                    &nbsp;</div>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfResult" runat="server" />
</div>
