' ===============================================================================
' ViewWorkflow
'   Views workflows
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common.TM

Partial Class TM_ViewWorkflow
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Check the parameters passed to the page
        filterStatus.Value = TaskStatus.Pending
        Dim filter As String = Request.Params("filter")
        If Not filter Is Nothing Then
            'RunFilter(filter)
        Else
            ResetFilters()
            BindWorkflows()
        End If
    End Sub

    ''' <summary>
    ''' Show all user tasks for the current user
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindWorkflows()
        Try
            Dim ds As DataSet = UserTasksFacade.GetWorkflows(TMCommon.GetCampusID(), _
                                                             Nothing, _
                                                             TMCommon.GetCurrentUserId(), _
                                                             filterReId.Value, _
                                                             filterPriority.Value, _
                                                             CType(filterStatus.Value, TaskStatus), _
                                                             filterDueDateMin.Value, _
                                                             filterDueDateMax.Value)
            rptWorkflows.DataSource = ds
            rptWorkflows.DataBind()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
        ' Hide or display the "No Tasks" message
        If rptWorkflows.Items.Count = 0 Then
            scrolldiv.Visible = False
            lblTasksMsgs.Visible = True
            'lbFilter.Visible = False
        Else
            scrolldiv.Visible = True
            lblTasksMsgs.Visible = False
            'lbFilter.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' Resets all the hidden fields that make up a filter
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetFilters()
        ' start with showing on tasks that have not been started
        Me.filterAssignedToId.Value = ""
        Me.filterDueDateMax.Value = ""
        Me.filterDueDateMin.Value = ""
        Me.filterEntityId.Value = ""
        Me.filterReId.Value = ""
        Me.filterPriority.Value = ""
        Me.filterReType.Value = ""
        Me.filterStatus.Value = TaskStatus.Pending
    End Sub

    ''' <summary>
    ''' Called in response to the user wanting to filter the list
    ''' of tasks shown on the repeater.
    ''' Before this method is called, FilterUserTasks.aspx has already
    ''' been popped up and the result of that popup is stored in hfResult.
    ''' For more info on the format of hrResult, see FilterUserTasks.aspx.vb.
    ''' </summary>
    ''' <param name="filter"></param>
    ''' <remarks></remarks>
    Protected Sub RunFilter(ByVal filter As String)
        ' reset the filter and the dropdownlist to reflect that there is not 
        ' pre-defined filter set
        ResetFilters()

        ' parse the returned filter string 
        Dim filters As String() = filter.Split("|")
        filterStatus.Value = filters(0)
        filterPriority.Value = filters(1)
        filterReId.Value = filters(2)
        filterAssignedToId.Value = filters(3)
        filterEntityId.Value = filters(4)

        If filterAssignedToId.Value Is Nothing Or filterAssignedToId.Value = "" Then
            filterAssignedToId.Value = TMCommon.GetCurrentUserId()
        End If

        ' rebind the user tasks control
        BindWorkflows()
    End Sub

    ''' <summary>
    ''' Called for each item in the repeater.
    ''' Gives us the change to set the images and other text values correctly
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rptWorkflows_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptWorkflows.ItemDataBound
        Try
            ' set the status to the correct string
            Dim lb As Label = e.Item.FindControl("lblStatus")
            If Not lb Is Nothing Then
                If lb.Text = TaskStatus.Cancelled Then
                    lb.Text = "Cancelled"
                ElseIf lb.Text = TaskStatus.Completed Then
                    lb.Text = "Completed"
                ElseIf lb.Text = TaskStatus.Pending Then
                    lb.Text = "Pending"
                Else
                    lb.Text = "N/A"
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

    'Protected Sub ddlTasks_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTasks.SelectedIndexChanged
    'If ddlTasks.SelectedIndex <> 0 Then

    'Dim sb As String = String.Format("<iframe src=""GetWorkflowXml.aspx?taskid={0}"" Width=500 Height=500 />", _
    'Dim sb As String = String.Format("<embed src=""GetWorkflowXml.aspx?taskid={0}"" Width=500 Height=500 Type=""image/svg+xml"" pluginspage=""http://www.adobe.com/svg/viewer/install/"" />", _
    'ddlTasks.SelectedValue)
    'Dim sb As String = "<embed src=""test.svg"" width=""500"" height=""500"" Type=""image/svg+xml"" pluginspage=""http://www.adobe.com/svg/viewer/install/"" />"
    'Dim sb As String = String.Format("<embed src=""GetWorkflowXML.aspx?taskid={0}"" width=""500"" height=""500"" Type=""image/svg+xml"" pluginspage=""http://www.adobe.com/svg/viewer/install/"" />", _
    'ddlTasks.SelectedValue)
    'Me.lblHTML.Text = sb
    'End If
    'End Sub
End Class
