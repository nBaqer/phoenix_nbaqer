<%@ Register TagName="SysHeader" TagPrefix="Header" Src="~/usercontrols/TMHeader.ascx" %>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ContactLookup.aspx.vb" Inherits="AdvantageApp_MSG_RecipientLookup" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<head runat="server">
    <title>Contact Search</title>
    <meta http-equiv="pragma" content="NO-CACHE">
    <link href="../CSS/systememail.css" type="text/css" rel="stylesheet">
    <link href="../css/TM.css" type="text/css" rel="stylesheet">
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td><img src="../images/contact_lookup.jpg"></td>
                <td class="topemail">
                    <a class="close" onclick="top.close()" href=#>X Close</a></td>
            </tr>
        </table>
        <!-- search form -->
        <table cellpadding="0" cellspacing="0" width="70%" align="center" summary="Search Options Table">
            <tr>
                <td class="labelcell" style="vertical-align: middle; width: 30%; padding-top: 20px">
                    Contact type</td>
                <td class="contentcell" style="width: 70%; padding-top: 20px">
                    <asp:DropDownList ID="ddlContactType" runat="server" Width="100%" CssClass="DropDownListReq" /></td>
            </tr>
            <tr>
                <td class="labelcell" style="vertical-align: middle; width: 30%">
                    Contact Name</td>
                <td class="contentcell" style="width: 70%">
                    <asp:TextBox ID="txtSearch" CssClass="textboxreq" Width="100%" Visible="True" runat="server" /></td>
            </tr>
            <tr>
                <td class="labelcell" style="font-weight: bold;">
                    &nbsp;</td>
                <td class="contentcell" style="text-align: center">
                    <asp:Button ID="btnSearch"  Width="80px" runat="server" Text="Search" />
                    <asp:Button ID="btnCancel"  Width="80px" runat="server" Text="Cancel" />
                </td>
            </tr>
        </table>
        <!-- end search form -->
        <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="DetailsFrameTopemail">
                <div class="scrollwholelook">
                <table align="center" width="100%" border="0" bgcolor="white" cellpadding="0" cellspacing="0">
                     <tr>
                                        <th style="text-align: center">
                                            <h1>
                                                Search Results</h1>
                                        </th>
                                    </tr></table>
                        <asp:Repeater ID="rptResults" runat="server" OnItemDataBound="rptResults_OnItemDataBound">
                            <HeaderTemplate>
                                <table align="center" width="100%" border="0" bgcolor="white" cellpadding="0" cellspacing="0">  
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr onmouseover="this.style.backgroundColor='#e7edf3';" onmouseout="this.style.backgroundColor='';">
                                    <td style="border-style: solid; border-width: 1px; border-color: #83929f; padding: 0px">
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td class="labelcell">
                                                    <asp:LinkButton ID="lbFullName" runat="server" CausesValidation="False" CommandName='<%# Container.DataItem("Name") %>'
                                                        CommandArgument='<%# Container.DataItem("RecipientId") %>' Width="100%" Text='<%# Container.DataItem("Name") %>'></asp:LinkButton></td>
                                                <td class="contentcell" style="text-align: right">
                                                    <asp:LinkButton ID="lbSSN" runat="server" CommandName='<%# Container.DataItem("Name") %>'
                                                        CommandArgument='<%# Container.DataItem("RecipientId") %>' Width="100%" Text='<%# Container.DataItem("SSN") %>'></asp:LinkButton></td>
                                            </tr>
                                            <tr>
                                                <td class="labelcell">
                                                </td>
                                                <td class="contentcell">
                                                    <asp:LinkButton ID="lbProgram" runat="server" CommandName='<%# Container.DataItem("Name") %>'
                                                        CommandArgument='<%# Container.DataItem("RecipientId") %>' Text='<%# Container.DataItem("SecondIdDesc") %>'></asp:LinkButton></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
        </table>
          <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
    </form>
</body>
</html>
