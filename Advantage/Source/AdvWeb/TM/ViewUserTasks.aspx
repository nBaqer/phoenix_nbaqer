<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewUserTasks.aspx.vb" Inherits="AdvantageApp_TM_ViewUserTasks" %>
<%@ Register TagName="SysHeader" TagPrefix="Header" Src="~/usercontrols/TMHeader.ascx" %>
<%@ Register TagName="TaskMenu" TagPrefix="Menu" Src="Menu_Tasks.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>View Tasks Asignments</title>
    <link href="../css/TM.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-color: White;  overflow-y:auto;">
    <form id="form1" runat="server">
        <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="left">
                <Menu:TaskMenu ID="taskMenu" runat="server" />
            </td>
        </tr>
        <tr>
            <td style="padding: 10px">
                <asp:Label ID="lblTasksMsgs" runat="server" CssClass="MsgLabel" Text="No tasks defined.  Click Add Tasks"
                    Visible="false" /></td>
        </tr>
        </table>
        <div id="scrolldiv" runat="server" class="ScrolledDiv">            
        <asp:Repeater ID="rptUserTasks" runat="server">
            <HeaderTemplate>
                <table width="90%" cellpadding="0" cellspacing="0" class="Repeater" align="left">
                    <tr class="RepeaterHeader">
                        <td class="RepeaterHeaderLabel" />
                        <td class="RepeaterHeaderLabel" style="padding-left: 10px; padding-right: 10px">
                            Task Name</td>
                        <td class="RepeaterHeaderLabel" style="padding-left: 10px; padding-right: 10px">
                            Entity</td>
                        <td class="RepeaterHeaderLabel" style="padding-left: 10px; padding-right: 10px">
                            Contact</td>
                        <td class="RepeaterHeaderLabel" style="padding-left: 10px; padding-right: 10px">
                            Campus</td>
                        <td class="RepeaterHeaderLabel" style="padding-left: 10px; padding-right: 10px">
                            Start Date</td>
                        <td class="RepeaterHeaderLabel" style="padding-left: 10px; padding-right: 10px">
                            Status</td>
                        <td class="RepeaterHeaderLabel" style="padding-left: 10px; padding-right: 10px">
                            Owner</td>
                        <td class="RepeaterHeaderLabel" style="padding-right: 10px">&nbsp;</td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr bgcolor="white" onmouseover="this.style.backgroundColor='#e7edf3'" onmouseout="this.style.backgroundColor='white'">
                    <td class="RepeateritemLabel">
                        <asp:Image ID="imgPriority" runat="Server" Visible="false" ImageAlign="Middle" />
                        <asp:Label ID="lblPriority" runat="server" Visible="false" Text='<%# Container.DataItem("Priority").ToString() %>' />
                        <asp:HiddenField ID="hfReId" runat="server" Value='<%# Container.DataItem("ReId") %>' />
                    </td>    
                    <td class="RepeateritemLabel" style="padding-left: 10px">
                        <asp:HyperLink ID="hlDescrip" runat="server" Text='<%# Container.DataItem("TaskDescrip") %>'
                            NavigateUrl='<%# "AddUserTask.aspx?UserTaskId=" & Container.DataItem("UserTaskId").ToString() %>' />
                    </td>
                    <td class="RepeateritemLabel">
                        <asp:Label ID="lblEntity" runat="server" Text='<%# Container.DataItem("EntityName") %>' />
                    </td>
                    <td class="RepeateritemLabel">
                        <asp:ImageButton ID="ibHistory" runat="server" ImageUrl="~/images/TM/history.gif"
                            AlternateText="View History" Width="12" Height="12" />
                        <asp:Label ID="lblContact" runat="server" Text='<%# Container.DataItem("ReName") %>' />
                    </td>
                    <td class="RepeateritemLabel" nowrap style="padding-right: 10px;">
                        <asp:Label ID="lblCampDescrip" runat="server" Text='<%# Container.DataItem("CampusDescrip") %>' />
                    </td>
                    <td class="RepeateritemLabel" nowrap>
                        <asp:Label ID="lblStartDate" runat="server" Text='<%# Container.DataItem("StartDate") %>' />
                    </td>
                    <td class="RepeateritemLabel">
                        <asp:Label ID="lblStatus" runat="server" Text='<%# Container.DataItem("Status") %>' />
                    </td>
                    <td class="RepeateritemLabel">
                        <asp:Label ID="lblOwner" runat="server" Text='<%# Container.DataItem("OwnerName") %>' />
                    </td>
                    <td class="RepeateritemLabel" style="padding-right: 10px; text-align: center" nowrap>
                        <asp:HyperLink ID="hlView" runat="server" CssClass="graphicalview" Text="Graphical View" NavigateUrl='<%# "ViewWorkflowPopup.aspx?taskid=" & Container.DataItem("TaskId").ToString() %>' />
                    </td>
                </tr>
            </ItemTemplate>
            <SeparatorTemplate>
                <tr class="RepeaterSeparator" />
            </SeparatorTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
            </asp:Repeater>            
        </div>
        <asp:HiddenField ID="hfResult" runat="server" />
        <asp:HiddenField ID="filterReType" runat="server" />
        <asp:HiddenField ID="filterReId" runat="server" />
        <asp:HiddenField ID="filterPriority" runat="server" />
        <asp:HiddenField ID="filterAssignedById" runat="server" />
        <asp:HiddenField ID="filterAssignedToId" runat="server" />
        <asp:HiddenField ID="filterStatus" runat="server" />
        <asp:HiddenField ID="filterEntityId" runat="server" />
        <asp:HiddenField ID="filterDueDateMin" runat="server" />
        <asp:HiddenField ID="filterDueDateMax" runat="server" />
        <asp:HiddenField ID="filterOthers" runat="server" />
         <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    </form>
</body>
</html>
