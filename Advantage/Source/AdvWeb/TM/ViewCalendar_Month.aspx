<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewCalendar_Month.aspx.vb" Inherits="TM_ViewCalendary_Month" %>
<%@ Register TagName="SysHeader" TagPrefix="Header" Src="~/usercontrols/TMHeader.ascx" %>
<%@ Register TagName="CalendarMenu" TagPrefix="Menu" Src="Menu_Calendar.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Calendar [Monthly View]</title>
    <link href="../css/TM.css" rel="stylesheet" type="text/css" />    
</head>
<body>
    <form id="form1" runat="server">
    <div>

		<menu:CalendarMenu ID="sysMenu" runat="server" />
		<div class="AlignedDivCalendar">	
		<table id="Table2" width="100%" cellpadding="0" cellspacing="0">
        		<tr>
		    <th style="text-align: center"><h1>Calendar [Monthly View]</h1></th></tr>	
		<tr>
		    <td align="left" valign="top">
		    <div class="ScrolledDivCalendarMonth">
            <asp:Calendar ID="Calendar1" runat="server" CssClass="TMLabel"
                NextPrevFormat="FullMonth" 
                BorderColor="#83929f" BorderStyle="Solid" BorderWidth="1px" DayNameFormat="Full">
                <SelectedDayStyle BackColor="#00C0C0" Width="14%" ForeColor="Black" CssClass="TMLabel" />
                <TodayDayStyle BackColor="Wheat" Width="14%" ForeColor="Black" BorderColor="#83929f" BorderWidth="1px" CssClass="TMLabel" />
                <DayStyle VerticalAlign="Top" Wrap="False" HorizontalAlign="Right" 
                            BackColor="#f9fbe6" BorderColor="#83929f" BorderWidth="1px" 
                            Height="60px" Width="90px"
                             CssClass="TMLabel" />
                <OtherMonthDayStyle ForeColor="Black" BackColor="PapayaWhip" Width="90px" BorderColor="#83929f" BorderStyle="Solid"
                             BorderWidth="1px" HorizontalAlign="Right" VerticalAlign="Top" Wrap="False" CssClass="TMLabel" />
                <NextPrevStyle CssClass="TMLabelBold" />
                <DayHeaderStyle CssClass="TMLabel" Width="90px" BackColor="#CCCCCC" BorderColor="#83929f" BorderWidth="1px" Height="13px" Wrap="False" />
                <TitleStyle BackColor="#e7edf3" CssClass="TMLabelBold" Font-Size="11px"  />                
            </asp:Calendar>
            </div>      
            </td>
        </tr>
        </table>  
        </div>      
    </div>
    </form>
</body>
</html>
