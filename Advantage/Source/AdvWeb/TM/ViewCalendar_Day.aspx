<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewCalendar_Day.aspx.vb" Inherits="TM_ViewCalendar_Day" %>
<%@ Register TagName="SysHeader" TagPrefix="Header" Src="~/usercontrols/TMHeader.ascx" %>
<%@ Register TagName="CalendarMenu" TagPrefix="Menu" Src="Menu_Calendar.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Calendar [Daily View]</title>
    <link href="../css/TM.css" rel="stylesheet" type="text/css" />    
</head>
<body>
    <form id="form1" runat="server">
    <div>
		
		<menu:CalendarMenu ID="sysMenu" runat="server" /></div>		
		<div class="AlignedDivCalendar">
		<table id="Table2" width="100%" cellpadding="0" cellspacing="0">
		<tr>
		    <th style="text-align: center"><h1>Calendar [Daily View]</h1></th></tr>
		<tr>
			<td style="text-align: center; background-color:#dee2e7; border-left: 1px solid #83929f; border-right: 1px solid #83929f">
				<asp:Panel id="pnlHeader" Width="100%" Wrap="False" runat="server">
                <asp:LinkButton id="lnkPrev" runat="server" CssClass="LabelBoldcalendar" Text="<<" />
                <asp:Label id="lblDay" runat="server" CssClass="LabelBoldcalendar" />
                <asp:LinkButton id="lnkNext" runat="server" CssClass="LabelBoldcalendar" Text=">>" />
                </asp:Panel>
            </td>
		</tr>
		<tr>
			<td>			
			    <div class="ScrolledDivCalendar">
			    <asp:DataGrid id="dgDayView" Width="100%" runat="server" 
			        CellPadding="3" AutoGenerateColumns="False" ShowHeader="False">
			    <ItemStyle HorizontalAlign="Left" />
				<Columns>
					<asp:BoundColumn DataField="EventTime" DataFormatString="{0:t}">
						<ItemStyle CssClass="LabelBold" Wrap="False" HorizontalAlign="Right" BackColor="#e7edf3"
						    Width="16%" BorderColor="#83929f" BorderWidth="1px" BorderStyle=Solid />
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Events">
						<ItemStyle Wrap="False" Width="85%" BackColor="#f9fbe6" BorderColor="#83929f" 
						    BorderWidth="1px" BorderStyle="Solid" CssClass="Label" />
					</asp:BoundColumn>					
				</Columns>
				</asp:DataGrid>
				</div>    
			</td>
		</tr>
	    </table>
	    </div>
    
    </form>
</body>
</html>
