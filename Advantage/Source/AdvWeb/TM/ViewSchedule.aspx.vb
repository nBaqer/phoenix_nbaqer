﻿Imports Telerik.Web.UI
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common.TM
Imports System.Data

Partial Class TM_ViewSchedule
    Inherits System.Web.UI.Page
    Dim DtTaskDetails As DataTable
    Private Const AppointmentsLimit As Integer = 1


#Region "Events"


    Protected Sub RadScheduler1_AppointmentCommand(sender As Object, e As Telerik.Web.UI.AppointmentCommandEventArgs) Handles RadScheduler1.AppointmentCommand

        If e.CommandName = "Insert" OrElse e.CommandName = "More" Then
            Dim radtaskddl As RadComboBox = DirectCast(e.Container.FindControl("radtaskddl"), RadComboBox)
            Dim Message As RadTextBox = DirectCast(e.Container.FindControl("txtMessage"), RadTextBox)

            Dim taskID As String
            taskID = radtaskddl.SelectedItem.Value

            e.Container.Appointment.Subject = radtaskddl.SelectedItem.Text
            e.Container.Appointment.Attributes.Add("TaskID", taskID)
            e.Container.Appointment.Attributes.Add("Message", Message.Text)
            e.Container.Appointment.Attributes.Add("AssignedById", TMCommon.GetCurrentUserId().ToString)

            Dim errmsg As String = ""
            If taskID = "" Then
                errmsg = "Select a task."
                Dim errMessage As Label = DirectCast(e.Container.FindControl("lblerrmsg"), Label)
                errMessage.Text = errmsg
            End If
            If e.Container.Appointment.Start >= e.Container.Appointment.End Then
                errmsg = errmsg + " End time should be greater than start time."
                Dim errMessage As Label = DirectCast(e.Container.FindControl("lblerrmsg"), Label)
                errMessage.Text = errmsg
            End If
         


            Dim radpriorityddl As RadComboBox = DirectCast(e.Container.FindControl("radpriorityddl"), RadComboBox)
            If Not radpriorityddl Is Nothing Then
                e.Container.Appointment.Attributes.Add("Priority", radpriorityddl.SelectedItem.Value)
            End If

            'If ExceedsLimit(e.Container.Appointment) Then
            '    '    Dim WarMessage As Literal = DirectCast(e.Container.FindControl("literalWarningMessage"), Literal)
            '    literalWarningMessage.Text = "Another appointment exists in this time slot. "
            'End If

        ElseIf e.CommandName = "Update" Then
            Dim radtaskddl As RadComboBox = DirectCast(e.Container.FindControl("radtaskddl"), RadComboBox)
            Dim Message As RadTextBox = DirectCast(e.Container.FindControl("txtMessage"), RadTextBox)
            Dim radpriorityddl As RadComboBox = DirectCast(e.Container.FindControl("radpriorityddl"), RadComboBox)

            Dim taskID As String
            taskID = radtaskddl.SelectedItem.Value

            e.Container.Appointment.Subject = radtaskddl.SelectedItem.Text
            e.Container.Appointment.Attributes.Add("Message", Message.Text)
            e.Container.Appointment.Attributes.Add("TaskID", taskID)
            e.Container.Appointment.Attributes.Add("Priority", radpriorityddl.SelectedItem.Value)
            e.Container.Appointment.Attributes.Add("AssignedById", TMCommon.GetCurrentUserId().ToString)

            Dim startInput As RadDateTimePicker = DirectCast(e.Container.FindControl("StartInput"), RadDateTimePicker)
            e.Container.Appointment.Start = startInput.SelectedDate

            Dim endInput As RadDateTimePicker = DirectCast(e.Container.FindControl("EndInput"), RadDateTimePicker)
            e.Container.Appointment.End = endInput.SelectedDate

            Dim errmsg As String = ""
            If taskID = "" Then
                errmsg = "Select a task."
            End If
            If e.Container.Appointment.Start >= e.Container.Appointment.End Then
                errmsg = errmsg + " End time should be greater than start time."
            End If
            Dim errMessage As Label = DirectCast(e.Container.FindControl("lblerrmsg"), Label)
            errMessage.Text = errmsg
            'If ExceedsLimit(e.Container.Appointment) Then
            '    '    Dim WarMessage As Literal = DirectCast(e.Container.FindControl("literalWarningMessage"), Literal)
            '    literalWarningMessage.Text = "Another appointment exists in this time slot. "
            'End If

        End If
    End Sub

    Protected Sub RadScheduler1_AppointmentDataBound(sender As Object, e As Telerik.Web.UI.SchedulerEventArgs) Handles RadScheduler1.AppointmentDataBound

        e.Appointment.ToolTip = e.Appointment.Subject + ": " + e.Appointment.Attributes("Message") + ";priority-" + GetPriorityType(e.Appointment.Attributes("Priority")) + ";by- " + e.Appointment.Attributes("AssignedByName")

        If e.Appointment.Resources(0).Key <> Nothing And e.Appointment.Attributes("AssignedById") <> Nothing Then

            If e.Appointment.Attributes("AssignedById").ToString = TMCommon.GetCurrentUserId().ToString Then
                'e.Appointment.AllowEdit = True
                'e.Appointment.AllowDelete = True
                e.Appointment.CssClass = "rsCategoryGreen"

            ElseIf e.Appointment.Attributes("AssignedById").ToString = e.Appointment.Resources(0).Key.ToString Then
                e.Appointment.CssClass = "rsCategoryYellow"
                'e.Appointment.AllowEdit = False
                'e.Appointment.AllowDelete = False

            Else
                'e.Appointment.AllowEdit = False
                'e.Appointment.AllowDelete = False
                e.Appointment.CssClass = "rsCategoryRed"
            End If
        End If
    End Sub

    Protected Sub RadScheduler1_AppointmentDelete(sender As Object, e As Telerik.Web.UI.SchedulerCancelEventArgs) Handles RadScheduler1.AppointmentDelete
        Try
            Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
            Dim tmFac As New FAME.AdvantageV1.BusinessFacade.TM.UserTasksFacade

            Dim usertaskid As String = ""

            Dim utInfo As New UserTaskInfo
            utInfo.UserTaskId = e.Appointment.ID.ToString

            ' Update the task and check the result
            If Not FAME.AdvantageV1.BusinessFacade.TM.UserTasksFacade.DeleteUserTaks(utInfo.UserTaskId) Then
                'Alert(Page, "The update/add operation failed")
                Return
            Else
                bindpage(ViewState("Filter"))
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            '   TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    Protected Sub RadScheduler1_AppointmentInsert(sender As Object, e As Telerik.Web.UI.SchedulerCancelEventArgs) Handles RadScheduler1.AppointmentInsert
        Try
            If e.Appointment.Attributes("TaskID") = "" Then
                e.Cancel = True
                Exit Sub
            ElseIf e.Appointment.Start >= e.Appointment.End Then
                e.Cancel = True
                Exit Sub
            End If

            If ExceedsLimit(e.Appointment) Then
                '    Dim WarMessage As Literal = DirectCast(e.Container.FindControl("literalWarningMessage"), Literal)
                '  literalWarningMessage.Text = "Another appointment exists in this time slot. "
                Label1.Text = "Another appointment occurs in this time slot"
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "LabelUpdated", "$telerik.$('.lblError').show().animate({ opacity: 0.9 }, 10000).fadeOut('slow');", True)

            ElseIf e.Appointment.Start < DateTime.Now Or e.Appointment.End < DateTime.Now Then
                Label1.Text = "This appointment occurs in the past "
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "LabelUpdated", "$telerik.$('.lblError').show().animate({ opacity: 0.9 }, 10000).fadeOut('slow');", True)

            Else
                ' literalWarningMessage.Text = " "
                Label1.Text = " "
            End If


            Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
            Dim tmFac As New FAME.AdvantageV1.BusinessFacade.TM.UserTasksFacade

            Dim usertaskid As String = ""

            Dim utInfo As New UserTaskInfo

            utInfo.IsInDB = False
            utInfo.AssignedById = e.Appointment.Attributes("AssignedById")
            utInfo.TaskId = e.Appointment.Attributes("TaskID")
            utInfo.StartDate = e.Appointment.Start
            utInfo.EndDate = e.Appointment.End
            utInfo.Message = e.Appointment.Attributes("Message")
            utInfo.OwnerId = e.Appointment.Resources(0).Key.ToString
            utInfo.Priority = e.Appointment.Attributes("Priority")

            ' Update the task and check the result
            If Not FAME.AdvantageV1.BusinessFacade.TM.UserTasksFacade.UpdateUserTask(utInfo, TMCommon.GetCurrentUserId()) Then
                'Alert(Page, "The update/add operation failed")
                Return
            Else
                bindpage(ViewState("Filter"))
            End If
          

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            '   TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try

    End Sub

    Protected Sub RadScheduler1_AppointmentUpdate(sender As Object, e As Telerik.Web.UI.AppointmentUpdateEventArgs) Handles RadScheduler1.AppointmentUpdate
        Try
            If e.ModifiedAppointment.Start >= e.ModifiedAppointment.End Then
                e.Cancel = True
                Exit Sub
            End If
            If ExceedsLimit(e.ModifiedAppointment) Then
                '    Dim WarMessage As Literal = DirectCast(e.Container.FindControl("literalWarningMessage"), Literal)
                '  literalWarningMessage.Text = "Another appointment exists in this time slot. "
                Label1.Text = "Another appointment occurs in this time slot"
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "LabelUpdated", "$telerik.$('.lblError').show().animate({ opacity: 0.9 }, 10000).fadeOut('slow');", True)

            ElseIf e.ModifiedAppointment.Start < DateTime.Now Or e.ModifiedAppointment.End < DateTime.Now Then
                Label1.Text = "This appointment occurs in the past "
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "LabelUpdated", "$telerik.$('.lblError').show().animate({ opacity: 0.9 }, 10000).fadeOut('slow');", True)

            Else
                ' literalWarningMessage.Text = " "
                Label1.Text = " "
            End If


            Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
            Dim tmFac As New FAME.AdvantageV1.BusinessFacade.TM.UserTasksFacade

            Dim usertaskid As String = ""

            Dim utInfo As New UserTaskInfo
            ' determine if this is an update or a new task by looking
            ' at the ViewState("usertaskid") which is set by passing a parameter to 
            ' the page.

            utInfo.IsInDB = True
            utInfo.AssignedById = TMCommon.GetCurrentUserId()
            utInfo.UserTaskId = e.ModifiedAppointment.ID.ToString
            utInfo.TaskId = e.ModifiedAppointment.Attributes("TaskID")
            utInfo.StartDate = e.ModifiedAppointment.Start
            utInfo.EndDate = e.ModifiedAppointment.End
            utInfo.Message = e.ModifiedAppointment.Attributes("Message")
            utInfo.OwnerId = e.ModifiedAppointment.Resources(0).Key.ToString
            utInfo.Priority = e.ModifiedAppointment.Attributes("Priority")

            ' Update the task and check the result
            If Not FAME.AdvantageV1.BusinessFacade.TM.UserTasksFacade.UpdateUserTask(utInfo, TMCommon.GetCurrentUserId()) Then
                'Alert(Page, "The update/add operation failed")
                Return
            Else
                bindpage(ViewState("Filter"))
            End If
          
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            '   TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try

    End Sub


    Protected Sub RadScheduler1_FormCreated(sender As Object, e As Telerik.Web.UI.SchedulerFormCreatedEventArgs) Handles RadScheduler1.FormCreated

        literalWarningMessage.Text = " "
        Dim scheduler As RadScheduler = DirectCast(sender, RadScheduler)
        Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
        If e.Container.Mode = SchedulerFormMode.Insert Then
            Dim radtaskddl As RadComboBox = DirectCast(e.Container.FindControl("radtaskddl"), RadComboBox)
            radtaskddl.DataSource = tm.GetTaskDetails
            radtaskddl.DataBind()
            For Each item As RadComboBoxItem In radtaskddl.Items
                item.ToolTip = item.Text
            Next

        ElseIf e.Container.Mode = SchedulerFormMode.Edit Then

            Dim radtaskddl As RadComboBox = DirectCast(e.Container.FindControl("radtaskddl"), RadComboBox)
            radtaskddl.DataSource = tm.GetTaskDetails
            radtaskddl.DataBind()
            For Each item As RadComboBoxItem In radtaskddl.Items
                item.ToolTip = item.Text
            Next
            radtaskddl.SelectedItem.Text = e.Appointment.Subject
            radtaskddl.SelectedItem.Value = e.Appointment.ID.ToString

            Dim subjectBox As RadTextBox = DirectCast(e.Container.FindControl("txtMessage"), RadTextBox)
            subjectBox.Text = e.Appointment.Attributes("Message")

        ElseIf e.Container.Mode = SchedulerFormMode.AdvancedEdit Or e.Container.Mode = SchedulerFormMode.AdvancedInsert Then

            Dim radtaskddl As RadComboBox = DirectCast(e.Container.FindControl("radtaskddl"), RadComboBox)
            radtaskddl.DataSource = tm.GetTaskDetails
            radtaskddl.DataBind()
            For Each item As RadComboBoxItem In radtaskddl.Items
                item.ToolTip = item.Text
            Next
            radtaskddl.SelectedItem.Text = e.Appointment.Subject
            radtaskddl.SelectedItem.Value = e.Appointment.Attributes("TaskID")

            Dim radpriorityddl As RadComboBox = DirectCast(e.Container.FindControl("radpriorityddl"), RadComboBox)
            BuildPriorityDLL(radpriorityddl)
            radpriorityddl.SelectedIndex = e.Appointment.Attributes("Priority")

            Dim subjectBox As RadTextBox = DirectCast(e.Container.FindControl("txtMessage"), RadTextBox)
            subjectBox.Text = e.Appointment.Attributes("Message")

            Dim startInput As RadDateTimePicker = DirectCast(e.Container.FindControl("StartInput"), RadDateTimePicker)
            '   startInput.DateFormat = scheduler.AdvancedForm.DateFormat + " " + scheduler.AdvancedForm.TimeFormat
            startInput.SelectedDate = RadScheduler1.DisplayToUtc(e.Appointment.Start)

            Dim endInput As RadDateTimePicker = DirectCast(e.Container.FindControl("EndInput"), RadDateTimePicker)
            '   endInput.DateFormat = scheduler.AdvancedForm.DateFormat + " " + scheduler.AdvancedForm.TimeFormat
            endInput.SelectedDate = RadScheduler1.DisplayToUtc(e.Appointment.[End])

            If startInput IsNot Nothing Then
                ' advanced form is shown  
                startInput.ClientEvents.OnDateSelected = "changeEndDate"
            End If
            If endInput IsNot Nothing Then
                ' advanced form is shown  
                endInput.ClientEvents.OnDateSelected = "changeStartDate"
            End If
        End If
    End Sub


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            ' bindscheduler()
            Dim Filter As String = ""
            Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager

            Dim viewSetting As String

            viewSetting = tm.GetUserTaskCalendarDefaultView(TMCommon.GetCurrentUserId())
            Try
                Filter = Request.QueryString("f")
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Filter = "tasks assigned to me"
            End Try


            If Filter = "" Or Filter = Nothing Then
                If viewSetting = "" Then
                    Filter = "tasks assigned to me"
                Else
                    Filter = viewSetting
                End If
            End If
                ViewState("Filter") = Filter
                ViewState("SelectedDate") = Date.Now
                bindpage(Filter)

            End If

    End Sub

    Protected Sub RadScheduler1_NavigationComplete(sender As Object, e As Telerik.Web.UI.SchedulerNavigationCompleteEventArgs) Handles RadScheduler1.NavigationComplete
        Dim SelectedDate As Date
        SelectedDate = CDate(ViewState("SelectedDate"))
        ViewState("SelectedDate") = RadScheduler1.SelectedDate

        If Not (SelectedDate.Month = RadScheduler1.SelectedDate.Month And SelectedDate.Year = RadScheduler1.SelectedDate.Year) Then
            bindpage(ViewState("Filter"))
        End If


    End Sub

    Protected Sub RadButton1_Click(sender As Object, e As EventArgs)
        RadScheduler1.ExportToPdf()
    End Sub

#End Region

#Region "Methods"

    Private Sub BuildPriorityDLL(ByRef ddl As RadComboBox)
        ddl.Items.Clear()
        ddl.Items.Add(New RadComboBoxItem("Normal", 0))
        ddl.Items.Add(New RadComboBoxItem("High", 1))
        ddl.Items.Add(New RadComboBoxItem("Low", 2))
        ddl.SelectedIndex = 0
    End Sub

    Private Sub bindscheduler()
        Dim dt As DataTable
        Dim DtResourceUserNames As DataTable
        Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager

        dt = tm.GetAdmissionRepSchedule
        DtResourceUserNames = tm.GetAdmissionRepUserDetails("A11CB992-2538-49A6-A726-6FD8DAD050FC")

        RadScheduler1.DataSource = dt
        RadScheduler1.ResourceTypes(0).DataSource = DtResourceUserNames
        RadScheduler1.DataBind()
    End Sub

    Private Function GetStartDate() As Date
        Dim StartDate As Date
        Dim selectedDate As Date
        selectedDate = CDate(ViewState("SelectedDate"))
        StartDate = New DateTime(selectedDate.Year, selectedDate.Month, 1).AddDays(-8)
        Return StartDate
    End Function

    Private Function GetEndDate() As Date
        Dim EndDate As Date
        Dim selectedDate As Date
        selectedDate = CDate(ViewState("SelectedDate"))
        EndDate = New DateTime(selectedDate.Year, selectedDate.Month, 1).AddMonths(1).AddDays(8)
        Return EndDate
    End Function

    Private Sub bindpage(ByVal Filter As String)
        Dim cmpId As String
        cmpId = Session("campusid")
        Dim ds As New DataSet
        Dim dtUsernames As New DataTable
        Dim OwnerID As String
        Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
        Dim StartDate As Date
        Dim EndDate As Date
        Dim usersCount As String = ""
        StartDate = GetStartDate()
        EndDate = GetEndDate()

        If Not Filter Is Nothing Then


            If Filter.ToLower = "tasks assigned to me" Then
                usersCount = ""
                OwnerID = TMCommon.GetCurrentUserId()
                ds = tm.GetUserTasks(cmpId, Nothing, OwnerID, _
                                    Nothing, Nothing, TaskStatus.None, StartDate, EndDate, "", Nothing)
                dtUsernames = tm.GetUsernameForTasks(OwnerID, TMCommon.GetCurrentUserId())

            ElseIf Filter.ToString.ToLower = "tasks assigned to others" Then

                OwnerID = GetOwnerofTasks()
                usersCount = GetusersCount()
                If OwnerID = "" Then
                    Response.Redirect("../TM/Userlist_PopUp.aspx?TaskCategory=2")

                End If

                ' get all the tasks for this user and save it in the viewstate
                ds = tm.GetUserTasks(cmpId, Nothing, OwnerID, _
                                    Nothing, Nothing, TaskStatus.None, StartDate, EndDate, "", Nothing)
                dtUsernames = tm.GetUsernameForTasks(OwnerID, TMCommon.GetCurrentUserId())
            ElseIf Filter.ToString.ToLower = "tasks assigned to others by me" Then

                OwnerID = GetOwnerofTasks()
                usersCount = GetUsersCount()
                If OwnerID = "" Then
                    Response.Redirect("../TM/Userlist_PopUp.aspx?TaskCategory=2")
                End If

                ' get all the tasks for this user and save it in the viewstate
                ds = tm.GetUserTasks(cmpId, TMCommon.GetCurrentUserId(), OwnerID, _
                                    Nothing, Nothing, TaskStatus.None, StartDate, EndDate, "others", TMCommon.GetCurrentUserId())
                dtUsernames = tm.GetUsernameForTasks(OwnerID, TMCommon.GetCurrentUserId())

            End If

            RadScheduler1.DataSource = ds.Tables(0)
            RadScheduler1.ResourceTypes(0).DataSource = dtUsernames
            RadScheduler1.DataBind()

            lblCAlView.Text = Filter.Substring(0, 1).ToUpper + Filter.Substring(1, Filter.Length - 1) + usersCount
        End If


    End Sub

    Private Function GetUsersCount() As String
        Dim OwnerIDs As New StringBuilder
        Dim Count As Integer
        Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
        Count = tm.GetUserTaskCalendarDefaultView_usersCount(TMCommon.GetCurrentUserId())
        If Count = 0 Then
            Return ""
        Else
            Return " (" + Count.ToString + ")"
        End If

    End Function

    Private Function GetOwnerofTasks() As String
        Dim OwnerIDs As New StringBuilder
        Dim returnId As String
        Dim ds As New DataSet
        Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
        ds = tm.GetUserTaskCalendarDefaultView_users(TMCommon.GetCurrentUserId())
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    OwnerIDs.Append(ds.Tables(0).Rows(i)(0).ToString)
                    OwnerIDs.Append(",")
                Next
                OwnerIDs.Append(TMCommon.GetCurrentUserId())
                returnId = OwnerIDs.ToString
                Return returnId
            Else
                Return ""
            End If
        Else
            Return ""
        End If

    

        'If Not (Session("selectedItems") Is Nothing) Then
        '    Dim selectedItems As ArrayList = CType(Session("selectedItems"), ArrayList)
        '    Dim i As Integer
        '    For i = 0 To selectedItems.Count - 1
        '        OwnerIDs.Append(selectedItems(i).ToString)
        '        OwnerIDs.Append(",")
        '    Next
        'Else
        '    OwnerIDs.Append(TMCommon.GetCurrentUserId())
        'End If

        'returnId = OwnerIDs.ToString
        'returnId = returnId.Remove(returnId.LastIndexOf(","), 1)
        'Return returnId

    End Function

    Protected Function GetPriorityType(ByVal number As Integer) As String
        If (number = 0) Then
            Return "Normal"
        ElseIf (number = 1) Then
            Return "High"
        Else
            Return "Low"
        End If
    End Function

    Protected Function GetPriorityStyle(ByVal number As Integer) As String
        If (number = 0) Then ''normal
            Return "float:right;background-color:Yellow;"
        ElseIf (number = 1) Then ''high
            Return "float:right;background-color:#FA5858;"
        Else ''low

            Return "float:right;background-color:#BFFF00;"
        End If
    End Function
    Protected Function HighlightLoggedinUser(ByVal user As String) As String
        If (user = TMCommon.GetUserNameFromUserId(TMCommon.GetCurrentUserId)) Then ''normal
            Return "background-color:Green;"
        Else ''low
            Return ""
        End If
    End Function

    Protected Function ExceedsLimit(ByVal apt As Appointment) As Boolean
        Dim appointmentsCount As Integer = 0
        For Each existingApt As Appointment In RadScheduler1.Appointments.GetAppointmentsInRange(apt.Start, apt.[End])
            If existingApt.Resources(0).Key.ToString = apt.Resources(0).Key.ToString Then
                If existingApt.Visible Then
                    appointmentsCount += 1
                End If
            End If
        Next
        Return (appointmentsCount > AppointmentsLimit - 1)
    End Function
#End Region

End Class
