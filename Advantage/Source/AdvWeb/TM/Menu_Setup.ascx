<%@ Control Language="VB" ClassName="SetupMenu" %>
<link href="../css/TM.css" rel="stylesheet" type="text/css" />    
<script runat="server">
</script>
<div>
<table width="100%" summary="SysMenu">
<tr>
    <td align="left">
    <asp:Menu ID="Menu1" runat="server" DynamicHorizontalOffset="5"  StaticMenuItemStyle-ItemSpacing="1"
        DynamicVerticalOffset="0" StaticEnableDefaultPopOutImage="false" Orientation="Horizontal">
        <StaticMenuStyle />
        <StaticMenuItemStyle ForeColor="Black" 
            HorizontalPadding="8px" VerticalPadding="0px"
            Font-Names="Verdana" Font-Size="10pt" Font-Bold="false" 
            BorderStyle="Solid" BorderWidth="1px"  BorderColor="Black"
            BackColor="White" />
        <DynamicMenuStyle BorderStyle="Solid" BorderWidth="1px" BorderColor="Black"
             HorizontalPadding="8px" />
        <DynamicMenuItemStyle ForeColor="Black" 
            Font-Names="Tahoma" Font-Size="10pt" Font-Bold="false" BackColor="White" />
        <DynamicHoverStyle ForeColor="White" />      
        <Items>
            <asp:MenuItem NavigateUrl="SetupTasks.aspx" Text="Setup Tasks" Value="Setup Tasks">
                <asp:MenuItem NavigateUrl="AddTask.aspx" Text="Add Task" Value="Add Task" />
            </asp:MenuItem>
            <asp:MenuItem NavigateUrl="SetupResults.aspx" Text="Setup Results" Value="Setup Results">
                <asp:MenuItem NavigateUrl="AddResult.aspx" Text="Add Result" Value="Add Result" />
            </asp:MenuItem>                    
            <asp:MenuItem NavigateUrl="SetupCategories.aspx" Text="Setup Categories" Value="Setup Categories" />            
        </Items>                                                       
    </asp:Menu>
    </td>
</tr>
</table>
</div>