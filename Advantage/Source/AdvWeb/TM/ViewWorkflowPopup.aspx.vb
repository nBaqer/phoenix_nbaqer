
' ===============================================================================
' ViewWorkflowPopup
'   Views the workflow for a specific task in a graphical view
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' 7/27/06 - Ben: Added code to allow the user to limit the number of levels
'                That the workflow tree will display

Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common.TM

Partial Class TM_ViewWorkflowPopup
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim TaskId As String = Request.Params("taskid")            

            ' check for invalid taskid
            If TaskId Is Nothing Or TaskId = "" Then
                'Return
            End If

            BuildMaxLevelsDDL(Me.ddlMaxLevels)
            ddlMaxLevels.SelectedValue = 7 ' start with 7 levels
            ViewState("id") = TaskId
            Me.lblData.Text = GetWorkflowScript(TaskId, ddlMaxLevels.SelectedValue)
        End If
    End Sub

    ''' <summary>
    ''' Add a series of integer into the drop down list
    ''' which allows the user to change the max number of levels
    ''' </summary>
    ''' <param name="ddl"></param>
    ''' <remarks></remarks>
    Protected Sub BuildMaxLevelsDDL(ByVal ddl As DropDownList)
        For i As Integer = 2 To 50
            ddl.Items.Add(New ListItem(i.ToString(), i))
        Next
    End Sub

    ''' <summary>
    ''' User changed the number of levels they wish to see.  
    ''' Redraw the workflow tree with the selected value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlMaxLevels_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMaxLevels.SelectedIndexChanged
        Dim taskId As String = ViewState("id")
        Me.lblData.Text = GetWorkflowScript(taskId, ddlMaxLevels.SelectedValue)
    End Sub

    ''' <summary>
    ''' Returns javascript to load the workflow.  The return string
    ''' contains a function def for loadData which is called in the 
    ''' body's OnLoad method.
    ''' </summary>
    ''' <param name="taskid"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function GetWorkflowScript(ByVal taskid As String, ByVal maxLevels As Integer) As String
        Dim sb As New StringBuilder()
        sb.Append("<script>" + vbCrLf)
        sb.Append("function loadData() { " + vbCrLf)
        Dim info As TaskInfo = TasksFacade.BuildTree(taskid, maxLevels)
        Dim s As String = GetWorkflowScript(0, info, "")
        sb.Append(s)
        sb.Append(";")
        sb.Append("  renderIt();" + vbCrLf)
        sb.Append("}" + vbCrLf)
        sb.Append("</script>" + vbCrLf)
        Return sb.ToString()
    End Function

    Protected Function GetWorkflowScript(ByVal level As Integer, ByVal info As TaskInfo, ByVal result As String) As String
        Dim sb As New StringBuilder()

        Dim s As String = "new Node({name:'" + info.Descrip + "',result:'" + result + "'}"
        sb.Append(s)

        Dim cResult As String = ""
        If info.Children.Count > 0 Then
            sb.Append(",[" + vbCrLf)
            level = level + 1
            For i As Integer = 0 To info.Children.Count - 1
                ' retrieve the description for the result that got us to this child node
                If i >= info.ChildrenResults.Count Then
                    cResult = ""
                Else
                    cResult = info.ChildrenResults(i)
                End If

                s = GetWorkflowScript(level, info.Children(i), cResult)
                If i = info.Children.Count - 1 Then
                    sb.Append(s + vbCrLf)
                Else
                    sb.Append(s + ",")
                End If

            Next
            sb.Append("]")
        End If

        If level = 0 Then
            sb.Append(");" + vbCrLf)
        Else
            sb.Append(")" + vbCrLf)
        End If

        'new Node({name:'Ben of Org',email:'boss@example.com'},[
        '   new Node({name:'One Manager',email:'man1@example.com'},[
        '       new Node({name:'B. Fast',email:'bfast@example.com'},[
        '           new Node({name:'Bea Worker',email:'beaworker@example.com'}), 
        '           new Node({name:'D. Plebian',email:'dplebian@example.com'})
        '       ])
        '   ]),
        '   new Node({name:'Manny Two',email:'man2@example.com'},[
        '      new Node({name:'Slick Talkman',email:'stalkman@example.com'},[" + vbCrLf)
        '        new Node({name:'Patricia Guru',email:'pguru@example.com'})," + vbCrLf)
        '        new Node({name:'Mark Time',email:'mtime@example.com'})" + vbCrLf)
        '      ])," + vbCrLf)
        '      new Node({name:'D. Warbucks',email:'dwarbucks@example.com'})," + vbCrLf)
        '      new Node({name:'Berry Smart',email:'bsmart@example.com'})" + vbCrLf)
        '    ])" + vbCrLf)
        '  ]);" + vbCrLf)
        Return sb.ToString()
    End Function
End Class
