' ===============================================================================
' ViewUserTask
'   Views all tasks by various filters
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

' This page can take "filter" as a paramter.
' This filter is "|" delimitted with the format 
'       {status}|{priority}|{contactid}|{assignedtoid}|{entityid}
' For example, ViewUserTasks.aspx?filter=1...

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common.TM
Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.Common
Partial Class AdvantageApp_TM_ViewUserTasks
    Inherits System.Web.UI.Page
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = "SY"
        Dim m_Context As HttpContext
        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId

        Session("UserId") = userId
        Session("UserName") = AdvantageSession.UserState.UserName.ToString
        Session("cmpid") = campusId
        Session("resid") = txtResourceId.Text.Trim
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        TMCommon.AdvInit(Me)

        If Not Page.IsPostBack Then
            ' Check the parameters passed to the page
            filterStatus.Value = TaskStatus.Pending
            Dim filter As String = Request.Params("filter")
            Dim predef As String = Request.Params("predef")

            ' default to tasks that are assigned to the current user if
            ' no paramter has been passed
            If (filter Is Nothing Or filter = "") AndAlso (predef Is Nothing Or predef = "") Then
                predef = "Tasks assigned to me"
            End If

            If Not filter Is Nothing Then
                RunFilter(filter)
            ElseIf Not predef Is Nothing Then
                RunPreDefFilter(predef)
            Else
                ResetFilters()
                BindUserTasks()
            End If
        End If

    End Sub

    ''' <summary>
    ''' Show all user tasks for the current user
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindUserTasks()
        Try
            Dim ds As DataSet = UserTasksFacade.GetUserTasks(TMCommon.GetCampusID(), _
                                                             filterAssignedById.Value, _
                                                             filterAssignedToId.Value, _
                                                             filterReId.Value, _
                                                             filterPriority.Value, _
                                                             CType(filterStatus.Value, TaskStatus), _
                                                             filterDueDateMin.Value, _
                                                             filterDueDateMax.Value, _
                                                             filterOthers.Value)
            Dim facInputMasks As New InputMasksFacade
            Dim strMask As String

            'Get the mask for phone numbers and zip
            strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)


            For Each dr As DataRow In ds.Tables(0).Rows
                If Not dr("Phone") Is System.DBNull.Value Then
                    Dim strPhone As String
                    Try
                        strPhone = facInputMasks.ApplyMask(strMask, dr("Phone"))
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        strPhone = dr("Phone")
                    End Try

                    dr("ReName") = dr("ReName") & " - " & strPhone
                    If Not dr("Email") Is System.DBNull.Value Then
                        dr("ReName") = dr("ReName") & " - " & dr("Email")
                    End If
                Else
                    If Not dr("Email") Is System.DBNull.Value Then
                        dr("ReName") = dr("ReName") & " - " & dr("Email")
                    End If
                End If
            Next

            rptUserTasks.DataSource = ds
            rptUserTasks.DataBind()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
        ' Hide or display the "No Tasks" message
        If rptUserTasks.Items.Count = 0 Then
            scrolldiv.Visible = False
            lblTasksMsgs.Visible = True
            'lbFilter.Visible = False
        Else
            scrolldiv.Visible = True
            lblTasksMsgs.Visible = False
            'lbFilter.Visible = True
        End If
    End Sub

    ''' <summary>
    ''' Resets all the hidden fields that make up a filter
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetFilters()
        ' start with showing on tasks that have not been started
        Me.filterAssignedById.Value = ""
        Me.filterAssignedToId.Value = ""
        Me.filterDueDateMax.Value = ""
        Me.filterDueDateMin.Value = ""
        Me.filterEntityId.Value = ""
        Me.filterReId.Value = ""
        Me.filterPriority.Value = ""
        Me.filterReType.Value = ""
        Me.filterStatus.Value = TaskStatus.Pending
        Me.filterOthers.Value = ""
    End Sub

    ''' <summary>
    ''' Perform a filter based on the pre-defined ones in the drop down
    ''' </summary>
    ''' <param name="predef"></param>
    ''' <remarks></remarks>
    Protected Sub RunPreDefFilter(ByVal predef As String)
        Me.filterStatus.Value = TaskStatus.Pending
        filterAssignedById.Value = "" ' start with no AssignedById


        ' setup the filter
        If predef = "Tasks due today" Then
            filterDueDateMin.Value = Date.Today.ToShortDateString()
            filterDueDateMax.Value = Date.Today.AddDays(1).ToShortDateString()
            filterAssignedToId.Value = TMCommon.GetCurrentUserId()
        ElseIf predef = "Tasks due in 1 week" Then
            filterDueDateMin.Value = Date.Today.ToShortDateString()
            filterDueDateMax.Value = Date.Today.AddDays(7).ToString()
            filterAssignedToId.Value = TMCommon.GetCurrentUserId()
        ElseIf predef = "Overdue tasks" Then
            filterDueDateMax.Value = Date.Now.ToString()
            filterAssignedToId.Value = TMCommon.GetCurrentUserId()
        ElseIf predef = "High priority tasks" Then
            Me.filterPriority.Value = "1"
            filterAssignedToId.Value = TMCommon.GetCurrentUserId()
        ElseIf predef = "Tasks assigned to me" Then
            filterAssignedToId.Value = TMCommon.GetCurrentUserId()
        ElseIf predef = "Tasks I assigned to others" Then
            filterOthers.Value = "others"
            filterAssignedById.Value = TMCommon.GetCurrentUserId()
        ElseIf predef = "Completed in the last week" Then
            filterStatus.Value = TaskStatus.Completed
            filterAssignedToId.Value = TMCommon.GetCurrentUserId()
        End If

        ' now that the filter has been setup, bind the filters tasks
        BindUserTasks()
    End Sub

    ''' <summary>
    ''' Called in response to the user wanting to filter the list
    ''' of tasks shown on the repeater.
    ''' Before this method is called, FilterUserTasks.aspx has already
    ''' been popped up and the result of that popup is stored in hfResult.
    ''' For more info on the format of hrResult, see FilterUserTasks.aspx.vb.
    ''' </summary>
    ''' <param name="filter"></param>
    ''' <remarks></remarks>
    Protected Sub RunFilter(ByVal filter As String)
        ' reset the filter and the dropdownlist to reflect that there is not 
        ' pre-defined filter set
        ResetFilters()

        ' parse the returned filter string 
        Dim filters As String() = filter.Split("|")
        filterStatus.Value = filters(0)
        filterPriority.Value = filters(1)
        filterReId.Value = filters(2)
        filterAssignedToId.Value = filters(3)
        filterEntityId.Value = filters(4)

        If filterAssignedToId.Value Is Nothing Or filterAssignedToId.Value = "" Then
            filterAssignedToId.Value = TMCommon.GetCurrentUserId()
        End If

        ' rebind the user tasks control
        BindUserTasks()
    End Sub

    ''' <summary>
    ''' Called for each item in the repeater.
    ''' Gives us the change to set the images and other text values correctly
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rptUserTasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptUserTasks.ItemDataBound
        Try
            ' set the image for the priority
            Dim imgPriority As System.Web.UI.WebControls.Image = e.Item.FindControl("imgPriority")
            If Not imgPriority Is Nothing Then
                Dim lbPriority As Label = e.Item.FindControl("lblPriority")
                If Not lbPriority Is Nothing Then
                    Dim iPriority As Int32 = Int32.Parse(lbPriority.Text)
                    Select Case iPriority
                        Case 0 ' normal
                            imgPriority.Visible = False
                        Case 1 ' high
                            imgPriority.ImageUrl = "../Images/TM/high_priority.jpg"
                            imgPriority.Visible = True
                        Case 2 ' low
                            imgPriority.ImageUrl = "../Images/TM/low_priority.jpg"
                            imgPriority.Visible = True
                    End Select
                End If
            End If

            ' set the status to the correct string
            Dim lb As Label = e.Item.FindControl("lblStatus")
            If Not lb Is Nothing Then
                If lb.Text = TaskStatus.Cancelled Then
                    lb.Text = "Cancelled"
                ElseIf lb.Text = TaskStatus.Completed Then
                    lb.Text = "Completed"
                ElseIf lb.Text = TaskStatus.Pending Then
                    lb.Text = "Pending"
                Else
                    lb.Text = "N/A"
                End If
            End If

            Dim ib As System.Web.UI.WebControls.ImageButton = e.Item.FindControl("ibHistory")
            Dim hf As HiddenField = e.Item.FindControl("hfReId")
            If Not ib Is Nothing AndAlso Not hf Is Nothing Then
                Dim url As String = String.Format("ViewUserTaskHistory.aspx?reid={0}", hf.Value)
                Dim js As String = TMCommon.GetJavsScriptPopup(url, 600, 500, Nothing)
                ib.Attributes.Add("onclick", js)
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
End Class
