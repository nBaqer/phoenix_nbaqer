' ===============================================================================
' SetupCategories
' Handles adding, editing and deleting (make inactive) of categories
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common.TM
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class AdvantageApp_TM_SetupCategories
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim fac As New UserSecurityFacade
        Dim m_Context As HttpContext
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        Session("UserId") = userId
        Session("UserName") = AdvantageSession.UserState.UserName.ToString
        Session("cmpid") = campusId
        Session("resid") = txtResourceId.Text.Trim
        TMCommon.AdvInit(Me)
        If Not Page.IsPostBack Then
            InitButtonsForLoad()
            'Header1.EnableHistoryButton(False)
            ' clear out the form with original values
            ResetForm()

            ' display the tasks defined in the system
            BindCategories()

            ' Bind the task to the form
            Dim CategoryId As String = Request.Params("categoryid")
            If Not CategoryId Is Nothing Then
                BindCategory(CategoryId)
            End If

        Else
            InitButtonsForEdit()
        End If
        headerTitle.Text = Header.Title

    End Sub
#Region "Advantage Permissions"
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnnew.Enabled = False
        End If

        btndelete.Enabled = False

        'If pObj.HasFull Or pObj.HasEdit Then
        '    btnSave.Enabled = True
        'Else
        '    btnSave.Enabled = False
        'End If
        'If pObj.HasFull Or pObj.HasDelete Then
        '    btnDelete.Enabled = True
        'Else
        '    btnDelete.Enabled = False
        'End If
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If
        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If
        'btnNew.Enabled = True
    End Sub
#End Region

    ''' <summary>
    ''' Fills the form with a Category given a CategoryId
    ''' </summary>
    ''' <param name="CategoryId"></param>
    ''' <remarks></remarks>
    Protected Sub BindCategory(ByVal CategoryId As String)
        Try
            Dim info As CategoryInfo = CategoriesFacade.GetCategoryInfo(CategoryId)
            ddlActive.SelectedValue = info.Active.ToString()
            txtCode.Text = info.Code
            txtName.Text = info.Descrip
            ViewState("moddate") = info.ModDate ' need this to do a proper delete

            BindTasksForCategory(CategoryId)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Show all the tasks that belong to this category
    ''' </summary>
    ''' <param name="CategoryId"></param>
    ''' <remarks></remarks>
    Protected Sub BindTasksForCategory(ByVal CategoryId As String)
        ' get all the tasks from cache so that we can do a select on it
        Dim ds As DataSet = ViewState("AllTasks")
        If ds Is Nothing Then
            ds = TasksFacade.GetTasks(Nothing, Nothing, Nothing, True, True)
            ViewState("AllTasks") = ds
        End If

        ' get the subset of rows
        Dim ds2 As New DataTable
        Dim drs As DataRow() = ds.Tables(0).Select("CategoryId='" + CategoryId + "'")
        Dim dr As DataRow
        ' insert columns into table
        For Each dc As DataColumn In ds.Tables(0).Columns
            ds2.Columns.Add(New DataColumn(dc.ColumnName, dc.DataType, dc.Expression))
        Next
        ' import the rows now
        For Each dr In drs
            ds2.ImportRow(dr)
        Next

        lbSysTasks.DataSource = ds2
        lbSysTasks.DataValueField = "TaskId"
        lbSysTasks.DataTextField = "Descrip"
        lbSysTasks.DataBind()
    End Sub

    ''' <summary>
    ''' Binds all categories
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindCategories()
        If radstatus.SelectedIndex = 0 Then
            dlCategories.DataSource = CategoriesFacade.GetCategories(TMCommon.GetCampusID(), True, False)
        ElseIf radstatus.SelectedIndex = 1 Then
            dlCategories.DataSource = CategoriesFacade.GetCategories(TMCommon.GetCampusID(), False, True)
        Else
            dlCategories.DataSource = CategoriesFacade.GetCategories(TMCommon.GetCampusID(), True, True)
        End If
        dlCategories.DataBind()
    End Sub

    ''' <summary>
    ''' Resets the form to the intial state.  All controls that are databound are initialized
    ''' from their datasource.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub ResetForm()
        ' clear out the form
        TMCommon.BuildStatusDDL(ddlActive)
        ddlActive.SelectedValue = True

        txtCode.Text = ""
        txtName.Text = ""

        lbSysTasks.Items.Clear()
    End Sub

    ''' <summary>
    ''' Event handler for when the user clicks on the left datalist containing all the users.
    ''' In response, this handler should populate the info for a task
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlCategories_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlCategories.ItemCommand
        ' get the selected categoryid and store it in the viewstate object
        Dim categoryid As String = e.CommandArgument.ToString
        ViewState("id") = categoryid
        dlCategories.SelectedIndex = e.Item.ItemIndex

        ' do some error checking
        If categoryid Is Nothing Or categoryid = "" Then
            TMCommon.Alert(Me, "There was a problem displaying the details for this category.")
            Return
        End If

        ' bind the result to the form
        BindCategory(categoryid)

        ' Advantage specific stuff
        ' CommonWebUtilities.SetStyleToSelectedItem(dlCategories, categoryid, ViewState, Header1)
        InitButtonsForEdit()

        CommonWebUtilities.RestoreItemValues(Me.dlCategories, ViewState("id").ToString)

        btndelete.Enabled = True

    End Sub

    ''' <summary>
    ''' Checks the form's contents for validity.  If any control is
    ''' not valid, an alert is made and the function returns false.
    ''' 
    ''' If we are in edit mode (UserTaskId was passed to the page as a parameter).
    ''' and the status is set to "Complete" then the user must select a Result.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ValidateForm() As Boolean
        ' Check the task drop down
        If Me.txtCode.Text.Length = 0 Then
            TMCommon.Alert(Me, "Code is required.")
            Return False
        End If

        If txtCode.Text.Length > 12 Then
            TMCommon.DisplayErrorMessage(Me, "Code cannot be greater than 12 characters.")
            Return False
        End If

        If Me.txtName.Text.Length = 0 Then
            TMCommon.Alert(Me, "Description is required.")
            Return False
        End If

        Return True
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            ' check that the user has entered in all the right values
            If Not ValidateForm() Then Return

            Dim categoryid As String = ViewState("id")
            Dim info As New CategoryInfo
            ' determine if this is an update or a new by looking
            ' at the ViewState which is set by passing a parameter to the page.
            If categoryid Is Nothing Or categoryid = "" Then
                info.IsInDB = False
                ViewState("id") = info.CategoryId
            Else
                info.IsInDB = True
                info.CategoryId = categoryid
            End If

            info.Active = CType(ddlActive.SelectedValue, Boolean)
            info.Code = txtCode.Text
            info.Descrip = txtName.Text
            'info.CampGroupId = TMCommon.GetCampusID()

            If CategoriesFacade.UpdateCategory(info, TMCommon.GetCurrentUserId()) Then
                TMCommon.Alert(Page, "Category was saved successfully")
            Else
                TMCommon.Alert(Page, "Errors ocurred while saving the category")
            End If

            ' update the left side with the list of categories
            BindCategories()

            ' Advantage specific stuff
            'CommonWebUtilities.SetStyleToSelectedItem(dlCategories, ViewState("id"), ViewState, Header1)
            InitButtonsForEdit()

            CommonWebUtilities.RestoreItemValues(Me.dlCategories, ViewState("id").ToString)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, "Cateogory could not be saved. Error=" + ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Simply reset the form
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        ViewState("id") = Nothing
        ViewState("moddate") = Nothing
        ResetForm()

        ' Advantage specific stuff
        Dim objCommon As New CommonUtilities
        'objCommon.SetBtnState(form1, "NEW", Header1.UserPagePermission)
        'Initialize Buttons
        InitButtonsForLoad()
        ' CommonWebUtilities.SetStyleToSelectedItem(Me.dlCategories, Guid.Empty.ToString, ViewState, Header1)

        CommonWebUtilities.RestoreItemValues(Me.dlCategories, Guid.Empty.ToString)
    End Sub

    ''' <summary>
    ''' Delete a task or set inactive
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            Dim id As String = ViewState("id")
            If Not id Is Nothing AndAlso id <> "" Then
                Dim modDate As DateTime = ViewState("moddate")
                Dim res As String = CategoriesFacade.DeleteCategory(id, modDate)
                If res <> "" Then
                    TMCommon.DisplayErrorMessage(Me, res)
                Else
                    ViewState("id") = Nothing
                    ResetForm()
                    ' show the tasks in the left pane
                    BindCategories()

                    ' Advantage specific stuff
                    'CommonWebUtilities.SetStyleToSelectedItem(Me.dlCategories, Guid.Empty.ToString, ViewState, Header1)
                    InitButtonsForLoad()
                End If
                CommonWebUtilities.RestoreItemValues(Me.dlCategories, Guid.Empty.ToString)
            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Me, "Error.  Record could not be deleted.")
        End Try
    End Sub

    ''' <summary>
    ''' Display active, inactive or all based on the user checking the radio box option
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        InitButtonsForLoad()
        ResetForm()
        BindCategories()
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
