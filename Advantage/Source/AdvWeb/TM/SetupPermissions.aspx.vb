' ===============================================================================
' SetupPermissions
' Manage user permissions for taskmanager
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' 6/1/06 - Made checkboxlist
' 6/2/06 - Forced New and Delete button to be disabled

Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.AdvantageV1.Common.TM
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class AdvantageApp_TM_SetupPermissions
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        Session("UserId") = userId
        Session("UserName") = AdvantageSession.UserState.UserName.ToString
        Session("cmpid") = campusId
        Session("resid") = txtResourceId.Text.Trim
        TMCommon.AdvInit(Me)
        If Not Page.IsPostBack Then
            ' Advantage Specific
            InitButtonsForLoad()
            'Header1.EnableHistoryButton(False)
            ' Clear out the form
            ResetForm()

        Else
            InitButtonsForEdit()
        End If
        headerTitle.Text = Header.Title

    End Sub

#Region "Advantage Permissions"
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            'btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnnew.Enabled = True
        Else
            btnnew.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            'btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If
        'btnnew.Enabled = True
    End Sub
#End Region

    Protected Sub ResetForm()
        Try
            ' build the list of users, start out showing only active
            dlUsers.DataSource = PermissionsFacade.GetSystemUsers(Nothing, True, False)
            dlUsers.DataBind()

            ' build the list of campus groups
            rptPermissions.DataSource = Nothing
            'rptPermissions.DataSource = PermissionsFacade.GetCampusGroups()
            rptPermissions.DataBind()
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            TMCommon.Alert(Page, ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Event handler for when the user clicks on the left datalist containing all the users.
    ''' In response, this handler should populate the permissions shown in the repeater.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlUsers_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlUsers.ItemCommand
        ' get the selected userid and store it in the viewstate object
        Dim userid As String = e.CommandArgument.ToString
        ViewState("userid") = userid
        dlUsers.SelectedIndex = e.Item.ItemIndex

        ' do some error checking
        If userid Is Nothing Or userid = "" Then
            TMCommon.Alert(Me, "There was a problem displaying the permissions for this user.")
            Return
        End If

        ' build the list of campus groups for this user
        ' For each campus, the userid is retrived from the viewstate.  That is why it is
        ' not passed into this bind operation.
        ' Note: See rptPermissions_OnItemDataBound
        rptPermissions.DataSource = PermissionsFacade.GetCampusGroups()
        rptPermissions.DataBind()

        ' Advantage specific stuff
        ' CommonWebUtilities.SetStyleToSelectedItem(dlUsers, userid, ViewState, Header1)
        InitButtonsForEdit()
        CommonWebUtilities.RestoreItemValues(dlUsers, ViewState("userid").ToString)
    End Sub

    ''' <summary>
    ''' User has filtered which users they want to see.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        Try
            InitButtonsForLoad()
            ResetForm()
            If radstatus.SelectedIndex = 0 Then
                dlUsers.DataSource = PermissionsFacade.GetSystemUsers(TMCommon.GetCampusID(), True, False)
            ElseIf radstatus.SelectedIndex = 1 Then
                dlUsers.DataSource = PermissionsFacade.GetSystemUsers(TMCommon.GetCampusID(), False, True)
            Else
                dlUsers.DataSource = PermissionsFacade.GetSystemUsers(TMCommon.GetCampusID(), True, True)
            End If
            dlUsers.DataBind()

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            TMCommon.Alert(Me, "There was a problem displaying the list of users for this filter.")
        End Try
    End Sub

    ''' <summary>
    ''' Bind the roles for each campus
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Public Sub rptPermissions_OnItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptPermissions.ItemDataBound
        Try
            Dim userid As String = ViewState("userid")
            ' get the campus id
            Dim hf As HiddenField = e.Item.FindControl("hfCampGrpId")
            Dim CampGrpId As String = ""
            If Not hf Is Nothing Then CampGrpId = hf.Value

            Dim cbl As CheckBoxList = e.Item.FindControl("cblRoles")
            ' fill up the system roles
            If Not cbl Is Nothing Then
                Dim ds As DataSet = Cache("SystemRoles")
                If ds Is Nothing Then
                    ds = PermissionsFacade.GetSystemRoles()
                    Cache("SystemRoles") = ds
                End If
                cbl.DataSource = ds
                cbl.DataTextField = "Role"
                cbl.DataValueField = "RoleId"
                cbl.DataBind()

                ' check the ones that in the dataset                
                ds = PermissionsFacade.GetPermissions(userid, CampGrpId)
                For Each dr As DataRow In ds.Tables(0).Rows
                    Try
                        Dim li As ListItem = cbl.Items.FindByValue(dr("RoleId").ToString())
                        If Not li Is Nothing Then
                            li.Selected = True
                        End If
                    Catch ex As System.Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(ex)

                    End Try
                Next
            End If

            ' Advantage specific stuff
            'CommonWebUtilities.SetStyleToSelectedItem(dlUsers, userid, ViewState, Header1)
            InitButtonsForEdit()
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            TMCommon.DisplayErrorMessage(Page, ex.Message)
        End Try
    End Sub

    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        ' get the current user and save the changes for this user
        Dim UserId As String = ViewState("userid")
        If UserId Is Nothing Then
            TMCommon.Alert(Me.Page, "No User Selected")
            Return
        End If

        ' delete all roles in tmPermissions
        PermissionsFacade.DeleteUserPermissions(UserId)
        ' iterate for each campus
        Dim rItem As RepeaterItem
        Dim opOk As Boolean = True
        For Each rItem In rptPermissions.Items
            ' get the campgrpid and listbox full of assigned roles
            Dim hf As HiddenField = rItem.FindControl("hfCampGrpId")
            Dim cbl As CheckBoxList = rItem.FindControl("cblRoles")
            If Not hf Is Nothing AndAlso Not cbl Is Nothing Then
                Dim CampGrpId As String = hf.Value
                ' save all the roles contained in lbRoles for this campus
                Dim li As ListItem
                For Each li In cbl.Items
                    If li.Selected Then
                        Dim pInfo As New PermissionsInfo
                        pInfo.RoleId = li.Value
                        pInfo.UserId = UserId
                        pInfo.CampGrpId = CampGrpId
                        ' Add the new permission and check the result
                        If Not PermissionsFacade.UpdatePermissions(pInfo, TMCommon.GetCurrentUserId()) Then
                            opOk = False
                        End If
                    End If
                Next
            End If
        Next

        ' look at the opOk var to see if all the changes were recorded correctly
        If opOk Then
            ' Tell the user that saving was ok
            TMCommon.Alert(Page, "All roles for this user have been saved successfully.")
        Else
            TMCommon.Alert(Page, "Errors occured during saving..")
        End If

        InitButtonsForEdit()

        CommonWebUtilities.RestoreItemValues(dlUsers, ViewState("userid").ToString)
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnnew.Click
        ResetForm()

        ' Advantage specific stuff
        'CommonWebUtilities.SetStyleToSelectedItem(dlUsers, Guid.Empty.ToString, ViewState, Header1)
        InitButtonsForLoad()

        CommonWebUtilities.RestoreItemValues(dlUsers, Guid.Empty.ToString)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
