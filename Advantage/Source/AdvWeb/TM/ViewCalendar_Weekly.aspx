<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ViewCalendar_Weekly.aspx.vb" Inherits="TM_ViewCalendar" %>
<%@ Register TagName="SysHeader" TagPrefix="Header" Src="~/usercontrols/TMHeader.ascx" %>
<%@ Register TagName="CalendarMenu" TagPrefix="Menu" Src="Menu_Calendar.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Calendar [Weekly View]</title>
    <link href="../css/TM.css" rel="stylesheet" type="text/css" />        
    
    <style><!-- 
        a {text-decoration: none; } 
    --></style> 

</head>
<body>
    <form id="form1" runat="server">
    <div>
		<menu:CalendarMenu ID="sysMenu" runat="server" />
		<div class="AlignedDivCalendar">	
        <table id="Table2" width="100%" cellpadding="0" cellspacing="0">
        		<tr>
		    <th style="text-align: center"><h1>Calendar [Weekly View]</h1></th></tr>				    
		<tr>
			<td style="text-align: center; background-color:#dee2e7; border-left: 1px solid #83929f; border-right: 1px solid #83929f">
				<asp:Panel id="pnlHeader" Width="100%" Wrap="False" runat="server">
                <asp:LinkButton id="lnkPrev" runat="server" CssClass="LabelBoldcalendar" Text="<<" />
                <asp:Label id="lblWeekOf" runat="server" CssClass="LabelBoldcalendar" />
                <asp:LinkButton id="lnkNext" runat="server" CssClass="LabelBoldcalendar" Text=">>" />
                </asp:Panel>
            </td>
		</tr>
		<tr>
			<td>			    
			    <div class="ScrolledDivCalendar">
			    <asp:DataGrid id="grdWeekView" runat="server" Width="100%"
			        ItemStyle-VerticalAlign="Top" ItemStyle-CssClass="TMLabel" CellPadding="3" AutoGenerateColumns="False">
			    <ItemStyle VerticalAlign="Top" HorizontalAlign="left" CssClass="TMLabel" />
				<HeaderStyle CssClass="DataGridHeaderCalendar" />
				<Columns>
					<asp:BoundColumn DataField="EventTime" DataFormatString="{0:t}">
						<HeaderStyle Wrap="False" HorizontalAlign="Right" Width="16%" CssClass="DataGridHeaderCalendarright" />
						<ItemStyle CssClass="TMLabelBold" Wrap="False" HorizontalAlign="Right" BackColor="#e7edf3" 
						    BorderColor="#83929f" BorderWidth="1px" BorderStyle=Solid />
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Sunday" HeaderText="Sun">
						<HeaderStyle CssClass="DataGridHeaderCalendar" HorizontalAlign="Center" />
						<ItemStyle Wrap="False" BackColor="#f9fbe6" BorderColor="#83929f" BorderWidth="1px" BorderStyle="Solid" />
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Monday" HeaderText="Mon">
					    <HeaderStyle CssClass="DataGridHeaderCalendar" HorizontalAlign="Center" />
						<ItemStyle Wrap="False" BackColor="#f9fbe6" BorderColor="#83929f" 
						    BorderWidth="1px" BorderStyle="Solid" CssClass="TMLabel" />
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Tuesday" HeaderText="Tue">
					    <HeaderStyle CssClass="DataGridHeaderCalendar" HorizontalAlign="Center" />
						<ItemStyle Wrap="False" BackColor="#f9fbe6" BorderColor="#83929f" 
						    BorderWidth="1px" BorderStyle="Solid" CssClass="TMLabel" />
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Wednesday" HeaderText="Wed">
					    <HeaderStyle CssClass="DataGridHeaderCalendar" HorizontalAlign="Center" />
						<ItemStyle Wrap="False" BackColor="#f9fbe6" BorderColor="#83929f" 
						    BorderWidth="1px" BorderStyle="Solid" CssClass="TMLabel" />
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Thursday" HeaderText="Thu">
					    <HeaderStyle CssClass="DataGridHeaderCalendar" HorizontalAlign="Center" />
						<ItemStyle Wrap="False" BackColor="#f9fbe6" BorderColor="#83929f" 
						    BorderWidth="1px" BorderStyle="Solid" CssClass="TMLabel" />
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Friday" HeaderText="Fri">
					    <HeaderStyle CssClass="DataGridHeaderCalendar" HorizontalAlign="Center" />
						<ItemStyle Wrap="False" BackColor="#f9fbe6" BorderColor="#83929f" 
						    BorderWidth="1px" BorderStyle="Solid" CssClass="TMLabel" />
					</asp:BoundColumn>
					<asp:BoundColumn DataField="Saturday" HeaderText="Sat">
						<HeaderStyle CssClass="DataGridHeaderCalendar" HorizontalAlign="Center" />
						<ItemStyle Wrap="False" BackColor="WhiteSmoke" BorderColor="#83929f" BorderWidth="1px" BorderStyle="Solid" />
					</asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="Sunday" HeaderText="Sun2">
						<HeaderStyle CssClass="DataGridHeaderCalendar" HorizontalAlign="Center" />
						<ItemStyle BackColor="WhiteSmoke" BorderColor="#83929f" BorderWidth="1px" BorderStyle=Solid />
					</asp:BoundColumn>
				</Columns>
				</asp:DataGrid>
				</div>
			</td>
		</tr>
	    </table>
	    </div>
    </div>
    </form>
</body>
</html>
