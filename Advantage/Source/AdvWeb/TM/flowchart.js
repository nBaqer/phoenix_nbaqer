﻿var nr=1;
var nodeBag={};
var sel=0;var attach_sel=0;
var selnode;

function Node(data,childNodes) {
  this.data=data;
  this.id=nr++;
  nodeBag[this.id]=this;
  if (childNodes) {
    this.first=childNodes[0];
    for (var i=0;i<childNodes.length;i++) {
      childNodes[i].prev=childNodes[i-1];
      childNodes[i].next=childNodes[i+1];
      childNodes[i].parent=this;
    }
  }
}
Node.prototype={
render:function(finalver) {
  var s='';var next='';var l=1;
  if (this.first) {
    l=0;var node=this.first;
    while (node) {
      next+='<TD valign="top">'+node.render(finalver)+'</td>';
      node=node.next;l++;
    }
  }
  s+='\n<table cellspacing="0" cellpadding="0" width="100%"><tr><td colspan='+l+'>';
  s+='\n<table cellspacing="0" cellpadding="0" width="100%">';
  if (this.parent) {
    s+='\n<tr><td width="50%" class="'+(!this.prev?'lineright':'leftabove')+'">&nbsp;</td>';
    s+='<td width="50%" class="'+(!this.next?'nolines':'rightabove')+'">&nbsp;</td></tr>';
  }
  s+='\n<tr><td colspan="2" class="boxc">\n<table cellspacing="0" cellpadding="0">';
  s+='\n<tr><td class="'+(sel==this.id&&!finalver?'boxChecked':'box')+'"'
  if (!finalver) s+=' onclick="unhover(this);choose('+this.id+');hover(this);" id="box'+this.id+'" onmouseover="hover(this)" onmouseout="unhover(this)"';
  s+='"><nobr>';
  if (finalver&&this.data.email) s+='<a href="mailto:'+this.data.email+'">';
  s+=(this.data.name||'()');
  if (finalver&&this.data.email) s+='</a>';
  s+='</nobr></td></tr></table>\n</td></tr>';
  if (this.first) s+='\n<tr><td width="50%" class="lineright">&nbsp;</td><td width="50%"></td></tr>';
  s+='</table></td></tr><tr>'+next+'</tr></table>';
  return s;
}
} //end node prototype definition

function choose(s) {
  if (attach_sel) {selnode.doAttach(s);renderIt();return;}
  if (sel) try{document.getElementById('box'+sel).className='box';}catch(e){unselect()}
  if (sel!=s) {
    sel=s;selnode=nodeBag[sel];
    document.getElementById('box'+sel).className='boxChecked';
    var f=document.forms[0];f.reset();
    for (var p in selnode.data) f[p].value=selnode.data[p];
    document.getElementById('props').style.display=document.getElementById('chartpart').style.display;
  } else {
    unselect();
  }
}

function renderIt() {
  var s='';var l="0;"
  for (var i in nodeBag)
    if (!nodeBag[i].parent) {
      if (l) s+='<HR width="70%">';
      s+=nodeBag[i].render();
      l++;
    }
  document.getElementById('chart').innerHTML=s||'&nbsp;';
}

function hover(el) {
  el.oldclass=el.className;
  el.className=el.className+'Hover';
}

function unhover(el) {
  el.className=el.oldclass;
}


