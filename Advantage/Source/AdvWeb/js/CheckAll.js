
    //checks all DataGrid CheckBoxes with the given name with the given value
    function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {

        re = new RegExp('\\$' + aspCheckBoxID + '$')  
        //generated control name starts with a colon

        for(i = 0; i < document.forms[0].elements.length; i++) {
            
            elm = document.forms[0].elements[i]
            
            if (elm.type == 'checkbox' && elm.disabled == false) {

                if (re.test(elm.name)) {

                    elm.checked = checkVal;

                }
            }
        }
    }

