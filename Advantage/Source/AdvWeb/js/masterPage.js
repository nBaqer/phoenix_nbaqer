﻿//-----------------------------------------------------------------------------
// Method that gets called whenever the user clicks a link in the menu
//-----------------------------------------------------------------------------
var navLink = function (pageUrl, text) {

    //fade out the page groups and pages div
    $(".content").fadeOut();
    $(".headerFooter").fadeOut();
    $(".heading").fadeOut();

    //open rad window if this is a popup, else redirect to page
    if (pageUrl.toLowerCase().indexOf("&ispopup=1") >= 0) openRadWindow(pageUrl);
    else window.location.href = pageUrl;
};


function openKendoWindow(pageUrl, title) {

    if ($("#kendoWindow").data("kendoWindow")) {
        $("#kendoWindow").data("kendoWindow").destroy();
        $("#RAD_SPLITTER_PANE_CONTENT_ctl00_ContentPane").append("<div id='kendoWindow'></div>");

    }
    $("#kendoWindow").kendoWindow({
        refresh: function () {

        },
        title: title,
        resizable: false,
        minWidth: 435,
        minHeight: 525,
        width: 435,
        height: 525,
        content: {
            url: pageUrl,
            iframe: true
        }
    }).data("kendoWindow").center().open();

    var windowWidget = $("#kendoWindow").data("kendoWindow");

    kendo.ui.progress(windowWidget.element, true);

    $('#kendoWindow iframe').on('load', function () {
        $(this).contents().find("body").css({ "margin-left": "5px" });
        kendo.ui.progress(windowWidget.element, false);
    });
}

//-----------------------------------------------------------------------------
// Method that gets called whenever the user clicks a link in the menu that is a
// pop up window
//-----------------------------------------------------------------------------
function openRadWindow(pageUrl) {
    var oWnd = $find('radPop');
    oWnd.setSize(800, 810);
    oWnd.setUrl(pageUrl);
    oWnd.show();
}

//-----------------------------------------------------------------------------
// Method that gets called whenever the user clicks to open the task maanager
// window from the info-bar mail icon
//-----------------------------------------------------------------------------
function openTaskRadWindow() {
    var oWnd = $find('radPop');
    oWnd.setSize(800, 810);
    oWnd.setUrl(XMASTER_GET_BASE_URL + '/defaultTMAddNew.html?addnew=true');
    oWnd.show();
}

//-----------------------------------------------------------------------------
// Open the audit history pop up window
//-----------------------------------------------------------------------------
function AuditHistory() {
    var objectId01 = document.getElementById("hdnObjectId").value;
    var objectId = document.getElementById("hdnObjectId").value;
    var pageResourceId = document.getElementById("hdnPageResourceId").value;
    //             alert(ObjectId);
    if (!(objectId == '')) {
        var url;
        url = "../SY/AuditHistDisplay.aspx?TableRowIds=" + objectId + "&resid=" + pageResourceId;
        //alert(URL);
        var oWnd = window.$find('radPop');
        oWnd.setSize(900, 650);
        oWnd.setUrl(url);
        oWnd.show();
    }

}
function updatehdnObjectId(value) {
    $('#hdnObjectId').val(value);
};

function onTabSelecting(sender, args) {
    if (args.get_tab().get_pageViewID()) {
        args.get_tab().set_postBack(false);
    }
}
function SlidingPaneResized(sender) {
    window.$telerik.repaintChildren(sender);
}
function ShowViewStateSize() {

    var buf = document.forms[0]["__VIEWSTATE"].value;
    alert("View state is " + buf.length + " bytes");
}
function OldPageResized(sender, args) {
    $telerik.repaintChildren(sender);
}
function PageResized(sender, args) {
    $telerik.repaintChildren(sender);
}
function initializeItems(sender, args) {
    var items = sender.get_masterTableView().get_dataItems();
}


function showContent(sender, args) {
    toggleContent(sender, args, true);
}

function hideContent(sender, args) {
    toggleContent(sender, args, false);
}

function toggleContent(sender, args, flag) {
    var wrapperEl = window.$telerik.getElementByClassName(args.get_gridDataItem().get_element(), "HiddenWrapper", "span");
    if (wrapperEl)
        wrapperEl.style.visibility = flag ? "visible" : "";
}

function requestStart(sender, args) {
    if (args.get_eventTarget().indexOf("DownloadPDF") > 0)
        args.set_enableAjax(false);

    if (args.get_eventTarget().indexOf("btnExportToExcel") > 0)
        args.set_enableAjax(false);


}
function PreserveState() {
    //var zone = $find('<%= LeftSlidingZone.ClientID %>');
    //var expandedPaneId =  zone.get_dockedPaneId();
    //SetCookie('dockedID', expandedPaneId);

    // SetCookie('docketID', "Navigation");

}

function OnClientLoaded(object, args) {
    //var zone = object;
    //var dockedPaneID = GetCookie('dockedID');
    //if (dockedPaneID) {
    //    setTimeout(function () {
    //        zone.dockPane(dockedPaneID);
    //    }, 0);
    // }
}

function SetCookie(sName, sValue) {
    document.cookie = sName + '=' + escape(sValue) + ';path=/;';
}

function GetCookie(sName) {
    var docCookie = document.cookie;
    if (docCookie.length > 0) {
        var begin = docCookie.indexOf(sName + '=');
        if (begin != -1) {
            end = docCookie.indexOf(';', begin);
            if (end == -1) end = docCookie.length;
            return unescape(docCookie.substring(begin + sName.length + 1, end));
        }
    }

    return null;
}
