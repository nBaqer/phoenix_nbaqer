function DoTitleCase(TextBoxName){
				var str = "document.Form1." + TextBoxName;
				var textBox = eval(str);
				var myString = textBox.value;
				var myArray = myString.split(" ");
				
				//loop through the elements in the array. For each element
				//we will capitalize the first letter.			
				for (var i=0;i < myArray.length;i++){
					var e = myArray[i];
					e = CapitalizeFirstLetter(e);
					myArray[i] = e;				
				}
				
				//Use the join method of the array object to obtain the 
				//resulting string.
				textBox.value = myArray.join(" ");
				 
			}
			
function CapitalizeFirstLetter(aString){
				var str1 = aString.substring(0,1).toUpperCase();
				var str2 = aString.substring(1);
				return str1 + str2;
			}