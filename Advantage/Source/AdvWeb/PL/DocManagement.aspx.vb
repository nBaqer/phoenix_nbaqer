' ===============================================================================
'
' FAME AdvantageV1
'
' EmployeeInfo.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports System.Diagnostics
Imports FAME.Common
Imports System.IO
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Data
Imports Telerik.Web.UI

Partial Class DocManagement
    Inherits Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents ddlScannedId As DropDownList
    Protected WithEvents txtStudentDocs As TextBox
    Protected strCount As String
    Protected WithEvents btnhistory As Button
    Protected WithEvents lblDocumentModule As Label
    Protected WithEvents ddlDocumentTypeId As DropDownList
    Protected WithEvents lblStudentName As Label
    Protected WithEvents ddlStudentName As DropDownList
    Protected StudentId As String '= "14094BD8-18CA-4B95-9195-069D47774306"

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region

    Private pObj As New UserPagePermissionInfo
    Protected selectedModule As Integer
    Protected campusId As String
    Protected campus As String
    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        ' 05/2/2012 Janet Robinson telerik bug caused dynamic end year to blow up page so it was moved here
        lblFooterText.Text = " Copyright @ FAME 2005 - " + Year(DateTime.Now).ToString + ". All rights reserved."

        Dim userId As String
        '  Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim objCommon As New CommonUtilities

        'Put user code to initialize the page here
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        StudentId = Session("StudentID")
        Session("ModuleId") = 6

        Dim AdvantageUserState As New User
        AdvantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString
        campus = AdvantageSession.UserState.CampusId.ToString
        campusId = campus
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageUserState, resourceId, campus)
        If Not Page.IsPostBack Then
            strCount = 0
            objCommon.SetCaptionsAndColorRequiredFieldsPopUps(Form1)

            'Disable the new and delete buttons
            objCommon.SetBtnStatePopups(Form1, "NEW")
            ViewState("MODE") = "NEW"

            'Bind The DropDownList
            BuildDDLs()

            'Get PK Value
            txtStudentDocId.Text = Guid.NewGuid.ToString()

            'ddlDocumentId.Enabled = False
            ddlDocumentModule.SelectedIndex = 0
            ddlModulesId.SelectedIndex = 0
            'ddlDocumentId.Enabled = True

            'By default placement will be selected
            selectedModule = 6

            'BindDataList(CInt(ddlDocumentModule.SelectedValue), ddlStudentFilter.SelectedValue)
            'BuildDocumentListDDL(CInt(ddlModulesId.SelectedValue))
            InitButtonsForLoad()
        Else
            'ddlDocumentModule.SelectedValue = 6
            objCommon.SetCaptionsAndColorRequiredFieldsPopUps(Form1)
        End If
        'If ddlDocumentId.SelectedValue = "" Or ddlDocStatusId.SelectedValue = "" Then
        '    chkOverride.Enabled = False
        'Else
        '    chkOverride.Enabled = True
        'End If
        'If campus = "" Then
        '    CloseChildWindow(Me.Page, "Close")
        'End If

        ' ddlDocumentModule.SelectedIndex = ddlDocumentModule.Items.IndexOf(ddlDocumentModule.Items.FindByText("Admissions"))
        'ddlModulesId.SelectedIndex = ddlModulesId.Items.IndexOf(ddlModulesId.Items.FindByText("Admissions"))
        'ddlDocumentModule.Enabled = False
        'ddlModulesId.Enabled = False

        'pObj = fac.GetUserResourcePermissions(userId, resourceId, Trim(campus))

        'btnNew.Enabled = True
        'btnSave.Enabled = True
    End Sub
    'Public Shared Sub CloseChildWindow(ByVal page As Page, ByVal Key As String)
    '    If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
    '        Dim scriptBegin As String = "<script type='text/javascript'>window.close;"
    '        Dim scriptEnd As String = "</script>"

    '        '   Register a javascript to display error message
    '        page.RegisterStartupScript("Close", scriptBegin + scriptEnd)
    '        'page.ClientScript.RegisterStartupScript(Me.GetType(), "Close", (scriptBegin + scriptEnd), false )
    '    End If
    'End Sub
    'Private Sub BuildStudentFilterDDL()
    '    Dim StudentName As New AdReqsFacade 'LeadFacade
    '    With ddlStudentFilter
    '        .DataTextField = "fullname"
    '        .DataValueField = "StudentId"
    '        .DataSource = StudentName.GetAllDocumentStudentNames(Trim(campus))
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildStudentDDL()
        Dim studentName As New AdReqsFacade
        Dim strStudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
        With ddlStudentId
            .DataTextField = "fullname"
            .DataValueField = "StudentId"
            .DataSource = studentName.GetAllDocumentStudentNames(Trim(campus), strStudentIdentifier)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        With ddlStudentFilter
            .DataTextField = "fullname"
            .DataValueField = "StudentId"
            .DataSource = studentName.GetAllDocumentStudentNames(Trim(campus), strStudentIdentifier)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    'Private Sub EnableBtnStudentType()
    '    If Trim(Session("StudentType")) = "ExistingStudent" Then
    '        btnNew.Enabled = False
    '    ElseIf Trim(Session("StudentType")) = "NewStudent" Then
    '        btnNew.Enabled = True
    '    End If
    'End Sub
    Private Sub BuildDocumentListDDL(ByVal ModuleID As Integer, ByVal StudentDocId As String)
        ddlDocumentId.Items.Clear()
        'Dim DocumentList As New LeadDocsFacade
        Dim docsFacade As New LeadEntranceFacade

        Dim intPrgVerId As Integer = docsFacade.getPrgVersionByStudent(ddlStudentId.SelectedValue)
        With ddlDocumentId
            .DataTextField = "DocumentDescrip"
            .DataValueField = "DocumentId"
            If intPrgVerId >= 1 Then
                .DataSource = docsFacade.GetAllExistingStandardDocumentsByStudentEnrollment(ddlStudentId.SelectedValue, ModuleID, StudentDocId, campus)
                'docsFacade.GetAllStandardDocumentsByStudentEnrollment(ddlStudentId.SelectedValue, ModuleID, campus, StudentDocId)
            End If
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildAllDocumentListDDL()
        'Bind the Document DropDownList
        Dim documentList As New LeadDocsFacade
        With ddlDocumentId
            .DataTextField = "DocumentDescrip"
            .DataValueField = "DocumentId"
            .DataSource = documentList.GetAllDocumentsByStudent(campus)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildDocumentStatusDDL(Optional ByVal StudentDocId As String = "")
        'Bind the Document DropDownList
        Dim documentStatus As New StudentDocsFacade
        With ddlDocStatusId
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = documentStatus.GetAllDocumentStatus(campus, StudentDocId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
   
    ''' <summary>
    ''' The BuildDocumentsTypesList method gets data from database and bind data into ddlDocTypeSearchControl.
    ''' </summary>
    Private Sub BindDocumentsTypesList()
        'Bind the DocumentType DropDownList
        Dim documentList As New LeadDocsFacade
        With ddlDocTypeSearchControl
            .DataTextField = "DocumentTypeDescription"
            .DataValueField = "DocumentTypeId"
            .DataSource = documentList.GetDocumentTypes(campus)
            .DataBind()
        End With
    End Sub

    'Private Sub BuildModuleDDL()
    '    'Bind the Document DropDownList
    '    Dim DocumentStatus As New StudentDocsFacade
    '    With ddlModulesId
    '        .DataTextField = "ModuleName"
    '        .DataValueField = "ModuleId"
    '        .DataSource = DocumentStatus.GetAllDocumentModules()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildDocumentModuleDDL()
        'Bind the Document DropDownList
        Dim documentStatus As New StudentDocsFacade
        With ddlDocumentModule
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleId"
            .DataSource = documentStatus.GetAllDocumentModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        With ddlModulesId
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleId"
            .DataSource = documentStatus.GetAllDocumentModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildDocumentFilterStatusDDL()
        'Bind the Document DropDownList
        Dim documentStatus As New StudentDocsFacade
        With ddlDocFilterStatus
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = documentStatus.GetAllDocumentStatus(campus)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        With ddlDocStatusId
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = documentStatus.GetAllDocumentStatus(campus)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildDDLs()
        BuildDocumentModuleDDL()
        BuildDocumentFilterStatusDDL()
        BuildStudentDDL()
        BuildAllDocumentListDDL()
        BindDocumentsTypesList()
        BindDataList()
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim studentDocs As New StudentDocsFacade
        Dim result As String
        '  Dim StudentDocsId As String
        Dim message As String = ""
        strCount = 0

        If ddlStudentId.SelectedValue = "" Then
            message = "Student is required " & vbLf
        End If
        If ddlDocumentId.SelectedValue = "" Then
            message &= "Document is required" & vbLf
        End If
        If ddlDocStatusId.SelectedValue = "" Then
            message &= "Document status is required" & vbLf
        End If
        If ddlModulesId.SelectedValue = "" Then
            message &= "Module is required" & vbLf
        End If

        Dim intValidateDate As Integer
        If Not txtRequestDate.SelectedDate Is Nothing Then
            intValidateDate = ValidateDateFormat(CType(txtRequestDate.SelectedDate, String))
            If intValidateDate = -1 Then
                message &= "Invalid date format for Requested Date" & vbLf
            End If
        End If

        If Not txtReceiveDate.SelectedDate Is Nothing Then
            intValidateDate = ValidateDateFormat(txtReceiveDate.SelectedDate)
            If intValidateDate = -1 Then
                message &= "Invalid date format for Received Date" & vbLf
            End If
        End If

        If Not message = "" Then
            DisplayErrorMessage(message)
            Exit Sub
        End If

        'Call Update Function in the plEmployerInfoFacade
        result = studentDocs.UpdateStudentDocs(BuildStudentDocsInfo(), CType(Session("UserName"), String), txtStudentDocId.Text, CType(ddlModulesId.SelectedValue, Integer))

        '  If DML is not successful then Prompt Error Message
        If Not result = "" Then
            DisplayErrorMessage(result)
            Exit Sub
        End If


        'Reset The Checked Property Of CheckBox To True
        ChkIsInDB.Checked = True

        ''Bind The DataList Based On StudentId and Education Institution Type.
        'BindDataList(CInt(ddlDocumentModule.SelectedValue), ddlStudentFilter.SelectedValue)

        GetDocumentsByStatus(ddlDocFilterStatus.SelectedValue, ddlStudentFilter.SelectedValue, ddltypeofreq.SelectedValue, ddlDocTypeSearchControl.SelectedValue)

        'Reset The Filter Dropdown List
        'ddlDocumentModule.SelectedValue = ddlModulesId.SelectedValue
        'ddlDocFilterStatus.SelectedIndex = 0


        UploadFile()

        'If Page is free of errors Show Edit Buttons
        If Page.IsValid Then
            InitButtonsForEdit()
        End If
        'btnSave.Enabled = True
        'btnNew.Enabled = True
        'btnDelete.Enabled = True
        RadGrid1.Visible = True
    End Sub
    Private Function BuildStudentDocsInfo() As StudentDocsInfo
        Dim StudentDocsInfo As New StudentDocsInfo
        Dim intDocHasBeenApproved As Integer
        intDocHasBeenApproved = (New LeadDocsFacade).CheckIfDocHasBeenApproved(ddlDocStatusId.SelectedValue)
        With StudentDocsInfo
            .IsInDb = ChkIsInDB.Checked
            .StudentId = ddlStudentId.SelectedValue
            .DocumentId = ddlDocumentId.SelectedValue
            .DocStatusId = ddlDocStatusId.SelectedValue
            If txtRequestDate.SelectedDate Is Nothing Then
                .RequestDate = ""
            Else
                .RequestDate = Date.Parse(txtRequestDate.SelectedDate)
            End If
            If txtReceiveDate.SelectedDate Is Nothing Then
                .ReceiveDate = ""
            Else
                .ReceiveDate = Date.Parse(txtReceiveDate.SelectedDate)
            End If
            '.RequestDate = txtRequestDate.Text
            '.ReceiveDate = txtReceiveDate.Text
            .ScannedId = chkScannedId.Checked
            .StudentDocsId = txtStudentDocId.Text
            .ModuleId = ddlModulesId.SelectedValue
            If intDocHasBeenApproved >= 1 Then
                chkOverride.Checked = False
                .Override = False
            Else
                .Override = chkOverride.Checked
            End If
        End With
        Return StudentDocsInfo
    End Function
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Sub BindDataList()
        With New LeadFacade
            'dlstDocumentStatus.DataSource = .GetDocsStudent(ModuleId, Studentid, Trim(campus))
            dlstDocumentStatus.DataSource = .GetAllDocsStudentByCampus(Trim(campus))
            dlstDocumentStatus.DataBind()
        End With
    End Sub
    'Private Sub BindDataListNoModule(ByVal Studentid As String)
    '    With New LeadFacade
    '        dlstDocumentStatus.DataSource = .GetDocsStudentNoModule(Studentid, Trim(campus))
    '        dlstDocumentStatus.DataBind()
    '    End With
    'End Sub
    Private Sub dlstDocumentStatus_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstDocumentStatus.ItemCommand
        ChkIsInDB.Checked = True
        txtStudentDocId.Text = e.CommandArgument
        GetStudentInfo(e.CommandArgument)

        'set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstDocumentStatus, e.CommandArgument, ViewState)
        CommonWebUtilities.RestoreItemValues(dlstDocumentStatus, e.CommandArgument)
        Dim strStudentId As String = Trim(ddlStudentId.SelectedValue)
        Dim facade As New DocumentManagementFacade
        Dim strStudentName As String = facade.GetStudentNamesById(strStudentId)
        Dim strFileNameOnly As String = strStudentName & "_" & strStudentId

        txtExtension.Text = facade.GetDocumentExtension(strFileNameOnly, ddlDocumentId.SelectedItem.Text)
        txtDocumentName.Text = ddlDocumentId.SelectedItem.Text
        txtStudentName.Text = strStudentName
        txtStudentId.Text = strStudentId
        ddlDocumentId.Enabled = True

        'btnSave.Enabled = False

        pnlViewDoc.Visible = False
        dgrdDocs.Visible = False
        pnlUpload.Visible = False

        'Refresh The DataGrid With Document Types
        BuildFileDataGrid(strFileNameOnly)
        InitButtonsForEdit()
        'btnSave.Enabled = True
        'btnDelete.Enabled = True
        RadGrid1.Visible = True
    End Sub
    Private Sub GetStudentInfo(ByVal StStudentDocId As String)
        Dim studentinfo As New StudentDocsFacade
        BindStudentInfoExist(studentinfo.GetStudentDocs(StStudentDocId), StStudentDocId)
    End Sub
    Private Sub BindStudentInfoExist(ByVal StudentDocsInfo As StudentDocsInfo, ByVal StudentDocId As String)
        'Bind The StudentInfo Data From The Database
        With StudentDocsInfo
            'txtRequestDate.Text = .RequestDate
            Try
                txtRequestDate.SelectedDate = .RequestDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtRequestDate.Clear()
            End Try
            'txtReceiveDate.Text = .ReceiveDate
            Try
                txtReceiveDate.SelectedDate = .ReceiveDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtReceiveDate.Clear()
            End Try
            ddlModulesId.SelectedValue = .ModuleId
            ddlStudentId.SelectedValue = .StudentId
            Try
                BuildDocumentListDDL(.ModuleId, StudentDocId)
                ddlDocumentId.SelectedValue = .DocumentId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlDocumentId.SelectedIndex = 0
            End Try
            Try
                BuildDocumentStatusDDL(StudentDocId)
                ddlDocStatusId.SelectedValue = .DocStatusId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlDocStatusId.SelectedIndex = 0
            End Try
            If .ScannedId = "1" Then
                chkScannedId.Checked = True
            Else
                chkScannedId.Checked = False
            End If
            chkOverride.Checked = .Override
        End With
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        'Reset The Value of chkIsInDb Checkbox
        'To Identify an Insert
        BindEmploymentInfo()
        txtStudentDocId.Text = Guid.NewGuid.ToString()
        InitButtonsForLoad()
        dgrdDocs.Visible = False
        pnlViewDoc.Visible = True
        ' btnSave.Enabled = True
        'btnDelete.Enabled = False
        BuildAllDocumentListDDL()
        BuildDocumentStatusDDL("")
        Dim dt As New DataTable
        dt.Columns.Add("DisplayName")
        RadGrid1.DataSource = dt
        RadGrid1.DataBind()
        RadGrid1.Visible = False
        InitButtonsForLoad()
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub BindEmploymentInfo()
        txtRequestDate.Clear()
        txtReceiveDate.Clear()
        ddlStudentId.SelectedIndex = 0
        ddlModulesId.SelectedIndex = 0
        ddlDocumentId.SelectedIndex = 0
        ddlDocStatusId.SelectedIndex = 0
        chkScannedId.Checked = False
        ChkIsInDB.Checked = False
        chkOverride.Checked = False
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        If Not (StudentId = Guid.Empty.ToString) Then
            '    '   instantiate component
            Dim StudentDocs As New StudentDocsFacade

            '    'Delete The Row Based on StudentId
            Dim result As Integer = StudentDocs.DeleteStudentDocs(txtStudentDocId.Text, ddlStudentId.SelectedValue, ddlDocumentId.SelectedValue)

            '    'If Delete Operation was unsuccessful
            If result < 0 Then
                DisplayErrorMessage("Related record exists")
                Exit Sub
            End If
            Dim strStudentId As String = Trim(ddlStudentId.SelectedValue)
            BindEmploymentInfo()
            ChkIsInDB.Checked = False
            GetDocumentsByStatus(ddlDocFilterStatus.SelectedValue, ddlStudentFilter.SelectedValue, ddltypeofreq.SelectedValue, ddlDocTypeSearchControl.SelectedValue)

            'Dim strStudentId As String = Trim(ddlStudentId.SelectedValue)
            Dim facade As New DocumentManagementFacade
            Dim strStudentName As String = facade.GetStudentNamesById(strStudentId)
            Dim strFileNameOnly As String = strStudentName & "_" & strStudentId

            BuildFileDataGrid(strFileNameOnly)
            pnlViewDoc.Visible = False
            dgrdDocs.Visible = False
            'initialize buttons
            InitButtonsForLoad()
        End If
        ddlDocumentModule.ClearSelection()
    End Sub
    Private Sub btnApplyFilter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnApplyFilter.Click
        BindEmploymentInfo()
        Dim facade As New LeadFacade
        GetDocumentsByStatus(ddlDocFilterStatus.SelectedValue, ddlStudentFilter.SelectedValue, ddltypeofreq.SelectedValue, ddlDocTypeSearchControl.SelectedValue)
        pnlViewDoc.Visible = False
        Dim dt As New DataTable
        dt.Columns.Add("DisplayName")
        RadGrid1.DataSource = dt
        RadGrid1.DataBind()
        RadGrid1.Visible = False
        dgrdDocs.Visible = False
        InitButtonsForLoad()
    End Sub
    Private Sub GetDocumentsByStatus(ByVal DocumentStatusId As String, ByVal StudentId As String, ByVal TypeofRequirement As Integer, ByVal DocumentTypeId As String)
        Dim intModuleId As Integer
        If ddlDocumentModule.SelectedValue = "" Then
            intModuleId = 0
        Else
            intModuleId = CInt(ddlDocumentModule.SelectedValue)
        End If
        With New LeadFacade
            dlstDocumentStatus.DataSource = .GetDocsStudentByModuleIdAndStatus(intModuleId, StudentId, campus, DocumentStatusId, TypeofRequirement, DocumentTypeId)
            dlstDocumentStatus.DataBind()
        End With
    End Sub
    Private Sub ddlModulesId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlModulesId.SelectedIndexChanged
        ddlDocumentId.Enabled = True
        If ddlModulesId.SelectedIndex >= 1 Then
            BuildDocumentListDDL(ddlModulesId.SelectedValue, "")
        Else
            BuildDocumentListDDL(0, "")
        End If
    End Sub
    Private Sub ddlDocStatusId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlDocStatusId.SelectedIndexChanged
    End Sub
    Private Sub ddlDocumentModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlDocumentModule.SelectedIndexChanged
        If Not ddlDocumentModule.SelectedIndex = 0 Then
            Session("ModuleID") = ddlDocumentModule.SelectedValue
        Else
            Session("ModuleID") = ""
        End If
    End Sub
    Private Sub chkScannedId_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkScannedId.CheckedChanged
        If chkScannedId.Checked = True Then
            pnlUpload.Visible = False
        Else
            pnlUpload.Visible = False
        End If
    End Sub
    Private Sub BuildDocumentDDL()
        'Bind the County DrowDownList
        Dim Documents As New DocumentManagementFacade
        With ddlDocumentTypeId
            .DataTextField = "DocumentDescrip"
            .DataValueField = "DocumentId"
            .DataSource = Documents.GetAllDocumentManagement()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildStudentsDDL()
        'Bind the County DrowDownList
        Dim Documents As New DocumentManagementFacade
        With ddlStudentName
            .DataTextField = "FullName"
            .DataValueField = "StudentId"
            .DataSource = Documents.GetStudentNames()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Function CreateDirectory(ByVal FolderName As String) As String
        Dim strDirectoryPath = MyAdvAppSettings.AppSettings("CreateDocumentPath")
        Dim oDirectoryInfo As DirectoryInfo
        '        Dim oSubDirectoryInfo As DirectoryInfo
        Dim strDirectory As String
        If Directory.Exists(strDirectoryPath & FolderName) = False Then
            'oDirectoryInfo = Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\Docs\" + FolderName)
            oDirectoryInfo = Directory.CreateDirectory(strDirectoryPath + FolderName)
            strDirectory = strDirectoryPath & FolderName
        Else
            strDirectory = strDirectoryPath & FolderName
        End If
        Return strDirectory
    End Function
    Private Sub btnUpload_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpload.Click
        Dim strDocumentInsert As New DocumentManagementFacade
        Dim Result As Integer
        Dim strDirectory As String
        Dim sMessage As String = ""

        If ddlStudentId.SelectedValue = "" Then
            sMessage = "Student is required" & vbLf
        End If

        If ddlModulesId.SelectedIndex = 0 Then
            sMessage &= "Module is required" & vbLf
        End If

        If ddlDocumentId.SelectedValue = "" Then
            sMessage &= "Document is required" & vbLf
        End If

        If ddlDocStatusId.SelectedIndex = 0 Then
            sMessage &= "Document status is required" & vbLf
        End If

        If Not sMessage = "" Then
            DisplayErrorMessage(sMessage)
            Exit Sub
        End If

        If Not txtUpLoad.PostedFile Is Nothing And Not txtUpLoad.Value = "" Then

            'Get Directory
            Dim strDocumentType As String = ddlDocumentId.SelectedItem.Text
            strDirectory = CreateDirectory(strDocumentType)

            'Get FileName from the client machine
            Dim strExt As String = txtUpLoad.PostedFile.FileName.Substring(txtUpLoad.PostedFile.FileName.LastIndexOf("."))

            'Get Friendly Name for storing
            Dim slashposition As Integer = txtUpLoad.PostedFile.FileName.LastIndexOf("\")
            Dim strDisplayName As String = txtUpLoad.PostedFile.FileName.Substring(slashposition + 1)
            strDisplayName = strDisplayName.ToString.Split(".")(0)

            Dim intBackSlashPos As Integer = InStrRev(txtUpLoad.PostedFile.FileName, "\")
            If intBackSlashPos < 1 Then
                intBackSlashPos = InStrRev(txtUpLoad.PostedFile.FileName, "/")
            End If
            Dim strOrigFileName As String = Mid(txtUpLoad.PostedFile.FileName, intBackSlashPos + 1)
            Dim strFindExtPos As Integer = InStr(strOrigFileName, ".")
            Dim strUserFileName As String = Mid(strOrigFileName, 1, strFindExtPos - 1)

            'Get Size of file
            Dim filesize As Integer = txtUpLoad.PostedFile.ContentLength

            'Get type of file
            Dim facade As New DocumentManagementFacade
            Dim strStudentId As String = Trim(ddlStudentId.SelectedValue)
            Dim strStudentName As String = facade.GetStudentNamesById(strStudentId)
            Dim filetype As String = txtUpLoad.PostedFile.ContentType
            Dim strFileNameOnly As String = strStudentName & "_" & strStudentId & "_" & strUserFileName
            Dim strFileName As String = strStudentName & "_" & strStudentId & "_" & strUserFileName & strExt

            If filesize <= 0 Then
                DisplayErrorMessage("Document upload process failed")
            Else
                txtUpLoad.PostedFile.SaveAs(strDirectory + "\" + strFileNameOnly + strExt)
                Result = strDocumentInsert.InsertDocument(strFileNameOnly, strExt, Session("UserName"), ddlDocumentId.SelectedItem.Text, ddlStudentId.SelectedValue, ddlDocumentId.SelectedValue, ddlModulesId.SelectedValue, strDisplayName)
                If Result = 0 Then
                    DisplayErrorMessage("Document uploaded successfully")
                End If
            End If
            Dim getExtension As New DocumentManagementFacade
            txtExtension.Text = getExtension.GetDocumentExtension(strFileNameOnly, ddlDocumentId.SelectedItem.Text)

            txtDocumentName.Text = strDocumentType
            txtStudentName.Text = strStudentName
            txtStudentId.Text = strStudentId

            'Refresh The DataGrid With Document Types
            BuildFileDataGrid(strFileNameOnly)
            pnlViewDoc.Visible = False
            dgrdDocs.Visible = False
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub UploadFile()
    End Sub
    Private Sub lnkViewDoc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkViewDoc.Click
        Session("DocumentName") = txtDocumentName.Text
        Session("StudentName") = txtStudentName.Text
        Session("StudentId") = txtStudentId.Text
        Session("Path") = AppDomain.CurrentDomain.BaseDirectory
        Session("DocumentType") = ddlDocumentId.SelectedItem.Text
        Session("Extension") = txtExtension.Text
        txtDocumentType.Text = Session("DocumentType")
    End Sub
    Private Sub BuildFileDataGrid(Optional ByVal FileName As String = "")
        Dim File As New DocumentManagementFacade
        'dgrdDocs.DataSource = File.GetAllDocumentsByStudentDocType(FileName, ddlDocumentId.SelectedItem.Text)
        'dgrdDocs.DataBind()
        Dim dsnew As New DataSet
        If FileName <> "" Then
            dsnew = File.GetAllDocumentsByStudentDocType(FileName, ddlDocumentId.SelectedItem.Text)
            dgrdDocs.DataSource = dsnew
            dgrdDocs.DataBind()
        End If


        If ddlDocumentId.SelectedValue <> "" Then
            dsnew = File.GetAllDocumentsByStudentIdandDocumentID(ddlStudentId.SelectedValue, ddlDocumentId.SelectedValue)
        End If

        RadGrid1.DataSource = dsnew
        RadGrid1.DataBind()
    End Sub
    Private Sub dgrdDocs_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgrdDocs.ItemCommand
        Dim FileName As String = e.CommandArgument
        Dim DocumentType As String = ddlDocumentId.SelectedItem.Text
        Dim result As String
        Dim docfacade As New DocumentManagementFacade

        If e.CommandName = "DeleteFile" Then
            'Delete The File,ReBind The DataList and Exit Procedure
            Dim strPath As String = MyAdvAppSettings.AppSettings("DocumentPath")
            Dim strFileNameWithExtension As String = CType(e.Item.FindControl("Linkbutton1"), LinkButton).CommandArgument
            Dim strExtension As String = Mid(strFileNameWithExtension, InStrRev(strFileNameWithExtension, ".") + 1)
            Dim strFullPath As String = strPath + DocumentType + "\" + FileName + "." + strExtension
            File.Delete(strFullPath)
            result = docfacade.DeleteDocument(FileName, ddlModulesId.SelectedValue, ddlDocumentId.SelectedValue)
            If Not result = "" Then
                DisplayErrorMessage(result)
            End If
            BuildFileDataGrid(FileName)
            chkScannedId.Checked = False
            Exit Sub
        End If
        Session("File2Browse") = MyAdvAppSettings.AppSettings("DocumentPath") & DocumentType.Replace("/", "\") & "\" & FileName

        Dim popupScript As String = "<script type='text/javascript'>window.Onload=OpenDocuments1();function OpenDocuments1(){window.open('../FileBrowser.aspx','HistWin','width=700,height=600,resizable=yes');}</script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "PopUpScript", popupScript)
    End Sub

    Private Sub btnGetDoc_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetDoc.Click
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsMedium
        Dim name As String = "LibertyIMS"
        Dim url As String = "http://127.0.0.1:2080/LibertyIMS::/anon/CMD%3DXMLGetRequest%3BName%3D%232b%3BNoUI%3D1%3BF0%3D" + ddlModulesId.SelectedItem.Text + "%3BF1%3D" + txtStudentName.Text + " %3BF2%3D" + txtDocumentName.Text
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub

    Private Sub btnApplyFilter_Command(ByVal sender As Object, ByVal e As CommandEventArgs) Handles btnApplyFilter.Command

    End Sub
    Private Function ValidateDateFormat(ByVal strDate As String) As Integer
        Try
            Dim intMonth As Integer
            'Check if the date starts from Jan-Dec

            If Not IsDate(CDate(strDate)) Then
                Return -1
            End If

            intMonth = Month(CDate(strDate))
            If intMonth >= 1 And intMonth <= 12 Then
                Return 0
            Else
                Return -1
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return -1
        End Try
    End Function
    'Public Function WinHTTPPostRequest()
    '    'Dim i As Integer
    '    'Dim http As Object
    '    'Dim urlString As String

    '    'urlString = "http://aphost.net:9496/143"

    '    'http = New WinHttpRequest

    '    ''Open URL as Post Request
    '    'http.open("Post", urlString, False)


    'End Function
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnApplyFilter)
        controlsToIgnore.Add(btnUpload)
        controlsToIgnore.Add(ddlStudentId)
        controlsToIgnore.Add(ddlModulesId)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    'Private Sub BuildDropDownLists()
    '    'this is the list of ddls
    '    Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()


    '    'Countries DDL 
    '    ddlList.Add(New AdvantageDDLDefinition(ddlDocumentModule, AdvantageDropDownListName.Modules, campusId, True, True))

    '    'Countries DDL 
    '    ddlList.Add(New AdvantageDDLDefinition(ddlDocFilterStatus, AdvantageDropDownListName.DocStatuses, campusId, True, True, String.Empty))

    '    'States DDL
    '    ddlList.Add(New AdvantageDDLDefinition(ddlDocStatusId, AdvantageDropDownListName.DocStatuses, campusId, True, True, String.Empty))

    '    'States DDL
    '    ddlList.Add(New AdvantageDDLDefinition(ddlStudentId, AdvantageDropDownListName.StudentsWithEnrollments, campusId, True, True))

    '    'Shifts DDL
    '    ddlList.Add(New AdvantageDDLDefinition(ddlDocumentId, AdvantageDropDownListName.Documents, campusId, True, True, String.Empty))


    '    'Build DDLs
    '    CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)


    'End Sub
    Protected Sub RadGrid1_DeleteCommand(sender As Object, e As GridCommandEventArgs)

        Dim documentType As String = ddlDocumentId.SelectedItem.Text
        Dim result As String
        Dim docfacade As New DocumentManagementFacade

        Dim ID As String = TryCast(e.Item, GridDataItem).OwnerTableView.DataKeyValues(e.Item.ItemIndex)("FileID").ToString()

        Dim DeleteItem As GridDataItem = TryCast(e.Item, GridDataItem)

        Dim strPath As String = CType(MyAdvAppSettings.AppSettings("DocumentPath"), String)
        Dim imageName As String = TryCast(DeleteItem("DisplayName").FindControl("lblName"), Label).Text
        ' Dim strFileNameWithExtension As String = DeleteItem.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("FileName").ToString()
        '  Dim strExtension As String = Mid(strFileNameWithExtension, InStrRev(strFileNameWithExtension, ".") + 1)
        '  Dim StrExtension As String = TryCast(e.Item, GridDataItem).OwnerTableView.DataKeyValues(e.Item.ItemIndex)("DocumentCategory").ToString()
        '        Dim strFullPath As String = strPath + DocumentType + "\" + imageName ''".gif" ''
        Dim extnposition As Integer

        extnposition = imageName.LastIndexOf(".", StringComparison.Ordinal)

        Dim extn As String
        extn = imageName.Substring(extnposition, imageName.Length - extnposition)


        Dim strFullPath As String = strPath + documentType + "\" + ID + extn

        If File.Exists(strFullPath) Then
            File.Delete(strFullPath)
        End If
        'Delete The File,ReBind The DataList and Exit Procedure
        result = docfacade.DeleteDocumentbyID(ID)
        If Not result = "" Then
            DisplayErrorMessage(result)
        End If
        BuildFileDataGrid()
        chkScannedId.Checked = False
        Exit Sub

    End Sub

    Private Function ValidatePage() As String
        Dim strDocumentInsert As New DocumentManagementFacade
        Dim sMessage As String = ""

        If ddlStudentId.SelectedValue = Guid.Empty.ToString Then
            sMessage = "Student is required" & vbLf
        End If

        If ddlDocumentId.SelectedValue = "" Then
            sMessage &= "Document is required" & vbLf
        End If

        If ddlModulesId.SelectedValue = "" Then
            sMessage &= "Module is required" & vbLf
        End If

        If ddlDocStatusId.SelectedValue = "" Then
            sMessage &= "Document Status is required" & vbLf
        End If

        If Not sMessage = "" Then
            '   DisplayErrorMessage(sMessage)
            Return sMessage
            '  Exit Function
        End If

        ''Check if the student record is saved
        ''If the record is not saved then force the user to save the document info before uploading the document.
        Dim isStudentDocumentRecordCreated As Boolean
        isStudentDocumentRecordCreated = strDocumentInsert.IsStudentDocumentAlreadyCreated(Trim(ddlStudentId.SelectedValue), Trim(ddlDocumentId.SelectedValue))
        If Not isStudentDocumentRecordCreated Then
            Return (" Please save the information to upload the document ")
            
        End If
        Return ""
    End Function
    Protected Sub RadGrid1_InsertCommand(sender As Object, e As GridCommandEventArgs)

        Dim strDirectory As String
        Dim strDocumentInsert As New DocumentManagementFacade
        Dim smessage As String
        Dim result As String
        smessage = ValidatePage()
        If Not smessage = "" Then
            DisplayErrorMessage(smessage)
            e.Canceled = True
            RadGrid1.MasterTableView.IsItemInserted = False
            RadGrid1.MasterTableView.ClearSelectedItems()
            RadGrid1.Rebind()
            Exit Sub
        End If


        Dim insertItem As GridEditFormInsertItem = TryCast(e.Item, GridEditFormInsertItem)
        '    Dim imageName As String = TryCast(insertItem("DisplayName").FindControl("txbName"), RadTextBox).Text
        Dim radAsyncUpload As RadAsyncUpload = TryCast(insertItem("Upload").FindControl("AsyncUpload1"), RadAsyncUpload)
        '' Dim file As UploadedFile = radAsyncUpload.UploadedFiles(0)
        If radAsyncUpload.UploadedFiles.Count > 0 Then
            For Each file As UploadedFile In radAsyncUpload.UploadedFiles
                'Get Directory
                Dim strDocumentType As String = ddlDocumentId.SelectedItem.Text
                strDirectory = CreateDirectory(strDocumentType)

                'Get FileName from the client machine
                Dim strExt As String = file.FileName.Substring(file.FileName.LastIndexOf(".")).ToLower()

                'Get Friendly Name for storing
                Dim slashposition As Integer = file.FileName.LastIndexOf("\")
                Dim strDisplayName As String = file.FileName.Substring(slashposition + 1)
                strDisplayName = strDisplayName.ToString.Split(".")(0)

                Dim intBackSlashPos As Integer = InStrRev(file.FileName, "\")
                If intBackSlashPos < 1 Then
                    intBackSlashPos = InStrRev(file.FileName, "/")
                End If
                Dim strOrigFileName As String = Mid(file.FileName, intBackSlashPos + 1)
                Dim strFindExtPos As Integer = InStr(strOrigFileName, ".")
                Dim strUserFileName As String = Mid(strOrigFileName, 1, strFindExtPos - 1)

                'Get Size of file
                Dim filesize As Integer = file.ContentLength

                'Get type of file
                Dim facade As New DocumentManagementFacade
                Dim strStudentId As String = Trim(ddlStudentId.SelectedValue)
                Dim strStudentName As String = facade.GetStudentNamesById(strStudentId)
                'Dim filetype As String = file.ContentType
                Dim strFileNameOnly As String = Server.UrlEncode(strStudentName & "_" & strStudentId & "_" & strUserFileName).ToLower()
                'Dim strFileName As String = strStudentName & "_" & strStudentId & "_" & strUserFileName & strExt


                If filesize <= 0 Then
                    DisplayErrorMessage("Document upload process failed")
                Else

                    result = strDocumentInsert.InsertDocumentbyID(strFileNameOnly, strExt, Session("UserName"), ddlDocumentId.SelectedItem.Text, ddlStudentId.SelectedValue, ddlDocumentId.SelectedValue, ddlModulesId.SelectedValue, strDisplayName)

                    If result <> "" Then
                        file.SaveAs(strDirectory + "\" + result + strExt)
                        DisplayErrorMessage("Document uploaded successfully")
                    Else
                        DisplayErrorMessage("Data not saved")
                    End If
                End If
            Next
        End If
        e.Canceled = True
        RadGrid1.MasterTableView.IsItemInserted = False
        RadGrid1.MasterTableView.ClearSelectedItems()
        RadGrid1.Rebind()


    End Sub

    Protected Sub RadGrid1_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs) Handles RadGrid1.ItemCommand
        If e.CommandName = RadGrid.EditCommandName Then
            ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "SetEditMode", "isEditMode = true;", True)
        End If
        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked
            Dim strDocumentInsert As New DocumentManagementFacade
            Dim isStudentDocumentRecordCreated As Boolean
            isStudentDocumentRecordCreated = strDocumentInsert.IsStudentDocumentAlreadyCreated(Trim(ddlStudentId.SelectedValue), Trim(ddlDocumentId.SelectedValue))
            If Not isStudentDocumentRecordCreated Then
                e.Canceled = True
                RadGrid1.MasterTableView.IsItemInserted = False
                RadGrid1.MasterTableView.ClearSelectedItems()
                RadGrid1.Rebind()
            Else
                e.Canceled = True
                Dim newValues As ListDictionary = New ListDictionary()
                newValues("DisplayName") = ""
                ''Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues)
            End If
        End If
            If e.CommandName = "StudentSearch" Then
                'Dim FileName As String = e.CommandArgument
                Session("File2Browse") = e.CommandArgument


                Dim img As LinkButton = TryCast(e.Item.FindControl("Linkbutton1"), LinkButton)
                img.Attributes.Add("target", "_blank")
                Const popupScript As String = "<script type='text/javascript'>window.Onload=OpenDocuments1();function OpenDocuments1(){window.open('../FileBrowser.aspx','_blank','HistWin','width=700,height=600,resizable=yes'); }</script>"
                ScriptManager.RegisterStartupScript(RadGrid1, RadGrid1.GetType(), "PopUpScript", popupScript, True)


            End If

    End Sub



    Protected Sub RadGrid1_ItemCreated(sender As Object, e As GridItemEventArgs) Handles RadGrid1.ItemCreated
        If TypeOf e.Item Is GridEditableItem AndAlso e.Item.IsInEditMode Then
            Dim upload As RadAsyncUpload = TryCast(DirectCast(e.Item, GridEditableItem)("Upload").FindControl("AsyncUpload1"), RadAsyncUpload)
            Dim cell As TableCell = DirectCast(upload.Parent, TableCell)

            Dim validator As New CustomValidator()
            validator.ErrorMessage = "Please select file to be uploaded"
            validator.ClientValidationFunction = "validateRadUpload"
            validator.Display = ValidatorDisplay.Dynamic
            cell.Controls.Add(validator)
        End If
    End Sub

    Protected Sub RadGrid1_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs) Handles RadGrid1.NeedDataSource
        If ddlDocumentId.SelectedValue <> "" Then
            Dim file As New DocumentManagementFacade
            Dim dsnew As  DataSet

            dsnew = file.GetAllDocumentsByStudentIdandDocumentID(ddlStudentId.SelectedValue, ddlDocumentId.SelectedValue)

            RadGrid1.DataSource = dsnew
        End If
    End Sub

    Protected Sub RadGrid1_UpdateCommand(sender As Object, e As GridCommandEventArgs)
        Dim strDirectory As String
        Dim strDocumentInsert As New DocumentManagementFacade
        Dim smessage As String
        Dim result As Integer
        smessage = ValidatePage()
        If Not smessage = "" Then
            DisplayErrorMessage(smessage)
            Exit Sub
        End If

        Dim editedItem As GridEditableItem = TryCast(e.Item, GridEditableItem)
        Dim ID As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("FileID").ToString()
        '   Dim imageName As String = TryCast(editedItem("DisplayName").FindControl("txbName"), RadTextBox).Text
        '   Dim description As String = TryCast(editedItem("Description").FindControl("txbDescription"), RadTextBox).Text
        Dim radAsyncUpload As RadAsyncUpload = TryCast(editedItem("Upload").FindControl("AsyncUpload1"), RadAsyncUpload)


        '   If Not txtUpLoad.PostedFile Is Nothing And Not txtUpLoad.Value = "" Then
        If radAsyncUpload.UploadedFiles.Count > 0 Then


            'Get Directory
            Dim strDocumentType As String = ddlDocumentId.SelectedItem.Text
            strDirectory = CreateDirectory(strDocumentType)

            'Get FileName from the client machine
            Dim strExt As String = radAsyncUpload.UploadedFiles(0).FileName.Substring(radAsyncUpload.UploadedFiles(0).FileName.LastIndexOf(".")).ToLower()

            'Get Friendly Name for storing
            Dim slashposition As Integer = radAsyncUpload.UploadedFiles(0).FileName.LastIndexOf("\")
            Dim strDisplayName As String = radAsyncUpload.UploadedFiles(0).FileName.Substring(slashposition + 1)
            strDisplayName = strDisplayName.ToString.Split(".")(0)

            Dim intBackSlashPos As Integer = InStrRev(radAsyncUpload.UploadedFiles(0).FileName, "\")
            If intBackSlashPos < 1 Then
                intBackSlashPos = InStrRev(radAsyncUpload.UploadedFiles(0).FileName, "/")
            End If
            Dim strOrigFileName As String = Mid(radAsyncUpload.UploadedFiles(0).FileName, intBackSlashPos + 1)
            Dim strFindExtPos As Integer = InStr(strOrigFileName, ".")
            Dim strUserFileName As String = Mid(strOrigFileName, 1, strFindExtPos - 1)

            'Get Size of file
            Dim filesize As Integer = CType(radAsyncUpload.UploadedFiles(0).ContentLength, Integer)

            'Get type of file
            Dim facade As New DocumentManagementFacade
            Dim strStudentId As String = Trim(ddlStudentId.SelectedValue)
            Dim strStudentName As String = facade.GetStudentNamesById(strStudentId)
            Dim filetype As String = radAsyncUpload.UploadedFiles(0).ContentType
            Dim strFileNameOnly As String = Server.UrlEncode(strStudentName & "_" & strStudentId & "_" & strUserFileName).ToLower()
            Dim strFileName As String = strStudentName & "_" & strStudentId & "_" & strUserFileName & strExt

            If filesize <= 0 Then
                DisplayErrorMessage("Document upload process failed")
            Else
                ' radAsyncUpload.UploadedFiles(0).SaveAs(strDirectory + "\" + strFileNameOnly + strExt)
                result = strDocumentInsert.UpdateDocument(ID, strFileNameOnly, strExt, Session("UserName"), ddlDocumentId.SelectedItem.Text, ddlStudentId.SelectedValue, ddlDocumentId.SelectedValue, ddlModulesId.SelectedValue, strDisplayName)
                radAsyncUpload.UploadedFiles(0).SaveAs(strDirectory + "\" + ID + strExt)
                If result = 0 Then
                    DisplayErrorMessage("Document uploaded successfully")
                End If
            End If
        Else
            DisplayErrorMessage("Only one document can be uploaded")
        End If
        RadGrid1.MasterTableView.ClearEditItems()
        RadGrid1.Rebind()
        'RadGrid1.DataBind()
    End Sub

    Protected Sub RadGrid1_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles RadGrid1.ItemDataBound
        Dim bEdit As Boolean
        Dim bAdd As Boolean
        Dim bDelete As Boolean


        bEdit = False
        bAdd = False
        bDelete = False

        If pObj.HasEdit Then
            bEdit = True
            bAdd = True
        End If
        If pObj.HasAdd Then
            bAdd = True
        End If
        If pObj.HasDelete Then
            bDelete = True
        End If
        If bAdd Then
            RadGrid1.AllowAutomaticInserts = True
        ElseIf Not bAdd Then
            If TypeOf e.Item Is GridCommandItem Then
                DisableGridAdd(e)
            End If
        End If
            If bEdit Then
                RadGrid1.AllowAutomaticUpdates = True
            ElseIf bEdit = False And bAdd = False Then
                If TypeOf e.Item Is GridDataItem Then
                    DisableGridLink(e, "EditCommandColumn", "edit")
                End If
            ElseIf bEdit = False And bAdd = True Then
            If TypeOf e.Item Is GridDataItem Then 'And e.Item.OwnerTableView.IsItemInserted = False Then
                DisableGridLink(e, "EditCommandColumn", "edit")
            End If
            End If
            If Not bDelete Then
                If TypeOf e.Item Is GridDataItem Then
                    DisableGridLink(e, "DeleteColumn", "delete")
                End If
            End If


            'Dim img As LinkButton = TryCast(e.Item.FindControl("Linkbutton1"), LinkButton)
            'img.Attributes.Add("target", "_blank")
            'Dim popupScript As String = "<script type='text/javascript'>window.Onload=OpenDocuments1();function OpenDocuments1(){window.open('../FileBrowser.aspx','_blank','HistWin','width=700,height=600,resizable=yes'); }</script>"
            'ScriptManager.RegisterStartupScript(RadGrid1, RadGrid1.GetType(), "PopUpScript", popupScript, True)
            '  If TypeOf e.Item Is GridItem Then
            Dim img As New LinkButton
            img = TryCast(e.Item.FindControl("Linkbutton1"), LinkButton)
            If Not img Is Nothing Then
                Dim javascripttoopenfile As String
                '  Dim parsequerystring As String

                'parsequerystring = "  function ParseQueryString(queryString) {    if(queryString == 'undefined' || queryString == '') {        return false;    } else {      if(queryString.substr(0, 1) == '?') { queryString = queryString.substr(1); }        var components = queryString.split('&');       var finalObject = new Object();        var parts;       for (var i = 0; i < components.length; i++) {      parts = components[i].split('=');            finalObject[parts[0]] = decodeURI(parts[1]);        }        return finalObject;    }} "
                'javascripttoopenfile = "var pqs = new ParseQueryString();var pn = pqs.param('" + Server.UrlEncode(img.CommandArgument.ToString()) + "');window.open('../FileBrowser.aspx?fileurl= pn','HistWin','width=700,height=600,resizable=yes');"



                '    parsequerystring = "  function ParseQueryString(queryString) {    if(queryString == 'undefined' || queryString == '') {        return false;    } else {      if(queryString.substr(0, 1) == '?') { queryString = queryString.substr(1); }        var components = queryString.split('&');       var finalObject = new Object();        var parts;       for (var i = 0; i < components.length; i++) {      parts = components[i].split('=');            finalObject[parts[0]] = decodeURI(parts[1]);        }        return finalObject;    }} "
                javascripttoopenfile = "window.open('../FileBrowser.aspx?fileurl=" + Server.UrlEncode(img.CommandArgument.ToString()) + "','HistWin','width=700,height=600,resizable=yes');"
                '' "window.Onload=OpenDocuments1();function OpenDocuments1(){window.open('../FileBrowser.aspx?fileurl='" + Server.UrlEncode(img.CommandArgument.ToString()) + "','_blank','HistWin','width=700,height=600,resizable=yes')} "

                img.Attributes.Add("onclick", javascripttoopenfile)
            End If

            '   End If
    End Sub
    Private Sub DisableGridLink(ByVal e As GridItemEventArgs,
                           ByVal sCmdName As String,
                           ByVal sDesc As String)

        Dim sToolTip As String = "User does not have the permission to " & sDesc & " record"
        Dim cell As TableCell = CType(e.Item, GridDataItem)(sCmdName)
        Dim lnk As ImageButton = CType(cell.Controls(0), ImageButton)

        'lnk.Enabled = False
        'lnk.ToolTip = sToolTip
        lnk.Visible = False
        cell.ToolTip = sToolTip


    End Sub

    Private Sub DisableGridAdd(ByVal e As GridItemEventArgs)

        Dim cmditm As GridCommandItem = DirectCast(e.Item, GridCommandItem)
        Dim btn1 As Button = DirectCast(cmditm.FindControl("btn1"), Button)
        Dim sMessage As String
        
        sMessage = "User does not have permission to add new record"
        Try
            'btn1.Enabled = False
            btn1.Visible = False
            btn1.ToolTip = sMessage   '"User does not have permission to add new record"
            Dim lnkbtn1 As LinkButton = DirectCast(cmditm.FindControl("linkbuttionInitInsert"), LinkButton)
            'lnkbtn1.Enabled = False
            lnkbtn1.ToolTip = sMessage  ' "User does not have permission to add new record"
            lnkbtn1.Visible = False
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

    End Sub


End Class
