﻿Imports System.Diagnostics
Imports Telerik.Web.UI.Calendar
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Advantage.Business.Objects
Imports System.Collections
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Imports FAME.Advantage.Common
Imports System.Collections.Generic
Partial Class StudentMasterSummary
    Inherits BasePage

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private pObj As New UserPagePermissionInfo
    Private stuMasterInfo As New StudentMasterInfo
    Private addressHasChanged As Boolean = False
    Private bDispSsNbyRoles As Boolean
    Private sHiddenSSNValue As String = "XXX-XX-XXXX"

    Protected StudentId As String
    Protected ResourceId As Integer
    Protected ModuleId As String
    Protected CampusId As String
    Protected EnrollmentId As String
    Protected UserId As String
    Protected ModCode As String
    Protected State As AdvantageSessionState
    Protected LeadId As String
    Protected MyAdvAppSettings As AdvAppSettings



    ' ReSharper disable InconsistentNaming
    Protected WithEvents advMessage As AdvantageMessage

    Protected boolSwitchCampus As Boolean = False
    Protected boolReloadURL As Boolean = False
    ' ReSharper restore InconsistentNaming



    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreInit
        'AdvantageSession.PageTheme = PageTheme.Blue_Theme
        'Dim strVID As String = MyBase.getStudentObjectPointer(203)
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    'Private Function GetStudentFromStateObject() As StudentMRU

    '    'Dim CampusId As String
    '    Dim objStudentState As New StudentMRU

    '    Try

    '        MyBase.GlobalSearchHandler(0)

    '        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

    '        If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
    '            StudentId = Guid.Empty.ToString()
    '        Else
    '            StudentId = AdvantageSession.MasterStudentId
    '        End If

    '        If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
    '            LeadId = Guid.Empty.ToString()
    '        Else
    '            LeadId = AdvantageSession.MasterLeadId
    '        End If


    '        With objStudentState
    '            .StudentId = New Guid(StudentId)
    '            .LeadId = New Guid(LeadId)
    '            .Name = AdvantageSession.MasterName
    '        End With

    '        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
    '        HttpContext.Current.Items("Language") = "En-US"

    '        Master.ShowHideStatusBarControl(True)

    '    Catch ex As Exception
    '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Dim strSearchUrl As String = ""
    '        strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
    '        Response.Redirect(strSearchUrl)
    '    End Try
    '    Return objStudentState

    'End Function
#End Region

    Private Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'wire the event source with the process method
        advMessage = New AdvantageMessage
        AddHandler advMessage.AdvantageMessaging, AddressOf (New CommonWebUtilities).ProcessAddressChangeMessage

        Session("SEARCH") = 0
        Dim sdfControls As New SDFComponent
        Dim fac As New UserSecurityFacade
        Dim objCommon As New CommonUtilities
        Dim userId As String
        'Dim strVID As String
        'Dim state As AdvantageSessionState

        ResourceId = CType(HttpContext.Current.Items("ResourceId"), Integer)
        CampusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString
        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId

        Dim objStudentState As StudentMRU
        objStudentState = GetObjStudentState(0) ' GetStudentFromStateObject() 'Pass resourceid so that user can be redirected to same page while swtiching students
        Master.ShowHideStatusBarControl(True)
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            StudentId = .StudentId.ToString
            'LeadId = .LeadId.ToString
            LeadId = .ShadowLead.ToString()
            Dim studentInfoBar = DirectCast(Master.FindControl("statusBarControl"), UserControls_StudentInfoBar)
            If studentInfoBar IsNot Nothing Then
                studentInfoBar.SetStuEnrollment(studentInfoBar.StudentEnrollmentId)
            End If
        End With


        LoadStudentInformation()

    End Sub

    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub Page_LoadComplete(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.LoadComplete
        Dim studentInfoBar = DirectCast(Master.FindControl("statusBarControl"), UserControls_StudentInfoBar)
        If studentInfoBar IsNot Nothing Then
            EnrollmentId = studentInfoBar.StudentEnrollmentId
            Dim currentHiddenEnrollmentValue = DirectCast(studentInfoBar.FindControl("currentStudentEnrollment"), HiddenField).Value
            Dim sessionEnrollment = Session("CurrentEnrollmentId")
            If ( EnrollmentId <> currentHiddenEnrollmentValue)
            studentInfoBar.SetStuEnrollment(EnrollmentId)

            End If
        End If
    End Sub
  

    Private Sub LoadStudentInformation()
        Status.InnerText = "Currently Attending"
        LoaStartDate.InnerText = "07/18/2017"
        LoaEndDate.InnerText = "07/18/2017"
        StartDate.InnerText = "07/18/2017"
        EnrollmentDate.InnerText = "07/03/2017"
        ReenrollDate.InnerText = ""
        GraduationDate.InnerText = "08/18/2018"
        RevisedGraduationDate.InnerText = "08/18/2018"
        BadgeId.InnerText = "2357"
        'StudentGroup.InnerText = "COS FT 07/18/17 Cash Pay"


        TotalHours.InnerText = "1392.93"
        ScheduledHours.InnerText = "1710.00"
        AbsentHours.InnerText = "574.15"
        MakeupHours.InnerText = "257.08"
        ScheduledHoursWeek.InnerText = "25"
        ScheduledHoursLastDate.InnerText = "1705.00"
        LastDateAttended.InnerText = "07/16/2018"
        AttendancePercentage.InnerText = "8146"



        Address.InnerText = "2920 101St Pl #22 Hightland, IN 46322"
        Phone.InnerText = "(219) 512-5817"
        Email.InnerText = "parisallen@gmail.com"
    End Sub


End Class
