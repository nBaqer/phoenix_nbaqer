﻿Imports System.Xml
Imports System.Diagnostics
Imports FAME.Common
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports BO = Advantage.Business.Objects
Imports BL = Advantage.Business.Logic.Layer
Partial Class StudentSearch
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlStatus As DropDownList
    Protected WithEvents btnhistory As Button
    Protected WithEvents dgrdStudentSearch As DataGrid
    Protected WithEvents StudentLNameLinkButton As LinkButton
    Protected state As AdvantageSessionState
    Protected WithEvents RequiredFieldValidator1 As RequiredFieldValidator

    Private mruProvider As MRURoutines
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))

    End Sub

#End Region
    Private m_context As HttpContext
    Private campusId As String
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected userId As String
    Protected MyAdvAppSettings As AdvAppSettings
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        BindToolTip()
    End Sub
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here


        Dim m_Context As HttpContext
        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        campusId = Master.CurrentCampusId
        Me.Form.DefaultButton = btnSearch.UniqueID
        SetFocusOnControl("txtLastName")
        'Page.Title = "Student Search"

        'Set the Cohort start date parameters
        RDPCohortStartDate.MinDate = "1/1/1945"

        If Not Page.IsPostBack Then
            'Header1.EnableHistoryButton(False)
            BuildStatusDDL()
            BuildCampusGroupsDDL()
            BuildProgramsDDL()
            BuildStudentStatusDDL()
            BuildStudentGroupDDL()
            Session("NameCaption") = ""
            Session("NameValue") = ""
            Session("IdValue") = ""
            Session("IdCaption") = ""
            Session("EmployerID") = Nothing
            Session("StudentID") = ""
            '  ElseIf txtEnterKey.Text = 1 Then
        End If
        DisableAllButtons()
        GetInputMaskValue()

    End Sub
    Private Sub GetInputMaskValue()
        Dim facInputMasks As New InputMasksFacade
        'Dim correctFormat As Boolean
        'Dim strMask As String
        'Dim zipMask As String
        'Dim errorMessage As String
        'Dim strPhoneReq As String
        'Dim strZipReq As String
        'Dim strFaxReq As String
        Dim objCommon As New CommonUtilities
        Dim ssnMask As String

        'Apply Mask Only If Its Local Phone Number
        'Get The Input Mask for Phone/Fax and Zip
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        'Replace The Mask Character from # to 9 as Masked Edit TextBox 
        'accepts only certain characters as mask characters
        'txtSSN.Mask = Replace(ssnMask, "#", "9")
        'Get The Format Of the input masks and display it next to caption
        'labels
        lblSSN.ToolTip = ssnMask

        'Get The RequiredField Value
        Dim strSSNReq As String

        strSSNReq = objCommon.SetRequiredColorMask("SSN")

        'If The Field Is Required Field Then Color The Masked
        'Edit Control
        If strSSNReq = "Yes" Then
            '    ssnreqasterisk.Visible = True
            'txtSSN.BackColor = Color.FromName("#ffff99")
        End If
    End Sub

    Private Sub DisableAllButtons()
        btnNew.Enabled = False
        btnDelete.Enabled = False
        btnSave.Enabled = False
    End Sub
    Private Sub BuildStatusDDL()
        'Bind the Status DropDownList
        Dim statuses As New StatusCodeFacade
        With ddlStatusId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataSource = statuses.GetAllStatusCodesForStudents(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildStudentStatusDDL()
        'Bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStudentStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCampusGroupsDDL()
        'Bind the CampusGroups DrowDownList
        Dim campusGroups As New CampusGroupsFacade
        With ddlCampGrpID
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            .DataSource = campusGroups.GetAllCampusEnrollment()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    'Private Sub BuildProgramsDDL()
    '    'Bind the CampusGroups DrowDownList
    '    Dim Programs As New StatusesFacade
    '    With ddlProgram
    '        .DataTextField = "PrgVerDescrip"
    '        .DataValueField = "PrgVerId"
    '        .DataSource = Programs.GetAllProgramVersions()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildProgramsDDL()
        'Bind the CampusGroups DrowDownList
        Dim Programs As New StatusesFacade
        With ddlProgram
            '.DataTextField = "PrgVerDescrip"
            .DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = Programs.GetAllProgramVersions(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildStudentGroupDDL()
        'Bind the Student Group DrowDownList
        Dim leadFac As New LeadFacade
        With ddlLeadGrpId
            .DataTextField = "Descrip"
            .DataValueField = "LeadGrpId"
            .DataSource = leadFac.GetLeadGroupsByCampus(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BindDataList()
        'Bind The First,Middle,LastName based on EmployerContactId
        Dim SearchFacade As New StudentSearchFacade
        Dim defaultCampusId As String
        Dim strSSN As String

        defaultCampusId = HttpContext.Current.Request.Params("cmpid")
        defaultCampusId = XmlConvert.ToGuid(defaultCampusId).ToString

        If txtSSN.Text.Length >= 1 Then
            strSSN = RemoveHypen(txtSSN.Text)
        Else
            strSSN = ""
        End If
        If Len(txtSSN.Text) >= 1 And Len(txtSSN.Text) < 9 Then
            DisplayErrorMessage("Please enter the full SSN")
            Exit Sub
        End If
        ''CohortStartDate in the search List added by Saraswathi lakshmanan
        ''on sept 29 - 2008
        Dim CohortStartDate As String = ""
        If RDPCohortStartDate.SelectedDate IsNot Nothing Then
            Try
                Dim dtDate As DateTime = Convert.ToDateTime(RDPCohortStartDate.SelectedDate)
                CohortStartDate = dtDate.ToString
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ' Invalid date
                DisplayErrorMessage("Cohort Start Date is not a valid date.")
                Exit Sub
            End Try
        End If
        dgrdTransactionSearch.DataSource = SearchFacade.StudentSearchResults(txtStudentId.Text, txtLastName.Text, txtFirstName.Text, strSSN, ddlStatusId.SelectedValue, txtEnrollment.Text, ddlProgram.SelectedValue, defaultCampusId, txtStudentNumber.Text, ddlStudentStatusId.SelectedValue, ddlLeadGrpId.SelectedValue, CohortStartDate)
        dgrdTransactionSearch.DataBind()
    End Sub
    Private Function RemoveHypen(ByVal strSSN As String) As String
        Dim strRemoveHypenSSN As String = String.Empty
        Dim chr As Char
        For Each chr In strSSN
            If Not chr = "-" Then
                strRemoveHypenSSN &= chr
            End If
        Next
        Return strRemoveHypenSSN
    End Function

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        'Dim strAllowableChar As String = "~!@#$%^&*()_+-=[]"
        'Dim strSSNAllowableChar As String = "0123456789-"
        '        Dim strErrorMessage As String
        Try
            ''Cohort StartDate added by Saraswathi Lakshmanan on sept 29 - 2008
            If txtStudentId.Text = "" And txtLastName.Text = "" And txtFirstName.Text = "" And txtEnrollment.Text = "" And ddlStatusId.SelectedValue = Guid.Empty.ToString And ddlCampGrpID.SelectedValue = Guid.Empty.ToString And ddlProgram.SelectedValue = Guid.Empty.ToString And txtSSN.Text = "" And txtStudentNumber.Text = "" And ddlStudentStatusId.SelectedValue = "" And ddlLeadGrpId.SelectedValue = "" And RDPCohortStartDate.SelectedDate Is Nothing Then
                DisplayErrorMessage("Please enter a value to search")
                Exit Sub
            End If


            ''Validate LastName % Character Not Allowed
            'If txtLastName.Text.Length >= 1 Then
            '    If InStr(txtLastName.Text, "%") >= 1 Then
            '        strErrorMessage = "% is not allowed in last name " & vbLf
            '    End If
            'End If

            ''Validate FirstName % Character Not Allowed
            'If txtFirstName.Text.Length >= 1 Then
            '    If InStr(txtFirstName.Text, "%") >= 1 Then
            '        strErrorMessage &= "% is not allowed in first name " & vbLf
            '    End If
            'End If

            ''SSN % not allowed
            'If txtSSN.Text.Length >= 1 Then
            '    If InStr(txtSSN.Text, "%") >= 1 Then
            '        strErrorMessage &= "% is not allowed in ssn " & vbLf
            '    End If
            'End If

            ''Enrollment % not allowed
            'If txtEnrollment.Text.Length >= 1 Then
            '    If InStr(txtEnrollment.Text, "%") >= 1 Then
            '        strErrorMessage &= "% is not allowed in enollment # " & vbLf
            '    End If
            'End If

            ''StudentId - % not allowed
            'If txtStudentNumber.Text.Length >= 1 Then
            '    If InStr(txtStudentNumber.Text, "%") >= 1 Then
            '        strErrorMessage &= "% is not allowed in student id " & vbLf
            '    End If
            'End If

            'If Not strErrorMessage = "" Then
            '    DisplayErrorMessage(strErrorMessage)
            '    Exit Sub
            'End If


            Session("StudentType") = "ExistingStudent"
            BindDataList()
            Session("NameCaption") = ""
            Session("NameValue") = ""
            Session("IdValue") = ""
            Session("IdCaption") = ""

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Response.Write(ex.Message.ToString)
        End Try
    End Sub
    Private Sub dgrdTransactionSearch_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgrdTransactionSearch.ItemCommand
        Dim StudentName As New StudentSearchFacade
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String = ""
        '        Dim state As AdvantageSessionState
        Dim fac As New UserSecurityFacade
        Dim arrUPP As New ArrayList
        Dim pURL As String

        If e.CommandName = "StudentSearch" Then
            Dim defaultCampusId As String

            defaultCampusId = HttpContext.Current.Request.Params("cmpid")
            defaultCampusId = XmlConvert.ToGuid(defaultCampusId).ToString

            'save studentId in the session so that it can be used by the StudentLedger program
            'Session("StudentID") = e.CommandArgument
            'Session("NameCaption") = "Student"
            'Session("NameValue") = StudentName.GetStudentNameByID(Session("StudentID"))
            Session("IdValue") = ""
            Session("IdCaption") = ""
            Session("SEARCH") = 1

            'Set relevant properties on the state object
            'objStateInfo.StudentId = e.CommandArgument.ToString()
            'objStateInfo.NameCaption = "Student : "
            'objStateInfo.NameValue = StudentName.GetStudentNameByID(e.CommandArgument)

            'If the school is setup to restrict search by student group then we should
            'send the LeadGrpId selected to the next page.
            If MyAdvAppSettings.AppSettings("RestrictSearchByStudentGroup").ToUpper = "TRUE" Then
                If ddlLeadGrpId.SelectedValue <> "" Then
                    objStateInfo.StudentGrpId = XmlConvert.ToGuid(ddlLeadGrpId.SelectedValue).ToString
                Else
                    objStateInfo.StudentGrpId = ""
                End If

            End If

            ''Create a new guid to be associated with the employer pages
            'strVID = Guid.NewGuid.ToString

            'Session("StudentObjectPointer") = strVID
            'Session("LeadObjectPointer") = ""
            'Session("LeadMRUFlag") = "false"

            ''Add an entry to AdvantageSessionState for this guid and object
            ''load Advantage state
            'state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            'state(strVID) = objStateInfo
            ''save current State
            'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)


            '**DD REM this and redirect to Student Address page for now.
            Dim str As String = Session("LastSelectedModule")

            Dim strModuleCode As String = HttpContext.Current.Request.Params("mod").ToString
            Dim strModuleDescription As String = ""
            Dim strModuleResourceId As String = ""
            Select Case strModuleCode
                Case "AR"
                    strModuleDescription = "Academic Records"
                    strModuleResourceId = "26"
                Case "AD"
                    strModuleDescription = "Admissions"
                    strModuleResourceId = "189"
                Case "FC"
                    strModuleDescription = "Faculty"
                    strModuleResourceId = "300"
                Case "FA"
                    strModuleDescription = "Financial Aid"
                    strModuleResourceId = "191"
                Case "PL"
                    strModuleDescription = "Placement"
                    strModuleResourceId = "193"
                Case "SA"
                    strModuleDescription = "Student Accounts"
                    strModuleResourceId = "194"
            End Select

            'Dim pObj As New UserPagePermissionInfo
            strVID = BuildStudentSearchStateObject(e.CommandArgument.ToString())

            Session("StudentObjectPointer") = strVID
            Session("LeadObjectPointer") = strVID '""
            Session("LeadMRUFlag") = "false"

            Dim intSchoolOption As Integer = CommonWebUtilities.SchoolSelectedOptions(MyAdvAppSettings.AppSettings("SchedulingMethod"))

            Select Case strModuleDescription 'Session("LastSelectedModule")
                'If the user has access to the default page redirect to that page.
                'If not, then we should try to redirect the user to the first page that he has access to
                'within the submodule. If the user does not have any access to any page within the
                'submodule then we should redirect him/her to the error page with a message that he
                'does not have access to any page within the submodule.
                'For example, the default page within the student submodule in academic records is the
                'student master. However, the user could have been granted access to only the addresses
                'tab. In that case if we tried to redirect the user to the student master page he/she
                'would be redirected to the standard error page because he/she does not have permission
                'to that page. In this case the user should be redirected to the addresses tab.
                Case Is = "Academic Records"
                    Session("Module") = strModuleDescription 'Session("LastSelectedModule")
                    Dim strCampusId As String = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString()
                    arrUPP = mruProvider.checkEntityPageSecurityStudentSearch(AdvantageSession.UserState, 26, strCampusId, 394, intSchoolOption)
                    If arrUPP.Count = 0 Then
                        'User does not have permission to any resource for this submodule
                        'Session("Error") = "You do not have permission to any of the pages for existing student<br> for the campus that you are logged in to."
                        'Response.Redirect("../ErrorPage.aspx")
                        RadNotification1.Show()
                        RadNotification1.Text = "You do not have permission to access any of the existing students pages in the current campus"
                    ElseIf mruProvider.DoesUserHasAccessToSubModuleResource(arrUPP, 203) Then
                        AdvantageSession.StudentMRU = Nothing
                        Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                        Dim strVSI As String = R.Next()
                        mruProvider.InsertMRU(1, txtStudentId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                        fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)
                        Response.Redirect("../PL/StudentMaster.aspx?resid=203&mod=AR&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1" + "&VSI=" + strVSI, True)
                    Else
                        'redirect to the first page that the user has permission to for the submodule
                        pURL = BuildPartialURL(arrUPP(0))
                        mruProvider.InsertMRU(1, txtStudentId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                        Response.Redirect(pURL & "&mod=AR&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1", True)
                    End If
                Case Is = "Placement"
                    Session("Module") = strModuleDescription 'Session("LastSelectedModule")
                    Dim strCampusId As String = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString()
                    arrUPP = mruProvider.checkEntityPageSecurityStudentSearch(AdvantageSession.UserState, 193, strCampusId, 394, intSchoolOption)
                    If arrUPP.Count = 0 Then
                        'User does not have permission to any resource for this submodule
                        'Session("Error") = "You do not have permission to any of the pages for existing student<br> for the campus that you are logged in to."
                        'Response.Redirect("../ErrorPage.aspx")
                        RadNotification1.Show()
                        RadNotification1.Text = "You do not have permission to access any of the existing students pages in the current campus"

                    ElseIf mruProvider.DoesUserHasAccessToSubModuleResource(arrUPP, 203) Then
                        Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                        Dim strVSI As String = R.Next()
                        mruProvider.InsertMRU(1, txtStudentId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                        fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)
                        Response.Redirect("../PL/StudentMaster.aspx?resid=203&mod=AR&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1" + "&VSI=" + strVSI, True)
                        'Response.Redirect("../PL/StudentMaster.aspx?resid=203&mod=PL&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1", True)
                    Else
                        'redirect to the first page that the user has permission to for the submodule
                        pURL = BuildPartialURL(arrUPP(0))
                        Response.Redirect(pURL & "&mod=PL&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1", True)
                    End If
                Case Is = "Student Accounts"
                    Session("Module") = strModuleDescription 'Session("LastSelectedModule")
                    Dim strCampusId As String = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString()
                    arrUPP = mruProvider.checkEntityPageSecurityStudentSearch(AdvantageSession.UserState, 194, strCampusId, 394, intSchoolOption)
                    If arrUPP.Count = 0 Then
                        'User does not have permission to any resource for this submodule
                        'Session("Error") = "You do not have permission to any of the pages for existing student<br> for the campus that you are logged in to."
                        'Response.Redirect("../ErrorPage.aspx")
                        RadNotification1.Show()
                        RadNotification1.Text = "You do not have permission to access any of the existing students pages in the current campus"
                    ElseIf mruProvider.DoesUserHasAccessToSubModuleResource(arrUPP, 116) Then
                        Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                        Dim strVSI As String = R.Next()
                        mruProvider.InsertMRU(1, txtStudentId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                        fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)
                        Response.Redirect("../SA/StudentLedger.aspx?resid=116&mod=SA&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1" + "&VSI=" + strVSI, True)
                    Else
                        'redirect to the first page that the user has permission to for the submodule
                        pURL = BuildPartialURL(arrUPP(0))
                        mruProvider.InsertMRU(1, txtStudentId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                        Response.Redirect(pURL & "&mod=SA&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1", True)
                    End If
                Case Is = "Faculty"
                    Session("Module") = strModuleDescription 'Session("LastSelectedModule")
                    Dim strCampusId As String = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString()
                    arrUPP = mruProvider.checkEntityPageSecurityStudentSearch(AdvantageSession.UserState, 300, strCampusId, 394, intSchoolOption)
                    If arrUPP.Count = 0 Then
                        'User does not have permission to any resource for this submodule
                        'Session("Error") = "You do not have permission to any of the pages for existing student<br> for the campus that you are logged in to."
                        'Response.Redirect("../ErrorPage.aspx")
                        RadNotification1.Show()
                        RadNotification1.Text = "You do not have permission to access any of the existing students pages in the current campus"
                    ElseIf mruProvider.DoesUserHasAccessToSubModuleResource(arrUPP, 203) Then
                        Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                        Dim strVSI As String = R.Next()
                        mruProvider.InsertMRU(1, txtStudentId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                        fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)
                        Response.Redirect("../PL/StudentMaster.aspx?resid=203&mod=FC&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1" + "&VSI=" + strVSI, True)
                    Else
                        'redirect to the first page that the user has permission to for the submodule
                        pURL = BuildPartialURL(arrUPP(0))
                        mruProvider.InsertMRU(1, txtStudentId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                        Response.Redirect(pURL & "&mod=FC&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1", True)
                    End If
                Case Is = "Task Manager"
                    Session("Module") = strModuleDescription 'Session("LastSelectedModule")
                    'Session("LastSelectedModule") = "Contact Manager"
                    fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)
                    Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                    Dim strVSI As String = R.Next()
                    mruProvider.InsertMRU(1, txtStudentId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                    Response.Redirect("../PL/ExitInterview.aspx?resid=97&mod=TM&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1" + "&VSI=" + strVSI, True)
                Case Is = "Financial Aid"

                    Session("Module") = strModuleDescription 'Session("LastSelectedModule")
                    Dim strCampusId As String = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString()
                    arrUPP = mruProvider.checkEntityPageSecurityStudentSearch(AdvantageSession.UserState, 191, strCampusId, 394, intSchoolOption)
                    If arrUPP.Count = 0 Then
                        'User does not have permission to any resource for this submodule
                        'Session("Error") = "You do not have permission to any of the pages for existing student<br> for the campus that you are logged in to."
                        'Response.Redirect("../ErrorPage.aspx")
                        RadNotification1.Show()
                        RadNotification1.Text = "You do not have permission to access any of the existing students pages in the current campus"
                    ElseIf mruProvider.DoesUserHasAccessToSubModuleResource(arrUPP, 203) Then
                        Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                        Dim strVSI As String = R.Next()
                        mruProvider.InsertMRU(1, txtStudentId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                        fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)
                        Response.Redirect("../FA/StudentAwards.aspx?resid=175&mod=FA&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1" + "&VSI=" + strVSI, True)
                    Else
                        'redirect to the first page that the user has permission to for the submodule
                        pURL = BuildPartialURL(arrUPP(0))
                        mruProvider.InsertMRU(1, txtStudentId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                        Response.Redirect(pURL & "&mod=FA&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=1", True)
                    End If
            End Select
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Function BuildPartialURL(ByVal uppInfo As UserPagePermissionInfo) As String
        Return uppInfo.Url & "?resid=" & uppInfo.ResourceId
    End Function


    ' ''Added by Saraswathi lakshmanan on August 24 2009
    ' ''To find the list controls and add a tool tip to those items in the control
    ' ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    '********
    '    Dim placeholder As New ContentPlaceHolder
    '    placeholder = Master.FindControl("ContentMain2")

    '    For Each ctl In placeholder.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If

    '        '***Added by DD 6/14/11
    '        If TypeOf ctl Is Telerik.Web.UI.RadSplitter Then

    '            'Find the Radpane
    '            Dim radpane As Telerik.Web.UI.RadPane

    '            Dim ctl2 As Control
    '            For Each ctl2 In ctl.Controls
    '                If TypeOf ctl2 Is Telerik.Web.UI.RadPane Then
    '                    'Get to the radpane
    '                    radpane = ctl2
    '                    BindToolTipForControlsInsideaRadpane(radpane)
    '                End If
    '            Next
    '        End If
    '    Next


    '    '********
    '    '***Rem'd by DD 6/14/11
    '    'For Each ctl In Page.Form.Controls
    '    '    If TypeOf ctl Is ListControl Then
    '    '        For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '    '            DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '    '        Next
    '    '    End If
    '    '    If TypeOf ctl Is Panel Then
    '    '        BindToolTipForControlsInsideaPanel(ctl)
    '    '    End If
    '    '    If TypeOf ctl Is DataGrid Then
    '    '        BindToolTipForControlsInsideaGrid(ctl)
    '    '    End If
    '    'Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaRadpane(ByVal Ctrlpanel As Telerik.Web.UI.RadPane)
    '    '***Added by DD 6/14/11
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            Dim strClientId As String = ctrl.ClientID

    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                Dim intItemCount As Integer = DirectCast(ctrl, ListControl).Items.Count
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
    Public Function BuildStudentSearchStateObject(ByVal studentId As String) As String
        Dim objStateInfo As AdvantageStateInfo
        Dim strVID As String = String.Empty

        Dim strStudentId As String

        If studentId.ToString.Trim = "" Then 'No Student was selected from MRU
            strStudentId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                                   1, _
                                                                   AdvantageSession.UserState.CampusId.ToString)
        Else
            strStudentId = studentId
        End If

        If Not strStudentId.Trim = "" Then
            objStateInfo = mruProvider.BuildStudentStatusBar(strStudentId, AdvantageSession.UserState.CampusId.ToString)
            With objStateInfo
                If .StudentIdentifierCaption.ToLower = "ssn" Then
                    Dim ssnMask As String
                    Dim facInputMasks As New InputMasksFacade
                    ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
                    If .StudentIdentifier <> "" Then
                        .StudentIdentifier = facInputMasks.ApplyMask(ssnMask, .StudentIdentifier)
                    End If
                End If

                AdvantageSession.MasterStudentId = objStateInfo.StudentId
                AdvantageSession.MasterLeadId = objStateInfo.LeadId
                AdvantageSession.MasterName = objStateInfo.NameValue

                Session("hdnSearchCampusId") = .CampusId.ToString()
                Session("previousCampusId") = ""
                Session("MasterName1") = objStateInfo.NameValue

            End With

            'Create a new guid to be associated with the employer pages
            strVID = Guid.NewGuid.ToString

            Session("StudentObjectPointer") = strVID
            Session("LeadObjectPointer") = strVID

            txtStudentId.Text = strStudentId

            'Add an entry to AdvantageSessionState for this guid and object
            'load Advantage state
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            state(strVID) = objStateInfo
            'save current State
            CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
        End If
        Return strVID
    End Function



    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtSSN.Text = ""
        txtStudentId.Text = ""
        txtEnrollment.Text = ""
        txtStudentNumber.Text = ""
        ddlCampGrpID.SelectedIndex = 0
        ddlLeadGrpId.SelectedIndex = 0
        ddlProgram.SelectedIndex = 0
        'ddlStatus.SelectedIndex = 0
        ddlStatusId.SelectedIndex = 0
        ddlStudentStatusId.SelectedIndex = 0
        RDPCohortStartDate.Clear()

    End Sub
End Class
