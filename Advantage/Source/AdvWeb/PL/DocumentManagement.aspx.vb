﻿Imports FAME.common
Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.Advantage.Common

Partial Class PL_DocumentManagement
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected campus As String
    Protected strCount As String
    Protected StudentId As String
    Protected selectedModule As Integer
    Protected MyAdvAppSettings As AdvAppSettings
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim userId As String
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim objCommon As New CommonUtilities

        'Put user code to initialize the page here
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        StudentId = Session("StudentID")
        Session("ModuleId") = 6

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = resourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = resourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        campus = AdvantageSession.UserState.CampusId.ToString


        If Not Page.IsPostBack Then
            strCount = 0
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"

            'Bind The DropDownList
            BuildDDLs()

            'Get PK Value
            txtStudentDocId.Text = Guid.NewGuid.ToString()

            'ddlDocumentId.Enabled = False
            ddlDocumentModule.SelectedIndex = 0
            ddlModulesId.SelectedIndex = 0
            'ddlDocumentId.Enabled = True

            'By default placement will be selected
            selectedModule = 6

            'BindDataList(CInt(ddlDocumentModule.SelectedValue), ddlStudentFilter.SelectedValue)
            'BuildDocumentListDDL(CInt(ddlModulesId.SelectedValue))

        Else
            'ddlDocumentModule.SelectedValue = 6
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
        End If
        'If ddlDocumentId.SelectedValue = "" Or ddlDocStatusId.SelectedValue = "" Then
        '    chkOverride.Enabled = False
        'Else
        '    chkOverride.Enabled = True
        'End If
        'If campus = "" Then
        '    CloseChildWindow(Me.Page, "Close")
        'End If

        ' ddlDocumentModule.SelectedIndex = ddlDocumentModule.Items.IndexOf(ddlDocumentModule.Items.FindByText("Admissions"))
        'ddlModulesId.SelectedIndex = ddlModulesId.Items.IndexOf(ddlModulesId.Items.FindByText("Admissions"))
        'ddlDocumentModule.Enabled = False
        'ddlModulesId.Enabled = False


        btnnew.Enabled = True
        btnsave.Enabled = True
    End Sub
    Private Sub BuildDDLs()
        BuildDocumentModuleDDL()
        BuildDocumentFilterStatusDDL()
        BuildStudentDDL()
        BuildAllDocumentListDDL()
        BindDataList()
    End Sub
    Private Sub BindDataList()
        With New LeadFacade
            dlstDocumentStatus.DataSource = .GetAllDocsStudentByCampus(Trim(campus))
            dlstDocumentStatus.DataBind()
        End With
    End Sub
    Private Sub BuildAllDocumentListDDL()
        'Bind the Document DropDownList
        Dim DocumentList As New LeadDocsFacade
        With ddlDocumentId
            .DataTextField = "DocumentDescrip"
            .DataValueField = "DocumentId"
            .DataSource = DocumentList.GetAllDocumentsByStudent(campus)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildStudentDDL()
        Dim StudentName As New AdReqsFacade
        Dim strStudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
        With ddlStudentId
            .DataTextField = "fullname"
            .DataValueField = "StudentId"
            .DataSource = StudentName.GetAllDocumentStudentNames(Trim(campus), strStudentIdentifier)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        With ddlStudentFilter
            .DataTextField = "fullname"
            .DataValueField = "StudentId"
            .DataSource = StudentName.GetAllDocumentStudentNames(Trim(campus), strStudentIdentifier)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildDocumentModuleDDL()
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlDocumentModule
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleId"
            .DataSource = DocumentStatus.GetAllDocumentModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        With ddlModulesId
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleId"
            .DataSource = DocumentStatus.GetAllDocumentModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub BuildDocumentFilterStatusDDL()
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlDocFilterStatus
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = DocumentStatus.GetAllDocumentStatus(campus)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        With ddlDocStatusId
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = DocumentStatus.GetAllDocumentStatus(campus)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Page.Theme = "Blue_Theme"
    End Sub
End Class
