﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="EmployerInfo.aspx.vb" Inherits="EmployerInfo" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Enabled="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none;">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                    <h3> <asp:Label ID="headerTitle" runat="server"></asp:Label></h3>
                                        <!-- begin content table-->
                                        <table class="contentleadmastertable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="contentcellheader" nowrap="nowrap" colspan="8" style="border-top: 0; border-left: 0; border-right: 0; height: 20px;">
                                                    <asp:Label ID="label1" runat="server" CssClass="label" Font-Bold="true">General Information</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" nowrap="nowrap" style="padding-top: 16px">
                                                    <asp:Label ID="lblCode" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnei" nowrap="nowrap" style="padding-top: 16px">
                                                    <asp:TextBox ID="txtCode" TabIndex="1" CssClass="textbox" runat="server"></asp:TextBox>
                                                </td>
                                                <td class="leadcell4columnei" nowrap="nowrap" style="padding-top: 16px">
                                                    <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnrightei" nowrap="nowrap" style="padding-top: 16px">
                                                    <asp:DropDownList ID="ddlStatusId" TabIndex="6" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" nowrap="nowrap">
                                                    <asp:Label ID="lblEmployerDescrip" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnei" nowrap="nowrap">
                                                    <asp:TextBox ID="txtEmployerDescrip" TabIndex="2" CssClass="textbox" runat="server"
                                                        ></asp:TextBox><asp:CheckBox ID="chkGroupName" CssClass="checkboxinternational"
                                                            runat="server"></asp:CheckBox>
                                                </td>
                                                <td class="leadcell4columnei" nowrap="nowrap">
                                                    <asp:Label ID="lblFeeId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnrightei">
                                                    <asp:DropDownList ID="ddlFeeId" TabIndex="7" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" nowrap="nowrap">
                                                    <asp:Label ID="lblParentId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnei" nowrap="nowrap">
                                                    <asp:DropDownList ID="ddlParentId" TabIndex="3" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcell4columnei" nowrap="nowrap">
                                                    <asp:Label ID="lblIndustryId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnrightei">
                                                    <asp:DropDownList ID="ddlIndustryId" TabIndex="8" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" nowrap="nowrap">
                                                    <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnei" nowrap="nowrap">
                                                    <asp:DropDownList ID="ddlCampGrpId" TabIndex="4" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcell4columnei" nowrap="nowrap">&nbsp;
                                                </td>
                                                <td class="contentcell4columnrightei">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" nowrap="nowrap" style="padding-bottom: 16px">
                                                    <asp:Label ID="lblJobCatId" runat="server" CssClass="label">Jobs Offered</asp:Label>
                                                </td>
                                                <td class="contentcell4columnei" style="vertical-align: middle" colspan="5" >
                                                    <asp:CheckBoxList ID="chkJobCatId" TabIndex="5" runat="Server" CssClass="checkboxBox"
                                                        RepeatColumns="7">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                            width="100%">
                                            <tr>
                                                <td class="contentcellheader" nowrap="nowrap" align="left" colspan="6" style="border-top: 0; border-right: 0; border-left: 0">
                                                    <asp:Label ID="lblAddress" runat="server" CssClass="label" Font-Bold="true">Address</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" style="padding-top: 10px" nowrap="nowrap">&nbsp;
                                                </td>
                                                <td class="contentcell4columnei" style="padding-top: 10px">
                                                    <asp:CheckBox ID="chkForeignZip" TabIndex="9" CssClass="checkboxinternational" runat="server"
                                                        AutoPostBack="true"></asp:CheckBox>
                                                </td>
                                                <td class="leadcell4columnei" style="padding-top: 10px">&nbsp;
                                                </td>
                                                <td class="contentcell4columnrightei" style="padding-top: 10px">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" nowrap="nowrap">
                                                    <asp:Label ID="lblAddress1" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnei">
                                                    <asp:TextBox ID="txtAddress1" TabIndex="10" CssClass="textbox" runat="server"></asp:TextBox>
                                                </td>
                                                <td class="leadcell4columnei" nowrap="nowrap">
                                                    <asp:Label ID="lblCountyId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnrightei">
                                                    <asp:DropDownList ID="ddlCountyId" TabIndex="17" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" nowrap="nowrap">
                                                    <asp:Label ID="lblAddress2" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnei">
                                                    <asp:TextBox ID="txtAddress2" TabIndex="12" CssClass="textbox" runat="server"></asp:TextBox>
                                                </td>
                                                <td class="leadcell4columnei" nowrap="nowrap">
                                                    <asp:Label ID="lblLocationId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnrightei">
                                                    <asp:DropDownList ID="ddlLocationId" TabIndex="18" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" nowrap="nowrap">
                                                    <asp:Label ID="lblCity" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnei">
                                                    <asp:TextBox ID="txtcity" TabIndex="13" CssClass="textbox" runat="server" AutoPostBack="True"></asp:TextBox>
                                                </td>
                                                <td class="leadcell4columnei" nowrap="nowrap"></td>
                                                <td class="contentcell4columnrightei" nowrap="nowrap"></td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" nowrap="nowrap">
                                                    <asp:Label ID="lblStateId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnei">
                                                    <asp:DropDownList ID="ddlStateId" TabIndex="14" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcell4columnei" nowrap="nowrap">
                                                    <asp:Label ID="lblOtherState" runat="server" CssClass="label" Visible="False"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnrightei" nowrap="nowrap">
                                                    <asp:TextBox ID="txtOtherState" CssClass="textbox" runat="server" Visible="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" nowrap="nowrap">
                                                    <asp:Label ID="lblZip" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnei">
                                                    <telerik:RadMaskedTextBox ID="txtZip" TabIndex="15" runat="server" Width="200px" 
                                                        CssClass="textbox" Mask="#####" DisplayMask="#####" DisplayPromptChar="" AutoPostBack="false">
                                                    </telerik:RadMaskedTextBox>
                                                </td>
                                                <td class="leadcell4columnei" nowrap="nowrap"></td>
                                                <td class="contentcell4columnrightei" nowrap="nowrap"></td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" style="padding-bottom: 16px" nowrap="nowrap">
                                                    <asp:Label ID="lblCountryId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnei">
                                                    <asp:DropDownList ID="ddlCountryId" TabIndex="16" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcell4columnei" nowrap="nowrap"></td>
                                                <td class="contentcell4columnrightei" nowrap="nowrap"></td>
                                            </tr>
                                        </table>
                                        <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                            width="100%">
                                            <tr>
                                                <td class="contentcellheader" nowrap="nowrap" align="left" colspan="6" style="border-top: 0; border-right: 0; border-left: 0">
                                                    <asp:Label ID="lblPhones" runat="server" CssClass="label" Font-Bold="true">Phone and Fax</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" style="padding-top: 16px; padding-bottom: 16px">
                                                    <asp:Label ID="lblPhone" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="contentcell4columnei" nowrap="nowrap" style="padding-top: 16px; padding-bottom: 16px">
                                                    <telerik:RadMaskedTextBox ID="txtPhone" TabIndex="20" runat="server"
                                                        Width="200px" DisplayFormatPosition="Right" Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar="">
                                                    </telerik:RadMaskedTextBox>
                                                    &nbsp;
                                                    <asp:CheckBox ID="chkForeignPhone" CssClass="checkboxinternational" TabIndex="19"
                                                        runat="server" AutoPostBack="true"></asp:CheckBox>
                                                </td>
                                                <td class="leadcell4columnei" style="padding-top: 16px; padding-bottom: 16px">
                                                    <asp:Label ID="lblFax" runat="server" CssClass="label">Fax</asp:Label>
                                                </td>
                                                <td class="contentcell4columnrightei" nowrap="nowrap" style="padding-top: 16px; padding-bottom: 16px">
                                                    <telerik:RadMaskedTextBox ID="txtFax" TabIndex="22" runat="server"
                                                        Width="200px" DisplayFormatPosition="Right" Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar="">
                                                    </telerik:RadMaskedTextBox>
                                                    &nbsp;
                                                    <asp:CheckBox ID="chkForeignFax" CssClass="checkboxinternational" TabIndex="21" runat="server"
                                                        AutoPostBack="true"></asp:CheckBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                            width="100%">
                                            <tr>
                                                <td class="contentcellheader" nowrap="nowrap" align="left" colspan="6" style="border-top: 0; border-right: 0; border-left: 0">
                                                    <asp:Label ID="label3" runat="server" CssClass="label" Font-Bold="true">Email</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleftei" style="padding-top: 16px; padding-bottom: 16px"
                                                    nowrap="nowrap">
                                                    <asp:Label ID="lblEmail" runat="server" CssClass="label">Email</asp:Label>
                                                </td>
                                                <td class="contentcell4columnei" style="padding-top: 16px; padding-bottom: 16px">
                                                    <asp:TextBox ID="txtEmail" TabIndex="24" CssClass="textbox" runat="server" ></asp:TextBox><asp:RegularExpressionValidator
                                                        ID="RegularExpressionValidator3" runat="server" Display="None" ErrorMessage="Invalid Email Format"
                                                        ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Invalid Email Format</asp:RegularExpressionValidator>
                                                </td>
                                                <td class="leadcell4columnei">&nbsp;
                                                </td>
                                                <td class="contentcell4columnrightei"></td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="spacertables"></td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcellheader" nowrap="nowrap" colspan="6">
                                                        <asp:Label ID="lblSDF" runat="server" Font-Bold="true" CssClass="label">School Defined Fields</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="spacertables"></td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell2" colspan="6">
                                                        <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:TextBox ID="txtEmployerId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox
                                            ID="chkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox><asp:TextBox
                                                ID="txtModDate" runat="server" Visible="false"></asp:TextBox><asp:TextBox ID="txtModUser"
                                                    runat="server" Visible="false"></asp:TextBox><asp:TextBox ID="txtRowIds" Style="display: none"
                                                        runat="server"></asp:TextBox><asp:TextBox ID="txtResourceId" Style="display: none"
                                                            runat="server"></asp:TextBox>
                                        <!-- end content table-->
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>

            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

