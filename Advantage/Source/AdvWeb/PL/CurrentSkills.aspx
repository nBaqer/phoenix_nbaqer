<%@ Page Language="vb" AutoEventWireup="false" Inherits="CurrentSkills" CodeFile="CurrentSkills.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Current Skills</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	<%--	<LINK href="../CSS/localhost.css" type="text/css" rel="stylesheet">--%>
          <link href="../css/systememail.css" type="text/css" rel="stylesheet" />
	</HEAD>
	<body leftMargin="0" topMargin="0" runat=server>
		<form id="Form1" method="post" runat="server">
			<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
				<tr>
					<td class="DetailsFrameTop"><table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="MenuFrame" align="right">
                                    <div id="hidebuttons" runat="server" visible="false">
                                    <asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:button><asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
										Enabled="False"></asp:button> </div></td>
                               
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
							height="95%">
							<tr>
								<td class="detailsframeff"><div  style="overflow:auto;margin:25px;">
										<!-- begin table content-->
										<table class="contenttable" cellSpacing="0" cellPadding="0" width="100%">
											<tr>
												<td class="spacertables"></td>
											</tr>
											<tr>
												<td class="contentcellheader" noWrap colSpan="6"><asp:label id="lblCurrentSkill" runat="server" cssclass="Label" Font-Bold="true">Current Skills - </asp:label><asp:Label ID="lblStudentName" cssClass="label" Runat="server" Font-Bold=True></asp:Label></td>
											</tr>
											<tr>
												<td class="spacertables"></td>
											</tr>
										</table>
										<table border="0" width="100%">
											<tr>
												<td align="left" class="Label">
													<asp:Label ID="lblOutput" cssclass="label" Runat="server"></asp:Label>
												</td>
											</tr>
										</table>
										<!--end table content-->
									</div>
								</td>
							</tr>
						</table>
					</td>
					<!-- end rightcolumn -->
				</tr>
			</table>
			<!-- begin footer -->
			<!-- end footer -->
			<!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="ValidationSummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
		</form>
	</body>
</HTML>
