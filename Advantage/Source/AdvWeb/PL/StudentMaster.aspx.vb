﻿Imports System.Diagnostics
Imports Telerik.Web.UI.Calendar
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Advantage.Business.Objects
Imports System.Collections
Imports System.Drawing.Imaging
Imports System.Drawing.Drawing2D
Imports FAME.Advantage.Common
Imports System.Collections.Generic
Imports Telerik.Web.UI

Partial Class StudentMaster
    Inherits BasePage

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private pObj As New UserPagePermissionInfo
    Private stuMasterInfo As New StudentMasterInfo
    Private addressHasChanged As Boolean = False
    Private bDispSsNbyRoles As Boolean
    Private sHiddenSSNValue As String = "XXX-XX-XXXX"

    Protected StudentId As String
    Protected ResourceId As Integer
    Protected ModuleId As String
    Protected CampusId As String
    Protected UserId As String
    Protected ModCode As String
    Protected State As AdvantageSessionState
    Protected LeadId As String
    Protected MyAdvAppSettings As AdvAppSettings
    Protected StuEnrollmentId As String


    ' ReSharper disable InconsistentNaming
    Protected WithEvents advMessage As AdvantageMessage
    Protected WithEvents txtEmployerContactId As TextBox
    Protected WithEvents txtEmployerId As TextBox
    Protected WithEvents lblTitleId As Label
    Protected WithEvents ddlTitleId As DropDownList
    Protected WithEvents lblWorkPhone As Label
    Protected WithEvents txtWorkPhone As TextBox
    Protected WithEvents lblCellPhone As Label
    Protected WithEvents txtCellPhone As TextBox
    Protected WithEvents lblEmails As Label
    Protected WithEvents lblBeeper As Label
    Protected WithEvents txtBeeper As TextBox
    Protected WithEvents ChkStatus As CheckBox
    Protected WithEvents dlstEmployerContact As DataList
    Protected WithEvents btnhistory As Button
    Protected WithEvents txtState As TextBox
    Protected WithEvents lblExt As Label
    Protected WithEvents txtExtension As TextBox
    Protected WithEvents lblBestTime As Label
    Protected WithEvents txtBestTime As TextBox
    Protected WithEvents lblPinNumber As Label
    Protected WithEvents txtPinNumber As TextBox
    Protected WithEvents lblWorkExt As Label
    Protected WithEvents txtWorkExt As TextBox
    Protected WithEvents lblWorkBestTime As Label
    Protected WithEvents txtWorkBestTime As TextBox
    Protected WithEvents lblHomeBestTime As Label
    Protected WithEvents txtHomeBestTime As TextBox
    Protected WithEvents txtCellBestTime As TextBox
    Protected WithEvents txtRowIds As TextBox
    Protected WithEvents txtResourceId As TextBox
    Protected WithEvents lblSource As Label
    Protected WithEvents lblCategoryID As Label
    Protected WithEvents ddlCategoryID As DropDownList
    Protected WithEvents lblSourceDate As Label
    Protected WithEvents lblSourceTypeID As Label
    Protected WithEvents ddlSourceTypeID As DropDownList
    Protected WithEvents lblAreaID As Label
    Protected WithEvents ddlAreaID As DropDownList
    Protected WithEvents lblProgramID As Label
    Protected WithEvents ddlProgramID As DropDownList
    Protected WithEvents ddlDriverLicenseNumber As DropDownList
    Protected WithEvents lblCountyID As Label
    Protected WithEvents txtPhone1 As TextBox
    Protected WithEvents lblCatagorySource As Label
    Protected WithEvents ddlCatagorySource As DropDownList
    Protected WithEvents lblSourceAdvertisement As Label
    Protected WithEvents ddlSourceAdvertisement As DropDownList
    Protected WithEvents lblCampusId As Label
    Protected WithEvents ddlCampusId As DropDownList
    Protected WithEvents lbl2 As Label
    Protected WithEvents chkForeign As CheckBox
    Protected WithEvents lblPhoneType As Label
    Protected WithEvents lblPhoneStatus As Label
    Protected boolSwitchCampus As Boolean = False
    Protected boolReloadURL As Boolean = False
    ' ReSharper restore InconsistentNaming

    Protected WithEvents RegularExpressionValidator1 As RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator2 As RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator3 As RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator4 As RegularExpressionValidator
    Protected WithEvents Label2 As Label
    Protected WithEvents Label3 As Label
    Protected WithEvents Textbox1 As TextBox
    Protected WithEvents Label4 As Label
    Protected WithEvents Textbox2 As TextBox
    Protected WithEvents Label5 As Label
    Protected WithEvents Textbox3 As TextBox
    Protected WithEvents Label6 As Label
    Protected WithEvents Textbox4 As TextBox
    Protected WithEvents Dropdownlist1 As DropDownList
    Protected WithEvents Img1 As HtmlImage
    Protected WithEvents ZipFormat As Label
    Protected WithEvents PhoneFormat As Label
    Protected WithEvents Label1 As Label


    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreInit
        'AdvantageSession.PageTheme = PageTheme.Blue_Theme
        'Dim strVID As String = MyBase.getStudentObjectPointer(203)
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    'Private Function GetStudentFromStateObject() As StudentMRU

    '    'Dim CampusId As String
    '    Dim objStudentState As New StudentMRU

    '    Try

    '        MyBase.GlobalSearchHandler(0)

    '        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

    '        If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
    '            StudentId = Guid.Empty.ToString()
    '        Else
    '            StudentId = AdvantageSession.MasterStudentId
    '        End If

    '        If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
    '            LeadId = Guid.Empty.ToString()
    '        Else
    '            LeadId = AdvantageSession.MasterLeadId
    '        End If


    '        With objStudentState
    '            .StudentId = New Guid(StudentId)
    '            .LeadId = New Guid(LeadId)
    '            .Name = AdvantageSession.MasterName
    '        End With

    '        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
    '        HttpContext.Current.Items("Language") = "En-US"

    '        Master.ShowHideStatusBarControl(True)

    '    Catch ex As Exception
    '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Dim strSearchUrl As String = ""
    '        strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
    '        Response.Redirect(strSearchUrl)
    '    End Try
    '    Return objStudentState

    'End Function
#End Region

    Private Sub Page_Load1(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'wire the event source with the process method
        advMessage = New AdvantageMessage
        AddHandler advMessage.AdvantageMessaging, AddressOf (New CommonWebUtilities).ProcessAddressChangeMessage

        Session("SEARCH") = 0
        Dim sdfControls As New SDFComponent
        Dim fac As New UserSecurityFacade
        Dim objCommon As New CommonUtilities
        Dim userId As String
        'Dim strVID As String
        'Dim state As AdvantageSessionState

        ResourceId = CType(HttpContext.Current.Items("ResourceId"), Integer)
        CampusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId

        ''Trace.Warn("Begin MRU")
        Dim objStudentState As StudentMRU
        objStudentState = GetObjStudentState(0) ' GetStudentFromStateObject() 'Pass resourceid so that user can be redirected to same page while swtiching students
        Master.ShowHideStatusBarControl(True)
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            'LeadId = .LeadId.ToString
            LeadId = .ShadowLead.ToString()

        End With
        ''Trace.Warn("End MRU")
        'There are scenarios when a user may migrate from a student page but the student may not have any lead
        'associated data. In that case, the behavior is to load the lead user previously accessed instead of 
        'redirecting user to an error page


        If MyAdvAppSettings.AppSettings("ShowApportioningCreditBalance") = True Then
            pnlApportioningCrCalance.Visible = True
        End If
        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''
        ''Trace.Warn("begin permission")
        pObj = fac.GetUserResourcePermissions(userId, ResourceId, CampusId)
        ''Trace.Warn("end permission")
        txtDate.Text = Date.Now.ToShortDateString

        ' Header1.ObjectID = StudentId
        Master.PageObjectId = StudentId
        Master.PageResourceId = ResourceId
        Master.SetHiddenControlForAudit()

        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        'US3434 - SSN field on the Student Master page to visible to only certain security roles 
        bDispSsNbyRoles = fac.GetRolePermissionforSSN(userId, CampusId)
        If Master.IsSwitchedCampus Then
            Session("CurrentEnrollmentId") = Nothing
        End If
        GetStudentInfo(StudentId, False)
        '  SSNbyRolesGetRolePermissionforSSN()
        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            InitButtonsForLoad()

            'objCommon.PageSetup(Form1, "NEW")
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), , chkForeignZip.Checked)
            txtAddress2.MaxLength = 250
            ViewState("MODE") = "NEW"
            'Trace.Warn("begin build dropdown")
            BuildDropDownLists()
            'Trace.Warn("end build dropdown")
            'btnDelete.Enabled = True
            If Trim(StudentId) <> "" Then
                Dim boolRegent As Boolean = False
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    lblEntranceInterviewDate.Visible = True
                    txtEntranceInterviewDate.Visible = True
                    Img7.Visible = True
                    lblHighSchoolProgramCode.Visible = True
                    txtHighSchoolProgramCode.Visible = True
                    boolRegent = True
                End If
                GetStudentInfo(StudentId, boolRegent)
                txtStudentID.Text = StudentId
                chkIsInDB.Checked = True
            Else
                chkIsInDB.Checked = False
                txtStudentID.Text = Guid.NewGuid.ToString
            End If

            ''Trace.Warn("Begin Populate Value")

            BindStudentMaster(stuMasterInfo)
            BindLeadGroupList()
            ' 'Trace.Warn("End Populate Value")
            Session("ScrollValue") = 0
            If chkForeignZip.Checked Then
                Dim ctl As Control
                Dim rfv As RequiredFieldValidator
                ctl = pnlRequiredFieldValidators.FindControl("rfvStateId1273")
                If Not (ctl Is Nothing) Then
                    rfv = CType(ctl, RequiredFieldValidator)
                    rfv.Enabled = False
                    lblStateId.Text = "State"
                End If
            End If

            ' 'Trace.Warn("Begin UDF")
            If Trim(txtStudentID.Text) <> "" Then
                'SDFControls.GenerateControlsEdit(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("pnlSDF"), resourceId, txtStudentID.Text, ModuleId)
                sdfControls.GenerateControlsEdit(CType(MyBase.FindControlRecursive("pnlSDF"), Panel), ResourceId, txtStudentID.Text, ModuleId)
            Else
                sdfControls.GenerateControlsNew(CType(MyBase.FindControlRecursive("pnlSDF"), Panel), ResourceId, ModuleId)
            End If
            ''Trace.Warn("End UDF")

            MyBase.uSearchEntityControlId.Value = ""


        Else

            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), , chkForeignZip.Checked)
            txtAddress2.MaxLength = 250
            If Trim(txtStudentID.Text) <> "" Then
                sdfControls.GenerateControlsEdit(CType(MyBase.FindControlRecursive("pnlSDF"), Panel), ResourceId, txtStudentID.Text, ModuleId)
            Else
                sdfControls.GenerateControlsNew(CType(MyBase.FindControlRecursive("pnlSDF"), Panel), ResourceId, ModuleId)
            End If



        End If
        'IPEDSRequirements()


        'Check If any UDF exists for this resource
        Dim intSdfExists As Integer = sdfControls.GetSDFExists(ResourceId, ModuleId)
        If intSdfExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        'Disable State DropdownList if International is Checked
        If chkForeignZip.Checked = True Then
            txtOtherState.Visible = True
            lblOtherState.Visible = True
            ddlStateId.Enabled = False
        Else
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            ddlStateId.Enabled = True
        End If


        'Apply Mask/Disable Mask Based on if user 
        'selects International
        GetInputMaskValue()

        'Set the back color for the Lead Groups
        chkLeadGrpId.BackColor = Color.FromName("#ffff99")

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        If (Master.IsSwitchedCampus) Then
            CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
        End If
    End Sub

    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub GetInputMaskValue()
        Dim facInputMasks As New InputMasksFacade
        'Dim strMask As String
        Dim zipMask As String
        'Dim objCommon As New CommonUtilities
        Dim ssnMask As String

        'Apply Mask Only If Its Local Phone Number
        'Get The Input Mask for Phone/Fax and Zip
        'ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        'Replace The Mask Character from # to 9 as Masked Edit TextBox 
        'accepts only certain characters as mask characters
        'txtSSN.Mask = Replace(ssnMask, "#", "9")
        'Get The Format Of the input masks and display it next to caption
        'labels

        'strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        'Replace The Mask Character from # to 9 as Masked Edit TextBox 
        'accepts only certain characters as mask characters
        'txtSSN.Mask = Replace(ssnMask, "#", "9")
        lblSSN.ToolTip = ssnMask
        SetPhoneMask(chkForeignPhone.Checked)

        If chkForeignZip.Checked = False Then
            ' txtZip.Mask = Replace(zipMask, "#", "9")
            txtZip.Mask = "#####"
            lblZip.ToolTip = zipMask
        Else
            txtZip.Mask = "aaaaaaaaaa"
            lblZip.ToolTip = ""
        End If
    End Sub
    Private Sub GetStudentInfo(ByVal stdId As String, Optional ByVal useRegent As Boolean = False)
        Dim studentInfo As New StudentMasterFacade
        Dim studentInfoBar = DirectCast(Master.FindControl("statusBarControl"), UserControls_StudentInfoBar)
        If studentInfoBar IsNot Nothing Then
            StuEnrollmentId = If(studentInfoBar.StudentEnrollmentId Is Nothing, Session("CurrentEnrollmentId"), studentInfoBar.StudentEnrollmentId)
        End If
        stuMasterInfo = studentInfo.GetStudentInfo(stdId, useRegent, StuEnrollmentId)
        'BindStudentMaster(StudentInfo.GetStudentInfo(StdId))
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        'Display error in message box in the client
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim studentMasterUpdate As New StudentMasterFacade
        'Dim objCommon As New CommonUtilities
        Dim result As String
        Dim errorMessage As String = String.Empty

        If IsDate(txtDOB.DbSelectedDate) Then
            If DateDiff(DateInterval.Year, CType(txtDOB.DbSelectedDate, Date), CDate(txtDate.Text)) < 1 Then
                DisplayErrorMessage("The DOB cannot be greater than or equal to today's date ")
                txtAge.Text = ""
                Exit Sub
            End If

            Dim intAge As Integer
            Dim intConfigAge As Integer = CType(MyAdvAppSettings.AppSettings("StudentAgeLimit"), Integer)
            '2/13/2013
            'intAge = Now.Year - Year(CType(txtDOB, RadDatePicker).DbSelectedDate) - 1

            Dim dob As Date = CType(txtDOB.SelectedDate, Date)
            'dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If

            If intAge < intConfigAge Then
                DisplayErrorMessage("The minimum age requirement  is " & intConfigAge)
                txtAge.Text = ""
                Exit Sub
            Else
                txtAge.Text = CType(intAge, String)
            End If
        End If

        Dim strAddressType As String = String.Empty
        If ddlStudentStatus.SelectedValue = Guid.Empty.ToString Then
            strAddressType = "Student Status is required" & vbLf
        End If

        If chkForeignZip.Checked = False Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountryId.SelectedValue = "" Or Not ddlStateId.SelectedValue = "" Or Not txtZip.Text = "" Then
                If ddlAddressTypeId.SelectedValue = "" Or ddlAddressTypeId.SelectedValue = Guid.Empty.ToString Then
                    strAddressType = "Address type is required" & vbLf
                End If
                If ddlAddressStatus.SelectedValue = "" Or ddlAddressStatus.SelectedValue = Guid.Empty.ToString Then
                    strAddressType &= "Address status is required" & vbLf
                End If
            End If
        End If

        If chkForeignZip.Checked = True Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountryId.SelectedValue = Guid.Empty.ToString Or Not txtOtherState.Text = "" Or Not txtZip.Text = "" Then
                If ddlAddressTypeId.SelectedValue = "" Or ddlAddressTypeId.SelectedValue = Guid.Empty.ToString Then
                    strAddressType = "Address type is required" & vbLf
                End If
                If ddlAddressStatus.SelectedValue = "" Or ddlAddressStatus.SelectedValue = Guid.Empty.ToString Then
                    strAddressType &= "Address status is required" & vbLf
                End If
            End If
        End If

        If (Not ddlAddressTypeId.SelectedValue = "" Or Not ddlAddressTypeId.SelectedValue = Guid.Empty.ToString) Then
            If txtAddress1.Text = "" Then
                strAddressType &= "Address 1 is required" & vbLf
            Else
                If txtAddress1.Text.Length >= 251 Then
                    strAddressType &= "Address 1 should not be more than 250 charaters" & vbLf
                End If
            End If
        End If

        If Not txtPhone.Text = "" Then
            If ddlPhoneTypeId.SelectedValue = "" Or ddlPhoneTypeId.SelectedValue = Guid.Empty.ToString Then
                strAddressType &= "Phone type is required" & vbLf
            End If
            If ddlStatusId.SelectedValue = "" Or ddlStatusId.SelectedValue = Guid.Empty.ToString Then
                strAddressType &= "Phone status is required" & vbLf
            End If
        End If

        'DE9025 02/26/2013 Janet Robinson
        If Not txtPhone.Text = "" Then
            If Not chkForeignPhone.Checked And txtPhone.Text.Length < 10 Then
                strAddressType &= "Domestic phone numbers must include the area code and phone number and be 10 characters in length (Phone field)" & vbLf
            End If
        End If



        If Not strAddressType = "" Then
            DisplayErrorMessage(strAddressType)
            Exit Sub
        End If



        For i As Integer = 0 To tbBalY1.Text.Length - 1
            If Not tbBalY1.Text.Chars(i) = "." Then
                If Not IsNumeric(tbBalY1.Text.Chars(i)) Then
                    DisplayErrorMessage("The apportioned balance Y1 must be numeric")
                    Exit Sub
                End If
            End If
        Next

        For i As Integer = 0 To tbBalY2.Text.Length - 1
            If Not tbBalY2.Text.Chars(i) = "." Then
                If Not IsNumeric(tbBalY2.Text.Chars(i)) Then
                    DisplayErrorMessage("The apportioned balance Y2 must be numeric")
                    Exit Sub
                End If
            End If
        Next

        If Not String.IsNullOrEmpty(txtSSN.Text) Then
            Dim strDupSsnMsg As String = studentMasterUpdate.CheckDuplicateSSN(txtOriginalFirstName.Text, txtOriginalLastName.Text, txtSSN.Text)
            If Not String.IsNullOrEmpty(strDupSsnMsg) Then
                DisplayErrorMessage(strDupSsnMsg)
                Exit Sub
            End If
        End If


        If errorMessage = "" Then

            'Call Update Function in the plEmployerInfoFacade
            Dim boolRegent = MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes"
            Dim newStudentMasterInfo As StudentMasterInfo = BuildStudentMaster(Trim(StudentId))
            result = studentMasterUpdate.UpdateStudentMaster(newStudentMasterInfo, CType(Session("UserName"), String), String.Empty, boolRegent, bDispSsNbyRoles)

            If Not result = "" Then
                DisplayErrorMessage(result)
                Exit Sub
            Else

                'here we check the AddressChange Event
                If addressHasChanged Then
                    'there are 13 parameters
                    Dim sqlParameters As New ArrayList
                    Dim oldStudentMasterInfo As StudentMasterInfo = CType(ViewState("PreviousStudentMasterInfo"), StudentMasterInfo)

                    'Parameter 0 is StudentId
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@StudentId", newStudentMasterInfo.StudentID))

                    'Parameter 1 is OldAddress1
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@OldAddress1", oldStudentMasterInfo.Address1))

                    'Parameter 2 is OldAddress2
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@OldAddress2", oldStudentMasterInfo.Address2))

                    'Parameter 3 is OldCity
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@OldCity", oldStudentMasterInfo.City))

                    'Parameter 4 is OldState
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@OldState", oldStudentMasterInfo.State))

                    'Parameter 5 is OldZip
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@OldZip", oldStudentMasterInfo.Zip))

                    'Parameter 6 is OldCountry
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@OldCountry", oldStudentMasterInfo.Country))

                    'Parameter 7 is NewAddress1
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@NewAddress1", newStudentMasterInfo.Address1))

                    'Parameter 8 is NewAddress2
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@NewAddress2", newStudentMasterInfo.Address2))

                    'Parameter 9 is NewCity
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@NewCity", newStudentMasterInfo.City))

                    'Parameter 10 is NewState
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@NewState", newStudentMasterInfo.State))

                    'Parameter 11 is NewZip
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@NewZip", newStudentMasterInfo.Zip))

                    'Parameter 12 is NewCountry
                    sqlParameters.Add(New AdvantageMessagingSqlParameter("@NewCountry", newStudentMasterInfo.Country))

                    'raise Address Change event
                    advMessage.RaiseAdvantageMessagingEvent(AdvantageEvent.Address_Change, CampusId, sqlParameters)
                End If
                'DE8788
                BuildDropDownLists()
                If Trim(StudentId) <> "" Then
                    If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                        lblEntranceInterviewDate.Visible = True
                        txtEntranceInterviewDate.Visible = True
                        Img7.Visible = True
                        lblHighSchoolProgramCode.Visible = True
                        txtHighSchoolProgramCode.Visible = True
                        boolRegent = True
                    End If
                    GetStudentInfo(StudentId, boolRegent)
                    txtStudentID.Text = StudentId
                    chkIsInDB.Checked = True
                Else
                    chkIsInDB.Checked = False
                    txtStudentID.Text = Guid.NewGuid.ToString
                End If

                ''Trace.Warn("Begin Populate Value")
                'BindStudentMaster(newStudentMasterInfo)
                BindStudentMaster(stuMasterInfo)
                BindLeadGroupList()
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim sdfid As ArrayList
            Dim sdfidValue As ArrayList
            '   Dim newArr As ArrayList
            Dim z As Integer
            Dim sdfControl As New SDFComponent
            Dim strResult As String
            ' Dim strResultMessage As String = ""
            Try
                sdfid = sdfControl.GetAllLabels(pnlSDF)
                sdfidValue = sdfControl.GetAllValues(pnlSDF)
                For z = 0 To sdfid.Count - 1
                    strResult = sdfControl.InsertValues(txtStudentID.Text, Mid(sdfid(z).id, 5), sdfidValue(z), Session("UserName"))
                    If Not strResult = "" Then
                        ' strResultMessage &= strResult & vbLf
                    End If
                Next
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                DisplayErrorMessage(ex.Message)
                chkIsInDB.Checked = True
                btnNew.Enabled = False
                'btnDelete.Enabled = True
                Exit Sub
            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            'If DML is not successful then Prompt Error Message

            'Results.Visible = True
            chkIsInDB.Checked = True
            btnNew.Enabled = False
            If pObj.HasFull Or pObj.HasDelete Then
                'btnDelete.Enabled = True
            Else
                btnDelete.Enabled = False
            End If


        Else
            DisplayErrorMessageMask(errorMessage)
        End If
    End Sub

    Private Function BuildStudentMaster(ByVal studentIdp As String) As StudentMasterInfo
        Dim studentMaster As New StudentMasterInfo
        Dim studentInfoBar = DirectCast(Master.FindControl("statusBarControl"), UserControls_StudentInfoBar)
        If studentInfoBar IsNot Nothing Then
            StuEnrollmentId = studentInfoBar.StudentEnrollmentId
        End If
        With studentMaster
            .EnrollmentId = StuEnrollmentId

            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get StudentMasterID
            .StudentID = Trim(studentIdp)

            'get LeadId
            .LeadId = txtLeadId.Text

            'Get LastName
            .LastName = txtLastName.Text

            'Get FirstName
            .FirstName = txtFirstName.Text

            'Get MiddleName
            .MiddleName = txtMiddleName.Text

            'Get StatusId 
            .Status = ddlStudentStatus.SelectedValue

            'Get PrefixId 
            .Prefix = ddlPrefix.SelectedValue

            'Get SuffixId 
            .Suffix = ddlSuffix.SelectedValue

            'Get Title 
            '.Title = ddlTitleId.SelectedValue

            'Get BirthDate
            .BirthDate = CType(txtDOB.DbSelectedDate, String)

            'Get Age
            '.Age = txtAge.Text

            'Get sponsor
            .Sponsor = ddlSponsor.SelectedValue

            'Get AdmissionsRep
            '.AdmissionsRep = ddlAdmissionsRep.SelectedValue

            'Get Gender
            .Gender = ddlGender.SelectedValue

            'Get Race
            .Race = ddlRace.SelectedValue

            'Get MaritalStatus
            .MaritalStatus = ddlMaritalStatus.SelectedValue

            'Get Children
            .Children = txtChildren.Text

            'Get FamilyIncome
            .FamilyIncome = ddlFamilyIncome.SelectedValue

            'Get Phone
            .Phone = txtPhone.Text

            'Get PhoneType
            .PhoneType = ddlPhoneTypeId.SelectedValue

            'Get Phone Status
            .PhoneStatus = ddlStatusId.SelectedValue

            'Get HomeEmail  -- Email Best
            .HomeEmail = txtHomeEmail.Text

            'Get WorkEmail
            .WorkEmail = txtWorkEmail.Text

            'Get Address1
            .Address1 = txtAddress1.Text

            'Get Address2
            .Address2 = txtAddress2.Text

            'Get City
            .City = txtCity.Text

            'Get State
            .State = ddlStateId.SelectedValue

            .StudentGrpId = ddlStudentGrpId.SelectedValue

            .Zip = txtZip.Text

            'Get the check button is international
            .IsInternational = chkForeignZip.Checked

            'Get Country 
            .Country = ddlCountryId.SelectedValue

            'Get County
            .County = ddlCountyId.SelectedValue

            'Get AddressType
            .AddressType = ddlAddressTypeId.SelectedValue

            'Address Status
            .AddressStatus = ddlAddressStatus.SelectedValue

            'Get Education
            '.Education = ddlEdLvlId.SelectedValue


            'Get Nationality
            .Nationality = ddlNationality.SelectedValue

            'Get Citizen
            .Citizen = ddlCitizen.SelectedValue

            'Get SSN 
            If bDispSsNbyRoles Then
                .SSN = txtSSN.Text
            End If
            'Get DriverLicState 
            .DriverLicState = ddlDrivLicStateId.SelectedValue

            'Get DriverLicNumber
            .DriverLicNumber = txtDrivLicNumber.Text

            'Get AlienNumber
            .AlienNumber = txtAlienNumber.Text

          
            'Notes
            .Notes = txtComments.Text


            .AssignmentDate = CType(txtAssignedDate.DbSelectedDate, String)

            'CampusId
            ' .CampusId = ddlCampusId.SelectedValue

            'StudentNumber 
            .StudentNumber = txtStudentNumber.Text

            'Foreign Zip
            If chkForeignZip.Checked = True Then
                .ForeignZip = 1
            Else
                .ForeignZip = 0
            End If

            'Foreign Phone
            If chkForeignPhone.Checked = True Then
                .ForeignPhone = 1
            Else
                .ForeignPhone = 0
            End If
            .OtherState = txtOtherState.Text

            txtModDate.Text = CType(Date.Now, String)

            .ModDate = CType(txtModDate.Text, Date)

            .DependencyTypeId = ddlDependencyTypeId.SelectedValue
            .GeographicTypeId = ddlGeographicTypeId.SelectedValue
            .AdminCriteriaId = ddlAdminCriteriaId.SelectedValue
            .HousingId = ddlHousingId.SelectedValue

            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                .EntranceInterviewDate = CType(txtEntranceInterviewDate.Text, Date)
                .HighSchoolProgramCode = txtHighSchoolProgramCode.Text
            End If


            If MyAdvAppSettings.AppSettings("ShowApportioningCreditBalance") = True Then
                .ApportionedCreditBalanceAY1 = tbBalY1.Text
                .ApportionedCreditBalanceAY2 = tbBalY2.Text
            End If


        End With
        Return studentMaster
    End Function
    Private Sub BindStudentMaster(ByVal studentMaster As StudentMasterInfo)

        'Bind The EmployerInfo Data From The Database
        Dim getStudentFormat As New StudentMasterFacade
        Dim StudInfo As StudentMasterInfo = getStudentFormat.GetStudentFormat()
        Dim intYearNumber As Integer
        Dim intMonthNumber As Integer
        Dim intDateNumber As Integer
        Dim intLNameNumber As Integer
        Dim intFNameNumber As Integer
        Dim intSeqNumber As Integer
        Dim intStartSeqNumber As Integer
        Dim strFormatType As String

        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        Dim ssnMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)


        With StudInfo
            intYearNumber = .YearNumber
            intMonthNumber = .MonthNumber
            intDateNumber = .DateNumber
            intLNameNumber = .LNameNumber
            intFNameNumber = .FNameNumber
            intSeqNumber = .SeqNumber
            intStartSeqNumber = .SeqStartNumber
            strFormatType = .FormatType
        End With

        'Troy: 9/6/2013 I added the try block because conversions databases will not have any value for the format type
        Try
            If strFormatType = 3 Then
                txtStudentNumber.Enabled = False
            ElseIf strFormatType = 1 Then
                txtStudentNumber.Enabled = True
            Else
                txtStudentNumber.Enabled = False
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'If the code comes here it should be most likely from a conversion where the format type is an empty string since
            'the syStudentFormat table most likely will not have any avalue. I encountered this problem while running the BSA 3.4 conversion.
            'Discussed the problem with Balaji and he said to go ahead and just fix the code and proceed.
            txtStudentNumber.Enabled = False
        End Try


        With studentMaster

            'Get Foreign Checkbox
            If .ForeignPhone = True Then
                chkForeignPhone.Checked = True
            Else
                chkForeignPhone.Checked = False
            End If

            ' Get Is International ...
            chkForeignZip.Checked = .IsInternational
            'If .ForeignZip = True Then
            '    chkForeignZip.Checked = True
            'Else
            '    chkForeignZip.Checked = False
            'End If

            'get IsInDB
            chkIsInDB.Checked = .IsInDB

            'get LeadMasterID
            txtStudentID.Text = .StudentID

            'Get LastName
            txtLastName.Text = .LastName

            'Get FirstName
            txtFirstName.Text = .FirstName

            'Get MiddleName
            txtMiddleName.Text = .MiddleName

            'Get LeadId
            txtLeadId.Text = .LeadId

            'Get BirthDate
            txtDOB.DbSelectedDate = .BirthDate

            If IsDate(txtDOB.DbSelectedDate) Then
                Dim dob As Date = CType(txtDOB.SelectedDate, Date)
                Dim intage As Int16
                'dob.ToString("yyyy/MM/dd")
                intage = CType((Today.Year - dob.Year), Short)
                If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso
                    dob.Day <> Today.Day) Then
                    'The current year is not yet complete. 
                    intage -= CType(1, Short)
                End If
                txtAge.Text = CType(intage, String)
            Else
                txtAge.Text = ""
            End If

            'Get Children
            txtChildren.Text = .Children

            'Get Phone
            SetPhoneMask(chkForeignPhone.Checked)
            txtPhone.Text = .Phone

            'Get HomeEmail
            txtHomeEmail.Text = .HomeEmail

            'Get WorkEmail
            txtWorkEmail.Text = .WorkEmail

            'Get Address1
            txtAddress1.Text = .Address1

            'Get Address2
            txtAddress2.Text = .Address2

            'Get City
            txtCity.Text = .City

            'ddlCounty.Text = .CountyDescrip

            'Get Zip
            'txtZip.Text = .Zip
            'If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            '    txtZip.Text = facInputMasks.ApplyMask(zipMask, txtZip.Text)
            'End If
            'If .Zip <> "" And .ForeignZip = 0 Then
            If Not chkForeignZip.Checked Then
                txtZip.Mask = "#####"
                lblZip.ToolTip = zipMask
            Else
                txtZip.Mask = "aaaaaaaaaa"
                lblZip.ToolTip = ""
            End If
            txtZip.Text = .Zip


            'Get SSN 
            If bDispSsNbyRoles Then
                txtSSN.Text = .SSN
                'If txtSSN.Text <> "" Then
                '    txtSSN.Text = facInputMasks.ApplyMask(ssnMask, txtSSN.Text)
                'End If
            Else
                'txtSSNValid.Text = "Y"
                If .SSN = "" Then
                    '    txtSSNValid.Text = ""
                    sHiddenSSNValue = ""
                End If
                HideSSNValue()
            End If



            'Get DriverLicNumber
            txtDrivLicNumber.Text = .DriverLicNumber

            'Get AlienNumber
            txtAlienNumber.Text = .AlienNumber



            'Notes
            txtComments.Text = .Notes

            'Assignment Date
            txtAssignedDate.DbSelectedDate = .AssignmentDate

            'CampusId
            'ddlCampusId.SelectedValue = .CampusId

            'StudentNumber
            txtStudentNumber.Text = .StudentNumber

            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                txtEntranceInterviewDate.Text = CType(.EntranceInterviewDate, String)
                txtHighSchoolProgramCode.Text = .HighSchoolProgramCode
            End If

            'Get Foreign Zip
            If .ForeignZip = True Then
                chkForeignZip.Checked = True
                txtOtherState.Visible = True
                lblOtherState.Visible = True
                txtOtherState.Text = .OtherState
                ddlStateId.Enabled = False
            Else
                chkForeignZip.Checked = False
            End If

            txtModDate.Text = CType(.ModDate, String)

            tbBalY1.Text = .ApportionedCreditBalanceAY1

            tbBalY2.Text = .ApportionedCreditBalanceAY2


            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrefix, .Prefix, .PrefixDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSuffix, .Suffix, .SuffixDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdminCriteriaId, .AdminCriteriaId, .AdminCriteriaDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGender, .Gender, .GenderDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRace, .Race, .EthCodeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlMaritalStatus, .MaritalStatus, .MaritalStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFamilyIncome, .FamilyIncome, .FamilyIncomeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneTypeId, .PhoneType, .PhoneTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStatusId, .PhoneStatus, .PhoneStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountyId, .County, .CountyDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateId, .State, .StateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountryId, .Country, .CountryDescrip)
            'CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlEdLvlId, .Education, .EdLvlDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStudentStatus, .Status, .StudentStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlNationality, .Nationality, .NationalityDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCitizen, .Citizen, .CitizenDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDrivLicStateId, .DriverLicState, .DriverLicenseStateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressTypeId, .AddressType, .AddressTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressStatus, .AddressStatus, .AddressStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDependencyTypeId, .DependencyTypeId, .DependencyTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGeographicTypeId, .GeographicTypeId, .GeographicTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSponsor, .Sponsor, .SponsorTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlHousingId, .HousingId, .HousingDescrip)

            'If .Country = "" Then
            '    Try
            '        ddlCountryId.SelectedValue = .Country
            '    Catch ex As System.Exception
            '    	Dim exTracker = new AdvApplicationInsightsInitializer()
            '    	exTracker.TrackExceptionWrapper(ex)

            '        ddlCountryId.SelectedIndex = 0
            '    End Try
            'End If

        End With
        Return
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        'Assign A New Value For Primary Key Column
        txtStudentID.Text = Guid.NewGuid.ToString()

        'Reset The Value of chkIsInDb Checkbox To Identify an Insert
        chkIsInDB.Checked = False

        'Create a Empty Object and Initialize the Object
        'BindStudentMasterItemCommand(New StudentMasterInfo)


        Dim sdfControls As New SDFComponent
        sdfControls.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)

        'Initialize Buttons
        InitButtonsForLoad()

        'Initialize SourceDate and ExpectedStart Date
        'txtSourceDate.Text = ""
        'txtExpectedStartDate.Text = ""
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        If pObj.HasFull Or pObj.HasDelete Then
            'btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        btnNew.Enabled = False

    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        If Not (txtStudentID.Text = Guid.Empty.ToString) Then
            'instantiate component
            Dim studentMaster As New StudentMasterFacade

            'Delete The Row Based on LeadId
            Dim result As String = studentMaster.DeleteStudentMaster(txtStudentID.Text, Date.Parse(txtModDate.Text))


            'If Delete Fails
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else

                chkIsInDB.Checked = False

                'bind an empty new BankAcctInfo
                BindStudentMasterItemCommand(New StudentMasterInfo)

                'initialize buttons
                InitButtonsForLoad()

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim sdfControl As New SDFComponent
                sdfControl.DeleteSDFValue(txtStudentID.Text)
                sdfControl.GenerateControlsNew(pnlSDF, ResourceId, ModuleId)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End If
        End If
    End Sub
    Private Sub BindStudentMasterItemCommand(ByVal studentMaster As StudentMasterInfo)
        ''Bind The EmployerInfo Data From The Database
        With studentMaster
            'get IsInDB
            chkIsInDB.Checked = .IsInDB

            'get LeadMasterID
            txtStudentID.Text = .StudentID

            'Get LastName
            txtLastName.Text = .LastName

            'Get FirstName
            txtFirstName.Text = .FirstName

            'Get MiddleName
            txtMiddleName.Text = .MiddleName

            'Get BirthDate
            txtDOB.DbSelectedDate = .BirthDate

            'Get Age
            txtAge.Text = ""

            'Get Phone
            txtPhone.Text = .Phone

            'Get HomeEmail
            txtHomeEmail.Text = .HomeEmail

            'Get WorkEmail
            txtWorkEmail.Text = .WorkEmail

            'Get Address1
            txtAddress1.Text = .Address1

            'Get Address2
            txtAddress2.Text = .Address2

            'Get City
            txtCity.Text = .City

            'Get State
            'txtDOB.Text = ""

            'Get Zip
            txtZip.Text = .Zip

            'Get SourceDate 
            'txtSourceDate.Text = .SourceDate

            'Get ExpectedStart
            'txtExpectedStartDate.Text = .ExpectedStart

            'Get SSN 
            If bDispSsNbyRoles Then
                txtSSN.Text = .SSN
            Else
                'txtSSNValid.Text = "Y"
                If .SSN = "" Or Len(.SSN) < 9 Then
                    '    txtSSNValid.Text = ""
                    sHiddenSSNValue = ""
                End If
                HideSSNValue()
            End If

            'Get DriverLicNumber
            txtDrivLicNumber.Text = .DriverLicNumber

            'Get AlienNumber
            txtAlienNumber.Text = .AlienNumber

            'Notes
            txtComments.Text = .Notes

            'Assigned Date
            txtAssignedDate.DbSelectedDate = .AssignmentDate

            'StudentNumber
            txtStudentNumber.Text = .StudentNumber

            chkForeignPhone.Checked = False
            chkForeignZip.Checked = False
            txtOtherState.Visible = False

            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                txtEntranceInterviewDate.Text = CType(.EntranceInterviewDate, String)
                txtHighSchoolProgramCode.Text = .HighSchoolProgramCode
            End If
        End With
        'BuildDropDownLists()
        InitializeDropDownLists()
    End Sub
    Private Sub InitializeDropDownLists()
        ddlStudentStatus.SelectedIndex = 0
        ddlPrefix.SelectedIndex = 0
        ddlSuffix.SelectedIndex = 0
        ddlSponsor.SelectedIndex = 0
        'ddlAdmissionsRep.SelectedIndex = 0
        ddlGender.SelectedIndex = 0
        ddlRace.SelectedIndex = 0
        ddlMaritalStatus.SelectedIndex = 0
        ddlFamilyIncome.SelectedIndex = 0
        ddlPhoneTypeId.SelectedIndex = 0
        ddlStatusId.SelectedIndex = 0
        ddlStateId.SelectedIndex = 0
        ddlCountryId.SelectedIndex = 0
        ddlNationality.SelectedIndex = 0
        ddlCitizen.SelectedIndex = 0
        ddlDrivLicStateId.SelectedIndex = 0
        ddlAddressTypeId.SelectedIndex = 0
        ddlAddressStatus.SelectedIndex = 0
        'ddlEdLvlId.SelectedIndex = 0
        ddlDependencyTypeId.SelectedIndex = 0
        ddlGeographicTypeId.SelectedIndex = 0
        ddlAdminCriteriaId.SelectedIndex = 0
        ddlHousingId.SelectedIndex = 0
    End Sub
    'Private Sub txtDOB_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDOB.TextChanged
    '    If Not CType(txtDOB, RadDatePicker).DbSelectedDate = "" And IsDate(CType(txtDOB, RadDatePicker).DbSelectedDate) Then
    '        If DateDiff(DateInterval.Year, CType(txtDOB, RadDatePicker).DbSelectedDate, CDate(txtDate.Text)) < 1 Then
    '            DisplayErrorMessage("The minimum age requirement for student is 18 ")
    '            txtAge.Text = ""
    '            Exit Sub
    '        End If

    '        Dim intAge As Integer
    '        intAge = Now.Year - Year(CType(txtDOB, RadDatePicker).DbSelectedDate) - 1
    '        'If intAge < 18 Then
    '        '    DisplayErrorMessage("The minimum age requirement for student is 18 ")
    '        '    txtAge.Text = ""
    '        '    Exit Sub
    '        'End If
    '        txtAge.Text = intAge 'Math.Floor(DateDiff(DateInterval.Day, CDate(txtDOB.Text), Now) / 365)
    '    End If
    'End Sub
    Private Sub ddlCategoryID_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCategoryID.SelectedIndexChanged
        'BuildSourceTypeDDL(ddlCategoryID.SelectedValue)
    End Sub
    Private Sub ddlSourceTypeID_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlSourceTypeID.SelectedIndexChanged
    End Sub
    Private Sub ddlCatagorySource_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlCatagorySource.SelectedIndexChanged
        'BuildSourceAdvDDL(ddlCatagorySource.SelectedValue)
    End Sub
    Private Sub ddlAreaID_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAreaID.SelectedIndexChanged
        ' BuildProgramsDDL(ddlAreaID.SelectedValue)
    End Sub
    Private Sub txtSourceDate_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSourceDate.TextChanged
        If Trim(txtSourceDate.Text.Substring(1, 2)) = "12" Then
            txtSourceDate.Text = ""
        End If
    End Sub
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreRender
        'save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        'save studentMasterInfo in the viewstate
        ViewState("PreviousStudentMasterInfo") = stuMasterInfo

        If Not Trim(Request.Form("scrollposition")) = "" Then
            Dim i As Integer = CInt(Trim(Request.Form("scrollposition")))
            Session("ScrollValue") = i
        End If
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'controlsToIgnore.Add(Img2)
        'controlsToIgnore.Add(Img4)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignPhone.CheckedChanged
        SetPhoneMask(chkForeignPhone.Checked)
        txtPhone.Focus()
    End Sub

    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignZip.CheckedChanged

        If chkForeignZip.Checked = True Then
            lblOtherState.Enabled = True
            txtZip.Mask = "aaaaaaaaaa"
            txtZip.Text = String.Empty
            ddlStateId.SelectedIndex = 0
            txtZip.MaxLength = 10
        Else
            txtZip.Mask = "#####"
            'txtZip.DisplayMask = "#####"
            txtZip.Text = String.Empty
            txtZip.MaxLength = 5
            Dim ctl As Control
            Dim rfv As RequiredFieldValidator
            ctl = pnlRequiredFieldValidators.FindControl("rfvStateId1273")
            If Not (ctl Is Nothing) Then
                rfv = CType(ctl, RequiredFieldValidator)
                rfv.Enabled = True
            End If
        End If
        'Dim objCommon As New CommonWebUtilities
        'CommonWebUtilities.SetFocus(Me.Page, txtAddress1)
        txtAddress1.Focus()

    End Sub

    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtOtherState.TextChanged
        'Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Page, txtZip)
    End Sub
    Private Sub txtCity_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtCity.TextChanged
        If chkForeignZip.Checked = True Then
            'Dim objCommon As New CommonWebUtilities
            CommonWebUtilities.SetFocus(Page, txtOtherState)
        End If
    End Sub
    Private Sub txtAddress1_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtAddress1.TextChanged, txtAddress1.TextChanged
        addressHasChanged = True
    End Sub

    Private Sub BindLeadGroupList()

        'Get Degrees data to bind the CheckBoxList
        Dim facade As New AdReqsFacade
        Dim ds As DataSet = facade.GetLeadgroupsSelectedByStudent(txtStudentID.Text)
        Dim jobCount As Integer = facade.GetLeadGroupCountByStudent(txtStudentID.Text)

        If jobCount >= 1 Then

            'Select the items on the CheckBoxList
            Dim i As Integer
            If Not (ds.Tables(0).Rows.Count < 0) Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim row As DataRow
                    row = ds.Tables(0).Rows(i)
                    Dim item As ListItem
                    For Each item In chkLeadGrpId.Items
                        If item.Value.ToString = DirectCast(row("LeadGrpId"), Guid).ToString Then
                            item.Selected = True
                            Exit For
                        End If
                    Next
                Next
            End If
        End If
    End Sub
    'Private Sub InitializeDropDown()

    'End Sub
    Private Sub BuildDropDownLists()

        'BuildPrefixDDL()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Lead Status
        ddlList.Add(New AdvantageDDLDefinition(ddlStudentStatus, AdvantageDropDownListName.Statuses, Nothing, True, True))

        'Prefixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlPrefix, AdvantageDropDownListName.Prefixes, CampusId, True, True))

        'Suffixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlSuffix, AdvantageDropDownListName.Suffixes, CampusId, True, True))

        'Country DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlCountryId, AdvantageDropDownListName.Countries, CampusId, True, True))

        'Address Type
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressTypeId, AdvantageDropDownListName.Address_Types, CampusId, True, True))

        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlDrivLicStateId, AdvantageDropDownListName.States, Nothing, True, True, String.Empty))

        'Races
        ddlList.Add(New AdvantageDDLDefinition(ddlRace, AdvantageDropDownListName.Races, CampusId, True, True, String.Empty))

        'Genders
        ddlList.Add(New AdvantageDDLDefinition(ddlGender, AdvantageDropDownListName.Genders, CampusId, True, True, String.Empty))

        'Marital Status
        ddlList.Add(New AdvantageDDLDefinition(ddlMaritalStatus, AdvantageDropDownListName.MaritalStatus, CampusId, True, True, String.Empty))

        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlStateId, AdvantageDropDownListName.States, Nothing, True, True, String.Empty))

        'Citizenships
        ddlList.Add(New AdvantageDDLDefinition(ddlCitizen, AdvantageDropDownListName.Citizen, CampusId, True, True, String.Empty))

        'Nationality
        ddlList.Add(New AdvantageDDLDefinition(ddlNationality, AdvantageDropDownListName.Nationality, CampusId, True, True, String.Empty))

        'Address States 
        ddlList.Add(New AdvantageDDLDefinition(ddlHousingId, AdvantageDropDownListName.HousingType, CampusId, True, True, String.Empty))

        'Shifts
        'ddlList.Add(New AdvantageDDLDefinition(ddlShiftID, AdvantageDropDownListName.Shifts, CampusId, True, False, String.Empty))

        'County
        ddlList.Add(New AdvantageDDLDefinition(ddlCountyId, AdvantageDropDownListName.Counties, CampusId, True, True, String.Empty))

        'Family Income
        ddlList.Add(New AdvantageDDLDefinition(ddlFamilyIncome, AdvantageDropDownListName.FamilyIncome, CampusId, True, True, String.Empty))

        'Previous Education
        'ddlList.Add(New AdvantageDDLDefinition(ddlEdLvlId, AdvantageDropDownListName.EducationLvl, campusId, True, True, String.Empty))

        'Phone Types
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneTypeId, AdvantageDropDownListName.Phone_Types, CampusId, True, True, String.Empty))

        'Phone Statuses
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Address Status
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressStatus, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Admission Reps
        ddlList.Add(New AdvantageDDLDefinition(ddlAdminCriteriaId, AdvantageDropDownListName.AdminCriteriaType, CampusId, True, True, String.Empty))

        'Dependency Type
        ddlList.Add(New AdvantageDDLDefinition(ddlDependencyTypeId, AdvantageDropDownListName.DependencyType, CampusId, True, True, String.Empty))

        'Geographic Type
        ddlList.Add(New AdvantageDDLDefinition(ddlGeographicTypeId, AdvantageDropDownListName.GeographicType, CampusId, True, True, String.Empty))

        'Sponsor
        ddlList.Add(New AdvantageDDLDefinition(ddlSponsor, AdvantageDropDownListName.Sponsors, CampusId, True, True, String.Empty))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

        'BuildResumeObjective()
        'BuildSourceCatagoryDDL()
        'BuildProgramGroupDDL()

        'BuildLeadGroupDDL()
        'BuildLeadGroupsDDL()
        'BuildAdmissionRepsDDL()
    End Sub
    'Private Sub IPEDSRequirements()
    '    isIPEDSApplicable = MyAdvAppSettings.AppSettings("IPEDS")
    '    If isIPEDSApplicable.ToLower = "yes" Then
    '        'Make Gender,Ethnic Code,Previous Education,Citizenship,BirthDate,state,Nationality Reqd
    '        ddlGender.BackColor = Color.FromName("#ffff99")
    '        ddlRace.BackColor = Color.FromName("#ffff99")
    '        'ddlEdLvlId.BackColor = Color.FromName("#ffff99")
    '        ddlCitizen.BackColor = Color.FromName("#ffff99")
    '        txtDOB.BackColor = Color.FromName("#ffff99")
    '        ddlStateId.BackColor = Color.FromName("#ffff99")
    '        ddlNationality.BackColor = Color.FromName("#ffff99")
    '    End If
    'End Sub
    'Private Function ValidateIPEDS() As String
    '    Dim strValidateIPEDS As String = ""
    '    isIPEDSApplicable = MyAdvAppSettings.AppSettings("IPEDS")
    '    If isIPEDSApplicable.ToLower = "yes" Then
    '        If ddlGender.SelectedValue = "" Then
    '            strValidateIPEDS = "Gender is required"
    '        End If
    '        If ddlRace.SelectedValue = "" Then
    '            strValidateIPEDS &= "Race is required" & vbLf
    '        End If
    '        'If ddlEdLvlId.SelectedValue = "" Then
    '        '    strValidateIPEDS &= "Previous Education is required" & vbLf
    '        'End If
    '        If ddlCitizen.SelectedValue = "" Or ddlCitizen.SelectedValue = Guid.Empty.ToString Then
    '            strValidateIPEDS &= "Citizen is required" & vbLf
    '        End If
    '        If CType(txtDOB, RadDatePicker).DbSelectedDate = "" Then
    '            strValidateIPEDS &= "DOB is required" & vbLf
    '        End If
    '        If ddlStateId.SelectedValue = "" Then
    '            strValidateIPEDS &= "State is required" & vbLf
    '        End If
    '        If ddlNationality.SelectedValue = "" Or ddlNationality.SelectedValue = Guid.Empty.ToString Then
    '            strValidateIPEDS &= "Nationality is required" & vbLf
    '        End If
    '        Return strValidateIPEDS
    '    Else
    '        Return strValidateIPEDS
    '    End If
    'End Function
    Public Sub DisplayStudentImage(ByVal imageUrl1 As String, ByVal imageLocation As String)
        'Get the image name – yourimage.jpg – from the query String
        Dim imageUrl As String '= Request.QueryString("img")
        Dim imageHeight As Integer
        'Set the thumbnail width in px – the width will be calculated later to keep the original ratio.
        Const imageWidth As Integer = 100 '300
        imageUrl = "C:\Test\test.jpg"
        Dim fullSizeImg As Image
        Try
            fullSizeImg = Image.FromFile(imageUrl)
            'Dim CurrentimgHeight As Integer = fullSizeImg.Height
            'Dim CurrentimgWidth As Integer = fullSizeImg.Width
            imageHeight = 100 'CurrentimgHeight / CurrentimgWidth * imageWidth


            'This will only work for jpeg images

            Response.ContentType = "image/jpeg"
            If imageHeight > 0 Then
                Dim dummyCallBack As Image.GetThumbnailImageAbort
                dummyCallBack = New _
                   Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)




                Dim thumbNailImg As Image
                thumbNailImg = fullSizeImg.GetThumbnailImage(imageWidth, imageHeight,
                                                             dummyCallBack, IntPtr.Zero)



                Dim g As Graphics = Graphics.FromImage(thumbNailImg)

                Dim wmFont As Font
                'Const strWatermark As String = "Michael"

                'Set the watermark font	 
                'wmFont = New Font("Verdana", 14, FontStyle.Bold)
                'Dim desiredWidth As Single = imageWidth * 0.5

                'use the MeasureString method to position the watermark in the centre of the image

                'Dim stringSizeF As SizeF = g.MeasureString(strWatermark, wmFont)
                'Dim Ratio As Single = stringSizeF.Width / wmFont.SizeInPoints
                'Dim RequiredFontSize As Single = desiredWidth / Ratio
                'wmFont = New Font("Verdana", RequiredFontSize, FontStyle.Bold)
                wmFont = New Font("Verdana", 10, FontStyle.Regular)

                'Sets the interpolation mode for a high quality image

                g.InterpolationMode = InterpolationMode.HighQualityBicubic
                g.DrawImage(fullSizeImg, 0, 0, imageWidth, imageHeight)
                g.SmoothingMode = SmoothingMode.HighQuality

                Dim letterBrush As SolidBrush = New SolidBrush(Color.FromArgb(50, 255, 255, 255))
                Dim shadowBrush As SolidBrush = New SolidBrush(Color.FromArgb(50, 0, 0, 0))

                'Enter the watermark text 
                g.DrawString("Michael", wmFont, shadowBrush, 75, CType(((imageHeight * 0.5) - 36), Single))
                g.DrawString("Michael", wmFont, letterBrush, 77, CType(((imageHeight * 0.5) - 38), Single))
                thumbNailImg.Save(Response.OutputStream, ImageFormat.Jpeg)
            Else
                fullSizeImg.Save(Response.OutputStream, ImageFormat.Jpeg)
            End If
            'Important, dispose of the image  – otherwise the image file will be locked by the server for several minutes
            fullSizeImg.Dispose()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Function ThumbnailCallback() As Boolean
        Return False
    End Function
    Public Function GetStudentImagePath(ByVal strStudentId As String) As String
        Dim facade As New DocumentManagementFacade
        Dim strDocumentType As String = facade.GetLeadPhotoDocumentType(StudentId, "Photo", "StudentId")
        Dim strPath As String = CType(MyAdvAppSettings.AppSettings("StudentImagePath"), String) 'SingletonAppSettings.AppSettings("DocumentPath")
        Dim strFileNameOnly As String = facade.GetStudentPhotoFileName(strStudentId, "Photo")
        Dim strFullPath As String = strPath + strDocumentType + "/" + strFileNameOnly
        'Dim strPath As String = SingletonAppSettings.AppSettings("DocumentPath")
        'Dim strFileNameOnly As String = facade.GetStudentPhotoFileName(strStudentId, "PhotoId")
        Return strFullPath

    End Function

    Protected Sub txtDOB_SelectedDateChanged(ByVal sender As Object, ByVal e As SelectedDateChangedEventArgs) Handles txtDOB.SelectedDateChanged
        If IsDate(txtDOB.DbSelectedDate) Then
            If DateDiff(DateInterval.Year, CType(txtDOB.DbSelectedDate, Date), CDate(txtDate.Text)) < 1 Then
                DisplayErrorMessage("The minimum age requirement for student is 18 ")
                txtAge.Text = ""
                Exit Sub
            End If

            Dim intAge As Integer
            'intAge = Now.Year - Year(CType(txtDOB, RadDatePicker).DbSelectedDate) - 1
            '2/13/2013
            Dim dob As Date = CType(txtDOB.SelectedDate, Date)
            'dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If
            'If intAge < 18 Then
            '    DisplayErrorMessage("The minimum age requirement for student is 18 ")
            '    txtAge.Text = ""
            '    Exit Sub
            'End If
            txtAge.Text = CType(intAge, String) 'Math.Floor(DateDiff(DateInterval.Day, CDate(txtDOB.Text), Now) / 365)
        Else
            txtAge.Text = ""
        End If
    End Sub

    'US3434 - SSN field on the Student Master page to visible to only certain security roles 
    Protected Sub HideSSNValue()
        txtSSN.Enabled = bDispSsNbyRoles
        If Not bDispSsNbyRoles Then
            If sHiddenSSNValue = "" Then
                txtSSN.Mask = ""
                txtSSN.DisplayMask = ""
            Else
                txtSSN.Mask = "aaaaaaaaaaa"
                txtSSN.DisplayMask = "aaaaaaaaaaa"
            End If
            txtSSN.Text = sHiddenSSNValue
            MaskedTextBoxRegularExpressionValidator.Enabled = False
            'SSNRequiredValidation()
        End If
    End Sub

    ''' <summary>
    ''' Set phone mask
    ''' </summary>
    ''' <param name="isInternational">true set international mask else national mask</param>
    ''' <remarks></remarks>
    Private Sub SetPhoneMask(isInternational As Boolean)
        Dim phone As String = txtPhone.Text
        If isInternational = False Then
            txtPhone.Mask = "(###)-###-####"
            txtPhone.DisplayMask = "(###)-###-####"
            txtPhone.DisplayPromptChar = ""
        Else
            txtPhone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtPhone.DisplayMask = ""
            txtPhone.DisplayPromptChar = ""
        End If
        txtPhone.Text = phone

    End Sub


  
    Public Sub ValidateLicensingExamDetails(source As Object, args As ServerValidateEventArgs)
        Dim cv As CustomValidator = CType(source, CustomValidator)
        Dim parent = cv.Parent
        Dim message = String.Empty
        If (parent IsNot Nothing) Then
            For Each control As Control In parent.Controls
                If TypeOf control Is RadDatePicker Then
                    Dim dp = CType(control, RadDatePicker)
                    Dim selectedDate = dp.SelectedDate
                    Dim isValid = dp.CssClass.Contains("required") AndAlso selectedDate.HasValue
                    Dim dpInput = dp.Controls.Cast(Of RadDateInput).Where(Function(x) TypeOf (x) Is RadDateInput).FirstOrDefault()
                    If (Not isValid) Then

                        If (Not dpInput Is Nothing) Then
                            dpInput.BackColor = Color.Yellow
                        End If
                        cv.IsValid = isValid
                    Else
                        dpInput.BackColor = Color.White
                    End If


                End If
                'Return
            Next
        End If

        args.IsValid = cv.IsValid
    End Sub

End Class
