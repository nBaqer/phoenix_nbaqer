<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="Skills.aspx.vb" Inherits="Skills" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>Skills</title>
   <%-- <link rel="stylesheet" type="text/css" href="../CSS/localhost.css" />--%>
    <link  rel="stylesheet"  type="text/css" href="../css/localhost_lowercase.css"/>
 </asp:Content>
<asp:content id="content2" contentplaceholderid="contentmain1" runat="server">
</asp:content>
<asp:content id="content3" contentplaceholderid="contenterror" runat="server">
</asp:content>
<asp:content id="content4" contentplaceholderid="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>            
           <telerik:AjaxSetting AjaxControlID="pnlRHS">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstSkills" />
                    <telerik:AjaxUpdatedControl ControlID="radstatus" />
                </UpdatedControls>
            </telerik:AjaxSetting>    
            <telerik:AjaxSetting AjaxControlID="dlstSkills">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>    
            <telerik:AjaxSetting AjaxControlID="radstatus">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstSkills" />
                    <telerik:AjaxUpdatedControl ControlID="radstatus" />
                </UpdatedControls>
            </telerik:AjaxSetting>     
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow:auto;">
       <telerik:RadSplitter ID="RadSplitter1" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
              BorderWidth="0px">
            <telerik:RadPane id="oldmenupane" runat="server" BackColor="#FAFAFA" width="350" scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                <td width="10%" nowrap align="left"><asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                <td width="85%" nowrap>
                                <asp:radiobuttonlist id="radstatus" cssclass="radiobutton" autopostback="true" runat="server" repeatdirection="horizontal">
                                <asp:listitem text="Active" selected="true" />
                                <asp:listitem text="Inactive" />
                                <asp:listitem text="All" />
                                </asp:radiobuttonlist>
                                </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:datalist id="dlstSkills" runat="server" datakeyfield="SkillId" width="100%">
                                    <selecteditemstyle cssclass="selecteditemstyle"></selecteditemstyle>
                                    <itemstyle cssclass="itemstyle"></itemstyle>
                                    <itemtemplate>
                                        <asp:imagebutton id="imginactive" imageurl="../images/inactive.gif" runat="server" visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' causesvalidation="false">
                                        </asp:imagebutton>
                                        <asp:imagebutton id="imgactive" imageurl="../images/active.gif" runat="server" visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' causesvalidation="false">
                                        </asp:imagebutton>
                                        <asp:label id="lblid" runat="server" visible="false" text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="Linkbutton1" text='<%# Container.DataItem("SkillDescrip")%>' Runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("SkillId")%>'  CausesValidation="False" />
                                    </itemtemplate>
                                </asp:datalist>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane id="Radpane1" runat="server" borderwidth="0px" scrolling="both" orientation="horizontaltop">
                <asp:panel id="PnlRHS" Runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                                <td class="menuframe" align="right">
                                        <asp:button id="btnsave" runat="server" cssclass="save" text="Save"></asp:button>
                                        <asp:button id="btnnew" runat="server" cssclass="new" text="New" causesvalidation="false"></asp:button>
                                        <asp:button id="btndelete" runat="server" cssclass="delete" text="Delete" causesvalidation="false"></asp:button>
                                 </td>
                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
						    <td class="detailsframe">
							    <div class="scrollright2">
								    <!--begin content table-->
                                	<TABLE class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                        <TR>
											<TD class="twocolumnlabelcell" noWrap>
												<asp:label id="lblSkillCode" Runat="server" CssClass="Label"></asp:label></TD>
											<TD class="twocolumncontentcell">
												<asp:textbox id="txtSkillCode" Runat="server"  MaxLength="128" Width="150px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="twocolumnlabelcell" noWrap>
												<asp:label id="lblStatusId" Runat="server" CssClass="Label"></asp:label></TD>
											<TD class="twocolumncontentcell">
												<asp:dropdownlist id="ddlStatusId" runat="server" ></asp:dropdownlist></TD>
										</TR>
										<TR>
											<TD class="twocolumnlabelcell" noWrap>
												<asp:label id="lblSkillDescrip" Runat="server" CssClass="Label"></asp:label></TD>
											<TD class="twocolumncontentcell">
												<asp:textbox id="txtSkillDescrip" Runat="server"  MaxLength="128" Width="250px"></asp:textbox></TD>
										</TR>
										<TR>
											<TD class="twocolumnlabelcell" noWrap>
												<asp:label id="lblSkillGrpId" Runat="server" CssClass="Label"></asp:label></TD>
											<TD class="twocolumncontentcell">
												<asp:dropdownlist id="ddlSkillGrpId" runat="server"></asp:dropdownlist></TD>
										</TR>
										<TR>
											<TD class="twocolumnlabelcell" noWrap>
												<asp:label id="lblCampGrpId" Runat="server" CssClass="Label"></asp:label></TD>
											<TD class="twocolumncontentcell">
												<asp:dropdownlist id="ddlCampGrpId" runat="server" ></asp:dropdownlist></TD>
										</TR>
									</TABLE>
								    <!--end content table-->
								    <asp:textbox id="txtRowIds" style="VISIBILITY: hidden" runat="server" CssClass="Label" width="10%"></asp:textbox>
								    <asp:textbox id="txtResourceId" style="VISIBILITY: hidden" runat="server" CssClass="Label"
									    Width="10%"></asp:textbox>
							    </div>
						    </td>
					    </tr>
                    </table> 
                </asp:panel>            
            </telerik:RadPane> 
       </telerik:RadSplitter> 
       <asp:panel id="pnlRequiredFieldValidators" runat="server" Width="50px">
       </asp:panel>
       <asp:validationsummary id="ValidationSummary1" runat="server" ShowMessageBox="True" ShowSummary="False"></asp:validationsummary>
       <asp:textbox id="txtSkillId" CssClass="Label" Runat="server" MaxLength="128" Width="180px" Visible="false"></asp:textbox>
    </div> 
</asp:content>
