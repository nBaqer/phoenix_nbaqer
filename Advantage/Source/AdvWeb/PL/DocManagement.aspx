<%@ Page Language="vb" AutoEventWireup="false" Inherits="DocManagement" CodeFile="DocManagement.aspx.vb" %>

<%@ Register Src="~/usercontrols/SearchControls/DocumentTypeSearchControl.ascx" TagPrefix="FAME" TagName="DocumentTypeSearchControl" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Document Management</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../css/systememail.css" type="text/css" rel="stylesheet" />
    <%--<link href="../css/localhost_lowercase.css" type="text/css" rel="stylesheet" />--%>
    <%--<script language="javascript" src="ViewDocument.js"></script>
		<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        function closewindow() {
            //				typeof(theChild);
            //				 if (typeof(theChild) != "undefined"){
            //					if (theChild.open && !theChild.closed){
            //						theChild.close();
            //					}
            //				}
        }
        function pendingdocsbystatus() {
            var campusid = "<%=campusId%>";
            var features = "toolbar=no, status=no, resizable=yes,width=600px,height=370px";
            var winname = "pendingdocs";
            var studentid = "<%=ddlStudentId.selectedvalue%>";
            var studentname = "<%=ddlStudentId.selecteditem.text%>";
            if (studentid.length <= 0) {
                alert("please select student from the list");
                return (false);
            }
            var url = "../AD/ViewPendingDocsByStudent.aspx?studentid=" + studentid + "&studentname=" + studentname + "&campusid=" + campusid;
            window.open(url, winname, features);
        }
    </script>
</head>
<body runat="server" leftmargin="0" topmargin="0">
    <form id="Form1" method="post" runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-image: url('../images/PageHeader3.png'); height:auto; vertical-align: top;">
        <tr>
            <td>
                <asp:Label ID="lblHeader" runat="server" Text="Document Management - Upload and View Documents"
                    Style="font-family: Verdana; font-size: 16px; color: White; font-weight: bolder;
                    padding-left: 10px;"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellspacing="0" cellpadding="0" width="100%" border="0" style="height: 640px;">
        <tr>
            <!-- begin leftcolumn -->
            <td class="listframeleftdocmgt">

                <table cellspacing="0" cellpadding="0" width="100%" border="0" >
                    <tr>
                        <td class="listframedocmgt">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="docmgmtsearch1" nowrap>
                                        <asp:Label ID="Label1" runat="server" CssClass="Label">Document Module</asp:Label>
                                    </td>
                                    <td class="docmgmtsearch21">
                                        <asp:DropDownList ID="ddlDocumentModule" runat="server" width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="docmgmtsearch1" nowrap>
                                        <asp:Label ID="lblDocumentStatus" runat="server" CssClass="Label">Document Status</asp:Label>
                                    </td>
                                    <td class="docmgmtsearch21">
                                        <asp:DropDownList ID="ddlDocFilterStatus" runat="server" width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="docmgmtsearch1" nowrap>
                                        <asp:Label ID="lblStudentFilter" runat="server" CssClass="Label">Student</asp:Label>
                                    </td>
                                    <td class="docmgmtsearch21">
                                        <asp:DropDownList ID="ddlStudentFilter" runat="server" width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="docmgmtsearch1" nowrap>
                                        <asp:Label ID="lblreqFor" runat="server" CssClass="Label">Required For</asp:Label>
                                    </td>
                                    <td class="docmgmtsearch21">
                                        <asp:DropDownList ID="ddltypeofreq"  runat="server" width="200px">
                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                            <asp:ListItem Value="1">Req for Enrollment</asp:ListItem>
                                            <asp:ListItem Value="2">Req for Financial Aid</asp:ListItem>
                                            <asp:ListItem Value="3">Req for Graduation</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="docmgmtsearch1" nowrap>
                                        <asp:Label ID="lblType" runat="server" CssClass="Label">Type</asp:Label>
                                    </td>
                                    <td class="docmgmtsearch21">
                                        <FAME:DocumentTypeSearchControl runat="server" ID="ddlDocTypeSearchControl" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="docmgmtsearch1" nowrap>
                                    </td>
                                    <td class="docmgmtsearch21" style="text-align: center">
                                        <asp:Button ID="btnApplyFilter" runat="server" CssClass="buttonTopFilter" Text="Apply Filter"
                                            CausesValidation="False"></asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom" nowrap >
                           <div  class="scrollleftdocmanage1">
                                <asp:DataList ID="dlstDocumentStatus" runat="server" Width="100%" >
                                    <SelectedItemStyle CssClass="SelectedItem" Wrap ="true"></SelectedItemStyle>
                                    <SelectedItemTemplate>
                                    </SelectedItemTemplate>
                                    <ItemStyle CssClass="NonSelectedItem" Wrap="true" ></ItemStyle>
                                    <ItemTemplate>
                                        <asp:LinkButton Text='<%# Container.DataItem("Descrip") %>' runat="server" CssClass="NonSelectedItem"
                                            CommandArgument='<%# Container.DataItem("StudentDocId")%>' ID="Linkbutton2" CausesValidation="False" />
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>                           
                        </td>
                    </tr>
                </table>
            </td>
            <!-- end leftcolumn -->
            <!-- begin rightcolumn -->
            <td class="leftsidedocmgt">
                <table cellspacing="0" cellpadding="0" width="9" border="0">
                </table>
            </td>

            <td class="detailsframerightdocmgt">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="MenuFrame" align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="true">
                            </asp:Button><asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" Enabled="true">
                            </asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete"
                                CausesValidation="true" Enabled="False"></asp:Button>
                        </td>
                    </tr>
                </table>
                <!-- end top menu (save,new,reset,delete,history)-->
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="DetailsFrame">
                            <div class="scrollwholedocs1">
                                <!--begin content table-->
                                <table class="contenttable" border="0" cellspacing="0" cellpadding="0" width="90%" align="center">
                                    <asp:TextBox ID="txtStEmploymentId" runat="server" Visible="false"></asp:TextBox>
                                    <asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                                    <asp:CheckBox ID="chkScannedId" runat="server" AutoPostBack="True" Visible="False">
                                    </asp:CheckBox>
                                    <asp:Label ID="lblScanned" runat="server" Visible="False"></asp:Label>
                                    <asp:LinkButton ID="lnkViewDoc" runat="server" Visible="False"></asp:LinkButton>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblStudentId" runat="server" CssClass="Label">Student</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlStudentId" runat="server" AutoPostBack="true" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell" >
                                            <asp:Label ID="Label2" runat="server" CssClass="Label">Module<font color="red">*</font></asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell" >
                                            <asp:DropDownList ID="ddlModulesId" runat="server" AutoPostBack="True" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblDocumentId" runat="server" CssClass="Label">Document</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlDocumentId" runat="server" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblDocStatusId" runat="server" CssClass="Label">Document Status</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlDocStatusId" runat="server" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell" >
                                          &nbsp;
                                        </td>
                                        <td class="twocolumncontentcell" >
                                            <asp:CheckBox ID="chkOverride" runat="server" CssClass="Label" Text="Override" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblRequestDate" runat="server" CssClass="Label">Requested Date</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell" >
                                           <%-- <asp:TextBox ID="txtRequestDate" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                                            <a onclick="javascript:OpenCalendar('ClsSect','txtRequestDate', true, 1945)">
                                                <img id="IMG1" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                                    align="absMiddle"></a>--%>
                                            <telerik:RadDatePicker ID="txtRequestDate" MinDate="1/1/1945" runat="server">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblReceiveDate" runat="server" CssClass="Label">Received Date</asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell" >
                                          <%--  <asp:TextBox ID="txtReceiveDate" runat="server" CssClass="TextBoxDate"></asp:TextBox>
                                            <a onclick="javascript:OpenCalendar('ClsSect','txtReceiveDate', true, 1945)">
                                                <img id="Img2" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                                    align="absMiddle"></a>--%>
                                            <telerik:RadDatePicker ID="txtReceiveDate" MinDate="1/1/1945" runat="server">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            &nbsp;
                                        </td>
                                        <td class="twocolumncontentcell" >
                                            <a href="#" onclick="pendingdocsbystatus()" class="Label">View documents by status</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label3" runat="server" class="LabelBold" Text="Upload Documents"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <p>
                                </p>
                                <asp:Panel ID="pnlUpload" runat="server" Visible="false">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                        <tr>
                                            <td class="spacertables">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:Label ID="lblHeading" CssClass="LabelBold" runat="server">Link to an electronic document:</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblFileName" CssClass="Label" runat="server">File</asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <input class="TextBox" id="txtUpLoad" type="file" name="txtUpLoad" runat="server"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                            </td>
                                            <td class="twocolumncontentcell" nowrap>
                                                <asp:Button ID="btnUpload" runat="server" Text="Upload Document" >
                                                </asp:Button>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                            </td>
                                            <td class="twocolumncontentcell" nowrap>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlViewDoc" runat="server" Visible="False">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="90%" align="center">
                                        <tr>
                                            <td >
                                                <asp:Label ID="lblgeneral" runat="server" CssClass="Label" Font-Bold="true">View Documents</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables">
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <telerik:RadSkinManager ID="RadSkinManager1" runat="server" >
                                </telerik:RadSkinManager>
                                <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
                                    <script type="text/javascript">
                                        var uploadedFilesCount = 0;
                                        var isEditMode;
                                        function validateRadUpload(source, e) {
                                            debugger;
                                            if (isEditMode == null || isEditMode == undefined) {
                                                e.IsValid = false;

                                                if (uploadedFilesCount > 0) {
                                                    e.IsValid = true;
                                                }
                                            }
                                            isEditMode = null;
                                        }

                                        function OnClientFileUploaded(sender, eventArgs) {
                                            uploadedFilesCount++;
                                        }

                                        function openFileBrowser(fileurl) {
                                            alert(fileurl);
                                            //   var fileurl = e.CommandArgument;
                                            window.open('../FileBrowser.aspx?fileurl=' + fileurl, '_blank', 'HistWin', 'width=700,height=600,resizable=yes');
                                        }
            
            
                                    </script>
                                </telerik:RadCodeBlock>
                                <div id="OldStudentDocGrid" visible="false" runat="server">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="90%" align="center">
                                        <tr>
                                            <td>
                                                <asp:DataGrid ID="dgrdDocs" runat="server" Width="100%" align="center" GridLines="Horizontal"
                                                    EditItemStyle-Wrap="false" HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False"
                                                    BorderStyle="Solid" CellPadding="0">
                                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                    <HeaderStyle CssClass="DataGridHeader"></HeaderStyle>
                                                    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="FileName" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                            <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="Linkbutton1" runat="server" Text='<%# Container.DataItem("DisplayName")%>'
                                                                    CssClass="Label" CausesValidation="False" CommandArgument='<%# Container.DataItem("FileName") & Container.DataItem("FileExtension")%>'
                                                                    CommandName="StudentSearch">
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="DocumentType" HeaderStyle-HorizontalAlign="left"
                                                            ItemStyle-HorizontalAlign="Left">
                                                            <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocumentType" Text='<%# Container.DataItem("DocumentCategory") %>'
                                                                    CssClass="Label" runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                            <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                            <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnDeleteFile" Text="Delete"  runat="server" CommandArgument='<%# Container.DataItem("FileName")%>'
                                                                    CommandName="DeleteFile" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid><asp:Panel ID="pnlRequiredFieldValidators" runat="server" Visible="False">
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Visible="False" ShowMessageBox="True"
                                                        ShowSummary="False"></asp:ValidationSummary>
                                                    <asp:CustomValidator ID="CustomValidator1" runat="server" Visible="False"></asp:CustomValidator>
                                                </asp:Panel>
                                                <asp:TextBox ID="txtStudentDocId" runat="server" Visible="False"></asp:TextBox><asp:TextBox
                                                    ID="txtDocumentName" runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtStudentName"
                                                        runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtPath" runat="server"
                                                            Visible="False"></asp:TextBox><asp:TextBox ID="txtDocumentType" runat="server" Visible="False"></asp:TextBox><asp:TextBox
                                                                ID="txtExtension" runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtStudentId"
                                                                    runat="server" Visible="False"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                                    <Scripts>
                                        <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.Core.js" />
                                        <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQuery.js" />
                                        <asp:ScriptReference Assembly="Telerik.Web.UI" Name="Telerik.Web.UI.Common.jQueryInclude.js" />
                                    </Scripts>
                                </telerik:RadScriptManager>
                                <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
                                    <AjaxSettings>
                                        <telerik:AjaxSetting AjaxControlID="RadGrid1">
                                            <UpdatedControls>
                                                <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                                            </UpdatedControls>
                                        </telerik:AjaxSetting>
                                    </AjaxSettings>
                                </telerik:RadAjaxManager>
                                <div style="padding-left:20px">
                                    <telerik:RadGrid runat="server" ID="RadGrid1" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" Width="90%" ShowStatusBar="True" GridLines="None"
                                        PageSize="5" OnInsertCommand="RadGrid1_InsertCommand" OnDeleteCommand="RadGrid1_DeleteCommand"
                                        OnUpdateCommand="RadGrid1_UpdateCommand">
                                        <PagerStyle Mode="NumericPages" AlwaysVisible="true" />
                                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="FileID" InsertItemPageIndexAction="ShowItemOnFirstPage">
                                            <CommandItemTemplate>
                                                <div style="height: 20px;">
                                                    <div style="float: left; vertical-align: top;">
                                                        <asp:Button runat="server" ID="btn1" CommandName="InitInsert" CssClass="rgAdd" Text=" " />
                                                        <asp:LinkButton runat="server" ID="linkbuttionInitInsert" CommandName="InitInsert"
                                                            Text="Add New Record"></asp:LinkButton>
                                                    </div>                                                   
                                                </div>
                                            </CommandItemTemplate>
                                            <Columns>
                                                <telerik:GridEditCommandColumn ButtonType="ImageButton" EditText="Replace">
                                                    <HeaderStyle Width="3%" />
                                                </telerik:GridEditCommandColumn>
                                                <telerik:GridTemplateColumn HeaderText="Document Name" EditFormHeaderTextFormat="Document to upload"
                                                    UniqueName="DisplayName" SortExpression="DisplayName" >
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblName" Text='<%# Eval("DisplayName") & Eval("FileExtension") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderStyle Width="15%" />
                                                    
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn DataField="Data" HeaderText="" EditFormHeaderTextFormat=""
                                                    UniqueName="Upload">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="Linkbutton1" runat="server" Text='click here to view the document'
                                                            CssClass="Label" CausesValidation="False" CommandArgument='<%# Eval("FileUrl") %>'>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <telerik:RadAsyncUpload runat="server" ID="AsyncUpload1" OnClientFileUploaded="OnClientFileUploaded" DisablePlugins="true"
                                                            AllowedFileExtensions="jpg,jpeg,png,gif,pdf,txt,doc,docx,xls,xlsx" Width="20%">
                                                        </telerik:RadAsyncUpload>
                                                    </EditItemTemplate>   
                                                    <HeaderStyle Width="35%" />                                                
                                                </telerik:GridTemplateColumn>
                                              <%--  <telerik:GridButtonColumn Text="Delete" CommandName="Delete" ButtonType="ImageButton">
                                                    <HeaderStyle Width="2%" />
                                                </telerik:GridButtonColumn>--%>
                                                <telerik:GridTemplateColumn DataField="User" HeaderText="User" EditFormHeaderTextFormat="" UniqueName="User">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblUser" Text='<%# Eval("modUser") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderStyle Width="15%" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn DataField="Date" HeaderText="Date Uploaded" EditFormHeaderTextFormat="" UniqueName="UpLoadedDate">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblDate" Text='<%# Eval("modDate") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderStyle Width="15%" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" ConfirmDialogType="Classic"
                                                    ConfirmText="Delete this item?" ConfirmTitle="Delete" Text="Delete" UniqueName="DeleteColumn"
                                                    HeaderStyle-Width="2%">
                                                    <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" />
                                                </telerik:GridButtonColumn>
                                            </Columns>
                                            <EditFormSettings>
                                                <EditColumn ButtonType="ImageButton" />
                                            </EditFormSettings>
                                            <PagerStyle AlwaysVisible="True"  />
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </div>
                            </div>
                            <!--<table width="60%" align="center">
											<TR>
												<TD class="contentcell" nowrap><asp:Button id="btnGetDoc" Runat="server" Text="   Liberty   "  Visible="true"></asp:Button></TD>
												<TD class="contentcell4" nowrap><asp:Button id="btnSchoolDocs" Runat="server" Text="SchoolDocs"  Visible="true"></asp:Button></TD>
											</TR>
										</table>-->
                           <%-- </div>--%>
                        </td>
                    </tr>
                </table>
            </td>
            <!-- end rightcolumn -->
        </tr>
    </table>
    <div id="footer" runat="server">
        <asp:Label ID="lblFooterText" runat="server"></asp:Label>
    </div>
    </form>
</body>
</html>
