
<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master" Inherits="DynamicResume" CodeFile="DynamicResume.aspx.vb" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <link href="../CSS/resume.css" type="text/css" rel="stylesheet" media="screen">
   <link href="../CSS/print.css" type="text/css" rel="stylesheet" media="print">
  
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
   
        <!-- end top menu (save,new,reset,delete,history)-->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable">
            <tr>
                <td class="detailsframe">
                    <div class="scrollpopupResume">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="resumecontainer">
                            <tr>
                                <td>
                                    <asp:DataList ID="dlstStudentAddress" HorizontalAlign="Center" Width="90%" runat="server">
                                        <ItemTemplate>
                                            <h1>
                                                <%# Container.DataItem( "firstname" ) & "  " & Container.DataItem( "lastname" )%>
                                            </h1>
                                            <ul class="resumeheader">
                                                <li>
                                                    <%# Container.DataItem( "Address" )%>
                                                </li>
                                                <li>
                                                    <%# Container.DataItem( "city" )%>
                                                    ,<%# Container.DataItem( "state" )%><%# Container.DataItem( "zip" )%></li>
                                                <li>
                                                    <%# Container.DataItem( "Phone" )%>
                                                </li>
                                                <li>
                                                    <%# Container.DataItem( "HomeEmail" )%>
                                                </li>
                                            </ul>
                                        </ItemTemplate>
                                    </asp:DataList>
                                    <h2>
                                        Objective</h2>
                                    <ul>
                                        <li>
                                            <asp:Label ID="lblobj" runat="server"></asp:Label>
                                        </li>
                                    </ul>
                                    <h2>
                                        Experience</h2>
                                    <ul>
                                        <li>
                                            <asp:Label ID="lblExperience" runat="server"></asp:Label>
                                        </li>
                                    </ul>
                                    <br>
                                    <h2>
                                        Education</h2>
                                    <ul>
                                        <li>
                                            <asp:DataList ID="dlstCollegeHistory" Width="60%" runat="server">
                                                <ItemStyle Font-Size="Small" Font-Names="verdana,verdana" Font-Bold="True"></ItemStyle>
                                                <ItemTemplate>
                                                    <table border="0" width="100%">
                                                        <tr>
                                                            <td nowrap width="92%" align="left">
                                                                <font face="verdana" size="2"><b>
                                                                    <%# StrConv(Container.DataItem( "CollegeName" ),VbStrConv.UpperCase)%>
                                                                </b></font>
                                                                </font>
                                                        </tr>
                                                    </table>
                                                    <table border="0" width="100%">
                                                        <tr>
                                                            <td nowrap width="35%" align="left">
                                                                <font face="verdana" size="1">
                                                                    <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"GraduatedDate", "{0:d}") %>'
                                                                        runat="server"> </asp:Label>
                                                                </font>
                                                            </td>
                                                            <td nowrap width="35%" align="left">
                                                                <font face="verdana" size="1">
                                                                    <%# Container.DataItem( "DegreeDescrip" ) & " " & Container.DataItem( "Major" ) %>
                                                                </font>
                                                            </td>
                                                        </tr>
                                                        <tr height="2">
                                                            <td>
                                                                &nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </li>
                                        <h2>
                                            Skills</h2>
                                        <ul>
                                            <li>
                                                <asp:Label ID="lblOutput" runat="server"></asp:Label>
                                            </li>
                                        </ul>
                                        <h2>
                                            Extracurriculars</h2>
                                        <ul>
                                            <li>
                                                <asp:Label ID="lblExtracurroutput" runat="server"></asp:Label>
                                            </li>
                                        </ul>
                                        </ul>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <!-- end rightcolumn -->
        <asp:TextBox ID="txtStudentId" runat="server" Visible="False"></asp:TextBox>
        <!-- end footer -->
   </asp:Content>
