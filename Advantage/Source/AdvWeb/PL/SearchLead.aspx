﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SearchLead.aspx.vb" Inherits="SearchLead" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
     
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">

    
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
     <%-- Add class ListFrameTop2 to the table below --%>
        <table cellspacing="0" class="listframetop2" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="listframetop">
                    Search Lead
                </td>
            </tr>
            <tr>
                <td><!--class="listframebottom" -->
                    <div><!--class="scrollleft" -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="listframebottom">
                                    <div class="scrollleft" >
                                        <table cellspacing="0" cellpadding="2" align="center" width="100%">
                                            <tr>
                                                <td class="employersearch">
                                                    <asp:Label ID="lblLastName" runat="Server" CssClass="label">Last Name</asp:Label>
                                                </td>
                                                <td class="employersearch2">
                                                    <asp:TextBox ID="txtLastName" Width="200px" runat="Server" CssClass="textbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="employersearch">
                                                    <asp:Label ID="lblFirstName" runat="Server" CssClass="label">First Name</asp:Label>
                                                </td>
                                                <td class="employersearch2">
                                                    <asp:TextBox ID="txtFirstName" Width="200px" runat="Server" CssClass="textbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="employersearch">
                                                <%--    <asp:Label ID="lblSSN" runat="Server" CssClass="label" Visible="true">SSN<font runat="server" visible="false" id="ssnreqasterisk" color="red">*</font></asp:Label>--%>
                                             <asp:Label ID="lblSSN" runat="Server" CssClass="label" Visible="true">SSN</asp:Label>
                                                </td>
                                                <td class="employersearch2">
                                                    <telerik:RadMaskedTextBox ID="txtSSN" runat="server" CssClass="textbox"
                                                        Width="200px" DisplayFormatPosition="Right" Mask="#########" DisplayMask="###-##-####"
                                                        DisplayPromptChar="">
                                                    </telerik:RadMaskedTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="employersearch">
                                                    <asp:Label ID="lblPhone" runat="Server" CssClass="label">Phone</asp:Label>
                                                </td>
                                                <td class="employersearch2">
                                                    <asp:TextBox ID="txtPhone" Width="200px" runat="Server" CssClass="textbox"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="employersearch">
                                                    <asp:Label ID="lblStatus" runat="Server" CssClass="label">Status</asp:Label>
                                                </td>
                                                <td class="employersearch2">
                                                    <asp:DropDownList ID="ddlStatusId" runat="Server" Width="220px" CssClass="dropdownlist">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="employersearch">
                                                    <asp:Label ID="lblDOB" runat="Server" CssClass="label" Visible="False">Date Of Birth</asp:Label>
                                                </td>
                                                <td class="employersearch2">
                                                    <asp:DropDownList ID="ddlDOB" runat="Server" Width="220px" CssClass="dropdownlist" Visible="False">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="employersearch">
                                                    <asp:Label ID="lblCampusID" runat="Server" CssClass="label">Campus</asp:Label>
                                                </td>
                                                <td class="employersearch2">
                                                    <asp:DropDownList ID="ddlCampusID" Width="220px" runat="Server" CssClass="dropdownlist" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="employersearch">
                                                    <asp:Label ID="lblAdmissionRepID" runat="Server" CssClass="label">Admission Rep</asp:Label>
                                                </td>
                                                <td class="employersearch2">
                                                    <asp:DropDownList ID="ddlAdmissionRepID" Width="220px" runat="Server" CssClass="dropdownlist">
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="txtLeadId" runat="server" Visible="false"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="employersearch">
                                                </td>
                                                <td class="employersearch2">
                                                    <table cellspacing="0" cellpadding="2" align="center" width="100%">
                                                        <tr>
                                                            <td width="50%">
                                                                <%--<asp:Button ID="btnSearch" runat="Server" Text="Search" CssClass="button"></asp:Button>--%>
                                                                 <telerik:RadButton ID="btnSearch" runat="server" Text="Search"></telerik:RadButton>
                                                                 &nbsp;
                                                                   <telerik:RadButton ID="btnReset" runat="server" Text="Reset"></telerik:RadButton>
                                                            </td>
                                                            <td width="50%">
                                                                <%--<input id="Reset1" type="reset" value="Reset" name="Reset1" runat="server" class="button" />--%>
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>

    </telerik:RadPane>


    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="false"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False" Enabled="false"></asp:Button><asp:Button
                                        ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False" Enabled="false">
                                    </asp:Button>
                                    </td>
                        </tr>
                    </table>
        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="detailsframe">
                    <div class="scrollright2">
                        <!-- begin content table-->
                        <table width="98%" align="center" border="0">
                            <tr valign="top">
                                <td>
                                    <asp:DataGrid ID="dgrdTransactionSearch" DataKeyField="LeadId" CellPadding="0" BorderWidth="1px"
                                        BorderStyle="Solid" BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True"
                                        HeaderStyle-Wrap="true" EditItemStyle-Wrap="false" GridLines="Horizontal" Width="100%"
                                        runat="server">
                                        <EditItemStyle Wrap="False"></EditItemStyle>
                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Last Name">
                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Linkbutton1" runat="server" Text='<%# Container.DataItem("LastName") %>'
                                                        CausesValidation="False" CommandArgument='<%# Container.DataItem("LeadId") %>'
                                                        CommandName="LeadSearch">
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="First Name">
                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDgFirstName" Text='<%# Container.DataItem("FirstName") %>' CssClass="label"
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="SSN">
                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="label1" Text='<%# Container.DataItem("SSN") %>' CssClass="label" runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="DOB">
                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="label2" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"BirthDate", "{0:d}")  %>'
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Admission Rep">
                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAdmissionRep" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"AdmissionRep")  %>'
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Campus">
                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCampus" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"Campus")  %>'
                                                        runat="server"></asp:Label>
                                                    <asp:Label ID="lblCampusId" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"CampusId")  %>'
                                                        runat="server" Visible="false">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <!-- end content table-->
        <asp:textbox id="txtStudentDocId" Runat="server" Visible="False"></asp:textbox>
        
                      <telerik:RadNotification runat="server" ID="RadNotification1" 
                Text="This is a test" ShowCloseButton="true" 
                Width="400px" Height="125px"
                TitleIcon="" 
                Position="Center" Title="Message" 
                EnableRoundedCorners="true" 
                EnableShadow="true" 
                Animation="Fade" 
                AnimationDuration="1000" 
                   
                style="padding-left:120px; padding-top:5px; word-spacing:2pt;"> 
    </telerik:RadNotification>  
    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

