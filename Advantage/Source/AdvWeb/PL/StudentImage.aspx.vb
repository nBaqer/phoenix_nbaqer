Imports System.Drawing.Imaging
Imports system.drawing
Imports System.Drawing.Drawing2D
Partial Class PL_StudentImage
    Inherits System.Web.UI.Page
    'Function ThumbnailCallback() As Boolean
    '    Return False
    'End Function
    'Public Function GetStudentImagePath(ByVal strStudentId As String, ByVal Entity As String) As String
    '    Dim facade As New DocumentManagementFacade
    '    Dim strDocumentType As String = "PhotoId"
    '    Dim strPath As String = SingletonAppSettings.AppSettings("DocumentPath")
    '    Dim strFileNameOnly As String
    '    If Entity = "Lead" Then
    '        strFileNameOnly = facade.GetLeadPhotoFileName(strStudentId, "PhotoId")
    '    Else
    '        strFileNameOnly = facade.GetStudentPhotoFileName(strStudentId, "PhotoId")
    '    End If
    '    Dim strFullPath As String = strPath + strDocumentType + "\" + strFileNameOnly
    '    Return strFullPath
    'End Function

    'Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs)
    '    'Get the image name � yourimage.jpg � from the query String
    '    Dim imageURL As String = GetStudentImagePath(Request.QueryString("id"), Request.QueryString("Ent"))
    '    Response.Write(imageURL)
    '    'Dim Name As String = Request.QueryString("Name")
    '    'Dim imageHeight As Integer


    '    ''Set the thumbnail width in px � the width will be calculated later to keep the original ratio.

    '    'Dim imageWidth As Integer = 500 '300
    '    'Dim CurrentimgHeight As Integer
    '    'Dim CurrentimgWidth As Integer

    '    'Dim fullSizeImg As System.Drawing.Image
    '    'Try
    '    '    fullSizeImg = System.Drawing.Image.FromFile(imageURL)
    '    '    CurrentimgHeight = fullSizeImg.Height + 70
    '    '    CurrentimgWidth = fullSizeImg.Width + 100
    '    '    imageHeight = CurrentimgHeight / CurrentimgWidth * imageWidth


    '    '    'This will only work for jpeg images

    '    '    Response.ContentType = "image/jpeg"
    '    '    If imageHeight > 0 And imageWidth > 0 Then
    '    '        Dim dummyCallBack As System.Drawing.Image.GetThumbnailImageAbort
    '    '        dummyCallBack = New _
    '    '           System.Drawing.Image.GetThumbnailImageAbort(AddressOf ThumbnailCallback)



    '    '        Dim thumbNailImg As System.Drawing.Image
    '    '        thumbNailImg = fullSizeImg.GetThumbnailImage(imageWidth, imageHeight, _
    '    '                                                     dummyCallBack, IntPtr.Zero)



    '    '        Dim g As System.Drawing.Graphics = System.Drawing.Graphics.FromImage(thumbNailImg)

    '    '        Dim StringSizeF As SizeF, DesiredWidth As Single, wmFont As Font, RequiredFontSize As Single, Ratio As Single
    '    '        Dim strWatermark = Name

    '    '        'Set the watermark font	 
    '    '        wmFont = New Font("Verdana", 8, FontStyle.Regular)
    '    '        DesiredWidth = imageWidth * 0.5

    '    '        'use the MeasureString method to position the watermark in the centre of the image
    '    '        StringSizeF = g.MeasureString(strWatermark, wmFont)
    '    '        Ratio = StringSizeF.Width / wmFont.SizeInPoints
    '    '        RequiredFontSize = DesiredWidth / Ratio
    '    '        wmFont = New Font("Verdana", RequiredFontSize, FontStyle.Bold)

    '    '        'Sets the interpolation mode for a high quality image
    '    '        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic
    '    '        g.DrawImage(fullSizeImg, 0, 0, imageWidth, imageHeight)
    '    '        g.SmoothingMode = SmoothingMode.HighQuality

    '    '        Dim letterBrush As SolidBrush = New SolidBrush(Color.FromArgb(50, 255, 255, 255))
    '    '        Dim shadowBrush As SolidBrush = New SolidBrush(Color.FromArgb(50, 0, 0, 0))

    '    '        'Enter the watermark text 
    '    '        g.DrawString(Name, wmFont, shadowBrush, 75, (imageHeight * 0.75) - 36)
    '    '        g.DrawString(Name, wmFont, letterBrush, 77, (imageHeight * 0.75) - 38)
    '    '        thumbNailImg.Save(Response.OutputStream, ImageFormat.Jpeg)
    '    '    Else
    '    '        fullSizeImg.Save(Response.OutputStream, ImageFormat.Jpeg)
    '    '    End If
    '    '    'Important, dispose of the image  � otherwise the image file will be locked by the server for several minutes
    '    '    fullSizeImg.Dispose()
    '    'Catch ex As System.Exception
     '    '	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    '	exTracker.TrackExceptionWrapper(ex)


    '    'End Try

    'End Sub
End Class
