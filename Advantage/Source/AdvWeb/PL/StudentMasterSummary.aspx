﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentMasterSummary.aspx.vb" Inherits="StudentMasterSummary" %>

<%@ Register Src="~/usercontrols/StudentInfoBar.ascx" TagPrefix="uc1" TagName="StudentInfoBar" %>


<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script type="text/javascript" src="../Scripts/Storage/storageCache.js"></script>
    <script type="text/javascript" src="../Scripts/common-util.js"></script>

    <script src="../Scripts/ElementQueries.min.js"></script>
    <script src="../Scripts/ResizeSensor.min.js"></script>

    <script src="../Scripts/Fame.Advantage.API.Client.js"></script>
    <link rel="stylesheet" type="text/css" href="../CSS/ImportLeads.css" />
    <style>
        #summaryBody {
            margin: 20px;
            border: none !important;
        }

        #redirectButtons {
            margin: 20px;
        }

        .summaryBlock {
            width: 30%;
            display: inline-block;
            margin: 5px;
            padding: 5px;
            vertical-align: top;
        }

        .labelWrapper {
            width: 30%;
            margin: 15px;
            display: inline-block;
            vertical-align: top;
        }

        .summaryLabelValueWrapper {
            display: inline-block;
            width: 40%;
            margin: 15px;
            vertical-align: top;
            border: 1px solid #ccc !important;
            padding: 0.3em;
            border-radius: 0.3em;
            min-height: 20px;
        }

        .summaryLabelValueWrapperNoBorder {
            display: inline-block;
            width: 25%;
            margin: 15px;
            vertical-align: top;
            border: none !important;
        }

        .summaryLabelValueWrapper > .summaryLabel {
            font-weight: 400;
        }

        .summaryLabel {
            font-size: 15px !Important;
        }

        .summaryButton {
            width: 12%;
            font-size: 15px;
            margin: 2px;
        }

        #attendanceSummary .summaryLabelValueWrapper > .summaryLabel {
            text-align: right;
            float: right;
        }

        a > .summaryLabel {
            text-decoration: underline;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

    <script type="text/javascript">
        $(document).ready(function () {
            var hdnHostName = $("#hdnHostName").val();
            var manageUser = new Api.ViewModels.Placement.StudentSummary();
            manageUser.initialize(hdnHostName);
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
    <%-- Get the Current Campus from the Server Side and put it into a hidden field that is accesible --%>
    <asp:HiddenField runat="server" ID="hdnHostName" ClientIDMode="Static" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Width="100%" Orientation="HorizontalTop" Scrolling="Both">
            <div class="flex-container" style="padding: 20px; width: 97%">
                <br />
                <h4>Student Summary</h4>
                <br />

                <div id="redirectButtons">
                    <div class="formRow">
                        <button id="revenueRedirect" type="button" class="k-button summaryButton">Revenue</button>
                        <button id="awardsRedirect" type="button" class="k-button summaryButton">Awards</button>
                        <button id="ledgerRedirect" type="button" class="k-button summaryButton">Ledger</button>
                        <button id="paymentPlanRedirect" type="button" class="k-button summaryButton">Payment Plan</button>
                        <button id="statusHistoryButton" type="button" class="k-button summaryButton">Status History</button>
                        <button id="examsRedirect" type="button" class="k-button summaryButton">Exams</button>
                        <button id="NotesRedirect" type="button" class="k-button summaryButton">Notes</button>
                        <button id="progressReportRedirect" type="button" class="k-button summaryButton">Progress Report</button>
                    </div>
                </div>

                <div id="summaryBody">
                    <div class="formRow">
                        <div id="programSummary" class="summaryBlock">
                            <h3>PROGRAM</h3>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Status:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="Status" class="summaryLabel" runat="server" data-bind="text: Status"></label>
                                    <div id="statusToolTipContainer" style="display: inline-block;"></div>
                                </div>
                                <div id="loaDates">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">LOA Start:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="LoaStartDate" class="summaryLabel" runat="server" data-bind="text: LoaStartDate"></label>
                                    
                                </div>
                               <div class="labelWrapper">
                                    <label class="summaryLabel">LOA End:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="LoaEndDate" class="summaryLabel" runat="server" data-bind="text: LoaEndDate"></label>
                                </div>
                              </div>

                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Start Date:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="StartDate" class="summaryLabel" runat="server" data-bind="text: StartDate"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Enrollment Date:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="EnrollmentDate" class="summaryLabel" runat="server" data-bind="text: EnrollmentDate"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Reenroll Date:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="ReenrollDate" class="summaryLabel" runat="server" data-bind="text: ReenrollDate"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Graduation Date:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="GraduationDate" class="summaryLabel" runat="server" data-bind="text: GraduationDate"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Revised Graduation Date:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="RevisedGraduationDate" class="summaryLabel" runat="server" data-bind="text: RevisedGraduationDate"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Badge ID:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="BadgeId" class="summaryLabel" runat="server" data-bind="text: BadgeId"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Overall GPA:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="OverallGPA" class="summaryLabel" runat="server" data-bind="text: OverallGPA"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Student Group(s):</label>
                                </div>
                                <div class="summaryLabelValueWrapperNoBorder">
                                    <select id="StudentGroup" class="summaryLabel" data-bind="source: StudentGroup"></select>
                                </div>
                            </div>
                        </div>
                        <div id="attendanceSummary" class="summaryBlock">
                            <h3>ATTENDANCE</h3>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Total Hours to Date:</label>
                                </div>
                                <div class="summaryLabelValueWrapper" style="text-align: right;">
                                    <label id="TotalHours" class="summaryLabel" style="float: none;" runat="server" data-bind="text: TotalHours"></label>
                                    <div id="hoursToolTipContainer" style="display: inline-block;"></div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Scheduled Hours to Date:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="ScheduledHours" class="summaryLabel" runat="server" data-bind="text: ScheduledHours"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Total Absent Hours:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="AbsentHours" class="summaryLabel" runat="server" data-bind="text: AbsentHours"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Total Makeup Hours:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="MakeupHours" class="summaryLabel" runat="server" data-bind="text: MakeupHours"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Scheduled Hours(Week):</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="ScheduledHoursWeek" class="summaryLabel" runat="server" data-bind="text: ScheduledHoursWeek"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Schedule Hours(Last Day):</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="ScheduledHoursLastDate" class="summaryLabel" runat="server" data-bind="text: ScheduledHoursLastDate"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Last Day Attended:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="LastDateAttended" class="summaryLabel" runat="server" data-bind="text: LastDateAttended"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Attendance %:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="AttendancePercentage" class="summaryLabel" runat="server" data-bind="text: AttendancePercentage"></label>
                                </div>
                            </div>
                        </div>
                        <div id="ContactSummary" class="summaryBlock">
                            <h3>CONTACT</h3>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Address:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="Address" class="summaryLabel" runat="server" data-bind="text: Address"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Phone:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <label id="Phone" class="summaryLabel" runat="server" data-bind="text: Phone"></label>
                                    <asp:TextBox ID="PhoneHidden" CssClass="hidden" runat="server" TextMode="Phone" data-bind="value: PhoneHidden" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="labelWrapper">
                                    <label class="summaryLabel">Email:</label>
                                </div>
                                <div class="summaryLabelValueWrapper">
                                    <a id="emailLink" runat="server">
                                        <label id="Email" class="summaryLabel" runat="server" data-bind="text: Email"></label>
                                    </a>
                                    <asp:TextBox ID="EmailHidden" CssClass="hidden" runat="server" TextMode="Email" data-bind="value: EmailHidden" />
                                </div>
                            </div>
                            <div class="row" style="text-align: left">
                                <div class="labelWrapper">
                                </div>
                                <button id="moreContactsRedirect" type="button" class="k-button" style="margin-left: 15px;">More Contacts</button>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>

