﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="FieldsEmployerContact.aspx.vb" Inherits="FieldsEmployerContact" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstEmployerContact">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop2">
                        <br />
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td nowrap align="left" width="15%">
                                                <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                            </td>
                                            <td nowrap width="85%">
                                                <asp:RadioButtonList ID="radStatus" runat="Server" RepeatDirection="Horizontal" AutoPostBack="true"
                                                    CssClass="label">
                                                    <asp:ListItem Text="Active" Selected="True" />
                                                    <asp:ListItem Text="Inactive" />
                                                    <asp:ListItem Text="All" />
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="listframebottom">
                                    <div class="scrollleftfilters">
                                        <asp:DataList ID="dlstEmployerContact" runat="server">
                                            <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                            <ItemStyle CssClass="itemstyle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
                                                    CommandArgument='<%# Container.DataItem("EmployerContactId")%>' Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'
                                                    CausesValidation="False"></asp:ImageButton>
                                                <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" CommandArgument='<%# Container.DataItem("EmployerContactId")%>'
                                                    Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'
                                                    CausesValidation="False"></asp:ImageButton>
                                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                                <asp:LinkButton Text='<%# Container.DataItem("LastName") & ", " & Container.Dataitem("FirstName") & " " & Container.DataItem("MiddleName")%>'
                                                    runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("EmployerContactId")%>'
                                                    ID="Linkbutton2" CausesValidation="False" />
                                            </ItemTemplate>
                                        </asp:DataList></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
            Orientation="HorizontalTop">
               <asp:Panel ID="pnlRHS" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                        <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                        <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none;">
                <tr>
                    <td class="detailsframe">
                    <div class="boxContainer">
                    <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->
                         
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="98%">
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblFirstName" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtFirstName" TabIndex="1" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblPrefixId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlPrefixId" TabIndex="5" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblMiddleName" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtMiddleName" TabIndex="2" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblSuffixId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlSuffixId" TabIndex="6" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblLastName" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtLastName" TabIndex="3" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblTitleId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtTitleId" TabIndex="7" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlStatusId" TabIndex="4" runat="server" CssClass="dropdownlist" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                        </td>
                                        <td class="contentcell4">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcellheader" nowrap colspan="6">
                                            <asp:Label ID="lblAddress" runat="server" CssClass="label" Font-Bold="true">Address</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            &nbsp;
                                        </td>
                                        <td class="contentcell4">
                                            <asp:CheckBox ID="chkForeignZip" TabIndex="8" runat="server" AutoPostBack="true"
                                                CssClass="checkboxinternational"></asp:CheckBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            &nbsp;
                                        </td>
                                        <td class="contentcell4" nowrap>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblAddress1" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtAddress1" TabIndex="9" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblAddressTypeId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlAddressTypeId" TabIndex="15" runat="server" CssClass="dropdownlist" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblAddress2" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtAddress2" TabIndex="10" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                            &nbsp;
                                        </td>
                                        <td class="contentcell4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblCity" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtCity" TabIndex="11" runat="server" AutoPostBack="true" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                            &nbsp;
                                        </td>
                                        <td class="contentcell4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblState" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlState" TabIndex="12" runat="server" CssClass="dropdownlist" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblOtherState" runat="server" CssClass="label" Visible="False">Other state not listed</asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtOtherState" TabIndex="45" runat="server" AutoPostBack="true"
                                                CssClass="textbox" Visible="False"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblZip" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <telerik:RadMaskedTextBox ID="txtZip" tabindex="13" runat="server" cssclass="textbox"
                                                          Mask="##################################################" DisplayMask="##################################################" 
                                                          DisplayPromptChar="" AutoPostBack="false" Width="200px" ></telerik:RadMaskedTextBox>	
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                            &nbsp;
                                        </td>
                                        <td class="contentcell4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblCountry" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlCountry" TabIndex="14" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                            &nbsp;
                                        </td>
                                        <td class="contentcell4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcellheader" nowrap colspan="6">
                                            <asp:Label ID="lblPhone" runat="server" CssClass="label" Font-Bold="true">Phones</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblWorkPhone" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4" nowrap>
<telerik:RadMaskedTextBox ID="txtWorkPhone" tabindex="17" runat="server" cssclass="textbox" DisplayFormatPosition="Right" Width="200px"></telerik:RadMaskedTextBox>
                                            <asp:CheckBox ID="chkForeignWorkPhone" TabIndex="16" runat="server" AutoPostBack="true"
                                                CssClass="checkboxinternational"></asp:CheckBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblWorkBestTime" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtWorkBestTime" TabIndex="19" runat="server" CssClass="textbox"
                                                Columns="9"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblWorkExt" runat="server" CssClass="label" Width="5px"></asp:Label>
                                        </td>
                                        <td class="contentcell4" nowrap>
                                            <asp:TextBox ID="txtWorkExt" TabIndex="18" runat="server" CssClass="textbox" ></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                            &nbsp;
                                        </td>
                                        <td class="contentcell4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblHomePhone" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4" nowrap>
                                            <telerik:RadMaskedTextBox ID="txtHomePhone" tabindex="21" runat="server" cssclass="textbox" width="200px" DisplayFormatPosition="Right" ></telerik:RadMaskedTextBox>

                                            <asp:CheckBox ID="chkForeignHomePhone" TabIndex="20" runat="server" AutoPostBack="true"
                                                CssClass="checkboxinternational"></asp:CheckBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblHomeBestTime" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtHomeBestTime" TabIndex="22" runat="server" CssClass="textbox"
                                                Columns="9"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblCellPhone" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4" nowrap>
<telerik:RadMaskedTextBox ID="txtCellPhone" tabindex="24" runat="server" cssclass="textbox" width="200px" DisplayFormatPosition="Right"></telerik:RadMaskedTextBox>

                                            <asp:CheckBox ID="chkForeignCellPhone" TabIndex="23" runat="server" AutoPostBack="true"
                                                CssClass="checkboxinternational"></asp:CheckBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblCellBestTime" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtCellBestTime" TabIndex="25" runat="server" CssClass="textbox"
                                                Columns="9"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblBeeper" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtBeeper" TabIndex="26" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblPinNumber" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtPinNumber" TabIndex="27" runat="server" CssClass="textbox" Columns="9"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcellheader" nowrap colspan="6">
                                            <asp:Label ID="label3" runat="server" CssClass="label" Font-Bold="true">Emails</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblWorkEmail" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtWorkEmail" TabIndex="28" runat="server" CssClass="textbox"></asp:TextBox><asp:RegularExpressionValidator
                                                ID="Regularexpressionvalidator5" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ControlToValidate="txtWorkEmail" ErrorMessage="Invalid WorkEmail Format" Display="None">Invalid Email Format</asp:RegularExpressionValidator>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                            <asp:Label ID="lblHomeEmail" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtHomeEmail" TabIndex="29" runat="server" CssClass="textbox"></asp:TextBox><asp:RegularExpressionValidator
                                                ID="Regularexpressionvalidator6" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ControlToValidate="txtHomeEmail" ErrorMessage="Invalid HomeEmail Format" Display="None">Invalid Email Format</asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcellheader" nowrap colspan="6">
                                            <asp:Label ID="label5" runat="server" CssClass="label" Font-Bold="true">Notes</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblNotes" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell2" colspan="5">
                                            <asp:TextBox ID="txtNotes" TabIndex="30" runat="server" CssClass="tocommentsnowrap"
                                                Columns="60" MaxLength="300" TextMode="MultiLine" Rows="3" Width="500px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                           
                            <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="98%">
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcellheader" nowrap colspan="6">
                                            <asp:Label ID="label4" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell2" colspan="6">
                                            <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- end content table-->
                            <asp:TextBox ID="txtModDate" runat="server"  Style="display: none" CssClass="donothing" Width="0px"></asp:TextBox>
                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtEmployerContactId" runat="server" Visible="false"></asp:TextBox>
            <asp:CheckBox ID="chkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
            <asp:TextBox ID="txtRowIds" Style="display: none" runat="server"  CssClass="donothing"></asp:TextBox>
            <asp:TextBox ID="txtResourceId" Style="display: none" runat="server" CssClass="donothing"></asp:TextBox>
            <asp:TextBox ID="txtEmployerId" runat="server" Visible="False"></asp:TextBox>
             </asp:Panel>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>
