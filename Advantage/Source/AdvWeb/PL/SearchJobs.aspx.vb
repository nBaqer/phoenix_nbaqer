﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects

Partial Class SearchJobs
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblStudentId As System.Web.UI.WebControls.Label
    Protected WithEvents txtStudentId As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlStatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents ddlStatusId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dgrdStudentSearch As System.Web.UI.WebControls.DataGrid
    Protected WithEvents StudentLNameLinkButton As System.Web.UI.WebControls.LinkButton
    Protected state As AdvantageSessionState
    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
    Protected WithEvents lblProgOfStudy As System.Web.UI.WebControls.Label
    Protected WithEvents txtProgOfStudy As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSkills As System.Web.UI.WebControls.Label
    Protected WithEvents txtSkills As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblStatusId As System.Web.UI.WebControls.Label
    Protected WithEvents lblJobTitleId As System.Web.UI.WebControls.Label
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents LinkButton1 As System.Web.UI.WebControls.LinkButton


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        ''load Advantage state
        'state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        'state("empInfo") = Nothing
        ''save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        ''Call mru component for students
        'Dim objMRUFac As New MRUFacade
        'Dim ds As New DataSet

        'ds = objMRUFac.LoadMRU("Students", Session("UserId").ToString(), HttpContext.Current.Request.Params("cmpid"))

        ''load Advantage state
        'state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        'state("MRUDS") = ds
        ''save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
    End Sub

#End Region

    Private campusId As String
    Private pObj As New UserPagePermissionInfo
    Protected resourceId As Integer
    Protected userId As String
    Private m_context As HttpContext
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Me.Form.DefaultButton = btnSearch.UniqueID


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        campusid = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        If Not Page.IsPostBack Then
            'header1.EnableHistoryButton(False)
            BuildPrgVersionDDL()
            BuildCountyDDL()
            'BuildSkillsDDL()
            'BuildJobTitlesDDL()
            'BuildExpertiseDDL()
            BuildFullPartTimeDDL()
        End If
    End Sub
    Private Sub BuildPrgVersionDDL()
        'Bind the Status DropDownList
        Dim PrgVersion As New JobSearchFacade
        With ddlPrgVerId
            '.DataTextField = "PrgVerDescrip"
            .DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = PrgVersion.GetProgramVersions()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCountyDDL()
        'Bind the Status DropDownList
        Dim PrgVersion As New JobSearchFacade
        With ddlCountyId
            .DataTextField = "CountyDescrip"
            .DataValueField = "CountyId"
            .DataSource = PrgVersion.GetCounties
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    'Private Sub BuildExpertiseDDL()
    '    'Bind the Status DropDownList
    '    Dim PrgVersion As New JobSearchFacade
    '    With ddlExpertiseLevelId
    '        .DataTextField = "ExpertiseDescrip"
    '        .DataValueField = "ExpertiseId"
    '        .DataSource = PrgVersion.GetExpertiseLevel
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildFullPartTimeDDL()
        'Bind the Status DropDownList
        Dim PrgVersion As New JobSearchFacade
        With ddlFullTime
            .DataTextField = "FullPartTimeDescrip"
            .DataValueField = "FullTimeId"
            .DataSource = PrgVersion.GetFullPartTime
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    'Private Sub BuildSkillsDDL()
    '    'Bind the Status DropDownList
    '    Dim PrgVersion As New JobSearchFacade
    '    With ddlSkillId
    '        .DataTextField = "SkillDescrip"
    '        .DataValueField = "SkillId"
    '        .DataSource = PrgVersion.GetSkills
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildJobTitlesDDL()
    '    'Bind the Status DropDownList
    '    Dim JobTitleId As New JobSearchFacade
    '    With ddlJobTitleId
    '        .DataTextField = "TitleDescrip"
    '        .DataValueField = "TitleId"
    '        .DataSource = JobTitleId.GetTitles
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Public Sub BindDataList()
        Dim jobsearch As New JobSearchFacade
        Dim strPrgVerId As String
        Dim strMinSalary As String
        Dim strMaxSalary As String
        Dim strCountyId As String
        Dim strAvailableDate As String
        '        Dim strAvailableDays As String
        '   Dim strJobTitleId As String
        Dim strCampus As String
        Dim strSkillID As String
        '   Dim strExpertiseLevelId As String
        Dim strFullTimeId As String
        Dim strAvailableDateTo As String

        If ddlPrgVerId.SelectedValue = "" Then
            strPrgVerId = ""
        Else
            strPrgVerId = ddlPrgVerId.SelectedValue
        End If
        If txtMinSalary.Text = "" Then
            strMinSalary = ""
        Else
            strMinSalary = txtMinSalary.Text
        End If
        If txtMaxSalary.Text = "" Then
            strMaxSalary = ""
        Else
            strMaxSalary = txtMaxSalary.Text
        End If
        If ddlCountyId.SelectedValue = "" Then
            strCountyId = ""
        Else
            strCountyId = ddlCountyId.SelectedValue
        End If
        If txtAvailableDate.SelectedDate Is Nothing Then
            strAvailableDate = ""
        Else
            strAvailableDate = txtAvailableDate.SelectedDate
        End If
        If txtAvailableDateTo.SelectedDate Is Nothing Then
            strAvailableDateTo = ""
        Else
            strAvailableDateTo = txtAvailableDateTo.SelectedDate
        End If

        If Not IsNothing(txtAvailableDate.SelectedDate) Then
            If IsNothing(txtAvailableDateTo.SelectedDate) Then
                'DisplayErrorMessage("The Available Date To cannot be empty")
                DisplayRADAlert(CallbackType.Postback, "ValError1", "The Available Date To cannot be empty", "Validation Error")
                Exit Sub
            End If
            Try
                If DateDiff(DateInterval.Day, CDate(txtAvailableDate.SelectedDate), CDate(txtAvailableDateTo.SelectedDate)) < 1 Then
                    'DisplayErrorMessage("The Available Date To should be greater than Available Date From")
                    DisplayRADAlert(CallbackType.Postback, "ValError2", "The Available Date To should be greater than Available Date From", "Validation Error")
                    Exit Sub
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'DisplayErrorMessage("Please enter a valid date")
                DisplayRADAlert(CallbackType.Postback, "ValError3", "Please enter a valid date", "Validation Error")
                Exit Sub
            End Try
        End If

        If ddlFullTime.SelectedValue = "" Then
            strFullTimeId = ""
        Else
            strFullTimeId = ddlFullTime.SelectedValue
        End If

        strCampus = HttpContext.Current.Request.Params("cmpid")

        'If ddlSkillId.SelectedValue = "" Then
        '    strSkillID = ""
        'Else
        '    strSkillID = ddlSkillId.SelectedValue
        'End If
         strSkillID = ""

        dgrdTransactionSearch.DataSource = jobsearch.JobSearchResults(strPrgVerId, strMinSalary, strMaxSalary, strCountyId, strAvailableDate, strSkillID, strCampus, strFullTimeId, strAvailableDateTo)
        dgrdTransactionSearch.DataBind()
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        ' Try
        BindDataList()
        dgrdTransactionSearch.Visible = True
        'Catch ex As System.Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        ' Response.Write(ex.Message)
        '  End Try
    End Sub
    Public Sub DisplayInMessageBox(ByVal page As Page, ByVal StudentId As String)
        'ByVal DocumentType As String, ByVal StudentName As String, ByVal StudentId As String, ByVal Extension As String
        'If page.Request.Browser.JavaScript Then
        '    Dim scriptBegin As String = "<script type='text/javascript'>window.onload=OpenDocuments1();function OpenDocuments1(){window.open("
        '    Dim scriptEnd As String = "');}</script>"
        '    Dim strOpenString As String = "'DynamicResume.aspx?StudentId="
        '    Dim strStudentValue As String = StudentId
        '    Dim strWindowName As String = "','HistWindow','width=700,height=600,ScreenX=0,ScreenY=0"
        '    '   Register a javascript to display error message
        '    Dim strPath As String = scriptBegin + strOpenString + strStudentValue + strWindowName + scriptEnd
        '    page.RegisterStartupScript("ErrorMessage", strPath)
        'End If
        Dim winSettings As String = "toolbar=no, status=no, resizable=yes,width=900px,height=470px"
        Dim name As String = "DocManagement"
        Dim resId = Request.QueryString("resid").ToString()
        Dim url As String = "DynamicResume.aspx?StudentId=" + StudentId + "&resid=" + resId
        CommonWebUtilities.OpenChildWindow(page, url, name, winSettings)
    End Sub

    Private Sub LinkButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles LinkButton1.Click

    End Sub
    Private Sub dgrdTransactionSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTransactionSearch.ItemCommand
        DisplayInMessageBox(Me, e.CommandArgument)
    End Sub

    Private Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        BuildPrgVersionDDL()
        BuildCountyDDL()
        'BuildSkillsDDL()
        BuildFullPartTimeDDL()
        txtMinSalary.Text = ""
        txtMaxSalary.Text = ""
        txtAvailableDate.Clear()
        dgrdTransactionSearch.Visible = False
    End Sub
    'Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    'BindToolTip()
    'End Sub
End Class
