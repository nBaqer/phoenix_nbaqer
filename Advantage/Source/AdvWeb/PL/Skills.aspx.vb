Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class Skills
    Inherits BasePage

    Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
    Protected WithEvents btnClose As System.Web.UI.WebControls.Button
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button
    Protected WithEvents Button2 As System.Web.UI.WebControls.Button
    Protected WithEvents Button3 As System.Web.UI.WebControls.Button
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected ResourceId As String
    Protected ModuleId As String
    Protected campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim objCommon As New CommonUtilities()
        Dim objListGen As New DataListGenerator()
        Dim ds As New DataSet()
        Dim sw As New System.IO.StringWriter()
        Dim ds2 As New DataSet()
        Dim sStatusId As String
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = resourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        Try
            If Not Page.IsPostBack Then
                m_Context = HttpContext.Current
                txtResourceId.Text = CInt(m_Context.Items("ResourceId"))

                Session("NameCaption") = ""
                Session("EmployerCode") = ""
                Session("NameValue") = ""
                Session("IdCaption") = ""
                Session("IdValue") = ""
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                txtSkillId.Text = Guid.NewGuid.ToString()
                ds = objListGen.SummaryListGenerator(userId, campusId)

                'Generate the status id
                ds2 = objListGen.StatusIdGenerator()

                'Set up the primary key on the datatable
                ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

                Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
                sStatusId = row("StatusId").ToString()

                Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "SkillDescrip", DataViewRowState.CurrentRows)

                With dlstSkills
                    .DataSource = dv
                    .DataBind()
                End With
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
            Else

                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)

            End If

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objCommon As New CommonUtilities()
        Dim objListGen As New DataListGenerator()
        Dim ds As New DataSet()
        Dim sw As New System.IO.StringWriter
        Dim msg As String
        Try
            If ViewState("MODE") = "NEW" Then
                'Set the Primary Key value, so it can be used later by the DoUpdate method
                objCommon.PagePK = txtSkillId.Text
                txtRowIds.Text = objCommon.PagePK
                msg = objCommon.DoInsert(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If
                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstSkills.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"

            ElseIf viewstate("MODE") = "EDIT" Then
                objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstSkills.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Response.Write(objCommon.strText)
            End If
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstSkills, txtSkillId.Text, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstSkills, txtSkillId.Text)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim msg As String

        Try
            msg = objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            If msg <> "" Then
                DisplayErrorMessage(msg)
            End If
            ClearRHS()
            ds = objListGen.SummaryListGenerator(userId, campusId)
            dlstSkills.SelectedIndex = -1
            PopulateDataList(ds)
            ds.WriteXml(sw)
            ViewState("ds") = sw.ToString()
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            Viewstate("MODE") = "NEW"
            txtSkillId.Text = Guid.NewGuid.ToString()
            'Header1.EnableHistoryButton(False)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Try
            ClearRHS()
            ds.ReadXml(sr)
            'PopulateDataList(ds)
            'disable new and delete buttons.
            'objcommon.SetBtnState(Form1, "NEW", pObj)
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            txtSkillId.Text = Guid.NewGuid.ToString()
            'Reset Style in the Datalist
            'CommonWebUtilities.SetStyleToSelectedItem(dlstSkills, Guid.Empty.ToString, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstSkills, Guid.Empty.ToString)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub dlstSkills_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstSkills.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim dv2 As New DataView

        Try
            strGUID = dlstSkills.DataKeys(e.Item.ItemIndex).ToString()
            txtRowIds.Text = e.CommandArgument.ToString()
            objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGUID)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = e.Item.ItemIndex
            dlstSkills.SelectedIndex = selIndex
            ds.ReadXml(sr)
            PopulateDataList(ds)
            '   set Style to Selected Item
            '.SetStyleToSelectedItem(dlstSkills, e.CommandArgument, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstSkills, e.CommandArgument)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstSkills_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstSkills_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As New DataSet
        Dim dv2 As New DataView
        Dim objListGen As New DataListGenerator
        Dim sStatusId As String

        ds2 = objListGen.StatusIdGenerator()
        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        'If (chkStatus.Checked = True) Then 'Show Active Only

        '    Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
        '    sStatusId = row("StatusId").ToString()
        '    'Create dataview which displays active records only
        '    Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "SkillDescrip", DataViewRowState.CurrentRows)
        '    dv2 = dv
        'ElseIf (chkStatus.Checked = False) Then

        '    Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
        '    sStatusId = row("StatusId").ToString()
        '    'Create dataview which displays inactive records only
        '    Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "SkillDescrip", DataViewRowState.CurrentRows)
        '    dv2 = dv
        'End If

        If (radstatus.SelectedItem.Text = "Active") Then   'Show Active Only
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays active records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "SkillDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        ElseIf (radstatus.SelectedItem.Text = "Inactive") Then   'Show InActive Only
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays inactive records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "SkillDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        Else  'Show All
            ds = objListGen.SummaryListGeneratorSort()
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays inactive records only
            Dim dv As New DataView(ds.Tables(0), "", "", DataViewRowState.CurrentRows)
            dv2 = dv
        End If

        With dlstSkills
            .DataSource = dv2
            .DataBind()
        End With
        dlstSkills.SelectedIndex = -1


    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView

        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            Viewstate("MODE") = "NEW"
            txtSkillId.Text = Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
            'Header1.EnableHistoryButton(False)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
End Class
