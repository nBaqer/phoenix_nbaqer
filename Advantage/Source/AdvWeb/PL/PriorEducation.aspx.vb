﻿
Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.Common

Namespace AdvWeb.PL

    Partial Class PlPriorEducation
        Inherits BasePage

        Private pObj As New UserPagePermissionInfo
        Public ObjStudentState As StudentMRU
        Public SelectedCampusId As String

        Private Sub Page_Load(sender As System.Object, e As EventArgs) Handles MyBase.Load

            'Get necessary objects.....................
            ObjStudentState = GetObjStudentState()
            If ObjStudentState Is Nothing OrElse ObjStudentState.StudentId = Nothing OrElse ObjStudentState.StudentId = Guid.Empty Then
                RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
                Exit Sub
            End If
            Dim advantageUserState As User = AdvantageSession.UserState
            Dim resourceId = HttpContext.Current.Request.Params("resid")
            Dim campusid = Master.CurrentCampusId
            SelectedCampusId = Master.CurrentCampusId
            Master.PageObjectId = ObjStudentState.StudentId.ToString()
            Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
            Master.SetHiddenControlForAudit()
            Master.ShowHideStatusBarControl(True) ' Show student bar
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusid)
            If Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusid.ToString + "&redirect=switch-campus", False)
                    Return
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(1, ObjStudentState.Name)
                End If
            End If
        End Sub
    End Class
End Namespace