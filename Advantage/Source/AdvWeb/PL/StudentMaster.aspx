﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentMaster.aspx.vb" Inherits="StudentMaster" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Reference Control="~/usercontrols/StudentInfoBar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False" Visible="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Enabled="False" Visible="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable, Table100" align="center">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                        <!-- begin table content-->

                                        <table class="contentleadmastertable, Table100">
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="8" style="border-top: 0 !important; border-left: 0 !important; border-right: 0">
                                                    <asp:Label ID="lblgeneral" runat="server" CssClass="label" Font-Bold="true">General Information</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap style="padding-top: 16px">


                                                    <asp:Label ID="lblFirstName" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap style="padding-top: 16px">
                                                    <asp:TextBox ID="txtFirstName" TabIndex="1" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox></td>
                                                <td class="leadcellspacer" nowrap style="padding-top: 16px">&nbsp;</td>
                                                <td class="leadcell" nowrap style="padding-top: 16px">
                                                    <asp:Label ID="lblGender" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap style="padding-top: 16px">
                                                    <asp:DropDownList ID="ddlGender" TabIndex="9" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList></td>
                                                <td class="leadcellspacer" nowrap style="padding-top: 16px">&nbsp;</td>
                                                <td class="leadcell" nowrap style="padding-top: 16px">
                                                    <asp:Label ID="lblSSN" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2right" nowrap style="padding-top: 16px">
                                                    <telerik:RadMaskedTextBox ID="txtSSN" TabIndex="17" runat="server" CssClass="textbox" Width="200px"
                                                        Mask="###-##-####">
                                                    </telerik:RadMaskedTextBox>
                                                    <asp:RegularExpressionValidator Display="None" ID="MaskedTextBoxRegularExpressionValidator" runat="server"
                                                        ErrorMessage="SSN format is ###-##-####" ControlToValidate="txtSSN" ValidationExpression="^\d{3}-\d{2}-\d{4}$" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblMiddleName" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:TextBox ID="txtMiddleName" TabIndex="2" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblDOB" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <telerik:RadDatePicker ID="txtDOB" runat="server" MinDate="01/01/1900" MaxDate="01/01/2100" Width="200" daynameformat="Short" AutoPostBack="true">
                                                    </telerik:RadDatePicker>
                                                </td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblFamilyIncome" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2right" nowrap>
                                                    <asp:DropDownList ID="ddlFamilyIncome" TabIndex="18" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblLastName" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:TextBox ID="txtLastName" TabIndex="3" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblAge" runat="server" CssClass="label" Text="Age"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:TextBox ID="txtAge" CssClass="textbox" runat="server" ReadOnly="True"></asp:TextBox></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblSponsor" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2right" nowrap>
                                                    <asp:DropDownList ID="ddlSponsor" TabIndex="20" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblPrefix" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlPrefix" TabIndex="4" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblRace" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlRace" TabIndex="13" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblAssignedDate" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2right" nowrap>
                                                    <telerik:RadDatePicker ID="txtAssignedDate" runat="server" Width="200" daynameformat="Short">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblSuffix" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlSuffix" TabIndex="5" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblMaritalStatus" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlMaritalStatus" TabIndex="14" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell">
                                                    <asp:Label ID="lblHousingId" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2right" nowrap>
                                                    <asp:DropDownList ID="ddlHousingId" TabIndex="23" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblStudentStatus" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlStudentStatus" TabIndex="6" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblChildren" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:TextBox ID="txtChildren" TabIndex="15" CssClass="textbox" runat="server"></asp:TextBox></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblAdminCriteriaId" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2right" nowrap>
                                                    <asp:DropDownList ID="ddlAdminCriteriaId" TabIndex="24" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblDependencyTypeId" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlDependencyTypeId" TabIndex="7" CssClass="dropdownlist" runat="server" Width="200px">
                                                    </asp:DropDownList></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblGeographicTypeId" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlGeographicTypeId" TabIndex="16" CssClass="dropdownlist" Width="200px"
                                                        runat="server">
                                                    </asp:DropDownList></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>

                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap style="padding-bottom: 16px">
                                                    <asp:Label ID="lblStudentNumber" runat="server" CssClass="label"></asp:Label></td>
                                                <td class="leadcell2" nowrap style="padding-bottom: 16px">
                                                    <asp:TextBox ID="txtStudentNumber" runat="server" TabIndex="8" CssClass="textbox"></asp:TextBox></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap style="padding-bottom: 16px">
                                                    <asp:Label ID="lblEntranceInterviewDate" runat="server" CssClass="label" Visible="false" Text="Entrance Interview Date"></asp:Label>
                                                </td>
                                                <td class="leadcell2right" nowrap style="padding-bottom: 16px">
                                                    <asp:TextBox ID="txtEntranceInterviewDate" TabIndex="18" CssClass="textboxdate" runat="server" Visible="false" Width="200px"></asp:TextBox>&nbsp;<a
                                                        onclick="javascript:OpenCalendar('ClsSect','txtEntranceInterviewDate', true, 1945)"><img id="Img7"
                                                            src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server" align="absMiddle" visible="false" /></a>
                                                </td>
                                                <td class="leadcellspacer" nowrap style="padding-bottom: 16px">&nbsp;</td>
                                                <td class="leadcell" nowrap style="padding-bottom: 16px">
                                                    <asp:Label ID="lblHighSchoolProgramCode" runat="server" CssClass="label" Visible="false" Text="HighSchool Program Code"></asp:Label>
                                                </td>
                                                <td class="leadcell2right" nowrap style="padding-bottom: 16px">
                                                    <asp:TextBox ID="txtHighSchoolProgramCode" runat="Server" CssClass="textbox" Visible="false"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell" nowrap style="padding-bottom: 16px">
                                                    <asp:Label ID="lblStudentGrpId" runat="server" CssClass="label" Visible="False"></asp:Label></td>
                                                <td class="leadcell2" nowrap style="padding-bottom: 16px">
                                                    <asp:DropDownList ID="ddlStudentGrpId" runat="server" CssClass="dropdownlist" Visible="False" Width="200px">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell2" colspan="5">
                                                    <asp:CheckBoxList ID="chkLeadGrpId" runat="Server" CssClass="checkboxstyle"
                                                        RepeatColumns="7" Visible="False">
                                                    </asp:CheckBoxList></td>
                                            </tr>
                                        </table>


                                        <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                            width="100%">
                                            <tr>
                                                <td class="leadcell3">
                                                    <table class="contenttableborder" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="contentcellheader" nowrap colspan="4" style="border-top: 0 !important; border-left: 0 !important; border-right: 0">
                                                                <asp:Label ID="lblAddress" runat="server" CssClass="label" Font-Bold="true">Address</asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap style="padding-top: 10px">&nbsp;</td>
                                                            <td class="contentcell4column" nowrap style="padding-top: 10px">
                                                                <asp:CheckBox ID="chkForeignZip" TabIndex="25" CssClass="checkboxinternational" runat="server"
                                                                    AutoPostBack="true"></asp:CheckBox></td>
                                                            <td class="leadcell4column" nowrap style="padding-top: 10px">&nbsp;</td>
                                                            <td class="contentcell4columnright" nowrap style="padding-top: 10px">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblAddress1" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4column">
                                                                <asp:TextBox ID="txtAddress1" TabIndex="26" CssClass="textbox" runat="server" TextMode="MultiLine" Width="200px"
                                                                    MaxLength="50"></asp:TextBox></td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblAddressStatus" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4columnright">
                                                                <asp:DropDownList ID="ddlAddressStatus" TabIndex="33" CssClass="dropdownlist" runat="server" Width="200px">
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblAddress2" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4column">
                                                                <asp:TextBox ID="txtAddress2" TabIndex="27" CssClass="textbox" runat="server"></asp:TextBox></td>
                                                            <td class="leadcell4column" nowrap>
                                                                <asp:Label ID="lblAddressTypeId" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4columnright">
                                                                <asp:DropDownList ID="ddlAddressTypeId" TabIndex="34" CssClass="dropdownlist" runat="server" Width="200px">
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblCity" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4column">
                                                                <asp:TextBox ID="txtCity" TabIndex="28" CssClass="textbox" runat="server" MaxLength="50"
                                                                    AutoPostBack="true"></asp:TextBox></td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblCounty" runat="server" Text="County" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4columnright">
                                                                <asp:DropDownList ID="ddlCountyId" TabIndex="29" CssClass="dropdownlist" runat="server" Width="200px">
                                                                </asp:DropDownList></td>


                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblStateId" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4column">
                                                                <asp:DropDownList ID="ddlStateId" TabIndex="29" CssClass="dropdownlist" runat="server" Width="200px">
                                                                </asp:DropDownList></td>
                                                            <td class="leadcell4column" style="white-space: normal">
                                                                <asp:Label ID="lblOtherState" runat="server" CssClass="label" Visible="False"></asp:Label></td>
                                                            <td class="contentcell4columnright" nowrap>
                                                                <asp:TextBox ID="txtOtherState" CssClass="textbox" runat="server" TabIndex="30" AutoPostBack="True"
                                                                    Visible="false"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblZip" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4column">
                                                                <telerik:RadMaskedTextBox ID="txtZip" TabIndex="31" runat="server" Width="200px"
                                                                    DisplayPromptChar="" AutoPostBack="false" DisplayFormatPosition="Left">
                                                                </telerik:RadMaskedTextBox>
                                                            </td>
                                                            <td class="leadcell4column">&nbsp;</td>
                                                            <td class="contentcell4columnright">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
                                                                <asp:Label ID="lblCountryId" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4column" style="padding-bottom: 16px">
                                                                <asp:DropDownList ID="ddlCountryId" TabIndex="32" CssClass="dropdownlist" runat="server" Width="200px">
                                                                </asp:DropDownList></td>
                                                            <td class="leadcell4column" style="padding-bottom: 16px">&nbsp;</td>
                                                            <td class="contentcell4columnright" style="padding-bottom: 16px">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="leadcell3">
                                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="contentcellheader" nowrap colspan="4" style="border-top: 0 !important; border-right: 0 !important; border-left: 0">
                                                                <asp:Label ID="lblPhone1" runat="server" CssClass="label" Font-Bold="true">Phones and Email</asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" style="padding-top: 10px" nowrap>&nbsp;</td>
                                                            <td class="contentcell4column" style="padding-top: 10px" nowrap>
                                                                <asp:CheckBox ID="chkForeignPhone" TabIndex="35" CssClass="checkboxinternational"
                                                                    Text="International" runat="server" AutoPostBack="true"></asp:CheckBox></td>
                                                            <td class="leadcell4column" style="padding-top: 10px" nowrap>&nbsp;</td>
                                                            <td class="contentcell4columnright" style="padding-top: 10px" nowrap>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblPhone" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4column">

                                                                <telerik:RadMaskedTextBox ID="txtPhone" TabIndex="36" runat="server"
                                                                    Width="200px" DisplayFormatPosition="Right" Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar="">
                                                                </telerik:RadMaskedTextBox>
                                                            </td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblWorkEmail" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4columnright">
                                                                <asp:TextBox ID="txtWorkEmail" TabIndex="39" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox><asp:RegularExpressionValidator
                                                                    ID="Regularexpressionvalidator5" runat="server" Display="None" ErrorMessage="Invalid WorkEmail Format"
                                                                    ControlToValidate="txtWorkEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Invalid Email Format</asp:RegularExpressionValidator></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblPhoneTypeId" CssClass="label" runat="server"></asp:Label></td>
                                                            <td class="contentcell4column">
                                                                <asp:DropDownList ID="ddlPhoneTypeId" TabIndex="37" CssClass="dropdownlist" runat="server" Width="200px">
                                                                </asp:DropDownList></td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblHomeEmail" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4columnright">
                                                                <asp:TextBox ID="txtHomeEmail" TabIndex="40" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox><asp:RegularExpressionValidator
                                                                    ID="Regularexpressionvalidator6" runat="server" Display="None" ErrorMessage="Invalid HomeEmail Format"
                                                                    ControlToValidate="txtHomeEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Invalid Email Format</asp:RegularExpressionValidator></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
                                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4column" style="padding-bottom: 16px">
                                                                <asp:DropDownList ID="ddlStatusId" TabIndex="38" CssClass="dropdownlist" runat="server" Width="200px">
                                                                </asp:DropDownList></td>
                                                            <td class="leadcell4column" style="padding-bottom: 16px">&nbsp;</td>
                                                            <td class="contentcell4columnright" style="padding-bottom: 16px">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>

                                        </table>


                                        <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                            width="100%">
                                            <tr>
                                                <td class="leadcell3">
                                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="contentcellheader" nowrap colspan="4" style="border-left: 0 !important; border-right: 0 !important; border-top: 0">
                                                                <asp:Label ID="lblPersonalIdentification" runat="server" CssClass="label" Font-Bold="true">Personal Identification</asp:Label></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap style="padding-top: 16px;width: 170px !important;">
                                                                <asp:Label ID="lblNationality" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4column" style="padding-top: 16px">
                                                                <asp:DropDownList ID="ddlNationality" TabIndex="41" CssClass="dropdownlist" runat="server" Width="200px">
                                                                </asp:DropDownList></td>
                                                            <td class="leadcell4column" style="padding-top: 16px">
                                                                <asp:Label ID="lblDrivLicStateId" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4columnright" style="padding-top: 16px" nowrap>
                                                                <asp:DropDownList ID="ddlDrivLicStateId" TabIndex="44" CssClass="dropdownlist" runat="server" Width="200px">
                                                                </asp:DropDownList></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" style="width: 170px !important;" nowrap>
                                                                <asp:Label ID="lblCitizen" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4column">
                                                                <asp:DropDownList ID="ddlCitizen" TabIndex="42" CssClass="dropdownlist" runat="server" Width="200px">
                                                                </asp:DropDownList></td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblDrivLicNumber" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4columnright">
                                                                <asp:TextBox ID="txtDrivLicNumber" TabIndex="45" CssClass="textbox" runat="server"
                                                                    MaxLength="50"></asp:TextBox></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px;width: 170px !important;">
                                                                <asp:Label ID="lblAlienNumber" runat="server" CssClass="label"></asp:Label></td>
                                                            <td class="contentcell4column" nowrap style="padding-bottom: 16px">
                                                                <asp:TextBox ID="txtAlienNumber" TabIndex="43" CssClass="textbox" runat="server"
                                                                    MaxLength="50"></asp:TextBox></td>
                                                            <td class="leadcell4column" nowrap style="padding-bottom: 16px">&nbsp;</td>
                                                            <td class="contentcell4columnright" style="padding-bottom: 16px">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>

                                     
                                        <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                            width="100%">
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="6" style="border-left: 0 !important; border-right: 0 !important; border-top: 0">
                                                    <asp:Label ID="ToTheme5" runat="server" CssClass="label" Font-Bold="true">Comments</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td style="padding-top: 16px; width: 170px; padding-left: 16px; vertical-align: top;" nowrap>
                                                    <asp:Label ID="lblComments" runat="server" CssClass="label"></asp:Label></td>
                                                <td style="padding-top: 16px !important; padding-bottom: 16px; width: 80%;"
                                                    colspan="5">
                                                    <asp:TextBox ID="txtComments" TabIndex="46" runat="server" MaxLength="300" CssClass="tocommentsnowrap"
                                                        Columns="60" Rows="3" TextMode="MultiLine" Width="80%"></asp:TextBox></td>
                                            </tr>
                                        </table>

                                        <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                            <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                                width="100%">
                                                <tr>
                                                    <td class="contentcellheader" nowrap colspan="6" style="border-left: 0 !important; border-right: 0 !important; border-top: 0">
                                                        <asp:Label ID="lblSDF" runat="server" Font-Bold="true" CssClass="label">School Defined Fields</asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" style="padding-top: 16px !important; padding-bottom: 16px" colspan="6">
                                                        <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                                            <asp:ValidationSummary ID="sdfvalidationsummary" runat="server" />
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>


                                        <asp:Panel ID="pnlApportioningCrCalance" runat="server" Visible="false">
                                            <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                                width="100%">
                                                <tr>
                                                    <td class="contentcellheader" nowrap colspan="6" style="border-left: 0 !important; border-right: 0 !important; border-top: 0">
                                                        <asp:Label ID="lbACB" runat="server" Font-Bold="true" CssClass="label">Apportioned Credit Balance</asp:Label></td>
                                                </tr>

                                                <tr>
                                                    <td class="leadcell4columnleft" style="padding-top: 16px">
                                                        <asp:Label ID="lbBalY1" runat="server" CssClass="label" Text="Balance Y1"></asp:Label></td>

                                                    <td class="contentcell4columnright" style="padding-top: 16px !important;"
                                                        colspan="5">
                                                        <asp:TextBox ID="tbBalY1" TabIndex="47" runat="server" MaxLength="300" CssClass="tocommentsnowrap"
                                                            Columns="60"></asp:TextBox></td>
                                                </tr>

                                                <tr>
                                                    <td class="leadcell4columnleft" style="padding-bottom: 16px !important;">
                                                        <asp:Label ID="lbBalY2" runat="server" CssClass="label" Text="Balance Y2"></asp:Label></td>

                                                    <td class="contentcell4columnright" style="padding-bottom: 16px"
                                                        colspan="5">
                                                        <asp:TextBox ID="tbBalY2" TabIndex="47" runat="server" MaxLength="300" CssClass="tocommentsnowrap"
                                                            Columns="60"></asp:TextBox></td>
                                                </tr>
                                            </table>
                                        </asp:Panel>

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <asp:TextBox ID="txtDate" runat="server" Visible="False" />
            <asp:TextBox ID="txtOriginalLastName" runat="server" Visible="false" />
            <asp:TextBox ID="txtOriginalFirstName" runat="server" Visible="false" />
            <!-- start validation panel-->
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
            <asp:TextBox ID="txtStudentID" runat="server" Visible="False"></asp:TextBox><asp:TextBox
                ID="txtLeadId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox ID="chkIsInDB"
                    runat="server" Visible="False"></asp:CheckBox><asp:TextBox ID="txtSourceDate" runat="server"
                        Visible="False"></asp:TextBox><asp:TextBox ID="txtExpectedStart" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox
                            ID="txtEnrollmentId" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="txtModDate"
                                runat="server" Width="0px" Visible="false"></asp:TextBox>
        <input type="hidden" name="scrollposition"
                                    id="scrollposition" />

        </telerik:RadPane>
    </telerik:RadSplitter>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>

