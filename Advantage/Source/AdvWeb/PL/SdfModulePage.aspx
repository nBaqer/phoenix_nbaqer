﻿<%@ Page Title="Add Custom Field to Page" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SdfModulePage.aspx.vb" Inherits="AdvWeb.PL.SdfModulePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../css/SdfPages.css" rel="stylesheet" />
    <script src="../Scripts/Advantage.Client.PL.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <br />
    <main id="udfPageWrapper">
        <!-- Use this to put content -->
        <div id="udfTitle">
            Instructions
        </div>
        <div class="udfhelp">
            <ol>
                <li>Select the Entity and Page where you want to add the Custom Field.</li>
                <li>Select an item from Available Custom Fields.</li>
                <li>Drag and drop the selected item into an empty box on the Current Page.</li>
                <li>If you wish to change the order of the Current Page, drag and drop the selected item to the desired field.</li>
                <li>If you wish to remove an item from the Current Page, drag the item back to Available Custom Fields.</li>
                <li>Select Save Changes to save the new layout.</li>
                <li>The new layout will then show on the selected Page.</li>
                <li>If the Available Custom Field shows as (Required) and is saved on the Current Page, 
                    the user will be required to populate that field on the selected Page. Fields are defined as required in
                     Maintenance\System\Create Custom Field.</li>
            </ol>
        </div>

        <div id="udfBody">
            <script type="text/kendo-x-tmpl" id="UnassignedTemplate">
             <div class="udfItem">
               <span> #=Description# </span>
            </div>
            </script>
            <script type="text/kendo-x-tmpl" id="AssignedTemplate">
             <div>
                #:Description#
            </div>
            </script>
            <script type="text/kendo-x-tmpl" id="PositionTemplate">
               <div class="positionCell" data-position="#:Position#" >
                    <span>#= Description#</span><br/>
               </div>
            </script>
            <%--   <script type="text/kendo-x-tmpl" id="PositionTemplate">
               <div class="positionCell" data-position="#:Position#" >
                    <span>#= Description#</span><br/>
                    <div class= "checkboxtemplateInVisible" name="checkboxUdf">   
                     <input type="checkbox"  #= SdfVisibility? checked="checked" : "" #  />Editable?
                   </div>
               </div>
            </script>--%>


            <section id="udfFilter" class="udffilter">
                <label class="udflabel">Entity<span class="udfrequired"> *</span></label>
                <input id="udfcbEntity" name="udfcbEntity" class="udfdd" />
                <span class="udfseparator">&nbsp;</span>
                <label class="udflabel">Page<span class="udfrequired"> *</span></label>
                <input id="udfcbPages" name="udfcbEntity" class="udfdd" />
                <br />
                <br />
            </section>

            <section id="udfPage" style="display: none;">
                <div id="udfTable">
                    <div id="udfcolumn1" >
                        <label class="udflabel">Available Custom Fields</label>

                        <div id="udfUnassigned" class="udfUnAssigned"></div>
                    </div>
                     <div id="udfcolumn2" class="udfcolumnseparator"></div>
                    <div id="udfcolumn3">
                        <label class="udflabel">Current Page</label>
                        <div id="udfPositionTable" class="positionTable"></div>
                        <button id="udfSave" type="button" class="k-button udfsaveclass">Save Changes</button>
                    </div>

                </div>
            </section>
        </div>
    </main>
    <script type="text/javascript">
        $(document).ready(function () {
            var udf = new PL.SdfPages();
        });
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

