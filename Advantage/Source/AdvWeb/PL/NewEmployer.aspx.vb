﻿Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports BL = Advantage.Business.Logic.Layer
Partial Class NewEmployer
    Inherits BasePage

    Protected WithEvents lblDescription As System.Web.UI.WebControls.Label

    Protected WithEvents RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents RegularExpressionValidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents lblStatus As System.Web.UI.WebControls.Label
    Protected WithEvents txtLocationId As System.Web.UI.WebControls.TextBox
    Protected WithEvents dlstEmployers As System.Web.UI.WebControls.DataList
    Protected WithEvents chkStatus As System.Web.UI.WebControls.CheckBox

    Protected WithEvents Results As System.Web.UI.UserControl
    Protected WithEvents ModDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents ModUser As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnHistory As System.Web.UI.WebControls.Button
    Protected state As AdvantageSessionState
    'This variable holds employerId. The value assigned is for testing only
    Protected EmployerId As String '= "255A50B2-0AD2-4020-934C-D4CFE5A2BB51"
    Protected WithEvents PhoneFormat As System.Web.UI.WebControls.Label
    Protected WithEvents FaxFormat As System.Web.UI.WebControls.Label
    Protected WithEvents ZipFormat As System.Web.UI.WebControls.Label
    Protected WithEvents btnTest As System.Web.UI.WebControls.Button
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lbl2 As System.Web.UI.WebControls.Label
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents Textbox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents chkForeign As System.Web.UI.WebControls.CheckBox
    Private requestContext As HttpContext
    Private pObj As New UserPagePermissionInfo
    Protected resourceId As Integer
    Protected ModuleId As String
    Protected sdfcontrols As New SDFComponent

    Private mruProvider As BL.MRURoutines
    Protected strDefaultCountry As String
    Private campusId As String
    Protected userId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New BL.MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
        'header1.ObjectID = Session("EmployerID")
        'Session("ObjectId") = Session("EmployerID")
        ''Call mru component for employers
        'Dim objMRUFac As New MRUFacade
        'Dim ds As New DataSet

        'If Session("SEARCH") = 1 Then
        '    ds = objMRUFac.LoadAndUpdateMRU("Employers", Session("EmployerID").ToString(), userId, HttpContext.Current.Request.Params("cmpid"))
        'Else
        '    ds = objMRUFac.LoadMRU("Employers", userId, HttpContext.Current.Request.Params("cmpid"))
        'End If

        ''load Advantage state
        'state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        'state("MRUDS") = ds
        ''save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Session("SEARCH") = 0

        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        campusId = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        'Check if this page still exists in the menu while switching campus
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        '   disable History button at all time
        'header1.EnableHistoryButton(False)

        Dim SearchEmployerID As New EmployerSearchFacade
        '  Dim startPos As String
        Session("IdCaption") = ""
        Session("IdValue") = ""
        'If Session("EmployerCode") = "" Then
        '    Session("Error") = "The Session Value Has Expired"
        '    Response.Redirect("ErrorPage.aspx")
        'End If
        'startPos = InStr(SearchEmployerID.GetEmployerID(Session("EmployerCode")), ",") - 1
        'EmployerId = Mid(SearchEmployerID.GetEmployerID(Session("EmployerCode")), 1, startPos)


        Dim objCommon As New CommonUtilities
        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString
        If Not Page.IsPostBack Then
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder))
            txtCode.BackColor = Color.White
            txtEmployerDescrip.BackColor = Color.White
            ddlCampGrpId.BackColor = Color.White
            txtAddress1.BackColor = Color.White
            txtcity.BackColor = Color.White
            ddlStateId.BackColor = Color.White
            'objCommon.SetBtnState(Form1, "NEW")
            BuildDropDownLists(campusId)
            ViewState("MODE") = "NEW"
            EmployerId = Guid.NewGuid.ToString
            m_Context = HttpContext.Current
            txtEmployerId.Text = EmployerId
            txtRowIds.Text = EmployerId
            txtResourceId.Text = CInt(m_Context.Items("ResourceId"))
            chkIsInDB.Checked = False
            chkForeignPhone.Checked = False
            chkForeignZip.Checked = False
            chkForeignFax.Checked = False
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "NEW", pObj)


            'Get Default Employer Data when page is loaded for first time
            Dim employerinfo As New PlEmployerInfoFacade
            BindEmployerInfoData(employerinfo.GetEmployerInfo(txtEmployerId.Text))
            BindEmployerJobsListBox(txtEmployerId.Text)
        Else
            ' objCommon.PageSetup(Form1, "EDIT")
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT")
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder))
            txtCode.BackColor = Color.White
            txtEmployerDescrip.BackColor = Color.White
            ddlCampGrpId.BackColor = Color.White
            txtAddress1.BackColor = Color.White
            txtcity.BackColor = Color.White
            ddlStateId.BackColor = Color.White
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT", pObj)
        End If
        ''Disable State DropdownList if International is Checked
        'If chkForeignZip.Checked = True Then
        '    txtOtherState.Visible = True
        '    lblOtherState.Visible = True
        '    ddlStateId.Enabled = False
        'Else
        '    txtOtherState.Visible = False
        '    lblOtherState.Visible = False
        '    ddlStateId.Enabled = True
        'End If
        'Apply Mask/Disable Mask Based on if user 
        'selects International
        Dim Phone As RadMaskedTextBox = txtPhone
        SetPhoneMask(chkForeignPhone.Checked, Phone)
        Dim Fax As RadMaskedTextBox = txtFax
        SetPhoneMask(chkForeignFax.Checked, Fax)
        EnableBtnEmployerType()

        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfcontrols.GetSDFExists(resourceId, ModuleId)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtEmployerId.Text) <> "" Then
            sdfcontrols.GenerateControlsEdit(pnlSDF, resourceId, txtEmployerId.Text, ModuleId)
        Else
            sdfcontrols.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
        End If

    End Sub
    Private Sub EnableBtnEmployerType()
        If Trim(Session("EmployerType")) = "ExistingEmployer" Then
            'btnNew.Enabled = False
        ElseIf Trim(Session("EmployerType")) = "NewEmployer" Then
            ' btnNew.Enabled = True
        End If
    End Sub
    Private Sub btnNew_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'Hide Results Label
        'Results.Visible = False

        'Assign A New Value For Primary Key Column
        txtEmployerId.Text = Guid.NewGuid.ToString()

        'Reset The Value of chkIsInDb Checkbox
        'To Identify an Insert
        chkIsInDB.Checked = False

        'Create a Empty Object and Initialize the Object
        BindEmployerInfoData(New plEmployerInfo)

        'Refresh The DropDownList For EmployerGroup
        BuildEmployerGroupDDL()

        'Initialize Buttons
        InitButtonsForLoad()

        sdfcontrols.GenerateControlsNew(pnlSDF, resourceId, ModuleId)

        HandleInternationalAddress(False)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Results.Visible = False
        If Not (txtEmployerId.Text = Guid.Empty.ToString) Then

            '   instantiate component
            Dim EmployerInfo As New PlEmployerInfoFacade

            'Delete The Row Based on EmployerId 
            Dim result As String = EmployerInfo.DeleteEmployerInfo(txtEmployerId.Text, Date.Parse(txtModDate.Text))

            'If Delete Fails
            If result <> "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error2", result, "Delete Error")
            Else
                'Reset The Value of chkIsInDb Checkbox
                'To Identify an Insert
                chkIsInDB.Checked = False

                'bind an empty new BankAcctInfo
                BindEmployerInfoData(New plEmployerInfo)

                '   initialize buttons
                InitButtonsForLoad()

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim SDFControl As New SDFComponent
                SDFControl.DeleteSDFValue(txtEmployerId.Text)
                SDFControl.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                HandleInternationalAddress(False)

            End If
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim EmployerInfo As New PlEmployerInfoFacade
        Dim Result As String
        Dim errorMessage As String
        'Check If Mask is successful only if Foreign Is not checked
        If chkForeignZip.Checked = False Then
            errorMessage = ValidateFieldsWithInputMasks()
        Else
            errorMessage = ""
        End If

        If Not txtPhone.Text = "" Then
            If Not chkForeignPhone.Checked And txtPhone.Text.Length < 10 Then
                errorMessage &= IIf(errorMessage <> String.Empty, vbLf, String.Empty).ToString() _
                              + "Domestic phone numbers must include the area code and phone number and be 10 characters in length (Phone field)"
            End If
        End If
        If Not txtFax.Text = "" Then
            If Not chkForeignFax.Checked And txtFax.Text.Length < 10 Then
                errorMessage &= IIf(errorMessage <> String.Empty, vbLf, String.Empty).ToString() _
                              + "Domestic phone numbers must include the area code and phone number and be 10 characters in length (Fax field)"
            End If
        End If
        If errorMessage = "" Then
            'Call Update Function in the plEmployerInfoFacade
            Result = EmployerInfo.UpdateEmployerInfo(BuildEmployerInfo(txtEmployerId.Text), AdvantageSession.UserState.UserName)

            If Not Result = "" Then
                DisplayRADAlert(CallbackType.Postback, "Error1", Result, "Save Error")
                Exit Sub
            End If


            'Reset The Checked Property Of CheckBox To True
            chkIsInDB.Checked = True

            'Update The Jobs Offered(CheckBoxes)
            UpdateEmployerJobCats(txtEmployerId.Text)


            'If Page is free of errors 
            If Page.IsValid Then

                'Initialize Buttons
                InitButtonsForEdit()
            End If
            EnableBtnEmployerType()

            'Add MRU
            mruProvider.InsertMRU(2, txtEmployerId.Text, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)



            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            ' Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                SDFControl.DeleteSDFValue(txtEmployerId.Text)
                SDFID = SDFControl.GetAllLabels(pnlSDF)
                SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtEmployerId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            HandleInternationalAddress(chkForeignZip.Checked)
        Else
            DisplayErrorMessageMask(errorMessage)
        End If
    End Sub

    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False
        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Function ValidateFieldsWithInputMasks() As String
        Dim facInputMasks As New InputMasksFacade
        Dim correctFormat As Boolean
        Dim zipMask As String
        Dim errorMessage As String = String.Empty
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        'Validate the zip field format. If the field is empty we should not apply the mask
        'against it.
        If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtZip.Text)
            If correctFormat = False Then
                errorMessage &= "Incorrect format for zip field."
            End If
        End If

        Return errorMessage

    End Function
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If
        'btnNew.Enabled = True
        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = True
        End If
    End Sub
    Private Sub BuildDropDownLists(ByVal campusId As String)

        'Build All The DropDown List Controls With
        'Values from Database

        BuildStatusDDL()
        BuildStatesDDL()
        BuildCampusGroupsDDL()
        BuildCountyDDL()
        BuildEmployerFeeDDL()
        BuildEmployerIndustryDDL(campusId)
        BuildEmployerJobsDDL()
        BuildLocationDDL()
        BuildEmployerGroupDDL()
        BuildCountryDDL()
    End Sub
    Private Sub BuildStatusDDL()

        'Bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With
    End Sub
    Private Sub BuildStatesDDL()

        'Bind the states DrowDownList
        Dim states As New StatesFacade
        With ddlStateId
            .DataTextField = "StateDescrip"
            .DataValueField = "StateId"
            .DataSource = states.GetAllStates()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCampusGroupsDDL()

        'Bind the CampusGroups DrowDownList
        Dim campusGroups As New CampusGroupsFacade
        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCountyDDL()

        'Bind the County DrowDownList
        Dim county As New CountyFacade
        With ddlCountyId
            .DataTextField = "CountyDescrip"
            .DataValueField = "CountyId"
            .DataSource = county.GetAllCounty()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildEmployerFeeDDL()

        'Bind the Employer Fee DrowDownList
        Dim EmployerFee As New EmployerFeeFacade
        With ddlFeeId
            .DataTextField = "FeeDescrip"
            .DataValueField = "FeeId"
            .DataSource = EmployerFee.GetAllEmployerFee()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildEmployerIndustryDDL(ByVal campusId As String)

        'Bind the EmployerIndustry DrowDownList
        Dim Industry As New IndustryFacade
        With ddlIndustryId
            .DataTextField = "IndustryDescrip"
            .DataValueField = "IndustryId"
            .DataSource = Industry.GetAllIndustryForCampus(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildEmployerJobsDDL()
        'Bind the EmployerJobs DrowDownList
        Dim EmployerJobs As New EmployerJobsFacade
        With chkJobCatId
            .DataTextField = "JobGroupDescrip"
            .DataValueField = "JobGroupId"
            .DataSource = EmployerJobs.GetAllJobType()
            .DataBind()
        End With
    End Sub
    Private Sub BuildEmployerGroupDDL()
        'Bind the Employer Group DrowDownList
        Dim EmployerGroups As New EmployerJobsFacade
        With ddlParentId
            .DataTextField = "EmployerDescrip"
            .DataValueField = "EmployerId"
            .DataSource = EmployerGroups.GetAllEmployerGroup()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildLocationDDL()
        'Bind the Location DrowDownList
        Dim Location As New LocationFacade
        With ddlLocationId
            .DataTextField = "LocationDescrip"
            .DataValueField = "LocationId"
            .DataSource = Location.GetAllLocation()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCountryDDL()
        Dim Country As New PrefixesFacade
        With ddlCountryId
            .DataTextField = "CountryDescrip"
            .DataValueField = "CountryId"
            .DataSource = Country.GetAllCountries()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        End With
    End Sub
    Private Function BuildEmployerInfo(ByVal EmployerId As String) As plEmployerInfo
        Dim EmployerInfo As New plEmployerInfo
        Dim facInputMask As New InputMasksFacade
        'Dim phoneMask As String
        Dim zipMask As String


        With EmployerInfo

            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'Get EmployerId
            .EmployerId = txtEmployerId.Text

            'Get Code
            .Code = txtCode.Text

            'Get StatusId 
            .StatusId = ddlStatusId.SelectedValue

            'IsGroup
            .Group = chkGroupName.Checked

            'Get Description
            .Description = txtEmployerDescrip.Text

            'Get StateId
            If ddlStateId.SelectedValue = "" Then
                .StateId = Guid.Empty.ToString
            Else
                .StateId = ddlStateId.SelectedValue
            End If

            'Get CampusGroup
            .CampGrpId = ddlCampGrpId.SelectedValue

            'Get Address1
            .Address1 = txtAddress1.Text

            'Get Address2
            .Address2 = txtAddress2.Text

            'Get Phone
            .Phone = txtPhone.Text

            'Get City
            .City = txtcity.Text

            'Get Zip
            If txtZip.Text <> "" And chkForeignZip.Checked = False Then
                zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
                .Zip = facInputMask.RemoveMask(zipMask, txtZip.Text)
            Else
                .Zip = txtZip.Text
            End If

            'Get Location
            .Location = ddlLocationId.SelectedValue

            'Get County
            .County = ddlCountyId.SelectedValue

            'Get Email
            .Email = txtEmail.Text

            'Get Industry
            .IndustryId = ddlIndustryId.SelectedValue

            'Country
            .CountryId = ddlCountryId.SelectedValue

            'Get Fee
            .FeeId = ddlFeeId.SelectedValue

            'Get EmployerGroup 
            .ParentId = ddlParentId.SelectedValue

            'Get Fax
            .Fax = txtFax.Text

            'Foreign Phone
            .ForeignPhone = chkForeignPhone.Checked

            'Foreign Fax
            .ForeignFax = chkForeignFax.Checked

            'Foreign Zip
            .ForeignZip = chkForeignZip.Checked

            .OtherState = txtOtherState.Text

            .CountryId = ddlCountryId.SelectedValue

            txtModDate.Text = CType(Date.Now, String)

            .ModDate = CType(txtModDate.Text, Date)
        End With
        Return EmployerInfo
    End Function
    Private Sub BindEmployerInfoData(ByVal EmployerInfo As plEmployerInfo)

        'Bind The EmployerInfo Data From The Database
        Dim facInputMasks As New InputMasksFacade
        Dim zipMask As String

        'Get the mask for phone numbers and zip
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        With EmployerInfo
            'Get Foreign phone Checkbox
            chkForeignPhone.Checked = .ForeignPhone

            'Get Foreign Fax Checkbox
            chkForeignFax.Checked = .ForeignFax

            txtEmployerId.Text = .EmployerId
            txtCode.Text = .Code
            'ddlStatusId.SelectedValue = .StatusId
            ddlCampGrpId.SelectedValue = .CampGrpId
            txtEmployerDescrip.Text = .Description
            txtAddress1.Text = .Address1
            txtAddress2.Text = .Address2
            txtcity.Text = .City
            txtEmail.Text = .Email

            txtZip.Text = .Zip
            If txtZip.Text <> "" Then
                txtZip.Text = facInputMasks.ApplyMask(zipMask, txtZip.Text)
            End If

            'Get Phone
            SetPhoneMask(chkForeignPhone.Checked, txtPhone)
            txtPhone.Text = .Phone

            'Get Phone
            SetPhoneMask(chkForeignFax.Checked, txtFax)
            txtFax.Text = .Phone

            If .StateId <> Guid.Empty.ToString Then
                ddlStateId.SelectedValue = .StateId
            Else
                BuildStatesDDL()
            End If
            If .CountyId <> Guid.Empty.ToString Then
                ddlCountyId.SelectedValue = .County
            Else
                BuildCountyDDL()
            End If
            If .CountryId <> Guid.Empty.ToString Then
                ddlCountryId.SelectedValue = .CountryId
            Else
                BuildCountryDDL()
            End If
            If .LocationId <> Guid.Empty.ToString Then
                ddlLocationId.SelectedValue = .Location
            Else
                BuildLocationDDL()
            End If
            If .FeeId <> Guid.Empty.ToString Then
                ddlFeeId.SelectedValue = .FeeId
            Else
                BuildEmployerFeeDDL()
            End If
            If .IndustryId <> Guid.Empty.ToString Then
                ddlIndustryId.SelectedValue = .IndustryId
            Else
                BuildEmployerIndustryDDL(Master.CurrentCampusId)
            End If
            'Get EmployerGroup 
            .ParentId = ddlParentId.SelectedValue
            chkJobCatId.ClearSelection()
            chkForeignZip.Checked = False
            chkForeignFax.Checked = False
            txtModDate.Text = .ModDate.ToString()

            Try
                ddlCountryId.SelectedValue = strDefaultCountry
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                ddlCountryId.SelectedIndex = 0
            End Try
        End With
        BuildStatusDDL()
    End Sub
    Private Sub UpdateEmployerJobCats(ByVal empId As String)

        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        'Dim selectedDegrees() As String = selectedDegrees.CreateInstance(GetType(String), chkJobCatId.Items.Count)
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), chkJobCatId.Items.Count)

        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In chkJobCatId.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)

        'update Selected Jobs
        Dim employerJobs As New PlEmployerInfoFacade
        If employerJobs.UpdateEmployerJobCats(empId, AdvantageSession.UserState.UserName, selectedDegrees) < 0 Then
            'DisplayErrorMessage("A related record exists,you can not perform this operation")
            DisplayRADAlert(CallbackType.Postback, "Error1", "A related record exists,you can not perform this operation", "Save Error")
            Exit Sub
        End If
    End Sub
    Private Sub BindEmployerJobsListBox(ByVal empId As String)

        'Get Degrees data to bind the CheckBoxList
        Dim employees As New PlEmployerInfoFacade
        Dim ds As DataSet = employees.GetJobsOffered(empId)
        Dim JobCount As Integer = employees.GetValidJobsOffered(empId)

        If JobCount >= 1 Then

            'Select the items on the CheckBoxList
            Dim i As Integer
            If Not (ds.Tables(0).Rows.Count < 0) Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim row As DataRow
                    row = ds.Tables(0).Rows(i)
                    Dim item As ListItem
                    For Each item In chkJobCatId.Items
                        If item.Value.ToString = DirectCast(row("JobCatId"), Guid).ToString Then
                            item.Selected = True
                            Exit For
                        End If
                    Next
                Next
            End If
        End If
    End Sub
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPhone.CheckedChanged
        SetPhoneMask(chkForeignPhone.Checked, txtPhone)
        txtPhone.Focus()
    End Sub
    Private Sub chkForeignFax_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignFax.CheckedChanged
        SetPhoneMask(chkForeignFax.Checked, txtFax)
        txtFax.Focus()
    End Sub
    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignZip.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        'objCommon.SetFocus(Me.Page, txtAddress1)
        CommonWebUtilities.SetFocus(Me.Page, txtAddress1)

        HandleInternationalAddress(chkForeignZip.Checked)
    End Sub
    Private Sub ddlStateId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStateId.SelectedIndexChanged
        If ddlStateId.SelectedIndex = 1 Then
            txtOtherState.Enabled = True
            Dim objCommon As New CommonWebUtilities
            'objCommon.SetFocus(Me.Page, txtOtherState)
            CommonWebUtilities.SetFocus(Me.Page, txtOtherState)
        End If
    End Sub
    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOtherState.TextChanged
        Dim objCommon As New CommonWebUtilities
        'objCommon.SetFocus(Me.Page, txtZip)
        CommonWebUtilities.SetFocus(Me.Page, txtZip)
    End Sub
    Private Sub txtcity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtcity.TextChanged
        Dim objCommon As New CommonWebUtilities
        If chkForeignZip.Checked = True Then
            'objCommon.SetFocus(Me.Page, txtOtherState)
            CommonWebUtilities.SetFocus(Me.Page, txtOtherState)
        End If
    End Sub
    ''' <summary>
    ''' Set phone mask
    ''' </summary>
    ''' <param name="isInternational">true set international mask else national mask</param>
    ''' <param name="Phone">the control affectred for the Internantional param </param>
    ''' <remarks></remarks>
    Private Sub SetPhoneMask(ByVal isInternational As Boolean, ByVal Phone As RadMaskedTextBox)
        Dim strPhone As String = Phone.Text
        If isInternational = False Then
            Phone.Mask = "(###)-###-####"
            Phone.DisplayMask = "(###)-###-####"
            Phone.DisplayPromptChar = ""
        Else
            Phone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            Phone.DisplayMask = ""
            Phone.DisplayPromptChar = ""
        End If
        Phone.Text = strPhone

    End Sub
    'Private Function BuildEmployerStateObject(ByVal EmployerId As String) As String
    '    Dim objStateInfo As New AdvantageStateInfo
    '    Dim objGetStudentStatusBar As New AdvantageStateInfo
    '    Dim strVID As String
    '    Dim facInputMasks As New InputMasksFacade
    '    Dim strEmployerId As String = ""

    '    If EmployerId.ToString.Trim = "" Then 'No Student was selected from MRU
    '        strEmployerId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
    '                                                               2, _
    '                                                               AdvantageSession.UserState.CampusId.ToString)
    '    Else
    '        strEmployerId = EmployerId
    '    End If

    '    objGetStudentStatusBar = mruProvider.BuildEmployerStatusBar(strEmployerId)
    '    With objStateInfo
    '        .EmployerId = objGetStudentStatusBar.EmployerId
    '        .NameValue = objGetStudentStatusBar.NameValue
    '        .Address1 = objGetStudentStatusBar.Address1
    '        .Address2 = objGetStudentStatusBar.Address2
    '        .City = objGetStudentStatusBar.City
    '        .State = objGetStudentStatusBar.State
    '        .Zip = objGetStudentStatusBar.Zip
    '        .Phone = objGetStudentStatusBar.Phone
    '    End With

    '    'Create a new guid to be associated with the employer pages
    '    strVID = Guid.NewGuid.ToString

    '    'Add an entry to AdvantageSessionState for this guid and object
    '    'load Advantage state
    '    state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
    '    state(strVID) = objStateInfo
    '    'save current State
    '    CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

    '    'UpdateMRUTable
    '    'Reason: We need to keep track of the last student record the user worked with
    '    'Scenario1 : User can log in and click on a student page without using MRU 
    '    'and we need to display the data of the last student the user worked with
    '    'If the user is a first time user, we will load the student who was last added to advantage
    '    mruProvider.InsertMRU(2, strEmployerId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

    '    Return strVID
    'End Function
    'Private Sub BuildEmployerMRU()
    '    Dim lstEmployerMRU As List(Of BO.EmployerMRU) = mruProvider.BuildEmployerMRU(AdvantageSession.UserState)
    '    Dim MainPanelBar As RadPanelBar = CType(FindControlRecursive("RadPanelBar1"), RadPanelBar)
    '    Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), Telerik.Web.UI.RadPanelItem).Controls(3), Telerik.Web.UI.RadGrid)
    '    grdMRU.ToolTip = ""
    '    With grdMRU
    '        .DataSource = lstEmployerMRU
    '        .DataBind()
    '    End With
    '    'Change the Icons to match the MRU Dropdown icon
    '    Try
    '        'If there is only one item in the MRU hide delete button
    '        If grdMRU.Items.Count = 1 Then
    '            CType(grdMRU.Items(0).FindControl("imgDeleteMRU"), ImageButton).Visible = False
    '            Exit Try
    '        End If
    '        For i = 0 To 19 'The List will always have 20 or less than 20 employers
    '            CType(grdMRU.Items(i).FindControl("imgDeleteMRU"), ImageButton).Visible = True
    '            CType(grdMRU.Items(i).FindControl("Image1"), Image).ImageUrl = "~/images/placement.png"
    '        Next
    '    Catch ex As Exception
    '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '    End Try

    'End Sub

    Public Sub HandleInternationalAddress(ByVal isInternationalAddress As Boolean)
        Dim ctl2 As Control
        ctl2 = CType(Master.FindControl("ContentMain1"), ContentPlaceHolder).FindControl("pnlRequiredFieldValidators")

        If isInternationalAddress = True Then
            txtOtherState.Visible = True
            lblOtherState.Visible = True
            ddlStateId.Enabled = False
            ddlStateId.SelectedIndex = 0
            txtZip.Mask = "####################"
            txtZip.DisplayMask = "####################"

            If chkIsInDB.Checked = False Then
                txtZip.Text = String.Empty
            End If

            'Remove the required field validator for the ddlStateId field
            Dim ctl As Control

            Dim rfvId As String = ""

            For Each c As Control In ctl2.Controls
                If c.ID.Contains("rfvStateId") Then
                    rfvId = c.ID
                End If
            Next

            If rfvId <> "" Then
                ctl = ctl2.FindControl(rfvId)
                ctl2.Controls.Remove(ctl)
            End If

            'Remove the red asterisk
            lblStateId.Text = "State"

        Else
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            ddlStateId.Enabled = True
            txtZip.Mask = "#####"
            txtZip.DisplayMask = "#####"

            If chkIsInDB.Checked = False Then
                txtZip.Text = String.Empty
            End If

            'Add back the red asterisk
            lblStateId.Text = "State <font color=""red"">*</font>"

        End If
    End Sub

End Class