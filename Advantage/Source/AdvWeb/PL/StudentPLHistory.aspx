<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master"
    Inherits="StudentPLHistory" CodeFile="StudentPlHistory.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>Student Placement History</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstEmployerContact">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <%--LEFT PANE CONTENT HERE--%>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop">
                        <asp:Label ID="lblEnrollmentType" CssClass="totheme6" runat="server"><b>
											Enrollment</b></asp:Label><br>
                        <asp:DropDownList ID="ddlEnrollmentTypeId" CssClass="dropdownlist" runat="server"
                            AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfltr1row">
                            <asp:DataList ID="dlstEmployerContact" runat="server">
                                <SelectedItemStyle CssClass="selecteditem"></SelectedItemStyle>
                                <SelectedItemTemplate>
                                </SelectedItemTemplate>
                                <ItemStyle CssClass="nonselecteditem"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# Container.DataItem("EmployerDescrip") & " - " & Container.DataItem("EmployerJobTitle") %>'
                                        runat="server" CssClass="nonselecteditem" CommandArgument='<%# Container.DataItem("PlacementId")%>'
                                        ID="Linkbutton2" CausesValidation="False" />
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Orientation="HorizontalTop">
            <asp:Panel ID="pnlRHS" runat="server">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete"
                                    CausesValidation="False"></asp:Button>
                        </td>
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none;">
                    <tr>
                        <td class="detailsframe">
                            <div class="boxContainer">
                                <h3><%=Header.Title  %></h3>
                                <!-- begin content table-->
                                <%-- MAIN CONTENT HERE--%>

                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblEmployerId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlEmployerId" TabIndex="1" runat="server" AutoPostBack="True" Width="200px" 
                                                CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblSalary" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtSalary" TabIndex="11" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblEmployerJobId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlJobTitleId" TabIndex="2" runat="server" CssClass="dropdownlist" Width="200px" 
                                                Visible="True">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblSalaryTypeId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlSalaryTypeId" TabIndex="12" runat="server" CssClass="dropdownlist" Width="200px" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblJobDescrip" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtJobDescrip" TabIndex="3" runat="server" CssClass="textbox" Columns="21" Width="200px"
                                                Rows="4" TextMode="MultiLine" MaxLength="200"></asp:TextBox>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblScheduleId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlScheduleId" TabIndex="13" CssClass="dropdownlist" runat="server" Width="200px" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblPlacementRep" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlPlacementRep" TabIndex="4" runat="server" CssClass="dropdownlist" Width="200px" >
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblBenefitsId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlBenefitsId" TabIndex="14" CssClass="dropdownlist" runat="server" Width="200px" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblJobStatusId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlJobStatusId" TabIndex="5" runat="server" CssClass="dropdownlist" Width="200px" >
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblHowPlacedId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlHowPlacedId" TabIndex="15" runat="server" CssClass="dropdownlist" Width="200px" >
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblInterViewId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlInterViewId" TabIndex="6" runat="server" CssClass="dropdownlist" Width="200px" >
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblPlacedDate" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                      
                                            <telerik:RadDatePicker ID="txtPlacedDate" runat="server" Width="200px" >
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblSupervisor" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtSupervisor" TabIndex="7" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td class="contentcell">
                                            <asp:Label ID="lblStartDate" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                    
                                            <telerik:RadDatePicker ID="txtStartDate" runat="server" Width="200px" >
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblFldStudyId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlFldStudyId" TabIndex="8" runat="server" CssClass="dropdownlist" Width="200px" >
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblTerminationDate" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                        
                                            <telerik:RadDatePicker ID="txtTerminationDate" runat="server" MinDate="1/1/1945" Width="200px" >
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblFee" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlFee" TabIndex="9" runat="server" CssClass="dropdownlist" Width="200px" >
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td class="contentcell">
                                            <asp:Label ID="lblTerminationReason" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtTerminationReason" TabIndex="19" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblStuEnrollId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlStuEnrollId" TabIndex="10" runat="server" CssClass="dropdownlist" Width="200px" >
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td class="contentcell">&nbsp;
                                        </td>
                                        <td class="contentcell4">&nbsp;
                                        </td>
                                    </tr>
    
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblNotes" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell2" colspan="6">
                                            <asp:TextBox ID="txtNotes" TabIndex="20" CssClass="ToCommentsNoWrap" runat="server" Width="200px"
                                                Columns="60" TextMode="MultiLine" MaxLength="300" Rows="3"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <asp:CheckBox ID="chkIsInDB" runat="server" Visible="false"></asp:CheckBox>
                                    <asp:TextBox ID="txtPlacementId" runat="server" Visible="False"></asp:TextBox>
                                </table>

                                <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" nowrap colspan="6">
                                                <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell2" colspan="6">
                                                <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <!--end content table-->
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <asp:TextBox ID="txtfirstname" runat="server" Visible="False"></asp:TextBox><asp:TextBox
        ID="txtmiddlename" runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtlastname"
            runat="server" Visible="False"></asp:TextBox>
</asp:Content>
