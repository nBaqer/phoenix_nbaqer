<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master" Inherits="ExitInterview" CodeFile="ExitInterView.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstEmployerContact">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <%--LEFT PANE CONTENT HERE--%>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframenofilterblue">No Filter Applicable</td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfltr1row">
                            <asp:DataList ID="dlstEmployerContact" runat="server">
                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton Text='<%# DataBinder.Eval(Container.DataItem,"AvailableDate", "{0:d}") & " - " &  Container.DataItem("PrgVerDescrip")%>'
                                        runat="server" CssClass="nonselecteditem" CommandArgument='<%# Container.DataItem("ExtInterviewID")%>'
                                        ID="Linkbutton2" CausesValidation="False" />
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <asp:Panel ID="pnlRHS" runat="server">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                    ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                        </td>
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%; border: none;">
                    <tr>
                        <td class="detailsframe">
                            <div class="boxContainer">
                                <h3><%=Header.Title  %></h3>
                                <!-- begin content table-->


                                <table width="100%" cellpadding="0" cellspacing="0" class="contenttable">
                                    <tr>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblPrgVerId" runat="server" CssClass="label">Enrollment</asp:Label></td>
                                        <td class="contentcell4" align="left">
                                            <asp:DropDownList ID="ddlPrgVerId" runat="server" CssClass="dropdownlist" Width="200px"
                                                TabIndex="1">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblExitInterviewDate" runat="server" CssClass="label">Exit Interview Date<font color="red">*</font></asp:Label></td>
                                        <td class="contentcell4">
                                            <telerik:RadDatePicker ID="txtExitInterviewDate" runat="server" Width="200px" daynameformat="Short">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblSchoolStatusId" runat="server" CssClass="label">Placement Status<font color="red">*</font></asp:Label></td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlScStatusId" runat="server" CssClass="dropdownlist" TabIndex="2" Width="200px">
                                            </asp:DropDownList></td>
                                        <td class="emptycell"></td>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblAvailableDate" runat="server" CssClass="label">Available Date</asp:Label></td>
                                        <td class="contentcell4">
                                            <telerik:RadDatePicker ID="txtAvailableDate" runat="server" Width="200px" daynameformat="Short">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblWantsAssistance" runat="server" CssClass="label">Want Assistance</asp:Label></td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="rblWantsAssistance" runat="server" CssClass="dropdownlist" TabIndex="7" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblAvailableDays" runat="server" CssClass="label">Available Days</asp:Label></td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtAvailableDays" runat="server" CssClass="textbox" TabIndex="9"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblWaiverSigned" runat="server" CssClass="label">Waiver Signed</asp:Label></td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="rblWaiverSigned" runat="server" CssClass="dropdownlist" TabIndex="7" Width="200px">
                                            </asp:DropDownList>

                                        </td>
                                        <td class="emptycell"></td>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblAvailableHours" runat="server" CssClass="label">Available Hours</asp:Label></td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtAvailableHours" runat="server" CssClass="textbox" TabIndex="10"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblAreaId" runat="server" CssClass="label">County</asp:Label></td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlAreaId" runat="server" CssClass="dropdownlist" TabIndex="7" Width="200px">
                                            </asp:DropDownList></td>
                                        <td class="emptycell"></td>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblLowSalary" runat="server" CssClass="label">Min Salary</asp:Label></td>
                                        <td class="contentcell4" nowrap>
                                            <asp:TextBox runat="server" ID="txtLowSalary" CssClass="textbox" TabIndex="11"  /><asp:Label
                                                ID="lblCurrencyTo" runat="server" CssClass="label">&nbsp;US Dollars</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblTransportationId" runat="server" CssClass="label">Transportation</asp:Label></td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlTransportationId" runat="server" CssClass="dropdownlist" Width="200px" 
                                                TabIndex="13">
                                            </asp:DropDownList></td>
                                        <td class="emptycell"></td>
                                        <td nowrap class="contentcell">
                                            <asp:Label ID="lblHighSalary" runat="server" CssClass="label">Max Salary</asp:Label></td>
                                        <td class="contentcell4" nowrap>
                                            <asp:TextBox runat="server" ID="txtHighSalary" CssClass="textbox" TabIndex="12" /><asp:Label
                                                ID="Label1" runat="server" CssClass="label">&nbsp;US Dollars</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap class="contentcell" valign="top">
                                            <asp:Label ID="lblEligible" runat="server" CssClass="label">Eligible <font color="red">*</font></asp:Label></td>
                                        <td class="contentcell4">

                                            <asp:DropDownList ID="rblEligible" runat="server" CssClass="dropdownlist" TabIndex="7" AutoPostBack="true" OnSelectedIndexChanged="EligibleNO" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td nowrap class="contentcell" style="min-width: 125px">
                                            <div style="width: 100px">
                                                <asp:Label ID="Reason" runat="server" CssClass="label">Ineligibility Reasons<font color="red">*</font></asp:Label>
                                            </div>
                                        </td>

                                        <td class="contentcell4" align="left">
                                            <asp:DropDownList ID="ddlReason" runat="server" CssClass="dropdownlist" TabIndex="7" Width="200px">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell"></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                            <tr>
                                <td nowrap class="contentcell">
                                    <asp:Label ID="lblFullTimeId" runat="server" CssClass="label" Visible="False">Full/Part Time</asp:Label></td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlFullTimeId" runat="server" CssClass="dropdownlist" TabIndex="7" Width="200px"
                                        Visible="False">
                                    </asp:DropDownList></td>
                                <td class="emptycell"></td>
                                <td nowrap class="contentcell">
                                    <asp:Label ID="lblExpertiseLevelId" runat="server" CssClass="label" Visible="False">Expertise Level</asp:Label></td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlExpertiseLevelId" runat="server" CssClass="dropdownlist" Width="200px"
                                        TabIndex="13" Visible="False">
                                    </asp:DropDownList></td>
                            </tr>
                </table>
                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" hidden>
                    <tr>
                        <td class="contentcell" nowrap>
                            <asp:Label ID="lblReason" runat="server" CssClass="label" Visible="True"></asp:Label></td>
                        <td class="contentcell2" colspan="5">
                            <asp:TextBox ID="txtReason" TabIndex="6" runat="server" CssClass="ToCommentsNoWrap"
                                MaxLength="300" TextMode="MultiLine" Rows="3" Columns="60"></asp:TextBox></td>
                    </tr>
                </table>
                <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                            <td class="spacertables"></td>
                        </tr>
                        <tr>
                            <td class="contentcellheader" nowrap colspan="6">
                                <asp:Label ID="lblSDF" runat="server" Font-Bold="true" CssClass="label">School Defined Fields</asp:Label></td>
                        </tr>
                        <tr>
                            <td class="spacertables"></td>
                        </tr>
                        <tr>
                            <td class="contentcell2" colspan="6">
                                <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <!-- End Content Table -->


                </div>
        </td>
        </tr>
        </table>	
            </asp:Panel>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server" CssClass="ValidationSummary">
        <asp:CustomValidator ID="CustomValidator1" CssClass="ValidationSummary" runat="server"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
            ShowSummary="False"></asp:ValidationSummary>
    </asp:Panel>
    <asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
    <asp:TextBox ID="txtEnrollmentID" runat="server" Visible="False"></asp:TextBox>
    <asp:TextBox ID="txtExitInterviewId" runat="server" Visible="false"></asp:TextBox>
    <!-- end footer -->
</asp:Content>

