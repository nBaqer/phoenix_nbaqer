﻿Imports FAME.Common
Imports System
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports FAME.Advantage.Common
Imports BL = Advantage.Business.Logic.Layer

Partial Class SearchLead
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblStudentID As System.Web.UI.WebControls.Label
    Protected WithEvents txtStudentID As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlStatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents dgrdStudentSearch As System.Web.UI.WebControls.DataGrid
    Protected WithEvents StudentLNameLinkButton As System.Web.UI.WebControls.LinkButton
    Protected state As AdvantageSessionState
    Protected WithEvents lblMI As System.Web.UI.WebControls.Label
    Protected WithEvents txtMiddleName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtDOB As System.Web.UI.WebControls.TextBox
    Private m_context As HttpContext
    Protected campusId, userId, resourceId As String
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()


        mruProvider = New BL.MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))

        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state("empInfo") = Nothing
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        ''Call mru component for employees
        'Dim objMRUFac As New MRUFacade
        'Dim ds As New DataSet

        'ds = objMRUFac.LoadMRU("Leads", userId, HttpContext.Current.Request.Params("cmpid"))

        ''load Advantage state
        'state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        'state("MRUDS") = ds
        ''save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo

    Private mruProvider As BL.MRURoutines
    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        '        Dim m_Context2 As HttpContext

        Me.Form.DefaultButton = btnSearch.UniqueID
        SetFocusOnControl("txtLastName")
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        campusid = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        If Not Page.IsPostBack Then
            'BuildLeadStatusDDL()
            'BuildCampusGroupsDDL()
            'BuildProgramsDDL()
            'BuildLeadDOBDDL()
            BuildDropDownLists()
            Session("NameCaption") = ""
            Session("NameValue") = ""
            Session("IdValue") = ""
            Session("IdCaption") = ""
            Session("EmployerID") = Nothing
            Session("StudentID") = ""
        End If
        GetInputMaskValue()
    End Sub
    Private Sub BuildDropDownLists()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.LeadStatus, campusId, True, True))
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
        '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
        BuildCampusDDL()

        '' Code Added on 23 August 2010 to Resolve mantis issue id 19606
        ddlAdmissionRepID.Items.Insert(0, New ListItem("Select", ""))
        ''

    End Sub
    Private Sub GetInputMaskValue()
        Dim facInputMasks As New InputMasksFacade
        Dim objCommon As New CommonUtilities
        Dim ssnMask As String

        'Apply Mask Only If Its Local Phone Number
        'Get The Input Mask for Phone/Fax and Zip
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        'Replace The Mask Character from # to 9 as Masked Edit TextBox 
        'accepts only certain characters as mask characters
        'txtSSN.Mask = Replace(ssnMask, "#", "9")
        'Get The Format Of the input masks and display it next to caption
        'labels
        lblSSN.ToolTip = ssnMask

        'Get The RequiredField Value
        Dim strSSNReq As String

        strSSNReq = objCommon.SetRequiredColorMask("SSN")

        'If The Field Is Required Field Then Color The Masked
        'Edit Control
        If strSSNReq = "Yes" Then
            '   ssnreqasterisk.Visible = True
            'txtSSN.BackColor = Color.FromName("#ffff99")
        End If
    End Sub

    Private Function RemoveHypen(ByVal strSSN As String) As String
        Dim strRemoveHypenSSN As String = String.Empty
        Dim chr As Char
        For Each chr In strSSN
            If Not chr = "-" Then
                strRemoveHypenSSN &= chr
            End If
        Next
        Return strRemoveHypenSSN
    End Function
    'Private Sub BuildLeadStatusDDL()
    '    Dim Statusfacade As New StatusCodeFacade
    '    Dim strLeadStatusId As String = ""
    '    With ddlStatusId
    '        .DataTextField = "StatusCodeDescrip"
    '        .DataValueField = "StatusCodeID"
    '        '.DataSource = Statusfacade.GetAvailLeadStatuses(strLeadStatusId, CampusId, userId) 'Status.GetNewLeadStatus()
    '        '.DataSource = Statusfacade.GetSearchAvailLeadStatuses(strLeadStatusId, campusId, userId)
    '        .DataSource = Statusfacade.PopulateDataList(1, "F23DE1E2-D90A-4720-B4C7-0F6FB09C9965")
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildLeadDOBDDL()
    '    Dim DOB As New StudentSearchFacade
    '    With ddlDOB
    '        .DataTextField = "BirthDate"
    '        .DataTextFormatString = "{0:d}"
    '        .DataValueField = "BirthDate"
    '        .DataSource = DOB.GetLeadDOB()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildCampusGroupsDDL()
    'End Sub
    'Private Sub BuildProgramsDDL()
    'End Sub
    Private Sub BindDataList()
        'Bind The First,Middle,LastName based on EmployerContactId
        Dim userId As String
        Dim campusId As String
        Dim strSSN As String


        campusId = HttpContext.Current.Request.Params("cmpid")

        If txtSSN.Text.Length >= 1 Then
            strSSN = RemoveHypen(txtSSN.Text)
        Else
            strSSN = ""
        End If
        Dim SearchFacade As New StudentSearchFacade
        ''dgrdTransactionSearch.DataSource = SearchFacade.LeadSearchResults(txtLastName.Text, txtFirstName.Text, strSSN, Guid.Empty.ToString, ddlStatusId.SelectedValue, txtPhone.Text, userId, campusId)

        '' Code Added/Modified by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
        Dim LeadCampuses As String

        If MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "ownleads" Then
            LeadCampuses = "ownleads"
        ElseIf MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "permissiblecampuses" Then
            LeadCampuses = "permissiblecampuses"
        ElseIf MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "allcampuses" Then
            '''' Code changed to resolve mantis issue id 19549 by kamalesh Ahuja on 17th August 2010
            ''If SingletonAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "yes" Then
            LeadCampuses = "allcampuses"
            ''Else
            ''  LeadCampuses = "permissiblecampuses"
            ''End If
            '''''
        Else
            LeadCampuses = "ownleads"
        End If

        dgrdTransactionSearch.Visible = True

        userId = AdvantageSession.UserState.UserId.ToString

        '' Code changes made on 17 August 2010 to resolve mantis issue id 19560
        ''dgrdTransactionSearch.DataSource = SearchFacade.LeadSearchResults(txtLastName.Text, txtFirstName.Text, strSSN, Guid.Empty.ToString, ddlStatusId.SelectedValue, txtPhone.Text, userId, ddlCampusID.SelectedValue, LeadCampuses, IIf(SingletonAppSettings.AppSettings("EditOtherLeads").ToLower = "yes", True, False))
        dgrdTransactionSearch.DataSource = SearchFacade.LeadSearchResults(txtLastName.Text, txtFirstName.Text, strSSN, Guid.Empty.ToString, ddlStatusId.SelectedValue, txtPhone.Text, userId, ddlCampusID.SelectedValue, LeadCampuses, IIf(MyAdvAppSettings.AppSettings("EditOtherLeads").ToLower = "yes", True, False), ddlAdmissionRepID.SelectedValue)
        ''''''''''''''''''''''''''''''''''
        dgrdTransactionSearch.DataBind()
    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        ' Using (New WaitCursor) 'This line of code changes the mouse pointer to Hour Glass During search and then reset after search is complete
        'Dim hourglassCursor As New WaitCursor
        'If txtLastName.Text = "" And txtFirstName.Text = "" And txtSSN.Text = "" And ddlDOB.SelectedValue = Guid.Empty.ToString And ddlStatusId.SelectedValue = Guid.Empty.ToString And txtPhone.Text = "" Then
        'DE10011 8/30/2013 Janet Robinson removed ddlDOB from if stmt
        If txtLastName.Text = "" And txtFirstName.Text = "" And txtSSN.Text = "" And ddlStatusId.SelectedValue = Guid.Empty.ToString And txtPhone.Text = "" Then
            'DisplayErrorMessage("Please enter a value to search ")
            DisplayRADAlert(CallbackType.Postback, "Error1", "Please enter a value to search ", "Search Error")
            Exit Sub
        End If

        Dim strErrorMessage As String = String.Empty

        'Validate LastName % Character Not Allowed
        If txtLastName.Text.Length >= 1 Then
            If InStr(txtLastName.Text, "%") >= 1 Then
                strErrorMessage = "% is not allowed in last name " & vbLf
            End If
        End If

        'Validate FirstName % Character Not Allowed
        If txtFirstName.Text.Length >= 1 Then
            If InStr(txtFirstName.Text, "%") >= 1 Then
                strErrorMessage &= "% is not allowed in first name " & vbLf
            End If
        End If

        'SSN % not allowed
        If txtSSN.Text.Length >= 1 Then
            If InStr(txtSSN.Text, "%") >= 1 Then
                strErrorMessage &= "% is not allowed in ssn " & vbLf
            End If
        End If

        'Phone % not allowed
        If txtPhone.Text.Length >= 1 Then
            If InStr(txtPhone.Text, "%") >= 1 Then
                strErrorMessage &= "% is not allowed in phone " & vbLf
            End If
        End If

        If Not strErrorMessage = "" Then
            'DisplayErrorMessage(strErrorMessage)
            DisplayRADAlert(CallbackType.Postback, "Error2", strErrorMessage, "Search Error")
            Exit Sub
        End If

        Session("StudentType") = "ExistingStudent"
        BindDataList()
        Session("NameCaption") = ""
        Session("NameValue") = ""
        Session("IdValue") = ""
        Session("IdCaption") = ""
        'End Using
        'hourglassCursor.Dispose()
    End Sub
    Private Sub dgrdTransactionSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTransactionSearch.ItemCommand
        'Dim StudentName As New StudentSearchFacade
        Dim defaultCampusId As String
        'Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String
        '        Dim state As AdvantageSessionState
        Dim fac As New UserSecurityFacade
        Dim arrUpp As ArrayList
        Dim pURL As String

        'defaultCampusId = HttpContext.Current.Request.Params("cmpid")
        'defaultCampusId = XmlConvert.ToGuid(defaultCampusId).ToString



        If e.CommandName = "LeadSearch" Then
            'save studentId in the session so that it can be used by the StudentLedger program
            'Session("LeadID") = e.CommandArgument
            'Session("NameCaption") = "Lead"
            'Session("NameValue") = StudentName.GetLeadNameByID(Session("LeadID"))
            'Session("IdValue") = ""
            'Session("IdCaption") = ""
            Session("SEARCH") = 1
            'Set relevant properties on the state object
            strVID = BuildLeadStateObject(e.CommandArgument.ToString)

            'Dim strCampusId As String = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString()


            'Get the Latest CampusId from lead data
            defaultCampusId = AdvantageSession.UserState.CampusId.ToString

            Dim intSchoolOption As Integer = CommonWebUtilities.SchoolSelectedOptions(MyAdvAppSettings.AppSettings("SchedulingMethod"))
            'DE7914
            'If Not ddlCampusID.SelectedValue = Guid.Empty.ToString Then
            '    strCampusId = ddlCampusID.SelectedValue.ToString
            '    defaultCampusId = strCampusId
            '    Dim advResetUserState As New BO.User
            '    advResetUserState = AdvantageSession.UserState
            '    With advResetUserState
            '        .CampusId = New Guid(defaultCampusId)
            '    End With
            '    AdvantageSession.UserState = advResetUserState
            'End If


            AdvantageSession.MasterLeadId = txtLeadId.Text.ToString.Trim



            arrUpp = mruProvider.checkEntityPageSecurityStudentSearch(AdvantageSession.UserState, 189, defaultCampusId, 395, intSchoolOption)
            If MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "allcampuses" Then
                'arrUPP = mruProvider.checkEntityPageSecurityStudentSearchAllCampuses(AdvantageSession.UserState, 189, strCampusId, 395, intSchoolOption)
                Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                Dim strVSI As String = R.Next()

                'DE8026
                If arrUpp.Count = 0 Then
                    'User does not have permission to any resource for this submodule
                    RadNotification1.Show()
                    RadNotification1.Text = "You do not have permission to access any of the existing lead pages in the current campus"
                    Exit Sub
                End If

                fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, defaultCampusId, False)
                If arrUpp.Count >= 1 And mruProvider.DoesUserHasAccessToSubModuleResource(arrUpp, 170) = True Then
                    mruProvider.InsertMRU(4, txtLeadId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, defaultCampusId)
                    Response.Redirect("../AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=4" + "&VSI=" + strVSI, True)
                Else
                    pURL = BuildPartialURL(arrUpp(0))
                    mruProvider.InsertMRU(4, txtLeadId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, defaultCampusId)
                    Response.Redirect(pURL & "&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=4", True)
                End If
            Else
                'arrUPP = mruProvider.checkEntityPageSecurityStudentSearch(AdvantageSession.UserState, 189, strCampusId, 395, intSchoolOption)
                If arrUpp.Count = 0 Then
                    'User does not have permission to any resource for this submodule
                    'Session("Error") = "You do not have permission to any of the pages for existing lead<br> for the campus that you are logged in to."
                    'Response.Redirect("../ErrorPage.aspx")
                    RadNotification1.Show()
                    RadNotification1.Text = "You do not have permission to any of the pages for existing lead for the campus that you are logged in to."
                ElseIf mruProvider.DoesUserHasAccessToSubModuleResource(arrUpp, 170) Then
                    AdvantageSession.LeadMRU = Nothing
                    Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                    Dim strVsi As String = R.Next()
                    fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)
                    mruProvider.InsertMRU(4, txtLeadId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, defaultCampusId)
                    'CType(Master.FindControl("CampusSelector"), RadComboBox).SelectedValue = defaultCampusId
                    Response.Redirect("../AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=4" + "&VSI=" + strVsi, True)
                Else
                    'redirect to the first page that the user has permission to for the submodule
                    pURL = BuildPartialURL(arrUpp(0))
                    mruProvider.InsertMRU(4, txtLeadId.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, defaultCampusId)
                    Response.Redirect(pURL & "&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=4", True)
                End If
            End If
        End If
    End Sub
    Private Function BuildPartialURL(ByVal uppInfo As UserPagePermissionInfo) As String
        Return uppInfo.URL & "?resid=" & uppInfo.ResourceId
    End Function

    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)
    '    'Set error condition
    '    'Display error in message box in the client
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub
    '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
    Private Sub BuildCampusDDL()
        'Bind the CampusGroups DrowDownList
        Dim campusGroups As New CampusGroupsFacade
        With ddlCampusID
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"

            If MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "ownleads" Then
                .DataSource = campusGroups.GetCampusesByUser(userId)
            ElseIf MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "permissiblecampuses" Then
                .DataSource = campusGroups.GetCampusesByUser(userId)
            ElseIf MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "allcampuses" Then
                '''' Code changed to resolve mantis issue id 19548 by kamalesh Ahuja on 16th August 2010
                'If SingletonAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "yes" Then
                '    .DataSource = campusGroups.GetAllCampuses()
                'Else
                '    .DataSource = campusGroups.GetCampusesByUser(userId)
                'End If
                .DataSource = campusGroups.GetAllCampuses()
                ''''''''''
            Else
                .DataSource = campusGroups.GetAllCampusEnrollmentByCampus(campusId)
            End If

            .DataBind()

            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    '' Code Added by kamalesh Ahuja on 17 August 2010 to Resolve mantis issue id 19560
    Private Sub BuildAdmissionRepDDL()
        Dim userName As String
        userName = AdvantageSession.UserState.UserName

        Dim LeadAdmissionReps As New LeadFacade
        Dim DirectorAdmissionsorFrontDeskCheck As New UserSecurityFacade
        ''Find if the User is DirectorOfAdmission or FrontDesk

        Dim boolDirOrFDeskCheck As Boolean = DirectorAdmissionsorFrontDeskCheck.IsDirectorOfAdmissionsorFrontDesk(userId)

        If ddlCampusID.SelectedIndex = 0 Then

            ddlAdmissionRepID.Items.Clear()
            ddlAdmissionRepID.Items.Insert(0, New ListItem("Select", ""))

        Else
            With ddlAdmissionRepID
                .DataTextField = "fullname"
                .DataValueField = "userid"
                '''' Code changes by Kamalesh Ahuja on 30 August 2010 to resolve mantis issue id 19652
                If MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "ownleads" And boolDirOrFDeskCheck = False Then
                    .DataSource = LeadAdmissionReps.GetAllAdmissionRepsByCampusAndUserId(ddlCampusID.SelectedValue, userId)
                Else
                    .DataSource = LeadAdmissionReps.GetAllAdmissionRepsByCampus(ddlCampusID.SelectedValue)
                End If
                '''''''''''''''''''
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If

    End Sub
    '' Code Added by kamalesh Ahuja on 17 August 2010 to Resolve mantis issue id 19560
    Protected Sub ddlCampusID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampusID.SelectedIndexChanged
        BuildAdmissionRepDDL()
    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtLastName.Text = ""
        txtFirstName.Text = ""
        txtSSN.Text = ""
        txtPhone.Text = ""
        ddlStatusId.SelectedIndex = 0
        ddlCampusID.SelectedIndex = 0
        ddlAdmissionRepID.SelectedIndex = 0
        dgrdTransactionSearch.Visible = False
    End Sub
    Public Function BuildLeadStateObject(ByVal LeadId As String) As String
        Dim objStateInfo As AdvantageStateInfo
        Dim strVID As String = ""
        Dim strLeadId As String = ""

        If LeadId.ToString.Trim = "" Then 'No Student was selected from MRU
            strLeadId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                                   4, _
                                                                   AdvantageSession.UserState.CampusId.ToString)
        Else
            strLeadId = LeadId
        End If

        objStateInfo = mruProvider.BuildLeadStatusBar(strLeadId)

        'Create a new guid to be associated with the employer pages
        strVID = Guid.NewGuid.ToString

        txtLeadId.Text = strLeadId

        'strLeadObjectPointer = strVID 'Set VID to Lead Object Pointer

        Session("LeadObjectPointer") = strVID ' This step is required, as master page loses values stored in class variables
        If objStateInfo.SystemStatus = 6 Then 'If lead is enrolled set the student pointer
            Session("StudentObjectPointer") = strVID
        Else
            Session("StudentObjectPointer") = ""
        End If
        Session("StudentMRUFlag") = "false"


        'Add an entry to AdvantageSessionState for this guid and object
        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        'UpdateMRUTable
        'Reason: We need to keep track of the last student record the user worked with
        'Scenario1 : User can log in and click on a student page without using MRU 
        'and we need to display the data of the last student the user worked with
        'If the user is a first time user, we will load the student who was last added to advantage
        'mruProvider.UpdateMRUList(strLeadId, AdvantageSession.UserState.UserId.ToString, _
        '                          AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

        'mruProvider.UpdateMRUList(strLeadId, AdvantageSession.UserState.UserId.ToString, _
        '                         AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

        Try
            Dim advResetUserState As New BO.User
            advResetUserState = AdvantageSession.UserState
            With advResetUserState
                .CampusId = New Guid(objStateInfo.CampusId.ToString)
            End With
            AdvantageSession.UserState = advResetUserState
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New ArgumentException(ex.Message.ToString)
        End Try

        'DE8026
        'The lead should be added to MRU only if user has access to lead info page
        'Dim arrUPP As New ArrayList
        'arrUPP = mruProvider.checkEntityPageSecurity(AdvantageSession.UserState, 189, AdvantageSession.UserState.CampusId.ToString, 395)
        'If arrUPP.Count >= 1 And mruProvider.DoesUserHasAccessToSubModuleResource(arrUPP, 170) Then 'lead info
        '    mruProvider.InsertMRU(4, strLeadId, AdvantageSession.UserState.UserId.ToString, objStateInfo.CampusId.ToString)
        'ElseIf arrUPP.Count >= 1 And mruProvider.DoesUserHasAccessToSubModuleResource(arrUPP, 170) = False Then 'Any page other than lead info
        '    mruProvider.InsertMRU(4, strLeadId, AdvantageSession.UserState.UserId.ToString, objStateInfo.CampusId.ToString)
        'End If

        Return strVID

    End Function
End Class
'Public Class WaitCursor
'    Implements IDisposable
'    Public Sub New()
'        Cursor.Current = Cursors.WaitCursor
'    End Sub
'    'Use the Dispose method of this interface to explicitly release unmanaged resources 
'    'in conjunction with the garbage collector. The consumer of an object can call 
'    'this method when the object is no longer needed.
'    Public Overloads Sub Dispose() Implements IDisposable.Dispose
'        Cursor.Current = Cursors.Default
'        ' This object will be cleaned up by the Dispose method.
'        ' Therefore, you should call GC.SupressFinalize to
'        ' take this object off the finalization queue 
'        ' and prevent finalization code for this object
'        ' from executing a second time.
'        GC.SuppressFinalize(Me)
'    End Sub
'End Class



