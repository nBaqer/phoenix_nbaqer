Imports Fame.AdvantageV1.BusinessFacade

Partial Class DynamicResume
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents DgTestRules As System.Web.UI.WebControls.DataGrid
    Protected LeadId As String
    Protected WithEvents btnSave As System.Web.UI.WebControls.Button
    Protected WithEvents btnNew As System.Web.UI.WebControls.Button
    Protected WithEvents btnDelete As System.Web.UI.WebControls.Button
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected StudentId As String

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            StudentId = Request.QueryString("StudentId")
            txtStudentId.Text = StudentId
            BuildStudentAddressDDL()
            BuildSkills()
            BuildStudentCollgeHistory()
            BuildExtracurriculars()
            BuildSchoolPlacementHistory()
            EditButtons()
            BuildObjective()
            BuildExperience()

        End If
    End Sub
    Private Sub BuildExperience()
        Dim output As New PlacementFacade
        lblExperience.Text = output.StudentExperience(StudentId)
        If lblExperience.Text = "None" Then
            lblExperience.Text = ""
        End If
    End Sub
    Private Sub EditButtons()

    End Sub
    Private Sub BuildStudentAddressDDL()
        'Bind the Certificate DropDownList
        'Based on Degrees DropDownList
        Dim StudentAddress As New PlacementFacade
        With dlstStudentAddress
            .DataSource = StudentAddress.GetResumeStudentAddress(StudentId)
            .DataBind()
        End With
    End Sub
    Private Sub BuildObjective()
        'Bind the Certificate DropDownList
        'Based on Degrees DropDownList
        Dim StudentAddress As New LeadFacade
        lblobj.Text = StudentAddress.GetLeadObjective(StudentId)
    End Sub
    Private Sub BuildStudentCollgeHistory()
        'Bind the Certificate DropDownList
        'Based on Degrees DropDownList
        Dim StudentCollege As New PlacementFacade
        With dlstCollegeHistory
            .DataSource = StudentCollege.ResumeStudentEducationHistory(StudentId)
            .DataBind()
        End With
    End Sub
    Private Sub BuildSchoolPlacementHistory()
    End Sub
    Private Sub BuildSkills()
        Dim output As New PlacementFacade
        lblOutput.Text = output.SkillGroup(StudentId)
        If lblOutput.Text = "None" Then
            lblOutput.Text = ""
        End If
    End Sub
    Private Sub BuildExtracurriculars()
        Dim ExtracurrOutput As New PlacementFacade
        lblExtracurroutput.Text = ExtracurrOutput.GetStudentExtracurriculars(StudentId)
        If lblExtracurroutput.Text = "None" Then
            lblExtracurroutput.Text = ""
        End If
    End Sub
    Private Sub dlstStudentAddress_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlstStudentAddress.ItemDataBound
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
