﻿<%@ Page Title="Document Management" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentDocumentManagement.aspx.vb" Inherits="PL_StudentDocumentManagement" %>

<%@ Register Src="~/usercontrols/SearchControls/DocumentTypeSearchControl.ascx" TagPrefix="FAME" TagName="DocumentTypeSearchControl" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>Document Management</title>

    <style>
        .textStrikeThrough ~ td > a {
            pointer-events: none;
            cursor: default;
            opacity: .3;
        }
    </style>

    <script type="text/javascript" src="../js/CheckAll.js"></script>
    <script type="text/javascript" src="../js/ViewDocument.js"></script>

    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
    <script type="text/javascript">
        var uploadedFilesCount = 0;
        var isEditMode;
        function validateRadUpload(source, e) {
            if (isEditMode == null || isEditMode == undefined) {
                e.IsValid = false;
                if (uploadedFilesCount > 0) {
                    e.IsValid = true;
                }
            }
            isEditMode = null;
        }

        function OnClientFileUploaded(sender, eventArgs) {
            uploadedFilesCount++;
        }

        function openFileBrowser(fileurl) {
            window.open('../FileBrowser.aspx?fileurl=' + fileurl, '_blank', 'HistWin', 'width=700,height=600,resizable=yes');
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelContent" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="300px" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop2">
                        <br />
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="listframetop" nowrap>
                                                <table cellspacing="1" cellpadding="1" border="0">
                                                    <tr>
                                                        <td class="twocolumnlabelcell" align="left">
                                                            <asp:Label ID="Label1" CssClass="label" runat="server"> Module</asp:Label>
                                                        </td>
                                                        <td class="twocolumncontentcell">
                                                            <asp:DropDownList ID="ddlDocumentModule" Width="200px" CssClass="dropdownlist" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="emptycell_leftpane"></td>
                                                    </tr>

                                                    <tr>
                                                        <td class="twocolumnlabelcell">
                                                            <asp:Label ID="lblDocumentStatus" CssClass="label" runat="server"> Status</asp:Label>
                                                        </td>
                                                        <td class="twocolumncontentcell">
                                                            <asp:DropDownList ID="ddlDocFilterStatus" Width="200px" CssClass="dropdownlist" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="emptycell_leftpane"></td>
                                                    </tr>
                                                    <%--student ddl -left panel start--%>
                                                    <tr id="trStudentName" runat="server">
                                                        <td class="twocolumnlabelcell">
                                                            <asp:Label ID="Label6" CssClass="label" runat="server">Student</asp:Label>
                                                        </td>
                                                        <td class="twocolumncontentcell">
                                                            <asp:DropDownList ID="ddlStudent_Name" Width="200px" CssClass="dropdownlist" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="emptycell_leftpane"></td>
                                                    </tr>
                                                    <%--student ddl -left panel end--%>
                                                    <tr>
                                                        <td class="twocolumnlabelcell" style="vertical-align: middle">
                                                            <asp:Label ID="Labelreq" CssClass="label" align="left" runat="server">Required For</asp:Label>
                                                        </td>
                                                        <td class="twocolumncontentcell">
                                                            <asp:DropDownList ID="ddltypeofreq" Width="200px" CssClass="dropdownlist" runat="server">
                                                                <asp:ListItem Value="0">Select</asp:ListItem>
                                                                <asp:ListItem Value="1">Req for Enrollment</asp:ListItem>
                                                                <asp:ListItem Value="2">Req for Financial Aid</asp:ListItem>
                                                                <asp:ListItem Value="3">Req for Graduation</asp:ListItem>
                                                                <asp:ListItem Value="4">Req for Termination</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="emptycell_leftpane"></td>
                                                    </tr>
                                                    <tr id="trTypeFilter" runat="server">
                                                        <td class="twocolumnlabelcell">
                                                            <asp:Label ID="lblType" runat="server" CssClass="Label">Type</asp:Label>
                                                        </td>
                                                        <td class="docmgmtsearch21">
                                                            <FAME:DocumentTypeSearchControl runat="server" ID="ddlDocTypeSearchControl" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="emptycell_leftpane"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="twocolumnlabelcell">&nbsp;
                                                        </td>
                                                        <td class="twocolumncontentcell" style="text-align: right">
                                                            <telerik:RadButton ID="btnApplyFilter" runat="server" Text="Apply Filter"
                                                                CausesValidation="False">
                                                            </telerik:RadButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr id="trStudentDocs" runat="server">
                                            <td class="listframebottom">
                                                <div class="scrollleftfltr2rows">
                                                    <asp:DataList ID="dlstDocumentStatus" runat="server">
                                                        <SelectedItemStyle CssClass="selecteditem"></SelectedItemStyle>
                                                        <SelectedItemTemplate>
                                                        </SelectedItemTemplate>
                                                        <ItemStyle CssClass="nonselecteditem"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:LinkButton Text='<%# Container.DataItem("DocumentDescription") %>' runat="server"
                                                                CssClass="nonselecteditem" CommandArgument='<%# Container.DataItem("StudentDocId")%>'
                                                                ID="Linkbutton2" CausesValidation="False" />
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="listframebottom" nowrap>
                                                <div class="scrollleftdocmanage1">
                                                    <asp:DataList ID="dlstDocumentMgmt" runat="server" Width="100%">
                                                        <SelectedItemStyle CssClass="SelectedItem" Wrap="true"></SelectedItemStyle>
                                                        <SelectedItemTemplate>
                                                        </SelectedItemTemplate>
                                                        <ItemStyle CssClass="NonSelectedItem" Wrap="true"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:LinkButton Text='<%# Container.DataItem("Descrip") %>' runat="server" CssClass="NonSelectedItem"
                                                                CommandArgument='<%# Container.DataItem("StudentDocId")%>' ID="Linkbutton2" CausesValidation="False" />
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete"
                                CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%;" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->
                            <asp:Panel ID="pnlDocMgmtByFAME" runat="server">
                                <asp:TextBox ID="txtStEmploymentId" CssClass="label" runat="server" Visible="false"></asp:TextBox><asp:CheckBox
                                    ID="ChkIsInDB" runat="server" Checked="False" Visible="false"></asp:CheckBox>
                                <table class="contenttable" cellspacing="0" cellpadding="0" style="width: 100%;">
                                    <tr>
                                        <td style="width: 10%; min-width: 150px;">
                                            <asp:Label ID="lblStudentId" runat="server" CssClass="label">Student</asp:Label>
                                        </td>
                                        <td style="padding-bottom: 5px;">
                                            <asp:DropDownList ID="ddlStudentId" Width="200px" CssClass="dropdownlist" runat="server"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%; min-width: 150px;">
                                            <asp:Label ID="lblModuleId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td style="padding-bottom: 5px;">
                                            <asp:DropDownList ID="ddlModulesId" Width="200px" CssClass="dropdownlist" runat="server"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%; min-width: 150px;">
                                            <asp:Label ID="lblDocumentId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td style="padding-bottom: 5px;">
                                            <asp:DropDownList ID="ddlDocumentId" Width="200px" CssClass="dropdownlist" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%; min-width: 150px;">
                                            <asp:Label ID="lblDocStatusId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td style="padding-bottom: 5px;">
                                            <asp:DropDownList ID="ddlDocStatusId" Width="200px" CssClass="dropdownlist" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%; min-width: 150px;"></td>
                                        <td style="padding-bottom: 5px;">
                                            <asp:CheckBox ID="chkOverride" runat="server" CssClass="checkboxstyle" Text="Override" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%; min-width: 150px;">
                                            <asp:Label ID="lblRequestDate" runat="server" Text="Date Requested " CssClass="label"></asp:Label>
                                        </td>
                                        <td style="padding-bottom: 5px;">
                                            <telerik:RadDatePicker ID="RdRequestDate" runat="server" Width="200px">
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td style="text-align: left">
                                            <telerik:RadDatePicker ID="txtRequestDate" runat="server" Visible="false" MinDate="1/1/1945" Width="200px">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%; min-width: 150px;">
                                            <asp:Label ID="lblReceiveDate" runat="server" Text="Date Received " CssClass="label"></asp:Label>
                                        </td>
                                        <td style="padding-bottom: 5px;">
                                            <telerik:RadDatePicker ID="RdReceivedDate" runat="server" Width="200px">
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td style="text-align: left">
                                            <telerik:RadDatePicker ID="txtReceiveDate" runat="server" Visible="false" MinDate="1/1/1945" Width="200px">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%; min-width: 150px;"></td>
                                        <td valign="bottom" nowrap>
                                            <asp:CheckBox ID="chkScannedId" runat="server" Visible="False" AutoPostBack="True"></asp:CheckBox><asp:Label ID="lblScanned" CssClass="label" runat="server" Visible="False"
                                                Width="100px"></asp:Label><asp:LinkButton ID="lnkViewDoc" CssClass="label" runat="server"
                                                    Visible="False" Width="200px"></asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%; min-width: 150px;"></td>
                                        <td>
                                            <telerik:RadButton ID="btnViewDocsByStatus" runat="server" Text="View docs by status"
                                                CausesValidation="false">
                                            </telerik:RadButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label3" runat="server" class="labelbold" Text="Upload Documents"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                                <asp:Panel ID="pnlRegent" runat="server" Visible="false">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center"
                                        border="0">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblTrackCode" runat="server" CssClass="label">Track Code</asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlTrackCode" CssClass="dropdownlist" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblDueDate" runat="server" CssClass="label">Due Date</asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlDueDate" CssClass="dropdownlist" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNotifiedDate" runat="server" CssClass="label">Notified Date</asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlTransDate" CssClass="dropdownlist" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblCompletedDate" runat="server" CssClass="label">Completed Date</asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlCompletedDate" CssClass="dropdownlist" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNotificationCode" runat="server" CssClass="label">Notification Date</asp:Label>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlNotificationCode" CssClass="dropdownlist" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNotes" runat="server" CssClass="label">Note</asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtNotes" CssClass="textbox" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlUpload" runat="server" Visible="true">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="70%" align="center"
                                        border="0">
                                        <tr>
                                            <td colspan="5">
                                                <asp:Label ID="lblHeading" runat="server" CssClass="labelbold">&nbsp;Link to an 
                                                        electronic document:</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblFileName" runat="server" CssClass="label">File</asp:Label>
                                            </td>
                                            <td>
                                                <input id="txtUpLoad" runat="server" class="TextBox" name="txtUpLoad" type="file"
                                                    width="200px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <asp:Button ID="btnUpload" runat="server" Text="Upload Document" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <!--end content table-->
                                <asp:Panel ID="pnlViewDoc" runat="server" Visible="False">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="70%" align="center">
                                        <tr>
                                            <td class="contentcellheader" colspan="8" nowrap>
                                                <asp:Label ID="lblgeneral" runat="server" CssClass="label" Font-Bold="true">View 
                                                        Documents</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <div id="OldStudentDocGrid" visible="false" runat="server">
                                    <asp:DataGrid ID="dgrdDocs" runat="server" Width="70%" CellPadding="0" BorderStyle="Solid"
                                        AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true" EditItemStyle-Wrap="false"
                                        GridLines="Horizontal" align="center">
                                        <EditItemStyle Wrap="False"></EditItemStyle>
                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="FileName" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Linkbutton1" runat="server" Text='<%# Container.DataItem("DisplayName")%>'
                                                        CssClass="label" CausesValidation="False" CommandArgument='<%# Container.DataItem("FileName") & Container.DataItem("FileExtension")%>'
                                                        CommandName="StudentSearch">
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Button ID="btnDeleteFile" Text="Delete" runat="server" CommandArgument='<%# Container.DataItem("FileName")%>'
                                                        CommandName="DeleteFile" />

                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </div>
                                <telerik:RadAjaxManagerProxy ID="RadAjaxManager1" runat="server">
                                    <AjaxSettings>
                                        <telerik:AjaxSetting AjaxControlID="RadGrid1">
                                            <UpdatedControls>
                                                <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                                            </UpdatedControls>
                                        </telerik:AjaxSetting>
                                    </AjaxSettings>
                                </telerik:RadAjaxManagerProxy>
                                <div>
                                    <telerik:RadGrid runat="server" ID="RadGrid1" AllowPaging="True" AllowSorting="True"
                                        AutoGenerateColumns="False" Width="97%" ShowStatusBar="True" GridLines="None"
                                        OnItemCreated="RadGrid1_ItemCreated" PageSize="5" OnInsertCommand="RadGrid1_InsertCommand" OnRowDataBound=""
                                        OnNeedDataSource="RadGrid1_NeedDataSource" OnDeleteCommand="RadGrid1_DeleteCommand"
                                        OnUpdateCommand="RadGrid1_UpdateCommand" OnItemCommand="RadGrid1_ItemCommand">
                                        <PagerStyle Mode="NumericPages" AlwaysVisible="true" />
                                        <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="FileID" InsertItemPageIndexAction="ShowItemOnFirstPage">
                                            <CommandItemSettings AddNewRecordText="Add New Record" RefreshText="View All" ShowRefreshButton="false" />
                                            <CommandItemTemplate>
                                                <div style="height: 20px;">
                                                    <div style="float: left; vertical-align: top;">
                                                        <asp:ImageButton ImageUrl="~\images\icon\icon_add.png" runat="server" ID="btn1" CommandName="InitInsert" CssClass="rgAdd" Text=" " />
                                                        <asp:LinkButton runat="server" ID="linkbuttionInitInsert" CommandName="InitInsert"
                                                            Text="Add New Record"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </CommandItemTemplate>
                                            <Columns>
                                                <telerik:GridEditCommandColumn ButtonType="ImageButton" EditText="Replace" EditImageUrl="~\images\icon\icon_edit.png" CancelImageUrl="~\images\icon\icon_cancel.png" UpdateImageUrl="~\images\icon\icon_save.png" InsertImageUrl="~\images\icon\icon_save.png">
                                                    <HeaderStyle Width="3%" />
                                                </telerik:GridEditCommandColumn>
                                                <telerik:GridTemplateColumn HeaderText="Document Name" EditFormHeaderTextFormat="Document to upload"
                                                    UniqueName="DisplayName" SortExpression="DisplayName">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblName" Text='<%# Eval("DisplayName") & Eval("FileExtension")  %> ' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderStyle Width="15%" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn HeaderText="Modified Date" EditFormHeaderTextFormat="" UniqueName="ModifiedDate">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblModifiedDate" Text='<%# Eval("modDate") %> ' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderStyle Width="15%" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn DataField="Data" HeaderText="" EditFormHeaderTextFormat=""
                                                    UniqueName="Upload">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="Linkbutton1" runat="server" Text='click here to view the document'
                                                            CssClass="Label" CausesValidation="False" CommandArgument='<%# Eval("FileUrl") %>'>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                        <telerik:RadAsyncUpload runat="server" ID="AsyncUpload1" OnClientFileUploaded="OnClientFileUploaded" DisablePlugins="true"
                                                            AllowedFileExtensions="jpg,jpeg,png,gif,pdf,txt,doc,docx,xls,xlsx">
                                                        </telerik:RadAsyncUpload>
                                                    </EditItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn DataField="User" HeaderText="User" EditFormHeaderTextFormat="" UniqueName="User">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblUser" Text='<%# Eval("modUser") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderStyle Width="15%" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn DataField="Date" HeaderText="Date Uploaded" EditFormHeaderTextFormat="" UniqueName="UpLoadedDate">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblDate" Text='<%# Eval("modDate") %>' />
                                                    </ItemTemplate>
                                                    <EditItemTemplate>
                                                    </EditItemTemplate>
                                                    <HeaderStyle Width="15%" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridTemplateColumn Visible="False" DataField="IsArchived" HeaderText="IsArchived" EditFormHeaderTextFormat="" UniqueName="IsArchived">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblIsArchived" Text='<%# Eval("IsArchived") %>' />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridButtonColumn ConfirmTextFormatString="Are you sure you want to delete {0}{1}?" ConfirmTextFields="DisplayName,FileExtension" ImageUrl="../Images/icon/icon_delete.png" ButtonType="ImageButton" CommandName="Delete" ConfirmDialogType="Classic"
                                                    ConfirmTitle="Delete" Text="Delete" UniqueName="DeleteColumn" HeaderStyle-Width="2%">
                                                    <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" />
                                                </telerik:GridButtonColumn>
                                            </Columns>
                                            <PagerStyle AlwaysVisible="True" />
                                        </MasterTableView>
                                    </telerik:RadGrid>
                                </div>
                                <p>
                                </p>
                                <table width="70%" align="center">
                                    <tr>
                                        <td nowrap>
                                            <asp:Button ID="btnGetDoc" runat="server" Text="   Liberty   "
                                                Visible="false"></asp:Button>
                                        </td>
                                        <td nowrap>
                                            <asp:Button ID="btnSchoolDocs" runat="server" Text="SchoolDocs"
                                                Visible="false"></asp:Button>
                                        </td>
                                    </tr>
                                </table>
                                <asp:TextBox ID="txtStudentDocId" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBox ID="txtDocumentName" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBox ID="txtStudentName" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBox ID="txtPath" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBox ID="txtDocumentType" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBox ID="txtExtension" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBox ID="txtStudentId" runat="server" Visible="False"></asp:TextBox>
                            </asp:Panel>
                            <asp:Panel ID="Panel2" runat="server" Visible="false">
                                <div class="scrollwhole2">
                                    <table class="DataGridHeaderStyle" width="40%">
                                        <tr height="20">
                                            <td nowrap>Documents for Student:&nbsp;&nbsp;&nbsp;<asp:Label ID="Label2" runat="Server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                    <asp:Label ID="Label4" runat="server" />
                                </div>
                            </asp:Panel>
                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="pnlDocMgmtBySchoolDocs" runat="server" Visible="false">
        <div class="scrollwhole2">
            <table class="DataGridHeaderStyle" width="40%">
                <tr height="20px">
                    <td nowrap>Documents for Student:&nbsp;&nbsp;&nbsp;<asp:Label ID="Label5" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <asp:Label ID="lbl1" runat="server" />
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary" Visible="false">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>


