﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="DocumentManagement.aspx.vb" Inherits="PL_DocumentManagement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/checkall.js" type="text/javascript"/>
<script type="text/javascript">

    function OldPageResized(sender, args) {
        $telerik.repaintChildren(sender);
    }

   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
<asp:scriptmanagerproxy id="scriptmanagerproxy1" runat="server">
</asp:scriptmanagerproxy>
<div style="overflow:auto;">
   <telerik:radsplitter id="oldcontentsplitter" runat="server" collapsemode="none" height="100%" orientation="vertical" 
visibleduringinit="false" borderwidth="0px" onclientresized="OldPageResized" style="overflow:auto;">
<telerik:radpane id="oldmenupane" runat="server" BackColor="#FAFAFA" width="350" scrolling="Y">
<table cellspacing="0" cellpadding="0" width="100%" border="0">
<tr>
<td class="listframetop">
<table width="100%" border="0" cellpadding="2" cellspacing="0">
										<tr>
											<td class="employersearch" nowrap><asp:label id="Label1" Runat="server" cssClass="label">Document Module</asp:label></td>
											<td class="employersearch2"><asp:dropdownlist id="ddlDocumentModule" Runat="server" cssClass="dropdownlist"></asp:dropdownlist></td>
										</tr>
										<tr>
											<td class="employersearch" nowrap><asp:label id="lblDocumentStatus" Runat="server" cssClass="label">Document Status</asp:label></td>
											<td class="employersearch2"><asp:dropdownlist id="ddlDocFilterStatus" Runat="server" cssClass="dropdownlist"></asp:dropdownlist></td>
										</tr>
										<tr>
											<td class="employersearch" nowrap><asp:label id="lblStudentFilter" runat="server" cssClass="label">Student</asp:label></td>
											<td class="employersearch2"><asp:dropdownlist id="ddlStudentFilter" Runat="server" cssClass="dropdownlist"></asp:dropdownlist></td>
										</tr>
                                        <tr>
											<td class="employersearch" nowrap><asp:label id="lblreqFor" runat="server" cssClass="label">Required For</asp:label></td>
											<td class="employersearch2"><asp:dropdownlist id="ddltypeofreq" cssClass="dropdownlist" Runat="server">
                                                <asp:ListItem Value="0">Select</asp:ListItem>
                                                <asp:ListItem Value="1">Req for Enrollment</asp:ListItem>
                                                <asp:ListItem Value="2">Req for Financial Aid</asp:ListItem>
                                                <asp:ListItem Value="3">Req for Graduation</asp:ListItem>
                                                                                              </asp:dropdownlist></td>
										</tr>
										<tr>
											<td class="employersearch" nowrap></td>
											<td class="employersearch2" style="TEXT-ALIGN: center"><asp:button id="btnApplyFilter" Runat="server" CssClass="buttontopfilter" Text="Apply Filter"
													CausesValidation="False"></asp:button></td>
										</tr>
									</table>
</td>
</tr>
<tr>
<td class="listframebottom">
<div class="scrollleftfilters">
<asp:datalist id="dlstDocumentStatus" runat="server">
											<SelectedItemStyle CssClass="selecteditem"></SelectedItemStyle>
											<SelectedItemTemplate>
											</SelectedItemTemplate>
											<ItemStyle CssClass="nonselecteditem"></ItemStyle>
											<ItemTemplate>
												<asp:LinkButton text='<%# Container.DataItem("Descrip") %>' Runat="server" CssClass="nonselecteditem" CommandArgument='<%# Container.DataItem("StudentDocId")%>' ID="Linkbutton2" CausesValidation="False" />
											</ItemTemplate>
										</asp:datalist>
</div>
</td>
</tr>
</table>

</telerik:radpane>
<telerik:radpane id="oldcontentpane" runat="server" borderwidth="0px" scrolling="both" orientation="horizontaltop">
<table cellspacing="0" cellpadding="0" width="100%" border="0">
<!-- begin top menu (save,new,reset,delete,history)-->
<tr>
<td class="menuframe" align="right"><asp:button id="btnsave" runat="server" cssclass="save" text="Save"></asp:button><asp:button id="btnnew" runat="server" cssclass="new" text="New" causesvalidation="false"></asp:button>
<asp:button id="btndelete" runat="server" cssclass="delete" text="Delete" causesvalidation="false"></asp:button></td>
</tr>
</table>
<table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
<tr>
<td class="detailsframe">
<!-- begin content table-->
<asp:panel id="pnlrhs" runat="server">
<div class="scrollright2">
<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" align="center">
											<asp:textbox id="txtStEmploymentId" Runat="server" visible="false"></asp:textbox>
											<asp:checkbox id="ChkIsInDB" runat="server" Visible="false" checked="False"></asp:checkbox>
											<asp:checkbox id="chkScannedId" Runat="server" AutoPostBack="True" Visible="False"></asp:checkbox>
											<asp:label id="lblScanned" Runat="server" Visible="False"></asp:label>
											<asp:linkbutton id="lnkViewDoc" Runat="server" Visible="False"></asp:linkbutton>
											<TR>
												<TD class="twocolumnlabelcell"><asp:label id="lblStudentId" runat="server" cssClass="label">Student</asp:label></TD>
												<TD class="twocolumncontentcell"><asp:dropdownlist id="ddlStudentId" Runat="server" cssClass="dropdownlist" AutoPostBack="true"></asp:dropdownlist></TD>
											</TR>
											<TR>
												<TD class="twocolumnlabelcell" style="HEIGHT: 24px"><asp:label id="Label2" runat="server" cssClass="label">Module</asp:label></TD>
												<TD class="twocolumncontentcell" style="HEIGHT: 24px"><asp:dropdownlist id="ddlModulesId" Runat="server" cssClass="dropdownlist" AutoPostBack="True"></asp:dropdownlist></TD>
											</TR>
											<TR>
												<TD class="twocolumnlabelcell"><asp:label id="lblDocumentId" runat="server" cssClass="label">Document</asp:label></TD>
												<TD class="twocolumncontentcell"><asp:dropdownlist id="ddlDocumentId" Runat="server" cssClass="dropdownlist"></asp:dropdownlist></TD>
											</TR>
											<TR>
												<TD class="twocolumnlabelcell"><asp:label id="lblDocStatusId" runat="server" cssClass="label">Document Status</asp:label></TD>
												<TD class="twocolumncontentcell"><asp:dropdownlist id="ddlDocStatusId" Runat="server" cssClass="dropdownlist"></asp:dropdownlist></TD>
											</TR>
											<tr>
												<TD class="twocolumnlabelcell"></TD>
												<td class="twocolumncontentcell" align=left><asp:CheckBox ID="chkOverride" Runat="server" cssClass="label" Text="Override" /></td>
											</tr>
											<TR>
												<TD class="twocolumnlabelcell"><asp:label id="lblRequestDate" runat="server" cssClass="label">Requested Date</asp:label></TD>
												<TD class="twocolumncontentcell" style="TEXT-ALIGN: left">    
                                                        <telerik:RadDatePicker ID="txtRequestDate" MinDate="1/1/1945" runat="server" CssClass="textbox">
                                                        </telerik:RadDatePicker>
                                                 </TD>
											</TR>
											<TR>
												<TD class="twocolumnlabelcell"><asp:label id="lblReceiveDate" runat="server" cssClass="label">Received Date</asp:label></TD>
												<TD class="twocolumncontentcell" style="TEXT-ALIGN: left">
                                                     <telerik:RadDatePicker ID="txtReceiveDate" MinDate="1/1/1945" runat="server" CssClass="textbox">
                                                        </telerik:RadDatePicker>            
                                                </TD>
											</TR>
											<tr>
												<td class="twocolumnlabelcell">&nbsp;</td>
												<td class="twocolumncontentcell" style="TEXT-ALIGN: left"><a href="#" class="label">View 
														documents by status</a></td>
											</tr>
										</TABLE>
										<p></p>
										<asp:panel id="pnlUpload" Runat="server" Visible="true">
											<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" align="center">
												<TR>
													<TD class="spacertables"></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell"></TD>
													<TD class="twocolumncontentcell">
														<asp:Label id="lblHeading" CssClass="LabelBold" Runat="server">Link to an electronic document:</asp:Label></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblFileName" cssClass="label" Runat="server">File</asp:label></TD>
													<TD class="twocolumncontentcell"><INPUT class="TextBox" id="txtUpLoad"  type="file" name="txtUpLoad" runat="server"></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell"></TD>
													<TD class="twocolumncontentcell" noWrap>
														<asp:Button id="btnUpload" Runat="server" Text="Upload Document" ></asp:Button>&nbsp;
													</TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell"></TD>
													<TD class="twocolumncontentcell" noWrap></TD>
												</TR>
											</TABLE>
										</asp:panel>
										<asp:Panel ID="pnlViewDoc" Runat="server" Visible="False">
											<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" align="center">
												<TR>
													<TD class="contentcellheader" noWrap>
														<asp:label id="lblgeneral" runat="server" cssClass="label" Font-Bold="true">View Documents</asp:label></TD>
												</TR>
												<TR>
													<TD class="spacertables"></TD>
												</TR>
											</TABLE>
										</asp:Panel>
										<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" align="center">
											<tr>
												<td><asp:datagrid id="dgrdDocs" Runat="server" width="100%" align="center" GridLines="Horizontal"
														EditItemStyle-Wrap="false" HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False"
														BorderStyle="Solid" cellpadding="0">
														<EditItemStyle Wrap="False"></EditItemStyle>
														<ItemStyle cssClass="DataGridItemStyle"></ItemStyle>
														<HeaderStyle cssClass="DataGridHeader"></HeaderStyle>
														<AlternatingItemStyle cssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="FileName" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
																<HeaderStyle HorizontalAlign="left"></HeaderStyle>
																<ItemStyle HorizontalAlign="left"></ItemStyle>
																<ItemTemplate>
																	<asp:LinkButton id="Linkbutton1" runat="server" Text='<%# Container.DataItem("DisplayName")%>' cssClass="label" CausesValidation="False" CommandArgument='<%# Container.DataItem("FileName") & Container.DataItem("FileExtension")%>' CommandName="StudentSearch">
																	</asp:LinkButton>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="DocumentType" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
																<HeaderStyle HorizontalAlign="left"></HeaderStyle>
																<ItemStyle HorizontalAlign="left"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lblDocumentType" Text='<%# Container.DataItem("DocumentCategory") %>' cssClass="label" Runat="server">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
																<HeaderStyle HorizontalAlign="left"></HeaderStyle>
																<ItemStyle HorizontalAlign="left"></ItemStyle>
																<ItemTemplate>
																	<asp:button id="btnDeleteFile" Text="Delete"  Runat="server" CommandArgument='<%# Container.DataItem("FileName")%>' CommandName="DeleteFile" />
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid><asp:panel id="Panel2" runat="server" Visible="False">
														<asp:ValidationSummary id="ValidationSummary2" runat="server" Visible="False" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
														<asp:CustomValidator id="CustomValidator2" Runat="server" Visible="False"></asp:CustomValidator>
													</asp:panel><asp:textbox id="txtStudentDocId" Runat="server" Visible="False"></asp:textbox><asp:textbox id="txtDocumentName" Runat="server" Visible="False"></asp:textbox><asp:textbox id="txtStudentName" Runat="server" Visible="False"></asp:textbox><asp:textbox id="txtPath" Runat="server" Visible="False"></asp:textbox><asp:textbox id="txtDocumentType" Runat="server" Visible="False"></asp:textbox><asp:textbox id="txtExtension" Runat="server" Visible="False"></asp:textbox><asp:textbox id="txtStudentId" Runat="server" Visible="False"></asp:textbox></td>
											</tr>
										</TABLE>
										<!--<table width="60%" align="center">
											<TR>
												<TD class="contentcell" nowrap><asp:Button id="btnGetDoc" Runat="server" Text="   Liberty   "  Visible="true"></asp:Button></TD>
												<TD class="contentcell4" nowrap><asp:Button id="btnSchoolDocs" Runat="server" Text="SchoolDocs"  Visible="true"></asp:Button></TD>
											</TR>
										</table>-->
</div>
</asp:panel>
<!--end table content-->
</td>
</tr>
</table>
</telerik:radpane>
</telerik:radsplitter>
<asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
<asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
</div>
</asp:Content>

