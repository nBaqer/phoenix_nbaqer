﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentSearch.aspx.vb" Inherits="StudentSearch" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <script language="javascript" type="text/jscript">
        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }
    </script>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">


            <!-- begin leftcolumn -->
            <%-- Add class ListFrameTop2 to the table below --%>


            <%--LEFT PANE CONTENT HERE--%>
            <table cellspacing="0" class="listframetop2" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframenofilterblue">Search Student</td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleft">
                            <table cellspacing="0" cellpadding="2" align="center" width="100%">
                                <tr>
                                    <td class="employersearch" style="width: 34%;">
                                        <asp:Label ID="lblLastName" runat="Server" CssClass="label">Last Name</asp:Label></td>
                                    <td class="employersearch2">
                                        <asp:TextBox ID="txtLastName" runat="Server" CssClass="textbox"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="revLastName" CssClass="regularexpression" Display="None"
                                            runat="server" ErrorMessage="Invalid search string" ControlToValidate="txtLastName"
                                            ValidationExpression="[A-Za-z][\w\.\'\ \-]{0,15}">Invalid search string</asp:RegularExpressionValidator></td>
                                </tr>
                                <tr>
                                    <td class="employersearch" style="width: 34%;">
                                        <asp:Label ID="lblFirstName" runat="Server" CssClass="label">First Name</asp:Label></td>
                                    <td class="employersearch2">
                                        <asp:TextBox ID="txtFirstName" runat="Server" CssClass="textbox"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" Display="None" runat="server"
                                            ErrorMessage="Invalid search string" ControlToValidate="txtFirstName" CssClass="regularexpression"
                                            ValidationExpression="[A-Za-z][\w\.\'\ \-]{0,15}">Invalid search string</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch" style="width: 34%;">
                                        <%--   <asp:Label ID="lblSSN" runat="Server" CssClass="label">SSN<font runat="server" visible="false" id="ssnreqasterisk" color="red">*</font></asp:Label>--%>
                                        <asp:Label ID="lblSSN" runat="Server" CssClass="label">SSN</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <telerik:RadMaskedTextBox ID="txtSSN" runat="server" CssClass="textbox"
                                            Width="200px" DisplayFormatPosition="Right" Mask="#########" DisplayMask="###-##-####"
                                            DisplayPromptChar="">
                                        </telerik:RadMaskedTextBox>
                                        <%-- <ew:MaskedTextBox ID="txtSSN" runat="server" CssClass="textbox">
                                                </ew:MaskedTextBox>--%>
                                    </td>
                                </tr>
                                <!--tr>
											<td align="left"><asp:label id="lblStudentID" runat="Server" cssClass="label" Width="100%">StudentID</asp:label></td>
											<td></td>
										</tr>-->
                                <tr>
                                    <td class="employersearch" style="width: 34%;">
                                        <asp:Label ID="lblStudentNumber" runat="Server" CssClass="label">Student Id</asp:Label></td>
                                    <td class="employersearch2">
                                        <asp:TextBox ID="txtStudentNumber" runat="Server" CssClass="textbox"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td class="employersearch" style="width: 34%;">
                                        <asp:Label ID="label3" runat="Server" CssClass="label">Student Status</asp:Label></td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlStudentStatusId" runat="Server" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td class="employersearch" style="width: 34%;">
                                        <asp:Label ID="lblStatus" runat="Server" CssClass="label">Enrollment Status</asp:Label></td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlStatusId" runat="Server" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td class="employersearch" nowrap style="width: 34%;">
                                        <asp:Label ID="lblProgram" runat="Server" CssClass="label">Program Version</asp:Label></td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlProgram" runat="Server" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td class="employersearch" nowrap style="width: 34%;">
                                        <asp:Label ID="label4" runat="Server" CssClass="label">Student Group</asp:Label></td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlLeadGrpId" runat="Server" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch" style="width: 34%;">
                                        <asp:Label ID="lblCampGrpID" runat="Server" CssClass="label" Visible="False">Campus</asp:Label></td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlCampGrpID" runat="Server" CssClass="dropdownlist" Visible="False">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td class="employersearch" style="width: 34%;">
                                        <asp:Label ID="lblCohortStartDate" runat="server" CssClass="label">Cohort Start Date</asp:Label></td>
                                    <td class="employersearch2">
                                        <telerik:RadDatePicker ID="RDPCohortStartDate" runat="server" Width="200px">
                                        </telerik:RadDatePicker>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td class="employersearch" style="width: 34%;"></td>
                                    <td class="employersearch2">
                                        <table cellspacing="0" cellpadding="2" align="left" width="100%">
                                            <tr>
                                                <td width="50%" align="left">
                                                    <%--  <asp:Button ID="btnSearch" runat="Server" Text="Search"></asp:Button>--%>
                                                    <telerik:RadButton ID="btnSearch" runat="server" Text="Search">
                                                    </telerik:RadButton>
                                                    &nbsp;
                                                             <telerik:RadButton ID="btnReset" runat="server" Text="Reset ">
                                                             </telerik:RadButton>
                                                </td>
                                                <td width="50%" align="left"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="employersearch" style="width: 34%;">
                                        <asp:Label ID="lblEnrollment" runat="Server" CssClass="label" Visible="False">Enrollment #</asp:Label></td>
                                    <td class="employersearch2">
                                        <asp:TextBox ID="txtEnrollment" runat="Server" CssClass="textbox" Visible="False"></asp:TextBox>
                                        <asp:TextBox ID="txtStudentId" runat="server" Visible="false"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator3" runat="server" ErrorMessage="Invalid search string"
                                            ControlToValidate="txtEnrollment" ValidationExpression="[A-Za-z0-9]{0,15}">Invalid search string</asp:RegularExpressionValidator>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>

                </tr>

            </table>


            <%--                        <td class="leftside">
                            <table cellspacing="0" cellpadding="0" width="9" border="0">
                            </table>
                        </td>--%>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">

            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>


            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->

                            <%-- MAIN CONTENT HERE--%>
                            <table width="100%" border="0">
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgrdTransactionSearch" CellPadding="0" BorderWidth="1px" BorderStyle="Solid"
                                            BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true"
                                            EditItemStyle-Wrap="false" GridLines="Horizontal" Width="100%" runat="server" 
                                                      r>
                                            <EditItemStyle Wrap="False"></EditItemStyle>
                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Last Name">
                                                    <HeaderStyle CssClass="k-grid-header" Width="20%"></HeaderStyle>

                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="Linkbutton1" runat="server" Text='<%# Container.DataItem("LastName") %>'
                                                            CssClass="label" CausesValidation="False" CommandArgument='<%# Container.DataItem("StudentId") %>'
                                                            CommandName="StudentSearch">
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="First Name">
                                                    <HeaderStyle CssClass="k-grid-header" Width="15%"></HeaderStyle>

                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDgFirstName" Text='<%# Container.DataItem("FirstName") %>' CssClass="label"
                                                            runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="SSN">
                                                    <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>

                                                    <ItemTemplate>
                                                        <asp:Label ID="label1" Text='<%# Container.DataItem("SSN") %>' CssClass="label" runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Program">
                                                    <HeaderStyle CssClass="k-grid-header" Width="40%"></HeaderStyle>

                                                    <ItemTemplate>
                                                        <asp:Label ID="label2" Text='<%# Container.DataItem("PrgVerDescrip") %>' CssClass="label"
                                                            runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Enrollment Status">
                                                    <HeaderStyle CssClass="k-grid-header" Width="30%"></HeaderStyle>

                                                    <ItemTemplate>
                                                        <asp:Label ID="label3" Text='<%# Container.DataItem("EnrollStatus") %>' CssClass="label"
                                                            runat="server">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>

                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadNotification runat="server" ID="RadNotification1"
                Text="This is a test" ShowCloseButton="true"
                Width="400px" Height="125px"
                TitleIcon=""
                Position="Center" Title="Message"
                EnableRoundedCorners="true"
                EnableShadow="true"
                Animation="Fade"
                AnimationDuration="1000"
                Style="padding-left: 120px; padding-top: 5px; word-spacing: 2pt;">
            </telerik:RadNotification>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
</asp:Content>





