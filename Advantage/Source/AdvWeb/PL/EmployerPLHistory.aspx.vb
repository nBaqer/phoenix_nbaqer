﻿Imports FAME.Advantage.Common
Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Advantage.Business.Logic.Layer

Partial Class EmployerPLHistory
    Inherits BasePage
    Protected WithEvents txtTermination As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblJobType As System.Web.UI.WebControls.Label
    Protected WithEvents lblJobDescription As System.Web.UI.WebControls.Label
    Protected WithEvents lblSchedule As System.Web.UI.WebControls.Label
    Protected WithEvents lblTermination As System.Web.UI.WebControls.Label
    Protected WithEvents lblReason As System.Web.UI.WebControls.Label


    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected EmployerId As String
    Protected ResourceId As Integer
    Protected ModuleId As String
    Protected sdfcontrols As New SDFComponent
    Protected boolSwitchCampus As Boolean = False
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub




#End Region

    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Protected userId As String

    Private mruProvider As MRURoutines
    ' Protected EmployerId As String
    Protected state As AdvantageSessionState
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#Region "MRURoutines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function getEmployerFromStateObject(ByVal paramResourceId As Integer) As BO.EmployerMRU


        Dim objStateInfo As New AdvantageStateInfo
        Dim objEmployerState As New BO.EmployerMRU

        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        MyBase.GlobalSearchHandler(2)

        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
        HttpContext.Current.Items("Language") = "En-US"

        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployerId) Then
            EmployerId = Guid.Empty.ToString()
        Else
            EmployerId = AdvantageSession.MasterEmployerId
        End If

        With objEmployerState
            .EmployerId = New Guid(EmployerId)
            .Name = objStateInfo.NameValue
        End With


        Dim objGetStudentStatusBar As New AdvantageStateInfo

        mruProvider = New MRURoutines(Me.Context, GetAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)
        objGetStudentStatusBar = mruProvider.BuildEmployerStatusBar(EmployerId)
        With objGetStudentStatusBar

            AdvantageSession.MasterEmpName = objGetStudentStatusBar.NameValue
            AdvantageSession.MasterEmpAddress1 = objGetStudentStatusBar.Address1
            AdvantageSession.MasterEmpAddress2 = objGetStudentStatusBar.Address2
            AdvantageSession.MasterEmpCity = objGetStudentStatusBar.City
            AdvantageSession.MasterEmpState = objGetStudentStatusBar.State
            AdvantageSession.MasterEmpZip = objGetStudentStatusBar.Zip
            AdvantageSession.MasterEmpPhone = objGetStudentStatusBar.Phone
            objEmployerState.Name = objGetStudentStatusBar.NameValue
        End With



        If Not String.IsNullOrEmpty(AdvantageSession.MasterEmployerId) Then
            Master.ShowHideStatusBarControl(True) 'Show Lead Bar only when Lead is not blank
            Master.PageObjectId = EmployerId
            Master.PageResourceId = Request.QueryString("resid").ToString()
            Master.setHiddenControlForAudit()
        Else
            Master.ShowHideStatusBarControl(False) 'Hide Lead Bar only when Lead is blank
        End If

        Return objEmployerState

    End Function
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim SearchEmployerID As New EmployerSearchFacade
        '  Dim startPos As String
        'Dim strVID As String
        'Dim state As AdvantageSessionState
        '  Dim m_Context As HttpContext

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

        '''''''''''''''''' Call to get EmployerId starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objEmployerState As New BO.EmployerMRU
        objEmployerState = getEmployerFromStateObject(79) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objEmployerState Is Nothing Then
            MyBase.RedirectToEmployerSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objEmployerState
            EmployerId = .EmployerId.ToString

        End With

        '''''''''''''''''' Call to get EmployerId  ends here ''''''''''''''''''''
        SetPageControlsEnabledState(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), True)
        ViewState("MODE") = "NEW"
        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            BuildEmployerJobTitle(EmployerId)
            MyBase.uSearchEntityControlId.Value = ""
        End If
        BindPlacementHistoryList()
        btnSave.Enabled = False
        btnDelete.Enabled = False
        btnNew.Enabled = False
        
        If (Master.IsSwitchedCampus) Then
            CampusObjects.ShowNotificationWhileSwitchingCampus(2, objEmployerState.Name)
        End If
        '   disable History button at all time
        'header1.EnableHistoryButton(False)
    End Sub
    Private Sub dlstEmployerContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstEmployerContact.ItemCommand
        GetEmployerPlacementHistory(e.CommandArgument)
        'set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, e.CommandArgument, ViewState, header1)
    End Sub
    Private Sub BindPlacementHistoryList()
        'Bind The PlacementHistory Based on PlacementId
        With New EmployerJobsFacade
            dlstEmployerContact.DataSource = .GetAllPlacementHistory(EmployerId, txtLastName.Text, txtFirstName.Text, ddlJobTitleId.SelectedValue)
            dlstEmployerContact.DataBind()
        End With
    End Sub
    Private Sub GetEmployerPlacementHistory(ByVal placementid As String)
        Dim PlacementInfo As New EmployerJobsFacade
        BindPlacementHistoryData(PlacementInfo.GetEmployerPlacementHistory(placementid))
    End Sub
    Private Sub BindPlacementHistoryData(ByVal placementinfo As PlacementHistoryInfo)

        'Bind The EmployerInfo Data From The Database
        Dim facInputMasks As New InputMasksFacade
        Dim ssnMask As String

        'Get the mask for SSN
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        With placementinfo

            'Get PlacementId
            txtPlacementId.Text = .PlacementId

            'Get Code
            txtSupervisor.Text = .Supervisor

            'Get Fee
            txtFee.Text = .Fee


            'Get StartDate
            txtStartDate.Text = .StartDate

            'Get Salary
            txtSalary.Text = .Salary

            'Get TerminationReason
            txtTerminationReason.Text = .TerminationReason

            'Get Termination Date
            txtTerminationDate.Text = .TerminationDate

            'Get Job Title
            txtJobTitleId.Text = .JobTypeId & " - " & .JobTitleId


            'Get Job Description
            txtJobDescrip.Text = .JobDescrip

            'Get Job Group
            txtJobTypeId.Text = .JobTypeId


            'BenefitsId
            txtBenefitsId.Text = .BenefitsId


            'Schedule
            txtScheduleId.Text = .ScheduleId


            'Start
            txtStartDate.Text = .StartDate

            'Notes
            txtNotes.Text = .Notes

            'SSN
            txtSSN.Text = .SSN
            If txtSSN.Text <> "" Then
                'txtSSN.Text = facInputMasks.ApplyMask(ssnMask, txtSSN.Text)
                Dim temp As String = txtSSN.Text
                txtSSN.Text = facInputMasks.ApplyMask(ssnMask, "*****" & temp.Substring(5))
            End If

            'StudentName 
            txtStudent.Text = Trim(.FirstName) & " " & Trim(.MiddleName) & " " & Trim(.LastName)

            'Placement Rep
            txtPlacementRep.Text = .PlacementRep

            'HowPlaced
            txtHowPlaced.Text = .HowPlaced

            'JobStatus
            txtJobStatus.Text = .JobStatus

            txtJobInterview.Text = .Interview

            txtFldStudy.Text = .FldStudy

            txtDatePlaced.Text = .PlacedDate

            txtSalaryType.Text = .SalaryType

            txtEnrollment.Text = .Enrollment
        End With
        'BuildStatusDDL()
    End Sub
    Public Sub SetPageControlsEnabledState(ByVal pgContentPlaceHolder As WebControls.ContentPlaceHolder, ByVal EnabledFlag As Boolean)
        Dim oForm As ContentPlaceHolder
        Dim oCtl As Control
        Dim oTextBoxCtl As TextBox

        oForm = pgContentPlaceHolder
        For Each oCtl In oForm.Controls
            Select Case oCtl.GetType.ToString
                Case "System.Web.UI.WebControls.TextBox"
                    oTextBoxCtl = CType(oCtl, TextBox)
                    oTextBoxCtl.ReadOnly = EnabledFlag
            End Select
        Next
        txtFirstName.ReadOnly = False
        txtLastName.ReadOnly = False
    End Sub
    Private Sub ChkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        BindPlacementHistoryList()
        BindPlacementHistoryData(New PlacementHistoryInfo)
    End Sub
    Private Sub DisableButtons()
        btnNew.Enabled = False
        btnDelete.Enabled = False
        btnSave.Enabled = False
    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        BindPlacementHistoryList()
    End Sub
    Private Sub BuildEmployerJobTitle(ByVal EmployerId As String)
        'Bind the Status DropDownList
        Dim EmployerJobTitle As New EmployerJobsFacade
        With ddlJobTitleId
            .DataTextField = "EmployerJobTitle"
            .DataValueField = "EmployerJobId"
            .DataSource = EmployerJobTitle.GetJobTitlesByEmployer(EmployerId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function

End Class
