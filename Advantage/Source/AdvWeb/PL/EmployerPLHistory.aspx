﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="EmployerPLHistory.aspx.vb" Inherits="EmployerPLHistory" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop2">
                        <br />
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="listframetop" style="background-color: transparent;">
                                    <table cellspacing="0" cellpadding="2" align="center" width="100%">
                                        <tr>
                                            <td class="contentcell" width="100px" style="vertical-align: top; line-height: 30px">
                                                <asp:Label ID="lblLastName" runat="Server" CssClass="label">Last Name</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left" style="background-color: transparent;">
                                                <asp:TextBox ID="txtLastName" runat="Server" CssClass="textbox" Enabled="True"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="revLastName" runat="server" ErrorMessage="Invalid search string"
                                                    ControlToValidate="txtLastName" ValidationExpression="[A-Za-z][\w\.\ \-]{0,15}">Invalid search string</asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" width="100px" style="vertical-align: top; line-height: 30px">
                                                <asp:Label ID="lblFirstName" runat="Server" CssClass="label">First Name</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left" style="background-color: transparent;">
                                                <asp:TextBox ID="txtFirstName" runat="Server" CssClass="textbox" Enabled="True"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" ErrorMessage="Invalid search string"
                                                    ControlToValidate="txtFirstName" ValidationExpression="[A-Za-z][\w\.\ \-]{0,15}">Invalid search string</asp:RegularExpressionValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" width="100px" style="vertical-align: top; line-height: 30px">
                                                <asp:Label ID="lblJobTitle" runat="Server" CssClass="label">Job Title</asp:Label>
                                            </td>
                                            <td class="contentcell4" align="left" style="background-color: transparent;">
                                                <asp:DropDownList ID="ddlJobTitleId" runat="Server" CssClass="dropdownlist">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="background-color: transparent;"></td>
                                            <td class="contentcell4" style="background-color: transparent;">
                                                <table cellspacing="0" cellpadding="2" align="center" width="100%" style="background-color: transparent;">
                                                    <tr>
                                                        <td width="50%" align="left" style="background-color: transparent;">
                                                            <%--<asp:Button ID="btnSearch" runat="Server" CssClass="button" Text="Search"></asp:Button>--%>
                                                            <telerik:RadButton ID="btnSearch" runat="server" Text="Search"></telerik:RadButton>
                                                        </td>
                                                        <td width="50%" style="background-color: transparent;">
                                                            <%--<input id="Reset1" type="reset" class="button" value="Reset" name="Reset1" runat="server" />--%>
                                                            <telerik:RadButton ID="btnReset" runat="server" Text="Reset"></telerik:RadButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="listframebottom">
                                    <div class="scrollleftfltr4rows">
                                        <asp:DataList ID="dlstEmployerContact" runat="server">
                                            <SelectedItemStyle CssClass="selecteditem"></SelectedItemStyle>
                                            <SelectedItemTemplate>
                                            </SelectedItemTemplate>
                                            <ItemStyle CssClass="NonSelectedItem"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:LinkButton Text='<%# Container.DataItem("LastName") & ", " & Container.Dataitem("FirstName") & " " & Container.DataItem("MiddleName")%>'
                                                    runat="server" CssClass="nonselecteditem" CommandArgument='<%# Container.DataItem("PlacementId")%>'
                                                    ID="Linkbutton2" CausesValidation="False" />
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="maincontenttable" style="width: 98%; border: none;">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->
                            <table width="98%" cellpadding="0" cellspacing="0" class="contenttable">
                                <asp:TextBox ID="txtPlacementId" CssClass="label" runat="server" Visible="False"></asp:TextBox>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblStudent" runat="server" CssClass="label">Student</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtStudent" CssClass="TextBox" runat="server" TabIndex="1"></asp:TextBox>
                                    </td>
                                    <td class="emptycell"></td>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblSalary" runat="server" CssClass="label">Salary</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtSalary" CssClass="TextBox" runat="server" TabIndex="12"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblSSN" runat="server" CssClass="label">SSN</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtSSN" CssClass="textbox" runat="server" TabIndex="2"></asp:TextBox>
                                    </td>
                                    <td class="emptycell"></td>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblSalaryType" runat="server" CssClass="label">Salary Type</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtSalaryType" CssClass="textbox" runat="server" TabIndex="12"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblJobTitleId" runat="server" CssClass="label">Employer Job Title</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtJobTitleId" CssClass="textbox" runat="server" TabIndex="3"></asp:TextBox>
                                    </td>
                                    <td class="emptycell"></td>
                                    <td nowrap class="contentcell" valign="top">
                                        <asp:Label ID="lblScheduleId" runat="server" CssClass="label">Schedule</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtScheduleId" CssClass="textbox" runat="server" TabIndex="14"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell" valign="top">
                                        <asp:Label ID="lblJobDescrip" runat="server" CssClass="label">Job Description</asp:Label>
                                    </td>
                                    <td class="contentcell2" colspan="4">
                                        <asp:TextBox ID="txtJobDescrip" CssClass="tocommentsnowrap" runat="server" TextMode="MultiLine" Width="200px" 
                                            Rows="4" TabIndex="5"></asp:TextBox>
                                    </td>
                                    <td class="emptycell"></td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblPlacementRep" CssClass="label" runat="server">Placement Rep</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtPlacementRep" CssClass="textbox" runat="server" TabIndex="8"></asp:TextBox>
                                    </td>
                                    <td class="emptycell"></td>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblBenefitsId" runat="server" CssClass="label">Benefits</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtBenefitsId" CssClass="textbox" runat="server" TabIndex="13"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblJobStatus" runat="server" CssClass="label">Job Status</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtJobStatus" CssClass="textbox" runat="server" TabIndex="6"></asp:TextBox>
                                    </td>
                                    <td class="emptycell"></td>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblHowPlaced" runat="server" CssClass="label">Placement Method</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtHowPlaced" CssClass="textbox" runat="server" TabIndex="15"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblJobInterview" runat="server" CssClass="label">From Interview?</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtJobInterview" CssClass="textbox" runat="server" TabIndex="16"></asp:TextBox>
                                    </td>
                                    <td class="emptycell"></td>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblDatePlaced" CssClass="label" runat="server">Date Placed</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtDatePlaced" CssClass="textbox" runat="server" TabIndex="17"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblSupervisor" CssClass="label" runat="server">Supervisor</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtSupervisor" CssClass="textbox" runat="server" TabIndex="7"></asp:TextBox>
                                    </td>
                                    <td class="emptycell"></td>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblStartDate" runat="server" CssClass="label">Start Date</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtStartDate" CssClass="textbox" runat="server" TabIndex="11"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="LblFldStudy" runat="server" CssClass="label">Field of Study</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtFldStudy" CssClass="textbox" runat="server" TabIndex="16"></asp:TextBox>
                                    </td>
                                    <td class="emptycell"></td>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblTerminationDate" CssClass="label" runat="server">Termination Date</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtTerminationDate" CssClass="textbox" runat="server" TabIndex="17"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblFee" runat="server" CssClass="label">Fee</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtFee" CssClass="textbox" runat="server" TabIndex="10"></asp:TextBox>
                                    </td>
                                    <td class="emptycell"></td>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblTerminationReason" CssClass="label" runat="server">Termination Reason</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtTerminationReason" CssClass="textbox" runat="server" TabIndex="9"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblEnrollment" runat="server" CssClass="label">Enrollment</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtEnrollment" CssClass="textbox" runat="server" TabIndex="10"></asp:TextBox>
                                    </td>
                                    <td class="emptycell"></td>
                                    <td class="contentcell">
                                        <asp:Label ID="lblJobTypeId" runat="server" CssClass="label" Visible="False">Job Type</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtJobTypeId" CssClass="textbox" runat="server" TabIndex="4" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblNotes" CssClass="label" runat="server">Comments</asp:Label>
                                    </td>
                                    <td colspan="4" class="contentcell2">
                                        <asp:TextBox ID="txtNotes" CssClass="tocommentsnowrap" runat="server" TextMode="MultiLine" Width="200px" 
                                            Rows="4" TabIndex="18"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>

                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

