<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master" Inherits="StudentsEmployment" CodeFile="StudentsEmployment.aspx.vb" %>
<%@ MasterType  virtualPath="~/NewSite.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
<%--    <script language="javascript" type="text/javascript">
        function OpenResponsibility() {
            window.open("../AD/Responsibility.aspx", "histWin", "width=500,height=150,resizable,scrollbars")
        }
        // document.body.onkeyup = ValidateResponsibilities();
    </script>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="listframetop">
                   Filter Not Applicable
                </td>
            </tr>
            <tr>
                <td class="listframebottom">
                    <div class="scrollleftfltr1row">
                       <asp:DataList ID="dlstEmployerContact" runat="server" DataKeyField="StEmploymentId">
                                        <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                        <ItemStyle CssClass="itemstyle"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton Text='<%# Container.DataItem("EmployerDescrip") & " - " & Container.DataItem("EmployerJobTitle") %> '
                                                runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("StEmploymentId")%>'
                                                ID="Linkbutton2" CausesValidation="False" />
                                        </ItemTemplate>
                        </asp:DataList>
                    </div>
                </td>
            </tr>
        </table>
    </telerik:RadPane>
    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
    
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                        ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                                    </asp:Button>
                                    </td>
                        </tr>
                    </table>
        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
        <td class="detailsframe">
        <div class="scrollright2">
                  <table width="100%" cellpadding="0" cellspacing="0" class="contenttable">
                                        <tr>
                                            <td nowrap class="contentcell">
                                                <asp:label ID="lblEmployerName" runat="server" CssClass="label"></asp:label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtEmployerName" CssClass="dropdownlist" runat="server" TabIndex="1"></asp:TextBox></td>
                                            <td class="emptycell">
                                            </td>
                                            <td nowrap class="contentcell">
                                                <asp:label ID="lblStartDate" runat="server" CssClass="label"></asp:label></td>
                                            <td class="contentcell4">
                                               <telerik:RadDatePicker ID="txtStartDate" runat="server" Width="300" daynameformat="Short">
                                                </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap class="contentcell">
                                                <asp:label ID="lblEmployerJobTitle" runat="server" CssClass="label"></asp:label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtEmployerJobTitle" CssClass="dropdownlist" runat="server" TabIndex="2"></asp:TextBox></td>
                                            <td class="emptycell">
                                            </td>
                                            <td nowrap class="contentcell">
                                                <asp:label ID="lblEndDate" runat="server" CssClass="label"></asp:label></td>
                                            <td class="contentcell4">
                                                 <telerik:RadDatePicker ID="txtEndDate" runat="server" Width="300" daynameformat="Short">
                                                 </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap class="contentcell">
                                                <asp:label ID="lblJobTitleId" runat="server" CssClass="label"></asp:label></td>
                                            <td class="contentcell4">
                                                <asp:dropdownlist ID="ddlJobTitleId" CssClass="dropdownlist" runat="server" TabIndex="3">
                                                </asp:dropdownlist></td>
                                            <td class="emptycell">
                                            </td>
                                            <td nowrap class="contentcell">
                                                &nbsp;</td>
                                            <td class="contentcell4">
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td nowrap class="contentcell">
                                                <asp:label ID="lblJobStatusId" runat="server" CssClass="label"></asp:label></td>
                                            <td class="contentcell4">
                                                <asp:dropdownlist ID="ddlJobStatusId" CssClass="dropdownlist" runat="server" TabIndex="4">
                                                </asp:dropdownlist></td>
                                            <td class="emptycell">
                                            </td>
                                            <td nowrap class="contentcell">
                                                &nbsp;</td>
                                            <td class="contentcell4">
                                                &nbsp;</td>
                                        </tr>
                                    </table>
                                    <table width="100%" cellpadding="0" cellspacing="0" class="contenttable">
                                        <tr>
                                            <td nowrap class="contentcell">
                                                <asp:label ID="lblComments" runat="server" CssClass="label"></asp:label></td>
                                            <td colspan="5" class="contentcell2">
                                                <asp:TextBox ID="txtComments" CssClass="ToCommentsNoWrap" runat="server" Columns="60"
                                                    Rows="3" TextMode="MultiLine" MaxLength="300" TabIndex="7"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td nowrap class="contentcell">
                                                <asp:label ID="lblJobResponsibilities" runat="server" CssClass="label"></asp:label></td>
                                            <td colspan="5" class="contentcell2">
                                                <asp:TextBox ID="txtJobResponsibilities" CssClass="ToCommentsNoWrap" runat="server"
                                                    Columns="60" Rows="3" TextMode="MultiLine" MaxLength="300" TabIndex="8"></asp:TextBox><br />
                                                <asp:label ID="lblRoleHelp" CssClass="label" runat="server">Please Separate The Job Responsibilities with a Period(.) To See an example 
													<a href="#BuildResume.aspx" onclick="OpenResponsibility();"><span style="text-decoration:underline">Click Here</span></a></asp:label></td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="6">
                                                    <asp:label ID="lblSDF" runat="server" Font-Bold="true" CssClass="label">School Defined Fields</asp:label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell2" colspan="6">
                                                    <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:TextBox ID="txtStudentId" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtStEmploymentId" runat="server" Visible="false"></asp:TextBox>
                                    <asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                                    <asp:TextBox ID="txtResourceID" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtRowIds" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtModDate" runat="server" Visible="False"></asp:TextBox>
                                    <!--end content tables-->
          
        </div>
        </td>
        </tr>
        </table>

    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>--%>
</asp:Content>

