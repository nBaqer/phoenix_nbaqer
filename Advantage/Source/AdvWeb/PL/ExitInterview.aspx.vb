Imports Fame.Common
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports System.Data

Partial Class ExitInterview
    Inherits BasePage
    Dim IneligibilityReasonsMandatory = True
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtPlacementId As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEnrollment As System.Web.UI.WebControls.Label
    Protected WithEvents lblEnrollmentDescrip As System.Web.UI.WebControls.Label
    Protected WithEvents lblScStatusId As System.Web.UI.WebControls.Label
    '    Protected WithEvents ddlAssistanceId As System.Web.UI.WebControls.DropDownList


    Protected WithEvents txtEnrollmentDescrip As System.Web.UI.WebControls.TextBox
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected state As AdvantageSessionState
    Protected WithEvents ddlEnrollmentId As System.Web.UI.WebControls.DropDownList

    'For Test Purpose
    'Protected StudentId As String '= "DCAF913F-F4B6-4ED3-B8E1-46C9CDFEAB69"


    Protected StudentId, LeadId As String
    Protected resourceId As Integer
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected userId As String
    Protected modCode As String

    Protected boolSwitchCampus As Boolean = False
    Protected sdfcontrols As New SDFComponent



#End Region

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        'AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)



        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim userId As String
        '        Dim m_context As HttpContext
        'Dim fac As New UserSecurityFacade
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ddlReason.BackColor = Drawing.Color.White
        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New StudentMRU
        objStudentState = getStudentFromStateObject(97) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With
        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''
        Dim advantageUserState As User = AdvantageSession.UserState

        Dim objCommon As New CommonUtilities

        'advantageUserState = AdvantageSession.UserState
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If
            InitButtonsForLoad()
            'Header1.EnableHistoryButton(False)
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'Disable the new and delete buttons
            'objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtExitInterviewId.Text = Guid.NewGuid.ToString
            BuildDDLS()
            BindDataList()
            ChkIsInDB.Checked = False
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(ResourceId, StudentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, ResourceId.ToString)
            'End If
            MyBase.uSearchEntityControlId.Value = ""
        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
        End If




        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfcontrols.GetSDFExists(resourceId, ModuleId)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtExitInterviewId.Text) <> "" Then
            sdfcontrols.GenerateControlsEdit(pnlSDF, resourceId, txtExitInterviewId.Text, ModuleId)
        Else
            sdfcontrols.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
        End If
    End Sub

    'Private Sub BuildExpertiseDDL()
    '    'Bind the Status DropDownList
    '    Dim PrgVersion As New JobSearchFacade
    '    With ddlExpertiseLevelId
    '        .DataTextField = "ExpertiseDescrip"
    '        .DataValueField = "ExpertiseId"
    '        .DataSource = PrgVersion.GetExpertiseLevel
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildFullPartTimeDDL()
    '    'Bind the Status DropDownList
    '    Dim PrgVersion As New JobSearchFacade
    '    With ddlFullTimeId
    '        .DataTextField = "FullPartTimeDescrip"
    '        .DataValueField = "FullTimeId"
    '        .DataSource = PrgVersion.GetFullPartTime
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildDDLS()

        BuildEnrollmentDDL()
        BuildEligibleDDL()
        BuildWantsAssistanceDDL()
        BuildWaiverSignedDDL()
        BuildReasonDDL()
        'BuildCountyDDL()
        'BuildSchoolStatusDDL()
        'BuildTransportationDDL()
        'BuildExpertiseDDL()
        'BuildFullPartTimeDDL()

        'BuildAreaDDL() (Counties)
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Counties DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlAreaId, AdvantageDropDownListName.Counties, campusId, True, True, String.Empty))

        'SchoolStatusDDL
        ddlList.Add(New AdvantageDDLDefinition(ddlScStatusId, AdvantageDropDownListName.Statuses, Nothing, True, True))

        'Transportation 
        ddlList.Add(New AdvantageDDLDefinition(ddlTransportationId, AdvantageDropDownListName.Transportation, campusId, True, True, String.Empty))

        'Expertise
        ddlList.Add(New AdvantageDDLDefinition(ddlExpertiseLevelId, AdvantageDropDownListName.ExpertiseLevel, campusId, True, True, String.Empty))

        'FullPartTime
        ddlList.Add(New AdvantageDDLDefinition(ddlFullTimeId, AdvantageDropDownListName.FullPartTime, campusId, True, True, String.Empty))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub
    Private Sub BuildEnrollmentDDL()
        'Bind The Enrollment DDL
        Dim studentEnrollments As New StudentsAccountsFacade
        With ddlPrgVerId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(StudentId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildEligibleDDL()
        rblEligible.Items.Clear()
        'Bind The Eligible DDL
        With rblEligible
            .Items.Insert(0, New ListItem("No", "No"))
            .Items.Insert(1, New ListItem("Yes", "Yes"))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildWantsAssistanceDDL()
        rblWantsAssistance.Items.Clear()
        'Bind The rblWantsAssistance DDL
        With rblWantsAssistance
            .Items.Insert(0, New ListItem("No", "No"))
            .Items.Insert(1, New ListItem("Yes", "Yes"))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildWaiverSignedDDL()
        rblWaiverSigned.Items.Clear()
        'Bind The rblWaiverSigned DDL
        With rblWaiverSigned
            .Items.Insert(0, New ListItem("No", "No"))
            .Items.Insert(1, New ListItem("Yes", "Yes"))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildReasonDDL()
        'Bind The ddlReason DDL
        ddlReason.ClearSelection()
        Dim IneligibilityReasons As New PlacementFacade
        With ddlReason
            .DataTextField = "InelReasonName"
            .DataValueField = "InelReasonId"
            .DataSource = IneligibilityReasons.GetIneligibilityReasons()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0

        End With
    End Sub
    Private Sub dlstEmployerContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstEmployerContact.ItemCommand
        Dim enrollmenthistory As New PlacementFacade
        txtExitInterviewId.Text = e.CommandArgument
        GetEnrollmentDetails(e.CommandArgument)
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = resourceId
        Master.SetHiddenControlForAudit()
        'set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, e.CommandArgument, ViewState, Header1)
        InitButtonsForEdit()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEdit(pnlSDF, resourceId, e.CommandArgument, ModuleId)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, e.CommandArgument)

    End Sub
    Private Sub GetEnrollmentDetails(ByVal ExitInterViewId As String)
        Dim enrollmenthistory As New PlacementFacade
        BindExitInterView(enrollmenthistory.GetExitInterview(ExitInterViewId))
    End Sub
    Private Sub BindExitInterView(ByVal enrollmentinfo As PlacementInfo)
        'Bind The EmployerInfo Data From The Database
        With enrollmentinfo
            ddlPrgVerId.SelectedValue = .EnrollmentId
            ddlScStatusId.SelectedValue = .SchoolStatusId
            rblWantsAssistance.SelectedValue = .WantAssistance
            rblWaiverSigned.SelectedValue = .WaiverSigned
            rblEligible.SelectedValue = .Eligible
            txtReason.Text = .Reason
            ddlReason.SelectedValue = .InelReasonId
            'ddlAreaId.SelectedValue = .AreaId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAreaId, .AreaId, .Area)
            CType(txtAvailableDate, RadDatePicker).DbSelectedDate = .AvailableDate
            txtAvailableDays.Text = .AvailableDays
            txtAvailableHours.Text = .AvailableHours
            txtLowSalary.Text = .LowSalary
            txtHighSalary.Text = .HighSalary
            'ddlTransportationId.SelectedValue = .TransportationId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlTransportationId, .TransportationId, .Transportation)
            ddlExpertiseLevelId.SelectedValue = .ExpertiseLevelId
            ddlFullTimeId.SelectedValue = .FullPartTime
            CType(txtExitInterviewDate, RadDatePicker).DbSelectedDate = .ExitInterviewDate
            ChkIsInDB.Checked = True
        End With
    End Sub
    Private Sub BindBlankExitInterView(ByVal enrollmentinfo As PlacementInfo)
        'Bind The EmployerInfo Data From The Database
        With enrollmentinfo
            txtReason.Text = .Reason
            CType(txtAvailableDate, RadDatePicker).DbSelectedDate = .AvailableDate
            CType(txtExitInterviewDate, RadDatePicker).DbSelectedDate = .ExitInterviewDate
            txtAvailableDays.Text = .AvailableDays
            txtAvailableHours.Text = .AvailableHours
            txtLowSalary.Text = .LowSalary
            txtHighSalary.Text = .HighSalary
            ChkIsInDB.Checked = False
            txtExitInterviewId.Text = .StEmploymentId
        End With
        rblWantsAssistance.ClearSelection()
        rblWaiverSigned.ClearSelection()
        rblEligible.ClearSelection()
        ddlReason.ClearSelection()
        'BuildEnrollmentDDL()
        'BuildCountyDDL()
        'BuildSchoolStatusDDL()
        'BuildTransportationDDL()
        'BuildExpertiseDDL()
        'BuildFullPartTimeDDL()
        BuildDDLS()
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim PlacementHistory As New PlacementFacade
        Dim Result As String
        '        Dim enrollmentcount As Integer

        'If ChkIsInDB.Checked = False Then
        '    enrollmentcount = PlacementHistory.GetCountExitInterView(ddlPrgVerId.SelectedValue, StudentId, txtAvailableDate.Text)
        '    If enrollmentcount >= 1 Then
        '        DisplayErrorMessage("An interview has already been scheduled for this Enrollment")
        '        Exit Sub
        '    End If
        'End If
        If rblEligible.SelectedValue.ToLower = "yes" Then
            IneligibilityReasonsMandatory = False
        Else
            IneligibilityReasonsMandatory = True
        End If

        If ( CType(txtExitInterviewDate, RadDatePicker).DbSelectedDate Is nothing) Then
            
            AlertAjaxMessage("- Exit Interview Date is required")
            Exit sub
        End If


        If IneligibilityReasonsMandatory Then
            If ddlReason.SelectedValue = "" Then
                AlertAjaxMessage("- Ineligibility Reasons is required")
                Exit Sub
            End If
        End If
            'Call Update Function in the plEmployerInfoFacade
            Result = PlacementHistory.UpdateStudentExitInterview(BuildExitInterviewHistory(StudentId), AdvantageSession.UserState.UserName)

        If Not Result = "" Then
            DisplayErrorMessage(Result)
            Exit Sub
        End If


        'Reset The Checked Property Of CheckBox To True
        ChkIsInDB.Checked = True

        BindDataList()

        ' CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, txtExitInterviewId.Text, ViewState, Header1)

        InitButtonsForEdit()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim SDFID As ArrayList
        Dim SDFIDValue As ArrayList
        '        Dim newArr As ArrayList
        Dim z As Integer
        Dim SDFControl As New SDFComponent
        Try
            SDFControl.DeleteSDFValue(txtExitInterviewId.Text)
            SDFID = SDFControl.GetAllLabels(pnlSDF)
            SDFIDValue = SDFControl.GetAllValues(pnlSDF)
            For z = 0 To SDFID.Count - 1
                SDFControl.InsertValues(txtExitInterviewId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
            Next
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try

        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, txtExitInterviewId.Text)

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        CustomValidator1.ErrorMessage = errorMessage
        CustomValidator1.IsValid = False

        If ValidationSummary1.ShowMessageBox = True And ValidationSummary1.ShowSummary = False And CustomValidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub AlertAjaxMessage(ByVal errorMessage As String)
        CommonWebUtilities.AlertAjax(errorMessage, CType(Me.Master.FindControl("RadAjaxManager1"), RadAjaxManager))
    End Sub
    Private Sub BindDataList()
        Dim Schedule As New PlacementFacade
        With dlstEmployerContact
            .DataSource = Schedule.GetAllInterviewScheduleByStudent(StudentId)
            .DataBind()
        End With
    End Sub
    Private Function BuildExitInterviewHistory(ByVal StudentId As String) As PlacementInfo
        Dim StudentEducation As New PlacementInfo
        With StudentEducation
            ''get IsInDB
            .IsInDb = ChkIsInDB.Checked
            .StudentId = StudentId
            .EnrollmentId = ddlPrgVerId.SelectedValue
            .SchoolStatusId = ddlScStatusId.SelectedValue
            .WantAssistance = rblWantsAssistance.SelectedValue
            .WaiverSigned = rblWaiverSigned.SelectedValue
            .Eligible = rblEligible.SelectedValue
            .Reason = txtReason.Text
            .InelReasonId = ddlReason.SelectedValue
            .AreaId = ddlAreaId.SelectedValue
            .AvailableDate = CType(txtAvailableDate, RadDatePicker).DbSelectedDate
            .AvailableDays = txtAvailableDays.Text
            .AvailableHours = txtAvailableHours.Text
            .LowSalary = txtLowSalary.Text
            .HighSalary = txtHighSalary.Text
            .TransportationId = ddlTransportationId.SelectedValue
            .InterviewId = txtExitInterviewId.Text
            .ExpertiseLevelId = ddlExpertiseLevelId.SelectedValue
            .FullPartTime = ddlFullTimeId.SelectedValue
            .InelReasonId = ddlReason.SelectedValue
            .ExitInterviewDate = CType(txtExitInterviewDate, RadDatePicker).DbSelectedDate
        End With
        Return StudentEducation
    End Function
    'Private Sub BuildCountyDDL()
    '    'Bind the County DrowDownList
    '   Dim county As New CountyFacade
    '    With ddlAreaId
    '        .DataTextField = "CountyDescrip"
    '        .DataValueField = "CountyId"
    '        .DataSource = county.GetAllCounty()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildSchoolStatusDDL()
    '    'Bind the County DrowDownList
    '    Dim SchoolStatus As New PlacementFacade
    '    With ddlScStatusId
    '        .DataTextField = "Status"
    '        .DataValueField = "StatusId"
    '        .DataSource = SchoolStatus.GetAllSchoolStatus()
    '        .DataBind()
    '        ' .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildTransportationDDL()
    '    'Bind the County DrowDownList
    '    Dim SchoolTransportation As New PlacementFacade
    '    With ddlTransportationId
    '        .DataTextField = "TransportationDescrip"
    '        .DataValueField = "TransportationId"
    '        .DataSource = SchoolTransportation.GetAllTransportation()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub buttonsforload()
        btnDelete.Enabled = False
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        'instantiate component
        Dim ExitInterview As New PlacementFacade

        'Delete The Row Based on EmployerId 
        Dim result As String = ExitInterview.DeleteExitInterview(txtExitInterviewId.Text)

        'If Delete Operation was unsuccessful
        If Not result = "" Then
            DisplayErrorMessage(result)
            Exit Sub
        End If

        'initialize buttons
        InitButtonsForLoad()

        'Create A Blank Record
        BindBlankExitInterView(New PlacementInfo)

        'Bind The DataList
        BindDataList()

        'Header1.EnableHistoryButton(False)

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
        'SDF Code Starts Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim SDFControl As New SDFComponent
        SDFControl.DeleteSDFValue(txtExitInterviewId.Text)
        SDFControl.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)

        Response.Redirect(HttpContext.Current.Request.Url.ToString(), True)

    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        BindBlankExitInterView(New PlacementInfo)
        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
        InitButtonsForLoad()
        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)
        checkIfReasonMandatiry()
    End Sub
    Protected Sub EligibleNO(sender As Object, e As EventArgs) Handles rblEligible.SelectedIndexChanged
        checkIfReasonMandatiry()
    End Sub

    Private Sub checkIfReasonMandatiry()
        If rblEligible.SelectedValue.ToLower = "yes" Then
            'mark Ineligibility Reasons as mandatory
            ddlReason.SelectedIndex = 0
            IneligibilityReasonsMandatory = False
            Reason.Text = "Ineligibility Reasons"
        Else
            Reason.Text = "Ineligibility Reasons" & "<font color=""red"">*</font>"
            IneligibilityReasonsMandatory = True
        End If
    End Sub
End Class
