﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports BL = Advantage.Business.Logic.Layer
Imports FAME.Advantage.Common

Partial Class EmployerSearch
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtLastName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtFirstName As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblSSN As System.Web.UI.WebControls.Label
    Protected WithEvents txtSSN As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblStudentID As System.Web.UI.WebControls.Label
    Protected WithEvents txtStudentID As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEnrollment As System.Web.UI.WebControls.Label
    Protected WithEvents txtEnrollment As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblStatus As System.Web.UI.WebControls.Label
    Protected WithEvents ddlStatus As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblProgram As System.Web.UI.WebControls.Label
    Protected WithEvents ddlProgram As System.Web.UI.WebControls.DropDownList
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents dgrdStudentSearch As System.Web.UI.WebControls.DataGrid
    Protected WithEvents StudentLNameLinkButton As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lblLastName As System.Web.UI.WebControls.Label
    Protected WithEvents lblFirstName As System.Web.UI.WebControls.Label
    Protected state As AdvantageSessionState


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()


        mruProvider = New BL.MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))

        'load Advantage state
        'state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        'state("empInfo") = Nothing
        ''save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        'Call mru component for employees
        'Dim objMRUFac As New MRUFacade
        'Dim ds As New DataSet

        'ds = objMRUFac.LoadMRU("Employers", Session("UserId").ToString(), HttpContext.Current.Request.Params("cmpid"))

        ''load Advantage state
        'state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        'state("MRUDS") = ds
        ''save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
    End Sub

#End Region

    Private campusId As String
    Private pObj As New UserPagePermissionInfo
    Protected resourceId As Integer
    Protected userId As String

    Private mruProvider As BL.MRURoutines
    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        Me.Form.DefaultButton = btnSearch.UniqueID
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        campusid = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            BuildStatusDDL()
            BuildCampusGroupsDDL()
            BuildCountyDDL()
            BuildEmployerIndustryDDL()
            BuildEmployerJobsDDL()
            BuildLocationDDL()
            BuildCityDDL()
            BuildStateDDL()
            BuildZipDDL()
            Session("NameCaption") = ""
            Session("NameValue") = ""
            Session("IdValue") = ""
            Session("IdCaption") = ""
            Session("StudentID") = Nothing
            Session("EmployerID") = ""
        End If
        DisableAllButtons()

        headerTitle.Text = Header.Title
    End Sub
    Private Sub DisableAllButtons()
        btnNew.Enabled = False
        btnDelete.Enabled = False
        btnSave.Enabled = False

    End Sub
    Private Sub BuildStatusDDL()
        'Bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStatusID
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCampusGroupsDDL()
        'Bind the CampusGroups DrowDownList
        Dim campusGroups As New CampusGroupsFacade
        With ddlCampGrpID
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCountyDDL()
        'Bind the County DrowDownList
        Dim county As New CountyFacade
        With ddlCountyID
            .DataTextField = "CountyDescrip"
            .DataValueField = "CountyId"
            .DataSource = county.GetAllCounty()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildEmployerIndustryDDL()
        'Bind the EmployerIndustry DrowDownList
        Dim Industry As New IndustryFacade
        With ddlIndustryID
            .DataTextField = "IndustryDescrip"
            .DataValueField = "IndustryId"
            .DataSource = Industry.GetAllIndustryForCampus(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildEmployerJobsDDL()

    End Sub
    Private Sub BuildCityDDL()
        Dim EmployerCity As New SearchEmployerFacade
        With ddlCity
            .DataTextField = "City"
            .DataValueField = "City"
            .DataSource = EmployerCity.GetAllCity()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildStateDDL()
        Dim EmployerCity As New SearchEmployerFacade
        With ddlState
            .DataTextField = "StateDescrip"
            .DataValueField = "StateID"
            .DataSource = EmployerCity.GetAllState()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildZipDDL()
        Dim EmployerCity As New SearchEmployerFacade
        With ddlZip
            .DataTextField = "Zip"
            .DataValueField = "Zip"
            .DataSource = EmployerCity.GetAllZip()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildLocationDDL()
        'Bind the Location DrowDownList
        Dim Location As New LocationFacade
        With ddlLocationID
            .DataTextField = "LocationDescrip"
            .DataValueField = "LocationId"
            .DataSource = Location.GetAllLocation()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BindDataList()
        'Bind The First,Middle,LastName based on EmployerContactId
        dgrdTransactionSearch.Visible = True
        Dim SearchFacade As New SearchEmployerFacade
        'dgrdTransactionSearch.DataSource = SearchFacade.GetEmployerSearch(txtCode.Text, txtEmployerID.Text, txtEmployerDescrip.Text, ddlCity.SelectedValue, ddlState.SelectedValue, ddlZip.SelectedValue, ddlCountyID.SelectedValue, ddlLocationID.SelectedValue, ddlStatusID.SelectedValue, ddlIndustryID.SelectedValue, ddlCampGrpID.SelectedValue)
        dgrdTransactionSearch.DataSource = SearchFacade.GetEmployerSearch(txtCode.Text, txtEmployerID.Text, txtEmployerDescrip.Text, ddlCity.SelectedValue, ddlState.SelectedValue, ddlZip.SelectedValue, ddlCountyID.SelectedValue, ddlLocationID.SelectedValue, ddlStatusID.SelectedValue, ddlIndustryID.SelectedValue, campusId)
        dgrdTransactionSearch.DataBind()
    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        'If txtStudentID.Text = "" And txtLastName.Text = "" And txtFirstName.Text = "" And txtEnrollment.Text = "" And ddlStatusId.SelectedValue = Guid.Empty.ToString And ddlProgram.SelectedValue = Guid.Empty.ToString Then
        '    DisplayErrorMessage(" Please Enter A Value To Search ")
        '    Exit Sub
        'End If
        Session("EmployerType") = "ExistingEmployer"
        If txtCode.Text = "" And txtEmployerID.Text = "" And txtEmployerDescrip.Text = "" And ddlCity.SelectedValue = Guid.Empty.ToString And ddlState.SelectedValue = Guid.Empty.ToString And ddlZip.SelectedValue = Guid.Empty.ToString And ddlCountyID.SelectedValue = Guid.Empty.ToString And ddlLocationID.SelectedValue = Guid.Empty.ToString And ddlStatusID.SelectedValue = Guid.Empty.ToString And ddlIndustryID.SelectedValue = Guid.Empty.ToString And ddlCampGrpID.SelectedValue = Guid.Empty.ToString Then
            'DisplayErrorMessage("Please select a search option")
            DisplayRADAlert(CallbackType.Postback, "Error1", "Please select a search option", "Save Error")
            Exit Sub
        End If

        BindDataList()
        Session("NameCaption") = ""
        Session("NameValue") = ""
        Session("IdValue") = ""
        Session("IdCaption") = ""
    End Sub
    Private Sub dgrdTransactionSearch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdTransactionSearch.ItemCommand
        If e.CommandName = "StudentSearch" Then
            'save studentId in the session so that it can be used by the StudentLedger program
            Dim EmployerName As New EmployerSearchFacade
            Dim defaultCampusId As String
            Dim objStateInfo As New AdvantageStateInfo
            Dim strVID As String = ""
            '  Dim state As AdvantageSessionState
            Dim fac As New UserSecurityFacade
            Dim arrUPP As New ArrayList
            Dim pURL As String

            defaultCampusId = HttpContext.Current.Request.Params("cmpid")
            defaultCampusId = XmlConvert.ToGuid(defaultCampusId).ToString

            'Session("EmployerId") = e.CommandArgument
            'Session("NameCaption") = "Employer"
            'Session("NameValue") = EmployerName.GetEmployerNameByID(Session("EmployerID"))
            'Session("IdValue") = ""
            'Session("IdCaption") = ""
            Session("SEARCH") = 1

            strVID = BuildEmployerStateObject(e.CommandArgument.ToString)
            Dim strCampusId As String = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString()
            Dim intSchoolOption As Integer = CommonWebUtilities.SchoolSelectedOptions(MyAdvAppSettings.AppSettings("SchedulingMethod"))

            AdvantageSession.MasterEmployerId = txtEmployerId.Text

            arrUPP = mruProvider.checkEntityPageSecurityStudentSearch(AdvantageSession.UserState, 193, strCampusId, 397, intSchoolOption)
            If arrUPP.Count = 0 Then
                'User does not have permission to any resource for this submodule
                'Session("Error") = "You do not have permission to any of the pages for existing student<br> for the campus that you are logged in to."
                'Response.Redirect("../ErrorPage.aspx")
                RadNotification1.Show()
                RadNotification1.Text = "You do not have permission to access any of the existing employer pages in the current campus"
            ElseIf mruProvider.DoesUserHasAccessToSubModuleResource(arrUPP, 79) Then
                AdvantageSession.EmployerMRU = Nothing
                Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
                Dim strVSI As String = R.Next()
                mruProvider.InsertMRU(2, txtEmployerID.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                Response.Redirect("../PL/EmployerInfo.aspx?resid=79&mod=PL&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=2" + "&VSI=" + strVSI, True)
            Else
                'redirect to the first page that the user has permission to for the submodule
                pURL = BuildPartialURL(arrUPP(0))
                mruProvider.InsertMRU(2, txtEmployerID.Text.ToString.Trim, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                Response.Redirect(pURL & "&mod=PL&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=2", True)
            End If
        End If
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)
    '    'Set error condition
    '    'Display error in message box in the client
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnSearch)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        'BIndToolTip()
    End Sub
    
    Private Function BuildPartialURL(ByVal uppInfo As UserPagePermissionInfo) As String
        Return uppInfo.URL & "?resid=" & uppInfo.ResourceId
    End Function


    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        txtCode.Text = ""
        txtEmployerDescrip.Text = ""
        ddlCity.SelectedIndex = 0
        ddlState.SelectedIndex = 0
        ddlZip.SelectedIndex = 0
        ddlCountyID.SelectedIndex = 0
        ddlLocationID.SelectedIndex = 0
        ddlStatusID.SelectedIndex = 0
        ddlIndustryID.SelectedIndex = 0
        ddlCampGrpID.SelectedIndex = 0

        dgrdTransactionSearch.Visible = False
    End Sub
    Private Function BuildEmployerStateObject(ByVal EmployerId As String) As String
        Dim objStateInfo As New AdvantageStateInfo
        Dim objGetStudentStatusBar As New AdvantageStateInfo
        Dim strVID As String
        Dim facInputMasks As New InputMasksFacade
        Dim strEmployerId As String = ""

        If EmployerId.ToString.Trim = "" Then 'No Student was selected from MRU
            strEmployerId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                                   2, _
                                                                   AdvantageSession.UserState.CampusId.ToString)
        Else
            strEmployerId = EmployerId
        End If

        objGetStudentStatusBar = mruProvider.BuildEmployerStatusBar(strEmployerId)
        With objStateInfo
            .EmployerId = objGetStudentStatusBar.EmployerId
            .NameValue = objGetStudentStatusBar.NameValue
            .Address1 = objGetStudentStatusBar.Address1
            .Address2 = objGetStudentStatusBar.Address2
            .City = objGetStudentStatusBar.City
            .State = objGetStudentStatusBar.State
            .Zip = objGetStudentStatusBar.Zip
            .Phone = objGetStudentStatusBar.Phone
        End With

        'Create a new guid to be associated with the employer pages
        strVID = Guid.NewGuid.ToString

        Session("EmployerObjectPointer") = strVID

        txtEmployerID.Text = strEmployerId

        'Add an entry to AdvantageSessionState for this guid and object
        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        'UpdateMRUTable
        'Reason: We need to keep track of the last student record the user worked with
        'Scenario1 : User can log in and click on a student page without using MRU 
        'and we need to display the data of the last student the user worked with
        'If the user is a first time user, we will load the student who was last added to advantage
        'mruProvider.InsertMRU(2, strEmployerId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

        Return strVID
    End Function
End Class
