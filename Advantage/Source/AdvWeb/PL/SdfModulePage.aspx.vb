﻿
Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.Common

Namespace AdvWeb.PL
    Partial Class SdfModulePage
        Inherits BasePage

        Protected ResourceId As String
        Protected ModuleId As String
        Private pObj As New UserPagePermissionInfo
        Protected CampusId As String
        Protected UserId As String
        Protected HasEdit As Boolean

        Private Sub SdfModulePage_Load(sender As Object, e As EventArgs) Handles Me.Load
           
            Dim advantageUserState As User = AdvantageSession.UserState
            ResourceId = HttpContext.Current.Request.Params("resid")
            CampusId = AdvantageSession.UserState.CampusId.ToString
            UserId = AdvantageSession.UserState.UserId.ToString
            ModuleId = AdvantageSession.UserState.ModuleCode.ToString
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)
            HasEdit = pObj.HasEdit

        End Sub
    End Class
End Namespace