﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SearchJobs.aspx.vb" Inherits="SearchJobs" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table cellpadding="0" cellspacing="0" class="maincontenttable" style="width: 98%; border: none;">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <!-- begin table content-->
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="8">
                                                    <asp:Label ID="lblgeneral" runat="server" Font-Bold="true" CssClass="label">Student Search For Jobs</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblPrgVerId" runat="server" CssClass="label">Program of Study</asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlPrgVerId" TabIndex="1" CssClass="dropdownlist" runat="server"></asp:DropDownList></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap></td>
                                                <td class="leadcell2" nowrap></td>
                                             
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblAvailableDate" runat="server" CssClass="label">Available Date From</asp:Label></td>
                                                <td class="leadcell2" nowrap valign="bottom">
                                                 
                                                    <telerik:RadDatePicker ID="txtAvailableDate" MinDate="1/1/1945" runat="server">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblMinSalary" runat="server" CssClass="label">Min Salary</asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:TextBox ID="txtMinSalary" TabIndex="2" CssClass="textbox" runat="server"></asp:TextBox></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblMaxSalary" runat="server" CssClass="label">Max Salary</asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:TextBox ID="txtMaxSalary" TabIndex="5" CssClass="textbox" runat="server"></asp:TextBox></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblAvailableDateTo" runat="server" CssClass="label">Available Date To</asp:Label></td>
                                                <td class="leadcell2" nowrap valign="bottom">
                                                   
                                                    <telerik:RadDatePicker ID="txtAvailableDateTo" MinDate="1/1/1945" runat="server">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblCountyId" runat="server" CssClass="label">County</asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlCountyId" TabIndex="3" CssClass="dropdownlist" runat="server"></asp:DropDownList></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblFullTime" runat="server" CssClass="label" Visible="False">Full Time / Part Time</asp:Label></td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlFullTime" TabIndex="46" CssClass="dropdownlist" runat="server" Visible="False"></asp:DropDownList></td>
                                                <td class="leadcellspacer" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>&nbsp;</td>
                                                <td class="leadcell" nowrap>&nbsp;</td>
                                            </tr>
                                        </table>
                                        <table width="100%" align="center" border="0">
                                            <tr>
                                                <td style="text-align: center; padding: 16px">
                                                    <telerik:RadButton ID="btnSearch" runat="server" CssClass="buttoncontent" Width="80px" Text="Search" TabIndex="7"></telerik:RadButton>
                                                    <telerik:RadButton ID="btnReset" runat="server" CssClass="buttoncontent" Width="80px" Text="Reset" TabIndex="8"></telerik:RadButton>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="8">
                                                    <asp:Label ID="label3" runat="server" Font-Bold="true" CssClass="label">Student Search Results</asp:Label></td>
                                            </tr>
                                        </table>
                                        <table width="100%" align="center" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td style="padding-top: 16px">
                                                    <asp:DataGrid ID="dgrdTransactionSearch" runat="server" CellPadding="0" BorderStyle="Solid" AutoGenerateColumns="False"
                                                        AllowSorting="True" HeaderStyle-Wrap="true" EditItemStyle-Wrap="false" GridLines="Horizontal"
                                                        Width="100%" BorderWidth="1px" BorderColor="#E0E0E0">
                                                        <EditItemStyle CssClass="datagriditemstyle"></EditItemStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Last Name">
                                                                <HeaderStyle CssClass="datagridheader" Width="40%"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkLastName" runat="server" Text='<%# container.dataitem("LastName")%>' CssClass="label" CausesValidation="False" CommandArgument='<%# Container.DataItem("StudentId") %>'>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="First Name">
                                                                <HeaderStyle CssClass="datagridheader" Width="40%"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDgFirstName" Text='<%# container.dataitem("FirstName")%>' CssClass="label" runat="server">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="SSN">
                                                                <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="label1" Text='<%# container.dataitem("SSN")%>' CssClass="label" runat="server">
                                                                    </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <asp:TextBox ID="txtStudentDocId" runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtEnterKey" runat="server" Visible="false"></asp:TextBox>
            <!-- start validation panel-->
            <asp:Panel ID="Panel2" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator2" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="Panel3" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary2" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>

