<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="SDFModulePageOld.aspx.vb" Inherits="SDFModulePage" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <title>Add Custom Field to Page</title>
    <link rel="stylesheet" type="text/css" href="../CSS/localhost.css" />
    <script type="text/javascript" language="javascript">
        function DatePopUp(ctl, w, h) {
            var PopupWindow = null;
            settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('JobsDate.aspx?Ctl=' + ctl, 'DatePicker', settings);
            PopupWindow.focus();
        }
        function refreshLeadInfoSession() {
            sessionStorage.setItem("captionRequirementdb", "{}");
        }
	</script>    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
   <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
   </asp:ScriptManagerProxy>
   <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
             <telerik:AjaxSetting AjaxControlID="PnlList">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PnlList"  />
                    <telerik:AjaxUpdatedControl ControlID="PnlContent"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="btnAdd">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PnlList"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
             <telerik:AjaxSetting AjaxControlID="btnRemove">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PnlContent"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnPages">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddlModuleId"  />
                    <telerik:AjaxUpdatedControl ControlID="lstAvailablePages"  />
                    <telerik:AjaxUpdatedControl ControlID="lstVisibility"  />
                    <telerik:AjaxUpdatedControl ControlID="lstSelectedPages"  />
                    <telerik:AjaxUpdatedControl ControlID="PnlList"  />
                    <telerik:AjaxUpdatedControl ControlID="PnlContent"  />
                </UpdatedControls>
            </telerik:AjaxSetting>

          <telerik:AjaxSetting AjaxControlID="ddlModuleId">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="lstAvailablePages"  />
                    <telerik:AjaxUpdatedControl ControlID="lstVisibility"  />
                    <telerik:AjaxUpdatedControl ControlID="lstSelectedPages"  />
                    <telerik:AjaxUpdatedControl ControlID="PnlList"  />
                    <telerik:AjaxUpdatedControl ControlID="PnlContent"  />
                </UpdatedControls>
            </telerik:AjaxSetting>

                     

            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                     <telerik:AjaxUpdatedControl ControlID="PnlContent"  />
                     <telerik:AjaxUpdatedControl ControlID="PnlList"  />
                </UpdatedControls>
            </telerik:AjaxSetting>

        </AjaxSettings> 
</telerik:RadAjaxManagerProxy>
   <div style="overflow:auto;">
        <telerik:radsplitter id="oldcontentsplitter" runat="server" collapsemode="none" height="100%" orientation="vertical" 
            visibleduringinit="false" borderwidth="0px" onclientresized="OldPageResized" style="overflow:auto;">
                <telerik:radpane id="oldcontentpane" runat="server" borderwidth="0px" scrolling="Both" orientation="horizontaltop">
                   
                    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
				        <tr>
					        <!-- begin rightcolumn -->
					        <td class="DetailsFrameTop">
                                <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
							        <!-- begin top menu (save,new,reset,delete,history)-->
							        <tr>
								        <td class="MenuFrame" align="right">
                                        <asp:button id="btnSave" runat="server" Text="Save" CssClass="save"></asp:button>
                                        <asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="false"></asp:button>
                                        <asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Enabled="false"></asp:button></td>
							        </tr>
						        </table>
						        <!-- end top menu (save,new,reset,delete,history)-->
						        <!--begin right column-->
                                
						            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
							            <tr>
								            <td class="detailsframe">
									            <div class="scrollsingleframe">
										            <!-- begin table content-->
                                                    <asp:panel id="PnlList" runat="server">
										                <table cellspacing="0" cellpadding="0" width="40%" border="0" align="center">
											                <asp:textbox id="txtStudentId" CssClass="Label" Runat="server" Width="200px" visible="false"></asp:textbox>
											                <asp:textbox id="txtStEmploymentId" CssClass="Label" Runat="server" Width="200px" visible="false"></asp:textbox>
											                <asp:checkbox id="ChkIsInDB" runat="server" Visible="false" checked="False"></asp:checkbox>
											                <tr>
												                <td class="twocolumnlabelcell"><asp:label id="lblSDFId" runat="server" cssClass="Label">User Defined Field</asp:label><font color="red">*</font></td>
												                <td class="twocolumncontentcellleft"><asp:dropdownlist id="ddlSDFId"  Runat="server"></asp:dropdownlist>
												                </td>
                                                                <td><asp:RequiredFieldValidator ID="rqSDF" runat="server" ControlToValidate="ddlSDFId" Display="None" ErrorMessage="User Defined Field is required"></asp:RequiredFieldValidator></td>
											                </tr>
											                <tr>
												                <td class="twocolumnlabelcell"><asp:label id="lblEntity" runat="server" cssClass="Label">Entity</asp:label><font color="red">*</font></td>
												                <td class="twocolumncontentcellleft"><asp:dropdownlist id="ddlEntity"  Runat="server" AutoPostBack="false"></asp:dropdownlist></td>
                                                                <td><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlEntity" Display="None" ErrorMessage="Entity is required"></asp:RequiredFieldValidator></td>
											                </tr>
											                <tr>
												                <td class="twocolumnlabelcell"><asp:label id="lblModuleId" runat="server" cssClass="Label" Visible="false">Module</asp:label></td>
												                <td class="twocolumncontentcellleft"><asp:dropdownlist id="ddlModuleId"  Runat="server" Enabled="false" AutoPostBack="true" Visible="false"></asp:dropdownlist></td>
											                </tr>
											                <tr>
												                <td class="twocolumnlabelcell"></td>
												                <td class="twocolumncontentcellleft"><asp:button id="btnPages"  Runat="server" Text="Build Pages" Visible ="true"/></td>
											                </tr>
											                <tr>
												                <td class="spacertables"></td>
											                </tr>
											                <tr>
												                <td class="spacertables"></td>
											                </tr>
										                </table>
                                                    
										                <table width="800px" cellpadding="0" cellspacing="0" align="center" class="contenttable">
											                <asp:label id="lblError" CssClass="Label" Runat="server" visible="false" ForeColor="red"></asp:label>
											                <tr>
												                <td nowrap class="fourcolumnheader"><asp:Label ID="Label2" Runat="server" CssClass="Label" Font-Bold="True">Available Pages</asp:Label></td>
												                 <td></td>
                                                                <td nowrap class="fourcolumnheader"><asp:Label ID="lblPossibleSkills" Runat="server" CssClass="Label" Font-Bold="True">UDF Visibility</asp:Label></td>
												                <td nowrap class="fourcolumnheaderspacer"></td>
												                <td nowrap class="fourcolumnheader"><asp:Label ID="Label3" Runat="server" CssClass="Label" Font-Bold="True">Apply To Pages   - Visibility</asp:Label></td>
											                </tr>
											                <tr>
												                <td nowrap class="fourcolumncontent">
                                                                    <asp:listbox id="lstAvailablePages" Runat="server" cssClass="listboxes" height="200px" width="235px" AutoPostback="false"
														                Rows="10" >
                                                                    </asp:listbox>
													             </td>
												                <td></td>
                                                                <td nowrap class="fourcolumncontent">
                                                                    <asp:listbox id="lstVisibility" height="200px" Runat="server" cssClass="listboxes" width="235px" >
                                                                    </asp:listbox>			
												                </td>
												                <td nowrap class="fourcolumnbuttons">
                                                                    <asp:Button ID="btnAdd" Text="Add >" Runat="server" Width="100px"></asp:Button><br>
													                <asp:Button ID="btnRemove" Text="< Remove" Runat="server" Width="100px"></asp:Button><br>
												                <td nowrap class="fourcolumncontent">
                                                                    <asp:listbox id="lstSelectedPages" height="200px" Runat="server" cssClass="listboxes" width="235px"></asp:listbox></td>
											                </tr>
										                </table>
                                                    </asp:panel>
									            </div>
								            </td>
							            </tr>
						            </table>
                               
					        </td>
					        <!-- end rightcolumn --></tr>
			        </table>
                </telerik:radpane>
            </telerik:radsplitter> 
      <!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel>
       <asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        <asp:TextBox ID="txtStudentDocId" Runat="server" Visible="False"></asp:TextBox>
		<!--end validation panel-->
        <telerik:RadNotification runat="server" ID="RadNotification1" 
                Text="" ShowCloseButton="true" 
                Width="400px" Height="125px"
                TitleIcon="" 
                Position="Center" Title="Message" 
                EnableRoundedCorners="true" 
                EnableShadow="true" 
                Animation="Fade" 
                AnimationDuration="1000" 
                   
                style="padding-left:120px; padding-top:5px; word-spacing:2pt;"> 
    </telerik:RadNotification> 	
    </div>
</asp:Content>

