﻿Imports System.Activities.Statements
Imports System.Data
Imports System.Diagnostics.Eventing.Reader
Imports System.IO
Imports System.Net
Imports Fame.Advantage.Common
Imports Fame.Advantage.MultiTenantHost.Lib.Presenters
Imports Fame.AdvantageV1.BusinessFacade
Imports Fame.AdvantageV1.Common
Imports Fame.Common
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects

Partial Class PL_StudentDocumentManagement
    Inherits BasePage

    Protected WithEvents ddlScannedId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtStudentDocs As System.Web.UI.WebControls.TextBox
    Protected strCount As String
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblDocumentModule As System.Web.UI.WebControls.Label

    Protected WithEvents ddlDocumentTypeId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblStudentName As System.Web.UI.WebControls.Label
    Protected WithEvents ddlStudentName As System.Web.UI.WebControls.DropDownList
    Protected StudentId As String '= "14094BD8-18CA-4B95-9195-069D47774306"
    Private Presenter As TenantPickerPresenter
    Private ReqforTermination As String = "Req for Termination"
    Private NotApproved As String = "Not Approved"
    Private R2T4 As String = "R2T4"
    Private TerminationDetails As String = "Termination details"

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected state As AdvantageSessionState
    Protected LeadId As String
    Dim resourceId As Integer
    Private pObj As New UserPagePermissionInfo
    Protected selectedModule As Integer
    Protected campusid As String
    Dim userId As String
    Protected ModuleString As String
    Protected boolSwitchCampus As Boolean = False
    Protected boolSwitchStudent As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings
    Public _isSupportUser As Boolean = False

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub

#End Region

#Region "MRU Routines"

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU
        Try
            MyBase.GlobalSearchHandler(0)
            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus
            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If

            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"
            Master.ShowHideStatusBarControl(True)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + Master.CurrentCampusId + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim objCommon As New CommonUtilities
        resourceId = HttpContext.Current.Items("ResourceId")
        campusid = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleString = Request.QueryString("mod").ToString
        _isSupportUser = IsSupportUser()


        'Put user code to initialize the page here
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(301) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString
        End With

        Session("ModuleId") = 2

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusid)

        pnlDocMgmtByFAME.Visible = True
        pnlDocMgmtBySchoolDocs.Visible = False
        'End If
        pnlUpload.Visible = False
        OldStudentDocGrid.Visible = False
        pnlViewDoc.Visible = False

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            If Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value) Then
                RadGrid1.Visible = False
            End If

            InitButtonsForLoad()
            strCount = 0
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"

            ''Bind The DropDownList
            BuildDDLs()
            IsStudentDocsPage()
            'Get PK Value
            txtStudentDocId.Text = Guid.NewGuid.ToString()

            'Get Default Document when page is loaded for first time
            ddlDocumentId.Enabled = True

            If ModuleString = "AR" Then
                ddlDocumentModule.SelectedValue = 1
                ddlModulesId.SelectedValue = 1
            ElseIf ModuleString = "AD" Then
                ddlDocumentModule.SelectedValue = 2
                ddlModulesId.SelectedValue = 2
            ElseIf ModuleString = "HR" Then
                ddlDocumentModule.SelectedValue = 5
                ddlModulesId.SelectedValue = 5
            ElseIf ModuleString = "FA" Then
                ddlDocumentModule.SelectedValue = 4
                ddlModulesId.SelectedValue = 4
            ElseIf ModuleString = "PL" Then
                ddlDocumentModule.SelectedValue = 6
                ddlModulesId.SelectedValue = 6
            ElseIf ModuleString = "SA" Then
                ddlDocumentModule.SelectedValue = 7
                ddlModulesId.SelectedValue = 7

            ElseIf ModuleString = "FC" Then
                ddlDocumentModule.SelectedValue = 3
                ddlModulesId.SelectedValue = 3
            End If

            If resourceId = CommonConstants.StudentDocsResourceId Then 'if resourceid=301(student docs)
                applyFilterForStudentDocs()
            ElseIf resourceId = CommonConstants.DocumentManagementResourceId Then
                applyFilterForDocumentMgmt()
            End If

            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                BuildSession()
            End If

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""
        Else
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
        End If
    End Sub

    Private Sub BuildSession()
        With ddlTrackCode
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("TrackCode", txtStudentDocId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlDueDate
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("DueDate", txtStudentDocId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", FormatDateTime(Date.Now, DateFormat.ShortDate)))
            .SelectedIndex = 0
        End With
        With ddlTransDate
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("TransDate", txtStudentDocId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", FormatDateTime(Date.Now, DateFormat.ShortDate)))
            .SelectedIndex = 0
        End With
        With ddlCompletedDate
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("CompletedDate", txtStudentDocId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", FormatDateTime(Date.Now, DateFormat.ShortDate)))
            .SelectedIndex = 0
        End With
        With ddlNotificationCode
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("NotificationCode", txtStudentDocId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    ''' <summary>
    ''' 'if Resource = Document Management(283) hide the header section where it displays student details and hide Student ddl and show the left panel with all the documents present .
    ''' 'if resourceid=301(student docs) then display the same page hide studentnameddl, the left panel with all the documents present and show the panel with studentname and docname.
    ''' </summary>
    Private Sub IsStudentDocsPage()
        If resourceId = CommonConstants.DocumentManagementResourceId Then
            CType(Master.FindControl("ContentMain1"), ContentPlaceHolder).Visible = False
            BindDocumentManagementDataList()
            trStudentName.Visible = True
            trTypeFilter.Visible = True
            dlstDocumentMgmt.Visible = True
            dlstDocumentStatus.Visible = False
            ddlStudentId.Enabled = True
        ElseIf resourceId = CommonConstants.StudentDocsResourceId Then
            CType(Master.FindControl("ContentMain1"), ContentPlaceHolder).Visible = True
            trStudentName.Visible = False
            trTypeFilter.Visible = False
            dlstDocumentStatus.Visible = True
            dlstDocumentMgmt.Visible = False
            ddlStudentId.SelectedValue = LCase(StudentId)
            ddlStudentId.Enabled = False
        End If
    End Sub

    Private Sub BuildStudentDDL()
        Dim StudentName As New LeadFacade
        With ddlStudentId
            .DataTextField = "FullName"
            .DataValueField = "StudentID"
            .DataSource = StudentName.GetAllStudentNames()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildStudentNameFilterDDL()
        Dim StudentNameList As New LeadFacade
        With ddlStudent_Name
            .DataTextField = "FullName"
            .DataValueField = "StudentID"
            .DataSource = StudentNameList.GetAllStudentNames()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub EnableBtnStudentType()
    End Sub

    Private Sub BuildDocumentListDDL(ByVal ModuleID As Integer)
        ddlDocumentId.Items.Clear()
        Dim DocumentList As New LeadDocsFacade
        Dim docsFacade As New LeadEntranceFacade
        Dim intPrgVerId As Integer = docsFacade.getPrgVersionByStudent(StudentId)
        With ddlDocumentId
            .DataTextField = "DocumentDescrip"
            .DataValueField = "DocumentId"
            If intPrgVerId >= 1 Then
                .DataSource = docsFacade.GetAllStandardDocumentsByStudentEnrollment(StudentId, ModuleID, campusid)
            Else
                .DataSource = docsFacade.GetAllStandardDocumentsByEffectiveDates(StudentId, campusid)
            End If
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildExistingDocumentListDDL(ByVal ModuleID As Integer, ByVal StudentDocumentId As String, ByVal CampusId As String, Optional ByVal includeSubmittedDocumentsThatAreExpired As Boolean = False, Optional _
                                                ByVal documentTypeSelected As String = "")
        ddlDocumentId.Items.Clear()
        Dim DocumentList As New LeadDocsFacade
        Dim docsFacade As New LeadEntranceFacade
        Dim intPrgVerId As Integer = docsFacade.getPrgVersionByStudent(StudentId)
        With ddlDocumentId
            Dim ds As DataSet
            .DataTextField = "DocumentDescrip"
            .DataValueField = "DocumentId"
            If (documentTypeSelected <> TerminationDetails And documentTypeSelected <> R2T4) Then
                If intPrgVerId >= 1 Then
                    ds = docsFacade.GetAllExistingStandardDocumentsByStudentEnrollment(StudentId, ModuleID, StudentDocumentId, CampusId, includeSubmittedDocumentsThatAreExpired)
                Else
                    ds = docsFacade.GetAllStandardDocumentsByEffectiveDates(StudentId, CampusId)
                End If
            Else
                ds = docsFacade.GetAllTerminationDocuments(ModuleID)
            End If
            If Not ds Is Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    .DataSource = ds
                    .DataBind()
                End If
            End If

            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildDocumentStatusDDL(Optional ByVal StudentDocId As String = "")
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlDocStatusId
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = DocumentStatus.GetAllDocumentStatus(campusid, StudentDocId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    ''' <summary>
    ''' The BuildDocumentsTypesList method gets data from database and bind data into ddlDocTypeSearchControl.
    ''' </summary>
    Private Sub BindDocumentsTypesList()
        Dim documentList As New LeadDocsFacade
        With ddlDocTypeSearchControl
            .DataTextField = "DocumentTypeDescription"
            .DataValueField = "DocumentTypeId"
            .DataSource = documentList.GetDocumentTypes(campusid)
            .DataBind()
        End With
    End Sub

    Private Sub BuildExistingDocumentStatusDDL(ByVal StudentDocumentId As String)
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlDocStatusId
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = DocumentStatus.GetAllExistingDocumentStatus(StudentDocumentId, campusid)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildModuleDDL()
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlModulesId
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleId"
            .DataSource = DocumentStatus.GetAllDocumentModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            '.SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildDocumentModuleDDL()
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlDocumentModule
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleId"
            .DataSource = DocumentStatus.GetAllDocumentModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", "0"))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildDocumentModuleDDLForDocManagement()
        'Bind the Document DropDownList
        Dim documentStatus As New StudentDocsFacade
        With ddlDocumentModule
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleId"
            .DataSource = documentStatus.GetAllDocumentModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        With ddlModulesId
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleId"
            .DataSource = documentStatus.GetAllDocumentModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub BuildDocumentFilterStatusDDL()
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlDocFilterStatus
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = DocumentStatus.GetAllDocumentStatus(campusid)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildDDLs()
        If resourceId = CommonConstants.StudentDocsResourceId Then
            BuildDocumentFilterStatusDDL()
            BuildStudentDDL()
            BuildDocumentStatusDDL()
            BuildDocumentModuleDDL()
            BuildModuleDDL()
            BindDocumentsTypesList()
            BindDataList(CInt(ddlDocumentModule.SelectedValue), StudentId)
            BuildDocumentListDDL(CInt(ddlDocumentModule.SelectedValue))
            If Not ddlModulesId.SelectedIndex = 0 Then
                BuildExistingDocumentListDDL(ddlModulesId.SelectedValue, "", campusid)
            End If
        ElseIf resourceId = CommonConstants.DocumentManagementResourceId Then
            BuildDocumentModuleDDLForDocManagement()
            BuildDocumentFilterStatusDDLForDocManagement()
            BuildStudentDDLForDocManagement()
            BuildAllDocumentListDDLForDocManagement()
            BindDocumentsTypesList()
            BindDataList()
        End If
    End Sub

    Private Sub BuildAllDocumentListDDLForDocManagement()
        'Bind the Document DropDownList
        Dim documentList As New LeadDocsFacade
        With ddlDocumentId
            .DataTextField = "DocumentDescrip"
            .DataValueField = "DocumentId"
            .DataSource = documentList.GetAllDocumentsByStudent(campusid)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildStudentDDLForDocManagement()
        Dim studentName As New AdReqsFacade
        Dim strStudentIdentifier As String = MyAdvAppSettings.AppSettings("StudentIdentifier")
        With ddlStudentId
            .DataTextField = "Fullname"
            .DataValueField = "StudentId"
            .DataSource = studentName.GetAllDocumentStudentNames(Trim(campusid), strStudentIdentifier)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        With ddlStudent_Name
            .DataTextField = "FullName"
            .DataValueField = "StudentId"
            .DataSource = studentName.GetAllDocumentStudentNames(Trim(campusid), strStudentIdentifier)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildDocumentFilterStatusDDLForDocManagement()
        'Bind the Document DropDownList
        Dim documentStatus As New StudentDocsFacade
        With ddlDocFilterStatus
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = documentStatus.GetAllDocumentStatus(campusid)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        With ddlDocStatusId
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = documentStatus.GetAllDocumentStatus(campusid)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim StudentDocs As New StudentDocsFacade
        If resourceId = CommonConstants.DocumentManagementResourceId Then
            StudentId = ddlStudentId.SelectedValue
        End If
        Dim Result As String

        Dim message As String
        strCount = 0
        message = ""
        If ddlModulesId.SelectedValue = "" Then
            message &= "Module is required" & vbLf
        End If
        If ddlDocumentId.SelectedValue = "" Then
            message &= "Document is required" & vbLf
        End If
        If ddlDocStatusId.SelectedValue = "" Then
            message &= "Document Status is required" & vbLf
        End If

        If Not message = "" Then
            DisplayRADAlert(CallbackType.Postback, "Error1", message, "Save Error")
            Exit Sub
        End If

        'Call Update Function in the plEmployerInfoFacade
        Result = StudentDocs.UpdateStudentDocs(BuildStudentDocsInfo(), Session("UserName"), txtStudentDocId.Text, ddlModulesId.SelectedValue)

        '  If DML is not successful then Prompt Error Message
        If Not Result = "" Then
            Result = Result.Replace("'", " ")
            DisplayRADAlert(CallbackType.Postback, "Error2", Result, "Save Error")
            Exit Sub
        End If

        'Reset The Checked Property Of CheckBox To True
        ChkIsInDB.Checked = True

        ''Bind The DataList Based On StudentId and Education Institution Type if request is from StudentDocs page .
        If resourceId = CommonConstants.DocumentManagementResourceId Then
            BindDataList()
        ElseIf resourceId = CommonConstants.StudentDocsResourceId Then
            BindDataList(CInt(ddlDocumentModule.SelectedValue), StudentId)
        End If

        'If Page is free of errors Show Edit Buttons
        If Page.IsValid Then
            InitButtonsForEdit()
        End If
        If resourceId = CommonConstants.DocumentManagementResourceId Then
            CommonWebUtilities.RestoreItemValues(dlstDocumentMgmt, txtStudentDocId.Text)
        ElseIf resourceId = CommonConstants.StudentDocsResourceId Then
            CommonWebUtilities.RestoreItemValues(dlstDocumentStatus, txtStudentDocId.Text)
        End If
        RadGrid1.Visible = True
    End Sub

    Private Function BuildStudentDocsInfo() As StudentDocsInfo
        Dim StudentDocsInfo As New StudentDocsInfo
        Dim intDocHasBeenApproved As Integer = 0
        intDocHasBeenApproved = (New LeadDocsFacade).CheckIfDocHasBeenApproved(ddlDocStatusId.SelectedValue)
        With StudentDocsInfo
            .IsInDb = ChkIsInDB.Checked
            .StudentId = StudentId
            .DocumentId = ddlDocumentId.SelectedValue
            .DocStatusId = ddlDocStatusId.SelectedValue
            If resourceId = CommonConstants.StudentDocsResourceId Then
                If Not RdRequestDate.SelectedDate Is Nothing Then
                    .RequestDate = RdRequestDate.SelectedDate
                End If
                If Not RdReceivedDate.SelectedDate Is Nothing Then
                    .ReceiveDate = RdReceivedDate.SelectedDate
                End If

                .ScannedId = chkScannedId.Checked
                .StudentDocsId = txtStudentDocId.Text
                .ModuleId = ddlModulesId.SelectedValue
                If intDocHasBeenApproved >= 1 Then
                    chkOverride.Checked = False
                    .Override = False
                Else
                    .Override = chkOverride.Checked
                End If
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    .TrackCode = ddlTrackCode.SelectedValue
                    .DueDate = ddlDueDate.SelectedValue
                    .TransDate = ddlTransDate.SelectedValue
                    .CompletedDate = ddlCompletedDate.SelectedValue
                    .NotificationCode = ddlNotificationCode.SelectedValue
                    .DocDescrip = txtNotes.Text
                End If
            ElseIf resourceId = CommonConstants.DocumentManagementResourceId Then
                If RdRequestDate.SelectedDate Is Nothing Then
                    .RequestDate = ""
                Else
                    .RequestDate = Date.Parse(RdRequestDate.SelectedDate)
                End If
                If RdReceivedDate.SelectedDate Is Nothing Then
                    .ReceiveDate = ""
                Else
                    .ReceiveDate = Date.Parse(RdReceivedDate.SelectedDate)
                End If
                .StudentId = ddlStudentId.SelectedValue
                .ScannedId = chkScannedId.Checked
                .StudentDocsId = txtStudentDocId.Text
                .ModuleId = ddlModulesId.SelectedValue
                If intDocHasBeenApproved >= 1 Then
                    chkOverride.Checked = False
                    .Override = False
                Else
                    .Override = chkOverride.Checked
                End If
            End If
        End With
        Return StudentDocsInfo
    End Function

    Private Sub BindDataList(ByVal ModuleId As Integer, ByVal StudentId As String)
        With New LeadFacade
            dlstDocumentStatus.DataSource = .GetDocsByStudentAndModule(ModuleId, StudentId, campusid)
            dlstDocumentStatus.DataBind()
        End With
    End Sub

    Private Sub BindDataList()
        With New LeadFacade
            'dlstDocumentStatus.DataSource = .GetDocsStudent(ModuleId, Studentid, Trim(campus))
            dlstDocumentMgmt.DataSource = .GetAllDocsStudentByCampus(Trim(campusid))
            dlstDocumentMgmt.DataBind()
        End With
    End Sub

    Private Sub dlstDocumentStatus_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstDocumentStatus.ItemCommand
        ChkIsInDB.Checked = True
        Dim selectedItem = DirectCast(e.CommandSource, System.Web.UI.WebControls.LinkButton).[Text]

        txtStudentDocId.Text = e.CommandArgument
        GetStudentInfo(e.CommandArgument, selectedItem)

        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = resourceId
        Master.SetHiddenControlForAudit()
        'set Style to Selected Item

        Dim facade As New DocumentManagementFacade
        Dim strStudentName As String = facade.GetStudentNamesById(StudentId)
        Dim strFileNameOnly As String = Server.UrlEncode(strStudentName & "_" & StudentId)

        txtExtension.Text = facade.GetDocumentExtension(strFileNameOnly, ddlDocumentId.SelectedItem.Text)
        txtDocumentName.Text = ddlDocumentId.SelectedItem.Text
        txtStudentName.Text = strStudentName
        txtStudentId.Text = StudentId
        ddlDocumentId.Enabled = True

        pnlViewDoc.Visible = False
        RadGrid1.Visible = True

        'Refresh The DataGrid With Document Types
        BuildFileDataGrid(strFileNameOnly)
        dgrdDocs.Visible = True

        InitButtonsForEdit()
        CommonWebUtilities.RestoreItemValues(dlstDocumentStatus, e.CommandArgument)

        Dim linkName = DirectCast(e.CommandSource, System.Web.UI.WebControls.LinkButton).[Text]
        If linkName = R2T4 Or linkName = TerminationDetails Then
            InitializeTerminationDetails(False)
        Else
            InitializeTerminationDetails(True)
        End If
    End Sub

    Private Sub GetStudentInfo(ByVal StStudentDocId As String, Optional ByVal documentTypeSelected As String = "")
        Dim studentinfo As New StudentDocsFacade
        BindStudentInfoExist(studentinfo.GetStudentDocs(StStudentDocId), StStudentDocId, documentTypeSelected)
    End Sub

    Private Sub BindStudentInfoExist(ByVal StudentDocsInfo As StudentDocsInfo, ByVal StudentDocumentid As String, Optional ByVal documentTypeSelected As String = "")
        'Bind The StudentInfo Data From The Database
        With StudentDocsInfo
            If resourceId = CommonConstants.DocumentManagementResourceId Then  'if Resource = Document Management(283)
                'txtRequestDate.Text = .RequestDate
                Try
                    RdRequestDate.SelectedDate = .RequestDate
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    RdRequestDate.Clear()
                End Try
                Try
                    RdReceivedDate.SelectedDate = .ReceiveDate
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    RdReceivedDate.Clear()
                End Try
                ddlModulesId.SelectedValue = .ModuleId
                ddlStudentId.SelectedValue = .StudentId
                Try
                    BuildDocumentListDDL_DocMgmt(.ModuleId, StudentDocumentid, documentTypeSelected)  '' doc mgmt
                    ddlDocumentId.SelectedValue = .DocumentId
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlDocumentId.SelectedIndex = 0
                End Try
                Try
                    BuildDocumentStatusDDL(StudentDocumentid)
                    ddlDocStatusId.SelectedValue = .DocStatusId
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlDocStatusId.SelectedIndex = 0
                End Try
                If .ScannedId = "1" Then
                    chkScannedId.Checked = True
                Else
                    chkScannedId.Checked = False
                End If
                chkOverride.Checked = .Override
                'End With

            ElseIf resourceId = CommonConstants.StudentDocsResourceId Then 'if resourceid=301(student docs)
                'txtRequestDate.Text = .RequestDate
                'txtReceiveDate.Text = .ReceiveDate
                If .RequestDate <> "" Then
                    RdRequestDate.SelectedDate = .RequestDate
                End If
                If .ReceiveDate <> "" Then
                    RdReceivedDate.SelectedDate = .ReceiveDate
                End If

                ddlModulesId.SelectedValue = .ModuleId
                ddlStudentId.SelectedValue = .StudentId
                Try
                    BuildExistingDocumentListDDL(.ModuleId, StudentDocumentid, campusid, True, documentTypeSelected)
                    ddlDocumentId.SelectedValue = .DocumentId
                Catch ex As System.Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlDocumentId.SelectedIndex = 0
                End Try
                Try
                    BuildExistingDocumentStatusDDL(StudentDocumentid)
                    ddlDocStatusId.SelectedValue = .DocStatusId
                Catch ex As System.Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlDocStatusId.SelectedIndex = 0
                End Try
                If .ScannedId = "1" Then
                    chkScannedId.Checked = True
                Else
                    chkScannedId.Checked = False
                End If
                chkOverride.Checked = .Override
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    ddlTrackCode.SelectedValue = .TrackCode
                    ddlDueDate.SelectedValue = .DueDate
                    ddlTransDate.SelectedValue = .TransDate
                    ddlCompletedDate.SelectedValue = .CompletedDate
                    ddlNotificationCode.SelectedValue = .NotificationCode
                    txtNotes.Text = .DocDescrip
                End If
            End If
        End With
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'Reset The Value of chkIsInDb Checkbox
        'To Identify an Insert
        InitializeTerminationDetails(True)
        ddlStudentId.Enabled = (resourceId <> CommonConstants.StudentDocsResourceId)
        BindEmploymentInfo()
        txtStudentDocId.Text = Guid.NewGuid.ToString()
        InitButtonsForLoad()
        dgrdDocs.Visible = False
        Dim dt As New DataTable
        dt.Columns.Add("DisplayName")

        RadGrid1.DataSource = dt
        RadGrid1.DataBind()
        RadGrid1.Visible = False
        btnDelete.Enabled = False
        With ddlDocumentId
            .Items.Clear()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        BuildDocumentStatusDDL()
        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            pnlRegent.Visible = True
            BuildSession()
        End If
        'set Style to Selected Item
        CommonWebUtilities.RestoreItemValues(dlstDocumentStatus, Guid.Empty.ToString)
    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub

    Private Sub BindEmploymentInfo()
        RdRequestDate.Clear()
        RdReceivedDate.Clear()
        If resourceId = CommonConstants.StudentDocsResourceId Then
            ddlStudentId.SelectedValue = StudentId
        ElseIf resourceId = CommonConstants.DocumentManagementResourceId Then
            ddlStudentId.SelectedIndex = 0
        End If
        ddlModulesId.SelectedIndex = 0
        ddlDocumentId.SelectedIndex = 0

        ddlDocStatusId.SelectedIndex = 0
        chkScannedId.Checked = False
        ChkIsInDB.Checked = False
        chkOverride.Checked = False
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If resourceId = CommonConstants.DocumentManagementResourceId Then
            StudentId = ddlStudentId.SelectedValue
        End If
        If Not (StudentId = Guid.Empty.ToString) Then
            '    '   instantiate component
            Dim StudentDocs As New StudentDocsFacade

            '    'Delete The Row Based on StudentId
            Dim result As Integer = StudentDocs.DeleteStudentDocs(txtStudentDocId.Text, StudentId, ddlDocumentId.SelectedValue)

            '    'If Delete Operation was unsuccessful
            If result < 0 Then
                Customvalidator1.ErrorMessage = "There was a problem with Referential Integrity.<BR> You can not perform this operation."
                Customvalidator1.IsValid = False
            End If
            BindEmploymentInfo()
            ChkIsInDB.Checked = False

            Dim intModule As Integer
            If ddlDocumentModule.SelectedIndex = 0 Then
                intModule = 0
            Else
                intModule = CInt(ddlDocumentModule.SelectedValue)
            End If

            If resourceId = CommonConstants.DocumentManagementResourceId Then
                GetDocumentsByStatusForDocMgmt(ddlDocFilterStatus.SelectedValue, ddlStudent_Name.SelectedValue, ddltypeofreq.SelectedValue, ddlDocTypeSearchControl.SelectedValue)
            ElseIf resourceId = CommonConstants.StudentDocsResourceId Then
                GetDocumentsByStatus(ddlDocFilterStatus.SelectedValue, StudentId, intModule, ddltypeofreq.SelectedValue, ddlDocTypeSearchControl.SelectedValue)
            End If

            'initialize buttons
            InitButtonsForLoad()
        End If
        ddlDocumentModule.ClearSelection()
        If resourceId = CommonConstants.DocumentManagementResourceId Then
            CommonWebUtilities.RestoreItemValues(dlstDocumentMgmt, Guid.Empty.ToString)
        ElseIf resourceId = CommonConstants.StudentDocsResourceId Then
            CommonWebUtilities.RestoreItemValues(dlstDocumentStatus, Guid.Empty.ToString)
        End If
        RadGrid1.Rebind()
    End Sub

    Public Sub btnApplyFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApplyFilter.Click
        InitializeTerminationDetails(True)
        If resourceId = CommonConstants.StudentDocsResourceId Then 'if resourceid=301(student docs)
            applyFilterForStudentDocs()
        ElseIf resourceId = CommonConstants.DocumentManagementResourceId Then
            applyFilterForDocumentMgmt()
        End If
    End Sub

    Private Sub GetDocumentsByStatus(ByVal DocumentStatusId As String, ByVal StudentId As String, ByVal ModuleId As Integer, ByVal TypeofRequirement As Integer, ByVal DocumentTypeId As String)
        With New LeadFacade
            dlstDocumentStatus.DataSource = .GetDocsStudentByStatusAndModule(ddlDocFilterStatus.SelectedValue, ModuleId, StudentId, campusid, TypeofRequirement, DocumentTypeId)
            dlstDocumentStatus.DataBind()
        End With
    End Sub

    Private Sub ddlModulesId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModulesId.SelectedIndexChanged
        ddlDocumentId.Enabled = True
        If Not ddlModulesId.SelectedIndex = 0 Then
            BuildExistingDocumentListDDL(ddlModulesId.SelectedValue, "", campusid)
        End If
    End Sub

    Private Sub ddlDocStatusId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocStatusId.SelectedIndexChanged
    End Sub

    Private Sub ddlDocumentModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocumentModule.SelectedIndexChanged
        If Not ddlDocumentModule.SelectedIndex = 0 Then
            Session("ModuleID") = ddlDocumentModule.SelectedValue
        Else
            Session("ModuleID") = ""
        End If
        RadGrid1.Visible = False
    End Sub

    Private Sub chkScannedId_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkScannedId.CheckedChanged
        If chkScannedId.Checked = True Then
            '   pnlUpload.Visible = True
        Else
            pnlUpload.Visible = False
        End If
    End Sub

    Private Sub BuildDocumentDDL()
        'Bind the County DrowDownList
        Dim Documents As New DocumentManagementFacade
        With ddlDocumentTypeId
            .DataTextField = "DocumentDescrip"
            .DataValueField = "DocumentId"
            .DataSource = Documents.GetAllDocumentManagement()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildStudentsDDL()
        'Bind the County DrowDownList
        Dim Documents As New DocumentManagementFacade
        With ddlStudentName
            .DataTextField = "FullName"
            .DataValueField = "StudentId"
            .DataSource = Documents.GetStudentNames()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Function CreateDirectory(ByVal FolderName As String) As String
        Dim strDirectoryPath = MyAdvAppSettings.AppSettings("CreateDocumentPath")
        Dim oDirectoryInfo As DirectoryInfo
        '        Dim oSubDirectoryInfo As DirectoryInfo
        Dim strDirectory As String
        If Directory.Exists(strDirectoryPath & FolderName) = False Then
            oDirectoryInfo = Directory.CreateDirectory(strDirectoryPath + FolderName)
            strDirectory = strDirectoryPath & FolderName
        Else
            strDirectory = strDirectoryPath & FolderName
        End If
        Return strDirectory
    End Function

    Private Function ValidatePage() As String
        Dim strDocumentInsert As New DocumentManagementFacade
        Dim sMessage As String = ""

        If ddlStudentId.SelectedValue = Guid.Empty.ToString Then
            sMessage = "Student is required" & vbLf
        End If

        If ddlDocumentId.SelectedValue = "" Then
            sMessage &= "Document is required" & vbLf
        End If

        If ddlModulesId.SelectedValue = "" Then
            sMessage &= "Module is required" & vbLf
        End If

        If ddlDocStatusId.SelectedValue = "" Then
            sMessage &= "Document Status is required" & vbLf
        End If

        If Not sMessage = "" Then
            '   DisplayErrorMessage(sMessage)
            Return sMessage
            '  Exit Function
        End If

        ''Check which page
        If resourceId = CommonConstants.DocumentManagementResourceId Then
            StudentId = ddlStudentId.SelectedValue
        End If
        ''Check if the student record is saved
        ''If the record is not saved then force the user to save the document info before uploading the document.
        Dim isStudentDocumentRecordCreated As Boolean
        isStudentDocumentRecordCreated = strDocumentInsert.IsStudentDocumentAlreadyCreated(Trim(StudentId), Trim(ddlDocumentId.SelectedValue))
        If Not isStudentDocumentRecordCreated Then
            Return (" Please save the information to upload the document ")
            Exit Function
        End If
        Return ""
    End Function

    Private Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim Result As Integer
        Dim strDirectory As String

        Dim strDocumentInsert As New DocumentManagementFacade
        Dim sMessage As String

        sMessage = ValidatePage()
        If Not sMessage = "" Then
            DisplayErrorMessage(sMessage)
            Exit Sub
        End If
        If Not txtUpLoad.PostedFile Is Nothing And Not txtUpLoad.Value = "" Then
            'Get Directory
            Dim strDocumentType As String = ddlDocumentId.SelectedItem.Text
            strDirectory = CreateDirectory(strDocumentType)

            'Get FileName from the client machine
            Dim strExt As String = txtUpLoad.PostedFile.FileName.Substring(txtUpLoad.PostedFile.FileName.LastIndexOf(".")).ToLower()

            'Get Friendly Name for storing
            Dim slashposition As Integer = txtUpLoad.PostedFile.FileName.LastIndexOf("\")
            Dim strDisplayName As String = txtUpLoad.PostedFile.FileName.Substring(slashposition + 1)
            strDisplayName = strDisplayName.ToString.Split(".")(0)

            Dim intBackSlashPos As Integer = InStrRev(txtUpLoad.PostedFile.FileName, "\")
            If intBackSlashPos < 1 Then
                intBackSlashPos = InStrRev(txtUpLoad.PostedFile.FileName, "/")
            End If
            Dim strOrigFileName As String = Mid(txtUpLoad.PostedFile.FileName, intBackSlashPos + 1)
            Dim strFindExtPos As Integer = InStr(strOrigFileName, ".")
            Dim strUserFileName As String = Mid(strOrigFileName, 1, strFindExtPos - 1)

            'Get Size of file
            Dim filesize As Integer = txtUpLoad.PostedFile.ContentLength

            'Get type of file
            Dim facade As New DocumentManagementFacade
            Dim strStudentId As String
            If resourceId = CommonConstants.DocumentManagementResourceId Then
                strStudentId = Trim(ddlStudentId.SelectedValue)
            ElseIf resourceId = CommonConstants.StudentDocsResourceId Then
                strStudentId = Trim(StudentId)
            End If

            Dim strStudentName As String = facade.GetStudentNamesById(strStudentId)
            Dim filetype As String = txtUpLoad.PostedFile.ContentType
            Dim strFileNameOnly As String = Server.UrlEncode(strStudentName & "_" & strStudentId & "_" & strUserFileName).ToLower()
            Dim strFileName As String = strStudentName & "_" & strStudentId & "_" & strUserFileName & strExt

            If filesize <= 0 Then
                DisplayErrorMessage("Document upload process failed")
            Else
                txtUpLoad.PostedFile.SaveAs(strDirectory + "\" + strFileNameOnly + strExt)
                Result = strDocumentInsert.InsertDocument(strFileNameOnly, strExt, AdvantageSession.UserState.UserName, ddlDocumentId.SelectedItem.Text, StudentId, ddlDocumentId.SelectedValue, ddlModulesId.SelectedValue, strDisplayName)
                If Result = 0 Then
                    DisplayErrorMessage("Document uploaded successfully")
                End If
            End If
            Dim getExtension As New DocumentManagementFacade
            txtExtension.Text = getExtension.GetDocumentExtension(strFileNameOnly, ddlDocumentId.SelectedItem.Text)

            txtDocumentName.Text = strDocumentType
            txtStudentName.Text = strStudentName
            txtStudentId.Text = strStudentId

            'Refresh The DataGrid With Document Types
            BuildFileDataGrid(strFileNameOnly)
            dgrdDocs.Visible = True
        End If
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False
        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub

    Private Sub lnkViewDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkViewDoc.Click
        Session("DocumentName") = txtDocumentName.Text
        Session("StudentName") = txtStudentName.Text
        Session("StudentId") = txtStudentId.Text
        Session("Path") = AppDomain.CurrentDomain.BaseDirectory
        Session("DocumentType") = ddlDocumentId.SelectedItem.Text
        Session("Extension") = txtExtension.Text
        txtDocumentType.Text = Session("DocumentType")
    End Sub

    Private Sub BuildFileDataGrid(Optional ByVal FileName As String = "")
        Dim File As New DocumentManagementFacade
        Dim dsnew As New DataSet
        If FileName <> "" Then
            dsnew = File.GetAllDocumentsByStudentDocType(FileName, ddlDocumentId.SelectedItem.Text, IsSupportUser())
            dgrdDocs.DataSource = dsnew
            dgrdDocs.DataBind()
        End If

        If resourceId = CommonConstants.DocumentManagementResourceId Then
            StudentId = ddlStudentId.SelectedValue
        End If

        If ddlDocumentId.SelectedValue <> "" Then
            dsnew = File.GetAllDocumentsByStudentIdandDocumentID(StudentId, ddlDocumentId.SelectedValue, IsSupportUser())
        End If

        TableRow.DisabledCssClass = FileName
        RadGrid1.MasterTableView.IsItemInserted = False
        RadGrid1.MasterTableView.ClearEditItems()
        RadGrid1.DataSource = dsnew
        RadGrid1.DataBind()
    End Sub

    Private Sub dgrdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdDocs.ItemCommand

        Dim FileName As String = e.CommandArgument
        Dim DocumentType As String = ddlDocumentId.SelectedItem.Text
        Dim result As String
        Dim docfacade As New DocumentManagementFacade

        If e.CommandName = "DeleteFile" Then
            Dim strPath As String = MyAdvAppSettings.AppSettings("DocumentPath")
            Dim strFileNameWithExtension As String = CType(e.Item.FindControl("Linkbutton1"), LinkButton).CommandArgument
            Dim strExtension As String = Mid(strFileNameWithExtension, InStrRev(strFileNameWithExtension, ".") + 1)
            Dim strFullPath As String = strPath + DocumentType + "\" + FileName + "." + strExtension
            System.IO.File.Delete(strFullPath)
            'Delete The File,ReBind The DataList and Exit Procedure
            result = docfacade.DeleteDocument(FileName, ddlModulesId.SelectedValue, ddlDocumentId.SelectedValue)
            If Not result = "" Then
                DisplayErrorMessage(result)
            End If
            BuildFileDataGrid(FileName)
            chkScannedId.Checked = False
            Exit Sub
        End If

        Session("File2Browse") = MyAdvAppSettings.AppSettings("DocumentPath") & DocumentType.Replace("/", "\") & "\" & FileName
        Dim popupScript As String = "<script type='text/javascript'>window.Onload=OpenDocuments1();function OpenDocuments1(){window.open('../FileBrowser.aspx','HistWin','width=700,height=600,resizable=yes');}</script>"
        ClientScript.RegisterStartupScript(Me.GetType(), "PopUpScript", popupScript)

    End Sub

    Private Sub ddlDocFilterStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDocFilterStatus.SelectedIndexChanged
    End Sub

    Private Function ValidateDateFormat(ByVal strDate As String) As Integer
        Try
            Dim myDateTimeUS As System.DateTime
            Dim format As New System.Globalization.CultureInfo("en-US", True)
            myDateTimeUS = System.DateTime.Parse(strDate, format)
            Return 0
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return -1
        End Try
    End Function

    Public Sub PostDataForStudent()
        Dim uriString As String
        uriString = "http://aphost.net:9493/143?User%Name=Administrator&Password=temp&DB=FAME"
        ' Create a new WebClient instance.
        Dim myCache As New CredentialCache()
        myCache.Add(New Uri(uriString), "Basic",
                    New NetworkCredential("Administrator", "temp"))

        Dim myWebClient As New WebClient()
        myWebClient.Credentials = myCache
        Console.WriteLine(ControlChars.Cr + "Please enter the data to be posted to the URI {0}:", uriString)
        Dim postData As String = uriString

        Dim getStudentDataToPost As New StudentDocsFacade
        Dim objSchoolDocsInfo As New SchoolDocsInfo
        Dim strStudentID As String

        objSchoolDocsInfo = getStudentDataToPost.GetStudentDataToPostToSchoolDocs(StudentId)
        If objSchoolDocsInfo.StudentNumber = "" Then
            strStudentID = objSchoolDocsInfo.LastName & objSchoolDocsInfo.FirstName & objSchoolDocsInfo.SSN
        Else
            strStudentID = objSchoolDocsInfo.StudentNumber
        End If

        myWebClient.Headers.Add("Student_ID", strStudentID)
        myWebClient.Headers.Add("Last_Name", objSchoolDocsInfo.LastName)
        myWebClient.Headers.Add("First_Name", objSchoolDocsInfo.FirstName)
        myWebClient.Headers.Add("Social_Security_Number", objSchoolDocsInfo.SSN)
        myWebClient.Headers.Add("Student_Status_ID", objSchoolDocsInfo.StudentStatus)
        myWebClient.Headers.Add("Drop_Number", "")
        myWebClient.Headers.Add("Drop_Comment", objSchoolDocsInfo.DropReason)
        myWebClient.Headers.Add("Drop_Date", "")
        myWebClient.Headers.Add("Probation_Status", "")
        myWebClient.Headers.Add("Probation_Type", "")
        myWebClient.Headers.Add("Probation_Level", "")
        myWebClient.Headers.Add("Begin_Date", objSchoolDocsInfo.StartDate.ToShortDateString)
        myWebClient.Headers.Add("Reason", "")
        myWebClient.Headers.Add("End_Date", "6/6/2005")
        myWebClient.Headers.Add("Gender", objSchoolDocsInfo.StudentGender)
        myWebClient.Headers.Add("Date_of_Birth", objSchoolDocsInfo.DOB.ToShortDateString)
        myWebClient.Headers.Add("Ethnic_ID", objSchoolDocsInfo.StudentRace)
        myWebClient.Headers.Add("Campus_ID", objSchoolDocsInfo.Campuses)
        myWebClient.Headers.Add("Funding_Number", "")
        myWebClient.Headers.Add("Drivers_License", "")
        myWebClient.Headers.Add("Admin_Rep_Employee_ID", objSchoolDocsInfo.AdmissionsRep)
        myWebClient.Headers.Add("First_Term_ID", "")
        myWebClient.Headers.Add("Program_Name", objSchoolDocsInfo.ProgramName)
        myWebClient.Headers.Add("Session", "D")
        myWebClient.Headers.Add("LDA", objSchoolDocsInfo.LDA.ToShortDateString)
        myWebClient.Headers.Add("Determination_Date", objSchoolDocsInfo.DateDetermined.ToShortDateString)
        myWebClient.Headers.Add("Expected_Graduation_Date", objSchoolDocsInfo.ExpGradDate.ToShortDateString)
        myWebClient.Headers.Add("Grad_or_Termination_Date", "")
        myWebClient.Headers.Add("Last_Date_of_Attendance", "")
        myWebClient.Headers.Add("Student_Status_Date", "")
        myWebClient.Headers.Add("Status_Dropped", objSchoolDocsInfo.StatusDropped)
        myWebClient.Headers.Add("Status_Graduated", objSchoolDocsInfo.StatusGraduated)

        ' Apply ASCII Encoding to obtain the string as a byte array.
        Dim byteArray As Byte() = Encoding.ASCII.GetBytes(postData)
        Console.WriteLine("Uploading to {0} ...", uriString)
        ' Upload the input string using the HTTP 1.0 POST method.
        Dim responseArray As Byte() = myWebClient.UploadData(uriString, "POST", byteArray)
        ' Decode and display the response.
        PostDocsByURL("http://aphost.net:9493/144?User%Name=Administrator&Password=temp&DB=FAME&Student_ID=" + strStudentID)
    End Sub

    Public Sub PostDocsByURL(ByVal URL As String)
        Dim URLString As String
        Dim http As HttpWebRequest
        URLString = URL
        http = CType(WebRequest.Create(URLString), HttpWebRequest)

        Dim myCache As New CredentialCache()
        myCache.Add(New Uri(URLString), "Basic",
                    New NetworkCredential("Administrator", "temp"))
        http.Credentials = myCache
        Dim WebResponse As HttpWebResponse
        WebResponse = CType(http.GetResponse(), HttpWebResponse)
        Dim ResponseStream As StreamReader
        ResponseStream = New StreamReader(WebResponse.GetResponseStream())
        lbl1.Text = ResponseStream.ReadToEnd
        lbl1.Text = Replace(lbl1.Text, "<TH Style=""font: bold 11px Verdana; color: #000066; text-align: left; background-color: #E9EDF2; border-right: 1px solid #ebebeb; border-bottom: 1px solid #ebebeb; padding: 3px; vertical-align: middle"" nowrap>DT</TH>", "<TH Style=""font: bold 11px Verdana; color: #000066; text-align: left; background-color: #E9EDF2; border-right: 1px solid #ebebeb; border-bottom: 1px solid #ebebeb; padding: 3px; vertical-align: middle"" nowrap>Document</TH>")
        lbl1.Text = Replace(lbl1.Text, "<Table Style=""Font-Family: Verdana; Font-Size: 12"" CellPadding=""1"" CellSpacing=""0"" Border=""1"" Width=""100%"">", "<Table Style=""Font: normal 10px verdana; border: 1px solid #ebebeb"" CellPadding=""0"" CellSpacing=""0"" Width=""100%"">")
        lbl1.Text = Replace(lbl1.Text, "<TD NoWrap BGColor=""#D9FFFF"">", "<TD Style=""padding: 3px; font:normal 11px Verdana;color: #000066;text-align: left;border-right: 1px solid #ebebeb;border-bottom: 1px solid #ebebeb;vertical-align: top;"" nowrap>")
        lbl1.Text = Replace(lbl1.Text, "<TH>", "<TH Style=""font: bold 11px Verdana; color: #000066; text-align: left; background-color: #E9EDF2; border-right: 1px solid #ebebeb; border-bottom: 1px solid #ebebeb; padding: 3px; vertical-align: middle"" nowrap>")
        lbl1.Text = Replace(lbl1.Text, "<TH NoWrap>", "<TH Style=""	font: bold 11px Verdana; color: #000066; text-align: left; background-color: #E9EDF2; border-right: 1px solid #ebebeb; border-bottom: 1px solid #ebebeb; padding: 3px; vertical-align: middle"" nowrap>")
        lbl1.Text = Replace(lbl1.Text, "<TD Style=""padding: 3px; font:normal 11px Verdana;color: #000066;text-align: left;border-right: 1px solid #ebebeb;border-bottom: 1px solid #ebebeb;vertical-align: top;"" nowrap></TD>", "<TD Style=""padding: 3px; font:normal 11px Verdana;color: #000066;text-align: left;border-right: 1px solid #ebebeb;border-bottom: 1px solid #ebebeb;vertical-align: top;"" nowrap>&nbsp;</TD>")
        lbl1.Text = Replace(lbl1.Text, "<TH Style=""font: bold 11px Verdana; color: #000066; text-align: left; background-color: #E9EDF2; border-right: 1px solid #ebebeb; border-bottom: 1px solid #ebebeb; padding: 3px; vertical-align: middle"" nowrap>Document", "<TH Style=""font: bold 11px Verdana; color: #000066; text-align: left; background-color: #E9EDF2; border-right: 1px solid #ebebeb; border-bottom: 1px solid #ebebeb; padding: 3px; vertical-align: middle"" nowrap>Link to view document")
        lbl1.Text = Replace(lbl1.Text, "<TH Style=""font: bold 11px Verdana; color: #000066; text-align: left; background-color: #E9EDF2; border-right: 1px solid #ebebeb; border-bottom: 1px solid #ebebeb; padding: 3px; vertical-align: middle"" nowrap>DT", "<TH Style=""font: bold 11px Verdana; color: #000066; text-align: left; background-color: #E9EDF2; border-right: 1px solid #ebebeb; border-bottom: 1px solid #ebebeb; padding: 3px; vertical-align: middle"" nowrap>Document")
        lbl1.Text = Replace(lbl1.Text, "<TD Style=""padding: 3px; font:normal 11px Verdana;color: #000066;text-align: left;border-right: 1px solid #ebebeb;border-bottom: 1px solid #ebebeb;vertical-align: top;"" nowrap>01/01/1900</TD>", "<TD Style=""padding: 3px; font:normal 11px Verdana;color: #000066;text-align: left;border-right: 1px solid #ebebeb;border-bottom: 1px solid #ebebeb;vertical-align: top;"" nowrap></TD>")

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If resourceId = CommonConstants.StudentDocsResourceId Then 'if resourceid=301(student docs)
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(btnApplyFilter)
            'Add javascript code to warn the user about non saved changes
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
            If ddltypeofreq.SelectedItem.Text = ReqforTermination And ddlDocFilterStatus.SelectedItem.Text <> NotApproved Or ddlDocumentId.SelectedItem.Text = R2T4 Or ddlDocumentId.SelectedItem.Text = TerminationDetails Then
                RadGrid1.MasterTableView.GetColumn("User").Display = False
                RadGrid1.MasterTableView.GetColumn("UpLoadedDate").Display = False
                RadGrid1.MasterTableView.GetColumn("ModifiedDate").Display = True
            Else
                RadGrid1.MasterTableView.GetColumn("User").Display = True
                RadGrid1.MasterTableView.GetColumn("UpLoadedDate").Display = True
                RadGrid1.MasterTableView.GetColumn("ModifiedDate").Display = False
            End If
        ElseIf resourceId = CommonConstants.DocumentManagementResourceId Then
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(btnApplyFilter)
            controlsToIgnore.Add(btnUpload)
            controlsToIgnore.Add(ddlStudentId)
            controlsToIgnore.Add(ddlModulesId)
            'Add javascript code to warn the user about non saved changes
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
            If ddltypeofreq.SelectedItem.Text = ReqforTermination And ddlDocFilterStatus.SelectedItem.Text <> NotApproved Or ddlDocumentId.SelectedItem.Text = R2T4 Or ddlDocumentId.SelectedItem.Text = TerminationDetails Then
                RadGrid1.MasterTableView.GetColumn("User").Display = False
                RadGrid1.MasterTableView.GetColumn("UpLoadedDate").Display = False
                RadGrid1.MasterTableView.GetColumn("ModifiedDate").Display = True
            Else
                RadGrid1.MasterTableView.GetColumn("User").Display = True
                RadGrid1.MasterTableView.GetColumn("UpLoadedDate").Display = True
                RadGrid1.MasterTableView.GetColumn("ModifiedDate").Display = False
            End If
        End If

    End Sub

    Protected Sub RadGrid1_DeleteCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs)
        Dim DocumentType As String = ddlDocumentId.SelectedItem.Text
        Dim isDelete = DocumentType <> R2T4 And DocumentType <> TerminationDetails
        Dim result As String
        Dim docfacade As New DocumentManagementFacade
        Dim deletedItem As GridDataItem = DirectCast(e.Item, GridDataItem)
        Dim field1_PKID As String = ""

        Dim ID As String = TryCast(e.Item, GridDataItem).OwnerTableView.DataKeyValues(e.Item.ItemIndex)("FileID").ToString()
        Dim DeleteItem As GridDataItem = TryCast(e.Item, GridDataItem)

        Dim strPath As String = MyAdvAppSettings.AppSettings("DocumentPath")
        Dim imageName As String = TryCast(DeleteItem("DisplayName").FindControl("lblName"), Label).Text

        ' Dim strFileNameWithExtension As String = DeleteItem.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("FileName").ToString()
        Dim extnposition As Integer
        extnposition = imageName.LastIndexOf(".")
        Dim extn As String
        extn = imageName.Substring(extnposition, imageName.Length - extnposition)
        Dim strFullPath As String = String.Format("{0}{1}\{2}{3}", strPath, DocumentType, ID, extn)
        If File.Exists(strFullPath) And isDelete = True Then
            File.Delete(strFullPath)
        End If
        'Delete The File,ReBind The DataList and Exit Procedure 
        result = docfacade.DeleteDocumentbyID(ID, isDelete, Session("UserName"))
        If Not result = "" Then
            DisplayErrorMessage(result)
        End If
        BuildFileDataGrid()
        chkScannedId.Checked = True

        Exit Sub
    End Sub

    Protected Sub RadGrid1_InsertCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs)
        Dim strDirectory As String
        Dim strDocumentInsert As New DocumentManagementFacade
        Dim smessage As String
        Dim result As String
        smessage = ValidatePage()
        If Not smessage = "" Then
            DisplayErrorMessage(smessage)
            e.Canceled = True
            RadGrid1.MasterTableView.IsItemInserted = False
            RadGrid1.MasterTableView.ClearSelectedItems()
            RadGrid1.Rebind()
            Exit Sub
        End If

        Dim insertItem As GridEditFormInsertItem = TryCast(e.Item, GridEditFormInsertItem)
        Dim radAsyncUpload As RadAsyncUpload = TryCast(insertItem("Upload").FindControl("AsyncUpload1"), RadAsyncUpload)
        If radAsyncUpload.UploadedFiles.Count > 0 Then
            For Each file As UploadedFile In radAsyncUpload.UploadedFiles
                'Get Directory
                Dim strDocumentType As String = ddlDocumentId.SelectedItem.Text
                strDirectory = CreateDirectory(strDocumentType)

                'Get FileName from the client machine
                Dim strExt As String = file.FileName.Substring(file.FileName.LastIndexOf(".")).ToLower()

                'Get Friendly Name for storing
                Dim slashposition As Integer = file.FileName.LastIndexOf("\")
                Dim strDisplayName As String = file.FileName.Substring(slashposition + 1)
                strDisplayName = strDisplayName.ToString.Split(".")(0)

                Dim intBackSlashPos As Integer = InStrRev(file.FileName, "\")
                If intBackSlashPos < 1 Then
                    intBackSlashPos = InStrRev(file.FileName, "/")
                End If
                Dim strOrigFileName As String = Mid(file.FileName, intBackSlashPos + 1)
                Dim strFindExtPos As Integer = InStr(strOrigFileName, ".")
                Dim strUserFileName As String = Mid(strOrigFileName, 1, strFindExtPos - 1)

                'Get Size of file
                Dim filesize As Integer = file.ContentLength

                'Get type of file
                Dim facade As New DocumentManagementFacade
                Dim strStudentId As String
                If resourceId = CommonConstants.DocumentManagementResourceId Then
                    strStudentId = Trim(ddlStudentId.SelectedValue)
                ElseIf resourceId = CommonConstants.StudentDocsResourceId Then
                    strStudentId = Trim(StudentId)
                End If
                Dim strStudentName As String = facade.GetStudentNamesById(strStudentId)
                Dim filetype As String = file.ContentType
                Dim strFileNameOnly As String = Server.UrlEncode(strStudentName & "_" & strStudentId & "_" & strUserFileName).ToLower()
                Dim strFileName As String = strStudentName & "_" & strStudentId & "_" & strUserFileName & strExt

                If filesize <= 0 Then
                    DisplayErrorMessage("Document upload process failed")
                Else
                    result = strDocumentInsert.InsertDocumentbyID(strFileNameOnly, strExt, Session("UserName"), ddlDocumentId.SelectedItem.Text, StudentId, ddlDocumentId.SelectedValue, ddlModulesId.SelectedValue, strDisplayName)

                    If result <> "" Then
                        file.SaveAs(strDirectory + "\" + result + strExt)
                        DisplayErrorMessage("Document uploaded successfully")
                        e.Canceled = True
                        RadGrid1.MasterTableView.IsItemInserted = False
                        RadGrid1.MasterTableView.ClearEditItems()
                        RadGrid1.Rebind()
                    Else
                        DisplayErrorMessage("Data not saved")
                    End If
                End If
            Next

        End If
        e.Canceled = True
        RadGrid1.MasterTableView.IsItemInserted = False
        RadGrid1.MasterTableView.ClearSelectedItems()
        RadGrid1.Rebind()

    End Sub

    Protected Sub RadGrid1_ItemCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        If e.CommandName = RadGrid.EditCommandName Then
            ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "SetEditMode", "isEditMode = true;", True)
        End If

        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked
            Dim strDocumentInsert As New DocumentManagementFacade
            Dim isStudentDocumentRecordCreated As Boolean

            If resourceId = CommonConstants.DocumentManagementResourceId Then
                StudentId = ddlStudentId.SelectedValue
            End If
            isStudentDocumentRecordCreated = strDocumentInsert.IsStudentDocumentAlreadyCreated(Trim(StudentId), Trim(ddlDocumentId.SelectedValue))
            If Not isStudentDocumentRecordCreated Then
                e.Canceled = True
                RadGrid1.MasterTableView.IsItemInserted = False
                RadGrid1.MasterTableView.ClearSelectedItems()
                RadGrid1.Rebind()
            Else
                e.Canceled = True
                Dim newValues As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary()
                newValues("DisplayName") = ""
                'Insert the item and rebind
                e.Item.OwnerTableView.InsertItem(newValues)
            End If

        End If
        If e.CommandName = "StudentSearch" Then
            Session("File2Browse") = e.CommandArgument

            If (TypeOf e.Item Is GridDataItem) Then
                Dim gridItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                'Dim img As New LinkButton
                Dim img As LinkButton = DirectCast(gridItem("Upload").FindControl("Linkbutton1"), LinkButton)
                img.Attributes.Add("target", "_blank")
                Dim popupScript As String = "<script type='text/javascript'>window.Onload=OpenDocuments1();function OpenDocuments1(){window.open('../FileBrowser.aspx','_blank','HistWin','width=700,height=600,resizable=yes'); }</script>"
                ScriptManager.RegisterStartupScript(RadGrid1, RadGrid1.GetType(), "PopUpScript", popupScript, True)
            End If
        End If

    End Sub

    Protected Sub RadGrid1_ItemCreated(sender As Object, e As Telerik.Web.UI.GridItemEventArgs)
        If TypeOf e.Item Is GridEditableItem AndAlso e.Item.IsInEditMode Then
            Dim upload As RadAsyncUpload = TryCast(DirectCast(e.Item, GridEditableItem)("Upload").FindControl("AsyncUpload1"), RadAsyncUpload)
            Dim cell As TableCell = DirectCast(upload.Parent, TableCell)
            Dim validator As New CustomValidator()
            validator.ErrorMessage = "Please select file to be uploaded"
            validator.ClientValidationFunction = "validateRadUpload"
            validator.Display = ValidatorDisplay.Dynamic
            cell.Controls.Add(validator)
        End If
    End Sub

    Protected Sub RadGrid1_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        If ddlDocumentId.SelectedValue <> "" Then
            Dim File As New DocumentManagementFacade
            Dim dsnew As New DataSet
            If resourceId = CommonConstants.DocumentManagementResourceId Then
                StudentId = ddlStudentId.SelectedValue
            End If
            dsnew = File.GetAllDocumentsByStudentIdandDocumentID(StudentId, ddlDocumentId.SelectedValue, IsSupportUser())
            RadGrid1.DataSource = dsnew
        End If
    End Sub

    Protected Sub RadGrid1_UpdateCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs)
        Dim strDirectory As String
        Dim strDocumentInsert As New DocumentManagementFacade
        Dim smessage As String
        Dim result As Integer
        smessage = ValidatePage()
        If Not smessage = "" Then
            DisplayErrorMessage(smessage)
            Exit Sub
        End If

        Dim editedItem As GridEditableItem = TryCast(e.Item, GridEditableItem)
        Dim ID As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("FileID").ToString()
        Dim radAsyncUpload As RadAsyncUpload = TryCast(editedItem("Upload").FindControl("AsyncUpload1"), RadAsyncUpload)
        '   If Not txtUpLoad.PostedFile Is Nothing And Not txtUpLoad.Value = "" Then
        If radAsyncUpload.UploadedFiles.Count > 0 Then
            'Get Directory
            Dim strDocumentType As String = ddlDocumentId.SelectedItem.Text
            strDirectory = CreateDirectory(strDocumentType)

            'Get FileName from the client machine
            Dim strExt As String = radAsyncUpload.UploadedFiles(0).FileName.Substring(radAsyncUpload.UploadedFiles(0).FileName.LastIndexOf(".")).ToLower()

            'Get Friendly Name for storing
            Dim slashposition As Integer = radAsyncUpload.UploadedFiles(0).FileName.LastIndexOf("\")
            Dim strDisplayName As String = radAsyncUpload.UploadedFiles(0).FileName.Substring(slashposition + 1)
            strDisplayName = strDisplayName.ToString.Split(".")(0)

            Dim intBackSlashPos As Integer = InStrRev(radAsyncUpload.UploadedFiles(0).FileName, "\")
            If intBackSlashPos < 1 Then
                intBackSlashPos = InStrRev(radAsyncUpload.UploadedFiles(0).FileName, "/")
            End If
            Dim strOrigFileName As String = Mid(radAsyncUpload.UploadedFiles(0).FileName, intBackSlashPos + 1)
            Dim strFindExtPos As Integer = InStr(strOrigFileName, ".")
            Dim strUserFileName As String = Mid(strOrigFileName, 1, strFindExtPos - 1)

            'Get Size of file
            Dim filesize As Integer = radAsyncUpload.UploadedFiles(0).ContentLength

            'Get type of file
            Dim facade As New DocumentManagementFacade
            Dim strStudentId As String = Trim(StudentId)
            Dim strStudentName As String = facade.GetStudentNamesById(strStudentId)
            Dim filetype As String = radAsyncUpload.UploadedFiles(0).ContentType
            Dim strFileNameOnly As String = Server.UrlEncode(strStudentName & "_" & strStudentId & "_" & strUserFileName).ToLower()
            Dim strFileName As String = strStudentName & "_" & strStudentId & "_" & strUserFileName & strExt

            If filesize <= 0 Then
                DisplayErrorMessage("Document upload process failed")
            Else
                result = strDocumentInsert.UpdateDocument(ID, strFileNameOnly, strExt, Session("UserName"), ddlDocumentId.SelectedItem.Text, StudentId, ddlDocumentId.SelectedValue, ddlModulesId.SelectedValue, strDisplayName)
                radAsyncUpload.UploadedFiles(0).SaveAs(strDirectory + "\" + ID + strExt)
                If result = 0 Then
                    DisplayErrorMessage("Document uploaded successfully")
                End If
            End If
        Else
            DisplayErrorMessage("Only one document can be uploaded")
        End If
        RadGrid1.MasterTableView.ClearEditItems()
        RadGrid1.Rebind()
    End Sub

    Protected Sub RadGrid1_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrid1.ItemDataBound
        Dim bEdit As Boolean
        Dim bAdd As Boolean
        Dim bDelete As Boolean

        bEdit = False
        bAdd = False
        bDelete = False
        If pObj.HasEdit Then
            bEdit = True
            bAdd = True
        End If
        If pObj.HasAdd Then
            bAdd = True
        End If
        If pObj.HasDelete Then
            bDelete = True
        End If
        If bAdd Then
            RadGrid1.AllowAutomaticInserts = True
        ElseIf Not bAdd Then
            If TypeOf e.Item Is GridCommandItem Then
                DisableGridAdd(e)
            End If
        End If
        If ddltypeofreq.SelectedItem.Text = ReqforTermination And ddlDocFilterStatus.SelectedItem.Text <> NotApproved Or ddlDocumentId.SelectedItem.Text = R2T4 Or ddlDocumentId.SelectedItem.Text = TerminationDetails Then
            bEdit = False
            If IsSupportUser() = True Then
                bDelete = True
            Else
                bDelete = False
            End If
            DisableGridAdd(e)
        Else
            If Not IsSupportUser() Then
                bEdit = False
            Else
                bEdit = True
                bDelete = True
            End If
        End If

        If IsSupportUser() = True Then
            bDelete = True
            bEdit = True
        End If

        If bEdit Then
            RadGrid1.AllowAutomaticUpdates = True
        ElseIf bEdit = False And bAdd = False Then
            If TypeOf e.Item Is GridDataItem Then
                DisableGridLink(e, "EditCommandColumn", "edit")
            End If
        ElseIf bEdit = False And bAdd = True Then
            If TypeOf e.Item Is GridDataItem Then ' And e.Item.OwnerTableView.IsItemInserted = False Then
                DisableGridLink(e, "EditCommandColumn", "edit")
            End If
        End If
        If Not bDelete Then
            If TypeOf e.Item Is GridDataItem Then
                DisableGridLink(e, "DeleteColumn", "delete")
            End If
        End If

        If (TypeOf e.Item Is GridDataItem) Then
            Dim gridItem As GridDataItem = DirectCast(e.Item, GridDataItem)
            'Dim img As New LinkButton
            Dim img As LinkButton = DirectCast(gridItem("Upload").FindControl("Linkbutton1"), LinkButton)
            If Not img Is Nothing Then
                Dim javascripttoopenfile As String
                Dim isDeleted As String = gridItem.DataItem.Row("IsArchived").ToString()
                If Not isDeleted = "True" OrElse IsSupportUser() Then
                    javascripttoopenfile = "window.open('../FileBrowser.aspx?fileurl=" + Server.UrlEncode(img.CommandArgument.ToString()) + "','HistWin','width=700,height=600,resizable=yes');"
                    img.Attributes.Add("onclick", javascripttoopenfile)
                Else
                    img.Attributes.Add("disabled", "disabled")
                    img.Attributes.Add("href", "")
                End If
            End If
        End If

        Dim item As GridDataItem = TryCast(e.Item, GridDataItem)
        If (item IsNot Nothing) Then
            Dim isDeleted As String = item.DataItem.Row("IsArchived").ToString()
            If isDeleted = "True" Then
                Dim imgBtn = item("DeleteColumn")
                imgBtn.Enabled = False
                Dim fileName = item("DisplayName")
                fileName.CssClass = "textStrikeThrough"
            End If
        End If
    End Sub

    Private Sub btnViewDocsByStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewDocsByStatus.Click
        Dim winSettings As String = "toolbar=no, status=no, resizable=yes,width=600px,height=370px"
        Dim name As String = "pendingdocs"
        Dim url As String = "../AD/ViewPendingDocsByStudent.aspx?studentid=" + StudentId + "&studentname=" + ddlStudentId.SelectedItem.Text + "&campusid=" + campusid
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub

    Private Sub DisableGridLink(ByVal e As Telerik.Web.UI.GridItemEventArgs,
                            ByVal sCmdName As String,
                            ByVal sDesc As String)

        Dim sToolTip As String = "User does not have the permission to " & sDesc & " record"
        Dim cell As TableCell = CType(e.Item, GridDataItem)(sCmdName)
        Dim lnk As ImageButton = CType(cell.Controls(0), ImageButton)
        lnk.Enabled = False
        lnk.Style(HtmlTextWriterStyle.Cursor) = If(lnk.Enabled, CommonConstants.Pointer, CommonConstants.DefaultCursor)
        If resourceId = CommonConstants.DocumentManagementResourceId Then
            If Not IsSupportUser() Then
                If ddltypeofreq.SelectedItem.Text = ReqforTermination And ddlDocFilterStatus.SelectedItem.Text <> NotApproved Or ddlDocumentId.SelectedItem.Text = R2T4 Or ddlDocumentId.SelectedItem.Text = TerminationDetails Then
                    lnk.Visible = True
                    btnDelete.Enabled = False
                Else
                    lnk.Visible = False
                End If
            End If
        End If
        cell.ToolTip = sToolTip
    End Sub

    Private Sub DisableGridAdd(ByVal e As Telerik.Web.UI.GridItemEventArgs)
        If TypeOf e.Item Is GridCommandItem Then
            Try
                Dim cmditm As GridCommandItem = DirectCast(e.Item, GridCommandItem)
                Dim btn1 As Button = DirectCast(cmditm.FindControl("btn1"), Button)
                btn1.Enabled = False
                btn1.Style(HtmlTextWriterStyle.Cursor) = If(btn1.Enabled, CommonConstants.Pointer, CommonConstants.DefaultCursor)
                btn1.ToolTip = "User does not have permission to add new record"
                Dim lnkbtn1 As LinkButton = DirectCast(cmditm.FindControl("linkbuttionInitInsert"), LinkButton)
                lnkbtn1.ToolTip = "User does not have permission to add new record"
                lnkbtn1.Enabled = False
                lnkbtn1.Style(HtmlTextWriterStyle.Cursor) = If(lnkbtn1.Enabled, CommonConstants.Pointer, CommonConstants.DefaultCursor)

                If resourceId = CommonConstants.DocumentManagementResourceId Then
                    If Not IsSupportUser() Then
                        Dim bEdit As Boolean
                        Dim bAdd As Boolean
                        Dim bDelete As Boolean
                        bEdit = False
                        btnSave.Enabled = False
                        btnNew.Enabled = False
                        btn1.Visible = False
                        lnkbtn1.Visible = False
                        If ddltypeofreq.SelectedItem.Text = ReqforTermination And ddlDocFilterStatus.SelectedItem.Text <> NotApproved Or ddlDocumentId.SelectedItem.Text = R2T4 Or ddlDocumentId.SelectedItem.Text = TerminationDetails Then
                            btnDelete.Enabled = False
                        End If
                    Else
                        If ddltypeofreq.SelectedItem.Text = ReqforTermination And ddlDocFilterStatus.SelectedItem.Text <> NotApproved Or ddlDocumentId.SelectedItem.Text = R2T4 Or ddlDocumentId.SelectedItem.Text = TerminationDetails Then
                            btnNew.Enabled = True
                            btnSave.Enabled = False
                            btnDelete.Enabled = False
                        End If
                    End If
                End If
                btnSave.Style(HtmlTextWriterStyle.Cursor) = If(btnSave.Enabled, CommonConstants.Pointer, CommonConstants.DefaultCursor)
                btnNew.Style(HtmlTextWriterStyle.Cursor) = If(btnNew.Enabled, CommonConstants.Pointer, CommonConstants.DefaultCursor)
                btnDelete.Style(HtmlTextWriterStyle.Cursor) = If(btnDelete.Enabled, CommonConstants.Pointer, CommonConstants.DefaultCursor)
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

            End Try
        End If
    End Sub

    Private Sub InitializeTerminationDetails(ByVal status As Boolean)
        If resourceId = CommonConstants.DocumentManagementResourceId Then
            If Not IsSupportUser() Then
                btnSave.Enabled = False
                btnNew.Enabled = False
                If ddltypeofreq.SelectedItem.Text = ReqforTermination And ddlDocFilterStatus.SelectedItem.Text <> NotApproved Or ddlDocumentId.SelectedItem.Text = R2T4 Or ddlDocumentId.SelectedItem.Text = TerminationDetails Then
                    btnDelete.Enabled = False
                End If
            Else
                If ddltypeofreq.SelectedItem.Text = ReqforTermination And ddlDocFilterStatus.SelectedItem.Text <> NotApproved Or ddlDocumentId.SelectedItem.Text = R2T4 Or ddlDocumentId.SelectedItem.Text = TerminationDetails Then
                    btnNew.Enabled = True
                    btnSave.Enabled = False
                    btnDelete.Enabled = False
                End If
            End If
        End If
        btnSave.Style(HtmlTextWriterStyle.Cursor) = If(btnSave.Enabled, CommonConstants.Pointer, CommonConstants.DefaultCursor)
        btnNew.Style(HtmlTextWriterStyle.Cursor) = If(btnNew.Enabled, CommonConstants.Pointer, CommonConstants.DefaultCursor)
        btnDelete.Style(HtmlTextWriterStyle.Cursor) = If(btnDelete.Enabled, CommonConstants.Pointer, CommonConstants.DefaultCursor)

        If resourceId = CommonConstants.StudentDocsResourceId Then
            btnSave.Enabled = status
            btnDelete.Enabled = status
        End If
        ddlStudentId.Enabled = status
        ddlModulesId.Enabled = status
        ddlDocumentId.Enabled = status
        ddlDocStatusId.Enabled = status
        chkOverride.Enabled = status
        RdRequestDate.Enabled = status
        RdReceivedDate.Enabled = status
        btnViewDocsByStatus.Enabled = status
    End Sub

    Private Function IsSupportUser() As Boolean
        Presenter = New TenantPickerPresenter(User.Identity.Name)
        Return Presenter.IsSupportUser()
    End Function

    '' code for document management page start
    Private Sub BindDocumentManagementDataList()
        With New LeadFacade
            dlstDocumentMgmt.DataSource = .GetAllDocsStudentByCampus(Trim(campusid))
            dlstDocumentMgmt.DataBind()
        End With
    End Sub

    Private Sub dlstDocumentMgmt_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstDocumentMgmt.ItemCommand
        ChkIsInDB.Checked = True
        txtStudentDocId.Text = e.CommandArgument
        Dim selectedItem = DirectCast(e.CommandSource, System.Web.UI.WebControls.LinkButton).[Text]
        GetStudentInfo(e.CommandArgument, selectedItem)
        'set Style to Selected Item
        CommonWebUtilities.RestoreItemValues(dlstDocumentMgmt, e.CommandArgument)
        Dim strStudentId As String = Trim(ddlStudentId.SelectedValue)
        Dim facade As New DocumentManagementFacade
        Dim strStudentName As String = facade.GetStudentNamesById(strStudentId)
        Dim strFileNameOnly As String = strStudentName & "_" & strStudentId

        txtExtension.Text = facade.GetDocumentExtension(strFileNameOnly, ddlDocumentId.SelectedItem.Text)
        txtDocumentName.Text = ddlDocumentId.SelectedItem.Text
        txtStudentName.Text = strStudentName
        txtStudentId.Text = strStudentId
        ddlDocumentId.Enabled = True
        pnlViewDoc.Visible = False
        dgrdDocs.Visible = False
        pnlUpload.Visible = False

        'Refresh The DataGrid With Document Types
        BuildFileDataGrid(strFileNameOnly)
        InitButtonsForEdit()
        RadGrid1.Visible = True

        If ddltypeofreq.SelectedItem.Text = ReqforTermination And ddlDocFilterStatus.SelectedItem.Text <> NotApproved Or ddlDocumentId.SelectedItem.Text = R2T4 Or ddlDocumentId.SelectedItem.Text = TerminationDetails Then
            InitializeTerminationDetails(False)
        Else
            InitializeTerminationDetails(True)
        End If
    End Sub

    Private Sub GetDocumentsByStatusForDocMgmt(ByVal DocumentStatusId As String, ByVal StudentId As String, ByVal TypeofRequirement As Integer, ByVal DocumentTypeId As String)
        Dim intModuleId As Integer
        If ddlDocumentModule.SelectedValue = "" Then
            intModuleId = 0
        Else
            intModuleId = CInt(ddlDocumentModule.SelectedValue)
        End If
        With New LeadFacade
            dlstDocumentMgmt.DataSource = .GetDocsStudentByModuleIdAndStatus(intModuleId, StudentId, campusid, DocumentStatusId, TypeofRequirement, DocumentTypeId)
            dlstDocumentMgmt.DataBind()
        End With
    End Sub

    Private Sub applyFilterForDocumentMgmt()
        BindEmploymentInfo()
        Dim facade As New LeadFacade
        GetDocumentsByStatusForDocMgmt(ddlDocFilterStatus.SelectedValue, ddlStudent_Name.SelectedValue, ddltypeofreq.SelectedValue, ddlDocTypeSearchControl.SelectedValue)
        pnlViewDoc.Visible = False
        Dim dt As New DataTable
        dt.Columns.Add("DisplayName")
        RadGrid1.DataSource = dt
        RadGrid1.DataBind()
        RadGrid1.Visible = False
        dgrdDocs.Visible = False
        InitButtonsForLoad()
    End Sub

    Private Sub applyFilterForStudentDocs()
        BindEmploymentInfo()
        dgrdDocs.Visible = False
        Dim dt As New DataTable
        dt.Columns.Add("DisplayName")
        RadGrid1.DataSource = dt
        RadGrid1.DataBind()
        RadGrid1.Visible = False
        Dim intModule As Integer
        If ddlDocumentModule.SelectedIndex = 0 Then
            intModule = 0
        Else
            intModule = CInt(ddlDocumentModule.SelectedValue)
        End If
        GetDocumentsByStatus(ddlDocFilterStatus.SelectedValue, StudentId, intModule, ddltypeofreq.SelectedValue, ddlDocTypeSearchControl.SelectedValue)
        InitButtonsForLoad()
    End Sub

    Private Sub BuildDocumentListDDL_DocMgmt(ByVal ModuleID As Integer, ByVal StudentDocId As String, Optional ByVal documentTypeSelected As String = "")
        ddlDocumentId.Items.Clear()
        'Dim DocumentList As New LeadDocsFacade
        Dim docsFacade As New LeadEntranceFacade
        Dim documentTypeSelecteds = documentTypeSelected.Split("-")
        documentTypeSelected = documentTypeSelecteds(documentTypeSelecteds.Length - 1).Trim()

        Dim intPrgVerId As Integer = docsFacade.getPrgVersionByStudent(ddlStudentId.SelectedValue)
        With ddlDocumentId
            .DataTextField = "DocumentDescrip"
            .DataValueField = "DocumentId"
            If intPrgVerId >= 1 Then
                If documentTypeSelected = R2T4 Or documentTypeSelected = TerminationDetails Then
                    .DataSource = docsFacade.GetAllTerminationDocuments(ModuleID)
                Else
                    .DataSource = docsFacade.GetAllExistingStandardDocumentsByStudentEnrollment(ddlStudentId.SelectedValue, ModuleID, StudentDocId, campusid)
                    'docsFacade.GetAllStandardDocumentsByStudentEnrollment(ddlStudentId.SelectedValue, ModuleID, campus, StudentDocId)
                End If
            End If
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
End Class