﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="NewEmployerJobs.aspx.vb" Inherits="NewEmployerJobs" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstEmployerContact">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                </UpdatedControls>
            </telerik:AjaxSetting>
              <telerik:AjaxSetting AjaxControlID="radstatus">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstEmployerContact" />
                    <telerik:AjaxUpdatedControl ControlID="radstatus" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop2">
                        <br />
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td nowrap align="left" width="15%">
                                                <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                            </td>
                                            <td nowrap width="85%">
                                                <asp:RadioButtonList ID="radStatus" runat="Server" RepeatDirection="Horizontal" AutoPostBack="true"
                                                    CssClass="label">
                                                    <asp:ListItem Text="Active" Selected="True" />
                                                    <asp:ListItem Text="Inactive" />
                                                    <asp:ListItem Text="All" />
                                                </asp:RadioButtonList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="listframebottom">
                                    <div class="scrollleftfilters">
                                        <asp:DataList ID="dlstEmployerContact" runat="server">
                                            <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                            <ItemStyle CssClass="itemstyle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
                                                    CommandArgument='<%# Container.DataItem("EmployerJobId")%>' Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'
                                                    CausesValidation="False"></asp:ImageButton>
                                                <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" CommandArgument='<%# Container.DataItem("EmployerJobId")%>'
                                                    Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'
                                                    CausesValidation="False"></asp:ImageButton>
                                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                                <asp:LinkButton Text='<%# Container.DataItem("EmployerJobTitle") %>' runat="server"
                                                    CssClass="itemstyle" CommandArgument='<%# Container.DataItem("EmployerJobId")%>'
                                                    ID="Linkbutton2" CausesValidation="False" />
                                            </ItemTemplate>
                                        </asp:DataList></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
            Orientation="HorizontalTop">
            <asp:Panel ID="pnlRHS" runat="server">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False">
                        </asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete"
                            CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="scrollright2">
                            <!-- begin content table-->

                                <table class="contenttable" cellspacing="0" cellpadding="0" width="98%">
                                    <asp:TextBox ID="txtEmployerJobId" runat="server" Visible="false"></asp:TextBox>
                                    <asp:CheckBox ID="chkIsInDB" runat="server" CssClass="label" Visible="false" Checked="False">
                                    </asp:CheckBox>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblCode" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtCode" TabIndex="1" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblFeeId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlFeeId" TabIndex="18" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlStatusId" TabIndex="2" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblNumberOpen" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtNumberOpen" TabIndex="19" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblJobGroupId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlJobGroupId" TabIndex="3" runat="server" CssClass="dropdownlist"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblNumberFilled" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtNumberFilled" TabIndex="20" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblEmployerJobTitle" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtEmployerJobTitle" TabIndex="4" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblExpertiseId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlExpertiseId" runat="server" CssClass="dropdownlist" TabIndex="21">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblJobTitleId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlJobTitleId" TabIndex="5" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td>
                                            <asp:Label ID="lblJobPostedDate" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <%--<asp:TextBox ID="txtJobPostedDate" TabIndex="22" runat="server" CssClass="textboxdateforjobs"
                                        Enabled="true"></asp:TextBox>&nbsp; <a onclick="javascript:OpenCalendar('ClsSect','txtJobPostedDate', true, 1945)">
                                            <img id="Img3" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
                                                border="0" runat="server"></a>--%>
                                            <telerik:RadDatePicker ID="txtJobPostedDate" MinDate="1/1/1945" runat="server">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblJobDescription" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell2" colspan="4">
                                            <asp:TextBox ID="txtJobDescription" TabIndex="6" runat="server" MaxLength="300" TextMode="MultiLine"
                                                Rows="3" Columns="60" CssClass="tocommentsnowrap"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblJobRequirements" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell2" colspan="4">
                                            <asp:TextBox ID="txtJobRequirements" TabIndex="7" runat="server" MaxLength="300"
                                                TextMode="MultiLine" Rows="3" Columns="60" CssClass="tocommentsnowrap"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblSalaryFrom" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtSalaryFrom" TabIndex="8" runat="server" CssClass="textbox" Width="100px"></asp:TextBox><asp:Label
                                                ID="lblCurrency" runat="server" CssClass="label" Width="70px">US Dollars</asp:Label>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            &nbsp;
                                        </td>
                                        <td class="contentcell4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblSalaryTo" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtSalaryTo" TabIndex="9" runat="server" CssClass="textbox" Width="100px"></asp:TextBox><asp:Label
                                                ID="lblCurrencyTo" runat="server" CssClass="label" Width="70px">US Dollars</asp:Label>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblContactId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlContactId" TabIndex="23" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblSalaryTypeID" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlSalaryTypeID" TabIndex="10" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblOpenedFrom" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <%--<asp:TextBox ID="txtOpenedFrom" TabIndex="24" runat="server" CssClass="textboxdateforjobs"></asp:TextBox>&nbsp;
                                    <a onclick="javascript:OpenCalendar('ClsSect','txtOpenedFrom', true, 1945)">
                                        <img id="Img1" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
                                            border="0" runat="server"></a>--%>
                                            <telerik:RadDatePicker ID="txtOpenedFrom" MinDate="1/1/1945" runat="server">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblTypeId" runat="server" CssClass="label">Job Type</asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlTypeId" TabIndex="11" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblOpenedTo" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <%-- <asp:TextBox ID="txtOpenedTo" TabIndex="25" runat="server" CssClass="textboxdateforjobs"></asp:TextBox>&nbsp;
                                    <a onclick="javascript:OpenCalendar('ClsSect','txtOpenedTo', true, 1945)">
                                        <img id="Img4" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
                                            border="0" runat="server"></a>--%>
                                            <telerik:RadDatePicker ID="txtOpenedTo" MinDate="1/1/1945" runat="server">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblAreaId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlAreaId" TabIndex="12" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                            <asp:Label ID="lblStart" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <%--<asp:TextBox ID="txtStart" TabIndex="26" runat="server" CssClass="textboxdateforjobs"></asp:TextBox>&nbsp;
                                    <a onclick="javascript:OpenCalendar('ClsSect','txtStart', true, 1945)">
                                        <img id="Img2" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
                                            border="0" runat="server"></a>--%>
                                            <telerik:RadDatePicker ID="txtStart" MinDate="1/1/1945" runat="server">
                                            </telerik:RadDatePicker>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlCampGrpId" TabIndex="13" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                            <asp:Label ID="lblBenefitsId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlBenefitsId" TabIndex="27" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblScheduleId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlScheduleId" TabIndex="14" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblHoursFrom" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtHoursFrom" TabIndex="15" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                        </td>
                                        <td class="contentcell4">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblHoursTo" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:TextBox ID="txtHoursTo" TabIndex="16" runat="server" CssClass="textbox"></asp:TextBox>
                                        </td>
                                        <td class="emptycell">
                                        </td>
                                        <td class="contentcell">
                                        </td>
                                        <td class="contentcell4">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblWorkDays" runat="server" CssClass="label">Work Days</asp:Label>
                                        </td>
                                        <td class="contentcell2" colspan="4">
                                            <asp:CheckBoxList ID="chkJobWorkDays" TabIndex="17" runat="Server" CssClass="dropdownlist"
                                                RepeatColumns="7" RepeatDirection="vertical">
                                            </asp:CheckBoxList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell" nowrap>
                                            <asp:Label ID="lblNotes" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell2" colspan="4">
                                            <asp:TextBox ID="txtNotes" TabIndex="28" runat="server" MaxLength="300" TextMode="MultiLine"
                                                Rows="3" Columns="60" CssClass="tocommentsnowrap"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                           
                            <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcellheader" nowrap colspan="6">
                                            <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell2" colspan="6">
                                            <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>
             
            <asp:TextBox ID="txtModDate" Visible="false" runat="server"></asp:TextBox><asp:TextBox
                ID="txtModUser" Visible="false" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtRowIds" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtResourceId" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtEmployerId" Style="display: none" runat="server"></asp:TextBox>
            </asp:Panel>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>
