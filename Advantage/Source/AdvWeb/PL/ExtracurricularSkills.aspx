<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="ExtracurricularSkills.aspx.vb" Inherits="AdvWeb.PL.ExtracurricularSkills" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../css/AD/LeadExtra.css" rel="stylesheet" />
    <script src="../Scripts/Advantage.Client.AD.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
  

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    
    <div class="boxContainer">
        <h3><%=Header.Title  %>
            <a id="extraPlusImage" href="javascript:void(0);">
                <span class="k-icon k-i-plus-circle font-green"></span>
            </a>

        </h3>
        <div id="extraWrapper">
            <div id="extraMainPanel" style="margin: 10px">
                <div id="extraGrid"></div>
            </div>
        </div>
    </div>
    <!-- Use this to put content -->


    <script type="text/javascript">
        var XSTUDENT_GET_SHADOW_LEAD = "<%=ObjStudentState.ShadowLead%>";
        $(document).ready(function () {
            <%-- ReSharper disable once UnusedLocals --%>
            var manager = new AD.LeadExtraCurricular(XSTUDENT_GET_SHADOW_LEAD);
        });
    </script>
</asp:Content>




