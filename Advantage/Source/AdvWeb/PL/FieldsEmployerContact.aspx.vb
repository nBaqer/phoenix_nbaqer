﻿Imports System.Diagnostics
Imports FAME.Advantage.Common
Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Advantage.Business.Objects
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports Advantage.Business.Logic.Layer

Partial Class FieldsEmployerContact
    Inherits BasePage
    Protected WithEvents lblEmails As Label

    Protected EmployerContactId As String


    Protected WithEvents Label2 As Label
    Protected WithEvents btnhistory As Button
    Protected WithEvents lblExt As Label
    Protected WithEvents txtExtension As TextBox
    Protected WithEvents lblBestTime As Label
    Protected WithEvents txtBestTime As TextBox
    Protected WithEvents Textbox1 As TextBox
    Protected WithEvents Textbox2 As TextBox
    Protected WithEvents Textbox3 As TextBox
    Protected WithEvents Textbox4 As TextBox
    Protected WithEvents ddlAddressType As DropDownList
    Protected EmployerId As String
    Protected WithEvents txtComments As TextBox
    Protected WithEvents PhoneFormat As Label
    Protected WithEvents lblHomeFormat As Label
    Protected WithEvents lblCellFormat As Label
    Protected WithEvents ZipFormat As Label
    Protected WithEvents lbl2 As Label
    Protected WithEvents lblCountryId As Label
    Protected WithEvents ddlCountryId As DropDownList
    Protected ResourceId As Integer
    Protected boolSwitchCampus As Boolean = False
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
#End Region

    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Protected userId As String
    Protected boolStatus As String
    Protected ModuleID As String
    Protected sdfcontrols As New SDFComponent
    Protected strDefaultCountry As String

    Private mruProvider As MRURoutines
    Protected state As AdvantageSessionState
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#Region "MRURoutines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function getEmployerFromStateObject(ByVal paramResourceId As Integer) As EmployerMRU


        Dim objStateInfo As New AdvantageStateInfo
        Dim objEmployerState As New EmployerMRU

        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        MyBase.GlobalSearchHandler(2)

        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
        HttpContext.Current.Items("Language") = "En-US"

        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployerId) Then
            EmployerId = Guid.Empty.ToString()
        Else
            EmployerId = AdvantageSession.MasterEmployerId
        End If

        With objEmployerState
            .EmployerId = New Guid(EmployerId)
            .Name = objStateInfo.NameValue
        End With


        Dim objGetStudentStatusBar As New AdvantageStateInfo

        mruProvider = New MRURoutines(Me.Context, GetAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)
        objGetStudentStatusBar = mruProvider.BuildEmployerStatusBar(EmployerId)
        With objGetStudentStatusBar

            AdvantageSession.MasterEmpName = objGetStudentStatusBar.NameValue
            AdvantageSession.MasterEmpAddress1 = objGetStudentStatusBar.Address1
            AdvantageSession.MasterEmpAddress2 = objGetStudentStatusBar.Address2
            AdvantageSession.MasterEmpCity = objGetStudentStatusBar.City
            AdvantageSession.MasterEmpState = objGetStudentStatusBar.State
            AdvantageSession.MasterEmpZip = objGetStudentStatusBar.Zip
            AdvantageSession.MasterEmpPhone = objGetStudentStatusBar.Phone
            objEmployerState.Name = objGetStudentStatusBar.NameValue
        End With



        If Not String.IsNullOrEmpty(AdvantageSession.MasterEmployerId) Then
            Master.ShowHideStatusBarControl(True) 'Show Lead Bar only when Lead is not blank
            Master.PageObjectId = EmployerId
            Master.PageResourceId = Request.QueryString("resid").ToString()
            Master.SetHiddenControlForAudit()
        Else
            Master.ShowHideStatusBarControl(False) 'Hide Lead Bar only when Lead is blank
        End If

        Return objEmployerState

    End Function


    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Return MyAdvAppSettings
    End Function
#End Region
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim SearchEmployerID As New EmployerSearchFacade
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString


        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

        Dim objCommon As New CommonUtilities


        If radStatus.SelectedItem.Text = "Active" Then
            boolStatus = "True"
        ElseIf radStatus.SelectedItem.Text = "Inactive" Then
            boolStatus = "False"
        Else
            boolStatus = "All"
        End If

        '  startPos = InStr(SearchEmployerID.GetEmployerID(Session("EmployerCode")), ",") - 1
        ' EmployerContactId = Mid(SearchEmployerID.GetEmployerID(Session("EmployerCode")), 1, startPos)
        'Dim SDFControls As New SDFComponent
        '   SDFControls.GenerateControlsNew(pnlSDF, ResourceID)

        chkForeignWorkPhone.Attributes.Add("CheckedChanged", "document.Form1.txtWorkPhone.focus();")

        'Get the EmployerId from the state object associated with this page
        '''''''''''''''''' Call to get EmployerId starts here ''''''''''''''''''''
        Dim objEmployerState As New EmployerMRU
        objEmployerState = getEmployerFromStateObject(82) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objEmployerState Is Nothing Then
            MyBase.RedirectToEmployerSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objEmployerState
            EmployerId = .EmployerId.ToString

        End With

        '''''''''''''''''' Call to get EmployerId  ends here ''''''''''''''''''''

        'resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'campusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        ModuleID = HttpContext.Current.Request.Params("Mod").ToString

        'pObj = fac.GetUserResourcePermissions(userId, ResourceId, campusId)
        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            InitButtonsForLoad()
            'objCommon.PageSetup(Form1, "NEW")
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            txtFirstName.BackColor = Color.White
            txtLastName.BackColor = Color.White
            ddlCountry.BackColor = Color.White

            ' Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"

            ' Assign The Variable Value to the textbox
            m_Context = HttpContext.Current
            txtResourceId.Text = CInt(m_Context.Items("ResourceId"))

            'txtRowIds.Text = txtEmployerContactId.Text
            chkIsInDB.Checked = False
            BindEmployerInfoDataBind(New plEmployerContact)

            'header1.EnableHistoryButton(False)

            BindDataList(boolStatus, EmployerId)

            Try
                ddlCountry.SelectedValue = strDefaultCountry
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlCountry.SelectedIndex = 0
            End Try

            setInternationalOrDomestic(txtWorkPhone, chkForeignWorkPhone.Checked)
            setInternationalOrDomestic(txtHomePhone, chkForeignHomePhone.Checked)
            setInternationalOrDomestic(txtCellPhone, chkForeignCellPhone.Checked)
            MyBase.uSearchEntityControlId.Value = ""
        Else
            'objCommon.PageSetup(Form1, "EDIT")
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            txtFirstName.BackColor = Color.White
            txtLastName.BackColor = Color.White
            ddlCountry.BackColor = Color.White

        End If
        If chkForeignZip.Checked = True Then
            lblOtherState.Visible = True
            txtOtherState.Visible = True
            ddlState.Enabled = False
        Else
            lblOtherState.Visible = False
            txtOtherState.Visible = False
            ddlState.Enabled = True
        End If
        'GetInputMaskValue()


        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfcontrols.GetSDFExists(ResourceId, ModuleID)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtEmployerContactId.Text) <> "" Then
            sdfcontrols.GenerateControlsEdit(pnlSDF, ResourceId, txtEmployerContactId.Text, ModuleID)
        Else
            sdfcontrols.GenerateControlsNew(pnlSDF, ResourceId, ModuleID)
        End If

        If (Master.IsSwitchedCampus) Then
            CampusObjects.ShowNotificationWhileSwitchingCampus(2, objEmployerState.Name)
        End If
    End Sub
    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)

        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub

    'Private Function GetInputMaskValue() As String
    '    Dim facInputMasks As New InputMasksFacade
    '    '    Dim correctFormat As Boolean
    '    Dim strMask As String
    '    Dim zipMask As String
    '    '   Dim errorMessage As String
    '    Dim strHomePhoneReq As String
    '    Dim strCellPhoneReq As String
    '    Dim strWorkPhoneReq As String
    '    Dim strZipReq As String
    '    '  Dim strFaxReq As String
    '    Dim objCommon As New CommonUtilities

    '    'Get The Input Mask for Phone/Fax and Zip
    '    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
    '    zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

    '    'Replace The Mask Character from # to 9 as Masked Edit TextBox 
    '    'accepts only certain characters as mask characters
    '    If chkForeignHomePhone.Checked = False Then
    '        txtHomePhone.Mask = Replace(strMask, "#", "9")
    '        lblWorkPhone.ToolTip = strMask
    '    Else
    '        txtHomePhone.Mask = ""
    '        lblWorkPhone.ToolTip = ""
    '    End If
    '    If chkForeignCellPhone.Checked = False Then
    '        txtCellPhone.Mask = Replace(strMask, "#", "9")
    '        lblCellPhone.ToolTip = strMask
    '    Else
    '        txtCellPhone.Mask = ""
    '        lblCellPhone.ToolTip = ""
    '    End If
    '    If chkForeignWorkPhone.Checked = False Then
    '        txtWorkPhone.Mask = Replace(strMask, "#", "9")
    '        lblWorkPhone.ToolTip = strMask
    '    Else
    '        txtWorkPhone.Mask = ""
    '        lblWorkPhone.ToolTip = ""
    '    End If
    '    If chkForeignZip.Checked = False Then
    '        txtZip.Mask = Replace(zipMask, "#", "9")
    '        lblZip.ToolTip = zipMask
    '    Else
    '        txtZip.Mask = ""
    '        lblZip.ToolTip = ""
    '    End If

    '    'Get The RequiredField Value
    '    strHomePhoneReq = objCommon.SetRequiredColorMask("HomePhone")
    '    strCellPhoneReq = objCommon.SetRequiredColorMask("CellPhone")
    '    strWorkPhoneReq = objCommon.SetRequiredColorMask("WorkPhone")

    '    strZipReq = objCommon.SetRequiredColorMask("Zip")

    '    'If The Field Is Required Field Then Color The Masked
    '    'Edit Control
    '    'If strWorkPhoneReq = "Yes" Then
    '    '    txtWorkPhone.BackColor = Color.FromName("#ffff99")
    '    'End If
    '    'If strHomePhoneReq = "Yes" Then
    '    '    txtHomePhone.BackColor = Color.FromName("#ffff99")
    '    'End If
    '    'If strCellPhoneReq = "Yes" Then
    '    '    txtCellPhone.BackColor = Color.FromName("#ffff99")
    '    'End If
    '    'If strZipReq = "Yes" Then
    '    '    txtZip.BackColor = Color.FromName("#ffff99")
    '    'End If
    'End Function

    Private Sub BuildDropDownLists()
        'BuildStatusDDL()
        'BuildPrefixDDL()
        'BuildSuffixDDL()
        'BuildTitlesDDL()
        'BuildCountryDDL()
        'BuildAddressTypeDDL()
        'BuildStatesDDL()

        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing))

        'Prefixes DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlPrefixId, AdvantageDropDownListName.Prefixes, Nothing, True, True))

        'Suffixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlSuffixId, AdvantageDropDownListName.Suffixes, Nothing, True, True))

        'Countries DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlCountry, AdvantageDropDownListName.Countries, Nothing, True, True))

        'Address Types DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressTypeId, AdvantageDropDownListName.Address_Types, Nothing, True, True))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlState, AdvantageDropDownListName.States, Nothing, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub
    Private Sub BuildStatesDDL()
        'Bind the Status DropDownList
        Dim states As New StatesFacade
        With ddlState
            .DataTextField = "StateDescrip"
            .DataValueField = "StateID"
            .DataSource = states.GetAllStates()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .Items.Insert(1, New ListItem("Other States Not Listed", "5451720D-583D-4364-802A-BF500EDA1234"))
        End With
    End Sub
    Private Sub BuildStatusDDL()
        'Bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With
    End Sub
    Private Sub BuildPrefixDDL()
        Dim Prefix As New PrefixesFacade
        With ddlPrefixId
            .DataTextField = "PrefixDescrip"
            .DataValueField = "PrefixId"
            .DataSource = Prefix.GetAllPrefixes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCountryDDL()
        Dim Country As New PrefixesFacade
        With ddlCountry
            .DataTextField = "CountryDescrip"
            .DataValueField = "CountryId"
            .DataSource = Country.GetAllCountries()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            'selected index must be the default country if it is defined in configuration file
            '            .SelectedIndex = CommonWebUtilities.GetDefaultCountryIndexInDDL(ddlCountry)
        End With
    End Sub
    Private Sub BuildAddressTypeDDL()
        Dim AddressType As New PrefixesFacade
        With ddlAddressTypeId
            .DataTextField = "AddressDescrip"
            .DataValueField = "AddressTypeId"
            .DataSource = AddressType.GetAllAddressType()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSuffixDDL()
        Dim Suffix As New SuffixesFacade
        With ddlSuffixId
            .DataTextField = "SuffixDescrip"
            .DataValueField = "SuffixId"
            .DataSource = Suffix.GetAllSuffixes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildTitlesDDL()
        'Dim Title As New PlEmployerInfoFacade
        'With ddlTitleId
        '    .DataTextField = "TitleDescrip"
        '    .DataValueField = "TitleId"
        '    .DataSource = Title.GetAllTitles()
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        '    .SelectedIndex = 0
        'End With
    End Sub
    Private Function ValidateFieldsWithInputMasks() As String
        Dim facInputMasks As New InputMasksFacade
        Dim correctFormat, correctFormat1, correctFormat2, correctFormat3 As Boolean
        Dim strMask As String
        Dim zipMask As String
        Dim errorMessage As String

        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        'Validate the phone field format. If the field is empty we should not apply the mask
        'agaist it.
        errorMessage = ""

        If txtWorkPhone.Text <> "" And chkForeignWorkPhone.Checked = False Then
            correctFormat1 = facInputMasks.ValidateStringWithInputMask(strMask, txtWorkPhone.Text)
            If correctFormat1 = False Then
                errorMessage &= "Incorrect format for work phone field." & vbLf
            End If
        End If


        If txtHomePhone.Text <> "" And chkForeignHomePhone.Checked = False Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtHomePhone.Text)
            If correctFormat = False Then
                errorMessage &= "Incorrect format for home phone field." & vbLf
            End If
        End If


        If txtCellPhone.Text <> "" And chkForeignCellPhone.Checked = False Then
            correctFormat2 = facInputMasks.ValidateStringWithInputMask(strMask, txtCellPhone.Text)
            If correctFormat2 = False Then
                errorMessage &= "Incorrect format for cell phone field." & vbLf
            End If
        End If


        'Validate the zip field format. If the field is empty we should not apply the mask
        'against it.
        If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            correctFormat3 = facInputMasks.ValidateStringWithInputMask(zipMask, txtZip.Text)
            If correctFormat3 = False Then
                errorMessage &= "Incorrect format for zip field." & vbLf
            End If
        End If

        Return Trim(errorMessage)
    End Function
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim EmployerContactInfo As New plEmployerContactInfoFacade
        Dim Result As String
        Dim errorMessage As String = ""

        ''Check If Mask is successful only if Foreign Is not checked
        'If chkForeignWorkPhone.Checked = False Or chkForeignHomePhone.Checked = False Or chkForeignCellPhone.Checked = False Or chkForeignZip.Checked = False Then
        '    errorMessage = ValidateFieldsWithInputMasks()
        'Else
        '    errorMessage = ""
        'End If

        If errorMessage = "" Then
            'Call Update Function in the plEmployerInfoFacade
            Result = EmployerContactInfo.UpdateEmployerContactInfo(BuildEmployerContact(EmployerId), AdvantageSession.UserState.UserName)


            If Not Result = "" Then
                '   Display Error Message
                'DisplayErrorMessage(Result)
                DisplayRADAlert(CallbackType.Postback, "Error1", Result, "Save Error")
                Exit Sub
            End If


            'Reset The Checked Property Of CheckBox To True
            chkIsInDB.Checked = True

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            '  Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                SDFControl.DeleteSDFValue(txtEmployerContactId.Text)
                SDFID = SDFControl.GetAllLabels(pnlSDF)
                SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtEmployerContactId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


            'If DML is not successful then Prompt Error Message

            BindDataList(boolStatus, EmployerId)

            'Initialize Buttons
            InitButtonsForEdit()

            'set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, txtEmployerContactId.Text, ViewState, header1)

            ' US3037 5/4/2012 Janet Robinson
            CommonWebUtilities.RestoreItemValues(dlstEmployerContact, txtEmployerContactId.Text)
            setInternationalOrDomestic(txtWorkPhone, chkForeignWorkPhone.Checked)
            setInternationalOrDomestic(txtHomePhone, chkForeignHomePhone.Checked)
            setInternationalOrDomestic(txtCellPhone, chkForeignCellPhone.Checked)
        Else
            DisplayErrorMessageMask(errorMessage)
        End If



    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Function BuildEmployerContact(ByVal EmployerId As String) As plEmployerContact
        Dim EmployerContact As New plEmployerContact

        With EmployerContact

            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'Get EmployerId
            .EmployerContactId = txtEmployerContactId.Text

            'Get LastName
            .LastName = txtLastName.Text

            'Get FirstName
            .FirstName = txtFirstName.Text

            'Get MiddleName
            .MiddleName = txtMiddleName.Text

            'Get StatusId 
            .Status = ddlStatusId.SelectedValue

            'Get PrefixId 
            .Prefix = ddlPrefixId.SelectedValue

            'Get SuffixId 
            .Suffix = ddlSuffixId.SelectedValue

            'Get Title 
            .Title = txtTitleId.Text

            'Get HomePhone
            'phoneMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
            ' If txtHomePhone.Text <> "" And chkForeignHomePhone.Checked = False Then
            '    .HomePhone = facInputMask.RemoveMask(phoneMask, txtHomePhone.Text)
            'Else
            .HomePhone = txtHomePhone.Text
            ' End If


            'Get WorkPhone
            ' If txtWorkPhone.Text <> "" And chkForeignWorkPhone.Checked = False Then
            '    .WorkPhone = facInputMask.RemoveMask(phoneMask, txtWorkPhone.Text)
            'Else
            .WorkPhone = txtWorkPhone.Text
            'End If

            'Get CellPhone
            'If txtCellPhone.Text <> "" And chkForeignCellPhone.Checked = False Then
            '    .CellPhone = facInputMask.RemoveMask(phoneMask, txtCellPhone.Text)
            'Else
            .CellPhone = txtCellPhone.Text
            ' End If

            'Get Beeper
            .Beeper = txtBeeper.Text


            'Get City
            .City = txtCity.Text

            'Get Zip
            ' If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            '    zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
            '    .Zip = facInputMask.RemoveMask(zipMask, txtZip.Text)
            'Else
            .Zip = txtZip.Text
            ' End If


            'Get HomeEmail
            .HomeEmail = txtHomeEmail.Text

            'Get WorkEmail
            .WorkEmail = txtWorkEmail.Text

            'Get Address1
            .Address1 = txtAddress1.Text

            'Get Address2
            .Address2 = txtAddress2.Text

            'Get City
            .City = txtCity.Text

            'Get State
            .State = ddlState.SelectedValue


            'Get Country 
            .Country = ddlCountry.SelectedValue

            'WorkExt
            .WorkExt = txtWorkExt.Text

            'WorkBestTime
            .WorkBestTime = txtWorkBestTime.Text

            'HomeBestTime
            .HomeBestTime = txtHomeBestTime.Text

            'CellBestTime
            .CellBestTime = txtCellBestTime.Text

            'PinNumber
            .PinNumber = txtPinNumber.Text

            'Notes
            .Notes = txtNotes.Text

            .AddressTypeId = ddlAddressTypeId.SelectedValue

            .EmployerId = EmployerId

            If chkForeignHomePhone.Checked = True Then
                .ForeignHomePhone = 1
            Else
                .ForeignHomePhone = 0
            End If

            If chkForeignCellPhone.Checked = True Then
                .ForeignCellPhone = 1
            Else
                .ForeignCellPhone = 0
            End If

            If chkForeignZip.Checked = True Then
                .ForeignZip = 1
            Else
                .ForeignZip = 0
            End If

            If chkForeignWorkPhone.Checked = True Then
                .ForeignWorkPhone = 1
            Else
                .ForeignWorkPhone = 0
            End If


            .OtherState = txtOtherState.Text

            txtModDate.Text = Date.Now

            .ModDate = txtModDate.Text
        End With
        Return EmployerContact
    End Function
    Private Sub BindEmployerInfoData(ByVal EmployerContact As plEmployerContact)
        'Bind The EmployerInfo Data From The Database

        With EmployerContact

            'get IsInDB
            chkIsInDB.Checked = .IsInDB

            'Get EmployerId
            txtEmployerContactId.Text = .EmployerContactId

            'Get LastName
            txtLastName.Text = .LastName

            'Get FirstName
            txtFirstName.Text = .FirstName

            'Get MiddleName
            txtMiddleName.Text = .MiddleName

            'Get StatusId 
            ddlStatusId.SelectedValue = .StatusId

            'zip
            Try
                txtZip.Text = .Zip
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtZip.Text = ""
            End Try
            Try
                If txtZip.Text <> "" And .ForeignZip = False Then
                    txtZip.Text = txtZip.Text 'facInputMasks.ApplyMask(zipMask, txtZip.Text)
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtZip.Text = ""
            End Try


            'ForeignHomePhone
            Try
                If .ForeignHomePhone = True Then
                    chkForeignHomePhone.Checked = True
                Else
                    chkForeignHomePhone.Checked = False
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                chkForeignHomePhone.Checked = False
            End Try


            'Foreign Cell Phone
            Try
                If .ForeignCellPhone = True Then
                    chkForeignCellPhone.Checked = True
                Else
                    chkForeignCellPhone.Checked = False
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                chkForeignCellPhone.Checked = False
            End Try

            'Foreign Fax
            Try
                If .ForeignZip = True Then
                    chkForeignZip.Checked = True
                    ddlState.Enabled = False
                    lblOtherState.Visible = True
                    txtOtherState.Visible = True
                    txtOtherState.Text = .OtherState
                Else
                    chkForeignZip.Checked = False
                    ddlState.Enabled = True
                    lblOtherState.Visible = False
                    txtOtherState.Visible = False
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                chkForeignZip.Checked = False
                ddlState.Enabled = True
                lblOtherState.Visible = False
                txtOtherState.Visible = False
            End Try

            'Work Phone Checkbox
            Try
                If .ForeignWorkPhone = True Then
                    chkForeignWorkPhone.Checked = True
                Else
                    chkForeignWorkPhone.Checked = False
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                chkForeignWorkPhone.Checked = False
            End Try

            setInternationalOrDomestic(txtWorkPhone, chkForeignWorkPhone.Checked)
            setInternationalOrDomestic(txtHomePhone, chkForeignHomePhone.Checked)
            setInternationalOrDomestic(txtCellPhone, chkForeignCellPhone.Checked)

            'Get HomePhone
            txtHomePhone.Text = .HomePhone
            Try
                If txtHomePhone.Text <> "" And .ForeignHomePhone = False Then
                    txtHomePhone.Text = txtHomePhone.Text 'facInputMasks.ApplyMask(strMask, txtHomePhone.Text)
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtHomePhone.Text = ""
            End Try



            'Get WorkPhone
            txtWorkPhone.Text = .WorkPhone
            Try
                If txtWorkPhone.Text <> "" And .ForeignWorkPhone = False Then
                    txtWorkPhone.Text = txtWorkPhone.Text 'facInputMasks.ApplyMask(strMask, txtWorkPhone.Text)
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtWorkPhone.Text = ""
            End Try


            'Get CellPhone
            txtCellPhone.Text = .CellPhone
            Try
                If txtCellPhone.Text <> "" And .ForeignCellPhone = False Then
                    txtCellPhone.Text = txtCellPhone.Text 'facInputMasks.ApplyMask(strMask, txtCellPhone.Text)
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtCellPhone.Text = ""
            End Try


            'Get Beeper
            txtBeeper.Text = .Beeper

            'Get HomeEmail
            txtHomeEmail.Text = .HomeEmail

            'Get WorkEmail
            txtWorkEmail.Text = .WorkEmail

            ''Get Prefix
            'ddlPrefixId.SelectedValue = .Prefix

            ''Get Suffix 
            'ddlSuffixId.SelectedValue = .Suffix

            '   bind preffix
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrefixId, .PrefixID, .Prefix)

            '   bind suffix
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSuffixId, .SuffixId, .Suffix)

            'Get Title
            txtTitleId.Text = .Title

            'Get Address1
            txtAddress1.Text = .Address1

            'Get Address2
            txtAddress2.Text = .Address2

            'Get City
            txtCity.Text = .City

            'Get State
            'Try
            '    ddlState.SelectedValue = .State
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    BuildStatesDDL()
            'End Try
            '   bind StateId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlState, .StateId, .State)

            'Get WorkExt
            txtWorkExt.Text = .WorkExt

            'Get WorkBestTime
            txtWorkBestTime.Text = .WorkBestTime

            'Get CellBestTime
            txtCellBestTime.Text = .CellBestTime

            'Get PinNumber
            txtPinNumber.Text = .PinNumber

            'Get Home BestTime
            txtHomeBestTime.Text = .HomeBestTime

            'Get Notes
            txtNotes.Text = .Notes

            'Country
            'ddlCountry.SelectedValue = .Country
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountry, .CountryId, .Country)

            'AddressType
            'ddlAddressTypeId.SelectedValue = .AddressTypeID
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressTypeId, .AddressTypeId, .AddressType)



            Try
                txtModDate.Text = .ModDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtModDate.Text = Date.Now.ToShortDateString
            End Try

        End With
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        'Assign A New Value For Primary Key Column
        txtEmployerContactId.Text = Guid.NewGuid.ToString()

        'Reset The Value of chkIsInDb Checkbox
        'To Identify an Insert
        chkIsInDB.Checked = False

        'Create a Empty Object and Initialize the Object
        BindEmployerInfoDataBind(New plEmployerContact)

        Try
            ddlCountry.SelectedValue = strDefaultCountry
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlCountry.SelectedIndex = 0
        End Try

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, ResourceId, ModuleID)

        'Initialize Buttons
        InitButtonsForLoad()

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, Guid.Empty.ToString, ViewState, header1)

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)

    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub dlstEmployerContact_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstEmployerContact.ItemCommand
        '   get the EmployerContactId from the backend and display it
        GetEmployerName(e.CommandArgument)
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.SetHiddenControlForAudit()

        'Initialize Buttons For Edit
        InitButtonsForEdit()

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, e.CommandArgument, ViewState, header1)

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEdit(pnlSDF, ResourceId, e.CommandArgument, ModuleID)

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, e.CommandArgument)
        setInternationalOrDomestic(txtWorkPhone, chkForeignWorkPhone.Checked)
        setInternationalOrDomestic(txtHomePhone, chkForeignHomePhone.Checked)
        setInternationalOrDomestic(txtCellPhone, chkForeignCellPhone.Checked)

    End Sub
    Private Sub GetEmployerName(ByVal employercontactid As String)
        Dim employerinfo As New plEmployerContactInfoFacade
        BindEmployerInfoData(employerinfo.GetEmployerContactInfo(employercontactid))
    End Sub
    Private Sub BindDataList(ByVal showActiveOnly As String, ByVal EmployerId As String)
        'Bind The First,Middle,LastName based on EmployerContactId
        With New plEmployerContactInfoFacade
            dlstEmployerContact.DataSource = .GetAllEmployerContacts(showActiveOnly, EmployerId)
            dlstEmployerContact.DataBind()
        End With
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click

        If Not (txtEmployerContactId.Text = Guid.Empty.ToString) Then
            'instantiate component
            Dim EmployerInfo As New plEmployerContactInfoFacade

            'Delete The Row Based on EmployerId 
            Dim result As String = EmployerInfo.DeleteEmployerContactInfo(txtEmployerContactId.Text, Date.Parse(txtModDate.Text))

            'If Delete Operation was unsuccessful
            If result <> "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error2", result, "Delete Error")

            Else
                'bind an empty new BankAcctInfo
                BindEmployerInfoDataBind(New plEmployerContact)

                'initialize buttons
                InitButtonsForLoad()

                Try
                    ddlCountry.SelectedValue = strDefaultCountry
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlCountry.SelectedIndex = 0
                End Try

                Dim ResourceID As Integer
                ResourceID = Trim(Request.QueryString("resid"))

                Dim SDFControls As New SDFComponent
                SDFControls.DeleteSDFValue(txtEmployerContactId.Text)
                SDFControls.GenerateControlsNew(pnlSDF, ResourceID, ModuleID)

                'Bind The DataList
                BindDataList(boolStatus, EmployerId)
                'header1.EnableHistoryButton(False)
            End If
        End If

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)

    End Sub
    Private Sub BindEmployerInfoDataBind(ByVal EmployerContact As plEmployerContact)
        'Bind The EmployerInfo Data From The Database
        BuildDropDownLists()
        With EmployerContact

            'get IsInDB
            chkIsInDB.Checked = .IsInDB

            'Get EmployerId
            txtEmployerContactId.Text = .EmployerContactId

            'Get LastName
            txtLastName.Text = .LastName

            'Get FirstName
            txtFirstName.Text = .FirstName

            'Get MiddleName
            txtMiddleName.Text = .MiddleName


            'Get HomePhone
            txtHomePhone.Text = .HomePhone

            'Get WorkPhone
            txtWorkPhone.Text = .WorkPhone

            'Get CellPhone
            txtCellPhone.Text = .CellPhone

            'Get Beeper
            txtBeeper.Text = .Beeper

            'Get HomeEmail
            txtHomeEmail.Text = .HomeEmail

            'Get WorkEmail
            txtWorkEmail.Text = .WorkEmail

            ''Get Prefix
            'ddlPrefixId.SelectedValue = .Prefix

            ''Get Suffix 
            'ddlSuffixId.SelectedValue = .Suffix

            '   bind preffix
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrefixId, .PrefixID, .Prefix)

            '   bind suffix
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSuffixId, .SuffixId, .Suffix)

            'Get Title
            txtTitleId.Text = .Title

            'Get Address1
            txtAddress1.Text = .Address1

            'Get Address2
            txtAddress2.Text = .Address2

            'Get City
            txtCity.Text = .City

            'Get Zip
            txtZip.Text = .Zip

            'Get WorkExt
            txtWorkExt.Text = .WorkExt

            'Get WorkBestTime
            txtWorkBestTime.Text = .WorkBestTime

            'Get CellBestTime
            txtCellBestTime.Text = .CellBestTime

            'Get PinNumber
            txtPinNumber.Text = .PinNumber

            'Get HomeBestTime
            txtHomeBestTime.Text = .HomeBestTime

            'Get Notes
            txtNotes.Text = .Notes

            'ModDate
            txtModDate.Text = .ModDate

            ''State
            'ddlState.SelectedValue = .State
            '   bind StateId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlState, .StateId, .State)

        End With
    End Sub
    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radStatus.SelectedIndexChanged
        BindDataList(boolStatus, EmployerId)
        BindEmployerInfoDataBind(New plEmployerContact)
        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, ResourceId, ModuleID)
        InitButtonsForLoad()

    End Sub
    Private Sub chkForeignWorkPhone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignWorkPhone.CheckedChanged
        SetFocus(txtWorkPhone)
        setInternationalOrDomestic(txtWorkPhone, chkForeignWorkPhone.Checked)
    End Sub
    Private Overloads Sub SetFocus(ByVal ctrl As Control)
        ' Define the JavaScript function for the specified control.
        Dim focusScript As String = "<script language='javascript'>" &
          "document.getElementById('" + ctrl.ClientID &
          "').focus();</script>"

        ' Add the JavaScript code to the page.
        Page.ClientScript.RegisterStartupScript(Me.GetType(), "FocusScript", focusScript, False)
    End Sub
    Private Sub chkForeignHomePhone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignHomePhone.CheckedChanged
        SetFocus(txtHomePhone)
        setInternationalOrDomestic(txtHomePhone, chkForeignHomePhone.Checked)
    End Sub
    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignZip.CheckedChanged
        If chkForeignZip.Checked = True Then
            ddlState.Enabled = False
            lblOtherState.Visible = True
            txtOtherState.Visible = True
        Else
            ddlState.Enabled = True
        End If
        SetFocus(txtAddress1)
    End Sub
    Private Sub chkForeignCellPhone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignCellPhone.CheckedChanged
        SetFocus(txtCellPhone)
        setInternationalOrDomestic(txtCellPhone, chkForeignCellPhone.Checked)
    End Sub
    Private Sub ddlState_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlState.SelectedIndexChanged
    End Sub
    Private Sub txtCity_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtCity.TextChanged
        If chkForeignZip.Checked = True Then
            SetFocus(txtOtherState)
        Else
            SetFocus(ddlState)
        End If
    End Sub
    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtOtherState.TextChanged
        SetFocus(txtZip)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(chkForeignCellPhone)
        controlsToIgnore.Add(chkForeignHomePhone)
        controlsToIgnore.Add(chkForeignWorkPhone)
        controlsToIgnore.Add(chkForeignZip)

        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub


    Private Sub setInternationalOrDomestic(ByVal txtBox As RadMaskedTextBox, ByVal isInternational As Boolean)
        If isInternational Then
            txtBox.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtBox.DisplayMask = ""
            txtBox.RequireCompleteText = False
            txtBox.EmptyMessage = "enter digits only"
        Else
            txtBox.Mask = "(###)-###-####"
            txtBox.RequireCompleteText = True
            txtBox.DisplayMask = "(###) ###-####"
            txtBox.DisplayPromptChar = ""
            txtBox.EmptyMessage = "(area) phone number"
        End If

    End Sub
End Class
