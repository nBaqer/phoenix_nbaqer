﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="NewEmployer.aspx.vb" Inherits="NewEmployer" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Enabled="False"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table cellpadding="0" cellspacing="0" class="maincontenttable" style="width: 98%; border: none;">
                            <tr>
                                <td class="detailsframe">
                                    <div class="boxContainer">
                                        <h3><%=Header.Title  %></h3>
                                        <asp:Panel ID="pnlRHS" runat="server">
                                            <table class="contentleadmastertable" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="contentcellheader" nowrap="nowrap" colspan="6" style="border-top: 0; border-left: 0; border-right: 0">
                                                        <asp:Label ID="label1" runat="server" CssClass="label" Font-Bold="true">General Information</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap" style="padding-top: 16px">
                                                        <asp:Label ID="lblCode" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column" style="padding-top: 16px">
                                                        <asp:TextBox ID="txtCode" TabIndex="1" CssClass="textbox" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap" style="padding: 16px 0 0 3em">
                                                        <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright" style="padding-top: 16px">
                                                        <asp:DropDownList ID="ddlStatusId" TabIndex="6" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap">
                                                        <asp:Label ID="lblEmployerDescrip" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <table class="contenttable" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="width: 60%">
                                                                    <asp:TextBox ID="txtEmployerDescrip" TabIndex="2" CssClass="textbox" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:CheckBox ID="chkGroupName" CssClass="checkboxinternational" Text="Group?" runat="server"></asp:CheckBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap" style="padding-left: 3em">
                                                        <asp:Label ID="lblFeeId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright">
                                                        <asp:DropDownList ID="ddlFeeId" TabIndex="7" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap">
                                                        <asp:Label ID="lblParentId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <asp:DropDownList ID="ddlParentId" TabIndex="3" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap" style="padding-left: 3em">
                                                        <asp:Label ID="lblIndustryId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright">
                                                        <asp:DropDownList ID="ddlIndustryId" TabIndex="8" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap">
                                                        <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <asp:DropDownList ID="ddlCampGrpId" TabIndex="4" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap">&nbsp;
                                                    </td>
                                                    <td class="contentcell4columnright">&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap" style="padding-bottom: 16px">
                                                        <asp:Label ID="lblJobCatId" runat="server" CssClass="label">Jobs Offered</asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright" colspan="5" style="padding-bottom: 16px">
                                                        <asp:CheckBoxList ID="chkJobCatId" TabIndex="5" runat="Server" CssClass="checkboxBox"
                                                            RepeatColumns="7">
                                                        </asp:CheckBoxList>
                                                    </td>

                                                </tr>
                                            </table>
                                            <table class="contentleadmastertable" cellspacing="0" cellpadding="0" width="100%"
                                                style="border-top: 0">
                                                <tr>
                                                    <td class="contentcellheader" nowrap="nowrap" colspan="6" style="border-top: 0; border-left: 0; border-right: 0">
                                                        <asp:Label ID="lblAddress" runat="server" CssClass="label" Font-Bold="true">Address</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap" style="padding-top: 16px">&nbsp;
                                                    </td>
                                                    <td class="contentcell4column" style="padding-top: 16px">
                                                        <asp:CheckBox ID="chkForeignZip" TabIndex="9" CssClass="checkboxinternational" runat="server"
                                                            AutoPostBack="true"></asp:CheckBox>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap" style="padding: 16px 0 0 3em">&nbsp;
                                                    </td>
                                                    <td class="contentcell4columnright" style="padding-top: 16px">&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap">
                                                        <asp:Label ID="lblAddress1" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <asp:TextBox ID="txtAddress1" TabIndex="10" CssClass="textbox" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap" style="padding-left: 3em">
                                                        <asp:Label ID="lblCountyId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright">
                                                        <asp:DropDownList ID="ddlCountyId" TabIndex="16" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap">
                                                        <asp:Label ID="lblAddress2" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <asp:TextBox ID="txtAddress2" TabIndex="11" CssClass="textbox" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap" style="padding-left: 3em">
                                                        <asp:Label ID="lblLocationId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright">
                                                        <asp:DropDownList ID="ddlLocationId" TabIndex="17" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap">
                                                        <asp:Label ID="lblCity" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <asp:TextBox ID="txtcity" TabIndex="12" CssClass="textbox" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap"></td>
                                                    <td class="contentcell4columnright"></td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap">
                                                        <asp:Label ID="lblStateId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <asp:DropDownList ID="ddlStateId" TabIndex="13" CssClass="dropdownlist" runat="server" Width="200px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap">
                                                        <asp:Label ID="lblOtherState" runat="server" CssClass="label" Visible="false"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright">
                                                        <asp:TextBox ID="txtOtherState" CssClass="textbox" runat="server" AutoPostBack="True"
                                                            Visible="false"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap">
                                                        <asp:Label ID="lblZip" runat="server" CssClass="label">Zip Code</asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <telerik:RadMaskedTextBox ID="txtZip" TabIndex="14" runat="server"  Width="200px"
                                                            CssClass="textbox" Mask="#####" DisplayMask="#####" DisplayPromptChar="" AutoPostBack="false">
                                                        </telerik:RadMaskedTextBox>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap">&nbsp;
                                                    </td>
                                                    <td class="contentcell4columnright">&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap="nowrap" style="padding-bottom: 16px">
                                                        <asp:Label ID="lblCountryId" runat="server" CssClass="label">Country</asp:Label>
                                                    </td>
                                                    <td class="contentcell4column" style="padding-bottom: 16px">
                                                        <asp:DropDownList ID="ddlCountryId" TabIndex="15" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap">&nbsp;
                                                    </td>
                                                    <td class="contentcell4columnright">&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="contentleadmastertable" cellspacing="0" cellpadding="0" width="100%"
                                                style="border-top: 0">
                                                <tr>
                                                    <td class="contentcellheader" nowrap="nowrap" colspan="6" style="border-top: 0; border-left: 0; border-right: 0">
                                                        <asp:Label ID="lblPhones" runat="server" CssClass="label" Font-Bold="true">Phone and Fax</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" style="padding-top: 16px">
                                                        <asp:Label ID="lblPhone" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column" style="padding-top: 16px">
                                                        <table class="contenttable" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 60%">
                                                                    <telerik:RadMaskedTextBox ID="txtPhone" TabIndex="19" runat="server"
                                                                        Width="200px" DisplayFormatPosition="Right" Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar="">
                                                                    </telerik:RadMaskedTextBox>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:CheckBox ID="chkForeignPhone" TabIndex="18" CssClass="checkboxinternational"
                                                                        runat="server" AutoPostBack="true"></asp:CheckBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap" style="padding: 16px 0 16px 3em">
                                                        <asp:Label ID="lblFax" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright" style="padding: 16px 0 16px 0">
                                                        <table class="contenttable" cellpadding="0" cellspacing="0" width="100%">
                                                            <tr>
                                                                <td style="width: 60%">
                                                                    <telerik:RadMaskedTextBox ID="txtFax" TabIndex="21" runat="server"
                                                                        Width="200px" DisplayFormatPosition="Right" Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar="">
                                                                    </telerik:RadMaskedTextBox>
                                                                </td>
                                                                <td style="width: 40%">
                                                                    <asp:CheckBox ID="chkForeignFax" TabIndex="20" CssClass="checkboxinternational" runat="server"
                                                                        AutoPostBack="true"></asp:CheckBox>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table class="contentleadmastertable" cellspacing="0" cellpadding="0" width="100%"
                                                style="border-top: 0">
                                                <tr>
                                                    <td class="contentcellheader" nowrap="nowrap" colspan="6" style="border-top: 0; border-left: 0; border-right: 0">
                                                        <asp:Label ID="label3" runat="server" CssClass="label" Font-Bold="true">Email</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" style="padding: 16px 0 16px 16px">
                                                        <asp:Label ID="lblEmail" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column" style="padding: 16px 0 16px 0">
                                                        <asp:TextBox ID="txtEmail" TabIndex="22" CssClass="textbox" runat="server"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="None"
                                                            ErrorMessage="Invalid Email Format" ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">Invalid Email Format</asp:RegularExpressionValidator>
                                                    </td>
                                                    <td class="leadcell4column" nowrap="nowrap" style="padding-top: 16px">&nbsp;
                                                    </td>
                                                    <td class="contentcell4columnright" style="padding-top: 16px">&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                    <tr>
                                                        <td class="contentcellheader" nowrap="nowrap" colspan="6">
                                                            <asp:Label ID="lblSDF" runat="server" Font-Bold="true" CssClass="label">School Defined Fields</asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="contentcell2" colspan="6">
                                                            <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                                            </asp:Panel>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:TextBox ID="txtEmployerId" runat="server" Visible="False"></asp:TextBox>
                                            <asp:CheckBox ID="chkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                                            <asp:TextBox ID="txtModDate" runat="server" Visible="false"></asp:TextBox>
                                            <asp:TextBox ID="txtModUser" runat="server" Visible="false"></asp:TextBox>
                                            <asp:TextBox ID="txtRowIds" Style="display: none" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtResourceId" Style="display: none" runat="server"></asp:TextBox>
                                        </asp:Panel>
                                        <!-- end content table-->
                                    </div>
                                </td>
                                <!-- end rightcolumn -->
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

