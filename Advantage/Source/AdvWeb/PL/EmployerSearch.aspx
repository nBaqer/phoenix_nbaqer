﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="EmployerSearch.aspx.vb" Inherits="EmployerSearch" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table cellspacing="0" class="listframetop2" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframenofilterblue">Search Employer
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleft">
                            <table cellspacing="0" cellpadding="2" align="center" width="100%">
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblCode" runat="Server" CssClass="label">Code</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:TextBox ID="txtCode" runat="Server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblEmployerDescrip" runat="Server" CssClass="label">Name</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:TextBox ID="txtEmployerDescrip" runat="Server" CssClass="textbox"></asp:TextBox>
                                        <asp:TextBox ID="txtEmployerId" runat="Server" Visible="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblCity" runat="Server" CssClass="label">City</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlCity" runat="Server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblState" runat="Server" CssClass="label">State</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlState" runat="Server" CssClass="dropdownlist" Width="200px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblZip" runat="Server" CssClass="label">Zip</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlZip" runat="Server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblCounty" runat="Server" CssClass="label">County</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlCountyID" runat="Server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblLocation" runat="Server" CssClass="label">Location</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlLocationID" runat="Server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblStatusID" runat="Server" CssClass="label">Status</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlStatusID" runat="Server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                 
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblIndustry" runat="Server" CssClass="label">Industry</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlIndustryID" runat="Server" CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        <asp:Label ID="lblCampGrpID" runat="Server" CssClass="label" Visible="false">Campus Group</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlCampGrpID" runat="Server" CssClass="dropdownlist" Visible="false">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                                <tr>
                                    <td class="employersearch"></td>
                                    <td class="employersearch2" align="left">
                                        <table cellspacing="0" cellpadding="2" align="left" width="100%">
                                            <tr>
                                                <td align="left">
                                                    <telerik:RadButton ID="btnSearch" runat="server" Text="Search"></telerik:RadButton>
                                                    &nbsp;
                                                <telerik:RadButton ID="btnReset" runat="server" Text="Reset "></telerik:RadButton>
                                                </td>
                                                <td align="left">
                                                
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>


        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" class="maincontenttable" style="width: 98%; border: none;">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3> <asp:Label ID="headerTitle" runat="server"></asp:Label></h3>
                            <!-- begin content table-->
                            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgrdTransactionSearch" Width="100%" CellPadding="0" BorderWidth="1px"
                                            BorderStyle="Solid" BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True"
                                            HeaderStyle-Wrap="true" EditItemStyle-Wrap="false" GridLines="Horizontal" runat="server">
                                            <EditItemStyle Wrap="false"></EditItemStyle>
                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                            <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                            <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Employer">
                                                    <HeaderStyle CssClass="datagridheader" Width="25%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="Linkbutton1" runat="server" CssClass="label" Text='<%# Container.DataItem("EmployerDescrip") %>'
                                                            CausesValidation="False" CommandArgument='<%# Container.DataItem("EmployerId") %>'
                                                            CommandName="StudentSearch"> </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Address">
                                                    <HeaderStyle CssClass="datagridheader" Width="30%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="TextBoxDate" Text='<%# Container.DataItem("Address1") %>' CssClass="label"
                                                            runat="server"> </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="City">
                                                    <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDgFirstName" Text='<%# Container.DataItem("City") %>' CssClass="label"
                                                            runat="server"> </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="State">
                                                    <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ToTheme1" Text='<%# Container.DataItem("State") %>' CssClass="label"
                                                            runat="server"> </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Zip">
                                                    <HeaderStyle CssClass="datagridheader" Width="15%"></HeaderStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="TextBox" Text='<%# Container.DataItem("Zip") %>' runat="server"> </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadNotification runat="server" ID="RadNotification1"
                Text="This is a test" ShowCloseButton="true"
                Width="400px" Height="125px"
                TitleIcon=""
                Position="Center" Title="Message"
                EnableRoundedCorners="true"
                EnableShadow="true"
                Animation="Fade"
                AnimationDuration="1000"
                Style="padding-left: 120px; padding-top: 5px; word-spacing: 2pt;">
            </telerik:RadNotification>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

