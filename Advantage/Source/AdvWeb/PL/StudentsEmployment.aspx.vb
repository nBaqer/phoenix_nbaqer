
' ===============================================================================
'
' FAME AdvantageV1
'
' EmployeeInfo.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Partial Class StudentsEmployment
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblSelectType As System.Web.UI.WebControls.Label
    'Protected WithEvents ddlSchoolType As System.Web.UI.WebControls.DropDownList
    'Protected WithEvents lblStatus As System.Web.UI.WebControls.Label
    'Protected WithEvents lblExtracurricular As System.Web.UI.WebControls.Label
    'Protected WithEvents chkExtracurricularId As System.Web.UI.WebControls.CheckBoxList
    'Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    'Protected WithEvents txtJobStatusId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    'Protected WithEvents ChkStatus As System.Web.UI.WebControls.CheckBox
    'Protected WithEvents lblShow As System.Web.UI.WebControls.Label
    'Protected ResourceID As Integer
    'Protected stEmploymentID As String
    'Protected strFrom As String
    'Protected strStatus As String
    'Private requestContext As HttpContext
    'Protected StudentId As String
    'Protected WithEvents ddlStateIdId As System.Web.UI.WebControls.TextBox


#End Region
'    Private pObj As New UserPagePermissionInfo
'    Protected ModuleId As String
'    Protected state As AdvantageSessionState

'    Dim LeadId As String
'    Protected boolSwitchCampus As Boolean = False


'    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
'        AdvantageSession.PageTheme = PageTheme.Blue_Theme

'    End Sub
'#Region "MRU Routines"
'    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
'        'CODEGEN: This method call is required by the Web Form Designer
'        'Do not modify it using the code editor.
'        InitializeComponent()

'    End Sub
'    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As Advantage.Business.Objects.StudentMRU

'        Dim objStudentState As New Advantage.Business.Objects.StudentMRU

'        Try
'            MyBase.GlobalSearchHandler(0)

'            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

'            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
'                StudentId = Guid.Empty.ToString()
'            Else
'                StudentId = AdvantageSession.MasterStudentId
'            End If

'            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
'                LeadId = Guid.Empty.ToString()
'            Else
'                LeadId = AdvantageSession.MasterLeadId
'            End If


'            With objStudentState
'                .StudentId = New Guid(StudentId)
'                .LeadId = New Guid(LeadId)
'                .Name = AdvantageSession.MasterName
'            End With

'            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
'            HttpContext.Current.Items("Language") = "En-US"

'            Master.ShowHideStatusBarControl(True)



'        Catch ex As Exception
 '        	Dim exTracker = new AdvApplicationInsightsInitializer()
'        	exTracker.TrackExceptionWrapper(ex)

'            Dim strSearchUrl As String = ""
'            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
'            Response.Redirect(strSearchUrl)
'        End Try

'        Return objStudentState

'    End Function
'#End Region

'    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        Dim objCommon As New CommonUtilities
'        Dim SDFControls As New SDFComponent
'        Dim campusId As String
'        Dim userId As String
'        Dim m_Context As HttpContext
'        Dim fac As New UserSecurityFacade

'        ResourceID = Trim(Request.QueryString("resid"))
'        campusid = Master.CurrentCampusId
'        userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
'        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

'        'Get StudentId and LeadId
'        Dim objStudentState As New StudentMRU
'        objStudentState = getStudentFromStateObject(92) 'Pass resourceid so that user can be redirected to same page while swtiching students

'        If objStudentState Is Nothing Then
'            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
'            Exit Sub
'        End If

'        With objStudentState
'            StudentId = .StudentId.ToString
'            LeadId = .LeadId.ToString

'        End With

'        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

'        strStatus = "All"



'        pObj = fac.GetUserResourcePermissions(userId, ResourceID, campusId)

'        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

'            If Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value) Then
'                BindEmploymentInfo(New PlacementInfo)
'            End If


'            InitButtonsForLoad()

'            m_Context = HttpContext.Current
'            txtResourceID.Text = CInt(m_Context.Items("ResourceId"))
'            'objCommon.PageSetup(Form1, "NEW")
'            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
'            'Disable the new and delete buttons
'            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
'            ViewState("MODE") = "NEW"
'            Session("From") = "FirstLoad"
'            'Bind The DropDownList
'            BuildDDLs()

'            'studentId = Session("StudentID") #BN 02/02/05 State Changes.
'            txtStudentId.Text = StudentId

'            'Get Default Employer Data when page is loaded for first time
'            BindDataList(txtStudentId.Text, strStatus)
'            'SDFControls.GenerateControlsNew(pnlSDF, ResourceID)

'            '   disable History button the first time
'            'Header1.EnableHistoryButton(False)
'            '16521: ENH: Galen: FERPA: Compliance Issue 
'            'added by Theresa G on May 7th 2010
'            'If (New StudentFERPA).HasFERPAPermission(ResourceID, StudentId) Then
'            '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, ResourceID.ToString)
'            'End If
'            InitButtonsForLoad()

'            If boolSwitchCampus = True Then
'                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
'            End If

'            MyBase.uSearchEntityControlId.Value = ""


'        Else
'            'objCommon.PageSetup(Form1, "EDIT")
'            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
'            'If the Unique Value(Primary Key Value) is not empty then
'            'Generate Controls Based on Unique Value.
'            'If Unique Value is Empty then Load Empty Controls
'        End If

'        'Check If any UDF exists for this resource
'        Dim intSDFExists As Integer = SDFControls.GetSDFExists(ResourceID, ModuleId)
'        If intSDFExists >= 1 Then
'            pnlUDFHeader.Visible = True
'        Else
'            pnlUDFHeader.Visible = False
'        End If

'        If Trim(txtStEmploymentId.Text) <> "" Then
'            SDFControls.GenerateControlsEdit(pnlSDF, ResourceID, txtStEmploymentId.Text, ModuleId)
'        Else
'            SDFControls.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)
'        End If
'    End Sub


'    Private Sub BuildJobStatusDDL()
'        'Bind the Certificate DropDownList
'        'Based on Degrees DropDownList
'        Dim JobStatus As New PlacementFacade
'        With ddlJobStatusId
'            .DataTextField = "JobStatusDescrip"
'            .DataValueField = "JobStatusId"
'            .DataSource = JobStatus.GetAllJobStatus()
'            .DataBind()
'            .Items.Insert(0, New ListItem("Select", ""))
'            .SelectedIndex = 0
'        End With
'    End Sub
'    Private Sub BuildJobTitleDDL()
'        'Bind the Certificate DropDownList
'        'Based on Degrees DropDownList
'        Dim JobTitle As New PlacementFacade
'        With ddlJobTitleId
'            .DataTextField = "JobTitleDescrip"
'            .DataValueField = "JobTitleId"
'            .DataSource = JobTitle.GetAllJobTitles()
'            .DataBind()
'            .Items.Insert(0, New ListItem("Select", ""))
'            .SelectedIndex = 0
'        End With
'    End Sub
'    Private Sub BuildDDLs()
'        BuildJobStatusDDL()
'        BuildJobTitleDDL()
'    End Sub
'    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

'        '   Set error condition
'        Customvalidator1.ErrorMessage = errorMessage
'        Customvalidator1.IsValid = False

'        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
'            '   Display error in message box in the client
'            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
'        End If

'    End Sub

'    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
'        Dim StudentEducation As New PlacementFacade
'        Dim Result As String

'        If Session("From") = "FirstLoad" Then
'            txtStEmploymentId.Text = Guid.NewGuid.ToString
'        End If
'        'CType(txtGraduatedDate, RadDatePicker).DbSelectedDate = .GraduationDate

'        If Not CType(txtStartDate, RadDatePicker).DbSelectedDate Is Nothing And Not CType(txtEndDate, RadDatePicker).DbSelectedDate Is Nothing Then
'            Dim WD As Integer = DateDiff(DateInterval.Day, CType(txtStartDate, RadDatePicker).DbSelectedDate, CType(txtEndDate, RadDatePicker).DbSelectedDate)
'            If WD < 1 Then
'                DisplayErrorMessage("The To Date must be greater than the From Date")
'                Exit Sub
'            End If
'        End If

'        txtRowIds.Text = txtStEmploymentId.Text
'        '        Dim errorMessage As String
'        'errorMessage = ValidateFieldsWithInputMasks()

'        'Call Update Function in the plEmployerInfoFacade
'        Result = StudentEducation.UpdateStudentEmployment(BuildStudentEmployment(txtStudentId.Text), Session("UserName"), txtStEmploymentId.Text)

'        If Not Result = "" Then
'            '   Display Error Message
'            DisplayErrorMessage(Result)
'            Exit Sub
'        End If

'        ''  Reset The Checked Property Of CheckBox To True
'        ChkIsInDB.Checked = True

'        Dim SDFID As ArrayList
'        Dim SDFIDValue As ArrayList
'        '        Dim newArr As ArrayList
'        Dim z As Integer
'        Dim SDFControl As New SDFComponent
'        SDFControl.DeleteSDFValue(txtStEmploymentId.Text)

'        SDFID = SDFControl.GetAllLabels(pnlSDF)
'        SDFIDValue = SDFControl.GetAllValues(pnlSDF)
'        For z = 0 To SDFID.Count - 1
'            SDFControl.InsertValues(txtStEmploymentId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
'        Next

'        'Bind The DataList Based On StudentId 
'        'and Education Institution Type.
'        BindDataList(txtStudentId.Text, strStatus)

'        ''  If DML is not successful then Prompt Error Message


'        ''If Page is free of errors Show Edit Buttons
'        If Page.IsValid Then
'            InitButtonsForEdit()
'        End If

'        'EnableBtnStudentType()
'        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, txtStudentId.Text, ViewState, Header1)
'        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, txtStEmploymentId.Text)
'    End Sub
'    Private Function BuildStudentEmployment(ByVal stdId As String) As PlacementInfo
'        Dim StudentEducation As New PlacementInfo
'        With StudentEducation
'            ''get IsInDB
'            .IsInDb = ChkIsInDB.Checked

'            'Get EmployerId
'            .StEmploymentId = txtStEmploymentId.Text

'            .StudentId = txtStudentId.Text

'            .Comments = txtComments.Text

'            .EmployerName = txtEmployerName.Text

'            .EmployerJobTitle = txtEmployerJobTitle.Text

'            .StartDate = CType(txtStartDate, RadDatePicker).DbSelectedDate

'            .EndDate = CType(txtEndDate, RadDatePicker).DbSelectedDate

'            .JobStatus = ddlJobStatusId.SelectedValue

'            .JobTitleId = ddlJobTitleId.SelectedValue

'            .JobResponsibilities = txtJobResponsibilities.Text

'            txtModDate.Text = Date.Now

'            .ModDate = txtModDate.Text
'        End With

'        Return StudentEducation
'    End Function
'    Private Sub InitButtonsForEdit()
'        If pObj.HasFull Or pObj.HasEdit Then
'            btnSave.Enabled = True
'        Else
'            btnSave.Enabled = False
'        End If

'        If pObj.HasFull Or pObj.HasDelete Then
'            btnDelete.Enabled = True
'        Else
'            btnDelete.Enabled = False
'        End If

'        If pObj.HasFull Or pObj.HasAdd Then
'            btnNew.Enabled = True
'        Else
'            btnNew.Enabled = False
'        End If
'    End Sub
'    Private Sub BindDataList(ByVal StudId As String, ByVal showActiveOnly As String)
'        With New PlacementFacade
'            dlstEmployerContact.DataSource = .GetEmployerNames(txtStudentId.Text, showActiveOnly)
'            dlstEmployerContact.DataBind()
'        End With
'    End Sub
'    Private Sub dlstEmployerContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstEmployerContact.ItemCommand
'        Dim strId As String = dlstEmployerContact.DataKeys(e.Item.ItemIndex).ToString()
'        Dim SDFControls As New SDFComponent

'        ChkIsInDB.Checked = True
'        GetEmploymentInfo(strId)

'        txtRowIds.Text = strId
'        Session("From") = "ItemCommand"
'        ResourceID = Trim(Request.QueryString("resid"))

'        SDFControls.GenerateControlsEdit(pnlSDF, ResourceID, strId, ModuleId)
'        txtStEmploymentId.Text = e.CommandArgument

'        'set Style to Selected Item
'        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, strId, ViewState, Header1)
'        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, strId)
'        'Initialize Buttons For Edit
'        InitButtonsForEdit()

'    End Sub
'    Private Sub GetEmploymentInfo(ByVal StEmploymentId As String)
'        Dim employmentinfo As New PlacementFacade
'        BindEmploymentInfoExist(employmentinfo.GetEmploymentInfo(StEmploymentId))
'    End Sub
'    Private Sub BindEmploymentInfoExist(ByVal AddressInfo As PlacementInfo)
'        'Bind The StudentInfo Data From The Database
'        Dim facInputMasks As New InputMasksFacade

'        With AddressInfo

'            txtEmployerName.Text = .EmployerName

'            txtEmployerJobTitle.Text = .EmployerJobTitle

'            Try
'                ddlJobTitleId.SelectedValue = .JobTitleId
'            Catch ex As System.Exception
 '            	Dim exTracker = new AdvApplicationInsightsInitializer()
'            	exTracker.TrackExceptionWrapper(ex)

'                BuildJobTitleDDL()
'            End Try
'            Try
'                ddlJobStatusId.SelectedValue = .JobStatus
'            Catch ex As System.Exception
 '            	Dim exTracker = new AdvApplicationInsightsInitializer()
'            	exTracker.TrackExceptionWrapper(ex)

'                ddlJobStatusId.SelectedIndex = 0
'            End Try
'            If Len(.StartDate) > 4 Then
'                CType(txtStartDate, RadDatePicker).DbSelectedDate = Convert.ToDateTime(.StartDate).ToShortDateString
'            Else
'                CType(txtStartDate, RadDatePicker).DbSelectedDate = Nothing
'            End If
'            If Len(.EndDate) > 4 Then
'                CType(txtEndDate, RadDatePicker).DbSelectedDate = Convert.ToDateTime(.EndDate).ToShortDateString
'            Else
'                CType(txtEndDate, RadDatePicker).DbSelectedDate = Nothing
'            End If
'            txtComments.Text = .Comments
'            txtStEmploymentId.Text = .StEmploymentId
'            txtJobResponsibilities.Text = .JobResponsibilities
'            txtModDate.Text = .ModDate
'        End With
'    End Sub

'    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
'        'Reset The Value of chkIsInDb Checkbox
'        'To Identify an Insert
'        ChkIsInDB.Checked = False
'        Session("From") = "New"
'        ResourceID = Trim(Request.QueryString("resid"))

'        'Create a Empty Object and Initialize the Object
'        BindEmploymentInfo(New PlacementInfo)
'        BuildDDLs()

'        ddlJobTitleId.SelectedIndex = 0

'        Dim SDFControls As New SDFComponent
'        SDFControls.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)
'        'Initialize Buttons
'        InitButtonsForLoad()

'        'Reset Style in the Datalist
'        'MERGEFIX
'        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, Guid.Empty.ToString, ViewState, Header1)
'        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)
'    End Sub
'    Private Sub InitButtonsForLoad()
'        If pObj.HasFull Or pObj.HasAdd Then
'            btnSave.Enabled = True
'        Else
'            btnSave.Enabled = False
'        End If

'        'btnNew.Enabled = False
'        If pObj.HasFull Or pObj.HasAdd Then
'        Else
'            btnNew.Enabled = False
'        End If

'        btnDelete.Enabled = False
'    End Sub
'    Private Sub BindEmploymentInfo(ByVal AddressInfo As PlacementInfo)
'        Dim facInputMasks As New InputMasksFacade
'        With AddressInfo
'            If .StartDate <> "" Then
'                CType(txtStartDate, RadDatePicker).DbSelectedDate = Convert.ToDateTime(.StartDate).ToShortDateString
'            End If
'            txtEmployerName.Text = .EmployerName
'            txtEmployerJobTitle.Text = .EmployerJobTitle
'            If .EndDate <> "" Then
'                CType(txtEndDate, RadDatePicker).DbSelectedDate = Convert.ToDateTime(.EndDate).ToShortDateString
'            End If
'            txtComments.Text = .Comments
'            txtStEmploymentId.Text = .StEmploymentId
'            txtJobResponsibilities.Text = .JobResponsibilities
'        End With
'        BuildDDLs()
'    End Sub
'    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
'        If Not (txtStudentId.Text = Guid.Empty.ToString) Then
'            '   instantiate component
'            Dim StudentInfo As New PlacementFacade
'            'Delete The Row Based on StudentId
'            Dim result As String = StudentInfo.DeleteStudentEmployment(txtStEmploymentId.Text)
'            'If Delete Operation was unsuccessful
'            If Not result = "" Then
'                DisplayErrorMessage(result)
'                Exit Sub
'            End If
'            ResourceID = Trim(Request.QueryString("resid"))
'            Dim SDFControls As New SDFComponent
'            SDFControls.DeleteSDFValue(txtStEmploymentId.Text)
'            SDFControls.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)

'            BindEmploymentInfo(New PlacementInfo)
'            ChkIsInDB.Checked = False
'            BindDataList(txtStudentId.Text, strStatus)
'            InitButtonsForLoad()
'        End If
'    End Sub
'    Private Sub radStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
'        'Bind The DataList Based On StudentId and Status.
'        BindDataList(txtStudentId.Text, strStatus)
'    End Sub
'    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
'        'add to this list any button or link that should ignore the Confirm Exit Warning.
'        Dim controlsToIgnore As New ArrayList()
'        'add save button 
'        controlsToIgnore.Add(btnSave)
'        'controlsToIgnore.Add(Img1)
'        'controlsToIgnore.Add(Img2)
'        'Add javascript code to warn the user about non saved changes 
'        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
'    End Sub
End Class
