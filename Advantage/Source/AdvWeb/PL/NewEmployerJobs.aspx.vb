﻿Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports BO = Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer

Partial Class NewEmployerJobs
    Inherits BasePage
    Protected WithEvents lblBenefits As System.Web.UI.WebControls.Label
    Protected WithEvents lblSchedule As System.Web.UI.WebControls.Label



    Protected WithEvents txtEmployerContactId As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtWorkDays As System.Web.UI.WebControls.TextBox
    Protected EmployerJobId As String
    Protected WithEvents CompareValidator1 As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected EmployerId As String
    Protected ResourceID As Integer
    Protected WithEvents Comparevalidator3 As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents lbltxtJobDescription As System.Web.UI.WebControls.Label
    Protected boolstatus As String
    Protected ModuleId As String
    Protected sdfcontrols As New SDFComponent
    Protected boolSwitchCampus As Boolean = False
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
#End Region

    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Protected userId As String
    Protected strDefaultCountry As String

    Private mruProvider As MRURoutines
    Protected state As AdvantageSessionState
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRURoutines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function getEmployerFromStateObject(ByVal paramResourceId As Integer) As BO.EmployerMRU


        Dim objStateInfo As New AdvantageStateInfo
        Dim objEmployerState As New BO.EmployerMRU

        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        MyBase.GlobalSearchHandler(2)

        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
        HttpContext.Current.Items("Language") = "En-US"

        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployerId) Then
            EmployerId = Guid.Empty.ToString()
        Else
            EmployerId = AdvantageSession.MasterEmployerId
        End If

        With objEmployerState
            .EmployerId = New Guid(EmployerId)
            .Name = objStateInfo.NameValue
        End With


        Dim objGetStudentStatusBar As New AdvantageStateInfo

        mruProvider = New MRURoutines(Me.Context, GetAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)
        objGetStudentStatusBar = mruProvider.BuildEmployerStatusBar(EmployerId)
        With objGetStudentStatusBar

            AdvantageSession.MasterEmpName = objGetStudentStatusBar.NameValue
            AdvantageSession.MasterEmpAddress1 = objGetStudentStatusBar.Address1
            AdvantageSession.MasterEmpAddress2 = objGetStudentStatusBar.Address2
            AdvantageSession.MasterEmpCity = objGetStudentStatusBar.City
            AdvantageSession.MasterEmpState = objGetStudentStatusBar.State
            AdvantageSession.MasterEmpZip = objGetStudentStatusBar.Zip
            AdvantageSession.MasterEmpPhone = objGetStudentStatusBar.Phone
            objEmployerState.Name = objGetStudentStatusBar.NameValue
        End With



        If Not String.IsNullOrEmpty(AdvantageSession.MasterEmployerId) Then
            Master.ShowHideStatusBarControl(True) 'Show Lead Bar only when Lead is not blank
            Master.PageObjectId = EmployerId
            Master.PageResourceId = Request.QueryString("resid").ToString()
            Master.setHiddenControlForAudit()
        Else
            Master.ShowHideStatusBarControl(False) 'Hide Lead Bar only when Lead is blank
        End If

        Return objEmployerState

    End Function


    'Private Function GetAdvAppSettings() As AdvAppSettings
    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If
    '    Return MyAdvAppSettings
    'End Function
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim SearchEmployerID As New EmployerSearchFacade
        '        Dim startPos As String
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade

        ResourceID = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceID, campusId)

        Dim objCommon As New CommonUtilities
        '        Dim strVID As String
        '     Dim state As AdvantageSessionState

        If radStatus.SelectedItem.Text = "Active" Then
            boolstatus = "True"
        ElseIf radStatus.SelectedItem.Text = "Inactive" Then
            boolstatus = "False"
        Else
            boolstatus = "All"
        End If

        'Get the EmployerId from the state object associated with this page
        '''''''''''''''''' Call to get EmployerId starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objEmployerState As New BO.EmployerMRU
        objEmployerState = getEmployerFromStateObject(86) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objEmployerState Is Nothing Then
            MyBase.RedirectToEmployerSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objEmployerState
            EmployerId = .EmployerId.ToString

        End With

        '''''''''''''''''' Call to get EmployerId  ends here ''''''''''''''''''''

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            InitButtonsForLoad()
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            txtCode.BackColor = Color.White
            ddlJobGroupId.BackColor = Color.White
            txtEmployerJobTitle.BackColor = Color.White
            ddlJobTitleId.BackColor = Color.White

            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"

            'Bind The DropDownList
            BuildDropDownLists()

            'Assign The Variable Value to the textbox
            m_Context = HttpContext.Current
            txtResourceId.Text = CInt(m_Context.Items("ResourceId"))
            txtEmployerJobId.Text = Guid.NewGuid.ToString()
            ResourceID = Trim(Request.QueryString("resid"))
            txtResourceId.Text = ResourceID

            'Get Default Employer Data when page is loaded for first time
            BindDataList(boolstatus, EmployerId)
            'ChkStatus.Checked = True
            ClearIntegerFields()
            'txtSalaryFrom.Text = 0.ToString("$#,##0.00;($#,##0.00)")
            'txtSalaryTo.Text = 0.ToString("$#,##0.00;($#,##0.00)")
            txtJobPostedDate.SelectedDate = Date.Parse(Date.Now.ToShortDateString)
            MyBase.uSearchEntityControlId.Value = ""
        Else
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            txtCode.BackColor = Color.White
            ddlJobGroupId.BackColor = Color.White
            txtEmployerJobTitle.BackColor = Color.White
            ddlJobTitleId.BackColor = Color.White
            txtRowIds.Text = txtEmployerJobId.Text
            txtResourceId.Text = ResourceID
        End If

        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfcontrols.GetSDFExists(ResourceID, ModuleId)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtEmployerJobId.Text) <> "" Then
            sdfcontrols.GenerateControlsEdit(pnlSDF, ResourceID, txtEmployerJobId.Text, ModuleId)
        Else
            sdfcontrols.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)
        End If
        If (Master.IsSwitchedCampus) Then
            CampusObjects.ShowNotificationWhileSwitchingCampus(2, objEmployerState.Name)
        End If
        'EnableBtnEmployerType()
    End Sub
    'Private Sub BuildExpertiseDDL()
    '    'Bind the Status DropDownList
    '    Dim PrgVersion As New JobSearchFacade
    '    With ddlExpertiseId
    '        .DataTextField = "ExpertiseDescrip"
    '        .DataValueField = "ExpertiseId"
    '        .DataSource = PrgVersion.GetExpertiseLevel
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildDropDownLists()

        BuildJobWorkdaysDDL()

        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing))

        'Campus Groups DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlCampGrpId, AdvantageDropDownListName.CampGrps, campusId, True, True))

        'Employer Fees DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlFeeId, AdvantageDropDownListName.Employer_Fees, campusId, True, True))

        'Employer Job Types DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlTypeId, AdvantageDropDownListName.JobTypes, Nothing, True, True))

        'Employer Job Categories DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlJobGroupId, AdvantageDropDownListName.Job_Categories, campusId, True, True))

        'Counties DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlAreaId, AdvantageDropDownListName.Counties, campusId, True, True))

        'Job Benefits DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlBenefitsId, AdvantageDropDownListName.Benefits, Nothing, True, True))

        'Job Schedules DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlScheduleId, AdvantageDropDownListName.JobSchedule, Nothing, True, True))

        'Employer Contacts DDL
        'ddlList.Add(New AdvantageDDLDefinition(ddlContactId, AdvantageDropDownListName.EmpContacts, Nothing, True, True))
        BuildJobContactsDDL()

        'Salary Types DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlSalaryTypeID, AdvantageDropDownListName.SalaryType, campusId, True, True))

        'Expertise Levels DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlExpertiseId, AdvantageDropDownListName.ExpertiseLevel, campusId, True, True))

        'JobTitles DDL
        'ddlList.Add(New AdvantageDDLDefinition(ddlJobTitleId, AdvantageDropDownListName.JobTitles, campusId, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub
    'Private Sub BuildStatusDDL()
    '    'Bind the Status DropDownList
    '    Dim statuses As New StatusesFacade
    '    With ddlStatusId
    '        .DataTextField = "Status"
    '        .DataValueField = "StatusId"
    '        .DataSource = statuses.GetAllStatuses()
    '        .DataBind()
    '    End With
    'End Sub
    'Private Sub BuildSalaryTypeDDL()
    '    Dim SalaryTypeId As New PlacementFacade
    '    With ddlSalaryTypeID
    '        .DataTextField = "SalaryTypeDescrip"
    '        .DataValueField = "SalaryTypeId"
    '        .DataSource = SalaryTypeId.GetAllSalaryType()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildCampusGroupsDDL()
    '    'Bind the CampusGroups DrowDownList
    '    Dim campusGroups As New CampusGroupsFacade
    '    With ddlCampGrpId
    '        .DataTextField = "CampGrpDescrip"
    '        .DataValueField = "CampGrpId"
    '        .DataSource = campusGroups.GetAllCampusGroups()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildEmployerFeeDDL()
    '    'Bind the Employer Fee DrowDownList
    '    Dim EmployerFee As New EmployerFeeFacade
    '    With ddlFeeId
    '        .DataTextField = "FeeDescrip"
    '        .DataValueField = "FeeId"
    '        .DataSource = EmployerFee.GetAllEmployerFee()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildCountyDDL()

    '    'Bind the County DrowDownList
    '    Dim county As New CountyFacade
    '    With ddlAreaId
    '        .DataTextField = "CountyDescrip"
    '        .DataValueField = "CountyId"
    '        .DataSource = county.GetAllCounty()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildEmployerJobsDDL()
    '    'Bind the EmployerJobsType DrowDownList
    '    Dim EmployerJobs As New EmployerJobsFacade
    '    With ddlTypeId
    '        .DataTextField = "JobGroupDescrip"
    '        .DataValueField = "JobGroupId"
    '        .DataSource = EmployerJobs.GetAllJobType()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildJobTitleDDL()
    '    'Bind the EmployerJobs DrowDownList
    '    Dim JobTitle As New EmployerJobsFacade
    '    With ddlJobTitleId
    '        .DataTextField = "TitleDescrip"
    '        .DataValueField = "TitleId"
    '        .DataSource = JobTitle.GetAllJobTitles()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildTitlesPageLoadDDL()
    '    ddlJobTitleId.Items.Clear()
    '    With ddlJobTitleId
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildJobGroupDDL()
    '    'Bind the EmployerGroup DrowDownList
    '    Dim JobGroup As New EmployerJobsFacade
    '    With ddlJobGroupId
    '        .DataTextField = "JobCatDescrip"
    '        .DataValueField = "JobCatId"
    '        .DataSource = JobGroup.GetAllEmployerJobs()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildJobTitlesByCatagoryDDL()
        'Bind the EmployerGroup DrowDownList
        Dim JobGroup As New EmployerJobsFacade
        With ddlJobTitleId
            .DataTextField = "TitleDescrip"
            .DataValueField = "TitleId"
            .DataSource = JobGroup.GetAllEmployerJobsByCategory(ddlJobGroupId.SelectedValue)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    'Private Sub BuildJobBenefitDDL()
    '    'Bind the EmployerJobs DrowDownList
    '    Dim JobTitle As New EmployerJobsFacade
    '    With ddlBenefitsId
    '        .DataTextField = "JobBenefitDescrip"
    '        .DataValueField = "JobBenefitId"
    '        .DataSource = JobTitle.GetAllJobBenefit
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildJobScheduleDDL()
    '    'Bind the EmployerJobs DrowDownList
    '    Dim JobTitle As New EmployerJobsFacade
    '    With ddlScheduleId
    '        .DataTextField = "JobScheduleDescrip"
    '        .DataValueField = "JobScheduleId"
    '        .DataSource = JobTitle.GetAllJobSchedule
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildJobContactsDDL()
        'Bind the EmployerJobs DrowDownList
        Dim JobTitle As New EmployerJobsFacade
        With ddlContactId
            .DataTextField = "FullName"
            .DataValueField = "EmployerContactId"
            .DataSource = JobTitle.GetAllJobContactsByEmployer(EmployerId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildJobWorkdaysDDL()
        'Bind the EmployerJobs DrowDownList
        Dim JobWorkDays As New EmployerJobsFacade
        With chkJobWorkDays
            .DataTextField = "WorkDaysDescrip"
            .DataValueField = "WorkDaysId"
            .DataSource = JobWorkDays.GetAllJobWorkDays
            .DataBind()
        End With
    End Sub
    Private Function BuildEmployerJobs(ByVal EmployerJobId As String) As EmployerJobsInfo
        Dim EmployerJobsData As New EmployerJobsInfo

        With EmployerJobsData

            ''get IsInDB
            .IsInDB = chkIsInDB.Checked

            'Get EmployerJobId
            .EmployerJobId = txtEmployerJobId.Text

            'Get EmployerID
            .EmployerId = EmployerId

            'Get Code
            .Code = txtCode.Text

            'Get StatusId 
            .StatusId = ddlStatusId.SelectedValue

            'Get Description
            .JobDescription = txtJobDescription.Text

            'Get CampusGroup
            .CampGrpId = ddlCampGrpId.SelectedValue

            'Get County
            .AreaId = ddlAreaId.SelectedValue

            'Get Fee
            .FeeId = ddlFeeId.SelectedValue

            'Get Job Title
            .JobTitleId = ddlJobTitleId.SelectedValue

            'Get Job Description
            .JobDescription = txtJobDescription.Text

            'Get Job Group
            .JobGroupId = ddlJobGroupId.SelectedValue

            'Get Type
            .TypeId = ddlTypeId.SelectedValue

            'Fee
            .FeeId = ddlFeeId.SelectedValue

            'NumberOpen
            If txtNumberOpen.Text = "" Then
                .NumberOpen = 0
            Else
                .NumberOpen = txtNumberOpen.Text
            End If

            'Number Filled
            If txtNumberFilled.Text = "" Then
                .NumberFilled = 0
            Else
                .NumberFilled = txtNumberFilled.Text
            End If

            'Opened From
            If txtOpenedFrom.SelectedDate Is Nothing Then
                .OpenedFrom = ""
            Else
                .OpenedFrom = txtOpenedFrom.SelectedDate
            End If


            'Opened To
            If txtOpenedTo.SelectedDate Is Nothing Then
                .OpenedTo = ""
            Else
                .OpenedTo = txtOpenedTo.SelectedDate
            End If


            'Start
            If txtStart.SelectedDate Is Nothing Then
                .Start = ""
            Else
                .Start = txtStart.SelectedDate
            End If


            'Salary From
            .SalaryFrom = txtSalaryFrom.Text

            'Salary To
            .SalaryTo = txtSalaryTo.Text

            'Benefits
            .BenefitsId = ddlBenefitsId.SelectedValue

            'Schedule 
            .ScheduleId = ddlScheduleId.SelectedValue

            'Hours From
            .HoursFrom = txtHoursFrom.Text



            'Hours To
            .HoursTo = txtHoursTo.Text

            'Notes
            .Notes = txtNotes.Text

            'ContactId
            .ContactId = ddlContactId.SelectedValue


            'JobTitleDesc
            '.JObTitleDescription = ddlJobTitleId.SelectedItem.Text

            'EmployerId
            .EmployerId = EmployerId

            'Job Requirement
            .Requirement = txtJobRequirements.Text

            'Salary Type
            .SalaryType = ddlSalaryTypeID.SelectedValue

            'EmployerJobTitle    
            .EmployerJobTitle = txtEmployerJobTitle.Text

            'Expertise Level
            .ExpertiseLevelId = ddlExpertiseId.SelectedValue

            'Job Posted Date
            If txtJobPostedDate.SelectedDate Is Nothing Then
                .JobPostedDate = ""
            Else
                .JobPostedDate = Date.Parse(txtJobPostedDate.SelectedDate)
            End If
            '.JobPostedDate = txtJobPostedDate.Text

            txtModDate.Text = Date.Now

            .ModDate = txtModDate.Text


        End With
        Return EmployerJobsData
    End Function
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim EmployerJobs As New EmployerJobsFacade
        Dim Result As String
        Dim strMessage As String = String.Empty
        Dim JobPostedDate As String

        If Not IsNothing(txtOpenedFrom.SelectedDate) And Not IsNothing(txtOpenedTo.SelectedDate) Then
            If DateDiff(DateInterval.Day, CDate(txtOpenedFrom.SelectedDate), CDate(txtOpenedTo.SelectedDate)) < 0 Then
                ' DisplayErrorMessage("The opento date should be greater than openfrom date", "../images/Information.gif")
                strMessage &= "The Open To date must be greater than Open From date" & vbLf
            End If
            If Not IsNothing(txtStart.SelectedDate) Then
                If DateDiff(DateInterval.Day, CDate(txtOpenedTo.SelectedDate), CDate(txtStart.SelectedDate)) < 0 Then
                    ' DisplayErrorMessage("The opento date should be greater than openfrom date", "../images/Information.gif")
                    strMessage &= "The Start date must be greater than Open To date" & vbLf
                End If
            End If
        End If

        If Not txtSalaryFrom.Text = "" And Not txtSalaryTo.Text = "" Then
            If Val(txtSalaryTo.Text) < Val(txtSalaryFrom.Text) Then
                strMessage &= "The Salary Range To should be greater than Salary Range From" & vbLf
            End If
        End If

        'If Trim(ddlJobTitleId.SelectedValue) = "" Then
        '    strMessage &= "School Job Title is required" & vbLf
        'End If

        If Not strMessage = "" Then
            'DisplayErrorMessage(strMessage)
            DisplayRADAlert(CallbackType.Postback, "Error1", strMessage, "Save Error")
            Exit Sub
        End If

        'Job Posted Date
        If txtJobPostedDate.SelectedDate Is Nothing Then
            JobPostedDate = ""
        Else
            JobPostedDate = Date.Parse(txtJobPostedDate.SelectedDate)
        End If
        ' Call Update Function in the plEmployerInfoFacade
        Result = EmployerJobs.UpdateEmployerInfo(BuildEmployerJobs(EmployerId), AdvantageSession.UserState.UserName, JobPostedDate)

        If Not Result = "" Then
            'DisplayErrorMessage(Result)
            DisplayRADAlert(CallbackType.Postback, "Error3", Result, "Save Error")
            Exit Sub
        End If

        '  Reset The Checked Property Of CheckBox To True
        chkIsInDB.Checked = True

        '   Update The Jobs Offered(CheckBoxes)
        UpdateEmployerJobWorkDays(txtEmployerJobId.Text)

        BindDataList(boolstatus, EmployerId)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim SDFID As ArrayList
        Dim SDFIDValue As ArrayList
        '        Dim newArr As ArrayList
        Dim z As Integer
        Dim SDFControl As New SDFComponent
        Try
            SDFControl.DeleteSDFValue(txtEmployerJobId.Text)
            SDFID = SDFControl.GetAllLabels(pnlSDF)
            SDFIDValue = SDFControl.GetAllValues(pnlSDF)
            For z = 0 To SDFID.Count - 1
                SDFControl.InsertValues(txtEmployerJobId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



        'If Page is free of errors 
        If Page.IsValid Then
            'Initialize Buttons
            InitButtonsForEdit()
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, txtEmployerJobId.Text, ViewState, header1)
        End If
        'EnableBtnEmployerType()

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, txtEmployerJobId.Text)

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        'CommonWebUtilities.TestDisplayErrorInMessageBox(Me.Page, errorMessage, strFileName)
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'Assign A New Value For Primary Key Column
        txtEmployerJobId.Text = Guid.NewGuid.ToString()

        'Reset The Value of chkIsInDb Checkbox
        'To Identify an Insert
        chkIsInDB.Checked = False

        'Create a Empty Object and Initialize the Object
        BindEmployerStatusChange(New EmployerJobsInfo)

        chkJobWorkDays.ClearSelection()

        ClearIntegerFields()

        'Initialize Buttons
        InitButtonsForLoad()

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, Guid.Empty.ToString, ViewState, header1)

        sdfcontrols.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)

    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub BindEmployerJobsData(ByVal EmployerInfo As EmployerJobsInfo)

        'Bind The EmployerInfo Data From The Database
        With EmployerInfo

            'Get EmployerId
            txtEmployerJobId.Text = .EmployerJobId

            'Get Check Status
            chkIsInDB.Checked = .IsInDB

            'Get Code
            txtCode.Text = .Code

            'Get StatusId 
            ddlStatusId.SelectedValue = .StatusId

            'Get Description
            txtJobDescription.Text = .JobDescription

            ''Get CampusGroup
            'If .CampGrpId <> "" Then
            '    ddlCampGrpId.SelectedValue = .CampGrpId
            'Else
            '    BuildCampusGroupsDDL()
            'End If
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCampGrpId, .CampGrpId, .CampGrpDescrip)

            ''Get ContactId
            'If .ContactId <> "" Then
            '    ddlContactId.SelectedValue = .ContactId
            'Else
            '    BuildJobContactsDDL()
            'End If
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlContactId, .ContactId, .Contact)

            ''Get County
            'If .AreaId <> "" Then
            '    ddlAreaId.SelectedValue = .AreaId
            'Else
            '    BuildCountyDDL()
            'End If
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAreaId, .AreaId, .Area)

            ''Get Fee
            'Try
            '    If .FeeId <> "" Then
            '        ddlFeeId.SelectedValue = .FeeId
            '    Else
            '        BuildEmployerFeeDDL()
            '    End If
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    ddlFeeId.SelectedIndex = 0
            'End Try
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFeeId, .FeeId, .Fee)

            'Get Job Description
            txtJobDescription.Text = .JobDescription

            ''Get Job Group
            'If .JobGroupId <> "" Then
            '    ddlJobGroupId.SelectedValue = .JobGroupId
            'Else
            '    BuildJobGroupDDL()
            'End If
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlJobGroupId, .JobGroupId, .JobGroup)

            ''Get Type
            'If .TypeId <> "" Then
            '    ddlTypeId.SelectedValue = .TypeId
            'Else
            '    BuildJobGroupDDL()
            'End If
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlTypeId, .TypeId, .Type)

            'Build The JobTitle Based On JobCatagory
            BuildJobTitlesByCatagoryDDL()

            'ddlJobTitleId.SelectedValue = .JobTitleId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlJobTitleId, .JobTitleId, .JobTitle)

            ''BenefitsId
            'If .BenefitsId <> "" Then
            '    ddlBenefitsId.SelectedValue = .BenefitsId
            'Else
            '    BuildJobBenefitDDL()
            'End If
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlBenefitsId, .BenefitsId, .Benefits)

            ''ScheduleId
            'If .ScheduleId <> "" Then
            '    ddlScheduleId.SelectedValue = .ScheduleId
            'Else
            '    BuildJobScheduleDDL()
            'End If
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlScheduleId, .ScheduleId, .Schedule)

            'NumberOpen
            If .NumberOpen = 0 Then
                txtNumberOpen.Text = ""
            Else
                txtNumberOpen.Text = .NumberOpen
            End If


            'Number Filled
            If .NumberFilled = 0 Then
                txtNumberFilled.Text = ""
            Else
                txtNumberFilled.Text = .NumberFilled
            End If


            'Opened From
            Try
                txtOpenedFrom.SelectedDate = .OpenedFrom
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtOpenedFrom.Clear()
            End Try

            'Opened To
            Try
                txtOpenedTo.SelectedDate = .OpenedTo
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtOpenedTo.Clear()
            End Try

            'Start
            Try
                txtStart.SelectedDate = .Start
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtStart.Clear()
            End Try

            'Salary From
            txtSalaryFrom.Text = .SalaryFrom

            'Salary To
            txtSalaryTo.Text = .SalaryTo

            'Hours From
            txtHoursFrom.Text = .HoursFrom

            'Hours To
            txtHoursTo.Text = .HoursTo

            'Notes
            txtNotes.Text = .Notes

            'Job Requirements
            txtJobRequirements.Text = .Requirement

            ''Get SalaryTypeID
            'If .SalaryType <> "" Then
            '    ddlSalaryTypeID.SelectedValue = .SalaryType
            'Else
            '    BuildSalaryTypeDDL()
            'End If
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSalaryTypeID, .SalaryTypeId, .SalaryType)

            txtModDate.Text = .ModDate

            'If .JobPostedDate <> "" Then
            '    txtJobPostedDate.Text = .JobPostedDate
            'Else
            '    txtJobPostedDate.Text = ""
            'End If

            Try
                txtJobPostedDate.SelectedDate = .JobPostedDate
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtJobPostedDate.Clear()
            End Try

            txtEmployerJobTitle.Text = .EmployerJobTitle

            'ddlExpertiseId.SelectedValue = .ExpertiseLevelId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlExpertiseId, .ExpertiseLevelId, .ExpertiseLevel)


        End With
    End Sub
    Private Sub BindEmployerStatusChange(ByVal EmployerInfo As EmployerJobsInfo)

        'Bind The EmployerInfo Data From The Database
        With EmployerInfo

            'Get EmployerId
            txtEmployerJobId.Text = .EmployerJobId

            'Get Check Status
            chkIsInDB.Checked = .IsInDB

            'Get Code
            txtCode.Text = .Code


            'Get Description
            txtJobDescription.Text = .JobDescription


            'NumberOpen
            If .NumberOpen = 0 Then
                txtNumberOpen.Text = ""
            Else
                txtNumberOpen.Text = .NumberOpen
            End If

            'Number Filled
            If .NumberFilled = 0 Then
                txtNumberFilled.Text = ""
            Else
                txtNumberFilled.Text = .NumberFilled
            End If

            'Opened From
            Try
                txtOpenedFrom.SelectedDate = .OpenedFrom
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtOpenedFrom.Clear()
            End Try

            'Opened To
            Try
                txtOpenedTo.SelectedDate = .OpenedTo
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtOpenedTo.Clear()
            End Try

            'Start
            Try
                txtStart.SelectedDate = .Start
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtStart.Clear()
            End Try

            'Salary From
            txtSalaryFrom.Text = .SalaryFrom

            'Salary To
            txtSalaryTo.Text = .SalaryTo

            'Hours From
            txtHoursFrom.Text = .HoursFrom

            'Hours To
            txtHoursTo.Text = .HoursTo

            'Notes
            txtNotes.Text = .Notes

            'Job Requirements
            txtJobRequirements.Text = .Requirement

            'Employer Job Title
            txtEmployerJobTitle.Text = .EmployerJobTitle

        End With
        BuildDropDownLists()
    End Sub
    Private Sub dlstEmployerContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstEmployerContact.ItemCommand

        'get the EmployerContactId from the backend and display it
        GetEmployerName(e.CommandArgument)
        txtRowIds.Text = e.CommandArgument.ToString()
        'header1.ObjectID = e.CommandArgument.ToString()
        BindJobWorkDayListBox(e.CommandArgument)
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceID
        Master.setHiddenControlForAudit()

        'set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, txtEmployerJobId.Text, ViewState, header1)

        'Initialize Buttons For Edit
        InitButtonsForEdit()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEdit(pnlSDF, ResourceID, e.CommandArgument, ModuleId)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, e.CommandArgument)

        'EnableBtnEmployerType()
    End Sub
    Private Sub GetEmployerName(ByVal employercontactid As String)
        Dim employerinfo As New EmployerJobsFacade
        BindEmployerJobsData(employerinfo.GetEmployerJobsInfo(employercontactid))
    End Sub
    Private Sub BindDataList(ByVal showActiveOnly As String, ByVal EmployerId As String)
        'Bind The First,Middle,LastName based on EmployerContactId
        With New EmployerJobsFacade
            dlstEmployerContact.DataSource = .GetAllEmployerJobsDescByEmployer(showActiveOnly, EmployerId)
            dlstEmployerContact.DataBind()
        End With
    End Sub
    Private Sub ClearIntegerFields()
        txtNumberOpen.Text = ""
        txtNumberFilled.Text = ""
    End Sub

    Private Sub UpdateEmployerJobWorkDays(ByVal empId As String)

        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        'Dim selectedDegrees() As String = selectedDegrees.CreateInstance(GetType(String), chkJobWorkDays.Items.Count)
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), chkJobWorkDays.Items.Count)

        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In chkJobWorkDays.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)

        'update Selected Jobs
        Dim JobWorkDays As New EmployerJobsFacade
        If JobWorkDays.UpdateEmployerJobWorkDays(empId, AdvantageSession.UserState.UserName, selectedDegrees) < 0 Then
            Customvalidator1.ErrorMessage = "There was a problem with Referential Integrity.<BR> You can not perform this operation."
            Customvalidator1.IsValid = False
        End If
    End Sub
    Private Sub BindJobWorkDayListBox(ByVal empId As String)

        'Get Degrees data to bind the CheckBoxList
        Dim WorkDays As New EmployerJobsFacade
        Dim ds As DataSet = WorkDays.GetWorkDays(empId)
        Dim JobCount As Integer = WorkDays.GetValidWorkdays(empId)
        chkJobWorkDays.ClearSelection()
        If JobCount >= 1 Then
            'Select the items on the CheckBoxList
            Dim i As Integer
            If Not (ds.Tables(0).Rows.Count < 0) Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim row As DataRow
                    row = ds.Tables(0).Rows(i)
                    Dim item As ListItem
                    For Each item In chkJobWorkDays.Items
                        If item.Value.ToString = DirectCast(row("WorkDayId"), Guid).ToString Then
                            item.Selected = True
                            Exit For
                        End If
                    Next
                Next
            End If
        End If
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Results.Visible = False
        If Not (txtEmployerJobId.Text = Guid.Empty.ToString) Then

            '   instantiate component
            Dim EmployerInfo As New EmployerJobsFacade


            'If Delete Operation was unsuccessful
            'Delete The Row Based on EmployerId 
            Dim result As String = EmployerInfo.DeleteEmployerJobs(txtEmployerJobId.Text, Date.Parse(txtModDate.Text))

            'If Delete Fails
            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error2", result, "Save Error")
            Else

                'Bind The Datalist
                BindDataList(boolstatus, EmployerId)

                'bind an empty new BankAcctInfo
                BindEmployerStatusChange(New EmployerJobsInfo)

                'initialize buttons
                InitButtonsForLoad()

                'Clear Integer Columns
                ClearIntegerFields()

                'Clear Checkboxes
                chkJobWorkDays.ClearSelection()

                'header1.EnableHistoryButton(False)

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                sdfcontrols.DeleteSDFValue(txtEmployerJobId.Text)
                sdfcontrols.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                ' US3037 5/4/2012 Janet Robinson
                CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)

            End If
        End If
    End Sub
    'Public Shared Sub TestDisplayErrorInMessageBox(ByVal page As Page, ByVal errorMessage As String, ByVal ImageInfo As String)
    '    If page.Request.Browser.JavaScript Then
    '        Dim scriptBegin As String = "<script type='text/javascript'>window.onload=DisplayError();function DisplayError(){window.open('../Test/TestPanel.aspx?Message="
    '        Dim strOtherParams As String = "&strFileType="
    '        Dim strWindowProperty As String = "','histwindow','width=400,height=150,screenX=400px,screenY=320px,left=400px,top=280px"
    '        Dim scriptEnd As String = "');}</script>"
    '        Dim strFullString As String = scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + strOtherParams + ImageInfo + strWindowProperty + scriptEnd
    '        '   Register a javascript to display error message
    '        page.RegisterStartupScript("ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + strOtherParams + ImageInfo + strWindowProperty + scriptEnd)
    '    End If
    'End Sub
    'Private Shared Function ReplaceSpecialCharactersInJavascriptMessage(ByVal s As String) As String
    '    '   replace \ by \\, ' by \', " by "", CR by \r, LF by \n
    '    Return s.Trim().Replace("\", "\\").Replace("'", "\'").Replace("""", "\""").Replace(vbCr, "\r").Replace(vbLf, "\n")
    'End Function

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        BindDataList(boolstatus, EmployerId)
        'BindEmployerJobsData(New EmployerJobsInfo)
        BindEmployerStatusChange(New EmployerJobsInfo)
        chkJobWorkDays.ClearSelection()
        ClearIntegerFields()

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)


    End Sub
    Private Sub ddlJobGroupId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlJobGroupId.SelectedIndexChanged
        If Not ddlJobGroupId.SelectedValue = "" Then
            BuildJobTitlesByCatagoryDDL()
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
