' ===============================================================================
'
' FAME AdvantageV1
'
' EmployeeInfo.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports FAME.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.BusinessFacade.Reports
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Domain.Users
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports User = Advantage.Business.Objects.User

Partial Class SDFModulePage
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblDocumentId As System.Web.UI.WebControls.Label
    Protected WithEvents ddlDocumentId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblDocStatusId As System.Web.UI.WebControls.Label
    Protected WithEvents ddlDocStatusId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtRequestDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtReceiveDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlScannedId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents chkScannedId As System.Web.UI.WebControls.CheckBox
    Protected WithEvents dlstDocumentStatus As System.Web.UI.WebControls.DataList
    Protected WithEvents btnApplyFilter As System.Web.UI.WebControls.Button
    Protected WithEvents lblRequestDate As System.Web.UI.WebControls.Label
    Protected WithEvents lblReceiveDate As System.Web.UI.WebControls.Label
    Protected WithEvents lblScanned As System.Web.UI.WebControls.Label
    Protected WithEvents Img1 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents Img2 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents txtStudentDocs As System.Web.UI.WebControls.TextBox
    Protected strCount As String
    Protected WithEvents lblApplyTo As System.Web.UI.WebControls.Label
    Protected WithEvents ddlApplyTo As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblSDFFilter As System.Web.UI.WebControls.Label
    Protected WithEvents ddlSDFFilter As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lstAvailablePage As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblSkillGroups As System.Web.UI.WebControls.Label
    Protected WithEvents btnAddAll As System.Web.UI.WebControls.Button
    Protected WithEvents btnRemoveAll As System.Web.UI.WebControls.Button
    Protected WithEvents lblStudentSkills As System.Web.UI.WebControls.Label
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents VSSDF As System.Web.UI.WebControls.ValidationSummary
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected StudentId As String '= "14094BD8-18CA-4B95-9195-069D47774306"
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region

#Region "page events"
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnPages)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnRemove)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '   disable History button at all time

        'pObj = Header1.UserPagePermission
        Dim userId As String
        Dim advantageUserState As User = AdvantageSession.UserState
        ResourceId = HttpContext.Current.Request.Params("resid")
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = AdvantageSession.UserState.ModuleCode.ToString
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        ' Dim objCommon As New CommonUtilities
        If Not Page.IsPostBack Then

            strCount = 0

            ViewState("MODE") = "NEW"

            'Bind The DropDownList
            BuildDDLs()

            'Get PK Value
            txtStudentDocId.Text = Guid.NewGuid.ToString()

            'Get Default Employer Data when page is loaded for first time
            'BindDataList(StudentId)
        End If
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        btnNew.Enabled = False
        btnDelete.Enabled = False
        txtStudentDocId.Visible = False
    End Sub
#End Region

#Region "build ddl"
    Private Sub BuildSdfddl()
        'Bind the Document DropDownList
        Dim SDFList As New SDFModulePageFacade
        With ddlSDFId
            .DataTextField = "SDFDescrip"
            .DataValueField = "SDFId"
            .DataSource = SDFList.GetAllSDF()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    'Private Sub BuildModulesDDL()
    '    'Bind the Document DropDownList
    '    Dim SDFList As New SDFModulePageFacade
    '    With ddlModuleId
    '        .DataTextField = "Resource"
    '        .DataValueField = "ResourceId"
    '        .DataSource = SDFList.GetAvailableModulesbyEntityId(ddlEntity.SelectedItem.Value)
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With

    '    '' ''Bind the Document DropDownList
    '    ' ''Dim SDFList As New SDFModulePageFacade
    '    ' ''With ddlModuleId
    '    ' ''    .DataTextField = "Resource"
    '    ' ''    .DataValueField = "ResourceId"
    '    ' ''    .DataSource = SDFList.GetAllModulesBySDF()
    '    ' ''    .DataBind()
    '    ' ''    .Items.Insert(0, New ListItem("Select", ""))
    '    ' ''    .SelectedIndex = 0
    '    ' ''End With
    'End Sub
    Private Sub BuildVisibilityDDL()
        ''Bind the Document DropDownList
        'Dim SDFList As New SDFModulePageFacade
        'With lstVisibility
        '    .DataTextField = "SDFVisibility"
        '    .DataValueField = "SDFVisibility"
        '    .DataSource = SDFList.GetAllVisibilityByModSDF(ddlSDFId.SelectedValue, ddlSDFId.SelectedItem.Text, 26)
        '    .DataBind()
        'End With
    End Sub
    'Private Sub BuildPagesDDL()
    '    'Bind the Document DropDownList
    '    Dim strPage As String
    '    strPage = "%" & "/" & Trim(ddlModuleId.SelectedValue) & "/" & "%"
    '    Dim SDFList As New SDFModulePageFacade
    '    With lstAvailablePages
    '        .DataSource = SDFList.GetAllPages(strPage)
    '        .DataTextField = "Resource"
    '        .DataValueField = "ResourceID"
    '        .DataBind()
    '    End With
    'End Sub
    Private Sub BuildEntitiesDDL()
        RPTCommon.BuildReportTypesDDL(RPTCommon.GetCurrentUserId(), ddlEntity)
    End Sub
    Private Sub BuildDDLs()
        BuildSdfddl()
        BuildEntitiesDDL()
    End Sub
    Private Sub BuildPages()
        Dim sErrorMessage As String = ""
        lstAvailablePages.Items.Clear()
        lstSelectedPages.Items.Clear()
        lstVisibility.Items.Clear()
        If Not sErrorMessage = "" Then
            DisplayErrorMessage(sErrorMessage)
            Exit Sub
        End If
        BuildFilterAvailablePages()
        BuildFilterSelectedPages()
        BuildVisibilityDDL()
    End Sub

    Protected Sub ddlModuleId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlModuleId.SelectedIndexChanged
        'Dim sErrorMessage As String = ""
        'lstAvailablePages.Items.Clear()
        'lstSelectedPages.Items.Clear()
        'lstVisibility.Items.Clear()
        'If ddlSDFId.SelectedItem.Text = "Select" Then
        '    sErrorMessage = "Please select a value from the User Defined Field dropdown"
        'End If
        'If ddlModuleId.SelectedItem.Text = "Select" Then
        '    sErrorMessage = "Please select a value from the ModuleID dropdown"
        'End If
        'If Not sErrorMessage = "" Then
        '    DisplayErrorMessage(sErrorMessage)
        '    Exit Sub
        'End If
        'BuildFilterAvailablePages()
        'BuildFilterSelectedPages()
        'BuildVisibilityDDL()
    End Sub
    'Protected Sub ddlEntity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlEntity.SelectedIndexChanged
    '    If ddlEntity.SelectedIndex <> 0 Then
    '        lstAvailablePages.Items.Clear()
    '        lstSelectedPages.Items.Clear()
    '        lstVisibility.Items.Clear()
    '        ddlModuleId.Enabled = True
    '        'BuildModulesDDL()
    '        BuildPages()
    '    Else
    '        ddlModuleId.Enabled = False
    '        ddlModuleId.Items.Clear()
    '    End If
    '    If ddlEntity.SelectedIndex = 0 Then
    '        lstAvailablePages.Items.Clear()
    '        lstSelectedPages.Items.Clear()
    '        lstVisibility.Items.Clear()
    '    End If
    'End Sub


#End Region

#Region "SERVICE functions"
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        ' TestDisplayErrorInMessageBox(Me.Page, errorMessage, strFileType)
    End Sub
    Private Shared Function ReplaceSpecialCharactersInJavascriptMessage(ByVal s As String) As String

        '   replace \ by \\, ' by \', " by "", CR by \r, LF by \n
        Return s.Trim().Replace("\", "\\").Replace("'", "\'").Replace("""", "\""").Replace(vbCr, "\r").Replace(vbLf, "\n")

    End Function
#End Region

#Region "button clicks"
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim StudentDocs As New SDFModulePageFacade
        Dim result As Integer
        strCount = 0

        'If Not ddlModuleId.SelectedValue = "" And Not ddlSDFId.SelectedValue = "" Then
        'NOTE: The module Id was eliminated it is not more used!!!!!!!!!
        If Not ddlSDFId.SelectedValue = "" Then
            ChkIsInDB.Checked = True
           
            UpdateModules(ddlSDFId.SelectedValue, 26)
            If result < 0 Then
                Customvalidator1.ErrorMessage = "A Problem Occurred Updating The Database"
                Customvalidator1.IsValid = False
            Else
                DisplayErrorMessage("Changes saved successfully")
            End If
        End If
    End Sub
    Private Sub UpdateModules(ByVal SDFID As String, ByVal ModuleId As String)
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), lstSelectedPages.Items.Count)
        Dim selectedVisibilty() As String = Array.CreateInstance(GetType(String), lstSelectedPages.Items.Count)
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim item As ListItem
        Dim intOccurance As String

        'In For Loop Check The Number of Items Selected
        For Each item In lstSelectedPages.Items
            selectedDegrees.SetValue(item.Value.ToString, i)
            i += 1
        Next

        For Each item In lstSelectedPages.Items
            intOccurance = InStr(item.Text.ToString, "-") + 2
            selectedVisibilty.SetValue(Mid(item.Text.ToString, intOccurance), j)
            j += 1
        Next

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)
        If j > 0 Then ReDim Preserve selectedVisibilty(j - 1)

        'If lstSelectedPages.Items.Count < 1 Then
        '    DisplayErrorMessage("No pages available in apply to pages - visibility list")
        '    Exit Sub
        'End If
        'update Selected Jobs
        Dim SDFModule As New SDFModulePageFacade
        Dim strReturn As Integer
        'strReturn = SDFModule.UpdateSDFModule(SDFID, ModuleId, AdvantageSession.UserState.UserName, selectedDegrees, selectedVisibilty, ddlEntity.SelectedValue)
        If strReturn = 1 Then
            If (Not ClientScript.IsStartupScriptRegistered("refershSession")) Then
                Page.ClientScript.RegisterStartupScript(Me.GetType(), "refershSession", "refreshLeadInfoSession();", True)
            End If
            DisplayErrorMessage("The udf pages saved successfully")
            Exit Sub
        End If
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'Reset The Value of chkIsInDb Checkbox
        'To Identify an Insert
        strCount = 0

        ChkIsInDB.Checked = False

        'Create a Empty Object and Initialize the Object
        BindEmploymentInfo(New StudentDocsInfo)

        txtStudentDocId.Text = Guid.NewGuid.ToString()

        BuildDDLs()

        'Initialize Buttons
        btnNew.Enabled = False
        btnDelete.Enabled = False

    End Sub
    Private Sub BtnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim strSelectedText As String
        Dim strSelectedValue As String
        Dim strVisibiltyText As String
        Dim strVisibiltyValue As String
        Dim strPage_Visibilty As String

        If lstAvailablePages.SelectedIndex = -1 Then
            'DisplayErrorMessage("Please select an item from the available List")
            RadNotification1.Show("Please select an item from the available pages list")
            Exit Sub
        End If

        If lstVisibility.SelectedIndex = -1 Then
            RadNotification1.Show("Please select an item from the udf visibility list")
            Exit Sub
        End If

        strSelectedText = lstAvailablePages.SelectedItem.Text
        strSelectedValue = lstAvailablePages.SelectedItem.Value

        strVisibiltyText = lstVisibility.SelectedItem.Text
        strVisibiltyValue = lstVisibility.SelectedItem.Value

        Dim i, count As Integer
        If lstSelectedPages.Items.Count >= 1 Then
            For i = 0 To lstSelectedPages.Items.Count - 1
                If lstSelectedPages.Items(i).Text = strSelectedText Then
                    count = 1
                End If
            Next
            If Not (count = 1) Then
                strPage_Visibilty = strSelectedText & " - " & strVisibiltyText
                lstAvailablePages.Items.RemoveAt(lstAvailablePages.SelectedIndex)
                lstSelectedPages.Items.Add(New ListItem(strPage_Visibilty, strSelectedValue))
            End If
        Else
            strPage_Visibilty = strSelectedText & " - " & strVisibiltyText
            lstSelectedPages.Items.Add(New ListItem(strPage_Visibilty, strSelectedValue))
            lstAvailablePages.Items.RemoveAt(lstAvailablePages.SelectedIndex)
        End If
        lstVisibility.SelectedIndex = -1
        btnAdd.CausesValidation = True
    End Sub
    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Dim StrSelectedText As String
        Dim strSelectedValue As String
        Dim instrValue As String

        btnAdd.CausesValidation = False
        'lblError.Visible = False
        If lstSelectedPages.Items.Count = 0 Then
            RadNotification1.Show("Unable to find data for removal")
            Exit Sub
        End If
        If lstSelectedPages.SelectedIndex = -1 And lstSelectedPages.Items.Count >= 1 Then
            RadNotification1.Show("Please select an item from Apply To Pages list")
            Exit Sub
        Else
            instrValue = InStr(lstSelectedPages.SelectedItem.Text, "-") - 1
            If instrValue >= 1 Then
                StrSelectedText = Mid(lstSelectedPages.SelectedItem.Text, 1, instrValue)
                strSelectedValue = lstSelectedPages.SelectedItem.Value
                lstAvailablePages.Items.Add(New ListItem(StrSelectedText, strSelectedValue))
            End If
            lstSelectedPages.Items.RemoveAt(lstSelectedPages.SelectedIndex)
        End If
    End Sub
    Protected Sub btnPages_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPages.Click
        'Validate

        If ddlEntity.SelectedIndex <> 0 Then
            lstAvailablePages.Items.Clear()
            lstSelectedPages.Items.Clear()
            lstVisibility.Items.Clear()
            ddlModuleId.Enabled = True
            BuildPages()
        Else
            ddlModuleId.Enabled = False
            ddlModuleId.Items.Clear()
        End If
        If ddlEntity.SelectedIndex = 0 Then
            lstAvailablePages.Items.Clear()
            lstSelectedPages.Items.Clear()
            lstVisibility.Items.Clear()
        End If
    End Sub
#End Region

#Region "LIST BUILDING"
    Private Sub BindStudentInfoExist(ByVal StudentDocsInfo As StudentDocsInfo)
        'Bind The StudentInfo Data From The Database
        With StudentDocsInfo
            ddlModuleId.SelectedValue = .ModuleName
            ddlApplyTo.SelectedValue = .Pages
            ddlSDFId.SelectedValue = .SDF
        End With
    End Sub
    Private Sub BindEmploymentInfo(ByVal StudentDocs As StudentDocsInfo)
        BuildDDLs()
        btnNew.Enabled = False
    End Sub
    Private Sub BuildFilterAvailablePages()
        Dim ModulePage As New SDFModulePageFacade
        ddlModuleId.SelectedValue = 26 'Just dummy data not used anywhere in SQL Statement
        With lstAvailablePages
            .DataTextField = "Resource"
            .DataValueField = "ResourceID"
            '.DataSource = ModulePage.GetAvailableUDFPagesByResource(ddlSDFId.SelectedValue, 26, ddlEntity.SelectedValue) 'ModulePage.GetAllResourceNotSelected(ddlSDFId.SelectedValue, ddlModuleId.SelectedValue, strURL)
            .DataBind()
        End With
    End Sub
    Private Sub BuildFilterSelectedPages()
        Dim ModulePage As New SDFModulePageFacade
        ddlModuleId.SelectedValue = 26 'Just dummy data not used anywhere in SQL Statement
        With lstSelectedPages
            .DataTextField = "Resource"
            .DataValueField = "ResourceID"
            '.DataSource = ModulePage.GetAllResourceSelected(ddlSDFId.SelectedValue, 26, ddlEntity.SelectedValue)
            .DataBind()
        End With
    End Sub
    'Private Sub BuildVisibilityList()
    '    Dim SDFList As New SDFModulePageFacade
    '    With lstVisibility
    '        .DataSource = SDFList.GetAllSDFVisibilty()
    '        .DataTextField = "SDFVisibility"
    '        .DataValueField = "VisID"
    '        .DataBind()
    '    End With
    'End Sub

#End Region


End Class
