
Imports Fame.AdvantageV1.BusinessFacade
Partial Class CurrentSkills
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblQualifications As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim studentId As String = Request.QueryString("studentid")
        BuildSkills(studentId)
        Dim facade As New DocumentManagementFacade
        lblStudentName.Text = facade.GetStudentNamesById(studentId)
    End Sub
    Private Sub BuildSkills(ByVal StudentId As String)
        Dim output As New PlacementFacade
        lblOutput.Text = output.SkillGroup(StudentId)
        If lblOutput.Text = "None" Then
            lblOutput.Text = ""
        End If
    End Sub
End Class
