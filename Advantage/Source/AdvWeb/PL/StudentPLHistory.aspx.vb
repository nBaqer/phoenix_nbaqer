Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports BO = Advantage.Business.Objects
Partial Class StudentPLHistory
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtComments As System.Web.UI.WebControls.TextBox
    Protected WithEvents ddlEnrollmentId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblEnrollmentId As System.Web.UI.WebControls.Label

    'For Test Purpose
    Protected StudentId As String '= "DCAF913F-F4B6-4ED3-B8E1-46C9CDFEAB69"
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblPrgVerId As System.Web.UI.WebControls.Label
    Protected WithEvents ddlPrgVerId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtPlacementRep As System.Web.UI.WebControls.TextBox
#End Region

    Private pObj As New UserPagePermissionInfo
    Dim campusId As String
    Dim userId As String
    Protected ModuleId As String
    Protected resourceId As Integer
    Protected sdfControls As New SDFComponent
    Protected LeadId As String
    Protected state As AdvantageSessionState

    Protected boolSwitchCampus As Boolean = False


    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        'AdvantageSession.PageTheme = PageTheme.Blue_Theme
        InitializeComponent()

    End Sub

    'MERGEFIX - copied code from duplicate method below into original method above and commented out duplicate method
    'Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
    '    'CODEGEN: This method call is required by the Web Form Designer
    '    'Do not modify it using the code editor.
    '    InitializeComponent()

    'End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)



        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim objCommon As New CommonUtilities

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString 'XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New StudentMRU
        objStudentState = getStudentFromStateObject(95) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        Dim advantageUserState As User = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)


        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            If Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value) Then
                ClearControls()
            End If

            InitButtonsForLoad()

            'objCommon.PageSetup(Form1, "NEW")
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

            'Build All DropDownList
            BuildAllDDLs()

            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"

            'Bind The DataList and Default To All
            BindDataList(StudentId, "All")

            'Generate The Value For Primary Key
            txtPlacementId.Text = Guid.NewGuid.ToString()

            'Load The Buttons and Disable New,Delete Buttons
            InitButtonsForLoad()

            'Hide The JobTitleId DropDownList on Page Load
            ddlJobTitleId.Visible = True

            'Disable JobTitleId
            ddlJobTitleId.Enabled = False

            'Get FirstName,LastName,MiddleName from arStudent Table
            'Based On StudentId
            GetNameByStudentId(StudentId)
            'SetRequiredBackColor()

            '   disable History button the first time
            'Header1.EnableHistoryButton(False)
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, StudentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
            'End If

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""
        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
        End If

        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfControls.GetSDFExists(resourceId, ModuleId)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtPlacementId.Text) <> "" Then
            sdfControls.GenerateControlsEdit(pnlSDF, resourceId, txtPlacementId.Text, ModuleId)
        Else
            sdfControls.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
        End If
    End Sub

    Private Sub ClearControls()
        ddlEmployerId.Items.Clear()
        ddlJobTitleId.Items.Clear()
        txtJobDescrip.Text = ""
        ddlPlacementRep.Items.Clear()
        ddlJobStatusId.Items.Clear()
        ddlInterViewId.Items.Clear()
        txtSupervisor.Text = ""
        ddlFldStudyId.Items.Clear()
        ddlStuEnrollId.Items.Clear()
        txtNotes.Text = ""
        txtSalary.Text = ""
        ddlSalaryTypeId.Items.Clear()
        ddlScheduleId.Items.Clear()
        ddlBenefitsId.Items.Clear()
        ddlHowPlacedId.Items.Clear()
        txtPlacedDate.SelectedDate = Nothing
        txtStartDate.SelectedDate = Nothing
        txtTerminationDate.SelectedDate = Nothing
        txtTerminationReason.Text = ""


    End Sub
    'Private Sub SetRequiredBackColor()
    '    'Get The RequiredField Value
    '    Dim strSalaryReq As String
    '    Dim objCommon As New CommonUtilities
    '    strSalaryReq = objCommon.SetRequiredColorMask("Salary")

    '    'If The Field Is Required Field Then Color The Masked
    '    'Edit Control
    '    If strSalaryReq = "Yes" Then
    '        txtSalary.BackColor = Color.FromName("#ffff99")
    '    End If
    'End Sub
    'Private Sub BuildEmployerDDL()
    '    'Bind the Employers DropDownList
    '    Dim EmployerList As New PlacementFacade
    '    With ddlEmployerId
    '        .DataTextField = "EmployerDescrip"
    '        .DataValueField = "EmployerId"
    '        .DataSource = EmployerList.GetAllEmployers()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildJobTitle(ByVal EmployerId As String)
        'Bind the JobTitleId DropDownList Based On Employer
        If Not EmployerId = "" Or Not EmployerId = Guid.Empty.ToString Then
            Dim JobTitle As New PlacementFacade
            With ddlJobTitleId
                .DataTextField = "CodeDescrip"
                .DataValueField = "EmployerJobId"
                .DataSource = JobTitle.GetJobTitleByEmployer(EmployerId)
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            ddlJobTitleId.Items.Insert(0, New ListItem("Select", ""))
            ddlJobTitleId.SelectedIndex = 0
        End If
    End Sub
    'Private Sub BuildEmployerFeeDDL()
    '    'Bind the Employer Fee DrowDownList
    '    Dim EmployerFee As New EmployerFeeFacade
    '    With ddlFee
    '        .DataTextField = "FeeDescrip"
    '        .DataValueField = "FeeId"
    '        .DataSource = EmployerFee.GetAllEmployerFee()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildJobStatusDDL()
    '    'Bind the JobStatus DropDownlist
    '    Dim JobStatus As New PlacementFacade
    '    With ddlJobStatusId
    '        .DataTextField = "JobStatusDescrip"
    '        .DataValueField = "JobStatusId"
    '        .DataSource = JobStatus.GetAllJobStatus()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildSalaryTypeDDL()
    '    'Bind The Salary Type DDL
    '    Dim SalaryTypeId As New PlacementFacade
    '    With ddlSalaryTypeId
    '        .DataTextField = "SalaryTypeDescrip"
    '        .DataValueField = "SalaryTypeId"
    '        .DataSource = SalaryTypeId.GetAllSalaryType()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildJobBenefitDDL()
    '    'Bind the Employer Benefits DrowDownList
    '    Dim JobTitle As New EmployerJobsFacade
    '    With ddlBenefitsId
    '        .DataTextField = "JobBenefitDescrip"
    '        .DataValueField = "JobBenefitId"
    '        .DataSource = JobTitle.GetAllJobBenefit
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildJobScheduleDDL()
    '    'Bind the EmployerSchedule DrowDownList
    '    Dim JobTitle As New EmployerJobsFacade
    '    With ddlScheduleId
    '        .DataTextField = "JobScheduleDescrip"
    '        .DataValueField = "JobScheduleId"
    '        .DataSource = JobTitle.GetAllJobSchedule
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildFieldStudyDDL()
    '    'Bind the Employer Field Of Study DrowDownList
    '    Dim JobTitle As New PlacementFacade
    '    With ddlFldStudyId
    '        .DataTextField = "FldStudyDescrip"
    '        .DataValueField = "FldStudyId"
    '        .DataSource = JobTitle.GetAllFieldStudy()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildInterviewDDL()
    '    'Bind the Employer InterView DrowDownList
    '    Dim Interview As New PlacementFacade
    '    With ddlInterViewId
    '        .DataTextField = "InterviewDescrip"
    '        .DataValueField = "InterviewId"
    '        .DataSource = Interview.GetAllInterview()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    'Private Sub BuildHowPlacedDDL()
    '    'Bind the Employer InterView DrowDownList
    '    Dim HowPlaced As New PlacementFacade
    '    With ddlHowPlacedId
    '        .DataTextField = "HowPlacedDescrip"
    '        .DataValueField = "HowPlacedId"
    '        .DataSource = HowPlaced.GetHowPlaced()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildEnrollmentDDL()
        'Bind The Enrollment DDL
        Dim studentEnrollments As New StudentsAccountsFacade
        With ddlStuEnrollId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            '            .DataValueField = "PrgVerId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(StudentId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub ddlEmployerId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEmployerId.SelectedIndexChanged
        If Not ddlEmployerId.SelectedValue = Guid.Empty.ToString And Not ddlEmployerId.SelectedValue = "" Then
            ddlJobTitleId.Enabled = True
            BuildJobTitle(ddlEmployerId.SelectedValue)
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim PlacementHistory As New PlacementFacade
        Dim Result As String

        Dim PlacementDate As String = PlacementHistory.GetStartDate(StudentId, ddlStuEnrollId.SelectedValue)

        If Not PlacementDate = "" And Not txtStartDate.SelectedDate Is Nothing Then
            If DateDiff(DateInterval.Day, CDate(txtStartDate.SelectedDate), CDate(PlacementDate)) > 1 Then
                DisplayErrorMessage(" The start date should not be earlier than the enrollment start date " & "(" & CDate(PlacementDate) & ") ")
                Exit Sub
            End If
        End If

        ''Call Update Function in the plEmployerInfoFacade
        Result = PlacementHistory.UpdateStudentPlHistory(BuildPlacementHistory(StudentId), Session("UserName"))

        '  If DML is not successful then Prompt Error Message
        If Not Result = "" Then
            '   Display Error Message
            DisplayErrorMessage(Result)
            Exit Sub
        End If

        'Reset The Checked Property Of CheckBox To True
        chkIsInDB.Checked = True

        'If Page is free of errors 
        If Page.IsValid Then
            'Initialize New,Delete Buttons
            InitButtonsForEdit()
        End If
        'EnableBtnStudentType()
        BindDataList(StudentId, ddlEnrollmentTypeId.SelectedValue)

        'set Style to Selected Item
        'MERGEFIX
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, txtPlacementId.Text, ViewState, Header1)
        CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, txtPlacementId.Text, ViewState)

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim SDFID As ArrayList
        Dim SDFIDValue As ArrayList
        '        Dim newArr As ArrayList
        Dim z As Integer
        Dim SDFControl As New SDFComponent
        Try
            SDFControl.DeleteSDFValue(txtPlacementId.Text)
            SDFID = SDFControl.GetAllLabels(pnlSDF)
            SDFIDValue = SDFControl.GetAllValues(pnlSDF)
            For z = 0 To SDFID.Count - 1
                SDFControl.InsertValues(txtPlacementId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
            Next
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try

        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, txtPlacementId.Text)

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub BuildPlacementRepsDDL()
        Dim LeadAdmissionReps As New LeadFacade
        With ddlPlacementRep
            .DataTextField = "fullname"
            .DataValueField = "userid"
            .DataSource = LeadAdmissionReps.GetAllPlacementRepsByCampusAndUserId(campusId, userId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Function BuildPlacementHistory(ByVal StudentId As String) As PlacementInfo
        Dim PlacementData As New PlacementInfo

        With PlacementData
            ''get IsInDB
            .IsInDb = chkIsInDB.Checked

            'Get EmployerId
            .EmployerId = ddlEmployerId.SelectedValue

            'Get JobTitleId
            .EmployerJobTitle = ddlJobTitleId.SelectedValue

            'Get Job Description
            .Description = txtJobDescrip.Text

            'Get Placement Rep
            .PlacementRep = ddlPlacementRep.SelectedValue

            'Get JobStatusId
            .JobStatus = ddlJobStatusId.SelectedValue

            'Get Supervisor
            .Supervisor = txtSupervisor.Text

            'Get Fee
            .FeeId = ddlFee.SelectedValue

            'Get Comments
            .Comments = txtNotes.Text

            'Get Salary
            If InStr(txtSalary.Text, ",") >= 1 Then
                .Salary = Replace(txtSalary.Text, ",", "")
            Else
                .Salary = txtSalary.Text
            End If

            'Get SalaryTypeId
            .SalaryTypeId = ddlSalaryTypeId.SelectedValue

            'Schedule
            .ScheduleId = ddlScheduleId.SelectedValue

            'Benefits Id
            .BenefitsId = ddlBenefitsId.SelectedValue


            'Start
            If Not (txtStartDate.SelectedDate Is Nothing) Then
                .StartDate = txtStartDate.SelectedDate
            Else
                .StartDate = Nothing
            End If

            'End Date
            .EndDate = txtTerminationDate.DbSelectedDate

            'Interview Id
            .InterviewId = ddlInterViewId.SelectedValue

            'Field Of Study
            .FldStudyId = ddlFldStudyId.SelectedValue

            'Termination Reason
            .TerminationReason = txtTerminationReason.Text

            'StudentId
            .StudentId = StudentId

            'PlacedDate
            If Not (txtPlacedDate.SelectedDate Is Nothing) Then
                .PlacedDate = txtPlacedDate.SelectedDate
            Else
                .PlacedDate = Nothing
            End If

            'Enrollment Id
            .EnrollmentId = ddlStuEnrollId.SelectedValue

            'How Placed
            .HowPlaced = ddlHowPlacedId.SelectedValue

            'PlacementId
            .PlacementId = txtPlacementId.Text

            'firstname
            .firstname = txtfirstname.Text

            'middlename
            .MiddleName = txtmiddlename.Text

            'lastname
            .LastName = txtlastname.Text
        End With
        Return PlacementData
    End Function
    Private Sub BuildEnrollmentDataListDDL()
        Dim studentEnrollments As New StudentsAccountsFacade
        With ddlEnrollmentTypeId
            .DataTextField = "PrgVerDescrip"
            '.DataValueField = "PrgVerId"
            .DataValueField = "StuEnrollId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(StudentId)
            .DataBind()
            .Items.Insert(0, New ListItem("All", "All"))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub ddlEnrollmentTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEnrollmentTypeId.SelectedIndexChanged
        BindDataList(StudentId, ddlEnrollmentTypeId.SelectedValue)

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, resourceId, ModuleId)


    End Sub
    Private Sub BindDataList(ByVal StudentId As String, ByVal EnrollmentId As String)
        With New PlacementFacade
            dlstEmployerContact.DataSource = .GetEmployerNamesByEnrollment(StudentId, EnrollmentId)
            dlstEmployerContact.DataBind()
        End With
    End Sub
    Private Sub GetPlacementDetails(ByVal PlacementId As String)
        Dim placementhistory As New PlacementFacade
        BindPlacementDatas(placementhistory.GetPlacementDetails(PlacementId), PlacementId)
    End Sub
    'Private Sub BindPlacementData(ByVal PlacementDataInfo As PlacementInfo, ByVal PlacementId As String)

    '    'Bind The EmployerInfo Data From The Database
    '    With PlacementDataInfo

    '        'Get EmployerId
    '        ddlEmployerId.SelectedValue = .EmployerId

    '        BuildJobTitle(ddlEmployerId.SelectedValue)

    '        'Get Check Status
    '        chkIsInDB.Checked = .IsInDb

    '        'Get Description
    '        txtJobDescrip.Text = .Description

    '        'Get Fee
    '        ddlFee.SelectedValue = .FeeId

    '        'Get Job Title
    '        ddlJobTitleId.SelectedValue = .EmployerJobTitle

    '        'BenefitsId
    '        If Mid(.BenefitsId, 1, 3) = "000" Then
    '            BuildJobBenefitDDL()
    '        Else
    '            ddlBenefitsId.SelectedValue = .BenefitsId
    '        End If

    '        'ScheduleId
    '        ddlScheduleId.SelectedValue = .ScheduleId

    '        'Start
    '        txtStartDate.SelectedDate = .StartDate

    '        'Salary From
    '        txtSalary.Text = .Salary

    '        'Salary TypeId
    '        ddlSalaryTypeId.SelectedValue = .SalaryTypeId

    '        'Placement Rep
    '        ddlPlacementRep.SelectedValue = .PlacementRep

    '        'JobStatus Id
    '        ddlJobStatusId.SelectedValue = .JobStatus

    '        'Supervisor
    '        txtSupervisor.Text = .Supervisor

    '        'Placeddate
    '        txtPlacedDate.SelectedDate = .PlacedDate

    '        'Comments
    '        txtNotes.Text = .Comments

    '        'Interviewid
    '        ddlInterViewId.SelectedValue = .InterviewId

    '        'FldStudy
    '        ddlFldStudyId.SelectedValue = .FldStudyId

    '        'Termination Date
    '        txtTerminationDate.SelectedDate = .AvailableDate

    '        'Termination Reason
    '        txtTerminationReason.Text = .TerminationReason

    '        'Enrollment Id
    '        ddlStuEnrollId.SelectedValue = .EnrollmentId

    '        'FirstName
    '        txtfirstname.Text = .firstname

    '        'LastName
    '        txtlastname.Text = .LastName

    '        'MiddleName
    '        txtmiddlename.Text = .MiddleName

    '    End With
    'End Sub
    Private Sub BindPlacementDatas(ByVal PlacementDataInfo As PlacementInfo, ByVal PlacementId As String)

        'Bind The EmployerInfo Data From The Database
        With PlacementDataInfo

            'Get EmployerId
            'ddlEmployerId.SelectedValue = .EmployerId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlEmployerId, .EmployerId, .EmployerName)

            BuildJobTitle(ddlEmployerId.SelectedValue)

            'Get Check Status
            chkIsInDB.Checked = .IsInDb

            'Get Description
            txtJobDescrip.Text = .Description

            'Get Fee
            'ddlFee.SelectedValue = .FeeId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFee, .FeeId, .Fee)


            'Get Job Title
            '            Try
            'ddlJobTitleId.SelectedValue = .EmployerJobTitleId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlJobTitleId, .EmployerJobTitleId, .EmployerJobTitle)
            '           Catch ex As System.Exception
            '           	Dim exTracker = new AdvApplicationInsightsInitializer()
            '           	exTracker.TrackExceptionWrapper(ex)

            '          BuildJobTitle(ddlEmployerId.SelectedValue)
            '         End Try


            'BenefitsId
            'ddlBenefitsId.SelectedValue = .BenefitsId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlBenefitsId, .BenefitsId, .Benefits)

            'ScheduleId
            'ddlScheduleId.SelectedValue = .ScheduleId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlScheduleId, .ScheduleId, .Schedule)

            'Start
            Try
                If Not (.StartDate = "") Then
                    txtStartDate.SelectedDate = .StartDate
                Else
                    txtStartDate.SelectedDate = Nothing
                End If

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                txtStartDate.SelectedDate = Nothing
            End Try


            'Salary From
            txtSalary.Text = .Salary

            'Salary TypeId
            'ddlSalaryTypeId.SelectedValue = .SalaryTypeId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSalaryTypeId, .SalaryTypeId, .SalaryType)

            'Placement Rep
            If FindByValue(ddlPlacementRep, .PlacementRep) = True Then
                ddlPlacementRep.SelectedValue = .PlacementRep
            End If

            'JobStatus Id
            'ddlJobStatusId.SelectedValue = .JobStatusId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlJobStatusId, .JobStatusId, .JobStatus)

            'Supervisor
            txtSupervisor.Text = .Supervisor

            'Placeddate
            Try
                If Not (.PlacedDate = "") Then
                    txtPlacedDate.SelectedDate = .PlacedDate
                Else
                    txtPlacedDate.SelectedDate = Nothing
                End If
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                txtPlacedDate.SelectedDate = Nothing
            End Try


            'Comments
            txtNotes.Text = .Comments

            'Interviewid
            'ddlInterViewId.SelectedValue = .InterviewId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlInterViewId, .InterviewId, .Interview)

            'FldStudy
            'ddlFldStudyId.SelectedValue = .FldStudyId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFldStudyId, .FldStudyId, .FldStudy)

            'Termination Date
            Try
                txtTerminationDate.SelectedDate = .AvailableDate
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                txtTerminationDate.SelectedDate = Nothing
            End Try


            'Termination Reason
            txtTerminationReason.Text = .TerminationReason

            'Enrollment Id
            ddlStuEnrollId.SelectedValue = .EnrollmentId

            'HowPlaced
            'ddlHowPlacedId.SelectedValue = .HowPlaced
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlHowPlacedId, .HowPlacedId, .HowPlaced)

            'PlacementId
            txtPlacementId.Text = PlacementId
        End With
    End Sub
    Private Function FindByValue(ByVal ddl As DropDownList, ByVal selValue As String) As Boolean
        Dim rtn As Boolean = False
        Dim i As Integer = 0
        For i = 0 To ddl.Items.Count - 1
            If ddl.Items(i).Value = selValue Then
                Return True
            End If
        Next
        Return False
    End Function
    Private Sub dlstEmployerContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstEmployerContact.ItemCommand
        'Get Placement Information Based On PlacementId(PK)
        'Show The JobTitleId DDL
        ddlJobTitleId.Enabled = True
        GetPlacementDetails(e.CommandArgument)
        InitButtonsForEdit()

        'set Style to Selected Item
        'MERGEFIX
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, e.CommandArgument, ViewState, Header1)
        CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, e.CommandArgument, ViewState)


        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEdit(pnlSDF, resourceId, e.CommandArgument, ModuleId)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, e.CommandArgument)

    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click

        'Assign A New Value For Primary Key Column
        txtPlacementId.Text = Guid.NewGuid.ToString()

        BindNew(New PlacementInfo)

        'Reset The Value of chkIsInDb Checkbox To Identify an Insert
        chkIsInDB.Checked = False

        'Disable New,Delete Buttons
        InitButtonsForLoad()

        'set Style to Selected Item
        'MERGEFIX
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, System.Guid.Empty.ToString, ViewState, Header1)
        CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, System.Guid.Empty.ToString, ViewState)

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, resourceId, ModuleId)

        ' US3037 5/4/2012 Janet Robinson
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)

    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub BuildAllDDLs()
        'BuildEmployerDDL()
        'BuildJobStatusDDL()
        'BuildEmployerFeeDDL()
        'BuildSalaryTypeDDL()
        'BuildJobScheduleDDL()
        'BuildJobBenefitDDL()
        'BuildFieldStudyDDL()
        'BuildInterviewDDL()
        BuildEnrollmentDDL()
        BuildEnrollmentDataListDDL()
        'BuildHowPlacedDDL()
        BuildPlacementRepsDDL()
        chkIsInDB.Checked = False

        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Employers DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlEmployerId, AdvantageDropDownListName.Employers, campusId, True, True))

        'JobStatusDDL
        ddlList.Add(New AdvantageDDLDefinition(ddlJobStatusId, AdvantageDropDownListName.JobStatus, campusId, True, True))

        'EmployerFee 
        ddlList.Add(New AdvantageDDLDefinition(ddlFee, AdvantageDropDownListName.Fees, campusId, True, True))

        'SalaryType
        ddlList.Add(New AdvantageDDLDefinition(ddlSalaryTypeId, AdvantageDropDownListName.SalaryType, campusId, True, True, String.Empty))

        'JobSchedule
        ddlList.Add(New AdvantageDDLDefinition(ddlScheduleId, AdvantageDropDownListName.JobSchedule, Nothing, True, True, String.Empty))

        'Benefits
        ddlList.Add(New AdvantageDDLDefinition(ddlBenefitsId, AdvantageDropDownListName.Benefits, Nothing, True, True))

        'FieldStudy
        ddlList.Add(New AdvantageDDLDefinition(ddlFldStudyId, AdvantageDropDownListName.FieldStudy, campusId, True, True, String.Empty))

        'Interview
        ddlList.Add(New AdvantageDDLDefinition(ddlInterViewId, AdvantageDropDownListName.Interview, campusId, True, True, String.Empty))

        'How Placed
        ddlList.Add(New AdvantageDDLDefinition(ddlHowPlacedId, AdvantageDropDownListName.PlacedBy, campusId, True, True, String.Empty))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub
    'Private Sub BindPlacementData(ByVal PlacementDataInfo As PlacementInfo)
    '    'Bind The EmployerInfo Data From The Database
    '    With PlacementDataInfo

    '        'Get EmployerId
    '        ddlEmployerId.SelectedValue = .EmployerId


    '        'Get Check Status
    '        chkIsInDB.Checked = .IsInDb

    '        'Get Description
    '        txtJobDescrip.Text = .Description

    '        'Get Fee
    '        ddlFee.SelectedValue = .FeeId

    '        'Get Job Title
    '        ddlJobTitleId.SelectedValue = .JobTitleId

    '        'BenefitsId
    '        If ddlBenefitsId.SelectedValue = "NULL" Then
    '            BuildJobBenefitDDL()
    '        Else
    '            ddlBenefitsId.SelectedValue = .BenefitsId
    '        End If

    '        'ScheduleId
    '        ddlScheduleId.SelectedValue = .ScheduleId

    '        'Start
    '        txtStartDate.SelectedDate = .StartDate

    '        'Salary From
    '        txtSalary.Text = .Salary

    '        'Salary TypeId
    '        ddlSalaryTypeId.SelectedValue = .SalaryTypeId

    '        'Placement Rep
    '        ddlPlacementRep.SelectedValue = .PlacementRep

    '        'JobStatus Id
    '        ddlJobStatusId.SelectedValue = .JobStatus

    '        'Supervisor
    '        txtSupervisor.Text = .Supervisor

    '        'Placeddate
    '        txtPlacedDate.SelectedDate = .PlacedDate

    '        'Comments
    '        txtNotes.Text = .Comments

    '        'Interviewid
    '        ddlInterViewId.SelectedValue = .InterviewId

    '        'FldStudy
    '        ddlFldStudyId.SelectedValue = .FldStudyId

    '        'Termination Date
    '        txtTerminationDate.SelectedDate = .EndDate

    '        'Termination Reason
    '        txtTerminationReason.Text = .TerminationReason

    '        'Enrollment Id
    '        ddlStuEnrollId.SelectedValue = .EnrollmentId

    '    End With
    'End Sub
    Private Sub BindNew(ByVal PlacementDataInfo As PlacementInfo)
        'Bind The EmployerInfo Data From The Database
        With PlacementDataInfo

            'Get Description
            txtJobDescrip.Text = .Description

            'Start
            Try
                If Not (.StartDate = "") Then
                    txtStartDate.SelectedDate = .StartDate
                Else
                    txtStartDate.SelectedDate = Nothing
                End If
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                txtStartDate.SelectedDate = Nothing
            End Try


            'Salary From
            txtSalary.Text = .Salary

            'Placement Rep
            'ddlPlacementRep.SelectedValue = .PlacementRep

            'Supervisor
            txtSupervisor.Text = .Supervisor

            'Placeddate
            Try
                If Not (.PlacedDate = "") Then
                    txtPlacedDate.SelectedDate = .PlacedDate
                Else
                    txtPlacedDate.SelectedDate = Nothing
                End If
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                txtPlacedDate.SelectedDate = Nothing
            End Try


            'Comments
            txtNotes.Text = .Comments

            'Termination Date
            Try
                txtTerminationDate.SelectedDate = .EndDate
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                txtTerminationDate.SelectedDate = Nothing
            End Try


            'Termination Reason
            txtTerminationReason.Text = .TerminationReason

            'Disable JobTitleId
            ddlJobTitleId.Enabled = False

        End With
        BuildAllDDLs()

    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not (txtPlacementId.Text = Guid.Empty.ToString) Then
            'Instantiate component
            Dim PlacementInfo As New PlacementFacade

            'Delete The Row Based on EmployerId 
            Dim result As String = PlacementInfo.DeletePlacementInfo(txtPlacementId.Text)

            'If Delete Operation was unsuccessful
            If Not result = "" Then
                DisplayErrorMessage(result)
                Exit Sub
            End If

            'Bind The Datalist
            BindDataList(StudentId, ddlEnrollmentTypeId.SelectedValue)

            'Bind Empty Data
            BindNew(New PlacementInfo)

            'initialize buttons
            InitButtonsForLoad()

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
            'SDF Code Starts Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim SDFControl As New SDFComponent
            SDFControl.DeleteSDFValue(txtPlacementId.Text)
            SDFControl.GenerateControlsNew(pnlSDF, resourceId, ModuleId)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            ' US3037 5/4/2012 Janet Robinson
            CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)

        End If
    End Sub
    Private Sub GetNameByStudentId(ByVal StudentId As String)
        Dim StudentName As New PlacementFacade
        'Get StudentName Based On StudentId
        BindStudentName(StudentName.GetNameByStudentId(StudentId))
    End Sub
    Private Sub BindStudentName(ByVal PlacementDataInfo As PlacementInfo)
        'Bind The EmployerInfo Data From The Database
        With PlacementDataInfo
            txtfirstname.Text = .firstname
            txtlastname.Text = .LastName
            txtmiddlename.Text = .MiddleName
        End With
    End Sub

    Private Sub btnSave_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnSave.Command

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        ' controlsToIgnore.Add(Img1)
        'controlsToIgnore.Add(Img2)
        'controlsToIgnore.Add(Img3)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
