﻿Imports FAME.Advantage.Common
Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports Advantage.Business.Logic.Layer
Imports Telerik.Web.UI

Partial Class EmployerInfo
    Inherits BasePage

    Protected WithEvents RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents RegularExpressionValidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents lblStatus As System.Web.UI.WebControls.Label
    Protected WithEvents txtLocationId As System.Web.UI.WebControls.TextBox
    Protected WithEvents dlstEmployers As System.Web.UI.WebControls.DataList
    Protected WithEvents chkStatus As System.Web.UI.WebControls.CheckBox

    Protected WithEvents Results As System.Web.UI.UserControl
    Protected WithEvents ModDate As System.Web.UI.WebControls.TextBox
    Protected WithEvents ModUser As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnHistory As System.Web.UI.WebControls.Button
    Protected state As AdvantageSessionState
    'This variable holds employerId. The value assigned is for testing only
    Protected EmployerId As String
    Protected WithEvents ZipFormat As System.Web.UI.WebControls.Label
    Protected WithEvents PhoneFormat As System.Web.UI.WebControls.Label
    Protected WithEvents FaxFormat As System.Web.UI.WebControls.Label
    Protected WithEvents lbl2 As System.Web.UI.WebControls.Label
    Private requestContext As HttpContext
    Protected resourceid As Integer
    Protected ModuleId As String
    Protected sdfcontrols As New SDFComponent
    Protected strVID As String
    Protected boolSwitchCampus As Boolean = False
    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Protected strDefaultCountry As String
    Protected userId As String

    Private mruProvider As MRURoutines
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


#End Region

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRURoutines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function getEmployerFromStateObject(ByVal paramResourceId As Integer) As BO.EmployerMRU


        Dim objStateInfo As New AdvantageStateInfo
        Dim objEmployerState As New BO.EmployerMRU

        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        MyBase.GlobalSearchHandler(2)


        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
        HttpContext.Current.Items("Language") = "En-US"

        boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployerId) Then
            EmployerId = Guid.Empty.ToString()
        Else
            EmployerId = AdvantageSession.MasterEmployerId
        End If

        With objEmployerState
            .EmployerId = New Guid(EmployerId)
            .Name = objStateInfo.NameValue
        End With


        Dim objGetStudentStatusBar As New AdvantageStateInfo

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
        objGetStudentStatusBar = mruProvider.BuildEmployerStatusBar(EmployerId)
        With objGetStudentStatusBar

            AdvantageSession.MasterEmpName = objGetStudentStatusBar.NameValue
            AdvantageSession.MasterEmpAddress1 = objGetStudentStatusBar.Address1
            AdvantageSession.MasterEmpAddress2 = objGetStudentStatusBar.Address2
            AdvantageSession.MasterEmpCity = objGetStudentStatusBar.City
            AdvantageSession.MasterEmpState = objGetStudentStatusBar.State
            AdvantageSession.MasterEmpZip = objGetStudentStatusBar.Zip
            AdvantageSession.MasterEmpPhone = objGetStudentStatusBar.Phone
            objEmployerState.Name = objGetStudentStatusBar.NameValue
        End With



        If Not String.IsNullOrEmpty(AdvantageSession.MasterEmployerId) Then
            Master.ShowHideStatusBarControl(True) 'Show Lead Bar only when Lead is not blank
            Master.PageObjectId = EmployerId
            Master.PageResourceId = Request.QueryString("resid").ToString()
            Master.setHiddenControlForAudit()
        Else
            Master.ShowHideStatusBarControl(False) 'Hide Lead Bar only when Lead is blank
        End If

        Return objEmployerState

    End Function
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim SearchEmployerID As New EmployerSearchFacade

        Session("SEARCH") = 0

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        campusId = Master.CurrentCampusId
        resourceid = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceid, campusId)

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        '''''''''''''''''' Call to get EmployerId starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objEmployerState As New BO.EmployerMRU
        objEmployerState = getEmployerFromStateObject(79) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objEmployerState Is Nothing Then
            MyBase.RedirectToEmployerSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objEmployerState
            EmployerId = .EmployerId.ToString

        End With

        '''''''''''''''''' Call to get EmployerId  ends here ''''''''''''''''''''
        'pObj = fac.GetUserResourcePermissions(userId, resourceid, campusId)
        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString

        Dim objCommon As New CommonUtilities
        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(2, objEmployerState.Name)
            End If
            InitButtonsForEdit()
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder),, True)
            txtCode.BackColor = Color.White
            txtEmployerDescrip.BackColor = Color.White
            ddlCampGrpId.BackColor = Color.White
            txtAddress1.BackColor = Color.White
            txtcity.BackColor = Color.White
            ddlStateId.BackColor = Color.White
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"
            BuildDropDownLists(campusId)
            'Assign The Variable Value to the textbox
            m_Context = HttpContext.Current
            txtEmployerId.Text = EmployerId
            txtRowIds.Text = EmployerId
            txtResourceId.Text = CInt(m_Context.Items("ResourceId"))
            chkIsInDB.Checked = True
            chkForeignPhone.Checked = False
            chkForeignZip.Checked = False
            chkForeignFax.Checked = False
            'Get Default Employer Data when page is loaded for first time
            Dim employerinfo As New PlEmployerInfoFacade
            BindEmployerInfoData(employerinfo.GetEmployerInfo(EmployerId))
            BindEmployerJobsListBox(EmployerId)

            HandleInternationalAddress(chkForeignZip.Checked)

            Try
                ddlCountryId.SelectedValue = strDefaultCountry
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlCountryId.SelectedIndex = 0
            End Try
            MyBase.uSearchEntityControlId.Value = ""
        Else
            'objCommon.PageSetup(Form1, "EDIT")
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT")
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder),, True)
            txtCode.BackColor = Color.White
            txtEmployerDescrip.BackColor = Color.White
            ddlCampGrpId.BackColor = Color.White
            txtAddress1.BackColor = Color.White
            txtcity.BackColor = Color.White
            ddlStateId.BackColor = Color.White
        End If

        'Apply Mask/Disable Mask Based on if user 
        'selects International
        Dim Phone As RadMaskedTextBox = txtPhone
        SetPhoneMask(chkForeignPhone.Checked, Phone)
        Dim Fax As RadMaskedTextBox = txtFax
        SetPhoneMask(chkForeignFax.Checked, Fax)
        EnableBtnEmployerType()

        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfcontrols.GetSDFExists(resourceid, ModuleId)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If


        If Trim(txtEmployerId.Text) <> "" Then
            sdfcontrols.GenerateControlsEdit(pnlSDF, resourceid, txtEmployerId.Text, ModuleId)
        Else
            sdfcontrols.GenerateControlsNew(pnlSDF, resourceid, ModuleId)
        End If
        headerTitle.Text = Header.Title
    End Sub
    Private Sub EnableBtnEmployerType()
        If Trim(Session("EmployerType")) = "ExistingEmployer" Then
            'btnNew.Enabled = False
        ElseIf Trim(Session("EmployerType")) = "NewEmployer" Then
            ' btnNew.enabled = false
        End If
    End Sub
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Try
            ddlCountryId.SelectedValue = strDefaultCountry
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlCountryId.SelectedIndex = 0
        End Try
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Results.Visible = False
        If Not (txtEmployerId.Text = Guid.Empty.ToString) Then

            '   instantiate component
            Dim EmployerInfo As New PlEmployerInfoFacade

            'Delete The Row Based on EmployerId 
            Dim result As String = EmployerInfo.DeleteEmployerInfo(txtEmployerId.Text, Date.Parse(txtModDate.Text))

            'If Delete Fails
            If result <> "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error2", result, "Delete Error")
            Else

                'bind an empty new BankAcctInfo
                BindEmployerInfoData(New plEmployerInfo)

                '   initialize buttons
                InitButtonsForLoad()

                Try
                    ddlCountryId.SelectedValue = strDefaultCountry
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlCountryId.SelectedIndex = 0
                End Try


                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim SDFControl As New SDFComponent
                SDFControl.DeleteSDFValue(txtEmployerId.Text)
                SDFControl.GenerateControlsNew(pnlSDF, resourceid, ModuleId)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                HandleInternationalAddress(chkForeignZip.Checked)
            End If
        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim EmployerInfo As New PlEmployerInfoFacade
        Dim Result As String
        Dim errorMessage As String
        Dim strEmployerGroup As String = ddlParentId.SelectedValue
        'Check If Mask is successful only if Foreign Is not checked
        If chkForeignZip.Checked = False Then
            errorMessage = ValidateFieldsWithInputMasks()
        Else
            errorMessage = ""
        End If

        If Not txtPhone.Text = "" Then
            If Not chkForeignPhone.Checked And txtPhone.Text.Length < 10 Then
                errorMessage &= IIf(errorMessage <> String.Empty, vbLf, String.Empty).ToString() _
                              + "Domestic phone numbers must include the area code and phone number and be 10 characters in length (Phone field)"
            End If
        End If
        If Not txtFax.Text = "" Then
            If Not chkForeignFax.Checked And txtFax.Text.Length < 10 Then
                errorMessage &= IIf(errorMessage <> String.Empty, vbLf, String.Empty).ToString() _
                              + "Domestic phone numbers must include the area code and phone number and be 10 characters in length (Fax field)"
            End If
        End If
        If errorMessage = "" Then
            'Call Update Function in the plEmployerInfoFacade
            Result = EmployerInfo.UpdateEmployerInfo(BuildEmployerInfo(txtEmployerId.Text), AdvantageSession.UserState.UserName)

            If Not Result = "" Then
                DisplayRADAlert(CallbackType.Postback, "Error3", Result, "Save Error")
                Exit Sub
            End If

            'Reset The Checked Property Of CheckBox To True
            chkIsInDB.Checked = True

            'Update The Jobs Offered(CheckBoxes)
            UpdateEmployerJobCats(txtEmployerId.Text)


            'If Page is free of errors 
            If Page.IsValid Then

                'Initialize Buttons
                InitButtonsForEdit()
            End If
            EnableBtnEmployerType()

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            '  Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                SDFControl.DeleteSDFValue(txtEmployerId.Text)
                SDFID = SDFControl.GetAllLabels(pnlSDF)
                SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtEmployerId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            HandleInternationalAddress(chkForeignZip.Checked)


        Else
            DisplayErrorMessageMask(errorMessage)
        End If

        'Refresh the EmployerGroup DDL
        BuildEmployerGroupDDL()
        Try
            ddlParentId.SelectedValue = strEmployerGroup
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlParentId.SelectedValue = Guid.Empty.ToString
        End Try

    End Sub
    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Function ValidateFieldsWithInputMasks() As String
        Dim facInputMasks As New InputMasksFacade
        Dim correctFormat As Boolean
        Dim zipMask As String
        Dim errorMessage As String = String.Empty
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        'Validate the zip field format. If the field is empty we should not apply the mask
        'against it.
        If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtZip.Text)
            If correctFormat = False Then
                errorMessage &= "Incorrect format for zip field."
            End If
        End If

        Return errorMessage

    End Function
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        'In this case the New button is not relevant
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If
    End Sub
    Private Sub BuildDropDownLists(ByVal campusId As String)
        'Build All The DropDown List Controls With
        'Values from Database
        'BuildStatusDDL()
        'BuildStatesDDL()
        'BuildCampusGroupsDDL()
        'BuildCountyDDL()
        'BuildEmployerFeeDDL()
        'BuildEmployerIndustryDDL()
        BuildEmployerJobsDDL()
        'BuildLocationDDL()
        'BuildEmployerGroupDDL()
        'BuildCountryDDL()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, Nothing))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStateId, AdvantageDropDownListName.States, Nothing, True, True))

        'Campus Groups DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlCampGrpId, AdvantageDropDownListName.CampGrps, Nothing, True, True))

        'Counties DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlCountyId, AdvantageDropDownListName.Counties, Nothing, True, True))

        'Employer Fees DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlFeeId, AdvantageDropDownListName.Employer_Fees, Nothing, True, True))

        'Employer Industries DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlIndustryId, AdvantageDropDownListName.Industries, campusId, True, True))

        ''Employer Jobs DDL 
        'ddlList.Add(New AdvantageDDLDefinition(chkJobCatId, AdvantageDropDownListName.Employer_Jobs, campusId, True, False))

        'Locations DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlLocationId, AdvantageDropDownListName.Locations, Nothing, True, True))

        'Employers Groups DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlParentId, AdvantageDropDownListName.Employer_Groups, Nothing, True, True))

        'Countries DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlCountryId, AdvantageDropDownListName.Countries, Nothing, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub
    Private Sub BuildStatusDDL()

        'Bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With
    End Sub
    Private Sub BuildStatesDDL()
        'Bind the states DrowDownList
        Dim states As New StatesFacade
        With ddlStateId
            .DataTextField = "StateDescrip"
            .DataValueField = "StateId"
            .DataSource = states.GetAllStates()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCampusGroupsDDL()

        'Bind the CampusGroups DrowDownList
        Dim campusGroups As New CampusGroupsFacade
        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCountyDDL()

        'Bind the County DrowDownList
        Dim county As New CountyFacade
        With ddlCountyId
            .DataTextField = "CountyDescrip"
            .DataValueField = "CountyId"
            .DataSource = county.GetAllCounty()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildEmployerFeeDDL()
        'Bind the Employer Fee DrowDownList
        Dim EmployerFee As New EmployerFeeFacade
        With ddlFeeId
            .DataTextField = "FeeDescrip"
            .DataValueField = "FeeId"
            .DataSource = EmployerFee.GetAllEmployerFee()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildEmployerIndustryDDL()

        'Bind the EmployerIndustry DrowDownList
        Dim Industry As New IndustryFacade
        With ddlIndustryId
            .DataTextField = "IndustryDescrip"
            .DataValueField = "IndustryId"
            .DataSource = Industry.GetAllIndustry()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildEmployerJobsDDL()
        'Bind the EmployerJobs DrowDownList
        Dim EmployerJobs As New EmployerJobsFacade
        With chkJobCatId
            .DataTextField = "JobGroupDescrip"
            .DataValueField = "JobGroupId"
            .DataSource = EmployerJobs.GetAllJobType()
            .DataBind()
        End With
    End Sub
    Private Sub BuildEmployerGroupDDL()
        'Bind the Employer Group DrowDownList
        Dim EmployerGroups As New EmployerJobsFacade
        With ddlParentId
            .DataTextField = "EmployerDescrip"
            .DataValueField = "EmployerId"
            .DataSource = EmployerGroups.GetAllEmployerGroup()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildLocationDDL()
        'Bind the Location DrowDownList
        Dim Location As New LocationFacade
        With ddlLocationId
            .DataTextField = "LocationDescrip"
            .DataValueField = "LocationId"
            .DataSource = Location.GetAllLocation()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCountryDDL()
        Dim Country As New PrefixesFacade
        With ddlCountryId
            .DataTextField = "CountryDescrip"
            .DataValueField = "CountryId"
            .DataSource = Country.GetAllCountries()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            'selected index must be the default country if it is defined in configuration file
            '.SelectedIndex = CommonWebUtilities.GetDefaultCountryIndexInDDL(ddlCountryId)
        End With
    End Sub
    Private Function BuildEmployerInfo(ByVal EmployerId As String) As plEmployerInfo
        Dim EmployerInfo As New plEmployerInfo
        Dim facInputMask As New InputMasksFacade
        'Dim phoneMask As String
        Dim zipMask As String

        With EmployerInfo

            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'Get EmployerId
            .EmployerId = txtEmployerId.Text

            'Get Code
            .Code = txtCode.Text

            'IsGroup
            .Group = chkGroupName.Checked

            'Get StatusId 
            .StatusId = ddlStatusId.SelectedValue

            'Get Description
            .Description = txtEmployerDescrip.Text

            'Get StateId
            .StateId = ddlStateId.SelectedValue

            'Get CampusGroup
            .CampGrpId = ddlCampGrpId.SelectedValue

            'Get Address1
            .Address1 = txtAddress1.Text

            'Get Address2
            .Address2 = txtAddress2.Text

            'Get Phone
            .Phone = txtPhone.Text

            'Get City
            .City = txtcity.Text

            'Get Zip
            If txtZip.Text <> "" And chkForeignZip.Checked = False Then
                zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
                .Zip = facInputMask.RemoveMask(zipMask, txtZip.Text)
            Else
                .Zip = txtZip.Text
            End If

            'Get Location
            .Location = ddlLocationId.SelectedValue

            'Get County
            .County = ddlCountyId.SelectedValue

            'Get Email
            .Email = txtEmail.Text

            'Get Industry
            .IndustryId = ddlIndustryId.SelectedValue

            'Country
            .CountryId = ddlCountryId.SelectedValue

            'Get Fee
            .FeeId = ddlFeeId.SelectedValue

            'Get EmployerGroup 
            .ParentId = ddlParentId.SelectedValue

            'Get Fax
            .Fax = txtFax.Text

            'Foreign Phone
            .ForeignPhone = chkForeignPhone.Checked

            'Foreign Fax
            .ForeignFax = chkForeignFax.Checked

            'Foreign Zip
            .ForeignZip = chkForeignZip.Checked

            .OtherState = txtOtherState.Text

            .CountryId = ddlCountryId.SelectedValue

            txtModDate.Text = CType(Date.Now, String)

            .ModDate = CType(txtModDate.Text, Date)

        End With
        Return EmployerInfo
    End Function
    Private Sub BindEmployerInfoData(ByVal EmployerInfo As plEmployerInfo)
        Dim facInputMasks As New InputMasksFacade
        Dim zipMask As String

        'Get the mask for phone numbers and zip
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        'Bind The EmployerInfo Data From The Database
        With EmployerInfo
            'Get Foreign phone Checkbox
            chkForeignPhone.Checked = .ForeignPhone

            'Get Foreign Fax Checkbox
            chkForeignFax.Checked = .ForeignFax

            txtEmployerId.Text = .EmployerId
            txtCode.Text = .Code

            'ddlCampGrpId.SelectedValue = .CampGrpId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCampGrpId, .CampGrpId, .CampGrpDescrip)

            txtEmployerDescrip.Text = .Description
            txtAddress1.Text = .Address1
            txtAddress2.Text = .Address2
            txtcity.Text = .City
            txtZip.Text = .Zip
            If txtZip.Text <> "" And .ForeignZip = False Then
                txtZip.Text = facInputMasks.ApplyMask(zipMask, txtZip.Text)
            End If

            'Get Phone
            SetPhoneMask(chkForeignPhone.Checked, txtPhone)
            txtPhone.Text = .Phone

            'Get Phone
            SetPhoneMask(chkForeignFax.Checked, txtFax)
            txtFax.Text = .Phone

            txtEmail.Text = .Email
            'Try
            '    ddlStateId.SelectedValue = .StateId
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    BuildStatesDDL()
            'End Try
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateId, .StateId, .State)

            'ddlCountyId.SelectedValue = .County
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountyId, .CountyId, .County)

            'ddlLocationId.SelectedValue = .Location
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlLocationId, .LocationId, .Location)

            'ddlFeeId.SelectedValue = .FeeId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFeeId, .FeeId, .Fee)

            'ddlIndustryId.SelectedValue = .IndustryId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlIndustryId, .IndustryId, .Industry)

            'ddlParentId.SelectedValue = .ParentId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlParentId, .ParentId, .Parent)


            chkGroupName.Checked = .Group

            chkJobCatId.ClearSelection()

            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountryId, .CountryId, .Country)

            'Foreign Zip
            If .ForeignZip = True Then
                chkForeignZip.Checked = True
                txtOtherState.Visible = True
                ddlStateId.Enabled = False
                lblOtherState.Visible = True
                txtOtherState.Text = .OtherState
            Else
                chkForeignZip.Checked = False
                ddlStateId.Enabled = True
                txtOtherState.Visible = False
                lblOtherState.Visible = False
            End If
            txtModDate.Text = .ModDate.ToString()
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStatusId, .StatusId, .Status)
            'ddlStatusId.SelectedIndex = 0
        End With
    End Sub
    Private Sub UpdateEmployerJobCats(ByVal empId As String)

        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        'Dim selectedDegrees() As String = selectedDegrees.CreateInstance(GetType(String), chkJobCatId.Items.Count)
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), chkJobCatId.Items.Count)

        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In chkJobCatId.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)

        'update Selected Jobs
        Dim employerJobs As New PlEmployerInfoFacade
        If employerJobs.UpdateEmployerJobCats(empId, AdvantageSession.UserState.UserName, selectedDegrees) < 0 Then
            'DisplayErrorMessage("A related record exists , you can not perform this operation")
            DisplayRADAlert(CallbackType.Postback, "Error1", "A related record exists , you can not perform this operation", "Save Error")
            Exit Sub
        End If
    End Sub
    Private Sub BindEmployerJobsListBox(ByVal empId As String)

        'Get Degrees data to bind the CheckBoxList
        Dim employees As New PlEmployerInfoFacade
        Dim ds As DataSet = employees.GetJobsOffered(empId)
        Dim JobCount As Integer = employees.GetValidJobsOffered(empId)

        If JobCount >= 1 Then

            'Select the items on the CheckBoxList
            Dim i As Integer
            If Not (ds.Tables(0).Rows.Count < 0) Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim row As DataRow
                    row = ds.Tables(0).Rows(i)
                    Dim item As ListItem
                    For Each item In chkJobCatId.Items
                        If item.Value.ToString = DirectCast(row("JobCatId"), Guid).ToString Then
                            item.Selected = True
                            Exit For
                        End If
                    Next
                Next
            End If
        End If
    End Sub
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(chkForeignPhone)
        controlsToIgnore.Add(chkForeignFax)
        controlsToIgnore.Add(chkForeignZip)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPhone.CheckedChanged
        SetPhoneMask(chkForeignPhone.Checked, txtPhone)
        txtPhone.Focus()
    End Sub
    Private Sub chkForeignFax_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignFax.CheckedChanged
        SetPhoneMask(chkForeignFax.Checked, txtFax)
        txtFax.Focus()
    End Sub
    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignZip.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        'objCommon.SetFocus(Me.Page, txtAddress1)
        CommonWebUtilities.SetFocus(Me.Page, txtAddress1)

        HandleInternationalAddress(chkForeignZip.Checked)
    End Sub
    Private Sub ddlStateId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStateId.SelectedIndexChanged

    End Sub
    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOtherState.TextChanged
        Dim objCommon As New CommonWebUtilities
        'objCommon.SetFocus(Me.Page, txtZip)
        CommonWebUtilities.SetFocus(Me.Page, txtZip)
    End Sub
    Private Sub txtcity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtcity.TextChanged
        Dim objCommon As New CommonWebUtilities
        If chkForeignZip.Checked = True Then
            'objCommon.SetFocus(Me.Page, txtOtherState)
            CommonWebUtilities.SetFocus(Me.Page, txtOtherState)
        Else
            'objCommon.SetFocus(Me.Page, ddlStateId)
            CommonWebUtilities.SetFocus(Me.Page, ddlStateId)
        End If
    End Sub
    ''' <summary>
    ''' Set phone mask
    ''' </summary>
    ''' <param name="isInternational">true set international mask else national mask</param>
    ''' <param name="Phone">the control affectred for the Internantional param </param>
    ''' <remarks></remarks>
    Private Sub SetPhoneMask(ByVal isInternational As Boolean, ByVal Phone As RadMaskedTextBox)
        Dim strPhone As String = Phone.Text
        If isInternational = False Then
            Phone.Mask = "(###)-###-####"
            Phone.DisplayMask = "(###)-###-####"
            Phone.DisplayPromptChar = ""
        Else
            Phone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            Phone.DisplayMask = ""
            Phone.DisplayPromptChar = ""
        End If
        Phone.Text = strPhone

    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String, Optional ByVal strFileType As String = "")
    '    'Set error condition
    '    'Display error in message box in the client
    '    CustomValidator1.ErrorMessage = errorMessage
    '    CustomValidator1.IsValid = False
    '    If ValidationSummary1.ShowMessageBox = True And ValidationSummary1.ShowSummary = False And CustomValidator1.Display = ValidatorDisplay.None Then
    '        '   Display error in message box in the client
    '        'CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    '        TestDisplayErrorInMessageBox(Me.Page, errorMessage, strFileType)
    '    End If
    'End Sub
    'Public Shared Sub TestDisplayErrorInMessageBox(ByVal page As Page, ByVal errorMessage As String, ByVal ImageInfo As String)
    '    If page.Request.Browser.JavaScript Then
    '        Dim scriptBegin As String = "<script type='text/javascript'>window.onload=DisplayError();function DisplayError(){window.open('../Test/TestPanel.aspx?Message="
    '        Dim strOtherParams As String = "&strFileType="
    '        Dim strWindowProperty As String = "','histwindow','width=400,height=150,screenX=400px,screenY=320px,left=400px,top=280px"
    '        Dim scriptEnd As String = "');}</script>"
    '        Dim strFullString As String = scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + strOtherParams + ImageInfo + strWindowProperty + scriptEnd
    '        '   Register a javascript to display error message
    '        page.RegisterStartupScript("ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + strOtherParams + ImageInfo + strWindowProperty + scriptEnd)
    '    End If
    'End Sub

    Public Sub HandleInternationalAddress(ByVal isInternationalAddress As Boolean)
        Dim ctl2 As Control
        ctl2 = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("pnlRequiredFieldValidators")

        If isInternationalAddress = True Then
            If ViewState("IsInternational") = "false" Then
                txtOtherState.Visible = True
                lblOtherState.Visible = True
                ddlStateId.Enabled = False
                ddlStateId.SelectedIndex = 0
                txtZip.Mask = "####################"
                txtZip.DisplayMask = "####################"
                txtZip.Text = String.Empty

                'Remove the red asterisk
                lblStateId.Text = "State"
            End If

            ViewState("IsInternational") = "true"
        Else
            If ViewState("IsInternational") = "true" Then
                txtOtherState.Visible = False
                lblOtherState.Visible = False
                ddlStateId.Enabled = True
                txtZip.Mask = "#####"
                txtZip.DisplayMask = "#####"
                txtZip.Text = String.Empty

                'Add back the red asterisk
                lblStateId.Text = "State <font color=""red"">*</font>"
            End If

            'Add the validator the state field
            AddRequiredFieldValidatorForState()

            ViewState("IsInternational") = "false"

        End If
    End Sub

    Public Sub AddRequiredFieldValidatorForState()
        Dim ctl2 As Control
        ctl2 = CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).FindControl("pnlRequiredFieldValidators")

        Dim rfv As New WebControls.RequiredFieldValidator
        With rfv
            .ID = "rfvTest"
            .ControlToValidate = "ddlStateId"
            .Display = WebControls.ValidatorDisplay.None
            .ErrorMessage = "State is required"
            .InitialValue = "00000000-0000-0000-0000-000000000000"
            .EnableClientScript = True
        End With
        ctl2.Controls.Add(rfv)
    End Sub


End Class
