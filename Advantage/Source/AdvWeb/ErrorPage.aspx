﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ErrorPage.aspx.vb" Inherits="ErrorPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error Occurred</title>
    <link rel="Stylesheet" type="text/css" href="css/Login.css" />
    <link rel="Stylesheet" type="text/css" href="css/localhost_lowercase.css" />
    <style type="text/css">
        .style1 {
            height: 17px;
        }

        h1 {
            font-size: 5em;
            color: #2a2a2a;
        }

        body, html {
            background-color: #f2f2f2
        }

        .clwyLoginBox {
            width: 700px !important;
        }

        .headerBanner {
            width: 742px !important;
        }

        .errorHeader {
            color: #FFF !important;
            background-color: #ff9090 !important;
        }
        a {
            color:#2962ff
        }
        a:hover {
            color: #ff9090 !important;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" enableviewstate="False">
        <telerik:RadScriptManager ID="scriptmanager1" runat="server"></telerik:RadScriptManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="Default, ValidationSummary" />
        <div align="center">
            <div>
                <h1>:(</h1>
            </div>
            <div class="headerBanner errorHeader">
                Something went wrong displaying the Web page
            </div>
            <div class="clwyLoginBox">
                <div class="ErrorPageContent">
                    <div>
                        <asp:Label ID="lblText" runat="server" Visible="false">Error Occured</asp:Label>
                        <asp:HiddenField ID="hdnpreviousURL" runat="server" />
                    </div>

                    <div>
                        <asp:Button ID="btnSendEmail" runat="server"
                            Text="Send notification to Advantage Customer Support" />
                    </div>

                    <br />

                    <div class="ErrorMessageheader">
                        To continue, go to
                    <asp:LinkButton ID="lnkPreviousPage" runat="server" CssClass="ErrorMessageheader" Text="Previous Page"></asp:LinkButton>
                        or 
                    <asp:LinkButton ID="lnkLogin" runat="server" CssClass="ErrorMessageheader" Text="Log In"></asp:LinkButton>
                        page 
                    </div>

                    <br />

                    <div>
                        <asp:Label ID="lblErrorheader" runat="server" CssClass="labelheader" Text="Detailed error for FAME"></asp:Label>
                    </div>

                    <br />

                    <div>
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 50%">
                                   
                                </td>
                                <td style="width: 50%; text-align: right">
                                    <a href="javascript:void(0);" onclick="copyToClipboard(this)" target="_blank" style="color:#ff9090 !important">Copy to Clipboard</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="height: 400px; overflow-x: auto; word-break: break-all; line-height: 1.6em; background-color: #f2f2f2; border-radius: 4px; padding-top: 0.5em; padding-bottom: 0.5em;">
                        <asp:Label ID="lblerror" runat="server" Text="Label" CssClass="label" Width="80%" ForeColor="#525151"></asp:Label>
                    </div>
                    <br/>
                    <div style="text-align: center;">
                        <a href="https://support.fameinc.com/hc/en-us/requests/new" target="_blank" >Submit a Support Request</a>
                    </div>


                </div>
            </div>
        </div>
        <div class="clwyLoginPageFooterLabel">
            <table style="height: 40px; width: 100%">
                <tr>
                    <td>
                        <asp:Label ID="lblfooter" runat="server" Style="width: 250px; margin: auto;">
					Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>
                        </asp:Label>
                        <asp:Label ID="lblApplicationHeaderVersion" runat="server" />
                    </td>
                </tr>
            </table>
        </div>

    </form>
<script type="text/javascript">
    function copyToClipboard(link) {

        var copyText = document.getElementById("lblerror");
        var textArea = document.createElement("textarea");
        textArea.value = copyText.textContent;
        document.body.appendChild(textArea);
        textArea.select();
        document.execCommand("Copy");
        textArea.remove();

        link.text = "Copied!";
    }

</script>
</body>
</html>
