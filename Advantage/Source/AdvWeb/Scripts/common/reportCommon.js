﻿(function (jQuery) {

    //-------------------------------------------------------------------------------------------------
    // Render the links for the report section passed in.
    // Creates html table with list of links for each page group.
    // If there are more than 5 page groups, drop down to new row.
    //-------------------------------------------------------------------------------------------------
    var renderLinks = function (controlName, selectedSubLevel, data) {
        var headerTitle = '';
        if (data.length > 0) {
            var htmlData = "<table border='0'><tr><td class='reportHeader'> " + selectedSubLevel + "</td></tr><tr><td ><table border=0 cellpadding='5' cellspacing='5' ><tr>";


            for (var i = 0; i < data.length; i++) {

                if (data[i].items.length > 0) {

                    var divisor = 4;
                    if (controlName == 'IPEDSReports')
                        divisor = 3;
                    else if (controlName == 'Academics') {
                        divisor = 5;
                    }

                    if (i % divisor == 0) htmlData += "</td><tr>";

                    htmlData += "<td valign='top' align='left' width='250px' ><span class='linkHeader'>" + data[i].text + "</span><br><br>";
                    for (var x = 0; x < data[i].items.length; x++) {

                        if (data[i].items[x].IsEnabled == "True") {
                            htmlData += "<a href='#' onclick=\"navLink('" + data[i].items[x].url + "');\">" + data[i].items[x].text + "</a><br>";
                        } else {
                            htmlData += "<span class='disabledLink'>" + data[i].items[x].text + "</span><br>";
                        }
                    }


                    htmlData += "</td>";
                }
                
            }

            htmlData += "</tr></table></td></tr></table><table align='center' width='100%'><tr><td><hr width='100%' /></td></tr></table>";

            return htmlData;


        } else {
            return null;
        }

    };

    //-------------------------------------------------------------------------------------------------
    // Clear all of the html for the page groups on the report page.  This is called.
    // before rendering controls.
    //-------------------------------------------------------------------------------------------------
     var clearAllHtmlControls = function() {
        $('#AdmissionsLinks').html("");
        $('#AcademicsLinks').html("");
        $('#IPEDSReportsLinks').html("");
        $('#StudentAccountsLinks').html("");
        $('#FinancialAidLinks').html("");
        $('#FacultyLinks').html("");
        $('#PlacementLinks').html("");
        $('#HumanResourcesLinks').html("");
        $('#SystemLinks').html("");
        $('#NACCASReportsLinks').html("");
     };

    //-------------------------------------------------------------------------------------------------
    // Set the various page group html controls using the links array passed in
    //-------------------------------------------------------------------------------------------------
     var setAllHtmlControls = function (links) {
        $('#AdmissionsLinks').html(links[0]);
        $('#AcademicsLinks').html(links[1]);
        $('#IPEDSReportsLinks').html(links[2]);
         var showNACCAReports = $('#hdnShowNACCASReports').val();
         if (showNACCAReports === "yes") {
             $('#NACCASReportsLinks').html(links[3]);
             $('#StudentAccountsLinks').html(links[4]);
             $('#FinancialAidLinks').html(links[5]);
             $('#FacultyLinks').html(links[6]);
             $('#PlacementLinks').html(links[7]);
             $('#HumanResourcesLinks').html(links[8]);
             $('#SystemLinks').html(links[9]);
         } else {
             $('#NACCASReportsLinks').html("");
             $('#StudentAccountsLinks').html(links[3]);
             $('#FinancialAidLinks').html(links[4]);
             $('#FacultyLinks').html(links[5]);
             $('#PlacementLinks').html(links[6]);
             $('#HumanResourcesLinks').html(links[7]);
             $('#SystemLinks').html(links[8]);
         }
        
     };

    //-------------------------------------------------------------------------------------------------
    // Set the html for a specific html control
    //-------------------------------------------------------------------------------------------------
    var setSpecificHtmlControl = function(control, ctrlLinks) {
        if (ctrlLinks) {
            $('#' + control + 'Links').html(ctrlLinks);
        }
        else {
            $('#' + control + 'Links').html("<span style='font-size:14px;font-weight:bold;'>No reports found for this module</span>");
        }
    };


    window.reportCommon = window.reportCommon || {};
    window.reportCommon.renderLinks = renderLinks;
    window.reportCommon.clearAllHtmlControls = clearAllHtmlControls;
    window.reportCommon.setAllHtmlControls = setAllHtmlControls;
    window.reportCommon.setSpecificHtmlControl = setSpecificHtmlControl;
})(window.jQuery);