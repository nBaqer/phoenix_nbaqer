﻿(function (jQuery) {

    //-------------------------------------------------------------------------------------------------
    // Render the links for the maintenance section passed in.
    // Creates html table with list of links for each page group.
    // If there are more than 5 page groups, drop down to new row.
    //-------------------------------------------------------------------------------------------------
    var renderLinks = function (controlName, selectedSubLevel, data) {
        var headerTitle = '';
        if (data.length > 0) {
            var htmlData = "<table border='0'><tr><td class='reportHeader' valign='top'>" + selectedSubLevel + "</td></tr><tr><td ><table border=0 cellpadding='5' cellspacing='5' ><tr>";


            for (var i = 0; i < data.length; i++) {

                if (data[i].items.length > 0) {
                    if (i % 10 == 0) htmlData += "</td><tr>";

                    htmlData += "<td valign='top' align='left' width='250px'><span class='linkHeader'>" + data[i].text + "</span><br><br>";
                    for (var x = 0; x < data[i].items.length; x++) {

                        if (data[i].items[x].IsEnabled == "True") {
                            htmlData += "<a href='#' onclick=\"navLink('" + data[i].items[x].url + "');\">" + data[i].items[x].text + "</a><br>";
                        } else {
                            htmlData += "<span class='disabledLink'>" + data[i].items[x].text + "</span><br>";
                        }
                    }


                    htmlData += "</td>";
                }
            }

            htmlData += "</tr></table></td></tr>";

            return htmlData;


        } else {
            return null;
        }

    };

    //-------------------------------------------------------------------------------------------------
    // Clear all of the html for the page groups on the report page.  This is called.
    // before rendering controls.
    //-------------------------------------------------------------------------------------------------
    var clearAllHtmlControls = function () {
        $('#AdmissionsLinks').html("");
        $('#AcademicsLinks').html("");
        $('#StudentAccountsLinks').html("");
        $('#FinancialAidLinks').html("");
        $('#PlacementLinks').html("");
        $('#HumanResourcesLinks').html("");
        $('#SystemLinks').html("");
        $('#ReportsLinks').html("");
    };

    //-------------------------------------------------------------------------------------------------
    // Set the various page group html controls using the links array passed in
    //-------------------------------------------------------------------------------------------------
    var setAllHtmlControls = function (links) {
        $('#AdmissionsLinks').html(links[0]);
        $('#AcademicsLinks').html(links[1]);
        $('#StudentAccountsLinks').html(links[2]);
        $('#FinancialAidLinks').html(links[3]);
        $('#PlacementLinks').html(links[4]);
        $('#HumanResourcesLinks').html(links[5]);
        $('#SystemLinks').html(links[6]);
        $('#ReportsLinks').html(links[7]);
    };

    //-------------------------------------------------------------------------------------------------
    // Set the html for a specific html control
    //-------------------------------------------------------------------------------------------------
    var setSpecificHtmlControl = function (control, ctrlLinks) {
        if (ctrlLinks) {
            $('#' + control + 'Links').html(ctrlLinks);
        }
        else {
            $('#' + control + 'Links').html("<span style='font-size:14px;font-weight:bold;'>No items found for this module</span>");
        }
        
    };


    var hideHrControls = function() {
        $('#hr1').html("");
        $('#hr2').html("");
        $('#hr3').html("");
    };

    window.maintenanceCommon = window.maintenanceCommon || {};
    window.maintenanceCommon.renderLinks = renderLinks;
    window.maintenanceCommon.clearAllHtmlControls = clearAllHtmlControls;
    window.maintenanceCommon.setAllHtmlControls = setAllHtmlControls;
    window.maintenanceCommon.setSpecificHtmlControl = setSpecificHtmlControl;
    window.maintenanceCommon.hideHrControls = hideHrControls;
})(window.jQuery);