﻿(function (jQuery) {

    //----------------------------------------------------------------------------------------------
    // formSimulator
    //
    // method to allow user to user enter key on kayboard to submit.  Will look for an input control
    // with type set to 'submit'.  Requires that only one submit button exists on the page.
    //----------------------------------------------------------------------------------------------
    jQuery.fn.formSimulator = function () {
        jQuery.each(this, function () {
            var submitButton = jQuery(this).find(':input[type="submit"]');

            if (submitButton.length !== 1) throw 'A single submit button must be present';

            jQuery(this).keypress(function () {
                if (event.which == 13) {
                    var activeElement = document.activeElement;
                    submitButton.focus();

                    submitButton.click();

                    jQuery(activeElement).focus();

                }

            });

        });

    };

    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    // supplant
    //
    //Taken from: http://javascript.crockford.com/remedial.html
    //May want to use a better performing method of string interpolation for intensive applications
    //----------------------------------------------------------------------------------------------
    String.prototype.supplant = function (o) {
        return this.replace(/{([^{}]*)}/g,
            function (a, b) {
                var r = o[b];
                return typeof r === 'string' || typeof r === 'number' ? r : a;
            }

);

    };

    //----------------------------------------------------------------------------------------------


    //----------------------------------------------------------------------------------------------
    // errorWin
    //
    // Functionality to handle displaying an error window to the user when there is an error 
    // communicating with the service layer
    //----------------------------------------------------------------------------------------------
    var errorWindow;
    var errorWin = function (win) {
        win.content("<table id='table1' width='98%' >" +
                    "<tr>" +
                        "<td colspan='2' align='center'><span class='tblLabels'>The following error occurred while communicating with the server:</span></td>" +
                    "</tr>" +
                    "<tr>" +
                        "<td align='right' ><span class='tblLabels'>Status Code</span></td>" +
                        "<td align='left' ><textarea cols='40' rows='1' class='k-label' data-bind='value:statusCode' readonly='readonly'></textarea></td>" +
                    "</tr>" +
                    "<tr>" +
                        "<td align='right' valign='top' ><span class='tblLabels'>Error Message</span></td>" +
                        "<td align='left' ><textarea cols='40' rows='12' data-bind='value:errorMessage' readonly='readonly' ></textarea></td>" +
                    "</tr>" +
                    "</table>");

        errorWindow = win;
    };

    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    // ajaxSetup
    //
    // setup method used to initialize ajax for a page.  Will handle showing a loading message
    // as well as handle showing a kendo error window.
    //----------------------------------------------------------------------------------------------
    var ajaxSetup = function (viewModel) {
        ajaxSetup.addHeaders = function (xhr) {
            var usernameHiddenField = jQuery('#hdnUserName');

            if (usernameHiddenField.length === 0) return;

            xhr.setRequestHeader('Username', usernameHiddenField.val());

        };


        jQuery.ajaxSetup({
            timeout: 25000, //25 seconds,   
            dataType: 'json',
            contentType: 'application/json',
            beforeSend: ajaxSetup.addHeaders
        });


        jQuery(document)
        .ajaxStart(function () {
            viewModel.set('loading', true);

        }).ajaxComplete(function () {
            viewModel.set('loading', false);

        })
        .ajaxError(function (event, request, settings) {
            if (request.status == 401 || request.status == 403) {
                alert("Your session has timed out.\r\nPlease login again to continue.");

                location.href = '~/../../host/default.aspx';
            }

            else {
                if (request.responseText != null) {
                    var num = request.responseText.search('Login1');

                    if (num != -1) {
                        location.href = '~/../../host/default.aspx';
                        return;
                    }

                }


                if (!request.statusCode && !request.statusCode == 0) {
                    viewModel.set("statusCode", request.status);

                    viewModel.set("errorMessage", request.statusText + ". \r\n" + request.responseText);

                    errorWindow.open();

                }



                //alert("The following error occured when communicating with the server.\r\nError Code: " + request.status + "\r\nError Message: " + request.statusText + " - " + request.responseText);

            }

        });

    };



    //----------------------------------------------------------------------------------------------
    // ajaxSetup
    //
    // setup method used to initialize ajax for a page.  Will handle showing a loading message
    // as well as handle showing a kendo error window.
    //----------------------------------------------------------------------------------------------
    var ajaxSetupBasic = function () {
        ajaxSetup.addHeaders = function (xhr) {
            var usernameHiddenField = jQuery('#hdnUserName');

            if (usernameHiddenField.length === 0) return;

            xhr.setRequestHeader('Username', usernameHiddenField.val());

        };


        jQuery.ajaxSetup({
            timeout: 25000, //25 seconds,   
            dataType: 'json',
            contentType: 'application/json',
            beforeSend: ajaxSetup.addHeaders
        });


        jQuery(document)
        //.ajaxStart(function () {
        //    viewModel.set('loading', true);

        //}).ajaxComplete(function () {
        //    viewModel.set('loading', false);

        //})
        .ajaxError(function (event, request, settings) {
            if (request.status === 0 && request.statusText === "timeout") {
                return;
            }

            if (request.status === 401 || request.status === 403) {
                alert("Your session has timed out.\r\nPlease login again to continue.");

                location.href = '~/../../host/default.aspx';
            }

            else {
                if (request.responseText != null) {
                    var num = request.responseText.search('Login1');

                    if (num != -1)
                        location.href = '~/../../host/default.aspx';
                    else
                        if (!request.statusCode && !request.statusCode == 0) alert("Error code:", request.statusCode);

                }

            }

        });

    };

    //----------------------------------------------------------------------------------------------

    var pageFromUrl = function () {
        var fileName = document.location.href;
        var end = (fileName.indexOf("?") == -1) ? fileName.length : fileName.indexOf("?");

        return fileName.substring(fileName.lastIndexOf("/") + 1, end);

    };



    var filterFunc = function (anyArray) {
        for (var i = 0; i < anyArray.length; i++) {
            var v = anyArray[i];
            for (var j = 1; j < arguments.length; j++) {
                if (v == arguments[j]) {
                    anyArray.splice(i, 1);

                    i--;
                }

            }

        }

        return anyArray;
    };




    window.common = window.common || {};

    window.common.ajaxSetup = ajaxSetup;
    window.common.ajaxSetupBasic = ajaxSetupBasic;
    window.common.pageFromUrl = pageFromUrl;
    window.common.errorWin = errorWin;
    window.common.filterFunc = filterFunc;

})(window.jQuery);
