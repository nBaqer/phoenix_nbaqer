﻿$(document).ready(function () {
    $("#ctl00_ContentMain2_ddlDocTypeSearchControl_CmbBxDocumentTypeSearch_Input").bind("cut copy paste",
        function (e) {
            e.preventDefault();
        });

    $("#ctl00_ContentMain2_ddlDocTypeSearchControl_CmbBxDocumentTypeSearch_MoreResultsBox").click((event) => {
        event.stopImmediatePropagation();
        return false;
    }); 
});