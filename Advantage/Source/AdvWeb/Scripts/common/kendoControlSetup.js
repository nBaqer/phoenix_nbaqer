﻿(function (jQuery) {

    //----------------------------------------------------------
    // setup the kendo tooltip
    //----------------------------------------------------------
    var setupToolTip = function(control, content, position) {
        
        $(control).kendoTooltip({
            position: position,
            content: content,
            showAfter: 800,
            animation: {
                open: {
                    effects: "fade:in",
                    duration: 500,
                },
                close: {
                    effects: "fade:out",
                    duration: 500,
                }
            }
        });
    };

    //----------------------------------------------------------
    // setup the kendo tooltip
    //----------------------------------------------------------
    var setupToolTip = function (control, content, position,showAfter) {

        $(control).kendoTooltip({
            position: position,
            content: content,
            showAfter: showAfter,
            animation: {
                open: {
                    effects: "fade:in",
                    duration: 500,
                },
                close: {
                    effects: "fade:out",
                    duration: 500,
                }
            }
        });
    };


    var setupTabStrip = function(control) {

        //setup tabstrip
        $(control).kendoTabStrip({
            animation: {
                open: {
                    effects: "fade:in",
                    duration: 200
                },
                close: {
                    effects: "fade:out",
                    duration: 100
                }
            }
        });
    };


    window.kendoControlSetup = window.kendoControlSetup || {};
    window.kendoControlSetup.setupToolTip = setupToolTip;
    window.kendoControlSetup.setupTabStrip = setupTabStrip;
})(window.jQuery);