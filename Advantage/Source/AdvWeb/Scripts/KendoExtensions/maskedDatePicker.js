﻿//maskedDatePicker.js
(function ($) {
    var kendo = window.kendo,
        ui = kendo.ui,
        Widget = ui.Widget,
        proxy = $.proxy,
        CHANGE = "change",
        PROGRESS = "progress",
        ERROR = "error",
        NS = ".generalInfo";

    var MaskedDatePicker = Widget.extend({
        init: function (element, options) {
            var that = this;
            Widget.fn.init.call(this, element, options);

            $(element).kendoMaskedTextBox({ mask: that.options.dateOptions.mask || "00/00/0000" })
            .kendoDatePicker({
                format: that.options.dateOptions.format || "MM/dd/yyyy",
                parseFormats: that.options.dateOptions.parseFormats || ["MM/dd/yyyy", "MM/dd/yy"]
            })
            .closest(".k-datepicker")
            .add(element)
            .removeClass("k-textbox");

            that.element.data("kendoDatePicker").bind("change", function () {
                that.trigger(CHANGE);
            });
            if (that.options.max !== undefined) {
                that.element.data("kendoDatePicker").max(that.options.max);
            }
            if (that.options.value !== undefined) {
                that.element.data("kendoDatePicker").value(that.options.value);
            }
            if (that.options.start !== undefined) {
                that.element.data("kendoDatePicker").options.start = that.options.start;
            }
            if (that.options.depth !== undefined) {
                that.element.data("kendoDatePicker").options.depth = that.options.depth;
            }
        },
        options: {
            name: "MaskedDatePicker",
            dateOptions: {}
        },
        events: [
          CHANGE
        ],
        destroy: function () {
            var that = this;
            Widget.fn.destroy.call(that);

            kendo.destroy(that.element);
        },
        value: function (value) {
            var datepicker = this.element.data("kendoDatePicker");

            if (value === undefined) {
                return datepicker.value();
            }

            datepicker.value(value);
        }
    });

    ui.plugin(MaskedDatePicker);

})(window.kendo.jQuery);