﻿/// <reference path="../models/Lead.js" />
/// <reference path="../common-util.js" />
/// <reference path="~/Scripts/jquery-1.3.2.min.js" />
/// <reference path="~/Kendo/vsdoc/kendo.web.min.intellisense.js" />
/// <reference path="~/Kendo/vsdoc/kendo.all.min.intellisense.js" />
/// <reference path="~/Scripts/Advantage.Client.js" />
/// <reference path="~/Scripts/Advantage.Client.MasterPage.js" />
(function ($, kendo, datasources) {


    //----------------------------------------------------------------------------------------
    // View Model that represents the data to be bound to the 
    // view
    //----------------------------------------------------------------------------------------
    var viewModel = kendo.observable({
        XPOST_LEAD_PHONE_MODEL: "../proxy/api/Lead/PostLeadPhoneContact/",
        XDELETE_LEAD_PHONE_MODEL: "../proxy/api/Lead/DeleteLeadPhoneContact/",
        XPOST_LEAD_EMAIL_MODEL: "../proxy/api/Lead/PostLeadEmailContact/",
        XDELETE_LEAD_EMAIL_MODEL: "../proxy/api/Lead/DeleteLeadEmailContact/",
        XPOST_LEAD_ADDRESS_MODEL: "../proxy/api/Lead/PostLeadAddressContact/",
        XDELETE_LEAD_ADDRESS_MODEL: "../proxy/api/Lead/DeleteLeadAddressContact/",
        XPOST_LEAD_COMMENTS_MODEL: "../proxy/api/Lead/UpdateLeadComment/",
        XGET_LEAD_OTHER_CONTACTS: "/proxy/api/Lead/GetLeadOtherContacts/",

        XPOST_LEAD_OTHER_CONTACTS: "../proxy/api/Lead/PostLeadOtherContacts/",
        XPOST_LEAD_OTHER_CONTACTS_PHONE: "../proxy/api/Lead/PostLeadOtherContactsPhone/",
        XPOST_LEAD_OTHER_CONTACTS_EMAIL: "../proxy/api/Lead/PostLeadOtherContactsEmail/",
        XPOST_LEAD_OTHER_CONTACTS_ADDRESS: "../proxy/api/Lead/PostLeadOtherContactsAddress/",
        XPOST_LEAD_OTHER_CONTACTS_COMMENTS: "../proxy/api/Lead/PostLeadOtherContactsComments/",

        XDELETE_LEAD_OTHER_CONTACTS: "../proxy/api/Lead/DeleteLeadOtherContacts/",
        XDELETE_LEAD_OTHER_CONTACTS_PHONE: "../proxy/api/Lead/DeleteLeadOtherContactsPhone/",
        XDELETE_LEAD_OTHER_CONTACTS_EMAIL: "../proxy/api/Lead/DeleteLeadOtherContactsEmail/",
        XDELETE_LEAD_OTHER_CONTACTS_ADDRESS: "../proxy/api/Lead/DeleteLeadOtherContactsAddress/",

        SAVE_MODE_ADD_OTHER_INFO: "SAVE_MODE_ADD_OTHER_INFO ",
        SAVE_MODE_EDIT_OTHER_INFO: "SAVE_MODE_EDIT_OTHER_INFO",
        SAVE_MODE_EDIT_PHONE_NUMBER: "SAVE_MODE_EDIT_PHONE_NUMBER",
        SAVE_MODE_EDIT_EMAIL: "SAVE_MODE_EDIT_EMAIL",
        SAVE_MODE_ADDRESS: "SAVE_MODE_ADDRESS",

        leadPhonesDataSource: datasources.leadPhonesDataSource,
        leadEmailDataSource: datasources.leadEmailDataSource,
        leadAddressDataSource: datasources.leadAddressDataSource,
        phoneTypesDataSource: datasources.phoneTypesDataSource,
        addressTypesDatSource: datasources.addressTypesDatSource,
        emailTypesDataSource: datasources.emailTypesDataSource,
        countriesDataSource: datasources.countriesDataSource,
        statesDataSource: datasources.statesDataSource,
        statusDataSource: datasources.statusDataSource,
        countiesDataSource: datasources.countiesDataSource,
        contactTypesDataSource: datasources.contactTypesDataSource,
        prefixDataSource: datasources.prefixDataSource,
        sufixDataSource: datasources.sufixDataSource,
        relationshipsDataSource: datasources.relationshipsDataSource,
        otherContactsDataSource: datasources.otherContactsDataSource,
        pageValidator: undefined,

        shouldShowException: false,
        initializeLeadPhoneGrid: function () {
            viewModel.showLoading(true);

            var uri = XMASTER_GET_BASE_URL + "/proxy/api/Lead/GetLeadContactInfo";
            var filter = { 'LeadGuid': $("#hdnLeadId").val() };
            var studentContactPage = ($.QueryString["resid"] === "155");

            var requestData2 = $.getJSON(uri, filter, function (data) {
                viewModel.set("leadPhonesDataSource", data.LeadPhonesList);
                viewModel.set("leadEmailDataSource", data.LeadEmailList);
                viewModel.set("leadAddressDataSource", data.LeadAddressList);
                viewModel.set("phoneTypesDataSource", data.PhoneTypes);
                viewModel.set("addressTypesDatSource", data.AddressTypes);
                viewModel.set("emailTypesDataSource", data.EmailTypes);
                viewModel.set("countriesDataSource", data.Countries);
                viewModel.set("statesDataSource", data.States);
                viewModel.set("statusDataSource", data.Statuses);
                viewModel.set("countiesDataSource", data.Counties);
                viewModel.set("contactTypesDataSource", data.ContactTypes);
                viewModel.set("prefixDataSource", data.Prefixes);
                viewModel.set("sufixDataSource", data.Sufixes);
                viewModel.set("relationshipsDataSource", data.Relationships);

                $("#leadCommentsText").val(data.Comments);

                $("#leadPhonesGrid").kendoGrid({
                    dataSource: viewModel.leadPhonesDataSource,
                    dataBound: function () {
                        viewModel.showLoading(false);
                    },
                    sortable: false,
                    selectable: false,
                    scrollable: true,
                    resizable: true,
                    pageable: false,
                    noRecords: true,
                    columns: [
                        { field: "ID", title: "Id", hidden: true },
                        { field: "StatusId", title: "StatusId", hidden: true },
                        { field: "PhoneTypeId", title: "PhoneTypeId", hidden: true },
                        { field: "PhoneType", title: "Type", width: 80, attributes: { style: "text-align:left;" } },
                        { field: "Phone", title: "Phone", width: 120, attributes: { style: "text-align:left;" } },
                        { field: "Extension", title: "Ext", width: 60, attributes: { style: "text-align:left;" } },
                        { field: "Status", title: "Status", width: 60, attributes: { style: "text-align:center;" } },
                        { field: "ShowOnLeadPage", title: "Show on Lead Page", width: 120, hidden: studentContactPage, attributes: { style: "text-align:center;" } },
                        { field: "Best", title: "Best", width: 50,   attributes: { style: "text-align:center;" } },
                        { field: "Edit", title: "", attributes: { style: "text-align:center;" }, template: "<a class='updatePhoneClick pointer' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-change-manually font-blue-darken-1'></span></a>" },
                        { field: "Delete", title: "", attributes: { style: "text-align:center;" }, template: "<a class='deletePhoneClick pointer' data-ElementId='#=data.ID#' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-close font-red'></span></a>" },
                        { field: "IsForeignPhone", title: "IsForeignPhone", hidden: true }
                    ]

                });

                var grid = $("#leadPhonesGrid").data("kendoGrid");
                grid.table.on("click", ".updatePhoneClick", viewModel.initAddEditPhoneWindow);
                grid.table.on("click", ".deletePhoneClick", viewModel.deletePhone);


                $("#leadEmailGrid").kendoGrid({
                    dataSource: viewModel.leadEmailDataSource,
                    dataBound: function () {

                    },
                    sortable: false,
                    selectable: false,
                    scrollable: true,
                    resizable: true,
                    pageable: false,
                    noRecords: true,
                    width: 400,
                    columns: [
                        { field: "ID", title: "Id", hidden: true },
                        { field: "StatusId", title: "StatusId", hidden: true },
                        { field: "EmailTypeId", title: "EmailTypeId", hidden: true },
                        { field: "EmailType", title: "Type", width: 50, attributes: { style: "text-align:left;" } },
                        { field: "Email", title: "", width: 90, attributes: { style: "text-align:left;" } },
                        { field: "Status", title: "Status", width: 40, attributes: { style: "text-align:center;" } },
                        { field: "IsShowOnLeadPage", title: "Show on Lead Page", width: 80, hidden: studentContactPage, attributes: { style: "text-align:center;" } },
                        { field: "IsPreferred", title: "Best", width: 30, attributes: { style: "text-align:center;" }, template: kendo.template($("#templateIsPrefered").html()) },
                        { field: "Edit", title: "", width: 40, attributes: { style: "text-align:center;" }, template: "<a class='updateEmailClick pointer'  href='javascript:void(0);'> <span class='k-icon k-i-change-manually font-blue-darken-1'></span></a>" },
                        { field: "Delete", title: "", width: 40, attributes: { style: "text-align:center;" }, template: "<a class='deleteEmailClick pointer' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-close font-red'></span></a>" }
                    ]

                });

                var leadEmailGrid = $("#leadEmailGrid").data("kendoGrid");
                leadEmailGrid.table.on("click", ".updateEmailClick", viewModel.initAddEditEmailWindow);
                leadEmailGrid.table.on("click", ".deleteEmailClick", viewModel.deleteEmail);

                $("#leadAddressGrid").kendoGrid({
                    dataSource: viewModel.leadAddressDataSource,
                    dataBound: function () {

                    },
                    sortable: false,
                    selectable: false,
                    scrollable: true,
                    resizable: true,
                    pageable: false,
                    noRecords: true,
                    columns: [
                    { field: "ID", title: "Id", hidden: true, attributes: { data_Column: "ID" } },
                        { field: "StatusId", title: "StatusId", hidden: true, attributes: { data_Column: "StatusId" } },
                        { field: "AddressTypeId", title: "AddressTypeId", hidden: true, attributes: { data_Column: "AddressTypeId" } },
                        { field: "CountryId", title: "CountryId", hidden: true, attributes: { data_Column: "CountryId" } },
                        { field: "StateId", title: "StateId", hidden: true, attributes: { data_Column: "StateId" } },
                        { field: "CountyId", title: "CountyId", hidden: true, attributes: { data_Column: "CountyId" } },
                        { field: "City", title: "City", hidden: true, attributes: { data_Column: "City" } },
                        { field: "State", title: "State", hidden: true, attributes: { data_Column: "State" } },
                        { field: "IsInternational", title: "IsInternational", hidden: true, attributes: { data_Column: "IsInternational" } },
                        { field: "ZipCode", title: "Zip Code", hidden: true, attributes: { data_Column: "ZipCode" } },
                        { field: "Address1", title: "Address1", hidden: true, attributes: { data_Column: "Address1" } },
                        { field: "Address2", title: "Address2", hidden: true, attributes: { data_Column: "Address2" } },
                        { field: "AddressType", title: "Type", width: 50, attributes: { style: "text-align:left;" } },
                        { field: "Address1", title: "Address", width: 180, attributes: { style: "text-align:left;", data_Column: "Address" }, template: '#if (data.Address1 !== null && data.Address1 !== ""){ # <span>#=data.Address1#</span> #}# #if (data.Address2 !== null && data.Address2 !== ""){ # <span>,#=data.Address2#</span> #}#' },
                        { field: "City", title: "City, State, Zip", width: 130, attributes: { style: "text-align:left;" }, template: '#=data.City# #if (data.State !== null){ # <span>, #=data.State#</span> #}# #if (data.ZipCode !== null && data.ZipCode !== ""){ # <span>,#=data.ZipCode#</span> #}#' },
                        { field: "County", title: "County", width: 60, attributes: { style: "text-align:left;", data_Column: "County" } },
                        { field: "Country", title: "Country", width: 60, attributes: { style: "text-align:left;", data_Column: "Country" } },
                        { field: "Status", title: "Status", width: 50, attributes: { style: "text-align:center;" } },
                        { field: "IsMailingAddress", title: "Is Mailing Address", width: 120, attributes: { style: "text-align:center;", data_Column: "IsMailingAddress" } },
                        { field: "IsShowOnLeadPage", title: "Show on Lead Page", width: 120, hidden: studentContactPage,  attributes: { style: "text-align:center;", data_Column: "IsShowOnLeadPage" } },
                        { field: "Edit", title: "", width: 50, attributes: { style: "text-align:center;" }, template: "<a class='updateAddressClick pointer' data-ElementId='#=data.ID#'  href='javascript:void(0);'> <span class='k-icon k-i-change-manually font-blue-darken-1'></span></a>" },
                        { field: "Delete", title: "", width: 50, attributes: { style: "text-align:center;" }, template: "<a class='deleteAddressClick pointer' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-close font-red'></span></a>" }
                    ]

                });

                var leadAddressGrid = $("#leadAddressGrid").data("kendoGrid");
                leadAddressGrid.table.on("click", ".updateAddressClick", viewModel.initAddEditAddressWindow);
                leadAddressGrid.table.on("click", ".deleteAddressClick", viewModel.deleteAddress);


            });

            uri = XMASTER_GET_BASE_URL + viewModel.XGET_LEAD_OTHER_CONTACTS;
            var requestOtherContacts = $.getJSON(uri, filter, function (data) {
                viewModel.set("otherContactsDataSource", data.Data);

                $("#leadOtherContactsGrid").kendoGrid({
                    dataSource: viewModel.otherContactsDataSource,
                    dataBound: function () {

                    },
                    sortable: false,
                    selectable: false,
                    scrollable: true,
                    resizable: true,
                    pageable: false,
                    noRecords: true,
                    columns: [
                        { field: "OtherContactId", title: "OtherContactId", hidden: true, attributes: { data_Column: "ID" } },
                        { field: "TypeId", title: "ContactTypeId", hidden: true, attributes: { data_Column: "TypeId" } },
                        { field: "RelationshipId", title: "RelationshipId", hidden: true, attributes: { data_Column: "RelationshipId" } },
                        { field: "StatusId", title: "StatusId", hidden: true, attributes: { data_Column: "StatusId" } },
                        { field: "FirstName", title: "FirstName", hidden: true, attributes: { data_Column: "FirstName" } },
                        { field: "LastName", title: "LastName", hidden: true, attributes: { data_Column: "LastName" } },
                        { field: "Prefix", title: "Prefix", hidden: true, attributes: { data_Column: "Prefix" } },
                        { field: "ContactType", title: "Type", width: 80, attributes: { style: "text-align:left;", data_Column: "Type" } },
                        { field: "Relationship", title: "Relationship", width: 50, attributes: { style: "text-align:left;", data_Column: "Relationship" } },
                        { field: "FullName", title: "Name", width: 120, attributes: { style: "text-align:left;", data_Column: "Name" }, template: '<a href="javascript:void(0);" class="viewOtherContactDetails" data-elementId="#=data.OtherContactId#">#=data.FullName#</a>' },
                        { field: "Status", title: "Status", width: 50, attributes: { style: "text-align:left;", data_Column: "Status" } }
                    ]

                });

                var leadOtherContactsGrid = $("#leadOtherContactsGrid").data("kendoGrid");
                leadOtherContactsGrid.table.on("click", ".viewOtherContactDetails", viewModel.viewOtherContactDetails);
            });
        },
        initAddEditPhoneWindow: function (e) {

            var isEditMode = $(e.currentTarget).hasClass("updatePhoneClick");
            $(".requiredTooltip").hide();

            $("#phoneForm").kendoWindow({
                //animation: false,
                modal: true,
                width: "600",
                title: "Add / Edit Phone Number",
                actions: ["close"],
                resizable: false,
                draggable: true
            });

            $("#phoneForm").data("kendoWindow").center().open();

            if ($("#ddlPhoneType").data("kendoDropDownList") == undefined) {
                $("#ddlPhoneType").kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "--Select--",
                    dataSource: viewModel.phoneTypesDataSource
                });
                $("#ddlPhoneStatus").kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "--Select--",
                    dataSource: viewModel.statusDataSource
                });
            }

            if ($("#txtPhoneExtension").data("kendoMaskedTextBox") === undefined) {
                $("#txtPhoneExtension").kendoMaskedTextBox({
                    mask: "0000000000"
                });
            }

            $("#txtPhone").unbind("keyup");

            $("#chkIsForeign").change(function (e) {
                var maskedtextbox = $("#txtPhone").data("kendoMaskedTextBox");
                if ($(e.currentTarget).prop("checked")) {
                    // detach events
                    if (maskedtextbox != undefined) {
                        maskedtextbox.setOptions({ mask: '+9999999999999999999999999' });
                    }
                    $("#txtPhone").attr("data-role", "international");
                } else {
                    $("#txtPhone").removeAttr("data-role");
                    $("#txtPhone").unbind("keyup");
                    if ($("#txtPhone").data("kendoMaskedTextBox") === undefined) {
                        $("#txtPhone")
                            .kendoMaskedTextBox({
                                mask: "(999)-000-0000"
                            });
                    } else {
                        maskedtextbox.setOptions({ mask: '(000)-000-0000' });
                    }
                }
            });
            $("#chkPhoneBest").change(function () {
                $("#chkPhoneShowOnLeadPage").prop("checked", $("#chkPhoneBest").prop("checked"));
            });
            $("#chkPhoneShowOnLeadPage").change(function () {
                if ($("#chkPhoneShowOnLeadPage").prop("checked")) {
                    $("#chkPhoneBest").prop("checked", false);
                }
            });
            if (isEditMode) {
                //Load Current Data to Edit Form
                var row = $(e.currentTarget).closest("tr");
                if (row != undefined) {
                    var columns = $(row).find("td");
                    $("#hdnLeadPhoneId").val($(columns)[0].innerText);
                    $("#ddlPhoneStatus").data("kendoDropDownList").value($(columns)[1].innerText);
                    $("#ddlPhoneType").data("kendoDropDownList").value($(columns)[2].innerText);
                    $("#ddlPhoneType, #ddlPhoneStatus").data("kendoDropDownList").trigger("change");
                    $("#txtPhone").val($(columns)[4].innerText);
                    $("#txtPhoneExtension").val($(columns)[5].innerText);
                    var showOnLeadPage = $(columns)[7].innerText === "" ? false : $(columns)[7].innerText.trim() === "No" ? false : $(columns)[7].innerText.trim() === "Yes" ? true : false;
                    var isBest = $(columns)[8].innerText === "" ? false : $(columns)[8].innerText.trim() === "No" ? false : $(columns)[8].innerText.trim() === "Yes" ? true : false;
                    var IsForeignPhone = $(columns)[11].innerText === "" ? false : $(columns)[11].innerText.trim() === "false" ? false : $(columns)[11].innerText.trim() === "true" ? true : false;
                    $("#chkPhoneShowOnLeadPage").prop("checked", showOnLeadPage);
                    $("#chkPhoneBest").prop("checked", isBest);
                    $("#chkIsForeign").prop("checked", IsForeignPhone);
                    if (!IsForeignPhone) {
                        $("#txtPhone")
                            .kendoMaskedTextBox({
                                mask: "(999)-000-0000"
                            });
                    } else {
                        $("#txtPhone")
                            .kendoMaskedTextBox({
                                mask: '+9999999999999999999999999'
                            });
                    }
                }
            } else {
                $("#hdnLeadPhoneId").val("");
                $("#ddlPhoneStatus").data("kendoDropDownList").select(0);
                $("#ddlPhoneType").data("kendoDropDownList").select(0);
                $("#ddlPhoneType, #ddlPhoneStatus").data("kendoDropDownList").trigger("change");
                $("#txtPhone").val("");
                $("#txtEmail").val("");
                $("#txtPhoneExtension").val("");
                $("#chkPhoneShowOnLeadPage").prop("checked", false);
                $("#chkPhoneBest").prop("checked", false);
                $("#chkIsForeign").prop("checked", false);
                $("#txtPhone").kendoMaskedTextBox({
                    mask: "(999)-000-0000"
                });
            }
            $("#btnPhoneSave").unbind("click");
            $("#btnPhoneSave").bind("click", function () {
                viewModel.validateForm("phoneForm", function () { viewModel.saveLeadPone(); });
            });

            $("#btnPhoneCancel").unbind("click");
            $("#btnPhoneCancel").bind("click", function () {
                var form = $("#phoneForm").data("kendoWindow");
                if (form !== undefined && form !== null) {
                    form.close();
                }
                MasterPage.advantageValitorReset("phoneForm");
            });

            MasterPage.ADVANTAGE_VALIDATOR("phoneForm");
            viewModel.clearRequiredDropdown("#ddlPhoneType,#ddlPhoneStatus");
        },
        deletePhone: function (e) {
            var isEditMode = $(e.currentTarget).hasClass("deletePhoneClick");
            if (isEditMode) {
                var onConfirm = function () {
                    viewModel.showLoading(true);

                    var leadInfo = new LeadContactInputModel();
                    var row = $(e.currentTarget).closest("tr");
                    var columns = $(row).find("td");
                    leadInfo.LeadId = $("#hdnLeadId").val();
                    leadInfo.UserId = $("#hdnUserId").val();
                    leadInfo.CampusId = XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;
                    leadInfo.ID = $(columns)[0].innerText;

                    $.ajax({
                        url: viewModel.XDELETE_LEAD_PHONE_MODEL,
                        data: JSON.stringify(leadInfo),
                        type: "post",
                        timeout: 20000,
                        dataType: "json",
                        contentType: "application/json"
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        viewModel.showLoading(false);
                        viewModel.showErrorNotification(textStatus + ": " + errorThrown);
                    }).done(function (msg) {
                        viewModel.showLoading(false);
                        if (msg.ResponseCode === 200) {
                            viewModel.rebindDataSources(msg.Data);
                            viewModel.showSuccessDeletedNotification();
                        } else {
                            viewModel.showErrorNotification(msg.ResponseMessage);
                        }
                    });

                };
                viewModel.confirmNotification(function () { onConfirm(); });
            }
        },
        initAddEditEmailWindow: function (e) {
            var isEditMode = $(e.currentTarget).hasClass("updateEmailClick");
            $(".requiredTooltip").hide();

            $("#emailForm").kendoWindow({
                //animation: false,
                modal: true,
                width: "400",
                title: "Add / Edit Email",
                actions: ["close"],
                resizable: false,
                draggable: true
            });
            $("#emailForm").data("kendoWindow").center().open();

            if ($("#ddlEmailType").data("kendoDropDownList") == undefined) {
                $("#ddlEmailType").kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "--Select--",
                    dataSource: viewModel.emailTypesDataSource
                });
                $("#ddlEmailStatus").kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "--Select--",
                    dataSource: viewModel.statusDataSource
                });
            }
            $("#chkEmailBest").unbind("change");
            $("#chkEmailBest").bind("change", function () {
                $("#chkEmailShowOnLeadPage").prop("checked", $("#chkEmailBest").prop("checked"));
            });
            $("#chkEmailShowOnLeadPage").unbind("change");
            $("#chkEmailShowOnLeadPage").bind("change", function () {
                if ($("#chkEmailShowOnLeadPage").prop("checked")) {
                    $("#chkEmailBest").prop("checked", false);
                }
            });

            if (isEditMode) {
                //Load Current Data to Edit Form
                var row = $(e.currentTarget).closest("tr");
                if (row != undefined) {
                    var columns = $(row).find("td");
                    $("#hdnLeadEmailId").val($(columns)[0].innerText);
                    $("#ddlEmailStatus").data("kendoDropDownList").value($(columns)[1].innerText);
                    $("#ddlEmailType").data("kendoDropDownList").value($(columns)[2].innerText);
                    $("#ddlEmailType, #ddlEmailStatus").data("kendoDropDownList").trigger("change");
                    $("#txtEmail").val($(columns)[4].innerText);
                    var showOnLeadPage = $(columns)[6].innerText === "" ? false : $(columns)[6].innerText.trim() === "No" ? false : $(columns)[6].innerText.trim() === "Yes" ? true : false;
                    var isBest = $(columns)[7].innerText === "" ? false : $(columns)[7].innerText.trim() === "No" ? false : $(columns)[7].innerText.trim() === "Yes" ? true : false;
                    $("#chkEmailShowOnLeadPage").prop("checked", showOnLeadPage);
                    $("#chkEmailBest").prop("checked", isBest);
                }
            } else {
                $("#hdnLeadEmailId").val("");
                $("#ddlEmailStatus").data("kendoDropDownList").select(0);
                $("#ddlEmailType").data("kendoDropDownList").select(0);
                $("#ddlEmailType, #ddlEmailStatus").data("kendoDropDownList").trigger("change");
                $("#txtEmail").val("");
                $("#chkEmailShowOnLeadPage").prop("checked", false);
                $("#chkEmailBest").prop("checked", false);
            }
            $("#btnEmailSave").unbind("click");
            $("#btnEmailSave").bind("click", function () {
                viewModel.validateForm("emailForm", function () { viewModel.saveLeadEmail(); });
            });
            $("#btnEmailCancel").unbind("click");
            $("#btnEmailCancel").bind("click", function () {
                $("#emailForm").data("kendoWindow").close();
                MasterPage.advantageValitorReset("emailForm");
            });

            MasterPage.ADVANTAGE_VALIDATOR("emailForm");
            viewModel.clearRequiredDropdown("#ddlEmailType,#ddlEmailStatus");
        },
        deleteEmail: function (e) {
            var isEditMode = $(e.currentTarget).hasClass("deleteEmailClick");
            if (isEditMode) {
                viewModel.confirmNotification(function () {
                    viewModel.showLoading(true);

                    var leadInfo = new LeadContactInputModel();
                    var row = $(e.currentTarget).closest("tr");
                    var columns = $(row).find("td");
                    leadInfo.LeadId = $("#hdnLeadId").val();
                    leadInfo.UserId = $("#hdnUserId").val();
                    leadInfo.CampusId = XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;
                    leadInfo.ID = $(columns)[0].innerText;

                    $.ajax({
                        url: viewModel.XDELETE_LEAD_EMAIL_MODEL,
                        data: JSON.stringify(leadInfo),
                        type: "post",
                        timeout: 20000,
                        dataType: "json",
                        contentType: "application/json"
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        viewModel.showLoading(false);
                        viewModel.showErrorNotification(textStatus + ": " + errorThrown);
                    }).done(function (msg) {
                        viewModel.showLoading(false);
                        if (msg.ResponseCode === 200) {
                            viewModel.rebindDataSources(msg.Data);
                            viewModel.showSuccessDeletedNotification();
                        } else {
                            viewModel.showErrorNotification(msg.ResponseMessage);
                        }
                    });
                });
            }
        },
        initAddEditAddressWindow: function (e) {
            var isEditMode = $(e.currentTarget).hasClass("updateAddressClick");
            $("#addressForm").kendoWindow({
                //animation: false,
                modal: true,
                width: "600",
                title: "Add / Edit Address",
                actions: ["close"],
                resizable: false,
                draggable: true
            });

            $("#addressForm").data("kendoWindow").center().open();

            MasterPage.advantageValitorReset("addressForm");

            if ($("#ddlAddressType").data("kendoDropDownList") == undefined) {
                $("#ddlAddressType").kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "--Select--",
                    dataSource: viewModel.addressTypesDatSource
                });
                $("#ddlCountry").kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "--Select--",
                    dataSource: viewModel.countriesDataSource
                });
                $("#ddlState").kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "--Select--",
                    dataSource: viewModel.statesDataSource
                });
                $("#ddlAddressStatus").kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "--Select--",
                    dataSource: viewModel.statusDataSource
                });
                $("#ddlCounty").kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "--Select--",
                    dataSource: viewModel.countiesDataSource
                });
            }
            $("#txtZipCode").kendoMaskedTextBox({ mask: "00000" });

            $("#chkIsInternational").unbind("change");
            $("#chkIsInternational").bind("change", function () {
                var maskedtextbox = $("#txtZipCode").data("kendoMaskedTextBox");
                if ($("#chkIsInternational").prop("checked")) {
                    $("#txtState, #txtCountry").removeClass("hidden").removeAttr("required");
                    $("#txtCounty").removeClass("hidden");
                    $(".txtState_required").addClass("hidden");
                    $("#ddlCountry").data("kendoDropDownList").wrapper.hide();
                    $("#ddlState").data("kendoDropDownList").wrapper.hide();
                    $("#ddlCounty").data("kendoDropDownList").wrapper.hide();
                    $("#ddlCountry, #ddlState, #txtState").removeAttr("required");
                    maskedtextbox.setOptions({ mask: '' });
                } else {
                    $("#txtState, #txtCountry").addClass("hidden").removeAttr("required");
                    $("#ddlCountry, #ddlState, #txtState").attr("required", "required");
                    $(".txtState_required").removeClass("hidden");
                    $("#txtCounty").addClass("hidden");
                    $("#ddlCountry").data("kendoDropDownList").wrapper.show();
                    $("#ddlState").data("kendoDropDownList").wrapper.show();
                    $("#ddlCounty").data("kendoDropDownList").wrapper.show();
                    maskedtextbox.setOptions({ mask: '00000' });
                }
            });
            if (isEditMode) {
                //Load Current Data to Edit Form
                var row = $(e.currentTarget).closest("tr");
                if (row != undefined) {
                    var columns = $(row).find("td");
                    $("#hdnLeadAddressId").val($(row).find('[data_column="ID"]').text());
                    $("#ddlAddressStatus").data("kendoDropDownList").value($(row).find('[data_column="StatusId"]').text());
                    $("#ddlAddressType").data("kendoDropDownList").value($(row).find('[data_column="AddressTypeId"]').text());

                    var isInternational = $(row).find('[data_column="IsInternational"]').text() === "" ? false : $(row).find('[data_column="IsInternational"]').text() === "false" ? false : $(row).find('[data_column="IsInternational"]').text() === "true" ? true : false;
                    $("#chkIsInternational").prop("checked", isInternational);
                    if (!isInternational) {
                        $("#ddlCountry").data("kendoDropDownList").value($(row).find('[data_column="CountryId"]').text());
                        $("#ddlState").data("kendoDropDownList").value($(row).find('[data_column="StateId"]').text());
                        $("#ddlCounty").data("kendoDropDownList").value($(row).find('[data_column="CountyId"]').text());
                        $("#ddlCountry, #ddlState #ddlCounty").data("kendoDropDownList").trigger("change");
                        $("#txtState, #txtCountry, #txtState").addClass("hidden").removeAttr("required");
                        $(".txtState_required").removeClass("hidden");
                        $("#txtCounty").addClass("hidden");
                        $("#ddlCountry, #ddlState").attr("required", "");
                        $("#ddlCountry").data("kendoDropDownList").wrapper.show();
                        $("#ddlState").data("kendoDropDownList").wrapper.show();
                        $("#ddlCounty").data("kendoDropDownList").wrapper.show();
                    }
                    else {
                        $("#txtState").val($(row).find('[data_column="State"]').text());
                        $("#txtCounty").val($(row).find('[data_column="County"]').text());
                        $("#txtCountry").val($(row).find('[data_column="Country"]').text());
                        $("#txtState, #txtCountry").removeClass("hidden").attr("required", "");
                        $("#ddlCountry, #ddlState, #txtState").removeAttr("required");
                        $(".txtState_required").addClass("hidden");
                        $("#txtCounty").removeClass("hidden");
                        $("#ddlCountry").data("kendoDropDownList").wrapper.hide();
                        $("#ddlState").data("kendoDropDownList").wrapper.hide();
                        $("#ddlCounty").data("kendoDropDownList").wrapper.hide();
                    }

                    $("#ddlAddressType, #ddlAddressStatus").data("kendoDropDownList").trigger("change");

                    $("#txtAddress1").val($(row).find('[data_column="Address1"]').text());
                    $("#txtAddress2").val($(row).find('[data_column="Address2"]').text());

                    $("#txtCity").val($(row).find('[data_column="City"]').text());
                    $("#txtZipCode").val($(row).find('[data_column="ZipCode"]').text());

                    var isMailingAddress = $(row).find('[data_column="IsMailingAddress"]').text().trim() === "" ? false : $(row).find('[data_column="IsMailingAddress"]').text().trim() === "No" ? false : $(row).find('[data_column="IsMailingAddress"]').text().trim() === "Yes" ? true : false;
                    var showOnLeadPage = $(row).find('[data_column="IsShowOnLeadPage"]').text().trim() === "" ? false : $(row).find('[data_column="IsShowOnLeadPage"]').text().trim() === "No" ? false : $(row).find('[data_column="IsShowOnLeadPage"]').text().trim() === "Yes" ? true : false;

                    $("#chkIsMaillingAddress").prop("checked", isMailingAddress);
                    $("#chkAddressShowOnLeadPage").prop("checked", showOnLeadPage);
                }
            } else {
                $("#hdnLeadAddressId").val("");
                $("#ddlAddressStatus").data("kendoDropDownList").select(0);
                $("#ddlAddressType").data("kendoDropDownList").select(0);
                $("#ddlCountry").data("kendoDropDownList").select(0);
                $("#ddlState").data("kendoDropDownList").select(0);
                $("#ddlCounty").data("kendoDropDownList").select(0);
                $("#ddlAddressType, #ddlAddressStatus, #ddlCountry, #ddlState").data("kendoDropDownList").trigger("change");
                $("#txtAddress1").val("");
                $("#txtAddress2").val("");
                $("#txtCity").val("");
                $("#txtZipCode").val("");
                $("#txtState, #txtCountry").val("");
                $("#chkIsMaillingAddress").prop("checked", false);
                $("#chkAddressShowOnLeadPage").prop("checked", false);
                $("#chkIsInternational").prop("checked", false);
                $("#txtState, #txtCountry").addClass("hidden").removeAttr("required");
                $("#txtCounty").addClass("hidden");
                $("#ddlCountry").data("kendoDropDownList").wrapper.show();
                $("#ddlState").data("kendoDropDownList").wrapper.show();
                $("#ddlCounty").data("kendoDropDownList").wrapper.show();
                $(".txtState_required").removeClass("hidden");
                $("#ddlState").attr("required", "required");
            }

            $("#btnAddressSave").unbind("click");
            $("#btnAddressSave").bind("click", function () {
                viewModel.validateForm("addressForm", function () { viewModel.saveLeadAddress(); });
            });

            $("#btnAddressCancel").unbind("click");
            $("#btnAddressCancel").bind("click", function () {
                $("#addressForm").data("kendoWindow").close();
                MasterPage.advantageValitorReset("addressForm");
            });
            MasterPage.ADVANTAGE_VALIDATOR("addressForm");
            viewModel.clearRequiredDropdown("#ddlAddressType,#ddlAddressStatus,#ddlCountry,#ddlState");
        },
        saveLeadPone: function () {
            var leadInfo = new LeadContactInputModel();

            leadInfo.LeadId = $("#hdnLeadId").val();
            leadInfo.UserId = $("#hdnUserId").val();
            leadInfo.CampusId = XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;
            leadInfo.TypeId = $("#ddlPhoneType").val();
            leadInfo.StatusId = $("#ddlPhoneStatus").val();
            leadInfo.Phone = $("#txtPhone").val();
            var txtPhoneExtension = $("#txtPhoneExtension").data("kendoMaskedTextBox");
            leadInfo.Extension = txtPhoneExtension.value().trim().replace("_", "");
            leadInfo.IsBest = $("#chkPhoneBest").prop("checked");
            leadInfo.ShowOnLeadPage = $("#chkPhoneShowOnLeadPage").prop("checked");
            leadInfo.ID = $("#hdnLeadPhoneId").val();
            leadInfo.IsForeignPhone = $("#chkIsForeign").prop("checked");

            viewModel.showLoading(true);

            $.ajax({
                url: viewModel.XPOST_LEAD_PHONE_MODEL,
                data: JSON.stringify(leadInfo),
                type: "post",
                timeout: 20000,
                dataType: "json",
                contentType: "application/json"
            }).fail(function (jqXHR, textStatus, errorThrown) {
                viewModel.showLoading(false);
                viewModel.showErrorNotification(textStatus + ": " + errorThrown);
            }).done(function (msg) {
                viewModel.showLoading(false);
                if (msg.ResponseCode === 200) {
                    $("#phoneForm").data("kendoWindow").center().close();
                    viewModel.rebindDataSources(msg.Data);
                    viewModel.showSuccessNotification();
                } else {
                    if ($.QueryString["resid"] === "155" && msg.ResponseMessage.indexOf("can be set as shown on lead page and only") >= 0) {
                        msg.ResponseMessage = msg.ResponseMessage.replace("2 phone numbers can be set as shown on lead page and only ", "");
                    }
                    viewModel.showErrorNotification(msg.ResponseMessage);
                }
            });

        },
        saveLeadEmail: function () {
            var leadInfo = new LeadContactInputModel();

            leadInfo.LeadId = $("#hdnLeadId").val();
            leadInfo.UserId = $("#hdnUserId").val();
            leadInfo.CampusId = XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;
            leadInfo.ID = $("#hdnLeadEmailId").val();
            leadInfo.StatusId = $("#ddlEmailStatus").data("kendoDropDownList").value();
            leadInfo.TypeId = $("#ddlEmailType").data("kendoDropDownList").value();
            leadInfo.Email = $("#txtEmail").val();
            leadInfo.ShowOnLeadPage = $("#chkEmailShowOnLeadPage").prop("checked");
            leadInfo.IsBest = $("#chkEmailBest").prop("checked");

            viewModel.showLoading(true);

            $.ajax({
                url: viewModel.XPOST_LEAD_EMAIL_MODEL,
                data: JSON.stringify(leadInfo),
                type: "post",
                timeout: 20000,
                dataType: "json",
                contentType: "application/json"
            }).fail(function (jqXHR, textStatus, errorThrown) {
                viewModel.showLoading(false);
                viewModel.showErrorNotification(textStatus + ": " + errorThrown);
            }).done(function (msg) {
                viewModel.showLoading(false);
                if (msg.ResponseCode === 200) {
                    $("#emailForm").data("kendoWindow").center().close();
                    viewModel.rebindDataSources(msg.Data);
                    viewModel.showSuccessNotification();
                } else {
                    if ($.QueryString["resid"] === "155" && msg.ResponseMessage.indexOf("can be set as shown on lead page and only") >= 0) {
                        msg.ResponseMessage = msg.ResponseMessage.replace("1 email address can be set as shown on lead page and only ", "");
                    }
                    viewModel.showErrorNotification(msg.ResponseMessage);
                }
            });

        },
        saveLeadAddress: function () {
            var leadInfo = new LeadAddressInputModel();

            leadInfo.LeadId = $("#hdnLeadId").val();
            leadInfo.UserId = $("#hdnUserId").val();
            leadInfo.CampusId = XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;
            leadInfo.ID = $("#hdnLeadAddressId").val();
            leadInfo.StatusId = $("#ddlAddressStatus").data("kendoDropDownList").value();
            leadInfo.TypeId = $("#ddlAddressType").data("kendoDropDownList").value();
            leadInfo.ShowOnLeadPage = $("#chkAddressShowOnLeadPage").prop("checked");
            leadInfo.IsMaillingAddress = $("#chkIsMaillingAddress").prop("checked");
            leadInfo.Address1 = $("#txtAddress1").val();
            leadInfo.Address2 = $("#txtAddress2").val();
            leadInfo.City = $("#txtCity").val();
            leadInfo.ZipCode = $("#txtZipCode").val();

            leadInfo.IsInternational = $("#chkIsInternational").prop("checked");
            if (leadInfo.IsInternational) {
                leadInfo.County = $("#txtCounty").val();
                leadInfo.Country = $("#txtCountry").val();
                leadInfo.State = $("#txtState").val();
            } else {
                leadInfo.CountryId = $("#ddlCountry").data("kendoDropDownList").value();
                leadInfo.StateId = $("#ddlState").data("kendoDropDownList").value();
                leadInfo.CountyId = $("#ddlCounty").data("kendoDropDownList").value();
            }

            viewModel.showLoading(true);

            $.ajax({
                url: viewModel.XPOST_LEAD_ADDRESS_MODEL,
                data: JSON.stringify(leadInfo),
                type: "post",
                timeout: 20000,
                dataType: "json",
                contentType: "application/json"
            }).fail(function (jqXHR, textStatus, errorThrown) {
                viewModel.showLoading(false);
                viewModel.showErrorNotification(textStatus + ": " + errorThrown);
            }).done(function (msg) {
                viewModel.showLoading(false);
                if (msg.ResponseCode === 200) {
                    $("#addressForm").data("kendoWindow").center().close();
                    viewModel.rebindDataSources(msg.Data);
                    viewModel.showSuccessNotification();
                } else {
                    viewModel.showErrorNotification(msg.ResponseMessage);
                }
            });
        },
        deleteAddress: function (e) {
            var isEditMode = $(e.currentTarget).hasClass("deleteAddressClick");
            if (isEditMode) {
                viewModel.confirmNotification(function () {
                    viewModel.showLoading(true);

                    var leadInfo = new LeadAddressInputModel();

                    var row = $(e.currentTarget).closest("tr");
                    var columns = $(row).find("td");
                    leadInfo.LeadId = $("#hdnLeadId").val();
                    leadInfo.UserId = $("#hdnUserId").val();
                    leadInfo.CampusId = XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;
                    leadInfo.ID = $(columns)[0].innerText;

                    $.ajax({
                        url: viewModel.XDELETE_LEAD_ADDRESS_MODEL,
                        data: JSON.stringify(leadInfo),
                        type: "post",
                        timeout: 20000,
                        dataType: "json",
                        contentType: "application/json"
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        viewModel.showLoading(false);
                        viewModel.showErrorNotification(textStatus + ": " + errorThrown);
                    }).done(function (msg) {
                        viewModel.showLoading(false);
                        if (msg.ResponseCode === 200) {
                            viewModel.rebindDataSources(msg.Data);
                            viewModel.showSuccessDeletedNotification();
                        } else {
                            viewModel.showErrorNotification(msg.ResponseMessage);
                        }
                    });
                });
            }
        },
        saveLeadComment: function () {
            var leadInfo = new LeadContactInputModel();

            leadInfo.LeadId = $("#hdnLeadId").val();
            leadInfo.UserId = $("#hdnUserId").val();
            leadInfo.CampusId = XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;
            leadInfo.Comments = $("#leadCommentsText").val();

            viewModel.showLoading(true);

            $.ajax({
                url: viewModel.XPOST_LEAD_COMMENTS_MODEL,
                data: JSON.stringify(leadInfo),
                type: "post",
                timeout: 20000,
                dataType: "json",
                contentType: "application/json"
            }).fail(function (jqXHR, textStatus, errorThrown) {
                viewModel.showLoading(false);
                viewModel.showErrorNotification(textStatus + ": " + errorThrown);
            }).done(function (msg) {
                viewModel.showLoading(false);
                if (msg.ResponseCode === 200) {
                    viewModel.rebindDataSources(msg.Data);
                    if (leadInfo.Comments.length === 0) {
                        viewModel.showSuccessDeletedNotification();
                    } else {
                        viewModel.showSuccessNotification();
                    }
                } else {
                    viewModel.showErrorNotification(msg.ResponseMessage);
                }
            });
        },
        initAddEditOtherContactsWindow: function (e) {
            var isEditMode = $(e.currentTarget).hasClass("updateOtherContacts") || $(e.currentTarget).hasClass("updateOtherContactsPhones") || $(e.currentTarget).hasClass("updateOtherContactsEmails") || $(e.currentTarget).hasClass("updateOtherContactsAddresses");
            var isInserDetailsMode = $(e.currentTarget).hasClass("lnkAddOtherContactPhone") || $(e.currentTarget).hasClass("lnkAddOtherContactEmails") || $(e.currentTarget).hasClass("lnkAddOtherContactAddresses");

            $("#otherContactsForm").kendoWindow({
                //animation: fals,
                modal: true,
                width: "600",
                title: "Add Contact",
                actions: ["close"],
                resizable: false,
                draggable: true
            });
            $("#otherContactsForm").data("kendoWindow").center().open();
            if ($("#otherContactsForm #ddlContactType").data("kendoDropDownList") == undefined) {
                $("#otherContactsForm #ddlContactType").kendoDropDownList({ dataTextField: "Description", dataValueField: "ID", optionLabel: "--Select--", dataSource: viewModel.contactTypesDataSource });
                $("#otherContactsForm .ddlContactStatus, #otherContactsForm .ddlContactPhoneStatus, #otherContactsForm .ddlContactEmailStatus, #otherContactsForm .ddlContactAddressStatus").kendoDropDownList({
                    dataTextField: "Description", dataValueField: "ID", optionLabel: "--Select--", dataSource: viewModel.statusDataSource
                });
                $("#otherContactsForm .ddlContactPhoneType").kendoDropDownList({ dataTextField: "Description", dataValueField: "ID", optionLabel: "--Select--", dataSource: viewModel.phoneTypesDataSource });
                $("#otherContactsForm .ddlContactEmailType").kendoDropDownList({ dataTextField: "Description", dataValueField: "ID", optionLabel: "--Select--", dataSource: viewModel.emailTypesDataSource });
                $("#otherContactsForm .ddlContactAddressType").kendoDropDownList({ dataTextField: "Description", dataValueField: "ID", optionLabel: "--Select--", dataSource: viewModel.addressTypesDatSource });
                $("#otherContactsForm .ddlContactCountry").kendoDropDownList({ dataTextField: "Description", dataValueField: "ID", optionLabel: "--Select--", dataSource: viewModel.countriesDataSource });
                $("#otherContactsForm .ddlContactState").kendoDropDownList({ dataTextField: "Description", dataValueField: "ID", optionLabel: "--Select--", dataSource: viewModel.statesDataSource });
                $("#otherContactsForm .ddlContactCounty").kendoDropDownList({ dataTextField: "Description", dataValueField: "ID", optionLabel: "--Select--", dataSource: viewModel.countiesDataSource });
                $("#otherContactsForm .ddlRelationship").kendoDropDownList({ dataTextField: "Description", dataValueField: "ID", optionLabel: "--Select--", dataSource: viewModel.relationshipsDataSource });
                $("#otherContactsForm .ddlPrefix").kendoDropDownList({ dataTextField: "Description", dataValueField: "ID", optionLabel: "--Select--", dataSource: viewModel.prefixDataSource });
                $("#otherContactsForm .ddlSufix").kendoDropDownList({ dataTextField: "Description", dataValueField: "ID", optionLabel: "--Select--", dataSource: viewModel.sufixDataSource });
            }

            $("#btnOtherContactSave").unbind("click");
            $("#otherContactsForm .txtContactPhoneExtension").kendoMaskedTextBox({
                mask: "0000000000"
            });

            $("#otherContactsForm .txtContactPhone").unbind("keyup");

            $("#otherContactsForm .chkContactIsForeign").unbind("change");

            $("#otherContactsForm .chkContactIsForeign").change(function (e) {
                if ($(e.currentTarget).prop("checked")) {
                    var maskedtextbox = $("#otherContactsForm .txtContactPhone").data("kendoMaskedTextBox");
                    // detach events
                    if (maskedtextbox != undefined) {
                        maskedtextbox.destroy();
                        $("#otherContactsForm .txtContactPhone").removeAttr("data-role");
                    }
                    $("#otherContactsForm .txtContactPhone").attr("data-role", "international");

                    $("#otherContactsForm .txtContactPhone").kendoMaskedTextBox({
                        mask: '+9999999999999999999999999'
                    });

                } else {
                    $("#otherContactsForm .txtContactPhone").removeAttr("data-role");
                    $("#otherContactsForm .txtContactPhone").unbind("keyup");
                    $("#otherContactsForm .txtContactPhone").kendoMaskedTextBox({
                        mask: "(999)-000-0000"
                    });
                }
            });

            var maskedtextbox = $("#otherContactsForm .txtContactPhone").data("kendoMaskedTextBox");
            if (maskedtextbox != undefined) {
                maskedtextbox.destroy();
                $("#otherContactsForm .txtContactPhone").removeAttr("data-role");
            }
            $("#otherContactsForm .txtContactPhone").kendoMaskedTextBox({
                mask: "(999)-000-0000"
            });


            $("#otherContactAddAddress .txtContactZipCode").kendoMaskedTextBox({
                mask: "99999"
            });
            $("#otherContactsForm .chkContactIsInternational").unbind("change");
            $("#otherContactsForm .chkContactIsInternational").bind("change", function () {
                var maskedtextbox = $("#otherContactAddAddress .txtContactZipCode").data("kendoMaskedTextBox");

                if ($("#otherContactsForm .chkContactIsInternational").prop("checked")) {
                    $("#otherContactsForm .txtContactState, #otherContactsForm .txtContactCountry").removeClass("hidden");
                    $("#otherContactsForm .txtContactCounty").removeClass("hidden");
                    $("#otherContactsForm .ddlContactCountry .ddl").data("kendoDropDownList").wrapper.hide();
                    $("#otherContactsForm .ddlContactState .ddl").data("kendoDropDownList").wrapper.hide();
                    $("#otherContactsForm .ddlContactCounty .ddl").data("kendoDropDownList").wrapper.hide();
                    $("#otherContactsForm .ddlContactCountry .ddl, #otherContactsForm .ddlContactState .ddl").removeAttr("required");
                    maskedtextbox.setOptions({ mask: "" });
                } else {
                    $("#otherContactsForm .txtContactState, #otherContactsForm .txtContactCountry").addClass("hidden");
                    $("#otherContactsForm .ddlContactCountry .ddl, #otherContactsForm .ddlContactState .ddl").attr("required", "required");
                    $("#otherContactsForm .txtContactCounty").addClass("hidden");
                    $("#otherContactsForm .ddlContactCountry .ddl").data("kendoDropDownList").wrapper.show();
                    $("#otherContactsForm .ddlContactState .ddl").data("kendoDropDownList").wrapper.show();
                    $("#otherContactsForm .ddlContactCounty .ddl").data("kendoDropDownList").wrapper.show();
                    maskedtextbox.setOptions({ mask: "00000" });
                }
            });

            var btnSave_OnClick = undefined;
            var model = undefined;
            $(".requiredTooltip").remove();
            if (isEditMode) {
                var row = $(e.currentTarget).closest("tr");
                var ID = undefined;
                var otherContactId = undefined;
                if ($(e.currentTarget).hasClass("updateOtherContacts")) {
                    //Edit Mode Of the Contact Info
                    $("#otherContactsForm").data("kendoWindow").title("Edit Contact");
                    $("#otherContactAddPhone, .otherContactAddPhone").hide();
                    $("#otherContactAddEmail, .otherContactAddEmail").hide();
                    $("#otherContactAddAddress, .otherContactAddAddress").hide();
                    $("#otherContactInfoForm").show();

                    $("#otherContactAddAddress .required").addClass("hidden");

                    if (row != undefined) {
                        ID = $(row).find('[data_column="ID"]').text();
                        model = undefined;
                        model = utils.findObjectInDataSource(viewModel.otherContactsDataSource, ID, "OtherContactId");
                        if (model != null) {
                            $("#hdnOtherContactId").val(model.OtherContactId);
                            $("#hdnLeadPhoneId").val("");

                            $("#otherContactsForm #ddlContactType").data("kendoDropDownList").value(model.ContactTypeId);
                            $("#otherContactsForm .ddlContactStatus .ddl").data("kendoDropDownList").value(model.StatusId);
                            $("#otherContactsForm .ddlRelationship .ddl").data("kendoDropDownList").value(model.RelationshipId);
                            $("#otherContactsForm .ddlPrefix .ddl").data("kendoDropDownList").value(model.PrefixId);
                            $("#otherContactsForm .ddlSufix .ddl").data("kendoDropDownList").value(model.SufixId);

                            $("#otherContactsForm .txtContactFirstName").val(model.FirstName);
                            $("#otherContactsForm .txtContactLastName").val(model.LastName);
                            $("#otherContactsForm .txtContactMiddleName").val(model.MiddleName);

                            btnSave_OnClick = function () {
                                viewModel.validateOtherContactForm(
                                    function () {
                                        viewModel.saveOtherContactInfo(viewModel.SAVE_MODE_EDIT_OTHER_INFO);
                                    }
                                );
                            };
                        }
                    }
                }
                else if ($(e.currentTarget).hasClass("updateOtherContactsPhones")) {
                    //Edit Mode of the Contact Phone
                    $("#otherContactsForm").data("kendoWindow").title("Edit Contact Phone");
                    $("#otherContactAddPhone, .otherContactAddPhone").show();
                    $("#otherContactAddEmail, .otherContactAddEmail").hide();
                    $("#otherContactAddAddress, .otherContactAddAddress").hide();
                    $("#otherContactInfoForm").hide();
                    if (row != undefined) {
                        ID = $(row).find('[data_column="ID"]').text();
                        otherContactId = $(row).find('[data_column="OtherContactId"]').text();
                        model = undefined;
                        model = utils.findObjectInDataSource(viewModel.otherContactsDataSource, otherContactId, "OtherContactId");
                        model = utils.findObjectInDataSource(model.Phones, ID, "OtherContactsPhoneId");
                        if (model != undefined) {
                            $("#hdnOtherContactId").val(model.OtherContactId);
                            $("#hdnLeadId").val(model.LeadId);
                            $("#hdnOtherContactPhoneId").val(ID);

                            $("#otherContactAddPhone .ddlContactPhoneType .ddl").data("kendoDropDownList").value(model.PhoneTypeId);
                            $("#otherContactAddPhone .ddlContactPhoneStatus .ddl").data("kendoDropDownList").value(model.StatusId);

                            $("#otherContactAddPhone .txtContactPhone").val(model.Phone);
                            $("#otherContactAddPhone .txtContactPhoneExtension").val(model.Extension);
                            $("#otherContactAddPhone .chkContactIsForeign").prop("checked", model.IsForeignPhone);

                            if (model.IsForeignPhone) {
                                maskedtextbox = $("#otherContactsForm .txtContactPhone").data("kendoMaskedTextBox");
                                // detach events
                                if (maskedtextbox != undefined) {
                                    maskedtextbox.destroy();
                                    $("#otherContactsForm .txtContactPhone").removeAttr("data-role");
                                }
                                $("#otherContactsForm .txtContactPhone").attr("data-role", "international");
                                $("#otherContactsForm .txtContactPhone")
                                    .kendoMaskedTextBox({
                                        mask: '+9999999999999999999999999'
                                    });

                            } else {
                                $("#otherContactsForm .txtContactPhone").removeAttr("data-role");
                                $("#otherContactsForm .txtContactPhone").unbind("keyup");
                                $("#otherContactsForm .txtContactPhone").kendoMaskedTextBox({
                                    mask: "(999)-000-0000"
                                });
                            }

                            btnSave_OnClick = function () {
                                viewModel.validateOtherContactPhoneForm(
                                    function () {
                                        viewModel.saveOtherContactInfo(viewModel.SAVE_MODE_EDIT_PHONE_NUMBER);
                                    }
                                );
                            };
                        }
                    }
                }
                else if ($(e.currentTarget).hasClass("updateOtherContactsEmails")) {
                    //Edit Mode of the Contact Email
                    $("#otherContactsForm").data("kendoWindow").title("Edit Contact Email");
                    $("#otherContactsForm").data("kendoWindow").title("Add Contact Email");
                    $("#otherContactAddPhone, .otherContactAddPhone").hide();
                    $("#otherContactAddEmail, .otherContactAddEmail").show();
                    $("#otherContactAddAddress, .otherContactAddAddress").hide();
                    $("#otherContactInfoForm").hide();
                    if (row != undefined) {
                        ID = $(row).find('[data_column="ID"]').text();
                        otherContactId = $(row).find('[data_column="OtherContactId"]').text();
                        model = undefined;
                        model = utils.findObjectInDataSource(viewModel.otherContactsDataSource, otherContactId, "OtherContactId");
                        model = utils.findObjectInDataSource(model.Emails, ID, "OtherContactsEmailId");
                        if (model != undefined) {
                            $("#hdnOtherContactId").val(model.OtherContactId);
                            $("#hdnLeadId").val(model.LeadId);
                            $("#hdnOtherContactEmailId").val(ID);

                            $("#otherContactAddEmail .ddlContactEmailType .ddl").data("kendoDropDownList").value(model.EmailTypeId);
                            $("#otherContactAddEmail .ddlContactEmailStatus .ddl").data("kendoDropDownList").value(model.StatusId);

                            $("#otherContactAddEmail .txtContactEmail").val(model.Email);

                            btnSave_OnClick = function () {
                                viewModel.validateOtherContactEmailForm(
                                    function () {
                                        viewModel.saveOtherContactInfo(viewModel.SAVE_MODE_EDIT_EMAIL);
                                    }
                                );
                            };
                        }
                    }
                }
                else if ($(e.currentTarget).hasClass("updateOtherContactsAddresses")) {
                    //Edit Mode of the Contact Address
                    $("#otherContactsForm").data("kendoWindow").title("Edit Contact Address");
                    $("#otherContactAddPhone, .otherContactAddPhone").hide();
                    $("#otherContactAddEmail, .otherContactAddEmail").hide();
                    $("#otherContactAddAddress, .otherContactAddAddress").show();
                    $("#otherContactInfoForm").hide();

                    $("#otherContactAddAddress .required").removeClass("hidden");

                    if (row != undefined) {
                        ID = $(row).find('[data_column="ID"]').text();
                        otherContactId = $(row).find('[data_column="OtherContactId"]').text();
                        model = undefined;
                        model = utils.findObjectInDataSource(viewModel.otherContactsDataSource, otherContactId, "OtherContactId");
                        model = utils.findObjectInDataSource(model.Addresses, ID, "OtherContactsAddresesId");
                        if (model != undefined) {
                            $("#hdnOtherContactId").val(model.OtherContactId);
                            $("#hdnLeadId").val(model.LeadId);
                            $("#hdnOtherContactAddressId").val(ID);

                            $("#otherContactAddAddress .ddlContactAddressType .ddl").data("kendoDropDownList").value(model.AddressTypeId);
                            $("#otherContactAddAddress .ddlContactAddressStatus .ddl").data("kendoDropDownList").value(model.StatusId);

                            $("#otherContactAddAddress .txtContactAddress1").val(model.Address1);
                            $("#otherContactAddAddress .txtContactAddress2").val(model.Address2);
                            $("#otherContactAddAddress .txtContactCity").val(model.City);
                            $("#otherContactAddAddress .txtContactZipCode").val(model.ZipCode);

                            $("#otherContactAddAddress .chkContactIsInternational").prop("checked", model.IsInternational);
                            $("#otherContactAddAddress .chkContactIsMaillingAddress").prop("checked", model.IsMailingAddress);

                            var maskZipCode = $("#otherContactAddAddress .txtContactZipCode").data("kendoMaskedTextBox");

                            if (model.IsInternational) {
                                $("#otherContactAddAddress .txtContactCountry").val(model.CountryInternational);
                                $("#otherContactAddAddress .txtContactState").val(model.StateInternational);
                                $("#otherContactAddAddress .txtContactCounty").val(model.CountyInternational);

                                $("#otherContactAddAddress .txtContactState").removeAttr("required");

                                $("#otherContactsForm .txtContactState, #otherContactsForm .txtContactCountry").removeClass("hidden");
                                $("#otherContactsForm .txtContactCounty").removeClass("hidden");
                                $("#otherContactsForm .ddlContactCountry .ddl").data("kendoDropDownList").wrapper.hide();
                                $("#otherContactsForm .ddlContactState .ddl").data("kendoDropDownList").wrapper.hide();
                                $("#otherContactsForm .ddlContactCounty .ddl").data("kendoDropDownList").wrapper.hide();
                                $("#otherContactsForm .ddlContactCountry .ddl, #otherContactsForm .ddlContactState .ddl").removeAttr("required");

                                $("#otherContactsForm .txtContactState_required").addClass("hidden");
                                maskZipCode.setOptions({ mask: "" });

                            } else {
                                $("#otherContactAddAddress .ddlContactCountry .ddl").data("kendoDropDownList").value(model.CountryId);
                                $("#otherContactAddAddress .ddlContactState .ddl").data("kendoDropDownList").value(model.StateId);
                                $("#otherContactAddAddress .ddlContactCounty .ddl").data("kendoDropDownList").value(model.CountyId);

                                $("#otherContactsForm .txtContactState, #otherContactsForm .txtContactCountry").addClass("hidden");
                                $("#otherContactAddAddress .txtContactState").attr("required", "required");
                                $("#otherContactsForm .ddlContactCountry .ddl, #otherContactsForm .ddlContactState .ddl").attr("required", "required");
                                $("#otherContactsForm .txtContactCounty").addClass("hidden");
                                $("#otherContactsForm .ddlContactCountry .ddl").data("kendoDropDownList").wrapper.show();
                                $("#otherContactsForm .ddlContactState .ddl").data("kendoDropDownList").wrapper.show();
                                $("#otherContactsForm .ddlContactCounty .ddl").data("kendoDropDownList").wrapper.show();
                                $("#otherContactsForm .txtContactState_required").removeClass("hidden");
                                maskZipCode.setOptions({ mask: "00000" });
                            }

                            $("#otherContactAddEmail .txtContactEmail").val(model.Email);

                            btnSave_OnClick = function () {
                                viewModel.validateOtherContactAddressForm(
                                    function () {
                                        viewModel.saveOtherContactInfo(viewModel.SAVE_MODE_ADDRESS);
                                    }
                                );
                            };
                        }
                    }
                }
            } else {
                if (isInserDetailsMode) {
                    $("#hdnOtherContactPhoneId").val("");
                    $("#hdnOtherContactAddressId").val("");
                    $("#hdnOtherContactEmailId").val("");
                    if ($(e.currentTarget).hasClass("lnkAddOtherContactPhone")) {
                        $("#hdnOtherContactId").val($(e.currentTarget).attr("data-OtherContactId"));
                        $("#otherContactsForm").data("kendoWindow").title("Add Contact Phone");
                        $("#otherContactAddPhone, .otherContactAddPhone").show();
                        $("#otherContactAddEmail, .otherContactAddEmail").hide();
                        $("#otherContactAddAddress, .otherContactAddAddress").hide();
                        $("#otherContactInfoForm").hide();

                        $('#otherContactAddPhone input[type="text"], #otherContactAddPhone input[type="tel"]').each(function (index, element) {
                            $(element).val("");
                        });
                        $('#otherContactAddPhone input[type="checkbox"]').each(function (index, element) {
                            $(element).prop("checked", false);
                        });

                        $("#otherContactsForm .ddlContactPhoneType .ddl").data("kendoDropDownList").select(0);
                        $("#otherContactsForm .ddlContactPhoneStatus .ddl").data("kendoDropDownList").select(0);

                        btnSave_OnClick = function () {
                            viewModel.validateOtherContactPhoneForm(
                                function () {
                                    viewModel.saveOtherContactInfo(viewModel.SAVE_MODE_EDIT_PHONE_NUMBER);
                                }
                            );
                        };
                    }
                    else if ($(e.currentTarget).hasClass("lnkAddOtherContactEmails")) {
                        $("#hdnOtherContactId").val($(e.currentTarget).attr("data-OtherContactId"));
                        $("#otherContactsForm").data("kendoWindow").title("Add Contact Email");
                        $("#otherContactAddPhone, .otherContactAddPhone").hide();
                        $("#otherContactAddEmail, .otherContactAddEmail").show();
                        $("#otherContactAddAddress, .otherContactAddAddress").hide();
                        $("#otherContactInfoForm").hide();
                        $('#otherContactAddEmail input[type="email"]').each(function (index, element) {
                            $(element).val("");
                        });
                        $("#otherContactsForm .ddlContactEmailType .ddl").data("kendoDropDownList").select(0);
                        $("#otherContactsForm .ddlContactEmailStatus .ddl").data("kendoDropDownList").select(0);

                        btnSave_OnClick = function () {
                            viewModel.validateOtherContactEmailForm(
                                function () {
                                    viewModel.saveOtherContactInfo(viewModel.SAVE_MODE_EDIT_EMAIL);
                                }
                            );
                        };
                    }
                    else if ($(e.currentTarget).hasClass("lnkAddOtherContactAddresses")) {
                        $("#hdnOtherContactId").val($(e.currentTarget).attr("data-OtherContactId"));
                        $("#otherContactsForm").data("kendoWindow").title("Add Contact Address");
                        $("#otherContactAddPhone, .otherContactAddPhone").hide();
                        $("#otherContactAddEmail, .otherContactAddEmail").hide();
                        $("#otherContactAddAddress, .otherContactAddAddress").show();
                        $("#otherContactInfoForm").hide();

                        $("#otherContactAddAddress .required").removeClass("hidden");

                        $('#otherContactAddAddress input[type="text"]').each(function (index, element) {
                            $(element).val("");
                        });
                        $('#otherContactAddAddress input[type="checkbox"]').each(function (index, element) {
                            $(element).prop("checked", false);
                        });
                        $("#otherContactAddAddress .ddlContactAddressType .ddl").data("kendoDropDownList").select(0);
                        $("#otherContactAddAddress .ddlContactAddressStatus .ddl").data("kendoDropDownList").select(0);
                        $("#otherContactAddAddress .ddlContactCountry .ddl").data("kendoDropDownList").select(0);
                        $("#otherContactAddAddress .ddlContactState .ddl").data("kendoDropDownList").select(0);
                        $("#otherContactAddAddress .ddlContactCounty .ddl").data("kendoDropDownList").select(0);

                        $("#otherContactAddAddress .txtContactState, #otherContactAddAddress .txtContactCountry").addClass("hidden");
                        $("#otherContactAddAddress .txtContactCounty").addClass("hidden");
                        $("#otherContactAddAddress .ddlContactCountry .ddl").data("kendoDropDownList").wrapper.show();
                        $("#otherContactAddAddress .ddlContactState .ddl").data("kendoDropDownList").wrapper.show();
                        $("#otherContactAddAddress .ddlContactCounty .ddl").data("kendoDropDownList").wrapper.show();

                        btnSave_OnClick = function () {
                            viewModel.validateOtherContactAddressForm(
                                function () {
                                    viewModel.saveOtherContactInfo(viewModel.SAVE_MODE_ADDRESS);
                                }
                            );
                        };
                    }
                } else {
                    ///Data Entry For New Lead Contact
                    $("#hdnOtherContactId").val("");
                    $("#otherContactAddAddress .required").addClass("hidden");
                    $("#otherContactsForm").data("kendoWindow").title("Add Contact");
                    $("#otherContactAddPhone, .otherContactAddPhone").show();
                    $("#otherContactAddEmail, .otherContactAddEmail").show();
                    $("#otherContactAddAddress, .otherContactAddAddress").show();
                    $("#otherContactInfoForm").show();

                    $('#otherContactsForm input[type="text"], #otherContactsForm input[type="tel"]').each(function (index, element) {
                        $(element).val("");
                    });
                    $('#otherContactsForm input[type="email"]').each(function (index, element) {
                        $(element).val("");
                    });
                    $('#otherContactsForm input[type="checkbox"]').each(function (index, element) {
                        $(element).prop("checked", false);
                    });

                    $("#otherContactsForm #ddlContactType").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlContactStatus .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlRelationship .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlPrefix .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlSufix .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlContactPhoneType .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlContactPhoneStatus .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlContactEmailType .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlContactEmailStatus .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlContactAddressType .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlContactAddressStatus .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlContactCountry .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlContactState .ddl").data("kendoDropDownList").select(0);
                    $("#otherContactsForm .ddlContactCounty .ddl").data("kendoDropDownList").select(0);

                    $("#otherContactsForm .txtContactState, #otherContactsForm .txtContactCountry").addClass("hidden");
                    $("#otherContactsForm .txtContactCounty").addClass("hidden");
                    $("#otherContactsForm .ddlContactCountry .ddl").data("kendoDropDownList").wrapper.show();
                    $("#otherContactsForm .ddlContactState .ddl").data("kendoDropDownList").wrapper.show();
                    $("#otherContactsForm .ddlContactCounty .ddl").data("kendoDropDownList").wrapper.show();

                    $("#otherContactAddAddress .ddlContactAddressType .ddl").removeAttr("required");
                    $("#otherContactAddAddress .ddlContactAddressStatus .ddl").removeAttr("required");
                    $("#otherContactAddAddress .txtContactAddress1").removeAttr("required");
                    $("#otherContactAddAddress .txtContactCity").removeAttr("required");
                    $("#otherContactAddAddress .txtContactZipCode").removeAttr("required");
                    $("#otherContactAddAddress .ddlContactCountry .ddl").removeAttr("required");
                    $("#otherContactAddAddress .ddlContactState .ddl").removeAttr("required");

                    $("#otherContactAddAddress .txtContactCountry").removeAttr("required");
                    $("#otherContactAddAddress .txtContactState").removeAttr("required");

                    btnSave_OnClick =
                        function () {
                            viewModel.validateOtherContactFormFull(
                                function () {
                                    viewModel.saveOtherContactInfo(viewModel.SAVE_MODE_ADD_OTHER_INFO);
                                }
                            );
                        };
                }
            }

            $("#btnOtherContactSave").unbind("click");
            $("#btnOtherContactSave").bind("click", btnSave_OnClick);

            $("#btnOtherContactCancel").unbind("click");
            $("#btnOtherContactCancel").bind("click", function () {
                $("#otherContactsForm").data("kendoWindow").close();
                MasterPage.advantageValitorReset("otherContactsForm");
            });
            MasterPage.advantageValitorReset("otherContactsForm");
            viewModel.clearRequiredDropdown("#otherContactsForm #ddlContactType,#otherContactsForm .ddlContactPhoneType .ddl,#otherContactsForm .ddlContactPhoneStatus .ddl,#otherContactsForm .ddlContactStatus .ddl");
            viewModel.clearRequiredDropdown("#otherContactsForm .ddlContactEmailType .ddl,#otherContactsForm .ddlContactEmailStatus .ddl,#otherContactsForm .ddlRelationship .ddl");
            viewModel.clearRequiredDropdown("#otherContactAddAddress .ddlContactAddressType .ddl,#otherContactAddAddress .ddlContactAddressStatus .ddl,#otherContactAddAddress .ddlContactCountry .ddl,#otherContactAddAddress .ddlContactState .ddl");
            MasterPage.ADVANTAGE_VALIDATOR("otherContactsForm");
        },
        viewOtherContactDetails: function (e) {
            var iscollapased = $(e.currentTarget).attr("data-collapsed");
            if (iscollapased === undefined || iscollapased === null) {
                iscollapased = "true";
            }
            if (iscollapased === "true") {
                viewModel.showOtherContactDetails(false, e.currentTarget);
                $(e.currentTarget).attr("data-collapsed", "false");
            } else {
                viewModel.hideOtherContactDetails(e.currentTarget);
                $(e.currentTarget).attr("data-collapsed", "true");
            }
        },
        hideOtherContactDetails: function (target) {
            var elementId = '[data-contactDetails="' + $(target).attr("data-elementid") + '"]';
            $(elementId).remove();
        },
        showOtherContactDetails: function (showAll, target) {
            var row = $(target).closest("tr");
            if (row != undefined) {
                var ID = $(row).find('[data_column="ID"]').text();

                $("#hdnOtherContactId").val(ID);

                var model = utils.findObjectInDataSource(viewModel.otherContactsDataSource, ID, "OtherContactId");
                if (model != null) {
                    var template = $("#otherContactsDetailsTemplate").html();
                    var elementID = '[data-contactDetails="' + ID + '"]';
                    var contactDetails = $(elementID);
                    if (showAll == false) {
                        $("#otherContactsDetails").html("");
                    }
                    if (contactDetails.length === 0) {
                        $("#otherContactsDetails").append('<div class="container detailGrid" data-contactDetails="' + ID + '">' + template + "</div>");
                    } else {
                        $(elementID).remove();
                        $("#otherContactsDetails").append('<div class="container detailGrid" data-contactDetails="' + ID + '">' + template + "</div>");
                    }
                    var dataSource = new Array();
                    dataSource.push(model);
                    var grid = $(elementID).find(".otherContactDetailGrid").data("kendoGrid");
                    if (grid != undefined) {
                        grid.destroy();
                    }
                    $(elementID).find(".otherContactDetailGrid").kendoGrid({
                        dataSource: dataSource,
                        dataBound: function () {
                            viewModel.showLoading(false);
                        },
                        sortable: false,
                        selectable: false,
                        scrollable: true,
                        resizable: true,
                        pageable: false,
                        noRecords: true,
                        columns: [
                            { field: "OtherContactId", title: "OtherContactId", hidden: true, attributes: { data_Column: "ID" } },
                            { field: "TypeId", title: "ContactTypeId", hidden: true, attributes: { data_Column: "TypeId" } },
                            { field: "RelationshipId", title: "RelationshipId", hidden: true, attributes: { data_Column: "RelationshipId" } },
                            { field: "StatusId", title: "StatusId", hidden: true, attributes: { data_Column: "StatusId" } },
                            { field: "FirstName", title: "FirstName", hidden: true, attributes: { data_Column: "FirstName" } },
                            { field: "LastName", title: "LastName", hidden: true, attributes: { data_Column: "LastName" } },
                            { field: "Prefix", title: "Prefix", hidden: true, attributes: { data_Column: "Prefix" } },
                            { field: "ContactType", title: "Type", width: 80, attributes: { style: "text-align:left;", data_Column: "Type" } },
                            { field: "Relationship", title: "Relationship", width: 50, attributes: { style: "text-align:left;", data_Column: "Relationship" } },
                            { field: "FullName", title: "Name", width: 120, attributes: { style: "text-align:left;", data_Column: "Name" }, template: "#=data.FullName#" },
                            { field: "Status", title: "Status", width: 50, attributes: { style: "text-align:left;", data_Column: "Status" } },
                            { field: "Edit", title: "", width: 50, attributes: { style: "text-align:center;" }, template: "<a class='updateOtherContacts pointer' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-change-manually font-blue-darken-1'></span></a>"},
                            { field: "Delete", title: "", width: 50, attributes: { style: "text-align:center;" }, template: "<a class='deleteOtherContacts pointer' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-close font-red'></span></a>"}
                        ]
                    });

                    var leadOtherContactsGrid = $(elementID).find(".otherContactDetailGrid").data("kendoGrid");
                    leadOtherContactsGrid.table.on("click", ".updateOtherContacts", viewModel.initAddEditOtherContactsWindow);
                    leadOtherContactsGrid.table.on("click", ".deleteOtherContacts", viewModel.deleteOtherContacts);

                    if ((model.Phones != undefined && model.Phones.length === 0) || (model.Phones === undefined)) {
                        model.Phones = new Array();
                    }
                    grid = $(elementID).find(".otherContactPhoneGrid").data("kendoGrid");
                    if (grid != undefined) {
                        grid.destroy();
                    }
                    $(elementID).find(".otherContactPhoneGrid").kendoGrid({
                        dataSource: model.Phones,
                        dataBound: function () {
                            viewModel.showLoading(false);
                        },
                        sortable: false,
                        selectable: false,
                        scrollable: true,
                        resizable: true,
                        pageable: false,
                        noRecords: true,
                        columns: [
                            { field: "OtherContactsPhoneId", title: "OtherContactsPhoneId", hidden: true, attributes: { data_Column: "ID" } },
                            { field: "OtherContactId", title: "OtherContactId", hidden: true, attributes: { data_Column: "OtherContactId" } },
                            { field: "PhoneTypeId", title: "PhoneTypeId", hidden: true, attributes: { data_Column: "TypeId" } },
                            { field: "StatusId", title: "StatusId", hidden: true, attributes: { data_Column: "StatusId" } },
                            { field: "PhoneType", title: "Type", width: 120, attributes: { tyle: "text-align:left;", data_Column: "PhoneType" } },
                            { field: "Phone", title: "Phone", width: 120, attributes: { tyle: "text-align:left;", data_Column: "Phone" } },
                            { field: "Extension", title: "Ext.", width: 80, attributes: { tyle: "text-align:left;", data_Column: "Extension" } },
                            { field: "Status", title: "Status", width: 50, attributes: { style: "text-align:left;", data_Column: "Status" } },
                            { field: "Edit", title: "", width: 50, attributes: { style: "text-align:center;" }, template: "<a class='updateOtherContactsPhones pointer' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-change-manually font-blue-darken-1'></span></a>"},
                            { field: "Delete", title: "", width: 50, attributes: { style: "text-align:center;" }, template: "<a class='deleteOtherContactsPhones pointer' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-close font-red'></span></a>" }
                        ]
                    });

                    var otherContactPhoneGrid = $(elementID).find(".otherContactPhoneGrid").data("kendoGrid");
                    otherContactPhoneGrid.table.on("click", ".updateOtherContactsPhones", viewModel.initAddEditOtherContactsWindow);
                    otherContactPhoneGrid.table.on("click", ".deleteOtherContactsPhones", viewModel.deleteOtherContactsPhones);

                    $(elementID).find(".lnkAddOtherContactPhone").unbind("unbind");
                    $(elementID).find(".lnkAddOtherContactPhone").bind("click", viewModel.initAddEditOtherContactsWindow);
                    $(elementID).find(".lnkAddOtherContactPhone").attr("data-OtherContactId", ID);

                    if ((model.Emails != undefined && model.Emails.length === 0) || (model.Emails === undefined)) {
                        model.Emails = new Array();
                    }
                    grid = $(elementID).find(".otherContactEmailsGrid").data("kendoGrid");
                    if (grid != undefined) {
                        grid.destroy();
                    }
                    $(elementID).find(".otherContactEmailsGrid").kendoGrid({
                        dataSource: model.Emails,
                        dataBound: function () {
                            viewModel.showLoading(false);
                        },
                        sortable: false,
                        selectable: false,
                        scrollable: true,
                        resizable: true,
                        pageable: false,
                        noRecords: true,
                        columns: [
                            { field: "OtherContactsEmailId", title: "OtherContactsEmailId", hidden: true, attributes: { data_Column: "ID" } },
                            { field: "OtherContactId", title: "OtherContactId", hidden: true, attributes: { data_Column: "OtherContactId" } },
                            { field: "EmailTypeId", title: "EmailTypeId", hidden: true, attributes: { data_Column: "TypeId" } },
                            { field: "StatusId", title: "StatusId", hidden: true, attributes: { data_Column: "StatusId" } },
                            { field: "EmailType", title: "Type", width: 120, attributes: { tyle: "text-align:left;", data_Column: "PhoneType" } },
                            { field: "Email", title: "Email", width: 120, attributes: { tyle: "text-align:left;", data_Column: "Phone" } },
                            { field: "Status", title: "Status", width: 50, attributes: { style: "text-align:left;", data_Column: "Status" } },
                            { field: "Edit", title: "", width: 50, attributes: { style: "text-align:center;" }, template: "<a class='updateOtherContactsEmails pointer' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-change-manually font-blue-darken-1'></span></a>" },
                            { field: "Delete", title: "", width: 50, attributes: { style: "text-align:center;" }, template: "<a class='deleteOtherContactsEmails pointer' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-close font-red'></span></a>" }
                        ]
                    });

                    var otherContactEmailsGrid = $(elementID).find(".otherContactEmailsGrid").data("kendoGrid");
                    otherContactEmailsGrid.table.on("click", ".updateOtherContactsEmails", viewModel.initAddEditOtherContactsWindow);
                    otherContactEmailsGrid.table.on("click", ".deleteOtherContactsEmails", viewModel.deleteOtherContactsEmails);

                    $(elementID).find(".lnkAddOtherContactEmails").unbind("unbind");
                    $(elementID).find(".lnkAddOtherContactEmails").bind("click", viewModel.initAddEditOtherContactsWindow);
                    $(elementID).find(".lnkAddOtherContactEmails").attr("data-OtherContactId", ID);

                    if ((model.Addresses != undefined && model.Addresses.length === 0) || (model.Addresses === undefined)) {

                        model.Addresses = new Array();
                    }
                    grid = $(elementID).find(".otherContactAddressesGrid").data("kendoGrid");
                    if (grid != undefined) {
                        grid.destroy();
                    }
                    $(elementID).find(".otherContactAddressesGrid").kendoGrid({
                        dataSource: model.Addresses,
                        dataBound: function () {
                            viewModel.showLoading(false);
                        },
                        sortable: false,
                        selectable: false,
                        scrollable: true,
                        resizable: true,
                        pageable: false,
                        noRecords: true,
                        columns: [
                            { field: "OtherContactsAddresesId", title: "OtherContactsAddresesId", hidden: true, attributes: { data_Column: "ID" } },
                            { field: "OtherContactId", title: "OtherContactId", hidden: true, attributes: { data_Column: "OtherContactId" } },
                            { field: "AddressTypeId", title: "AddressTypeId", hidden: true, attributes: { data_Column: "TypeId" } },
                            { field: "StatusId", title: "StatusId", hidden: true, attributes: { data_Column: "StatusId" } },
                            { field: "AddressType", title: "Type", width: 50, attributes: { style: "text-align:left;" } },
                            { field: "Address1", title: "Address", width: 180, attributes: { style: "text-align:left;", data_Column: "Address" }, template: '#if (data.Address1 !== null && data.Address1 !== ""){ # <span>#=data.Address1#</span> #}# #if (data.Address2 !== null && data.Address2 !== ""){ # <span>,#=data.Address2#</span> #}#' },
                            { field: "City", title: "City, State, Zip", width: 130, attributes: { style: "text-align:left;" }, template: '#=data.City# #if (data.State !== null){ # <span>, #=data.State#</span> #}# #if (data.ZipCode !== null && data.ZipCode !== ""){ # <span>,#=data.ZipCode#</span> #}#' },
                            { field: "County", title: "County", width: 60, attributes: { style: "text-align:left;", data_Column: "County" } },
                            { field: "Country", title: "Country", width: 60, attributes: { style: "text-align:left;", data_Column: "Country" } },
                            { field: "Status", title: "Status", width: 50, attributes: { style: "text-align:center;" } },
                            { field: "IsMailingAddress", title: "Is Mailing Address", width: 120, attributes: { style: "text-align:center;", data_Column: "IsMailingAddress" }, template: kendo.template($("#templateIsMailing").html()) },
                            { field: "Edit", title: "", width: 50, attributes: { style: "text-align:center;" }, template: "<a class='updateOtherContactsAddresses pointer' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-change-manually font-blue-darken-1'></span></a>" },
                            { field: "Delete", title: "", width: 50, attributes: { style: "text-align:center;" }, template: "<a class='deleteOtherContactsAddresses pointer' data-ElementId='#=data.ID#' href='javascript:void(0);'> <span class='k-icon k-i-close font-red'></span></a>" }
                        ]
                    });

                    var otherContactAddressesGrid = $(elementID).find(".otherContactAddressesGrid").data("kendoGrid");
                    otherContactAddressesGrid.table.on("click", ".updateOtherContactsAddresses", viewModel.initAddEditOtherContactsWindow);
                    otherContactAddressesGrid.table.on("click", ".deleteOtherContactsAddresses", viewModel.deleteOtherContactsAddresses);

                    $(elementID).find(".lnkAddOtherContactAddresses").unbind("unbind");
                    $(elementID).find(".lnkAddOtherContactAddresses").bind("click", viewModel.initAddEditOtherContactsWindow);
                    $(elementID).find(".lnkAddOtherContactAddresses").attr("data-OtherContactId", ID);

                    $(elementID).find(".lnkSaveOtherContactComments").unbind("unbind");
                    $(elementID).find(".lnkSaveOtherContactComments").bind("click", viewModel.updateOtherContactComments);
                    $(elementID).find(".lnkSaveOtherContactComments").attr("data-OtherContactId", ID);

                    $(elementID).find(".lnkDeleteOtherContactComments").unbind("unbind");
                    $(elementID).find(".lnkDeleteOtherContactComments").bind("click", viewModel.updateOtherContactComments);
                    $(elementID).find(".lnkDeleteOtherContactComments").attr("data-OtherContactId", ID);
                    $(elementID).find(".otherContactCommentsText").attr("data-OtherContactId", ID);

                    $(elementID).find(".otherContactCommentsText").val(model.Comments);
                }
            }
        },
        deleteOtherContactsPhones: function (e) {
            var isEditMode = $(e.currentTarget).hasClass("deleteOtherContactsPhones");
            if (isEditMode) {
                viewModel.confirmNotification(function () {
                    viewModel.showLoading(true);
                    var leadInfo = new LeadOtherContactPhoneModel();
                    var row = $(e.currentTarget).closest("tr");
                    leadInfo.LeadId = $("#hdnLeadId").val();
                    leadInfo.ModUser = $("#hdnUserId").val();
                    leadInfo.OtherContactId = $(row).find('[data_column="OtherContactId"]').text();
                    leadInfo.OtherContactsPhoneId = $(row).find('[data_column="ID"]').text();
                    $.ajax({
                        url: viewModel.XDELETE_LEAD_OTHER_CONTACTS_PHONE,
                        data: JSON.stringify(leadInfo),
                        type: "post",
                        timeout: 20000,
                        dataType: "json",
                        contentType: "application/json"
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        viewModel.showLoading(false);
                        viewModel.showErrorNotification(textStatus + ": " + errorThrown);
                    }).done(function (msg) {
                        viewModel.showLoading(false);
                        if (msg.ResponseCode === 200) {
                            viewModel.rebindOtherContactsDataSource(msg.Data);
                            $('.viewOtherContactDetails[data-elementId="' + $("#hdnOtherContactId").val() + '"]').click();
                            viewModel.showSuccessDeletedNotification();
                        } else {
                            viewModel.showErrorNotification(msg.ResponseMessage);
                        }
                    });
                });
            }
        },
        deleteOtherContactsEmails: function (e) {
            var isEditMode = $(e.currentTarget).hasClass("deleteOtherContactsEmails");
            if (isEditMode) {
                viewModel.confirmNotification(function () {
                    viewModel.showLoading(true);
                    var leadInfo = new LeadOtherContactEmailModel();
                    var row = $(e.currentTarget).closest("tr");
                    leadInfo.LeadId = $("#hdnLeadId").val();
                    leadInfo.ModUser = $("#hdnUserId").val();
                    leadInfo.OtherContactId = $(row).find('[data_column="OtherContactId"]').text();
                    leadInfo.OtherContactsEmailId = $(row).find('[data_column="ID"]').text();
                    $.ajax({
                        url: viewModel.XDELETE_LEAD_OTHER_CONTACTS_EMAIL,
                        data: JSON.stringify(leadInfo),
                        type: "post",
                        timeout: 20000,
                        dataType: "json",
                        contentType: "application/json"
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        viewModel.showLoading(false);
                        viewModel.showErrorNotification(textStatus + ": " + errorThrown);
                    }).done(function (msg) {
                        viewModel.showLoading(false);
                        if (msg.ResponseCode === 200) {
                            viewModel.rebindOtherContactsDataSource(msg.Data);
                            $('.viewOtherContactDetails[data-elementId="' + $("#hdnOtherContactId").val() + '"]').click();
                            viewModel.showSuccessDeletedNotification();
                        } else {
                            viewModel.showErrorNotification(msg.ResponseMessage);
                        }
                    });
                });
            }
        },
        deleteOtherContactsAddresses: function (e) {
            var isEditMode = $(e.currentTarget).hasClass("deleteOtherContactsAddresses");
            if (isEditMode) {
                viewModel.confirmNotification(function () {
                    viewModel.showLoading(true);
                    var leadInfo = new LeadOtherContactAddressModel();
                    var row = $(e.currentTarget).closest("tr");
                    leadInfo.LeadId = $("#hdnLeadId").val();
                    leadInfo.ModUser = $("#hdnUserId").val();
                    leadInfo.OtherContactId = $(row).find('[data_column="OtherContactId"]').text();
                    leadInfo.OtherContactsAddresesId = $(row).find('[data_column="ID"]').text();
                    $.ajax({
                        url: viewModel.XDELETE_LEAD_OTHER_CONTACTS_ADDRESS,
                        data: JSON.stringify(leadInfo),
                        type: "post",
                        timeout: 20000,
                        dataType: "json",
                        contentType: "application/json"
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        viewModel.showLoading(false);
                        viewModel.showErrorNotification(textStatus + ": " + errorThrown);
                    }).done(function (msg) {
                        viewModel.showLoading(false);
                        if (msg.ResponseCode === 200) {
                            viewModel.rebindOtherContactsDataSource(msg.Data);
                            $('.viewOtherContactDetails[data-elementId="' + $("#hdnOtherContactId").val() + '"]').click();
                            viewModel.showSuccessDeletedNotification();
                        } else {
                            viewModel.showErrorNotification(msg.ResponseMessage);
                        }
                    });
                });
            }
        },
        updateOtherContactComments: function (e) {
            var isEditMode = $(e.currentTarget).hasClass("lnkDeleteOtherContactComments") || $(e.currentTarget).hasClass("lnkSaveOtherContactComments");
            if (isEditMode) {
                viewModel.showLoading(true);
                var leadInfo = new LeadOtherContactModel();
                leadInfo.LeadId = $("#hdnLeadId").val();
                leadInfo.ModUser = $("#hdnUserId").val();
                leadInfo.OtherContactId = $(e.currentTarget).attr("data-OtherContactId");
                if ($(e.currentTarget).hasClass("lnkDeleteOtherContactComments")) {
                    leadInfo.Comments = "";
                    $('.otherContactCommentsText[data-OtherContactId="' + leadInfo.OtherContactId + '"]').val("");
                }
                else
                    leadInfo.Comments = $('.otherContactCommentsText[data-OtherContactId="' + leadInfo.OtherContactId + '"]').val();
                $.ajax({
                    url: viewModel.XPOST_LEAD_OTHER_CONTACTS_COMMENTS,
                    data: JSON.stringify(leadInfo),
                    type: "post",
                    timeout: 20000,
                    dataType: "json",
                    contentType: "application/json"
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    viewModel.showLoading(false);
                    viewModel.showErrorNotification(textStatus + ": " + errorThrown);
                }).done(function (msg) {
                    viewModel.showLoading(false);
                    if (msg.ResponseCode === 200) {
                        viewModel.rebindOtherContactsDataSource(msg.Data);
                        $('.viewOtherContactDetails[data-elementId="' + $("#hdnOtherContactId").val() + '"]').click();
                        if (leadInfo.Comments.length === 0) {
                            viewModel.showSuccessDeletedNotification();
                        } else {
                            viewModel.showSuccessNotification();
                        }
                    } else {
                        viewModel.showErrorNotification(msg.ResponseMessage);
                    }
                });
            }
        },
        validateOtherContactFormFull: function (onSuccess) {
            $(".ddlContactPhoneType .ddl, .ddlContactPhoneStatus .ddl, .ddlContactEmailType .ddl, .ddlContactEmailStatus .ddl").removeAttr("required");
            $(".txtContactPhone, .txtContactEmail").removeAttr("required");
            $("#otherContactAddPhone .ddlContactPhoneType").removeAttr("required");
            $("#otherContactAddPhone .ddlContactPhoneStatus").removeAttr("required");
            $("#otherContactAddEmail .ddlContactEmailType").removeAttr("required");
            $("#otherContactAddEmail .ddlContactEmailStatus").removeAttr("required");
            viewModel.validateForm("otherContactsForm", function () {
                if (($("#otherContactsForm .txtContactPhone").val().length === 0) && ($("#otherContactsForm .txtContactEmail").val().length === 0)) {
                    $("#otherContactsForm .txtContactPhone").attr("required", "required");
                    $("#otherContactsForm .txtContactEmail").attr("required", "required");
                    //viewModel.showErrorNotification("");
                    MasterPage.SHOW_WARNING_WINDOW_PROMISE("At least one form of contact is required. Please enter a valid phone number or valid email.");
                }

                var otherContactAddPhone = MasterPage.ADVANTAGE_VALIDATOR("otherContactAddPhone");
                var otherContactAddEmail = MasterPage.ADVANTAGE_VALIDATOR("otherContactAddEmail");

                if ($("#otherContactAddPhone .txtContactPhone").val().length === 0) {
                    $("#otherContactAddPhone .ddlContactPhoneType").attr("required", "required");
                    $("#otherContactAddPhone .ddlContactPhoneStatus").attr("required", "required");
                } else {
                    $("#otherContactAddPhone .ddlContactPhoneType").removeAttr("required");
                    $("#otherContactAddPhone .ddlContactPhoneStatus").removeAttr("required");
                }

                if ($("#otherContactAddEmail .txtContactEmail").val().length === 0) {
                    $("#otherContactAddEmail .ddlContactEmailType").attr("required", "required");
                    $("#otherContactAddEmail .ddlContactEmailStatus").attr("required", "required");
                } else {
                    $("#otherContactAddEmail .ddlContactEmailType").removeAttr("required");
                    $("#otherContactAddEmail .ddlContactEmailStatus").removeAttr("required");
                }

                if (otherContactAddPhone.validate() || otherContactAddEmail.validate()) {
                    $(".txtContactPhone, .txtContactEmail").removeAttr("required");
                    $("#otherContactAddPhone .valueRequired, #otherContactAddEmail .valueRequired").removeClass('valueRequired');
                    $("#otherContactAddPhone .ddlContactPhoneType").removeAttr("required");
                    $("#otherContactAddPhone .ddlContactPhoneStatus").removeAttr("required");
                    $("#otherContactAddEmail .ddlContactEmailType").removeAttr("required");
                    $("#otherContactAddEmail .ddlContactEmailStatus").removeAttr("required");
                    otherContactAddPhone.validate();
                    otherContactAddEmail.validate();
                    onSuccess();
                }
            });
        },

        validateOtherContactForm: function (onSuccess) {
            viewModel.validateForm("otherContactsForm", function () {
                onSuccess();
            });
        },
        validateOtherContactPhoneForm: function (onSuccess) {
            $(".ddlContactPhoneType .ddl").attr("required", "required");
            $(".ddlContactPhoneType .ddl").attr("data-labelrequired", "Phone Type");
            $(".ddlContactPhoneStatus .ddl").attr("required", "required");
            $(".ddlContactPhoneStatus .ddl").attr("data-labelrequired", "Status");
            $(".txtContactPhone").attr("required", "required");
            $(".txtContactPhone").attr("data-labelrequired", "Phone");
            var thisOnSuccess = onSuccess;
            viewModel.validateForm("otherContactAddPhone", function () {
                thisOnSuccess();
            });
        },
        validateOtherContactEmailForm: function (onSuccess) {
            $(".ddlContactEmailType .ddl").attr("required", "required");
            $(".ddlContactEmailType .ddl").attr("data-labelrequired", "Email Type");
            $(".ddlContactEmailStatus .ddl").attr("required", "required");
            $(".ddlContactEmailStatus .ddl").attr("data-labelrequired", "Status");
            $(".txtContactEmail").attr("required", "required");
            $(".txtContactEmail").attr("data-labelrequired", "Email");
            var thisOnSuccess = onSuccess;
            viewModel.validateForm("otherContactAddEmail", function () {
                thisOnSuccess();
            });
        },
        validateOtherContactAddressForm: function (onSuccess) {

            $("#otherContactAddAddress .ddlContactAddressType .ddl").attr("required", "required");
            $("#otherContactAddAddress .ddlContactAddressStatus .ddl").attr("required", "required");

            $("#otherContactAddAddress .txtContactAddress1").attr("required", "required");
            $("#otherContactAddAddress .txtContactCity").attr("required", "required");
            $("#otherContactAddAddress .txtContactZipCode").attr("required", "required");

            if ($("#otherContactAddAddress .chkContactIsInternational").prop("checked")) {
                $("#otherContactAddAddress .txtContactCountry").attr("required", "required");

                $("#otherContactAddAddress .ddlContactCountry .ddl").removeAttr("required");
                $("#otherContactAddAddress .ddlContactState .ddl").removeAttr("required");

            } else {
                $("#otherContactAddAddress .ddlContactCountry .ddl").attr("required", "required");
                $("#otherContactAddAddress .ddlContactState .ddl").attr("required", "required");

                $("#otherContactAddAddress .txtContactCountry").removeAttr("required");
                $("#otherContactAddAddress .txtContactState").removeAttr("required");
            }
            var thisOnSuccess = onSuccess;
            viewModel.validateForm("otherContactAddAddress", function () {
                thisOnSuccess();
            });
        },
        saveOtherContactInfo: function (saveMode) {
            var leadOtherContact = new LeadOtherContactModel();
            var email = undefined;
            var phone = undefined;
            var address = undefined;

            leadOtherContact.Addresses = new Array();
            leadOtherContact.Phones = new Array();
            leadOtherContact.Emails = new Array();

            leadOtherContact.LeadId = $("#hdnLeadId").val();
            leadOtherContact.OtherContactId = $("#hdnOtherContactId").val();
            leadOtherContact.ModUser = $("#hdnUserId").val();

            leadOtherContact.ContactTypeId = $("#otherContactInfoForm #ddlContactType").data("kendoDropDownList").value();
            leadOtherContact.PrefixId = $("#otherContactInfoForm .ddlPrefix .ddl").data("kendoDropDownList").value();
            leadOtherContact.RelationshipId = $("#otherContactInfoForm .ddlRelationship .ddl").data("kendoDropDownList").value();
            leadOtherContact.SufixId = $("#otherContactInfoForm .ddlSufix .ddl").data("kendoDropDownList").value();
            leadOtherContact.StatusId = $("#otherContactInfoForm .ddlContactStatus .ddl").data("kendoDropDownList").value();
            leadOtherContact.FirstName = $("#otherContactInfoForm .txtContactFirstName").val();
            leadOtherContact.LastName = $("#otherContactInfoForm .txtContactLastName").val();
            leadOtherContact.MiddleName = $("#otherContactInfoForm .txtContactMiddleName").val();

            if ($("#otherContactsForm input.txtContactPhone").val().length > 0) {
                phone = new LeadOtherContactPhoneModel();
                phone.LeadId = leadOtherContact.LeadId;
                phone.OtherContactId = leadOtherContact.OtherContactId;
                phone.ModUser = leadOtherContact.ModUser;
                phone.OtherContactsPhoneId = $("#hdnOtherContactPhoneId").val();

                phone.StatusId = $("#otherContactAddPhone .ddlContactPhoneStatus .ddl").data("kendoDropDownList").value();
                phone.PhoneTypeId = $("#otherContactAddPhone .ddlContactPhoneType .ddl").data("kendoDropDownList").value();
                if ($("#otherContactAddPhone .chkContactIsForeign").prop("checked")) {
                    phone.IsForeignPhone = true;
                } else {
                    phone.IsForeignPhone = false;
                }

                phone.Phone = $("#otherContactAddPhone input.txtContactPhone").val();
                phone.Extension = $("#otherContactAddPhone input.txtContactPhoneExtension").val();
            }
            if ($("#otherContactAddEmail .txtContactEmail").val().length > 0) {
                email = new LeadOtherContactEmailModel();
                email.LeadId = leadOtherContact.LeadId;
                email.OtherContactId = leadOtherContact.OtherContactId;
                email.ModUser = leadOtherContact.ModUser;
                email.OtherContactsEmailId = $("#hdnOtherContactEmailId").val();

                email.StatusId = $("#otherContactAddEmail .ddlContactEmailStatus .ddl").data("kendoDropDownList").value();
                email.EmailTypeId = $("#otherContactAddEmail .ddlContactEmailType .ddl").data("kendoDropDownList").value();
                email.Email = $("#otherContactAddEmail .txtContactEmail").val();
            }

            if ($("#otherContactAddAddress .txtContactAddress1").val().length > 0) {
                address = new LeadOtherContactAddressModel();
                address.LeadId = leadOtherContact.LeadId;
                address.OtherContactId = leadOtherContact.OtherContactId;
                address.ModUser = leadOtherContact.ModUser;
                address.OtherContactsAddresesId = $("#hdnOtherContactAddressId").val();

                address.AddressTypeId = $("#otherContactAddAddress .ddlContactAddressType .ddl").data("kendoDropDownList").value();
                address.StatusId = $("#otherContactAddAddress .ddlContactAddressStatus .ddl").data("kendoDropDownList").value();
                address.Address1 = $("#otherContactAddAddress .txtContactAddress1").val();
                address.Address2 = $("#otherContactAddAddress .txtContactAddress2").val();
                address.City = $("#otherContactAddAddress .txtContactCity").val();
                address.ZipCode = $("#otherContactAddAddress input.txtContactZipCode").val();
                if ($("#otherContactAddAddress .chkContactIsMaillingAddress").prop("checked")) {
                    address.IsMailingAddress = true;
                } else {
                    address.IsMailingAddress = false;
                }
                if ($("#otherContactAddAddress .chkContactIsInternational").prop("checked")) {
                    address.IsInternational = true;
                } else {
                    address.IsInternational = false;
                }

                if (address.IsInternational) {
                    address.CountryInternational = $("#otherContactAddAddress .txtContactCountry").val();
                    address.StateInternational = $("#otherContactAddAddress .txtContactState").val();
                    address.CountyInternational = $("#otherContactAddAddress .txtContactCounty").val();
                } else {
                    address.CountryId = $("#otherContactAddAddress .ddlContactCountry .ddl").data("kendoDropDownList").value();
                    address.StateId = $("#otherContactAddAddress .ddlContactState .ddl").data("kendoDropDownList").value();
                    address.CountyId = $("#otherContactAddAddress .ddlContactCounty .ddl").data("kendoDropDownList").value();
                }
            }

            var url = "";
            var data = undefined;
            var onSuccessEdit = undefined;

            if (saveMode === viewModel.SAVE_MODE_ADD_OTHER_INFO) {
                url = viewModel.XPOST_LEAD_OTHER_CONTACTS;

                if (address != undefined)
                    leadOtherContact.Addresses.push(address);

                if (email != undefined)
                    leadOtherContact.Emails.push(email);

                if (phone != undefined)
                    leadOtherContact.Phones.push(phone);

                data = leadOtherContact;
                onSuccessEdit = function (msg) { viewModel.rebindOtherContactsDataSource(msg.Data); }
            }
            else if (saveMode === viewModel.SAVE_MODE_EDIT_OTHER_INFO) {
                url = viewModel.XPOST_LEAD_OTHER_CONTACTS;

                data = leadOtherContact;
                onSuccessEdit = function (msg) {
                    viewModel.rebindOtherContactsDataSource(msg.Data);
                    $('.viewOtherContactDetails[data-elementId="' + $("#hdnOtherContactId").val() + '"]').click();
                }
            }
            else if (saveMode === viewModel.SAVE_MODE_EDIT_PHONE_NUMBER) {
                url = viewModel.XPOST_LEAD_OTHER_CONTACTS_PHONE;
                data = phone;
                onSuccessEdit = function (msg) {
                    viewModel.rebindOtherContactsDataSource(msg.Data);
                    $('.viewOtherContactDetails[data-elementId="' + $("#hdnOtherContactId").val() + '"]').click();
                }
            }
            else if (saveMode === viewModel.SAVE_MODE_EDIT_EMAIL) {
                url = viewModel.XPOST_LEAD_OTHER_CONTACTS_EMAIL;
                data = email;
                onSuccessEdit = function (msg) {
                    viewModel.rebindOtherContactsDataSource(msg.Data);
                    $('.viewOtherContactDetails[data-elementId="' + $("#hdnOtherContactId").val() + '"]').click();
                }
            }
            else if (saveMode === viewModel.SAVE_MODE_ADDRESS) {
                url = viewModel.XPOST_LEAD_OTHER_CONTACTS_ADDRESS;
                data = address;
                onSuccessEdit = function (msg) {
                    viewModel.rebindOtherContactsDataSource(msg.Data);
                    $('.viewOtherContactDetails[data-elementId="' + $("#hdnOtherContactId").val() + '"]').click();
                }
            }

            viewModel.showLoading(true);

            $.ajax({
                url: url,
                data: JSON.stringify(data),
                type: "post",
                timeout: 20000,
                dataType: "json",
                contentType: "application/json"
            }).fail(function (jqXHR, textStatus, errorThrown) {
                viewModel.showLoading(false);
                viewModel.showErrorNotification(textStatus + ": " + errorThrown);
            }).done(function (msg) {
                viewModel.showLoading(false);
                if (msg.ResponseCode === 200) {
                    $("#otherContactsForm").data("kendoWindow").center().close();
                    onSuccessEdit(msg);
                    viewModel.showSuccessNotification();
                } else {
                    viewModel.showErrorNotification(msg.ResponseMessage);
                }
            });

        },
        deleteOtherContacts: function (e) {
            var isEditMode = $(e.currentTarget).hasClass("deleteOtherContacts");
            if (isEditMode) {
                viewModel.confirmNotification(function () {
                    viewModel.showLoading(true);

                    var leadInfo = new LeadOtherContactModel();
                    var row = $(e.currentTarget).closest("tr");
                    var columns = $(row).find("td");
                    leadInfo.LeadId = $("#hdnLeadId").val();
                    leadInfo.ModUser = $("#hdnUserId").val();
                    leadInfo.OtherContactId = $(columns)[0].innerText;

                    $.ajax({
                        url: viewModel.XDELETE_LEAD_OTHER_CONTACTS,
                        data: JSON.stringify(leadInfo),
                        type: "post",
                        timeout: 20000,
                        dataType: "json",
                        contentType: "application/json"
                    }).fail(function (jqXHR, textStatus, errorThrown) {
                        viewModel.showLoading(false);
                        viewModel.showErrorNotification(textStatus + ": " + errorThrown);
                    }).done(function (msg) {
                        viewModel.showLoading(false);
                        if (msg.ResponseCode === 200) {
                            viewModel.rebindOtherContactsDataSource(msg.Data);
                            viewModel.showSuccessDeletedNotification();
                            $('[data-contactdetails="' + leadInfo.OtherContactId + '"]').remove();
                        } else {
                            viewModel.showErrorNotification(msg.ResponseMessage);
                        }
                    });
                });
            }
        },
        showLoading: function (show) {
            var pageLoading = $(document).find(".page-loading");
            if (pageLoading.length === 0)
                $(document).find("body").append('<div class="page-loading"></div>');

            kendo.ui.progress($(".page-loading"), show);
        },
        showErrorNotification: function (msg) {
            MasterPage.SHOW_ERROR_WINDOW_PROMISE(msg);
        },
        showSuccessNotification: function () {
            MasterPage.SHOW_INFO_WINDOW_PROMISE("Information was saved successfully!");
        },
        showSuccessDeletedNotification: function () {
            MasterPage.SHOW_INFO_WINDOW_PROMISE("Information was deleted successfully!");
        },
        confirmNotification: function (onConfirm) {
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this record?"))
                        .then(function (confirmed) {
                            if (confirmed) {
                                onConfirm();
                            } else {
                                // do nothing
                            }
                        });
        },
        rebindDataSources: function (datasources) {
            viewModel.leadPhonesDataSource = datasources.LeadPhonesList;
            viewModel.leadEmailDataSource = datasources.LeadEmailList;
            viewModel.leadAddressDataSource = datasources.LeadAddressList;

            var dataSourcePhone = new kendo.data.DataSource({ data: viewModel.leadPhonesDataSource });
            var leadPhonesGrid = $("#leadPhonesGrid").data("kendoGrid");
            dataSourcePhone.read();
            leadPhonesGrid.setDataSource(dataSourcePhone);
            leadPhonesGrid.refresh();

            var dataSourceEmail = new kendo.data.DataSource({ data: viewModel.leadEmailDataSource });
            var leadEmailGrid = $("#leadEmailGrid").data("kendoGrid");
            dataSourceEmail.read();
            leadEmailGrid.setDataSource(dataSourceEmail);
            leadEmailGrid.refresh();

            var dataSourceAddress = new kendo.data.DataSource({ data: viewModel.leadAddressDataSource });
            var leadAddressGrid = $("#leadAddressGrid").data("kendoGrid");
            dataSourceAddress.read();
            leadAddressGrid.setDataSource(dataSourceAddress);
            leadAddressGrid.refresh();
        },
        rebindOtherContactsDataSource: function (datasource) {
            viewModel.otherContactsDataSource = null;
            viewModel.otherContactsDataSource = datasource;

            var data = new kendo.data.DataSource({ data: viewModel.otherContactsDataSource });
            var leadOtherContactsGrid = $("#leadOtherContactsGrid").data("kendoGrid");
            data.read();
            leadOtherContactsGrid.setDataSource(data);
            leadOtherContactsGrid.refresh();
        },
        validateForm: function (formId, onSuccess) {
            var validator = MasterPage.ADVANTAGE_VALIDATOR(formId);
            var thisOnSuccess = onSuccess;
            if (validator !== undefined && validator !== null) {
                if (validator.validate()) {
                    if (thisOnSuccess !== undefined) {
                        thisOnSuccess();
                    } else {
                        MasterPage.SHOW_WARNING_WINDOW_PROMISE("Please fill in all required fields to continue.");
                    }
                }
            } else {
                MasterPage.SHOW_WARNING_WINDOW_PROMISE("Please fill in all required fields to continue.");
            }
        },
        clearRequiredDropdown: function (dropdown) {
            $(dropdown.split(",")).each(function (index, element) {
                var input = $(element).data("kendoDropDownList");
                input.unbind("change");
                input.bind("change", function (e) {
                    $(input.wrapper).children(".k-dropdown-wrap").children('.k-input').removeClass("valueRequired");
                });
            });
        }
    });

    $(function () {
        viewModel.pageValidator = MasterPage.ADVANTAGE_VALIDATOR("leadContacts");
        viewModel.initializeLeadPhoneGrid();

        $("#lnkAddPhone").click(function (e) {
            viewModel.initAddEditPhoneWindow(e);
        });
        $("#lnkAddEmail").click(function (e) {
            viewModel.initAddEditEmailWindow(e);
        });
        $("#lnkAddAddress").click(function (e) {
            viewModel.initAddEditAddressWindow(e);
        });
        $("#lnkAddOtherContacts").click(function (e) {
            viewModel.initAddEditOtherContactsWindow(e);
        });
        $("#lnkSaveComments").click(function (e) {
            viewModel.saveLeadComment();
        });
        $("#lnkDeleteComments").click(function (e) {
            $("#leadCommentsText").val("");
            viewModel.saveLeadComment();
        });
        $("#btnGoBack").click(function () {
            document.location.href = document.location.href.replace("leadContacts", "AleadInfoPage").replace("?resid=270", "?resid=170");
        });
        $("#btnShowAllDetails").click(function () {
            $("#leadOtherContactsGrid .viewOtherContactDetails").each(function (index, element) {
                viewModel.showOtherContactDetails(true, element);
            });
            if (!$("#otherContactsCollapsable").is(":visible")) {
                $("#lnkCollapOtherContacts").click();
            }
        });
        $(".lnkCollapse").click(function (e) {
            if ($(e.currentTarget).find("span").hasClass("collapse")) {
                $("#" + $(e.currentTarget).attr("data-Container")).hide("slow");
                $(e.currentTarget).find("span").removeClass("collapse");
                $(e.currentTarget).find("span").addClass("expand");
                $(e.currentTarget).parent().addClass("border-separator");
            }
            else {
                $("#" + $(e.currentTarget).attr("data-Container")).show("slow");
                $(e.currentTarget).find("span").removeClass("expand");
                $(e.currentTarget).find("span").addClass("collapse");
                $(e.currentTarget).parent().removeClass("border-separator");
            }
        });
    });


})(window.$, window.kendo, window.datasources, window.kendoControlSetup);


