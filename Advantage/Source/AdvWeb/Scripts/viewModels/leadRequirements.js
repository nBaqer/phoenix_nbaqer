﻿(function ($, kendo, datasources) {

    var deleteCounter = 0;
    //----------------------------------------------------------------------------------------
    // View Model that represents the data to be bound to the 
    // view
    //----------------------------------------------------------------------------------------
    var viewModel = kendo.observable({

        requirementsDataSource: datasources.requirementsDataSource,
        optionalRequirementsDataSource: datasources.optionalRequirementsDataSource,
        reqGroupDataSource: datasources.requirementGrpDataSource,
        reqByGroupDataSource: datasources.requirementByGrpDataSource,
        reqDetailsByReqIdDataSource: datasources.requirementDetailsByReqIdDataSource,
        transCodeDataSource: datasources.transactionCodeDataSource,
        documentRequirementId: "",
        selectedRequirementId: "",
        isCancellingForm: false,
        activeFormData: undefined,
        documentStatuses: undefined,

        initializeGrid: function () {

            viewModel.requirementsDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            viewModel.requirementsDataSource.transport.options.read.data.IsMandatory = true;
            viewModel.requirementsDataSource.transport.options.read.data.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            viewModel.requirementsDataSource.transport.options.read.data.LeadId = $("#hdnLeadId").val();
            viewModel.requirementsDataSource.transport.options.read.data.UserId = $("#hdnUserId").val();

            //setup kendo grid for log
            $("#mandReqsGrid").kendoGrid({
                dataSource: viewModel.requirementsDataSource,

                dataBound: function () {
                },
                sortable: false,
                selectable: false,
                scrollable: true,
                width: 480,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    { field: "Id", title: "Id", hidden: true },
                    { field: "OverReason", title: "OverReason", hidden: true },
                    { field: "DocumentStatusId", title: "DocumentStatusId", hidden: true },
                    { field: "IsApproved", title: "Met", width: 30, template: "# if (IsApproved === true) { # <span class='k-icon k-i-check font-green'></span>" + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;cursor:pointer;" } },
                    { field: "Description", title: "Name", width: 120, template: "# if (DateReceived){ # <span class='descriptionClick'>#=Description #</span> #} else { # <span >#=Description #</span> # }#" },
                    { field: "DisplayType", title: "Type", width: 60 },
                    { field: "DateReceived", title: "Received", template: "#= kendo.toString(DateReceived,'MM/dd/yyyy') #", width: 63 },
                    { field: "ApprovalDate", title: "Approved", template: "#= kendo.toString(ApprovalDate,'MM/dd/yyyy') #", width: 63 },
                    { field: "IsOverridden", title: "Overridden", width: 70, template: "# if (IsOverridden === true) { # <span class='isOverriden'>X</span>" + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                    { field: "ApprovedBy", title: "User", width: 65 },
                    { field: "Update", title: "Update", width: 44, template: "<a href='javascript:void(0);' class='icon updateMandClick'><span class='k-icon k-i-plus-circle font-blue-darken-1'></span></a>", attributes: { style: "text-align:center;cursor:pointer;" } }]
            });

            var grid = $("#mandReqsGrid").data("kendoGrid");

            //bind click event to the checkbox
            grid.table.on("click", ".descriptionClick", viewModel.updateMandGridSelect);

            grid.table.on("click", ".updateMandClick", viewModel.updateMandGridSelect);

            $.ajax({
                url: AD.XGET_SERVICE_LAYER_CATALOGS_COMMAND_GET + "?fldName=DocumentStatus",
                type: "GET",
                timeout: 20000,
                dataType: "json",
                asyn: true,
                success: function (data) {
                    viewModel.documentStatuses = data;
                }
            });

            viewModel.haveMetRequirements();
        },
        initializeGrpGrid: function () {

            viewModel.reqGroupDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            viewModel.reqGroupDataSource.transport.options.read.data.IsMandatory = true;
            viewModel.reqGroupDataSource.transport.options.read.data.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;

            viewModel.reqGroupDataSource.transport.options.read.data.leadId = $("#hdnLeadId").val();

            //setup kendo grid for log
            var grid = $("#mandReqGrpGrid").data("kendoGrid");
            if (grid === undefined || grid === null) {
                $("#mandReqGrpGrid")
                    .kendoGrid({
                        dataSource: viewModel.reqGroupDataSource,
                        dataBound: function () {

                            this.pager.element.find(".k-status-text")
                                .remove()
                                .end()
                                .append('<div class="k-status-text">&nbsp;Number of Requirements: ' +
                                    this.dataSource.view().length +
                                    "</div>");


                        },
                        sortable: false,
                        detailInit: viewModel.detailInit,
                        selectable: false,
                        scrollable: true,
                        resizable: true,
                        width: 480,
                        pageable: {
                            refresh: true,
                            pageSizes: true,
                            buttonCount: 5
                        },
                        columns: [
                            { field: "Id", title: "Id", hidden: true },
                            {
                                field: "IsApproved",
                                title: "Met",
                                width: 30,
                                template: "# if (IsApproved === true) { # " +
                                    " <span class='k-icon k-i-check font-green'></span> " +
                                    "# } else { #" +
                                    "" +
                                    "# }#",
                                attributes: { style: "text-align:center;" }
                            },
                            { field: "Description", title: "Name", width: 125 },
                            { field: "DisplayType", title: "Type", width: 50 },
                            {
                                field: "DateReceived",
                                title: "Received",
                                template: "#= kendo.toString(DateReceived,'MM/dd/yyyy') #",
                                width: 63
                            },
                            {
                                field: "ApprovalDate",
                                title: "Approved",
                                template: "#= kendo.toString(ApprovalDate,'MM/dd/yyyy') #",
                                width: 63
                            },
                            {
                                field: "IsOverridden",
                                title: "Overridden",
                                width: 60,
                                template: "# if (IsOverridden === true) { # <span class='isOverriden'>X</span>" +
                                    "# } else { #" +
                                    "" +
                                    "# }#",
                                attributes: { style: "text-align:center;" }
                            },
                            { field: "ApprovedBy", title: "User", width: 65 }
                        ]
                    });
            } else {
                grid.refresh();
            }
        },
        detailInit: function (e) {
            var requirementByGrpDataSource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: function (options) {
                            var uri = options.baseUrl + "/proxy/api/Leads/{grpId}/Requirements/GetRequirementsByGroupId".supplant({ grpId: e.data.Id });
                            delete options.baseUrl;
                            return uri;
                        },
                        asyn: true,
                        dataType: "json",
                        timeout: 90000,
                        type: "GET",
                        data: {
                            baseUrl: XMASTER_GET_BASE_URL,
                            CampusId: XMASTER_GET_CURRENT_CAMPUS_ID,
                            StatusCode: "A",
                            IsMandatory: "True",
                            leadId: $("#hdnLeadId").val(),
                            grpId: e.data.Id
                        }
                    }
                }
            });

            requirementByGrpDataSource.read();

            var grid = $(e.detailCell).find(".requirementGroupDetailLink").data("kendoGrid");
            if (grid === undefined || grid === null) {
                $("<div class='requirementGroupDetailLink'></div>").appendTo(e.detailCell);
            } else {
                grid.destroy();
                $(e.detailCell).find(".requirementGroupDetailLink").html("");
                grid = undefined;
            }

            $(e.detailCell).find(".requirementGroupDetailLink").kendoGrid({
                dataSource: requirementByGrpDataSource,
                scrollable: false,
                sortable: true,
                width: 480,
                pageable: {
                    info: false,
                    refresh: false,
                    pageSizes: false,
                    previousNext: false,
                    numeric: false
                },
                dataBound: function () {

                    this.table.on("click", ".descriptionClick", function () {

                        var row = $(this).closest("tr");

                        var reqId = row[0].cells[0].innerText;
                        var type = row[0].cells[3].innerText;

                        var isApproved = row[0].cells[1].innerHTML.length > 0;
                        var isOverridden = row[0].cells[6].innerHTML.length > 0;

                        var dataItem = {
                            Id: row[0].cells[0].innerText,
                            IsApproved: isApproved,
                            Description: row[0].cells[2].innerText,
                            DisplayType: row[0].cells[3].innerText,
                            DateReceived: row[0].cells[4].innerText,
                            ApprovalDate: row[0].cells[5].innerText,
                            IsOverridden: isOverridden,
                            ApprovedBy: row[0].cells[7].innerText,
                            OverrideReason: row[0].cells[8].innerText,
                            MinScore: row[0].cells[9].innerText,
                            Score: row[0].cells[10].innerText,
                            DocumentRequirementId: row[0].cells[11].innerText,
                            DocumentId: reqId
                        };

                        viewModel.SetupUpdatePanels(dataItem, type);
                    });

                    this.table.on("click", ".updateDetailClick", function () {

                        var row = $(this).closest("tr");

                        var reqId = row[0].cells[0].innerText;
                        var type = row[0].cells[3].innerText;

                        var isApproved = row[0].cells[1].innerHTML.length > 0;
                        var isOverridden = row[0].cells[6].innerHTML.length > 0;

                        var dataItem = {
                            Id: row[0].cells[0].innerText,
                            IsApproved: isApproved,
                            Description: row[0].cells[2].innerText,
                            DisplayType: row[0].cells[3].innerText,
                            DateReceived: row[0].cells[4].innerText,
                            ApprovalDate: row[0].cells[5].innerText,
                            IsOverridden: isOverridden,
                            ApprovedBy: row[0].cells[7].innerText,
                            OverrideReason: row[0].cells[8].innerText,
                            MinScore: row[0].cells[9].innerText,
                            Score: row[0].cells[10].innerText,
                            DocumentRequirementId: row[0].cells[11].innerText,
                            DocumentId: reqId
                        };

                        viewModel.SetupUpdatePanels(dataItem, type);

                    }).attr("class", "updateRequirementGroupDetailGrid");

                },
                columns: [{ field: "Id", title: "Id", hidden: true },
                    { field: "IsApproved", title: "Met", width: 35, template: "# if (IsApproved === true) { # <span class='k-icon k-i-check font-green'></span> " + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                    { field: "Description", title: "Name", width: 105, template: "# if (DateReceived){ # <span class='descriptionClick'>#=Description #</span> #} else { # <span >#=Description #</span> # }# # if (IsMandatory === true) {# <span class='red'>*</span> #}#" },
                    { field: "DisplayType", title: "Type", width: 65 },
                    { field: "DateReceived", title: "Received", template: "#= kendo.toString(DateReceived,'MM/dd/yyyy') #", width: 65 },
                    { field: "ApprovalDate", title: "Approved", template: "#= kendo.toString(ApprovalDate,'MM/dd/yyyy') #", width: 65 },
                    { field: "IsOverridden", title: "Overridden", width: 60, template: "# if (IsOverridden === true) { # <span class='isOverriden'>X</span>" + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                    { field: "ApprovedBy", title: "User", width: 65 },
                    { field: "OverReason", title: "", width: 90, hidden: true },
                    { field: "MinScore", title: "", width: 90, hidden: true },
                    { field: "Score", title: "", width: 90, hidden: true },
                    { field: "DocumentRequirementId", title: "", width: 90, hidden: true },
                    { field: "Update", title: "Update", width: 49, template: "<a href='javascript:void(0);' class='icon updateDetailClick'><span class='k-icon k-i-plus-circle font-blue-darken-1'></span></a>", attributes: { style: "text-align:center;cursor:pointer;" } },
                    { field: "DocumentStatusId", title: "DocumentStatusId", hidden: true }]
            });
            grid = $(e.detailCell).find(".requirementGroupDetailLink").data("kendoGrid");
            grid.refresh();
        },
        initializeOptionalGrid: function () {

            viewModel.optionalRequirementsDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            viewModel.optionalRequirementsDataSource.transport.options.read.data.IsMandatory = false;
            viewModel.optionalRequirementsDataSource.transport.options.read.data.CampusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            viewModel.optionalRequirementsDataSource.transport.options.read.data.leadId = $("#hdnLeadId").val();

            //setup kendo grid for log
            $("#optReqsGrid").kendoGrid({
                dataSource: viewModel.optionalRequirementsDataSource,
                dataBound: function () {
                    kendo.ui.progress($("#optReqsGrid"), false);
                },
                sortable: false,
                selectable: false,
                scrollable: true,
                resizable: true,
                width: 480,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    { field: "Id", title: "Id", hidden: true },
                    { field: "IsApproved", title: "Met", width: 30, template: "# if (IsApproved === true) { # <span class='k-icon k-i-check font-green'></span> " + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                    { field: "Description", title: "Name", width: 110, template: "# if (DateReceived){ # <span class='descriptionClick'>#=Description #</span> #} else { # <span >#=Description #</span> # }#" },
                    { field: "DisplayType", title: "Type", width: 60 },
                    { field: "DateReceived", title: "Received", template: "#= kendo.toString(DateReceived,'MM/dd/yyyy') #", width: 65 },
                    { field: "ApprovalDate", title: "Approved", template: "#= kendo.toString(ApprovalDate,'MM/dd/yyyy') #", width: 65 },
                    { field: "IsOverridden", title: "Overridden", width: 65, template: "# if (IsOverridden === true) { # <span class='isOverriden'>X</span>" + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                    { field: "ApprovedBy", title: "User", width: 65 },
                    { field: "Update", title: "Update", width: 55, template: "<a href='javascript:void(0);' class='icon updateOptClick'><span class='k-icon k-i-plus-circle font-blue-darken-1'></span></a>", attributes: { style: "text-align:center;cursor:pointer;" } }]
            });

            var grid = $("#optReqsGrid").data("kendoGrid");

            //bind click event to the checkbox
            grid.table.on("click", ".descriptionClick", viewModel.updateOptGridSelect);

            grid.table.on("click", ".updateOptClick", viewModel.updateOptGridSelect);

        },
        initializeDocumentDetailsGrid: function (reqId, reqType) {
            var uri = XMASTER_GET_BASE_URL + "/proxy/api/Leads/" + $("#hdnLeadId").val() + "/Requirements/GetRequirementsDetailsByReqId";
            var filter = { 'CampusId': XMASTER_GET_CURRENT_CAMPUS_ID, 'reqId': reqId, 'RequirementType': reqType, 'LeadId': $("#hdnLeadId").val() };

            var grid = $("#docReqGrid").data("kendoGrid");
            if (grid !== null && grid !== undefined) {
                grid.destroy();
                $("#docReqGrid").html("");
                grid = undefined;
            }

            var requestData2 = $.getJSON(uri, filter, function (data) {
                viewModel.set("reqDetailsByReqIdDataSource", data.DocumentHistory);

                $("#docReqGrid").kendoGrid({
                    dataSource: viewModel.reqDetailsByReqIdDataSource,
                    dataBound: function () {
                        kendo.ui.progress($("#docReqGrid"), false);
                    },
                    sortable: false,
                    selectable: false,
                    scrollable: true,
                    resizable: true,
                    pageable: false,
                    columns: [
                        { field: "Id", title: "Id", hidden: true },
                        { field: "FileNameWithExtension", title: "File Name" },
                        { field: "FilePath", title: "File Name", hidden: true },
                        { field: "User", title: "User"},
                        { field: "UploadDate", title: "Uploaded", template: "#= kendo.toString(UploadDate,'MM/dd/yyyy') #", width: 90 },
                        { field: "View", title: "", width: 45, template: "<span class='docdescriptionClick'>View</span>" },
                        { field: "Delete", title: "", width: 53, template: "<a class='imageDeleteClick' href='javascript:void(0);'> <span class='k-icon k-i-close font-red'></span></a>", attributes: { style: "text-align:center;" } }
                    ]
                });

                if (grid === undefined || grid === null) {
                    grid = $("#docReqGrid").data("kendoGrid");
                    grid.table.on("click", ".docdescriptionClick", viewModel.openFileBrowser);
                    grid.table.on("click", ".imageDeleteClick", viewModel.deleteDocumentFile);
                }
            });

        },
        initializeTestDetailsGrid: function (reqId, reqType) {

            var uri = XMASTER_GET_BASE_URL + "/proxy/api/Leads/" + $("#hdnLeadId").val() + "/Requirements/GetRequirementsDetailsByReqId";
            var filter = { 'CampusId': XMASTER_GET_CURRENT_CAMPUS_ID, 'reqId': reqId, 'RequirementType': reqType, 'LeadId': $("#hdnLeadId").val() };

            var grid = $("#testReqGrid").data("kendoGrid");

            var requestData2 = $.getJSON(uri, filter, function (data) {
                viewModel.set("reqDetailsByReqIdDataSource", data.TestHistory);

                var testName = $("#txtTestDescription").text();

                $("#testReqGrid").kendoGrid({
                    dataSource: viewModel.reqDetailsByReqIdDataSource,
                    dataBound: function () {
                        kendo.ui.progress($("#testReqGrid"), false);
                    },
                    sortable: false,
                    selectable: false,
                    scrollable: true,
                    resizable: true,
                    pageable: false,
                    columns: [
                        { field: "Id", title: "Id", hidden: true },
                        { field: "Description", title: "Test Name", template: testName },
                        { field: "DateReceived", title: "Completed", width: 80 },
                        { field: "Score", title: "Result", width: 50, template: "#: kendo.toString(Score, 'n2') #" },
                        { field: "Pass", title: "Pass", width: 45 },
                        { field: "IsOverridden", title: "Overridden", width: 80, template: "# if (IsOverridden === true) { # <span class='isOverriden'>X</span>" + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                        { field: "Delete", title: "", width: 50, template: "<a class='imageTestDeleteClick'  href='javascript:void(0);'> <span class='k-icon k-i-close font-red'></span></a>", attributes: { style: "text-align:center;" } }]
                });

                if (grid === undefined || grid === null) {
                    grid = $("#testReqGrid").data("kendoGrid");
                    grid.table.on("click", ".imageTestDeleteClick", viewModel.deleteTestRecord);
                }
            });
        },
        initializeLeadEventsDetailsGrid: function (reqId, reqType) {

            var header = reqType + " Name";

            var uri = XMASTER_GET_BASE_URL + "/proxy/api/Leads/" + $("#hdnLeadId").val() + "/Requirements/GetRequirementsDetailsByReqId";
            var filter = { 'CampusId': XMASTER_GET_CURRENT_CAMPUS_ID, 'reqId': reqId, 'RequirementType': reqType, 'LeadId': $("#hdnLeadId").val() };

            var grid = $("#reqDetailGrid").data("kendoGrid");
            if (grid !== undefined && grid !== null) {
                $(grid.thead).find('th[data-field="Description"]').html(header);
            }
            var requestData2 = $.getJSON(uri, filter, function (data) {
                viewModel.set("reqDetailsByReqIdDataSource", data.ReqsHistory);

                $("#reqDetailGrid").kendoGrid({
                    dataSource: viewModel.reqDetailsByReqIdDataSource,
                    dataBound: function () {
                        kendo.ui.progress($("#reqDetailGrid"), false);
                    },
                    sortable: false,
                    selectable: false,
                    scrollable: true,
                    resizable: true,
                    pageable: false,
                    columns: [
                        { field: "Id", title: "Id", hidden: true },
                        { field: "Description", title: header },
                        { field: "DateReceived", title: "Completed", width: 80 },
                        { field: "IsOverridden", title: "Overridden", width: 80, template: "# if (IsOverridden === true) { # <span class='k-icon k-i-check font-green'></span> " + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                        { field: "Delete", title: "", width: 50, template: "<a class='imageEventDeleteClick' href='javascript:void(0);'> <span class='k-icon k-i-close font-red'></span></a>", attributes: { style: "text-align:center;" } }]
                });

                if (grid === undefined || grid === null) {
                    grid = $("#reqDetailGrid").data("kendoGrid");
                    grid.table.on("click", ".imageEventDeleteClick", viewModel.deleteEventRecord);
                }

                $(grid.thead).find('th[data-field="Description"]').html(header);

            });
        },
        initializeLeadTransDetailsGrid: function (reqId, reqType) {

            var uri = XMASTER_GET_BASE_URL + "/proxy/api/Leads/" + $("#hdnLeadId").val() + "/Requirements/GetRequirementsDetailsByReqId";
            var filter = { 'CampusId': XMASTER_GET_CURRENT_CAMPUS_ID, 'reqId': reqId, 'RequirementType': reqType, 'LeadId': $("#hdnLeadId").val() };

            var grid = $("#tranReqGrid").data("kendoGrid");

            var requestData2 = $.getJSON(uri, filter, function (data) {

                //viewModel.reqDetailsByReqIdDataSource = data.FeeHistory;
                viewModel.set("reqDetailsByReqIdDataSource", data.FeeHistory);
                var imgSource = XMASTER_GET_BASE_URL + "/images/msg/Queue_Info.gif";

                if (grid !== undefined && grid !== null) {
                    grid.destroy();
                    $("#tranReqGrid").html("");
                    grid = undefined;
                }

                $("#tranReqGrid")
                    .kendoGrid({
                        dataSource: viewModel.reqDetailsByReqIdDataSource,
                        dataBound: function () {
                            kendo.ui.progress($("#tranReqGrid"), false);
                        },
                        sortable: false,
                        selectable: false,
                        scrollable: true,
                        height: 150,
                        resizable: true,
                        pageable: false,
                        columns: [
                            { field: "Id", title: "Id", hidden: true },
                            {
                                field: "TransDate",
                                title: "Date",
                                template: "#= kendo.toString(TransDate,'MM/dd/yyyy') #",
                                width: 65
                            },
                            {
                                field: "TranDescription",
                                title: "Description",
                                template: '<span class="moreClick">#=TranDescription #</span>',
                                width: 110
                            },
                            {
                                field: "Amount",
                                title: "Amount",
                                width: 80,
                                attributes: { style: "text-align:right;" }
                            },
                            { field: "User", title: "User", width: 55 },
                            {
                                field: "",
                                title: "",
                                width: 35,
                                template: '<span class="printClick">Print</span>',
                                attributes: { style: "text-align:center;" }
                            },
                            {
                                field: "",
                                title: "",
                                width: 55,
                                template: '# if (IsVoided === true) {# Voided #} else{# <span class="voidClick">Void</span> #}#',
                                attributes: { style: "text-align:center;" }
                            }
                        ]
                    });

                if (grid === undefined || grid === null) {
                    grid = $("#tranReqGrid").data("kendoGrid");
                    grid.table.on("click", ".moreClick", viewModel.initMoreWindow);

                    grid.table.on("click", ".printClick", viewModel.openTransactionRcptFromLink);

                    grid.table.on("click", ".voidClick", viewModel.voidTransaction);
                }
                grid.refresh();
            });
        },
        refreshGrids: function () {
            if (viewModel.requirementsDataSource !== undefined) {
                viewModel.requirementsDataSource.read();
                $("#mandReqsGrid").data("kendoGrid").refresh();
            }
            if (viewModel.reqGroupDataSource !== undefined) {
                viewModel.reqGroupDataSource.read();
                $("#mandReqGrpGrid").data("kendoGrid").refresh();
                //viewModel.initializeGrpGrid();
            }
            if (viewModel.optionalRequirementsDataSource !== undefined) {
                viewModel.optionalRequirementsDataSource.read();
                $("#optReqsGrid").data("kendoGrid").refresh();
            }
            var requirementGroupDetailGrid = $(".requirementGroupDetailGrid").data("kendoGrid");
            if (requirementGroupDetailGrid !== undefined && requirementGroupDetailGrid !== null) {
                requirementGroupDetailGrid.refresh();
            }
            viewModel.haveMetRequirements();
        },
        updateMandGridSelect: function (e) {

            var row = $(this).closest("tr");
            var dataItem = $("#mandReqsGrid").data("kendoGrid").dataItem(row);
            var type = dataItem.DisplayType;

            viewModel.SetupUpdatePanels(dataItem, type);

        },
        updateOptGridSelect: function (e) {

            var row = $(this).closest("tr");
            var dataItem = $("#optReqsGrid").data("kendoGrid").dataItem(row);

            var type = dataItem.DisplayType;

            viewModel.SetupUpdatePanels(dataItem, type);

        },
        SetupUpdatePanels: function (dataItem, type) {
            viewModel.isCancellingForm = false;
            $(window).unbind("beforeunload");
            $(window).bind("beforeunload",
                function () {
                    if (!viewModel.isCancellingForm && (viewModel.activeFormData !== undefined && (viewModel.activeFormData !== viewModel.serialize("#two")))) {
                        return "Changes you made may not be saved";
                    }
                });

            viewModel.currentType = type;
            if (type === "Document") {
                $("#updateHeader").show();
                $("#updateContainer").show();
                $("#two").show();
                $("#documentUpdateDiv").show();
                $("#testUpdateDiv").hide();
                $("#leadReqUpdateDiv").hide();
                $("#leadTransactionsUpdateDiv").hide();

                viewModel.initDocUpdateControls(dataItem);
            }
            else
            if (type === "Test") {
                $("#updateContainer").show();
                $("#two").show();
                $("#updateHeader").show();
                $("#documentUpdateDiv").hide();
                $("#testUpdateDiv").show();
                $("#leadReqUpdateDiv").hide();
                $("#leadTransactionsUpdateDiv").hide();

                viewModel.initTestUpdateControls(dataItem);

            }
            else
            if (type === "Event" || type === "Tour" || type === "Interview") {
                $("#updateContainer").show();
                $("#two").show();
                $("#updateHeader").show();
                $("#documentUpdateDiv").hide();
                $("#testUpdateDiv").hide();
                $("#leadReqUpdateDiv").show();
                $("#leadTransactionsUpdateDiv").hide();

                viewModel.initReqUpdateControls(dataItem);

            }
            else
            if (type === "Fee") {
                $("#updateContainer").show();
                $("#two").show();
                $("#updateHeader").show();
                $("#documentUpdateDiv").hide();
                $("#testUpdateDiv").hide();
                $("#leadReqUpdateDiv").hide();
                $("#leadTransactionsUpdateDiv").show();

                viewModel.initTransUpdateControls(dataItem);
            }

            $("#updateBlocker1").show();

            viewModel.activeFormData = viewModel.serialize("#two");
        },
        initDocUpdateControls: function (dataItem) {
            if (!dataItem) {
                viewModel.showNotification("Error retrieving row data!", 3);
                return;
            }

            if ($("#dtReceived").data("kendoDatePicker") === undefined) {
                $("#dtReceived").kendoMaskedDatePicker({
                    start: "month",
                    depth: "month",
                    max: new Date(),
                    value: new Date(),
                    format: "MM/dd/yyyy"
                });
            }

            var docStatus = $("#docStatus").data("kendoDropDownList");
            if (docStatus !== undefined && docStatus !== null) {
                docStatus.destroy();
            }

            $("#docStatus").kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "ID",
                dataSource: viewModel.documentStatuses,
                optionLabel: "Select"
            });

            if ($("#dtApproval").data("kendoDatePicker") === undefined) {
                $("#dtApproval").kendoMaskedDatePicker({
                    start: "month",
                    depth: "month",
                    max: new Date(),
                    value: new Date(),
                    format: "MM/dd/yyyy"
                });
            }

            viewModel.set("selectedRequirementId", dataItem.Id);

            $("#txtDocDescription").text(dataItem.Description);

            viewModel.initFileUpload();

            if (dataItem.DocumentRequirementId) {

                viewModel.set("documentRequirementId", dataItem.DocumentRequirementId);

                $("#fileUploadtr").show();
                $("#files").show();

                $("#chkDocOverridden").prop("checked", dataItem.IsOverridden);
                if (dataItem.IsOverridden === true) {
                    if (dataItem.OverReason !== undefined) {
                        $("#txtDocOverReason").val(dataItem.OverReason);
                    }

                    $("#txtDocOverReason").removeAttr("disabled").removeClass("rfdInputDisabled");

                    $("#dtReceived").data("kendoDatePicker").enable(false);
                    $("#dtApproval").data("kendoDatePicker").enable(false);
                    $("#docStatus").data("kendoDropDownList").enable(false);

                    $("#dtReceived").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                    $("#dtApproval").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                    $("#docStatus").data("kendoDropDownList").select(0);

                } else {
                    $("#txtDocOverReason").val("");
                    $("#txtDocOverReason").attr("disabled", "disabled");
                    $("#dtReceived").data("kendoDatePicker").enable(true);
                    $("#dtApproval").data("kendoDatePicker").enable(true);
                    $("#docStatus").data("kendoDropDownList").enable(true);

                    $("#dtReceived").data("kendoDatePicker").value(dataItem.DateReceived);
                    $("#dtApproval").data("kendoDatePicker").value(dataItem.ApprovalDate);

                    if (dataItem.DocumentStatusId !== undefined) {
                        var docStatusDropdownlist = $("#docStatus").data("kendoDropDownList");
                        docStatusDropdownlist.value(dataItem.DocumentStatusId);
                        docStatusDropdownlist.trigger("change");
                    }
                }

                viewModel.initializeDocumentDetailsGrid(dataItem.Id, dataItem.DisplayType);

            } else {

                viewModel.set("documentRequirementId", "");
                $("#fileUploadtr").hide();
                $("#files").hide();

                $("#chkDocOverridden").prop("checked", false);
                $("#txtDocOverReason").val("");
                $("#txtDocOverReason").attr("disabled", "disabled");
                $("#dtReceived").data("kendoDatePicker").enable(true);
                $("#dtApproval").data("kendoDatePicker").enable(true);
                $("#docStatus").data("kendoDropDownList").enable(true);
                if ($("#docStatus").data("kendoDropDownList") !== undefined) {
                    $("#docStatus").data("kendoDropDownList").select(0);
                }

                $("#dtReceived").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                $("#dtApproval").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));

                viewModel.initializeDocumentDetailsGrid(dataItem.Id, dataItem.DisplayType);

            }

            viewModel.activeFormData = viewModel.serialize("#two");

            viewModel.bindCheckBoxClick("Document");
            viewModel.bindDocSaveButton();
            viewModel.bindDocCancel();

        },

        initTestUpdateControls: function (dataItem) {
            if ($("#dtTestCompleted").data("kendoDatePicker") === undefined) {
                $("#dtTestCompleted").kendoMaskedDatePicker({
                    start: "month",
                    depth: "month",
                    max: new Date(),
                    format: "MM/dd/yyyy"
                });
            }

            utils.setDecimalMaskedTextBox("#txtTestScore");

            $("#txtTestOverReason").val("");
            $("#txtTestScore").val("");
            if (dataItem) {
                viewModel.set("selectedRequirementId", dataItem.Id);
                $("#txtTestDescription").text(dataItem.Description);
                $("#txtMinScore").text(dataItem.MinScore);

                $("#chTestOverridden").prop("checked", dataItem.IsOverridden);

                if (dataItem.IsOverridden) {
                    $("#txtTestOverReason").removeAttr("disabled").removeClass("rfdInputDisabled");
                    $("#dtTestCompleted").data("kendoDatePicker").enable(false);
                    $("#txtTestScore").attr("disabled", "disabled");
                    $("#dtTestCompleted").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                    $("#txtTestScore").val("");
                    if (dataItem.OverReason !== undefined && dataItem.OverReason !== null) {
                        $("#txtTestOverReason").val(dataItem.OverReason);
                    }
                } else {
                    $("#txtTestOverReason").attr("disabled", "disabled");
                    $("#dtTestCompleted").data("kendoDatePicker").enable(true);
                    $("#dtTestCompleted").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                    $("#txtTestScore").removeAttr("disabled").removeClass("rfdInputDisabled");
                    $("#txtTestOverReason").val("");
                }

                viewModel.initializeTestDetailsGrid(dataItem.Id, dataItem.DisplayType);
            } else {
                $("#txtTestOverReason").attr("disabled", "disabled");
                $("#dtTestCompleted").data("kendoDatePicker").enable(true);
                $("#dtTestCompleted").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                $("#txtTestScore").removeAttr("disabled").removeClass("rfdInputDisabled");
                $("#txtTestOverReason").val("");
                $("#chTestOverridden").prop("checked", false);
            }

            viewModel.bindCheckBoxClick("Test");
            viewModel.bindTestSaveButton();
            viewModel.bindTestCancel();

        },
        initReqUpdateControls: function (dataItem) {

            viewModel.set("selectedRequirementId", dataItem.Id);

            if ($("#dtEventCompleted").data("kendoDatePicker") === undefined) {
                $("#dtEventCompleted")
                    .kendoMaskedDatePicker({
                        start: "month",
                        depth: "month",
                        max: new Date(),
                        value: new Date(),
                        format: "MM/dd/yyyy"
                    });
            }
            $("#reqName").text(dataItem.DisplayType);
            $("#txtEventReason").val("");

            if (dataItem) {
                $("#txtEventDescription").text(dataItem.Description);
                $("#chEventOverridden").prop("checked", dataItem.IsOverridden);

                if (dataItem.IsOverridden) {

                    $("#dtEventCompleted").data("kendoDatePicker").enable(false);
                    $("#dtEventCompleted").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));

                    if (dataItem.OverReason !== undefined && dataItem.OverReason !== null) {
                        $("#txtEventReason").val(dataItem.OverReason);
                    }
                    $("#txtEventReason").removeAttr("disabled").removeClass("rfdInputDisabled");
                } else {
                    $("#txtEventReason").attr("disabled", "disabled");
                    $("#dtEventCompleted").data("kendoDatePicker").enable(true);
                    $("#dtEventCompleted").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                }

                viewModel.initializeLeadEventsDetailsGrid(dataItem.Id, dataItem.DisplayType);

            }
            viewModel.bindCheckBoxClick("ReqEvent");
            viewModel.bindReqSaveButton();
            viewModel.bindEventCancel();
        },
        initTransUpdateControls: function (dataItem) {
            if ($("#dtTransDate").data("kendoDatePicker") === undefined) {
                $("#dtTransDate")
                    .kendoMaskedDatePicker({
                        start: "month",
                        depth: "month",
                        format: "MM/dd/yyyy",
                        max: new Date(),
                        value: new Date()
                    });
            }

            var lstPaymentTypes = $("#lstPaymentTypes").data("kendoDropDownList");
            if (lstPaymentTypes !== undefined && lstPaymentTypes !== null) {
                lstPaymentTypes.destroy();
            }
            $("#lstPaymentTypes").kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "Id",
                dataSource: [{ Id: "0", Description: "Select" }, { Id: "1", Description: "Cash" }, { Id: "2", Description: "Check" }, { Id: "3", Description: "Credit Card" },
                    { Id: "4", Description: "EFT" }, { Id: "5", Description: "Money Order" }, { Id: "6", Description: "Non Cash" }],
                minLength: 5,
                change: function () {
                    var payTypeId = this.value();
                    switch (payTypeId) {
                    case "2":
                        $("#paymentLabel").html("Check Number");
                        $("#tblPaymentReference").show();
                        break;
                    case "3":
                        $("#paymentLabel").html("C/C Authorization");
                        $("#tblPaymentReference").show();
                        break;
                    case "4":
                        $("#paymentLabel").html("EFT Number");
                        $("#tblPaymentReference").show();
                        break;
                    case "5":
                        $("#paymentLabel").html("Money Order Number");
                        $("#tblPaymentReference").show();
                        break;
                    case "6":
                        $("#paymentLabel").html("Non Cash Reference #");
                        $("#tblPaymentReference").show();
                        break;
                    default:
                        $("#tblPaymentReference").hide();
                        $("#txtTranPaymentReference").val("");
                    }

                }
            });

            viewModel.transCodeDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            var lstTransactionCodes = $("#lstTransactionCodes").data("kendoDropDownList");
            if (lstTransactionCodes !== undefined && lstTransactionCodes !== null) {
                lstTransactionCodes.destroy();
            }
            $("#lstTransactionCodes").kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "Id",
                dataSource: viewModel.transCodeDataSource,
                dataBound: function () {
                    viewModel.activeFormData = viewModel.serialize("#two");
                    console.log(viewModel.activeFormData);
                },
                minLength: 5,
                optionLabel: {
                    Description: " Select ",
                    Id: 0
                },
                Description: " Select "
            });

            if ($("#dtApproval").data("kendoDatePicker") === undefined) {
                $("#dtApproval")
                    .kendoMaskedDatePicker({
                        start: "month",
                        depth: "month",
                        format: "MM/dd/yyyy",
                        max: new Date(),
                        value: new Date()
                    });
            }

            $("#txtTranAmount").val("$");
            $("#txtTranReason, #txtTranReference, #txtTranDescription, #txtTranPaymentReference").val("");
            $("#dtTransDate").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
            $("#dtApproval").data("kendoDatePicker").value("");
            $("#lstTransactionCodes").data("kendoDropDownList").select(0);
            $("#lstPaymentTypes").data("kendoDropDownList").select(0);
            $("#tblPaymentReference").hide();
            $("#chTranOverridden").prop("checked", false);

            if (dataItem) {

                viewModel.set("selectedRequirementId", dataItem.Id);
                viewModel.set("documentRequirementId", dataItem.DocumentRequirementId);

                $("#txtTransaction").text(dataItem.Description);

                if (dataItem.IsOverridden) {

                    $("#chTranOverridden").prop("checked", true);
                    $("#txtTranReason").removeAttr("disabled").removeClass("rfdInputDisabled");
                    if (dataItem.OverReason !== undefined && dataItem.OverReason !== null) {
                        $("#txtTranReason").val(dataItem.OverReason);
                    }

                    $("#txtTranDescription").attr("disabled", "disabled");
                    $("#txtTranReference").attr("disabled", "disabled");
                    $("#lstTransactionCodes").data("kendoDropDownList").enable(false);
                    $("#lstPaymentTypes").data("kendoDropDownList").enable(false);
                    $("#btnTranSavePrint").attr("disabled", "disabled");
                    $("#dtTransDate").data("kendoDatePicker").enable(false);
                    $("#dtTransDate").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));

                    $("#txtTranAmount").attr("disabled", "disabled");
                }
                else {

                    $("#txtTranReason").attr("disabled", "disabled");

                    $("#txtTranDescription").removeAttr("disabled").removeClass("rfdInputDisabled");
                    $("#txtTranReference").removeAttr("disabled").removeClass("rfdInputDisabled");
                    $("#btnTranSavePrint").removeAttr("disabled").removeClass("rfdInputDisabled");
                    $("#lstTransactionCodes").data("kendoDropDownList").enable(true);
                    $("#lstPaymentTypes").data("kendoDropDownList").enable(true);

                    $("#dtTransDate").data("kendoDatePicker").enable(true);
                    $("#txtTranAmount").removeAttr("disabled").removeClass("rfdInputDisabled");
                }

                viewModel.initializeLeadTransDetailsGrid(dataItem.Id, dataItem.DisplayType);

            }

            viewModel.bindCheckBoxClick("Fee");
            viewModel.bindTransSaveButton();
            viewModel.bindTransSavePrintButton();
            viewModel.bindTranCancel();
            viewModel.bindCurrencyFormatting();

        },
        initMoreWindow: function (e) {

            var x = $("#tranReqGrid").offset().left;
            var y = $("#tranReqGrid").offset().top;

            var row = $(this).closest("tr");
            var dataItem = $("#tranReqGrid").data("kendoGrid").dataItem(row);

            $("#tranDateDetail").html(dataItem.TransDate);
            $("#tranReferenceDetail").html(dataItem.Reference);
            $("#tranDocIdDetail").html(dataItem.TransDocumentId);
            $("#tranCodeDetail").html(dataItem.Code);
            $("#tranDescriptionDetail").html(dataItem.TranDescription);
            $("#tranUserDetail").html(dataItem.User);
            $("#tranAmountDetail").html(dataItem.Amount);

            var type = "";

            switch (dataItem.Type) {
            case "1":
                type = "Cash";
                break;
            case "2":
                type = "Check";
                break;
            case "3":
                type = "Credit Card";
                break;
            case "4":
                type = "EFT";
                break;
            case "5":
                type = "Money Order";
                break;
            case "6":
                type = "Non Cash";
                break;
            default:
                type = "Payment";
                break;
            }

            if (dataItem.IsVoided === true) {
                type += " (Voided)";
            }

            $("#tranTypeDetail").html(type);

            $("#moreDetails").kendoWindow({
                //animation: false,
                modal: false,
                width: "300",
                title: "Transaction Details",
                actions: ["close"],
                resizable: false,
                draggable: true
            });

            $("#moreDetails").closest(".k-window").css({ top: y + 57, left: x + 50 });
            $("#moreDetails").data("kendoWindow").open();

        },
        bindWindowHide: function () {
            //-----------------------------------------------------------
            // If NavBar is visible and user clicks outside the control, 
            // hide it.
            //-----------------------------------------------------------
            $(document).click(function (event) {
                if ($(event.target).parents().index($("#moreDetails")) === -1) {

                    if ($(event.target).parents().context.className !== "moreClick") {
                        if ($("#moreDetails").is(":visible")) {
                            $("#moreDetails").data("kendoWindow").close();
                        }
                    }

                }
            });
        },

        bindDocSaveButton: function () {
            $("#btnDocSave").unbind("click");
            $("#btnDocSave").bind("click", function () {
                $("#updateBlocker2").show();
                viewModel.docSave();
            });
        },
        docSave: function () {
            var userId = $("#hdnUserId").val();
            var leadId = $("#hdnLeadId").val();
            var reqType = "Document";
            var docReqId = viewModel.documentRequirementId;
            var reqId = viewModel.selectedRequirementId;

            var isOverride = $("#chkDocOverridden").is(":checked");

            var status = "";
            if (isOverride === false) status = $("#docStatus").data("kendoDropDownList").value();

            var approvalDate = "";
            if (isOverride === false) approvalDate = $("#dtApproval").data("kendoDatePicker").value();

            if (isOverride === false) {
                if (status === "0" || status === "") {

                    viewModel.showNotification("Document status is required", 2);
                    $("#updateBlocker2").hide();
                    return;
                } else {
                    if (!approvalDate) {
                        viewModel.showNotification("Approval date is required", 2);
                        $("#updateBlocker2").hide();
                        return;
                    }
                }

            }


            var dateReceived = null;
            if (isOverride === false) {
                dateReceived = $("#dtReceived").data("kendoDatePicker").value();

                if (!dateReceived) {
                    viewModel.showNotification("Date Received is required", 2);
                    $("#updateBlocker2").hide();
                    return;
                }
            }


            var overReason = $("#txtDocOverReason").val().trim();
            if (isOverride === false) {
                overReason = "";
                $("#txtDocOverReason").val("");
            } else {
                if (overReason.length === 0) {
                    viewModel.showNotification("Reason is required.", 2);
                    $("#updateBlocker2").hide();
                    return -1;
                }
            }

            var filter = { DocReqId: docReqId, ReqId: reqId, LeadId: leadId, UserId: userId, RequirementType: reqType, IsOverridden: isOverride, overReason: overReason, DateReceived: dateReceived, ApprovalDate: approvalDate, DocumentStatusId: status };
            var url = XMASTER_GET_BASE_URL + "/proxy/api/Leads/Requirements/PostRequirement";
            $.ajax({
                url: url,
                type: "POST",
                dataType: "text",
                data: JSON.stringify(filter),
                success: function () {
                    viewModel.showNotification("Requirement updated.", 1);
                    $("#updateBlocker2").hide();
                    $("#fileUploadtr").show();
                    $("#files").show();
                    viewModel.refreshGrids();
                },
                complete: function () {

                }
            });
        },
        bindTestSaveButton: function () {
            $("#btnTestSave").unbind("click");
            $("#btnTestSave").bind("click", function () {
                $("#updateBlocker2").show();
                viewModel.testSave();
            });
        },
        testSave: function () {
            var userId = $("#hdnUserId").val();
            var leadId = $("#hdnLeadId").val();
            var reqType = "Test";
            var reqId = viewModel.selectedRequirementId;

            var isOverride = $("#chTestOverridden").is(":checked");

            if (!utils.isValidDate("#dtTestCompleted")) {
                viewModel.showNotification("Date Completed have an invalid format.", 2);
                $("#updateBlocker2").hide();
                return -1;
            }

            var dateReceived = $("#dtTestCompleted").data("kendoDatePicker").value();
            if (isOverride === false && !dateReceived) {
                viewModel.showNotification("Date Completed is required", 2);
                $("#updateBlocker2").hide();
                return -1;
            }

            var overReason = $("#txtTestOverReason").val().trim();
            if (isOverride === false) {
                overReason = "";
                $("#txtTestOverReason").val("");
            } else {
                if (overReason.length === 0) {
                    viewModel.showNotification("Reason is required.", 2);
                    $("#updateBlocker2").hide();
                    return -1;
                }
            }

            var score = $("#txtTestScore").val().trim();
            var minScore = $("#txtMinScore").text().trim();;

            if (isOverride === false) {
                if (!score) {
                    viewModel.showNotification("Score is required", 2);
                    $("#updateBlocker2").hide();
                    return -1;
                }
            }

            var filter = { ReqId: reqId, LeadId: leadId, UserId: userId, RequirementType: reqType, IsOverridden: isOverride, overReason: overReason, DateReceived: dateReceived, Score: score, MinScore: minScore };
            var url = XMASTER_GET_BASE_URL + "/proxy/api/Leads/Requirements/PostRequirement";
            $.ajax({
                url: url,
                type: "POST",
                dataType: "text",
                data: JSON.stringify(filter),
                success: function () {

                    viewModel.showNotification("Requirement updated.", 1);
                    $("#updateBlocker2").hide();
                    $("#dtTestCompleted").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                    $("#txtTestOverReason").attr("disabled", "disabled");
                    $("#dtTestCompleted").data("kendoDatePicker").enable(true);
                    $("#txtTestScore").removeAttr("disabled").removeClass("rfdInputDisabled");
                    $("#txtTestOverReason").val("");
                    $("#chTestOverridden").prop("checked", false);

                    viewModel.initializeTestDetailsGrid(reqId, reqType);

                    viewModel.refreshGrids();
                },
                complete: function () {

                }
            });
        },
        bindReqSaveButton: function () {
            $("#btnEventSave").unbind("click");
            $("#btnEventSave").bind("click", function () {
                $("#updateBlocker2").show();
                viewModel.reqSave();
            });

        },
        reqSave: function () {
            var userId = $("#hdnUserId").val();
            var leadId = $("#hdnLeadId").val();
            var reqType = $("#reqName").text();
            var reqId = viewModel.selectedRequirementId;

            var isOverride = $("#chEventOverridden").is(":checked");

            if (!utils.isValidDate("#dtEventCompleted")) {
                viewModel.showNotification("Date Completed have an invalid format.", 2);
                $("#updateBlocker2").hide();
                return -1;
            }
            var dateReceived = $("#dtEventCompleted").data("kendoDatePicker").value();
            if (isOverride === false && !dateReceived) {
                viewModel.showNotification("Date Completed is required", 2);
                $("#updateBlocker2").hide();
                return -1;
            }

            var overReason = $("#txtEventReason").val().trim();
            if (isOverride === false) {
                overReason = "";
                $("#txtEventReason").val("");
            } else {
                if (overReason.length === 0) {
                    viewModel.showNotification("Reason is required.", 2);
                    $("#updateBlocker2").hide();
                    return -1;
                }
            }

            var filter = { ReqId: reqId, LeadId: leadId, UserId: userId, RequirementType: reqType, IsOverridden: isOverride, overReason: overReason, DateReceived: dateReceived };
            var url = XMASTER_GET_BASE_URL + "/proxy/api/Leads/Requirements/PostRequirement";
            $.ajax({
                url: url,
                type: "POST",
                dataType: "text",
                data: JSON.stringify(filter),
                success: function () {

                    viewModel.showNotification("Requirement updated.", 1);
                    $("#updateBlocker2").hide();
                    $("#dtEventCompleted").data("kendoDatePicker").value("");
                    $("#txtEventReason").val("");
                    $("#txtEventReason").attr("disabled", "disabled");
                    $("#chEventOverridden").prop("checked", false);
                    $("#dtEventCompleted").data("kendoDatePicker").enable(true);
                    $("#dtEventCompleted").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                    viewModel.initializeLeadEventsDetailsGrid(reqId, reqType);

                    viewModel.refreshGrids();
                },
                complete: function () {

                }
            });
        },
        bindTransSaveButton: function () {
            $("#btnTranSave").unbind("click");
            $("#btnTranSave").bind("click", function () {
                $("#updateBlocker2").show();
                viewModel.saveTransactionFee(false);
            });

        },

        bindTransSavePrintButton: function () {
            $("#btnTranSavePrint").unbind("click");
            $("#btnTranSavePrint").bind("click", function () {
                $("#updateBlocker2").show();
                viewModel.saveTransactionFee(true);
            });

        },
        saveTransactionFee: function (showReceipt) {
            var userId = $("#hdnUserId").val();
            var leadId = $("#hdnLeadId").val();
            var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            var reqType = "Fee";
            var reqId = viewModel.selectedRequirementId;
            var docReqId = viewModel.documentRequirementId;


            var isOverride = $("#chTranOverridden").is(":checked");
            var overReason = $("#txtTranReason").val().trim();
            if (isOverride !== true) {
                overReason = "";
                $("#txtTranReason").val("");
            } else {
                if (overReason.length === 0) {
                    viewModel.showNotification("Reason is required.", 2);
                    $("#updateBlocker2").hide();
                    return -1;
                }
            }

            var tranDescription = null;
            var tranReference = null;
            var transCodeId = null;
            var payTypeId = null;
            var tranPaymentReference = null;
            var transAmount = null;
            var transDate = null;
            var payTrId;
            var postTrId;
            if (!isOverride) {

                tranDescription = $("#txtTranDescription").val().trim();
                if (!tranDescription) {
                    viewModel.showNotification("Transaction Description required.", 2);
                    $("#updateBlocker2").hide();
                    return;
                }


                tranReference = $("#txtTranReference").val().trim();

                if (!utils.isValidDate("#dtTransDate")) {
                    viewModel.showNotification("Transaction date have an invalid format.", 2);
                    $("#updateBlocker2").hide();
                    return;
                }

                transDate = $("#dtTransDate").data("kendoDatePicker").value();
                if (!transDate) {
                    viewModel.showNotification("Transaction date is required.", 2);
                    $("#updateBlocker2").hide();
                    return;
                }

                transCodeId = $("#lstTransactionCodes").data("kendoDropDownList").value();
                if (!transCodeId || transCodeId === "0") {
                    viewModel.showNotification("Transaction Code is required.", 2);
                    $("#updateBlocker2").hide();
                    return;
                }

                payTypeId = $("#lstPaymentTypes").data("kendoDropDownList").value();
                if (!payTypeId || payTypeId === "0") {
                    viewModel.showNotification("Payment Type is required.", 2);
                    $("#updateBlocker2").hide();
                    return;
                }

                tranPaymentReference = $("#txtTranPaymentReference").val().trim();

                transAmount = $("#txtTranAmount").val().trim();

                if (transAmount.charAt(0) === "$")
                    transAmount = transAmount.substring(1);

                transAmount = transAmount.replace(/,/g, "");

                if (!transAmount) {
                    viewModel.showNotification("Transaction amount is required.", 2);
                    $("#updateBlocker2").hide();
                    return;
                }
                if (!$.isNumeric(transAmount)) {
                    viewModel.showNotification("Transaction amount is invalid.", 2);
                    $("#updateBlocker2").hide();
                    return;
                }
                postTrId = window.createGuid();
                payTrId = window.createGuid();
            }

            var filter = {
                DocReqId: docReqId, ReqId: reqId, LeadId: leadId, UserId: userId, RequirementType: reqType, IsOverridden: isOverride, overReason: overReason,
                TransDate: transDate, TransDescription: tranDescription, TransCodeId: transCodeId, PayTypeId: payTypeId, TransReference: tranReference,
                CampusId: campusId, PaymentReference: tranPaymentReference, TransAmount: transAmount, PostTransactionId: postTrId, PaymentTransactionId: payTrId
            };

            var url = XMASTER_GET_BASE_URL + "/proxy/api/Leads/Requirements/PostRequirement";
            $.ajax({
                url: url,
                type: "POST",
                dataType: "text",
                data: JSON.stringify(filter),
                success: function () {

                    viewModel.showNotification("Requirement updated.", 1);
                    $("#updateBlocker2").hide();
                    if (!filter.IsOverridden) {
                        $("#txtTranDescription").removeAttr("disabled").removeClass("rfdInputDisabled");
                        $("#txtTranReference").removeAttr("disabled").removeClass("rfdInputDisabled");
                        $("#lstTransactionCodes").data("kendoDropDownList").enable(true);
                        $("#lstPaymentTypes").data("kendoDropDownList").enable(true);

                        $("#dtTransDate").data("kendoDatePicker").enable(true);
                        $("#txtTranAmount").removeAttr("disabled").removeClass("rfdInputDisabled");
                    }

                    $("#txtTranAmount").val("$");
                    $("#txtTranReference, #txtTranDescription, #txtTranPaymentReference").val("");
                    $("#dtTransDate").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                    $("#dtApproval").data("kendoDatePicker").value("");
                    $("#lstTransactionCodes").data("kendoDropDownList").select(0);
                    $("#lstPaymentTypes").data("kendoDropDownList").select(0);
                    $("#tblPaymentReference").hide();

                    viewModel.initializeLeadTransDetailsGrid(reqId, reqType);

                    $("#postedTransactionId").val(payTrId);

                    if (!isOverride) {
                        $("#txtTranReason").val("");
                        $("#txtTranReason").attr("disabled", "disabled");
                    } else {
                        $("#txtTranReason").removeAttr("disabled").removeClass("rfdInputDisabled");
                    }


                    viewModel.refreshGrids();

                },
                complete: function () {
                    if (showReceipt === true) {
                        viewModel.openTransactionRcptFromButton(campusId, payTrId, leadId);
                    }
                }
            });
        },
        bindCheckBoxClick: function (type) {

            switch (type) {
            case "Document":
                $("#chkDocOverridden").unbind("click");
                $("#chkDocOverridden").bind("click", function () {
                    if ($("#chkDocOverridden").is(":checked")) {
                        $("#txtDocOverReason").removeAttr("disabled").removeClass("rfdInputDisabled");
                        $("#dtReceived").data("kendoDatePicker").enable(false);
                        $("#dtApproval").data("kendoDatePicker").enable(false);
                        $("#docStatus").data("kendoDropDownList").enable(false);

                        $("#dtReceived").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                        $("#dtApproval").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                        $("#docStatus").data("kendoDropDownList").select(0);

                    }
                    else {
                        $("#dtReceived").data("kendoDatePicker").enable(true);
                        $("#dtApproval").data("kendoDatePicker").enable(true);
                        $("#docStatus").data("kendoDropDownList").enable(true);
                        $("#txtDocOverReason").attr("disabled", "disabled");
                        $("#txtDocOverReason").val("");
                    }
                });
                break;
            case "Test":
                $("#chTestOverridden").unbind("click");
                $("#chTestOverridden").bind("click", function () {
                    if ($("#chTestOverridden").is(":checked")) {
                        $("#txtTestOverReason").removeAttr("disabled").removeClass("rfdInputDisabled");
                        $("#dtTestCompleted").data("kendoDatePicker").enable(false);
                        $("#txtTestScore").attr("disabled", "disabled");
                        $("#dtTestCompleted").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                        $("#txtTestScore").val("");
                    }
                    else {
                        $("#txtTestOverReason").attr("disabled", "disabled");
                        $("#dtTestCompleted").data("kendoDatePicker").enable(true);
                        $("#txtTestScore").removeAttr("disabled").removeClass("rfdInputDisabled");
                        $("#txtTestOverReason").val("");
                    }
                });
                break;
            case "ReqEvent":
                $("#chEventOverridden").unbind("click");
                $("#chEventOverridden").bind("click", function () {
                    if ($("#chEventOverridden").is(":checked")) {
                        $("#txtEventReason").removeAttr("disabled").removeClass("rfdInputDisabled");
                        $("#dtEventCompleted").data("kendoDatePicker").enable(false);
                        $("#dtEventCompleted").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                    }
                    else {
                        $("#txtEventReason").attr("disabled", "disabled");
                        $("#dtEventCompleted").data("kendoDatePicker").enable(true);
                        $("#txtEventReason").val("");
                    }
                });
                break;
            case "Fee":
                $("#chTranOverridden").unbind("click");
                $("#chTranOverridden").bind("click", function () {
                    if ($("#chTranOverridden").is(":checked")) {
                        $("#txtTranReason").removeAttr("disabled").removeClass("rfdInputDisabled");
                        $("#txtTranDescription").attr("disabled", "disabled");
                        $("#txtTranReference").attr("disabled", "disabled");
                        $("#lstTransactionCodes").data("kendoDropDownList").enable(false);
                        $("#lstPaymentTypes").data("kendoDropDownList").enable(false);
                        $("#dtTransDate").data("kendoDatePicker").enable(false);
                        $("#dtTransDate").data("kendoDatePicker").value("");
                        $("#txtTranAmount").attr("disabled", "disabled");
                        $("#btnTranSavePrint").attr("disabled", "disabled");
                        $("#txtTranDescription").val("");
                        $("#txtTranReference").val("");
                        $("#lstTransactionCodes").data("kendoDropDownList").select(0);
                        $("#lstPaymentTypes").data("kendoDropDownList").select(0);
                        $("#dtTransDate").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                        $("#txtTranAmount").val("$");
                        $("#txtTranPaymentReference").val("");
                    }
                    else {

                        $("#txtTranReason").attr("disabled", "disabled");
                        $("#txtTranReason").val("");
                        $("#txtTranDescription").removeAttr("disabled").removeClass("rfdInputDisabled");
                        $("#txtTranReference").removeAttr("disabled").removeClass("rfdInputDisabled");
                        $("#btnTranSavePrint").removeAttr("disabled").removeClass("rfdInputDisabled");
                        $("#lstTransactionCodes").data("kendoDropDownList").enable(true);
                        $("#lstPaymentTypes").data("kendoDropDownList").enable(true);
                        $("#dtTransDate").data("kendoDatePicker").value(kendo.toString(new Date(), "MM/dd/yyyy"));
                        $("#dtTransDate").data("kendoDatePicker").enable(true);
                        $("#txtTranAmount").removeAttr("disabled").removeClass("rfdInputDisabled");
                    }
                });
                break;

            }
        },
        bindDocCancel: function () {
            $("#btnDocCancel").unbind("click");
            $("#btnDocCancel").bind("click", function () {
                viewModel.cancelForm();
            });
        },
        bindTestCancel: function () {
            $("#btnTestCancel").unbind("click");
            $("#btnTestCancel").bind("click", function () {
                viewModel.cancelForm();
            });
        },
        bindEventCancel: function () {
            $("#btnEventCancel").unbind("click");
            $("#btnEventCancel").bind("click", function () {
                viewModel.cancelForm();
            });
        },
        bindTranCancel: function () {
            $("#btnTranCancel").unbind("click");
            $("#btnTranCancel").bind("click", function () {
                viewModel.cancelForm();
            });
        },
        cancelForm: function () {
            $("#two").hide();
            viewModel.refreshGrids();
            $("#updateContainer, .updateBlocker").hide();
            viewModel.isCancellingForm = true;
        },
        deleteDocumentFile: function () {
            deleteCounter++;
            if (deleteCounter > 1) return;
            var row = $(this).closest("tr");
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this document?"))
                .then(function (confirmed) {
                    if (confirmed) {

                        var dataItem = $("#docReqGrid").data("kendoGrid").dataItem(row);
                        if (dataItem) {

                            var id = dataItem.Id;
                            var reqType = "Document";
                            var filePath = dataItem.FilePath;
                            var filter = { Id: id, RequirementType: reqType, FilePath: filePath };
                            var url = XMASTER_GET_BASE_URL + "/proxy/api/Leads/Requirements/DeleteRequirementHistoryRecord";
                            $.ajax({
                                url: url,
                                type: "POST",
                                dataType: "text",
                                data: JSON.stringify(filter),
                                success: function () {
                                    viewModel.showNotification("Document record deleted.", 1);
                                    viewModel.initializeDocumentDetailsGrid(viewModel.selectedRequirementId, "Document");
                                    deleteCounter = 0;
                                    viewModel.refreshGrids();
                                },
                                complete: function () {
                                    deleteCounter = 0;
                                }
                            });
                        } else {
                            deleteCounter = 0;
                        }
                    } else {
                        deleteCounter = 0;
                    }
                });
        },
        deleteTestRecord: function () {
            deleteCounter++;
            if (deleteCounter > 1) return;
            var row = $(this).closest("tr");
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this test?"))
                .then(function (confirmed) {
                    if (confirmed) {

                        var dataItem = $("#testReqGrid").data("kendoGrid").dataItem(row);
                        if (dataItem) {

                            var id = dataItem.Id;
                            var reqType = "Test";
                            var filter = { Id: id, RequirementType: reqType };
                            var url = XMASTER_GET_BASE_URL + "/proxy/api/Leads/Requirements/DeleteRequirementHistoryRecord";
                            $.ajax({
                                url: url,
                                type: "POST",
                                dataType: "text",
                                data: JSON.stringify(filter),
                                success: function () {
                                    viewModel.showNotification("Item has been deleted.", 1);
                                    viewModel.initializeTestDetailsGrid(viewModel.selectedRequirementId, reqType);
                                    deleteCounter = 0;
                                    viewModel.refreshGrids();
                                },
                                complete: function () {
                                    deleteCounter = 0;
                                }
                            });
                        } else {
                            deleteCounter = 0;
                        }
                    } else {
                        deleteCounter = 0;
                    }
                });
        },
        deleteEventRecord: function () {
            deleteCounter++;
            if (deleteCounter > 1) return;
            var row = $(this).closest("tr");
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this record?"))
                .then(function (confirmed) {
                    if (confirmed) {
                        var dataItem = $("#reqDetailGrid").data("kendoGrid").dataItem(row);
                        if (dataItem) {

                            var id = dataItem.Id;
                            var reqType = $("#reqName").text();


                            var filter = { Id: id, RequirementType: reqType };
                            var url = XMASTER_GET_BASE_URL + "/proxy/api/Leads/Requirements/DeleteRequirementHistoryRecord";
                            $.ajax({
                                url: url,
                                type: "POST",
                                dataType: "text",
                                data: JSON.stringify(filter),
                                success: function () {

                                    viewModel.showNotification("Item has been deleted.", 1);
                                    viewModel.initializeLeadEventsDetailsGrid(viewModel.selectedRequirementId, reqType);
                                    viewModel.refreshGrids();
                                    deleteCounter = 0;
                                },

                                complete: function () {
                                    deleteCounter = 0;
                                }
                            });
                        } else {
                            deleteCounter = 0;
                        }
                    } else {
                        deleteCounter = 0;
                    }
                });

        },
        voidTransaction: function () {
            deleteCounter++;
            if (deleteCounter > 1) return;

            var row = $(this).closest("tr");
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to void transaction?"))
                .then(function (confirmed) {
                    if (confirmed) {

                        var dataItem = $("#tranReqGrid").data("kendoGrid").dataItem(row);
                        if (dataItem) {

                            var id = dataItem.Id;
                            var userId = $("#hdnUserId").val();

                            var filter = { TransactionId: id, UserId: userId };

                            var url = XMASTER_GET_BASE_URL + "/proxy/api/Leads/Requirements/VoidTransaction";
                            $.ajax({
                                url: url,
                                type: "POST",
                                dataType: "text",
                                data: JSON.stringify(filter),
                                success: function () {
                                    viewModel.showNotification("Transaction has been voided.", 1);
                                    viewModel.initializeLeadTransDetailsGrid(viewModel.selectedRequirementId, "Fee");
                                    deleteCounter = 0;
                                    viewModel.refreshGrids();
                                },

                                complete: function () {
                                    deleteCounter = 0;
                                }
                            });
                        } else {
                            deleteCounter = 0;
                        }
                    } else {
                        deleteCounter = 0;
                    }
                });

        },

        haveMetRequirements: function () {
            $.ajax({
                url: XMASTER_GET_BASE_URL + "/proxy/api/Leads/Requirements/GetHaveMetRequirement" + "?leadId=" + $("#hdnLeadId").val() + "&campusId=" + XMASTER_GET_CURRENT_CAMPUS_ID,
                type: "GET",
                timeout: 20000,
                dataType: "json",
                asyn: true,
                success: function (data) {
                    var leadInfoBar = new AD.LeadInfoBar();
                    if (data === "True" || data === "enrolled") {
                        $("#leadRequirementStatus").text("Requirements Completed");
                        $("#leadRequirementStatus").removeClass("red");
                        $("#leadRequirementStatus").addClass("green-completed-requirements");
                        if (data === "True") {
                            leadInfoBar.refreshHaveMetRequirements();
                        }
                    } else {
                        $("#leadRequirementStatus").text("Pending Requirements");
                        $("#leadRequirementStatus").removeClass("green-completed-requirements");
                        $("#leadRequirementStatus").addClass("red");
                        leadInfoBar.refreshHaveMetRequirements();
                    }
                }
            });
        },

        openTransactionRcptFromButton: function (campusId, payTrId, leadId) {
            viewModel.openTransactionReceipt(campusId, payTrId, leadId);
        },
        openTransactionRcptFromLink: function () {
            var campusId = XMASTER_GET_CURRENT_CAMPUS_ID;

            var row = $(this).closest("tr");
            var dataItem = $("#tranReqGrid").data("kendoGrid").dataItem(row);
            var payTrId = dataItem.Id;
            var leadId = $("#hdnLeadId").val();
            console.log(dataItem);

            viewModel.openTransactionReceipt(campusId, payTrId, leadId);
        },
        openTransactionReceipt: function (campusId, payTrId, leadId) {
            window.open("LeadReqPrintWindow.aspx?transaction=" + payTrId + "&campus=" + campusId + "&lead=" + leadId, "popupWindow", "width=950,height=500");
        },
        initFileUpload: function () {

            var reqId = viewModel.selectedRequirementId;
            var leadId = $("#hdnLeadId").val();
            var userId = $("#hdnUserId").val();

            var reqParameter = leadId + "_" + reqId + "_" + userId;

            var fileUpload = $("#files").data("kendoUpload");
            if (fileUpload === undefined || fileUpload === null) {
                $("#files")
                    .kendoUpload({
                        async: {
                            saveUrl: XMASTER_GET_BASE_URL +
                                "/proxy/api/Leads/" +
                                "Requirements/PostDocumentFile?requirement=" + reqParameter,
                            autoUpload: true
                        },
                        success: viewModel.onSuccess,
                        error: viewModel.onError,
                        showFileList: false,
                        multiple: false,
                        select: function (e) {
                            $.each(e.files, function (index, value) {
                                var ext = value.extension.toLocaleLowerCase();
                                if (ext === ".txt" ||
                                    ext === ".jpeg" ||
                                    ext === ".jpg" ||
                                    ext === ".png" ||
                                    ext === ".doc" ||
                                    ext === ".docx" ||
                                    ext === ".pdf" ||
                                    ext === ".gif") {

                                   
                                    var limit = 34000000;
                                    if (value.size > limit) {
                                        
                                        e.preventDefault();
                                        viewModel.showNotification("Error uploading document. Documents cannot exceed 32MB (32 megabytes).", 3);
                                    }
                                } else {
                                    e.preventDefault();
                                    viewModel.showNotification("Invalid file type. Only jpeg, jpg, png, doc, docx, txt, pdf and gif file types can be uploaded.", 2);
                                }
                            });
                        }
                    });
            } else {
                $("#files").data("kendoUpload").options.async.saveUrl = XMASTER_GET_BASE_URL +
                    "/proxy/api/Leads/" +
                    "Requirements/PostDocumentFile?requirement=" +
                    reqParameter;
                $("#fileUploadtr .k-upload-status").remove();
            }

        },
        onSuccess: function (e) {
            viewModel.showNotification("File was uploaded successfully.", 1);
            viewModel.initializeDocumentDetailsGrid(viewModel.selectedRequirementId, "Document");
        },
        onError: function (e) {
           
            viewModel.showNotification("An error occurred attempting to upload your file", 3);
        },
        //-------------------------------------------------------------------------------------------------------
        // notification functions
        //-------------------------------------------------------------------------------------------------------
        showNotification: function (message, type) {
            if (type === 1) {
                MasterPage.SHOW_INFO_WINDOW_PROMISE(message);
            }
            else if (type === 2) {
                MasterPage.SHOW_WARNING_WINDOW(message);
            }
            else if (type === 3) {
                MasterPage.SHOW_ERROR_WINDOW_PROMISE(message);
            } else {
                MasterPage.SHOW_INFO_WINDOW_PROMISE(message);
            }
        },

        PositionAtCenter: function (e) {
            if (!$("." + e.sender._guid)[1]) {
                var element = e.element.parent(),
                    eWidth = element.width(),
                    eHeight = element.height(),
                    wWidth = $(window).width(),
                    wHeight = $(window).height(),
                    newTop, newLeft;

                newLeft = Math.floor(wWidth / 2 - eWidth / 2);
                newTop = Math.floor(wHeight / 2 - eHeight / 2);

                e.element.parent().css({ top: newTop, left: newLeft });
            }
        },
        getUserFullName: function () {
            var leadId = $("#hdnLeadId").val();
            var filter = { LeadGuid: leadId };
            var url = XMASTER_GET_BASE_URL + "/proxy/api/LeadSearch/GetById";
            var requestData3 = $.getJSON(url, filter, function (data) {
                $("#userFullName").text(data.FirstMiddleLastName);
            });
        },
        openFileBrowser: function (e) {
            var row = $(this).closest("tr");
            var dataItem = $("#docReqGrid").data("kendoGrid").dataItem(row);
            var filePath = dataItem.FilePath;
            if (filePath)
                window.open(XMASTER_GET_BASE_URL + "/filebrowser.aspx?fileurl=" + encodeURI(filePath), "FileBrowser", "height=600,width=600");
        },
        bindCurrencyFormatting: function () {
            utils.setCurrencyMaskedTextBox("#txtTranAmount");
        },
        serialize: function (el) {
            var serialized = $(el).serialize();
            if (!serialized) // not a form
                serialized = $(el).
                    find("input[name],select[name],textarea[name]").serialize();
            return serialized;
        }
    });

    $(function () {
        viewModel.bindWindowHide();
        viewModel.initializeGrid();
        viewModel.initializeGrpGrid();
        viewModel.initializeOptionalGrid();
        viewModel.getUserFullName();
        $(".updateBlocker").hide();
        viewModel.isCancellingForm = false;
        viewModel.activeFormData = viewModel.serialize("#two");
    });
})(window.$, window.kendo, window.datasources, window.kendoControlSetup);
