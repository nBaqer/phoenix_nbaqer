﻿(function ($, kendo, datasources) {

    var deleteCounter = 0;
    


    //----------------------------------------------------------------------------------------
    // View Model that represents the data to be bound to the 
    // view
    //----------------------------------------------------------------------------------------
    var viewModel = kendo.observable({

        requirementsDataSource: datasources.requirementsDataSource,
        reqGroupDataSource: datasources.requirementGrpDataSource,
        reqByGroupDataSource: datasources.requirementByGrpDataSource,

        initializeGrid: function () {

            viewModel.requirementsDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            viewModel.requirementsDataSource.transport.options.read.data.IsMandatory = true;
            viewModel.requirementsDataSource.transport.options.read.data.CampusId = $("#hdnCampusId").val();
            viewModel.requirementsDataSource.transport.options.read.data.LeadId = $("#hdnLeadId").val();
            viewModel.requirementsDataSource.transport.options.read.data.UserId = $("#hdnUserId").val();

            //setup kendo grid for log
            $("#mandReqsGrid").kendoGrid({
                dataSource: viewModel.requirementsDataSource,

                dataBound: function () {
                    viewModel.initializeGrpGrid();
                },
                sortable: false,
                selectable: false,
                scrollable: true,
                width:480,
                resizable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                { field: "Id", title: "Id", hidden: true },
                { field: "IsApproved", title: "Met", width: 30, template: "# if (IsApproved == true) { # <image src='" + XMASTER_GET_BASE_URL + "/images/icon/action_check.png' />" + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;cursor:pointer;" } },
                { field: "Description", title: "Name", width: 140, template: "# if (DateReceived){ # <span class='descriptionClick'>#=Description #</span> #} else { # <span >#=Description #</span> # }#" },
                { field: "DisplayType", title: "Type", width: 60 },
                { field: "DateReceived", title: "Received", template: "#= kendo.toString(DateReceived,'MM/dd/yyyy') #", width: 63 },
                { field: "ApprovalDate", title: "Approved", template: "#= kendo.toString(ApprovalDate,'MM/dd/yyyy') #", width: 63 },
                { field: "IsOverridden", title: "Override", width: 50, template: "# if (IsOverridden == true) { # <image src='" + XMASTER_GET_BASE_URL + "/images/icon/action_check_dark.png' />" + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                { field: "ApprovedBy", title: "User", width: 65 }]
               
            });

            var grid = $('#mandReqsGrid').data('kendoGrid');
            

        },
        initializeGrpGrid: function () {

            viewModel.reqGroupDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            viewModel.reqGroupDataSource.transport.options.read.data.IsMandatory = true;
            viewModel.reqGroupDataSource.transport.options.read.data.CampusId = $("#hdnCampusId").val(); 

            viewModel.reqGroupDataSource.transport.options.read.data.leadId = $("#hdnLeadId").val();

            //setup kendo grid for log
            $("#mandReqGrpGrid").kendoGrid({
                dataSource: viewModel.reqGroupDataSource,
                dataBound: function () {
                },
                sortable: false,
                detailInit: viewModel.detailInit,
                selectable: false,
                scrollable: true,
                resizable: true,
                width: 480,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                { field: "Id", title: "Id", hidden: true },
                { field: "IsApproved", title: "Met", width: 30, template: "# if (IsApproved == true) { # <image src='" + XMASTER_GET_BASE_URL + "/images/icon/action_check.png' />" + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                { field: "Description", title: "Name", width: 135 },
                { field: "DisplayType", title: "Type", width: 50 },
                { field: "DateReceived", title: "Received", template: "#= kendo.toString(DateReceived,'MM/dd/yyyy') #", width: 63 },
                { field: "ApprovalDate", title: "Approved", template: "#= kendo.toString(ApprovalDate,'MM/dd/yyyy') #", width: 63 },
                { field: "IsOverridden", title: "Override", width: 50, template: "# if (IsOverridden == true) { # <image src='" + XMASTER_GET_BASE_URL + "/images/icon/action_check_dark.png' />" + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                { field: "ApprovedBy", title: "User", width: 65 }]
            });


        },
        detailInit: function (e) {


            viewModel.reqByGroupDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            viewModel.reqByGroupDataSource.transport.options.read.data.IsMandatory = true;
            viewModel.reqByGroupDataSource.transport.options.read.data.CampusId = $("#hdnCampusId").val();

            viewModel.reqByGroupDataSource.transport.options.read.data.grpId = e.data.Id;
            viewModel.reqByGroupDataSource.transport.options.read.data.leadId = $("#hdnLeadId").val();

            $("<div/>").appendTo(e.detailCell).kendoGrid({
                dataSource: viewModel.reqByGroupDataSource,
                scrollable: false,
                sortable: true,
                width: 480,
                pageable: false,
                dataBound: function () {

                },
                columns: [{ field: "Id", title: "Id", hidden: true },
                { field: "IsApproved", title: "Met", width: 35, template: "# if (IsApproved == true) { # <image src='" + XMASTER_GET_BASE_URL + "/images/icon/action_check.png' />" + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                { field: "Description", title: "Name", width: 115, template: "# if (DateReceived){ # <span class='descriptionClick'>#=Description #</span> #} else { # <span >#=Description #</span> # }#" },
                { field: "DisplayType", title: "Type", width: 65 },
                { field: "DateReceived", title: "Received", template: "#= kendo.toString(DateReceived,'MM/dd/yyyy') #", width: 65 },
                { field: "ApprovalDate", title: "Approved", template: "#= kendo.toString(ApprovalDate,'MM/dd/yyyy') #", width: 65 },
                { field: "IsOverridden", title: "Override", width: 55, template: "# if (IsOverridden == true) { # <image src='" + XMASTER_GET_BASE_URL + "/images/icon/action_check_dark.png' />" + "# } else { #" + "" + "# }#", attributes: { style: "text-align:center;" } },
                { field: "ApprovedBy", title: "User", width: 65 },
                { field: "OverReason", title: "", width: 90, hidden: true },
                { field: "MinScore", title: "", width: 90, hidden: true },
                { field: "Score", title: "", width: 90, hidden: true },
                { field: "DocumentRequirementId", title: "", width: 90, hidden: true }]
                


            });
        },
        
        getUserFullName: function () {
            var leadId = $("#hdnLeadId").val();
            var filter = { LeadGuid: leadId };
            var url = XMASTER_GET_BASE_URL + '/proxy/api/LeadSearch/GetById';
            var requestData3 = $.getJSON(url, filter, function (data) {
                $("#userFullName").text(data.FirstMiddleLastName);
            });
        }
    });

    $(function () {

       
        viewModel.initializeGrid();
        viewModel.getUserFullName();
    });


})(window.$, window.kendo, window.datasources, window.kendoControlSetup);


