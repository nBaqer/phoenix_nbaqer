﻿



(function (jQuery) {
    
    
    var isEntityResource = function(resId) {
        var entityResources = [
            "203", "90", "92", "95", "97", "112", "114", "116", "155", "159", "169", "175", "177", "200", "213",
            "230", "286", "301", "303", "327", "372", "374", "380", "479", "530", "531", "532", "534", "535",
            "538", "539", "541", "542", "543", "614", "625", "628", "652", "113", "334", "170", "793", "145", "146",
            "147", "148", "225", "313", "456", "484", "79", "82", "86", "111", "52", "55", "69", "281","826", "831","268","174", "838", "868"
        ];

        if (!resId) return false;

        if ($.inArray(resId, entityResources) != -1)
            return true;
        else
            return false;
    };


    

    
    window.modelCommon = window.modelCommon || {};
    window.modelCommon.isEntityResource = isEntityResource;
    
})(window.jQuery);