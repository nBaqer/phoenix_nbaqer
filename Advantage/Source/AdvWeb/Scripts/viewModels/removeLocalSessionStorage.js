﻿(function ($, kendo, datasources) {

    //----------------------------------------------------------------------------------------
    // View Model that represents the data to be bound to the 
    // view
    //----------------------------------------------------------------------------------------
    var viewModel = kendo.observable({

        bindTabStrip: function () {
            $("#tabstrip").kendoTabStrip({
                animation: {
                    // fade-out current tab over 1000 milliseconds
                    close: {
                        duration: 200,
                        effects: "fadeOut"
                    },
                    // fade-in new tab over 500 milliseconds
                    open: {
                        duration: 200,
                        effects: "fadeIn"
                    }
                }


            });
        },
        
        bindClearCache: function () {
            $("#btnClear").bind("click", function () {
                
                localStorage.clear();
                sessionStorage.clear();

                viewModel.showNotification("Local and Session storage has been removed.", "center");

                var c = sessionStorage.length + localStorage.length;
                $("#totalCount").html("Total Cache Items: " + c);

            });

            var cnt = sessionStorage.length + localStorage.length;
            $("#totalCount").html("Total Cache Items: " + cnt);
        },
        bindRefresh: function () {
            $("#btnRefresh").bind("click", function () {
                
                
                var c = sessionStorage.length + localStorage.length;
                $("#totalCount").html("Total Cache Items: " + c);

            });

            var cnt = sessionStorage.length + localStorage.length;
            $("#totalCount").html("Total Cache Items: " + cnt);
        },
        //displayCache: function() {

        //    var tableContent = '';



        //    if (localStorage.length > 0) {
        //        tableContent = "<h3>Local Storage</h3><table border=0>";
        //        $.each(localStorage, function(key, value) {
        //            tableContent += "<tr><td>" + key + "</td></tr>";
        //        });

        //        tableContent += "</table>";
        //    } else {
        //        tableContent += "<h3>No Local Storage Found.<br></h3>";
        //    }

        //    if (sessionStorage.length > 0) {
        //        tableContent += "<h3>Session Storage</h3><table border=0>";
        //        $.each(sessionStorage, function(key, value) {
        //            tableContent += "<tr><td>" + key + "</td></tr>";
        //        });

        //        tableContent += "</table>";
        //    } else {
        //        tableContent += "<h3>No Session Storage Found.<br></h3>";
        //    }


        //    $("#storageContent").html(tableContent);

        //},
        showNotification: function (message, placement) {


            var notificationWidget;

            if (placement == "center") {
                notificationWidget = $("#kNotification").kendoNotification({
                    stacking: "down",
                    show: viewModel.PositionAtCenter,
                    autoHideAfter: 3000,
                    modal: false,
                    templates: [{ type: "info", template: $("#infoTemplate").html() }]

                }).data("kendoNotification");
            } else {

                notificationWidget = $("#kNotification").kendoNotification({
                    stacking: "down",
                    position: { bottom: 30, right: 50 },
                    autoHideAfter: 3000,
                    modal: false,
                    templates: [{ type: "info", template: $("#infoTemplate").html() }]

                }).data("kendoNotification");
            }


            notificationWidget.show({ title: "&nbsp;&nbsp;&nbsp;&nbsp;Cache removed", message: message, src: XMASTER_GET_BASE_URL + "/images/info.jpg" }, "info");

        },

        PositionAtCenter: function (e) {

            if (!$("." + e.sender._guid)[1]) {
                var element = e.element.parent(),
                    eWidth = element.width(),
                    eHeight = element.height(),
                    wWidth = $(window).width(),
                    wHeight = $(window).height(),
                    newTop, newLeft;

                newLeft = Math.floor(wWidth / 2 - eWidth / 2);
                newTop = Math.floor(wHeight / 2 - eHeight / 2);

                e.element.parent().css({ top: newTop, left: newLeft, zIndex: 22222 });
            }
        },
    });

    $(function () {

     
        viewModel.bindTabStrip();
        viewModel.bindClearCache();
        viewModel.bindRefresh();
        


    });


})(window.$, window.kendo, window.datasources, window.kendoControlSetup);