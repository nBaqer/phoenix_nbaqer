﻿//---------------------------------------------------------------------------
// setup ajax setting for subsequent calls
//---------------------------------------------------------------------------
var ajaxSetup = function () {

    jQuery.ajaxSetup({
        timeout: 25000, //25 seconds,
        dataType: 'json',
        contentType: 'application/json'

    });

    jQuery(document)
        //.ajaxStart(function () {
        //    jQuery('#loading').show();
        //}).ajaxComplete(function () {
        //    jQuery('#loading').hide();})
        //.ajaxSuccess(function () {
        //    alert("Request was successful. Check HTTP traffic.")})
        .ajaxError(function (event, jqXHR) {
            if (jqXHR.statusText === "canceled") return;

            switch (jqXHR.status) {
                case 401:
                    alert("Your request resulted in an Unauthorized response. Check your Api Authentication Key");
                    break;
                default:
                    alert("Error in making request. Check HTTP traffic.");
                    break;
            }
        });
};

//retrieve query string params
(function ($) {
    $.QueryString = (function (a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i) {
            var p = a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'))
})(jQuery);


$(document).ready(function () {
    ajaxSetup();
    

    var viewModel = kendo.observable({
        autoCompleteValue: null,
        students: null,
        displayAutoCompleteValue: function () {
            var autoCompleteValue = this.get("autoCompleteValue");
            return kendo.stringify(autoCompleteValue);
        },
        comboBoxValue: null,
        displayComboBoxValue: function () {
            var comboBoxValue = this.get("comboBoxValue");
            return kendo.stringify(comboBoxValue);
        },
        dropDownListValue: null,
        displayDropDownListValue: function () {
            var dropDownListValue = this.get("dropDownListValue");
            return kendo.stringify(dropDownListValue);
        }


    });


   
    var mappedStudents;

    $(function () {

        //load students dropdown
        var campusId = $.QueryString["cmpid"];
        var filter = { CampusId: campusId };

        $.getJSON('../proxy/api/Student/' + vm.StudentIdParam, null,
         function (response) {

             mappedStudents = jQuery.map(response, function (item) {
                 return { StudentId: item.Id, StudentName: "[" + item.StudentNumber + "]" + item.FirstName + " " + item.LastName };
    
                 viewModel.set("students", mappedStudents);
                  
             });
         });
       

    });      
              
    

    //viewModel.autoCompleteValue = viewModel.colors[0];
    //viewModel.dropDownListValue = viewModel.colors[0];
    //viewModel.comboBoxValue = viewModel.colors[0];

    
    
    

    kendo.bind($("table"), viewModel);
});