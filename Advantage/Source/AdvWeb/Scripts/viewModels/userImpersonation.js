﻿(function ($, kendo, datasources) {

    //----------------------------------------------------------------------------------------
    // View Model that represents the data to be bound to the 
    // view
    //----------------------------------------------------------------------------------------
    var viewModel = kendo.observable({
        //----------------------------------------------
        //Obsvervable objects
        //----------------------------------------------
        users: datasources.usersDataSource,
        selectedUsers: [],
        userImpersonationLog: datasources.userImpersonationLogDataSource,
        startDate: '',
        endDate: '',
        //----------------------------------------------
        // View Model Methods
        //----------------------------------------------
        impersonateUser: function (e) {
            
            if (e.preventDefault) e.preventDefault(); else e.returnValue = false;

            var dropdownlist = $("#userDropDownList").data("kendoDropDownList");
            var dataItem = dropdownlist.dataItem();

            if (dataItem.Id) {
                if (confirm("Are you sure you want to impersonate " + dataItem.UserAndName + "?")) {
                    $("#hdnImpIsImpersonatingLocal").val(true);
                    $("#hdnImpUserId").val(dataItem.Id);
                    $("#hdnImpUserName").val(dataItem.UserName);
                    $("#hdnImpUserAndName").val(dataItem.UserAndName);
                    sessionStorage.clear();
                    this.initNewLog(dataItem.UserName);
                } else {
                    if (event.preventDefault) event.preventDefault();
                    else event.returnValue = false;
                }
            }
            
            return false;
        },
        resetImpersonation: function (e) {

            if (e.preventDefault) e.preventDefault(); else e.returnValue = false;

            if (confirm("Reset user back to support?")) {
                $("#hdnImpIsImpersonatingLocal").val(false);
                $("#hdnImpUserId").val("");
                $("#hdnImpUserName").val("");
                $("#hdnImpUserAndName").val("");
                this.updateLogEndDate();
                sessionStorage.clear();
            }
            return false;
        },

        searchLog: function (e) {

            if (e.preventDefault) e.preventDefault(); else e.returnValue = false;

            this.userImpersonationLog.transport.options.read.data.StartDate = $('#startDatePicker').val();
            this.userImpersonationLog.transport.options.read.data.EndDate = $('#endDatePicker').val();
            this.userImpersonationLog.read();
        },
        initNewLog: function( userName ) {
            var filter = { ImpersonatedUser: userName };
            var url = '../proxy/api/SystemStuff/UserImpersonationLogs/NewLog';
            $.ajax({
                url: url,
                type: 'POST',
                data: JSON.stringify(filter),
                success: function (data, textStatus) {
                    $("#hdnUserImpersonationLogId").val(data.Id);
                    __doPostBack('<%= hiddenButton.ClientID  %>', 'OnClick');
                },
            });
        },
        updateLogEndDate: function() {
            var logId = $("#hdnUserImpersonationLogId").val();
            var url = '../proxy/api/SystemStuff/' + logId + '/UserImpersonationLogs';
            $.ajax({
                url: url,
                type: 'PUT',
                success: function () {
                    $("#hdnUserImpersonationLogId").val("");
                    __doPostBack('<%= hiddenButton.ClientID  %>', 'OnClick');
                }
            });
        },
        click: function (e) {

            if (e.preventDefault) e.preventDefault(); else e.returnValue = false;
        }
    });

    $(function() {

        var baseUrl = XMASTER_GET_BASE_URL;
        var currentCampusId = XMASTER_GET_CURRENT_CAMPUS_ID;

        //setup ajax for the page
        common.ajaxSetup(viewModel);

        //setup dropdown
        var setUserDropDownList = function( isEnabled ) {
            jQuery('#userDropDownList').kendoDropDownList({
                dataTextField: 'UserAndName',
                dataValueField: 'Id',
                enabled: isEnabled,
                optionLabel: {
                    UserAndName: "------------Select User Name------------",
                    id: null
                },
                UserAndName: "------------Select User Name------------",
            });

        };

        function checkDates() {
            if (startDate.val() != '' && endDate.val() != '') {
                if (Date.parse(startDate.val()) > Date.parse(endDate.val())) {
                    alert('End date should be after start date');
                    endDate.val(startDate.val());
                }
            }
        }

        var d = new Date();
        d = window.formattedDate(d, true);

        var fixedD = kendo.parseDate(d, "MM/dd/yyyy");

        //setup datepickers
        var startDate = jQuery('#startDatePicker').kendoDatePicker({
            change: checkDates,
            value: new Date(),
            format: "MM/dd/yyyy"
        });

        var endDate = jQuery('#endDatePicker' ).kendoDatePicker({
            change: checkDates,
            value: new Date(),
            format: "MM/dd/yyyy"
        });

        $("#startDate").kendoValidator({
            rules: {
                date: function (input) {
                    var d = kendo.parseDate(input.val());
                    return d instanceof Date;
                }
            }
        });

        $("#endDate").kendoValidator({
            rules: {
                date: function (input) {
                    var d = kendo.parseDate(input.val());
                    return d instanceof Date;
                }
            }
        });


        
        //setup kendo grid for log
        jQuery('#userImpersonationGrid').kendoGrid({
            groupable: false,
            scrollable: true,
            resizable: true,
            filterable: false,
            autoBind: false,
            height:400,
            columns: [
                { field: 'ImpersonatedUser', title: 'Impersonated User', width: 150 },
                { field: 'LStart', title: 'Impersonation Started ', width: 150 },
                { field: 'LEnd', title: 'Impersonation Ended', width: 150, }
            ],
            dataBound: function () {
                //Get the number of Columns in the grid
                var colCount = $("#kGrid").find('.k-grid-header colgroup > col').length;

                //If There are no results place an indicator row
                if (viewModel.userImpersonationLog._view.length == 0) {
                    $("#userImpersonationGrid").find('.k-grid-content tbody')
                        .append('<tr class="kendo-data-row"><td colspan="' +
                            colCount +
                            '" style="text-align:left"><b>There are no records for the dates selected.</b></td></tr>');
                }
            }
        
        });

        

        //method to set up visibility of controls
        var setControls = function() {

            var isImpersonating = $("#hdnImpIsImpersonatingLocal").val();
            if (isImpersonating) {
                if (isImpersonating.toUpperCase() == "TRUE") {
                    setUserDropDownList(false);
                    $("#sImp").prop('disabled', true);

                } else {
                    setUserDropDownList(true);
                    $("#sImp").prop('disabled', false);
                    $("#resetImp").prop('disabled', true);
                }
            } else {
                setUserDropDownList(true);
                $("#sImp").prop('disabled', false);
                $("#resetImp").prop('disabled', true);
            }
        };


        setControls();
        setUserDropDownList(true);

        //set up tabstrop and tooltip
        window.kendoControlSetup.setupTabStrip("#tabstrip");
        window.kendoControlSetup.setupToolTip("#submitImp", "Please remember return to this page and click the <b>Reset to Support</b> button<br> before logging out of the application", "top", 200);

        
        
        
        //get support user id
        var uri = baseUrl + 'proxy/api/User/Users';
        var filter = { ReturnSupport: 'true' };
        var requestData = $.getJSON(uri, filter, function (data) {
            $("#hdnNonImpersonatingUserId").val(data[0].Id);
        });

        ////bind the view to the view model
        kendo.bind(jQuery("#frame"), viewModel);
    });


})(window.$, window.kendo, window.datasources, window.kendoControlSetup);