﻿/// <reference path="../../common-util.js" />

var XGET_STATUS_CHANGE_MODEL = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/PostGetStatusHistory/";
var XGET_REQUESTED_BY_MODEL = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/GetRequestedBy/";
var XGET_LOA_REASONS = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/GetLoaReasons/";
var XGET_DROPPED_REASONS = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/GetDroppedReasons/";
var XGET_DELETE_REASONS = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/GetDeleteReasons/";
var XGET_STATUS_CODES_MODEL = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/GetStatusCodesList/";
var XPOST_VALIDATE_INSERT = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/GetValidateStatusChange/";
var XPOST_VALIDATE_ATTENDANCE = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/GetValidateAttendanceAndGrades/";
var XPOST_INSERT = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/PostApplyStatusChange/";
var XPOST_VALIDATE_DELETE = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/GetValidateDelete/";
var XPOST_VALIDATE_DELETE_SEQUENCE = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/GetValidateDeleteSequence/";
var XPOST_DELETE = XMASTER_GET_BASE_URL + "/proxy/api/SystemStuff/SystemStatusChange/PostDelete/";

var VALIDATION_MODE_INSERT = 1;
var VALIDATION_MODE_EDIT = 2;
var VALIDATION_MODE_DELETE = 3;

function ESystemStatus() {
    this.NoHistory = 0;
    this.FutureStart = 7;
    this.NoStart = 8;
    this.CurrentlyAttending = 9;
    this.LeaveOfAbsence = 10;
    this.Suspension = 11;
    this.Dropped = 12;
    this.Graduated = 14;
    this.TransferOut = 19;
    this.AcademicProbationSap = 20;
    this.Externship = 22;
    this.DisciplinaryProbation = 23;
    this.WarningProbation = 24;
}

function StatusChangeHistory(studentEnrollmentId, campusId, userId) {
    this.statusChangeHistoryDataSource = undefined;
    this.statusChangeModel = undefined;
    this.requestedByList = undefined;
    this.deleteReasonList = undefined;
    this.studentEnrollmentId = studentEnrollmentId;
    this.campusId = campusId;
    this.userId = userId;
    this.statusChangeToList = undefined;
    this.loaReasons = undefined;
    this.droppedReasons = undefined;
    this.ValidationMode = undefined;
    this.ElementId = undefined;
    this.btnUpdateSearchControlId = undefined;
    this.shouldConfirmDeleteAttendance = false;

    var _this = this;

    this.BindGrid = function () {
        this.shouldConfirmDeleteAttendance = false;
        var model = {
            StudentEnrollmentId: _this.studentEnrollmentId,
            CampusId: campusId,
            UserId: userId
        }
        _this.ShowLoading(true);
        $("#kendoStatusHistoryGrid").kendoGrid({
            dataBound: function () {
                _this.ShowLoading(false);
            },
            sortable: false,
            selectable: false,
            scrollable: true,
            resizable: true,
            pageable: false,
            noRecords: true,
            columns: [
                { field: "StatusCodeId", title: "Id", hidden: true },
                { field: "EffectiveDate", title: "Effective Date", width: 80, attributes: { style: "text-align:left;" }, template: "#=kendo.toString(data.EffectiveDate, 'MM/dd/yyyy')#" },
                { field: "Status", title: "Status", width: 120, attributes: { style: "text-align:left;" } },
                { field: "Reason", title: "Reason", width: 200, attributes: { style: "text-align:left;" } },
                { field: "RequestedBy", title: "Requested By", width: 100, attributes: { style: "text-align:left;" } },
                { field: "PostedBy", title: "Changed By", width: 100, attributes: { style: "text-align:left;" } },
                { field: "CaseNumber", title: "Case #", width: 100, attributes: { style: "text-align:center;" } },
                { field: "DateOfChange", title: "Date of Change", width: 100, attributes: { style: "text-align:center;" }, headerAttributes: { style: "text-align:center;" } },
                { field: "Delete", title: "", width: 50, attributes: { style: "text-align:center;" }, template: "<image class='deleteRecord' data-ElementId='#=data.StatusCodeId#' src='" + XMASTER_GET_BASE_URL + "/images/icon/action_delete.png' onclick='deleteRecord(this)'/>" }]
        });

        var request = $.ajax({
            url: XGET_STATUS_CHANGE_MODEL,
            data: JSON.stringify(model),
            type: "post",
            timeout: 20000,
            dataType: "json",
            contentType: "application/json"
        }).fail(function (jqXHR, textStatus, errorThrown) {
            kendo.ui.progress($(".loading-status"), false);
            _this.ShowErrorNotification(textStatus + ": " + errorThrown);
        }).done(function (msg) {
            _this.ShowLoading(false);
            if (msg.ResponseCode === 200) {

                _this.statusChangeHistoryDataSource = msg.Data;
                var dataSource = new kendo.data.DataSource({ data: _this.statusChangeHistoryDataSource });
                var grid = $("#kendoStatusHistoryGrid").data("kendoGrid");
                dataSource.read();
                grid.setDataSource(dataSource);
                grid.refresh();
                $('#btnPrint').show();

                //viewModel.showSuccessNotification();
            } else {
                kendo.ui.progress($(".loading-status"), false);
                _this.ShowErrorNotification(msg.ResponseMessage);
            }
        });
        _this.LoadRequestedByList(function () { });
    };
    this.ShowLoading = function (isShown) {
        var pageLoading = $(document).find(".page-loading");
        if (pageLoading.length === 0)
            $(document).find("body").append("<div class='age-loading'></div>");

        kendo.ui.progress($(".page-loading"), isShown);
    };
    this.GetStatusChangeHistory = function () {
        console.log("not implemented GetStatusChangeHistory");
    };
    this.ShowAddEditStatusHistory = function (mode) {
        this.shouldConfirmDeleteAttendance = false;
        var editAddForm = $("#editAddForm").data("kendoWindow");

        if (editAddForm == undefined) {
            $("#editAddForm").kendoWindow({
                modal: true,
                width: "500",
                title: "Add Status",
                actions: ["close"],
                resizable: false,
                draggable: true
            });
            $("#txtEffectiveDate, #txtEndDate").kendoMaskedDatePicker({ width: 300 });
            $("#txtEffectiveDate").data("kendoDatePicker").max(new Date());
            $("#txtChangeDate").kendoDatePicker({ width: 300 });
            $("#txtChangeDate").val(kendo.toString(new Date(), "MM/dd/yyyy"));
            $("#ddlRequestedBy, #ddlStatus, #ddlReason").kendoDropDownList({ width: 300 });
            $("#txtCaseNumber").kendoMaskedTextBox({
                mask: "0000000000"
            });
        } else {
            _this.ClearForm();
        }

        $("#rowReason, #txtReason, #rowEndDate").hide();
        $("#editAddForm").data("kendoWindow").center().open();

        $("#txtEffectiveDate").unbind("change");
        $("#txtEffectiveDate").bind("change", function (e) {

            if (utils.isValidDate("#txtEffectiveDate")) {

                kendo.ui.progress($(".loading-status"), true);
                $("#txtEffectiveDate").removeClass('red');
                $("#txtEffectiveDate").attr('title', '');

                var model = {
                    StudentEnrollmentId: _this.studentEnrollmentId,
                    CampusId: _this.campusId,
                    UserId: _this.userId,
                    ValidationMode: _this.ValidationMode,
                    EffectiveDate: $("#txtEffectiveDate").val(),
                    StatusCodeId: _this.ElementId,
                    StatusId: 0
                }

                $("#ddlStatus")
                    .kendoDropDownList({
                        dataTextField: "Description",
                        dataValueField: "ID",
                        optionLabel: "--Select--",
                        value: "-- Select --"
                    });

                _this.GetStatusCodeList(model);

                $("#ddlStatus").unbind("change");
                $("#ddlStatus")
                    .bind("change",
                        function(e) {
                            var dropdownlist = $("#ddlStatus").data("kendoDropDownList");
                            $("#btnSaveStatusChange").removeAttr("disabled");
                            var item = dropdownlist.select() - 1;
                            if (_this.statusChangeToList != undefined && item >= 0) {
                                var statusCode = _this.statusChangeToList[item];
                                var reason = $("#ddlReason").data("kendoDropDownList").wrapper;

                                model.StatusId = statusCode.StatusId;

                                if (statusCode.StatusId === new ESystemStatus().LeaveOfAbsence) {
                                    _this.GetReasons(XGET_LOA_REASONS, undefined);
                                    reason.show();
                                    $("#txtReason").hide();
                                    $("#rowReason, #rowEndDate").show();
                                    $("#txtEndDate")
                                        .data("kendoDatePicker")
                                        .min($("#txtEffectiveDate").data("kendoDatePicker").value());
                                } else if (statusCode.StatusId === new ESystemStatus().Dropped) {
                                    _this.GetReasons(XGET_DROPPED_REASONS, undefined);
                                    reason.show();
                                    $("#txtReason, #rowEndDate").hide();
                                    $("#rowReason").show();
                                } else if (statusCode.StatusId === new ESystemStatus().Suspension) {
                                    reason.hide();
                                    $("#txtReason").show();
                                    $("#rowReason, #rowEndDate").show();
                                    $("#txtEndDate")
                                        .data("kendoDatePicker")
                                        .min($("#txtEffectiveDate").data("kendoDatePicker").value());

                                } else if (statusCode.StatusId === new ESystemStatus().NoStart ||
                                    statusCode.StatusId === new ESystemStatus().FutureStart) {
                                    _this.ValidateAttendanceAndGrades(model);
                                    reason.hide();
                                    $("#txtReason").hide();
                                    $("#rowReason, #rowEndDate").hide();
                                } else {
                                    reason.hide();
                                    $("#txtReason").hide();
                                    $("#rowReason, #rowEndDate").hide();

                                }
                                console.log(item);
                            }

                        });
            } else {
                $("#txtEffectiveDate").addClass('red');
                $("#txtEffectiveDate").attr('title','Invalid Date Format. Please provide a date with the following format: mm/dd/yyyy');
            }


        });
        $("#btnSaveStatusChange").unbind("click");
        $("#btnSaveStatusChange").bind("click", function () {
            _this.ValidateRequiredFields(function () {
                var effectiveDate = $("#txtEffectiveDate").val();
                var requestedBy = $("#ddlRequestedBy").data("kendoDropDownList");
                var status = $("#ddlStatus").data("kendoDropDownList");
                var reason = undefined;
                var endDate = undefined;
                var statusId = 0;
                var item = status.select() - 1;

                if (_this.statusChangeToList != undefined && item >= 0) {
                    var statusCode = _this.statusChangeToList[item];
                    statusId = statusCode.StatusId;
                    if (statusCode.StatusId === new ESystemStatus().LeaveOfAbsence) {
                        endDate = $("#txtEndDate").val();
                        reason = $("#ddlReason").data("kendoDropDownList").value();
                    }
                    else if (statusCode.StatusId === new ESystemStatus().Dropped) {
                        reason = $("#ddlReason").data("kendoDropDownList").value();
                    }
                    else if (statusCode.StatusId === new ESystemStatus().Suspension) {
                        endDate = $("#txtEndDate").val();
                        reason = $("#txtReason").val();
                    }
                    console.log(item);
                }

                var model = {
                    EffectiveDate: effectiveDate,
                    StatusCodeId: status.value(),
                    Reason: reason,
                    EndDate: endDate,
                    RequestedBy: requestedBy != undefined ? requestedBy.value() : undefined,
                    CaseNumber: $("#txtCaseNumber").val(),
                    ValidationMode: _this.ValidationMode,
                    UserId: _this.userId,
                    PostedBy: _this.userId,
                    CampusId: _this.campusId,
                    StudentEnrollmentId: _this.studentEnrollmentId,
                    DateOfChange: kendo.toString(new Date(), "MM/dd/yyyy"),
                    StatusId: statusId,
                    HaveBackup: $('#chkDatabaseBackup').prop('checked'),
                    HaveClientConfirmation: $('#chkClientConfirmation').prop('checked')
                };
                _this.ValidateInsert(model);
            });
        });

        _this.BindRequestedByDropDown("#ddlRequestedBy");

        $("#btnCloseForm").css("display", "inline");
        $("#btnCloseForm").unbind("click");
        $("#btnCloseForm").bind("click", function () {
            $("#editAddForm").data("kendoWindow").center().close();
        });

        _this.SetCaseNumberValidation("#txtCaseNumber");

        var txtEffectiveDate = $("#txtEffectiveDate").data("kendoDatePicker");
        if (mode === "edit") {
            _this.ValidationMode = VALIDATION_MODE_EDIT;

            $("#previousStatusRow").removeClass("hidden");
            var dropDownDataSource = _this.statusChangeHistoryDataSource;
            $("#ddlPreviousStatus").kendoDropDownList({
                width: 300,
                dataTextField: "Status",
                dataValueField: "StatusCodeId",
                optionLabel: "--Select--",
                dataSource: dropDownDataSource
            });
            var dropdownlist = $("#ddlPreviousStatus").data("kendoDropDownList");
            dropdownlist.refresh();
            dropdownlist.value(_this.ElementId);

            txtEffectiveDate.value(dropDownDataSource[dropdownlist.select() - 1].EffectiveDate);
            $("#txtEffectiveDate").val(dropDownDataSource[dropdownlist.select() - 1].EffectiveDate);
            txtEffectiveDate.enable(false);
            $("#txtEffectiveDate").change();

        } else {
            _this.ValidationMode = VALIDATION_MODE_INSERT;
            $("#previousStatusRow").addClass("hidden");
            txtEffectiveDate.enable(true);
        }

    };
    this.SetCaseNumberValidation = function(id) {
        $(id).unbind("keydown");
        $(id)
            .bind("keydown",
                function(e) {
                    if ((e.keyCode >= 48 && e.keyCode <= 57) ||
                        (e.keyCode >= 96 && e.keyCode <= 105) ||
                        e.keyCode === 8 ||
                        e.keyCode === 37 ||
                        e.keyCode === 38 ||
                        e.keyCode === 46) {
                        return true;
                    }
                    if (e.keyCode === 13) {
                        $(id).next().focus();
                    } else {
                        return false;
                    }
                });
        $(id).unbind("keyup");
        $(id)
            .bind("keyup",
                function(e) {
                    var pattern = /^\d+$/;
                    return !pattern.test($(id).val());
                });
    };
    this.BindRequestedByDropDown = function(id) {
        var bindRequestedBy = function () {
            $(id).kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "ID",
                optionLabel: "--Select--",
                dataSource: _this.requestedByList
            });
            var dropdownlist = $(id).data("kendoDropDownList");
            dropdownlist.refresh();
        };

        if (_this.requestedByList === undefined) {
            kendo.ui.progress($(".loading-status"), true);
            _this.LoadRequestedByList(bindRequestedBy);
        } else {
            bindRequestedBy();
        }
    };
    this.DeleteStatusHistory = function (e) {
        _this.ValidationMode = VALIDATION_MODE_DELETE;
        _this.ElementId = undefined;
        kendo.ui.progress($(".loading-status"), false);
        //deleteForm
        if (_this.statusChangeHistoryDataSource != undefined) {
            var itemId = $(e).attr('data-elementid');

            var item = _this.statusChangeHistoryDataSource.findObjectInArray(itemId, "StatusCodeId");

            if (item != null) {

                if (item.StatusId === new ESystemStatus().AcademicProbationSap) {
                    _this.ShowErrorNotification("This status cannot be deleted.");
                } else {
                    //  _this.ShowLoading(true);
                    _this.ElementId = item.StatusCodeId;

                    var model =
                    {
                        UserId: _this.userId,
                        PostedBy: _this.userId,
                        CampusId: _this.campusId,
                        StudentEnrollmentId: _this.studentEnrollmentId,
                        StatusChangeHistoryId: _this.ElementId,
                        ValidationMode: _this.ValidationMode,
                        StatusId: item.StatusId
                }

                    _this.ValidateDelete(model, function () {
                        
                        var deleteForm = $("#deleteForm").data("kendoWindow");

                        if (deleteForm == undefined) {
                            $("#deleteForm")
                                .kendoWindow({
                                    modal: true,
                                    width: "500",
                                    title: "Delete Status",
                                    actions: ["close"],
                                    resizable: false,
                                    draggable: true
                                });
                            $("#txtChangeDateDelete").kendoDatePicker({ width: 300 });
                            $("#txtChangeDateDelete").val(kendo.toString(new Date(), "MM/dd/yyyy"));
                            $("#ddlRequestedByDelete, #ddlReasonDelete").kendoDropDownList({ width: 300 });
                            $("#txtCaseNumberDelete")
                                .kendoMaskedTextBox({
                                    mask: "0000000000"
                                });
                        } else {
                            _this.ClearDeleteForm();
                        }

                        $("#deleteForm").data("kendoWindow").center().open();

                        _this.BindRequestedByDropDown("#ddlRequestedByDelete");
                        _this.SetCaseNumberValidation("#txtCaseNumberDelete");
                        _this.LoadDeleteReasons();

                        $("#btnCloseDeleteForm").unbind("click");
                        $("#btnCloseDeleteForm").bind("click", function() {
                            $("#deleteForm").data("kendoWindow").center().close();
                            _this.ClearDeleteForm();
                        });
                        $("#btnDeleteStatusChange").unbind("click");
                        $("#btnDeleteStatusChange").bind("click", function () {
                            
                            _this.ValidateDeleteRequiredFields(
                                function () {
                                    var status = _this.statusChangeHistoryDataSource.findObjectInArray(_this.ElementId, "StatusCodeId");
                                    var statusId = undefined;

                                    if (status != undefined) {
                                        statusId = status.StatusId;
                                    }
                                    var json = {
                                        UserId: _this.userId,
                                        PostedBy: _this.userId,
                                        CampusId: _this.campusId,
                                        StudentEnrollmentId: _this.studentEnrollmentId,
                                        StatusChangeHistoryId: _this.ElementId,
                                        CaseNumber: $("#txtCaseNumberDelete").val(),
                                        Reason: $("#ddlReasonDelete").data("kendoDropDownList").value(),
                                        RequestedBy: $("#ddlRequestedByDelete").data("kendoDropDownList").value(),
                                        ValidationMode: _this.ValidationMode,
                                        StatusId: statusId
                                    }

                                    _this.ValidateDeleteSequence(json, function() {
                                        _this.DeleteStatus(json);
                                    });

                                }
                            );
                        });
                    });
                }
            }
        }
    };
    this.ClearDeleteForm = function () {
        $("#txtChangeDateDelete").val(kendo.toString(new Date(), "MM/dd/yyyy"));
        $("#ddlRequestedByDelete, #ddlReasonDelete").data("kendoDropDownList").select(0);
        $("#txtCaseNumberDelete").val("");
    };
    this.ShowErrorNotification = function (msg) {
        kendo.ui.progress($(".loading-status"), false);
        var notificationWidget = $("#kNotification").kendoNotification({
            stacking: "down",
            show: window.PositionAtCenter,
            autoHideAfter: 500000000,
            modal: false,
            templates: [{ type: "error", template: $("#errorTemplateEditStatus").html() }]

        }).data("kendoNotification");

        notificationWidget.show({ title: "An error has occurred!", message: msg, src: XMASTER_GET_BASE_URL + "/images/warningIcon.png" }, "error");

        $("#kNotification .closeNotification").unbind("click");
        $("#kNotification .closeNotification").bind("click", function () {
            notificationWidget.hide();
        });
    };

    this.ShowRequiredFieldNotification = function (msg) {
        var notificationWidget = $("#kNotification").kendoNotification({
            stacking: "down",
            show: window.PositionAtCenter,
            autoHideAfter: 500000000,
            modal: false,
            templates: [{ type: "error", template: $("#errorTemplateEditStatus").html() }]

        }).data("kendoNotification");

        notificationWidget.show({ title: "Missing Required Fields!", message: msg, src: XMASTER_GET_BASE_URL + "/images/warningIcon.png" }, "error");

        $("#kNotification .closeNotification").unbind("click");
        $("#kNotification .closeNotification").bind("click", function () {
            notificationWidget.hide();
        });
    };
    this.ShowConfirmDeleteAttendance = function (onSuccess) {
        var form = $("#confirmAttendanceActionForm").data("kendoWindow");
        if (form == undefined) {
            $("#confirmAttendanceActionForm").kendoWindow({
                modal: true,
                width: "500",
                title: "Confirm Status Change",
                actions: ["close"],
                resizable: false,
                draggable: true
            });
        }

        $("#confirmAttendanceActionForm").data("kendoWindow").center().open();

        $("#chkDatabaseBackup").prop("checked", false);
        $("#chkClientConfirmation").prop("checked", false);
        $("#btnContinueDeletingAttendance").attr("disabled", "disabled");

        $("#chkDatabaseBackup").unbind("change");
        $("#chkDatabaseBackup").bind("change", function () {
            if ($("#chkDatabaseBackup").prop("checked") === true && $("#chkClientConfirmation").prop("checked") === true) {
                $("#btnContinueDeletingAttendance").removeAttr("disabled");
            } else {
                $("#btnContinueDeletingAttendance").attr("disabled", "disabled");
            }
        });
        $("#chkClientConfirmation").unbind("change");
        $("#chkClientConfirmation").bind("change", function () {
            if ($("#chkDatabaseBackup").prop("checked") === true && $("#chkClientConfirmation").prop("checked") === true) {
                $("#btnContinueDeletingAttendance").removeAttr("disabled");
            } else {
                $("#btnContinueDeletingAttendance").attr("disabled", "disabled");
            }
        });
        $("#btnContinueDeletingAttendance").unbind("click");
        $("#btnContinueDeletingAttendance").bind("click", function () {
            $("#confirmAttendanceActionForm").data("kendoWindow").close();
            onSuccess();
        });
        $("#btnCancelDeletingAttendance").unbind("click");
        $("#btnCancelDeletingAttendance").bind("click", function () {
            $("#confirmAttendanceActionForm").data("kendoWindow").close();
        });
    };
    this.ClearForm = function () {
        $("#txtEffectiveDate").val("");
        $("#ddlStatus").data("kendoDropDownList").select(0);
        $("#ddlRequestedBy").data("kendoDropDownList").select(0);
        $("#txtReason").val("");
        $("#txtEndDate").val("");
        $("#ddlReason").data("kendoDropDownList").select(0);
        $("#txtCaseNumber").val("");
    };
    this.LoadDeleteReasons = function () {
        var bindDropDown = function () {
            $("#ddlReasonDelete").kendoDropDownList({
                dataTextField: "Description",
                dataValueField: "ID",
                optionLabel: "--Select--",
                dataSource: _this.deleteReasonList
            });
            var dropdownlist = $("#ddlReasonDelete").data("kendoDropDownList");
            dropdownlist.refresh();
        };

        if (_this.deleteReasonList === undefined) {
            var request2 = $.ajax({
                url: XGET_DELETE_REASONS,
                type: "get",
                timeout: 20000,
                dataType: "json",
                contentType: "application/json"
            }).fail(function (jqXHR, textStatus, errorThrown) {
                kendo.ui.progress($(".loading-status"), false);
                _this.ShowErrorNotification(textStatus + ": " + errorThrown);
            }).done(function (msg) {
                if (msg.ResponseCode === 200) {
                    kendo.ui.progress($(".loading-status"), false);
                    _this.deleteReasonList = msg.Data;
                    bindDropDown();
                } else {
                    _this.ShowErrorNotification(msg.ResponseMessage);
                }
            });
        } else {
            bindDropDown();
        }

       
    };
    this.LoadRequestedByList = function (onsuccess) {
        var request2 = $.ajax({
            url: XGET_REQUESTED_BY_MODEL,
            type: "get",
            timeout: 20000,
            dataType: "json",
            contentType: "application/json"
        }).fail(function (jqXHR, textStatus, errorThrown) {
            kendo.ui.progress($(".loading-status"), false);
            _this.ShowErrorNotification(textStatus + ": " + errorThrown);
        }).done(function (msg) {
            if (msg.ResponseCode === 200) {
                kendo.ui.progress($(".loading-status"), false);
                _this.requestedByList = msg.Data;
                onsuccess();
            } else {
                _this.ShowErrorNotification(msg.ResponseMessage);
            }
        });
    };
    this.GetStatusCodeList = function (model) {
        kendo.ui.progress($(".loading-status"), true);
        var request2 = $.ajax({
            url: XGET_STATUS_CODES_MODEL + "/?input=" + JSON.stringify(model),
            type: "get",
            timeout: 20000,
            dataType: "json",
            contentType: "application/json"
        }).fail(function (jqXHR, textStatus, errorThrown) {
            kendo.ui.progress($(".loading-status"), false);
            _this.ShowErrorNotification(textStatus + ": " + errorThrown);
        }).done(function (msg) {
            if (msg.ResponseCode === 200) {
                kendo.ui.progress($(".loading-status"), false);
                _this.statusChangeToList = msg.Data;
                var dropdownlist = $("#ddlStatus").data("kendoDropDownList");

                $("#ddlStatus").kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "--Select--",
                    value: "-- Select --",
                    dataSource: _this.statusChangeToList
                });
                dropdownlist.refresh();
                dropdownlist.select(0);
            } else {
                _this.ShowErrorNotification(msg.ResponseMessage);
            }
        });
    }
    this.GetReasons = function (url, selectedIndex) {
        kendo.ui.progress($(".loading-status"), true);
        var request = $.ajax({
            url: url,
            type: "get",
            timeout: 20000,
            dataType: "json",
            contentType: "application/json"
        }).fail(function (jqXHR, textStatus, errorThrown) {
            kendo.ui.progress($(".loading-status"), false);
            _this.ShowErrorNotification(textStatus + ": " + errorThrown);
        }).done(function (msg) {
            if (msg.ResponseCode === 200) {
                kendo.ui.progress($(".loading-status"), false);
                _this.loaReasons = msg.Data;
                $("#ddlReason").kendoDropDownList({
                    dataTextField: "Description",
                    dataValueField: "ID",
                    optionLabel: "--Select--",
                    dataSource: _this.loaReasons
                });
                var dropdownlist = $("#ddlReason").data("kendoDropDownList");
                dropdownlist.refresh();
                if (selectedIndex != undefined) {
                    dropdownlist.select(selectedIndex);
                }
                $("#rowReason").show();
            } else {
                _this.ShowErrorNotification(msg.ResponseMessage);
            }
        });
    };
    this.ValidateInsert = function (model) {
        kendo.ui.progress($(".loading-status"), true);
        var request = $.ajax({
            url: XPOST_VALIDATE_INSERT + "/?input=" + JSON.stringify(model),
            data: JSON.stringify(model),
            timeout: 12000000,
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).fail(function (jqXHR, textStatus, errorThrown) {
            kendo.ui.progress($(".loading-status"), false);
            _this.ShowErrorNotification(textStatus + ": " + errorThrown);
        }).done(function (msg) {
            kendo.ui.progress($(".loading-status"), false);
            if (msg.ResponseCode === 200) {

                if (msg.Data.ShouldConfirmAction === true && msg.Data.AllowInsert === true && msg.Data.HaveAttendancePosted === false) {
                    _this.ConfirmChange("Saving the selected status will create an incorrect status sequence. <br/>Modify the new status or save and edit additional statuses to correct.", function () { _this.ApplyStatusChange(model) });
                }
                else if (msg.Data.AllowInsert === true) {
                    _this.ApplyStatusChange(model);
                }

            } else {
                _this.ShowErrorNotification(msg.ResponseMessage);
            }
        });
    };
    this.ValidateDelete = function (model, onSuccess) {
        _this.ShowLoading(true);
        var request = $.ajax({
            url: XPOST_VALIDATE_DELETE + "/?input=" + JSON.stringify(model),
            data: JSON.stringify(model),
            timeout: 12000000,
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).fail(function (jqXHR, textStatus, errorThrown) {
            _this.ShowLoading(false);
            _this.ShowErrorNotification(textStatus + ": " + errorThrown);
        }).done(function (msg) {
            _this.ShowLoading(false);
            if (msg.ResponseCode === 200) {
                if (msg.Data.AllowDelete === true) {
                    onSuccess();
                } else {
                    _this.ShowErrorNotification("This status cannot be deleted.");
                }
            } else {
                _this.ShowErrorNotification(msg.ResponseMessage);
            }
        });
    };
    this.ValidateDeleteSequence = function (model, onSuccess) {
        _this.ShowLoading(true);
        var request = $.ajax({
            url: XPOST_VALIDATE_DELETE_SEQUENCE + "/?input=" + JSON.stringify(model),
            data: JSON.stringify(model),
            timeout: 12000000,
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).fail(function (jqXHR, textStatus, errorThrown) {
            _this.ShowLoading(false);
            _this.ShowErrorNotification(textStatus + ": " + errorThrown);
        }).done(function (msg) {
            _this.ShowLoading(false);
            if (msg.ResponseCode === 200) {
                if (msg.Data.AllowDelete === true && msg.Data.ShouldConfirmAction === false) {
                    onSuccess();
                } else if (msg.Data.AllowDelete === true && msg.Data.ShouldConfirmAction === true) {
                    _this.ConfirmChange("Deleting the selected status will create an incorrect status sequence. <br/>Add  new status or save and edit additional statuses to correct.", function () { onSuccess(); });
                    
                } else {
                    _this.ShowErrorNotification("This status cannot be deleted.");
                }
            } else {
                _this.ShowErrorNotification(msg.ResponseMessage);
            }
        });
    };
    this.ApplyStatusChange = function (model) {
        kendo.ui.progress($(".loading-status"), true);
        var request = $.ajax({
            url: XPOST_INSERT,
            type: "POST",
            data: JSON.stringify(model),
            timeout: 120000000,
            dataType: "text"
        }).fail(function (jqXHR, textStatus, errorThrown) {
            kendo.ui.progress($(".loading-status"), false);
            _this.ShowErrorNotification(textStatus + ": " + errorThrown);
        }).done(function (data) {
            var msg = JSON.parse(data);
            if (msg.ResponseCode === 200) {
                //_this.BindGrid();
                $(_this.btnUpdateSearchControlId).click();
                $("#editAddForm").data("kendoWindow").center().close();
                _this.ClearForm();
                kendo.ui.progress($(".loading-status"), false);
            } else {
                _this.ShowErrorNotification(msg.ResponseMessage);
            }
        });
    };
    this.DeleteStatus = function(model) {
        kendo.ui.progress($(".loading-status"), true);
        var request = $.ajax({
            url: XPOST_DELETE,
            type: "POST",
            data: JSON.stringify(model),
            timeout: 120000000,
            dataType: "text"
        }).fail(function (jqXHR, textStatus, errorThrown) {
            kendo.ui.progress($(".loading-status"), false);
            _this.ShowErrorNotification(textStatus + ": " + errorThrown);
        }).done(function (data) {
            var msg = JSON.parse(data);
            if (msg.ResponseCode === 200) {
                //_this.BindGrid();
                $(_this.btnUpdateSearchControlId).click();
                $("#deleteForm").data("kendoWindow").center().close();
                _this.ClearDeleteForm();
                kendo.ui.progress($(".loading-status"), false);
            } else {
                _this.ShowErrorNotification(msg.ResponseMessage);
            }
        });
    };
    this.ValidateAttendanceAndGrades = function (model) {
        kendo.ui.progress($(".loading-status"), true);
        var request = $.ajax({
            url: XPOST_VALIDATE_ATTENDANCE + "/?input=" + JSON.stringify(model),
            type: "get",
            timeout: 12000000,
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).fail(function (jqXHR, textStatus, errorThrown) {
            kendo.ui.progress($(".loading-status"), false);
            _this.ShowErrorNotification(textStatus + ": " + errorThrown);
        }).done(function (msg) {
            kendo.ui.progress($(".loading-status"), false);
            if (msg.ResponseCode === 200) {
                if (msg.Data.AllowInsert === false) {
                    $("#btnSaveStatusChange").attr("disabled", "disabled");
                    _this.ShowErrorNotification(msg.ResponseMessage);
                    _this.shouldConfirmDeleteAttendance = false;
                }
                else if (msg.Data.AllowInsert === true && msg.Data.ShouldConfirmAction === true && msg.Data.HaveAttendancePosted === true) {
                    _this.shouldConfirmDeleteAttendance = true;
                }
                else if ((msg.Data.AllowInsert === true && msg.Data.ShouldConfirmAction === true && msg.Data.HaveAttendancePosted === false) ||
                           (msg.Data.AllowInsert === true && msg.Data.ShouldConfirmAction === false && msg.Data.HaveAttendancePosted === false)) {
                    _this.shouldConfirmDeleteAttendance = false;
                    $("#btnSaveStatusChange").removeAttr("disabled");
                }
                //$("#btnSave").removeAttr("disabled");
                //Enable save Button
            } else {
                $("#btnSaveStatusChange").attr("disabled", "disabled");
                _this.ShowErrorNotification(msg.ResponseMessage);
            }
        });
    };
    this.ConfirmChange = function (msg, onConfirm) {
        $("#confirmAction").kendoWindow({
            //animation: false,
            modal: true,
            width: "400",
            title: "Confirm",
            actions: ["close"],
            resizable: false,
            draggable: true
        });
        $("#confirmAction .msg").html(msg);
        $("#confirmAction").data("kendoWindow").center().open();
        $("#btnConfirmYes").unbind("click");
        $("#btnConfirmNo").unbind("click");
        $("#btnConfirmYes").bind("click", function () {
            $("#confirmAction").data("kendoWindow").center().close();
            onConfirm();
        });
        $("#btnConfirmNo").bind("click", function () {
            $("#confirmAction").data("kendoWindow").center().close();
        });
    };
    this.ValidateRequiredFields = function (onSuccess) {
        var effectiveDate = $("#txtEffectiveDate").data("kendoDatePicker").value();
        var requestedBy = $("#ddlRequestedBy").data("kendoDropDownList").value();
        var status = $("#ddlStatus").data("kendoDropDownList");
        var reason = undefined;
        var endDate = undefined;
        var caseNumber = $("#txtCaseNumber").val();

        var canContinue = true;
        var msg = "";

        var item = status.select() - 1;

        if (requestedBy === undefined || requestedBy.length === 0) {
            canContinue = false;
            msg += "Requested By is a Required Field! <br/>";
        }
        if (status.value() === undefined || status.value().length === 0) {
            canContinue = false;
            msg += "New Status is a Required Field! <br/>";
        }
        if (caseNumber === undefined || caseNumber.length === 0) {
            canContinue = false;
            msg += "Case # is a Required Field! <br/>";
        }
        if (item >= 0) {
            if (_this.statusChangeToList != undefined) {
                var statusCode = _this.statusChangeToList[item];
                if (statusCode != undefined) {
                    if (statusCode.StatusId === new ESystemStatus().LeaveOfAbsence) {
                        reason = $("#ddlRequestedBy").data("kendoDropDownList").value();
                        endDate = $("#txtEndDate").data("kendoDatePicker").value();
                        if (reason === undefined || reason.length === 0) {
                            canContinue = false;
                            msg += "Reason is a Required Field! <br/>";
                        }
                        if (endDate === undefined || endDate == null || endDate.length === 0) {
                            endDate = $("#txtEndDate").val();
                            if (endDate === undefined || endDate == null || endDate.length === 0) {
                                canContinue = false;
                                msg += "End Date is a Required Field! <br/>";
                            } else {
                                var d1 = new Date(effectiveDate);
                                var d2 = new Date(endDate);
                                if (d1 > d2) {
                                    msg += "End Date must be higher than Effective Date! <br/>";
                                }
                            }
                        } else {
                            if (!utils.isValidDate("#txtEndDate")) {
                                canContinue = false;
                                msg += "End Date field have an invalid format. Please provide a date with the following format: mm/dd/yyyy <br/>";
                            }
                        }
                    }
                    else if (statusCode.StatusId === new ESystemStatus().Dropped) {
                        reason = $("#ddlRequestedBy").data("kendoDropDownList").value();
                        if (reason === undefined || reason.length === 0) {
                            canContinue = false;
                            msg += "Reason is a Required Field! <br/>";
                        }
                    }
                    else if (statusCode.StatusId === new ESystemStatus().Suspension) {
                        reason = $("#txtReason").val();
                        if (reason === undefined || reason.length === 0) {
                            canContinue = false;
                            msg += "Reason is a Required Field! <br/>";
                        }
                    }

                    if (statusCode.StatusId !== new ESystemStatus().FutureStart) {
                        if (effectiveDate === undefined || effectiveDate == null || effectiveDate.length === 0) {
                            effectiveDate = $("#txtEffectiveDate").val();
                            if (effectiveDate === undefined || effectiveDate == null || effectiveDate.length === 0) {
                                canContinue = false;
                                msg += "Effective Date is a Required Field! <br/>";
                            } else {
                                var date1 = new Date(effectiveDate);
                                var date2 = new Date();
                                if (date1 > date2) {
                                    msg += "Effective cannot be a future date! <br/>";
                                }
                            }
                        }
                    }
                }
            }
        }

        if (canContinue && _this.shouldConfirmDeleteAttendance === false) {
            onSuccess();
        }
        else if (_this.shouldConfirmDeleteAttendance === true) {
            _this.ShowConfirmDeleteAttendance(onSuccess);
        } else {
            _this.ShowRequiredFieldNotification(msg);
        }
    };
    this.ValidateDeleteRequiredFields = function(onSuccess) {
        var requestedBy = $("#ddlRequestedByDelete").data("kendoDropDownList").value();
        var reason = $("#ddlReasonDelete").data("kendoDropDownList").value();
        var caseNumber = $("#txtCaseNumberDelete").val();
        var canContinue = true;
        var msg = "";

        if (requestedBy === undefined || requestedBy.length === 0) {
            canContinue = false;
            msg += "Requested By is a Required Field! <br/>";
        }
        if (caseNumber === undefined || caseNumber.length === 0) {
            canContinue = false;
            msg += "Case # is a Required Field! <br/>";
        }
        if (reason === undefined || reason.length === 0) {
            canContinue = false;
            msg += "Reason is a Required Field! <br/>";
        }
        if (canContinue) {
            onSuccess();
        } else {
            _this.ShowRequiredFieldNotification(msg);
        }
    };
    this.Print = function () {
        var divContents = '<div class="contentPrint">' + $("#tableForm").html() + '</div>';

        var printWindow = window.open('', '', 'height=400,width=900');
        printWindow.document.write('<html><head><title>Edit Status History</title>');
        printWindow.document.write('<style type="text/css">');
        printWindow.document.write('th, td{white-space: normal !important; text-overflow: initial !important;');
        printWindow.document.write('</style>');
        printWindow.document.write('<style type="text/css" media="print">');
        printWindow.document.write('@page { size: landscape; } .rcbInput{ width: 300px;} th, td{white-space: normal !important; text-overflow: initial !important;}');
        printWindow.document.write('</style>');
        printWindow.document.write('<link href="/Advantage/Current/Site/App_Themes/Blue_Theme/Blue_Theme.min.css" rel="stylesheet" type="text/css" id="Link2" />');
        printWindow.document.write('<link href="/Advantage/Current/Site/Kendo/styles/kendo.common.min.css" rel="stylesheet"/>');
        printWindow.document.write('<link href="/Advantage/Current/Site/Kendo/styles/kendo.blueopal.min.css" rel="stylesheet"/>');
        printWindow.document.write('<link href="/Advantage/Current/Site/css/SiteStyle.min.css" rel="stylesheet"/>');
        printWindow.document.write('<link href="/Advantage/Current/Site/css/localhost_lowercase.css" rel="stylesheet"/>');
        printWindow.document.write('<link href="/Advantage/Current/Site/css/StudentSearchControl.css" rel="stylesheet"/>');
        printWindow.document.write('</head><body style="overflow-x: visible !important; overflow-y: visible !important;" >');
        printWindow.document.write('<div class="print">' + divContents + '</div>');
        printWindow.document.write('</body></html>');

        $(printWindow.document).find("a:contains('select')").remove();
        $(printWindow.document).find('#addStatus').remove();
        $(printWindow.document).find('.k-nav-prev, .k-nav-next, .k-pager-nav, .k-pager-sizes').remove();
        $(printWindow.document).find(".grid th:last-child, .grid td:last-child, colgroup col:last, .k-grid-header-wrap colgroup col:last").remove();
        $(printWindow.document).find("#btnPrint").remove();
        setTimeout(function () {
            printWindow.print();
            setTimeout(function () {
                printWindow.document.close();
                printWindow.close();
            }, 500);
        }, 1000);

    };
}
