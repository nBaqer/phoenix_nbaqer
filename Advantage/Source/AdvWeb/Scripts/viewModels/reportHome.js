﻿(function ($, kendo, datasources, common, reportCommon) {


    //----------------------------------------------------------------------------------------
    // View Model that represents the data to be bound to the 
    // view
    //----------------------------------------------------------------------------------------
    var viewModel = kendo.observable({
        //view data
        getPageGroupsAndPages: function () {
            kendo.ui.progress($("#loading"), true);
            
            var links = new Array();

            var selectedModule = "Reports";
            var selectedSubLevel = "Admissions";

            var currentCampusId = $.QueryString["cmpid"];

            var userId = $('#hdnUserId').val();
            var isImp = this.isImpersonating();
            var filter = { 'CampusId': currentCampusId, 'UserId': userId, 'IsImpersonating': isImp };

            var cacheKey = "LinksHtml_" + userId + "_" + currentCampusId;
            var cachedHtml = storageCache.getFromLocalCache(cacheKey);
            var showNACCASReports = $('#hdnShowNACCASReports').val();
            if (cachedHtml) {
                $('#reportLinks').html(cachedHtml);
                kendo.ui.progress($("#loading"), false);
            } else {

                links = [];
                var uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                $.getJSON(uri, filter, function (data1) {
                    var control = selectedSubLevel.replace(/\s+/g, '');
                    links.push(reportCommon.renderLinks(control, selectedSubLevel, data1));

                    selectedSubLevel = "Academics";
                    uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                    $.getJSON(uri, filter, function (data2) {
                        control = selectedSubLevel.replace(/\s+/g, '');
                        links.push(reportCommon.renderLinks(control, selectedSubLevel, data2));


                        selectedSubLevel = "IPEDS Reports";
                        uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                        $.getJSON(uri, filter, function (data3) {
                            control = selectedSubLevel.replace(/\s+/g, '');
                            links.push(reportCommon.renderLinks(control, selectedSubLevel, data3));
                            selectedSubLevel = "NACCAS Reports";

                            uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                            $.getJSON(uri, filter, function (data4) {
                                control = selectedSubLevel.replace(/\s+/g, '');
                                if (showNACCASReports === "yes") {
                                    links.push(reportCommon.renderLinks(control,selectedSubLevel,data4));
                                }

                            selectedSubLevel = "Student Accounts";
                            uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                            $.getJSON(uri, filter, function (data5) {
                                control = selectedSubLevel.replace(/\s+/g, '');
                                links.push(reportCommon.renderLinks(control, selectedSubLevel, data5));

                                selectedSubLevel = "Financial Aid";
                                uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                                $.getJSON(uri, filter, function (data6) {
                                    control = selectedSubLevel.replace(/\s+/g, '');
                                    links.push(reportCommon.renderLinks(control, selectedSubLevel, data6));


                                    selectedSubLevel = "Faculty";
                                    uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                                    $.getJSON(uri, filter, function (data7) {
                                        control = selectedSubLevel.replace(/\s+/g, '');
                                        links.push(reportCommon.renderLinks(control, selectedSubLevel, data7));

                                        selectedSubLevel = "Placement";
                                        uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                                        $.getJSON(uri, filter, function (data8) {
                                            control = selectedSubLevel.replace(/\s+/g, '');
                                            links.push(reportCommon.renderLinks(control, selectedSubLevel, data8));

                                            selectedSubLevel = "System";
                                            uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                                            $.getJSON(uri, filter, function (data9) {
                                                control = selectedSubLevel.replace(/\s+/g, '');
                                                links.push(reportCommon.renderLinks(control, selectedSubLevel, data9));
                                                
                                                    reportCommon.clearAllHtmlControls();
                                                reportCommon.setAllHtmlControls(links);

                                                kendo.ui.progress($("#loading"), false);

                                                var htmlToSave = $('#reportLinks').html();
                                                storageCache.insertInLocalCache(cacheKey, htmlToSave);
                                            });

                                        });

                                    });

                                });

                            });

                        });

                    });

                    });
                });

            }



        },
        isImpersonating: function () {

            var bReturn = false;
            var isImp = $("#hdnImpIsImpersonating").val();

            if (isImp) {
                isImp = isImp.toUpperCase();
                if (isImp == "TRUE") bReturn = true;
            }

            return bReturn;

        },

        //-------------------------------------------------------------
        // Bring back ontly the reports for one page group
        //-------------------------------------------------------------
        filterPageGroups: function (id, textValue) {

            if (id == 0) {
                this.getPageGroupsAndPages();
            } else {
                kendo.ui.progress($("#loading"), true);
                var selectedModule = "Reports";
                var selectedSubLevel = textValue;
                var uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });

                var currentCampusId = $.QueryString["cmpid"];

                var userId = $('#hdnUserId').val();
                var filter = { 'CampusId': currentCampusId, 'UserId': userId };



                $.getJSON(uri, filter, function (data) {

                    var control = selectedSubLevel.replace(/\s+/g, '');
                    var ctrlLinks = reportCommon.renderLinks(control, selectedSubLevel, data);

                    reportCommon.clearAllHtmlControls();
                    reportCommon.setSpecificHtmlControl(control, ctrlLinks);

                    kendo.ui.progress($("#loading"), false);
                });
            }


        }


    });
    //----------------------------------------------------------------------------------------


    $(function () {

        var userId = $('#hdnUserId').val();
        var currentCampusId = $.QueryString["cmpid"];

        var cacheKey = "LinksHtml_" + userId + "_" + currentCampusId;
        var cachedHtml = storageCache.getFromLocalCache(cacheKey);

        if (!cachedHtml) common.ajaxSetup(viewModel);

        //setup error details window
        jQuery("#errorDetails").kendoWindow({
            width: '450px',
            height: '300px',
            modal: true,
            title: 'An Error has Occurred!',
            open: function () {
                kendo.bind(this.element, viewModel);
                this.element.data('kendoWindow').center();
            },
        });

        var win = $("#errorDetails").data("kendoWindow");
        common.errorWin(win);



        //----------------------------------------------------------
        // add the 'choose' item to each dropdown
        //----------------------------------------------------------
        jQuery('#reportModulesDropDownList').kendoDropDownList({
            dataSource: [{ "Id": 0, "text": "-------View All Reports-------" },
                    { "Id": 1, "text": "Admissions" },
                    { "Id": 2, "text": "Academics" },
                    { "Id": 3, "text": "IPEDS Reports" },
                    { "Id": 4, "text": "NACCAS Reports" },
                    { "Id": 5, "text": "Student Accounts" },
                    { "Id": 6, "text": "Financial Aid" },
                    { "Id": 7, "text": "Faculty" },
                    { "Id": 8, "text": "Placement" },
                    { "Id": 9, "text": "System" }
            ],
            dataTextField: 'text',
            height: 500,
            dataValueField: 'Id',
            value: "-------View All Reports-------",
            change: function (e) {
                viewModel.filterPageGroups(this.value(), this.text());
            }
        });
        var showNACCASReports = $('#hdnShowNACCASReports').val();
        if (showNACCASReports === "no") {
            var ddl = $("#reportModulesDropDownList").data("kendoDropDownList");
            var oldData = ddl.dataSource.data();
            ddl.dataSource.remove(oldData[4]);
        }
        //----------------------------------------------------------

        //-------------------------------------------------------------
        // get the campusid from the query string
        //-------------------------------------------------------------

        datasources.reportModulesDataSource.transport.options.read.data.campusId = currentCampusId;



        //bind the view to the view model
        kendo.bind(jQuery("#content"), viewModel);


        var currentModule = storageCache.getFromSessionCache('currentModuleName');
        if (currentModule === 'Admissions') {
            $("#reportModulesDropDownList").data("kendoDropDownList").select(1);
            viewModel.filterPageGroups(1, 'Admissions');
        } else {
             viewModel.getPageGroupsAndPages();
        }


        
        

        
        


    });


})(window.$, window.kendo, window.datasources, window.common, window.reportCommon, window.kendoControlSetup);