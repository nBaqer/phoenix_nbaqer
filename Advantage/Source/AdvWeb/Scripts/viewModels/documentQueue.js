﻿(function (jQuery, kendo,   common, datasources) {

    //-------------------------------------------------------------------------------------------------------
    // View Model that represents the view
    //-------------------------------------------------------------------------------------------------------
    var viewModel = kendo.observable({
        //documents data
        documents: datasources.requirementApprovalDataSource,
        selectedDocument: {},

        //program versions data
        programVersions: datasources.programVersionDataSource,
        selectedProgramVersion: {},

        //enrollment status data
        enrollmentStatuses: datasources.enrollmentStatusesDataSource,
        selectedEnrollmentStatus: {},

        //document status data
        documentStatuses: datasources.documentStatusesDataSource,
        selectedDocumentStatus: {},

        //student groups data
        studentGroups: datasources.studentGroupsDataSource,
        selectedStudentGroup: {},

        //view data
        startDate: null,
        selectedStudent: null,
        documentName: null,
        documentApprovalLabel: null,
        approveDisabled: false,
        userName: null,

        //show last dropdown
        showLast: [{ Id: "30", Description: "30 Days" },
                    {Id:"60", Description: "60 Days"},
                    {Id:"90", Description: "90 Days"},
                    { Id: "1000", Description: "All Data" } ],
        selectedShowLast: {},

        //-----------------------------------------------
        //View Model methods
        //-----------------------------------------------

        //retrieve documents from service layer
        findDocuments: function () {

            var stDate = formattedDate(viewModel.startDate,true);

            datasources.requirementApprovalDataSource.transport.options.read.data.FileUploaded = true;
            datasources.requirementApprovalDataSource.transport.options.read.data.ProgramVersionId = viewModel.selectedProgramVersion.Id;
            datasources.requirementApprovalDataSource.transport.options.read.data.EnrollmentStatusId = viewModel.selectedEnrollmentStatus.Id;
            datasources.requirementApprovalDataSource.transport.options.read.data.StudentGroupId = viewModel.selectedStudentGroup.Id;
            datasources.requirementApprovalDataSource.transport.options.read.data.StartDate = stDate;
            datasources.requirementApprovalDataSource.transport.options.read.data.ShowLastValue = viewModel.selectedShowLast.Id;
            datasources.requirementApprovalDataSource.transport.options.read.data.UserId = jQuery('#hdnUserId').val();

            viewModel.set("documents", datasources.requirementApprovalDataSource);
            if (datasources.requirementApprovalDataSource.page() > 1)
                datasources.requirementApprovalDataSource.page(1);
            else {
                datasources.requirementApprovalDataSource.read();
            }
            
        },
        
        //get selected document and show approval window
        approveDocument: function (e) {
            
            var selectedDocument = e.data;
            
            viewModel.set("selectedDocument", selectedDocument);
            viewModel.set("selectedStudent", selectedDocument.FullName);
            viewModel.set("documentName", selectedDocument.DocumentName);
            
            datasources.documentStatusesDataSource.transport.options.read.data.IsApproved = true;
            $("#approvalWin").data("kendoWindow").open();
        },
        
        //approve the document and close the window
        approvalConfirmed: function () {

            //make sure an approval status was chosen
            if (!viewModel.selectedDocumentStatus) {
                alert("Please choose an approval status.");
                return;
            } else if (!viewModel.selectedDocumentStatus.Id) {
                alert("Please choose an approval status.");
                return;
            }
            


            datasources.requirementApprovalDataSource.transport.options.update.data.StudentDocumentId = viewModel.selectedDocument.Id;
            
            viewModel.selectedDocument.set("", true);
            viewModel.selectedDocument.set("DateRequested", viewModel.selectedDocument.DateRequested);
            viewModel.selectedDocument.set("DateReceived", viewModel.selectedDocument.DateReceived);
            viewModel.selectedDocument.set("DocumentStatusId", viewModel.selectedDocumentStatus.Id);
            viewModel.selectedDocument.set("ModUser", $('#hdnUserName').val());
            datasources.requirementApprovalDataSource.sync();

            viewModel.set("documentApprovalLabel", "Your document is approved.");
            viewModel.set("approveDisabled", true);

            datasources.requirementApprovalDataSource.read();

            alert('Your document has been approved!');
            $("#approvalWin").data("kendoWindow").close();

        },
        
        //cancel the approval
        cancelApproval: function() {
            $("#approvalWin").data("kendoWindow").close();
            
        },
        
        //download the document
        downloadDocument: function (e) {
            var file = e.data;
            var fileId = file.FileId;

            var uri = '../proxy/api/Students/Enrollments/Requirements/Documents/Files/{' + fileId + '}/Contents';
            window.location.href = uri;
        },
        
        //reload the page
        resetPage: function () {    
            window.document.forms[0].reset();
            
            viewModel.set("selectedProgramVersion", {});
            viewModel.set("selectedEnrollmentStatus", {});
            viewModel.set("selectedStudentGroup", {});
            viewModel.set("startDate", null);
            viewModel.set("selectedShowLast", {});
            datasources.requirementApprovalDataSource.data([]);
        }
        //-----------------------------------------------
    });

    
    //-----------------------------------------------
    // Initialize the view
    //-----------------------------------------------
    jQuery(function () {

        //setup ajax for the page
        common.ajaxSetup(viewModel);

       

        //setup error window
        jQuery("#errorDetails").kendoWindow({
            width: '450px',
            height: '300px',
            modal: true,
            title: 'An Error has Occurred!',
            open: function () {
                kendo.bind(this.element, viewModel);
                this.element.data('kendoWindow').center();
            },
        });
        
        var win = $("#errorDetails").data("kendoWindow");
        common.errorWin(win);
        
        //setup approval window
        jQuery("#approvalWin").kendoWindow({
            width: '475px',
            height: '210px',
            modal: true,
            title: 'Approve this Document?',
            open: function () {
                kendo.bind(this.element, viewModel);    
                this.element.data('kendoWindow').center();
            },
            close: function (e) {
                viewModel.set("selectedDocument", null);
                viewModel.set("selectedStudent", null);
                viewModel.set("documentName", null);
                viewModel.set("documentApprovalLabel", "");
                viewModel.set("approveDisabled", false);
                viewModel.set("selectedDocumentStatus", null);
                
            }
        });

        //setup documents grid
        jQuery("#pendingDocumentsGrid").kendoGrid({
            scrollable: true,
            sortable: false,
            pageable: {pageSize:20, page:1},
            selectable: false,
            filterable: false,
            columns: [
                { "template": "<button class='k-button' data-bind='click: approveDocument', title= 'Click to approve this document'>Approve</button>", width: 60 },
                { "template": "<button class='k-button' data-bind='click: downloadDocument', title= 'Click to download this document for viewing'>Download</button>", width: 60 },
                { field: 'Id', title: 'Id', width: 1, hidden: true },
                { "field": "FullName", "title": "Student Name", width: 100 },
                { "field": "DocumentName", "title": "Document", width: 125, attributes: { "style": "float:left;" } },
                { "field": "DateModified", "title": "Modified", width: 50 },
                //{ "field": "ModUser", "title": "User", width: 50 },
                { "field": "RequiredFor", "title": "Req For", width: 50 },
                { "field": "SystemModuleName", "title": "Module", width: 55 },
                { "field": "DateRequested", "title": "Date Requested", width: 1, hidden: true },
                { "field": "DateReceived", "title": "Date Received", width: 1, hidden: true },
                { "field": "ProgramVersion", "title": "Program Version", width: 75 },
                { "field": "EnrollmentStatus", "title": "Enroll Status", width: 80 },
                { "field": "StartDate", "title": "Start Date", width: 50 },
                { "field": "IsApproved", "title": "IsApproved", hidden: true, width: 1 },
                { "field": "FileId", "title": "FileId", hidden: true, width: 1 }
            ]
        });

        
        //-----------------------------------------------------------
        //setup the dropdown lists
        //-----------------------------------------------------------
       
        jQuery('#programVersionDropDownList,#enrollmentStatusDropDownList,#studentGroupsDropDownList,#documentStatusDropDownList').kendoDropDownList({
            dataTextField: 'Description',
            dataValueField: 'Id',
            optionLabel: {
                Description: "------------Select------------",
                id: null
            },
            value: "------------Select------------"
        });
        jQuery('#showLast').kendoDropDownList({
            value: "30 Days"
        });
        //-----------------------------------------------------------
        
        
        
        //-----------------------------------------------------------
        //setup the tooltip controls
        //-----------------------------------------------------------
        window.kendoControlSetup.setupTabStrip("#tabstrip");
        window.kendoControlSetup.setupToolTip("#btnSearch", "You can search for a list of un-approved documents by any combination of Program Version,<br> Start Date, Enroll Status, Student Group, and Show Last. We will retrieve<br> documents that meet the criteria for this campus.", "top");
        window.kendoControlSetup.setupToolTip("#btnReset", "Click here to reset all controls to their<br> default values and start a new search.", "top");
        //-----------------------------------------------------------
        

        
        //allow the user to user the enter key to submit the form
        jQuery('#searchDocsForm').formSimulator();
        jQuery('form').submit(function(e) {
            if (event.preventDefault) { event.preventDefault(); } else { event.returnValue = false; }
        });
        
        //-----------------------------------------------------------
        //get the campus id from the query string and feed it to the
        //datasources that need it
        //-----------------------------------------------------------
        var campusId = $.QueryString['cmpId'];
        if (!campusId) campusId = $.QueryString['campid'];
        dataSourceCommon.setCampus(datasources, campusId);
        //-----------------------------------------------------------
        
        
        //Retrieve the initial set of unapproved documents
        viewModel.findDocuments();

        //Bind the view model to the view
        kendo.bind(jQuery("#frame"), viewModel);
        
    });
})(window.jQuery, window.kendo,  window.common, window.datasources);





