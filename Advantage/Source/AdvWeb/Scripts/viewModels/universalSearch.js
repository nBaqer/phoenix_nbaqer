﻿var selectedCampusIds = [];
var selectedCampusDescriptions = [];
var selectedLeadStatusIds = [];
var selectedLeadStatusDescriptions = [];
var selectedAdmissionRepIds = [];
var selectedAdmissionRepDescriptions = [];
(function ($, kendo, datasources) {

    //----------------------------------------------------------------------------------------
    // View Model that represents the data to be bound to the 
    // view
    //----------------------------------------------------------------------------------------
    var viewModel = kendo.observable({
        studentDataSource: datasources.uSearchStudentDataSource,
        //autoCompleteTemplateOld:
        //    '<span class="k-state-default"><img onerror=\'this.src = \"#: data.NotFoundImageUrl #\"\' src=\"#: data.ImageUrl #\" alt=\"#:data.Id#\" /></span>' +
        //        '<span class="k-state-default"><h3>#: data.FullName #</h3><p> #: data.Field1 #&nbsp;&nbsp;#: data.Field2 #<br>' +
        //        '#: data.Field3 #&nbsp;&nbsp;#: data.Field4 # <br>' +
        //        '#: data.Field5 #<br>',

        autoCompleteTemplate: '<span class="k-state-default"><img src=\"#: data.Image64 #\" alt=\"#:data.Id#\" /></span>' +
        '<span class="k-state-default"><h3>#: data.FullName #</h3><p> #: data.Field1 #&nbsp;&nbsp;#: data.Field2 #<br>' +
        '#: data.Field3 #&nbsp;&nbsp;#: data.Field4 # <br>' +
        '#: data.Field5 #<br>',

        baseUrl: function () {
            return XMASTER_GET_BASE_URL;
        },

        getEntityData: function (id, text) {
            switch (text) {
                case 'Student':
                    datasources.uSearchStudentDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
                    return datasources.uSearchStudentDataSource;
                case 'Lead':
                    datasources.uSearchLeadDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
                    return datasources.uSearchLeadDataSource;
                case 'Employer':
                    datasources.uSearchEmployerDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
                    return datasources.uSearchEmployerDataSource;
                case 'Employee':
                    datasources.uSearchEmployeeDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
                    return datasources.uSearchEmployeeDataSource;
                default:
                    return null;
            }
        },
        getFormatteDataSource: function (baseUrl) {
            datasources.uSearchStudentDataSource.transport.options.read.data.baseUrl = baseUrl;
            return datasources.uSearchStudentDataSource;
        },

        GetEmployerCampusId: function (curCampusId, searchList) {
            if (searchList.toLowerCase().indexOf(curCampusId.toLowerCase()) >= 0)
                return curCampusId;
            else {
                var arr = searchList.split(';');
                return arr[0];
            }
        },
        saveModuleState: function (mouduleName) {
            storageCache.insertInSessionCache('currentModuleName', mouduleName);
            storageCache.removeFromSessionCache('currentSubLevelName');
        },
        saveSubLevelState: function (subLevelName) {
            storageCache.insertInSessionCache('currentSubLevelName', subLevelName);
        },
        saveLastSearchFilter: function (item) {
            storageCache.insertInSessionCache('currentSearchFilter', item);
        },
        getLastSearchFilter: function () {
            return storageCache.getFromSessionCache('currentSearchFilter');
        },
        initEntityDropdown: function (autocomplete) {
            var datasource = new kendo.data.DataSource({
                transport: {
                    read: {
                        url: function (options) {
                            var uri = XMASTER_GET_BASE_URL +
                                "/proxy/api/Catalogs/Catalogs/Get" +
                                "?FldName=UniversalSearchModules" +
                                "&CampusID=" +
                                XMASTER_GET_CURRENT_CAMPUS_ID +
                                "&UserId=" +
                                $("#hdnUserId").val() + "&DisableSort=true";
                            delete options.baseUrl;
                            return uri;
                        },
                        dataType: "json",
                        timeout: 20000,
                        type: "GET"
                    }
                }
            });
            jQuery('#entityDropDownList')
                .kendoDropDownList({
                    dataSource: datasource,
                    dataTextField: "Description",
                    dataValueField: "ID",
                    dataBound: function (e) {
                        var selectedItem = viewModel.getLastSearchFilter();
                        if (selectedItem !== undefined) {
                            var dropdownlist = $('#entityDropDownList').data("kendoDropDownList");
                            if (selectedItem >= 0) {
                                dropdownlist.select(selectedItem * 1);
                                dropdownlist.trigger("change");
                            }
                        }
                    },
                    change: function () {
                        autocomplete.setDataSource(viewModel.getEntityData(this.value(), this.text()));
                        viewModel.setFilterOptionsButtonColor();
                        var val = this.value();

                        if (this.select() >= 0)
                            viewModel.saveLastSearchFilter(this.select());
                        var tab = $("#tabstripFilterOptions").data("kendoTabStrip");
                        switch (val) {
                            case "1":
                                {
                                    $("#entities")[0].placeholder = "Search by First Name, Last Name, SSN, Phone, or Student Id...";
                                    viewModel.showLeadFilterTabs(false);
                                    viewModel.cleanLeadFilterTabs();
                                    break;
                                }
                            case "2": {
                                $("#entities")[0].placeholder = "Search by First Name, Last Name, SSN, or Phone...";
                                viewModel.showLeadFilterTabs(true);
                                viewModel.initLeadStatusGrid();
                                viewModel.initAdmissionsRepGrid();
                                viewModel.bindSelectAllLeadStatus();
                                viewModel.bindSelectAllAdminRep();
                                break;
                            }
                            case "3": {
                                $("#entities")[0].placeholder = "Seach by Employer Name, Address, City, State, Zip, or Industry...";
                                viewModel.showLeadFilterTabs(false);
                                viewModel.cleanLeadFilterTabs();
                                break;
                            }
                            case "4": {
                                $("#entities")[0].placeholder = "Seach by Employee Name, Address, City, State, Zip, or Department...";
                                viewModel.showLeadFilterTabs(false);
                                viewModel.cleanLeadFilterTabs();
                                break;
                            }
                            default: {
                                $("#entities")[0].placeholder = "Seach by First Name, Last Name, SSN, Phone or Student Id...";
                                viewModel.showLeadFilterTabs(false);
                                viewModel.cleanLeadFilterTabs();
                            }
                        }
                    }
                });
        },
        showLeadFilterTabs: function (isShown) {
            var tabstrip = $("#tabstripFilterOptions").data("kendoTabStrip");
            if (tabstrip !== undefined && tabstrip !== null) {
                tabstrip.select(0);
                if ($("#tabstripFilterOptions li").length > 1) {
                    if (isShown) {
                        $($("#tabstripFilterOptions li")[1]).show();
                        $($("#tabstripFilterOptions li")[2]).show();
                    } else {
                        $($("#tabstripFilterOptions li")[1]).hide();
                        $($("#tabstripFilterOptions li")[2]).hide();
                    }
                }
            }
        },
        cleanLeadFilterTabs: function () {
            //var itemsAdminRep = window.storageCache.getFromSessionCache(viewModel.FilterAdminRepOptionsKey());
            //var itemsLeadStatus = window.storageCache.getFromSessionCache(viewModel.FilterLeadStatusOptionsKey());
            var itemsCampus = window.storageCache.getFromSessionCache(viewModel.FilterOptionsKey());
            if (itemsCampus === undefined || itemsCampus === null) {
                $("#entitiesDiv").css("background-color", "");
            }
        },
        initAutoComplete: function () {


            var items = window.storageCache.getFromSessionCache(viewModel.FilterOptionsKey());
            if (items) {
                //add the items to the temp array for safe keeping
                selectedCampusIds = items.split(',');
                window.common.filterFunc(selectedCampusIds, "");
            }

            viewModel.setInitialCampus(!items);


            return $("#entities").kendoAutoComplete({
                minLength: 3,
                maxLength: 5,
                dataTextField: "SearchField",
                placeholder: "Search by First Name, Last Name, SSN, Phone, or Student Id...",
                template: viewModel.autoCompleteTemplate,
                dataSource: viewModel.getFormatteDataSource(viewModel.baseUrl()),
                height: 480,
                width: 400,
                filter: "contains",
                select: function (e) {
                    var dataItem = this.dataItem(e.item.index());
                    if (dataItem) {

                        $("#hdnCurrentEntityId").val(dataItem.Id);
                        $("#hdnCurrentEntityName").val(dataItem.FullName);

                        var dropdownlist = $("#entityDropDownList").data("kendoDropDownList");

                        var selectedIndex = dropdownlist.value() - 1;

                        $("#hdnEntityType").val(selectedIndex);

                        var resId = $.QueryString["resid"];

                        if (modelCommon.isEntityResource(resId)) {

                            $("#hdnSearchCampusId").val(dataItem.SearchCampusId);

                            switch (selectedIndex) {
                                case 0:

                                    viewModel.saveModuleState("Academics");
                                    viewModel.saveSubLevelState("Manage Students");
                                    break;
                                case 1:

                                    sessionStorage.setItem("NewLead", "false"); // Set the new lead flag to false to load the lead in info page
                                    viewModel.saveModuleState("Admissions");
                                    viewModel.saveSubLevelState("Leads");

                                    break;
                                case 2:

                                    viewModel.saveModuleState("Placement");
                                    viewModel.saveSubLevelState("Employers");

                                    $("#hdnSearchCampusId").val(viewModel.GetEmployerCampusId(XMASTER_GET_CURRENT_CAMPUS_ID, dataItem.SearchCampusStringList));

                                    break;
                                case 3:

                                    viewModel.saveModuleState("Human Resources");
                                    viewModel.saveSubLevelState("Employees");
                                    break;
                                default:

                                    viewModel.saveModuleState("Academics");
                                    viewModel.saveSubLevelState("Manage Students");
                            }
                            if (resId === "268") {
                                viewModel.GetUrlAddress(selectedIndex, dataItem);
                            } else {
                                var droplist = $('#entityDropDownList').data("kendoDropDownList");
                                var selectedSearch = droplist.value() * 1;
                                if (resId === "542" && selectedSearch == 1) {
                                    __doPostBack('mruChanged_and_redirect_542', dataItem);
                                } else {
                                    __doPostBack('mruChanged', dataItem);
                                }
                            }

                        } else {
                            viewModel.GetUrlAddress(selectedIndex, dataItem);
                        }
                    }
                }
            }).data("kendoAutoComplete");

        },
        //--------------------------
        // get the url address to redirect
        //--------------------------
        GetUrlAddress: function (selectedIndex, dataItem) {
            switch (selectedIndex) {
                case 0:
                    viewModel.saveModuleState("Academics");
                    viewModel.saveSubLevelState("Manage Students");
                    //location.href = XMASTER_GET_BASE_URL + "PL/StudentMaster.aspx?resid=203&mod=AR&cmpid=" + dataItem.SearchCampusId + "&desc=View Student&entityType=0&entity=" + dataItem.Id;
                    location.href = XMASTER_GET_BASE_URL + "PL/StudentMasterSummary.aspx?resid=868&mod=AR&cmpid=" + dataItem.SearchCampusId + "&desc=View Student&entityType=0&entity=" + dataItem.Id;
                    break;
                case 1:
                    viewModel.saveModuleState("Admissions");
                    viewModel.saveSubLevelState("Leads");
                    location.href = XMASTER_GET_BASE_URL + "AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + dataItem.SearchCampusId + "&desc=View Lead&entityType=1&entity=" + dataItem.Id;

                    break;
                case 2:
                    viewModel.saveModuleState("Placement");
                    viewModel.saveSubLevelState("Employers");
                    var campus = viewModel.GetEmployerCampusId(XMASTER_GET_CURRENT_CAMPUS_ID, dataItem.SearchCampusStringList);
                    location.href = XMASTER_GET_BASE_URL + "/PL/EmployerInfo.aspx?resid=79&mod=PL&cmpid=" + campus + "&desc=View Employer&entityType=2&entity=" + dataItem.Id;

                    break;
                case 3:
                    viewModel.saveModuleState("Human Resources");
                    viewModel.saveSubLevelState("Employees");
                    location.href = XMASTER_GET_BASE_URL + "SY/EmployeeInfo.aspx?resid=52&mod=HR&cmpid=" + dataItem.SearchCampusId + "&desc=View Employee&entityType=3&entity=" + dataItem.Id;

                    break;
                default:
                    viewModel.saveModuleState("Academics");
                    viewModel.saveSubLevelState("Manage Students");
                    //location.href = XMASTER_GET_BASE_URL + "PL/StudentMaster.aspx?resid=203&mod=AR&cmpid=" + dataItem.SearchCampusId + "&desc=View Student&entityType=0&entity=" + dataItem.Id;
                    location.href = XMASTER_GET_BASE_URL + "PL/StudentMasterSummary.aspx?resid=868&mod=AR&cmpid=" + dataItem.SearchCampusId + "&desc=View Student&entityType=0&entity=" + dataItem.Id;
            }
        },
        //-------------------------------------------------------------------------------------------------------
        // Filter Options functionatality
        //-------------------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------------------
        //viewModel properties
        //-------------------------------------------------------------------------------------------------------
        FilterOptionsKey: function () {
            return "CampusFilterOptions_" + $("#hdnUserId").val();
        },
        FilterLeadStatusOptionsKey: function () {
            return "LeadStatusFilterOptions_" + $("#hdnUserId").val();
        },
        FilterAdminRepOptionsKey: function () {
            return "AdminRepFilterOptions_" + $("#hdnUserId").val();
        },
        FilterOptionsDescriptionKey: function () {
            return "CampusFilterDescriptionOptions_" + $("#hdnUserId").val();
        },
        FilterLeadStatusOptionsDescriptionKey: function () {
            return "LeadStatusFilterDescriptionOptions_" + $("#hdnUserId").val();
        },
        FilterAdminRepOptionsDescriptionKey: function () {
            return "AdminRepFilterDescriptionOptions_" + $("#hdnUserId").val();
        },
        FilterOptionsColorKey: function () {
            return "CampusFilterOptionsColor_" + $("#hdnUserId").val();
        },
        campusItems: datasources.campusesDataSource,
        isAllCampusChecked: false,
        //-------------------------------------------------------------------------------------------------------

        //-------------------------------------------------------------------------------------------------------
        // Initialize the campus grid
        //-------------------------------------------------------------------------------------------------------
        setInitialCampus: function (setCampus) {
            if (setCampus) {
                datasources.uSearchStudentDataSource.transport.options.read.data.initialCampus = XMASTER_GET_CURRENT_CAMPUS_ID;
                datasources.uSearchLeadDataSource.transport.options.read.data.initialCampus = XMASTER_GET_CURRENT_CAMPUS_ID;
                datasources.uSearchEmployerDataSource.transport.options.read.data.initialCampus = XMASTER_GET_CURRENT_CAMPUS_ID;
                datasources.uSearchEmployeeDataSource.transport.options.read.data.initialCampus = XMASTER_GET_CURRENT_CAMPUS_ID;
            } else {
                datasources.uSearchStudentDataSource.transport.options.read.data.initialCampus = '';
                datasources.uSearchLeadDataSource.transport.options.read.data.initialCampus = '';
                datasources.uSearchEmployerDataSource.transport.options.read.data.initialCampus = '';
                datasources.uSearchEmployeeDataSource.transport.options.read.data.initialCampus = '';
            }
        },
        initCampusGrid: function () {
            var showInactive = window.storageCache.getFromSessionCache("filterShowInactiveCampus");
            if (showInactive === undefined || showInactive === null) {
                showInactive = false;
            } else {
                if (showInactive === "false") {
                    $("#chkShowInactiveCampus").prop("checked", false);
                } else if (showInactive === "true") {
                    $("#chkShowInactiveCampus").prop("checked", true);
                }
            }

            datasources.uSearchCampuses.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            datasources.uSearchCampuses.transport.options.read.data.input.UserId = $("#hdnUserId").val();
            datasources.uSearchCampuses.transport.options.read.data.input.ShowInactive = showInactive;
            datasources.uSearchCampuses.read();

            $("#grid").kendoGrid({
                dataSource: datasources.uSearchCampuses,
                height: 200,
                width: 300,
                dataBound: viewModel.onCampusDataBound,
                sortable: true,
                selectable: false,
                columns: [{ template: "<input type='checkbox' class='checkboxCampus' />", width: 35, headerTemplate: '<input type="checkbox" data-bind="checked: isAllCampusChecked" id="ckSelectAllCampus" />' },
                { field: "Id", title: "Id", width: 50, hidden: true },
                { field: "Description", title: "Campuses" }]
            });

            var grid = $('#grid').data('kendoGrid');

            //bind click event to the checkbox
            grid.table.on("click", ".checkboxCampus", viewModel.selectCampusRow);

            $("#chkShowInactiveCampus").unbind("change");
            $("#chkShowInactiveCampus").bind("change", function () {
                $(".checkboxCampus").prop("checked", false);
                window.storageCache.insertInSessionCache(viewModel.FilterOptionsKey(), "");
                window.storageCache.insertInSessionCache(viewModel.FilterOptionsDescriptionKey(), "");
                datasources.uSearchCampuses.transport.options.read.data.input
                    .ShowInactive = $("#chkShowInactiveCampus").prop("checked");
                datasources.uSearchCampuses.read();
                $("#grid").data("kendoGrid").refresh();
                window.storageCache.insertInSessionCache("filterShowInactiveCampus", $("#chkShowInactiveCampus").prop("checked"));

            });

        },
        initLeadStatusGrid: function () {
            var showInactive = window.storageCache.getFromSessionCache("filterShowInactiveLeadStatus");
            if (showInactive === undefined || showInactive === null) {
                showInactive = false;
            } else {
                if (showInactive === "false") {
                    $("#chkShowInactiveLeadStatus").prop("checked", false);
                } else if (showInactive === "true") {
                    $("#chkShowInactiveLeadStatus").prop("checked", true);
                }
            }

            var campuses = window.storageCache.getFromSessionCache(viewModel.FilterOptionsKey());
            if (!campuses) {
                campuses = XMASTER_GET_CURRENT_CAMPUS_ID + ",";
            }

            if (selectedCampusIds === undefined || selectedCampusIds.length === 0) {
                selectedCampusIds = campuses.split(",");
            }

            datasources.uSearchLeadStatus.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            datasources.uSearchLeadStatus.transport.options.read.data.input.UserId = $("#hdnUserId").val();
            datasources.uSearchLeadStatus.transport.options.read.data.input.ShowInactive = showInactive;
            datasources.uSearchLeadStatus.transport.options.read.data.input.Campuses = campuses.split(",");

            $("#gridLeadStatus").kendoGrid({
                dataSource: datasources.uSearchLeadStatus,
                height: 200,
                width: 300,
                dataBound: viewModel.onLeadStatusDataBound,
                sortable: true,
                selectable: false,
                columns: [{ template: "<input type='checkbox' class='checkboxLeadStatus' />", width: 35, headerTemplate: '<input type="checkbox" data-bind="checked: isAllLeadStatusChecked" id="ckSelectAllLeadStatus" />' },
                { field: "Id", title: "Id", width: 50, hidden: true },
                { field: "Description", title: "Status" }]
            });

            var grid = $('#gridLeadStatus').data('kendoGrid');

            //bind click event to the checkbox
            grid.table.on("click", ".checkboxLeadStatus", viewModel.selectLeadStatusRow);

            $("#chkShowInactiveLeadStatus").unbind("change");
            $("#chkShowInactiveLeadStatus").bind("change", function () {
                $(".checkboxLeadStatus").prop("checked", false);
                window.storageCache.insertInSessionCache(viewModel.FilterLeadStatusOptionsKey(), "");
                window.storageCache.insertInSessionCache(viewModel.FilterLeadStatusOptionsDescriptionKey(), "");
                datasources.uSearchLeadStatus.transport.options.read.data.input.ShowInactive = $("#chkShowInactiveLeadStatus").prop("checked");
                datasources.uSearchLeadStatus.read();
                $("#gridLeadStatus").data("kendoGrid").refresh();

                window.storageCache.insertInSessionCache("filterShowInactiveLeadStatus", $("#chkShowInactiveLeadStatus").prop("checked"));
            });
        },
        initAdmissionsRepGrid: function () {

            var showInactive = window.storageCache.getFromSessionCache("filterShowInactiveAdminRep");
            if (showInactive === undefined || showInactive === null) {
                showInactive = false;
            } else {
                if (showInactive === "false") {
                    $("#chkShowInactiveAdminRep").prop("checked", false);
                } else if (showInactive === "true") {
                    $("#chkShowInactiveAdminRep").prop("checked", true);
                }
            }

            var campuses = window.storageCache.getFromSessionCache(viewModel.FilterOptionsKey());
            if (!campuses) {
                campuses = XMASTER_GET_CURRENT_CAMPUS_ID + ",";
            }

            if (selectedCampusIds === undefined || selectedCampusIds.length === 0) {
                selectedCampusIds = campuses.split(",");
            }

            datasources.uSearchAdmissionRep.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            datasources.uSearchAdmissionRep.transport.options.read.data.input.UserId = $("#hdnUserId").val();
            datasources.uSearchAdmissionRep.transport.options.read.data.input.Campuses = campuses.split(",");
            datasources.uSearchAdmissionRep.transport.options.read.data.input.ShowInactive = showInactive;

            $("#gridAdminRep").kendoGrid({
                dataSource: datasources.uSearchAdmissionRep,
                height: 200,
                width: 300,
                dataBound: viewModel.onAdminRepDataBound,
                sortable: true,
                selectable: false,
                columns: [{ template: "<input type='checkbox' class='checkboxAdminRep' />", width: 35, headerTemplate: '<input type="checkbox" data-bind="checked: isAllAdminRepChecked" id="ckSelectAllAdminRep" />' },
                { field: "Id", title: "Id", width: 50, hidden: true },
                { field: "Description", title: "Admission Rep" }]
            });

            var grid = $('#gridAdminRep').data('kendoGrid');

            //bind click event to the checkbox
            grid.table.on("click", ".checkboxAdminRep", viewModel.selectAdmissionsRepRow);

            $("#chkShowInactiveAdminRep").unbind("change");
            $("#chkShowInactiveAdminRep").bind("change",
                function () {
                    $(".checkboxAdminRep").prop("checked", false);
                    window.storageCache.insertInSessionCache(viewModel.FilterAdminRepOptionsKey(), "");
                    window.storageCache.insertInSessionCache(viewModel.FilterAdminRepOptionsDescriptionKey(), "");
                    datasources.uSearchAdmissionRep.transport.options.read.data.input
                        .ShowInactive = $("#chkShowInactiveAdminRep").prop("checked");
                    datasources.uSearchAdmissionRep.read();
                    $("#gridAdminRep").data("kendoGrid").refresh();

                    window.storageCache
                        .insertInSessionCache("filterShowInactiveAdminRep",
                        $("#chkShowInactiveAdminRep").prop("checked"));
                });
        },
        //-------------------------------------------------------------------------------------------------------
        // event called when grid data is bound, set grid selections
        //-------------------------------------------------------------------------------------------------------
        onCampusDataBound: function (e) {
            viewModel.setCampusGridSelections();
        },
        onLeadStatusDataBound: function (e) {
            viewModel.setLeadStatusGridSelections();
        },
        onAdminRepDataBound: function (e) {
            viewModel.setAdminRepGridSelections();
        },
        //-------------------------------------------------------------------------------------------------------
        // set the grid selection when the user clicks one of the checkboxes
        //-------------------------------------------------------------------------------------------------------
        selectCampusRow: function () {

            var checked = this.checked;
            var row = $(this).closest("tr");
            var dataItem = $("#grid").data("kendoGrid").dataItem(row);

            var checkedItem = dataItem.ID;
            var checkedDesc = dataItem.Description;
            var index = -1;

            $("#entitiesDiv").css("background-color", "lightgreen");
            window.storageCache.insertInSessionCache(viewModel.FilterOptionsColorKey(), "lightgreen");

            if (checked) {

                row.addClass("k-state-selected");
                index = selectedCampusIds.indexOf(checkedItem);

                if (index === -1) {
                    selectedCampusIds.push(checkedItem);
                    selectedCampusDescriptions.push(checkedDesc);
                }

            } else {

                row.removeClass("k-state-selected");
                index = selectedCampusIds.indexOf(checkedItem);
                if (index !== -1) {
                    window.common.filterFunc(selectedCampusIds, checkedItem);
                    window.common.filterFunc(selectedCampusDescriptions, checkedDesc);
                }

                viewModel.set("isAllCampusChecked", false);
            }

            $("#btnClose").show();

            var campuses = selectedCampusIds.join(",");
            if (!selectedCampusIds) {
                campuses = XMASTER_GET_CURRENT_CAMPUS_ID + ",";
            }
            datasources.uSearchLeadStatus.transport.options.read.data.input.Campuses = campuses.split(",");
            datasources.uSearchAdmissionRep.transport.options.read.data.input.Campuses = campuses.split(",");
            datasources.uSearchLeadStatus.read();
            datasources.uSearchAdmissionRep.read();
        },
        selectLeadStatusRow: function () {

            var checked = this.checked;
            var row = $(this).closest("tr");
            var dataItem = $("#gridLeadStatus").data("kendoGrid").dataItem(row);

            var checkedItem = dataItem.ID;
            var checkedDesc = dataItem.Description;
            var index = -1;

            $("#entitiesDiv").css("background-color", "lightgreen");
            window.storageCache.insertInSessionCache(viewModel.FilterOptionsColorKey(), "lightgreen");

            if (checked) {

                row.addClass("k-state-selected");
                index = selectedLeadStatusIds.indexOf(checkedItem);

                if (index === -1) {
                    selectedLeadStatusIds.push(checkedItem);
                    selectedLeadStatusDescriptions.push(checkedDesc);
                }

            } else {

                row.removeClass("k-state-selected");
                index = selectedLeadStatusIds.indexOf(checkedItem);
                if (index !== -1) {
                    window.common.filterFunc(selectedLeadStatusIds, checkedItem);
                    window.common.filterFunc(selectedLeadStatusDescriptions, checkedDesc);
                }

                viewModel.set("isAllLeadStatusChecked", false);
            }

            $("#btnClose").show();
        },
        selectAdmissionsRepRow: function () {

            var checked = this.checked;
            var row = $(this).closest("tr");
            var dataItem = $("#gridAdminRep").data("kendoGrid").dataItem(row);

            var checkedItem = dataItem.ID;
            var checkedDesc = dataItem.Description;
            var index = -1;

            $("#entitiesDiv").css("background-color", "lightgreen");
            window.storageCache.insertInSessionCache(viewModel.FilterOptionsColorKey(), "lightgreen");

            if (checked) {

                row.addClass("k-state-selected");
                index = selectedAdmissionRepIds.indexOf(checkedItem);

                if (index === -1) {
                    selectedAdmissionRepIds.push(checkedItem);
                    selectedAdmissionRepDescriptions.push(checkedDesc);
                }

            } else {

                row.removeClass("k-state-selected");
                index = selectedAdmissionRepIds.indexOf(checkedItem);
                if (index !== -1) {
                    window.common.filterFunc(selectedAdmissionRepIds, checkedItem);
                    window.common.filterFunc(selectedAdmissionRepDescriptions, checkedDesc);
                }

                viewModel.set("isAllAdminRepChecked", false);
            }

            $("#btnClose").show();
        },
        //---------------------------------------------------------------------------------------
        // set the grid selections based on whether there is anything
        // cached or not.  If cached, use those items for selections.  If nothing
        // is cached, set to default, which is nothing selected
        //---------------------------------------------------------------------------------------
        setCampusGridSelections: function () {

            //set saved arrays to null to start with
            selectedCampusIds = [];
            selectedCampusDescriptions = [];
            //tempoary array
            var tmpselectedCampusIds = [];

            //get instance of grid
            var grid = $("#grid").data("kendoGrid");
            var view = grid.dataSource.view();

            var selectedItems = 0;

            // get filter options from session storage.  If we find items, set the selections
            // to these items
            var items = window.storageCache.getFromSessionCache(viewModel.FilterOptionsKey());
            if (!items) {
                items = XMASTER_GET_CURRENT_CAMPUS_ID + ",";
            }

            //add the items to the temp array for safe keeping
            tmpselectedCampusIds = items.split(',');
            window.common.filterFunc(tmpselectedCampusIds, "");

            //get the current view of the grid

            //Iterate through the view, and select the items in the grid
            //If they are not in the array, unselect them.
            //then add them to the array of selected items
            $.each(view, function (vi, viewItem) {

                var index = tmpselectedCampusIds.indexOf(viewItem.ID);
                var row;
                var ckbox;

                if (index !== -1) {
                    row = grid.tbody.find("tr[data-uid='" + viewItem.uid + "']");
                    if (row) {
                        row.addClass("k-state-selected");
                        ckbox = row.find("td:first input");
                        ckbox.prop("checked", true);
                        selectedItems++;
                    }

                    selectedCampusIds[vi] = viewItem.ID;
                    selectedCampusDescriptions[vi] = viewItem.Description;

                } else {
                    row = grid.tbody.find("tr[data-uid='" + viewItem.uid + "']");
                    if (row) {
                        row.removeClass("k-state-selected");
                        ckbox = row.find("td:first input");
                        ckbox.prop("checked", false);
                    }

                }
            });

            //refresh the tooltip content
            $("#btnFilterOptions").data("kendoTooltip").refresh();

            /*else {
                var itemsAdminRep = window.storageCache.getFromSessionCache(viewModel.FilterAdminRepOptionsKey());
                var itemsLeadStatus = window.storageCache.getFromSessionCache(viewModel.FilterLeadStatusOptionsKey());
                if ((itemsAdminRep === undefined || itemsAdminRep === null) && (itemsLeadStatus === undefined || itemsLeadStatus === null)) {
                    viewModel.unselectAllCampuses(view);
                }
            }*/

            if (view.length === selectedItems) viewModel.set("isAllCampusChecked", true);
            else viewModel.set("isAllCampusChecked", false);

        },
        setLeadStatusGridSelections: function () {

            //set saved arrays to null to start with

            selectedLeadStatusIds = [];
            selectedLeadStatusDescriptions = [];

            //tempoary array
            var tmpselectedLeadStatusIds = [];

            //get instance of grid
            var grid = $("#gridLeadStatus").data("kendoGrid");
            var view = grid.dataSource.view();

            var selectedItems = 0;

            // get filter options from session storage.  If we find items, set the selections
            // to these items
            var items = window.storageCache.getFromSessionCache(viewModel.FilterLeadStatusOptionsKey());
            if (items) {

                //add the items to the temp array for safe keeping
                tmpselectedLeadStatusIds = items.split(',');
                window.common.filterFunc(tmpselectedLeadStatusIds, "");

                //get the current view of the grid


                //Iterate through the view, and select the items in the grid
                //If they are not in the array, unselect them.
                //then add them to the array of selected items
                $.each(view, function (vi, viewItem) {

                    var index = tmpselectedLeadStatusIds.indexOf(viewItem.ID);
                    var row;
                    var ckbox;

                    if (index !== -1) {
                        row = grid.tbody.find("tr[data-uid='" + viewItem.uid + "']");
                        if (row) {
                            row.addClass("k-state-selected");
                            ckbox = row.find("td:first input");
                            ckbox.prop("checked", true);
                            selectedItems++;
                        }

                        selectedLeadStatusIds[vi] = viewItem.ID;
                        selectedLeadStatusDescriptions[vi] = viewItem.Description;

                    } else {
                        row = grid.tbody.find("tr[data-uid='" + viewItem.uid + "']");
                        if (row) {
                            row.removeClass("k-state-selected");
                            ckbox = row.find("td:first input");
                            ckbox.prop("checked", false);
                        }

                    }
                });

                //refresh the tooltip content
                $("#btnFilterOptions").data("kendoTooltip").refresh();

            }
            //nothing was found in cache, so unselect all items in grid
            else {
                var itemsAdminRep = window.storageCache.getFromSessionCache(viewModel.FilterAdminRepOptionsKey());
                var itemsCampus = window.storageCache.getFromSessionCache(viewModel.FilterOptionsKey());
                if ((itemsAdminRep === undefined || itemsAdminRep === null) && (itemsCampus === undefined || itemsCampus === null)) {
                    viewModel.unselectAllLeadStatuses(view);
                }
            }

            if (view.length === selectedItems) viewModel.set("isAllLeadStatusChecked", true);
            else viewModel.set("isAllLeadStatusChecked", false);

        },
        setAdminRepGridSelections: function () {

            //set saved arrays to null to start with

            selectedAdmissionRepIds = [];
            selectedAdmissionRepDescriptions = [];

            //tempoary array
            var tmpselectedAdmissionRepIds = [];

            //get instance of grid
            var grid = $("#gridAdminRep").data("kendoGrid");
            var view = grid.dataSource.view();

            var selectedItems = 0;

            // get filter options from session storage.  If we find items, set the selections
            // to these items
            var items = window.storageCache.getFromSessionCache(viewModel.FilterAdminRepOptionsKey());
            if (items) {

                //add the items to the temp array for safe keeping
                tmpselectedAdmissionRepIds = items.split(',');
                window.common.filterFunc(tmpselectedAdmissionRepIds, "");

                //get the current view of the grid

                //Iterate through the view, and select the items in the grid
                //If they are not in the array, unselect them.
                //then add them to the array of selected items
                $.each(view, function (vi, viewItem) {

                    var index = tmpselectedAdmissionRepIds.indexOf(viewItem.ID);
                    var row;
                    var ckbox;

                    if (index !== -1) {
                        row = grid.tbody.find("tr[data-uid='" + viewItem.uid + "']");
                        if (row) {
                            row.addClass("k-state-selected");
                            ckbox = row.find("td:first input");
                            ckbox.prop("checked", true);
                            selectedItems++;
                        }

                        selectedAdmissionRepIds[vi] = viewItem.ID;
                        selectedAdmissionRepDescriptions[vi] = viewItem.Description;

                    } else {
                        row = grid.tbody.find("tr[data-uid='" + viewItem.uid + "']");
                        if (row) {
                            row.removeClass("k-state-selected");
                            ckbox = row.find("td:first input");
                            ckbox.prop("checked", false);
                        }

                    }
                });

                //refresh the tooltip content
                $("#btnFilterOptions").data("kendoTooltip").refresh();

            }
            //nothing was found in cache, so unselect all items in grid
            else {
                var itemsLeadStatus = window.storageCache.getFromSessionCache(viewModel.FilterLeadStatusOptionsKey());
                var itemsCampus = window.storageCache.getFromSessionCache(viewModel.FilterOptionsKey());
                if ((itemsLeadStatus === undefined || itemsLeadStatus === null) && (itemsCampus === undefined || itemsCampus === null)) {
                    viewModel.unselectAllAdminRep(view);
                }

            }

            if (view.length === selectedItems) viewModel.set("isAllAdminRepChecked", true);
            else viewModel.set("isAllAdminRepChecked", false);

        },
        //-------------------------------------------------------------------------------------------------------
        // Set the color of the filter options button
        //-------------------------------------------------------------------------------------------------------
        setFilterOptionsButtonColor: function () {

            var colorVal = window.storageCache.getFromSessionCache(viewModel.FilterOptionsColorKey());

            if (colorVal && colorVal !== "default") {
                $("#entitiesDiv").css("background-color", colorVal);
            } else {
                $("#entitiesDiv").css("background-color", "");
            }

        },

        //-------------------------------------------------------------------------------------------------------
        // Bind the click event of the filter optiosn button to the opening of the filter options window
        //-------------------------------------------------------------------------------------------------------
        bindFilterOptionsButton: function () {

            $("#btnFilterOptions").bind("click", function () {
                viewModel.initOptionsWindow();
            });

            viewModel.bindOptionWindowButtons();

        },

        //-------------------------------------------------------------------------------------------------------
        // initialize and open the filter options window
        //-------------------------------------------------------------------------------------------------------
        initOptionsWindow: function () {

            var x = $("#btnFilterOptions").offset().left;
            var y = $("#btnFilterOptions").offset().top;

            $("#searchFilterOptions").kendoWindow({
                //animation: false,
                modal: true,
                width: "415",
                height: 350,
                title: "Filters",
                animation: { open: { effects: "expand:vertical", duration: 500 }, close: { effects: "expand:vertical", reverse: true, duration: 500 } },
                actions: ["close"],
                resizable: false,
                draggable: false,
                open: function () {
                    kendo.bind(this.element, viewModel);
                    $("#btnClose").hide();


                    $("#tabstripFilterOptions").kendoTabStrip({
                        animation: {
                            open: {
                                effects: "fadeIn"
                            }
                        }
                    });
                    var dropdownlist = $('#entityDropDownList').data("kendoDropDownList");
                    var selectedSearch = dropdownlist.value() * 1;
                    if (selectedSearch !== 2) {
                        viewModel.showLeadFilterTabs(false);
                        viewModel.cleanLeadFilterTabs();
                    } else {
                        viewModel.showLeadFilterTabs(true);
                        viewModel.initCampusGrid();
                        viewModel.initLeadStatusGrid();
                        viewModel.initAdmissionsRepGrid();
                        viewModel.bindSelectAllLeadStatus();
                        viewModel.bindSelectAllAdminRep();
                    }
                },
                close: function (e) {
                    var lengthCampus = 0;
                    var lengthLeadStatus = 0;
                    var lengthAdminRep = 0;
                    $.each(selectedCampusIds, function (index, value) { if (value && value.length > 0) lengthCampus++; });
                    $.each(selectedLeadStatusIds, function (index, value) { if (value && value.length > 0) lengthLeadStatus++; });
                    $.each(selectedAdmissionRepIds, function (index, value) { if (value && value.length > 0) lengthAdminRep++; });

                    if (lengthCampus > 0 || lengthLeadStatus > 0 || lengthAdminRep > 0) {
                        viewModel.saveSelectionsInCache();

                        viewModel.setInitialCampus(false);

                    } else {
                        window.storageCache.removeFromSessionCache(viewModel.FilterOptionsKey());
                        window.storageCache.removeFromSessionCache(viewModel.FilterLeadStatusOptionsKey());
                        window.storageCache.removeFromSessionCache(viewModel.FilterAdminRepOptionsKey());
                        window.storageCache.removeFromSessionCache(viewModel.FilterOptionsDescriptionKey());
                        window.storageCache.removeFromSessionCache(viewModel.FilterOptionsColorKey());
                        $("#entitiesDiv").css("background-color", "");

                        viewModel.setInitialCampus(true);
                    }

                    var dropdownlist = $('#entityDropDownList').data("kendoDropDownList");
                    var selectedSearch = dropdownlist.value() * 1;
                    if (selectedSearch !== 2) {
                        viewModel.showLeadFilterTabs(false);
                        viewModel.cleanLeadFilterTabs();
                    }

                    $("#btnFilterOptions").data("kendoTooltip").refresh();
                }
            });


            $("#searchFilterOptions").closest(".k-window").css({ top: y + 30, left: x });
            $("#searchFilterOptions").data("kendoWindow").open();

        },

        //-------------------------------------------------------------------------------------------------------
        // Bind the buttons in the filter options window to their respective click events
        //-------------------------------------------------------------------------------------------------------
        bindOptionWindowButtons: function () {

            $("#btnClose").click(function (event) {
                var searchFilterOptions = $("#searchFilterOptions").data("kendoWindow");
                if (searchFilterOptions !== undefined && searchFilterOptions !== null) {
                    searchFilterOptions.close();
                }
            });

            $("#btnCancel").click(function (event) {

                if (confirm("Are you sure you want to cancel your filter option selections?") === false) return false;

                viewModel.setCampusGridSelections();
                var dropdownlist = $('#entityDropDownList').data("kendoDropDownList");
                var selectedSearch = dropdownlist.value() * 1;
                if (selectedSearch === 2) {
                    viewModel.setLeadStatusGridSelections();
                    viewModel.setAdminRepGridSelections();
                }
                viewModel.showFilterClearNotification("Filter option selections have been cancelled.");
                $("#searchFilterOptions").data("kendoWindow").close();

                return true;
            });
        },
        bindSelectAllCampus: function () {
            $("#ckSelectAllCampus").bind("click", function () {

                selectedCampusIds = [];
                selectedCampusDescriptions = [];

                var view = $("#grid").data("kendoGrid").dataSource.view();

                var chkBox = $("#ckSelectAllCampus")[0];
                if (chkBox.checked) viewModel.selectAllCampuses(view);
                else viewModel.unselectAllCampuses(view);

            });
        },

        bindSelectAllLeadStatus: function () {
            $("#ckSelectAllLeadStatus").bind("click", function () {

                selectedLeadStatusIds = [];
                selectedLeadStatusDescriptions = [];

                var view = $("#gridLeadStatus").data("kendoGrid").dataSource.view();

                var chkBox = $("#ckSelectAllLeadStatus")[0];
                if (chkBox.checked) viewModel.selectAllLeadStatuses(view);
                else viewModel.unselectAllLeadStatuses(view);

            });
        },

        bindSelectAllAdminRep: function () {
            $("#ckSelectAllAdminRep").bind("click", function () {

                selectedAdmissionRepIds = [];
                selectedAdmissionRepDescriptions = [];

                var view = $("#gridAdminRep").data("kendoGrid").dataSource.view();

                var chkBox = $("#ckSelectAllAdminRep")[0];
                if (chkBox.checked) viewModel.selectAllAdminRep(view);
                else viewModel.unselectAllAdminRep(view);

            });
        },
        selectAllCampuses: function (view) {

            selectedCampusIds = [];
            selectedCampusDescriptions = [];

            $.each(view, function (index, viewItem) {
                $("#grid").data("kendoGrid").tbody.find("tr[data-uid='" + viewItem.uid + "']")
                    .addClass("k-state-selected");

                selectedCampusIds.push(viewItem.ID);
                selectedCampusDescriptions.push(viewItem.Description);

                $("#entitiesDiv").css("background-color", "lightgreen");
                window.storageCache.insertInSessionCache(viewModel.FilterOptionsColorKey(), "lightgreen");
            });


            $("#grid").data("kendoGrid").tbody.find("tr").each(function () {
                var $this = $(this), ckbox;

                if ($this.hasClass("k-state-selected")) {
                    ckbox = $this.find("td:first input");
                    ckbox.prop("checked", true);
                }
            });

            $("#btnClose").show();
        },

        selectAllLeadStatuses: function (view) {

            selectedLeadStatusIds = [];
            selectedLeadStatusDescriptions = [];

            $.each(view, function (index, viewItem) {
                $("#gridLeadStatus").data("kendoGrid").tbody.find("tr[data-uid='" + viewItem.uid + "']")
                    .addClass("k-state-selected");

                selectedLeadStatusIds.push(viewItem.ID);
                selectedLeadStatusDescriptions.push(viewItem.Description);

                $("#entitiesDiv").css("background-color", "lightgreen");
                window.storageCache.insertInSessionCache(viewModel.FilterOptionsColorKey(), "lightgreen");
            });


            $("#gridLeadStatus").data("kendoGrid").tbody.find("tr").each(function () {
                var $this = $(this), ckbox;

                if ($this.hasClass("k-state-selected")) {
                    ckbox = $this.find("td:first input");
                    ckbox.prop("checked", true);
                }
            });

            $("#btnClose").show();
        },
        selectAllAdminRep: function (view) {

            selectedAdmissionRepIds = [];
            selectedAdmissionRepDescriptions = [];

            $.each(view, function (index, viewItem) {
                $("#gridAdminRep").data("kendoGrid").tbody.find("tr[data-uid='" + viewItem.uid + "']")
                    .addClass("k-state-selected");

                selectedAdmissionRepIds.push(viewItem.ID);
                selectedAdmissionRepDescriptions.push(viewItem.Description);

                $("#entitiesDiv").css("background-color", "lightgreen");
                window.storageCache.insertInSessionCache(viewModel.FilterOptionsColorKey(), "lightgreen");
            });


            $("#gridAdminRep").data("kendoGrid").tbody.find("tr").each(function () {
                var $this = $(this), ckbox;

                if ($this.hasClass("k-state-selected")) {
                    ckbox = $this.find("td:first input");
                    ckbox.prop("checked", true);
                }
            });

            $("#btnClose").show();
        },
        //-------------------------------------------------------------------------------------------------------
        // Unselect all campuses in the grid
        //-------------------------------------------------------------------------------------------------------
        unselectAllCampuses: function (view) {

            $.each(view, function (i, viewItem) {
                $("#grid").data("kendoGrid").tbody.find("tr[data-uid='" + viewItem.uid + "']")
                    .removeClass("k-state-selected");
            });


            $("#grid").data("kendoGrid").tbody.find("tr").each(function () {
                var $this = $(this), ckbox;
                ckbox = $this.find("td:first input");
                ckbox.prop("checked", false);
            });

            selectedCampusIds = [];
            selectedCampusDescriptions = [];

            var colorVal = window.storageCache.getFromSessionCache(viewModel.FilterOptionsColorKey);
            if (colorVal) window.storageCache.insertInSessionCache(viewModel.FilterOptionsColorKey, "default");
            viewModel.clearFilterColorIfNoSelection();

            $("#btnClose").show();

        },
        unselectAllLeadStatuses: function (view) {

            $.each(view, function (i, viewItem) {
                $("#gridLeadStatus").data("kendoGrid").tbody.find("tr[data-uid='" + viewItem.uid + "']")
                    .removeClass("k-state-selected");
            });


            $("#gridLeadStatus").data("kendoGrid").tbody.find("tr").each(function () {
                var $this = $(this), ckbox;
                ckbox = $this.find("td:first input");
                ckbox.prop("checked", false);
            });

            selectedLeadStatusIds = [];
            selectedLeadStatusDescriptions = [];

            var colorVal = window.storageCache.getFromSessionCache(viewModel.FilterOptionsColorKey);
            if (colorVal) window.storageCache.insertInSessionCache(viewModel.FilterOptionsColorKey, "default");
            viewModel.clearFilterColorIfNoSelection();

            $("#btnClose").show();

        },

        unselectAllAdminRep: function (view) {

            $.each(view, function (i, viewItem) {
                $("#gridAdminRep").data("kendoGrid").tbody.find("tr[data-uid='" + viewItem.uid + "']")
                    .removeClass("k-state-selected");
            });


            $("#gridAdminRep").data("kendoGrid").tbody.find("tr").each(function () {
                var $this = $(this), ckbox;
                ckbox = $this.find("td:first input");
                ckbox.prop("checked", false);
            });

            selectedAdmissionRepIds = [];
            selectedAdmissionRepDescriptions = [];
            var colorVal = window.storageCache.getFromSessionCache(viewModel.FilterOptionsColorKey);
            if (colorVal) window.storageCache.insertInSessionCache(viewModel.FilterOptionsColorKey, "default");

            viewModel.clearFilterColorIfNoSelection();

            $("#btnClose").show();

        },
        clearFilterColorIfNoSelection: function () {
            var itemsLeadStatus = window.storageCache.getFromSessionCache(viewModel.FilterLeadStatusOptionsKey());
            var itemsCampus = window.storageCache.getFromSessionCache(viewModel.FilterOptionsKey());
            var itemsAdminRep = window.storageCache.getFromSessionCache(viewModel.FilterAdminRepOptionsKey());
            if ((itemsLeadStatus === undefined || itemsLeadStatus === null) &&
                (itemsCampus === undefined || itemsCampus === null) &&
                (itemsAdminRep === undefined || itemsAdminRep === null)) {
                $("#entitiesDiv").css("background-color", "");
            } else {
                if ((itemsLeadStatus !== undefined && itemsLeadStatus !== null && itemsLeadStatus.length === 0) &&
                    (itemsCampus !== undefined && itemsCampus !== null && itemsCampus.length === 0) &&
                    (itemsAdminRep !== undefined && itemsAdminRep !== null && itemsAdminRep.length === 0)) {
                    $("#entitiesDiv").css("background-color", "");
                }
            }
        },
        //-------------------------------------------------------------------------------------------------------
        // Save the grid selections in the session cache
        //-------------------------------------------------------------------------------------------------------
        saveSelectionsInCache: function () {
            /**
             * Save Campusses
             */
            var ids = "";

            $.each(selectedCampusIds, function (index, value) {
                if (value) ids += value + ',';
            });

            if (ids.substring(0, 1) === ',') ids = ids.substring(1);
            if (ids.substring(ids.length - 1) === ',') ids = ids.substring(0, ids.length - 1);
            window.storageCache.insertInSessionCache(viewModel.FilterOptionsKey(), ids);

            var descr = "";
            selectedCampusDescriptions.sort();
            $.each(selectedCampusDescriptions, function (index, value) {
                if (value) descr += value + ',';
            });

            if (descr.substring(0, 1) === ',') descr = desc.substring(1);
            if (descr.substring(descr.length - 1) === ',') descr = descr.substring(0, descr.length - 1);

            window.storageCache.insertInSessionCache(viewModel.FilterOptionsDescriptionKey(), descr);

            /*
             * Save Lead Statuses
             */
            ids = "";

            $.each(selectedLeadStatusIds, function (index, value) {
                if (value) ids += value + ',';
            });

            if (ids.substring(0, 1) === ',') ids = ids.substring(1);
            if (ids.substring(ids.length - 1) === ',') ids = ids.substring(0, ids.length - 1);
            window.storageCache.insertInSessionCache(viewModel.FilterLeadStatusOptionsKey(), ids);

            descr = "";
            selectedLeadStatusDescriptions.sort();
            $.each(selectedLeadStatusDescriptions, function (index, value) {
                if (value) descr += value + ',';
            });

            if (descr.substring(0, 1) === ',') descr = desc.substring(1);
            if (descr.substring(descr.length - 1) === ',') descr = descr.substring(0, descr.length - 1);

            window.storageCache.insertInSessionCache(viewModel.FilterLeadStatusOptionsDescriptionKey(), descr);

            /*
             * Save Admissions Rep
             */
            ids = "";

            $.each(selectedAdmissionRepIds, function (index, value) {
                if (value) ids += value + ',';
            });

            if (ids.substring(0, 1) === ',') ids = ids.substring(1);
            if (ids.substring(ids.length - 1) === ',') ids = ids.substring(0, ids.length - 1);
            window.storageCache.insertInSessionCache(viewModel.FilterAdminRepOptionsKey(), ids);

            descr = "";
            selectedAdmissionRepDescriptions.sort();
            $.each(selectedAdmissionRepDescriptions, function (index, value) {
                if (value) descr += value + ',';
            });

            if (descr.substring(0, 1) === ',') descr = desc.substring(1);
            if (descr.substring(descr.length - 1) === ',') descr = descr.substring(0, descr.length - 1);

            window.storageCache.insertInSessionCache(viewModel.FilterAdminRepOptionsDescriptionKey(), descr);
        },

        //-------------------------------------------------------------------------------------------------------
        // set up the filter tooltip
        //-------------------------------------------------------------------------------------------------------
        setupFilterTooltip: function () {


            var tooltip = $("#btnFilterOptions").kendoTooltip({
                position: "right",
                callout: false,
                content: viewModel.getFilterOptionsSelectedContent,
                showAfter: 100,
                animation: {
                    open: { effects: "fade:in", duration: 500, },
                    close: { effects: "fade:out", duration: 500, }
                }
            });


        },

        //-------------------------------------------------------------------------------------------------------
        // Set the content for the tooltip
        //-------------------------------------------------------------------------------------------------------
        getFilterOptionsSelectedContent: function () {

            var tmp = "<table  border='0px' cellpadding='2px' cellspacing='2px' width='455px;'>" +
                "<tr>" +
                "<td align='center' style='font-weight:bold;' colspan='2'>Selected Filters (Ctrl+Alt+O)</td>" +
                "</tr>" +
                "<tr>" +
                "<td valign='top' width='155px' style='text-align:right;'>Selected Campuses:</td>" +
                "<td align='left' width='300px'>#campusData#</td>" +
                "</tr>";

            var content = "";
            var arr = [];

            var descr = window.storageCache.getFromSessionCache(viewModel.FilterOptionsDescriptionKey());
            if (descr) {
                if (descr.substring(0, 1) === ',') descr = descr.substring(1);
                arr = descr.split(",");

            } else if (selectedCampusDescriptions.length > 0) {
                arr = selectedCampusDescriptions;
            }

            if (arr.length > 0) {
                $.each(arr, function (index, value) {
                    if (value !== undefined && value !== null && value !== "") content += value + ", ";
                    //if ((index > 0) && (index % 2 === 0)) content += "<br>";
                });

                if (content.substring(content.length - 5, content.length) === " <br>")
                    content = content.substring(0, content.length - 5);

                if (content.substring(content.length - 1, content.length) === " ")
                    content = content.substring(0, content.length - 1);

                if (content.substring(content.length - 1, content.length) === ",")
                    content = content.substring(0, content.length - 1);

                tmp = tmp.replace("#campusData#", content);

            } else {
                var defaultCampus = $("#MasterCampusDropDown").data("kendoDropDownList").text();
                tmp = tmp.replace("#campusData#", defaultCampus);
            }
            /**
             * Selected Lead Status 
             */
            arr = [];
            descr = window.storageCache.getFromSessionCache(viewModel.FilterLeadStatusOptionsDescriptionKey());
            if (descr) {
                if (descr.substring(0, 1) === ',') descr = descr.substring(1);
                arr = descr.split(",");

            } else if (selectedLeadStatusDescriptions.length > 0) {
                arr = selectedLeadStatusDescriptions;
            }
            content = "";
            if (arr.length > 0) {
                $.each(arr,
                    function (index, value) {
                        if (value !== undefined && value !== null && value !== "") content += value + ", ";
                        //if ((index > 0) && (index % 2 === 0)) content += "<br>";
                    });

                if (content.substring(content.length - 5, content.length) === " <br>")
                    content = content.substring(0, content.length - 5);

                if (content.substring(content.length - 1, content.length) === " ")
                    content = content.substring(0, content.length - 1);

                if (content.substring(content.length - 1, content.length) === ",")
                    content = content.substring(0, content.length - 1);

                tmp = tmp + "<tr>" +
                    "<td valign='top' width='155px' style='text-align:right;'>Selected Lead Statuses:</td>" +
                    "<td align='left' width='300px'>" + content + "</td>" +
                    "</tr>";
            }
            /**
             * Selected Admin Rep
             */
            arr = [];
            descr = window.storageCache.getFromSessionCache(viewModel.FilterAdminRepOptionsDescriptionKey());
            if (descr) {
                if (descr.substring(0, 1) === ',') descr = descr.substring(1);
                arr = descr.split(",");

            } else if (selectedAdmissionRepDescriptions.length > 0) {
                arr = selectedAdmissionRepDescriptions;
            }
            content = "";
            if (arr.length > 0) {
                $.each(arr,
                    function (index, value) {
                        if (value !== undefined && value !== null && value !== "") content += value + ", ";
                        //if ((index > 0) && (index % 2 === 0)) content += "<br>";
                    });

                if (content.substring(content.length - 5, content.length) === " <br>")
                    content = content.substring(0, content.length - 5);

                if (content.substring(content.length - 1, content.length) === " ")
                    content = content.substring(0, content.length - 1);

                if (content.substring(content.length - 1, content.length) === ",")
                    content = content.substring(0, content.length - 1);

                tmp = tmp + "<tr>" +
                    "<td valign='top' width='155px' style='text-align:right;'>Selected Admission Reps:</td>" +
                    "<td align='left' width='300px'>" + content + "</td>" +
                    "</tr>";
            }

            tmp = tmp + "</table>";
            return tmp;
        },

        //-------------------------------------------------------------------------------------------------------
        // Show the notification
        //-------------------------------------------------------------------------------------------------------
        showCampusErrorNotification: function (message) {

            var notificationWidget = $("#filterWarning").kendoNotification({
                stacking: "down",
                show: viewModel.PositionAtCenter,
                autoHideAfter: 3000,
                modal: true,
                templates: [{ type: "error", template: $("#errorTemplate").html() }]

            }).data("kendoNotification");

            notificationWidget.show({ title: "&nbsp;&nbsp;&nbsp;&nbsp;Warning", message: message, src: XMASTER_GET_BASE_URL + "/images/warningicon.png" }, "error");

        },

        //-------------------------------------------------------------------------------------------------------
        // Show the notification
        //-------------------------------------------------------------------------------------------------------
        showFilterClearNotification: function (message) {

            var notificationWidget = $("#filterWarning").kendoNotification({
                stacking: "down",
                show: viewModel.PositionAtCenter,
                autoHideAfter: 3000,
                modal: true,
                templates: [{ type: "error", template: $("#infoTemplate").html() }]

            }).data("kendoNotification");

            notificationWidget.show({ title: "&nbsp;&nbsp;&nbsp;&nbsp;Action Completed", message: message, src: XMASTER_GET_BASE_URL + "/images/info.jpg" }, "error");

        },

        //-------------------------------------------------------------------------------------------------------
        // set window position at center of screen
        //-------------------------------------------------------------------------------------------------------
        PositionAtCenter: function (e) {

            if (!$("." + e.sender._guid)[1]) {
                var element = e.element.parent(),
                    eWidth = element.width(),
                    eHeight = element.height(),
                    wWidth = $(window).width(),
                    wHeight = $(window).height(),
                    newTop, newLeft;

                newLeft = Math.floor(wWidth / 2 - eWidth / 2);
                newTop = Math.floor(wHeight / 2 - eHeight / 2);

                e.element.parent().css({ top: newTop, left: newLeft, zIndex: 22222 });
            }
        }
    });



    $(function () {

        $("#hdnSearchCampusId").val("");

        var autocomplete = viewModel.initAutoComplete();
        viewModel.initEntityDropdown(autocomplete);

        viewModel.setFilterOptionsButtonColor();
        viewModel.initCampusGrid();

        viewModel.bindFilterOptionsButton();
        viewModel.bindSelectAllCampus();
        viewModel.setupFilterTooltip();

        Mousetrap.bindGlobal("ctrl+alt+o", function () { $("#btnFilterOptions").trigger("click"); });

        ////bind the view to the view model
        kendo.bind(jQuery("#uContent"), viewModel);

        if ($("#entities")[0]) {
            if ($("#entities")[0].value === "0")
                $("#entities")[0].value = "";
        }
    });

})(window.$, window.kendo, window.datasources);