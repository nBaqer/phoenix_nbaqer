﻿(function ($, kendo, datasources, common, maintenanceCommon) {



    //----------------------------------------------------------------------------------------
    // View Model that represents the data to be bound to the 
    // view
    //----------------------------------------------------------------------------------------
    var viewModel = kendo.observable({
        //view data

        getPageGroupsAndPages: function () {

            kendo.ui.progress($("#loading"), true);

            var links = new Array();

            var selectedModule = "Maintenance1";
            var selectedSubLevel = "Admissions";

            var currentCampusId = $.QueryString["cmpid"];

            var userId = $('#hdnUserId').val();
            var isImp = this.isImpersonating();

            var filter = { 'CampusId': currentCampusId, 'UserId': userId, 'IsImpersonating': isImp };

            var cacheKey = "MaintenanceLinksHtml_" + userId + "_" + currentCampusId;
            var cachedHtml = storageCache.getFromLocalCache(cacheKey);

            if (cachedHtml) {
                $('#maintenanceLinks').html(cachedHtml);
                kendo.ui.progress($("#loading"), false);
            } else {

                links = [];
                //               var uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                var uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                $.getJSON(uri, filter, function (data) {
                    var control = selectedSubLevel.replace(/\s+/g, '');
                    links.push(maintenanceCommon.renderLinks(control, selectedSubLevel, data));

                    selectedSubLevel = "Academics";
                    uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                    $.getJSON(uri, filter, function (data2) {
                        control = selectedSubLevel.replace(/\s+/g, '');
                        links.push(maintenanceCommon.renderLinks(control, selectedSubLevel, data2));


                        selectedSubLevel = "Student Accounts";
                        uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                        $.getJSON(uri, filter, function (data3) {
                            control = selectedSubLevel.replace(/\s+/g, '');
                            links.push(maintenanceCommon.renderLinks(control, selectedSubLevel, data3));

                            selectedSubLevel = "Financial Aid";
                            uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                            $.getJSON(uri, filter, function (data4) {
                                control = selectedSubLevel.replace(/\s+/g, '');
                                links.push(maintenanceCommon.renderLinks(control, selectedSubLevel, data4));

                                selectedSubLevel = "Placement";
                                uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                                $.getJSON(uri, filter, function (data5) {
                                    control = selectedSubLevel.replace(/\s+/g, '');
                                    links.push(maintenanceCommon.renderLinks(control, selectedSubLevel, data5));

                                    selectedSubLevel = "Human Resources";
                                    uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                                    $.getJSON(uri, filter, function (data6) {
                                        control = selectedSubLevel.replace(/\s+/g, '');
                                        links.push(maintenanceCommon.renderLinks(control, selectedSubLevel, data6));

                                        selectedSubLevel = "System";
                                        uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                                        $.getJSON(uri, filter, function (data7) {
                                            control = selectedSubLevel.replace(/\s+/g, '');
                                            links.push(maintenanceCommon.renderLinks(control, selectedSubLevel, data7));

                                            selectedSubLevel = "Reports";
                                            uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
                                            $.getJSON(uri, filter, function (data8) {
                                                control = selectedSubLevel.replace(/\s+/g, '');
                                                links.push(maintenanceCommon.renderLinks(control, selectedSubLevel, data8));

                                                maintenanceCommon.clearAllHtmlControls();
                                                maintenanceCommon.setAllHtmlControls(links);

                                                kendo.ui.progress($("#loading"), false);

                                                var htmlToSave = $('#maintenanceLinks').html();
                                                storageCache.insertInLocalCache(cacheKey, htmlToSave);
                                            });
                                        });
                                    });

                                });

                            });

                        });

                    });
                });
            }
        },
        isImpersonating: function () {

            var bReturn = false;
            var isImp = $("#hdnImpIsImpersonating").val();

            if (isImp) {
                isImp = isImp.toUpperCase();
                if (isImp == "TRUE") bReturn = true;
            }

            return bReturn;

        },

        //-------------------------------------------------------------
        // Bring back only the reports for one page group
        //-------------------------------------------------------------
        filterPageGroups: function (id, textValue) {

            if (id == 0) {
                this.getPageGroupsAndPages();
            } else {

                maintenanceCommon.hideHrControls();

                kendo.ui.progress($("#loading"), true);
                var selectedModule = "Maintenance1";
                var selectedSubLevel = textValue;
                var uri = '../proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'.supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });

                var currentCampusId = $.QueryString["cmpid"];

                var userId = $('#hdnUserId').val();
                var isImp = this.isImpersonating();
                var filter = { 'CampusId': currentCampusId, 'UserId': userId, 'IsImpersonating': isImp };



                $.getJSON(uri, filter, function (data) {
                    var control = selectedSubLevel.replace(/\s+/g, '');
                    var ctrlLinks = maintenanceCommon.renderLinks(control, selectedSubLevel, data);

                    maintenanceCommon.clearAllHtmlControls();
                    maintenanceCommon.setSpecificHtmlControl(control, ctrlLinks);

                    kendo.ui.progress($("#loading"), false);
                });
            }
        }


    });
    //----------------------------------------------------------------------------------------


    $(function () {


        var userId = $('#hdnUserId').val();
        var currentCampusId = $.QueryString["cmpid"];

        var cacheKey = "MaintenanceLinksHtml_" + userId + "_" + currentCampusId;
        var cachedHtml = storageCache.getFromLocalCache(cacheKey);

        if (!cachedHtml) common.ajaxSetup(viewModel);

        //setup error details window
        jQuery("#errorDetails").kendoWindow({
            width: '450px',
            height: '300px',
            modal: true,
            title: 'An Error has Occurred!',
            open: function () {
                kendo.bind(this.element, viewModel);
                this.element.data('kendoWindow').center();
            },
        });

        var win = $("#errorDetails").data("kendoWindow");
        common.errorWin(win);



        //----------------------------------------------------------
        // add the 'choose' item to each dropdown
        //----------------------------------------------------------
        jQuery('#maintenanceModulesDropDownList').kendoDropDownList({
            dataSource: [{ "Id": 0, "text": "-------View All-------" },
            { "Id": 1, "text": "Admissions" },
            { "Id": 2, "text": "Academics" },
            { "Id": 3, "text": "Student Accounts" },
            { "Id": 4, "text": "Financial Aid" },
            { "Id": 6, "text": "Placement" },
            { "Id": 6, "text": "Human Resources" },
            { "Id": 7, "text": "System" },
            { "Id": 8, "text": "Reports" }],
            dataTextField: 'text',
            dataValueField: 'Id',
            height: 500,
            value: "-------View All-------",
            change: function (e) {
                viewModel.filterPageGroups(this.value(), this.text());
            }
        });
        //----------------------------------------------------------


        //-------------------------------------------------------------
        // get the dropdown items
        //-------------------------------------------------------------
        datasources.maintenanceModulesDataSource.transport.options.read.data.campusId = currentCampusId;



        //bind the view to the view model
        kendo.bind(jQuery("#content"), viewModel);

        //retireve all maintenance items
        viewModel.getPageGroupsAndPages();



    });


})(window.$, window.kendo, window.datasources, window.common, window.maintenanceCommon, window.kendoControlSetup);