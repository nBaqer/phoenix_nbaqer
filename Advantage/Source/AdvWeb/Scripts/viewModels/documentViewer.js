﻿(function ($, kendo, datasources, common) {

    //----------------------------------------------------------------------------------------
    // View Model that represents the data to be bound to the 
    // view
    //----------------------------------------------------------------------------------------
    var viewModel = kendo.observable({
        //view data
        loading: false,
        studentNumber: "",
        studentFirstName: "",
        studentLastName: "",
        studentFullName: "",
        programVersions: datasources.programVersionDataSource,
        selectedProgramVersion: {},
        enrollmentStatuses: datasources.enrollmentStatusesDataSource,
        selectedEnrollmentStatus: {},
        documentStatuses: datasources.documentStatusesDataSource,
        selectedDocumentStatus: {},
        studentGroups: datasources.studentGroupsDataSource,
        selectedStudentGroup: {},
        startDate: null,
        documentSetLabel: null,
        
        //students grid data
        students: datasources.studentsDataSource,

        //studen document grid data
        studentDocuments: datasources.studentDocumentRequirementsDataSource,

        //document history grid data
        documentHistory: datasources.documentFilesDataSource,

        //document clicked in grid to modify
        selectedDocument: datasources.studentDocumentDataSource,

        //file uploaded
        selectedFile: {},

        //---------------------------------------------------------
        // View Model Methods
        //---------------------------------------------------------
        
        //search for a single student based on criteria entered
        searchStudentSingle: function() {
            if (!this.studentFirstName && !this.studentLastName && !this.studentNumber) {
                alert("Please enter at least one field to search on (Student Number, First Name, or Last Name)");
                return;
            }
            
            this.studentDocuments.data([]);
            

            this.students.transport.options.read.data.campusId = $.QueryString["cmpid"];
            this.students.transport.options.read.data.firstName = this.studentFirstName;
            this.students.transport.options.read.data.lastName = this.studentLastName;
            this.students.transport.options.read.data.studentNumber = this.studentNumber;
            this.students.transport.options.read.data.programVersionId = null;
            this.students.transport.options.read.data.enrollmentStatusId = null;
            this.students.transport.options.read.data.studentGroupId = null;
            this.students.transport.options.read.data.startDate = null;

            if (this.students.page() > 1)
                this.students.page(1);
            else {
                this.students.read();
            }
            
        },
        
        //search multiple students based on criteria entered
        searchStudentsMutliple: function () {
            this.studentDocuments.data([]);
            

            this.students.transport.options.read.data.campusId = $.QueryString["cmpid"];
            this.students.transport.options.read.data.firstName = null;
            this.students.transport.options.read.data.lastName = null;
            this.students.transport.options.read.data.studentNumber = null;
            this.students.transport.options.read.data.programVersionId = this.selectedProgramVersion.Id;
            this.students.transport.options.read.data.enrollmentStatusId = this.selectedEnrollmentStatus.Id;
            this.students.transport.options.read.data.studentGroupId = this.selectedStudentGroup.Id;
            this.students.transport.options.read.data.startDate = this.startDate != null ? formattedDate(this.startDate,true) : null;
            
            if (this.students.page() > 1)
                this.students.page(1);
            else {
                this.students.read();
            }
        },
        
        //load documents for the selected student
        loadDocs: function (studentId, fullName) {
            
            
            this.studentDocuments.transport.options.read.data.campusId = $.QueryString["cmpid"];
            this.studentDocuments.transport.options.read.data.userId = jQuery('#hdnUserId').val();
            this.studentDocuments.transport.options.read.data.studentId = studentId;
            this.studentDocuments.page(1);
            this.studentDocuments.read();

            this.set('studentFullName', "for " + fullName);
            
        },
        
        //retrieve the document selected and load edit window
        loadEditWindow: function (e) {
            var requirement = e.data;

            if (!requirement.ShowUpdate)
                return;
            
            var documentId = requirement.StudentDocumentId != null ? requirement.StudentDocumentId : undefined;

            var document = { Id: documentId, DocumentStatusId: requirement.DocumentStatusId, StudentId: requirement.StudentId, RequirementId: requirement.Id, DateRequested: formattedDate(requirement.DateRequested,false), DateReceived: formattedDate(requirement.DateReceived,false), Description: requirement.Description };

            this.selectedDocument.data([document]);

            //check if file is present.  if so, enable document upload.
            //If not, disable it
            if (typeof (document.Id) !== 'undefined') {
                this.enableDocumentUpload();
            } else {
                this.disableDocumentUpload();
            }
            
            
            $("#documentDetails").data("kendoWindow").open();
        },
        
        //user chose to remove document file from document history.  
        //confirm to the user then remove permanently from database
        removeDocument: function(e) {

            if (this.selectedDocument.view()[0].IsApproved == true) {
                alert("Removing a document is not allowed after document is approved.  Please unchceck Approved and click Set Document before removing a document file.");
                return;
            }

            if (confirm('Are you sure you want to remove this file?')) {
                var file = e.data;
                
                this.documentHistory.transport.options.destroy.data.fileId = file.Id;
                this.documentHistory.remove(file);
                this.documentHistory.sync();
            }
        },
        
        //enable the upload view once there is a document record present
        enableDocumentUpload: function () {
            var documentId = this.selectedDocument.data()[0].Id;

            var uri = '../Proxy/api/Students/Enrollments/Requirements/Documents/{documentId}/Files/Contents'.supplant({ documentId: documentId });
            initializeUploadify(uri);

            this.documentHistory.transport.options.read.data.studentDocumentId = documentId;
            this.documentHistory.read();
            
            jQuery('#documentUploadIsUnavailable').hide();
            jQuery('#documentUpload').show();
        },
        
        //disable document upload when there is no document request set yet
        disableDocumentUpload: function () {
            this.documentHistory.data([]);
            
            jQuery('#documentUploadIsUnavailable').show();
            jQuery('#documentUpload').hide();
        },
        
        //update the document record
        updateDocument: function () {

            //get the approval items from the status dropdown. Will later be used for validation
            var selDoc = this.selectedDocument;
            var approveItem = $.grep(datasources.documentStatusesDataSource.data(), function (n, i) {
                return n.Id == selDoc.view()[0].DocumentStatusId;
            });
            

            //make sure a document status has been chosen
            if (approveItem.length == 0) {
                alert('Please select a document status before clicking Set Document.');
                return;
            }

            //if user has chosen to approve this document, but no file has been upload, do not allow
            //the update
            if (approveItem[0].IsApproved == true && this.documentHistory.view().length == 0) {
                alert('You must upload at least one document before approving this document request.');
                return;
            }
            
            //make sure the dateRequested is not in the future
            if (validateDate(this.selectedDocument.view()[0].DateRequested) == false ) {
                alert('DateRequested is an invalid date.  Please make sure your dates are not in the future.');
                return;
            }
            
            //make sure the dateRequested is not in the future
            if (validateDate(this.selectedDocument.view()[0].DateReceived) == false) {
                alert('DateReceived is an invalid date.  Please make sure your dates are not in the future.');
                return;
            }
            
            //update the document
            this.selectedDocument.sync();
            viewModel.set("documentSetLabel", "Document request is set.");
        },
        
        // user chose to download file to computer
        downloadDocument: function (e) {
            var file = e.data;
            var fileId = file.Id;

            var uri = '../proxy/api/Students/Enrollments/Requirements/Documents/Files/{fileId}/Contents'.supplant({ fileId: fileId });
            window.location.href = uri;
        },
        
        // trigger uploadify to upload the document
        upload: function (e) {
            jQuery('#fileInput').uploadify('upload', '*');
        },
        
        //reset the controls on the view to their default values
        resetPage: function () {
            
            this.studentDocuments.data([]);
            this.students.data([]);
            
            this.set("selectedProgramVersion", {});
            this.set("selectedEnrollmentStatus", {});
            this.set("selectedStudentGroup", {});
            this.set("startDate", null);
            this.set("documentSetLable", null);
            this.set('studentFullName', null);
            
            
            window.document.forms[0].reset();
            
        }
    });
    //----------------------------------------------------------------------------------------
    

    //----------------------------------------------------------------------------------------
    // Initialize the uploadify control to allow for uploading files to the server
    //----------------------------------------------------------------------------------------
    var initializeUploadify = function (uri) {
        var allowedFileTypes = '*.gif; *.jpg; *.png; *.bmp; *.txt; *.doc; *.docx; *.csv; *.xls; *.xlsx; *.pdf';

        jQuery('#fileInput').uploadify({
            formData: { ModUser: USER_NAME },
            buttonClass: 'k-button',
            buttonText: 'Step 1: Choose File...',
            width: 300,
            multi: false,
            auto: false,
            swf: '../cm/uploadify.swf',
            uploader: uri,
            fileTypeExts: allowedFileTypes,
            queueSizeLimit: 1,
            fileSizeLimit: '10MB',
            overrideEvents: ['onSelectError', 'onDialogClose'],
            onUploadSuccess: function () {
                viewModel.documentHistory.read();
            },
            onSelectError: function (file, errorCode) {
                var errorDescription;
                switch (errorCode) {
                    case SWFUpload.QUEUE_ERROR.QUEUE_LIMIT_EXCEEDED:
                        errorDescription = "Only one file may be in the upload queue at any time";
                        break;
                    case SWFUpload.QUEUE_ERROR.FILE_EXCEEDS_SIZE_LIMIT:
                        errorDescription = "The file exceeds the maximum allowed size of 10MB";
                        break;
                    case SWFUpload.QUEUE_ERROR.ZERO_BYTE_FILE:
                        errorDescription = "The file is empty";
                        break;
                    case SWFUpload.QUEUE_ERROR.INVALID_FILETYPE:
                        errorDescription = "The file is of an unsupported type.\rAllowed types are:\r" + allowedFileTypes;
                        break;
                    default:
                        errorDescription = "Unknown error";
                }
                alert("The file " + file.name + " could not be added to the queue.\r\r" + errorDescription);

                return true;
            }
        });
    };

    //----------------------------------------------------------------------------------------
    //Bind the view model for the selected document
    //----------------------------------------------------------------------------------------
    viewModel.selectedDocument.bind('sync', function () {
        viewModel.studentDocuments.read();
        viewModel.enableDocumentUpload();
    });
    


    //----------------------------------------------------------------------------------------
    // Initilization of the view
    //----------------------------------------------------------------------------------------
    $(function () {

        //setup ajax for the view
        common.ajaxSetup(viewModel);

       

        //allow for using the enter key on the keyboard to submit
        jQuery('form').submit(function (e) {
            if (e.preventDefault) { e.preventDefault(); } else { eImgPreviewLink.returnValue = false; }
        });
        //jQuery('#findSingleStudentForm, #findMultipleStudentsForm').formSimulator();
       
        //setup error details window
        jQuery("#errorDetails").kendoWindow({
            width: '450px',
            height: '300px',
            modal: true,
            title: 'An Error has Occurred!',
            open: function () {
                kendo.bind(this.element, viewModel);
                this.element.data('kendoWindow').center();
            },
        });

        var win = $("#errorDetails").data("kendoWindow");
        common.errorWin(win);

        //setup document details window
        jQuery("#documentDetails").kendoWindow({
            width: '600px',
            height: '600px',
            modal: true,
            title: 'Edit/Upload Document',
            open: function () {
                kendo.bind(this.element, viewModel);
                this.element.data('kendoWindow').center();
            },
            deactivate: function () {
                var documentId = viewModel.selectedDocument.data()[0].Id;
                if (typeof(documentId) !== "undefined") {
                    jQuery('#fileInput').uploadify('destroy');
                }
                viewModel.set("documentSetLabel", null);
            },
            animation: {
                open: {
                    effects: "zoom:in",
                    duration: 500,
                },
                close: {
                    effects: "zoom:out",
                    duration: 500,
                }

            }
        });
        
        //setup kendo grid for students
        jQuery('#students').kendoGrid({
            groupable: false,
            scrollable: false,
            resizable: true,
            selectable: true,
            pageable: { pageSize: 10, page: 1, pageSizes:[5,10, 20] },
            filterable: false,
            autoBind: false,
            dataBound: function () {
                viewModel.studentDocuments.data([]);
            },
            columns: [
                { field: 'StudentId', title: 'Student Id', width: 1, hidden: true },
                { field: 'FullName', title: 'Student Name', width: 150 },
                { field: 'StudentNumber', title: 'Student Number', width: 50 },
                { field: 'SSN', title: 'SSN', width: 40 },
                { field: 'ProgramVersion', title: 'Program Version', width: 100 },
                { field: 'EnrollmentStatus', title: 'Enroll Status', width: 75 },
                { field: 'StartDate', title: 'Start Date', width: 30, format: '{0:dd/MM/yyyy}' }],
            change: function() {
                var selectedRows = this.select();
                var id = selectedRows[0].cells[0].innerText;
                if (!id) id = selectedRows[0].cells[0].textContent;
                var fullName = selectedRows[0].cells[1].innerText;
                viewModel.loadDocs(id, fullName);
            }
        });
        
        //setup kendo grid for student documents
        jQuery('#studentDocuments').kendoGrid({
            groupable: false,
            sortable: true,
            scrollable: false,
            resizable: true,
            selectable: true,
            pageable: { pageSize: 10, pageSizes: [5, 10, 20] },
            filterable: false,
            autoBind: false,
            columns: [
                { template: "<button style='width:150px;' class='k-button' data-bind='click: loadEditWindow' >#= formatUpdateValue(ShowUpdate)#</button>", title: "", width: 150 },
                { field: 'ShowUpdate', title: 'ShowUpdate', hidden: true, width: 1 },
                { field: 'StudentId', title: 'Student Id', width: 1, hidden: true },
                { field: 'DocumentId', title: 'document Id', width: 1, hidden: true },
                { field: 'Description', title: 'Description', width: 225 },
                { field: 'DocumentRequirementId', title: 'Request Exists?', width: 25, template: '#= DocumentRequirementId != null ? "Yes" : "No" #' },
                { field: 'DocumentStatusDescription', title: 'Document Status', width: 50, },
                { field: 'DateRequested', title: 'Requested', width: 25, template: '#= formattedDate(DateRequested,false) #' },
                { field: 'DateReceived', title: 'Received', width: 25, template: '#= formattedDate(DateReceived,false) #' },
                { field: 'Module', title: 'Module', width: 100 },
                { field: 'RequiredFor', title: 'Required For', width: 100 },
                { field: 'RequirementType', title: 'Requirement Type', width: 100 },
                { field: 'StartDate', title: 'Eff Start Date', width: 25, template: '#= formattedDate(StartDate,false) #' },
                { field: 'EndDate', title: 'Eff End Date', width: 25, template: '#= formattedDate(EndDate,false) #' }]
        });


        

        //setup kendo grid for document history
        jQuery('#documentHistory').kendoGrid({
            groupable: false,
            sortable: true,
            scrollable: false,
            pageable: { pageSize: 10, pageSizes: [5, 10, 20] },
            editable: false,
            filterable: false,
            autoBind: false,
            columns: [
                { template: "<button class='k-button' data-bind='click: downloadDocument' title='Download the document to your computer for viewing'>Download</button>", title: "", width: 80 },
                { template: "<button class='k-button' data-bind='click: removeDocument' title='Permanently delete the document from the system'>Remove</button>", title: "", width: 80 },
                { field: 'Id', title: 'Id', hidden: true },
                { field: 'FileName', title: 'File Name' },
                { field: 'DateCreated', title: 'Date Created', template: '#= formattedDate(DateCreated,false) #' }]
        });
        
        
        //set up kendo date picker  controls
        jQuery('#startDate, #dateRequested, #receivedDate').kendoDatePicker();
        
        
        
        //----------------------------------------------------------
        // add the 'choose' item to each dropdown
        //----------------------------------------------------------
        jQuery('#programVersionDropDownList,#enrollmentStatusDropDownList,#studentGroupsDropDownList,#documentStatusDropDownList').kendoDropDownList({
            dataTextField: 'Description',
            dataValueField: 'Id',
            optionLabel: {
                Description: "------------Select------------",
                id: null
            },
            value: "------------Select------------"
        });
        //----------------------------------------------------------
        

        
        //----------------------------------------------------------
        //Setup tabstrip and tooltips
        //----------------------------------------------------------
        window.kendoControlSetup.setupTabStrip("#tabstrip");
        window.kendoControlSetup.setupToolTip("#btnSearchSingle", "You can search for a single student by any combination<br> of Student Number, First Name, and Last Name. We will retrieve<br> any student that meet the criteria for this campus.", "top");
        window.kendoControlSetup.setupToolTip("#btnSingleReset", "Reset all controls to their default to start a new search.", "top");
        window.kendoControlSetup.setupToolTip("#btnMultipleReset", "Reset all controls to their default to start a new search.", "top");
        window.kendoControlSetup.setupToolTip("#btnSearchMultiple", "You can search for a list of students by any combination of Program Version,<br> Start Date, Enroll Status, and Student Group. We will retrieve<br> students that meet the criteria for this campus.", "top");
        window.kendoControlSetup.setupToolTip("#btnSetDocument", "Insert or edit the document request.<br> Please note that you cannot approve a <br> request until a document file is uploaded.", "bottom");
        window.kendoControlSetup.setupToolTip("#students", "Click student to view Document Requirements ");
        //----------------------------------------------------------
        
        
        //----------------------------------------------------------
        // Retrieve campusId from query string and feed it to the 
        // datasources that need it
        //----------------------------------------------------------
        var campusId = jQuery.QueryString['cmpId'];
        dataSourceCommon.setCampusForManagement(viewModel, campusId);
        //----------------------------------------------------------
        
        //bind the view to the view model
        kendo.bind(jQuery("#viewrContent"), viewModel);
        




        //var entityGrid = $("#studentDocuments").data("kendoGrid");
        
    });
    //----------------------------------------------------------------------------------------
    
})(window.$, window.kendo, window.datasources, window.common, window.kendoControlSetup);