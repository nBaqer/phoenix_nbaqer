﻿(function ($, kendo, datasources) {


    //----------------------------------------------------------------------------------------
    // View Model that represents the data to be bound to the 
    // view
    //----------------------------------------------------------------------------------------
    var viewModel = kendo.observable({
        studentMruDataSource: datasources.studentMruDataSource,
        leadMruDataSource: datasources.leadMruDataSource,
        employerMruDataSource: datasources.employerMruDataSource,
        employeeMruDataSource: datasources.employeeMruDataSource,

        //-------------------------------------------------------------------------------------------------------
        // initialize and open the filter options window
        //-------------------------------------------------------------------------------------------------------
        initMruWindow: function () {

            var x = $("#btnMru").offset().left;
            var y = $("#btnMru").offset().top;

            $("#mruOptions").kendoWindow({
                //animation: false,
                modal: false,
                width: "415",
                Height: "500",
                title: "Most Recently Used",
                animation: { open: { effects: "expand:vertical", duration: 500 }, close: { effects: "expand:vertical", reverse: true, duration: 500 } },
                actions: ["close"],
                resizable: false,
                draggable: false,
                open: function () {
                    kendo.bind(this.element, viewModel);
                    var dropdownlist = $("#entityDropDownList").data("kendoDropDownList");
                    var selectedEntity = dropdownlist.select();

                    $("#mruOptions").data("kendoWindow").title("Most Recently Used " + dropdownlist.text());
                }

            });


            $("#mruOptions").closest(".k-window").css({ top: y + 32, left: x });
            $("#mruOptions").data("kendoWindow").open();


        },
        saveModuleState: function (mouduleName) {
            storageCache.insertInSessionCache('currentModuleName', mouduleName);
            storageCache.removeFromSessionCache('currentSubLevelName');
        },
        saveSubLevelState: function (subLevelName) {
            storageCache.insertInSessionCache('currentSubLevelName', subLevelName);
        },
        mruSelectionClick: function (selectedItemId, selectedFullName, selectedCampusId) {
            
            if (selectedItemId) {
                $("#hdnCurrentEntityId").val(selectedItemId);
                $("#hdnCurrentEntityName").val(selectedFullName);

                var dropdownlist = $("#entityDropDownList").data("kendoDropDownList");

                var selectedIndex = dropdownlist.value() - 1;

                $("#hdnEntityType").val(selectedIndex);

                var resId = $.QueryString["resid"];

                if (modelCommon.isEntityResource(resId)) {

                    $("#hdnSearchCampusId").val(selectedCampusId);

                    switch (selectedIndex) {
                        case 0:
                            viewModel.saveModuleState("Academics");
                            viewModel.saveSubLevelState("Manage Students");
                            break;
                        case 1:
                            sessionStorage.setItem("NewLead", "false"); // Set the new lead flag to false to load the lead in info page
                            viewModel.saveModuleState("Admissions");
                            viewModel.saveSubLevelState("Leads");
                            break;
                        case 2:
                            viewModel.saveModuleState("Placement");
                            viewModel.saveSubLevelState("Employers");
                            break;
                        case 3:
                            viewModel.saveModuleState("Human Resources");
                            viewModel.saveSubLevelState("Employees");
                            break;
                        default:
                            viewModel.saveModuleState("Academics");
                            viewModel.saveSubLevelState("Manage Students");
                    }
                    if (resId === "268") {
                        viewModel.GetUrlAddress(selectedIndex, selectedCampusId, selectedItemId);
                    } else {
                        var droplist = $('#entityDropDownList').data("kendoDropDownList");
                        var selectedSearch = droplist.value() * 1;
                        if (resId === "542" && selectedSearch == 1) {
                            __doPostBack('mruChanged_and_redirect_542', selectedItemId);
                        } else {
                            __doPostBack('mruChanged', selectedItemId);
                        }
                       
                    }

                } else {
                    viewModel.GetUrlAddress(selectedIndex, selectedCampusId, selectedItemId);
                }
            }

        },
        //--------------------------
        // get the url address to redirect
        //--------------------------
        GetUrlAddress: function (selectedIndex, selectedCampusId, selectedItemId) {
   
            switch (selectedIndex) {
                case 0:
                    viewModel.saveModuleState("Academics");
                    viewModel.saveSubLevelState("Manage Students");
                    //location.href = XMASTER_GET_BASE_URL + "PL/StudentMaster.aspx?resid=203&mod=AR&cmpid=" + selectedCampusId + "&desc=View Student&entityType=0&entity=" + selectedItemId;
                    location.href = XMASTER_GET_BASE_URL + "PL/StudentMasterSummary.aspx?resid=868&mod=AR&cmpid=" + selectedCampusId + "&desc=View Student&entityType=0&entity=" + selectedItemId;
                    break;
                case 1:
                    viewModel.saveModuleState("Admissions");
                    viewModel.saveSubLevelState("Leads");
                    location.href = XMASTER_GET_BASE_URL + "AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + selectedCampusId + "&desc=View Lead&entityType=1&entity=" + selectedItemId;

                    break;
                case 2:
                    viewModel.saveModuleState("Placement");
                    viewModel.saveSubLevelState("Employers");

                    location.href = XMASTER_GET_BASE_URL + "/PL/EmployerInfo.aspx?resid=79&mod=PL&cmpid=" + selectedCampusId + "&desc=View Employer&entityType=2&entity=" + selectedItemId;

                    break;
                case 3:
                    viewModel.saveModuleState("Human Resources");
                    viewModel.saveSubLevelState("Employees");
                    location.href = XMASTER_GET_BASE_URL + "SY/EmployeeInfo.aspx?resid=52&mod=HR&cmpid=" + selectedCampusId + "&desc=View Employee&entityType=3&entity=" + selectedItemId;

                    break;
                default:
                    viewModel.saveModuleState("Academics");
                    viewModel.saveSubLevelState("Manage Students");
                    //location.href = XMASTER_GET_BASE_URL + "PL/StudentMaster.aspx?resid=203&mod=AR&cmpid=" + selectedCampusId + "&desc=View Student&entityType=0&entity=" + selectedItemId;
                    location.href = XMASTER_GET_BASE_URL + "PL/StudentMasterSummary.aspx?resid=868&mod=AR&cmpid=" + selectedCampusId + "&desc=View Student&entityType=0&entity=" + selectedItemId;
            }
        },
        getMruData: function (selectedIndex) {
            $("#mruHtmlContent").html("");
            var filter;
            var uri;
            var itemId;
            var fullName;
            var sCampusId;
            switch (selectedIndex) {
                case 0:
                    filter = { 'CampusId': XMASTER_GET_CURRENT_CAMPUS_ID, 'UserId': $("#hdnUserId").val() };
                    uri = XMASTER_GET_BASE_URL + "/proxy/api/StudentSearch/GetByMru";
                    var requestData = $.getJSON(uri, filter, function (data) {
                        $("#mruHtmlContent").html(viewModel.setStudentHtml(data));
                        $(".imageMruDiv,.contentMruDiv").bind("click", function () {
                            itemId = jQuery(this).parent().children().first().children().first()[0].id;
                            fullName = jQuery(this).parent().children().first().children()[1].attributes[1].value;
                            sCampusId = jQuery(this).parent().children().first().children()[2].id;

                            viewModel.mruSelectionClick(itemId, fullName, sCampusId);
                        });
                    });
                    break;
                case 1:
                    filter = { 'CampusId': XMASTER_GET_CURRENT_CAMPUS_ID, 'UserId': $("#hdnUserId").val() };
                    uri = XMASTER_GET_BASE_URL + "/proxy/api/LeadSearch/GetByMru";
                    var requestData2 = $.getJSON(uri, filter, function (data) {
                        $("#mruHtmlContent").html(viewModel.setLeadHtml(data));
                        $(".imageMruDiv,.contentMruDiv").bind("click", function () {
                            itemId = jQuery(this).parent().children().first().children().first()[0].id;
                            fullName = jQuery(this).parent().children().first().children()[1].attributes[1].value;
                            sCampusId = jQuery(this).parent().children().first().children()[2].id;
                            if (!sCampusId || sCampusId == "None" || sCampusId == "null")
                                sCampusId = XMASTER_GET_CURRENT_CAMPUS_ID;

                            viewModel.mruSelectionClick(itemId, fullName, sCampusId);
                        });
                    });
                    break;
                case 2:
                    filter = { 'CampusId': XMASTER_GET_CURRENT_CAMPUS_ID, 'UserId': $("#hdnUserId").val() };
                    uri = XMASTER_GET_BASE_URL + "/proxy/api/EmployerSearch/GetByMru";
                    var requestData3 = $.getJSON(uri, filter, function (data) {
                        $("#mruHtmlContent").html(viewModel.setEmployerHtml(data));
                        $(".imageMruDiv,.contentMruDiv").bind("click", function () {
                            itemId = jQuery(this).parent().children().first().children().first()[0].id;
                            fullName = jQuery(this).parent().children().first().children()[1].attributes[1].value;
                            sCampusId = jQuery(this).parent().children().first().children()[2].id;

                            viewModel.mruSelectionClick(itemId, fullName, sCampusId);
                        });
                    });

                    break;
                case 3:
                    filter = { 'CampusId': XMASTER_GET_CURRENT_CAMPUS_ID, 'UserId': $("#hdnUserId").val() };
                    uri = XMASTER_GET_BASE_URL + "/proxy/api/EmployeesSearch/GetByMru";
                    var requestData4 = $.getJSON(uri, filter, function (data) {
                        $("#mruHtmlContent").html(viewModel.setEmployeeHtml(data));
                        $(".imageMruDiv,.contentMruDiv").bind("click", function () {
                            itemId = jQuery(this).parent().children().first().children().first()[0].id;
                            fullName = jQuery(this).parent().children().first().children()[1].attributes[1].value;
                            sCampusId = jQuery(this).parent().children().first().children()[2].id;

                            viewModel.mruSelectionClick(itemId, fullName, sCampusId);
                        });
                    });

                    break;
                    
                default:
                    filter = { 'CampusId': XMASTER_GET_CURRENT_CAMPUS_ID, 'UserId': $("#hdnUserId").val() };
                    uri = XMASTER_GET_BASE_URL + "/proxy/api/StudentSearch/GetByMru";
                    var requestData5 = $.getJSON(uri, filter, function (data) {
                        $("#mruHtmlContent").html(viewModel.setStudentHtml(data));
                        $(".imageMruDiv,.contentMruDiv").bind("click", function () {
                            viewModel.mruSelectionClick(jQuery(this).parent().children().first().children().first()[0].id);
                        });

                    });
                    break;
            }


        },
        setStudentHtml: function (view) {

            var outputHtml = "";
            var notFound = XMASTER_GET_BASE_URL + '/images/face75_notfound.png';

            $.each(view, function (index, viewItem) {
                outputHtml += "";
                outputHtml += "<div class='mruContainer' >";
                //outputHtml += "<div class='imageMruDiv'><asp:HiddenField ClientIDMode='Static' id=" + viewItem.Id + " /><asp:HiddenField ClientIDMode='Static' value='" + viewItem.FullName + "' />" + "<asp:HiddenField ClientIDMode='Static' id=" + viewItem.SearchCampusId + " /><img class='faceImage' style='width:75px;height:75px;' src='" + viewItem.ImageUrl + "' onerror='this.src=\"" + notFound + "\"' /></div>";
                outputHtml += "<div class='imageMruDiv'><asp:HiddenField ClientIDMode='Static' id=" + viewItem.Id + " /><asp:HiddenField ClientIDMode='Static' value='" + viewItem.FullName + "' />" + "<asp:HiddenField ClientIDMode='Static' id=" + viewItem.SearchCampusId + " /><img class='faceImage' style='width:75px;height:75px;' src='" + viewItem.Image64 + "' /></div>";

                outputHtml += "<div class='contentMruDiv'>";
                outputHtml += "<h3>" + viewItem.FullName + "</h3>";
                outputHtml += "<p>";
                outputHtml += "<div class='div-table'>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left' style='width:85px;'>Status:</div>";
                outputHtml += "<div class='div-table-col' aign='left'>" + viewItem.Status + "</div>";
                outputHtml += "</div>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left' style='width:85px;'>Program Version:</div>";
                outputHtml += "<div class='div-table-col' aign='left'>" + viewItem.Program + "</div>";
                outputHtml += "</div>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left' style='width:85px;'>Campus:</div>";
                outputHtml += "<div class='div-table-col' aign='left'>" + viewItem.Campus + "</div>";
                outputHtml += "</div>";
                outputHtml += "</div>";
                outputHtml += "</p>";
                outputHtml += "</div>";
                outputHtml += "</div>";

            });
            return outputHtml;
        },
        setLeadHtml: function (view) {

            var outputHtml = "";
            var notFound = XMASTER_GET_BASE_URL + '/images/face75_notfound.png';

            $.each(view, function (index, viewItem) {

                outputHtml += "";
                outputHtml += "<div class='mruContainer' >";
               // outputHtml += "<div class='imageMruDiv'><asp:HiddenField ClientIDMode='Static' id=" + viewItem.Id + " /><asp:HiddenField ClientIDMode='Static' value='" + viewItem.FullName + "' />" + "<asp:HiddenField ClientIDMode='Static' id=" + viewItem.SearchCampusId + " /><img class='faceImage' style='width:75px;height:75px;' src='" + viewItem.ImageUrl + "' onerror='this.src=\"" + notFound + "\"' /></div>";
                outputHtml += "<div class='imageMruDiv'><asp:HiddenField ClientIDMode='Static' id=" + viewItem.Id + " /><asp:HiddenField ClientIDMode='Static' value='" + viewItem.FullName + "' />" + "<asp:HiddenField ClientIDMode='Static' id=" + viewItem.SearchCampusId + " /><img class='faceImage' style='width:75px;height:75px;' src='" + viewItem.Image64 + "' /></div>";
                outputHtml += "<div class='contentMruDiv'>";
                outputHtml += "<h3>" + viewItem.FullName + "</h3>";
                outputHtml += "<p>";
                outputHtml += "<div class='div-table'>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left' style='width:85px;'>Status:</div>";
                outputHtml += "<div class='div-table-col' aign='left'>" + viewItem.Status + "</div>";
                outputHtml += "</div>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left' style='width:85px;'>Program Version:</div>";
                outputHtml += "<div class='div-table-col' aign='left'>" + viewItem.Program + "</div>";
                outputHtml += "</div>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left' style='width:85px;'>Campus:</div>";
                outputHtml += "<div class='div-table-col' aign='left'>" + viewItem.Campus + "</div>";
                outputHtml += "</div>";
                outputHtml += "</div>";
                outputHtml += "</p>";
                outputHtml += "</div>";
                outputHtml += "</div>";

            });
            return outputHtml;
        },
        setEmployerHtml: function (view) {

            var outputHtml = "";
            var notFound = XMASTER_GET_BASE_URL + '/images/face75_notfound.png';

            $.each(view, function (index, viewItem) {

                outputHtml += "";
                outputHtml += "<div class='mruContainer' >";
                outputHtml += "<div class='imageMruDiv'><asp:HiddenField ClientIDMode='Static' id=" + viewItem.Id + " /><asp:HiddenField ClientIDMode='Static' value='" + viewItem.FullName + "' />" + "<asp:HiddenField ClientIDMode='Static' id=" + viewItem.SearchCampusId + " /><img class='faceImage' style='width:75px;height:75px;' src='" + viewItem.ImageUrl + "' onerror='this.src=\"" + notFound + "\"' /></div>";
                outputHtml += "<div class='contentMruDiv'>";
                outputHtml += "<h3>" + viewItem.EmployerName + "</h3>";
                outputHtml += "<p>";
                outputHtml += "<div class='div-table'>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left' >" + viewItem.Address + "</div>";
                outputHtml += "</div>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left'>" + viewItem.CityStateZip + "</div>";
                outputHtml += "</div>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left'>" + viewItem.Phone + "</div>";
                outputHtml += "</div>";
                outputHtml += "</div>";
                outputHtml += "</p>";
                outputHtml += "</div>";
                outputHtml += "</div>";



            });
            return outputHtml;
        },
        setEmployeeHtml: function (view) {

            var outputHtml = "";
            var notFound = XMASTER_GET_BASE_URL + '/images/face75_notfound.png';

            $.each(view, function (index, viewItem) {

                outputHtml += "";
                outputHtml += "<div class='mruContainer' >";
               // outputHtml += "<div class='imageMruDiv'><asp:HiddenField ClientIDMode='Static' id=" + viewItem.Id + " /><asp:HiddenField ClientIDMode='Static' value='" + viewItem.FullName + "' />" + "<asp:HiddenField ClientIDMode='Static' id=" + viewItem.SearchCampusId + " /><img class='faceImage' style='width:75px;height:75px;' src='" + viewItem.ImageUrl + "' onerror='this.src=\"" + notFound + "\"' /></div>";
                outputHtml += "<div class='imageMruDiv'><asp:HiddenField ClientIDMode='Static' id=" + viewItem.Id + " /><asp:HiddenField ClientIDMode='Static' value='" + viewItem.FullName + "' />" + "<asp:HiddenField ClientIDMode='Static' id=" + viewItem.SearchCampusId + " /><img class='faceImage' style='width:75px;height:75px;' src='" + viewItem.Image64 + "' /></div>";
                outputHtml += "<div class='contentMruDiv'>";
                outputHtml += "<h3>" + viewItem.FullName + "</h3>";
                outputHtml += "<p>";
                outputHtml += "<div class='div-table'>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left' style='width:85px;'>Status:</div>";
                outputHtml += "<div class='div-table-col' aign='left'>" + viewItem.Status + "</div>";
                outputHtml += "</div>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left' style='width:85px;'>Department:</div>";
                outputHtml += "<div class='div-table-col' aign='left'>" + viewItem.Department + "</div>";
                outputHtml += "</div>";
                outputHtml += "<div class='div-table-row'>";
                outputHtml += "<div class='div-table-col' aign='left' style='width:85px;'>Campus:</div>";
                outputHtml += "<div class='div-table-col' aign='left'>" + viewItem.Campus + "</div>";
                outputHtml += "</div>";
                outputHtml += "</div>";
                outputHtml += "</p>";
                outputHtml += "</div>";
                outputHtml += "</div>";

            });
            return outputHtml;
        },
        bindMruButton: function () {
            $("#btnMru").bind("click", function () {
                var dropdownlist = $("#entityDropDownList").data("kendoDropDownList");
                var selectedEntity = dropdownlist.value() - 1;
                viewModel.getMruData(selectedEntity);

                viewModel.initMruWindow();
            });

        },
        imageError: function (element) {
            element.src = XMASTER_GET_BASE_URL + '/images/face75_notfound.png';
        },
        bindWindowHide: function () {
            //-----------------------------------------------------------
            // If NavBar is visible and user clicks outside the control, 
            // hide it.
            //-----------------------------------------------------------
            $(document).click(function (event) {
                if ($(event.target).parents().index($('#mruOptions')) == -1) {

                    if ($(event.target).parents().context.id != "btnMru") {
                        if ($('#mruOptions').is(":visible")) {
                            $("#mruOptions").data("kendoWindow").close();
                        }
                    }

                }
            });
        }


    });


    $(function () {

        viewModel.bindMruButton();
        viewModel.bindWindowHide();

    });


})(window.$, window.kendo, window.datasources);


