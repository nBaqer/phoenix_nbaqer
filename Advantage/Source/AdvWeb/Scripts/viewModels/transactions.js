﻿(function ($, kendo, datasources) {



    //----------------------------------------------------------------------------------------
    // View Model that represents the data to be bound to the 
    // view
    //----------------------------------------------------------------------------------------
    var viewModel = kendo.observable({
        //----------------------------------------------
        //Obsvervable objects
        //----------------------------------------------
        transactionsDataSource: datasources.transactionDataSource,
        transactionsSummaryDataSource: datasources.transactionSummaryDataSource,
        projectionDataSource: datasources.projectionDataSource,
        projectionSummaryDataSource: datasources.projectionSummaryDataSource,
        windowFromDate: '',
        windowToDate: '',
        selectedIncrementType: "3",
        selectedThreshValue:'',
        //-----------------------------------------------------------
        // retrieve the base url 
        //-----------------------------------------------------------
        baseUrl: function() {
            return XMASTER_GET_BASE_URL;
        },
        //-----------------------------------------------------------
        // retrieve the campus id from the either the campus control,
        // the campus variable, or query string
        //-----------------------------------------------------------
        getCurrentCampusId: function() {

            var campusId = $("#MasterCampusDropDown").val();
            if (!campusId) campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
            if (!campusId) campusId = $.QueryString["cmpid"];

            return campusId;
            
        },
        //-----------------------------------------------------------
        // determine if the widget should be enabled depending on the 
        // roles for the user.  If not, disable the widget
        //-----------------------------------------------------------
        isWidgetEnabled: function() {

            var cmpId = viewModel.getCurrentCampusId();
            var userId = $('#hdnUserId').val();
            var filter = { 'CampusId': cmpId, 'UserId': userId };

            var uri = viewModel.baseUrl() + 'proxy/api/Students/Enrollments/IsPaymentPeriodWidgetEnabled';
            var requestData = $.getJSON(uri, filter, function(data) {
                if (data == false) {

                    viewModel.hideWidget();

                } else {

                    //initialize the chart control
                    viewModel.initChart();
                    viewModel.initProjectionsChart();
                    //viewModel.showWidget();
                }

            });

        

        },
        hideWidget: function (htmlText) {
            // get a reference to the TabStrip
            var tabstrip = $("#tabstrip").data("kendoTabStrip");
    
            //get the items
            var items = tabstrip.items();

            //disable the items
            tabstrip.disable(items);

            $('#toolbar').hide();
            $('#chart').hide();
            if (htmlText)
                $("#moreDetail").html(htmlText);
            

            $("#noAuth").css("font-weight", "bold");
            $("#noAuth").css("font-size", "12px;");
            $("#tabstrip").hide();
            $("#noAuth").show();
            $("#chartContent").css({ height: 350 });
            $("#transactionContainer").css({ height: 395 });
        },
        showWidget: function () {
            // get a reference to the TabStrip
            var tabstrip = $("#tabstrip").data("kendoTabStrip");

            $("#tabstrip").show();

            //get the items
            var items = tabstrip.items();

            //disable the items
            tabstrip.enable(items);
           

            $('#toolbar').show();
            $('#chart').show();
            $("#noAuth").hide();
            $("#moreDetail").html("Click a bar or details button to see more detail");
            $("#transactionContainer").removeAttr('style');
        },
        //----------------------------------------------
        // render initial view of Chart
        //----------------------------------------------
        initChart: function() {

            viewModel.transactionsSummaryDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            viewModel.transactionsSummaryDataSource.transport.options.read.data.FromDate = "";
            viewModel.transactionsSummaryDataSource.transport.options.read.data.ToDate = "";
            viewModel.transactionsSummaryDataSource.transport.options.read.data.SummaryType = "DAILY";
            viewModel.transactionsSummaryDataSource.read();

        },
        //----------------------------------------------
        // render initial view of Projection Chart
        //----------------------------------------------
        initProjectionsChart: function() {
            var threshValue = $("#valueNumText").data("kendoMaskedTextBox").value().replace(/_/g, '');

            viewModel.projectionSummaryDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            viewModel.projectionSummaryDataSource.transport.options.read.data.ThreshValue = threshValue;
            viewModel.projectionSummaryDataSource.transport.options.read.data.IncrementType = viewModel.selectedIncrementType;

            viewModel.projectionSummaryDataSource.read();

        },
        //------------------------------------------------
        // refresh the chart based on base unit and dates
        //------------------------------------------------
        refreshChart: function(id) {
            viewModel.transactionsSummaryDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            viewModel.transactionsSummaryDataSource.transport.options.read.data.FromDate = "";
            viewModel.transactionsSummaryDataSource.transport.options.read.data.ToDate = "";

            var units = "days";
            if (id == "1") {
                viewModel.transactionsSummaryDataSource.transport.options.read.data.SummaryType = "DAILY";
                units = "days";
            } else if (id == "2") {
                viewModel.transactionsSummaryDataSource.transport.options.read.data.SummaryType = "WEEKLY";
                units = "weeks";
            } else if (id == "3") {
                viewModel.transactionsSummaryDataSource.transport.options.read.data.SummaryType = "MONTHLY";
                units = "months";
            } else {
                viewModel.transactionsSummaryDataSource.transport.options.read.data.SummaryType = "DAILY";
                units = "days";
            }

            viewModel.transactionsSummaryDataSource.read();

            var chart = $("#chart").data("kendoChart"),
                series = chart.options.series,
                categoryAxis = chart.options.categoryAxis;

            for (var i = 0, length = series.length; i < length; i++) {
                series[i].aggregate = "sum";
            };

            categoryAxis.baseUnit = units;

            chart.refresh();


        },
        //------------------------------------------------
        // refresh the projections chart 
        //------------------------------------------------
        refreshProjectionsChart: function () {
            
            var threshValue = $("#valueNumText").data("kendoMaskedTextBox").value().replace(/_/g, '');

            viewModel.projectionSummaryDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;
            viewModel.projectionSummaryDataSource.transport.options.read.data.ThreshValue = threshValue;
            viewModel.projectionSummaryDataSource.transport.options.read.data.IncrementType = viewModel.selectedIncrementType;

            viewModel.projectionSummaryDataSource.read();

            var projectionsChart = $("#projectionChart").data("kendoChart");

            projectionsChart.refresh();

        },
        //------------------------------------------------
        // initialize the kendo tool bar
        //------------------------------------------------
        initializeToolbar: function() {
            $("#toolbar").kendoToolBar({
                resizable: true,
                items: [
                    {
                        type: "buttonGroup",
                        buttons: [
                            { id: 1, text: "Week", togglable: true, group: "chartType", selected: true },
                            { id: 2, text: "Month", togglable: true, group: "chartType" },
                            { id: 3, text: "6 Months", togglable: true, group: "chartType" }
                        ],
                        height:25
                    },
                    { template: "<input id='detailButton' style='width: 60px;height:25px;margin:0;' value='Details' type='button' />", overflow: "never" }
                ],
                toggle: function(e) {
                    if (e.group == "chartType") viewModel.refreshChart(e.id);
                }
            });


            $("#detailButton").kendoButton({
                click: function(e) {

                    var chart = $("#chart").data("kendoChart");
                    var data = chart.dataSource.data();

                    var fromDate = new Date(data[0].TransDate);
                    var toDate = new Date(data[data.length - 1].TransDate);
                    viewModel.openDetailsWindow(e, fromDate, toDate);
                }
            });
        },
        //------------------------------------------------
        // initialize the kendo tool bar
        //------------------------------------------------
        initializeProjectionsToolbar: function() {
            $("#projectionToolbar").kendoToolBar({
                items:
                [
                    { template: "<input id='typeDropDown' style='width: 125px;height:27px;margin:0' />", overflow: "never" },
                    {template: "<input id='valueNumText' style='width: 45px;height:27px;margin:0' />", overflow: "never" },
                    { template: "<input id='detailProjButton' class='k-button' style='width: 60px;height:27px;margin:0;' value='Details' type='button' />", overflow: "never" }
                ],
                toggle: function(e) {
                    if (e.group == "chartType") viewModel.refreshChart(e.id);
                }
            });

            $("#typeDropDown").kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                value: "3",
                dataSource: [
                    { text: "Credits Attempted", value: 2 },
                    { text: "Credits Earned", value: 3 },
                    { text: "Scheduled Hours", value: 0 },
                    { text: "Actual Hours", value: 1 }
                ],
                change: function(e) {
                    var value = this.value();
                    viewModel.set("selectedIncrementType", value);
                    viewModel.refreshProjectionsChart();
                }
            });

            $("#valueNumText").kendoMaskedTextBox({
                mask: "0000",
                value: "10",
                
                change: function (e) {
                    e.preventDefault();
                    viewModel.refreshProjectionsChart();
                }
            });

            $('#valueNumText').keypress(function(event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                }
                return true;

            });
           

            $("#detailProjButton").kendoButton({
                click: function(e) {
                    viewModel.openProjectionWindow(e);
                }
            });
            
        },
        //------------------------------------------------
        // initialize the kendo chart control
        //------------------------------------------------
        initializeChart: function() {

            var rend = "SVG";
            var ieVer = viewModel.GetIEVersion();
            if (ieVer > 0 && ieVer < 11)
                rend = "canvas";


            $("#chart").kendoChart({
                dataSource: viewModel.transactionsSummaryDataSource,
                renderAs: rend,
                title: {
                    text: "Payment Period Charges"
                },
                seriesColors: ["#355B99"],
                series: [
                    {
                        type: "column",
                        aggregate: "sum",
                        field: "ChargeAmountSum",
                        categoryField: "TransDate"
                    }
                ],
                seriesClick: function (e) {
                    viewModel.openDetailsWindow(e);
                },
                categoryAxis: {
                    baseUnit: "days",
                    majorGridLines: { visible: true },
                },
                valueAxis: [
                    {
                        labels: {
                            template: kendo.template("#: kendo.toString(value, 'c') #")

                        }
                    }
                ],
                tooltip: {
                    visible: true,
                    template: "Charges: #= kendo.toString(value, 'c') #",
                    color: "white"
                }
            });
        },
        //------------------------------------------------
        // initialize the kendo chart control
        //------------------------------------------------
        initializeProjectionsChart: function () {

            $("#projectionChart").kendoChart({
                dataSource: viewModel.projectionDataSource,
                title: {
                    text: "Payment Period Projections"
                },
                seriesDefaults: {
                    type: "column"
                    
                },
                series: [
                    {
                        stack: true,
                        type: "column",
                        field: "Amount",
                        name: "Amount",
                        color: "#355b99"
                        
                    },
                    {
                        stack: true,
                        type: "column",
                        field: "CumulativeDifference",
                        name: "Cumulative Amount",
                        color: "#94CDD2"
                       
                    }
                ],
                seriesClick: function (e) {
                    viewModel.openProjectionWindow(e);
                },
                categoryAxis: {
                    field: "CreditsHoursLeft",
                    name: "CreditsHoursLeft",
                    crosshair: {
                        visible: true
                    }
                },
                valueAxis: {
                    labels: {
                        template: kendo.template("#: kendo.toString(value, 'c') #")
                        //template: "$#= kendo.format('{0:N0}', value / 1000) # K"
                    }
                },
                tooltip: {
                    visible: true,
                    // template: "#console.log('data', data);#",
                    template: "#= category #<br>Amount: #= kendo.format('{0:C}',value) #<br>Cumulative Amount: #= kendo.format('{0:C}',dataItem.CumulativeAmount) # ",
                    color: "white"
                }
            });
        },
        //------------------------------------------------
        // Open the Details window using the correct dates
        //------------------------------------------------
        openDetailsWindow: function (e, from, to) {
            if (e.originalEvent) {
                if (e.originalEvent.type === "contextmenu") e.originalEvent.preventDefault();
            } else {
                e.preventDefault();
            }

            var date = new Date();

            if (from)
                date = new Date(from);
            else if (e.category)
                date = new Date(e.category);

            
            var properlyFormattedFrom = ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();
            viewModel.set("windowFromDate", properlyFormattedFrom);

            var properlyFormattedTo = "";

            

            var bUnit = $("#chart").data("kendoChart").options.categoryAxis.baseUnit;
            if (bUnit == "days") {
                if (to) {
                    date = new Date(to);
                    properlyFormattedTo = ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();
                    viewModel.set("windowToDate", properlyFormattedTo);
                } else {
                    viewModel.set("windowToDate", properlyFormattedFrom);
                }
            }
            else if (bUnit == "weeks") {
                if (to) date = new Date(to);
                else date.setTime(date.getTime() + 7 * 86400000);
                properlyFormattedTo = ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();
                viewModel.set("windowToDate", properlyFormattedTo);
            } else if (bUnit == "months") {
                if (to) date = new Date(to);
                else date.setTime(date.getTime() + 30 * 86400000);
                properlyFormattedTo = ("0" + (date.getMonth() + 1)).slice(-2) + "/" + ("0" + date.getDate()).slice(-2) + "/" + date.getFullYear();
                viewModel.set("windowToDate", properlyFormattedTo);
            }

            jQuery("#trDetails").kendoWindow({
                actions: ['Close'],
                title: "Payment Periods",
                modal: true,
                draggable: true,
                resizable: false,
                
                width: 750,
                height: 510,
                open: function () {
                    kendo.bind(this.element, viewModel);
                    $("#trDetails").data('kendoWindow').center();
                    this.wrapper.css({ zIndex: 0 });

                    viewModel.searchTransactions();
                },
                close: function() {
                    //$("#chart").data("kendoChart").redraw();
                    //$("#projectionChart").data("kendoCddlIncrementTypehart").redraw();
                }

            });

            $("#trDetails").data("kendoWindow").open();
            viewModel.initializeStartDatePicker();
            viewModel.initializeEndDatePicker();

            var startDate = $("#startDatePicker").data("kendoDatePicker");
            var endDate = $("#endDatePicker").data("kendoDatePicker");

            startDate.value(properlyFormattedFrom);

            if (properlyFormattedTo) endDate.value(properlyFormattedTo);
            else endDate.value(properlyFormattedFrom);

            viewModel.initializeGrid();

        },
        //------------------------------------------------
        // Open the projections Details window using the correct dates
        //------------------------------------------------
        openProjectionWindow: function (e) {

            if (e.originalEvent) {
                if (e.originalEvent.type === "contextmenu") e.originalEvent.preventDefault();
            } else {
                e.preventDefault();
            } 

            var thValue = $("#valueNumText").data("kendoMaskedTextBox").value();
            if (e.category) thValue = e.category;

            jQuery("#prDetails").kendoWindow({
                actions: ['Close'],
                title: "Payment Period Projections",
                modal: true,
                draggable: true,
                resizable: false,
                width: 820,
                height: 540,
                open: function () {
                    kendo.bind(this.element, viewModel);
                    $("#prDetails").data('kendoWindow').center();

                    $("#threshValue").data("kendoNumericTextBox").value(thValue);

                    $("#ddlIncrementType").data("kendoDropDownList").value(viewModel.selectedIncrementType);

                    

                    viewModel.searchProjections();

                    var val = viewModel.selectedIncrementType;

                    if (val == 0 || val == 1) $("#projectionGrid th[data-field=CreditsHoursValue]").html("Hours");
                    else $("#projectionGrid th[data-field=CreditsHoursValue]").html("Credits");
                },
                close: function() {
                    //$("#chart").data("kendoChart").redraw();
                    //$("#projectionChart").data("kendoChart").redraw();
                    
                }

            });

            viewModel.initializeThreshValueTextBox();
            viewModel.initializeProjectionIncrementTypeDropDown();
            viewModel.initializeProjectionsGrid();

            $("#prDetails").data("kendoWindow").open();
        },
        //------------------------------------------------
        // validate the threshhold value and show an error 
        // if necessary
        //------------------------------------------------
        validateThreshValue: function (threshValue) {
            // Tab Projections
            if (threshValue < 0.0 || threshValue > 5000) {
                alert('Value exeded 5000.');
                threshValue.val(0.0);
                return false;
            } else {
                return true;
            }

        },
        //------------------------------------------------
        // initialize the threshhold value textbox
        //------------------------------------------------
        initializeThreshValueTextBox: function () {

            jQuery('#threshValue').kendoNumericTextBox({
                change: function () {
                    if (!viewModel.validateThreshValue(this.value())) return;
                    viewModel.set("selectedThreshValue", this.value());
                    viewModel.searchProjections();
                },
                format: "#.00",
                decimals: 2,
                min: 0.0,
                value: 0.0,
            });
        },
        //------------------------------------------------
        // initialize the projection increment dropdown
        //------------------------------------------------
        initializeProjectionIncrementTypeDropDown: function () {

            // setup incrementTypeId, and IncrmentTypeText
            jQuery('#ddlIncrementType').kendoDropDownList({
                dataTextField: "text",
                dataValueField: "value",
                dataSource: [
                     { text: "Credits Attempted", value: 2 },
                    { text: "Credits Earned", value: 3 },
                    { text: "Scheduled Hours", value: 0 },
                    { text: "Actual Hours", value: 1 }
                
                ],
                minLength: 5,
                change: function (e) {
                    viewModel.set("selectedIncrementType", this.value());
                    viewModel.searchProjections();

                    var val = viewModel.selectedIncrementType;

                    if (val == 0 || val == 1) $("#projectionGrid th[data-field=CreditsHoursValue]").html("Hours");
                    else $("#projectionGrid th[data-field=CreditsHoursValue]").html("Credits");
                    
                },

            });
           
        },
        //------------------------------------------------
        // initialize the kendo grid control
        //------------------------------------------------
        initializeGrid: function () {
            //setup kendo grid for log
            jQuery('#transactionGrid').kendoGrid({
                theme: "blueopal",
                groupable: false,
                scrollable: true,
                resizable: true,
                pageable: true,
                sortable: true,
                autoBind: true,
                height: 400,
                columns: [
                    { field: 'FullName', title: 'Student Name', width: 200 },
                    { field: 'StudentNumber', title: 'Student ID', width: 150 },
                    { field: 'PeriodNumber', title: 'Period', width: 80 },
                    { field: 'TranDate', title: 'Date Posted', width: 120, format: '{0:dd/MM/yyyy}' },
                    { field: 'ChargeAmount', title: 'Amount', width: 120, template: '<span style="float:right">#= kendo.toString(ChargeAmount, "c") #</span>', headerAttributes: { style: "text-align: right" } }
                ]
                

            });
        },
        //------------------------------------------------
        // initialize the projection kendo grid control
        //------------------------------------------------
        initializeProjectionsGrid: function () {

            //setup kendo grid for log
            jQuery('#projectionGrid').kendoGrid({

                scrollable: true,
                resizable: true,
                filterable: false,
                pageable: true,
                sortable: true,
                autoBind: true,
                height: 400,
                columns: [
                    { field: 'FullName', title: 'Student Name', width: 180 },
                    { field: 'ProgramVersionDescription', title: 'Program Version', width: 140 },
                    { field: 'CampusDescription', title: 'Campus', width: 130 },
                    { field: 'CreditsHoursValue', title: 'Credit/Hours', width: 85, format: "{0:0.00}", attributes: { style: "text-align:right;" }, headerAttributes: { style: "text-align: right" } },
                    { field: 'ChargeAmount', title: 'Amount', width: 95, template: '<span style="float:right">#= kendo.toString(ChargeAmount, "c") #</span>', headerAttributes: { style: "text-align: right" } }
                    //,   
                    //{   , footerTemplate: "Total Price: #= kendo.toString(sum, 'C') #"       ,  aggregates: ["sum"], footerTemplate: "tot :  #sum#" },
                    //    field: 'ChargeAmount', title: 'Charge  Amount', width: 85, format: "{0:c}", attributes: { style: "text-align:right;" },
                    //    aggregates: ["sum"], footerTemplate: "tot :  #= sum#",
                    //}
                ]
            });

           
        },
        //------------------------------------------------
        // initialize the startdate picker
        //------------------------------------------------
        initializeStartDatePicker: function() {
            $('#startDatePicker').kendoDatePicker({
                change: function () {
                    if (!viewModel.checkDates()) return;
                    viewModel.searchTransactions();
                },
                value: new Date(),
                format: "MM/dd/yyyy",
                width:150
            });

            $("#startDatePicker").closest("span.k-datepicker").width(125);
            
        },
        //------------------------------------------------
        // initialize the end date picker
        //------------------------------------------------
        initializeEndDatePicker: function() {
            $('#endDatePicker').kendoDatePicker({
                change: function () {
                    if (viewModel.checkDates()) return;
                    viewModel.searchTransactions();
                },
                value: new Date(),
                format: "MM/dd/yyyy",
                width: 150
            });
            $("#endDatePicker").closest("span.k-datepicker").width(125);
        },
        //------------------------------------------------
        // validate the datepicker ranges
        //------------------------------------------------
        checkDates: function () {
            var startDate = $("#startDatePicker").data("kendoDatePicker").value();
            var endDate = $("#endDatePicker").data("kendoDatePicker").value();
            if (startDate != '' && endDate != '') {
                if (Date.parse(startDate.val()) > Date.parse(endDate.val())) {
                    alert('End date should be after start date');
                    endDate.val(startDate.val());
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        },
        //------------------------------------------------
        // Search for transactions 
        //------------------------------------------------
        searchTransactions: function () {

            viewModel.transactionsDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;

            if (viewModel.windowFromDate) {

                $("#startDatePicker").data("kendoDatePicker").value(new Date(viewModel.windowFromDate));
                $("#endDatePicker").data("kendoDatePicker").value(new Date(viewModel.windowToDate));

                viewModel.transactionsDataSource.transport.options.read.data.FromDate = viewModel.windowFromDate;
                viewModel.transactionsDataSource.transport.options.read.data.ToDate = viewModel.windowToDate;

                viewModel.set("windowFromDate", "");
                viewModel.set("windowToDate", "");

            } else {
                viewModel.transactionsDataSource.transport.options.read.data.FromDate = $('#startDatePicker').val();
                viewModel.transactionsDataSource.transport.options.read.data.ToDate = $('#endDatePicker').val();
            }

            viewModel.transactionsDataSource.read();

        },
        //------------------------------------------------
        // Search for projected transactions 
        //------------------------------------------------
        searchProjections: function () {
            //$("#projectionGrid th[data-field=CreditsHoursValue]").html(viewModel.selectedIncrementType.text);
            viewModel.projectionDataSource.transport.options.read.data.baseUrl = XMASTER_GET_BASE_URL;

            viewModel.projectionDataSource.transport.options.read.data.ThreshValue = $('#threshValue').val();

            viewModel.projectionDataSource.transport.options.read.data.IncrementType = viewModel.selectedIncrementType;
            viewModel.projectionDataSource.read();
        },

        //------------------------------------------------
        // export to excel
        //------------------------------------------------
        exportExcel: function () {

            var grid = $("#transactionGrid").data("kendoGrid");

            var data = grid.dataSource.data();

            var result = "";

            result += "<table><tr><th>Student&#160; Name</th><th>Student&#160; Id</th><th>Period</th><th>Posted&#160; Date</th><th>Amount</th></tr>";

            for (var i = 0; i < data.length; i++) {
                result += "<tr>";

                result += "<td>";
                result += data[i].FullName;
                result += "</td>";

                result += "<td>";
                result += data[i].StudentNumber;
                result += "</td>";

                result += "<td>";
                result += data[i].PeriodNumber;
                result += "</td>";

                result += "<td>";
                result += data[i].TranDate;
                result += "</td>";

                result += "<td>";
                result += '$' + data[i].ChargeAmount.formatMoney(2, '.', ',');
                result += "</td>";

                result += "</tr>";
            }

            result += "</table>";

            var blob = new Blob([result], { type: 'application/vnd.ms-excel' }); //Blob.js
            saveAs(blob, 'PaymentPeriodData.xls'); //FileSaver.js


           // console.log(result);
            
        },
        //------------------------------------------------
        // call function to export projected transactions
        //------------------------------------------------
        saveGridCsvProjections: function () {
            viewModel.exportExcelProjections('projectionGrid', 'testDataExcel.xls');
        },
        //------------------------------------------------
        // export to excel
        //------------------------------------------------
        exportExcelProjections: function (gridId, fileName) {

            var grid = $("#" + gridId).data("kendoGrid");

            var data = grid.dataSource.data();

            var result = "";

            result += "<table><tr><th>Student&#160;Name</th><th>Program&#160;Version</th><th>Campus</th><th>Credit/Hours&#160;Value</th><th>Charge&#160;Amount</th></tr>";

            for (var i = 0; i < data.length; i++) {
                result += "<tr>";

                result += "<td>";
                result += data[i].FullName;
                result += "</td>";

                result += "<td>";
                result += data[i].ProgramVersionDescription;
                result += "</td>";

                result += "<td>";
                result += data[i].CampusDescription;
                result += "</td>";

                result += "<td>";
                result += data[i].CreditsHoursValue;
                result += "</td>";

                result += "<td>";
                result += '$' + data[i].ChargeAmount.formatMoney(2, '.', ',');
                result += "</td>";

                result += "</tr>";
            }

            result += "</table>";
            
            var blob = new Blob([result], { type: 'application/vnd.ms-excel' }); //Blob.js
            saveAs(blob, 'PaymentPeriodData.xls'); //FileSaver.js
            

        },
        
        GetIEVersion: function() {
            var sAgent = window.navigator.userAgent;
            var Idx = sAgent.indexOf("MSIE");

            // If IE, return version number.
            if (Idx > 0)
                return parseInt(sAgent.substring(Idx + 5, sAgent.indexOf(".", Idx)));

                // If IE 11 then look for Updated user agent string.
            else if (!!navigator.userAgent.match(/Trident\/7\./))
                return 11;

            else
                return 0; //It is not IE
        }

    });



    $(function () {

        //setup ajax for the page
        common.ajaxSetup(viewModel);

        //set up tabstrop and tooltip   
        window.kendoControlSetup.setupTabStrip("#tabstrip");

        //set up the toolbarnot
        viewModel.initializeToolbar();
        viewModel.initializeProjectionsToolbar();

        //set up the chart control
        viewModel.initializeChart();
        viewModel.initializeProjectionsChart();

        //bind the view Model
        kendo.init($("#transactionContainer"));
        kendo.bind($("#transactionContainer"), viewModel);

        
        viewModel.hideWidget("<img src='kendo/styles/blueopal/loading-image.gif'>");


        //check if widget is enabled for this user
        viewModel.isWidgetEnabled();

    });


})(window.$, window.kendo, window.datasources, window.kendoControlSetup);