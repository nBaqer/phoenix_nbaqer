var Api;
(function (Api) {
    var ViewModels;
    (function (ViewModels) {
        var Tools;
        (function (Tools) {
            var GradDateCalculator = (function () {
                function GradDateCalculator() {
                    this.gradDateCalculatorVm = new Tools.GradDateCalculatorVm();
                }
                GradDateCalculator.prototype.initialize = function () {
                    this.gradDateCalculatorVm.initialize();
                };
                return GradDateCalculator;
            }());
            Tools.GradDateCalculator = GradDateCalculator;
        })(Tools = ViewModels.Tools || (ViewModels.Tools = {}));
    })(ViewModels = Api.ViewModels || (Api.ViewModels = {}));
})(Api || (Api = {}));
var Api;
(function (Api) {
    var Holiday = (function () {
        function Holiday() {
        }
        Holiday.prototype.GetHolidayListByCampus = function (campusId, onResponseCallback) {
            var request = new Api.Request();
            var campus = { campusId: campusId };
            request.send(Api.RequestType.Get, "v1/SystemCatalog/Holiday/GetHolidayListByCampus", campus, Api.RequestParameterType.QueryString, onResponseCallback);
        };
        return Holiday;
    }());
    Api.Holiday = Holiday;
})(Api || (Api = {}));
var Api;
(function (Api) {
    var Campus = (function () {
        function Campus() {
        }
        Campus.prototype.getDataSource = function (onRequestFilterData) {
            return this.getAllCampuses(onRequestFilterData);
        };
        Campus.prototype.getAllCampuses = function (onRequestFilterData) {
            var request = new Api.Request();
            return request.getRequestAsKendoDataSource("v1/SystemCatalog/Campus/GetAllCampusesByUserId", onRequestFilterData);
        };
        Campus.prototype.fetchCampusInformation = function (campusId, onResponseCallback) {
            var campus = { campusId: campusId };
            var request = new Api.Request();
            request.send(Api.RequestType.Get, "v1/SystemCatalog/School/GetDetailJson", campus, Api.RequestParameterType.QueryString, onResponseCallback);
        };
        return Campus;
    }());
    Api.Campus = Campus;
})(Api || (Api = {}));
var Api;
(function (Api) {
    var FinancialAid;
    (function (FinancialAid) {
        var ProgramVersion = (function () {
            function ProgramVersion(dataProvider) {
                this.getProgramVersionsByCampus = function (onRequestFilterData) {
                    var request = new Api.Request();
                    return request.getRequestAsKendoDataSource("v1/AcademicRecords/ProgramVersions/GetTitleIVProgramVersionsByCampus", onRequestFilterData);
                };
                //This API is used to fetch the program version total hours.
                this.getProgramVersionTotalHours = function (programVersionId, onResponseCallback) {
                    var programVersion = { programVersionId: programVersionId };
                    var request = new Api.Request();
                    request.send(Api.RequestType.Post, "v1/AcademicRecords/ProgramVersions/GetProgramVersionTotalHours", programVersion, Api.RequestParameterType.QueryString, onResponseCallback);
                };
                //This API is used to fetch the program version schedule details for the given enrollment Id.
                this.getScheduleDetailsByProgramVersionId = function (onRequestFilterData) {
                    var request = new Api.Request();
                    return request.getRequestAsKendoDataSource("v1/AcademicRecords/ProgramVersions/GetScheduleDetailsByProgramVersionId", onRequestFilterData);
                };
            }
            ProgramVersion.prototype.getDataSource = function (onRequestFilterData, group, functionName) {
                if (functionName) {
                    if (this[functionName]) {
                        return this[functionName](onRequestFilterData);
                    }
                }
                return this.getProgramVersionsByCampus(onRequestFilterData);
            };
            return ProgramVersion;
        }());
        FinancialAid.ProgramVersion = ProgramVersion;
    })(FinancialAid = Api.FinancialAid || (Api.FinancialAid = {}));
})(Api || (Api = {}));
/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" /> 
/// <reference path="../../../AdvWeb/Kendo/typescript/jquery.d.ts" />
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
var Api;
(function (Api) {
    var Components;
    (function (Components) {
        var DropDownList = (function () {
            function DropDownList(element, dataProvider, filterParams, settings, onchange, dataProviderFunctionName, selectValue) {
                /**
                 * Declaring the UserType API object. The UserType API encapsulate the User Type Controller.
                 * We will use the UserType API object to get the kendo data source for the UserType dropdown.
                 */
                var that = this;
                /**
                 * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
                 * jquery reference to that DOM object the kendo UI control wil be created.
                 */
                if (typeof element === 'string') {
                    that.componentId = "#" + element;
                    that.element = $(that.componentId);
                }
                else {
                    that.element = element;
                }
                /**
                 * storing the component id in the global variable scope so it can be referenced elsewhere
                 */
                that.dataProvider = dataProvider;
                that.filterParams = filterParams;
                that.hasCustomFunctionName = dataProviderFunctionName !== undefined;
                if (that.hasCustomFunctionName) {
                    that.dataProviderFunctionName = dataProviderFunctionName;
                }
                var defaults = {
                    dataSource: dataProvider.getDataSource(function () { return that.onRequestFilterData(); }, null, that.hasCustomFunctionName ? dataProviderFunctionName : null),
                    optionLabel: "Select",
                    dataTextField: "text",
                    dataValueField: "value",
                };
                if (settings) {
                    $.extend(defaults, settings.settings);
                }
                /**
                 * declaration of the kendo ui control.
                 */
                that.element.kendoDropDownList(defaults).change(function (value) {
                    if (value)
                        that.selectedId = that.element.data("kendoDropDownList").value();
                    if (onchange)
                        onchange(value);
                });
                that.dropDownList = that.element.data("kendoDropDownList");
                that.dropDownList.value(selectValue);
            }
            DropDownList.prototype.onRequestFilterData = function () {
                var filter = {};
                if (this.filterParams) {
                    filter = $.extend(filter, this.filterParams.params);
                }
                return filter;
            };
            DropDownList.prototype.setParameters = function (filterParams) {
                this.filterParams = filterParams;
            };
            DropDownList.prototype.getSelected = function () {
                return this.selectedId;
            };
            DropDownList.prototype.fetch = function (func) {
                return this.dropDownList.dataSource.fetch(func);
            };
            DropDownList.prototype.reset = function () {
                this.dropDownList.value("");
                this.dropDownList.trigger("change");
            };
            DropDownList.prototype.reload = function (filterParams) {
                var _this = this;
                if (filterParams) {
                    this.setParameters(filterParams);
                }
                this.dropDownList.setDataSource(this.dataProvider.getDataSource(function () { return _this.onRequestFilterData(); }, null, this.hasCustomFunctionName ? this.dataProviderFunctionName : null));
            };
            DropDownList.prototype.hasValue = function () {
                return this.dropDownList.value() !== "";
            };
            DropDownList.prototype.getValue = function () {
                return this.dropDownList.value();
            };
            DropDownList.prototype.getText = function () {
                return this.dropDownList.text();
            };
            DropDownList.prototype.setReadOnly = function (val) {
                this.dropDownList.readonly(val);
            };
            DropDownList.prototype.getData = function () {
                return this.dropDownList.dataSource.data();
            };
            DropDownList.prototype.setDataSource = function (dataSource) {
                this.dropDownList.setDataSource(dataSource);
            };
            DropDownList.prototype.setValue = function (newValue) {
                this.dropDownList.value(newValue);
                this.dropDownList.trigger("change");
            };
            DropDownList.prototype.getKendoDropDown = function () {
                return this.dropDownList;
            };
            DropDownList.prototype.show = function () {
                $(this.dropDownList.element).closest(".k-widget").show();
            };
            DropDownList.prototype.hide = function () {
                $(this.dropDownList.element).closest(".k-widget").hide();
            };
            return DropDownList;
        }());
        Components.DropDownList = DropDownList;
    })(Components = Api.Components || (Api.Components = {}));
})(Api || (Api = {}));
var Api;
(function (Api) {
    var Tools;
    (function (Tools) {
        var GradDateCalculator = (function () {
            function GradDateCalculator() {
                //This API is used to fetch the program version total hours.
                this.calculateGraduationDate = function (data, onResponseCallback) {
                    var request = new Api.Request();
                    request.send(Api.RequestType.Post, "v1/Common/GraduationCalculator/CalculateContractedGraduationDate", data, Api.RequestParameterType.Body, onResponseCallback);
                };
            }
            return GradDateCalculator;
        }());
        Tools.GradDateCalculator = GradDateCalculator;
    })(Tools = Api.Tools || (Api.Tools = {}));
})(Api || (Api = {}));
/// <reference path="../../API/SystemCatalog/Holiday.ts" />
/// <reference path="../../API/SystemCatalog/Campus.ts" />
/// <reference path="../../API/FinancialAid/ProgramVersion.ts" />
/// <reference path="../../Components/Common/DropDownList.ts" />
/// <reference path="../../API/Tools/GradDateCalculator.ts" />
var Api;
(function (Api) {
    var ViewModels;
    (function (ViewModels) {
        var Tools;
        (function (Tools) {
            var DropDownList = Api.Components.DropDownList;
            var ProgramVersion = Api.FinancialAid.ProgramVersion;
            var Holiday = Api.Holiday;
            var Campus = Api.Campus;
            var GradDateCalculator = Api.Tools.GradDateCalculator;
            var DayOfWeek = API.Enums.DayOfWeek;
            var GradDateCalculatorVm = (function () {
                function GradDateCalculatorVm() {
                    this.emptyGuid = "00000000-0000-0000-0000-000000000000";
                    this.campus = new Campus();
                    this.programVersion = new ProgramVersion();
                    this.holiday = new Holiday();
                    this.gradDateCalculator = new GradDateCalculator();
                    this.inpTotalProgramHours = $("#txtHoursInProgram");
                    this.inpSundayHours = $("#txtSundayHours");
                    this.inpMondayHours = $("#txtMondayHours");
                    this.inpTuesdayHours = $("#txtTuesdayHours");
                    this.inpWednesdayHours = $("#txtWednesdayHours");
                    this.inpThursdayHours = $("#txtThursdayHours");
                    this.inpFridayHours = $("#txtFridayHours");
                    this.inpSaturdayHours = $("#txtSaturdayHours");
                    this.holidays = [];
                    this.calculationData = {};
                }
                GradDateCalculatorVm.prototype.initialize = function () {
                    var that = this;
                    // create DatePicker from input HTML element
                    $("#dpStudentStartDate").kendoDatePicker({
                        open: function () {
                            var calendar = this.dateView.calendar;
                            calendar.wrapper.height(170);
                        }
                    });
                    $("#holidaysListBox").kendoListBox({
                        selectable: "multiple",
                        dataSource: that.holidays,
                        template: kendo.template($("#javascriptTemplate").html()),
                        toolbar: {
                            tools: []
                        }
                    });
                    that.listBox = $("#holidaysListBox").data("kendoListBox");
                    that.startDatePicker = $("#dpStudentStartDate").data("kendoDatePicker");
                    //initialize dropdowns
                    that.scheduleDropDownList = new DropDownList("ddlLoadFromSchedule", that.programVersion, { params: { programVersionId: 1 } }, { settings: { dataValueField: "scheduleId", dataTextField: "description", autoBind: false, enable: false } }, function () {
                        var scheduleDetails = that.scheduleDropDownList.getKendoDropDown().dataItem(that.scheduleDropDownList.getKendoDropDown().select()).programScheduleDetails;
                        $.each(scheduleDetails, function (i, val) {
                            var dayOfWeek = val.dayOfWeek;
                            var total = val.total == 0 ? 0.00 : val.total;
                            switch (dayOfWeek) {
                                case DayOfWeek.Sunday:
                                    that.inpSundayHours.val(total);
                                    break;
                                case DayOfWeek.Monday:
                                    that.inpMondayHours.val(total);
                                    break;
                                case DayOfWeek.Tuesday:
                                    that.inpTuesdayHours.val(total);
                                    break;
                                case DayOfWeek.Wednesday:
                                    that.inpWednesdayHours.val(total);
                                    break;
                                case DayOfWeek.Thursday:
                                    that.inpThursdayHours.val(total);
                                    break;
                                case DayOfWeek.Friday:
                                    that.inpFridayHours.val(total);
                                    break;
                                case DayOfWeek.Saturday:
                                    that.inpSaturdayHours.val(total);
                                    break;
                            }
                        });
                    }, "getScheduleDetailsByProgramVersionId");
                    that.campusDropDownList = new DropDownList("ddlCampus", that.campus, undefined, {
                        settings: {
                            dataValueField: "value"
                        }
                    }, function () {
                        that.campusId = that.campusDropDownList.getValue();
                        if (that.campusDropDownList.hasValue()) {
                            that.programVersionDropDownList.getKendoDropDown().enable(true);
                            that.holiday.GetHolidayListByCampus(that.campusId, function (data) {
                                var holidays = [];
                                if (data.length > 0) {
                                    that.holidays = data;
                                    that.listBox.setDataSource(new kendo.data.DataSource({
                                        data: that.holidays
                                    }));
                                }
                            });
                            that.programVersionDropDownList.reload({ params: { campusId: that.campusDropDownList.getValue() } });
                        }
                        else {
                            that.programVersionDropDownList.getKendoDropDown().enable(false);
                            that.programVersionDropDownList.getKendoDropDown().select(0);
                            that.programVersionDropDownList.getKendoDropDown().element.trigger("change");
                            that.scheduleDropDownList.getKendoDropDown().enable(false);
                            that.scheduleDropDownList.getKendoDropDown().select(0);
                        }
                    });
                    that.campusDropDownList.getKendoDropDown().bind("dataBound", function () {
                        if (that.campusDropDownList.getKendoDropDown().list.length > 0) {
                            that.campusDropDownList.getKendoDropDown().select(1);
                            that.campusDropDownList.getKendoDropDown().element.trigger("change");
                            that.campusId = that.campusDropDownList.getValue();
                        }
                    });
                    that.programVersionDropDownList = new DropDownList("ddlProgramVersion", that.programVersion, undefined, { settings: { dataValueField: "value", autoBind: false, enable: false } }, function () {
                        var zero = "0.00";
                        that.inpTotalProgramHours.val(zero);
                        that.inpSundayHours.val(zero);
                        that.inpMondayHours.val(zero);
                        that.inpTuesdayHours.val(zero);
                        that.inpWednesdayHours.val(zero);
                        that.inpThursdayHours.val(zero);
                        that.inpFridayHours.val(zero);
                        that.inpSaturdayHours.val(zero);
                        $("#gradDate").html("N/A");
                        kendo.fx($("#gradDate")).fade("in").play();
                        that.programVersionId = that.programVersionDropDownList.getValue();
                        if (that.programVersionDropDownList.hasValue()) {
                            that.scheduleDropDownList.getKendoDropDown().enable(true);
                            that.programVersion.getProgramVersionTotalHours(that.programVersionId, function (data) {
                                var hours = data.toString() + ".00";
                                that.inpTotalProgramHours.val(hours);
                            });
                            that.scheduleDropDownList.reload({ params: { programVersionId: that.programVersionDropDownList.getValue() } });
                        }
                        else {
                            that.scheduleDropDownList.getKendoDropDown().enable(false);
                        }
                    });
                    $("#calculate-btn").on("click", function () {
                        var startDate = that.startDatePicker.value() != null ? that.startDatePicker.value().toISOString().split('T')[0] : "";
                        that.calculationData = {
                            campusId: that.campusId,
                            studentStartDate: startDate,
                            totalProgramHours: Number(that.inpTotalProgramHours.val()),
                            dailyHours: [{ DayOfWeek: DayOfWeek.Monday, Hours: Number(that.inpMondayHours.val()) }, { DayOfWeek: DayOfWeek.Tuesday, Hours: Number(that.inpTuesdayHours.val()) },
                                { DayOfWeek: DayOfWeek.Wednesday, Hours: Number(that.inpWednesdayHours.val()) }, { DayOfWeek: DayOfWeek.Thursday, Hours: Number(that.inpThursdayHours.val()) },
                                { DayOfWeek: DayOfWeek.Friday, Hours: Number(that.inpFridayHours.val()) }, { DayOfWeek: DayOfWeek.Saturday, Hours: Number(that.inpSaturdayHours.val()) },
                                { DayOfWeek: DayOfWeek.Sunday, Hours: Number(that.inpSundayHours.val()) }],
                            mondayHours: Number(that.inpMondayHours.val()), tuesdayHours: Number(that.inpTuesdayHours.val()), wednesdayHours: Number(that.inpWednesdayHours.val()),
                            thursdayHours: Number(that.inpThursdayHours.val()), fridayHours: Number(that.inpFridayHours.val()), saturdayHours: Number(that.inpSaturdayHours.val()), sundayHours: Number(that.inpSundayHours.val())
                        };
                        that.gradDateCalculator.calculateGraduationDate(that.calculationData, function (data) {
                            var result = data.asString == '1/1/0001' ? "N/A" : data.asString;
                            $("#gradDate").html(result);
                            kendo.fx($("#gradDate")).fade("in").play();
                        });
                    });
                };
                return GradDateCalculatorVm;
            }());
            Tools.GradDateCalculatorVm = GradDateCalculatorVm;
        })(Tools = ViewModels.Tools || (ViewModels.Tools = {}));
    })(ViewModels = Api.ViewModels || (Api.ViewModels = {}));
})(Api || (Api = {}));
//# sourceMappingURL=Fame.Advantage.Tools.js.map