function EmployerMruOutputModel () {
	this.Id = undefined;
	this.EmployerName = undefined;
	this.Address = undefined;
	this.City = undefined;
	this.State = undefined;
	this.Zip = undefined;
	this.CityStateZip = undefined;
	this.Phone = undefined;
	this.ImageUrl = undefined;
	this.NotFoundImageUrl = undefined;
	this.SearchCampusId = undefined;
	this.ModDate = undefined;
	this.MruModDate = undefined;
	return this;
}
 
function EmployerSearchOutputModel () {
	this.Id = undefined;
	this.FullName = undefined;
	this.Field1 = undefined;
	this.Field2 = undefined;
	this.Field3 = undefined;
	this.Field4 = undefined;
	this.Field5 = undefined;
	this.ImageUrl = undefined;
	this.NotFoundImageUrl = undefined;
	this.SearchCampusStringList = undefined;
	return this;
}
 
