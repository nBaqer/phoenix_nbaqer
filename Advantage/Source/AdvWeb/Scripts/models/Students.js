function StudentMruOutputModel () {
	this.Id = undefined;
	this.FullName = undefined;
	this.SSN = undefined;
	this.StudentNumber = undefined;
	this.StartDate = undefined;
	this.Status = undefined;
	this.Program = undefined;
	this.Campus = undefined;
	this.MruModDate = undefined;
	this.ModDate = undefined;
	this.ImageUrl = undefined;
	this.NotFoundImageUrl = undefined;
	this.SearchCampusId = undefined;
	return this;
}
 
function StudentSearchOutputModel () {
	this.Id = undefined;
	this.FullName = undefined;
	this.Field1 = undefined;
	this.Field2 = undefined;
	this.Field3 = undefined;
	this.Field4 = undefined;
	this.Field5 = undefined;
	this.ImageUrl = undefined;
	this.NotFoundImageUrl = undefined;
	this.SearchCampusId = undefined;
	return this;
}
 
