function ISmtpOutputModel () {
	this.Settings = undefined;
	this.Filter = undefined;
	this.Files = undefined;
	return this;
}
 
function SmtpConfigurationSettings () {
	this.Password = undefined;
	this.Username = undefined;
	this.Server = undefined;
	this.Port = undefined;
	this.IsSsl = undefined;
	this.IsOauth = undefined;
	this.From = undefined;
	this.AutClientSecret = undefined;
	this.Token = undefined;
	return this;
}
 
function SmtpInputModel () {
	this.Command = undefined;
	this.UserId = undefined;
	return this;
}
 
function SmtpOutputModel () {
	this.Settings = undefined;
	this.Filter = undefined;
	this.Files = undefined;
	return this;
}
 
function SmtpSendMessageInputModel () {
	this.Body64 = undefined;
	this.Subject = undefined;
	this.To = undefined;
	this.Cc = undefined;
	this.From = undefined;
	this.ContentType = undefined;
	this.Command = undefined;
	return this;
}
 
