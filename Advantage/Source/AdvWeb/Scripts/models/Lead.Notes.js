function INotesOutputModel () {
	this.NotesList = undefined;
	this.ModulesList = undefined;
	this.TypeNotesList = undefined;
	this.Filter = undefined;
	return this;
}
 
function LeadNotesInputFilter () {
	this.Command = undefined;
	this.LeadId = undefined;
	this.ModuleCode = undefined;
	this.UserId = undefined;
	this.CampusId = undefined;
	return this;
}
 
function NotesModel () {
	this.IdNote = undefined;
	this.NoteDate = undefined;
	this.ModuleName = undefined;
	this.Source = undefined;
	this.Type = undefined;
	this.Field = undefined;
	this.PageFieldId = undefined;
	this.NoteText = undefined;
	this.UserName = undefined;
	this.UserId = undefined;
	return this;
}
 
function NotesOutputModel () {
	this.NotesList = undefined;
	this.ModulesList = undefined;
	this.TypeNotesList = undefined;
	this.Filter = undefined;
	return this;
}
 
