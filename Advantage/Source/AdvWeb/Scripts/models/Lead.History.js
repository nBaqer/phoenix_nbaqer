function HistoryModel () {
	this.HistoryId = undefined;
	this.HistoryDate = undefined;
	this.HistoryModule = undefined;
	this.ModuleCode = undefined;
	this.Type = undefined;
	this.Description = undefined;
	this.ModUser = undefined;
	this.AdditionalContent = undefined;
	return this;
}
 
function HistoryOutputModel () {
	this.HistoryList = undefined;
	this.ModulesList = undefined;
	this.Filter = undefined;
	return this;
}
 
function IHistoryOutputModel () {
	this.HistoryList = undefined;
	this.ModulesList = undefined;
	this.Filter = undefined;
	return this;
}
 
function LeadHistoryInputFilter () {
	this.Command = undefined;
	this.LeadId = undefined;
	this.ModuleCode = undefined;
	this.UserId = undefined;
	this.CampusId = undefined;
	return this;
}
 
