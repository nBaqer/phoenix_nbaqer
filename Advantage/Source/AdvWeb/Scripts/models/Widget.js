function VoyantWidgetInputFilter () {
	this.UserGuid = undefined;
	this.CampusGuid = undefined;
	this.WindowServiceName = undefined;
	return this;
}
 
function VoyantWidgetOutputModel () {
	this.UrlWidget = undefined;
	this.UrlWidGetMaximized = undefined;
	this.ShowCustomCommand = undefined;
	this.ShowMaximizeCommand = undefined;
	this.ShowMinimizeCommand = undefined;
	this.ShowWidget = undefined;
	return this;
}
 
