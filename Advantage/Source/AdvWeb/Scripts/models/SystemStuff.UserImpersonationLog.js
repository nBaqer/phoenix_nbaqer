function GetUserImpersonationLogInputModel () {
	this.StartDate = undefined;
	this.EndDate = undefined;
	return this;
}
 
function PutUserImpersonationLogInputModel () {
	this.LogStart = undefined;
	this.LogEnd = undefined;
	this.ImpersonatedUser = undefined;
	return this;
}
 
function UserImpersonationLogOutputModel () {
	this.Id = undefined;
	this.ImpersonatedUser = undefined;
	this.LogStart = undefined;
	this.LogEnd = undefined;
	this.ModUser = undefined;
	this.LStart = undefined;
	this.LEnd = undefined;
	return this;
}
 
