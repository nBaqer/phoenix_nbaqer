function LeadQuickFieldsInputModel () {
	this.Command = undefined;
	this.CtrlName = undefined;
	this.Input = undefined;
	return this;
}
 
function LeadQuickFieldsOutputModel () {
	this.TblFldsId = undefined;
	this.FldId = undefined;
	this.SectionId = undefined;
	this.DdlId = undefined;
	this.FldName = undefined;
	this.Required = undefined;
	this.FldLen = undefined;
	this.FldType = undefined;
	this.Caption = undefined;
	this.ControlName = undefined;
	this.FldTypeId = undefined;
	this.CtrlIdName = undefined;
	this.PropName = undefined;
	this.ParentCtrlId = undefined;
	return this;
}
 
function QuickLeadDropdownDataSource (type) {
	this.Type = type;
	this.Name = undefined;
	this.DataSource = undefined;
	return this;
}
 
