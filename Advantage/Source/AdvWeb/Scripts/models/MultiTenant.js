function CredentialsModel () {
	this.UserName = undefined;
	this.Password = undefined;
	return this;
}
 
function SessionModel () {
	this.UserId = undefined;
	this.TenantId = undefined;
	this.IsPasswordChangeRequired = undefined;
	this.IsSupportUser = undefined;
	return this;
}
 
