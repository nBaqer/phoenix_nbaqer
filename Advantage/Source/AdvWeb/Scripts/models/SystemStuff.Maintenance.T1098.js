function Fame1098InputModel () {
	this.ArchiveList = undefined;
	this.ExclusionsList = undefined;
	this.TransactionsList = undefined;
	this.ValidEnrollmentsList = undefined;
	this.Year = undefined;
	return this;
}
 
function SchoolConfig () {
	this.CampusToken = undefined;
	this.School = undefined;
	this.KissKey = undefined;
	this.SchoolId = undefined;
	return this;
}
 
function TransactionT () {
	this.TransDate = undefined;
	this.TransAmount = undefined;
	this.TransCodeCode = undefined;
	this.EnrollmentId = undefined;
	this.PayType = undefined;
	return this;
}
 
