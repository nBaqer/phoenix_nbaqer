function EnrollmentStudentOutputModel () {
	this.Id = undefined;
	this.FirstName = undefined;
	this.MiddleName = undefined;
	this.LastName = undefined;
	this.SSN = undefined;
	this.StudentNumber = undefined;
	this.WorkEmail = undefined;
	this.HomeEmail = undefined;
	this.DOB = undefined;
	this.Gender = undefined;
	return this;
}
 
function GetEnrollmentsInputModel () {
	this.CampusId = undefined;
	this.ProgramVersionId = undefined;
	this.EnrollmentStatusId = undefined;
	this.StudentGroupId = undefined;
	this.PageNumber = undefined;
	this.PageSize = undefined;
	this.StudentId = undefined;
	this.SearchTerm = undefined;
	return this;
}
 
