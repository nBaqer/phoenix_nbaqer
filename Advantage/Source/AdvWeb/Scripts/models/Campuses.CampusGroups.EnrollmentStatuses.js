function EnrollmentStatusOutputModel () {
	this.Id = undefined;
	this.StatusCode = undefined;
	this.Description = undefined;
	this.CampGrpId = undefined;
	this.SystemStatusId = undefined;
	this.IsAcademicProbation = undefined;
	this.DiscloseProbation = undefined;
	this.IsDefaultLeadStatus = undefined;
	this.Status = undefined;
	this.CampusGroup = undefined;
	this.SystemStatus = undefined;
	return this;
}
 
function GetEnrollmentStatusInputModel () {
	this.StatusCode = undefined;
	this.StatusLevelId = undefined;
	return this;
}
 
