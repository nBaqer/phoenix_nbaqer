function GetStudentInputModel () {
	this.CampusId = undefined;
	this.ProgramVersionId = undefined;
	this.TermId = undefined;
	this.StudentGroupId = undefined;
	this.EnrollmentStatusId = undefined;
	this.StartDate = undefined;
	this.StudentNumber = undefined;
	this.FirstName = undefined;
	this.LastName = undefined;
	this.Page = undefined;
	this.PageSize = undefined;
	this.SearchTerm = undefined;
	this.filter = undefined;
	return this;
}
 
function StudentOutputModel () {
	this.Id = undefined;
	this.FirstName = undefined;
	this.MiddleName = undefined;
	this.LastName = undefined;
	this.FullName = undefined;
	this.SSN = undefined;
	this.StudentNumber = undefined;
	this.WorkEmail = undefined;
	this.HomeEmail = undefined;
	this.DOB = undefined;
	this.Gender = undefined;
	this.Enrollments = undefined;
	return this;
}
 
function StudentUpdateModel () {
	this.Id = undefined;
	this.Balance = undefined;
	return this;
}
 
