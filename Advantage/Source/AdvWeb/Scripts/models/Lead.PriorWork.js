function IPriorWorkItemOutputModel () {
	this.AddressOutputModel = undefined;
	this.Comments = undefined;
	this.Contacts = undefined;
	this.Employer = undefined;
	this.StartDate = undefined;
	this.EndDate = undefined;
	this.Id = undefined;
	this.JobResponsabilities = undefined;
	this.JobStatus = undefined;
	this.JobTitle = undefined;
	this.SchoolJobTitle = undefined;
	return this;
}
 
function IPriorWorkOutputModel () {
	this.InputModel = undefined;
	this.PriorWorkItemList = undefined;
	this.JobStatusItemList = undefined;
	this.AdSchoolJobTitlesItemList = undefined;
	this.StatesList = undefined;
	this.CountriesList = undefined;
	this.PriorWorkDetailObject = undefined;
	this.SdfList = undefined;
	return this;
}
 
function PriorWorkAddressOutputModel () {
	this.AddressId = undefined;
	this.PriorWorkId = undefined;
	this.Address = undefined;
	this.Address1 = undefined;
	this.AddressApto = undefined;
	this.Address2 = undefined;
	this.City = undefined;
	this.IsInternational = undefined;
	this.State = undefined;
	this.StateInternational = undefined;
	this.Country = undefined;
	this.ZipCode = undefined;
	this.ModDate = undefined;
	this.Moduser = undefined;
	return this;
}
 
function PriorWorkContactOutputModel () {
	this.ContactId = undefined;
	this.PriorWorkId = undefined;
	this.Title = undefined;
	this.Name = undefined;
	this.FirstName = undefined;
	this.LastName = undefined;
	this.MiddleName = undefined;
	this.IsPhoneInternational = undefined;
	this.Phone = undefined;
	this.Email = undefined;
	this.IsActive = undefined;
	this.Prefix = undefined;
	return this;
}
 
function PriorWorkDetailItemOutputModel () {
	this.ContactId = undefined;
	this.Comments = undefined;
	this.Employer = undefined;
	this.JobResponsabilities = undefined;
	this.ContactList = undefined;
	return this;
}
 
function PriorWorkInputModel () {
	this.Command = undefined;
	this.LeadId = undefined;
	this.CampusId = undefined;
	this.UserId = undefined;
	this.PriorWorkId = undefined;
	return this;
}
 
function PriorWorkItemOutputModel () {
	this.Id = undefined;
	this.AddressOutputModel = undefined;
	this.StartDate = undefined;
	this.EndDate = undefined;
	this.JobStatus = undefined;
	this.JobTitle = undefined;
	this.Employer = undefined;
	this.SchoolJobTitle = undefined;
	this.Contacts = undefined;
	this.JobResponsabilities = undefined;
	this.Comments = undefined;
	return this;
}
 
function PriorWorkOutputModel () {
	this.InputModel = undefined;
	this.PriorWorkItemList = undefined;
	this.JobStatusItemList = undefined;
	this.AdSchoolJobTitlesItemList = undefined;
	this.StatesList = undefined;
	this.CountriesList = undefined;
	this.PriorWorkDetailObject = undefined;
	this.SdfList = undefined;
	this.PrefixList = undefined;
	return this;
}
 
