function LeadEducationFilterInputModel () {
	this.Id = undefined;
	this.CampusId = undefined;
	this.LeadId = undefined;
	this.StudentId = undefined;
	this.UserId = undefined;
	return this;
}
 
function LeadEducationInputModel () {
	this.Id = undefined;
	this.LeadId = undefined;
	this.CampusId = undefined;
	this.LevelId = undefined;
	this.TypeId = undefined;
	this.InstitutionId = undefined;
	this.InstitutionName = undefined;
	this.Major = undefined;
	this.FinalGrade = undefined;
	this.Gpa = undefined;
	this.Rank = undefined;
	this.Percentile = undefined;
	this.Graduate = undefined;
	this.GraduationDate = undefined;
	this.CertificateOrDegree = undefined;
	this.Comments = undefined;
	this.UserId = undefined;
	this.IsInstitutionAddedToMru = undefined;
	return this;
}
 
function LeadEducationOutputModel () {
	this.Id = undefined;
	this.LeadId = undefined;
	this.LevelId = undefined;
	this.Level = undefined;
	this.Institution = undefined;
	this.Major = undefined;
	this.FinalGrade = undefined;
	this.Gpa = undefined;
	this.Rank = undefined;
	this.Percentile = undefined;
	this.Graduate = undefined;
	this.GraduationDate = undefined;
	this.CertificateOrDegree = undefined;
	this.Comments = undefined;
	this.InstitutionName = undefined;
	return this;
}
 
