function CampusOutputModel () {
	this.ID = undefined;
	this.Code = undefined;
	this.Description = undefined;
	this.Address1 = undefined;
	this.Address2 = undefined;
	this.City = undefined;
	this.StateId = undefined;
	this.Zip = undefined;
	return this;
}
 
function CampusesInputModel () {
	this.CampusGrpId = undefined;
	this.IncludeAll = undefined;
	this.UserId = undefined;
	this.IsSet = undefined;
	return this;
}
 
function CampusWithLeadsOutputModel () {
	this.Id = undefined;
	this.Description = undefined;
	this.ActiveLeadCount = undefined;
	return this;
}
 
