function GetMenuItemInputModel () {
	this.CampusId = undefined;
	this.UserId = undefined;
	this.MenuWithLinks = undefined;
	this.PageName = undefined;
	this.IsImpersonating = undefined;
	return this;
}
 
function MenuItemsOutputModel () {
	this.Id = undefined;
	this.text = undefined;
	this.url = undefined;
	this.IsEnabled = undefined;
	this.items = undefined;
	return this;
}
 
function MenuItemTypeOutputModel () {
	this.Id = undefined;
	this.ItemType = undefined;
	return this;
}
 
function MenuModulesOutputModel () {
	this.Id = undefined;
	this.text = undefined;
	return this;
}
 
