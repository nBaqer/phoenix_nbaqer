function IQueueOutputModel () {
	this.AdmissionRepList = undefined;
	this.LeadQueueInfoList = undefined;
	this.QueueInfoObj = undefined;
	return this;
}
 
function LeadQueueInfo () {
	this.Id = undefined;
	this.FirstName = undefined;
	this.LastName = undefined;
	this.Status = undefined;
	this.StatusSysId = undefined;
	this.ProgramInterest = undefined;
	this.Received = undefined;
	this.DateReceived = undefined;
	this.LastActivity = undefined;
	this.LastActivityCode = undefined;
	this.DateLast = undefined;
	this.Age = undefined;
	this.NextActivity = undefined;
	this.DateNext = undefined;
	this.Phone = undefined;
	this.IsInternational = undefined;
	return this;
}
 
function QueueInfo () {
	this.ShowAdmissionRep = undefined;
	this.DeliveryTime = undefined;
	this.RefreshQueueInterval = undefined;
	return this;
}
 
function QueueInputmodel () {
	this.UserId = undefined;
	this.AssingRepId = undefined;
	this.CampusId = undefined;
	this.Command = undefined;
	return this;
}
 
function QueueOutputModel () {
	this.AdmissionRepList = undefined;
	this.LeadQueueInfoList = undefined;
	this.QueueInfoObj = undefined;
	return this;
}
 
function QueuePhoneInputModel () {
	this.ID = undefined;
	this.Phone = undefined;
	this.LeadId = undefined;
	this.Position = undefined;
	this.UserId = undefined;
	this.IsForeignPhone = undefined;
	this.Extension = undefined;
	return this;
}
 
