function CustomField () {
	this.id = undefined;
	this.class_type = undefined;
	this.description = undefined;
	this.field_type = undefined;
	this.item_label = undefined;
	this.item_value = undefined;
	this.item_order = undefined;
	this.deleted = undefined;
	this.created_at = undefined;
	this.updated_at = undefined;
	return this;
}
 
function Error () {
	this.message = undefined;
	this.stack = undefined;
	this.code = undefined;
	this.sql = undefined;
	return this;
}
 
function GeneralResponse () {
	this.ErrorCode = undefined;
	this.KlassAppIdentification = undefined;
	this.IsDeleted = undefined;
	this.Explanation = undefined;
	this.ObjectFromServer = undefined;
	return this;
}
 
function IGeneralResponse () {
	this.ErrorCode = undefined;
	this.Explanation = undefined;
	this.IsDeleted = undefined;
	this.KlassAppIdentification = undefined;
	this.ObjectFromServer = undefined;
	return this;
}
 
function Locations () {
	this.id = undefined;
	this.name = undefined;
	this.address = undefined;
	this.phone = undefined;
	this.location_type = undefined;
	this.contact_email = undefined;
	this.contact_person = undefined;
	this.contact_phone = undefined;
	this.contact_position = undefined;
	this.deleted = undefined;
	this.referrals_email_address = undefined;
	this.created_at = undefined;
	this.updated_at = undefined;
	this.payments_on = undefined;
	this.payments_currency = undefined;
	this.payments_worldpay_id = undefined;
	this.payments_email_address = undefined;
	this.payments_worldpay_gateway_id = undefined;
	this.payments_worldpay_form_id = undefined;
	this.payments_worldpay_session = undefined;
	this.payments_worldpay_key = undefined;
	return this;
}
 
function IWapiOperation () {
	this.Id = undefined;
	this.CodeOperation = undefined;
	this.SecretCodeCompany = undefined;
	this.CodeExtOperationMode = undefined;
	this.ExternalUrl = undefined;
	this.ConsumerKey = undefined;
	this.PrivateKey = undefined;
	this.CodeWapiServ = undefined;
	this.SecondCodeWapiServ = undefined;
	this.OperationSecInterval = undefined;
	this.DateLastExecution = undefined;
	this.PollSecOnDemandOperation = undefined;
	this.FlagOnDemandOperation = undefined;
	this.FlagRefreshConfig = undefined;
	this.UrlProxy = undefined;
	this.LogFile = undefined;
	this.SizeLogFile = undefined;
	this.Tracer = undefined;
	this.KlassAppDaysToConsider = undefined;
	this.KlassAppUrlConfigCustomFields = undefined;
	this.KlassAppUrlConfigKeyValuesPairs = undefined;
	this.KlassAppUrlConfigMajors = undefined;
	this.KlassAppUrlConfigLocations = undefined;
	this.CampusActiveList = undefined;
	return this;
}
 
function WapiOperation () {
	this.Id = undefined;
	this.CodeOperation = undefined;
	this.SecretCodeCompany = undefined;
	this.CodeExtOperationMode = undefined;
	this.ExternalUrl = undefined;
	this.ConsumerKey = undefined;
	this.PrivateKey = undefined;
	this.CodeWapiServ = undefined;
	this.SecondCodeWapiServ = undefined;
	this.OperationSecInterval = undefined;
	this.DateLastExecution = undefined;
	this.PollSecOnDemandOperation = undefined;
	this.FlagOnDemandOperation = undefined;
	this.FlagRefreshConfig = undefined;
	this.UrlProxy = undefined;
	this.LogFile = undefined;
	this.SizeLogFile = undefined;
	this.Tracer = undefined;
	this.KlassAppDaysToConsider = undefined;
	this.KlassAppUrlConfigCustomFields = undefined;
	this.KlassAppUrlConfigKeyValuesPairs = undefined;
	this.KlassAppUrlConfigLocations = undefined;
	this.CampusActiveList = undefined;
	this.KlassAppUrlConfigMajors = undefined;
	return this;
}
 
function KeyValuePart () {
	this.id = undefined;
	this.type = undefined;
	this.name = undefined;
	this.deleted = undefined;
	this.created_at = undefined;
	this.updated_at = undefined;
	return this;
}
 
function Majors () {
	this.id = undefined;
	this.name = undefined;
	this.created_at = undefined;
	this.updated_at = undefined;
	this.deleted = undefined;
	this.type = undefined;
	this.auto_disabled = undefined;
	return this;
}
 
function ResponseBase () {
	this.success = undefined;
	this.timestamp = undefined;
	this.auth_id = undefined;
	this.total = undefined;
	this.offset = undefined;
	this.limit = undefined;
	this.response = undefined;
	this.errors = undefined;
	this.info = undefined;
	return this;
}
 
