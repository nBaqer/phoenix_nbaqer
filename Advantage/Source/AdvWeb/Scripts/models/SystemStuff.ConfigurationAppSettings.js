function ConfigurationAppSettingOutputModel () {
	this.Id = undefined;
	this.KeyName = undefined;
	this.Description = undefined;
	this.CampusSpecific = undefined;
	this.ExtraConfirmation = undefined;
	this.ConfigurationValues = undefined;
	return this;
}
 
function ConfigurationAppSettingValuesOutputModel () {
	this.SettingId = undefined;
	this.Campus = undefined;
	this.Value = undefined;
	this.Active = undefined;
	return this;
}
 
