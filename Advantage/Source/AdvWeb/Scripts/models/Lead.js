function LeadAddressInputModel () {
	this.LeadId = undefined;
	this.CampusId = undefined;
	this.UserName = undefined;
	this.UserId = undefined;
	this.TypeId = undefined;
	this.StatusId = undefined;
	this.ID = undefined;
	this.ShowOnLeadPage = undefined;
	this.IsMaillingAddress = undefined;
	this.Address1 = undefined;
	this.Address2 = undefined;
	this.City = undefined;
	this.ZipCode = undefined;
	this.CountryId = undefined;
	this.StateId = undefined;
	this.State = undefined;
	this.IsInternational = undefined;
	this.CountyId = undefined;
	this.County = undefined;
	this.Country = undefined;
	return this;
}
 
function LeadEmailInputModel () {
	this.LeadId = undefined;
	this.CampusId = undefined;
	this.UserName = undefined;
	this.TypeId = undefined;
	this.StatusId = undefined;
	this.ID = undefined;
	this.ShowOnLeadPage = undefined;
	this.IsBest = undefined;
	this.Email = undefined;
	return this;
}
 
function LeadEmailOutputModel () {
	this.ID = undefined;
	this.EmailType = undefined;
	this.EmailTypeId = undefined;
	this.Email = undefined;
	this.Status = undefined;
	this.StatusId = undefined;
	this.IsPreferred = undefined;
	this.IsShowOnLeadPage = undefined;
	return this;
}
 
function DuplicateOutputModel () {
	this.LastName = undefined;
	this.FirstName = undefined;
	this.Ssn = undefined;
	this.Dob = undefined;
	this.Address1 = undefined;
	this.Phone = undefined;
	this.Phone1 = undefined;
	this.Phone2 = undefined;
	this.Email = undefined;
	this.Email1 = undefined;
	this.Campus = undefined;
	this.AdmissionRep = undefined;
	this.Status = undefined;
	return this;
}
 
function LeadInfoCommentInputModel () {
	this.LeadId = undefined;
	this.CampusId = undefined;
	this.UserName = undefined;
	this.ID = undefined;
	this.Comment = undefined;
	return this;
}
 
function LeadInfoInsertOutputModel () {
	this.ValidationErrorMessages = undefined;
	this.DuplicatedMessages = undefined;
	this.PossiblesDuplicatesList = undefined;
	this.ValidStatusChangeList = undefined;
	return this;
}
 
function LeadInfoPageOutputModel () {
	this.LeadId = undefined;
	this.ModUser = undefined;
	this.InputModel = undefined;
	this.AvoidDuplicateAnalysis = undefined;
	this.StateChangeIdsList = undefined;
	this.FirstName = undefined;
	this.MiddleName = undefined;
	this.LastName = undefined;
	this.Prefix = undefined;
	this.Suffix = undefined;
	this.NickName = undefined;
	this.SSN = undefined;
	this.Dob = undefined;
	this.Citizenship = undefined;
	this.AlienNumber = undefined;
	this.Dependency = undefined;
	this.MaritalStatus = undefined;
	this.Dependants = undefined;
	this.FamilyIncoming = undefined;
	this.HousingType = undefined;
	this.DrvLicStateCode = undefined;
	this.DriverLicenseNumber = undefined;
	this.Transportation = undefined;
	this.Gender = undefined;
	this.Age = undefined;
	this.DistanceToSchool = undefined;
	this.IsDisabled = undefined;
	this.RaceId = undefined;
	this.LeadLastNameHistoryList = undefined;
	this.Vehicles = undefined;
	this.PreferredContactId = undefined;
	this.CountryId = undefined;
	this.CountyId = undefined;
	this.AddressStateId = undefined;
	this.OtherState = undefined;
	this.City = undefined;
	this.Zip = undefined;
	this.AddressTypeId = undefined;
	this.Address1 = undefined;
	this.AddressApt = undefined;
	this.Address2 = undefined;
	this.PhonesList = undefined;
	this.EmailList = undefined;
	this.BestTime = undefined;
	this.NoneEmail = undefined;
	this.SourceCategoryId = undefined;
	this.SourceTypeId = undefined;
	this.AdvertisementId = undefined;
	this.Note = undefined;
	this.SourceDateTime = undefined;
	this.DateApplied = undefined;
	this.AssignedDate = undefined;
	this.AdmissionRepId = undefined;
	this.AgencySponsorId = undefined;
	this.Comments = undefined;
	this.PreviousEducationId = undefined;
	this.HighSchoolId = undefined;
	this.HighSchoolGradDate = undefined;
	this.AttendingHs = undefined;
	this.AdminCriteriaId = undefined;
	this.ReasonNotEnrolled = undefined;
	this.CampusId = undefined;
	this.AreaId = undefined;
	this.ProgramId = undefined;
	this.PrgVerId = undefined;
	this.AttendTypeId = undefined;
	this.LeadStatusId = undefined;
	this.ExpectedStart = undefined;
	this.ScheduleId = undefined;
	this.SdfList = undefined;
	this.GroupIdList = undefined;
	return this;
}
 
function LeadInfoPageResourcesOutputModel () {
	this.SdfList = undefined;
	this.RequiredList = undefined;
	this.LeadGroupList = undefined;
	return this;
}
 
function LeadLastNameHistoryOutputModel () {
	this.Id = undefined;
	this.LastName = undefined;
	this.LeadId = undefined;
	return this;
}
 
function LeadAddressOutputModel () {
	this.ID = undefined;
	this.AddressType = undefined;
	this.AddressTypeId = undefined;
	this.Address1 = undefined;
	this.Address2 = undefined;
	this.City = undefined;
	this.State = undefined;
	this.StateId = undefined;
	this.ZipCode = undefined;
	this.Country = undefined;
	this.CountryId = undefined;
	this.Status = undefined;
	this.StatusId = undefined;
	this.IsMailingAddress = undefined;
	this.IsShowOnLeadPage = undefined;
	this.ModDate = undefined;
	this.Moduser = undefined;
	this.County = undefined;
	this.CountyId = undefined;
	this.IsInternational = undefined;
	return this;
}
 
function LeadContactInputModel () {
	this.LeadId = undefined;
	this.CampusId = undefined;
	this.UserName = undefined;
	this.UserId = undefined;
	this.TypeId = undefined;
	this.StatusId = undefined;
	this.ID = undefined;
	this.ShowOnLeadPage = undefined;
	this.IsBest = undefined;
	this.Phone = undefined;
	this.Extension = undefined;
	this.IsForeignPhone = undefined;
	this.Email = undefined;
	this.Comments = undefined;
	return this;
}
 
function LeadOtherContactAddressModel () {
	this.OtherContactsAddresesId = undefined;
	this.OtherContactId = undefined;
	this.LeadId = undefined;
	this.AddressTypeId = undefined;
	this.AddressType = undefined;
	this.Address1 = undefined;
	this.Address2 = undefined;
	this.City = undefined;
	this.StateId = undefined;
	this.State = undefined;
	this.StateInternational = undefined;
	this.CountryInternational = undefined;
	this.CountyInternational = undefined;
	this.ZipCode = undefined;
	this.CountryId = undefined;
	this.Country = undefined;
	this.CountyId = undefined;
	this.County = undefined;
	this.StatusId = undefined;
	this.Status = undefined;
	this.IsMailingAddress = undefined;
	this.IsInternational = undefined;
	this.ModUser = undefined;
	this.ModDate = undefined;
	return this;
}
 
function LeadOtherContactEmailModel () {
	this.OtherContactsEmailId = undefined;
	this.OtherContactId = undefined;
	this.LeadId = undefined;
	this.Email = undefined;
	this.EmailTypeId = undefined;
	this.EmailType = undefined;
	this.ModUser = undefined;
	this.ModDate = undefined;
	this.StatusId = undefined;
	this.Status = undefined;
	return this;
}
 
function LeadOtherContactModel () {
	this.OtherContactId = undefined;
	this.LeadId = undefined;
	this.StatusId = undefined;
	this.Status = undefined;
	this.RelationshipId = undefined;
	this.Relationship = undefined;
	this.ContactTypeId = undefined;
	this.ContactType = undefined;
	this.PrefixId = undefined;
	this.Prefix = undefined;
	this.SufixId = undefined;
	this.Sufix = undefined;
	this.FirstName = undefined;
	this.LastName = undefined;
	this.FullName = undefined;
	this.MiddleName = undefined;
	this.Comments = undefined;
	this.ModUser = undefined;
	this.ModDate = undefined;
	this.Phones = undefined;
	this.Emails = undefined;
	this.Addresses = undefined;
	return this;
}
 
function LeadOtherContactPhoneModel () {
	this.OtherContactsPhoneId = undefined;
	this.OtherContactId = undefined;
	this.LeadId = undefined;
	this.PhoneTypeId = undefined;
	this.PhoneType = undefined;
	this.Phone = undefined;
	this.Extension = undefined;
	this.IsForeignPhone = undefined;
	this.StatusId = undefined;
	this.Status = undefined;
	this.ModUser = undefined;
	this.ModDate = undefined;
	return this;
}
 
function LeadPhonesOutputModel () {
	this.ID = undefined;
	this.Phone = undefined;
	this.Extension = undefined;
	this.IsForeignPhone = undefined;
	this.Position = undefined;
	this.PhoneTypeId = undefined;
	this.PhoneType = undefined;
	this.Status = undefined;
	this.StatusId = undefined;
	this.ShowOnLeadPage = undefined;
	this.Best = undefined;
	return this;
}
 
function LeadContactsOutputModel () {
	this.LeadId = undefined;
	this.LeadPhonesList = undefined;
	this.LeadEmailList = undefined;
	this.LeadAddressList = undefined;
	this.PhoneTypes = undefined;
	this.AddressTypes = undefined;
	this.EmailTypes = undefined;
	this.Statuses = undefined;
	this.Countries = undefined;
	this.States = undefined;
	this.Comments = undefined;
	this.Counties = undefined;
	this.ContactTypes = undefined;
	this.Sufixes = undefined;
	this.Prefixes = undefined;
	this.Relationships = undefined;
	return this;
}
 
function LeadReqEntityOutputModel () {
	this.Id = undefined;
	this.Code = undefined;
	this.Description = undefined;
	this.DisplayType = undefined;
	this.DateReceived = undefined;
	this.ApprovalDate = undefined;
	this.ApprovedBy = undefined;
	this.IsApproved = undefined;
	this.IsOverridden = undefined;
	this.OverReason = undefined;
	this.DocumentRequirementId = undefined;
	this.IsMandatory = undefined;
	this.RequirementLevelType = undefined;
	this.Score = undefined;
	this.MinScore = undefined;
	return this;
}
 
function LeadGroupInfoOutputModel () {
	this.GroupId = undefined;
	this.Description = undefined;
	this.CampusId = undefined;
	return this;
}
 
function LeadInputModel () {
	this.LeadId = undefined;
	this.CampusId = undefined;
	this.LeadStatusId = undefined;
	this.UserId = undefined;
	this.PageResourceId = undefined;
	this.CommandString = undefined;
	return this;
}
 
function LeadRequirementOutputModel () {
	this.Id = undefined;
	this.Code = undefined;
	this.Description = undefined;
	this.Type = undefined;
	this.RequiredFor = undefined;
	this.RequirementType = undefined;
	this.Status = undefined;
	this.Module = undefined;
	this.StartDate = undefined;
	this.EndDate = undefined;
	this.DateRequested = undefined;
	this.DateReceived = undefined;
	this.IsApproved = undefined;
	this.DocumentStatusId = undefined;
	this.DocumentStatusDescription = undefined;
	this.DocumentRequirementId = undefined;
	this.LeadId = undefined;
	this.StudentDocumentId = undefined;
	this.ShowUpdate = undefined;
	return this;
}
 
function GetLeadRequirementInputModel () {
	this.StatusCode = undefined;
	this.CampusId = undefined;
	this.UserId = undefined;
	this.IsMandatory = undefined;
	this.leadId = undefined;
	this.RequirementType = undefined;
	this.TransactionId = undefined;
	return this;
}
 
function GetLeadInputModel () {
	this.VendorId = undefined;
	this.CampusOfInterest = undefined;
	this.CampusId = undefined;
	this.RepId = undefined;
	this.LastModDate = undefined;
	this.LeadGuid = undefined;
	return this;
}
 
function GetLeadAssignmentnputModel () {
	this.Id = undefined;
	return this;
}
 
function GetLeadUpdateInputModel () {
	this.LeadIds = undefined;
	this.CampusId = undefined;
	this.UserId = undefined;
	this.RepId = undefined;
	return this;
}
 
function LeadCampusesOfInterestOutputModel () {
	this.Text = undefined;
	this.Value = undefined;
	return this;
}
 
function LeadDuplicatesInputFilter () {
	this.LeadNewGuid = undefined;
	this.LeadDuplicateGuid = undefined;
	this.Command = undefined;
	this.UserId = undefined;
	return this;
}
 
function LeadDuplicatesOutputModel () {
	this.Header = undefined;
	this.Id = undefined;
	this.FullName = undefined;
	this.Address1 = undefined;
	this.Address2 = undefined;
	this.City = undefined;
	this.State = undefined;
	this.Zip = undefined;
	this.Status = undefined;
	this.Phone = undefined;
	this.Email = undefined;
	this.SourceInfo = undefined;
	this.AdmissionsRep = undefined;
	this.CampusOfInterest = undefined;
	this.ProgramOfInterest = undefined;
	this.Comments = undefined;
	this.AcquiredDate = undefined;
	this.From = undefined;
	this.Campus = undefined;
	return this;
}
 
function LeadInfoBarOutputModel () {
	this.LeadGuid = undefined;
	this.LeadFullName = undefined;
	this.ProgramVersionName = undefined;
	this.ExpectedStart = undefined;
	this.AdmissionRep = undefined;
	this.DateAssigned = undefined;
	this.LeadImagePath = undefined;
	this.ContentType = undefined;
	this.CurrentStatus = undefined;
	this.CampusName = undefined;
	return this;
}
 
function LeadInfoRequirementOutputModel () {
	this.RequirementName = undefined;
	this.TypeOfRequirement = undefined;
	this.RequirementStatus = undefined;
	this.IsMandatory = undefined;
	this.ImagePath = undefined;
	return this;
}
 
function LeadNewDuplicatesOutputModel () {
	this.NewLeadGuid = undefined;
	this.DuplicateGuid = undefined;
	this.PosibleDuplicateFullName = undefined;
	return this;
}
 
function LeadOutputModel () {
	this.Id = undefined;
	this.FullName = undefined;
	this.FirstMiddleLastName = undefined;
	this.ProgramOfInterest = undefined;
	this.ProgramVersion = undefined;
	this.CampusOfInterest = undefined;
	this.Campus = undefined;
	this.City = undefined;
	this.State = undefined;
	this.Zip = undefined;
	return this;
}
 
function LeadsAssignedCountOutputModel () {
	this.NotAssignedCampusCount = undefined;
	this.NotAssignedRepCount = undefined;
	return this;
}
 
function LeadSdfOutputModel () {
	this.SdfId = undefined;
	this.SdfValue = undefined;
	return this;
}
 
function LeadSearchOutputModel () {
	this.Id = undefined;
	this.FullName = undefined;
	this.Field1 = undefined;
	this.Field2 = undefined;
	this.Field3 = undefined;
	this.Field4 = undefined;
	this.Field5 = undefined;
	this.ImageUrl = undefined;
	this.NotFoundImageUrl = undefined;
	this.SearchCampusId = undefined;
	return this;
}
 
function LeadTransactionOutput () {
	this.SchoolName = undefined;
	this.Address = undefined;
	this.CityStateZip = undefined;
	this.LeadName = undefined;
	this.TransDate = undefined;
	this.TransactionId = undefined;
	this.PaymentType = undefined;
	this.TransactionCode = undefined;
	this.DocumentID = undefined;
	this.AmountReceived = undefined;
	return this;
}
 
function LeadVendorOutputModel () {
	this.Id = undefined;
	this.VendorName = undefined;
	this.VendorCode = undefined;
	this.Description = undefined;
	return this;
}
 
function LeadMruOutputModel () {
	this.Id = undefined;
	this.FullName = undefined;
	this.SSN = undefined;
	this.StartDate = undefined;
	this.Status = undefined;
	this.Program = undefined;
	this.Campus = undefined;
	this.MruModDate = undefined;
	this.ModDate = undefined;
	this.ImageUrl = undefined;
	this.NotFoundImageUrl = undefined;
	this.SearchCampusId = undefined;
	return this;
}
 
function VehicleInputModel () {
	this.VehicleId = undefined;
	this.LeadId = undefined;
	this.UserId = undefined;
	this.Command = undefined;
	return this;
}
 
function VehicleOutputModel () {
	this.Id = undefined;
	this.Position = undefined;
	this.Permit = undefined;
	this.Make = undefined;
	this.Model = undefined;
	this.Color = undefined;
	this.Plate = undefined;
	this.ModUser = undefined;
	this.ModDate = undefined;
	this.LeadId = undefined;
	return this;
}
 
function LeadReqDetailsInputModel () {
	this.Id = undefined;
	this.RequirementType = undefined;
	this.FilePath = undefined;
	return this;
}
 
function LeadReqDetailsTestsOutputModel () {
	this.Id = undefined;
	this.LeadId = undefined;
	this.DocumentId = undefined;
	this.Score = undefined;
	this.Pass = undefined;
	this.RequirementType = undefined;
	this.DateReceived = undefined;
	this.IsApproved = undefined;
	this.ApprovalDate = undefined;
	this.IsOverridden = undefined;
	return this;
}
 
function LeadReqDetailsFeeOutputModel () {
	this.Id = undefined;
	this.Code = undefined;
	this.TranDescription = undefined;
	this.TransDate = undefined;
	this.Reference = undefined;
	this.TransDocumentId = undefined;
	this.User = undefined;
	this.Amount = undefined;
	this.Type = undefined;
	return this;
}
 
function LeadReqDetailsDocOutputModel () {
	this.Id = undefined;
	this.FileName = undefined;
	this.FileNameWithExtension = undefined;
	this.Extension = undefined;
	this.UploadDate = undefined;
	this.FilePath = undefined;
	return this;
}
 
function LeadReqDetailsReqsOutputModel () {
	this.Id = undefined;
	this.LeadId = undefined;
	this.DocumentId = undefined;
	this.Description = undefined;
	this.DateReceived = undefined;
	this.IsApproved = undefined;
	this.ApprovalDate = undefined;
	this.IsOverridden = undefined;
	this.OverrideReason = undefined;
	return this;
}
 
function LeadReqDetailsOutputModel () {
	this.Id = undefined;
	this.LeadId = undefined;
	this.DocumentId = undefined;
	this.Description = undefined;
	this.DateReceived = undefined;
	this.IsApproved = undefined;
	this.ApprovalDate = undefined;
	this.IsOverridden = undefined;
	this.OverrideReason = undefined;
	this.DocumentHistory = undefined;
	this.FeeHistory = undefined;
	this.TestHistory = undefined;
	this.ReqsHistory = undefined;
	return this;
}
 
