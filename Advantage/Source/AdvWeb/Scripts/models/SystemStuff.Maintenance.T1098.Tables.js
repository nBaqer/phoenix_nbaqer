function ArchiveDto () {
	this.SchoolId = undefined;
	this.SchoolName = undefined;
	this.StuEnrollId = undefined;
	this.ExtractedDate = undefined;
	this.TaxYear = undefined;
	return this;
}
 
function ExclusionsDto () {
	this.StuAcctNum = undefined;
	this.FirstName = undefined;
	this.LastName = undefined;
	this.SocSecNum = undefined;
	this.Addr1 = undefined;
	this.City = undefined;
	this.State = undefined;
	this.ZipCode = undefined;
	this.ExclusionCode = undefined;
	return this;
}
 
function TransactionsDto () {
	this.StuAcctNum = undefined;
	this.TransDate = undefined;
	this.TransAmt = undefined;
	this.TransCode = undefined;
	this.IRSDataLine = undefined;
	this.PayType = undefined;
	this.Comment = undefined;
	return this;
}
 
function ValidEnrollmentsDto () {
	this.StuAcctNum = undefined;
	this.FirstName = undefined;
	this.LastName = undefined;
	this.SocSecNum = undefined;
	this.Addr1 = undefined;
	this.City = undefined;
	this.State = undefined;
	this.ZipCode = undefined;
	this.Line1Data = undefined;
	this.Line3Data = undefined;
	this.Line4Data = undefined;
	this.Line5Data = undefined;
	this.Line6Data = undefined;
	this.RfndGRs = undefined;
	this.RfndLNs = undefined;
	this.RfndXXs = undefined;
	this.PaidGRs = undefined;
	this.PaidSPs = undefined;
	this.PaidSHs = undefined;
	this.PaidLNs = undefined;
	this.GrossPaid = undefined;
	this.XHrsPerWeek = undefined;
	this.Line8Data = undefined;
	this.Corrected = undefined;
	this.LimitedByRemainingCost = undefined;
	this.TotalCost = undefined;
	this.PriorYrsPdAmt = undefined;
	this.UnpaidCosts = undefined;
	return this;
}
 
