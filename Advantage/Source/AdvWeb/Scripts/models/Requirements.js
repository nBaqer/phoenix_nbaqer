function LeadRequirementInputModel () {
	this.DocReqId = undefined;
	this.IsOverridden = undefined;
	this.overReason = undefined;
	this.Reason = undefined;
	this.DateReceived = undefined;
	this.IsApproved = undefined;
	this.ApprovalDate = undefined;
	this.LeadId = undefined;
	this.UserId = undefined;
	this.ReqId = undefined;
	this.RequirementType = undefined;
	this.Score = undefined;
	this.MinScore = undefined;
	this.TransCodeId = undefined;
	this.TransReference = undefined;
	this.TransDescription = undefined;
	this.TransAmount = undefined;
	this.TransDate = undefined;
	this.CampusId = undefined;
	this.PaymentReference = undefined;
	this.PayTypeId = undefined;
	this.PostTransactionId = undefined;
	this.PaymentTransactionId = undefined;
	return this;
}
 
function LeadRequirementsListOutputModel () {
	this.MandatoryRequirements = undefined;
	this.RequirementGroups = undefined;
	this.OptionalRequirements = undefined;
	return this;
}
 
function DocumentRequirementOutputModel () {
	this.Id = undefined;
	this.Code = undefined;
	this.Description = undefined;
	this.Type = undefined;
	this.RequiredFor = undefined;
	this.RequirementType = undefined;
	this.Status = undefined;
	this.Module = undefined;
	this.StartDate = undefined;
	this.EndDate = undefined;
	this.DateRequested = undefined;
	this.DateReceived = undefined;
	this.IsApproved = undefined;
	this.DocumentStatusId = undefined;
	this.DocumentStatusDescription = undefined;
	this.DocumentRequirementId = undefined;
	this.StudentId = undefined;
	this.StudentDocumentId = undefined;
	this.ShowUpdate = undefined;
	return this;
}
 
function EducationLevelOutputMap () {
	this.Code = undefined;
	this.Description = undefined;
	this.Status = undefined;
	this.CampusGroup = undefined;
	return this;
}
 
function GetDocumentRequirementInputModel () {
	this.StatusCode = undefined;
	this.CampusId = undefined;
	this.UserId = undefined;
	return this;
}
 
function RequirementEffectiveDatesOutputModel () {
	this.StartDate = undefined;
	this.EndDate = undefined;
	this.MinimumScore = undefined;
	this.IsMandatory = undefined;
	this.ValidDays = undefined;
	return this;
}
 
