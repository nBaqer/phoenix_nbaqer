function WapiLeadsOutputModel () {
	this.ProspectID = undefined;
	this.VendorName = undefined;
	this.VendorCode = undefined;
	this.CampaignId = undefined;
	this.AccountId = undefined;
	this.FirstName = undefined;
	this.LastName = undefined;
	this.Phone = undefined;
	this.PhoneType = undefined;
	this.Email = undefined;
	this.EmailType = undefined;
	this.Address1 = undefined;
	this.County = undefined;
	this.City = undefined;
	this.State = undefined;
	this.Zip = undefined;
	this.Country = undefined;
	this.ProgramOfInterest = undefined;
	this.DateOfContact = undefined;
	this.MiddleName = undefined;
	this.Address2 = undefined;
	this.Phone2 = undefined;
	this.Phone2Type = undefined;
	this.Email2 = undefined;
	this.Email2Type = undefined;
	this.SSN = undefined;
	this.BirthDate = undefined;
	this.Prefix = undefined;
	this.Suffix = undefined;
	this.GraduationYear = undefined;
	this.Comments = undefined;
	this.Source = undefined;
	this.LeadStatus = undefined;
	this.ExpStartDate = undefined;
	this.Gender = undefined;
	return this;
}
 
