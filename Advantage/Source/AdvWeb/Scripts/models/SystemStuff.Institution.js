function InstitutionContactModel () {
	this.Id = undefined;
	this.CampusId = undefined;
	this.InstitutionId = undefined;
	this.FirstName = undefined;
	this.LastName = undefined;
	this.MiddleName = undefined;
	this.StatusId = undefined;
	this.PrefixId = undefined;
	this.SuffixId = undefined;
	this.Suffix = undefined;
	this.Prefix = undefined;
	this.Status = undefined;
	this.Title = undefined;
	this.Phone = undefined;
	this.PhoneExtension = undefined;
	this.Email = undefined;
	this.IsDefault = undefined;
	this.UserId = undefined;
	this.IsForeignPhone = undefined;
	return this;
}
 
function InstitutionFilterInputModel () {
	this.Id = undefined;
	this.Name = undefined;
	this.UserId = undefined;
	this.CampusId = undefined;
	this.Level = undefined;
	this.InstitutionType = undefined;
	this.filter = undefined;
	return this;
}
 
function InstitutionInputModel () {
	this.Name = undefined;
	this.LevelId = undefined;
	this.TypeId = undefined;
	this.UserId = undefined;
	this.CampusId = undefined;
	return this;
}
 
function InstitutionOutputModel () {
	this.Id = undefined;
	this.Name = undefined;
	this.LevelId = undefined;
	this.TypeId = undefined;
	this.Addresses = undefined;
	this.Contacts = undefined;
	this.Phones = undefined;
	return this;
}
 
function InstitutionPhoneModel () {
	this.Id = undefined;
	this.InstitutionId = undefined;
	this.UserId = undefined;
	this.TypeId = undefined;
	this.Type = undefined;
	this.CampusId = undefined;
	this.StatusId = undefined;
	this.Phone = undefined;
	this.IsForeignPhone = undefined;
	return this;
}
 
function InstitutionAddressModel () {
	this.Id = undefined;
	this.CampusId = undefined;
	this.InstitutionId = undefined;
	this.TypeId = undefined;
	this.Address1 = undefined;
	this.Address2 = undefined;
	this.City = undefined;
	this.StateId = undefined;
	this.ZipCode = undefined;
	this.CountryId = undefined;
	this.StatusId = undefined;
	this.IsMailingAddress = undefined;
	this.UserId = undefined;
	this.State = undefined;
	this.IsInternational = undefined;
	this.CountyId = undefined;
	this.Country = undefined;
	this.County = undefined;
	this.Type = undefined;
	this.AddressApto = undefined;
	this.IsDefault = undefined;
	return this;
}
 
