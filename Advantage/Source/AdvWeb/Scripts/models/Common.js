function CatalogsInputModel () {
	this.FldName = undefined;
	this.CampusId = undefined;
	this.AdditionalFilter = undefined;
	return this;
}
 
function DropDownIntegerOutputModel () {
	this.ID = undefined;
	this.Description = undefined;
	return this;
}
 
function DropDownOutputModel () {
	this.ID = undefined;
	this.Description = undefined;
	return this;
}
 
function CampusOutput () {
	this.CampusId = undefined;
	return this;
}
 
function DropDownWithCampusOutputModel () {
	this.CampusesIdList = undefined;
	this.ID = undefined;
	this.Description = undefined;
	return this;
}
 
