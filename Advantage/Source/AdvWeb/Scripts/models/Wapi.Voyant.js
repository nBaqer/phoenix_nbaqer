function StudentAcademicOutputModel () {
	this.Code = undefined;
	this.Descrip = undefined;
	this.EndDate = undefined;
	this.Gpa = undefined;
	this.Grade = undefined;
	this.InstructorId = undefined;
	this.StartDate = undefined;
	return this;
}
 
function StudentEnrollmentOutputModel () {
	this.CampusId = undefined;
	this.EnrollDate = undefined;
	this.ModDate = undefined;
	this.ProgDescrip = undefined;
	this.StartDate = undefined;
	this.StatusCode = undefined;
	this.StuEnrollId = undefined;
	return this;
}
 
function StudentFacultyOutputModel () {
	this.FundSourceCode = undefined;
	this.GrossAmount = undefined;
	return this;
}
 
function VoyantStudentInputModel () {
	this.RequestHistorical = undefined;
	this.DashboardUrl = undefined;
	return this;
}
 
function VoyantStudentOutputModel () {
	this.CitizenGuid = undefined;
	this.FamilyIncoming = undefined;
	this.FirstName = undefined;
	this.Gender = undefined;
	this.LastName = undefined;
	this.MaritalStatus = undefined;
	this.Race = undefined;
	this.StudentId = undefined;
	this.AcademicsDataList = undefined;
	this.EnrollmentDataList = undefined;
	this.FacultyDataList = undefined;
	return this;
}
 
