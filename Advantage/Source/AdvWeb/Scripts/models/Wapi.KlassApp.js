function ConfigDto () {
	this.SchoolName = undefined;
	this.DisplayAttendanceUnitForProgressReportByClass = undefined;
	this.StudentIdentifier = undefined;
	this.GPAMethod = undefined;
	this.GradesFormat = undefined;
	this.TrackSapAttendance = undefined;
	this.GradeBookWeightingLevel = undefined;
	return this;
}
 
function SummaryDto () {
	this.SSN = undefined;
	this.FirstName = undefined;
	this.LastName = undefined;
	this.StatusCodeDescrip = undefined;
	this.StudentStartDate = undefined;
	this.StudentExpectedGraduationDate = undefined;
	this.PrgVerId = undefined;
	this.PrgVerDescrip = undefined;
	this.UnitTypeId = undefined;
	this.PrgVersionTrackCredits = undefined;
	this.UnitTypeDescripFrom_arAttUnitType = undefined;
	this.StudentLastDateAttended = undefined;
	this.WeeklySchedule = undefined;
	this.TotalDaysAttended = undefined;
	this.ScheduledDays = undefined;
	this.DaysAbsent = undefined;
	this.MakeupDays = undefined;
	this.StuEnrollId = undefined;
	this.CreditsEarned = undefined;
	this.FACreditsEarned = undefined;
	this.DateDetermined = undefined;
	this.InSchool = undefined;
	this.ProgDescrip = undefined;
	this.TotalCost = undefined;
	this.CurrentBalance = undefined;
	this.WeightedAverage_CumGPA = undefined;
	this.SimpleAverage_CumGPA = undefined;
	this.ContractedGradDate = undefined;
	this.StudentGroups = undefined;
	this.ClockHourProgram = undefined;
	this.UnitTypeDescrip = undefined;
	this.TransferHours = undefined;
	this.OverallAverage = undefined;
	return this;
}
 
function TranscriptDto () {
	this.ProgDescrip = undefined;
	this.PrgVerId = undefined;
	this.PrgVerDescrip = undefined;
	this.PrgVersionTrackCredits = undefined;
	this.ProgramVersionTypeId = undefined;
	this.DescriptionProgramVersionType = undefined;
	this.DegreeDescrip = undefined;
	this.OverallAverage = undefined;
	this.WeightedAverage_CumGPA = undefined;
	this.SimpleAverage_CumGPA = undefined;
	this.StudentStartDate = undefined;
	this.GraduantionDate = undefined;
	this.StatusDescrip = undefined;
	this.StudentLastDateAttended = undefined;
	this.UnitTypeDescripFrom_arAttUnitType = undefined;
	this.UnitTypeDescrip = undefined;
	this.TotalDaysAttended = undefined;
	this.MakeupDays = undefined;
	this.GPAMethod = undefined;
	this.GradesFormat = undefined;
	this.TrackSapAttendance = undefined;
	this.DisplayHours = undefined;
	this.MajorsMinorsConcentrations = undefined;
	return this;
}
 
