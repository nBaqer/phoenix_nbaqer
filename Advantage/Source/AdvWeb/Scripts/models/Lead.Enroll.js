function EnrollOutputModel () {
	this.EntranceInterviewDate = undefined;
	this.IsFirstTimeInSchool = undefined;
	this.IsFirstTimePostSecSchool = undefined;
	this.ShiftId = undefined;
	this.InternationalLead = undefined;
	this.Nationality = undefined;
	this.GeographicTypeId = undefined;
	this.DegCertSeekingId = undefined;
	this.ChargingMethodId = undefined;
	this.ProgramVersionType = undefined;
	this.LeadInfo = undefined;
	this.RequirementMet = undefined;
	this.EnrollmentDate = undefined;
	this.ExpectedStartDate = undefined;
	this.StartDate = undefined;
	this.MidPointDate = undefined;
	this.ContractedGradDate = undefined;
	this.TransferDate = undefined;
	this.TuitionCategory = undefined;
	this.FinAidAdvisor = undefined;
	this.AcademicAdvisor = undefined;
	this.BadgeNumber = undefined;
	this.TransferHours = undefined;
	this.FutureStartStatusMapped = undefined;
	this.NotMappedStatus = undefined;
	this.IsDisabled = undefined;
	this.StudentNumber = undefined;
	this.StudentStatus = undefined;
	this.StuEnrollId = undefined;
	this.StudentId = undefined;
	this.EnrollId = undefined;
	this.EducationLevel = undefined;
	this.EnrollStateId = undefined;
	this.FullName = undefined;
	this.DisableEnrolLeadStatus = undefined;
	return this;
}
 
