function ExtraInfo () {
	this.Id = undefined;
	this.Description = undefined;
	this.Comment = undefined;
	this.Level = undefined;
	this.Group = undefined;
	return this;
}
 
function ExtraInfoInputModel () {
	this.LeadId = undefined;
	this.CampusId = undefined;
	this.UserId = undefined;
	this.Command = undefined;
	return this;
}
 
function IExtraInfoOutputModel () {
	this.ExtraInfoList = undefined;
	this.LevelsItemsList = undefined;
	this.GroupItemsList = undefined;
	this.Filter = undefined;
	return this;
}
 
function ExtraInfoOutputModel () {
	this.ExtraInfoList = undefined;
	this.LevelsItemsList = undefined;
	this.GroupItemsList = undefined;
	this.Filter = undefined;
	return this;
}
 
