function EmployeeContactOutputModel () {
	this.WorkPhone = undefined;
	return this;
}
 
function EmployeeMruOutputModel () {
	this.Id = undefined;
	this.FullName = undefined;
	this.Address = undefined;
	this.City = undefined;
	this.State = undefined;
	this.Zip = undefined;
	this.CityStateZip = undefined;
	this.WorkPhone = undefined;
	this.MruModDate = undefined;
	this.ModDate = undefined;
	this.Campus = undefined;
	this.Department = undefined;
	this.Status = undefined;
	this.ImageUrl = undefined;
	this.NotFoundImageUrl = undefined;
	this.SearchCampusId = undefined;
	return this;
}
 
function EmployeeSearchOutputModel () {
	this.Id = undefined;
	this.FullName = undefined;
	this.Field1 = undefined;
	this.Field2 = undefined;
	this.Field3 = undefined;
	this.Field4 = undefined;
	this.Field5 = undefined;
	this.ImageUrl = undefined;
	this.NotFoundImageUrl = undefined;
	this.SearchCampusId = undefined;
	return this;
}
 
