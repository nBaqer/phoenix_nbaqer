function GetProgramVersionInputModel () {
	this.StatusCode = undefined;
	return this;
}
 
function ProgramVersionOutputModel () {
	this.Id = undefined;
	this.Code = undefined;
	this.Description = undefined;
	this.CampGrpId = undefined;
	this.Status = undefined;
	this.CampusGroup = undefined;
	return this;
}
 
