function IReportOutputModel () {
	this.ReportInfo = undefined;
	this.ReportServerName = undefined;
	this.SsrsHtmlPage = undefined;
	this.Filter = undefined;
	return this;
}
 
function ReportInformation () {
	this.Id = undefined;
	this.Name = undefined;
	this.Address = undefined;
	this.ParentId = undefined;
	this.Description = undefined;
	this.Hidden = undefined;
	this.Type = undefined;
	this.ChildrenList = undefined;
	return this;
}
 
function ReportInputModel () {
	this.Command = undefined;
	this.Directory = undefined;
	return this;
}
 
function ReportOutputModel () {
	this.ReportInfo = undefined;
	this.ReportServerName = undefined;
	this.SsrsHtmlPage = undefined;
	this.Filter = undefined;
	return this;
}
 
