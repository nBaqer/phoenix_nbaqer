function LeadInfoToolInputModel () {
	this.UserId = undefined;
	this.LeadAdmissionDataTime = undefined;
	this.LeadId = undefined;
	this.CampusId = undefined;
	this.Command = undefined;
	return this;
}
 
function LeadInfoToolOutputModel () {
	this.FlagLead = undefined;
	this.FlagAppointment = undefined;
	this.FlagTask = undefined;
	this.OkChangeLeadOperation = undefined;
	return this;
}
 
