function GetStudentGroupsInputModel () {
	this.StatusCode = undefined;
	this.CampusId = undefined;
	return this;
}
 
function StudentGroupOutputModel () {
	this.Id = undefined;
	this.Description = undefined;
	this.Code = undefined;
	this.UseForScheduling = undefined;
	this.CampGrpId = undefined;
	this.Status = undefined;
	this.CampusGroup = undefined;
	return this;
}
 
