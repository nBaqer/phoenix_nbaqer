function ClassAppConfigurationOutputModel () {
	this.Id = undefined;
	this.AdvantageIdentification = undefined;
	this.KlassAppIdentification = undefined;
	this.CodeEntity = undefined;
	this.CodeOperation = undefined;
	this.IsCustomField = undefined;
	this.IsActive = undefined;
	this.ItemStatus = undefined;
	this.ItemValue = undefined;
	this.ItemLabel = undefined;
	this.ItemValueType = undefined;
	this.CreationDate = undefined;
	this.ModDate = undefined;
	this.ModUser = undefined;
	this.LastOperationLog = undefined;
	return this;
}
 
function ClassAppInputModel () {
	this.Command = undefined;
	this.UserId = undefined;
	return this;
}
 
function ClassAppUiConfigurationOutputModel () {
	this.ConfigurationSetting = undefined;
	this.Filter = undefined;
	this.IsAnyConfigurationPending = undefined;
	return this;
}
 
function IClassAppConfigurationOutputModel () {
	this.AdvantageIdentification = undefined;
	this.CodeEntity = undefined;
	this.CodeOperation = undefined;
	this.CreationDate = undefined;
	this.IsActive = undefined;
	this.IsCustomField = undefined;
	this.ItemLabel = undefined;
	this.ItemStatus = undefined;
	this.ItemValue = undefined;
	this.ItemValueType = undefined;
	this.KlassAppIdentification = undefined;
	this.ModDate = undefined;
	this.ModUser = undefined;
	return this;
}
 
function IClassAppUiConfigurationOutputModel () {
	this.ConfigurationSetting = undefined;
	this.Filter = undefined;
	this.IsAnyConfigurationPending = undefined;
	return this;
}
 
