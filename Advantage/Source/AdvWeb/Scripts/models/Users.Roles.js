function GetRolesInputModel () {
	this.CampusId = undefined;
	this.ResourceId = undefined;
	return this;
}
 
function ResourceLevelOutputModel () {
	this.RoleId = undefined;
	this.AccessLevel = undefined;
	this.ResourceId = undefined;
	this.ModDate = undefined;
	this.ModUser = undefined;
	this.ParentId = undefined;
	this.Resource = undefined;
	return this;
}
 
function ResourceOutputModel () {
	this.Id = undefined;
	this.Resource = undefined;
	this.ResourceUrl = undefined;
	this.DisplayName = undefined;
	this.AccessLevel = undefined;
	this.ModDate = undefined;
	this.ModUser = undefined;
	this.ResourceLevels = undefined;
	return this;
}
 
function RolesOutputModel () {
	this.Id = undefined;
	this.Role = undefined;
	this.Code = undefined;
	this.Status = undefined;
	this.ModDate = undefined;
	this.ModUser = undefined;
	this.SystemRole = undefined;
	this.ResourceLevels = undefined;
	return this;
}
 
function SystemRolesOutputModel () {
	this.Description = undefined;
	this.Status = undefined;
	this.ModDate = undefined;
	this.ModUser = undefined;
	return this;
}
 
function UserRolesCampusGroupBridgeOutputModel () {
	this.User = undefined;
	this.Role = undefined;
	this.CampusGroup = undefined;
	this.ModDate = undefined;
	this.ModUser = undefined;
	return this;
}
 
