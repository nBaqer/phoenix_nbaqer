function AttendanceInputModel () {
	this.StuEnrollmentId = undefined;
	this.ClsSectionGuid = undefined;
	this.IsSummary = undefined;
	this.IsDaily = undefined;
	this.FilterIni = undefined;
	this.FilterEnd = undefined;
	this.CampusId = undefined;
	this.UserIdGuid = undefined;
	return this;
}
 
function BasicAttendanceModel () {
	this.AttendanceGuid = undefined;
	this.ClassSectionMeetingGuid = undefined;
	this.AttendanceDate = undefined;
	this.PeriodId = undefined;
	this.PeriodDescription = undefined;
	return this;
}
 
function AttendanceMinutesOutputModel () {
	this.Schedule = undefined;
	this.Absent = undefined;
	this.MakeUp = undefined;
	this.Actual = undefined;
	this.Tardies = undefined;
	this.Excused = undefined;
	this.AttendanceGuid = undefined;
	this.ClassSectionMeetingGuid = undefined;
	this.AttendanceDate = undefined;
	this.PeriodId = undefined;
	this.PeriodDescription = undefined;
	return this;
}
 
function AttendancePresentAbsentOutputModel () {
	this.StatusEnum = undefined;
	this.AttendanceGuid = undefined;
	this.ClassSectionMeetingGuid = undefined;
	this.AttendanceDate = undefined;
	this.PeriodId = undefined;
	this.PeriodDescription = undefined;
	return this;
}
 
function AttendanceSummaryEnrollmentOutput () {
	this.PercentagePresent = undefined;
	this.ScheduleHours = undefined;
	this.SummaryEnrollmentTableList = undefined;
	this.SummaryEnrollmentStatusList = undefined;
	this.SummaryEnrollmentLoaList = undefined;
	this.SummarySuspensionList = undefined;
	return this;
}
 
function SummaryEnrollmentTable () {
	this.LabelField = undefined;
	this.Present = undefined;
	this.Absent = undefined;
	this.Tardy = undefined;
	this.Excused = undefined;
	this.Makeup = undefined;
	return this;
}
 
function SpecialAttendanceTable () {
	this.DateIni = undefined;
	this.DateEnd = undefined;
	this.DateRequested = undefined;
	this.StatusReason = undefined;
	return this;
}
 
function CourseClassInputModel () {
	this.TermId = undefined;
	this.StuEnrollmentId = undefined;
	return this;
}
 
function CourseClassOutputModel () {
	this.ClassSectionGuid = undefined;
	this.RequerimentGuid = undefined;
	this.RequerimentDescription = undefined;
	this.UnitTypeDescription = undefined;
	this.UnitTypeGuid = undefined;
	this.IsTrackTardies = undefined;
	this.TardiesMakingAbsence = undefined;
	return this;
}
 
function EnrollmentAttendanceOutputModel () {
	this.ID = undefined;
	this.Description = undefined;
	this.TardiesMakingAusence = undefined;
	this.TrackTardies = undefined;
	this.AttendanceType = undefined;
	return this;
}
 
function EnrollmentOutputModel () {
	this.StudentId = undefined;
	this.EnrollmentDate = undefined;
	this.CampusId = undefined;
	this.EnrollmentStatusId = undefined;
	this.EnrollmentId = undefined;
	this.StartDate = undefined;
	this.Student = undefined;
	this.ProgramVersion = undefined;
	this.EnrollmentStatus = undefined;
	return this;
}
 
function EnrollmentTransactionPayPeriodOutputModel () {
	this.LastName = undefined;
	this.FirstName = undefined;
	this.FullName = undefined;
	this.StudentNumber = undefined;
	this.PeriodNumber = undefined;
	this.ChargeAmount = undefined;
	this.TranDate = undefined;
	return this;
}
 
function EnrollmentTransactionPayPeriodSummaryOutputModel () {
	this.ChargeAmountSum = undefined;
	this.TransDate = undefined;
	this.TDate = undefined;
	this.ChargeAmountSumString = undefined;
	this.date = undefined;
	return this;
}
 
function EnrollmentTransactionProjectedOutputModel () {
	this.PmtPeriodId = undefined;
	this.StudentId = undefined;
	this.StuEnrollId = undefined;
	this.LastName = undefined;
	this.FirstName = undefined;
	this.FullName = undefined;
	this.ProgramVersionDescription = undefined;
	this.ProgramVersionCode = undefined;
	this.CampusDescription = undefined;
	this.ChargeAmount = undefined;
	this.CreditsHoursValue = undefined;
	this.CumulativeValue = undefined;
	this.CreditHoursLeft = undefined;
	this.Threshhold = undefined;
	this.IncrementType = undefined;
	this.InputIncrementType = undefined;
	return this;
}
 
function EnrollmentTransactionProjectedSummaryOutputModel () {
	this.CreditsHoursLeft = undefined;
	this.Amount = undefined;
	this.CumulativeAmount = undefined;
	this.CumulativeDifference = undefined;
	return this;
}
 
function GetEnrollmentTransactionInputModel () {
	this.FromDate = undefined;
	this.ToDate = undefined;
	this.UserId = undefined;
	this.CampusId = undefined;
	this.SummaryType = undefined;
	return this;
}
 
function GetEnrollmentTransactionProjectedInputModel () {
	this.ThreshValue = undefined;
	this.IncrementType = undefined;
	return this;
}
 
function TransactionCodeOutputModel () {
	this.Id = undefined;
	this.Description = undefined;
	return this;
}
 
function PostAttendanceInputModel () {
	this.IdGuid = undefined;
	this.ClassSectionMeetingGuid = undefined;
	this.MeetDate = undefined;
	this.EnrollmentGuid = undefined;
	this.ClsSectionIdGuid = undefined;
	this.PeriodIdGuid = undefined;
	this.StatusEnum = undefined;
	this.IsTardy = undefined;
	this.IsExcused = undefined;
	this.MakeUp = undefined;
	this.Actual = undefined;
	this.Schedule = undefined;
	return this;
}
 
