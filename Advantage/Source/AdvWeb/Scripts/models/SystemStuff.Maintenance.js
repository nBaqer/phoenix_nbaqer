function T1098FameInputModel () {
	this.ListStudentDto = undefined;
	this.Year = undefined;
	return this;
}
 
function SchoolLogoInputModel () {
	this.ImageId = undefined;
	this.ImageCode = undefined;
	return this;
}
 
function SchoolLogoOutputModel () {
	this.ID = undefined;
	this.ImagenBytes = undefined;
	this.ImgLenth = undefined;
	this.ContentType = undefined;
	this.OfficialUse = undefined;
	this.ImageCode = undefined;
	this.Description = undefined;
	this.ImageFile = undefined;
	return this;
}
 
function T1098Dto () {
	this.StuEnrollId = undefined;
	this.EnrollmentId = undefined;
	this.StartDate = undefined;
	this.LDA = undefined;
	this.IsStudentNoStart = undefined;
	this.TransDate = undefined;
	this.FirstName = undefined;
	this.LastName = undefined;
	this.SSN = undefined;
	this.Address1 = undefined;
	this.City = undefined;
	this.State = undefined;
	this.Zip = undefined;
	this.TotalCost = undefined;
	this.TotalPaid = undefined;
	this.PriorYearPayments = undefined;
	this.PriorYearRefunds = undefined;
	this.GrossPayments = undefined;
	this.GrossRefunds = undefined;
	this.GrantRefunds = undefined;
	this.LoanRefunds = undefined;
	this.StudentRefunds = undefined;
	this.GrantPayments = undefined;
	this.LoanPayments = undefined;
	this.Scholarships = undefined;
	this.IsContinuingEd = undefined;
	this.StudentPayments = undefined;
	this.CostForEnrollmentExist = undefined;
	return this;
}
 
function T1098InputModel () {
	this.UserId = undefined;
	this.CampusId = undefined;
	this.Year = undefined;
	return this;
}
 
function T1098OutputModel () {
	this.NumberOfStudentProcessed = undefined;
	this.Message = undefined;
	return this;
}
 
function WapiGetDateTimeOutputModel () {
	this.Id = undefined;
	this.Value = undefined;
	return this;
}
 
function WapiLoggerInputModel () {
	this.WapiOperationCode = undefined;
	this.BeginDate = undefined;
	this.EndDate = undefined;
	this.LogType = undefined;
	return this;
}
 
function WapiLoggerOutputModel () {
	this.Id = undefined;
	this.ServiceCode = undefined;
	this.CompanyCode = undefined;
	this.DateExecution = undefined;
	this.NextPlanningDate = undefined;
	this.IsError = undefined;
	this.Comment = undefined;
	return this;
}
 
function WapiOnDemandFlagsInputModel () {
	this.Id = undefined;
	this.Value = undefined;
	return this;
}
 
function WapiOnDemandFlagsOutputModel () {
	this.Id = undefined;
	this.OnDemandOperation = undefined;
	this.Result = undefined;
	this.Error = undefined;
	return this;
}
 
function WapiOperationSettingOutputModel () {
	this.ID = undefined;
	this.CodeOperation = undefined;
	this.CodeExtCompany = undefined;
	this.DescripExtCompany = undefined;
	this.CodeExtOperationMode = undefined;
	this.DescripExtOperationMode = undefined;
	this.CodeWapiServ = undefined;
	this.DescripWapiServ = undefined;
	this.SecondCodeWapiServ = undefined;
	this.SecondDescWapiServ = undefined;
	this.UrlWapiServ = undefined;
	this.IsActiveWapiServ = undefined;
	this.ConsumerKey = undefined;
	this.PrivateKey = undefined;
	this.OperationSecInterval = undefined;
	this.PollSecOnDemandOperation = undefined;
	this.FlagOnDemandOperation = undefined;
	this.FlagRefreshConfig = undefined;
	this.ExternalUrl = undefined;
	this.DateLastExecution = undefined;
	this.IsActiveOperation = undefined;
	return this;
}
 
function WapiOperationSettingsInputModel () {
	this.IdOperation = undefined;
	this.OnlyActives = undefined;
	this.OperationMode = undefined;
	this.CompanyCode = undefined;
	return this;
}
 
function WapiSetDateTimeInputModel () {
	this.Id = undefined;
	this.Value = undefined;
	return this;
}
 
function WapiVendorsCampaignLeadOutputModel () {
	this.ID = undefined;
	this.VendorId = undefined;
	this.CampaignCode = undefined;
	this.AccountId = undefined;
	this.IsActive = undefined;
	this.DateCampaignBegin = undefined;
	this.DateCampaignEnd = undefined;
	this.Cost = undefined;
	this.FilterRejectByCounty = undefined;
	this.FilterRejectDuplicates = undefined;
	this.PayFor = undefined;
	return this;
}
 
function WapiVendorsLeadOutputModel () {
	this.ID = undefined;
	this.VendorName = undefined;
	this.VendorCode = undefined;
	this.IsActive = undefined;
	this.Description = undefined;
	this.DateOperationBegin = undefined;
	this.DateOperationEnd = undefined;
	return this;
}
 
function WapiWindowsServiceInputModel () {
	this.ServiceName = undefined;
	this.Command = undefined;
	return this;
}
 
