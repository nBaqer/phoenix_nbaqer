function GetUsersInputModel () {
	this.CampusId = undefined;
	this.UserId = undefined;
	this.Active = undefined;
	this.ReturnSupport = undefined;
	this.ReturnReps = undefined;
	return this;
}
 
function UserOutputModel () {
	this.Id = undefined;
	this.UserName = undefined;
	this.Email = undefined;
	this.CampusGroup = undefined;
	this.AccountActive = undefined;
	this.UserAndName = undefined;
	this.FullName = undefined;
	this.ActiveLeadCount = undefined;
	return this;
}
 
