function GetDocumentStatusInputModel () {
	this.IsApproved = undefined;
	return this;
}
 
function PostDocumentInputModel () {
	this.DocumentStatusId = undefined;
	this.IsApproved = undefined;
	this.DateRequested = undefined;
	this.DateReceived = undefined;
	return this;
}
 
function PostDocumentOutputModel () {
	this.Id = undefined;
	this.DateRequested = undefined;
	this.DateReceived = undefined;
	this.DateModified = undefined;
	this.IsApproved = undefined;
	return this;
}
 
function GetDocumentInputModel () {
	this.DocumentStatusId = undefined;
	this.IsApproved = undefined;
	this.Page = undefined;
	this.PageSize = undefined;
	this.CampusId = undefined;
	this.ProgramVersionId = undefined;
	this.EnrollmentStatusId = undefined;
	this.StudentGroupId = undefined;
	this.StartDate = undefined;
	this.ShowLastValue = undefined;
	this.ModifiedDate = undefined;
	this.FileUploaded = undefined;
	this.UserId = undefined;
	return this;
}
 
function PutDocumentInputModel () {
	this.DateRequested = undefined;
	this.DateReceived = undefined;
	this.DocumentStatusId = undefined;
	this.IsApproved = undefined;
	return this;
}
 
function DocumentOutputModel () {
	this.Id = undefined;
	this.FirstName = undefined;
	this.MiddleName = undefined;
	this.LastName = undefined;
	this.FullName = undefined;
	this.SystemModuleName = undefined;
	this.RequiredFor = undefined;
	this.DateRequested = undefined;
	this.DateReceived = undefined;
	this.DateModified = undefined;
	this.ModUser = undefined;
	this.DocumentName = undefined;
	this.Student = undefined;
	this.DocumentStatus = undefined;
	this.Files = undefined;
	this.Count = undefined;
	this.IsApproved = undefined;
	this.ShowUpdate = undefined;
	return this;
}
 
