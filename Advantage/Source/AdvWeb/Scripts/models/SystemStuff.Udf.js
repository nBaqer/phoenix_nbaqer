function IUdfOutputModel () {
	this.EntityList = undefined;
	this.AssignedToPageUdfList = undefined;
	this.UnAssignedToPageUdfList = undefined;
	this.Filter = undefined;
	return this;
}
 
function UdfDto () {
	this.Id = undefined;
	this.Description = undefined;
	this.Position = undefined;
	this.SdfVisibility = undefined;
	return this;
}
 
function UdfInputModel () {
	this.Command = undefined;
	this.LeadEntity = undefined;
	this.PageResourceId = undefined;
	this.UserId = undefined;
	return this;
}
 
function UdfOutputModel () {
	this.EntityList = undefined;
	this.AssignedToPageUdfList = undefined;
	this.UnAssignedToPageUdfList = undefined;
	this.Filter = undefined;
	return this;
}
 
