﻿$(document).ready(function () {
    // Get the Current campus from the hiddent Field
    var campusId = $("#hdnCampusId").val();
    // Initialize the Page calling the default class from the typescript SDK.
    var studentTermination = new API.ViewModels.StudentTermination(campusId);
    $('span.k-icon.k-clear-value.k-i-close').attr('title', "Clear");

    //Validation to allow input the following characters: hyphen, apostrophe along with Alphabets and numbers and space
    $('#txtStudentSearch').keypress(function (e) {
        var regex = new RegExp(/[a-zA-Z0-9\b\0\-\ ']/g);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if ($(this).val().length > 151) {
            event.preventDefault();
            return false;
        }
        else
            return regex.test(str);
    });

    //Validating texbox for numeric only
    $("#txtScheduledHours, #txtTicket, #txtNoOfDays").keypress(function (event) {
        var $this = $(this);
        if (((event.which < 48 || event.which > 57) &&
            (event.which !== 0 && event.which !== 8))) {
            event.preventDefault();
        }
    });

    //Validating texbox for numeric only
    $("#txtTotalHours").keypress(function (event) {
        var $this = $(this);
        if (((event.which < 48 || event.which > 57) &&
            (event.which !== 0 && event.which !== 8))) {
            event.preventDefault();
        }
    });

    //comma to be added to every third numeric character
    function addCommas(inputNum) {
        if (inputNum != "") {
            var cc = parseFloat(inputNum.replace(/,/g, '')).toFixed(2);
            while (/\d{4}/.test(cc)) {
                cc = cc.replace(/(\d{3}[,.])/, ',$1');
            }
            return cc;
        }
    }

    $('.money').keypress(function (event) {
        var $this = $(this);
        if ((event.which !== 46 || $this.val().indexOf('.') !== -1) &&
            ((event.which < 48 || event.which > 57) &&
                (event.which !== 0 && event.which !== 8))) {
            event.preventDefault();
        }

        var text = $(this).val();
        if ((event.which === 46) && (text.indexOf('.') === -1)) {
            setTimeout(function () {
                if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                }
            }, 1);
        }

        if ((text.indexOf('.') !== -1) &&
            (text.substring(text.indexOf('.')).length > 2) &&
            (event.which !== 0 && event.which !== 8) &&
            ($(this)[0].selectionStart >= text.length - 2)) {
            event.preventDefault();
        }
    });
    // function to postfix 00 after decimal point
    $('.money').blur(function (event) {
        var $this = $(this);
        var text = $(this).val();
        $this.val(addCommas(text));
        text = $(this).val();
        if (text !== '' && (text.indexOf('.') === -1))
            $this.val(text.concat('.00'));
        if (text.indexOf('.') !== -1) {
            var decimalPosition = $this.val().indexOf('.') + 1;
            if (text.substring(decimalPosition, decimalPosition + 2).length === 1) {
                $this.val(text.concat('0'));
            }
            if (text.substring(decimalPosition, decimalPosition + 2).length === 0) {
                $this.val(text.concat('00'));
            }
        }
        if (text.indexOf('.') === 0) {
            var zeroPrefixedText = '0' + $this.val();
            $this.val(zeroPrefixedText);
        }
    });

    //funtion to allow upto three decimal only
    $("#txtCreditActual").blur(function (event) {
        var text = $(this).val();
        if (text !== '' && (text.indexOf('.') === -1)) {
        var amt = (parseFloat(this.value));
        $(this).val(addCommas(amt.toFixed(3)));
    }
    });

    //funtion to prevent characters and not to enter more than three decimal
    $("#txtCreditActual").keypress(function (event) {
        var $this = $(this);
        if ((event.which !== 46 || $this.val().indexOf('.') !== -1) &&
        ((event.which < 48 || event.which > 57) &&
            (event.which !== 0 && event.which !== 8))) {
            event.preventDefault();
        }

        var text = $(this).val();
        if ((event.which === 46) && (text.indexOf('.') === -1)) {
            setTimeout(function () {
                if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                }
            }, 1);
        }

        if ((text.indexOf('.') !== -1) &&
            (text.substring(text.indexOf('.')).length > 3) &&
            (event.which !== 0 && event.which !== 8) &&
            ($(this)[0].selectionStart >= text.length - 3)) {
            event.preventDefault();
        }
    });

    //funtion to allow upto one decimal only
    $("#txtCreditAttendencePerc, #txtBoxH, #txt5BoxH, #txt5BoxM, #txt5BoxMResult").blur(function (event) {
        var text = $(this).val();
        if (text !== '' && (text.indexOf('.') === -1)) {
            var amt = (parseFloat(this.value));
            $(this).val(amt.toFixed(1));            
        }
    });
   

    //funtion to allow upto two decimal only
    $("#txtBox9Result").blur(function (event) {
        var text = $(this).val();
        if (text !== '' && (text.indexOf('.') === -1)) {
            var amt = (parseFloat(this.value));
            $(this).val(addCommas(amt.toFixed(2)));
        }
    });

    //funtion to prevent characters and not to enter more than one decimal
    $("#txtCreditAttendencePerc, #txtBoxH, #txt5BoxH, #txt5BoxM, #txt5BoxMResult").keypress(function (event) {
        var $this = $(this);
        if ((event.which !== 46 || $this.val().indexOf('.') !== -1) &&
            ((event.which < 48 || event.which > 57)) &&
            (event.which !== 0 && event.which !== 8))  {
            event.preventDefault();
        }

        var text = $(this).val();
        if ((event.which === 46) && (text.indexOf('.') === -1)) {
            setTimeout(function () {
                if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                }
            }, 1);
        }

        if ((text.indexOf('.') !== -1) &&
            (text.substring(text.indexOf('.')).length > 1) &&
            (event.which !== 0 && event.which !== 8) &&
            ($(this)[0].selectionStart >= text.length - 1)) {
            event.preventDefault();
        }
    });

    //funtion to prevent characters and not to enter more than two decimal and allow negative.
    $("#txtBox9Result").keypress(function (event) {
        var $this = $(this);
        if ((event.which !== 46 || $this.val().indexOf('.') !== -1) && (event.which !== 45 || $this.val().indexOf('-') !== -1) &&
        ((event.which < 48 || event.which > 57)) &&
            (event.which !== 0 && event.which !== 8)) {
            event.preventDefault();
        }

        var text = $(this).val();
        if ((event.which === 46) && (text.indexOf('.') === -1)) {
            setTimeout(function () {
                if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                }
            }, 1);
        }

        if ((text.indexOf('.') !== -1) &&
            (text.substring(text.indexOf('.')).length > 2) &&
            (event.which !== 0 && event.which !== 8) &&
            ($(this)[0].selectionStart >= text.length - 2)) {
            event.preventDefault();
        }
    });
});

$('input[type=text]').keydown(function (e) {
    if (e.ctrlKey && e.key === 'z') {
        e.preventDefault();
    }
});