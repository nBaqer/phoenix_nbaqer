﻿$(document).ready(function () { 
    var campusId = $("#hdnCampusId").val();
    var studentTermination = new API.ViewModels.AcademicRecords.UndoTermination.UndoStudentTermination(campusId);
    $('span.k-icon.k-clear-value.k-i-close').attr('title', "Clear");

    //Validation to allow input the following characters: hyphen, apostrophe along with Alphabets and numbers and space
    $('#txtUndoStudentSearch').keypress(function (e) {
        var regex = new RegExp(/[a-zA-Z0-9\b\0\-\ ']/g);
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if ($(this).val().length > 151) {
            event.preventDefault();
            return false;
        }
        else
            return regex.test(str);
    });
});