﻿// Constants
// Constants...........................
var ServiceAPIEnrollment = '';
var CampusId = '';

// View Model....
var TranscriptVm = kendo.observable({
    dataSource: new kendo.data.DataSource
    ({
        schema: {
            model: {
                id: "ID",
                description: "Description"
            }
        }  
    })
    });

// Page Ready Event
function TranscriptDocumentReadyk(serviceUrl, campusId) {

    ServiceAPIEnrollment = serviceUrl;
    CampusId = campusId;
    // DropDownList Code ......................................
    $("#EnrollmentComboBox").DropDownList(
        {
            dataTextField: "Description",
            dataValueField: "ID",
            //enable: MasterCampusViewModel.dropdowlistEnable,
            dataSource: TranscriptVm.dataSource,
            index: 0
        });

    kendo.bind($("#EnrollmentComboBox"), TranscriptVm);
    AjaxGetenrollmentDropDownList(ManageResponse, ShowError, CompleteOperation);
};

// Ajax DataSource callers
// 
function AjaxGetenrollmentDropDownList(callback, error, complete) {
    $.ajax({
        type: "GET",
        processData: false,
        cache: true,
        contentType: "application/json",
        timeout: 25000,
        url: ServiceAPIEnrollment,
        dataType: "json",
        data: 'CampusID=' + CampusId,
        success: function (data) { callback(data); },
        error: function (e) { error(e); },
        complete: function (e) { complete(e); }
    });

}

function ManageResponse(data) {
    for (x in data) {
        //alert(data[x].ID + data[x].Description);
        TranscriptVm.dataSource.fetch(function() {
            TranscriptVm.dataSource.add({ ID: data[x].ID, Description: data[x].Description });
            TranscriptVm.dataSource.sync();
        });
    }

    //var dropdownlist = $("#EnrollmentComboBox").data("kendoDropDownList");
    //dropdownlist.select(function (dataItem) {
    //    return dataItem.ID === MasterCampusViewModel.campusId
};

function ShowError(e) {
    var status = e.status;
    var statusText = e.statusText;

    //var responseText = e.responseText;

    $.when(kendo.ui.ExtAlertDialog.show({
        title: "Wep API Services Error",
        message: "Status " + status + ": " + statusText,
        icon: "k-ext-error"
    })
    );
};

function CompleteOperation(e) {
    //StopTimeOut = true;
    //window.setTimeout(CompleteDelayed, 100);
}


