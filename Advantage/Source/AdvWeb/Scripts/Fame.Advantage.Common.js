var API;
(function (API) {
    var Enums;
    (function (Enums) {
        var DayOfWeek;
        (function (DayOfWeek) {
            DayOfWeek[DayOfWeek["Sunday"] = 0] = "Sunday";
            DayOfWeek[DayOfWeek["Monday"] = 1] = "Monday";
            DayOfWeek[DayOfWeek["Tuesday"] = 2] = "Tuesday";
            DayOfWeek[DayOfWeek["Wednesday"] = 3] = "Wednesday";
            DayOfWeek[DayOfWeek["Thursday"] = 4] = "Thursday";
            DayOfWeek[DayOfWeek["Friday"] = 5] = "Friday";
            DayOfWeek[DayOfWeek["Saturday"] = 6] = "Saturday";
        })(DayOfWeek = Enums.DayOfWeek || (Enums.DayOfWeek = {}));
    })(Enums = API.Enums || (API.Enums = {}));
})(API || (API = {}));
/// <reference path="../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" /> 
/// <reference path="../../../AdvWeb/Kendo/typescript/jquery.d.ts" />
/**
 * We are calling the Module components to avoid confusion with ASP Net Server Side User Controls.
 * Components are Client Side Type Script User Controls that connects to the API using JWT Authorization.
 */
var Api;
(function (Api) {
    var Components;
    (function (Components) {
        var DropDownList = (function () {
            function DropDownList(element, dataProvider, filterParams, settings, onchange, dataProviderFunctionName, selectValue) {
                /**
                 * Declaring the UserType API object. The UserType API encapsulate the User Type Controller.
                 * We will use the UserType API object to get the kendo data source for the UserType dropdown.
                 */
                var that = this;
                /**
                 * In this step we are concatenating the # with the component id so we can find the HTML control on the using jquery and them with the
                 * jquery reference to that DOM object the kendo UI control wil be created.
                 */
                if (typeof element === 'string') {
                    that.componentId = "#" + element;
                    that.element = $(that.componentId);
                }
                else {
                    that.element = element;
                }
                /**
                 * storing the component id in the global variable scope so it can be referenced elsewhere
                 */
                that.dataProvider = dataProvider;
                that.filterParams = filterParams;
                that.hasCustomFunctionName = dataProviderFunctionName !== undefined;
                if (that.hasCustomFunctionName) {
                    that.dataProviderFunctionName = dataProviderFunctionName;
                }
                var defaults = {
                    dataSource: dataProvider.getDataSource(function () { return that.onRequestFilterData(); }, null, that.hasCustomFunctionName ? dataProviderFunctionName : null),
                    optionLabel: "Select",
                    dataTextField: "text",
                    dataValueField: "value",
                };
                if (settings) {
                    $.extend(defaults, settings.settings);
                }
                /**
                 * declaration of the kendo ui control.
                 */
                that.element.kendoDropDownList(defaults).change(function (value) {
                    if (value)
                        that.selectedId = that.element.data("kendoDropDownList").value();
                    if (onchange)
                        onchange(value);
                });
                that.dropDownList = that.element.data("kendoDropDownList");
                that.dropDownList.value(selectValue);
            }
            DropDownList.prototype.onRequestFilterData = function () {
                var filter = {};
                if (this.filterParams) {
                    filter = $.extend(filter, this.filterParams.params);
                }
                return filter;
            };
            DropDownList.prototype.setParameters = function (filterParams) {
                this.filterParams = filterParams;
            };
            DropDownList.prototype.getSelected = function () {
                return this.selectedId;
            };
            DropDownList.prototype.fetch = function (func) {
                return this.dropDownList.dataSource.fetch(func);
            };
            DropDownList.prototype.reset = function () {
                this.dropDownList.value("");
                this.dropDownList.trigger("change");
            };
            DropDownList.prototype.reload = function (filterParams) {
                var _this = this;
                if (filterParams) {
                    this.setParameters(filterParams);
                }
                this.dropDownList.setDataSource(this.dataProvider.getDataSource(function () { return _this.onRequestFilterData(); }, null, this.hasCustomFunctionName ? this.dataProviderFunctionName : null));
            };
            DropDownList.prototype.hasValue = function () {
                return this.dropDownList.value() !== "";
            };
            DropDownList.prototype.getValue = function () {
                return this.dropDownList.value();
            };
            DropDownList.prototype.getText = function () {
                return this.dropDownList.text();
            };
            DropDownList.prototype.setReadOnly = function (val) {
                this.dropDownList.readonly(val);
            };
            DropDownList.prototype.getData = function () {
                return this.dropDownList.dataSource.data();
            };
            DropDownList.prototype.setDataSource = function (dataSource) {
                this.dropDownList.setDataSource(dataSource);
            };
            DropDownList.prototype.setValue = function (newValue) {
                this.dropDownList.value(newValue);
                this.dropDownList.trigger("change");
            };
            DropDownList.prototype.getKendoDropDown = function () {
                return this.dropDownList;
            };
            DropDownList.prototype.show = function () {
                $(this.dropDownList.element).closest(".k-widget").show();
            };
            DropDownList.prototype.hide = function () {
                $(this.dropDownList.element).closest(".k-widget").hide();
            };
            return DropDownList;
        }());
        Components.DropDownList = DropDownList;
    })(Components = Api.Components || (Api.Components = {}));
})(Api || (Api = {}));
var API;
(function (API) {
    var Common;
    (function (Common) {
        var FlexGrid = (function () {
            function FlexGrid() {
            }
            FlexGrid.prototype.recalculateGridHeight = function () {
                var fg_cont = $(".flexGrid-container");
                var fg_filter = $(".flexGrid-filter");
                var fg_grouping_header = $(".flexGrid .k-grouping-header");
                var fg_grid_header = $(".flexGrid .k-grid-header");
                var fg_grid_content = $(".flexGrid .k-grid-content");
                var fg_grid_footer = $(".flexGrid .k-grid-pager");
                var fg_grid = $(".flexGrid .k-grid");
                fg_grid.css("height", "");
                if (!fg_cont || !fg_grid_content) {
                    return;
                }
                var adjustment_height = 0;
                var grid_content_height = fg_cont.innerHeight();
                if (fg_filter) {
                    adjustment_height += fg_filter.innerHeight();
                }
                if (fg_grouping_header) {
                    adjustment_height += fg_grouping_header.innerHeight();
                }
                if (fg_grid_header) {
                    adjustment_height += fg_grid_header.innerHeight();
                }
                if (fg_grid_footer) {
                    adjustment_height += fg_grid_footer.innerHeight();
                }
                grid_content_height -= adjustment_height + 26;
                fg_grid_content.innerHeight(grid_content_height);
            };
            return FlexGrid;
        }());
        Common.FlexGrid = FlexGrid;
    })(Common = API.Common || (API.Common = {}));
})(API || (API = {}));
/// <reference path="ResizeSensor.d.ts" />
var API;
(function (API) {
    var Common;
    (function (Common) {
        var Splitter = (function () {
            function Splitter() {
                var that = this;
                that.myFlexGrid = new Common.FlexGrid();
                that.mainPane = $(".main-container");
                $("#horizontal").kendoSplitter({
                    panes: [
                        { collapsible: true, resizable: false, min: "20%", size: "20%" },
                        { collapsible: false, resizable: false, min: "80%", size: "80%" }
                    ]
                });
                that.splitter = $("#horizontal").data("kendoSplitter");
                that.splitter.bind("collapse", function () { that.myFlexGrid.recalculateGridHeight(); });
                that.splitter.bind("resize", function () { that.myFlexGrid.recalculateGridHeight(); });
                new ResizeSensor(that.mainPane, function () {
                    that.splitter.resize();
                });
            }
            return Splitter;
        }());
        Common.Splitter = Splitter;
    })(Common = API.Common || (API.Common = {}));
})(API || (API = {}));
var MasterPage;
(function (MasterPage) {
    /* Show error*/
    function SHOW_DATA_SOURCE_ERROR(e) {
        try {
            if (e.xhr != undefined) {
                if (showSessionFinished(e.xhr.statusText)) {
                    return "";
                }
                var display = "";
                // Status 406 Continue (Informative Error Not Acceptable)
                if (e.xhr.statusText !== "OK" && e.xhr.status !== 406) {
                    display = "Error: " + e.xhr.statusText + ", ";
                }
                display += (e.xhr.responseText == undefined) ? e.xhr.responseXML : e.xhr.responseText;
                if (e.xhr.status === 406) {
                    SHOW_WARNING_WINDOW(display);
                }
                else {
                    SHOW_ERROR_WINDOW(display);
                }
            }
            else {
                var display = "";
                // Status 406 Continue (Informative Error Not Acceptable)
                if (e.statusText !== "OK" && e.status !== 406) {
                    display = "Error: " + e.statusText + ", ";
                }
                display += (e.responseText == undefined) ? e.responseXML : e.responseText;
                if (e.status === 406) {
                    SHOW_WARNING_WINDOW(display);
                }
                else {
                    if ((e.readyState !== 0 && e.responseText !== "" && e.status !== 0 && e.statusText !== "error")) {
                        SHOW_ERROR_WINDOW(display);
                    }
                }
            }
        }
        catch (ex) {
            SHOW_ERROR_WINDOW("Server returned an undefined error");
            //alert("Server returned an undefined error");
        }
        return "";
    }
    MasterPage.SHOW_DATA_SOURCE_ERROR = SHOW_DATA_SOURCE_ERROR;
    ;
    /**
     * Create a Confirmation Dialog Message that way for user selection
     * You are able to put your code in the yes or not case.
     * Use The Promise syntax to use it.
     * @param message
     */
    function SHOW_CONFIRMATION_WINDOW_PROMISE(message) {
        return showWindow('#confirmationTemplate', message);
    }
    MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE = SHOW_CONFIRMATION_WINDOW_PROMISE;
    ;
    /**
     * Create a Warning Windows that way for the confirmation
     * Use the promise syntax to use it
     * @param message
     */
    function SHOW_WARNING_WINDOW_PROMISE(message) {
        return showErrorWarningInfoWindow('#warningDialogTemplate', message, 400);
    }
    MasterPage.SHOW_WARNING_WINDOW_PROMISE = SHOW_WARNING_WINDOW_PROMISE;
    ;
    /**
     * Create a Info Windows that way for the confirmation
     * Use the promise syntax to use it
     * You are able to put your code in the return of the function.
     * @param message
     */
    function SHOW_INFO_WINDOW_PROMISE(message) {
        return showErrorWarningInfoWindow('#infoDialogTemplate', message, 400);
    }
    MasterPage.SHOW_INFO_WINDOW_PROMISE = SHOW_INFO_WINDOW_PROMISE;
    ;
    /**
    * Create a Info Windows that way for the confirmation
    * Use the promise syntax to use it
    * You are able to put your code in the return of the function.
    * @param message
    */
    function SHOW_ERROR_WINDOW_PROMISE(message) {
        return showErrorWarningInfoWindow('#errorDialogTemplate', message, 400);
    }
    MasterPage.SHOW_ERROR_WINDOW_PROMISE = SHOW_ERROR_WINDOW_PROMISE;
    ;
    /**
     * Create a WARNING Windows that way for the confirmation
     * Use this if you do not need to put code after the dialog.
     * @param message
     */
    function SHOW_WARNING_WINDOW(message) {
        $.when(SHOW_WARNING_WINDOW_PROMISE(message))
            .then(function (confirmed) {
            return true;
        });
    }
    MasterPage.SHOW_WARNING_WINDOW = SHOW_WARNING_WINDOW;
    /**
      * Create a Error Windows that way for the confirmation
      * Use this if you do not need to put code after the dialog.
      * @param message
      */
    function SHOW_ERROR_WINDOW(message) {
        $.when(SHOW_ERROR_WINDOW_PROMISE(message))
            .then(function (confirmed) {
            return true;
        });
    }
    MasterPage.SHOW_ERROR_WINDOW = SHOW_ERROR_WINDOW;
    /**
     * Create a Info Windows that way for the confirmation
     * Use this if you do not need to put code after the dialog.
     * @param message
     */
    function SHOW_INFO_WINDOW(message) {
        $.when(SHOW_INFO_WINDOW_PROMISE(message))
            .then(function (confirmed) {
            return true;
        });
    }
    MasterPage.SHOW_INFO_WINDOW = SHOW_INFO_WINDOW;
    function showWindow(template, message) {
        var dfd = $.Deferred();
        var result = false;
        var win = $("<div id='popupWindow'></div>")
            .appendTo("body")
            .kendoWindow({
            width: 400,
            resizable: false,
            title: false,
            modal: true,
            visible: false,
            scrollable: false,
            close: function (e) {
                this.destroy();
                dfd.resolve(result);
            }
        })
            .data("kendoWindow");
        win.content($(template).html()).center().open();
        $('.popupMessage').html(message);
        $("#popupWindow .confirm_yes").val('Yes');
        $("#popupWindow .confirm_no").val('No');
        $("#popupWindow .confirm_no").click(function () {
            win.close();
        });
        $("#popupWindow .confirm_yes").click(function () {
            result = true;
            win.close();
        });
        return dfd.promise();
    }
    ;
    function showErrorWarningInfoWindow(template, message, wWidth) {
        var dfd = $.Deferred();
        var result = false;
        var win = $("<div id='popupWindow'></div>")
            .appendTo("body")
            .kendoWindow({
            width: wWidth,
            resizable: false,
            title: false,
            modal: true,
            visible: false,
            scrollable: false,
            close: function (e) {
                this.destroy();
                dfd.resolve(result);
            }
        })
            .data("kendoWindow");
        win.content($(template).html()).center().open();
        if (message != null) {
            if (message.length > 200) {
                $("#wrapperMessage").css("overflow-y", "scroll");
            }
        }
        //replace new lines with breaks
        message = message.replace(/(?:\r\n|\r|\n)/g, '<br />');
        $('.popupMessage').html(message);
        $("#popupWindow .confirm_yes").val('OK');
        $("#popupWindow .confirm_yes").click(function () {
            result = true;
            win.close();
        });
        return dfd.promise();
    }
    ;
    function showSessionFinished(statusText) {
        if (statusText === "OK") {
            alert("Session expired");
            return true;
        }
        return false;
    }
    function showPageLoading(show) {
        var pageLoading = $(document).find(".page-loading");
        if (pageLoading.length === 0)
            $(document).find("body").append('<div class="page-loading"></div>');
        kendo.ui.progress($(".page-loading"), show);
    }
    MasterPage.showPageLoading = showPageLoading;
    function resizeContainer(element, widthPercentage, heightPercentage) {
        if (widthPercentage) {
            var parentWidth = element.parent().innerWidth() * (widthPercentage / 100);
            element.width(parentWidth);
        }
        if (heightPercentage) {
            var parentHeight = element.parent().innerHeight() * (heightPercentage / 100);
            element.height(parentHeight);
        }
    }
    MasterPage.resizeContainer = resizeContainer;
})(MasterPage || (MasterPage = {}));
var AD;
(function (AD) {
    //the same table as syFieldTypes in database
    var FieldType;
    (function (FieldType) {
        FieldType[FieldType["SmallInt"] = 2] = "SmallInt";
        FieldType[FieldType["Int"] = 3] = "Int";
        FieldType[FieldType["Float"] = 5] = "Float";
        FieldType[FieldType["Money"] = 6] = "Money";
        FieldType[FieldType["Bit"] = 11] = "Bit";
        FieldType[FieldType["TinyInt"] = 17] = "TinyInt";
        FieldType[FieldType["UniqueIdentifier"] = 72] = "UniqueIdentifier";
        FieldType[FieldType["Char"] = 129] = "Char";
        FieldType[FieldType["Decimal"] = 131] = "Decimal";
        FieldType[FieldType["DateTime"] = 135] = "DateTime";
        FieldType[FieldType["Varchar"] = 200] = "Varchar";
    })(FieldType = AD.FieldType || (AD.FieldType = {}));
    ;
    var SystemStatus;
    (function (SystemStatus) {
        SystemStatus[SystemStatus["NewLead"] = 1] = "NewLead";
        SystemStatus[SystemStatus["ApplicationReceived"] = 2] = "ApplicationReceived";
        SystemStatus[SystemStatus["ApplicationNotAccepted"] = 3] = "ApplicationNotAccepted";
        SystemStatus[SystemStatus["InterviewScheduled"] = 4] = "InterviewScheduled";
        SystemStatus[SystemStatus["Interviewed"] = 5] = "Interviewed";
        SystemStatus[SystemStatus["Enrolled"] = 6] = "Enrolled";
        SystemStatus[SystemStatus["FutureStart"] = 7] = "FutureStart";
        SystemStatus[SystemStatus["NoStart"] = 8] = "NoStart";
        SystemStatus[SystemStatus["CurrentlyAttending"] = 9] = "CurrentlyAttending";
        SystemStatus[SystemStatus["LeaveOfAbsence"] = 10] = "LeaveOfAbsence";
        SystemStatus[SystemStatus["Suspension"] = 11] = "Suspension";
        SystemStatus[SystemStatus["Dropped"] = 12] = "Dropped";
        SystemStatus[SystemStatus["Graduated"] = 14] = "Graduated";
        SystemStatus[SystemStatus["Active"] = 15] = "Active";
        SystemStatus[SystemStatus["InActive"] = 16] = "InActive";
        SystemStatus[SystemStatus["DeadLead"] = 17] = "DeadLead";
        SystemStatus[SystemStatus["WillEnrollintheFuture"] = 18] = "WillEnrollintheFuture";
        SystemStatus[SystemStatus["TransferOut"] = 19] = "TransferOut";
        SystemStatus[SystemStatus["AcademicProbation"] = 20] = "AcademicProbation";
        SystemStatus[SystemStatus["Externship"] = 22] = "Externship";
        SystemStatus[SystemStatus["DisciplinaryProbation"] = 23] = "DisciplinaryProbation";
        SystemStatus[SystemStatus["WarningProbation"] = 24] = "WarningProbation";
        SystemStatus[SystemStatus["Imported"] = 25] = "Imported";
        SystemStatus[SystemStatus["Duplicated"] = 26] = "Duplicated";
    })(SystemStatus = AD.SystemStatus || (AD.SystemStatus = {}));
    var ProgramUnitTypes;
    (function (ProgramUnitTypes) {
        ProgramUnitTypes[ProgramUnitTypes["CreditHour"] = 0] = "CreditHour";
        ProgramUnitTypes[ProgramUnitTypes["ClockHour"] = 1] = "ClockHour";
        ProgramUnitTypes[ProgramUnitTypes["Program"] = 2] = "Program";
    })(ProgramUnitTypes = AD.ProgramUnitTypes || (AD.ProgramUnitTypes = {}));
    ;
    // enumeration to declre the type of R2T4 results.
    var R2T4ResultType;
    (function (R2T4ResultType) {
        R2T4ResultType[R2T4ResultType["R2T4Result"] = 0] = "R2T4Result";
        R2T4ResultType[R2T4ResultType["R2T4Override"] = 1] = "R2T4Override";
    })(R2T4ResultType = AD.R2T4ResultType || (AD.R2T4ResultType = {}));
    var ReportDataSourceType;
    (function (ReportDataSourceType) {
        /// <summary>
        /// The api.
        /// Use this when the report uses the rest API as Data Source
        /// </summary>
        ReportDataSourceType[ReportDataSourceType["RestApi"] = 1] = "RestApi";
        /// <summary>
        /// The sql server.
        /// Use this option when the report uses SQL Server as Data Source.
        /// </summary>
        ReportDataSourceType[ReportDataSourceType["SqlServer"] = 2] = "SqlServer";
    })(ReportDataSourceType = AD.ReportDataSourceType || (AD.ReportDataSourceType = {}));
    var ReportOutput;
    (function (ReportOutput) {
        /// <summary>
        /// The pdf.
        /// </summary>
        ReportOutput[ReportOutput["Pdf"] = 1] = "Pdf";
    })(ReportOutput = AD.ReportOutput || (AD.ReportOutput = {}));
})(AD || (AD = {}));
/// <reference path="../../Fame.Advantage.Client.MasterPage/AdvantageMessageBoxs.ts" />
/// <reference path="../../AdvWeb/Kendo/typescript/jquery.d.ts" />
/// <reference path="../../Fame.Advantage.Client.AD/Common/Enumerations.ts" />
var Api;
(function (Api) {
    var FileOutputType;
    (function (FileOutputType) {
        FileOutputType[FileOutputType["Pdf"] = 1] = "Pdf";
        FileOutputType[FileOutputType["Xlsx"] = 2] = "Xlsx";
    })(FileOutputType = Api.FileOutputType || (Api.FileOutputType = {}));
    var RequestParameterType;
    (function (RequestParameterType) {
        RequestParameterType[RequestParameterType["QueryString"] = 1] = "QueryString";
        RequestParameterType[RequestParameterType["Body"] = 2] = "Body";
    })(RequestParameterType = Api.RequestParameterType || (Api.RequestParameterType = {}));
    ;
    var RequestType;
    (function (RequestType) {
        RequestType[RequestType["Get"] = 1] = "Get";
        RequestType[RequestType["Post"] = 2] = "Post";
        RequestType[RequestType["Put"] = 3] = "Put";
        RequestType[RequestType["Delete"] = 4] = "Delete";
    })(RequestType = Api.RequestType || (Api.RequestType = {}));
    var ContentType = (function () {
        function ContentType() {
        }
        return ContentType;
    }());
    ContentType.contentTypePdf = "application/pdf";
    ContentType.contentTypeXlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    Api.ContentType = ContentType;
    var FileExtension = (function () {
        function FileExtension() {
        }
        return FileExtension;
    }());
    FileExtension.pdfExtension = ".pdf";
    Api.FileExtension = FileExtension;
    /**
     * The request class encapsulates the JWT Authorization process with the request.
     * The token is retrived from the browser serssion along with the API domain url. Token experation exception handling is also encapsulated on this class.
     */
    var Request = (function () {
        function Request() {
            /**
             * hard coding the json token and app domain to until the full implementation for grabbing this from the session storage is ready.
             */
            this.shouldDoRequest = true;
            this.shouldShowIsLoading = true;
            this.jsonWebToken = sessionStorage.getItem("AdvantageApiToken");
            this.apiDomainName = sessionStorage.getItem("AdvantageApiUrl");
            this.refreshTick = sessionStorage.getItem("refreshTick");
            if (this.apiDomainName === undefined ||
                this.jsonWebToken === undefined ||
                this.apiDomainName === null ||
                this.jsonWebToken === null ||
                this.apiDomainName === "" ||
                this.jsonWebToken === "") {
                this.shouldDoRequest = false;
                $.when(MasterPage
                    .SHOW_ERROR_WINDOW_PROMISE("An error has occured. Unable to access the advantage web api, this may be due to session expiration or invalid configuration. By Pressing Ok, you will be redirected to the login screen."))
                    .then(function (confirmed) {
                    if (confirmed) {
                        window.location.replace("../Logout.aspx");
                    }
                });
            }
            else {
                if (this.apiDomainName.charAt(this.apiDomainName.length - 1) !== "/") {
                    this.apiDomainName = this.apiDomainName + "/";
                }
            }
        }
        Request.prototype.setShouldShowIsLoading = function (value) {
            this.shouldShowIsLoading = value;
        };
        /**
         * Send a custom ajax request to the API.
         * You can specify if the request is a POST or Get, the route to call the paramters to pass based on it's type (query string or body) and an event to be fired when the
         * request is ready.
         * This method encapculates error handling and token expiration exception.
         * @param requestType
         * The type of HTTP request (GET, POST, PUT or DELETE)
         * @param route
         * The controller action to be called
         * @param requestData
         * The object to be pass on the request as a parameter based on it's type
         * @param typeOfParameter
         * This is a enum that identify how do you want to pass the requestData to the API. Wether you want to pass it as Query String or as Body.
         * @param onResponseCallback
         * An event that will be called when the response is ready.
         */
        Request.prototype.send = function (requestType, route, requestData, typeOfParameter, onResponseCallback) {
            var that = this;
            if (that.shouldDoRequest) {
                var requestSettings = void 0;
                var typeOfRequest = "GET";
                var callback_1 = onResponseCallback;
                route = that.apiDomainName + route;
                typeOfRequest = that.getTypeOfRequest(requestType);
                this.showIsPageLoading(this.shouldShowIsLoading);
                if (typeOfParameter === RequestParameterType.QueryString) {
                    requestSettings = {
                        type: typeOfRequest,
                        url: route + "?" + $.param(requestData),
                        contentType: "application/json",
                        dataType: 'json',
                        global: false,
                        timeout: 1200000,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Authorization", 'Bearer ' + that.jsonWebToken);
                        }
                    };
                }
                else {
                    requestSettings = {
                        type: typeOfRequest,
                        url: route,
                        data: JSON.stringify(requestData),
                        contentType: "application/json",
                        dataType: 'json',
                        global: false,
                        timeout: 1200000,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Authorization", 'Bearer ' + that.jsonWebToken);
                        }
                    };
                }
                //$.ajax(requestSettings).done((ajaxResponse, textStatus, jqXHR) => {
                //    that.showIsPageLoading(false);
                //    if (jqXHR !== undefined) {
                //        that.refreshToken(jqXHR.getResponseHeader("Authorization"));
                //    }
                $.ajax(requestSettings).done(function (ajaxResponse) {
                    that.showIsPageLoading(false);
                    callback_1(ajaxResponse);
                }).fail(function (error) {
                    that.showIsPageLoading(false);
                    that.apiExceptionHandler(error);
                });
            }
        };
        /**
         * Send a custom ajax request to the API.
         * You can specify if the request is a POST or Get, the route to call the paramters to pass based on it's type (query string or body) and an event to be fired when the
         * request is ready.
         * This method encapculates error handling and token expiration exception.
         * @param requestType
         * The type of HTTP request (GET, POST, PUT or DELETE)
         * @param route
         * The controller action to be called
         * @param requestData
         * The object to be pass on the request as a parameter based on it's type
         * @param typeOfParameter
         * This is a enum that identify how do you want to pass the requestData to the API. Wether you want to pass it as Query String or as Body.
         * @param onResponseCallback
         * An event that will be called when the response is ready.
         */
        Request.prototype.sendSync = function (requestType, route, requestData, typeOfParameter, onResponseCallback) {
            var that = this;
            if (that.shouldDoRequest) {
                var requestSettings = void 0;
                var typeOfRequest = "GET";
                var callback_2 = onResponseCallback;
                route = that.apiDomainName + route;
                typeOfRequest = that.getTypeOfRequest(requestType);
                this.showIsPageLoading(this.shouldShowIsLoading);
                if (typeOfParameter === RequestParameterType.QueryString) {
                    requestSettings = {
                        type: typeOfRequest,
                        url: route + "?" + $.param(requestData),
                        contentType: "application/json",
                        dataType: 'json',
                        async: false,
                        global: false,
                        timeout: 1200000,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Authorization", 'Bearer ' + that.jsonWebToken);
                        }
                    };
                }
                else {
                    requestSettings = {
                        type: typeOfRequest,
                        url: route,
                        data: JSON.stringify(requestData),
                        contentType: "application/json",
                        dataType: 'json',
                        async: false,
                        global: false,
                        timeout: 1200000,
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Authorization", 'Bearer ' + that.jsonWebToken);
                        }
                    };
                }
                //$.ajax(requestSettings).done((ajaxResponse, textStatus, jqXHR) => {
                //    that.showIsPageLoading(false);
                //    if (jqXHR !== undefined) {
                //        that.refreshToken(jqXHR.getResponseHeader("Authorization"));
                //    }
                $.ajax(requestSettings).done(function (ajaxResponse) {
                    that.showIsPageLoading(false);
                    callback_2(ajaxResponse);
                }).fail(function (error) {
                    that.showIsPageLoading(false);
                    that.apiExceptionHandler(error);
                });
            }
        };
        /**
        * Send a custom ajax request to the API.
        * You can specify if the request is a POST or Get, the route to call the paramters to pass based on it's type (query string or body) and an event to be fired when the
        * request is ready.
        * This method encapculates error handling and token expiration exception.
        * @param requestType
        * The type of HTTP request (GET, POST, PUT or DELETE)
        * @param route
        * The controller action to be called
        * @param requestData
        * The object to be pass on the request as a parameter based on it's type
        * @param output
        * This is a enum that identify what type output file do you want to pass the requestData to the API.
        * * @param fileName
        * This is a string identify what name of output file do you want at time of download.
        * @param typeOfParameter
        * This is a enum that identify how do you want to pass the requestData to the API. Wether you want to pass it as Query String or as Body.
        * @param onResponseCallback
        * An event that will be called when the response is ready.
        */
        Request.prototype.sendFile = function (requestType, route, requestData, output, fileName, fileExtension, typeOfParameter, onResponseCallback) {
            var that = this;
            route = that.apiDomainName + route;
            var request = new XMLHttpRequest();
            request.open(that.getTypeOfRequest(requestType), route, true);
            request.setRequestHeader("Content-Type", "application/json");
            request.setRequestHeader("Authorization", "Bearer " + that.jsonWebToken);
            request.responseType = "blob";
            request.onload = function () {
                if (request.status === 200) {
                    var blob = new Blob([request.response], { type: that.getOutputContentType(output) });
                    //for microsoft IE
                    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                        window.navigator.msSaveOrOpenBlob(blob, fileName + "." + fileExtension);
                    }
                    else {
                        //other browsers
                        var link = document.createElement("a");
                        link.href = window.URL.createObjectURL(blob);
                        link.download = fileName + "." + fileExtension;
                        document.body.appendChild(link);
                        link.click();
                        document.body.removeChild(link);
                        onResponseCallback(request.response);
                    }
                    that.showIsPageLoading(false);
                }
                else {
                    that.showIsPageLoading(false);
                    MasterPage.SHOW_ERROR_WINDOW_PROMISE("Report generation failed due to internal server error, please try again.");
                }
            };
            request.send(JSON.stringify(requestData));
            this.showIsPageLoading(this.shouldShowIsLoading);
        };
        // this function will return content type based on the FileOutputType given as parameter.
        Request.prototype.getOutputContentType = function (outputType) {
            var contentOutputType = "";
            //add your new output type of files here 
            switch (outputType) {
                case FileOutputType.Pdf:
                    contentOutputType = ContentType.contentTypePdf;
                    break;
                case FileOutputType.Xlsx:
                    contentOutputType = ContentType.contentTypeXlsx;
                    break;
                default:
                    contentOutputType = "";
            }
            return contentOutputType;
        };
        /**
        * Prepare a Kendo Data Source that encapsulates the JWT Authorization process.
        * This Kendo data source is for a GET Operation that sends the paramters on the query string.
        * You can use this for loading dropdowns, auto completes, grid and other kendo controls that can be bound to a server side data set.
        * @param route
        * The controller action to be called
        * @param onRequestFilterData
        * The event to get the filter parameter that will be pass when the request is made.
        */
        Request.prototype.getRequestAsKendoDataSource = function (route, onRequestFilterData, requestType, group) {
            return this.getRequestAsKendoDataSourceFull(route, onRequestFilterData, requestType, group, 20);
        };
        Request.prototype.getRequestAsKendoDataSourceWithPageSize = function (route, onRequestFilterData, pageSize) {
            return this.getRequestAsKendoDataSourceFull(route, onRequestFilterData, null, null, pageSize);
        };
        /**
         * Prepare a Kendo Data Source that encapsulates the JWT Authorization process.
         * This Kendo data source is for a GET Operation that sends the paramters on the query string.
         * You can use this for loading dropdowns, auto completes, grid and other kendo controls that can be bound to a server side data set.
         * @param route
         * The controller action to be called
         * @param onRequestFilterData
         * The event to get the filter parameter that will be pass when the request is made.
         */
        Request.prototype.getRequestAsKendoDataSourceFull = function (route, onRequestFilterData, requestType, group, pageSize, serverPaging, timeOut) {
            var that = this;
            var groupList = [];
            if (group) {
                groupList.push(group);
            }
            route = that.apiDomainName + route;
            if (that.shouldDoRequest) {
                var kendoDataSource = new kendo.data.DataSource({
                    group: groupList,
                    serverFiltering: true,
                    serverPaging: serverPaging === null ? false : serverPaging,
                    pageSize: pageSize,
                    transport: {
                        read: function (options) {
                            $.ajax({
                                type: requestType == null ? "GET" : requestType,
                                url: route,
                                contentType: "application/json",
                                dataType: "json",
                                timeout: timeOut == null ? 60000 : timeOut,
                                global: false,
                                data: requestType == null
                                    ? onRequestFilterData()
                                    : JSON.stringify(onRequestFilterData()),
                                success: function (result, textStatus, jqXHR) {
                                    // notify the data source that the request succeeded
                                    options.success(result);
                                    if (jqXHR !== undefined) {
                                        that.refreshToken(jqXHR.getResponseHeader("Authorization"));
                                    }
                                },
                                error: function (result) {
                                    // notify the data source that the request failed
                                    options.error(result);
                                    that.apiExceptionHandler(result);
                                },
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("Authorization", "Bearer " + that.jsonWebToken);
                                }
                            });
                        }
                    }
                });
                return kendoDataSource;
            }
            else {
                return new kendo.data.DataSource({
                    serverFiltering: false,
                    serverPaging: false,
                    type: "GET"
                });
            }
        };
        Request.prototype.getTypeOfRequest = function (requestType) {
            var typeOfRequest = "GET";
            if (requestType === RequestType.Get) {
                typeOfRequest = "GET";
            }
            else if (requestType === RequestType.Post) {
                typeOfRequest = "POST";
            }
            else if (requestType === RequestType.Put) {
                typeOfRequest = "PUT";
            }
            else if (requestType === RequestType.Delete) {
                typeOfRequest = "DELETE";
            }
            return typeOfRequest;
        };
        Request.prototype.apiExceptionHandler = function (error) {
            if (error !== undefined) {
                try {
                    if (error.status === 400 && !!error.responseText && (error.responseText !== "" || error.statusText !== "")) {
                        var errorJson_1 = { message: "" };
                        if (error.responseText !== "") {
                            var parsed = JSON.parse(error.responseText);
                            if (parsed && parsed.length > 0) {
                                $.each(parsed, function (i, val) {
                                    if (val["message"]) {
                                        errorJson_1.message += val["message"] + "\n";
                                    }
                                });
                            }
                            else {
                                errorJson_1.message = error.responseText;
                            }
                        }
                        MasterPage.SHOW_WARNING_WINDOW(errorJson_1.message);
                    }
                    else if (error.status === 401) {
                        MasterPage
                            .SHOW_ERROR_WINDOW_PROMISE("Unable to access the Advantage web api, this may be due to token expiration or invalid configuration. By Pressing Ok, you will be redirected to the login screen.")
                            .then(function (confirmed) {
                            if (confirmed) {
                                window.location.replace("../Logout.aspx");
                            }
                        });
                    }
                }
                catch (e) {
                    MasterPage.SHOW_ERROR_WINDOW_PROMISE("An error has ocurred during your request.");
                }
            }
            else {
                MasterPage.SHOW_ERROR_WINDOW_PROMISE("An error has ocurred during your request.");
            }
            if (error !== undefined && error.statusText === 'timeout') {
                MasterPage.SHOW_ERROR_WINDOW_PROMISE("Your request has timed out please try after some time.");
                return;
            }
        };
        Request.prototype.showIsPageLoading = function (show) {
            var pageLoading = $(document).find(".page-loading");
            if (pageLoading.length === 0)
                $(document).find("body").append('<div class="page-loading"></div>');
            kendo.ui.progress($(".page-loading"), show);
        };
        Request.prototype.refreshToken = function (refreshedToken) {
            var that = this;
            that.refreshTick = sessionStorage.getItem("refreshTick");
            if (that.refreshTick === undefined || that.refreshTick === null || that.refreshTick === "0") {
                var tick = setTimeout(function () {
                    if (refreshedToken !== undefined && refreshedToken !== null && refreshedToken.length > 0) {
                        sessionStorage.setItem("AdvantageApiToken", refreshedToken);
                        $.ajax({
                            async: true,
                            url: XMASTER_GET_BASE_URL + "dash.aspx/RefreshToken",
                            data: JSON.stringify({ token: refreshedToken }),
                            type: "POST",
                            timeout: 15000,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json"
                        }).done(function (msg) { })
                            .fail(function (err) { });
                    }
                    that.refreshTick = "0";
                    sessionStorage.setItem("refreshTick", that.refreshTick);
                }, 5000);
                that.refreshTick = tick;
                sessionStorage.setItem("refreshTick", that.refreshTick);
            }
        };
        return Request;
    }());
    Api.Request = Request;
})(Api || (Api = {}));
//# sourceMappingURL=Fame.Advantage.Common.js.map