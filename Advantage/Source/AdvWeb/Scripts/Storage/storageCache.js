﻿//---------------------------------------------
// use session Storage to manage cache
//---------------------------------------------
(function ($) {

    //-------------------------------------------
    // Insert object into session storage
    //-------------------------------------------
    window.insertInSessionCache = function (key, value) {

        if (typeof (Storage) == "undefined") return false;
        sessionStorage.setItem(key, value);
        return true;
    };

    //-------------------------------------------
    // retrieve object from session storage
    //-------------------------------------------
    window.getFromSessionCache = function (key) {

        if (typeof (Storage) == "undefined") return false;

        var obj = sessionStorage.getItem(key);

        if (obj) {
            return obj;
        } else {
            return null;
        }
    };


    window.removeFromSessionCache = function (key) {

        if (typeof (Storage) == "undefined") return false;
        sessionStorage.removeItem(key);
        return true;
    };

    //-------------------------------------------
    // Insert object into local storage
    //-------------------------------------------
    window.insertInLocalCache = function (key, value) {

        if (typeof (Storage) == "undefined") return false;
        localStorage.setItem(key, value);
        return true;
    };

    //-------------------------------------------
    // retrieve object from local storage
    //-------------------------------------------
    window.getFromLocalCache = function (key) {

        if (typeof (Storage) == "undefined") return false;

        var obj = localStorage.getItem(key);

        if (obj) {
            return obj;
        } else {
            return null;
        }
    };

    window.removeFromLocalCache = function (key) {

        if (typeof (Storage) == "undefined") return false;
        localStorage.removeItem(key);
        return true;
    };

    window.createCookie = function (name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        name = name;

        document.cookie = name + "=" + value + expires + "; path=/";
    }

    window.readCookie = function (name) {
        var nameEq = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEq) == 0) return c.substring(nameEq.length, c.length);
        }
        return null;
    }

    window.eraseCookie = function (name) {
        createCookie(name, "", -1);
    }

    window.storageCache = window.storageCache || {};
    window.storageCache.insertInSessionCache = insertInSessionCache;
    window.storageCache.getFromSessionCache = getFromSessionCache;
    window.storageCache.removeFromSessionCache = removeFromSessionCache;
    window.storageCache.insertInLocalCache = insertInLocalCache;
    window.storageCache.getFromLocalCache = getFromLocalCache;
    window.storageCache.createCookie = createCookie;
    window.storageCache.readCookie = readCookie;
    window.storageCache.eraseCookie = eraseCookie;
    window.storageCache.removeFromLocalCache = removeFromLocalCache;

})(jQuery);


