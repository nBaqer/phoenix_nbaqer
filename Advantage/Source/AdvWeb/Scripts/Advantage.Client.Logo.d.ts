/// <reference path="../kendo/typescript/kendo.all.d.ts" />
/// <reference path="../kendo/typescript/jquery.d.ts" />
declare module Logo {
    class Logo {
        ViewModel: LogoViewModel;
        SelectedEnrollment: kendo.Observable;
        constructor();
    }
}
declare module Logo {
    class LogoViewModel extends kendo.Observable {
        LogoDataServices: LogoDb;
        LogoGridDataSource: kendo.data.DataSource;
        private selectedRow;
        constructor();
        loadLogoFromFileClick: () => void;
        windowsClosed(): void;
        editImage(row: any): number;
        deleteImage(row: any): number;
        updateImageServer(object: Object): void;
        deleteImageServer(object: Object): void;
        fileUploaded(): void;
    }
}
declare module Logo {
    var XGET_UPDATELOGO_URL: string;
    var XPOST_NEWLOGO_URL: string;
    var XPOST_UPDATE_LOGO_URL: string;
    var XDELETE_LOGO_URL: string;
}
declare module Logo {
    class LogoDb {
        GetLogoGridDataSource(): kendo.data.DataSource;
        PostLogoEdit(id: number, logocode: string, observations: string, officialUse: boolean): void;
        deleteLogo(id: number): void;
        showDataSourceError(e: any): void;
    }
}
