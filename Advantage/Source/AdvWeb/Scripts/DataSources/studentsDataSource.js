﻿(function (kendo) {
    var studentsDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: '../proxy/api/Students/',
                dataType: "json",
                type: "GET",
                data: {
                    firstName: '',
                    lastName: '',
                    studentNumber: '',
                    campusId: '',
                    programVersionId: '',
                    enrollmentStatusId: '',
                    studentGroupId: '',
                    startDate: ''
                }
            }
        },
        schema: {
            data: "Data",
            total: "Total",
            parse:
                function (response) {
                    var mappedStudents = jQuery.map(response.Data, function (item) {
                        var startDate = formattedDate(item.Enrollments[0].StartDate, false);

                        return {
                            ShowDocs: null,
                            StudentId: item.Id,
                            StudentNumber: item.StudentNumber,
                            FullName: item.FullName,
                            SSN: item.SSN,
                            ProgramVersion: item.Enrollments[0].ProgramVersion.Description,
                            EnrollmentStatus: item.Enrollments[0].EnrollmentStatus.Description,
                            StartDate: startDate
                        };
                    });

                    return { Data: mappedStudents, Total: response.Total };
                }
        },
        //change: function(e) {
        //    var grid = $('#students').data('kendoGrid');
        //    var rows = grid.select();
            
        //    var uid = rows.data("uid");
        //    var tr_first = $('[data-uid="' + uid + '"] td:first').text();
        //    alert(tr_first);
        //},
        pageSize: 10,
        serverPaging: true
    });

    window.datasources = window.datasources || {};
    window.datasources.studentsDataSource = studentsDataSource;
})(window.kendo);