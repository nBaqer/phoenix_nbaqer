﻿(function (kendo) {
    var reportModulesDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "../proxy/api/SystemStuff/Reports/Menu",
                dataType: "json",
                type: "GET",
                data: {
                    campusId: '',
                    userId: ''
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.reportModulesDataSource = reportModulesDataSource;
})(window.kendo);
