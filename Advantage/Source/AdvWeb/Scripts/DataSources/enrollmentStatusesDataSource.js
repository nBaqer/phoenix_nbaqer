﻿(function (kendo) {
    var enrollmentStatusesDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = '../proxy/api/Campuses/{campusId}/CampusGroups/EnrollmentStatuses/'.supplant({ campusId: options.campusId });
                    delete options.campusId;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    StatusCode: 'A'
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.enrollmentStatusesDataSource = enrollmentStatusesDataSource;
})(window.kendo);


