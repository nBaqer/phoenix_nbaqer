﻿(function (kendo) {
    var requirementsDataSource = new kendo.data.DataSource({
        pageSize: 5,
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + '/proxy/api/Leads/{leadId}/Requirements/GetRequirementsByLeadId'.supplant({ leadId: options.LeadId });
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                timeout: 90000,
                type: "GET",
                data: {
                    baseUrl: '',
                    CampusId: '',
                    StatusCode: 'A',
                    IsMandatory: '',
                    UserId:""
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.requirementsDataSource = requirementsDataSource;
})(window.kendo);
