﻿(function (kendo) {
    var requirementDetailsByReqIdDataSource = new kendo.data.DataSource({
        pageSize: 5,
        autoBind: false,
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + '/proxy/api/Leads/{grpId}/Requirements/GetRequirementsDetailsByReqId'.supplant({ reqId: options.reqId });
                    delete options.baseUrl;
                    return uri;
                },
                async: true,
                dataType: "json",
                timeout: 90000,
                type: "GET",
                data: {
                    baseUrl: '',
                    CampusId: '',
                    StatusCode: 'A',
                    LeadId: '',
                    RequirementType:''

                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.requirementDetailsByReqIdDataSource = requirementDetailsByReqIdDataSource;
})(window.kendo);
