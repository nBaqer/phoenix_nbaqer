﻿(function (kendo) {
    var reqListDataSource = new kendo.data.DataSource({
        pageSize: 5,
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + '/proxy/api/Leads/{leadId}/Requirements/GetRequirementsList'.supplant({ leadId: options.leadId });
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                timeout: 90000,
                type: "GET",
                data: {
                    baseUrl: '',
                    CampusId: '',
                    StatusCode: 'A'
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.reqListDataSource = reqListDataSource;
})(window.kendo);
