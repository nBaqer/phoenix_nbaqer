﻿(function (kendo) {
    var optionalRequirementsDataSource = new kendo.data.DataSource({
        pageSize: 5,
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + '/proxy/api/Leads/{leadId}/Requirements/GetOptionalRequirementsByLeadId'.supplant({ leadId: options.leadId });
                    delete options.baseUrl;
                    return uri;
                },
                async: true,
                dataType: "json",
                type: "GET",
                timeout: 90000,
                data: {
                    baseUrl: '',
                    CampusId: '',
                    StatusCode: 'A',
                    IsMandatory:'false'
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.optionalRequirementsDataSource = optionalRequirementsDataSource;
})(window.kendo);
