﻿(function (kendo) {
    var transactionCodeDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + '/proxy/api/Students/Enrollments/GetTransactionsCodesBySystemCode';
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: ''
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.transactionCodeDataSource = transactionCodeDataSource;
})(window.kendo);


