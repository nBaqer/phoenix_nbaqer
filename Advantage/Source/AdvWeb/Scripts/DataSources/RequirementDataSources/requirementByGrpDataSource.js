﻿(function (kendo) {
    var requirementByGrpDataSource = new kendo.data.DataSource({
        pageSize: 5,
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + '/proxy/api/Leads/{grpId}/Requirements/GetRequirementsByGroupId'.supplant({ grpId: options.grpId });
                    delete options.baseUrl;
                    return uri;
                },
                async: true,
                dataType: "json",
                timeout: 90000,
                type: "GET",
                data: {
                    baseUrl: '',
                    CampusId: '',
                    StatusCode: 'A',
                    IsMandatory:'True'

                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.requirementByGrpDataSource = requirementByGrpDataSource;
})(window.kendo);
