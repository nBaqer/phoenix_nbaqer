﻿(function (kendo) {
    var leadPhonesDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/Lead/GetLeadContactInfo";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: '',
                    LeadGuid:''

                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.leadPhonesDataSource = leadPhonesDataSource;
})(window.kendo);
