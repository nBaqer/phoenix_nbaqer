﻿(function(kendo) {
    var requirementApprovalDataSource = new kendo.data.DataSource({
        autoSync: false,
        transport: {
            read: {
                url: function (options) {
                    var uri = "../proxy/api/Students/EnrollmentsDocuments/Requirements/Documents";
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    IsApproved: false
                }
            },
            update: {
                url: function (options) {
                    var uri = "../proxy/api/Students/EnrollmentsDocuments/Requirements/Documents/{" + options.StudentDocumentId + "}";
                    return uri;
                },
                type: "PUT",
                data: {
                    IsApproved: null,
                    StudentDocumentId: null,
                    ModUser: null
                },
                success:function(result) {
                    alert("Document has been approved.");
                },
            },
            parameterMap: function (data, type) {
                if (type == 'update') {
                    return kendo.stringify(data);
                }
                else {
                    return data;
                }
            },
        },
        schema: {
            data: "Data",
            total: "Total",
            parse:
                function (response) {
                    var mappedDocuments = jQuery.map(response.Data, function (item) {   
                        var startDate = formattedDate(item.Student.Enrollments[0].StartDate,false);
                        var dateModified = formattedDate(item.DateModified, false);
                            
                        return {
                            ShowDocs: null,
                            Id: item.Id,
                            DocumentName: item.DocumentName,
                            RequiredFor: item.RequiredFor,                            
                            FullName: item.Student.FullName,
                            StudentNumber: item.Student.StudentNumber,
                            SSN: item.Student.SSN,    
                            ProgramVersion: item.Student.Enrollments[0].ProgramVersion.Description,
                            EnrollmentStatus: item.Student.Enrollments[0].EnrollmentStatus.Description,
                            SystemModuleName: item.SystemModuleName,
                            StartDate: startDate,
                            DateModified: dateModified,
                            ModUser: item.ModUser,
                            IsApproved: item.DocumentStatus.IsApproved,
                            DocumentStatusId: item.DocumentStatus.Id,
                            DateRequested: item.DateRequested,
                            DateReceived: item.DateReceived,
                            FileId: item.Files[0].Id
                        };
                    });

                    return { Data: mappedDocuments, Total: response.Total };                    
                },
            model: {
                id:'Id'                

            }

        },
        error: function(result) {
            
            //if (result.status == 401 || result.status == 403) 
            //    location.href = '../../host/default.aspx';
            
        },
        success: function(result) {
           // alert('success');
        },
        change: function (result) {
           // alert('change');
        },
        pageSize: 10,
        serverPaging: true,
    });

    window.datasources = window.datasources || {};
    window.datasources.requirementApprovalDataSource = requirementApprovalDataSource;
})(window.kendo);



 
PutApproval = function(studentDocId, targetDatasource ) {
    
    var filter = { IsApproved: false };
        
    $.ajax({
        url: "../proxy/api/Students/Enrollments/Requirements/Documents/{" + studentDocId + "}",
        type: 'PUT',
        data: JSON.stringify(filter)
    })
    .done(function(data) {
        targetDatasource.read();
        alert('Your document has been approved and will be removed from the list.');
        
    });

};
