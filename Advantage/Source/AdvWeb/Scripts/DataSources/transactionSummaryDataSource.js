﻿(function (kendo) {
    var transactionSummaryDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + '/proxy/api/Students/Enrollments/GetTransactionsSummary';
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: '',
                    FromDate: '',
                    ToDate: '',
                    SummaryType: ''
                }
            }
        },
        schema: {
            parse:
                function (response) {
                    var mappedTrans = jQuery.map(response, function (item) {
                        var tranDate = new Date(item.TransDate);

                        return {
                            ChargeAmountSum:item.ChargeAmountSum,
                            TransDate: tranDate,
                            TDate:item.TDate
                        };
                    });

                    return mappedTrans;
                }
        }

    });

    window.datasources = window.datasources || {};
    window.datasources.transactionSummaryDataSource = transactionSummaryDataSource;
})(window.kendo);










