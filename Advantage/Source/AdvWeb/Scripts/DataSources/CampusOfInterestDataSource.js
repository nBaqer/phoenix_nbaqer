﻿(function (kendo) {
    var campusOfInterestDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/Lead/GetCampusOfInterest";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: ''

                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.campusOfInterestDataSource = campusOfInterestDataSource;
})(window.kendo);
