﻿(function (kendo) {
    var admissionReRepDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = '../proxy/api/User/Users';
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    ReturnReps: 'true',
                    UserId: '',
                    CampusId: '',
                    Active:''
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.admissionReRepDataSource = admissionReRepDataSource;
})(window.kendo);