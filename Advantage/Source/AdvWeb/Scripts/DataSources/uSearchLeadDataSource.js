﻿
(function (kendo) {
    var uSearchLeadDataSource = new kendo.data.DataSource({
        serverFiltering: true,
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/LeadsSearch/GetBySearchTerm";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: '',
                    initialCampus: '',
                    SearchTerm: function () {
                        var term = '';

                        if (window.datasources.uSearchLeadDataSource) term = window.datasources.uSearchLeadDataSource.filter().filters[0].value;
                        if (term.length < 3) {
                            return null;
                        } else {
                            var iCampus = window.datasources.uSearchLeadDataSource.options.transport.read.data.initialCampus;

                            if (iCampus) {
                                term = term + "|" + window.datasources.uSearchLeadDataSource.options.transport.read.data.initialCampus;
                            } else {
                                var multiselect = [];
                                $.each(selectedCampusIds, function (index, value) {
                                    if (value && value.length > 0)
                                        multiselect.push(value);
                                });

                                if (multiselect.length > 0) {
                                    term = term + "|" + multiselect.join(",");
                                }
                            }

                        }

                        return term;
                    },
                    LeadStatus: function () {
                        return selectedLeadStatusIds.join(",");
                    },
                    AdmissionRep: function () {
                        return selectedAdmissionRepIds.join(",");
                    }
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.uSearchLeadDataSource = uSearchLeadDataSource;
})(window.kendo);

(function (kendo) {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/SystemStuff/UniversalSearch/GetCampuses";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                timeout: 20000,
                type: "GET",
                data: {
                    input: {
                        UserId: $("#hdnUserId").val(),
                        Campuses: undefined,
                        ShowInactive: false
                    }
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.uSearchCampuses = dataSource;
})(window.kendo);

(function (kendo) {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/SystemStuff/UniversalSearch/GetLeadStatus";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                timeout: 20000,
                type: "GET",
                data: {
                    input: {
                        UserId: $("#hdnUserId").val(),
                        Campuses: undefined,
                        ShowInactive: false
                    }
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.uSearchLeadStatus = dataSource;
})(window.kendo);

(function (kendo) {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/SystemStuff/UniversalSearch/GetAdminReps";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                timeout: 20000,
                type: "GET",
                data: {
                    input: {
                        UserId: $("#hdnUserId").val(),
                        Campuses: undefined,
                        ShowInactive: false
                    }
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.uSearchAdmissionRep = dataSource;
})(window.kendo);