﻿(function (kendo) {
    var maintenanceModulesDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: "../proxy/api/SystemStuff/Maintenance/Menu",
                dataType: "json",
                type: "GET",
                data: {
                    campusId: '',
                    userId: ''
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.maintenanceModulesDataSource = maintenanceModulesDataSource;
})(window.kendo);