﻿(function (kendo) {
    var campusWithActiveLeadDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/CampusWithLead/GetWithActiveLeads";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: '',
                    IncludeActiveLeads: '',
                    UserId: ''

                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.campusWithActiveLeadDataSource = campusWithActiveLeadDataSource;
})(window.kendo);
