﻿
(function (kendo) {
    var documentStatusesDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function () {
                    var uri = '../proxy/api/Students/EnrollmentsDocuments/Requirements/DocumentStatuses/';
                    
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    IsApproved: null
                }
            }
            
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.documentStatusesDataSource = documentStatusesDataSource;
})(window.kendo);



