﻿(function (kendo) {
    var studentDocumentRequirementsDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = '../proxy/api/Students/{studentId}/Requirements/Documents/'.supplant({ studentId: options.studentId });
                    delete options.studentId;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    statusCode: 'A',
                    campusId: null
                }
            }
        },
        pageSize: 10
    });

    window.datasources = window.datasources || {};
    window.datasources.studentDocumentRequirementsDataSource = studentDocumentRequirementsDataSource;
})(window.kendo);



