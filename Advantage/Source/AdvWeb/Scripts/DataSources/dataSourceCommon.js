﻿(function (jQuery) {
    var setCampus = function (ds, campusId) {
        
        ds.requirementApprovalDataSource.transport.options.read.data.campusId = campusId;
        ds.programVersionDataSource.transport.options.read.data.campusId = campusId;
        ds.enrollmentStatusesDataSource.transport.options.read.data.campusId = campusId;
        ds.studentGroupsDataSource.transport.options.read.data.campusId = campusId;        
        
    };
    
    var setCampusForManagement = function (vm, campusId) {

        vm.programVersions.transport.options.read.data.campusId = campusId;
        vm.enrollmentStatuses.transport.options.read.data.campusId = campusId;
        vm.studentGroups.transport.options.read.data.campusId = campusId;
       
    };
    
    window.dataSourceCommon = window.dataSourceCommon || {};
    window.dataSourceCommon.setCampus = setCampus;
    window.dataSourceCommon.setCampusForManagement = setCampusForManagement;
    
})(window.jQuery);