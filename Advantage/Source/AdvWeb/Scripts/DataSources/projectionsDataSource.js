﻿(function (kendo) {
    var projectionDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + '/proxy/api/Students/Enrollments/GetProjectedPaymentPeriodCharges';
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: '',
                    ThreshValue: '',
                    IncrementType: ''

                }
            }
        },
        pageSize: 10,
        aggregate: [{ field: "ChargeAmount", aggregate: "sum" }]
    });

    window.datasources = window.datasources || {};
    window.datasources.projectionDataSource = projectionDataSource;
})(window.kendo);