﻿(function (kendo) {
    var documentFilesDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function(options) {
                    var uri = '../proxy/api/Students/Enrollments/Requirements/Documents/{studentDocumentId}/Files'.supplant({ studentDocumentId: options.studentDocumentId });
                    delete options.studentDocumentId;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    studentDocumentId: null
                }
            },
            destroy: {
                url: function(options) {
                    var uri = '../proxy/api/Students/Enrollments/Requirements/Documents/File/{fileId}/Delete'.supplant({ fileId: options.fileId });
                    delete options.fileId;
                    return uri;
                },
                dataType: "json",
                type: "DELETE",
                data: {
                    fileId: null
                }
            },
            parameterMap: function(data, type) {
                var models = {};
                if (type == "destroy" || type == 'create') {
                    models = kendo.stringify(data);
                }
                return models;
            }
        },
        schema: {
            model: {
                id: 'Id',
                fields: {
                    "Id": { editable: false },
                    "FileName": { type: "string", editable: true }
                    
                }
            },

            pageSize: 10,
            batch: true
        },
        change: function(e) {
            if (e.action == 'remove') alert('File Document was removed.');

        },
        pageSize: 10
        
    });


    var removeDoc = function( documentHistory, file) {
        
        $.ajax({
            url: '../proxy/api/Students/Enrollments/Requirements/Documents/File/{fileId}/Delete'.supplant({ fileId: file.Id }),
            type: 'DELETE',
            success: function() {
                alert('document file removed');
                documentHistory.remove(file);
            },
            fail: function() {
                alert('error occured: document file could not be removed');
            }
        });
    };

    window.datasources = window.datasources || {};
    window.datasources.documentFilesDataSource = documentFilesDataSource;
    window.datasources.removeDoc = removeDoc;
})(window.kendo);   
    

