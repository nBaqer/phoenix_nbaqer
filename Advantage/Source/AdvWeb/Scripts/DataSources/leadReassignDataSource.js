﻿(function (kendo) {
    var leadReassignDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/Lead/GetByReassign";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: '',
                    CampusId: '',
                    RepId: '',
                    LastModDate: ''


                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.leadReassignDataSource = leadReassignDataSource;
})(window.kendo);
