﻿(function (kendo) {
    var employeeMruDataSource = new kendo.data.DataSource({
        
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/EmployeeSearch/GetByMru";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    UserId: '',
                    CampusId: ''
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.employeeMruDataSource = employeeMruDataSource;
})(window.kendo);


