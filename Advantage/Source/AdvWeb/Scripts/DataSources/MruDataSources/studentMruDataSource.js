﻿(function (kendo) {
    var studentMruDataSource = new kendo.data.DataSource({
        
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/StudentSearch/GetByMru";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    UserId: '',
                    CampusId: ''
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.studentMruDataSource = studentMruDataSource;
})(window.kendo);


