﻿(function (kendo) {
    var leadMruDataSource = new kendo.data.DataSource({
        
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/LeadSearch/GetByMru";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    UserId: '',
                    CampusId: ''
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.leadMruDataSource = leadMruDataSource;
})(window.kendo);


