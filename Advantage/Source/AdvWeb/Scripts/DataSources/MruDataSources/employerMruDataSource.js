﻿(function (kendo) {
    var employerMruDataSource = new kendo.data.DataSource({
        
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/EmployerSearch/GetByMru";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    UserId: '',
                    CampusId: ''
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.employerMruDataSource = employerMruDataSource;
})(window.kendo);


