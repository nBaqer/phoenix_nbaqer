﻿(function (kendo) {
    var projectionSummaryDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + '/proxy/api/Students/Enrollments/GetProjectedPaymentPeriodSummary';
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: '',
                    ThreshValue: '',
                    IncrementType: ''
                }
            }
        }

    });

    window.datasources = window.datasources || {};
    window.datasources.projectionSummaryDataSource = projectionSummaryDataSource;
})(window.kendo);










