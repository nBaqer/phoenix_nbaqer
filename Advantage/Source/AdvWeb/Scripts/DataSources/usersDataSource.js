﻿(function (kendo) {
    var usersDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = '../proxy/api/User/Users';
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    Active: 'true',
                    UserId: '',
                    CampusId:''
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.usersDataSource = usersDataSource;
})(window.kendo);






