﻿(function (kendo, jQuery) {
    var studentDocumentDataSource = new kendo.data.DataSource({
        transport: {
            create: {
                url: function (options) {
                    var document = options;
                    var uri = '../proxy/api/Students/{studentId}/Enrollments/Requirements/{requirementId}/Documents'.supplant({ studentId: document.StudentId, requirementId: document.RequirementId });
                    return uri;
                },
                dataType: "json",
                type: "POST"
            },
            update: {
                url: function (options) {
                    var document = options;
                    var uri = '../proxy/api/Students/Enrollments/Requirements/Documents/{documentId}'.supplant({ documentId: document.Id });
                    return uri;
                },
                dataType: "json",
                type: "PUT"
            },
            parameterMap: function(data, type) {
                if (type == "create" || type === "update") {
                    return kendo.stringify(data) ;
                }
            },
        },
        schema: {
            model: {
                id: 'Id',
                fields: {
                    "DateReceived": { type: "date" },
                    "DateRequested": { type: "date" },
                    "IsApproved": { type: "boolean" }
                }
            }
        },
        pageSize: 10
    });

    window.datasources = window.datasources || {};
    window.datasources.studentDocumentDataSource = studentDocumentDataSource;
})(window.kendo, window.jQuery);