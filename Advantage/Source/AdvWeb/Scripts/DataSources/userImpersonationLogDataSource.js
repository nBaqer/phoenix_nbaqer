﻿(function (kendo) {
    var userImpersonationLogDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: '../proxy/api/SystemStuff/UserImpersonationLogs',
                dataType: "json",
                type: "GET",
                data: {
                    StartDate: '',
                    EndDate: ''
                }
            }
        }
        
        
    });

    window.datasources = window.datasources || {};
    window.datasources.userImpersonationLogDataSource = userImpersonationLogDataSource;
})(window.kendo);










