﻿
(function (kendo) {
    var uSearchEmployeeDataSource = new kendo.data.DataSource({
        serverFiltering: true,
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/EmployeesSearch/GetBySearchTerm";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: '',
                    initialCampus: '',
                    SearchTerm: function () {
                        var term = '';



                        if (window.datasources.uSearchEmployeeDataSource) term = window.datasources.uSearchEmployeeDataSource.filter().filters[0].value;
                        if (term.length < 3) {
                            return null;
                        } else {
                            var iCampus = window.datasources.uSearchEmployeeDataSource.options.transport.read.data.initialCampus;

                            if (iCampus) {
                                term = term + "|" + window.datasources.uSearchEmployeeDataSource.options.transport.read.data.initialCampus;
                            } else {
                                var multiselect = [];
                                $.each(selectedCampusIds, function (index, value) {
                                    if (value && value.length > 0)
                                        multiselect.push(value);
                                });

                                if (multiselect.length > 0) {
                                    term = term + "|" + multiselect.join(",");
                                }
                            }

                        }

                        return term;
                    }
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.uSearchEmployeeDataSource = uSearchEmployeeDataSource;
})(window.kendo);




