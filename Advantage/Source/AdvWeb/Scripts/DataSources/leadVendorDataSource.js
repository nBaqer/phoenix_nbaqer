﻿(function (kendo) {
    var leadVendorDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/Lead/LeadVendor/Get";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: ''

                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.leadVendorDataSource = leadVendorDataSource;
})(window.kendo);
