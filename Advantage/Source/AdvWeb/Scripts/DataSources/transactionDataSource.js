﻿(function (kendo) {
    var transactionDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function(options) {
                    var uri = options.baseUrl + '/proxy/api/Students/Enrollments/GetTransactionsByPayPeriod';
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: '',
                    FromDate: '',
                    ToDate: ''
                }
            }
        },
        pageSize: 10,


    });

    window.datasources = window.datasources || {};
    window.datasources.transactionDataSource = transactionDataSource;
})(window.kendo);










