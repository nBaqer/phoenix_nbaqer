﻿(function (kendo) {
    var leadUnassignedDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + "/proxy/api/Lead/GetByUnassigned";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl: '',
                    VendorId:'',
                    CampusOfInterest: '',
                    CampusId:''

                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.leadUnassignedDataSource = leadUnassignedDataSource;
})(window.kendo);
