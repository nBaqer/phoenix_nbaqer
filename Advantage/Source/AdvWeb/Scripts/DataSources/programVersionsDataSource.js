﻿(function (kendo) {
    var programVersionDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = "../proxy/api/Campuses/{" + options.campusId + "}/CampusGroups/ProgramVersions/";
                    delete options.campusId;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    StatusCode: 'A'
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.programVersionDataSource = programVersionDataSource;
})(window.kendo);
