﻿(function (kendo) {
    var admissionRepDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl + '/proxy/api/User/Users';
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    ReturnReps: 'true',
                    UserId: '',
                    CampusId: '',
                    Active:''
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.admissionRepDataSource = admissionRepDataSource;
})(window.kendo);