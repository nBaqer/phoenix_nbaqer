﻿(function (kendo) {
    var campusesDataSource = new kendo.data.DataSource({
        transport: {
            read: {
                url: function (options) {
                    var uri = options.baseUrl +  "/proxy/api/Campuses";
                    delete options.baseUrl;
                    return uri;
                },
                dataType: "json",
                type: "GET",
                data: {
                    baseUrl:''
                    
                }
            }
        }
    });

    window.datasources = window.datasources || {};
    window.datasources.campusesDataSource = campusesDataSource;
})(window.kendo);
