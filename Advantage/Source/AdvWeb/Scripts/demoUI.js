﻿//----------------------- code related only to the demo UI --------------------------------------//


//all the timers declared below are used for the demo UI only - to update UI texts. 
//The functionality related to the scenario itself is handled by RadNotification automatically out of the box
var mainLblCounter = null;
var timeLeftCounter = null;
var seconds = 60;
var secondsBeforeShow = 60;
var mainLabel;

//start the main label counter when the page loads
function pageLoad() {
    //var xmlPanel = $find("")._xmlPanel;
    // xmlPanel.set_enableClientScriptEvaluation(true);
    // mainLabel = $get("mainLbl"); resetTimer("mainLblCounter", updateMainLabel, 1000);
};

//stop timers for UI 
function stopTimer(timer) {
    clearInterval(this[timer]);
    this[timer] = null;
};

//reset timers for UI
function resetTimer(timer, func, interval) {
    this.stopTimer(timer);
    this[timer] = setInterval(Function.createDelegate(this, func), interval);
};

function OnClientShowing(sender, args) {
    //deal with UI labels
    mainLabel.innerHTML = 0;
    resetTimer("timeLeftCounter", UpdateTimeLabel, 1000);
    stopTimer("mainLblCounter");
}

function updateMainLabel(toReset) {
    secondsBeforeShow = (toReset == true) ? 60 : secondsBeforeShow - 1;
    mainLabel.innerHTML = secondsBeforeShow;
}

function OnClientHidden() {
    updateMainLabel(true);
    mainLabel.style.display = "";
    resetTimer("mainLblCounter", updateMainLabel, 1000);
}


//update the text in the label in RadNotification
//this could also be done automatically by using UpdateInterval. However, this will cause callbacks [which is the second best solution than javascript] on every second that is being count
function UpdateTimeLabel(toReset) {
    var sessionExpired = (seconds == 0);
    if (sessionExpired) {
        stopTimer("timeLeftCounter");
        //redirect to session expired page - simply take the url which RadNotification sent from the server to the client as value
        // window.location.href = $find("").get_value();
    }
    else {
        var timeLbl = $get("timeLbl");
        timeLbl.innerHTML = seconds--;
    }
}
