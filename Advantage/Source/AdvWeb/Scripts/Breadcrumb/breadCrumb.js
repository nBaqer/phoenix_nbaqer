﻿(function (jQuery) {

    var getBreadCrumb = function () {

        var breadCrumbText;

        var module = storageCache.getFromSessionCache('currentModule');
        if (!module) return '';

        breadCrumbText = module + ' /';

        var subLevel = storageCache.getFromSessionCache('currentSubLevel');
        if (subLevel) breadCrumbText += subLevel + ' /';

        var pageLevel = storageCache.getFromSessionCache('currentPageName');
        if (pageLevel) breadCrumbText += pageLevel ;


        return breadCrumbText;
    };

    var setModuleCrumb = function () {

        var breadCrumbText;

        var module = storageCache.getFromSessionCache('currentModule');
        if (!module) return '';

        breadCrumbText = module + ' /';

        var subLevel = storageCache.getFromSessionCache('currentSubLevel');
        if (subLevel) breadCrumbText += subLevel + ' /';

        var pageLevel = storageCache.getFromSessionCache('currentPageName');
        if (pageLevel) breadCrumbText += pageLevel;


        return breadCrumbText;
    };

    var saveModuleCrumb = function(mouduleName) {
        storageCache.insertInSessionCache('currentModule', mouduleName);
        storageCache.removeFromSessionCache('currentSubLevel');
        storageCache.removeFromSessionCache('currentPageName');
        console.log('moduleSaved' + breadCrumb.getBreadCrumb());
    };

    var saveSubLevelCrumb = function (subLevelName) {
        storageCache.insertInSessionCache('currentSubLevel', subLevelName);
        storageCache.removeFromSessionCache('currentPageName');
        console.log('subLevelSaved' + breadCrumb.getBreadCrumb());
    };

    var savePageName = function (pageName) {
        storageCache.insertInSessionCache('currentPageName', pageName);
        console.log('pageNameSaved' + breadCrumb.getBreadCrumb());
    };


    window.breadCrumb = window.breadCrumb || {};
    window.breadCrumb.getBreadCrumb = getBreadCrumb;
    window.breadCrumb.saveModuleCrumb = saveModuleCrumb;
    window.breadCrumb.saveSubLevelCrumb = saveSubLevelCrumb;
    window.breadCrumb.savePageName = savePageName;
})(window.jQuery);