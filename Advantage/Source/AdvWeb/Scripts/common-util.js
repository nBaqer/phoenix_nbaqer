﻿var utils = {};
utils.findObjectInArray = function (self, searchFor, property) {
    var retVal = -1;
    for (var index = 0; index < self.length; index++) {
        var item = self[index];
        if (item.hasOwnProperty(property)) {
            if ((item[property] + '').toLowerCase() === (searchFor + '').toLowerCase()) {
                return item;
            }
        }
    };
    return undefined;
};
//---------------------------------------------
//retrieve query string params
// usage: $.QueryString["param"];
//---------------------------------------------
(function ($) {
    //---------------------------------------------------------------------------
    // retrieve query string params
    //---------------------------------------------------------------------------
    $.QueryString = (function (a) {
        if (a == "") return {};
        var b = {};
        for (var i = 0; i < a.length; ++i) {
            var p = a[i].split('=');
            if (p.length != 2) continue;
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
        }
        return b;
    })(window.location.search.substr(1).split('&'));

    //---------------------------------------------------------------------------
    // format a date as a short date
    //---------------------------------------------------------------------------
    Date.prototype.formatMMDDYYYY = function () {
        return (this.getMonth() + 1) +
            "/" + this.getDate() +
            "/" + this.getFullYear();

    };

    Number.prototype.formatMoney = function (c, d, t) {
        var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };



    window.validateDate = function (dateValue) {

        var isValid = true;

        if (dateValue) {
            var dVal = new Date(dateValue);
            var todayDate = new Date();

            if (dVal > todayDate) isValid = false;
        }


        return isValid;
    };

    //---------------------------------------------------------------------------
    // format a date object formatted to account for time zones
    //---------------------------------------------------------------------------
    window.formattedDate = function (val, ignoreSplit) {
        var formattedDate = "";
        if (val) {
            var valAsDate = null;
            if (ignoreSplit == true) valAsDate = new Date(val);
            else valAsDate = dateFromSplit(val);
            formattedDate = new Date(valAsDate.getUTCFullYear(), valAsDate.getUTCMonth(), valAsDate.getUTCDate()).formatMMDDYYYY();
        }

        return formattedDate;
    };

    window.formatUpdateValue = function (show) {
        if (show) return 'Update';
        else return 'Update (Disabled)';
    };

    function dateFromSplit(s) {
        s = s.split(/\D/);
        return new Date(Date.UTC(s[0], --s[1] || '', s[2] || '', s[3] || '', s[4] || '', s[5] || '', s[6] || ''));
    }

    window.toDate = function (value) {
        if (typeof (value) == 'undefined' || value == null)
            return null;

        return new Date(value);
    };


    window.formatCurrencyMask = function (num) {
        var str = num.toString().replace("$", ""), parts = false, output = [], i = 1, formatted = null;
        if (str.indexOf(".") > 0) {
            parts = str.split(".");
            str = parts[0];
        }
        str = str.split("").reverse();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] != ",") {
                output.push(str[j]);
                if (i % 3 == 0 && j < (len - 1)) {
                    output.push(",");
                }
                i++;
            }
        }
        formatted = output.reverse().join("");
        return ("$" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
    };

    window.formatDecimalMask = function (num) {
        var str = num.toString(), parts = false, output = [], i = 1, formatted = null;
        if (str.indexOf(".") > 0) {
            parts = str.split(".");
            str = parts[0];
        }
        str = str.split("").reverse();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] != ",") {
                output.push(str[j]);
                if (i % 3 == 0 && j < (len - 1)) {
                    output.push(",");
                }
                i++;
            }
        }
        formatted = output.reverse().join("");
        return (formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
    };

    window.createGuid = function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }
    window.PositionAtCenter = function (e) {

        if (!$("." + e.sender._guid)[1]) {
            var element = e.element.parent(),
                eWidth = element.width(),
                eHeight = element.height(),
                wWidth = $(window).width(),
                wHeight = $(window).height(),
                newTop,
                newLeft;
            newLeft = Math.floor(wWidth / 2 - eWidth / 2);
            newTop = Math.floor(wHeight / 2 - eHeight / 2);

            e.element.parent().css({ top: newTop, left: newLeft, zIndex: 22222 });
        }
    };
})(jQuery);


var utils = {};
utils.findObjectInArray = function (searchFor, property) {
    var retVal = -1;
    var self = this;
    for (var index = 0; index < self.length; index++) {
        var item = self[index];
        if (item.hasOwnProperty(property)) {
            if ((item[property] + '').toLowerCase() === (searchFor + '').toLowerCase()) {
                return item;
            }
        }
    };
    return undefined;
};

utils.findObjectInDataSource = function (dataSource, searchFor, property) {
    var retVal = -1;
    var self = dataSource;
    for (var index = 0; index < self.length; index++) {
        var item = self[index];
        if (item.hasOwnProperty(property)) {
            if ((item[property] + '').toLowerCase() === (searchFor + '').toLowerCase()) {
                return item;
            }
        }
    };
    return undefined;
};
utils.isValidDate = function (id) {
    var userFormat = "mm/dd/yyy";
    var value = $(id).val();
    var delimiter = /[^mdy]/.exec(userFormat)[0];
    var theFormat = userFormat.split(delimiter);
    var theDate = value.split(delimiter);
    function isDate(date, format) {
        var m, d, y, i = 0, len = format.length, f;
        for (i; i < len; i++) {
            f = format[i];
            if (/m/.test(f)) m = date[i];
            if (/d/.test(f)) d = date[i];
            if (/y/.test(f)) y = date[i];
        }
        return (
          m > 0 && m < 13 &&
          y && y.length === 4 &&
          d > 0 &&
          // Is it a valid day of the month?
          d <= (new Date(y, m, 0)).getDate()
        );
    }
    return isDate(theDate, theFormat);
};
utils.setCurrencyMaskedTextBox = function (id) {
    $(id).css("text-align", "right");
    $(id).unbind("keydown");
    $(id).bind("keydown",
            function (e) {
                if (((e.keyCode >= 48 && e.keyCode <= 57) ||
                    (e.keyCode >= 96 && e.keyCode <= 105) ||
                    e.keyCode === 8 ||
                    e.keyCode === 37 ||
                    e.keyCode === 38 ||
                    e.keyCode === 46 ||
                    e.keyCode === 190 ||
                    e.keyCode === 110)&& (!e.shiftKey)) {
                    if (e.keyCode !== 8 || e.keyCode !== 46) {
                       // $(id).val(window.formatCurrencyMask($(id).val()));
                    }
                    return true;
                }
                if (e.keyCode === 13) {
                    $(id).next().focus();
                } else {
                    return false;
                }
            });
    $(id).unbind("keyup");
    $(id).bind("keyup",
            function (e) {
                var pattern = /^\d+$/;
                var isValid = !pattern.test($(id).val());
                if (isValid && (e.keyCode !== 8 || e.keyCode !== 46) || !e.shiftKey)
                    $(id).val(window.formatCurrencyMask($(id).val()));
                return isValid;
            });
};
utils.setDecimalMaskedTextBox = function (id) {
    $(id).css("text-align", "right");
    $(id).unbind("keydown");
    $(id).bind("keydown",
            function (e) {
                if (((e.keyCode >= 48 && e.keyCode <= 57) ||
                    (e.keyCode >= 96 && e.keyCode <= 105) ||
                    e.keyCode === 8 ||
                    e.keyCode === 37 ||
                    e.keyCode === 38 ||
                    e.keyCode === 46 ||
                    e.keyCode === 190 ||
                    e.keyCode === 110) && (!e.shiftKey)) {
                    if (e.keyCode !== 8 || e.keyCode !== 46) {
                        // $(id).val(window.formatCurrencyMask($(id).val()));
                    }
                    return true;
                }
                if (e.keyCode === 13) {
                    $(id).next().focus();
                } else {
                    return false;
                }
            });
    $(id).unbind("keyup");
    $(id).bind("keyup",
            function (e) {
                var pattern = /^\d+$/;
                var isValid = !pattern.test($(id).val());
                if (isValid && (e.keyCode !== 8 || e.keyCode !== 46) || !e.shiftKey) {
                    $(id).val(window.formatDecimalMask($(id).val()));
                }
                return isValid;
            });
};

Array.prototype.findObjectInArray = utils.findObjectInArray;