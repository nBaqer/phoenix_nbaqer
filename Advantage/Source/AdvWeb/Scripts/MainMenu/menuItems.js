﻿$(function () {

    //-----------------------------------------------------------
    // Initial Setup
    //-----------------------------------------------------------
    common.ajaxSetupBasic();
    //-----------------------------------------------------------


    var handleStorageExpiration = function () {

        var lsCookie = storageCache.getFromSessionCache("AdvantageLocalStorageExpiration");

        if (!lsCookie) {
            //storageCache.createCookie("AdvantageLocalStorageExpiration", new Date().toString(), 2);
            //console.log('Menu Session is Empty Clearing Previous Cache');
            localStorage.clear();
        }
    }


    handleStorageExpiration();

    var isImpersonating = function () {

        var bReturn = false;
        var isImp = $("#hdnImpIsImpersonating").val();

        if (isImp) {
            isImp = isImp.toUpperCase();
            if (isImp === "TRUE") bReturn = true;
        }

        return bReturn;

    };

    var baseUrl = function () {
        return XMASTER_GET_BASE_URL;
    };

    var getMenuText = function (item) {
        var menuText = item.textContent;
        if (!menuText) menuText = item.innerText;
        return menuText;
    };

    //-----------------------------------------------------------
    // show the NavBar
    //-----------------------------------------------------------
    var showNavBar = function () {
        jQuery(".content").fadeIn();
        jQuery(".layer1").fadeIn();;
        jQuery(".heading").fadeIn();
        jQuery(".headerFooter").fadeIn();
    };
    //-----------------------------------------------------------


    //-----------------------------------------------------------
    // hide the NavBar
    //-----------------------------------------------------------
    var hideNavBar = function () {
        jQuery(".content").fadeOut();
        jQuery(".layer1").fadeOut();
        jQuery(".headerFooter").fadeOut();
        jQuery(".heading").fadeOut();

    };
    //-----------------------------------------------------------




    //-----------------------------------------------------------
    // save the module in local cache object
    //-----------------------------------------------------------
    var saveModuleState = function (mouduleName) {
        storageCache.insertInSessionCache('currentModuleName', mouduleName);
        storageCache.removeFromSessionCache('currentSubLevelName');
    };
    //-----------------------------------------------------------

    //-----------------------------------------------------------
    // save the module in local cache object
    //-----------------------------------------------------------
    var saveSubLevelState = function (subLevelName) {
        storageCache.insertInSessionCache('currentSubLevelName', subLevelName);
    };
    //-----------------------------------------------------------

    //-----------------------------------------------------------
    // check if the page group control can be hidden based on 
    // whether the user clicked on the same sub menu item and 
    // whether the control is already open
    //-----------------------------------------------------------
    var isSubMenuSameAndOpen = function (thisMenuItem) {

        var bReturn = false;
        var previousSelectedSub = storageCache.getFromSessionCache("currentSubLevelName");
        var selectedSub = getMenuText(thisMenuItem);

        if (previousSelectedSub == selectedSub && $('#pagegroupdata').is(":visible")) bReturn = true;

        return bReturn;
    };

    //-----------------------------------------------------------
    // Initialize the Kendo SubMenu
    //-----------------------------------------------------------
    var initializeKendoSubMenu = function (data) {
        $("#subMenu").kendoMenu({
            dataSource: data,
            select: function (e) {
                if (isSubMenuSameAndOpen(e.item)) {
                    hideNavBar();
                } else {
                    setSubMenuState(e, false);
                    getPageGroupsAndPages($('#hdnSelectedModule').val(), getMenuText(e.item));
                }
            }
        });
    };

    //-----------------------------------------------------------
    // Initialize the Kendo SubMenu
    //-----------------------------------------------------------
    var initializeKendoSubMenuWithLinks = function (data) {
        $("#subMenu").kendoMenu({
            dataSource: data,
            select: function (e) {
                if (isSubMenuSameAndOpen(e.item)) {
                    hideNavBar();
                } else {
                    setSubMenuState(e, false);
                    getPageGroupsAndPages($('#hdnSelectedModule').val(), getMenuText(e.item));
                }
            }
        });
    };
    //-----------------------------------------------------------


    //-----------------------------------------------------------
    // set the selected state for the module level menu items
    //-----------------------------------------------------------
    var setModuleState = function (e, isFromSelection) {

        var menuItem = e;
        if (isFromSelection == false)
            menuItem = e.item;

        $('#pagegroupdata').html("");
        $("#menu").find(".k-state-selected").removeClass("k-state-selected");
        $(menuItem).addClass("k-state-selected");
        $('#hdnSelectedModule').val(getMenuText(menuItem));

        saveModuleState(getMenuText(menuItem));

        hideNavBar();
    };
    //-----------------------------------------------------------




    //-----------------------------------------------------------
    // set the selected state for the sub menu level menu items
    //-----------------------------------------------------------
    var setSubMenuState = function (e, isFromSelection) {

        var menuItem = e;
        if (isFromSelection === false)
            menuItem = e.item;

        $("#subMenu").find(".k-state-selected").removeClass("k-state-selected");
        $(menuItem).addClass("k-state-selected");

        saveSubLevelState(getMenuText(menuItem));
    };
    //-----------------------------------------------------------



    //-----------------------------------------------------------
    // hide the RadPane to show the sub menu
    //-----------------------------------------------------------
    function resizeRadPane(clientId) {

        var pane = $find(clientId);
        if (pane != null) {
            pane.set_height(52);
        }
    }



    //-----------------------------------------------------------
    // retrieve the campus id from the either the campus control,
    // the campus variable, or query string
    //-----------------------------------------------------------
    var getCurrentCampusId = function () {

        var campusId = $("#MasterCampusDropDown").val();
        if (!campusId) campusId = XMASTER_GET_CURRENT_CAMPUS_ID;
        if (!campusId) campusId = $.QueryString["cmpid"];

        return campusId;

    };


    //-----------------------------------------------------------
    // Event handlers for NavBar
    //-----------------------------------------------------------
    jQuery(".heading,.headerFooter").click(function () {
        //event.preventDefault();
        hideNavBar();
    });
    //-----------------------------------------------------------


    //-----------------------------------------------------------
    // get SubMenu from cache or call method to get from service
    //-----------------------------------------------------------
    var getSubMenu = function (e) {

        var currentCampusId = getCurrentCampusId();
        if (!currentCampusId) currentCampusId = $.QueryString["cmpid"];
        var userId = $('#hdnUserId').val();

        if (getMenuText(e.item) == 'Reports') {
            window.location.href = baseUrl() + "Reports/ReportHome.aspx?resid=689&desc=reportHome&userid=" + userId + "&cmpid=" + currentCampusId;
        }
        else if (getMenuText(e.item) == 'Maintenance') {
            window.location.href = baseUrl() + "SY/MaintenanceHome.aspx?resid=688&desc=maintenanceHome&userid=" + userId + "&cmpid=" + currentCampusId;
        }
        else {

            var cacheKey = getMenuText(e.item) + "_" + userId + "_" + currentCampusId + "_" + "_submenuitem";



            if (isImpersonating())
                localStorage.clear();

            var cachedData = JSON.parse(storageCache.getFromSessionCache(cacheKey));

            if (cachedData) {
                if (getMenuText(e.item) == "Admissions")
                    initializeKendoSubMenuWithLinks(cachedData);
                else
                    initializeKendoSubMenu(cachedData);

                resizeRadPane($("#hdnKendoPane").val());

            } else {
                if (getMenuText(e.item) == "Admissions")
                    getSubFromServiceWithLinks(getMenuText(e.item));
                else
                    getSubFromService(getMenuText(e.item));
            }
        }

    };
    //-----------------------------------------------------------


    //-----------------------------------------------------------
    // get sub menu from service
    //-----------------------------------------------------------
    var getSubFromService = function (subMenuName) {

        if (subMenuName == 'Reports' || subMenuName == 'Maintenance') return;

        var currentCampusId = getCurrentCampusId();
        if (!currentCampusId) currentCampusId = $.QueryString["cmpid"];

        var userId = $('#hdnUserId').val();

        var cacheKey = subMenuName + "_" + userId + "_" + currentCampusId + "_" + "_submenuitem";
        var uri;
        if (subMenuName === "Maintenance") {
            uri = baseUrl() + 'proxy/api/SystemStuff/FastReports/Menu/GetPageGroupAndPages';
        }
        else {
            uri = baseUrl() + 'proxy/api/SystemStuff/{menuName}/Menu'.supplant({ menuName: subMenuName });
        }
        if (currentCampusId) {
            var filter = { 'CampusId': currentCampusId };

            var requestData2 = $.getJSON(uri, filter, function (data) {
                initializeKendoSubMenu(data);
                resizeRadPane($("#hdnKendoPane").val());

                storageCache.insertInSessionCache(cacheKey, JSON.stringify(data));

                var sub = $("#subMenu");
                for (var x = 0; x < sub[0].children.length; x++) {

                    var subName = getMenuText(sub[0].children[x]);
                    if (subName == subLevelNameFromCache)
                        setSubMenuState(sub[0].children[x], true);
                }
            });
        }
    };


    //-----------------------------------------------------------
    // get sub menu from service
    //-----------------------------------------------------------
    var getSubFromServiceWithLinks = function (subMenuName) {

        if (subMenuName == 'Reports' || subMenuName == 'Maintenance') return;

        var currentCampusId = getCurrentCampusId();
        if (!currentCampusId) currentCampusId = $.QueryString["cmpid"];

        var userId = $('#hdnUserId').val();

        var cacheKey = subMenuName + "_" + userId + "_" + currentCampusId + "_" + "_submenuitem";

        var uri = baseUrl() + 'proxy/api/SystemStuff/GetSubLevelItemsWithLinks'.supplant({ menuName: subMenuName });

        if (currentCampusId) {
            var filter = { 'CampusId': currentCampusId, UserId: userId, MenuWithLinks: subMenuName };

            var requestData2 = $.getJSON(uri, filter, function (data) {
                initializeKendoSubMenuWithLinks(data);
                resizeRadPane($("#hdnKendoPane").val());

                storageCache.insertInLocalCache(cacheKey, JSON.stringify(data));

                var sub = $("#subMenu");
                for (var x = 0; x < sub[0].children.length; x++) {

                    var subName = getMenuText(sub[0].children[x]);
                    if (subName == subLevelNameFromCache)
                        setSubMenuState(sub[0].children[x], true);
                }
            });
        }
    };

    //-----------------------------------------------------------
    // ajax call for page groups and pages
    //-----------------------------------------------------------
    var getPageGroupsAndPages = function (selectedModule, selectedSubLevel) {

        var currentCampusId = getCurrentCampusId();
        if (!currentCampusId) currentCampusId = $.QueryString["cmpid"];

        var userId = $('#hdnUserId').val();


        var cacheKey = selectedModule + "_" + selectedSubLevel + "_" + userId + "_" + currentCampusId + "_" + "_pagegroup_pages";

        if (isImpersonating())
            localStorage.clear();

        var cachedData = JSON.parse(storageCache.getFromSessionCache(cacheKey));

        if (cachedData) {
            renderLinks(cachedData);
        } else {


            var filter = { 'CampusId': currentCampusId, 'UserId': userId, 'IsImpersonating': isImpersonating() };
            var uri;
            if (selectedModule === "Maintenance") {
                uri = baseUrl() + 'proxy/api/SystemStuff/FastReports/Menu/GetPageGroupAndPages';
            } else {
                uri = baseUrl() +
                    'proxy/api/SystemStuff/{moduleName}/Menu/{subMenuName}'
                    .supplant({ moduleName: selectedModule, subMenuName: selectedSubLevel });
            }
            var requestData3 = $.getJSON(uri, filter, function (data) {
                renderLinks(data);
                storageCache.insertInSessionCache(cacheKey, JSON.stringify(data));
            });
        }
    };
    //-----------------------------------------------------------



    //-----------------------------------------------------------
    // Create NavBar content
    //-----------------------------------------------------------
    var renderLinks = function (data) {

        var htmlData = "<table border=0 cellpadding='10' cellspacing='10' ><tr>";
        for (var i = 0; i < data.length; i++) {

            if (data[i].items.length > 0) {
                htmlData += "<td valign='top' align='left' ><span class='linkHeader'>" + data[i].text + "</span><br><br>";
                for (var x = 0; x < data[i].items.length; x++) {
                    if (data[i].items[x].IsEnabled == "True") {
                        if (data[i].items[x].url.toLowerCase().indexOf("&ispopup=1") >= 0) {
                            htmlData += "<a href='#' onclick=\"openRadWindow('" + data[i].items[x].url + "');\">" + data[i].items[x].text + "</a><br>";
                        } else {
                            htmlData += "<a class='menuItemLink' href='" + data[i].items[x].url + "'>" + data[i].items[x].text + "</a><br>";
                        }

                    } else {
                        htmlData += "<span class='disabledLink'>" + data[i].items[x].text + "</span><br>";
                    }
                }

                htmlData += "</td>";
            }
        }

        htmlData += "</tr></table>";

        $('#pagegroupdata').html(htmlData);

        showNavBar();

    };
    //-----------------------------------------------------------


    //-----------------------------------------------------------
    // Redirect the page to the dashboard if the user is not enabled
    // for this page
    //-----------------------------------------------------------
    var redirectIfPageNotEnabled = function () {
        var pageName = common.pageFromUrl();

        if (pageName.toLowerCase() != 'dash.aspx' && pageName.toLowerCase() !== 'userimpersonation.aspx') {
            var cmpId = getCurrentCampusId();
            var userId = $('#hdnUserId').val();
            var filter = { 'CampusId': cmpId, 'UserId': userId, PageName: pageName };

            var uri = baseUrl() + 'proxy/api/SystemStuff/Menu/GetIsPageEnabledForUser';
            var requestData5 = $.getJSON(uri, filter, function (data) {
                if (data == false) {
                    window.location.href = baseUrl() + "dash.aspx?resid=264&desc=dashboard&userid=" + userId;
                    // console.log(data);
                }
            });
        }
    };


    //-----------------------------------------------------------
    // Load the modules from a static list
    //-----------------------------------------------------------
    $("#menu").kendoMenu({
        dataSource: [{ "Id": 2, "text": "Admissions" },
                    { "Id": 1, "text": "Academics" },
                    { "Id": 8, "text": "Faculty" },
                    { "Id": 6, "text": "Student Accounts" },
                    { "Id": 3, "text": "Financial Aid" },
                    { "Id": 5, "text": "Placement" },
                    { "Id": 4, "text": "Human Resources" },
                    { "Id": 10, "text": "Reports" },
                    { "Id": 13, "text": "Tools" },
                    { "Id": 11, "text": "Maintenance" }]
        // The System functionality was merged with the Maintenance module. { "Id": 12, "text": "System" }]
                    ,
        select: function (e) {
            setModuleState(e, false);
            getSubMenu(e);
        }
    });


    //-----------------------------------------------------------
    // Check for module and sublevel item in cache.  If it exits,
    // set the module and sublevel menus from cache.
    //-----------------------------------------------------------
    var moduleMenu = $("#menu");
    var moduleName = storageCache.getFromSessionCache('currentModuleName');
    var subLevelNameFromCache = storageCache.getFromSessionCache('currentSubLevelName');
    if (moduleName != null) {
        for (var i = 0; i < moduleMenu[0].children.length; i++) {
            var menuName = getMenuText(moduleMenu[0].children[i]);
            if (menuName == moduleName)
                setModuleState(moduleMenu[0].children[i], true);
        }

        if (moduleName == "Admissions")
            getSubFromServiceWithLinks(moduleName);
        else
            getSubFromService(moduleName);
    }
    //-----------------------------------------------------------


    //-----------------------------------------------------------
    // If NavBar is visible and user clicks outside the control, 
    // hide it.
    //-----------------------------------------------------------
    $(document).click(function (event) {
        if ($(event.target).parents().index($('#menuContent')) == -1) {
            if ($('#menuContent').is(":visible")) {
                hideNavBar();
            }
        }
    });


    // call method to hide the NavBar
    hideNavBar();

    // redirectIfPageNotEnabled();




});
