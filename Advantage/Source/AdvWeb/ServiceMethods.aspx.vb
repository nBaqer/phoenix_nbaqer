﻿
Imports System.Data
Imports System.Web.Script.Services
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports Newtonsoft.Json
Imports System.IO
Imports FAME.AdvantageV1.DataAccess
Imports FAME.AdvantageV1.BusinessFacade.TimeClock
Imports FAME.Advantage.Api.Library.Models.Common
Imports FAME.Advantage.Api.Library.TimeClock
Imports FAME.Advantage.Api.Library.Models
Imports FAME.Advantage.Api.Library.Models.TimeClock
Imports FAME.Advantage.Api.Library.Storage

Partial Class ServiceMethods
    Inherits System.Web.UI.Page

    <System.Web.Services.WebMethod(EnableSession:=True)>
    <ScriptMethod(ResponseFormat:=ResponseFormat.Json, UseHttpGet:=False)>
    Public Shared Function ImportTimeClockFile() As ActionResult(Of String)

        'parse body 
        Dim jsonBody = ""
        Using stream = New MemoryStream()
            Dim Request = HttpContext.Current.Request
            Request.InputStream.Seek(0, SeekOrigin.Begin)
            Request.InputStream.CopyTo(stream)
            jsonBody = Encoding.UTF8.GetString(stream.ToArray())
        End Using

        Dim inputParams = JsonConvert.DeserializeObject(Of InputParamsImportTimeClock)(jsonBody)
        Dim rv = New ActionResult(Of String)
        'set parameters
        Dim selected = "0"
        Dim username = inputParams.Username
        Dim tenantUserId = inputParams.TenantUserId
        Dim tenantId = inputParams.TenantId
        Dim advantageApiToken = inputParams.AdvantageApiToken
        Dim fileName = inputParams.FileName
        Dim campusId = inputParams.CampusId
        Dim apiUrl = inputParams.ApiUrl
        Dim sbuf As String
        Dim MyAdvAppSettings As AdvAppSettings

        'set session variables
        HttpContext.Current.Session("TenantUserId") = tenantUserId
        HttpContext.Current.Session("TenantNameId") = tenantId
        HttpContext.Current.Session("AdvantageApiToken") = New TokenResponse() With {
            .ApiUrl = apiUrl,
            .Token = advantageApiToken
        }

        Dim exTracker = New AdvApplicationInsightsInitializer()
        Dim tc As TimeClockFacade = New TimeClockFacade()
        Dim logResult = tc.UpsertTimeClockImportLog(New TimeClockLogParams() With {
                                    .CampusId = New Guid(campusId),
                                    .FileName = fileName,
                                    .Message = "Processing",
                                    .Status = 2,
                                    .IsAutoImport = True
                                    })
        Dim logId = IIf(String.IsNullOrEmpty(logResult.Result), Nothing, New Guid(logResult.Result))


        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            Dim readFileContentsRequest = New ReadFileContentsRequest(apiUrl, advantageApiToken)
            Dim fileContentParams = New ReadFileContentsParams() With {
                .CampusId = New Guid(campusId),
                .FeatureType = Enums.FileConfigurationFeature.TCI,
                .FileName = fileName
            }

            Dim fileRequestResult = readFileContentsRequest.ReadFileContents(fileContentParams)
            If fileRequestResult.ResultStatus = Enums.ResultStatus.Error Then
                Dim msg = "Could not read file contents : " + fileRequestResult.ResultStatusMessage _
                 + " | " + fileContentParams.CampusId.ToString() + " | " + fileContentParams.FileName
                exTracker.TrackExceptionWrapper(New Exception(msg))
                rv.ResultStatus = Enums.ResultStatus.Error
                rv.ResultStatusMessage = msg
                tc.UpsertTimeClockImportLog(New TimeClockLogParams() With {
                                    .CampusId = New Guid(campusId),
                                    .FileName = fileName,
                                    .Message = "Automated Import" + Environment.NewLine + msg,
                                    .Status = 0,
                                    .TimeClockImportLogId = logId
                                    })
                Return rv
            End If

            sbuf = fileRequestResult.Result

            If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" Then
                If selected = "0" Then
                    tc.Run(sbuf, username, campusId, fileName)
                ElseIf selected = "1" Then
                    tc.Run(sbuf, FAME.AdvantageV1.Common.TimeClock.TimeClockDataType.ZON580, username, campusId, fileName)
                Else
                    tc.Run(sbuf, FAME.AdvantageV1.Common.TimeClock.TimeClockDataType.CS2000, username, campusId, fileName)
                End If
            Else
                tc.Run_ByClass(sbuf, username, campusId, fileName)
            End If

            'save time clock import log to database
            Dim text = tc.Log()

            tc.UpsertTimeClockImportLog(New TimeClockLogParams() With {
                                    .CampusId = New Guid(campusId),
                                    .FileName = fileName,
                                    .Message = "Automated Import" + Environment.NewLine + text,
                                    .Status = 1,
                                    .TimeClockImportLogId = logId,
                                    .IsAutoImport = True
                                    })

            Return New ActionResult(Of String) With {
                .Result = text,
                .ResultStatus = Enums.ResultStatus.Success,
                .ResultStatusMessage = "Successfully processed file." + campusId.ToString() + " " + fileName
            }
        Catch ex As Exception

            Return New ActionResult(Of String) With {
                .Result = ex.Message,
                .ResultStatus = Enums.ResultStatus.Error,
                .ResultStatusMessage = "Error processing time clock file."
            }
        End Try
    End Function
End Class

Partial Public Class InputParamsImportTimeClock
    Public Property Username As String = ""
    Public Property TenantUserId As String = ""
    Public Property TenantId As String = ""
    Public Property AdvantageApiToken As String = ""
    Public Property FileName As String = ""
    Public Property CampusId As String = ""
    Public Property ApiUrl As String = ""
End Class
