
Partial Class ErrorPage_Old
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim strError As String
        'Dim instance As HttpRequest
        'Dim value As Uri

        'value = instance.UrlReferrer

        Dim MyUrl As Uri = Request.UrlReferrer
        Try
            If InStr(Server.HtmlEncode(MyUrl.AbsolutePath), "AdHocReportViewer") >= 1 Then
                Dim strMessage As String = ""
                strMessage = "The following error occured:" & "<br>"
                strMessage &= "File not loaded completely" & "<br>"
                strMessage &= "<br>"
                strMessage &= "In Microsoft Word,Microsoft Excel 97 for Windows, Microsoft Excel 2000, Microsoft Excel 2002, and Microsoft Office Excel 2003, " & "<br>"
                strMessage &= "text files that contain more than certain number of rows cannot be opened in their entirety. " & "<br>"
                strMessage &= "You cannot open these files because these versions of Microsoft Excel are limited to certain number of  rows." & "<br>"
                strMessage &= "If you open a file that contains more data than this, the text file is truncated " & "<br>"
                strMessage &= "<br>"
                strMessage &= "Versions of Excel earlier than Excel 97 have a limit of 16,384 rows."
                Response.Write("<div class=""errordescription"">" & strMessage & "</div>")
            Else
                strError = Session("Error")
                Response.Write("<div class=""error"">The following error occurred:</div><div class=""errordescription"">" & strError & "</div>")
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            strError = Session("Error")
            Response.Write("<div class=""error"">The following error occurred:</div><div class=""errordescription"">" & strError & "</div>")
        End Try






    End Sub

End Class
