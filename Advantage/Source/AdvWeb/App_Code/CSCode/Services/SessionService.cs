﻿using System;

/// <summary>
/// Session wrapper class to assist with readability and code cleanliness.
/// </summary>
[Serializable()]
public static class SessionService
{

    /// <summary>
    /// Custom delegate to perform action when key is not found in Session.
    /// </summary>
    /// <typeparam name="T">Generic Return Type</typeparam>
    /// <returns>Typed Object</returns>
    public delegate T SessionInvoke<T>();

    /// <summary>
    /// Add with a strongly type parameter, mostly to indicate object type getting added.
    /// </summary>
    /// <typeparam name="T">Typeof(Object)</typeparam>
    /// <param name="key">Session Key</param>
    /// <param name="obj">Typeof(Object)</param>
    public static void Add<T>(string key, T obj)
    {
        System.Web.HttpContext.Current.Session[key] = obj;
    }

    /// <summary>
    /// Set and Add perform the same functionality, but the verbs are clear specified in this fashion.
    /// </summary>
    /// <typeparam name="T">Typeof(Object)</typeparam>
    /// <param name="key">Session Key</param>
    /// <param name="obj">Typeof(Object)</param>
    public static void Set<T>(string key, T obj)
    {
        System.Web.HttpContext.Current.Session[key] = obj;
    }

    /// <summary>
    /// Set Session using the common key enums.
    /// </summary>
    /// <typeparam name="T">Typeof(Object)</typeparam>
    /// <param name="key">SessionKey Enum</param>
    /// <param name="obj">Typeof(Object)</param>
    public static void Set<T>(SessionKey key, T obj)
    {
        Set<T>(key.ToString(), obj);
    }

    /// <summary>
    /// Strongly Type retrieval from Session.
    /// </summary>
    /// <typeparam name="T">Typeof(Object)</typeparam>
    /// <param name="key">Session Key</param>
    /// <returns>Typeof(Object)</returns>
    public static T Get<T>(string key)
    {
        return (T)System.Web.HttpContext.Current.Session[key];
    }

    /// <summary>
    /// Retrieve from Session using common key enum.
    /// </summary>
    /// <typeparam name="T">Typeof(Object)</typeparam>
    /// <param name="key">SessionKey Enum</param>
    /// <returns>Typeof(Object)</returns>
    public static T Get<T>(SessionKey key)
    {
        if (key == SessionKey.NullObject)
            return default(T);

        return Get<T>(key.ToString());
    }

    public static T Get<T>(SessionKey key, SessionInvoke<T> sessionInvoke)
    {
        if (Contains(key.ToString()))
            return Get<T>(key);
        if (sessionInvoke==null)
            return default(T);

        return sessionInvoke.Invoke();
    }

    /// <summary>
    /// Strongly Type retrieval from Session allowing the specification of a Default return in case of null.
    /// </summary>
    /// <typeparam name="T">Typeof(Object)</typeparam>
    /// <param name="key">Session Key</param>
    /// <param name="defaultValue">Typeof(Object) default</param>
    /// <returns>Typeof(Object)</returns>
    public static T Get<T>(string key, T defaultValue = default(T))
    {
        var retValue = Get<T>(key);
        if (retValue == null)
            retValue = defaultValue;
        return retValue;
    }

    /// <summary>
    /// Clear matching Session key.
    /// </summary>
    /// <param name="key">Session Key</param>
    public static void Remove(string key)
    {
        System.Web.HttpContext.Current.Session[key] = null;
    }

    /// <summary>
    /// Clear matching SessionKey Enum.
    /// </summary>
    /// <param name="key">SessionKey Enum</param>
    public static void Remove(SessionKey key)
    {
        System.Web.HttpContext.Current.Session[key.ToString()] = null;
    }

    /// <summary>
    /// Clear Session.
    /// </summary>
    public static void Clear()
    {
        System.Web.HttpContext.Current.Session.Clear();
    }

    /// <summary>
    /// Check Session for matching key.
    /// </summary>
    /// <param name="key">Session Key</param>
    /// <returns>bool</returns>
    public static bool Contains(string key)
    {
        return System.Web.HttpContext.Current.Session[key] != null;
    } 

}