﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public enum ContextItemKey
{
    NullObject = 0,
    ResourceId = 1,
    Language = 2
}