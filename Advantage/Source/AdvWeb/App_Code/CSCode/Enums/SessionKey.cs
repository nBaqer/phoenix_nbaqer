﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public enum SessionKey
{
    NullObject = 0,
    AdvAppSettings = 1,
    EmployerObjectPointer = 2,
    ResId = 3,
    CurrentCampus = 4,
    UserName = 5,
    UserId = 6,
    MenuLoaded = 7,
    SwitchEntity = 8
}