﻿using System.Web;
using System.Web.Routing;

//Part of ingrastructure to share Web Forms Session with Web Api 
public class SessionStateRouteHandler : IRouteHandler
{
    IHttpHandler IRouteHandler.GetHttpHandler(RequestContext requestContext)
    {
        return new SessionableControllerHandler(requestContext.RouteData);
    }
}
