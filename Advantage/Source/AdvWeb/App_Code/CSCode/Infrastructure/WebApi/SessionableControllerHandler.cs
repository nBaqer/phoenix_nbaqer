﻿using System.Web.Http.WebHost;
using System.Web.Routing;
using System.Web.SessionState;

//Part of ingrastructure to share Web Forms Session with Web Api 
public class SessionableControllerHandler : HttpControllerHandler, IRequiresSessionState
{
    public SessionableControllerHandler(RouteData routeData)
        : base(routeData)
    { }
}