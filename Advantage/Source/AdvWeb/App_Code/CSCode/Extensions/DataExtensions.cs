﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public static class DataExtensions
{
    public static bool Contains(this DataTable dt, string field, string value)
    {
        if (dt == null || dt.Rows.Count == 0)
            return false;

        var result = dt.Select(string.Format("{0} = '{1}'", field, value));
        if (result.Length <= 0)
            return false;

        return true;
    }

    public static bool Contains(this DataTable dt, string key)
    {
        if (dt != null)
        {
            var result = dt.Rows.Find(key);
            if (result != null)
                return true;
        }

        return false;
    }
}