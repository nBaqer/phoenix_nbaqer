﻿using System;

public static class GuidExtensions
{
    public static bool IsEmpty(this Guid input)
    {
        return input.Equals(Guid.Empty);
    }

    public static bool IsEmpty(this Guid? input)
    {
        return !input.HasValue || input.Value.IsEmpty();
    }
}


