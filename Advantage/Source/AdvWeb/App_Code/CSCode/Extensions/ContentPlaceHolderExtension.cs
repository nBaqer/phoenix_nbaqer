﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

using FAME.Advantage.Common;
using FAME.AdvantageV1.BusinessFacade;
using FAME.ExceptionLayer;

/// <summary>
/// PlaceHolder Control applicable extensions
/// </summary>
public static class ContentPlaceHolderExtension
{

    public static void ApplyValidation(this ContentPlaceHolder placeHolder, bool InternationalZIP = false, bool InternationalPhone1 = false, bool InternationalPhone2 = false)
    {
        try
        {
            var appSettings = SessionService.Get<AdvAppSettings>(SessionKey.AdvAppSettings, () => { return new AdvAppSettings(); });
            var resourceId = Utility.GetValue<int>(HttpContext.Current.Items[ContextItemKey.ResourceId.ToString()]);
            var language = HttpContext.Current.Items[ContextItemKey.Language.ToString()].ToString().ToUpper();

            var resourceDef = ResourceDefinition.Load(new PageSetupFacade().GetResourceDef(resourceId, language));        // Get the resource definition table for this page.
            var requiredFields = new PageSetupFacade().GetDataDictionaryReqFields();                                      // Get the data dictionary required fields.
            var resourceRequiredFields = new PageSetupFacade().GetSchlRequiredFieldsForResource(resourceId);              // Get the school required fields for the resource being viewed.

            var ipedsFields = new DataTable();
            if (Utility.GetValue<bool>(appSettings.AppSettings("IPEDS")))
                ipedsFields = new PageSetupFacade().GetIPEDSFields();

            ipedsFields.PrimaryKey = new DataColumn[] { ipedsFields.Columns["FldId"] };

            string strControl, strPrefix, strFldLen = string.Empty;
            Control ctl = null, ctl2 = null;
            var cntMultiCheck = new string[] { "txt", "ddl" };
            var rfvMultiCheck = new string[] { "txt", "rad" };

            foreach (var resDef in resourceDef)
            {
                if (!string.IsNullOrEmpty(resDef.ControlName))
                {
                    strControl = resDef.ControlName;
                    strPrefix = strControl.Substring(0, 3);
                }
                else
                {
                    if (!resDef.DDLId.HasValue)                                         // If the DDLId associated with the record is null then we are
                        if (resDef.FldType == "Bit")                                    // dealing with a textbox or a checkbox. If not we are dealing with a dropdownlist.
                            strPrefix = "chk";                                          // A checkbox normally is associated with a bit field in the database.
                        else
                            strPrefix = "txt";
                    else
                        strPrefix = "ddl";

                    strControl = strPrefix + resDef.FldName;
                }

                ctl = placeHolder.FindControl(strControl);
                if (ctl == null)
                    throw new Exception(string.Format("Could not find the control: {0}", strControl));

                ctl2 = placeHolder.FindControl("pnlRequiredFieldValidators");
                if (ctl2 == null)
                    throw new Exception("Could not find panel named pnlRequiredFieldValidators");

                strFldLen = resDef.FldLen.ToString();

                // Add Regular Expressions for specific fields - Hack
                if (rfvMultiCheck.Contains(strPrefix))
                {
                    var rev = new RegularExpressionValidator
                    {
                        ID = string.Format("rev{0}{1}", resDef.FldName, resDef.TblFldsId),
                        ControlToValidate = strControl,
                        Display = ValidatorDisplay.None
                    };
                    if (!InternationalZIP && resDef.FldName == "Zip")
                    {
                        rev.ErrorMessage = "Please enter the proper Zip Code";
                        rev.ValidationExpression = string.Format("{0}{1},{2}{3}", "^(.|\n|\t){", 5, resDef.FldLen, "}$");
                        ctl2.Controls.Add(rev);
                    }
                    else if (!InternationalPhone1 && "Phone" == resDef.FldName)
                    {
                        rev.ErrorMessage = string.Format("{0} format is (###) ###-####", resDef.Caption);
                        rev.ValidationExpression = string.Format("{0}{1},{2}{3}", "^(.|\n|\t){", 10, resDef.FldLen, "}$");
                        ctl2.Controls.Add(rev);
                    }
                    else if (!InternationalPhone2 && "Phone2" == resDef.FldName)
                    {
                        rev.ErrorMessage = string.Format("{0} format is (###) ###-####", resDef.Caption);
                        rev.ValidationExpression = string.Format("{0}{1},{2}{3}", "^(.|\n|\t){", 10, resDef.FldLen, "}$");
                        ctl2.Controls.Add(rev);
                    }
                }

                // If we are dealing with a textbox or a ddl then we need to check if there is an associated label control.
                if (cntMultiCheck.Contains(strPrefix))
                {
                    var ctl3 = placeHolder.FindControl(string.Format("lbl{0}", resDef.FldName));
                    if (ctl3 != null) // The control might not have a label control associated with it.
                        try { (ctl3 as Label).Text = resDef.Caption; }
                        catch (Exception)
                        {
                            throw new BaseException(string.Format("Problem casting {0} to label with caption {1}", strControl, resDef.Caption));
                        }

                    // The label control might be named after the name of the ddl or textbox control.
                    // This should only be performed when the controlname is specified
                    if (!string.IsNullOrEmpty(resDef.ControlName))
                    {
                        var ctl30 = placeHolder.FindControl("lbl" + resDef.ControlName.Substring(3));
                        if (ctl30 != null)
                            try { (ctl30 as Label).Text = resDef.Caption; }
                            catch (Exception)
                            {
                                throw new BaseException(string.Format("Problem casting {0} to label with caption {1}", strControl, resDef.Caption));
                            }
                    }
                }
                else if (strPrefix == "chk")
                {

                    var ctl3 = placeHolder.FindControl("chk" + resDef.FldName);                         // We have to find the checkbox itself
                    if (ctl3 == null)                                                                   //If the checkbox was not found then we should notify the client
                        throw new Exception(string.Format("Could not find checkbox: {0}", strControl));

                    // The checkbox might be found but we might have a problem in the casting operation.
                    try { (ctl3 as CheckBox).Text = resDef.Caption; }
                    catch (Exception)
                    {
                        throw new BaseException(string.Format("Problem casting {0} to checkbox with caption {1}", strControl, resDef.Caption));
                    }
                }


                // Color the required fields.
                var resources = new int[] { 174, 203, 169 };
                if (resDef.Required ||
                    requiredFields.Contains("FldId", resDef.FldId.ToString()) ||
                    resourceRequiredFields.Contains("TblFldsId", resDef.TblFldsId.ToString()) ||
                    (resources.Contains(resourceId) &&
                    Utility.GetValue<bool>(appSettings.AppSettings("IPEDS")) &&
                    ipedsFields.Contains(resDef.FldId.ToString())))
                {
                    try
                    {
                        //If international address is checked and its a state field then do not mark it as required - Balaji 03/04/2011
                        if (InternationalZIP && resDef.FldId == 11)
                            break;

                        var ctl3 = placeHolder.FindControl(string.Format("lbl{0}", resDef.FldName));
                        if (ctl3 != null)                                                                   // The control might not have a label control associated with it.
                            if (resourceId == 174 && resDef.FldId == 11)
                                (ctl3 as Label).Text = resDef.Caption;
                            else if ((resourceId == 206 || resourceId == 170) && resDef.FldName == "Phone")
                                (ctl3 as Label).Text = "Phone 1<font color='red'>*</font>";                 // New Lead and Existing Lead pages
                            else
                                (ctl3 as Label).Text = resDef.Caption + "<font color='red'>*</font>";
                    }
                    catch (Exception ex) { throw ex; }


                    if (rfvMultiCheck.Contains(strPrefix))                                                  // Add a RequiredFieldVaidator to monitor the control.
                        try
                        {
                            if (resDef.FldName.ToLower() == "adreqid")
                                break;

                            var rfv = new RequiredFieldValidator
                            {
                                ID = string.Format("rfv{0}{1}", resDef.FldName, resDef.TblFldsId),
                                ControlToValidate = strControl,
                                Display = ValidatorDisplay.None,
                                ErrorMessage = string.Format("{0} is required", resDef.Caption)
                            };
                            ctl2.Controls.Add(rfv);

                        }
                        catch (Exception) { }


                    // Add a regular expression validator to monitor the control.
                    if (strPrefix == "ddl")
                        try
                        {
                            // If international address is checked and its a state field then do not mark it as required - Balaji 03/04/2011
                            if (InternationalZIP && resDef.FldId == 11)
                                break;
                            if (resourceId == 174 && resDef.FldId == 11)
                                break; // Don't add required validator for State Field - Balaji 03/09/2011
                            if (resourceId == 337 && resDef.FldName.ToLower() == "adreqid")
                                break;  //'Don't add required validator for State Field - Balaji 02/06/2012

                            var rfv = new RequiredFieldValidator
                            {
                                ID = string.Format("rfv{0}{1}", resDef.FldName, resDef.TblFldsId.ToString()),
                                ControlToValidate = strControl,
                                Display = ValidatorDisplay.None,
                                ErrorMessage = string.Format("{0} is required", resDef.Caption),
                                InitialValue = string.Empty,
                                EnableClientScript = true
                            };

                            var cv = new CompareValidator()
                            {
                                ID = string.Format("cv{0}{1}", resDef.FldName, resDef.TblFldsId),
                                ControlToValidate = strControl,
                                Display = ValidatorDisplay.None,
                                ErrorMessage = string.Format("{0} is required", resDef.Caption),
                                ValueToCompare = Guid.Empty.ToString(),
                                Operator = ValidationCompareOperator.NotEqual,
                                EnableClientScript = true
                            };

                            ctl2.Controls.Add(rfv);
                            ctl2.Controls.Add(cv);
                        }
                        catch (Exception) { }

                }
                else
                    if ((resourceId == 206 || resourceId == 170) && resDef.FldName == "Phone")
                    {
                        // New Lead and Existing Lead pages
                        // We want the label to just say Phone 1
                        var ctl3 = placeHolder.FindControl("lbl" + resDef.FldName);

                        if (ctl3 != null)                                                       // The control might not have a label control associated with it.
                            (ctl3 as Label).Text = "Phone 1";

                    }


                //If the control is a textbox we need to also set the MaxLength property and also add a CompareValidator to perform a data type check. 
                if (strPrefix == "txt")
                {
                    if (ctl.GetType() == typeof(TextBox))
                        if (resDef.FldType == "Varchar" || resDef.FldType == "Char")
                            (ctl as TextBox).MaxLength = resDef.FldLen;
                        else
                            (ctl as TextBox).MaxLength = 50;

                    try
                    {
                        if (resDef.FldName.ToLower() == "adreqid")
                            break;

                        var cmv = new CompareValidator()
                        {
                            ID = string.Format("cmv{0}{1}", resDef.FldName, resDef.TblFldsId),
                            ControlToValidate = strControl,
                            Display = ValidatorDisplay.None,
                            ErrorMessage = string.Format("Incorrect data type for {0}", resDef.Caption),
                            Operator = ValidationCompareOperator.DataTypeCheck,
                            Type = resDef.ValidatorType,
                        };

                        ctl2.Controls.Add(cmv);
                    }
                    catch (Exception) { }
                }

            }
        }
        catch (Exception ex)
        {
            if (ex.InnerException != null)
                throw new BaseException(string.Format("Error required fields info for ResourceId {0} - {1}", HttpContext.Current.Items[ContextItemKey.ResourceId.ToString()], ex.InnerException.Message));
            else
                throw new BaseException(string.Format("Error required fields info for ResourceId {0} - {1}", HttpContext.Current.Items[ContextItemKey.ResourceId.ToString()], ex.Message));
        }
    }
}