﻿using System.Net.Http;

namespace AdvWeb.CSCode.Extensions
{
    public static class HttpRequestMessageExtensions
    {
        public static void CopyHeadersTo(this HttpRequestMessage source, HttpRequestMessage target)
        {
            foreach (var header in source.Headers)
            {
                target.Headers.Add(header.Key, header.Value);
            }
        }
    }
}