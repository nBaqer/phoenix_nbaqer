﻿using System;

namespace AdvWeb.CSCode.Proxy
{
    public sealed class ProxySettings
    {
        private readonly Uri _advantageServiceUri;
        private readonly string _advantageServiceAuthenticationKey;

        public ProxySettings(Uri advantageServiceUri, string advantageServiceAuthenticationKey)
        {
            if (advantageServiceUri == null) throw new ArgumentNullException("advantageServiceUri");
            if (string.IsNullOrEmpty(advantageServiceAuthenticationKey)) throw new ArgumentException("advantageServiceAuthenticationKey cannot be null or empty");

            _advantageServiceUri = advantageServiceUri;
            _advantageServiceAuthenticationKey = advantageServiceAuthenticationKey;
        }

        public Uri AdvantageServiceUri
        {
            get { return _advantageServiceUri; }
        }

        public string AdvantageServiceAuthenticationKey
        {
            get { return _advantageServiceAuthenticationKey; }
        }
    }
}