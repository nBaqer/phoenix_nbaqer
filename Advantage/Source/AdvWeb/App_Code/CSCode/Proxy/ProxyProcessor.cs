﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProxyProcessor.cs" company="FAME">
//   2013
// </copyright>
// <summary>
//   Defines the ProxyProcessor type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace AdvWeb.CSCode.Proxy
{
    using System;
    using System.Net.Http;

    using AdvWeb.CSCode.Extensions;

    /// <summary>
    /// The proxy processor.
    /// </summary>
    public class ProxyProcessor
    {
        /// <summary>
        /// The authentication key header name.
        /// </summary>
        private const string AuthenticationKeyHeaderName = "AuthKey";

        /// <summary>
        /// The _proxy settings.
        /// </summary>
        private readonly ProxySettings proxySettings;

        /// <summary>
        ///  The request.
        /// </summary>
        private readonly HttpRequestMessage request;

        /// <summary>
        /// The route.
        /// </summary>
        private readonly string route;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProxyProcessor"/> class.
        /// </summary>
        /// <param name="proxySettings">
        /// The proxy settings.
        /// </param>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <param name="route">
        /// The route.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// The proxy setting coming null
        /// </exception>
        /// <exception cref="ArgumentException">
        /// The route or the request coming null
        /// </exception>
        public ProxyProcessor(ProxySettings proxySettings, HttpRequestMessage request, string route)
        {
            if (proxySettings == null)
            {
                throw new ArgumentNullException("proxySettings");
            }

            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            if (string.IsNullOrEmpty(route))
            {
                throw new ArgumentException("route cannot be null or empty");
            }

            this.proxySettings = proxySettings;
            this.request = request;
            this.route = route;
        }

        /// <summary>
        /// The proxy request message and get response.
        /// </summary>
        /// <returns>
        /// The <see cref="HttpResponseMessage"/>.
        /// </returns>
        internal virtual HttpResponseMessage ProxyRequestMessageAndGetReponse()
        {
            var requeuestMessage = this.BuildHttpRequestMessageForRoute();

            using (var client = new HttpClient())
            {
                this.AddAuthenticationHeader(client);
                return client.SendAsync(requeuestMessage).Result;
            }
        }

        /// <summary>
        /// The build http request message for route.
        /// </summary>
        /// <returns>
        /// The <see cref="HttpRequestMessage"/>.
        /// </returns>
        private HttpRequestMessage BuildHttpRequestMessageForRoute()
        {
            var newRequest = new HttpRequestMessage
            {
                RequestUri = this.BuildRequestUri(),
                Method = this.request.Method,
                Content = this.GetContentForRequest()
            };

            this.request.CopyHeadersTo(newRequest);

            return newRequest;
        }

        /// <summary>
        /// The build request URI.
        /// </summary>
        /// <returns>
        /// The <see cref="Uri"/>.
        /// </returns>
        private Uri BuildRequestUri()
        {
            var fullRoute = this.proxySettings.AdvantageServiceUri + this.route + this.request.RequestUri.Query;

            return new Uri(fullRoute);
        }

        /// <summary>
        /// The get content for request.
        /// </summary>
        /// <returns>
        /// The <see cref="HttpContent"/>.
        /// </returns>
        private HttpContent GetContentForRequest()
        {
            var contentLength = this.request.Content.Headers.ContentLength;
            var content = contentLength > 0 ? this.request.Content : null;

            return content;
        }

        /// <summary>
        /// The add authentication header.
        /// </summary>
        /// <param name="client">
        /// The client.
        /// </param>
        private void AddAuthenticationHeader(HttpClient client)
        {
            client.DefaultRequestHeaders.Add(ProxyProcessor.AuthenticationKeyHeaderName, this.proxySettings.AdvantageServiceAuthenticationKey);
        }
    }
}