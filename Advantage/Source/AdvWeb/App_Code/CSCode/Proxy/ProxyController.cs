﻿using System;
using System.Collections;
using System.Net.Http;
using System.Web.Http;
using FAME.Advantage.Common;

namespace AdvWeb.CSCode.Proxy
{
    public class ProxyController : ApiController
    {
        [HttpGet, HttpPost, HttpPut, HttpDelete]
        public HttpResponseMessage Proxy(string route)
        {
            var moduleSettings = GetModuleSettings();

            var proxySettings = GetProxySettingsFromModuleSettings(moduleSettings);

            var proxyProcessor = new ProxyProcessor(proxySettings, Request, route);

            var response = proxyProcessor.ProxyRequestMessageAndGetReponse();
            return response;
        }

        private static IDictionary GetModuleSettings()
        {
            var advantageSettings = new AdvAppSettings();

            return new Hashtable
            {
                { "AdvantageServiceUri", advantageSettings.AppSettings("AdvantageServiceUri") },
                { "AdvantageServiceAuthenticationKey", advantageSettings.AppSettings("AdvantageServiceAuthenticationKey") }
            };
        }

        private static ProxySettings GetProxySettingsFromModuleSettings(IDictionary settings)
        {
            var advantageServiceUrl = (string)settings["AdvantageServiceUri"];
            var advantageServiceAuthenticationKey = (string)settings["AdvantageServiceAuthenticationKey"];

            return new ProxySettings(new Uri(advantageServiceUrl), advantageServiceAuthenticationKey);
        }
    }
}
