﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Extended Key,Value pair structure for tracking AdHoc paging.
/// Created: 8/15/2013  Felipe Ramos
/// </summary>
/// <typeparam name="T">Template Key</typeparam>
/// <typeparam name="K">Template Value</typeparam>
[Serializable()]
public class ReportPager<T, K> : Dictionary<T, K>
{

    #region Fields

    public const string SESSION_KEY = "ReportPager_SessionKey";                         // Track the Unique SessionId for the report.

    #endregion

    #region Properties
    public string SessionKey
    {
        get { return SessionService.Get<string>(SESSION_KEY); }
        set { SessionService.Set<string>(SESSION_KEY, value); }
    }

    #endregion

    #region Constructors

    private ReportPager()
        : base()
    {
    }

    public ReportPager(T key, K value) : base()
    {
    }

    ~ReportPager() { }

    #endregion

    #region New Functionality

    public static ReportPager<T, K> GetInstance(string sessionKey)
    {
        if (SessionService.Contains(sessionKey))
            return SessionService.Get<ReportPager<T, K>>(sessionKey);

        var report = new ReportPager<T, K>
        {
            SessionKey = sessionKey
        };

        return report;
    }


    public void Persist()
    {
        if (string.IsNullOrEmpty(SessionKey))
            throw new Exception("Unable to persist ReportPager in Session.");

        SessionService.Add<ReportPager<T, K>>(SessionKey, this);
    }

    public new void Add(T key, K value)
    {
        if (ContainsKey(key))
            this[key] = value;
        else
            base.Add(key, value);

        Persist();
    }

    #endregion

}