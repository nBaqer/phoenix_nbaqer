﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

[Serializable()]
public class ResourceDefinition
{
    public int ResDefId { get; set; }
    public int TblFldsId { get; set; }
    public int FldId { get; set; }
    public string FldName {get;set;}
    public bool Required { get; set; }
    public Nullable<int> DDLId { get; set; }
    public int FldLen { get; set; }
    public string FldType { get; set; }
    public string Caption { get; set; }
    public string ControlName { get; set; }

    public ValidationDataType ValidatorType
    {
        get
        {
            switch (FldType.ToUpper())
            {
                case "INT":
                case "TINYINT":
                case "SMALLINT":
                    return ValidationDataType.Integer;
                case "FLOAT":
                    return ValidationDataType.Double;
                case "DATETIME":
                case "SMALLDATETIME":
                    return ValidationDataType.Date;
                case "CHAR":
                case "VARCHAR":
                    return ValidationDataType.String;
                case "MONEY":
                case "DECIMAL":
                    return ValidationDataType.Currency;
            }

            return ValidationDataType.String;
        }
    }

    public static List<ResourceDefinition> Load(DataTable table)
    {
        var retValue = new List<ResourceDefinition>();

        if (table != null && table.Rows.Count > 0)
            foreach (DataRow row in table.Rows)
                retValue.Add(new ResourceDefinition
                {
                    ResDefId = Utility.GetValue<int>(row["ResDefId"]),
                    TblFldsId = Utility.GetValue<int>(row["TblFldsId"]),
                    FldId = Utility.GetValue<int>(row["FldId"]),
                    FldName = row["FldName"].ToString(),
                    Required = Utility.GetValue<bool>(row["Required"]),
                    DDLId = row.IsNull("DDLId") ? (Nullable<int>)null : Utility.GetValue<int>(row["DDLId"]),
                    FldLen = Utility.GetValue<int>(row["FldLen"]),
                    FldType = row["FldType"].ToString(),
                    Caption = row["Caption"].ToString(),
                    ControlName = row["ControlName"].ToString()
                });

        return retValue;
    }
}