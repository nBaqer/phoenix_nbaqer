﻿using System;
using System.ComponentModel;

/// <summary>
/// Utility Wrapper
/// </summary>
public class Utility
{
    public static T GetValue<T>(string value) where T : struct
    {

        if (typeof(T) == typeof(bool))
        {
            switch (value.ToUpper())
            {
                case "TRUE":
                case "YES":
                case "1":
                    return (T)(object)true;
                default:
                    return default(T);
            }
        }

        var convertFrom = TypeDescriptor.GetConverter(typeof(T)).ConvertFrom(value);
        if (convertFrom != null)
        {
            var con = (T)convertFrom;
            return con;
        }


        throw new NotImplementedException(string.Format("{0} is not implemented by Utility.GetValue<T>(param).", typeof(T)));
    }

    public static T GetValue<T>(object value) where T : struct
    {
        return GetValue<T>(value.ToString());
    }
}