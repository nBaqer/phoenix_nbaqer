﻿using FAME.Advantage.Common;
using FAME.AdvantageV1.BusinessFacade;
using System;
using System.Web;

/// <summary>
/// Summary description for CampusObjects
/// </summary>
public class CampusObjects
{
    public CampusObjects()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static void CampusSelectorchanged(System.Web.SessionState.HttpSessionState session, Guid currentCampusId)
    {
        //session["PreviousCampus"] = AdvantageSession.UserState.CampusId.ToString();
        //This is needed to keep track of which campus the user is switching from

        //Change the User State Object to get the new campus
        AdvantageSession.UserState.CampusId = currentCampusId;
        //session["CurrentCampus"] = AdvantageSession.UserState.CampusId;
        UserSecurityFacade fac = new UserSecurityFacade();
        fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString(), AdvantageSession.UserState.CampusId.ToString(), false);

        //Reset this session, so that menus can be built based on campus level config settings
        session["MenuLoaded"] = false;

        //Create a new number for sesssion variable

        AdvAppSettings myAdvAppSettings = default(AdvAppSettings);
        if (((AdvAppSettings)HttpContext.Current.Session["AdvAppSettings"] != null))
        {
            myAdvAppSettings = (AdvAppSettings)HttpContext.Current.Session["AdvAppSettings"];
        }
        else
        {
            myAdvAppSettings = new AdvAppSettings();
        }

        myAdvAppSettings.RetrieveSettings(AdvantageSession.UserState.CampusId.ToString());
        HttpContext.Current.Session["AdvAppSettings"] = myAdvAppSettings;

        //Random random = new Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000);
        //return random;
    }

    /// <summary>
    /// Get the Campus Id guid from query string.
    /// </summary>
    /// <param name="request">The request string</param>
    /// <returns>The Campus Id Guid</returns>
    public static Guid GetQueryParameterValueGuidCampusId(System.Web.HttpRequest request)
    {
        var text = GetQueryParameterValueStrCampusId(request);
        Guid campusIdGuid;
        if (Guid.TryParse(text, out campusIdGuid))
        {
            return campusIdGuid;
        }

        throw new HttpRequestValidationException("Bad Query String Campus Id is not a Guid");
    }


    /// <summary>
    /// Return the Campus Id guid as String
    /// </summary>
    /// <param name="request">The request string</param>
    /// <returns>The Campus Id Guid as string</returns>
    public static string GetQueryParameterValueStrCampusId(System.Web.HttpRequest request)
    {
        var value = request.QueryString["cmpid"];
        if (value == null)
        {
           
            return Guid.Empty.ToString();
           
        }

        return value.ToString();
    }

    /// <summary>
    /// Show a message in screen. The message is controlled by the Session content in
    /// "ShowNotificationSwitchingCampus".
    /// This value is a cvs of two values separated by colon.
    /// if the first is 0, the windows is not show. if one yes.
    /// In the second parameter is the message.
    /// </summary>
    /// <param name="messageid">0 if the box is not show, and 1 if showed.</param>
    /// <param name="strName">The message to be sent.</param>
    public static void ShowNotificationWhileSwitchingCampus(int messageid = 0, string strName = "")
    {
        if (string.IsNullOrEmpty(strName))
        {
            messageid = 0;
        }

        switch (messageid)
        {
            case 0:
                AdvantageSession.ShowNotificationSwitchingCampus = "0, ";
                break;
            case 1:
                AdvantageSession.ShowNotificationSwitchingCampus = "1,You are currently viewing the details of student " + strName + " from ";
                break;
            case 2:
                AdvantageSession.ShowNotificationSwitchingCampus = "1,You are currently viewing the details of employer " + strName + " from ";
                break;
            case 3:
                AdvantageSession.ShowNotificationSwitchingCampus = "1,You are currently viewing the details of employee " + strName + " from ";
                break;
            case 4:
                AdvantageSession.ShowNotificationSwitchingCampus = "1,You are currently viewing the details of lead " + strName + " from ";
                break;
            case 6:
                AdvantageSession.ShowNotificationSwitchingCampus = "1,You have insufficient permission to work with the page you requested earlier. Tou were redirected to this page after switching to ";
                break;
            default:
                AdvantageSession.ShowNotificationSwitchingCampus = "1,The contents and filters (if available) of the page will be reloaded as you switched to ";
                break;
        }
    }
}
