﻿
Imports System.Web.UI

Public Class ServerSideViewStateAdapter
    Inherits System.Web.UI.Adapters.PageAdapter
    Public Overrides Function GetStatePersister() As PageStatePersister
        Return New SessionPageStatePersister(Me.Page)
    End Function
End Class
