﻿
Imports System.Web
Imports Advantage.Business.Logic.Layer
Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.Advantage.Common
Namespace FAME.Advantage.HTTPModules
    Public Class MRUModule
        Implements IHttpModule

        Dim newURL As String = ""
        Dim boolReferrer As Boolean = False
        Public Sub dispose() Implements IHttpModule.Dispose
        End Sub
        Public Sub Init(context As System.Web.HttpApplication) Implements System.Web.IHttpModule.Init
            AddHandler context.PreRequestHandlerExecute, AddressOf PreRequestHandlerExecute
        End Sub
        Public Sub PreRequestHandlerExecute(ByVal s As Object, ByVal e As EventArgs)
            'Dim app As HttpApplication = CType(s, HttpApplication)
            'If Not app.Context.Request.RawUrl.Contains("RESID") Then
            '    Exit Sub
            'End If

            'Select Case app.Context.Request.QueryString("RESID").ToString
            '    Case "90", "92", "95", "97", "112", _
            '                       "114", "116", "155", "159", "169", _
            '                           "175", "177", "200", "203", "213", _
            '                           "230", "286", "301", "303", "327", _
            '                           "372", "374", "380", "479", "530", _
            '                           "531", "532", "534", "535", "538", _
            '                           "539", "541", "542", "543", "614", _
            '                           "625", "628", "652", "113", _
            '                           "79", "82", "86", "111", _
            '                           "52", "55", "69", "281", _
            '                           "145", "146", "147", "148", "170", "225", "313", "456", "484", "334", "793"


            '        Dim EntityType As Integer = IsAnEntityPage(app)
            '        Dim newVID As String = ""
            '        Dim EntityTypeValue As Integer = 0
            '        newVID = app.Context.Request.QueryString("VID") 'VID is the object pointer for all entities
            '        If String.IsNullOrEmpty(newVID) Then
            '            Select Case EntityType
            '                Case 1
            '                    Try

            '                        newVID = getLastStudentEntityFromStateObject().ChildId.ToString
            '                        EntityTypeValue = 1
            '                    Catch ex As Exception
             '                    	Dim exTracker = new AdvApplicationInsightsInitializer()
            '                    	exTracker.TrackExceptionWrapper(ex)

            '                        newVID = "1"
            '                    End Try
            '                Case 2
            '                    Try
            '                        newVID = getLastEmployerEntityFromStateObject().ChildId.ToString
            '                        EntityTypeValue = 2
            '                    Catch ex As Exception
             '                    	Dim exTracker = new AdvApplicationInsightsInitializer()
            '                    	exTracker.TrackExceptionWrapper(ex)

            '                        newVID = "2"
            '                    End Try
            '                Case 3
            '                    Try
            '                        newVID = getLastEmployeeEntityFromStateObject().ChildId.ToString()
            '                        EntityTypeValue = 3
            '                    Catch ex As Exception
             '                    	Dim exTracker = new AdvApplicationInsightsInitializer()
            '                    	exTracker.TrackExceptionWrapper(ex)

            '                        newVID = "3"
            '                    End Try
            '                Case 4
            '                    Try
            '                        newVID = getLastLeadEntityFromStateObject().ChildId.ToString
            '                        EntityTypeValue = 4
            '                    Catch ex As Exception
             '                    	Dim exTracker = new AdvApplicationInsightsInitializer()
            '                    	exTracker.TrackExceptionWrapper(ex)

            '                        newVID = "4"
            '                    End Try
            '            End Select
            '            Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
            '            Dim newQueryString As String = app.Context.Request.QueryString.ToString + "&VSI=" + R.Next().ToString
            '            newURL = ReloadURL(newVID, newQueryString, app.Context.Request.Url.AbsolutePath)
            '            If newURL.Contains("Type") = False And EntityTypeValue > 0 Then
            '                newURL = newURL + "&Type=" + EntityTypeValue.ToString
            '            End If
            '            'app.Context.Response.Redirect(newURL)
            '            'System.Web.HttpContext.Current.Response.Redirect(newURL)
            '        End If
            '    Case Else
            '        Dim newVID As String = ""
            '        newVID = app.Context.Request.QueryString("VID") 'VID is the object pointer for all entities
            '        If String.IsNullOrEmpty(newVID) Then
            '            newVID = 1
            '            Dim R As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
            '            Dim newQueryString As String = app.Context.Request.QueryString.ToString + "&VSI=" + R.Next().ToString
            '            newURL = ReloadURL(newVID, newQueryString, app.Context.Request.Url.AbsolutePath)
            '            app.Context.Response.Redirect(newURL)
            '        End If

            'End Select
        End Sub

        ''' <summary>
        ''' We only want to change entity page urls that need a VID
        ''' This is a faked function to return values for student and lead on one page only
        ''' I wasn't sure how you are going to be able to tell if a page is an entity page (simple SQL query?)
        ''' If the function returns zero then the above Select Case Statement will not modify the url
        ''' </summary>
        ''' <param name="app"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function IsAnEntityPage(ByVal app As HttpApplication) As Integer
            ''MOdified for DE8076
            Dim EntityType As Integer = 0
            Select Case app.Context.Request.QueryString("ResId").ToString
                Case "90", "92", "95", "97", "112", _
                                   "114", "116", "155", "159", "169", _
                                       "175", "177", "200", "203", "213", _
                                       "230", "286", "301", "303", "327", _
                                       "372", "374", "380", "479", "530", _
                                       "531", "532", "534", "535", "538", _
                                       "539", "541", "542", "543", "614", _
                                       "625", "628", "652", "113", "334" ' Student Page"308",
                    EntityType = 1
                Case "79", "82", "86", "111" 'Employer Tabs , "197" 
                    EntityType = 2
                Case "52", "55", "69", "281" 'Employee Tabs , "309"
                    EntityType = 3
                Case "145", "146", "147", "148", "170", _
                       "225", "313", "456", "484", "793" 'Lead Tabs, "153"
                    EntityType = 4
            End Select

            Return EntityType
        End Function
        Public Function ReloadURL(ByVal strVID As String, ByVal OriginalURL As String, ByVal URLPath As String) As String
            Dim nameValues = HttpUtility.ParseQueryString(OriginalURL)
            nameValues.[Set]("VID", strVID)
            Dim url As String = URLPath
            Dim updatedQueryString As String = "?" + nameValues.ToString()
            Dim newURL As String = url + updatedQueryString
            Return newURL
        End Function
        Private Function getLastStudentEntityFromStateObject() As BO.StudentMRU
            Dim objStateInfo As New AdvantageStateInfo
            Dim strVID As String
            Dim strStudentId As String = ""
            Dim objEntityState As New BO.StudentMRU
            Dim mruProvider As MRURoutines
            Dim state As AdvantageSessionState

            Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
            mruProvider = New MRURoutines(System.Web.HttpContext.Current, MyAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)


            strStudentId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                              1, _
                                                              AdvantageSession.UserState.CampusId.ToString)
            If String.IsNullOrEmpty(strStudentId) Then
                Return Nothing
            ElseIf strStudentId = Guid.Empty.ToString Then
                Return Nothing
            End If

            objStateInfo = mruProvider.BuildStudentStatusBar(strStudentId, AdvantageSession.UserState.CampusId.ToString)
            With objStateInfo
                If .LeadId Is Nothing OrElse .LeadId = "" Then
                    .LeadId = Guid.Empty.ToString
                End If
            End With

            'Create a new guid to be associated with the employer pages
            strVID = Guid.NewGuid.ToString

            HttpContext.Current.Items.Add("StudentPointer", strVID)

            'Add an entry to AdvantageSessionState for this guid and object
            'load Advantage state
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current) 'New AdvantageSessionState  
            state(strVID) = objStateInfo
            CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

            'UpdateMRUTable
            'Reason: We need to keep track of the last student record the user worked with
            'Scenario1 : User can log in and click on a student page without using MRU 
            'and we need to display the data of the last student the user worked with
            'If the user is a first time user, we will load the student who was last added to advantage
            'mruProvider.UpdateMRUList(strStudentId, AdvantageSession.UserState.UserId.ToString, _
            '                          AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

            mruProvider.InsertMRU(1, strStudentId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

            With objEntityState
                .StudentId = New Guid(objStateInfo.StudentId.ToString)
                .LeadId = New Guid(objStateInfo.LeadId.ToString)
                .ChildId = New Guid(strVID)
                .Name = objStateInfo.NameValue
            End With

            Return objEntityState
        End Function
        Private Function getLastLeadEntityFromStateObject() As BO.StudentMRU
            Dim objStateInfo As New AdvantageStateInfo
            Dim objGetStudentStatusBar As New AdvantageStateInfo
            Dim strVID As String
            Dim facInputMasks As New InputMasksFacade
            Dim strLeadId As String = ""
            Dim objEntityState As New BO.StudentMRU
            Dim mruProvider As MRURoutines
            Dim state As AdvantageSessionState

            Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
            mruProvider = New MRURoutines(System.Web.HttpContext.Current, MyAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)

            strLeadId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                                   4, _
                                                                   AdvantageSession.UserState.CampusId.ToString)
            If String.IsNullOrEmpty(strLeadId) Then
                Return Nothing
            ElseIf strLeadId = Guid.Empty.ToString Then
                Return Nothing
            End If
            objStateInfo = mruProvider.BuildLeadStatusBar(strLeadId)

            'Create a new guid to be associated with the employer pages
            strVID = Guid.NewGuid.ToString

            'Add an entry to AdvantageSessionState for this guid and object
            'load Advantage state
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            state(strVID) = objStateInfo
            'save current State
            CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

            'UpdateMRUTable
            'Reason: We need to keep track of the last student record the user worked with
            'Scenario1 : User can log in and click on a student page without using MRU 
            'and we need to display the data of the last student the user worked with
            'If the user is a first time user, we will load the student who was last added to advantage
            'mruProvider.UpdateMRUList(strLeadId, AdvantageSession.UserState.UserId.ToString, _
            '                          AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

            mruProvider.InsertMRU(4, strLeadId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
            If IsNothing(objStateInfo.StudentId) Then
                objStateInfo.StudentId = Guid.NewGuid.ToString()
            ElseIf objStateInfo.StudentId.ToString = Guid.Empty.ToString Then
                objStateInfo.StudentId = Guid.NewGuid.ToString()
            End If
            With objEntityState
                .StudentId = New Guid(objStateInfo.StudentId.ToString())
                .LeadId = New Guid(objStateInfo.LeadId.ToString())
                .ChildId = New Guid(strVID)
                .Name = objStateInfo.NameValue
            End With
            Return objEntityState
        End Function
        Private Function getLastEmployerEntityFromStateObject() As BO.EmployerMRU
            Dim objStateInfo As New AdvantageStateInfo
            Dim objGetStudentStatusBar As New AdvantageStateInfo
            Dim strVID As String
            Dim facInputMasks As New InputMasksFacade
            Dim strEmployerId As String = ""
            Dim objEntityState As New BO.EmployerMRU
            Dim mruProvider As MRURoutines
            Dim state As AdvantageSessionState

            Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
            mruProvider = New MRURoutines(System.Web.HttpContext.Current, MyAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)

            strEmployerId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                                   2, _
                                                                   AdvantageSession.UserState.CampusId.ToString)


            If Not strEmployerId.Trim = "" Then
                objGetStudentStatusBar = mruProvider.BuildEmployerStatusBar(strEmployerId)
                With objStateInfo
                    .EmployerId = objGetStudentStatusBar.EmployerId
                    .NameValue = objGetStudentStatusBar.NameValue
                    .Address1 = objGetStudentStatusBar.Address1
                    .Address2 = objGetStudentStatusBar.Address2
                    .City = objGetStudentStatusBar.City
                    .State = objGetStudentStatusBar.State
                    .Zip = objGetStudentStatusBar.Zip
                    .Phone = objGetStudentStatusBar.Phone
                End With

                'Create a new guid to be associated with the employer pages
                strVID = Guid.NewGuid.ToString

                'Add an entry to AdvantageSessionState for this guid and object
                'load Advantage state
                state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
                state(strVID) = objStateInfo
                'save current State
                CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

                'UpdateMRUTable
                'Reason: We need to keep track of the last student record the user worked with
                'Scenario1 : User can log in and click on a student page without using MRU 
                'and we need to display the data of the last student the user worked with
                'If the user is a first time user, we will load the student who was last added to advantage
                'mruProvider.UpdateMRUList(strEmployerId, AdvantageSession.UserState.UserId.ToString, _
                '                          AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

                mruProvider.InsertMRU(2, strEmployerId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

                With objEntityState
                    .EmployerId = New Guid(objStateInfo.EmployerId.ToString)
                    .ChildId = New Guid(strVID)
                    .Name = objStateInfo.NameValue
                End With
                Return objEntityState
            Else
                Return Nothing
            End If
        End Function
        Private Function getLastEmployeeEntityFromStateObject() As BO.EmployeeMRU
            Dim objStateInfo As New AdvantageStateInfo
            Dim objGetStudentStatusBar As New AdvantageStateInfo
            Dim strVID As String
            Dim facInputMasks As New InputMasksFacade
            Dim strEmployeeId As String = ""
            Dim objEntityState As New BO.EmployeeMRU
            Dim mruProvider As MRURoutines
            Dim state As AdvantageSessionState

            Dim MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
            mruProvider = New MRURoutines(System.Web.HttpContext.Current, MyAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)

            strEmployeeId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                                   3, _
                                                                   AdvantageSession.UserState.CampusId.ToString)


            If Not strEmployeeId.Trim = "" Then
                objGetStudentStatusBar = mruProvider.BuildEmployeeStatusBar(strEmployeeId)
                With objStateInfo
                    .EmployeeId = objGetStudentStatusBar.EmployeeId
                    .NameValue = objGetStudentStatusBar.NameValue
                    .Address1 = objGetStudentStatusBar.Address1
                    .Address2 = objGetStudentStatusBar.Address2
                    .City = objGetStudentStatusBar.City
                    .State = objGetStudentStatusBar.State
                    .Zip = objGetStudentStatusBar.Zip
                End With

                'Create a new guid to be associated with the Employee pages
                strVID = Guid.NewGuid.ToString


                'Add an entry to AdvantageSessionState for this guid and object
                'load Advantage state
                state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
                state(strVID) = objStateInfo
                'save current State
                CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

                'UpdateMRUTable
                'Reason: We need to keep track of the last student record the user worked with
                'Scenario1 : User can log in and click on a student page without using MRU 
                'and we need to display the data of the last student the user worked with
                'If the user is a first time user, we will load the student who was last added to advantage
                'mruProvider.UpdateMRUList(strEmployeeId, AdvantageSession.UserState.UserId.ToString, _
                '                          AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

                mruProvider.InsertMRU(3, strEmployeeId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

                With objEntityState
                    .EmployeeId = New Guid(objStateInfo.EmployeeId.ToString)
                    .ChildId = New Guid(strVID)
                End With
                Return objEntityState
            Else
                Return Nothing
            End If
        End Function

        Private Function GetAdvAppSettings() As AdvAppSettings
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Return MyAdvAppSettings
        End Function
    End Class
End Namespace