﻿

Public MustInherit Class BaseUserControl
    Inherits System.Web.UI.UserControl
    ''' <summary>
    ''' Find Control starting at Page level
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overridable Function FindControlRecursive(id As String) As Control
        Return FindControlRecursive(id, Me)
    End Function
    ''' <summary>
    ''' Find Control within a parent control
    ''' </summary>
    ''' <param name="id"></param>
    ''' <param name="parent"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overridable Function FindControlRecursive(id As String, parent As Control) As Control
        ' If parent is the control we're looking for, return it
        If String.Compare(parent.ID, id, True) = 0 Then
            Return parent
        End If
        ' Search through children
        For Each ctrl As Control In parent.Controls
            Dim match As Control = FindControlRecursive(id, ctrl)
            If Not match Is Nothing Then
                Return match
            End If
        Next
        'If we reach here then no control with id was found
        Return Nothing
    End Function
End Class
