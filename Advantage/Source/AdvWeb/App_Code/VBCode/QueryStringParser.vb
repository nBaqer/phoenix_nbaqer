﻿
Imports System.Web
Public Class QueryStringParser
    Implements IHttpModule

#Region "Interface"
    Public Sub Init(context As HttpApplication) Implements IHttpModule.Init
        ' The PreRequestHandlerExecute is the first to fire after session state is activated (which we need below)
        AddHandler context.PreRequestHandlerExecute, AddressOf Execute
    End Sub
    Public Sub Dispose() Implements IHttpModule.Dispose

    End Sub

#End Region

    ''' <summary>
    ''' Handles the 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub Execute(sender As Object, e As EventArgs)
        Dim context As HttpContext = HttpContext.Current

        ' Load the current URL (
        Dim url = HttpContext.Current.Request.Url.AbsolutePath
        If String.IsNullOrEmpty(url) Then
            Return
        End If
        If Not url.Contains(".aspx") Then
            Return
        End If

        ' Force a style into it
        Dim existingVID = context.Request.Params(context.Request.QueryString("VID")) '(QueryStringKey.VID)
        'If Not String.IsNullOrEmpty(existingVID) Then
        '    ' Recall from previous setting
        '    url = url.Replace(context.Request.QueryString("VID"), existingVID)
        '    context.Response.Redirect(url)
        'End If

        Dim nameValues = HttpUtility.ParseQueryString(context.Request.QueryString.ToString())
        nameValues.[Set]("VID", existingVID)
        Dim updatedQueryString As String = "?" + nameValues.ToString()
        context.Response.Redirect(url + updatedQueryString, False)
    End Sub
End Class
