﻿
Imports Telerik.Web.UI
Imports System.Data
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Collections.Generic

Imports FAME.AdvantageV1.BusinessFacade.TM

Namespace FilteringTemplateColumns


    Public Class MyCustomFilteringColumn
        Inherits GridTemplateColumn

        'Public Function GetDataTable(ByVal queryString As String) As DataTable
        '    Dim ConnString As String = ConfigurationManager.ConnectionStrings("NorthwindConnectionString").ConnectionString
        '    Dim MySqlConnection As New SqlConnection(ConnString)
        '    Dim MySqlDataAdapter As New SqlDataAdapter()
        '    MySqlDataAdapter.SelectCommand = New SqlCommand(queryString, MySqlConnection)

        '    Dim myDataTable As New DataTable()
        '    MySqlConnection.Open()
        '    Try
        '        MySqlDataAdapter.Fill(myDataTable)
        '    Finally
        '        MySqlConnection.Close()
        '    End Try

        '    Return myDataTable
        'End Function

        Protected Overrides Sub SetupFilterControls(ByVal cell As TableCell)
            Dim rcBox As New RadComboBox()
            rcBox.ID = "DropDownList1"
            rcBox.AutoPostBack = True
            rcBox.DataTextField = Me.DataField
            rcBox.DataValueField = Me.DataField
            rcBox.CheckBoxes = True
            AddHandler rcBox.SelectedIndexChanged, AddressOf rcBox_SelectedIndexChanged
            '  Dim table As DataTable = GetDataTable(String.Format("SELECT DISTINCT {0} FROM {1}", Me.DataField, "Customers"))

            Dim UserID As String
            '   UserID = "4fcf31eb-7b2a-42d6-918c-9dbccc3df3ab" '' TMCommon.GetCurrentUserId()
            UserID = TMCommon.GetCurrentUserId()
            Dim tm As New FAME.AdvantageV1.BusinessFacade.TaskManager
            Dim table As DataTable = tm.GetUsersCampusList_ForwhomtheUsercanAssignTasksTo(UserID)

            Dim row As DataRow = table.NewRow()
            row(Me.DataField) = ""
            table.Rows.InsertAt(row, 0)
            rcBox.DataSource = table
            cell.Controls.Add(rcBox)
        End Sub

        Protected Overrides Sub SetCurrentFilterValueToControl(ByVal cell As TableCell)
            If Not (Me.CurrentFilterValue = "") Then
                If Me.CurrentFilterValue.Contains(",") Then
                    Dim filtervalues() As String
                    filtervalues = Me.CurrentFilterValue.Split(",")
                    For Each item As String In filtervalues
                        DirectCast(cell.Controls(0), RadComboBox).Items.FindItemByText(item).Selected = True
                    Next
                    '   DirectCast(cell.Controls(0), RadComboBox).Items.FindItemByText(Me.CurrentFilterValue).Selected = True
                Else
                    DirectCast(cell.Controls(0), RadComboBox).Items.FindItemByText(Me.CurrentFilterValue).Selected = True
                End If

            End If
        End Sub

        Protected Overrides Function GetCurrentFilterValueFromControl(ByVal cell As TableCell) As String
            'Dim currentValue As String = DirectCast(cell.Controls(0), RadComboBox).SelectedItem.Value
            Dim collection As IList(Of RadComboBoxItem) = DirectCast(cell.Controls(0), RadComboBox).CheckedItems
            Dim currentValue As String = ""
            '  Dim CurrentFilterFunction As String
            Dim totalFilterFunction As New StringBuilder
            Dim totalFilter As New StringBuilder

            For Each item As RadComboBoxItem In collection
                currentValue = item.Value
                '   CurrentFilterFunction = If((currentValue <> ""), GridKnownFunction.EqualTo, GridKnownFunction.NoFilter)
                '   totalFilterFunction.Append(CurrentFilterFunction)
                totalFilterFunction.Append(",")
                totalFilter.Append(currentValue)
                totalFilter.Append(",")

            Next

            If collection.Count > 0 Then
                '   totalFilterFunction.Remove(totalFilterFunction.Length - 3, 2)
                totalFilter.Remove(totalFilter.Length - 1, 1)
            ElseIf collection.Count = 0 Then
                currentValue = DirectCast(cell.Controls(0), RadComboBox).SelectedItem.Value
                '    CurrentFilterFunction = If((currentValue <> ""), GridKnownFunction.EqualTo, GridKnownFunction.NoFilter)
                '    totalFilterFunction.Append(CurrentFilterFunction)
                totalFilter.Append(currentValue)
            End If

            'Dim currentValue As String = DirectCast(cell.Controls(0), RadComboBox).SelectedItem.Value
            'Me.CurrentFilterFunction = If((currentValue <> ""), GridKnownFunction.EqualTo, GridKnownFunction.NoFilter)
            Me.CurrentFilterFunction = If((currentValue <> ""), GridKnownFunction.EqualTo, GridKnownFunction.NoFilter)
            Return totalFilter.ToString
        End Function

        Private Sub rcBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
            DirectCast(DirectCast(sender, RadComboBox).Parent.Parent, GridFilteringItem).FireCommandEvent("Filter", New Pair())
        End Sub
    End Class
End Namespace