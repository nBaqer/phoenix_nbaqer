﻿Imports Microsoft.VisualBasic
Imports Microsoft.Win32
Imports System.Management
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Net.NetworkInformation
Imports FAME.Advantage.Common
Imports System
Imports System.Web
Imports System.Text
Imports System.Text.RegularExpressions

Namespace BusinessLogicUtilities

    Public Class Utilities

        Public Shared Sub WriteLogFile(ByVal browser As String, ByVal browserversion As String, ByVal ex As Exception, ByVal dtHardDriveinfo As DataTable, ByVal dtProcessor As DataTable, ByVal SQLVersion As String, _
                   ByVal SQLCompatibility As String, ByVal OS As String, ByVal OSVersion As String, ByVal OSServicePack As String, ByVal IISVersion As String, ByVal NetworkSpeed As String, ByVal AdvantageDBBuild As String, _
                    ByVal AdvantageDBBuildDate As String, ByVal AvailableRam As String, ByVal TotalRam As String, ByVal MachineNameFromSQL As String)


            Dim AppName As String = "AdvantageErrorLog"
            Dim logTimeStampStart As DateTime = DateTime.Now
            Dim logPatternTimeStamp As String = "yyyyMMddhhmmss"
            Dim logTextTimeStampStart As String = logTimeStampStart.ToString(logPatternTimeStamp)
            Dim logFileName As String = AppName & "_" & logTextTimeStampStart & ".log"
            Dim rootPath As String = HttpContext.Current.Server.MapPath("~") + "\Logs\"
            Dim myStreamWriter As New StreamWriter(rootPath & logFileName)

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            myStreamWriter.WriteLine("------------------------------Advantage Error Log -----------------------------------")
            myStreamWriter.WriteLine(" ")
            myStreamWriter.WriteLine("School: " & CheckValue(MyAdvAppSettings.AppSettings("CorporateName")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Error Occurred at : " & DateTime.Now)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Advantage DB: " & AdvantageDBBuild & " " & AdvantageDBBuildDate)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("DB Name: " & GetDBName())
            myStreamWriter.WriteLine("")

            myStreamWriter.WriteLine("------------------------------Environment Information--------------------------------")
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("MachineName: " & Environment.MachineName.ToString)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("OS: " & OS & " SP " & OSServicePack)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Browser: " & browser & " " & browserversion)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("32/64: " & CheckValue(dtProcessor.Rows(0).Item("AddressWidth")))
            myStreamWriter.WriteLine("")

            For Each row As DataRow In dtProcessor.Rows
                myStreamWriter.WriteLine("Processor: " & row("Processor") & " " & row("Speed"))
            Next

            myStreamWriter.WriteLine("Free Ram: " & AvailableRam)
            myStreamWriter.WriteLine("TotalRam: " & TotalRam)
            myStreamWriter.WriteLine("")

            For Each row As DataRow In dtHardDriveinfo.Rows
                myStreamWriter.WriteLine("Drive: " & row("DeviceID"))
                myStreamWriter.WriteLine("Space: " & row("Size"))
                myStreamWriter.WriteLine("Space Remaining: " & row("FreeSpace"))
            Next

            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Network Connection: " & NetworkSpeed)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("SQL Server Edition: " & SQLVersion)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("SQL Server Compatibility: " & SQLCompatibility)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Report Execution Services: " & CheckValue(MyAdvAppSettings.AppSettings("ReportExecutionServices")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("IIS Version: " & IISVersion)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Dual Environment: " & IsDualEnvironment())
            myStreamWriter.WriteLine("")

            myStreamWriter.WriteLine("------------------------------School Settings--------------------------------------")
            myStreamWriter.WriteLine("Address to be Printed in Receipts: " & CheckValue(MyAdvAppSettings.AppSettings("AddressToBePrintedInReceipts")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Allow Adding Courses to ProgVersions After Students been Enrolled: " & CheckValue(MyAdvAppSettings.AppSettings("AllowAddingCoursesToProgramVersionsAfterStudentsHaveBeenEnrolled")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("# of Days to Allow Register Payments in future: " & CheckValue(MyAdvAppSettings.AppSettings("AllowRegisterPaymentsIntheFutureWithUpToThisNumberOfDaysInTheFuture")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Awards Cut Off Date: " & CheckValue(MyAdvAppSettings.AppSettings("AwardsCutOffDate")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Deferred Revenue Calc Method: " & MyAdvAppSettings.AppSettings("DeferredRevenueCalcMethod"))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Display Attendance Unit Prog Report by Class: " & CheckValue(MyAdvAppSettings.AppSettings("DisplayAttendanceUnitForProgressReportBYclass")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Document Path: " & CheckValue(MyAdvAppSettings.AppSettings("DocumentPath")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Display Attendance Unit Prog Report by Class: " & CheckValue(MyAdvAppSettings.AppSettings("DisplayAttendanceUnitForProgressReportBYclass")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Edit Other Leads: " & CheckValue(MyAdvAppSettings.AppSettings("EditOtherLeads")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("EDExpress Data FromRemote Computer: " & CheckValue(MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer")))
            myStreamWriter.WriteLine("")

            If UCase(CheckValue(MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer"))) = "YES" Then
                myStreamWriter.WriteLine("EDExpress Import File Path: " & CheckValue(MyAdvAppSettings.AppSettings("RemoteEDExpressIN")))
                myStreamWriter.WriteLine("")
                myStreamWriter.WriteLine("EDExpress Exception Path: " & CheckValue(MyAdvAppSettings.AppSettings("RemoteEDExpressException")))
                myStreamWriter.WriteLine("")
                myStreamWriter.WriteLine("EDExpress Archive Path: " & CheckValue(MyAdvAppSettings.AppSettings("RemoteEDExpressArchive")))
                myStreamWriter.WriteLine("")
            End If

            myStreamWriter.WriteLine("Grade Book Weighting Level: " & CheckValue(MyAdvAppSettings.AppSettings("GradeBookWeightingLevel")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Grade Rounding: " & CheckValue(MyAdvAppSettings.AppSettings("Graderounding")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Grades Format: " & CheckValue(MyAdvAppSettings.AppSettings("GradesFormat")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Include Credits Attempted for Repeat Courses: " & CheckValue(MyAdvAppSettings.AppSettings("IncludeCreditsAttemptedForRepeatedCourses")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Include Hours For Failing Grade: " & CheckValue(MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Logo For Transcripts: " & CheckValue(MyAdvAppSettings.AppSettings("LogoForTranscripts")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Max # Grade Book Weightings " & CheckValue(MyAdvAppSettings.AppSettings("MaxNoOfGradeBookWeightings")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Minimum GPA: " & CheckValue(MyAdvAppSettings.AppSettings("MinimumGPA")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Pass All Courses for Sap Check: " & CheckValue(MyAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Show Leads to Users: " & CheckValue(MyAdvAppSettings.AppSettings("ShowLeadstoUsers")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Show Externship Tabs: " & CheckValue(MyAdvAppSettings.AppSettings("ShowExternshipTabs")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Show Student Scheduling Group: " & CheckValue(MyAdvAppSettings.AppSettings("ShowStudentSchedulingGroup")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Show Ross Only Tab: " & CheckValue(MyAdvAppSettings.AppSettings("ShowRossOnlyTabsForStudent")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Standard Logo: " & CheckValue(MyAdvAppSettings.AppSettings("StandardLogo")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Student Identifier: " & CheckValue(MyAdvAppSettings.AppSettings("StudentIdentifier")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Student Image Path: " & CheckValue(MyAdvAppSettings.AppSettings("StudentImagePath")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Track SAP Attendance: " & CheckValue(MyAdvAppSettings.AppSettings("TrackSapAttendance")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Transcript Type: " & CheckValue(MyAdvAppSettings.AppSettings("TranscriptType")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Transcript Credits: " & CheckValue(MyAdvAppSettings.AppSettings("TranscriptCredits")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Use Automatic Scheduling for Traditional: " & CheckValue(MyAdvAppSettings.AppSettings("UseAutomaticSchedulingForTraditional")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Use Cohort Start Date for Post Term Fees: " & CheckValue(MyAdvAppSettings.AppSettings("UseCohortStartDateForPostTermFees")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("ESP Data from Remote Computer: " & CheckValue(MyAdvAppSettings.AppSettings("ESPDataFromRemoteComputer")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Fame ESP: " & CheckValue(MyAdvAppSettings.AppSettings("FAMEESP")))
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Exception Folder Location: " & CheckValue(MyAdvAppSettings.AppSettings("ExceptionFolderLocation")))
            myStreamWriter.WriteLine("")

            myStreamWriter.WriteLine("---------------------------------ERROR----------------------------------------------")
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("URL: " & HttpContext.Current.Request.Url.AbsoluteUri)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Source: " & ex.Source)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Message: " & ex.Message)
            myStreamWriter.WriteLine("")
            myStreamWriter.WriteLine("Stack Trace: " & ex.StackTrace)

            If Not (ex.InnerException Is Nothing) Then
                myStreamWriter.WriteLine("")
                myStreamWriter.WriteLine("Inner Exception: " & ex.InnerException.ToString)
            End If
            myStreamWriter.Flush()
            myStreamWriter.Close()
        End Sub

        Public Shared Sub SendEmailFromGmail(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String, ByVal strAttachments As String)
            'Start by creating a mail message object
            Dim MyMailMessage As New MailMessage()

            'From requires an instance of the MailAddress type
            MyMailMessage.From = New MailAddress(strFrom)

            'To is a collection of MailAddress types
            MyMailMessage.To.Add(strTo)
            MyMailMessage.IsBodyHtml = True
            MyMailMessage.Subject = strSubject
            MyMailMessage.Body = strBody
            MyMailMessage.Priority = MailPriority.High
            'MyMailMessage.CC.Add("sebadian@fameinc.com")

            'Create the SMTPClient object and specify the SMTP GMail server
            Dim SMTPServer As New SmtpClient("smtp.gmail.com")
            SMTPServer.Port = 587
            SMTPServer.Credentials = New System.Net.NetworkCredential("********", "********")
            SMTPServer.EnableSsl = True

            Try
                SMTPServer.Send(MyMailMessage)
            Catch ex As SmtpException
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        End Sub

        Public Shared Sub SendEmailFromFame(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String, ByVal strAttachments As String)
            Try

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim insMail As New MailMessage(New MailAddress(strFrom), New MailAddress(strTo))
                insMail.Priority = MailPriority.High
                insMail.IsBodyHtml = True
                With insMail
                    .Subject = strSubject
                    .Body = strBody
                    '.CC.Add(New MailAddress(strCC))
                    If Not strAttachments.Equals(String.Empty) Then
                        Dim strFile As String
                        Dim strAttach() As String = strAttachments.Split(";"c)
                        For Each strFile In strAttach
                            .Attachments.Add(New Attachment(strFile.Trim()))
                        Next
                    End If
                End With
                Dim smtp As New System.Net.Mail.SmtpClient
                smtp.Port = MyAdvAppSettings.AppSettings("FameSMTPPort")
                smtp.Host = MyAdvAppSettings.AppSettings("FameSMTPServer")
                'US3519: check for extra settings 
                If Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPUsername")) AndAlso Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPPassword")) Then
                    smtp.Credentials = New NetworkCredential(MyAdvAppSettings.AppSettings("FameSMTPUsername").ToString, MyAdvAppSettings.AppSettings("FameSMTPPassword").ToString)
                End If
                If Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPSSL")) Then
                    If UCase(MyAdvAppSettings.AppSettings("FameSMTPSSL")) = "YES" Then
                        smtp.EnableSsl = True
                    Else
                        smtp.EnableSsl = False
                    End If
                Else
                    smtp.EnableSsl = False
                End If
                smtp.Send(insMail)
            Catch e As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(e)

                Throw
            End Try
        End Sub



        Public Shared Sub SendEmailregardingErrorPage(ByVal strFrom As String, ByVal strTo As String, ByVal strSubject As String, ByVal strBody As String, ByVal strAttachments As String)
            Try

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim insMail As New MailMessage(New MailAddress(strFrom), New MailAddress(strTo))
                insMail.Priority = MailPriority.High
                insMail.IsBodyHtml = False
                With insMail
                    .Subject = strSubject
                    .Body = strBody
                    '.CC.Add(New MailAddress(strCC))
                    If Not strAttachments.Equals(String.Empty) Then
                        Dim strFile As String
                        Dim strAttach() As String = strAttachments.Split(";"c)
                        For Each strFile In strAttach
                            .Attachments.Add(New Attachment(strFile.Trim()))
                        Next
                    End If
                End With
                '   Dim smtp As New System.Net.Mail.SmtpClient("172.30.0.6", 25)
                Dim smtp As New System.Net.Mail.SmtpClient
                'smtp.EnableSsl = False
                'smtp.Port = 465 ''SingletonAppSettings.AppSettings("FameSMTPPort")
                'smtp.Host = "smtp.gmail.com" ''SingletonAppSettings.AppSettings("FameSMTPServer")
                smtp.Port = MyAdvAppSettings.AppSettings("FameSMTPPort")
                smtp.Host = MyAdvAppSettings.AppSettings("FameSMTPServer")
                'US3519: check for extra settings 
                If Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPUsername")) AndAlso Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPPassword")) Then
                    smtp.Credentials = New NetworkCredential(MyAdvAppSettings.AppSettings("FameSMTPUsername").ToString, MyAdvAppSettings.AppSettings("FameSMTPPassword").ToString)
                End If
                If Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPSSL")) Then
                    If UCase(MyAdvAppSettings.AppSettings("FameSMTPSSL")) = "YES" Then
                        smtp.EnableSsl = True
                    Else
                        smtp.EnableSsl = False
                    End If
                Else
                    smtp.EnableSsl = False
                End If
                'smtp.UseDefaultCredentials = False
                'smtp.Credentials = New System.Net.NetworkCredential("donotreply@fameinc.com", "Fame$6451")
                smtp.Send(insMail)
            Catch e As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(e)

                Throw
            End Try
        End Sub
        Public Shared Sub SendErrorInfoOverWebService(ByVal ex As Exception, ByVal Browser As String, ByVal BrowserVersion As String, ByVal dtHardDriveinfo As DataTable, ByVal dtProcessor As DataTable, ByVal SQLVersion As String, _
                                         ByVal SQLCompatibility As String, ByVal OS As String, ByVal OSVersion As String, ByVal OSServicePack As String, ByVal IISVersion As String, ByVal NetworkSpeed As String, _
                                         ByVal AdvantageDBBuild As String, ByVal AdvantageDBBuildDate As String, ByVal AvailableRam As String, ByVal TotalRam As String, ByVal MachineNameFromSQL As String)

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim WebServiceDataset As New DataSet

            Dim SystemInfo As New DataTable("SystemInfo")
            Dim ErrorDetails As New DataTable("ErrorDetails")
            Dim AdvantageConfigSettings As New DataTable("AdvantageConfigSettings")

            Dim AdvantageConfigSettingsDataRow As DataRow
            AdvantageConfigSettings.Columns.Add("KeyName")
            AdvantageConfigSettings.Columns.Add("Value")


            SystemInfo.Columns.Add("MachineName").DefaultValue = Environment.MachineName.ToString
            SystemInfo.Columns.Add("SQLMachineName").DefaultValue = MachineNameFromSQL
            SystemInfo.Columns.Add("NetworkSpeed").DefaultValue = NetworkSpeed
            SystemInfo.Columns.Add("Drive").DefaultValue = "C"

            Dim FreeSpaceinC As String = ""
            Dim TotalSpaceinC As String = ""
            For Each row As DataRow In dtHardDriveinfo.Rows
                If row("DeviceID").ToString.Contains("C") Then
                    FreeSpaceinC = row("FreeSpace")
                    TotalSpaceinC = row("Size")
                End If
            Next

            SystemInfo.Columns.Add("FreeSpaceinC").DefaultValue = FreeSpaceinC
            SystemInfo.Columns.Add("TotalSpaceinC").DefaultValue = TotalSpaceinC
            SystemInfo.Columns.Add("OS").DefaultValue = OS & " SP " & OSServicePack
            SystemInfo.Columns.Add("Browser").DefaultValue = Browser & " " & BrowserVersion
            SystemInfo.Columns.Add("FreeRam").DefaultValue = AvailableRam
            SystemInfo.Columns.Add("TotalRam").DefaultValue = TotalRam
            SystemInfo.Columns.Add("AddressWidth").DefaultValue = dtProcessor.Rows(0).Item("AddressWidth").ToString
            SystemInfo.Columns.Add("SQLVersion").DefaultValue = SQLVersion
            SystemInfo.Columns.Add("SQLCompatability").DefaultValue = SQLCompatibility
            SystemInfo.Columns.Add("IISVersion").DefaultValue = IISVersion
            SystemInfo.Columns.Add("DualEnvironment").DefaultValue = IsDualEnvironment()

            ErrorDetails.Columns.Add("ErrorDate").DefaultValue = Date.Now
            ErrorDetails.Columns.Add("ApplicationCode").DefaultValue = "ADV"
            ErrorDetails.Columns.Add("ApplicationVersion").DefaultValue = AdvantageDBBuild
            ErrorDetails.Columns.Add("SchoolCode").DefaultValue = CheckValue(MyAdvAppSettings.AppSettings("SchoolName"))
            ErrorDetails.Columns.Add("ErrorURL").DefaultValue = HttpContext.Current.Request.Url.AbsoluteUri
            ErrorDetails.Columns.Add("Source").DefaultValue = ex.Source
            ErrorDetails.Columns.Add("Message").DefaultValue = ex.Message
            ErrorDetails.Columns.Add("StackTrace").DefaultValue = ex.StackTrace

            If Not (ex.InnerException Is Nothing) Then
                ErrorDetails.Columns.Add("InnerException").DefaultValue = ex.InnerException
            End If

            Dim a As Integer = 0
            Dim i As Integer = 1
            Dim CampusId As String
            Dim ConfigSettingsCount As Integer
            CampusId = HttpContext.Current.Request.Params("cmpid").ToString()
            If Not String.IsNullOrWhiteSpace(CampusId) Then
                ConfigSettingsCount = MyAdvAppSettings.Count(CampusId)
            Else
                ConfigSettingsCount = 0
            End If




            While (i <= ConfigSettingsCount)
                AdvantageConfigSettingsDataRow = AdvantageConfigSettings.NewRow
                'AdvantageConfigSettingsDataRow("KeyName") = SingletonAppSettings.AppSettings.Keys(a).ToString
                'AdvantageConfigSettingsDataRow("Value") = SingletonAppSettings.AppSettings.Values(a).ToString


                a = a + 1
                i = i + 1
                AdvantageConfigSettings.Rows.Add(AdvantageConfigSettingsDataRow)
            End While

            WebServiceDataset.Tables.Add(SystemInfo)
            WebServiceDataset.Tables.Add(ErrorDetails)
            WebServiceDataset.Tables.Add(AdvantageConfigSettings)

            'Dim client = New ErrorWebService.Service1Client
            'client.GetData(WebServiceDataset)


        End Sub

        Public Shared Function FormatErrorToXML(ByVal ex As Exception, ByVal Browser As String, ByVal BrowserVersion As String, ByVal dtHardDriveinfo As DataTable, ByVal dtProcessor As DataTable, ByVal SQLVersion As String, _
                                         ByVal SQLCompatibility As String, ByVal OS As String, ByVal OSVersion As String, ByVal OSServicePack As String, ByVal IISVersion As String, ByVal NetworkSpeed As String, _
                                         ByVal AdvantageDBBuild As String, ByVal AdvantageDBBuildDate As String, ByVal AvailableRam As String, ByVal TotalRam As String, ByVal MachineNameFromSQL As String) As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                Dim builder As StringBuilder = New StringBuilder
                Dim body As String = ""

                builder.Append("<table bgcolor='#E4EBF7' width='100%' border='1' cellpadding='0' cellspacing='0'>")
                builder.Append("<tr>")
                builder.Append("<td width='100%' align='center' colspan='2' bgcolor='#63B1FF'>")
                builder.Append("<B>Advantage Error Log</B>")
                builder.Append("</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%'><B>School</B></td>")
                builder.Append("<td style='width: 80%'>&nbsp;")
                builder.Append(CheckValue(MyAdvAppSettings.AppSettings("SchoolName")))
                builder.Append("</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%'><B>Date</B></td>")
                builder.Append("<td style='width: 80%'>&nbsp;")
                builder.Append(Now().ToLongDateString & " " & Now().ToLongTimeString)
                builder.Append("</td>")
                builder.Append("</tr>")

                'builder.Append("<tr>")
                'builder.Append("<td style='width: 20%'><B>Time</B></td>")
                'builder.Append("<td style='width: 80%'>&nbsp;")
                'builder.Append(Now().ToLongTimeString)
                'builder.Append("</td>")
                'builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%'><B>Advantage DB</B></td>")
                builder.Append("<td style='width: 80%'>&nbsp;")
                builder.Append(AdvantageDBBuild & " - " & AdvantageDBBuildDate)
                builder.Append("</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%'><B>DB Name</B></td>")
                builder.Append("<td style='width: 80%'>&nbsp;")
                builder.Append(GetDBName)
                builder.Append("</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 100%' align='center' colspan='2' bgcolor='#63B1FF'><B>Environment</B></td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>MachineName</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & Environment.MachineName.ToString & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>OS</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & OS & " SP " & OSServicePack & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Browser</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & Browser & " " & BrowserVersion & "</td>")
                builder.Append("</tr>")

                For Each row As DataRow In dtProcessor.Rows
                    builder.Append("<tr>")
                    builder.Append("<td style='width: 20%' align='left'><B>Processor</B></td>")
                    If UCase(row("Processor").ToString).Contains("GHZ") Then
                        builder.Append("<td style='width: 80%' align='left'>" & row("Processor") & "</td>")
                    Else
                        builder.Append("<td style='width: 80%' align='left'>" & row("Processor") & " " & row("Speed") & "</td>")
                    End If

                    builder.Append("</tr>")
                Next

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>32/64</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & dtProcessor.Rows(0).Item("AddressWidth").ToString & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Free RAM</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & AvailableRam & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Total RAM</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & TotalRam & "</td>")
                builder.Append("</tr>")

                For Each row As DataRow In dtHardDriveinfo.Rows
                    builder.Append("<tr>")
                    builder.Append("<td style='width: 20%' align='left'><B>Drive</B></td>")
                    builder.Append("<td style='width: 80%' align='left'>" & row("DeviceID") & "</td>")
                    builder.Append("</tr>")

                    builder.Append("<tr>")
                    builder.Append("<td style='width: 20%' align='left'><B>Free Space</B></td>")
                    builder.Append("<td style='width: 80%' align='left'>" & row("FreeSpace") & "</td>")
                    builder.Append("</tr>")

                    builder.Append("<tr>")
                    builder.Append("<td style='width: 20%' align='left'><B>Total Space</B></td>")
                    builder.Append("<td style='width: 80%' align='left'>" & row("Size") & "</td>")
                    builder.Append("</tr>")
                Next

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Network Connection</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & NetworkSpeed & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>SQL Version</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & SQLVersion & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>SQL Compatibility</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & SQLCompatibility & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Report Execution Services</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("ReportExecutionServices")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>IIS Version</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & IISVersion & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>DUAL Environment</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & IsDualEnvironment() & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 100%' align='center' colspan='2' bgcolor='#63B1FF'><B>School Settings</B></td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Address to be Printed in Receipts</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("AddressToBePrintedInReceipts")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Allow Adding Courses to ProgVersions After Students been Enrolled</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("AllowAddingCoursesToProgramVersionsAfterStudentsHaveBeenEnrolled")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B># of Days to Allow Register Payments in future</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("AllowRegisterPaymentsIntheFutureWithUpToThisNumberOfDaysInTheFuture")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Award Cut Off Date</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("AwardsCutOffDate")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Deferred Revenue Calc Method</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("DeferredRevenueCalcMethod")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Display Attendance Unit Prog Report by Class</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("DisplayAttendanceUnitForProgressReportBYclass")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Document Path</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("DocumentPath")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Edit Other Leads</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("EditOtherLeads")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>EDExpress Data FromRemote Computer</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer")) & "</td>")
                builder.Append("</tr>")


                If UCase(CheckValue(MyAdvAppSettings.AppSettings("EDExpressDataFromRemoteComputer"))) = "YES" Then
                    builder.Append("<tr>")
                    builder.Append("<td style='width: 20%' align='left'><B>EDExpress Import File Path</B></td>")
                    builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("RemoteEDExpressIN")) & "</td>")
                    builder.Append("</tr>")

                    builder.Append("<tr>")
                    builder.Append("<td style='width: 20%' align='left'><B>EDExpress Exception Path</B></td>")
                    builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("RemoteEDExpressException")) & "</td>")
                    builder.Append("</tr>")

                    builder.Append("<tr>")
                    builder.Append("<td style='width: 20%' align='left'><B>EDExpress Archive Path</B></td>")
                    builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("RemoteEDExpressArchive")) & "</td>")
                    builder.Append("</tr>")
                End If

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>enforce # of Grading Components</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("EnforceNumberOfGradingComponents")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Grade Course Repetitions Method</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("GradeCourseRepetitionsMethod")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Grade Book Weighting Level</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("GradeBookWeightingLevel")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Grade Rounding</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("Graderounding")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Grades Format</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("GradesFormat")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Include Credits Attempted for Repeat Courses</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("IncludeCreditsAttemptedForRepeatedCourses")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Include Hours For Failing Grade</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("IncludeHoursForFailingGrade")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Logo for Transcripts</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("LogoForTranscripts")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Max # Grade Book Weightings</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("MaxNoOfGradeBookWeightings")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Minimum GPA</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("MinimumGPA")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Pass All Courses for Sap Check</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("PassAllCoursesForSAPCheck")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Scheduling Method</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("SchedulingMethod")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Show Leads to Users</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("ShowLeadstoUsers")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Show Externship Tabs</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("ShowExternshipTabs")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Show Student Scheduling Group</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("ShowStudentSchedulingGroup")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Show Ross Only Tab</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("ShowRossOnlyTabsForStudent")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Standard Logo</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("StandardLogo")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Student Identifier</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("StudentIdentifier")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Student Image Path</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("StudentImagePath")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Track SAP Attendance</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("TrackSapAttendance")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Transcript Credits</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("TranscriptCredits")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Transcript Type</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("TranscriptType")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Use Cohort Start Date for Post Term Fees</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("UseCohortStartDateForPostTermFees")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>ESP Data from Remote Computer</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("ESPDataFromRemoteComputer")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Fame ESP</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("FAMEESP")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%' align='left'><B>Exception Folder Location</B></td>")
                builder.Append("<td style='width: 80%' align='left'>" & CheckValue(MyAdvAppSettings.AppSettings("ExceptionFolderLocation")) & "</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 100%' align='center' colspan='2' bgcolor='#63B1FF'><B>Error</B></td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%'><B>Url</B></td>")
                builder.Append("<td style='width: 80%'>&nbsp;")
                builder.Append(HttpContext.Current.Request.Url.AbsoluteUri)
                builder.Append("</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%'><B>Source</B></td>")
                builder.Append("<td style='width: 80%'>&nbsp;")
                builder.Append(ex.Source)
                builder.Append("</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%'><B>Message</B></td>")
                builder.Append("<td style='width: 80%'>&nbsp;")
                builder.Append(ex.Message)
                builder.Append("</td>")
                builder.Append("</tr>")

                builder.Append("<tr>")
                builder.Append("<td style='width: 20%'><B>StackTrace</B></td>")
                builder.Append("<td style='width: 80%'>&nbsp;")
                builder.Append(ex.StackTrace)
                builder.Append("</td>")
                builder.Append("</tr>")

                If Not (ex.InnerException Is Nothing) Then
                    builder.Append("<tr>")
                    builder.Append("<td style='width: 20%'><B>InnerException</B></td>")
                    builder.Append("<td style='width: 80%'>&nbsp;")
                    builder.Append(ex.InnerException)
                    builder.Append("</td>")
                    builder.Append("</tr>")
                End If

                builder.Append("</table>")

                Return builder.ToString
            Catch e As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(e)

                Throw
            End Try
        End Function

#Region "Get Information Functions"

        Public Shared Function GetNetworkConnection() As String
            Dim myNA() As NetworkInterface = NetworkInterface.GetAllNetworkInterfaces
            Dim NetworkSpeed = SetBytes(myNA(0).Speed) & "ps"
            Return NetworkSpeed
        End Function

        Public Shared Function HardDriveInfo() As DataTable
            Dim dtHardDriveInfo As New DataTable
            Dim myNewRow As DataRow

            dtHardDriveInfo.Columns.Add("DeviceID")
            dtHardDriveInfo.Columns.Add("Size")
            dtHardDriveInfo.Columns.Add("FreeSpace")

            Dim MGMT As ManagementObject
            Dim Searcher As ManagementObjectSearcher
            Searcher = New ManagementObjectSearcher("SELECT * FROM Win32_LogicalDisk")
            For Each MGMT In Searcher.Get
                If Convert.ToString(MGMT("MediaType")) = "12" Then
                    myNewRow = dtHardDriveInfo.NewRow()
                    myNewRow("DeviceID") = (MGMT("DeviceID").ToString)
                    myNewRow("Size") = SetBytes(MGMT("Size").ToString)
                    myNewRow("FreeSpace") = SetBytes(MGMT("FreeSpace").ToString)
                    dtHardDriveInfo.Rows.Add(myNewRow)
                End If
            Next
            Return dtHardDriveInfo
        End Function

        Public Shared Function GetSpaceInDrives() As DataTable
            Dim dtHardDriveInfo As New DataTable

            Dim myNewRow As DataRow

            dtHardDriveInfo.Columns.Add("DeviceID")
            dtHardDriveInfo.Columns.Add("Size")
            dtHardDriveInfo.Columns.Add("FreeSpace")

            Dim oDrvs() As DriveInfo = DriveInfo.GetDrives
            For Each Drv As DriveInfo In oDrvs
                If Drv.IsReady Then
                    If Drv.DriveFormat = "NTFS" And Drv.DriveType.ToString = "Fixed" Then
                        myNewRow = dtHardDriveInfo.NewRow()
                        myNewRow("DeviceID") = Trim(Drv.Name.ToString)
                        myNewRow("Size") = SetBytes(Drv.TotalSize)
                        myNewRow("FreeSpace") = SetBytes(Drv.AvailableFreeSpace)
                        dtHardDriveInfo.Rows.Add(myNewRow)
                    End If
                End If
            Next
            Return dtHardDriveInfo
        End Function

        Public Shared Function Processors() As DataTable
            Dim moReturn As ManagementObjectCollection
            Dim moSearch As ManagementObjectSearcher
            Dim mo As ManagementObject

            Dim dtProcessors As New DataTable
            Dim myNewRow As DataRow

            dtProcessors.Columns.Add("Processor")
            dtProcessors.Columns.Add("AddressWidth")
            dtProcessors.Columns.Add("Speed")

            moSearch = New ManagementObjectSearcher("Select * from Win32_Processor")
            moReturn = moSearch.Get

            For Each mo In moReturn
                myNewRow = dtProcessors.NewRow()
                myNewRow("Processor") = mo("Name")
                myNewRow("AddressWidth") = mo("AddressWidth") & " BIT "
                myNewRow("Speed") = Format(mo("CurrentClockSpeed") / 1000, "#0.00") & " GHz"
                dtProcessors.Rows.Add(myNewRow)
            Next
            Return dtProcessors
        End Function

        Public Shared Function GetRamAndOSInformation() As String
            Dim MGMT As ManagementObject
            Dim Searcher As ManagementObjectSearcher
            Searcher = New ManagementObjectSearcher("SELECT * FROM Win32_OperatingSystem")

            Dim FreePhysicalMemory As String = String.Empty
            Dim FreeVirtualMemory As String = String.Empty
            Dim TotalVirtualMemorySize As String = String.Empty
            Dim TotalVisibleMemorySize As String = String.Empty
            Dim OS As String = String.Empty
            Dim OSVersion As String = String.Empty
            Dim OsVersionServicePack As String = String.Empty

            If IsNumeric(Searcher.Get.Count) Then
                For Each MGMT In Searcher.Get
                    FreePhysicalMemory = CheckValue(SetBytes(Regex.Replace(MGMT("FreePhysicalMemory").ToString, "[^.0-9]", "") * 1000))
                    FreeVirtualMemory = CheckValue(SetBytes(Regex.Replace(MGMT("FreeVirtualMemory").ToString, "[^.0-9]", "") * 1000))
                    TotalVirtualMemorySize = CheckValue(SetBytes(Regex.Replace(MGMT("TotalVirtualMemorySize").ToString, "[^.0-9]", "") * 1000))
                    TotalVisibleMemorySize = CheckValue(SetBytes(Regex.Replace(MGMT("TotalVisibleMemorySize").ToString, "[^.0-9]", "") * 1000))
                    OS = CheckValue(Replace(Trim(MGMT("Caption")), ",", " "))
                    OSVersion = CheckValue(Replace(Trim(MGMT("Version")), ",", " "))
                    OsVersionServicePack = CheckValue(Replace(Trim(MGMT("ServicePackMajorVersion")), ",", " "))
                Next
            End If
            Return FreePhysicalMemory & "," & TotalVisibleMemorySize & "," & OS & "," & OSVersion & "," & OsVersionServicePack
        End Function

        Public Shared Function GetIISVersion() As Version
            Using componentsKey As RegistryKey = Registry.LocalMachine.OpenSubKey("Software\Microsoft\InetStp", False)
                If componentsKey IsNot Nothing Then
                    Dim majorVersion As Integer = CInt(componentsKey.GetValue("MajorVersion", -1))
                    Dim minorVersion As Integer = CInt(componentsKey.GetValue("MinorVersion", -1))
                    If majorVersion <> -1 AndAlso minorVersion <> -1 Then
                        Return New Version(majorVersion, minorVersion)
                    End If
                End If
                Return New Version(0, 0)
            End Using
        End Function

        Public Shared Function GetSQLVersion() As String
            Try

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim myConnection As New SqlConnection(MyAdvAppSettings.AppSettings("ConnectionString"))
                myConnection.Open()
                Dim GetSQLVersionCommand As SqlCommand = myConnection.CreateCommand()

                Dim dr As SqlDataReader
                Dim GetSQLString As New StringBuilder
                GetSQLString.Append("SELECT @@VERSION as Version")
                GetSQLVersionCommand.CommandText = GetSQLString.ToString
                dr = GetSQLVersionCommand.ExecuteReader

                Dim SQLVersion As String = String.Empty

                If dr.HasRows Then
                    Do While dr.Read()
                        SQLVersion = Trim(dr.Item("Version"))
                    Loop
                Else
                    SQLVersion = "UNAVAILABLE"
                End If

                myConnection.Close()
                Return SQLVersion
            Catch e As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(e)

                Throw
            End Try
        End Function

        Public Shared Function GetSQLVersionCompatibility() As String
            Try

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim myConnection As New SqlConnection(MyAdvAppSettings.AppSettings("ConnectionString"))
                myConnection.Open()
                Dim GetSQLCompatibilityCommand As SqlCommand = myConnection.CreateCommand()

                Dim dr As SqlDataReader
                Dim GetSQLString As New StringBuilder
                GetSQLString.Append("select compatibility_level from sys.databases where name=db_name()")
                GetSQLCompatibilityCommand.CommandText = GetSQLString.ToString
                dr = GetSQLCompatibilityCommand.ExecuteReader

                Dim SQLCompatibilityLevel As String = String.Empty

                If dr.HasRows Then
                    Do While dr.Read()
                        SQLCompatibilityLevel = Trim(dr.Item("compatibility_level"))
                    Loop
                End If

                If SQLCompatibilityLevel = "100" Then
                    SQLCompatibilityLevel = "SQL Server 2008 (100)"
                ElseIf SQLCompatibilityLevel = "90" Then
                    SQLCompatibilityLevel = "SQL Server 2005 (90)"
                ElseIf SQLCompatibilityLevel = "80" Then
                    SQLCompatibilityLevel = "SQL Server 2000 (80)"
                ElseIf SQLCompatibilityLevel = "70" Then
                    SQLCompatibilityLevel = "SQL Server 7.0 (70)"
                ElseIf SQLCompatibilityLevel = "60" Then
                    SQLCompatibilityLevel = "SQL Server 6.0 (60)"
                Else
                    SQLCompatibilityLevel = "UNAVAILABLE"
                End If

                myConnection.Close()
                Return SQLCompatibilityLevel

            Catch e As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(e)

                Throw
            End Try
        End Function

        Public Shared Function AdvantageDBVersion() As String
            Try

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim myConnection As New SqlConnection(MyAdvAppSettings.AppSettings("ConnectionString"))
                myConnection.Open()
                Dim GetSQLVersionCommand As SqlCommand = myConnection.CreateCommand()

                Dim dr As SqlDataReader
                Dim GetSQLString As New StringBuilder
                GetSQLString.Append("SELECT * FROM syadvbuild ")
                GetSQLVersionCommand.CommandText = GetSQLString.ToString
                dr = GetSQLVersionCommand.ExecuteReader

                Dim BuildNumber As String = String.Empty
                Dim BuildDate As String = String.Empty

                If dr.HasRows Then
                    Do While dr.Read()
                        BuildNumber = Trim(dr.Item("BuildNumber"))
                        BuildDate = Trim(dr.Item("ModDate"))
                    Loop
                Else
                    BuildNumber = "UNAVAILABLE"
                    BuildDate = "UNAVAILABLE"
                End If

                myConnection.Close()

                Return BuildNumber + "," + BuildDate
            Catch e As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(e)

                Throw
            End Try
        End Function

        Public Shared Function GetEnvironmentFromSQL() As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                Dim myConnection As New SqlConnection(MyAdvAppSettings.AppSettings("ConnectionString"))
                myConnection.Open()
                Dim SQLEnvironmentName As SqlCommand = myConnection.CreateCommand()

                Dim dr As SqlDataReader
                Dim GetSQLString As New StringBuilder
                GetSQLString.Append("select SERVERPROPERTY ('ServerName') as Computer_Name")
                SQLEnvironmentName.CommandText = GetSQLString.ToString
                dr = SQLEnvironmentName.ExecuteReader

                Dim SQLVersion As String = String.Empty

                If dr.HasRows Then
                    Do While dr.Read()
                        If dr.Item("Computer_Name").ToString.Contains("\") Then
                            SQLVersion = Trim(dr.Item("Computer_Name").ToString.Split("\")(0))
                        Else
                            SQLVersion = Trim(dr.Item("Computer_Name"))
                        End If
                    Loop
                Else
                    SQLVersion = "UNAVAILABLE"
                End If

                myConnection.Close()

                Return SQLVersion
            Catch e As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(e)

                Throw
            End Try
        End Function

        Public Shared Function SetBytes(ByVal Bytes As String) As String
            Try
                If Bytes >= 1099511627776 Then
                    SetBytes = Format(Bytes / 1024 / 1024 / 1024 / 1024, "#0.00") & " Tb"
                ElseIf Bytes >= 1073741824 Then
                    SetBytes = Format(Bytes / 1024 / 1024 / 1024, "#0.00") & " Gb"
                ElseIf Bytes >= 1048576 Then
                    SetBytes = Format(Bytes / 1024 / 1024, "#0.00") & " Mb"
                ElseIf Bytes >= 1024 Then
                    SetBytes = Format(Bytes / 1024, "#0.00") & " Kb"
                ElseIf Bytes < 1024 Then
                    SetBytes = Fix(Bytes) & " Bytes"
                Else
                    SetBytes = "UNAVAILABLE"
                End If
                Return SetBytes
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw
            End Try
        End Function

        Public Shared Function CheckValue(ByVal sStr As Object) As String
            Try
                If sStr Is Nothing Then
                    CheckValue = "UNAVAILABLE"
                ElseIf IsDBNull(sStr) Then
                    CheckValue = "UNAVAILABLE"
                Else
                    Dim myStr As String
                    myStr = sStr.ToString.Trim

                    If Len(myStr) = 0 Then
                        CheckValue = "UNAVAILABLE"
                    Else
                        CheckValue = CType(sStr, String)
                    End If
                End If
                Return CheckValue
            Catch ex As Threading.ThreadAbortException
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ' Do Nothing. It is normal exception when redirecting
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw
            End Try
            Return "UNAVAILABLE"
        End Function

        Public Shared Function IsDualEnvironment() As String

            Dim DualEnvironment As String
            If Environment.MachineName.ToString = "UNAVAILABLE" Or GetEnvironmentFromSQL.ToString = "UNAVAILABLE" Then
                DualEnvironment = "UNKNOWN"
            ElseIf UCase(Environment.MachineName.ToString) <> UCase(GetEnvironmentFromSQL.ToString) Then
                DualEnvironment = "YES"
            ElseIf UCase(Environment.MachineName.ToString) = UCase(GetEnvironmentFromSQL.ToString) Then
                DualEnvironment = "NO"
            Else
                DualEnvironment = "UNKNOWN"
            End If

            Return DualEnvironment

        End Function

        Public Shared Function GetDBName() As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Dim DB As String = MyAdvAppSettings.AppSettings("ConnectionString").ToString.Split(";")(1)
            DB = DB.ToString.Split("=")(1)
            Return DB

        End Function

        Public Shared Function GetIdentityMruType(resId As String) As Integer
            Select Case resId

                Case "90", "92", "95", "97", "112",
                                   "114", "116", "155", "159", "169",
                                       "175", "177", "200", "203", "213",
                                       "230", "286", "301", "303", "327",
                                       "372", "374", "380", "479", "530",
                                       "531", "532", "534", "535", "538",
                                       "539", "541", "542", "543", "614",
                                       "625", "628", "652", "113", "334", "861", "868" ' Student Page , "308"
                    Return 1
                Case "79", "82", "86", "111" 'Employer Tabs , "197"
                    Return 2
                Case "52", "55", "69", "281" 'Employee Tabs , "309"
                    Return 3
                Case "145", "146", "147", "148", "170",
                       "225", "313", "456", "484", "793", "704", "826", "838", "174" 'Lead Tabs , "153"
                    Return 4
                Case Else
                    Return 0
            End Select
        End Function

        Public Shared Function ReloadUrl(ByVal curCampusId As Guid, ByVal originalUrl As String, ByVal urlPath As String) As String
            Dim nameValues = HttpUtility.ParseQueryString(originalUrl)
            'nameValues.[Set]("VSI", strVSI)
            nameValues.[Set]("cmpid", curCampusId.ToString())
            Dim url As String = urlPath
            Dim updatedQueryString As String = "?" + nameValues.ToString()
            Dim newUrl As String = url + updatedQueryString
            Return newUrl
        End Function

#End Region

    End Class

End Namespace
