﻿Imports System.Reflection
Imports System.Web.Optimization

Namespace AdvWeb.VBCode.Bundling
    Public Class BundleConfig
        Public Shared Sub RegisterBundles(bundles As BundleCollection)

            bundles.Add(New StyleBundle("~/bundles/masterpagestyle").Include(
                "~/css/colors.css",
                "~/css/KendoMaterial/Menu.KendoMaterial.css",
                "~/css/KendoMaterial/Navigation.KendoMaterial.css",
                "~/css/KendoMaterial/RibbonBar.KendoMaterial.css",
                "~/css/KendoMaterial/PanelBar.KendoMaterial.css",
                "~/css/KendoMaterial/TreeView.KendoMaterial.css",
                "~/css/KendoMaterial/ToolBar.KendoMaterial.css",
                "~/css/KendoMaterial/TabStrip.KendoMaterial.css",
                "~/css/KendoMaterial/SearchBox.KendoMaterial.css",
                "~/css/KendoMaterial/Grid.KendoMaterial.css",
                "~/css/KendoMaterial/TreeList.KendoMaterial.css",
                "~/css/KendoMaterial/DataForm.KendoMaterial.css",
                "~/css/KendoMaterial/ListBox.KendoMaterial.css",
                "~/css/KendoMaterial/Filter.KendoMaterial.css",
                "~/css/KendoMaterial/Editor.KendoMaterial.css",
                "~/css/KendoMaterial/ImageEditor.KendoMaterial.css",
                "~/css/KendoMaterial/AutoCompleteBox.KendoMaterial.css",
                "~/css/KendoMaterial/ComboBox.KendoMaterial.css",
                "~/css/KendoMaterial/DropDownList.KendoMaterial.css",
                "~/css/KendoMaterial/DropDownTree.KendoMaterial.css",
                "~/css/KendoMaterial/Input.KendoMaterial.css",
                "~/css/KendoMaterial/Slider.KendoMaterial.css",
                "~/css/KendoMaterial/ColorPicker.KendoMaterial.css",
                "~/css/KendoMaterial/Wizard.KendoMaterial.css",
                "~/css/KendoMaterial/Splitter.KendoMaterial.css",
                "~/css/KendoMaterial/Dock.KendoMaterial.css",
                "~/css/KendoMaterial/Window.KendoMaterial.css",
                "~/css/KendoMaterial/Notification.KendoMaterial.css",
                "~/css/KendoMaterial/ToolTip.KendoMaterial.css",
                "~/css/KendoMaterial/Tile.KendoMaterial.css",
                "~/css/KendoMaterial/TileList.KendoMaterial.css",
                "~/css/KendoMaterial/Rotator.KendoMaterial.css",
                "~/css/KendoMaterial/Button.KendoMaterial.css",
                "~/css/KendoMaterial/Scheduler.KendoMaterial.css",
                "~/css/KendoMaterial/Gantt.KendoMaterial.css",
                "~/css/KendoMaterial/Calendar.KendoMaterial.css",
                "~/css/KendoMaterial/MediaPlayer.KendoMaterial.css",
                "~/css/KendoMaterial/ImageGallery.KendoMaterial.css",
                "~/css/KendoMaterial/LightBox.KendoMaterial.css",
                "~/css/KendoMaterial/FileExplorer.KendoMaterial.css",
                "~/css/KendoMaterial/CloudUpload.KendoMaterial.css",
                "~/css/KendoMaterial/Upload.KendoMaterial.css",
                "~/css/KendoMaterial/TagCloud.KendoMaterial.css",
                "~/css/KendoMaterial/SocialShare.KendoMaterial.css",
                "~/css/KendoMaterial/Rating.KendoMaterial.css",
                "~/css/KendoMaterial/OrgChart.KendoMaterial.css",
                "~/css/KendoMaterial/Map.KendoMaterial.css",
                "~/css/KendoMaterial/ProgressArea.KendoMaterial.css",
                "~/css/KendoMaterial/ProgressBar.KendoMaterial.css",
                "~/css/KendoMaterial/FormDecorator.KendoMaterial.css",
                "~/Kendo/styles/kendo.common.min.css",
                "~/Kendo/styles/kendo.material.min.css",
                "~/css/SiteStyle.min.css",
                "~/css/localhost_lowercase.css",
                "~/css/MainMenu.css"))

            '"~/Kendo/styles/kendo.dataviz.min.css",   'Not used after UI2015 Q2
            '"~/Kendo/styles/kendo.dataviz.blueopal.min.css", 'Not used after UI2015 Q2
            '"~/kendo/js/kendo.web.min.js",
            '"~/kendo/js/kendo.panelbar.min.js",
            ' "~/Scripts/Advantage.Client.DataAccess.js",

            bundles.Add(New Bundle("~/bundles/ApiClientScripts").Include(
                "~/Scripts/Fame.Advantage.API.Client.js",
                "~/Scripts/Advantage.Client.js"
            ))

            bundles.Add(New ScriptBundle("~/bundles/masterpagescripts").Include(
                             "~/kendo/js/jquery.min.js",
                             "~/kendo/js/kendo.all.min.js",
                             "~/Scripts/common/common.js",
                             "~/Scripts/viewModels/modelCommon.js",
                             "~/Scripts/common-util.js",
                             "~/Scripts/Storage/storageCache.js",
                             "~/Scripts/KendoExtensions/kendo.web.ext.js",
                             "~/Scripts/demoUI.js",
                             "~/js/masterPage.js",
                             "~/Scripts/Advantage.Client.MasterPage.js",
                             "~/Scripts/datasources/uSearchStudentDataSource.js",
                             "~/Scripts/datasources/uSearchLeadDataSource.js",
                             "~/Scripts/datasources/uSearchEmployerDataSource.js",
                             "~/Scripts/datasources/uSearchEmployeeDataSource.js",
                             "~/Scripts/datasources/campusesDataSource.js",
                             "~/Scripts/datasources/admissionsRepDataSource.js",
                             "~/Scripts/common/mousetrap.js",
                            "~/Scripts/common/mousetrap-global-bind.js",
                             "~/Scripts/viewModels/universalSearch.js",
                             "~/Scripts/viewModels/mostRecentlyUsed.js",
                             "~/Scripts/DataSources/MruDataSources/studentMruDataSource.js",
                             "~/Scripts/DataSources/MruDataSources/leadMruDataSource.js",
                             "~/Scripts/DataSources/MruDataSources/employerMruDataSource.js",
                            "~/Scripts/DataSources/MruDataSources/employeeMruDataSource.js",
                            "~/Scripts/KendoExtensions/maskedDatePicker.js"))

            ' "~/Scripts/MainMenu/menuItems.js",

            bundles.Add(New ScriptBundle("~/bundles/popupscripts").Include("~/kendo/js/jquery.min.js",
                                                                               "~/kendo/js/kendo.all.min.js",
                                                                               "~/Scripts/common/common.js",
                                                                               "~/Scripts/viewModels/modelCommon.js",
                                                                               "~/Scripts/common-util.js",
                                                                               "~/Scripts/Storage/storageCache.js",
                                                                               "~/Scripts/KendoExtensions/kendo.web.ext.js",
                                                                               "~/Scripts/demoUI.js",
                                                                               "~/js/masterPage.js",
                                                                               "~/Scripts/Fame.Advantage.API.Client.js",
                                                                               "~/Scripts/Advantage.Client.MasterPage.js"))

            bundles.Add(New ScriptBundle("~/bundles/kwindowscripts").Include("~/kendo/js/jquery.min.js",
                                                                              "~/kendo/js/kendo.all.min.js",
                                                                              "~/Scripts/common/common.js",
                                                                              "~/Scripts/viewModels/modelCommon.js",
                                                                              "~/Scripts/common-util.js",
                                                                              "~/Scripts/Storage/storageCache.js",
                                                                              "~/Scripts/KendoExtensions/kendo.web.ext.js"))
            bundles.Add(New StyleBundle("~/bundles/popupstyle").Include(
                  "~/Kendo/styles/kendo.common.min.css",
                "~/Kendo/styles/kendo.material.min.css"))

            bundles.Add(New ScriptBundle("~/bundles/webcamScripts").Include("~/Scripts/WebCam/jquery.webcam.js","~/Scripts/common/DocumentTypeSearch.js"))

            bundles.Add(New StyleBundle("~/bundles/otherStyles").Include(
                "~/css/AdvantageValidator.css",
                "~/css/DocumentTypeSearch.css"))

            'Set the optimization key by web.config 
            BundleTable.EnableOptimizations = CType(ConfigurationManager.AppSettings("BundleEnableOptimizations"), Boolean)

            'BundleTable.EnableOptimizations = False

        End Sub

        'Public Shared Sub RegisterBundles(bundles As BundleCollection)
        '    bundles.Add(New StyleBundle("~/bundles/masterpagestyle").Include(
        '                "~/Kendo/styles/kendo.common.css",
        '                "~/Kendo/styles/kendo.blueopal.css",
        '                "~/css/SiteStyles.css",
        '                "~/css/localhost_lowercase.css"))


        '    bundles.Add(New ScriptBundle("~/bundles/kenodscripts").Include(
        '                    "~/kendo/js/jquery.min.js",
        '                    "~/kendo/js/kendo.web.min.js",
        '                    "~/kendo/js/kendo.panelbar.min.js"
        '                ))

        '    bundles.Add(New ScriptBundle("~/bundles/masterpagescripts").Include(
        '                  "~/Scripts/common/common.js",
        '                  "~/Scripts/common-util.js",
        '                  "~/Scripts/Storage/storageCache.js",
        '                  "~/Scripts/KendoExtensions/kendo.web.ext.js",
        '                  "~/Scripts/MainMenu/menuItem.js",
        '                  "~/Scripts/Advantage.Client.DataAccess.js",
        '                  "~/Scripts/demoUI.js",
        '                  "~/js/masterPage.js",
        '                  "~/Scripts/UserControls/MasterPageCampusDropDown.js",
        '                  "~/Scripts/UserControls/MasterPageUserOptionsPanelBar.js"))
        'End Sub
    End Class
End Namespace