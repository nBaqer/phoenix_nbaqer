﻿

Public Class DeferredRevenuePostingModesEnumerator
    Public Enum DeferredRevenuePostingMode
        BuildList = 1
        BindTransCodeDetails = 2
        PostDeferredRevenue = 3
    End Enum
End Class
