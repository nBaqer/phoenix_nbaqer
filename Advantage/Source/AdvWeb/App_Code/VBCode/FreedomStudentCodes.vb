﻿
Public Class FreedomStudentCodesEnumerator
    Public Enum PhoneType
        Home = 1
        Work = 2
        Mobile = 3
        Fax = 4
        Pager = 5
        Other = 6
    End Enum
    Public Enum Sex
        Male = 1
        Female = 2
    End Enum
    Public Enum GeographicTypes
        Urban = 1
        Suburban = 2
        Rural = 3
    End Enum
    Public Enum Race
        AsianPacificIslander = 1
        BlackNonHispanic = 2
        WhiteNonHispanic = 3
        Hispanic = 4
        AmericanIndianAlaskaNative = 5
        RaceEthnicityUnknown = 6
    End Enum
    Public Enum DependencyTypes
        Independent = 1
        Dependent = 2
    End Enum
    Public Enum EducationLevel
        Postsecondary = 1
        Highschool = 2
        Graduateschool = 3
    End Enum
    Public Enum MaritalStatus
        SingleStatus = 1
        Married = 2
        Separated = 5
        Divorced = 3
        Widowed = 6
    End Enum
    Public Enum CitizenShip
' ReSharper disable InconsistentNaming
        USCitizen = 1
' ReSharper restore InconsistentNaming
        Eligiblenoncitizen = 2
        NonCitizen = 3
    End Enum
    Public Enum Housing
        OnCampus = 1
        OffCampus = 2
        WithParents = 3
        Incarcerated = 4
    End Enum
End Class

Public Class FreedomStudentCodes
    Dim id As Integer
    Public Property PhoneType() As String
        Get
            Select Case id
                Case 1
                    Return "Home"
                Case 2
                    Return "Work"
                Case 3
                    Return "Mobile"
                Case 4
                    Return "Fax"
                Case 5
                    Return "Pager"
                Case 6
                    Return "Other"
                Case Else
                    Return "Other"
            End Select
        End Get
        Set(ByVal value As String)
            id = CType(value, Integer)
        End Set
    End Property
    Public Property Sex() As String
        Get
            Select Case id
                Case 1
                    Return "Male"
                Case 2
                    Return "Female"
                Case Else
                    Return "Unknown"
            End Select
        End Get
        Set(ByVal value As String)
            id = CType(value, Integer)
        End Set
    End Property
    Public Property GeographicTypes() As String
        Get
            Select Case id
                Case 1
                    Return "Urban (over 100,000)"
                Case 2
                    Return "Suburban (25,000 - 100,000)"
                Case 3
                    Return "Rural (under 25,000)"
            End Select
            Return Nothing
        End Get
        Set(ByVal value As String)
            id = CType(value, Integer)
        End Set
    End Property
    Public Property RaceTypes() As String
        Get
            Select Case id
                Case 1
                    Return "Asian/Pacific Islander"
                Case 2
                    Return "Black, non-Hispanic"
                Case 3
                    Return "White, non-Hispanic"
                Case 4
                    Return "Hispanic"
                Case 5
                    Return "American Indian/Alaska native"
                Case 6
                    Return "Race/ethnicity unknown"
            End Select
            Return String.Empty
        End Get
        Set(ByVal value As String)
            id = CType(value, Integer)
        End Set
    End Property
    Public Property DependencyTypes() As String
        Get
            Select Case id
                Case 1
                    Return "Independent"
                Case 2
                    Return "Dependent"
            End Select
            Return String.Empty
        End Get
        Set(ByVal value As String)
            id = CType(value, Integer)
        End Set
    End Property
    Public Property EducationLevel() As String
        Get
            Select Case id
                Case 1
                    Return "Postsecondary"
                Case 2
                    Return "High school"
                Case 3
                    Return "Graduate school"
            End Select
             Return String.Empty
        End Get
        Set(ByVal value As String)
            id = CType(value, Integer)
        End Set
    End Property
    Public Property MaritalStatus() As String
        Get
            Select Case id
                Case 1
                    Return "Single"
                Case 2
                    Return "Married"
                Case 3
                    Return "Divorced"
                Case 5
                    Return "Separated"
                Case 6
                    Return "Widowed"
            End Select
             Return String.Empty
        End Get
        Set(ByVal value As String)
            id = CType(value, Integer)
        End Set
    End Property
    Public Property Citizenship() As String
        Get
            Select Case id
                Case 1
                    Return "US citizen"
                Case 2
                    Return "Eligible non citizen"
                Case 3
                    Return "Non citizen"
            End Select
             Return String.Empty
        End Get
        Set(ByVal value As String)
            id = CType(value, Integer)
        End Set
    End Property
    Public Property Housing() As String
        Get
            Select Case id
                Case 1
                    Return "On campus"
                Case 2
                    Return "Off campus"
                Case 3
                    Return "With parents"
                Case 4
                    Return "Incarcerated"
            End Select
             Return String.Empty
        End Get
        Set(ByVal value As String)
            id = CType(value, Integer)
        End Set
    End Property
End Class


