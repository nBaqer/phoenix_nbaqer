' ===============================================================================
' IMaintFormBase
' Interface definition used as the base for implementing for that are used in
' MainPopup.aspx.
' ===============================================================================
' Copyright (C) 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data

Public Interface IMaintFormBase
    ''' <summary>
    ''' The title that gets displayed in the IE title bar and possibly the header
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property Title() As String

    ''' <summary>
    ''' The ObjectId or Primary Key of the current object loaded in the form
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ObjId() As String

    ''' <summary>
    ''' The parent Id of the current object.  For example, if this is a form for
    ''' Program Version, then the ParentId would be the ProgId.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ParentId() As String

    ''' <summary>
    ''' The moddate of the current record loaded on the form.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property ModDate() As Date

    ''' <summary>
    ''' Handles the save operation
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function Handle_Save() As String

    ''' <summary>
    ''' Handles the new operation
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function Handle_New() As String

    ''' <summary>
    ''' Handles the delete operation
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function Handle_Delete() As String

    ''' <summary>
    ''' Return a dataset with the following columns:
    '''     Code, Descrip, Active, ModUser, ModDate
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <param name="bActive"></param>
    ''' <param name="bInactive"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetLHSDataSet(ByVal ID As String, ByVal bActive As Boolean, ByVal bInactive As Boolean) As DataSet

    ''' <summary>
    ''' Url of the image to display on the LHS
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property LHSImageUrl() As String

    ''' <summary>
    ''' Bind the form given an ID
    ''' </summary>
    ''' <param name="ID"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function BindForm(ByVal ID As String, Optional ByVal campusId As String = "", Optional ByVal Permission As String = "", Optional ByVal InSchoolEnrollmentStatus As String = "", Optional ByVal OutSchoolEnrollmentStatus As String = "") As String

    'Function BindForm(ByVal ID As String) As String
End Interface
