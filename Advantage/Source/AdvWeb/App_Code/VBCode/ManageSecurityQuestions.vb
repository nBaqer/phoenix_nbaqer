﻿
Imports System.Data
Imports System.Data.SqlClient
Public Class ManageSecurityQuestions
    Public Function GetSecurityQuestions() As DataSet
        Dim ds As New DataSet
        Dim connTenant As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("TenantAuthDBConnection").ToString)
        Dim cmdTenant As SqlCommand = New SqlCommand("USP_SecurityQuestions", connTenant)
        Dim cmdAdapter As SqlDataAdapter = New SqlDataAdapter()

        cmdTenant.CommandType = CommandType.StoredProcedure
        connTenant.Open()
        Try
            cmdAdapter.SelectCommand = cmdTenant
            cmdAdapter.Fill(ds)
            Return ds
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(ex.Message)
        Finally
            connTenant.Close()
        End Try
    End Function
    Public Function GetUserName(ByVal userId As String) As String
        Dim ds As New DataSet
        Dim connTenant As SqlConnection = New SqlConnection(ConfigurationManager.ConnectionStrings("TenantAuthDBConnection").ToString)
        Dim cmdTenant As SqlCommand = New SqlCommand("USP_UserName_Get", connTenant)
        Dim cmdAdapter As SqlDataAdapter = New SqlDataAdapter()
        Dim strUserName As String = ""

        cmdTenant.CommandType = CommandType.StoredProcedure
        cmdTenant.Parameters.Add("@UserId", SqlDbType.UniqueIdentifier).Value = New Guid(userId)
        connTenant.Open()
        Try
            strUserName = cmdTenant.ExecuteScalar()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(ex.Message)
        Finally
            connTenant.Close()
        End Try
        Return strUserName
    End Function
End Class
