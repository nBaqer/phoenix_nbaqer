﻿Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports Advantage.Business.Logic.Layer
Imports Microsoft.VisualBasic
Imports Advantage.Business.Objects

Namespace AdvWeb.VBCode

    Public Class MasterPageState

        Private ReadOnly _mruProvider As MRURoutines
        Private ReadOnly _radNotification1 As Telerik.Web.UI.RadNotification

        Public Sub New(mruProv As MRURoutines, radNotification As Telerik.Web.UI.RadNotification)
            _mruProvider = mruProv
            _radNotification1 = radNotification

        End Sub

        Public Sub BuildStudentStateObject(studentId As String, Optional ByVal campusId As String = "")
            Dim objStateInfo As AdvantageStateInfo
            Dim strVid As String
            Dim strStudentId = ""
            Dim strCampusId = ""

            If studentId.ToString.Trim = "" Then 'No Student was selected from MRU
                strStudentId = _mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString,
                                                                       1,
                                                                       AdvantageSession.UserState.CampusId.ToString)
                strCampusId = AdvantageSession.UserState.CampusId.ToString
            Else
                strStudentId = studentId
                strCampusId = campusId
            End If

            If Not strStudentId.Trim = "" Then
                objStateInfo = _mruProvider.BuildStudentStatusBar(strStudentId, strCampusId)
                With objStateInfo
                    If .StudentIdentifierCaption.ToLower = "ssn" Then
                        Dim ssnMask As String
                        Dim facInputMasks As New InputMasksFacade
                        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
                        If .StudentIdentifier <> "" Then
                            .StudentIdentifier = facInputMasks.ApplyMask(ssnMask, .StudentIdentifier)
                        End If
                    End If

                    'add to the session
                    AdvantageSession.MasterStudentId = strStudentId
                    AdvantageSession.MasterName = objStateInfo.NameValue


                    HttpContext.Current.Session("hdnSearchCampusId") = .CampusId.ToString()
                    HttpContext.Current.Session("previousCampusId") = ""
                    HttpContext.Current.Session("MasterName1") = objStateInfo.NameValue
                End With

                'Create a new guid to be associated with the employer pages
                strVid = Guid.NewGuid.ToString



                HttpContext.Current.Session("StudentObjectPointer") = strVid 'Needed as master page doesn't retain variable values when refreshed
                HttpContext.Current.Session("LeadObjectPointer") = strVid '""
                HttpContext.Current.Session("LeadMRFlag") = "false"

                'Add an entry to AdvantageSessionState for this guid and object
                'load Advantage state
                Dim state As AdvantageSessionState = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
                state(strVid) = objStateInfo
                'save current State
                CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)


                'Balaji 02/23/2012
                'The MRU list may come from different campuses, so make sure the QueryString always contains the 
                'lead or student's default campus
                'Reset AdvantageSession User State Object's CampusId Property
                '   Reason to reset user state: All Pages in Advantage are getting the campusid from the AdvantageSession.UserState Object
                '   So resetting the object here will impact all pages
                Dim strRedirectUrl = ""
                Try
                    Dim advResetUserState As User
                    advResetUserState = AdvantageSession.UserState
                    With advResetUserState
                        .CampusId = New Guid(objStateInfo.CampusId.ToString)
                    End With
                    AdvantageSession.UserState = advResetUserState

                    strRedirectUrl = BuildRedirectUrlForStudentPages(AdvantageSession.UserState.CampusId.ToString, strVid)

                    If strRedirectUrl.Trim = "" Then
                        ShowNotificationWhenUserHasNoAccessToEntityPage("student")
                        Exit Sub
                    End If

                    Dim fac As New UserSecurityFacade
                    fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New ArgumentException(ex.Message.ToString)
                End Try
                _mruProvider.InsertMRU(1, strStudentId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                AdvantageSession.StudentMRU = Nothing

                HttpContext.Current.Response.Redirect(strRedirectUrl, False)


            Else
                RedirectToStudentSearchPage(HttpContext.Current.Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            End If
        End Sub

        Public Sub BuildLeadStateObject(leadId As String, Optional ByVal boolSwitchCampus As Boolean = False)
            Dim objStateInfo As AdvantageStateInfo
            Dim strVid As String
            Dim strLeadId As String

            If leadId.ToString.Trim = "" Then 'No Lead was selected from MRU
                strLeadId = _mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString,
                                                                       4,
                                                                       AdvantageSession.UserState.CampusId.ToString)
            Else
                strLeadId = leadId
            End If

            If Not strLeadId.Trim = "" Then
                objStateInfo = _mruProvider.BuildLeadStatusBar(strLeadId)

                'Create a new guid to be associated with the employer pages
                strVid = Guid.NewGuid.ToString



                HttpContext.Current.Session("LeadObjectPointer") = strVid ' This step is required, as master page loses values stored in class variables

                If objStateInfo.SystemStatus = 6 Then 'If lead is enrolled set the student pointer
                    HttpContext.Current.Session("StudentObjectPointer") = strVid
                Else
                    HttpContext.Current.Session("StudentObjectPointer") = ""
                End If

                With objStateInfo
                    HttpContext.Current.Session("hdnSearchCampusId") = .CampusId.ToString()
                    HttpContext.Current.Session("previousCampusId") = ""
                    HttpContext.Current.Session("MasterName1") = objStateInfo.NameValue
                End With

                'Add an entry to AdvantageSessionState for this guid and object
                'load Advantage state
                Dim state As AdvantageSessionState = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
                state(strVid) = objStateInfo

                'save current State
                CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

                'add to the session
                AdvantageSession.MasterLeadId = strLeadId
                AdvantageSession.MasterName = objStateInfo.NameValue

                'UpdateMRUTable
                'Reason: We need to keep track of the last student record the user worked with
                'Scenario1 : User can log in and click on a student page without using MRU 
                'and we need to display the data of the last student the user worked with
                'If the user is a first time user, we will load the student who was last added to advantage
                'mruProvider.UpdateMRUList(strLeadId, AdvantageSession.UserState.UserId.ToString, _
                '                          AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)


                'Balaji 02/23/2012
                'The MRU list may come from different campuses, so make sure the QueryString always contains the 
                'lead or student's default campus
                'Reset AdvantageSession User State Object's CampusId Property
                '   Reason to reset user state: All Pages in Advantage are getting the campusid from the AdvantageSession.UserState Object
                '   So resetting the object here will impact all pages
                Dim strRedirectUrl = ""
                Try
                    Dim advResetUserState As User
                    advResetUserState = AdvantageSession.UserState
                    With advResetUserState
                        .CampusId = New Guid(objStateInfo.CampusId.ToString)
                    End With
                    AdvantageSession.UserState = advResetUserState

                    strRedirectUrl = BuildRedirectUrlForLeadPages(AdvantageSession.UserState.CampusId.ToString, strVid)
                    If strRedirectUrl.Trim = "" Then
                        ShowNotificationWhenUserHasNoAccessToEntityPage("lead")
                        ' Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
                        ' Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), RadPanelItem).Controls(3), RadGrid)
                        '  grdMRU.ToolTip = "User has insufficient permissions to access lead pages"
                        Exit Sub
                    End If

                    Dim fac As New UserSecurityFacade

                    fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New ArgumentException(ex.Message.ToString)
                End Try

                _mruProvider.InsertMRU(4, strLeadId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
                AdvantageSession.LeadMRU = Nothing
                If strRedirectUrl.Trim = "" Then
                    ShowNotificationWhenUserHasNoAccessToEntityPage("lead")
                    ' Dim mainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
                    '  Dim grdMru As RadGrid = DirectCast(DirectCast(mainPanelBar.FindItemByValue("Recently Viewed").Controls(0), RadPanelItem).Controls(3), RadGrid)
                    '  grdMru.ToolTip = "User has insufficient permissions to access lead pages"
                    Exit Sub
                End If

                HttpContext.Current.Response.Redirect(strRedirectUrl, False)

            Else
                RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            End If
        End Sub

        Public Sub BuildEmployerStateObject(employerId As String, employerName As String, Optional ByVal source As String = "")
            Dim objStateInfo As New AdvantageStateInfo
            Dim objGetStudentStatusBar As AdvantageStateInfo
            Dim strVid As String
            'Dim facInputMasks As New InputMasksFacade
            Dim strEmployerId = ""

            'Reset Employer Object Session Variables
            HttpContext.Current.Session("EmployerObjectPointer") = ""
            HttpContext.Current.Session("EmployerMRUFlag") = "false"

            If employerId.ToString.Trim = "" Then 'No Student was selected from MRU
                strEmployerId = _mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString,
                                                                       2,
                                                                       AdvantageSession.UserState.CampusId.ToString)
            Else
                strEmployerId = employerId
            End If

            If Not strEmployerId.Trim = "" Then
                objGetStudentStatusBar = _mruProvider.BuildEmployerStatusBar(strEmployerId)
                With objStateInfo
                    AdvantageSession.MasterEmployerId = strEmployerId
                    .EmployerId = objGetStudentStatusBar.EmployerId
                    .NameValue = objGetStudentStatusBar.NameValue
                    .Address1 = objGetStudentStatusBar.Address1
                    .Address2 = objGetStudentStatusBar.Address2
                    .City = objGetStudentStatusBar.City
                    .State = objGetStudentStatusBar.State
                    .Zip = objGetStudentStatusBar.Zip
                    .Phone = objGetStudentStatusBar.Phone
                    .CampusDescrip = objGetStudentStatusBar.CampusDescrip 'This property stores campus group description and not campus description
                    If source.Trim = "" AndAlso Not .CampusDescrip.ToLower.Trim = "all" Then
                        .CampusId = objGetStudentStatusBar.CampusId



                    Else
                        .CampusId = AdvantageSession.UserState.CampusId.ToString 'comes from campus selector


                    End If

                    HttpContext.Current.Session("hdnSearchCampusId") = .CampusId.ToString()
                    HttpContext.Current.Session("previousCampusId") = ""
                    HttpContext.Current.Session("MasterName1") = objStateInfo.NameValue

                End With

                'Create a new guid to be associated with the employer pages
                strVid = Guid.NewGuid.ToString



                HttpContext.Current.Session("EmployerObjectPointer") = strVid 'Needed as master page doesn't retain variable values when refreshed
                HttpContext.Current.Session("EmployerMRUFlag") = "true"

                'Add an entry to AdvantageSessionState for this guid and object
                'load Advantage state
                Dim state As AdvantageSessionState = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
                state(strVid) = objStateInfo
                'save current State
                CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

                HttpContext.Current.Response.Clear()

                'Balaji 02/23/2012
                'The MRU list may come from different campuses, so make sure the QueryString always contains the 
                'lead or student's default campus
                'Reset AdvantageSession User State Object's CampusId Property
                '   Reason to reset user state: All Pages in Advantage are getting the campusid from the AdvantageSession.UserState Object
                '   So resetting the object here will impact all pages
                Dim strRedirectUrl = ""
                Try
                    Dim advResetUserState As New User
                    advResetUserState = AdvantageSession.UserState
                    With advResetUserState
                        .CampusId = New Guid(objStateInfo.CampusId.ToString) 'New Guid(Request.QueryString("cmpid"))
                    End With
                    AdvantageSession.UserState = advResetUserState

                    strRedirectUrl = BuildRedirectUrlForEmployerPages(AdvantageSession.UserState.CampusId.ToString, strVid)
                    If strRedirectUrl.Trim = "" Then
                        ShowNotificationWhenUserHasNoAccessToEntityPage("employer")
                        '   Dim MainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
                        '    Dim grdMRU As RadGrid = DirectCast(DirectCast(MainPanelBar.FindItemByValue("Recently Viewed").Controls(0), RadPanelItem).Controls(3), RadGrid)
                        '    grdMRU.ToolTip = "User has insufficient permissions to access employer pages"
                        Exit Sub
                    End If

                    Dim fac As New UserSecurityFacade
                    fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)

                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New ArgumentException(ex.Message.ToString)
                End Try

                'UpdateMRUTable
                'Reason: We need to keep track of the last student record the user worked with
                'Scenario1 : User can log in and click on a student page without using MRU 
                'and we need to display the data of the last student the user worked with
                'If the user is a first time user, we will load the student who was last added to advantage
                _mruProvider.UpdateEmployerMRUList(strEmployerId, AdvantageSession.UserState.UserId.ToString,
                                            AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

                HttpContext.Current.Response.Redirect(strRedirectUrl, False)
            Else
                RedirectToEmployerSearchPage(AdvantageSession.UserState.CampusId.ToString)
            End If
        End Sub

        Public Sub BuildEmployeeStateObject(employeeId As String, employeeName As String)
            Dim objStateInfo As New AdvantageStateInfo
            Dim objGetStudentStatusBar As AdvantageStateInfo
            Dim strVid As String
            'Dim facInputMasks As New InputMasksFacade
            Dim strEmployeeId = ""

            HttpContext.Current.Session("EmployeeObjectPointer") = ""
            HttpContext.Current.Session("EmployeeMRUFlag") = "false"


            If employeeId.ToString.Trim = "" Then 'No Student was selected from MRU
                strEmployeeId = _mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, 3, AdvantageSession.UserState.CampusId.ToString)
            Else
                strEmployeeId = employeeId
            End If

            If Not strEmployeeId = "" Then
                objGetStudentStatusBar = _mruProvider.BuildEmployeeStatusBar(strEmployeeId)
                With objStateInfo
                    AdvantageSession.MasterEmployeeId = strEmployeeId
                    .EmployeeId = objGetStudentStatusBar.EmployeeId
                    .NameValue = objGetStudentStatusBar.NameValue
                    .Address1 = objGetStudentStatusBar.Address1
                    .Address2 = objGetStudentStatusBar.Address2
                    .City = objGetStudentStatusBar.City
                    .State = objGetStudentStatusBar.State
                    .Zip = objGetStudentStatusBar.Zip
                    .CampusId = objGetStudentStatusBar.CampusId


                    HttpContext.Current.Session("hdnSearchCampusId") = .CampusId
                    HttpContext.Current.Session("previousCampusId") = ""
                    HttpContext.Current.Session("MasterName1") = objStateInfo.NameValue



                End With

                'Create a new guid to be associated with the Employee pages
                strVid = Guid.NewGuid.ToString


                HttpContext.Current.Session("EmployeeObjectPointer") = strVid 'Needed as master page doesn't retain variable values when refreshed
                HttpContext.Current.Session("EmployeeMRUFlag") = "true"

                'Add an entry to AdvantageSessionState for this guid and object
                'load Advantage state
                Dim state As AdvantageSessionState = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
                state(strVid) = objStateInfo
                'save current State
                CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

                HttpContext.Current.Response.Clear()

                'Balaji 02/23/2012
                'The MRU list may come from different campuses, so make sure the QueryString always contains the 
                'lead or student's default campus
                'Reset AdvantageSession User State Object's CampusId Property
                '   Reason to reset user state: All Pages in Advantage are getting the campusid from the AdvantageSession.UserState Object
                '   So resetting the object here will impact all pages
                Dim strRedirectUrl As String
                Try
                    Dim advResetUserState As User
                    advResetUserState = AdvantageSession.UserState
                    With advResetUserState
                        .CampusId = New Guid(objStateInfo.CampusId.ToString) 'New Guid(Request.QueryString("cmpid"))
                    End With
                    AdvantageSession.UserState = advResetUserState

                    strRedirectUrl = BuildRedirectUrlForEmployeePages(AdvantageSession.UserState.CampusId.ToString, strVid)
                    'Session("Type") = "3"
                    If strRedirectUrl.Trim = "" Then
                        ShowNotificationWhenUserHasNoAccessToEntityPage("employee")
                        '  Dim mainPanelBar As RadPanelBar = CType(MainPane.FindControl("RadPanelBar1"), RadPanelBar)
                        '  Dim grdMru As RadGrid = DirectCast(DirectCast(mainPanelBar.FindItemByValue("Recently Viewed").Controls(0), RadPanelItem).Controls(3), RadGrid)
                        '  grdMru.ToolTip = "User has insufficient permissions to access employee pages"
                        Exit Sub
                    End If

                    Dim fac As New UserSecurityFacade
                    fac.UpdateUserDefaultCampus(AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString, False)

                    'UpdateMRUTable
                    'Reason: We need to keep track of the last student record the user worked with
                    'Scenario1 : User can log in and click on a student page without using MRU 
                    'and we need to display the data of the last student the user worked with
                    'If the user is a first time user, we will load the student who was last added to advantage
                    _mruProvider.InsertMRU(3, strEmployeeId, AdvantageSession.UserState.UserId.ToString,
                                                AdvantageSession.UserState.CampusId.ToString)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New ArgumentException(ex.Message.ToString)
                End Try
                'ReBuildEmployeeTabs(strVID)

                HttpContext.Current.Response.Redirect(strRedirectUrl, False)

            Else
                RedirectToEmployeeSearchPage(AdvantageSession.UserState.CampusId.ToString)
            End If
        End Sub

        Private Function BuildRedirectUrlForStudentPages(campusId As String, strVid As String) As String
            Dim resIdToRedirect As Integer
            Dim strRedirectUrl As String

            resIdToRedirect = CInt(HttpContext.Current.Request.QueryString("resid"))

            'If user doesn't have access to default page, don't redirect user to the page, return an empty string
            Dim pObj As UserPagePermissionInfo
            Select Case resIdToRedirect.ToString
                Case "90", "92", "95", "97", "112",
                                   "114", "116", "155", "159", "169",
                                   "175", "177", "200", "213",
                                   "230", "286", "301", "303", "327",
                                   "372", "374", "380", "479", "530",
                                   "531", "532", "534", "535", "538",
                                   "539", "541", "542", "543", "614",
                                   "625", "628", "652", "113", "334" 'Student Pages
                    pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, resIdToRedirect.ToString, campusId)
                    If pObj.HasNone = True Then
                        Return ""
                    End If
                    strRedirectUrl = _mruProvider.getURL(resIdToRedirect).ToString + "?resid=" + resIdToRedirect.ToString + "&mod=AR&cmpid=" + campusId + "&VID=" + strVid + "&Type=1" + "&Source=MRU"
                Case Else
                    'Student Info Page
                    If AdvantageSession.UserState.FullPermission = True Then
                        resIdToRedirect = 203
                        strRedirectUrl = _mruProvider.getURL(resIdToRedirect).ToString + "?resid=" + resIdToRedirect.ToString + "&mod=AR&cmpid=" + campusId + "&VID=" + strVid + "&Type=1" + "&Source=MRU"
                        Return strRedirectUrl
                    End If

                    Dim iResource As New List(Of String)(New String() {"203", "90", "92", "95", "97", "112",
                                   "114", "116", "155", "159", "169",
                                   "175", "177", "200", "213",
                                   "230", "286", "301", "303", "327",
                                   "372", "374", "380", "479", "530",
                                   "531", "532", "534", "535", "538",
                                   "539", "541", "542", "543", "614",
                                   "625", "628", "652", "113", "334"})
                    pObj = SecurityRoutines.CheckUserPermissionByCampusAndResourceForEntity(AdvantageSession.UserState, iResource, campusId)
                    If pObj.HasNone = True Then
                        Return ""
                        Exit Function
                    End If
                    strRedirectUrl = _mruProvider.getURL(pObj.ResourceId).ToString + "?resid=" + pObj.ResourceId.ToString + "&mod=AR&cmpid=" + campusId + "&VID=" + strVid + "&Type=1" + "&Source=MRU"
            End Select
            Return strRedirectUrl
        End Function

        Public Function BuildRedirectUrlForLeadPages(campusId As String, strVid As String) As String
            Dim resIdToRedirect As Integer
            Dim strRedirectUrl As String
            resIdToRedirect = CInt(HttpContext.Current.Request.QueryString("resid"))

            'If user doesn't have access to default page, don't redirect user to the page, return an empty string
            Dim pObj As UserPagePermissionInfo
            Select Case resIdToRedirect.ToString
                Case "145", "146", "147", "148", "174", "826", "838",
                              "225", "313", "456", "484" 'Lead Tabs
                    pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, resIdToRedirect.ToString, campusId)
                    If pObj.HasNone = True Then
                        Return ""
                    End If
                    strRedirectUrl = _mruProvider.getURL(resIdToRedirect).ToString + "?resid=" + resIdToRedirect.ToString + "&mod=AD&cmpid=" + campusId + "&VID=" + strVid + "&Type=4" + "&Source=MRU"
                Case Else
                    'Lead Info Page
                    'resIdToRedirect = 170
                    If AdvantageSession.UserState.FullPermission = True Then
                        resIdToRedirect = 170
                        strRedirectUrl = _mruProvider.getURL(resIdToRedirect).ToString + "?resid=" + resIdToRedirect.ToString + "&mod=AD&cmpid=" + campusId + "&VID=" + strVid + "&Type=4" + "&Source=MRU"
                        Return strRedirectUrl
                    End If
                    Dim iResource As New List(Of String)(New String() {"170", "145", "146", "147", "148", "225", "313", "456", "484"})
                    pObj = SecurityRoutines.CheckUserPermissionByCampusAndResourceForEntity(AdvantageSession.UserState, iResource, campusId)
                    If pObj.HasNone = True Then
                        Return ""
                    End If
                    strRedirectUrl = _mruProvider.getURL(pObj.ResourceId).ToString + "?resid=" + pObj.ResourceId.ToString + "&mod=AD&cmpid=" + campusId + "&VID=" + strVid + "&Type=4" + "&Source=MRU"
            End Select
            Return strRedirectUrl
        End Function

        Public Function BuildRedirectUrlForEmployerPages(campusId As String, strVid As String) As String
            Dim resIdToRedirect As Integer
            Dim strRedirectUrl As String
            resIdToRedirect = CInt(HttpContext.Current.Request.QueryString("resid"))
            'If user doesn't have access to default page, don't redirect user to the page, return an empty string
            Dim pObj As UserPagePermissionInfo
            Select Case resIdToRedirect.ToString
                Case "82", "86", "111"
                    pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, resIdToRedirect.ToString, campusId)
                    If pObj.HasNone = True Then
                        Return ""
                    End If
                    strRedirectUrl = _mruProvider.getURL(resIdToRedirect).ToString + "?resid=" + resIdToRedirect.ToString + "&mod=PL&cmpid=" + campusId + "&VID=" + strVid + "&Type=2" + "&Source=MRU"
                Case Else
                    'resIdToRedirect = 79
                    If AdvantageSession.UserState.FullPermission = True Then
                        resIdToRedirect = 79
                        strRedirectUrl = _mruProvider.getURL(resIdToRedirect).ToString + "?resid=" + resIdToRedirect.ToString + "&mod=PL&cmpid=" + campusId + "&VID=" + strVid + "&Type=2" + "&Source=MRU"
                        Return strRedirectUrl
                    End If
                    Dim iResource As New List(Of String)(New String() {"79", "82", "86", "111"})
                    pObj = SecurityRoutines.CheckUserPermissionByCampusAndResourceForEntity(AdvantageSession.UserState, iResource, campusId)
                    If pObj.HasNone = True Then
                        Return ""
                    End If
                    strRedirectUrl = _mruProvider.getURL(pObj.ResourceId).ToString + "?resid=" + pObj.ResourceId.ToString + "&mod=PL&cmpid=" + campusId + "&VID=" + strVid + "&Type=2" + "&Source=MRU"
            End Select
            Return strRedirectUrl
        End Function

        Private Function BuildRedirectUrlForEmployeePages(campusId As String, strVid As String) As String
            Dim resIdToRedirect As Integer
            Dim strRedirectUrl As String
            resIdToRedirect = CInt(HttpContext.Current.Request.QueryString("resid"))
            'If user doesn't have access to default page, don't redirect user to the page, return an empty string
            Dim pObj As New UserPagePermissionInfo
            Select Case resIdToRedirect.ToString
                Case "55", "69", "281"
                    strRedirectUrl = _mruProvider.getURL(resIdToRedirect).ToString + "?resid=" + resIdToRedirect.ToString + "&mod=HR&cmpid=" + campusId + "&VID=" + strVid + "&Type=3" + "&Source=MRU"
                    pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, resIdToRedirect.ToString, campusId)
                    If pObj.HasNone = True Then
                        Return ""
                    End If
                Case Else
                    'Employee Info Page
                    If AdvantageSession.UserState.FullPermission = True Then
                        resIdToRedirect = 52
                        strRedirectUrl = _mruProvider.getURL(resIdToRedirect).ToString + "?resid=" + resIdToRedirect.ToString + "&mod=HR&cmpid=" + campusId + "&VID=" + strVid + "&Type=3" + "&Source=MRU"
                        Return strRedirectUrl
                    End If
                    Dim iResource As New List(Of String)(New String() {"52", "55", "69", "281"})
                    pObj = SecurityRoutines.CheckUserPermissionByCampusAndResourceForEntity(AdvantageSession.UserState, iResource, campusId)
                    If pObj.HasNone = True Then
                        Return ""
                    End If
                    strRedirectUrl = _mruProvider.getURL(pObj.ResourceId).ToString + "?resid=" + pObj.ResourceId.ToString + "&mod=HR&cmpid=" + campusId + "&VID=" + strVid + "&Type=3" + "&Source=MRU"
            End Select
            Return strRedirectUrl
        End Function

        Public Function GetLastEntityFromStateObject(currenCampusId As String, murTypeId As Integer) As String
            'student=1,employer=2,employee=3,lead=4
            Dim myAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
            Return _mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, murTypeId, currenCampusId)
        End Function





        Public Sub RedirectToStudentSearchPage(modulecode As String, campusId As String)
            Dim strSearchUrl As String
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + modulecode + "&cmpid=" + campusId + "&desc=View Existing Students"
            HttpContext.Current.Response.Redirect(strSearchUrl, False)
        End Sub
        Public Sub RedirectToLeadSearchPage(campusId As String)
            Dim strSearchUrl As String
            strSearchUrl = "~/PL/SearchLead.aspx?resid=153&mod=AD&cmpid=" + campusId + "&desc=View Existing Leads"
            HttpContext.Current.Response.Redirect(strSearchUrl, False)
        End Sub

        Public Sub RedirectToEmployerSearchPage(campusId As String)
            Dim strSearchUrl As String
            strSearchUrl = "~/PL/EmployerInfo.aspx?resid=79&mod=PL&cmpid=" + campusId + "&desc=View Existing Employers"
            HttpContext.Current.Response.Redirect(strSearchUrl, False)
        End Sub

        Public Sub RedirectToEmployeeSearchPage(campusId As String)
            Dim strSearchUrl As String
            strSearchUrl = "~/SY/EmployeeInfo.aspx?resid=52&mod=HR&cmpid=" + campusId + "&desc=View Existing Employees"
            HttpContext.Current.Response.Redirect(strSearchUrl, False)
        End Sub

        Private Sub ShowNotificationWhenUserHasNoAccessToEntityPage(entity As String)
            _radNotification1.Text = "User has insufficient permission to access " & entity & " pages"
            _radNotification1.Show()
        End Sub


        Private Function GetAdvAppSettings() As AdvAppSettings
            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If
            Return myAdvAppSettings
        End Function
    End Class
End Namespace