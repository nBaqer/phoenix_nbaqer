﻿
Imports System.Runtime.CompilerServices

Public Module StringExtensions

    ' Convert the string to Pascal case.
    <Extension()>
    Public Function ToPascalCase(ByVal the_string As String) As String
        ' If there are 0 or 1 characters, just return the string.
        If (the_string Is Nothing) Then Return the_string
        If (the_string.Length < 2) Then Return the_string.ToUpper()

        ' Split the string into words.
        Dim words() As String = the_string.Split(New Char() {}, StringSplitOptions.RemoveEmptyEntries)

        ' Combine the words.
        Dim result As String = ""
        For Each word As String In words
            result &= word.Substring(0, 1).ToUpper() & word.Substring(1)
        Next word

        Return result
    End Function

    ' Convert the string to camel case.
    <Extension()>
    Public Function ToCamelCase(ByVal the_string As String) As String
        ' If there are 0 or 1 characters, just return the string.
        If (the_string Is Nothing OrElse the_string.Length < 2) Then Return the_string

        ' Split the string into words.
        Dim words() As String = the_string.Split(New Char() {}, StringSplitOptions.RemoveEmptyEntries)

        ' Combine the words.
        Dim result As String = words(0).ToLower()
        For i As Integer = 1 To words.Length - 1
            result &= words(i).Substring(0, 1).ToUpper() & words(i).Substring(1)
        Next i

        Return result
    End Function

    ' Capitalize the first character and add a space before
    ' each capitalized letter (except the first character).
    <Extension()>
    Public Function ToProperCase(ByVal the_string As String) As String
        ' If there are 0 or 1 characters, just return the
        ' string.
        If (the_string Is Nothing) Then Return the_string
        If (the_string.Length < 2) Then Return the_string.ToUpper()

        ' Start with the first character.
        Dim result As String = the_string.Substring(0, 1).ToUpper()

        ' Add the remaining characters.
        For i As Integer = 1 To the_string.Length - 1
            If (Char.IsUpper(the_string(i))) Then result &= " "
            result &= the_string(i)
        Next i

        Return result
    End Function

End Module
