﻿
Imports System
Public Class TestEventArgs
    Inherits EventArgs
    Private _Test1 As String
    Private _Test2 As Integer
    Public Property Test2() As Integer
        Get
            Return _Test2
        End Get
        Set(value As Integer)
            _Test2 = Value
        End Set
    End Property

    Public Property Test1() As String
        Get
            Return _Test1
        End Get
        Set(value As String)
            _Test1 = Value
        End Set
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="t1"> </param>
    ''' <param name="t2"></param>
    ''' <remarks></remarks>
    Public Sub New(t1 As String, t2 As Integer)
        _Test1 = t1
        _Test2 = t2
    End Sub

    Public Delegate Sub TestEventHandler(sender As Object, e As TestEventArgs)
End Class

