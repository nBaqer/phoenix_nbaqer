﻿Imports Microsoft.VisualBasic
Imports System
Imports System.IO
Imports System.Web
Imports System.Net
Imports System.Collections.Generic
Imports FluentNHibernate.Diagnostics
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports Telerik.Web.UI
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports System.Collections
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.Security.Principal
Imports System.Security.Permissions


Public Class Impersonate
#Region "Private Vars"
    Private WIC_RemoteAccess As WindowsImpersonationContext = Nothing
#End Region

#Region "RemoteConnection"
    <DllImport("advapi32.DLL", SetLastError:=True)> Public Shared Function LogonUser(ByVal lpszUsername As String, ByVal lpszDomain As String,
                                   ByVal lpszPassword As String, ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, ByRef phToken As IntPtr) As Integer

    End Function

    Public Function RemoteAccess_LogOnAndImpersonate(ByVal sDomain As String, ByVal sUsername As String, ByVal RemotePassword As String) As Boolean


        Dim admin_token As IntPtr
        Dim blogged As Boolean = False

        blogged = False

        Dim wid_current As WindowsIdentity = WindowsIdentity.GetCurrent()

        Dim wid_admin As WindowsIdentity = Nothing

        Try

            If LogonUser(sUsername, sDomain, RemotePassword, 9, 0, admin_token) <> 0 Then

                wid_admin = New WindowsIdentity(admin_token)

                WIC_RemoteAccess = wid_admin.Impersonate()

                blogged = True
            End If

        Catch se As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(se)


            'Dim ret As Integer = Marshal.GetLastWin32Error()

        End Try
        Return blogged

    End Function

    Public Sub RemoteAccess_UndoImpersonate()
        Try
            If WIC_RemoteAccess IsNot Nothing Then
                WIC_RemoteAccess.Undo()
            End If
        Catch
        End Try
    End Sub

    Public Function GetDomainFromDomainAndUserName(ByVal strDomainAndUserName As String) As String
        Dim strDomain As String

        'Domain and user name passed as domain\username
        strDomain = Left(strDomainAndUserName, strDomainAndUserName.IndexOf("\"))

        Return strDomain
    End Function

    Public Function GetUserNameFromDomainAndUserName(ByVal strDomainAndUserName As String) As String
        Dim strUserName As String

        'Domain and user name passed as domain\username
        strUserName = Mid(strDomainAndUserName, strDomainAndUserName.IndexOf("\") + 2)

        Return strUserName
    End Function

#End Region
End Class
