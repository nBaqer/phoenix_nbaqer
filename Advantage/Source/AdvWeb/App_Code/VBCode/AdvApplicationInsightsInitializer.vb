﻿Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.Advantage.Site.Lib.Infrastruct.Initializer
Imports Microsoft.ApplicationInsights.Extensibility
Imports Microsoft.ApplicationInsights
Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationInsights.DataContracts

Public Class AdvApplicationInsightsInitializer

    Public Sub SetProperties(ByVal httpContext As HttpContext)
        Dim requestTelemetry = httpContext.GetRequestTelemetry()
        SetTelemetryContextProperties(httpContext, requestTelemetry)
    End Sub

    Public Sub SetTelemetryContextProperties(ByVal httpContext As HttpContext, ByVal requestTelemetry As RequestTelemetry)

        Dim TenantUser As User
        Dim AdvSettings As AdvAppSettings
        Dim telemetryProperties As ISupportProperties = requestTelemetry

        If (requestTelemetry IsNot Nothing AndAlso requestTelemetry.Context IsNot Nothing) Then

            If (httpContext.Session IsNot Nothing) Then

                'Session ID'
                If (httpContext.Session.SessionID IsNot Nothing) Then
                    requestTelemetry.Context.Session.Id = httpContext.Session.SessionID
                End If

                If (httpContext.Session(AdvantageSession.USERSTATE1) IsNot Nothing) Then
                    TenantUser = CType(httpContext.Session(AdvantageSession.USERSTATE1), User)
                End If

                If (httpContext.Session("AdvAppSettings") IsNot Nothing) Then
                    AdvSettings = DirectCast(httpContext.Session("AdvAppSettings"), AdvAppSettings)
                End If

                If (TenantUser IsNot Nothing) Then

                    'User ID'
                    If (Not TenantUser.UserId.Equals(Guid.Empty)) Then
                        requestTelemetry.Context.User.Id = TenantUser.UserId.ToString()
                        requestTelemetry.Context.User.AuthenticatedUserId = TenantUser.UserId.ToString()
                    End If

                    If (Not telemetryProperties.Properties.ContainsKey("tenant_UserId")) Then
                        requestTelemetry.Properties.Add("tenant_UserId", If(TenantUser.UserId.ToString(), ""))
                    Else
                        telemetryProperties.Properties("tenant_UserId") = If(TenantUser.UserId.ToString(), "")
                    End If

                    If (Not telemetryProperties.Properties.ContainsKey("tenant_UserName")) Then
                        telemetryProperties.Properties.Add("tenant_UserName", If(TenantUser.UserName.ToString(), ""))
                    Else
                        telemetryProperties.Properties("tenant_UserName") = If(TenantUser.UserName.ToString(), "")
                    End If

                    If (Not telemetryProperties.Properties.ContainsKey("tenant_IsSupport")) Then
                        telemetryProperties.Properties.Add("tenant_IsSupport", If(TenantUser.IsAdvantageSuperUser.ToString(), ""))
                    Else
                        telemetryProperties.Properties("tenant_IsSupport") = If(TenantUser.IsAdvantageSuperUser.ToString(), "")
                    End If

                    If (Not telemetryProperties.Properties.ContainsKey("tenant_CampusId")) Then
                        telemetryProperties.Properties.Add("tenant_CampusId", If(TenantUser.CampusId.ToString(), ""))
                    Else
                        telemetryProperties.Properties("tenant_CampusId") = If(TenantUser.CampusId.ToString(), "")
                    End If

                    If (Not telemetryProperties.Properties.ContainsKey("Advantage_App")) Then
                        telemetryProperties.Properties.Add("Advantage_App", "Site")
                    Else
                        telemetryProperties.Properties("Advantage_App") = "Site"
                    End If
                End If

                If (Not AdvSettings Is Nothing) Then

                    If (Not telemetryProperties.Properties.ContainsKey("tenant_Name")) Then
                        If (Not AdvSettings.AppSettings("SchoolName") Is Nothing) Then
                            telemetryProperties.Properties.Add("tenant_Name", If(AdvSettings.AppSettings("SchoolName").ToString, ""))
                        End If
                    Else
                        If (Not AdvSettings.AppSettings("SchoolName") Is Nothing) Then
                            telemetryProperties.Properties("tenant_Name") = If(AdvSettings.AppSettings("SchoolName").ToString, "")
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Public Sub TrackExceptionWrapper(ByVal exception As Exception, Optional ByVal httpContext As HttpContext = Nothing)
        Try
            If (httpContext Is Nothing) Then
                httpContext = HttpContext.Current
            End If
            Dim aiClient = New TelemetryClient()
            Dim exceptionTelemetry = New ExceptionTelemetry(exception)
            Dim properties As ISupportProperties = exceptionTelemetry
            SetTelemetryContextProperties(httpContext, properties)
            exceptionTelemetry = properties
            aiClient.TrackException(exceptionTelemetry)
        Catch ex As Exception
            '_log.Info("END Application_Start")
        End Try
    End Sub
End Class
