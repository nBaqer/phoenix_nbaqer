﻿Imports System.Web
Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.Common.Tables
Imports BO = Advantage.Business.Objects
Imports System.Collections.Generic

''' <summary>
''' AdvantageSession provides a facade to the ASP.NET Session object.
'''All access to Session variables must be through this class.
''' </summary>
''' <remarks></remarks>
Public NotInheritable Class AdvantageSession
    Private Const PAGE_THEME1 As String = "PageTheme"
    public Const USERSTATE1 As String = "UserState"
    Private Const USERNAME1 As String = "UserName"
    Private Shared ReadOnly userId1 As String = "UserId"
    Private Shared ReadOnly buildVersion1 As String = "BuildVersion"
    Private Shared ReadOnly commontaskmenuitem As String = "CommonTaskMenuAndTabItems"
    Private Shared ReadOnly reportmenuitems1 As String = "ReportMenuItems"
    Private Shared ReadOnly maintenancemenuitemsp As String = "MaintenanceMenuItems"
    Private Shared ReadOnly studenttabitemsp As String = "StudentTabItems"
    Private Shared ReadOnly leadtabitems As String = "LeadTabItems"
    Private Shared ReadOnly employertabitems As String = "EmployerTabItems"
    Private Shared ReadOnly modulenavigation1 As String = "ModuleNavigation"
    Private Shared ReadOnly commontask As String = "CommonTaskNavigation"
    Private Shared ReadOnly maintenance As String = "MaintenanceForAllModules"
    Private Shared ReadOnly tabmenuitems As String = "TabMenus"
    Private Shared ReadOnly toolmenuitems As String = "ToolsMenu"
    Private Shared ReadOnly pageBreadCrumb1 As String = "PageBreadCrumb"
    Private Shared ReadOnly studentMru1 As String = "StudentMRU"
    Private Shared ReadOnly leadMru1 As String = "LeadMRU"
    Private Shared ReadOnly employerMru1 As String = "EmployerMRU"
    Private Shared ReadOnly employeeMru1 As String = "EmployeeMRU"
    Private Shared ReadOnly refreshListCampusp As String = "RefreshListCampus"
    Private Shared ReadOnly showNotificationSwitchingCampusp As String = "showNotificationSwitchingCampusp"
    Private Shared ReadOnly enableCampusComboBoxp As String = "EnableCampusComboBox"
    Private Shared ReadOnly temporalPreviousCampusIdp As String = "TemporalPreviousCampusId"
    Private Shared ReadOnly _masterStudentId As String = "MasterStudentId"
    Private Shared ReadOnly _masterLeadId As String = "MasterLeadId"
    Private Shared ReadOnly _masterEmployerId As String = "MasterEmployerId"
    Private Shared ReadOnly _masterEmployeeId As String = "MasterEmployeeId"
    Private Shared ReadOnly _masterName As String = "MasterName"
    Private Shared ReadOnly _masterEmpAddress1 As String = "MasterEmpAddress1"
    Private Shared ReadOnly _masterEmpAddress2 As String = "MasterEmpAddress2"
    Private Shared ReadOnly _masterEmpCity As String = "MasterEmpCity"
    Private Shared ReadOnly _masterEmpState As String = "MasterEmpState"
    Private Shared ReadOnly _masterEmpZip As String = "MasterEmpZip"
    Private Shared ReadOnly _masterEmpName As String = "MasterEmpName"
    Private Shared ReadOnly _masterEmpPhone As String = "MasterEmpPhone"
    Private Shared ReadOnly _masterEmployeeAddress1 As String = "MasterEmployeeAddress1"
    Private Shared ReadOnly _masterEmployeeAddress2 As String = "MasterEmployeeAddress2"
    Private Shared ReadOnly _masterEmployeeCity As String = "MasterEmployeeCity"
    Private Shared ReadOnly _masterEmployeeState As String = "MasterEmployeeState"
    Private Shared ReadOnly _masterEmployeeZip As String = "MasterEmployeeZip"
    Private Shared ReadOnly _masterEmployeeName As String = "MasterEmployeeName"
    Private Shared ReadOnly _isImpersonating As String = "IsImpersonating"
    Private Shared ReadOnly _ImpersonatedUserId As String = "ImpersonatedUserId"
    Private Shared ReadOnly _ImpersonatedUserName As String = "ImpersonatedUserName"
    Private Shared ReadOnly _ImpersonatedUserAndName As String = "ImpersonatedUserAndName"
    Private Shared ReadOnly _nonImpersonateUserId As String = "NonImpersonateUserId"
    Private Shared ReadOnly _userImpersonationLogId As String = "UserImpersonationLogId"
    Private Shared ReadOnly _taskRecipientId As String = "TaskRecipientId"
    Private Shared ReadOnly _taskRecipientTypeId As String = "TaskRecipienTypeId"
    Private Shared ReadOnly _taskRecipientFullName As String = "TaskRecipienFullName"
    Private Shared ReadOnly _taskRecipientModule As String = "TaskRecipientModule"
    Private Const TREADYTOTRANSFERLIST As String = "TREADYTOTRANSFERLIST"
    Private Const TTRANSFERBYCOPYLIST As String = "TTRANSFERBYCOPYLIST"
    Private Const TREGISTERBYCOPYLIST As String = "TREGISTERBYCOPYLIST"
    Private Const TWEIRDTHINGLIST As String = "TWEIRDTHINGLIST"

#Region "Sessions variables for TransferGrades.aspx"

    ''' <summary>
    ''' List of ready to transfer requirements
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property TpReadyToTransferList() As IList(Of TransferRequirement)
        Get
            If HttpContext.Current.Session(TREADYTOTRANSFERLIST) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(TREADYTOTRANSFERLIST), IList(Of TransferRequirement))
            End If
        End Get
        Set(ByVal value As IList(Of TransferRequirement))
            HttpContext.Current.Session(TREADYTOTRANSFERLIST) = value
        End Set
    End Property

    ''' <summary>
    ''' List of one requirement transfer to one enrollment but not to others
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property TpTransferByCopyList() As IList(Of ArTransferGrades)
        Get
            If HttpContext.Current.Session(TTRANSFERBYCOPYLIST) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(TTRANSFERBYCOPYLIST), IList(Of ArTransferGrades))
            End If
        End Get
        Set(ByVal value As IList(Of ArTransferGrades))
            HttpContext.Current.Session(TTRANSFERBYCOPYLIST) = value
        End Set
    End Property

    ''' <summary>
    ''' List of requirements that was graded in one enrollment but not in the other
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property TpRegisterByCopyList() As IList(Of ArResultsClass)
        Get
            If HttpContext.Current.Session(TREGISTERBYCOPYLIST) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(TREGISTERBYCOPYLIST), IList(Of ArResultsClass))
            End If
        End Get
        Set(ByVal value As IList(Of ArResultsClass))
            HttpContext.Current.Session(TREGISTERBYCOPYLIST) = value
        End Set
    End Property

    ''' <summary>
    ''' Weird things. Register in several, scheduled, etc. A message field give the why. 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property TpWeirdThingList() As IList(Of TransferRequirement)
        Get
            If HttpContext.Current.Session(TWEIRDTHINGLIST) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(TWEIRDTHINGLIST), IList(Of TransferRequirement))
            End If
        End Get
        Set(ByVal value As IList(Of TransferRequirement))
            HttpContext.Current.Session(TWEIRDTHINGLIST) = value
        End Set
    End Property


    '----------------------------------------------------------------------------------------------------------------------
#End Region

    Public Shared Property TemporalPreviousCampusId() As String
        Get
            If HttpContext.Current.Session(temporalPreviousCampusIdp) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(temporalPreviousCampusIdp), String)
            End If
        End Get
        Set(ByVal value As String)
            HttpContext.Current.Session(temporalPreviousCampusIdp) = value
        End Set
    End Property



    Public Shared Property ShowNotificationSwitchingCampus() As String
        Get
            If HttpContext.Current.Session(showNotificationSwitchingCampusp) Is Nothing Then
                Return "0, "
            Else
                Return HttpContext.Current.Session(showNotificationSwitchingCampusp)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(showNotificationSwitchingCampusp) = value
        End Set
    End Property

    'Remark: Used in MasterPageComboBox...
    Public Shared Property UserId() As String
        Get
            If HttpContext.Current.Session(userId1) Is Nothing Then
                Return BasePage.PageTheme.None
            Else
                Return HttpContext.Current.Session(userId1)
            End If
            'Return _PageTheme
        End Get
        Set(value As String)
            HttpContext.Current.Session(userId1) = value
        End Set
    End Property

    ''' <summary>
    ''' This property control the enable disable combobox in Master Page
    ''' </summary>
    ''' <value>true false boolean</value>
    ''' <returns></returns>
    ''' <remarks>This is used in CampusAddress.aspx.vb</remarks>
    Public Shared Property EnableCampusComboBox() As Boolean
        Get
            If HttpContext.Current.Session(enableCampusComboBoxp) Is Nothing Then
                Return True
            Else
                Return HttpContext.Current.Session(enableCampusComboBoxp)
            End If
            'Return _PageTheme
        End Get
        Set(value As Boolean)
            HttpContext.Current.Session(enableCampusComboBoxp) = value
        End Set
    End Property

    'Remark: Used in MasterPageComboBox...
    Public Shared Property BuildVersion() As String
        Get
            If HttpContext.Current.Session(buildVersion1) Is Nothing Then
                Return BasePage.PageTheme.None
            Else
                Return HttpContext.Current.Session(buildVersion1)
            End If
            'Return _PageTheme
        End Get
        Set(value As String)
            HttpContext.Current.Session(buildVersion1) = value
        End Set
    End Property

    Public Shared Property PageTheme() As Integer
        Get
            If HttpContext.Current.Session(PAGE_THEME1) Is Nothing Then
                Return BasePage.PageTheme.None
            Else
                Return HttpContext.Current.Session(PAGE_THEME1)
            End If
            'Return _PageTheme
        End Get
        Set(value As Integer)
            HttpContext.Current.Session(PAGE_THEME1) = value
        End Set
    End Property

    ''' <summary>
    ''' This Property will hold the state of the user such as userid, default campus id, default module code,etc.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    <CLSCompliant(False)>
    Public Shared Property UserState As User
        Get
            Return CType(HttpContext.Current.Session(USERSTATE1), User)

            'If HttpContext.Current.Session(userstate1) Is Nothing Then
            '    Return Nothing
            'Else
            '    Return CType(HttpContext.Current.Session(userstate1), User)
            'End If
        End Get
        Set(ByVal value As User)
            HttpContext.Current.Session(USERSTATE1) = value
        End Set
    End Property
    Public Shared Property UserName() As String
        Get
            If HttpContext.Current.Session(USERNAME1) Is Nothing Then
                Return Nothing
            Else
                Return CType(HttpContext.Current.Session(USERNAME1), String)
            End If
        End Get
        Set(ByVal value As String)
            HttpContext.Current.Session(USERNAME1) = value
        End Set
    End Property
    Public Shared Property CommonTaskMenuItems() As List(Of BO.CommonTaskMenuItem)
        Get
            If HttpContext.Current.Session(commontaskmenuitem) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(commontaskmenuitem)
            End If
        End Get
        Set(ByVal value As List(Of BO.CommonTaskMenuItem))
            HttpContext.Current.Session(commontaskmenuitem) = value
        End Set
    End Property
    Public Shared Property ReportMenuItems() As List(Of BO.CommonTaskMenuItem)
        Get
            If HttpContext.Current.Session(reportmenuitems1) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(reportmenuitems1)
            End If
        End Get
        Set(ByVal value As List(Of BO.CommonTaskMenuItem))
            HttpContext.Current.Session(reportmenuitems1) = value
        End Set
    End Property
    Public Shared Property MaintenanceMenuItems() As List(Of BO.CommonTaskMenuItem)
        Get
            If HttpContext.Current.Session(maintenancemenuitemsp) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(maintenancemenuitemsp)
            End If
        End Get
        Set(ByVal value As List(Of BO.CommonTaskMenuItem))
            HttpContext.Current.Session(maintenancemenuitemsp) = value
        End Set
    End Property
    Public Shared Property StudentTabsMenuItems() As List(Of BO.CommonTaskMenuItem)
        Get
            If HttpContext.Current.Session(studenttabitemsp) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(studenttabitemsp)
            End If
        End Get
        Set(ByVal value As List(Of BO.CommonTaskMenuItem))
            HttpContext.Current.Session(studenttabitemsp) = value
        End Set
    End Property
    Public Shared Property LeadTabsMenuItems() As List(Of BO.CommonTaskMenuItem)
        Get
            If HttpContext.Current.Session(leadtabitems) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(leadtabitems)
            End If
        End Get
        Set(ByVal value As List(Of BO.CommonTaskMenuItem))
            HttpContext.Current.Session(leadtabitems) = value
        End Set
    End Property
    Public Shared Property EmployerTabsMenuItems() As List(Of BO.CommonTaskMenuItem)
        Get
            If HttpContext.Current.Session(employertabitems) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(employertabitems)
            End If
        End Get
        Set(ByVal value As List(Of BO.CommonTaskMenuItem))
            HttpContext.Current.Session(employertabitems) = value
        End Set
    End Property
    Public Shared Property ModuleNavigation() As List(Of BO.CommonTaskMenuItem)
        Get
            If HttpContext.Current.Session(modulenavigation1) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(modulenavigation1)
            End If
        End Get
        Set(ByVal value As List(Of BO.CommonTaskMenuItem))
            HttpContext.Current.Session(modulenavigation1) = value
        End Set
    End Property
    Public Shared Property CommonTaskNavigation() As List(Of BO.CommonTaskMenuItem)
        Get
            If HttpContext.Current.Session(commontask) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(commontask)
            End If
        End Get
        Set(ByVal value As List(Of BO.CommonTaskMenuItem))
            HttpContext.Current.Session(commontask) = value
        End Set
    End Property
    Public Shared Property MaintenanceForAllModules() As List(Of BO.CommonTaskMenuItem)
        Get
            If HttpContext.Current.Session(maintenance) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(maintenance)
            End If
        End Get
        Set(ByVal value As List(Of BO.CommonTaskMenuItem))
            HttpContext.Current.Session(maintenance) = value
        End Set
    End Property
    Public Shared Property TabMenus() As List(Of BO.CommonTaskMenuItem)
        Get
            If HttpContext.Current.Session(tabmenuitems) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(tabmenuitems)
            End If
        End Get
        Set(ByVal value As List(Of BO.CommonTaskMenuItem))
            HttpContext.Current.Session(tabmenuitems) = value
        End Set
    End Property
    Public Shared Property ToolsMenu() As List(Of BO.CommonTaskMenuItem)
        Get
            If HttpContext.Current.Session(toolmenuitems) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(toolmenuitems)
            End If
        End Get
        Set(ByVal value As List(Of BO.CommonTaskMenuItem))
            HttpContext.Current.Session(toolmenuitems) = value
        End Set
    End Property
    Public Shared Property PageBreadCrumb() As String
        Get
            If HttpContext.Current.Session(pageBreadCrumb1) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(pageBreadCrumb1)
            End If
        End Get
        Set(ByVal value As String)
            HttpContext.Current.Session(pageBreadCrumb1) = value
        End Set
    End Property
    Public Shared Property StudentMRU() As List(Of BO.StudentMRU)
        Get
            If HttpContext.Current.Session(studentMru1) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(studentMru1)
            End If
        End Get
        Set(value As List(Of BO.StudentMRU))
            HttpContext.Current.Session(studentMru1) = value
        End Set
    End Property
    Public Shared Property LeadMRU() As List(Of BO.LeadMRU)
        Get
            If HttpContext.Current.Session(studentMru1) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(leadMru1)
            End If
        End Get
        Set(value As List(Of BO.LeadMRU))
            HttpContext.Current.Session(leadMru1) = value
        End Set
    End Property
    Public Shared Property EmployerMRU() As List(Of BO.EmployerMRU)
        Get
            If HttpContext.Current.Session(employerMru1) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(employerMru1)
            End If
        End Get
        Set(value As List(Of BO.EmployerMRU))
            HttpContext.Current.Session(employerMru1) = value
        End Set
    End Property
    Public Shared Property EmployeeMRU() As List(Of BO.EmployeeMRU)
        Get
            If HttpContext.Current.Session(employeeMru1) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(employeeMru1)
            End If
        End Get
        Set(value As List(Of BO.EmployeeMRU))
            HttpContext.Current.Session(employeeMru1) = value
        End Set
    End Property

    ''' <summary>
    ''' This reflect changes in the campus List. If true campuslist should be refreshed.
    ''' </summary>
    ''' <returns>true if you need to refresh the lsit</returns>
    ''' <remarks>normal status false.</remarks>
    Public Shared Property RefreshListCampus() As Boolean
        Get
            If HttpContext.Current.Session(refreshListCampusp) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(refreshListCampusp)
            End If
        End Get
        Set(value As Boolean)
            HttpContext.Current.Session(refreshListCampusp) = value
        End Set
    End Property


    ''' <summary>
    ''' Student Id of last student accessed.  Used by Status bar,
    ''' MRU, and student pages
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property MasterStudentId() As String
        Get
            If HttpContext.Current.Session(_masterStudentId) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterStudentId)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterStudentId) = value
        End Set
    End Property

    ''' <summary>
    ''' Lead Id of last lead accessed
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property MasterLeadId() As String
        Get
            If HttpContext.Current.Session(_masterLeadId) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterLeadId)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterLeadId) = value
        End Set
    End Property

    ''' <summary>
    ''' Employer id of last employer accessed
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property MasterEmployerId() As String
        Get
            If HttpContext.Current.Session(_masterEmployerId) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmployerId)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmployerId) = value
        End Set
    End Property


    ''' <summary>
    ''' EmployeeId of last employee accessed
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property MasterEmployeeId() As String
        Get
            If HttpContext.Current.Session(_masterEmployeeId) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmployeeId)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmployeeId) = value
        End Set
    End Property

    ''' <summary>
    ''' name of student
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Property MasterName() As String
        Get
            If HttpContext.Current.Session(_masterName) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterName)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterName) = value
        End Set
    End Property


    Public Shared Property MasterEmpAddress1() As String
        Get
            If HttpContext.Current.Session(_masterEmpAddress1) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmpAddress1)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmpAddress1) = value
        End Set
    End Property


    Public Shared Property MasterEmpAddress2() As String
        Get
            If HttpContext.Current.Session(_masterEmpAddress2) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmpAddress2)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmpAddress2) = value
        End Set
    End Property


    Public Shared Property MasterEmpCity() As String
        Get
            If HttpContext.Current.Session() Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmpCity)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmpCity) = value
        End Set
    End Property

    Public Shared Property MasterEmpState() As String
        Get
            If HttpContext.Current.Session(_masterEmpState) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmpState)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmpState) = value
        End Set
    End Property


    Public Shared Property MasterEmpZip() As String
        Get
            If HttpContext.Current.Session(_masterEmpZip) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmpZip)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmpZip) = value
        End Set
    End Property

    Public Shared Property MasterEmpName() As String
        Get
            If HttpContext.Current.Session(_masterEmpName) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmpName)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmpName) = value
        End Set
    End Property

    Public Shared Property MasterEmpPhone() As String
        Get
            If HttpContext.Current.Session(_masterEmpPhone) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmpPhone)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmpPhone) = value
        End Set
    End Property

    Public Shared Property MasterEmployeeAddress1() As String
        Get
            If HttpContext.Current.Session(_masterEmployeeAddress1) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmployeeAddress1)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmployeeAddress1) = value
        End Set
    End Property


    Public Shared Property MasterEmployeeAddress2() As String
        Get
            If HttpContext.Current.Session(_masterEmployeeAddress2) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmployeeAddress2)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmployeeAddress2) = value
        End Set
    End Property


    Public Shared Property MasterEmployeeCity() As String
        Get
            If HttpContext.Current.Session() Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmployeeCity)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmployeeCity) = value
        End Set
    End Property

    Public Shared Property MasterEmployeeState() As String
        Get
            If HttpContext.Current.Session(_masterEmployeeState) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmployeeState)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmployeeState) = value
        End Set
    End Property


    Public Shared Property MasterEmployeeZip() As String
        Get
            If HttpContext.Current.Session(_masterEmployeeZip) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmployeeZip)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmployeeZip) = value
        End Set
    End Property

    Public Shared Property MasterEmployeeName() As String
        Get
            If HttpContext.Current.Session(_masterEmployeeName) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_masterEmployeeName)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_masterEmployeeName) = value
        End Set
    End Property

    Public Shared Property IsImpersonating() As String
        Get
            If HttpContext.Current.Session(_isImpersonating) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_isImpersonating)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_isImpersonating) = value
        End Set
    End Property

    Public Shared Property ImpersonatedUserId() As String
        Get
            If HttpContext.Current.Session(_ImpersonatedUserId) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_ImpersonatedUserId)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_ImpersonatedUserId) = value
        End Set
    End Property

    Public Shared Property ImpersonatedUserName() As String
        Get
            If HttpContext.Current.Session(_ImpersonatedUserName) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_ImpersonatedUserName)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_ImpersonatedUserName) = value
        End Set
    End Property

    Public Shared Property ImpersonatedUserAndName() As String
        Get
            If HttpContext.Current.Session(_ImpersonatedUserAndName) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_ImpersonatedUserAndName)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_ImpersonatedUserAndName) = value
        End Set
    End Property

    Public Shared Property NonImpersonateUserId() As String
        Get
            If HttpContext.Current.Session(_nonImpersonateUserId) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_nonImpersonateUserId)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_nonImpersonateUserId) = value
        End Set
    End Property

    Public Shared Property UserImpersonationLogId() As String
        Get
            If HttpContext.Current.Session(_userImpersonationLogId) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_userImpersonationLogId)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_userImpersonationLogId) = value
        End Set
    End Property

    Public Shared Property TaskRecipientId() As String
        Get
            If HttpContext.Current.Session(_taskRecipientId) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_taskRecipientId)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_taskRecipientId) = value
        End Set
    End Property

    Public Shared Property TaskRecipientTypeId() As String
        Get
            If HttpContext.Current.Session(_taskRecipientTypeId) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_taskRecipientTypeId)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_taskRecipientTypeId) = value
        End Set
    End Property

    Public Shared Property TaskRecipientFullName() As String
        Get
            If HttpContext.Current.Session(_taskRecipientFullName) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_taskRecipientFullName)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_taskRecipientFullName) = value
        End Set
    End Property

    Public Shared Property TaskRecipientModule() As String
        Get
            If HttpContext.Current.Session(_taskRecipientModule) Is Nothing Then
                Return Nothing
            Else
                Return HttpContext.Current.Session(_taskRecipientModule)
            End If
        End Get
        Set(value As String)
            HttpContext.Current.Session(_taskRecipientModule) = value
        End Set
    End Property





End Class

