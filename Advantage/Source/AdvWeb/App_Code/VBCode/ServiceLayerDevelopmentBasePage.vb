﻿Imports FAME.AdvantageV1.Common
Imports Microsoft.VisualBasic
Imports Advantage.Business.Objects

Namespace AdvWeb.VBCode
    ''' <summary>
    ''' Page base for development based in service layer.
    ''' This page is for include some code that can be common between
    ''' page. but the idea is that should be empty
    ''' </summary>
    ''' <remarks>Only for use in pages javascript based and Service layer</remarks>
    Public MustInherit Class ServiceLayerDevelopmentBasePage
        Inherits Page

        Protected CampusId As String
        Protected UserId As String
        Protected StudentId As String
        Protected LeadId As String
        Protected BoolSwitchCampus As Boolean = False
        Protected ResourceId As Integer
        Protected Pobj As New UserPagePermissionInfo


        Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
            ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
            UserId = AdvantageSession.UserState.UserId.ToString

        End Sub

        Protected Sub CheckSecurity(ByVal advantageUserState As User)
            Pobj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(ResourceId, String), CampusId)

            'While switching campus user may not have permission to this page in that campus
            'so redirect user to dashboard page
            If Pobj.HasNone = True Then
                Response.Redirect(String.Format("~/dash.aspx?resid=264&mod=SY&cmpid={0}&desc=dashboard", advantageUserState.CampusId.ToString))
                Exit Sub
            End If
        End Sub
    End Class
End Namespace