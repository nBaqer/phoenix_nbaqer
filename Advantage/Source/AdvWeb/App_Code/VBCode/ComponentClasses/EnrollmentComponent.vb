Imports FAME.AdvantageV1.BusinessFacade
Imports Microsoft.VisualBasic

Public Class EnrollmentComponent
    Public Function GenerateEnrollmentID(ByVal strLastName As String, ByVal strFirstName As String) As String
        Dim Sequence As New SingletonFacade
        Dim i, f, g, seqdiff As String
        Dim k As String = ""
        Dim No_Seq As String = ""
        Dim l, j, difference As Integer
        Dim l_LL, l_F, l_YY, l_MM, l_DD, l_Sequence As String

        'For Demo
        'l_LL = Mid(strLastName, 1, txtFormatLast.Text).ToUpper
        'l_F = Mid(strFirstName, 1, txtFormatFirst.Text).ToUpper


        l_LL = Mid(strLastName, 1, 2).ToUpper
        l_F = Mid(strFirstName, 1, 1).ToUpper

        l_YY = CDate(Now()).Year
        l_YY = Mid(l_YY, 1, 2)


        l_MM = CDate(Now()).Month
        l_MM = Mid(l_MM, 1, 1)

        l_DD = CDate(Now()).Day
        l_DD = Mid(l_DD, 1, 1)

        l_Sequence = 2

        Dim l_formatlength As Integer
        l_formatlength = 9

        SyncLock Sequence
            i = Sequence.GetStudentSequence()
            g = Len(i) 'Returns Length Of Sequence Number
            seqdiff = CInt(l_Sequence) - CInt(g)  'Get The Difference Between Length Of Sequence Number and Sequence Number Format
            If seqdiff > 0 Then
                For l = 1 To seqdiff
                    k += "0"
                Next
                i = k + i
            End If
            'For Demo Purpose 
            difference = 10 - (l_formatlength)
            'f = Mid(i, (g - 1), l_Sequence)
            f = Mid(i, g, l_Sequence)

            If difference >= 1 Then
                For j = 1 To difference
                    No_Seq += "0"
                Next
                f = No_Seq & f
            End If
            'Changed For Demo By Balaji on 08/04/2004
            Dim EnrollmentId As String
            EnrollmentId = StudentIDFormat(l_LL, l_F, l_YY, l_MM, l_DD, f)
            Return EnrollmentId
        End SyncLock
    End Function
    Public Function StudentIDFormat(Optional ByVal LL As String = "Null", Optional ByVal F As String = "Null", Optional ByVal YY As String = "", Optional ByVal MM As String = "Null", Optional ByVal DD As String = "Null", Optional ByVal SequenceID As String = "Null") As String
        'LL ---- First Two Letters Of Students LastName
        'F  ---- First Letter Of Students FirstName
        'YY ---- First Two Digits Of Lead Entry Year
        'MM ---- Month Of Lead Entry Year
        'DD ---- Day Of Lead Entry
        'IncrNUmber ---- Last Four Digits Of The System Generated Sequential Number
        '    Dim StudentID As String
        '  Dim StudentLastName As String
        '   Dim StudentFirstName As String
        'Set Values For Demo
        If YY = "Null" Then
            Return MM & DD & LL & F & SequenceID
        Else
            Return YY & MM & DD & LL & F & SequenceID
        End If
    End Function
    Public Function GenerateStudentId(ByVal strLastName As String, ByVal strFirstName As String, ByVal YearNumber As Integer, ByVal MonthNumber As Integer, ByVal DateNumber As Integer, ByVal LNameNumber As Integer, ByVal FNameNumber As Integer, ByVal seqNumber As Integer) As String
        Dim Sequence As New SingletonFacade
        Dim i As String 'f, g,  k,, No_Seq, seqdiff
        '  Dim l As Integer ', difference , j
        Dim l_LL, l_F, l_YY, l_MM, l_DD As String ', l_Sequence

        'Get The LastName Based On The Number Of Characters
        'Example : LastName is Kennedy and if School wants 2 characters
        'then l_LL = KE
        l_LL = Mid(strLastName, 1, LNameNumber).ToUpper

        'FirstName is John and if School wants 2 characters 
        'it will be Jo
        l_F = Mid(strFirstName, 1, FNameNumber).ToUpper

        'Get The Current Year
        l_YY = CDate(Now()).Year

        'Get The Number Of Characters For Year and 
        'Deceide the Starting Character
        If YearNumber = 4 Then
            l_YY = Mid(l_YY, 1, 4)
        ElseIf YearNumber = 3 Then
            l_YY = Mid(l_YY, 2, 3)
        ElseIf YearNumber = 2 Then
            l_YY = Mid(l_YY, 3, 2)
        ElseIf YearNumber = 1 Then
            l_YY = Mid(l_YY, 4, 1)
        Else
            l_YY = ""
        End If

        'Get The Current Month
        l_MM = CDate(Now()).Month
        l_MM = Mid(l_MM, 1, MonthNumber)

        'Get The Current Date
        l_DD = CDate(Now()).Day
        l_DD = Mid(l_DD, 1, DateNumber)


        'l_Sequence = seqNumber '2


        If l_MM.Length = 1 And MonthNumber = 2 Then
            l_MM = "0" + l_MM
        Else
            ' l_MM = l_MM - NZ 11/19 assignment has no effect
        End If

        If l_DD.Length = 1 And DateNumber = 2 Then
            l_DD = "0" + l_DD
        Else
            ' l_DD = l_DD - NZ 11/19 assignment has no effect
        End If

        ' Dim l_formatlength As Integer
        SyncLock Sequence
            i = Sequence.GetStudentFormatSequence()
        End SyncLock

        Dim EnrollmentId As String
        EnrollmentId = StudentIDFormat(l_LL, l_F, l_YY, l_MM, l_DD, i)
        Return EnrollmentId

        'If l_Sequence = 0 Then
        '    l_formatlength = l_LL.Length + l_F.Length + l_YY.Length + l_MM.Length + l_DD.Length
        'Else
        '    l_formatlength = l_LL.Length + l_F.Length + l_YY.Length + l_MM.Length + l_DD.Length + l_Sequence.Length
        'End If

        ' difference = 10 - (l_formatlength)
        'If l_Sequence <> 0 Or l_formatlength < 10 Then
        'SyncLock Sequence
        '    i = Sequence.GetStudentFormatSequence()
        '    g = Len(i) 'Returns Length Of Sequence Number

        '    If l_Sequence <> 0 Then
        '        seqdiff = CInt(l_Sequence) - CInt(g)  'Get The Difference Between Length Of Sequence Number and Sequence Number Format
        '        If seqdiff > 0 Then
        '            For l = 1 To seqdiff
        '                k += "0"
        '            Next
        '            i = k + i
        '        End If

        '        If g = 1 Then
        '            f = Mid(i, g, l_Sequence)
        '        Else
        '            f = Mid(i, (g - 1), l_Sequence)
        '        End If
        '    Else
        '        f = Mid(i, (g - 1), difference)
        '    End If

        '    l_formatlength = l_formatlength + f.Length
        '    difference = 10 - (l_formatlength)

        '    If l_formatlength < 10 Then
        '        For j = 1 To difference
        '            No_Seq += "0"
        '        Next
        '        f = No_Seq & f
        '    End If

        'End SyncLock
        ' End If
        'Changed For Demo By Balaji on 08/04/2004

    End Function
    Public Function GenerateStudentIdSeq(ByVal intStartNumber As Integer) As String
        Dim Sequence As New SingletonFacade
        Dim i, g As String ' f,, seqdiff, k, No_Seq
        ' Dim l As Integer ', difference, j 

        SyncLock Sequence
            i = Sequence.GetStudentSequenceNumber(intStartNumber)
            g = Len(i) 'Returns Length Of Sequence Number

            'Changed For Demo By Balaji on 08/04/2004
            Dim EnrollmentId As String
            EnrollmentId = i
            Return EnrollmentId
        End SyncLock
    End Function
End Class
