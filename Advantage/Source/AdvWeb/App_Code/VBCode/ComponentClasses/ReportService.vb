Imports System
Imports System.Data
Imports System.Web
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports Microsoft.VisualBasic


Namespace AdvWeb.VBCode.ComponentClasses

    Public Class ReportService

#Region "Private Data Members"

        Private mSchoolName As String

        'Template Report
        'Private rptTemp As TemplateReport
        'Admissions Reports
        Private rptLeadMasterDetail As adLeadMasterDetail
        Private rptLeadMasterSummary As adSummaryListingOfLeads
        Private rptCostPerLead As adCostPerLead
        Private rptInquiryByDay As adInquiryAnalysisByDay
        Private rptInquiryByTime As adInquiryAnalysisByTime
        Private rptStuListByStartDate As adStuListByStartDate
        Private rptDailyInquiryByMonth As adDailyInquiryByMonth
        Private rptLeadZipSummary As adLeadZipSummary
        Private rptLeadZipDetail As adLeadZipDetail
        Private rptAdmissionRepWTD As adAdmissionRepWTD
        Private rptAdmissionRepYTD As adAdmissionRepYTD
        Private rptAdmissionRepStartDate As adAdmissionRepStartDate
        'Academic Records Reports
        Private rptClassRoster As arClassRoster
        Private rptClassSchedule As arClassScheduleSummary
        Private rptCourseDrops As arCourseDrops
        Private rptStuClassSchedule As arStuClassSchedule
        Private rptStuTranscript As arStudentTranscript
        Private rptStuMultiTranscript As arMultiTranscript
        Private rptClsSectAttendance As arAttendance
        Private rptAttNoPosted As arAttNoPosted
        Private rptCoursePrereqs As arCoursePrereqs
        Private rptNoShow As arNoShowStudents
        Private rptStuLowGpa As arStuLowGPA
        Private rptStuNoScheduled As arStuNoScheduled
        Private rptStuPackingByRep As arAdmissionRepPackaging
        Private rptStuAbsencesSummary As arStuAbsencesSummary
        'Private rptSFE_AllADetail As arSFE_AllADetail
        'Private rptSFE_AllBDetail As arSFE_AllBDetail
        'Financial Aid Reports
        Private rptAidReceivedByStuDate As faAidReceivedByStudentDate
        Private rptAidReceivedSingleStu As faAidReceivedSingleStu
        Private rptStuBalProjNet As faStuBalProjNet
        Private rptEndingLoans As faListOfEndingLoans
        Private rptPastDueDisb As faListOfPastDueDisb
        Private rptStuWithoutProj As faStuWithoutProjections
        'Placement Reports
        Private rptEmployerMaster As plEmployerMasterListing
        Private rptJobListSummary As plJobListSummary
        Private rptJobListing As plJobListing
        Private rptStatsByField As plStatsByField
        Private rptMiscStats As plMiscStats
        'Student Accounts
        Private rptStuAgingDetail As saStuAgingDetail
        Private rptStuAgingSummary As saStuAgingSummary
        Private rptStuAccountBalance As saAccountBalance
        Private rptDepositsByType As saDepositsByType
        Private rptStuLedger As saStuLedger

#End Region


#Region "Public Properties"

        Public Property SchoolName() As String
            Get
                Return mSchoolName
            End Get
            Set(ByVal value As String)
                mSchoolName = value.ToUpper
            End Set
        End Property

#End Region


#Region "Public Methods"

        <CLSCompliant(False)> Public Function GetReport(ByVal rptParamInfo As ReportParamInfo, ByVal reportDataSet As DataSet) As ReportDocument

            If rptParamInfo.SqlId > 0 Then
                'Simple Reports
                Select Case rptParamInfo.SqlId
                    Case 7      'Misc Stats
                        rptMiscStats = New plMiscStats
                        reportDataSet.Tables(0).TableName = "MiscStats"
                        SetParameters(rptMiscStats, rptParamInfo, reportDataSet)
                        rptMiscStats.SetDataSource(reportDataSet)
                        Return rptMiscStats
                End Select

            Else
                'Complex Reports
                Select Case rptParamInfo.ObjId
                    Case 1      'Employer Master Listing
                        rptEmployerMaster = New plEmployerMasterListing
                        SetParameters(rptEmployerMaster, rptParamInfo, reportDataSet)
                        rptEmployerMaster.SetDataSource(reportDataSet)
                        Return rptEmployerMaster
                    Case 2      'Inquiry Analysis By Time
                        rptInquiryByTime = New adInquiryAnalysisByTime
                        SetParameters(rptInquiryByTime, rptParamInfo, reportDataSet)
                        rptInquiryByTime.SetDataSource(reportDataSet)
                        Return rptInquiryByTime
                    Case 3      'Listing Of Student By Start Date
                        rptStuListByStartDate = New adStuListByStartDate
                        SetParameters(rptStuListByStartDate, rptParamInfo, reportDataSet)
                        rptStuListByStartDate.SetDataSource(reportDataSet)
                        Return rptStuListByStartDate
                    Case 4      'List Of Course Drops
                        rptCourseDrops = New arCourseDrops
                        SetParameters(rptCourseDrops, rptParamInfo, reportDataSet)
                        rptCourseDrops.SetDataSource(reportDataSet)
                        Return rptCourseDrops
                    Case 5      'Student Transcript
                        rptStuTranscript = New arStudentTranscript
                        'SetParameters(rptStuTranscript, rptParamInfo, ReportDataSet)
                        rptStuTranscript.SetDataSource(reportDataSet)
                        Return rptStuTranscript
                    Case 6      'Multiple Student Transcript
                        rptStuMultiTranscript = New arMultiTranscript
                        'SetParameters(rptStuMultiTranscript, rptParamInfo, ReportDataSet)
                        rptStuMultiTranscript.SetDataSource(reportDataSet)
                        Return rptStuMultiTranscript
                    Case 7     'Summary Listing of Leads
                        rptLeadMasterSummary = New adSummaryListingOfLeads
                        SetParameters(rptLeadMasterSummary, rptParamInfo, reportDataSet)
                        rptLeadMasterSummary.SetDataSource(reportDataSet)
                        Return rptLeadMasterSummary
                    Case 8     'Lead Master Detail
                        rptLeadMasterDetail = New adLeadMasterDetail
                        SetParameters(rptLeadMasterDetail, rptParamInfo, reportDataSet)
                        rptLeadMasterDetail.SetDataSource(reportDataSet)
                        Return rptLeadMasterDetail
                    Case 9      'Student Aging Summary
                        rptStuAgingSummary = New saStuAgingSummary
                        SetParameters(rptStuAgingSummary, rptParamInfo, reportDataSet)
                        rptStuAgingSummary.SetDataSource(reportDataSet)
                        Return rptStuAgingSummary
                    Case 10     'Student Aging Detail
                        rptStuAgingDetail = New saStuAgingDetail
                        SetParameters(rptStuAgingDetail, rptParamInfo, reportDataSet)
                        rptStuAgingDetail.SetDataSource(reportDataSet)
                        Return rptStuAgingDetail
                    Case 11      'Job Listing
                        rptJobListing = New plJobListing
                        SetParameters(rptJobListing, rptParamInfo, reportDataSet)
                        rptJobListing.SetDataSource(reportDataSet)
                        Return rptJobListing
                    Case 12      'Job List Summary
                        rptJobListSummary = New plJobListSummary
                        SetParameters(rptJobListSummary, rptParamInfo, reportDataSet)
                        rptJobListSummary.SetDataSource(reportDataSet)
                        Return rptJobListSummary
                    Case 13      'Stats By Field Of Study
                        rptStatsByField = New plStatsByField
                        SetParameters(rptStatsByField, rptParamInfo, reportDataSet)
                        rptStatsByField.SetDataSource(reportDataSet)
                        Return rptStatsByField
                    Case 14      'Class Roster
                        rptClassRoster = New arClassRoster
                        SetParameters(rptClassRoster, rptParamInfo, reportDataSet)
                        rptClassRoster.SetDataSource(reportDataSet)
                        Dim subreportName As String
                        Dim subreportObject As SubreportObject
                        Dim subreport As ReportDocument
                        For Each rptObject As ReportObject In rptClassRoster.ReportDefinition.ReportObjects
                            ' Get the ReportObject by name and cast it as a SubreportObject.
                            If TypeOf (rptObject) Is SubreportObject Then
                                subreportObject = CType(rptObject, SubreportObject)
                                ' Get the subreport name.
                                subreportName = subreportObject.SubreportName
                                ' Open the subreport as a ReportDocument.
                                subreport = rptClassRoster.OpenSubreport(subreportName)
                                subreport.SetDataSource(reportDataSet.Tables(1))
                            End If
                        Next
                        Return rptClassRoster
                    Case 15      'Class Schedule
                        rptClassSchedule = New arClassScheduleSummary
                        SetParameters(rptClassSchedule, rptParamInfo, reportDataSet)
                        rptClassSchedule.SetDataSource(reportDataSet)
                        Dim subreportName As String
                        Dim subreportObject As SubreportObject
                        Dim subreport As ReportDocument
                        For Each rptObject As ReportObject In rptClassSchedule.ReportDefinition.ReportObjects
                            ' Get the ReportObject by name and cast it as a SubreportObject.
                            If TypeOf (rptObject) Is SubreportObject Then
                                subreportObject = CType(rptObject, SubreportObject)
                                ' Get the subreport name.
                                subreportName = subreportObject.SubreportName
                                ' Open the subreport as a ReportDocument.
                                subreport = rptClassSchedule.OpenSubreport(subreportName)
                                subreport.SetDataSource(reportDataSet.Tables(1))
                            End If
                        Next
                        Return rptClassSchedule
                    Case 16      'Student Class Schedule
                        rptStuClassSchedule = New arStuClassSchedule
                        SetParameters(rptStuClassSchedule, rptParamInfo, reportDataSet)
                        rptStuClassSchedule.SetDataSource(reportDataSet)
                        Return rptStuClassSchedule
                    Case 17     'Student Account Balance
                        rptStuAccountBalance = New saAccountBalance
                        SetParameters(rptStuAccountBalance, rptParamInfo, reportDataSet)
                        rptStuAccountBalance.SetDataSource(reportDataSet)
                        Return rptStuAccountBalance
                    Case 18     'Detailed Deposits By Type
                        rptDepositsByType = New saDepositsByType
                        SetParameters(rptDepositsByType, rptParamInfo, reportDataSet)
                        rptDepositsByType.SetDataSource(reportDataSet)
                        Dim subreportName As String
                        Dim subreportObject As SubreportObject
                        Dim subreport As ReportDocument
                        For Each rptObject As ReportObject In rptDepositsByType.ReportDefinition.ReportObjects
                            ' Get the ReportObject by name and cast it as a SubreportObject.
                            If TypeOf (rptObject) Is SubreportObject Then
                                subreportObject = CType(rptObject, SubreportObject)
                                ' Get the subreport name.
                                subreportName = subreportObject.SubreportName
                                ' Open the subreport as a ReportDocument.
                                subreport = rptDepositsByType.OpenSubreport(subreportName)
                                subreport.SetDataSource(reportDataSet.Tables("FundTypeSummary"))
                            End If
                        Next
                        Return rptDepositsByType
                    Case 19     'Student Ledger
                        rptStuLedger = New saStuLedger
                        SetParameters(rptStuLedger, rptParamInfo, reportDataSet)
                        rptStuLedger.SetDataSource(reportDataSet)
                        Return rptStuLedger
                    Case 20     'Cost Per Lead Analysis
                        rptCostPerLead = New adCostPerLead
                        SetParameters(rptCostPerLead, rptParamInfo, reportDataSet)
                        rptCostPerLead.SetDataSource(reportDataSet)
                        Return rptCostPerLead
                    Case 21     'Inquiry Analysis By Day
                        rptInquiryByDay = New adInquiryAnalysisByDay
                        SetParameters(rptInquiryByDay, rptParamInfo, reportDataSet)
                        rptInquiryByDay.SetDataSource(reportDataSet)
                        Return rptInquiryByDay
                    Case 22     'Aid Received by Date
                        rptAidReceivedByStuDate = New faAidReceivedByStudentDate
                        SetParameters(rptAidReceivedByStuDate, rptParamInfo, reportDataSet)
                        rptAidReceivedByStuDate.SetDataSource(reportDataSet)
                        Return rptAidReceivedByStuDate
                    Case 23     'Aid Received Single Student
                        rptAidReceivedSingleStu = New faAidReceivedSingleStu
                        SetParameters(rptAidReceivedSingleStu, rptParamInfo, reportDataSet)
                        rptAidReceivedSingleStu.SetDataSource(reportDataSet)
                        Return rptAidReceivedSingleStu
                    Case 24     'Student Balance Exceeding Projections
                        rptStuBalProjNet = New faStuBalProjNet
                        SetParameters(rptStuBalProjNet, rptParamInfo, reportDataSet)
                        rptStuBalProjNet.SetDataSource(reportDataSet)
                        Return rptStuBalProjNet
                    Case 25     'List Of Ending Loans 
                        rptEndingLoans = New faListOfEndingLoans
                        SetParameters(rptEndingLoans, rptParamInfo, reportDataSet)
                        rptEndingLoans.SetDataSource(reportDataSet)
                        Return rptEndingLoans
                    Case 26     'List of Past Due Disbursements
                        rptPastDueDisb = New faListOfPastDueDisb
                        SetParameters(rptPastDueDisb, rptParamInfo, reportDataSet)
                        rptPastDueDisb.SetDataSource(reportDataSet)
                        Return rptPastDueDisb
                    Case 27     'List of Students Without Financial Aid and Payment Plans
                        rptStuWithoutProj = New faStuWithoutProjections
                        SetParameters(rptStuWithoutProj, rptParamInfo, reportDataSet)
                        rptStuWithoutProj.SetDataSource(reportDataSet)
                        Return rptStuWithoutProj
                    Case 28     'Daily Inquiry Status By Month
                        rptDailyInquiryByMonth = New adDailyInquiryByMonth
                        SetParameters(rptDailyInquiryByMonth, rptParamInfo, reportDataSet)
                        rptDailyInquiryByMonth.SetDataSource(reportDataSet)
                        Return rptDailyInquiryByMonth
                    Case 29     'Zip Code Summary
                        rptLeadZipSummary = New adLeadZipSummary
                        SetParameters(rptLeadZipSummary, rptParamInfo, reportDataSet)
                        rptLeadZipSummary.SetDataSource(reportDataSet)
                        Return rptLeadZipSummary
                    Case 30     'Leads By Zip Code
                        rptLeadZipDetail = New adLeadZipDetail
                        SetParameters(rptLeadZipDetail, rptParamInfo, reportDataSet)
                        rptLeadZipDetail.SetDataSource(reportDataSet)
                        Return rptLeadZipDetail
                    Case 31     'Class Section Attendance
                        rptClsSectAttendance = New arAttendance
                        SetParameters(rptClsSectAttendance, rptParamInfo, reportDataSet)
                        rptClsSectAttendance.SetDataSource(reportDataSet)
                        Return rptClsSectAttendance
                    Case 32     'No Posted Attendance
                        rptAttNoPosted = New arAttNoPosted
                        SetParameters(rptAttNoPosted, rptParamInfo, reportDataSet)
                        rptAttNoPosted.SetDataSource(reportDataSet)
                        Return rptAttNoPosted
                    Case 33     'Course Prerequisits
                        rptCoursePrereqs = New arCoursePrereqs
                        SetParameters(rptCoursePrereqs, rptParamInfo, reportDataSet)
                        rptCoursePrereqs.SetDataSource(reportDataSet)
                        Return rptCoursePrereqs
                    Case 34     'No Show Students
                        rptNoShow = New arNoShowStudents
                        SetParameters(rptNoShow, rptParamInfo, reportDataSet)
                        rptNoShow.SetDataSource(reportDataSet)
                        Return rptNoShow
                    Case 35     'Students With Low GPA
                        rptStuLowGpa = New arStuLowGPA
                        SetParameters(rptStuLowGpa, rptParamInfo, reportDataSet)
                        rptStuLowGpa.SetDataSource(reportDataSet)
                        Return rptStuLowGpa
                    Case 36     'Students Not Scheduled
                        rptStuNoScheduled = New arStuNoScheduled
                        SetParameters(rptStuNoScheduled, rptParamInfo, reportDataSet)
                        rptStuNoScheduled.SetDataSource(reportDataSet)
                        Return rptStuNoScheduled
                    Case 37     'Admission Rep WTD/MTD Performance
                        rptAdmissionRepWTD = New adAdmissionRepWTD
                        SetParameters(rptAdmissionRepWTD, rptParamInfo, reportDataSet)
                        rptAdmissionRepWTD.SetDataSource(reportDataSet)
                        Return rptAdmissionRepWTD
                    Case 38     'Admission Rep YTD Performance
                        rptAdmissionRepYTD = New adAdmissionRepYTD
                        SetParameters(rptAdmissionRepYTD, rptParamInfo, reportDataSet)
                        rptAdmissionRepYTD.SetDataSource(reportDataSet)
                        Dim subreportName As String
                        Dim subreportObject As SubreportObject
                        Dim subreport As ReportDocument
                        For Each rptObject As ReportObject In rptAdmissionRepYTD.ReportDefinition.ReportObjects
                            ' Get the ReportObject by name and cast it as a SubreportObject.
                            If TypeOf (rptObject) Is SubreportObject Then
                                subreportObject = CType(rptObject, SubreportObject)
                                ' Get the subreport name.
                                subreportName = subreportObject.SubreportName
                                Select Case subreportName
                                    Case "adAdmissionRepYTDSubCampTtl.rpt"
                                        ' Open the subreport as a ReportDocument.
                                        subreport = rptAdmissionRepYTD.OpenSubreport(subreportName)
                                        subreport.SetDataSource(reportDataSet.Tables(1))
                                    Case "adAdmissionRepYTDSubCampGrpTtl.rpt"
                                        ' Open the subreport as a ReportDocument.
                                        subreport = rptAdmissionRepYTD.OpenSubreport(subreportName)
                                        subreport.SetDataSource(reportDataSet.Tables(2))
                                End Select
                            End If
                        Next
                        Return rptAdmissionRepYTD
                    Case 39     'Admission Rep Start Date Performance
                        rptAdmissionRepStartDate = New adAdmissionRepStartDate
                        SetParameters(rptAdmissionRepStartDate, rptParamInfo, reportDataSet)
                        rptAdmissionRepStartDate.SetDataSource(reportDataSet)
                        Return rptAdmissionRepStartDate
                    Case 40
                        rptStuPackingByRep = New arAdmissionRepPackaging
                        SetParameters(rptStuPackingByRep, rptParamInfo, reportDataSet)
                        rptStuPackingByRep.SetDataSource(reportDataSet)
                        Return rptStuPackingByRep
                    Case 41     'Student Absences Summary
                        rptStuAbsencesSummary = New arStuAbsencesSummary
                        SetParameters(rptStuAbsencesSummary, rptParamInfo, reportDataSet)
                        rptStuAbsencesSummary.SetDataSource(reportDataSet)
                        Return rptStuAbsencesSummary
                End Select
            End If

            Throw New Exception("Internal Exception, Report was not found")
        End Function


#End Region


#Region "Private Methods"

        Private Sub SetParameters(ByVal rptObject As ReportDocument, ByVal rptParamInfo As ReportParamInfo,
                                  Optional ByRef rptDataSet As DataSet = Nothing)

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            If rptDataSet.Tables.Count = 1 Or
               (rptObject.GetType.Name = "arClassRoster" And rptDataSet.Tables.Count = 2) Or
               (rptObject.GetType.Name = "arClassScheduleSummary" And rptDataSet.Tables.Count = 2) Or
               (rptObject.GetType.Name = "arStuClassSchedule" And rptDataSet.Tables.Count = 2) Or
               (rptObject.GetType.Name = "saStuLedger" And rptDataSet.Tables.Count = 2) Or
               (rptObject.GetType.Name = "saDepositsByType" And rptDataSet.Tables.Count = 4) Or
               (rptObject.GetType.Name = "faAidReceivedByStudentDate" And rptDataSet.Tables.Count = 3) Or
               (rptObject.GetType.Name = "adLeadZipSummary" And rptDataSet.Tables.Count = 2) Or
               (rptObject.GetType.Name = "arStudentTranscript" And rptDataSet.Tables.Count = 3) Or
               (rptObject.GetType.Name = "arMultiTranscript" And rptDataSet.Tables.Count = 3) Or
               (rptObject.GetType.Name = "adLeadZipDetail" And rptDataSet.Tables.Count = 2) Or
               (rptObject.GetType.Name = "arAttendance" And rptDataSet.Tables.Count = 5) Or
               (rptObject.GetType.Name = "faStuBalProjNet" And rptDataSet.Tables.Count = 3) Or
               (rptObject.GetType.Name = "faListOfPastDueDisb" And rptDataSet.Tables.Count = 3) Or
               (rptObject.GetType.Name = "saAccountBalance" And rptDataSet.Tables.Count = 3) Or
               (rptObject.GetType.Name = "adAdmissionRepWTD" And rptDataSet.Tables.Count = 5) Or
               (rptObject.GetType.Name = "adAdmissionRepYTD" And rptDataSet.Tables.Count = 3) Or
               (rptObject.GetType.Name = "adAdmissionRepStartDate" And rptDataSet.Tables.Count = 3) Or
               (rptObject.GetType.Name = "arAdmissionRepPackaging" And rptDataSet.Tables.Count = 4) Then

                'Create ReportParams table from ReportParamInfo object.
                Dim dt As DataTable = New DataTable("ReportParams")

                dt.Columns.Add("SchoolLogo", Type.GetType("System.Byte[]"))
                dt.Columns.Add("SchoolName", Type.GetType("System.String"))
                dt.Columns.Add("ShowFilters", Type.GetType("System.Boolean"))
                dt.Columns.Add("Filters", Type.GetType("System.String"))
                dt.Columns.Add("FilterOthers", Type.GetType("System.String"))
                dt.Columns.Add("ShowSortBy", Type.GetType("System.Boolean"))
                dt.Columns.Add("SortBy", Type.GetType("System.String"))
                dt.Columns.Add("ShowRptDescription", Type.GetType("System.Boolean"))
                dt.Columns.Add("ShowRptInstructions", Type.GetType("System.Boolean"))
                dt.Columns.Add("ShowRptNotes", Type.GetType("System.Boolean"))

                Dim r As DataRow = dt.NewRow

                If Not (rptParamInfo.SchoolLogo Is Nothing) Then
                    r("SchoolLogo") = rptParamInfo.SchoolLogo
                End If
                r("SchoolName") = myAdvAppSettings.AppSettings("SchoolName").ToUpper 'SchoolName
                r("ShowFilters") = rptParamInfo.ShowFilters
                r("ShowSortBy") = rptParamInfo.ShowSortBy
                r("ShowRptDescription") = rptParamInfo.ShowRptDescription
                r("ShowRptInstructions") = rptParamInfo.ShowRptInstructions
                r("ShowRptNotes") = rptParamInfo.ShowRptNotes

                If Not (rptParamInfo.FilterListString Is Nothing) Then
                    r("Filters") = "Filter criteria: " & rptParamInfo.FilterListString
                Else
                    r("Filters") = rptParamInfo.FilterListString
                End If

                If (rptParamInfo.FilterListString Is Nothing) And Not (rptParamInfo.FilterOtherString Is Nothing) Then
                    r("FilterOthers") = "Filter criteria: " & rptParamInfo.FilterOtherString
                Else
                    r("FilterOthers") = rptParamInfo.FilterOtherString
                End If

                If Not (rptParamInfo.OrderByString Is Nothing) Then
                    r("SortBy") = "Sort order: " & rptParamInfo.OrderByString
                Else
                    r("SortBy") = rptParamInfo.OrderByString
                End If

                'Some reports need StudentId column. 
                Select Case rptObject.GetType.Name
                    Case "adStuListByStartDate", "arCourseDrops", "arStuClassSchedule", "arStudentTranscript",
                        "arMultiTranscript", "saDepositsByType", "saAccountBalance", "saStuAgingDetail",
                        "saStuLedger", "saStuAgingSummary", "faAidReceivedByStudentDate",
                        "faAidReceivedSingleStu", "faStuBalProjNet", "faListOfEndingLoans",
                        "faListOfPastDueDisb", "faStuWithoutProjections", "arStuNoScheduled",
                        "faAidReceivedByStudentDate", "arAttendance", "arNoShowStudents", "arStuLowGPA",
                        "arAdmissionRepPackaging", "arStuAbsencesSummary"
                        'Add new column.
                        dt.Columns.Add("StudentIdentifier", Type.GetType("System.String"))
                        r("StudentIdentifier") = myAdvAppSettings.AppSettings("StudentIdentifier")
                        'Reports where student ID is used to group by, then add to the label a semicolon.
                        If rptObject.GetType.Name = "arStuClassSchedule" Or
                           rptObject.GetType.Name = "arStudentTranscript" Or
                           rptObject.GetType.Name = "arMultiTranscript" Or
                           rptObject.GetType.Name = "saStuLedger" Or
                           rptObject.GetType.Name = "saStuAgingDetail" Then
                            r("StudentIdentifier") &= ":"
                        End If
                End Select

                dt.Rows.Add(r)
                rptDataSet.Tables.Add(dt)
            End If
        End Sub



#End Region


    End Class


    '
    '   AdvantageReportFactory class
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class AdvantageReportFactory

        <CLSCompliant(False)> Public Function CreateAdvantageReport(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo) As ReportDocument
            Dim advRpt As New AdvantageReport

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            'advRpt.ReportAppServer = "Gnanapragasam.CentralManagementServer"
            If paramInfo.ResId = 353 Then
                'Admission Rep Performance
                advRpt = New AdmissionRepStartDateReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 351 Then
                'WTD/MTD Admission Reps Performance
                advRpt = New AdmissionRepWTDReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 352 Then
                'YTD Admission Reps Performance
                advRpt = New AdmissionRepYTDReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 485 Then
                'Weekly Admissions Report
                advRpt = New WeeklyAdmissionsReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 208 Then
                'Cost Per Lead Analysis
                advRpt = New CostPerLeadReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 318 Then
                'Inquiry Analysis By Month
                advRpt = New DailyInquiryByMonthReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 211 Then
                'Inquiry Analysis By Day
                advRpt = New InquiryByDayReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 223 Then
                'Inquiry Analysis By Time 
                advRpt = New InquiryByTimeReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 202 Then
                'Lead Master Detail 
                advRpt = New LeadMasterDetailReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 228 Then
                'Lead Master Summary
                advRpt = New SummaryListingOfLeadsReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 320 Then
                'Leads By Zip Code Detail 
                advRpt = New LeadZipDetailReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 319 Then
                'Leads By Zip Code Summary
                advRpt = New LeadZipSummaryReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 455 Then
                'Leads By Advertisements Detail
                advRpt = New LeadAdDetailReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 457 Then
                'Leads By Advertisements Summary
                advRpt = New LeadAdSummaryReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 229 Then
                'Student Listing 
                advRpt = New ListOfStudentReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 354 Then
                'Student Packaging
                advRpt = New AdmissionRepPackagingReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 325 Then
                'No Attendance Posted 
                advRpt = New AttendanceNotPostedReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 262 Then
                'Class Roster 
                advRpt = New ClassRosterReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 274 Then
                'Multiple Student Class Schedule 
                advRpt = New StudentClassScheduleReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 295 Then
                'Single Student Class Schedule
                advRpt = New StudentClassScheduleReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 267 Then
                'Class Schedule Summary 
                advRpt = New ClassScheduleSummaryReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 236 Then
                'Student Listing Of Courses Dropped 
                advRpt = New CourseDropsReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 515 Then
                'Dropped Courses Summary
                advRpt = New DroppedCoursesSummaryReport(ds, paramInfo)
                'Dim rptFileName As String = ""
                'rptFileName = "CrystalReport3.rpt"
                'Dim filename As String = HttpContext.Current.Server.MapPath(String.Format("~/Reports/{0}", rptFileName))
                'Return New AdvantageReport(filename, ds, paramInfo)

            ElseIf paramInfo.ResId = 328 Then
                'Course Prerequisites
                advRpt = New CoursePrereqsReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 245 Then
                'Multiple Student Transcripts 
                Dim transcriptTypeString As String = CType(myAdvAppSettings.AppSettings("TranscriptType"), String)
                Dim transcriptType As AdvantageCommonValues.TranscriptTypes = CType(System.Enum.Parse(GetType(AdvantageCommonValues.TranscriptTypes), transcriptTypeString), AdvantageCommonValues.TranscriptTypes)

                Select Case transcriptType
                    Case AdvantageCommonValues.TranscriptTypes.Traditional
                        advRpt = New MultiTranscriptReport(ds, paramInfo)
                    Case AdvantageCommonValues.TranscriptTypes.Traditional_A
                        advRpt = New MultiTranscriptTraditonalAReport(ds, paramInfo)
                    Case AdvantageCommonValues.TranscriptTypes.Traditional_B
                        advRpt = New MultiTranscriptTraditonalBReport(ds, paramInfo)
                    Case AdvantageCommonValues.TranscriptTypes.Traditional_Numeric
                        advRpt = New MultiTranscriptTraditonalCReport(ds, paramInfo)
                End Select

            ElseIf paramInfo.ResId = 238 Then
                'Single Student Transcript
                'this is the transcript type value from web config
                Dim transcriptTypeString As String = CType(myAdvAppSettings.AppSettings("TranscriptType"), String)
                Dim transcriptType As AdvantageCommonValues.TranscriptTypes = CType(System.Enum.Parse(GetType(AdvantageCommonValues.TranscriptTypes), transcriptTypeString), AdvantageCommonValues.TranscriptTypes)

                Select Case transcriptType
                    Case AdvantageCommonValues.TranscriptTypes.Traditional
                        advRpt = New StudentTranscriptReport(ds, paramInfo)
                    Case AdvantageCommonValues.TranscriptTypes.Traditional_A
                        advRpt = New StudentTranscriptTraditionalAReport(ds, paramInfo)
                    Case AdvantageCommonValues.TranscriptTypes.Traditional_B
                        advRpt = New StudentTranscriptTraditionalBReport(ds, paramInfo)
                    Case AdvantageCommonValues.TranscriptTypes.Traditional_Numeric
                        advRpt = New StudentTranscriptTraditionalCReport(ds, paramInfo)
                End Select
            ElseIf paramInfo.ResId = 329 Then
                'No Show Students 
                advRpt = New NoShowStudentsReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 366 Then
                'Summary Of Absences 
                advRpt = New StudentAbsencesSummaryReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 330 Then
                'Students With Low GPA
                advRpt = New StudentWithLowGPAReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 335 Then
                'Students Not Scheduled
                advRpt = New StudentsNotScheduledReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 285 Then
                'Aid Received by Date
                advRpt = New AidReceivedByStudentDateReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 310 Then
                'Financial Aid And Payment Plans About To Expire
                advRpt = New ListOfEndingLoansReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 311 Then
                'Past Due Disbursements/Payments
                advRpt = New ListOfPastDueProjectionsReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 307 Then
                'Student Balance Exceeding Projections
                advRpt = New StuBalProjNetReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 312 Then
                'Students Without Financial Aid and Payment Plans
                advRpt = New StudentsWithoutProjectionsReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 126 Then
                'Employer Master Listing
                advRpt = New EmployerMasterListingReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 122 Then
                'Job Listing Detail
                advRpt = New JobListingReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 123 Then
                'Job Listing Summary
                advRpt = New JobListSummaryReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 127 Then
                'Program Earnings Statistics
                advRpt = New ProgramEarningsStatsReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 125 Then
                'Program Placement By Field Of Study
                advRpt = New StatsByFieldReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 275 Then
                'Student Account Balance
                advRpt = New StudentsAccountBalanceReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 276 Then
                'Payments By Type Detail 
                advRpt = New DepositsByTypeReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 426 Then
                'Payments By Type Summary
                advRpt = New DepositsByTypeSummaryReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 249 Then
                'Student Aging Detail
                advRpt = New StudentAgingDetailReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 247 Then
                'Student Aging Summary
                advRpt = New StudentAgingSummaryReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 277 Then
                'Student Ledger
                advRpt = New StudentLedgerReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 384 Then
                'Revenue Ratio (90/10 Rule)
                advRpt = New RevenueRatioReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 632 Then
                'Revenue Ratio Summary (90/10 Rule)
                advRpt = New RevenueRatioSummaryReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 386 Then
                'Projected Cash Flow for 30, 60, 90 and 120 days
                advRpt = New ProjectedCashFlowReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 489 Then
                'Deferred Revenue Detail
                advRpt = New DeferredRevenueDetailReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 490 Then
                'Deferred Revenue Summary
                advRpt = New DeferredRevenueSummaryReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 392 Then
                'User Listing By Role
                advRpt = New UserListingByRole(ds, paramInfo)
            ElseIf paramInfo.ResId = 462 Then
                'Revenue Posted By Program Version Detail
                advRpt = New RevenuePostedReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 461 Then
                'Revenue Posted By Program Version Summary
                advRpt = New RevenuePostedSummaryReport(ds, paramInfo)
                'ElseIf paramInfo.ResId = 561 Then
                '    'Accounts Receivable Deferred Income
                '    advRpt = New DefAccountsReceivableReport(ds, paramInfo)

                ' NOTE from BEN - At some point, we should have a table that maps ResourceId to Report File Name
                ' Once, this is done, this entire function will only a couple lines
            ElseIf (paramInfo.ResId >= 492 AndAlso paramInfo.ResId <= 495 Or paramInfo.ResId = 514 Or paramInfo.ResId = 518 Or paramInfo.ResId = 525 Or paramInfo.ResId = 553 Or paramInfo.ResId = 561) Then
                ' 10/30/06 - BEN - Add StudentAttendance Report
                ' 11/4/06 - BEN - Added arMissingItems, arGradesByDateRange, arSAPCheckResults
                Dim rptFileName As String = ""
                Select Case paramInfo.ResId
                    Case 492
                        rptFileName = "arStudentAttendance.rpt"
                    Case 493
                        rptFileName = "arMissingItems.rpt"
                    Case 494
                        rptFileName = "arGradesByDateRange.rpt"
                    Case 495
                        rptFileName = "arSAPCheckResults.rpt"
                    Case 514
                        rptFileName = "arGradesBySummary.rpt"
                    Case 518
                        rptFileName = "arClockHourAttendance.rpt"
                    Case 525
                        rptFileName = "arCourseSequences.rpt"
                    Case 553
                        rptFileName = "arCourseEquivalent.rpt"
                    Case 561
                        rptFileName = "saDefAcctRec.rpt"
                End Select

                Dim filename As String = HttpContext.Current.Server.MapPath(String.Format("~/Reports/{0}", rptFileName))
                Return New AdvantageReport(filename, ds, paramInfo)

                'ElseIf paramInfo.ResId = 507 Then
                '    'Single Student Transcript Traditional1
                '    advRpt = New StudentTranscriptTraditionalAReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 513 Then
                'Student Account Balance Multiple Enrollments
                advRpt = New StudentsAccountBalanceMultipleEnrollmentsReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 516 Then
                'Student Account Balance Multiple Enrollments
                advRpt = New StudentsAccountBalanceCreditsBalanceOnlyReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 537 Then
                'Student Progress Report
                advRpt = New StudentProgressReportSummary(ds, paramInfo)
            ElseIf paramInfo.ResId = 550 Then
                advRpt = New MultiProgressReportSummary(ds, paramInfo)
            ElseIf paramInfo.ResId = 551 Then
                advRpt = New StudentSigninSheet(ds, paramInfo)
            ElseIf paramInfo.ResId = 554 Then
                advRpt = New ClassSectionAttendance(ds, paramInfo)
            ElseIf paramInfo.ResId = 555 Then
                advRpt = New AttendanceByDay(ds, paramInfo)
                'ElseIf paramInfo.ResId = 553 Then
                '    advRpt = New AttendanceByDay(ds, paramInfo)
            ElseIf paramInfo.ResId = 556 Then
                advRpt = New MultipleStudentLedger(ds, paramInfo)
            ElseIf paramInfo.ResId = 560 Then
                advRpt = New GradeBookResultsReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 580 Then
                advRpt = New AttendanceHistoryReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 582 Then
                advRpt = New IndividualSAPReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 585 Then
                advRpt = New TimeClockExceptionReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 586 Then
                advRpt = New PrintTimeClockPunchesReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 588 Then
                advRpt = New WeeklyAttendanceReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 591 Then
                advRpt = New PrintAttendanceSheet(ds, paramInfo)
            ElseIf paramInfo.ResId = 589 Then
                advRpt = New AttendanceHistoryMultipleStudent(ds, paramInfo)
            ElseIf paramInfo.ResId = 590 Then
                advRpt = New AttendanceHistorySingleStudent(ds, paramInfo)
                '--code added by Priyanka on date 13th of May 2009
            ElseIf paramInfo.ResId = 602 Then
                'List of Students Dropped Courses 
                advRpt = New StudentCoursesDropReport(ds, paramInfo)
                '--
            ElseIf paramInfo.ResId = 600 Then
                advRpt = New ExternshipAttendanceReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 606 Then
                advRpt = New StudentGPAReport(ds, paramInfo)

            ElseIf paramInfo.ResId = 615 Then
                advRpt = New DictationTestReport(ds, paramInfo)
                'Add bt Vijay Ramteke on Feb 22, 2010
            ElseIf paramInfo.ResId = 618 Then
                advRpt = New ExpectedDisbursementReport(ds, paramInfo)
            ElseIf paramInfo.ResId = 629 Then
                advRpt = New ClockHourTranscript(ds, paramInfo)
                ''New Code Added By Vijay Ramteke On July 09, 2010 For Mantis Id 11856
            ElseIf paramInfo.ResId = 633 Then
                advRpt = New ConsecutiveAbsentDays(ds, paramInfo)
                ''New Code Added By Vijay Ramteke On July 09, 2010 For Mantis Id 11856
                ''New Code Added By Vijay Ramteke On July 10, 2010 For Mantis Id 06156
            ElseIf paramInfo.ResId = 634 Then
                advRpt = New LOAExpiration(ds, paramInfo)
                ''New Code Added By Vijay Ramteke On July 10, 2010 For Mantis Id 06156
            ElseIf paramInfo.ResId = 636 Then
                advRpt = New SummaryReportByStudent9010(ds, paramInfo)
                ''New Code Added By Vijay Ramteke On July 22, 2010
            ElseIf paramInfo.ResId = 637 Then
                advRpt = New RevenueRatioDetailForStudent9010(ds, paramInfo)
                ''New Code Added By Vijay Ramteke On July 22, 2010
                ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 17901
            ElseIf paramInfo.ResId = 649 Then
                advRpt = New StudentByProgram(ds, paramInfo)
                ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 17901
                ''New Code Added By Vijay Ramteke On September 25, 2010 For Mantis Id 19129
            ElseIf paramInfo.ResId = 650 Then
                advRpt = New StudentAppliedPayments(ds, paramInfo)
                ''New Code Added By Vijay Ramteke On September 25, 2010 For Mantis Id 19129
            ElseIf paramInfo.ResId = 779 Then
                advRpt = New WeeklyAttendanceReport(ds, paramInfo)
            End If


            advRpt.SetDataSource(ds)

            Return advRpt

        End Function
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class AdvantageReport
        Inherits ReportDocument

        Protected MDataset As DataSet
        <CLSCompliant(False)> Protected MRptParamInfo As ReportParamInfo
        Protected MPath As String


        Public Sub New()
            MDataset = New DataSet
            MRptParamInfo = New ReportParamInfo
            'm_path = "ras://" & HttpContext.Current.Server.MapPath("..")
            MPath = HttpContext.Current.Server.MapPath("..")
        End Sub

        ' BEN - Add a constructor to take in the correct params
        <CLSCompliant(False)> Public Sub New(ByVal filename As String, ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MDataset = ds
            MRptParamInfo = paramInfo
            MPath = filename
            Dim dt As DataTable = BuildRptParamsTable()
            dt.TableName = "ReportParams"
            Try
                ds.Tables.Add(dt.Copy())
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            Load(filename, OpenReportMethod.OpenReportByDefault, 0)
            SetDataSource(ds)

            ' open up all sub-reports
            For Each rptObject As ReportObject In ReportDefinition.ReportObjects
                If TypeOf (rptObject) Is SubreportObject Then
                    Dim subreportObject As SubreportObject = CType(rptObject, SubreportObject)
                    Dim subreportName As String = subreportObject.SubreportName
                    Dim subreport As ReportDocument = OpenSubreport(subreportName)
                    subreport.SetDataSource(ds)
                End If
            Next
        End Sub

        Protected Overridable Sub SetReportParameters(Optional ByVal resourceid As String = "")
            Dim dtRptParams As DataTable = BuildRptParamsTable(resourceid)
            MDataset.Tables.Add(dtRptParams)
        End Sub
        Protected Overridable Sub SetReportParameters(ByVal useStudentIdLabel As Boolean)
            Dim dtRptParams As DataTable = BuildRptParamsTable()

            dtRptParams.Columns.Add("StudentIdentifier", Type.GetType("System.String"))
            Dim row As DataRow = dtRptParams.Rows(0)

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            row("StudentIdentifier") = myAdvAppSettings.AppSettings("StudentIdentifier")

            If useStudentIdLabel Then
                row("StudentIdentifier") &= ":"
            End If
            MDataset.Tables.Add(dtRptParams)
        End Sub
        Private Function BuildRptParamsTable(Optional ByVal resourceid As String = "") As DataTable
            'Create ReportParams table from ReportParamInfo object.
            Dim dt As DataTable = New DataTable("ReportParams")

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            dt.Columns.Add("SchoolLogo", Type.GetType("System.Byte[]"))
            dt.Columns.Add("SchoolName", Type.GetType("System.String"))
            dt.Columns.Add("ShowFilters", Type.GetType("System.Boolean"))
            dt.Columns.Add("Filters", Type.GetType("System.String"))
            dt.Columns.Add("FilterOthers", Type.GetType("System.String"))
            dt.Columns.Add("resid", Type.GetType("System.String"))
            Try
                If Not resourceid = "" Then
                    If resourceid.ToString = "560" Then
                        dt.Columns.Add("MinimumScore", Type.GetType("System.String"))
                    End If
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)


            End Try

            dt.Columns.Add("ShowSortBy", Type.GetType("System.Boolean"))
            dt.Columns.Add("SortBy", Type.GetType("System.String"))
            dt.Columns.Add("ShowRptDescription", Type.GetType("System.Boolean"))
            dt.Columns.Add("ShowRptInstructions", Type.GetType("System.Boolean"))
            dt.Columns.Add("ShowRptNotes", Type.GetType("System.Boolean"))
            dt.Columns.Add("ShowUseSignLineForAttnd", Type.GetType("System.Boolean"))
            Dim r As DataRow = dt.NewRow
            r("ShowUseSignLineForAttnd") = MRptParamInfo.ShowUseSignLineForAttnd
            If Not (MRptParamInfo.SchoolLogo Is Nothing) Then
                r("SchoolLogo") = MRptParamInfo.SchoolLogo
            End If
            r("SchoolName") = myAdvAppSettings.AppSettings("SchoolName").ToUpper 'SchoolName
            r("ShowFilters") = MRptParamInfo.ShowFilters
            r("ShowSortBy") = MRptParamInfo.ShowSortBy
            r("ShowRptDescription") = MRptParamInfo.ShowRptDescription
            r("ShowRptInstructions") = MRptParamInfo.ShowRptInstructions
            r("ShowRptNotes") = MRptParamInfo.ShowRptNotes
            r("resid") = MRptParamInfo.ResId
            If Not (MRptParamInfo.FilterListString Is Nothing) Then
                If Not resourceid = "" Then

                    If resourceid.ToString = "560" Then
                        r("Filters") = MRptParamInfo.FilterListString
                    End If
                Else
                    r("Filters") = "Filter criteria: " & MRptParamInfo.FilterListString
                End If
            Else
                r("Filters") = MRptParamInfo.FilterListString
            End If

            If Not resourceid = "" Then
                If resourceid.ToString = "560" Then
                    If Not (MRptParamInfo.FilterOther Is Nothing) Then
                        r("FilterOthers") = Replace(MRptParamInfo.FilterOther, "arClassSections.", "")
                    End If
                End If
            Else
                If (MRptParamInfo.FilterListString Is Nothing) And Not (MRptParamInfo.FilterOtherString Is Nothing) Then
                    r("FilterOthers") = "Filter criteria: " & MRptParamInfo.FilterOtherString
                Else
                    If MRptParamInfo.ResId = "554" Then
                        Dim arrtemp() As String = MRptParamInfo.FilterOther.Split(CType("'", Char()))
                        Dim dStartDate As Date = CType(arrtemp(1), Date)
                        Dim dEndDate As Date = CType(FormatDateTime(CType(arrtemp(3), Date), DateFormat.ShortDate), Date)
                        r("FilterOthers") = "Attendance Between " + dStartDate + " - " + dEndDate + vbCrLf + "Maximum Attendance (%) " + MRptParamInfo.FilterOtherString
                    Else
                        r("FilterOthers") = MRptParamInfo.FilterOtherString
                    End If
                End If
            End If
            If Not resourceid = "" Then
                If resourceid.ToString = "560" Then
                    r("MinimumScore") = MRptParamInfo.FilterOtherString
                End If
            End If

            If Not (MRptParamInfo.OrderByString Is Nothing) Then
                r("SortBy") = "Sort order: " & MRptParamInfo.OrderByString
            Else
                r("SortBy") = MRptParamInfo.OrderByString
            End If

            'added by Theresa on 4/20/2009 for student schedule to have the corporateinfo for the header like in Transcript report
            If MRptParamInfo.ResId = "295" Or MRptParamInfo.ResId = "274" Or MRptParamInfo.ResId = "582" Then
                dt.Columns.Add("CorporateName", Type.GetType("System.String"))
                dt.Columns.Add("FullAddress", Type.GetType("System.String"))
                dt.Columns.Add("Phone", Type.GetType("System.String"))
                dt.Columns.Add("Fax", Type.GetType("System.String"))
                dt.Columns.Add("Website", Type.GetType("System.String"))
                Dim corpInfo As CorporateInfo
                Dim streetAddress As String
                Dim cityStateZip As String
                Dim facInputMasks As New InputMasksFacade
                Dim zipMask As String = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

                corpInfo = (New CampusGroupsFacade).GetCorporateInfoFromCampusId(MRptParamInfo.CampusId)
                With corpInfo

                    r("CorporateName") = .CorporateName
                    streetAddress = .Address1
                    streetAddress &= " " & .Address2
                    r("FullAddress") = streetAddress
                    cityStateZip = .City
                    If .State <> "" Then
                        If cityStateZip <> "" Then
                            cityStateZip &= ", " & .State
                        Else
                            cityStateZip = .State
                        End If
                    End If
                    If .Zip <> "" Then
                        If cityStateZip <> "" Then
                            cityStateZip &= " " & facInputMasks.ApplyMask(zipMask, .Zip)
                        Else
                            cityStateZip = facInputMasks.ApplyMask(zipMask, .Zip)
                        End If
                    End If
                    If cityStateZip <> "" Then
                        If r("FullAddress") <> "" Then
                            r("FullAddress") &= Chr(10) & cityStateZip
                        Else
                            r("FullAddress") = cityStateZip
                        End If
                    End If

                    If .Phone <> "" Then
                        r("Phone") = "Phone: " & .Phone
                        'row("Phone") = "Phone: " & facInputMasks.ApplyMask(strMask, .Phone)
                    End If
                    If .Fax <> "" Then
                        r("Fax") = "Fax: " & .Fax
                        'row("Fax") = "Fax: " & facInputMasks.ApplyMask(strMask, .Fax)
                    End If
                    If .Website <> "" Then
                        r("Website") = .Website
                    End If

                End With
            End If

            dt.Rows.Add(r)

            Return dt
        End Function
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class AdmissionRepStartDateReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            ''Table Count increased as query is modified and tables increased
            ''modified by Saraswathi on nov 11 2009
            If MDataset.Tables.Count = 7 Then SetReportParameters()
            Load(MPath + "\Reports\adAdmissionRepStartDate.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class AdmissionRepWTDReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 14 Then SetReportParameters()
            Load(MPath + "\Reports\TestAdminReport.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class AdmissionRepYTDReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters()
            Load(MPath + "\Reports\adAdmissionRepYTD.rpt", OpenReportMethod.OpenReportByDefault, 0)
            Dim subreportName As String
            Dim subreportObject As SubreportObject
            Dim subreport As ReportDocument
            For Each rptObject As ReportObject In ReportDefinition.ReportObjects
                ' Get the ReportObject by name and cast it as a SubreportObject.
                If TypeOf (rptObject) Is SubreportObject Then
                    subreportObject = CType(rptObject, SubreportObject)
                    ' Get the subreport name.
                    subreportName = subreportObject.SubreportName
                    Select Case subreportName
                        Case "adAdmissionRepYTDSubCampTtl.rpt"
                            ' Open the subreport as a ReportDocument.
                            subreport = OpenSubreport(subreportName)
                            If Not ds.Tables(1).Columns.Contains("ReportDate") Then
                                ds.Tables(1).Columns.Add(New DataColumn("ReportDate", Type.GetType("System.String")))
                                Dim i As Integer
                                For i = 0 To ds.Tables(1).Rows.Count - 1
                                    ds.Tables(1).Rows(i)("ReportDate") = ds.Tables("ReportParams").Rows(0)("FilterOthers")
                                Next i
                                ds.AcceptChanges()
                            End If

                            subreport.SetDataSource(ds.Tables(1))
                        Case "adAdmissionRepYTDSubCampGrpTtl.rpt"
                            ' Open the subreport as a ReportDocument.
                            subreport = OpenSubreport(subreportName)
                            If Not ds.Tables(2).Columns.Contains("ReportDate") Then
                                ds.Tables(2).Columns.Add(New DataColumn("ReportDate", Type.GetType("System.String")))
                                Dim i As Integer
                                For i = 0 To ds.Tables(2).Rows.Count - 1
                                    ds.Tables(2).Rows(i)("ReportDate") = ds.Tables("ReportParams").Rows(0)("FilterOthers")
                                Next i
                                ds.AcceptChanges()
                            End If

                            subreport.SetDataSource(ds.Tables(2))
                    End Select
                End If
            Next
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class WeeklyAdmissionsReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 9 Then SetReportParameters()
            Load(MPath + "\Reports\adAdmissionsWeekly.rpt", OpenReportMethod.OpenReportByDefault, 0)
            Dim subreportName As String
            'Open subreports and set their datasources
            '1. Inquiries subreport
            subreportName = "adAdmissionsWeeklySubInquiries.rpt"
            OpenSubreport(subreportName)
            Subreports(subreportName).SetDataSource(ds.Tables("Inquiries"))
            '2. Lead statuses subreport
            subreportName = "adAdmissionsWeeklySubLeadStatuses.rpt"
            OpenSubreport(subreportName)
            Subreports(subreportName).SetDataSource(ds.Tables("LeadStatuses"))
            '3. Document and tests subreport
            subreportName = "adAdmissionsWeeklySubDocAndTest.rpt"
            OpenSubreport(subreportName)
            Dim ds1 As New DataSet
            ds1.Tables.Add(ds.Tables("DocumentsAndTests").Copy)
            ds1.Tables.Add(ds.Tables("DocTestRates").Copy)
            Subreports(subreportName).SetDataSource(ds1)
            '4. Appointments subreport
            subreportName = "adAdmissionsWeeklySubAppts.rpt"
            OpenSubreport(subreportName)
            Dim ds2 As New DataSet
            ds2.Tables.Add(ds.Tables("Appointments").Copy)
            ds2.Tables.Add(ds.Tables("ApptRates").Copy)
            Subreports(subreportName).SetDataSource(ds2)
            '5. Placements subreport
            subreportName = "adAdmissionsWeeklySubPlacements.rpt"
            OpenSubreport(subreportName)
            Subreports(subreportName).SetDataSource(ds.Tables("Placements"))
            '6. AdmissionsRepPerfomance subreport
            subreportName = "adAdmissionsWeeklySubAdRepPerformance.rpt"
            OpenSubreport(subreportName)
            Subreports(subreportName).SetDataSource(ds.Tables("AdRepPerformance"))
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class CostPerLeadReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Load(MPath + "\Reports\adCostPerLead.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class DailyInquiryByMonthReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Load(MPath + "\Reports\adDailyInquiryByMonth.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class InquiryByDayReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Load(MPath + "\Reports\adInquiryAnalysisByDay.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class InquiryByTimeReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Load(MPath + "\Reports\adInquiryAnalysisByTime.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class LeadMasterDetailReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Load(MPath + "\Reports\adLeadMasterDetail1.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class LeadZipDetailReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters()
            Load(MPath + "\Reports\adLeadZipDetail.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class LeadZipSummaryReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters()
            Load(MPath + "\Reports\adLeadZipSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class LeadAdDetailReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Load(MPath + "\Reports\adLeadAdDetail.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class LeadAdSummaryReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters()
            Load(MPath + "\Reports\adLeadAdSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class ListOfStudentReport
        Inherits AdvantageReport
      
      
        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Load(MPath + "\Reports\adStuListByStartDate.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class SummaryListingOfLeadsReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Load(MPath + "\Reports\adSummaryListingOfLeads.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class AdmissionRepPackagingReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 4 Then SetReportParameters(False)
            Load(MPath + "\Reports\arAdmissionRepPackaging.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class AttendanceReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 5 Then SetReportParameters(False)
            Load(MPath + "\Reports\arAttendance.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class AttendanceNotPostedReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Load(MPath + "\Reports\arAttNoPosted.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class ClassRosterReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters(True)
            Load(MPath + "\Reports\arClassRoster.rpt", OpenReportMethod.OpenReportByDefault, 0)

            Dim subreportName As String
            Dim subreportObject As SubreportObject
            Dim subreport As ReportDocument
            For Each rptObject As ReportObject In ReportDefinition.ReportObjects
                ' Get the ReportObject by name and cast it as a SubreportObject.
                If TypeOf (rptObject) Is SubreportObject Then
                    subreportObject = CType(rptObject, SubreportObject)
                    ' Get the subreport name.
                    subreportName = subreportObject.SubreportName
                    ' Open the subreport as a ReportDocument.
                    subreport = OpenSubreport(subreportName)
                    subreport.SetDataSource(ds.Tables(1))
                End If
            Next
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class ClassScheduleSummaryReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If ds.Tables.Count = 2 Then SetReportParameters(False)
            Load(MPath + "\Reports\arClassScheduleSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
            Dim subreportName As String
            Dim subreportObject As SubreportObject
            Dim subreport As ReportDocument
            For Each rptObject As ReportObject In ReportDefinition.ReportObjects
                ' Get the ReportObject by name and cast it as a SubreportObject.
                If TypeOf (rptObject) Is SubreportObject Then
                    subreportObject = CType(rptObject, SubreportObject)
                    ' Get the subreport name.
                    subreportName = subreportObject.SubreportName
                    ' Open the subreport as a ReportDocument.
                    subreport = OpenSubreport(subreportName)
                    subreport.SetDataSource(ds.Tables(1))
                End If
            Next
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class CourseDropsReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Load(MPath + "\Reports\arCourseDrops.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class DroppedCoursesSummaryReport
        Inherits AdvantageReport
        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Load(MPath + "\Reports\CrystalReport3.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class


    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class CoursePrereqsReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Load(MPath + "\Reports\arCoursePrereqs.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class MultiTranscriptReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(True)
            Load(MPath + "\Reports\arMultiTranscript.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    <Serializable()>
    <CLSCompliant(True)> Public Class MultiTranscriptTraditonalAReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(True)
            Load(MPath + "\Reports\arMultiTranscriptTraditionalA.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    <Serializable()>
    <CLSCompliant(True)> Public Class MultiTranscriptTraditonalBReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(True)
            Load(MPath + "\Reports\arMultiTranscriptTraditionalB.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class MultiTranscriptTraditonalCReport
        Inherits AdvantageReport
        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(True)
            Load(MPath + "\Reports\arMultiTranscriptTraditionalC.rpt", OpenReportMethod.OpenReportByDefault, 0)
            'Me.Load(m_path + "\Reports\arStudentTranscriptTraditionalC.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class NoShowStudentsReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Load(MPath + "\Reports\arNoShowStudents.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentAbsencesSummaryReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Load(MPath + "\Reports\arStuAbsencesSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentClassScheduleReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters(True)
            Load(MPath + "\Reports\arStuClassSchedule.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentTranscriptReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(True)
            Load(MPath + "\Reports\arStudentTranscript.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    <Serializable()>
    <CLSCompliant(True)> Public Class StudentTranscriptTraditionalAReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(True)
            Load(MPath + "\Reports\arStudentTranscriptTraditionalA.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    <Serializable()>
    <CLSCompliant(True)> Public Class StudentTranscriptTraditionalBReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(True)
            ds.Dispose()
            Dim path As String = MPath + "\Reports\arStudentTranscriptTraditionalB.rpt"
            Load(path, OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentTranscriptTraditionalCReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(True)
            Load(MPath + "\Reports\arStudentTranscriptTraditionalC.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentWithLowGPAReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\arStuLowGPA.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentsNotScheduledReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\arStuNoScheduled.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class AidReceivedByStudentDateReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\faAidReceivedByStudentDate.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class AidReceivedSingleStudentReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\faAidReceivedSingleStu.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class ListOfEndingLoansReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\faListOfEndingLoans.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class ListOfPastDueProjectionsReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\faListOfPastDueDisb.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StuBalProjNetReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\faStuBalProjNet.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentsWithoutProjectionsReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\faStuWithoutProjections.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class RevenueRatioReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 5 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\faRevenueRatio.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub

        Protected Overrides Sub SetReportParameters(ByVal useStudentIdLabel As Boolean)
            MyBase.SetReportParameters(useStudentIdLabel)
            '   Add an additional column for the report title, because the revenue ratio might change. 
            '   Need to change the report title based on the web.config variable: RevenueRatio.
            Dim dtRptParams As DataTable = MDataset.Tables("ReportParams")
            dtRptParams.Columns.Add("RptTitle", System.Type.GetType("System.String"))
            dtRptParams.Rows(0)("RptTitle") = MRptParamInfo.RptTitle.ToUpper
        End Sub

    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class RevenueRatioSummaryReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 5 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\faRevenueRatioSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub

        Protected Overrides Sub SetReportParameters(ByVal useStudentIdLabel As Boolean)
            MyBase.SetReportParameters(useStudentIdLabel)
            '   Add an additional column for the report title, because the revenue ratio might change. 
            '   Need to change the report title based on the web.config variable: RevenueRatio.
            Dim dtRptParams As DataTable = MDataset.Tables("ReportParams")
            dtRptParams.Columns.Add("RptTitle", System.Type.GetType("System.String"))
            dtRptParams.Rows(0)("RptTitle") = MRptParamInfo.RptTitle.ToUpper
        End Sub

    End Class
    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class ProjectedCashFlowReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 5 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\faProjectedCashFlow.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class


    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class DeferredRevenueDetailReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 4 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\saDefRevenueDetail.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    <Serializable()>
    <CLSCompliant(True)> Public Class DefAccountsReceivableReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\saDefAcctRec.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class DeferredRevenueSummaryReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 4 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\saDefRevenueSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class EmployerMasterListingReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If ds.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\plEmployerMasterListing.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class JobListingReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\plJobListing.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class JobListSummaryReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\plJobListSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class ProgramEarningsStatsReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "MiscStats"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\plMiscStats.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StatsByFieldReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\plStatsByField.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentsAccountBalanceReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\saAccountBalance.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentsAccountBalanceCreditsBalanceOnlyReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\saAccountBalanceCreditBalanceOnly.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentsAccountBalanceMultipleEnrollmentsReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\saAccountBalanceMultipleEnrollments.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class DepositsByTypeReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\saDepositsByType.rpt", OpenReportMethod.OpenReportByDefault, 0)
            Me.Subreports("saDepositsByTypeSub.rpt").SetDataSource(MDataset.Tables("FundTypeSummary"))
            Me.OpenSubreport("saDepositsByTypeSub.rpt")
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class DepositsByTypeSummaryReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters()
            Me.Load(MPath + "\Reports\saDepositsByTypeSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentAgingDetailReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(True)
            Me.Load(MPath + "\Reports\saStuAgingDetail.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentAgingSummaryReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\saStuAgingSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentLedgerReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            'Added by Vijay Ramteke in Feb 02, 2010
            'If m_dataset.Tables.Count = 2 Then SetReportParameters(True)
            If MDataset.Tables.Count = 5 Then SetReportParameters(True)
            'Added by Vijay Ramteke in Feb 02, 2010
            Me.Load(MPath + "\Reports\saStuLedger.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class UserListingByRole
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "UserListingByRole"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\syUserListingByRole.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class RevenuePostedReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 5 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\saRevenuePosted.rpt", OpenReportMethod.OpenReportByDefault, 0)
            Me.Subreports("saRevenuePostedSub.rpt").SetDataSource(MDataset.Tables("SummaryRevenuePosted"))
        End Sub
    End Class

    '
    '
    '
    <Serializable()>
    <CLSCompliant(True)> Public Class RevenuePostedSummaryReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\saRevenuePostedSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    ' 
    ' IPEDS Reports - class for reports
    '
    <CLSCompliant(True)> Public Class AdvantageReportIPEDS
        Inherits ReportDocument

        Private Const ParamTableName As String = "RptParams"

        Private m_RptDataSet As DataSet
        Private m_RptParamInfo As ReportParamInfoIPEDS

        <CLSCompliant(False)> Public Sub New(ByVal RptDataSet As DataSet, ByVal RptParamInfo As ReportParamInfoIPEDS)
            MyBase.New()

            ' store passed-in report data and param info
            Me.m_RptDataSet = RptDataSet.Copy
            Me.m_RptParamInfo = RptParamInfo

            ' add report parameters to report dataset
            SetRptParameters()

            ' load report document
            LoadRptDoc()

            ' set datasource of loaded report document
            SetRptDataSource()

            ' do any special setup required
            RptSpecialSetup()
        End Sub

        Private Sub SetRptParameters()
            ' add passed-in report parameters to report DataSet

            ' if report param table(s) have already been added, do nothing
            For Each dt As DataTable In Me.m_RptDataSet.Tables
                If dt.TableName.StartsWith(ParamTableName) Then
                    Exit Sub
                End If
            Next

            ' call appropriate routine to set specific report parameters
            Select Case Me.m_RptParamInfo.ResourceId
                Case IPEDSCommon.RptId_PartADetail
                    util_SetRptParameters_PartADetail()

                Case IPEDSCommon.RptId_PartASummary
                    util_SetRptParameters_PartASummary()

                Case IPEDSCommon.RptId_PartBDetail
                    util_SetRptParameters_PartBDetail()

                Case IPEDSCommon.RptId_PartBSummary
                    util_SetRptParameters_PartBSummary()

                Case IPEDSCommon.RptId_PartCSummary
                    util_SetRptParameters_PartCSummary()

                Case IPEDSCommon.RptId_PartF
                    util_SetRptParameters_PartF()

                Case IPEDSCommon.RptId_PartG
                    util_SetRptParameters_PartG()

                Case IPEDSCommon.RptId_GR2YR3Detail, IPEDSCommon.RptId_GR2YR3Summary,
                    IPEDSCommon.RptId_GRLT2YR3Detail, IPEDSCommon.RptId_GRLT2YR3Summary,
                    IPEDSCommon.RptId_GR4YR1Detail, IPEDSCommon.RptId_GR4YR1Summary,
                    IPEDSCommon.RptId_GR4YR2Detail, IPEDSCommon.RptId_GR4YR2Summary,
                    IPEDSCommon.RptId_GR4YR3Detail, IPEDSCommon.RptId_GR4YR3Summary
                    util_SetRptParameters_GradRateRpts()

                Case IPEDSCommon.RptId_FinAll
                    util_SetRptParameters_FinAll()

                Case IPEDSCommon.RptId_SFAPRDetail
                    util_SetRptParameters_SFAPRDetail()

                Case IPEDSCommon.RptId_SFAPRSummary
                    util_SetRptParameters_SFAPRSummary()

                Case IPEDSCommon.RptId_FCompDetail, IPEDSCommon.RptId_FCompSummary
                    util_SetRptParameters_FComp()

                Case IPEDSCommon.RptId_FICC3Detail, IPEDSCommon.RptId_FICC3Summary
                    util_SetRptParameters_FICC3()

                Case IPEDSCommon.RptId_FICC4Detail
                    util_SetRptParameters_FICC4Detail()

                Case IPEDSCommon.RptId_FICC4Summary
                    util_SetRptParameters_FICC4Summary()

                Case Else
                    util_SetRptParameters_General()
            End Select
        End Sub

        Private Sub util_SetRptParameters_PartADetail()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' no params for main report

            ' add params table for report Part 1
            ' create report Part 1 DataTable
            dtParams = New DataTable(ParamTableName & "Part1")

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("EnrollDescrip", GetType(String))
                .Add("DateDescrip", GetType(String))
                .Add("FilterProgramDescrips", GetType(String))
                .Add("StudentIDCaption", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("EnrollDescrip") = "ENROLLMENT " & .RptEndDate.Year
                If .SchoolRptType = IPEDSCommon.SchoolProgramReporter Then
                    drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                              IPEDSCommon.FmtRptDateDisplay(.CohortEndDate)
                Else
                    drParams("DateDescrip") = "ENROLLMENT AS OF " & IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                End If
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult1 IsNot Nothing, .FilterProgramDescripsMult1, "")
                drParams("StudentIDCaption") = .StudentIDCaption
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 2
            ' create report Part 2 DataTable
            dtParams = New DataTable(ParamTableName & "Part2")

            ' add columns 
            With dtParams.Columns
                .Add("StudentIDCaption", GetType(String))
            End With

            ' create DataRow with appropriate values
            drParams = dtParams.NewRow
            drParams("StudentIDCaption") = Me.m_RptParamInfo.StudentIDCaption

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 3
            ' create report Part 3 DataTable
            dtParams = New DataTable(ParamTableName & "Part3")

            ' add columns 
            With dtParams.Columns
                .Add("FilterProgramDescrips", GetType(String))
                .Add("StudentIDCaption", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult2 IsNot Nothing, .FilterProgramDescripsMult2, "")
                drParams("StudentIDCaption") = .StudentIDCaption
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 4
            ' create report Part 4 DataTable
            dtParams = New DataTable(ParamTableName & "Part4")

            ' add columns 
            With dtParams.Columns
                .Add("FilterProgramDescrips", GetType(String))
                .Add("StudentIDCaption", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult3 IsNot Nothing, .FilterProgramDescripsMult3, "")
                drParams("StudentIDCaption") = .StudentIDCaption
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)
        End Sub

        Private Sub util_SetRptParameters_PartASummary()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' no params for main report

            ' add params table for report Part 1
            ' create report Part 1 DataTable
            dtParams = New DataTable(ParamTableName & "Part1")

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("EnrollDescrip", GetType(String))
                .Add("DateDescrip", GetType(String))
                .Add("FilterProgramDescrips", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("EnrollDescrip") = "ENROLLMENT " & .RptEndDate.Year
                If .SchoolRptType = IPEDSCommon.SchoolProgramReporter Then
                    drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                              IPEDSCommon.FmtRptDateDisplay(.CohortEndDate)
                Else
                    drParams("DateDescrip") = "ENROLLMENT AS OF " & IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                End If
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult1 IsNot Nothing, .FilterProgramDescripsMult1, "")
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' no params for report Part 2


            ' add params table for report Part 3
            ' create report Part 3 DataTable
            dtParams = New DataTable(ParamTableName & "Part3")

            ' add columns 
            dtParams.Columns.Add("FilterProgramDescrips", GetType(String))

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult2 IsNot Nothing, .FilterProgramDescripsMult2, "")
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 4
            ' create report Part 4 DataTable
            dtParams = New DataTable(ParamTableName & "Part4")

            ' add columns 
            dtParams.Columns.Add("FilterProgramDescrips", GetType(String))

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult3 IsNot Nothing, .FilterProgramDescripsMult3, "")
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)
        End Sub

        Private Sub util_SetRptParameters_PartBDetail()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' no params for main report

            ' add params table for report Part 1
            ' create report Part 1 DataTable
            dtParams = New DataTable(ParamTableName & "Part1")

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("EnrollDescrip", GetType(String))
                .Add("DateDescrip", GetType(String))
                .Add("FilterProgramDescrips", GetType(String))
                .Add("StudentIDCaption", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("EnrollDescrip") = "ENROLLMENT " & .RptEndDate.Year
                If .SchoolRptType = IPEDSCommon.SchoolProgramReporter Then
                    drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                              IPEDSCommon.FmtRptDateDisplay(.CohortEndDate)
                Else
                    drParams("DateDescrip") = "ENROLLMENT AS OF " & IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                End If
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult1 IsNot Nothing, .FilterProgramDescripsMult1, "")
                drParams("StudentIDCaption") = .StudentIDCaption
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 2
            ' create report Part 2 DataTable
            dtParams = New DataTable(ParamTableName & "Part2")

            ' add columns 
            dtParams.Columns.Add("StudentIDCaption", GetType(String))

            ' create DataRow with appropriate values
            drParams = dtParams.NewRow
            drParams("StudentIDCaption") = Me.m_RptParamInfo.StudentIDCaption

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 3
            ' create report Part 3 DataTable
            dtParams = New DataTable(ParamTableName & "Part3")

            ' add columns 
            With dtParams.Columns
                .Add("FilterProgramDescrips", GetType(String))
                .Add("StudentIDCaption", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult2 IsNot Nothing, .FilterProgramDescripsMult2, "")
                drParams("StudentIDCaption") = .StudentIDCaption
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 4
            ' create report Part 4 DataTable
            dtParams = New DataTable(ParamTableName & "Part4")

            ' add columns 
            dtParams.Columns.Add("StudentIDCaption", GetType(String))

            ' create DataRow with appropriate values
            drParams = dtParams.NewRow
            drParams("StudentIDCaption") = Me.m_RptParamInfo.StudentIDCaption

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 5
            ' create report Part 5 DataTable
            dtParams = New DataTable(ParamTableName & "Part5")

            ' add columns 
            With dtParams.Columns
                .Add("FilterProgramDescrips", GetType(String))
                .Add("StudentIDCaption", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult3 IsNot Nothing, .FilterProgramDescripsMult3, "")
                drParams("StudentIDCaption") = .StudentIDCaption
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 6
            ' create report Part 6 DataTable
            dtParams = New DataTable(ParamTableName & "Part6")

            ' add columns 
            dtParams.Columns.Add("StudentIDCaption", GetType(String))

            ' create DataRow with appropriate values
            drParams = dtParams.NewRow
            drParams("StudentIDCaption") = Me.m_RptParamInfo.StudentIDCaption

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)

        End Sub

        Private Sub util_SetRptParameters_PartBSummary()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' no params for main report

            ' add params table for report Part 1
            ' create report Part 1 DataTable
            dtParams = New DataTable(ParamTableName & "Part1")

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("EnrollDescrip", GetType(String))
                .Add("DateDescrip", GetType(String))
                .Add("FilterProgramDescrips", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("EnrollDescrip") = "ENROLLMENT " & .RptEndDate.Year
                If .SchoolRptType = IPEDSCommon.SchoolProgramReporter Then
                    drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                              IPEDSCommon.FmtRptDateDisplay(.CohortEndDate)
                Else
                    drParams("DateDescrip") = "ENROLLMENT AS OF " & IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                End If
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult1 IsNot Nothing, .FilterProgramDescripsMult1, "")
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' no params for report Part 2


            ' add params table for report Part 3
            ' create report Part 3 DataTable
            dtParams = New DataTable(ParamTableName & "Part3")

            ' add columns 
            dtParams.Columns.Add("FilterProgramDescrips", GetType(String))

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult2 IsNot Nothing, .FilterProgramDescripsMult2, "")
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' no params for report Part 4


            ' add params table for report Part 5
            ' create report Part 5 DataTable
            dtParams = New DataTable(ParamTableName & "Part5")

            ' add columns 
            dtParams.Columns.Add("FilterProgramDescrips", GetType(String))

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("FilterProgramDescrips") = IIf(.FilterProgramDescripsMult3 IsNot Nothing, .FilterProgramDescripsMult3, "")
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' no params for report Part 6

        End Sub

        Private Sub util_SetRptParameters_PartCSummary()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' add params table for main report
            ' create main report DataTable
            dtParams = New DataTable(ParamTableName)

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("EnrollDescrip", GetType(String))
                .Add("DateDescrip", GetType(String))
                .Add("FilterProgramDescrips", GetType(String))
                .Add("DescripResUnknown", GetType(String))
                .Add("FIPSCodeResUnknown", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("EnrollDescrip") = "ENROLLMENT " & .RptEndDate.Year
                If .SchoolRptType = IPEDSCommon.SchoolProgramReporter Then
                    drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                              IPEDSCommon.FmtRptDateDisplay(.CohortEndDate)
                Else
                    drParams("DateDescrip") = "ENROLLMENT AS OF " & IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                End If
                drParams("FilterProgramDescrips") = .FilterProgramDescrips
            End With

            ' add values for "Residence Unknown/unreported"
            drParams("DescripResUnknown") = IPEDSCommon.StateResUnreportedDescrip
            drParams("FIPSCodeResUnknown") = IPEDSCommon.StateResUnreportedFIPSCode

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams)
        End Sub

        Private Sub util_SetRptParameters_PartF()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' add params table for main report
            ' create main report DataTable
            dtParams = New DataTable(ParamTableName)

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("EnrollDescrip", GetType(String))
                .Add("DateDescrip", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("EnrollDescrip") = "ENROLLMENT " & .RptEndDate.Year
                drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                          IPEDSCommon.FmtRptDateDisplay(.CohortEndDate)
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' HACK - temp disable of Report Part F, Part 1, pending changes in Advantage 
            '	to distinguish betweeen Clock Hour vs. Credit Hour courses

            '' add params table for report Part1
            '' create report Part 1 DataTable
            'dtParams = New DataTable(ParamTableName & "Part1")

            '' add columns 
            'With dtParams.Columns
            '	.Add("FilterProgramDescripsPart1", GetType(String))
            'End With

            '' create DataRow with appropriate values
            'With Me.m_RptParamInfo
            '	drParams = dtParams.NewRow
            '	drParams("FilterProgramDescripsPart1") = IIf(.FilterProgramDescripsMult1 IsNot Nothing, .FilterProgramDescripsMult1, "")
            'End With

            '' add DataRow to main report DataTable, add DataTable to report DataSet
            'dtParams.Rows.Add(drParams)
            'Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 2
            ' create report Part 2 DataTable
            dtParams = New DataTable(ParamTableName & "Part2")

            ' add columns 
            With dtParams.Columns
                .Add("FilterProgramDescripsPart2", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("FilterProgramDescripsPart2") = IIf(.FilterProgramDescripsMult2 IsNot Nothing, .FilterProgramDescripsMult2, "")
            End With

            ' add DataRow to report Part 2 DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 3
            ' create report Part 3 DataTable
            dtParams = New DataTable(ParamTableName & "Part3")

            ' add columns 
            With dtParams.Columns
                .Add("FilterProgramDescripsPart3", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("FilterProgramDescripsPart3") = IIf(.FilterProgramDescripsMult3 IsNot Nothing, .FilterProgramDescripsMult3, "")
            End With

            ' add DataRow to report Part 3 DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)
        End Sub

        Private Sub util_SetRptParameters_PartG()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' add params table for main report
            ' create main report DataTable
            dtParams = New DataTable(ParamTableName)

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("EnrollDescrip", GetType(String))
                .Add("DateDescrip", GetType(String))
                .Add("FilterProgramDescrips", GetType(String))
                .Add("StudentIDCaption", GetType(String))
                .Add("CohortStartDate", GetType(String))
                .Add("CohortEndDate", GetType(String))
                .Add("RptEndDate", GetType(String))
                .Add("EnrollAsOfDate", GetType(String))
                .Add("CohortYear", GetType(System.Int32))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("EnrollDescrip") = "ENROLLMENT " & .RptEndDate.Year
                If .SchoolRptType = IPEDSCommon.SchoolProgramReporter Then
                    drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                              IPEDSCommon.FmtRptDateDisplay(.CohortEndDate)
                Else
                    drParams("DateDescrip") = "ENROLLMENT AS OF " & IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                End If
                drParams("FilterProgramDescrips") = .FilterProgramDescrips
                drParams("StudentIDCaption") = .StudentIDCaption
                If .SchoolRptType = IPEDSCommon.SchoolProgramReporter Then
                    drParams("CohortStartDate") = IPEDSCommon.FmtRptDateParam(.CohortStartDate)
                    drParams("CohortEndDate") = IPEDSCommon.FmtRptDateParam(.CohortEndDate)
                    drParams("RptEndDate") = ""
                    drParams("EnrollAsOfDate") = IPEDSCommon.FmtRptDateParam(.CohortEndDate.AddYears(-1))
                    drParams("CohortYear") = .CohortEndDate.Year
                Else
                    drParams("CohortStartDate") = ""
                    drParams("CohortEndDate") = ""
                    drParams("RptEndDate") = IPEDSCommon.FmtRptDateParam(.RptEndDate)
                    drParams("EnrollAsOfDate") = IPEDSCommon.FmtRptDateParam(.RptEndDate.AddYears(-1))
                    drParams("CohortYear") = .RptEndDate.Year
                End If
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams)
        End Sub

        Private Sub util_SetRptParameters_GradRateRpts()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim IsDetailRpt As Boolean = Me.m_RptDataSet.Tables(0).Columns.Contains(IPEDSCommon.RptStuIdentifierColName)

            Dim StillEnrollYear As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' add params table for main report
            ' create main report DataTable
            dtParams = New DataTable(ParamTableName)

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("CohortDescrip", GetType(String))
                .Add("EnrollDescrip", GetType(String))
                .Add("CurStatusDescrip", GetType(String))
                .Add("FilterProgramDescrips", GetType(String))
                If IsDetailRpt Then
                    .Add("StudentIDCaption", GetType(String))
                End If
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("CohortDescrip") = "COHORT YEAR " & .CohortYear
                Select Case .CohortType
                    Case IPEDSCommon.CohortFullYear
                        drParams("EnrollDescrip") = "This report uses a Full Year Cohort: students enrolled during the period " &
                                                    IPEDSCommon.FmtRptDateParam(.CohortStartDate) & " through " &
                                                    IPEDSCommon.FmtRptDateParam(.CohortEndDate) & "."
                        'StillEnrollYear = .RptEndDate.Year + 2
                        'Modified by balaji on 11/05/2006
                        'As per IPEDS Spec, the as of date should be six years from Report End Date
                        StillEnrollYear = .RptEndDate.Year + 6
                    Case IPEDSCommon.CohortFall
                        drParams("EnrollDescrip") = "This report uses a Fall Cohort: students enrolled as of " &
                                                    IPEDSCommon.FmtRptDateParam(.RptEndDate) & "."
                        'StillEnrollYear = .RptEndDate.Year + 3
                        'Modified by balaji on 11/05/2006
                        'As per IPEDS Spec, the as of date should be six years from Report End Date
                        StillEnrollYear = .RptEndDate.Year + 6
                End Select
                drParams("CurStatusDescrip") = "This report shows the status of these students as of " &
                                               "8/31/" & StillEnrollYear & "."
                drParams("FilterProgramDescrips") = .FilterProgramDescrips
                If IsDetailRpt Then
                    drParams("StudentIDCaption") = .StudentIDCaption
                End If
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams)
        End Sub

        Private Sub util_SetRptParameters_FinAll()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' add params table for main report
            ' create main report DataTable
            dtParams = New DataTable(ParamTableName)

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("DateRangeDescrip", GetType(String))
                .Add("StudentIDCaption", GetType(String))
                .Add("ShowTuition", GetType(Boolean))
                .Add("ShowFees", GetType(Boolean))
                .Add("ShowPell", GetType(Boolean))
                .Add("ShowFSEOG", GetType(Boolean))
                .Add("ShowSSIG", GetType(Boolean))
                .Add("ShowScholarships", GetType(Boolean))
                .Add("ShowFellowships", GetType(Boolean))
                .Add("ShowTuitionWaiver", GetType(Boolean))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("DateRangeDescrip") = IPEDSCommon.FmtRptDateParam(.CohortStartDate) & " - " &
                                               IPEDSCommon.FmtRptDateParam(.RptEndDate)
                drParams("StudentIDCaption") = .StudentIDCaption
                drParams("ShowTuition") = .ShowTuition
                drParams("ShowFees") = .ShowFees
                With .FilterFundSourceDescrips
                    drParams("ShowPell") = .IndexOf("Pell") <> -1
                    drParams("ShowFSEOG") = .IndexOf("FSEOG") <> -1
                    drParams("ShowSSIG") = .IndexOf("SSIG") <> -1
                    drParams("ShowScholarships") = .IndexOf("Scholarships") <> -1
                    drParams("ShowFellowships") = .IndexOf("Fellowships") <> -1
                    drParams("ShowTuitionWaiver") = .IndexOf("Tuition Waiver") <> -1
                End With
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams)
        End Sub

        Private Sub util_SetRptParameters_SFAPRDetail()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' no params for main report

            ' add params table for report Part 1
            ' create report Part 1 DataTable
            dtParams = New DataTable(ParamTableName & "Part1")

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("CohortYearDescrip", GetType(String))
                .Add("EnrollDescrip", GetType(String))
                .Add("FilterProgramDescrips", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("CohortYearDescrip") = .CohortStartDate.Year & " - " & .RptEndDate.Year
                drParams("EnrollDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                            IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                drParams("FilterProgramDescrips") = .FilterProgramDescrips
            End With


            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 2
            ' create report Part 2 DataTable
            dtParams = New DataTable(ParamTableName & "Part2")

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("CohortYearDescrip", GetType(String))
                .Add("EnrollDescrip", GetType(String))
            End With

            ' create DataRow with appropriate values - these are exactly the same
            '	same as the params for Part 1, so take the values from that table
            drParams = dtParams.NewRow
            With Me.m_RptDataSet.Tables(ParamTableName & "Part1")
                drParams("SchoolName") = .Rows(0)("SchoolName")
                drParams("CohortYearDescrip") = .Rows(0)("CohortYearDescrip")
                drParams("EnrollDescrip") = .Rows(0)("EnrollDescrip")
            End With

            ' add DataRow to report Part 2 DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)
        End Sub

        Private Sub util_SetRptParameters_SFAPRSummary()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' add params table for main report
            ' create main report DataTable
            dtParams = New DataTable(ParamTableName)

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("CohortYearDescrip", GetType(String))
                .Add("EnrollDescrip", GetType(String))
                .Add("FilterProgramDescrips", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("CohortYearDescrip") = .CohortStartDate.Year & " - " & .RptEndDate.Year
                drParams("EnrollDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                            IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                drParams("FilterProgramDescrips") = .FilterProgramDescrips
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)
        End Sub

        Private Sub util_SetRptParameters_FComp()
            Dim IsDetailRpt As Boolean = Me.m_RptDataSet.Tables(0).Columns.Contains(IPEDSCommon.RptStuIdentifierColName)

            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' add params table for main report
            ' create main report DataTable
            dtParams = New DataTable(ParamTableName)

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("DateDescrip", GetType(String))
                If IsDetailRpt Then
                    .Add("StudentIDCaption", GetType(String))
                End If
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("DateDescrip") = "AWARDS/DEGREES CONFERRED BETWEEN " &
                                          IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " AND " &
                                          IPEDSCommon.FmtRptDateDisplay(.CohortEndDate)
                If IsDetailRpt Then
                    drParams("StudentIDCaption") = .StudentIDCaption
                End If
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams)
        End Sub

        Private Sub util_SetRptParameters_FICC3()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' add params table for main report
            ' create main report DataTable
            dtParams = New DataTable(ParamTableName)

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("DateDescrip", GetType(String))
                .Add("Is4Yr", GetType(Boolean))
                .Add("FilterProgramDescrips", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                ' HACK - report needs to be informed whether the school is a 4-year institution or not.
                '	For now, simply set to False, pending changes in Advantage allowing for the school and/or
                '	it's individual campuses to be designated as 4-year or not.
                drParams("Is4Yr") = False
                drParams("FilterProgramDescrips") = .FilterProgramDescrips
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams)
        End Sub

        Private Sub util_SetRptParameters_FICC4Detail()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' add params table for main report
            ' create main report DataTable
            dtParams = New DataTable(ParamTableName)

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("DateDescrip", GetType(String))
                .Add("Is4Yr", GetType(Boolean))
                .Add("FilterProgramDescrips", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                ' HACK - report needs to be informed whether the school is a 4-year institution or not.
                '	For now, simply set to False, pending changes in Advantage allowing for the school and/or
                '	it's individual campuses to be designated as 4-year or not.
                drParams("Is4Yr") = False
                drParams("FilterProgramDescrips") = .FilterProgramDescrips
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 1
            ' create report Part 1 DataTable
            dtParams = New DataTable(ParamTableName & "Part1")

            ' add columns 
            With dtParams.Columns
                .Add("StudentIdCaption", GetType(String))
            End With

            ' create DataRow with appropriate values
            drParams = dtParams.NewRow
            drParams("StudentIDCaption") = Me.m_RptParamInfo.StudentIDCaption

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' add params table for report Part 2
            ' create report Part 1 DataTable
            dtParams = New DataTable(ParamTableName & "Part2")

            ' add columns 
            With dtParams.Columns
                .Add("StudentIdCaption", GetType(String))
            End With

            ' create DataRow with appropriate values
            drParams = dtParams.NewRow
            drParams("StudentIDCaption") = Me.m_RptParamInfo.StudentIDCaption

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)


            ' no params for report Part 3

        End Sub

        Private Sub util_SetRptParameters_FICC4Summary()
            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' add params table for main report
            ' create main report DataTable
            dtParams = New DataTable(ParamTableName)

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("DateDescrip", GetType(String))
                .Add("Is4Yr", GetType(Boolean))
                .Add("FilterProgramDescrips", GetType(String))
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                ' HACK - report needs to be informed whether the school is a 4-year institution or not.
                '	For now, simply set to False, pending changes in Advantage allowing for the school and/or
                '	it's individual campuses to be designated as 4-year or not.
                drParams("Is4Yr") = False
                drParams("FilterProgramDescrips") = .FilterProgramDescrips
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams.Copy)
        End Sub

        Private Sub util_SetRptParameters_General()
            Dim IsDetailRpt As Boolean = Me.m_RptDataSet.Tables(0).Columns.Contains(IPEDSCommon.RptStuIdentifierColName)

            Dim dtParams As DataTable
            Dim drParams As DataRow

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ' add params table for main report
            ' create main report DataTable
            dtParams = New DataTable(ParamTableName)

            ' add columns 
            With dtParams.Columns
                .Add("SchoolName", GetType(String))
                .Add("EnrollDescrip", GetType(String))
                .Add("DateDescrip", GetType(String))
                .Add("FilterProgramDescrips", GetType(String))
                If IsDetailRpt Then
                    .Add("StudentIDCaption", GetType(String))
                End If
            End With

            ' create DataRow with appropriate values
            With Me.m_RptParamInfo
                drParams = dtParams.NewRow
                drParams("SchoolName") = MyAdvAppSettings.AppSettings("SchoolName").ToUpper
                drParams("EnrollDescrip") = "ENROLLMENT " & .RptEndDate.Year
                Select Case .ResourceId
                    Case IPEDSCommon.RptId_PartD
                        If .SchoolRptType = IPEDSCommon.SchoolProgramReporter Then
                            drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                                      IPEDSCommon.FmtRptDateDisplay(.CohortEndDate)
                        Else
                            drParams("DateDescrip") = "TOTAL ENTERING CLASS AS OF " &
                                                      IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                        End If
                    Case IPEDSCommon.RptId_PartEDetail
                        drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                                  IPEDSCommon.FmtRptDateDisplay(.CohortEndDate)
                    Case Else
                        If .SchoolRptType = IPEDSCommon.SchoolProgramReporter Then
                            drParams("DateDescrip") = IPEDSCommon.FmtRptDateDisplay(.CohortStartDate) & " THROUGH " &
                                                      IPEDSCommon.FmtRptDateDisplay(.CohortEndDate)
                        Else
                            drParams("DateDescrip") = "ENROLLMENT AS OF " & IPEDSCommon.FmtRptDateDisplay(.RptEndDate)
                        End If
                End Select
                drParams("FilterProgramDescrips") = .FilterProgramDescrips
                If IsDetailRpt Then
                    drParams("StudentIDCaption") = .StudentIDCaption
                End If
            End With

            ' add DataRow to main report DataTable, add DataTable to report DataSet
            dtParams.Rows.Add(drParams)
            Me.m_RptDataSet.Tables.Add(dtParams)
        End Sub

        Private Sub LoadRptDoc()
            ' load report document according to passed-in report title

            Dim RptFileName As String = "" ' avoid compiler warning

            Select Case Me.m_RptParamInfo.ResourceId
                Case IPEDSCommon.RptId_PartADetail
                    RptFileName = "arSFE_AllADetail.rpt"
                Case IPEDSCommon.RptId_PartASummary
                    RptFileName = "arSFE_AllASummary.rpt"
                Case IPEDSCommon.RptId_PartBDetail
                    RptFileName = "arSFE_AllBDetail.rpt"
                Case IPEDSCommon.RptId_PartBSummary
                    RptFileName = "arSFE_AllBSummary.rpt"
                Case IPEDSCommon.RptId_PartCDetail
                    RptFileName = "arSFE_AllCDetail.rpt"
                Case IPEDSCommon.RptId_PartCSummary
                    RptFileName = "arSFE_AllCSummary.rpt"
                Case IPEDSCommon.RptId_PartD
                    RptFileName = "arSFE_AllD.rpt"
                Case IPEDSCommon.RptId_PartEDetail
                    RptFileName = "arSFE_AllEDetail.rpt"
                Case IPEDSCommon.RptId_PartESummary
                    RptFileName = "arSFE_AllESummary.rpt"
                Case IPEDSCommon.RptId_PartF
                    RptFileName = "arSFE_AllF.rpt"
                Case IPEDSCommon.RptId_PartG
                    RptFileName = "arSFE_AllG.rpt"
                Case IPEDSCommon.RptId_GR2YR3Detail
                    RptFileName = "arSGR_2YR3Detail.rpt"
                Case IPEDSCommon.RptId_GR2YR3Summary
                    RptFileName = "arSGR_2YR3Summary.rpt"
                Case IPEDSCommon.RptId_GRLT2YR3Detail
                    RptFileName = "arSGR_LT2YR3Detail.rpt"
                Case IPEDSCommon.RptId_GRLT2YR3Summary
                    RptFileName = "arSGR_LT2YR3Summary.rpt"
                Case IPEDSCommon.RptId_GR4YR1Detail
                    RptFileName = "arSGR_4YR1Detail.rpt"
                Case IPEDSCommon.RptId_GR4YR1Summary
                    RptFileName = "arSGR_4YR1Summary.rpt"
                Case IPEDSCommon.RptId_GR4YR2Detail
                    RptFileName = "arSGR_4YR2Detail.rpt"
                Case IPEDSCommon.RptId_GR4YR2Summary
                    RptFileName = "arSGR_4YR2Summary.rpt"
                Case IPEDSCommon.RptId_GR4YR3Detail
                    RptFileName = "arSGR_4YR3Detail.rpt"
                Case IPEDSCommon.RptId_GR4YR3Summary
                    RptFileName = "arSGR_4YR3Summary.rpt"
                Case IPEDSCommon.RptId_FinAll
                    RptFileName = "arSFin_All.rpt"
                Case IPEDSCommon.RptId_SFAPRDetail
                    RptFileName = "arSFA_PRDetail.rpt"
                Case IPEDSCommon.RptId_SFAPRSummary
                    RptFileName = "arSFA_PRSummary.rpt"
                Case IPEDSCommon.RptId_FCompDetail
                    RptFileName = "saFComp_Detail.rpt"
                Case IPEDSCommon.RptId_FCompSummary
                    RptFileName = "saFComp_Summary.rpt"
                Case IPEDSCommon.RptId_FICC3Detail
                    RptFileName = "saFIC_C3Detail.rpt"
                Case IPEDSCommon.RptId_FICC3Summary
                    RptFileName = "saFIC_C3Summary.rpt"
                Case IPEDSCommon.RptId_FICC4Detail
                    RptFileName = "saFIC_C4Detail.rpt"
                Case IPEDSCommon.RptId_FICC4Summary
                    RptFileName = "saFIC_C4Summary.rpt"
            End Select

            Me.Load(HttpContext.Current.Server.MapPath("..\Reports\") & RptFileName)
        End Sub

        Private Sub SetRptDataSource()
            ' set datasource of loaded report document, and subreports, if any

            Select Case Me.m_RptParamInfo.ResourceId

                Case IPEDSCommon.RptId_PartADetail
                    SetSubRptDataSources(SFE_AllADetailObject.MainTableName)

                Case IPEDSCommon.RptId_PartASummary
                    SetSubRptDataSources(SFE_AllASummaryObject.MainTableName)

                Case IPEDSCommon.RptId_PartBDetail
                    SetSubRptDataSources(SFE_AllBDetailObject.MainTableName)

                Case IPEDSCommon.RptId_PartBSummary
                    SetSubRptDataSources(SFE_AllBSummaryObject.MainTableName)

                Case IPEDSCommon.RptId_PartF
                    ' pull needed data for main report
                    Dim dsWorking As New DataSet
                    With dsWorking.Tables
                        .Add(Me.m_RptDataSet.Tables(ParamTableName).Copy)
                        .Add(Me.m_RptDataSet.Tables(SFE_AllFObject.MainTableName & "Summary").Copy)
                    End With
                    ' set main report's DataSource to a copy of working DataSet
                    Me.SetDataSource(dsWorking.Copy)

                    SetSubRptDataSources(SFE_AllFObject.MainTableName)

                Case IPEDSCommon.RptId_SFAPRDetail
                    SetSubRptDataSources(SFA_PRDetailObject.MainTableName)

                Case IPEDSCommon.RptId_FICC4Detail
                    ' pull needed data for main report
                    Dim dsWorking As New DataSet
                    dsWorking.Tables.Add(Me.m_RptDataSet.Tables(ParamTableName).Copy)
                    ' set main report's DataSource to a copy of working DataSet
                    Me.SetDataSource(dsWorking.Copy)

                    SetSubRptDataSources(FIC_C4DetailObject.MainTableName)

                Case Else
                    ' no subreports, set datasource directly to report DataSet
                    Me.SetDataSource(Me.m_RptDataSet)

            End Select
        End Sub

        Private Sub SetSubRptDataSources(ByVal MainTableName As String)
            Dim dsWorking As New DataSet
            Dim SubreportName As String, SubRptParamTblName As String
            Dim i As Integer

            ' HACK - temp disable of Report Part F, Part 1, pending changes in Advantage 
            '	to distinguish betweeen Clock Hour vs. Credit Hour courses
            '	For Part F, skip over subreport #1
            Dim IsPartF As Boolean = Me.m_RptParamInfo.ResourceId = IPEDSCommon.RptId_PartF
            'For i = 1 To Me.Subreports.Count
            For i = IIf(Not IsPartF, 1, 2) To IIf(Not IsPartF, Me.Subreports.Count, Me.Subreports.Count + 1)
                ' set name of subreport and subreport's main table
                SubreportName = MainTableName & "Part" & i
                ' set name of subreport's parameter table
                SubRptParamTblName = ParamTableName & "Part" & i
                ' add tables to working DataSet
                With dsWorking.Tables
                    ' remove any previous tables 
                    .Clear()
                    ' if the report DataSet contains a parameter table for the subreport, 
                    '	add the parameter table to the working DataSet
                    If Me.m_RptDataSet.Tables.Contains(SubRptParamTblName) Then
                        .Add(Me.m_RptDataSet.Tables(SubRptParamTblName).Copy)
                    End If
                    ' add subreport's main table to working DataSet
                    .Add(Me.m_RptDataSet.Tables(SubreportName).Copy)
                End With
                ' set subreport's DataSource to copy of working DataSet
                Me.Subreports(SubreportName).SetDataSource(dsWorking.Copy)
            Next
        End Sub

        Private Sub RptSpecialSetup()
            ' do any special setup required by individual report

            Select Case Me.m_RptParamInfo.ResourceId

                Case IPEDSCommon.RptId_FinAll
                    Dim drParams As DataRow = Me.m_RptDataSet.Tables(ParamTableName).Rows(0)
                    Dim secPageHdr As Section = Me.ReportDefinition.Sections.Item("secPageHdr")
                    Dim secDetail As Section = Me.ReportDefinition.Sections.Item("secDetail")
                    Dim secRptFtr As Section = Me.ReportDefinition.Sections.Item("secRptFtr")
                    Dim i As Integer

                    For i = 1 To 8
                        secPageHdr.ReportObjects("linePageHdrCol" & i).ObjectFormat.EnableSuppress = True
                        secDetail.ReportObjects("lineDetailCol" & i).ObjectFormat.EnableSuppress = True
                        secRptFtr.ReportObjects("lineRptFtrCol" & i).ObjectFormat.EnableSuppress = True
                    Next

                    If CBool(drParams("ShowTuition")) Then
                        secPageHdr.ReportObjects("linePageHdrCol1").ObjectFormat.EnableSuppress = False
                        secPageHdr.ReportObjects("linePageHdrCol2").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol1").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol2").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol1").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol2").ObjectFormat.EnableSuppress = False
                    End If
                    If CBool(drParams("ShowFees")) Then
                        secPageHdr.ReportObjects("linePageHdrCol2").ObjectFormat.EnableSuppress = False
                        secPageHdr.ReportObjects("linePageHdrCol3").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol2").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol3").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol2").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol3").ObjectFormat.EnableSuppress = False
                    End If
                    If CBool(drParams("ShowPell")) Then
                        secPageHdr.ReportObjects("linePageHdrCol3").ObjectFormat.EnableSuppress = False
                        secPageHdr.ReportObjects("linePageHdrCol4").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol3").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol4").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol3").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol4").ObjectFormat.EnableSuppress = False
                    End If
                    If CBool(drParams("ShowFSEOG")) Then
                        secPageHdr.ReportObjects("linePageHdrCol4").ObjectFormat.EnableSuppress = False
                        secPageHdr.ReportObjects("linePageHdrCol5").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol4").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol5").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol4").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol5").ObjectFormat.EnableSuppress = False
                    End If
                    If CBool(drParams("ShowSSIG")) Then
                        secPageHdr.ReportObjects("linePageHdrCol5").ObjectFormat.EnableSuppress = False
                        secPageHdr.ReportObjects("linePageHdrCol6").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol5").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol6").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol5").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol6").ObjectFormat.EnableSuppress = False
                    End If
                    If CBool(drParams("ShowScholarships")) Then
                        secPageHdr.ReportObjects("linePageHdrCol6").ObjectFormat.EnableSuppress = False
                        secPageHdr.ReportObjects("linePageHdrCol7").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol6").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol7").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol6").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol7").ObjectFormat.EnableSuppress = False
                    End If
                    If CBool(drParams("ShowFellowships")) Then
                        secPageHdr.ReportObjects("linePageHdrCol7").ObjectFormat.EnableSuppress = False
                        secPageHdr.ReportObjects("linePageHdrCol8").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol7").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol8").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol7").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol8").ObjectFormat.EnableSuppress = False
                    End If
                    If CBool(drParams("ShowTuitionWaiver")) Then
                        secPageHdr.ReportObjects("linePageHdrCol8").ObjectFormat.EnableSuppress = False
                        secDetail.ReportObjects("lineDetailCol8").ObjectFormat.EnableSuppress = False
                        secRptFtr.ReportObjects("lineRptFtrCol8").ObjectFormat.EnableSuppress = False
                    End If

                    ' future code to handle other reports

            End Select

        End Sub

    End Class

    <Serializable()>
    <CLSCompliant(True)> Public Class StudentProgressReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\arStuProgressReport.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    <Serializable()>
    <CLSCompliant(True)> Public Class StudentProgressReportSummary
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\arStuProgressReportSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
            'Me.Load(m_path + "\Reports\arStuProgressReportSummary_BSA.rpt", OpenReportMethod.OpenReportByDefault, 0)

            'Me.Load(m_path + "\Reports\arStuProgressReport.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    <Serializable()>
    <CLSCompliant(True)> Public Class MultiProgressReportSummary
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\arMultiProgressReportSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class ClockHourTranscript
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\arClockHrTranscriptSummary.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    'Public Class StudentSigninSheet
    '    Inherits AdvantageReport

    '    Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
    '        MyBase.New()
    '        m_dataset = ds
    '        m_rptParamInfo = paramInfo
    '        If m_dataset.Tables.Count = 2 Then SetReportParameters(False)
    '        Me.Load(m_path + "\Reports\arSigninSheet.rpt", OpenReportMethod.OpenReportByDefault, 0)
    '    End Sub
    'End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentSigninSheet
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters(True)
            Me.Load(MPath + "\Reports\arSigninSheetSub.rpt", OpenReportMethod.OpenReportByDefault, 0)

            Dim subreportName As String
            Dim subreportObject As SubreportObject
            Dim subreport As New ReportDocument
            For Each rptObject As ReportObject In Me.ReportDefinition.ReportObjects
                ' Get the ReportObject by name and cast it as a SubreportObject.
                If TypeOf (rptObject) Is SubreportObject Then
                    subreportObject = rptObject
                    ' Get the subreport name.
                    subreportName = subreportObject.SubreportName
                    ' Open the subreport as a ReportDocument.
                    subreport = Me.OpenSubreport(subreportName)
                    subreport.SetDataSource(ds.Tables(1))
                End If
            Next
        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class ClassSectionAttendance
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "ClassStudentAttendance"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\atClsSectAttedance2.rpt", OpenReportMethod.OpenReportByDefault, 0)

        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class AttendanceByDay
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "StudentClockAttendance"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\arStuClockAttendance.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class MultipleStudentLedger
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters(True)
            Me.Load(MPath + "\Reports\saMultipleStuLedger.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub

    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class GradeBookResultsReport
        Inherits AdvantageReport
        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "GradeBookResults"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(paramInfo.ResId.ToString)
            Me.Load(MPath + "\Reports\arGradeBookResultsReport.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class AttendanceHistoryReport
        Inherits AdvantageReport
        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "AttendanceHistory"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(paramInfo.ResId.ToString)
            Me.Load(MPath + "\Reports\atAttendanceHistory.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class ExternshipAttendanceReport
        Inherits AdvantageReport
        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "AttendanceHistory"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(paramInfo.ResId.ToString)
            Me.Load(MPath + "\Reports\atExternshipAttendance.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class DictationTestReport
        Inherits AdvantageReport
        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "SpeedTest"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(paramInfo.ResId.ToString)
            Me.Load(MPath + "\Reports\StuDictationTest.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class IndividualSAPReport
        Inherits AdvantageReport
        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "IndividualSAPReport"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(paramInfo.ResId.ToString)
            Me.Load(MPath + "\Reports\arIndividualSAPReport.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class
    
    <Serializable()>
    <CLSCompliant(True)> Public Class TimeClockExceptionReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "StudentDetails"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\TimeClockException.rpt", OpenReportMethod.OpenReportByDefault, 0)


        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class PrintTimeClockPunchesReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "dtTimeClockPunches"
            MDataset.Tables(1).TableName = "dtClockAttHours"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters()
            Me.Load(MPath + "\Reports\PrintTimeClockPunchesReport.rpt", OpenReportMethod.OpenReportByDefault, 0)


        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class WeeklyAttendanceReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "dsWeeklyAttend"
            MDataset.Tables(1).TableName = "dsWeeklyAttend_Sub"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters()
            Me.Load(MPath + "\Reports\WeeklyAttendanceRep.rpt", OpenReportMethod.OpenReportByDefault, 0)


        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class PrintAttendanceSheet
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "dsAttendSheet"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count <= 4 Then SetReportParameters()
            Me.Load(MPath + "\Reports\PrintAttendSheet.rpt", OpenReportMethod.OpenReportByDefault, 0)


        End Sub

    End Class

    'Added by Vijay Ramteke on Feb 22, 2010
    <Serializable()>
    <CLSCompliant(True)> Public Class ExpectedDisbursementReport
        Inherits AdvantageReport
        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "ExpectedDisbursement"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\faExpectedDisbursement.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub
    End Class

    <Serializable()>
    <CLSCompliant(True)> Public Class AttendanceHistoryMultipleStudent
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "StudentDetails"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters()
            'Me.Load(m_path + "\Reports\testtimeclock.rpt", OpenReportMethod.OpenReportByDefault, 0)
            Me.Load(MPath + "\Reports\AttendHistoryMultiple.rpt", OpenReportMethod.OpenReportByDefault, 0)



        End Sub
    End Class

    <Serializable()>
    <CLSCompliant(True)> Public Class AttendanceHistorySingleStudent
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MDataset.Tables(0).TableName = "StudentDetails"
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 2 Then SetReportParameters()
            ' Me.Load(m_path + "\Reports\testtimeclock.rpt", OpenReportMethod.OpenReportByDefault, 0)
            Me.Load(MPath + "\Reports\AttedanceHistorySingle.rpt", OpenReportMethod.OpenReportByDefault, 0)



        End Sub
    End Class
    '--code added by Priyanka on date 13th of May 2009
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentCoursesDropReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            ''New Code Added By Vijay Ramteke On September 28, 2010 For Mantis Id 17685
            ''Me.Load(m_path + "\Reports\arStudentCourseDrops.rpt", OpenReportMethod.OpenReportByDefault, 0)
            If paramInfo.ShowLDA = True Then
                Me.Load(MPath + "\Reports\arStudentCourseDropsWithLDA.rpt", OpenReportMethod.OpenReportByDefault, 0)
            Else
                Me.Load(MPath + "\Reports\arStudentCourseDrops.rpt", OpenReportMethod.OpenReportByDefault, 0)
            End If
            ''New Code Added By Vijay Ramteke On September 28, 2010 For Mantis Id 17685
        End Sub
    End Class
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentGPAReport
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters()
            Me.Load(MPath + "\Reports\arStudentGPA.rpt", OpenReportMethod.OpenReportByDefault, 0)


        End Sub

    End Class
    ''New Code Added By Vijay Ramteke On July 09, 2010 For Mantis Id 11856
    <Serializable()>
    <CLSCompliant(True)> Public Class ConsecutiveAbsentDays
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\arConsecutiveAbsentDays.rpt", OpenReportMethod.OpenReportByDefault, 0)


        End Sub
    End Class
    ''New Code Added By Vijay Ramteke On July 09, 2010 For Mantis Id 11856
    ''New Code Added By Vijay Ramteke On July 10, 2010 For Mantis Id 06156
    <Serializable()>
    <CLSCompliant(True)> Public Class LOAExpiration
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters()
            Me.Load(MPath + "\Reports\arPendingLOAExpiration.rpt", OpenReportMethod.OpenReportByDefault, 0)


        End Sub
    End Class
    ''New Code Added By Vijay Ramteke On July 10, 2010 For Mantis Id 06156
    ''New Code Added By Vijay Ramteke On July 22, 2010
    <Serializable()>
    <CLSCompliant(True)> Public Class RevenueRatioDetailForStudent9010
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            ''New Code Added By Vijay Ramteke On September 05, 2010
            ''If m_dataset.Tables.Count = 1 Then SetReportParameters()
            If MDataset.Tables.Count = 2 Then SetReportParameters()
            ''New Code Added By Vijay Ramteke On September 05, 2010
            Me.Load(MPath + "\Reports\faRevenueRatioDetailForStudent9010.rpt", OpenReportMethod.OpenReportByDefault, 0)


        End Sub
    End Class
    ''New Code Added By Vijay Ramteke On July 22, 2010
    <Serializable()>
    <CLSCompliant(True)> Public Class SummaryReportByStudent9010
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 3 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\faSummaryReportByStudent9010.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub

        Protected Overrides Sub SetReportParameters(ByVal useStudentIdLabel As Boolean)
            MyBase.SetReportParameters(useStudentIdLabel)
            '   Add an additional column for the report title, because the revenue ratio might change. 
            '   Need to change the report title based on the web.config variable: RevenueRatio.
            Dim dtRptParams As DataTable = MDataset.Tables("ReportParams")
            dtRptParams.Columns.Add("RptTitle", System.Type.GetType("System.String"))
            dtRptParams.Rows(0)("RptTitle") = MRptParamInfo.RptTitle.ToUpper
        End Sub
    End Class
    ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 17901
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentByProgram
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 6 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\arStudentByProgram.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub

        Protected Overrides Sub SetReportParameters(ByVal useStudentIdLabel As Boolean)
            MyBase.SetReportParameters(useStudentIdLabel)
            '   Add an additional column for the report title, because the revenue ratio might change. 
            '   Need to change the report title based on the web.config variable: RevenueRatio.
            Dim dtRptParams As DataTable = MDataset.Tables("ReportParams")
            dtRptParams.Columns.Add("RptTitle", Type.GetType("System.String"))
            dtRptParams.Rows(0)("RptTitle") = MRptParamInfo.RptTitle.ToUpper
        End Sub

    End Class
    ''New Code Added By Vijay Ramteke On September 09, 2010 For Mantis Id 17901
    ''New Code Added By Vijay Ramteke On September 25, 2010 For Mantis Id 19129
    <Serializable()>
    <CLSCompliant(True)> Public Class StudentAppliedPayments
        Inherits AdvantageReport

        <CLSCompliant(False)> Public Sub New(ByVal ds As DataSet, ByVal paramInfo As ReportParamInfo)
            MyBase.New()
            MDataset = ds
            MRptParamInfo = paramInfo
            If MDataset.Tables.Count = 1 Then SetReportParameters(False)
            Me.Load(MPath + "\Reports\saStudentAppliedPayments.rpt", OpenReportMethod.OpenReportByDefault, 0)
        End Sub

        Protected Overrides Sub SetReportParameters(ByVal useStudentIdLabel As Boolean)
            MyBase.SetReportParameters(useStudentIdLabel)
            '   Add an additional column for the report title, because the revenue ratio might change. 
            '   Need to change the report title based on the web.config variable: RevenueRatio.
            Dim dtRptParams As DataTable = MDataset.Tables("ReportParams")
            dtRptParams.Columns.Add("RptTitle", System.Type.GetType("System.String"))
            dtRptParams.Rows(0)("RptTitle") = MRptParamInfo.RptTitle.ToUpper
        End Sub
    End Class
    ''New Code Added By Vijay Ramteke On September 25, 2010 For Mantis Id 19129
End Namespace