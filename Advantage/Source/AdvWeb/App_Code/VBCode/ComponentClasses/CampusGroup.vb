
Imports FAME.Common
Imports System.Data
Imports System.Data.OleDb
Imports FAME.Advantage.Common

Namespace FAME.AdvantageV1.BusinessLayer.CampusGroup

    Public Class CampusGroup
        Public Function GetCampuses() As DataSet
            Dim db As New DataAccessLayer.DataAccess
            Dim strSQL As String
            Dim ds As DataSet

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            strSQL = "SELECT CampusID,CampDescrip FROM syCampuses ORDER BY CampDescrip"
            db.ConnectionString = CType(myAdvAppSettings.AppSettings("ConString"), String)

            ds = db.RunSQLDataSet(strSQL)

            'Close Connection
            db.CloseConnection()

            Return ds

        End Function


        Public Function InsertCampGroup(ByVal strGuid As String, ByVal Code As String, ByVal sStatusId As String, ByVal Description As String, ByVal user As String) As String
            Dim db As New FAME.DataAccessLayer.DataAccess
            Dim sb As New System.Text.StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            Try
                With sb
                    .Append("INSERT INTO syCampGrps(CampGrpId,CampGrpCode,StatusId,CampGrpDescrip,ModUser,ModDate) ")
                    .Append("VALUES(?,?,?,?,?,?)")
                End With

                db.AddParameter("@campgrpid", strGuid, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@campgrpcode", Code, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@statusid", sStatusId, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@campgrpdescrip", Description, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 80, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                '   return without errors
                Return ""
            Catch ex As OleDbException
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)


                'DisplayOleDbErrorCollection(ex.GetBaseException())

                '   return Message
                Return DALExceptions.BuildErrorMessage(ex)

            'Catch ex As OleDbException
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    '   return an error to the client
            '    Return ex.Message
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                'Close Connection
                db.CloseConnection()
            End Try


        End Function

        Public Function InsertCampusCampGrp(ByVal strGuid As String, ByVal ID As String, ByVal user As String) As String
            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = CType(myAdvAppSettings.AppSettings("ConString"), String)

            With sb
                .Append("INSERT INTO syCmpGrpCmps(CampGrpId,CampusId,ModUser,ModDate) ")
                .Append("VALUES(?,?,?,?)")
            End With

            db.AddParameter("@campgrpid", strGuid, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campusid", ID, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccessLayer.DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            Return Nothing
        End Function

        Public Function UpdateCampGroup(ByVal Code As String, ByVal sStatusId As String, ByVal Description As String, ByVal storedGuid As String, ByVal user As String) As String
            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = CType(myAdvAppSettings.AppSettings("ConString"), String)
            Try
                With sb
                    .Append("UPDATE syCampGrps ")
                    .Append("SET CampGrpCode=?,")
                    .Append("StatusId=?,")
                    .Append("CampGrpDescrip=?, ")
                    .Append("ModUser=?, ")
                    .Append("ModDate=? ")
                    .Append("WHERE CampGrpId =? ")
                End With
                db.AddParameter("@campgrpcode", Code, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@statusid", sStatusId, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@descrip", Description, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 135, ParameterDirection.Input)
                db.AddParameter("@ModUser", user, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
                db.AddParameter("@ModDate", Date.Now, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                db.AddParameter("@campgrpid", storedGuid, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                '   return without errors
                Return ""
            Catch ex As UniqueIndexException
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)


                'DisplayOleDbErrorCollection(ex.GetBaseException())

                '   return Message
                Return DALExceptions.BuildUniqueIndexExceptionMessage(ex)

            Catch ex As OleDbException
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                '   return an error to the client
                Return ex.Message
            Finally
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                'Close Connection
                db.CloseConnection()
            End Try

        End Function

        Public Sub UpdateRowAdded(ByVal storedGuid As String, ByVal iCampusId As String, ByVal user As String)

            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = CType(myAdvAppSettings.AppSettings("ConString"), String)
            'Insert row into syCampGrps table
            With sb
                .Append("INSERT INTO syCmpGrpCmps(CampGrpId,CampusId,ModUser,ModDate) ")
                .Append("VALUES(?,?,?,?)")
            End With

            db.AddParameter("@campgrpid", storedGuid, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campusid", iCampusId, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@ModUser", user, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)
            db.AddParameter("@ModDate", Date.Now, DataAccessLayer.DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            'Return Nothing
        End Sub

        Public Sub UpdateRowDeleted(ByVal storedGuid As String, ByVal iCampusId As String)

            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New StringBuilder

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = CType(myAdvAppSettings.AppSettings("ConString"), String)
            'Try
            With sb
                .Append("DELETE FROM syCmpGrpCmps ")
                .Append("WHERE CampGrpId = ? ")
                .Append("AND CampusId = ? ")
            End With

            db.AddParameter("@campgrpid", storedGuid, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.AddParameter("@campusid", iCampusId, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            db.RunParamSQLExecuteNoneQuery(sb.ToString)
            db.ClearParameters()
            sb.Remove(0, sb.Length)
            'Return Nothing
        End Sub

        Public Function DeleteCampGroup(ByVal sCampGrpId As String) As String
            Dim db As New FAME.DataAccessLayer.DataAccess
            Dim sb As New System.Text.StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = CType(MyAdvAppSettings.AppSettings("ConString"), String)

            '   we must encapsulate all DB updates in one transaction
            Dim groupTrans As OleDbTransaction = db.StartTransaction()

            Try
                'Delete from database and clear the screen as well. 
                'And set the mode to "NEW" since the record has been deleted from the db.
                With sb
                    .Append("DELETE FROM syCmpGrpCmps ")
                    .Append("WHERE CampGrpId =  ? ")
                End With
                db.AddParameter("@campgrpid", sCampGrpId, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)

                db.ClearParameters()
                sb.Remove(0, sb.Length)



                With sb
                    .Append("DELETE FROM syCampGrps ")
                    .Append("WHERE CampGrpId =  ? ")
                End With
                db.AddParameter("@campgrpid", sCampGrpId, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString, groupTrans)
                '   commit transaction 
                groupTrans.Commit()

                '   return without errors
                Return ""
                'Catch ex As System.Exception
                 '	Dim exTracker = new AdvApplicationInsightsInitializer()
                '	exTracker.TrackExceptionWrapper(ex)

                '    'Return an Error To Client
                '    Return -1
            Catch ex As OleDbException
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                groupTrans.Rollback()
                '   do not report sql lost connection
                If Not groupTrans.Connection Is Nothing Then
                    ' Report the error
                End If
                '   return an error to the client
                Return DALExceptions.BuildErrorMessage(ex)
            Finally
                'Close Connection
                db.CloseConnection()
            End Try

        End Function

        Public Function GetCampusesOnItemCmd(ByVal strGuid As String) As DataSet
            Dim db As New FAME.DataAccessLayer.DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim da As OleDbDataAdapter
            Dim strSQL As String
            Dim ds As New DataSet

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

            'Build query to obtain Campuses already assigned to the selected campus group
            strSQL = "SELECT t1.CampusId, t2.CampDescrip,t1.ModUser,t1.ModDate  " & _
                     "FROM syCmpGrpCmps t1, syCampuses t2 " & _
                     "WHERE t1.CampGrpId = '" & strGuid & "'" & _
                     "AND t1.CampusId = t2.CampusId " & _
                     "ORDER BY t2.CampDescrip"

            db.ConnectionString = "ConString"
            db.OpenConnection()
            da = db.RunParamSQLDataAdapter(strSQL)
            da.Fill(ds, "FldsSelectCamps")


            'Build query to obtain Campuses not yet assigned to the selected campus group       
            strSQL = "SELECT t1.CampusId,t1.CampDescrip " & _
                     "FROM syCampuses t1 " & _
                     "WHERE NOT EXISTS " & _
                     "(SELECT t2.CampusId FROM syCmpGrpCmps t2 WHERE t2.CampGrpId = '" & strGuid & "' AND t1.CampusId = t2.CampusId)" & _
                     "ORDER BY t1.CampDescrip"

            da = db.RunParamSQLDataAdapter(strSQL)
            da.Fill(ds, "FldsAvailCamps")

            'Close Connection
            db.CloseConnection()

            Return ds
        End Function
    End Class

End Namespace
