Imports System
Imports System.Data
Imports System.Data.OleDb
Imports System.Xml
Imports System.Collections
Imports System.Diagnostics
Imports FAME.ExceptionLayer
Imports Microsoft.VisualBasic
Imports FAME.Advantage.Common

Namespace FAME.DataAccessLayer
    Public NotInheritable Class DataAccess
        Implements IDisposable

#Region "Private Variables and Objects"
        'ADO.NET Objects needed for data access
        Private MyAdvAppSettings As AdvAppSettings = GetAdvAppSettings()
        Private m_Connection As OleDbConnection
        Private m_Command As OleDbCommand
        Private m_DataReader As OleDbDataReader
        Private m_XMLReader As XmlReader
        Private m_DataAdapter As OleDbDataAdapter
        Private m_DataSet As DataSet
        Private m_Object As Object

        Public m_PageAddress As String
        'We are going to handle parameters internally and this array is going to hold them
        Private m_ParameterList As ArrayList = New ArrayList()

        'Connection string variables
        Private m_ConnectionString As String = MyAdvAppSettings.AppSettings("ConString")
        Private m_Server As String
        Private m_Database As String
        Private m_UserName As String
        Private m_Password As String

        'Naming, Cleanup and Exception variables
        Private m_ModuleName As String
        Private m_DisposedBoolean As Boolean
        Private Const m_ExceptionMessage As String = "Data Application Error. Detail Error Information can be found in the Application Log"

        Public Enum OleDbDataType
            OleDbString
            OleDbChar
            OleDbInteger
            OleDbDateTime
            OleDbDecimal
            OleDbMoney
            'added Enum for Guid Datatype - Corey Masson - Jan 7, 2004
            OleDbGuid
        End Enum


#End Region

#Region "Public Constructors"
        Public Sub New()
            MyBase.New()

            m_ModuleName = Me.GetType.ToString
        End Sub

#End Region

#Region "Public Properties"
        'ConnectionString
        Public Property ConnectionString() As String
            Get
                Try
                    Return m_Connection.ConnectionString
                Catch
                    Return ""
                End Try
            End Get
            Set(ByVal Value As String)
                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'If Value = "ConString" Then
                'm_ConnectionString = "Provider=SQLOLEDB;Data Source=FAMEDEV;Initial Catalog=AdvantageV1;uid=sa;pwd=fame.fame;Application Name=Advantage"
                'ElseIf Value = "ConStringAuditing" Then
                '    m_ConnectionString = "Provider=SQLOLEDB;Data Source=FAMEDEV;Initial Catalog=AdvantageV1;uid=sa;pwd=fame.fame;Application Name=Advantage"
                'ElseIf Value = "ConStringMaster" Then
                '    m_ConnectionString = "Provider=SQLOLEDB;Data Source=FAMEDEV;Initial Catalog=AdvantageV1Master;uid=sa;pwd=fame.fame;Application Name=Advantage"
                'Else
                '    m_ConnectionString = Value
                'End If
                'I left this logic in just in case there was any logic that used the property
                If Value = "ConString" Then
                    m_ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                ElseIf Value = "ConStringAuditing" Then
                    m_ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                ElseIf Value = "ConStringMaster" Then
                    m_ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                Else
                    m_ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                End If
            End Set
        End Property

        Public ReadOnly Property Connection() As OleDbConnection
            Get
                m_Connection = New OleDbConnection(m_ConnectionString)
                Return m_Connection
            End Get
        End Property

#End Region

#Region "Public Methods"
        Public Function StartTransaction() As OleDbTransaction
            m_Connection = New OleDbConnection(m_ConnectionString)
            m_Connection.Open()
            Return m_Connection.BeginTransaction()
        End Function
#End Region

#Region "DataAccess Methods"

#Region "Close connection method"
        Public Sub CloseConnection()
            m_Connection.Close()
        End Sub
#End Region

#Region "Open connection method"
        Public Sub OpenConnection()
            m_Connection = New OleDbConnection(m_ConnectionString)
            m_Connection.Open()
        End Sub
#End Region

#Region "RunSQLScalar - Standard SQL is run with ExecuteScalar"
        'The RunSQLScalar function accepts a SQL statement that is required 
        Public Function RunSQLScalar(ByVal SQL As String) As Object
            'Validate the SQL String to be larger than 10 characters
            ValidateSQLStatement(SQL)

            'We include all called object in the try block to catch any excepitons that could occur ( even in the creation of the connection)
            Try
                'Check to see if this object has already been disposed
                If m_DisposedBoolean = True Then
                    Throw New ObjectDisposedException(m_ModuleName, "This object has already been disposed. You cannot reuse it.")
                End If

                'Set a new Connecton
                m_Connection = New OleDbConnection(m_ConnectionString)
                'Set a new Command that accepts an SQL statement and the connection. 
                'The command.commandtype does not have to be set since it defaults to text
                m_Command = New OleDbCommand(SQL, m_Connection)
                'ExecuteScalar
                m_Object = m_Command.ExecuteScalar
                Return m_Object
            Catch ExceptionObject As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ExceptionObject)

                'Any exception will be logged through our private logexception function
                'LogException(ExceptionObject)
                'The exception is passed back to the calling code, with our custom message and specific exception information
                Throw New Exception(m_ExceptionMessage, ExceptionObject)
            Finally
                'Immediately Close the Connection after use to free up resources
                m_Connection.Close()
            End Try

        End Function
#End Region

#Region "RunSQLDataSet - Standard SQL is run and returns a DataSet"

        'The runSQLDataSet function accepts a SQL statement that is required and an optional table name
        Public Function RunSQLDataSet(ByVal SQL As String, Optional ByVal TableName As String = Nothing) As DataSet

            'Validate the SQL String to be larger than 10 characters
            ValidateSQLStatement(SQL)

            'We include all called object in the try block to catch any excepitons that could occur ( even in the creation of the connection)
            Try
                'Check to see if this object has already been disposed
                If m_DisposedBoolean = True Then
                    Throw New ObjectDisposedException(m_ModuleName, "This object has already been disposed. You cannot reuse it.")
                End If

                'Set a new Connecton
                m_Connection = New OleDbConnection(m_ConnectionString)
                'Set a new Command that accepts an SQL statement and the connection. 
                'The command.commandtype does not have to be set since it defaults to text
                m_Command = New OleDbCommand(SQL, m_Connection)
                'Set a new DataSet
                m_DataSet = New DataSet
                'Set a new DataAdapter that will run the SQL statement
                m_DataAdapter = New OleDbDataAdapter(m_Command)
                'Depending on table name passed, Fill the new DataSet with the returned Data in a table
                If TableName = Nothing Then
                    m_DataAdapter.Fill(m_DataSet)
                Else
                    m_DataAdapter.Fill(m_DataSet, TableName)
                End If

                Return m_DataSet

            Catch ExceptionObject As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ExceptionObject)

                'Any exception will be logged through our private logexception function
                'LogException(ExceptionObject)
                'The exception is passed back to the calling code, with our custom message and specific exception information
                Throw New Exception(m_ExceptionMessage, ExceptionObject)
            Finally
                'Immediately Close the Connection after use to free up resources
                m_Connection.Close()
            End Try

        End Function

#End Region

#Region "runSQLDataReader - Standard SQL is run and returns a DataReader"

        'The runSQLDataReader function accepts a SQL statement that is required
        Public Function RunSQLDataReader(ByVal SQL As String) As OleDbDataReader

            'Validate the SQL String to be larger than 10 characters
            ValidateSQLStatement(SQL)

            'We include all called object in the try block to catch any excepitons that could occur ( even in the creation of the connection)
            Try
                'Check to see if this object has already been disposed
                If m_DisposedBoolean = True Then
                    Throw New ObjectDisposedException(m_ModuleName, "This object has already been disposed. You cannot reuse it.")
                End If

                'Set a new Connecton
                m_Connection = New OleDbConnection(m_ConnectionString)
                'Set a new Command that accepts an SQL statement and the connection. 
                'The command.commandtype does not have to be set since it defaults to text
                m_Command = New OleDbCommand(SQL, m_Connection)
                'We need to open the connection for the DataReader explicitly
                m_Connection.Open()
                'Run the Execute Reader method of the Command Object
                m_DataReader = m_Command.ExecuteReader

                Return m_DataReader

            Catch ExceptionObject As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ExceptionObject)

                'Any exception will be logged through our private logexception function
                'LogException(ExceptionObject)
                'If an exception occurs, close the connection now!
                m_Connection.Close()
                'The exception is passed back to the calling code, with our custom message and specific exception information
                Throw New Exception(m_ExceptionMessage, ExceptionObject)
            End Try

        End Function

#End Region

#Region "RunParamSQLScalar - Parameterized SQL is run with ExecuteScalar"
        'The RunParamSQLScalar function accepts a SQL statement that is required
        Public Function RunParamSQLScalar(ByVal SQL As String) As Object
            'Validate SQL statement
            ValidateSQLStatement(SQL)

            'Setting the objects to handle parameters
            Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
            Dim prParameter As OleDbParameter            'will contain the converted SQLParameter
            'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
            Dim usedEnumerator As IEnumerator = m_ParameterList.GetEnumerator()


            'Check to see if this object has already been disposed
            If m_DisposedBoolean = True Then
                Throw New ObjectDisposedException(m_ModuleName, "This object has already been disposed. You cannot reuse it")
            End If

            'Set a new connection
            m_Connection = New OleDbConnection(m_ConnectionString)

            'Define the command object and set commandtype to process Stored Procedure
            m_Command = New OleDbCommand(SQL, m_Connection)

            'Move through the privateParameterList wiht the help of the enumerator
            Do While (usedEnumerator.MoveNext())
                'prUsedParameter = Nothing
                'Get parameter in privateParameterList
                prUsedParameter = CType(usedEnumerator.Current, Parameter)
                'Convert paramter to SQLParameter
                prParameter = ConvertParameters(prUsedParameter)
                'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                m_Command.Parameters.Add(prParameter)
            Loop

            m_Connection.Open()
            Try
                m_Object = m_Command.ExecuteScalar

                Return m_Object
                '   Changed by Anatoly on 3/22/2004.
                '   Catch only OleDbExceptions
                'Catch ex As OleDbException
                 '	Dim exTracker = new AdvApplicationInsightsInitializer()
                '	exTracker.TrackExceptionWrapper(ex)

                '    '   this error is specific for the OLeDb Provider of MSSQL. 
                '    If ex.ErrorCode = -2147217873 Then
                '        '   get error type
                '        'Select Case DALExceptions.ExceptionType(Ex)
                '        '    Case "547"  '   this is a referential integrity exception
                '        '        Throw New ReferentialIntegrityException("Referential Integrity Exception", Ex)
                '        '    Case "2601" '   this is a unique index violation exception
                '        '        Throw New UniqueIndexException("Unique Index Violation Exception", Ex)
                '        '    Case Else   '   do not intercept exception
                '        Throw ex
                '        'End Select
                '        'Else
                '        '    Dim objEx As New FAME.ExceptionLayer.BaseException("DataAccess Layer Error:", Ex)
                '        '    Throw objEx
                '    End If
                '    '   end of changes by Anatoly
                'Catch exceptionObject As Exception
                 '	Dim exTracker = new AdvApplicationInsightsInitializer()
                '	exTracker.TrackExceptionWrapper(exceptionObject)

                '    'An exception will be logged thorugh our private logexception funciton
                '    'LogException(ExceptionObject)
                '    'The exception is passed back to the calling code, with our custom message and specific exception information
                '    m_Connection.Close()
                '    Throw New Exception(m_ExceptionMessage, exceptionObject)
            Finally
                m_Connection.Close()
            End Try
        End Function
#End Region

#Region "RunParamSQLDataSet - Parameterized SQL is run and returns a DataSet"
        'The RunParamSQLDataSet function accepts a SQL statement that is required and an optinal table name
        Public Function RunParamSQLDataSet(ByVal SQL As String, Optional ByVal TableName As String = Nothing) As DataSet
            'Validate SQL statement
            ValidateSQLStatement(SQL)

            'Setting the objects to handle parameters
            Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
            Dim prParameter As OleDbParameter            'will contain the converted SQLParameter
            'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
            Dim usedEnumerator As IEnumerator = m_ParameterList.GetEnumerator()

            Try
                'Check to see if this object has already been disposed
                If m_DisposedBoolean = True Then
                    Throw New ObjectDisposedException(m_ModuleName, "This object has already been disposed. You cannot reuse it")
                End If

                'Set a new connection and DataSet
                m_Connection = New OleDbConnection(m_ConnectionString)
                Dim prDataSet As New DataSet

                'Define the command object and set commandtype to process Stored Procedure
                m_Command = New OleDbCommand(SQL, m_Connection)

                'Move through the privateParameterList with the help of the enumerator
                Do While (usedEnumerator.MoveNext())
                    prUsedParameter = Nothing
                    'Get parameter in privateParameterList
                    prUsedParameter = usedEnumerator.Current
                    'Convert paramter to SQLParameter
                    prParameter = ConvertParameters(prUsedParameter)
                    'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                    m_Command.Parameters.Add(prParameter)
                Loop
                'Have the DataAdapter run the Stored Procedure
                m_DataAdapter = New OleDbDataAdapter(m_Command)
                'Depending on table name passed, create the DataSet with or without specifically naming the table
                If TableName = Nothing Then
                    m_DataAdapter.Fill(prDataSet)
                Else
                    m_DataAdapter.Fill(prDataSet, TableName)
                End If

                Return prDataSet

            Catch ExceptionObject As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ExceptionObject)

                'An exception will be logged thorugh our private logexception funciton
                'LogException(ExceptionObject)
                'The exception is passed to the calling code
                Throw New Exception(m_ExceptionMessage, ExceptionObject)
            Finally
                'Always close the connection as soon as possible(only then will object be allowed to go out of scope)
                m_Connection.Close()
            End Try
        End Function
        Public Function RunParamSQLDataSetUsingSP(ByVal SQL As String, Optional ByVal TableName As String = Nothing) As DataSet
            'Validate SQL statement
            'ValidateSQLStatement(SQL)

            'Setting the objects to handle parameters
            Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
            Dim prParameter As OleDbParameter            'will contain the converted SQLParameter
            'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
            Dim usedEnumerator As IEnumerator = m_ParameterList.GetEnumerator()

            Try
                'Check to see if this object has already been disposed
                If m_DisposedBoolean = True Then
                    Throw New ObjectDisposedException(m_ModuleName, "This object has already been disposed. You cannot reuse it")
                End If

                'Set a new connection and DataSet
                m_Connection = New OleDbConnection(m_ConnectionString)
                Dim prDataSet As New DataSet

                'Define the command object and set commandtype to process Stored Procedure
                m_Command = New OleDbCommand(SQL, m_Connection)
                m_Command.CommandType = CommandType.StoredProcedure

                'Move through the privateParameterList with the help of the enumerator
                Do While (usedEnumerator.MoveNext())
                    prUsedParameter = Nothing
                    'Get parameter in privateParameterList
                    prUsedParameter = usedEnumerator.Current
                    'Convert paramter to SQLParameter
                    prParameter = ConvertParameters(prUsedParameter)
                    'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                    m_Command.Parameters.Add(prParameter)
                Loop
                'Have the DataAdapter run the Stored Procedure
                m_DataAdapter = New OleDbDataAdapter(m_Command)
                'Depending on table name passed, create the DataSet with or without specifically naming the table
                If TableName = Nothing Then
                    m_DataAdapter.Fill(prDataSet)
                Else
                    m_DataAdapter.Fill(prDataSet, TableName)
                End If

                Return prDataSet

            Catch ExceptionObject As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ExceptionObject)

                'An exception will be logged thorugh our private logexception funciton
                'LogException(ExceptionObject)
                'The exception is passed to the calling code
                Throw New Exception(m_ExceptionMessage, ExceptionObject)
            Finally
                'Always close the connection as soon as possible(only then will object be allowed to go out of scope)
                m_Connection.Close()
            End Try
        End Function
#End Region

#Region "RunParamSQLDataReader - Parameterized SQL is run and returns a DataReader"
        'The RunParamSQLDataReader function accepts a SQL statement that is requirede
        Public Function RunParamSQLDataReader(ByVal SQL As String) As OleDbDataReader
            'Validate Stored Procedure
            ValidateSQLStatement(SQL)

            'Setting the objects to handle parameters
            Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
            Dim prParameter As OleDbParameter            'will contain the converted SQLParameter
            'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
            Dim usedEnumerator As IEnumerator = m_ParameterList.GetEnumerator()

            Try
                'Check to see if this object has already been disposed
                If m_DisposedBoolean = True Then
                    Throw New ObjectDisposedException(m_ModuleName, "This object has already been disposed. You cannot reuse it")
                End If

                'Set a new connection and DataSet
                m_Connection = New OleDbConnection(m_ConnectionString)

                'Define the command object and set commandtype to process Stored Procedure
                m_Command = New OleDbCommand(SQL, m_Connection)

                'Move through the privateParameterList wiht the help of the enumerator
                Do While (usedEnumerator.MoveNext())
                    prUsedParameter = Nothing
                    'Get parameter in privateParameterList
                    prUsedParameter = usedEnumerator.Current
                    'Convert paramter to SQLParameter
                    prParameter = ConvertParameters(prUsedParameter)
                    'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                    m_Command.Parameters.Add(prParameter)
                Loop

                m_Connection.Open()
                m_DataReader = m_Command.ExecuteReader

                Return m_DataReader

            Catch ExceptionObject As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ExceptionObject)

                'An exception will be logged thorugh our private logexception funciton
                'LogException(ExceptionObject)
                'The exception is passed back to the calling code, with our custom message and specific exception information
                m_Connection.Close()
                Throw New Exception(m_ExceptionMessage, ExceptionObject)
            End Try
        End Function
#End Region

#Region "RunParamSQLDataAdapter - Parameterized SQL is run and returns a DataAdapter"
        'The RunParamSQLDataSet function accepts a SQL statement that is required
        Public Function RunParamSQLDataAdapter(ByVal SQL As String) As OleDbDataAdapter
            'Validate SQL statement
            ValidateSQLStatement(SQL)

            'Setting the objects to handle parameters
            Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
            Dim prParameter As OleDbParameter            'will contain the converted SQLParameter
            'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
            Dim usedEnumerator As IEnumerator = m_ParameterList.GetEnumerator()

            Try
                'Check to see if this object has already been disposed
                If m_DisposedBoolean = True Then
                    Throw New ObjectDisposedException(m_ModuleName, "This object has already been disposed. You cannot reuse it")
                End If

                'This method should be used in conjuntion with the constructor that 
                'specifies DataAdapter and opens the connection. At this point the connection
                'should be already opened.

                'Define the command object and set commandtype to process Stored Procedure
                m_Command = New OleDbCommand(SQL, m_Connection)

                'Move through the privateParameterList with the help of the enumerator
                Do While (usedEnumerator.MoveNext())
                    prUsedParameter = Nothing
                    'Get parameter in privateParameterList
                    prUsedParameter = usedEnumerator.Current
                    'Convert paramter to SQLParameter
                    prParameter = ConvertParameters(prUsedParameter)
                    'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                    m_Command.Parameters.Add(prParameter)
                Loop
                'Have the DataAdapter run the SQL Statement
                m_DataAdapter = New OleDbDataAdapter(m_Command)

                Return m_DataAdapter

            Catch ExceptionObject As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ExceptionObject)

                'An exception will be logged thorugh our private logexception funciton
                'LogException(ExceptionObject)
                'The exception is passed to the calling code
                Throw New Exception(m_ExceptionMessage, ExceptionObject)
            Finally
                'Always close the connection as soon as possible(only then will object be allowed to go out of scope)
                m_Connection.Close()
            End Try
        End Function
#End Region

#Region "Execute a none returning parameterized query"
        Public Sub RunParamSQLExecuteNoneQuery(ByVal SQL As String, Optional ByVal tran As OleDbTransaction = Nothing)
            'Validate Stored Procedure
            ValidateSQLStatement(SQL)

            'Setting the objects to handle parameters
            Dim prUsedParameter As Parameter           'will return the specific parameter in the privateParameterList
            Dim prParameter As OleDbParameter            'will contain the converted SQLParameter
            'The usedEnumerator makes it easy to step through the list of parameters in the privateParameterList
            Dim usedEnumerator As IEnumerator = m_ParameterList.GetEnumerator()

            Try
                'Check to see if this object has already been disposed
                If m_DisposedBoolean = True Then
                    Throw New ObjectDisposedException(m_ModuleName, "This object has already been disposed. You cannot reuse it")
                End If

                'Set a new connection and DataSet
                If tran Is Nothing Then
                    m_Connection = New OleDbConnection(m_ConnectionString)
                End If

                'Define the command object and set commandtype to process Stored Procedure
                m_Command = New OleDbCommand(SQL, m_Connection)
                If Not (tran Is Nothing) Then
                    m_Command.Transaction = tran
                End If
                'Move through the privateParameterList wiht the help of the enumerator
                Do While (usedEnumerator.MoveNext())
                    prUsedParameter = Nothing
                    'Get parameter in privateParameterList
                    prUsedParameter = usedEnumerator.Current
                    'Convert paramter to SQLParameter
                    prParameter = ConvertParameters(prUsedParameter)
                    'Add converted parameter to the privateCommand object that imports data through the DataAdapter
                    m_Command.Parameters.Add(prParameter)
                Loop

                If tran Is Nothing Then
                    m_Connection.Open()
                End If
                m_Command.ExecuteNonQuery()

                '   Changed by Anatoly on 01/05/2005.
                '   Catch only OleDbExceptions
            Catch Ex As OleDbException
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(Ex)

                '   this error is specific for the OLeDb Provider of MSSQL. 
                If Ex.ErrorCode = -2147217873 Then
                    '   get error type
                    'Select Case DALExceptions.ExceptionType(Ex)
                    '    Case "547"  '   this is a referential integrity exception
                    '        Throw New ReferentialIntegrityException("Referential Integrity Exception", Ex)
                    '    Case "2601" '   this is a unique index violation exception
                    '        Throw New UniqueIndexException("Unique Index Violation Exception", Ex)
                    '    Case Else   '   do not intercept exception
                    Throw Ex
                    'End Select
                    'Else
                    '    Dim objEx As New FAME.ExceptionLayer.BaseException("DataAccess Layer Error:", Ex)
                    '    Throw objEx
                End If
                'Throw Ex
                '   end of changes by Anatoly
            Catch ExceptionObject As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ExceptionObject)

                'An exception will be logged thorugh our private logexception funciton
                'LogException(ExceptionObject)
                'The exception is passed back to the calling code, with our custom message and specific exception information
                Dim objEx As New BaseException("DataAccess Layer Error:", ExceptionObject)
                Throw objEx
            Finally
                If tran Is Nothing Then
                    m_Connection.Close()
                End If
            End Try
        End Sub
#End Region

#Region "RunSchemaInfo - Runs a SQL regular statement and returns a Datatable"
        'The RunSchemaInfoForAuditing function accepts a SQL statement that is required
        Public Function RunSchemaInfo(ByVal SQL As String) As DataTable
            Dim dtSchema As DataTable
            'Validate the SQL String to be larger than 10 characters
            ValidateSQLStatement(SQL)

            'We include all called object in the try block to catch any excepitons that could occur ( even in the creation of the connection)
            Try
                'Check to see if this object has already been disposed
                If m_DisposedBoolean = True Then
                    Throw New ObjectDisposedException(m_ModuleName, "This object has already been disposed. You cannot reuse it.")
                End If

                'Set a new Connecton.
                m_Connection = New OleDbConnection(m_ConnectionString)

                'Set a new Command that accepts an SQL statement and the connection. 
                'The command.commandtype does not have to be set since it defaults to text
                m_Command = New OleDbCommand(SQL, m_Connection)
                'We need to open the connection for the DataReader explicitly
                m_Connection.Open()
                'Run the Execute Reader method of the Command Object
                m_DataReader = m_Command.ExecuteReader(CommandBehavior.KeyInfo Or CommandBehavior.SchemaOnly)
                dtSchema = m_DataReader.GetSchemaTable
                Return dtSchema

            Catch ExceptionObject As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ExceptionObject)

                'Any exception will be logged through our private logexception function
                'LogException(ExceptionObject)
                'The exception is passed back to the calling code, with our custom message and specific exception information
                Throw New Exception(m_ExceptionMessage, ExceptionObject)
            Finally
                m_Connection.Close()
            End Try

        End Function
#End Region

#End Region

#Region "AddParameter Public Method and Support functions"

        Public Sub AddParameter(ByVal ParameterName As String, _
                                Optional ByVal Value As Object = Nothing, _
                                Optional ByVal OleType As OleDbDataType = Nothing, _
                                Optional ByVal Size As Integer = Nothing, _
                                Optional ByVal Direction As ParameterDirection = ParameterDirection.Input)

            Dim buildDataType As OleDbType
            Dim buildParameter As Parameter = Nothing

            Select Case OleType
                Case OleDbDataType.OleDbString
                    buildDataType = OleDbType.VarChar
                Case OleDbDataType.OleDbChar
                    buildDataType = OleDbType.Char
                Case OleDbDataType.OleDbInteger
                    buildDataType = OleDbType.Integer
                Case OleDbDataType.OleDbDateTime
                    buildDataType = OleDbType.Date
                Case OleDbDataType.OleDbDecimal
                    buildDataType = OleDbType.Decimal
                Case OleDbDataType.OleDbMoney
                    buildDataType = OleDbType.Currency
                    ' added OleDbDataType Guid - Corey Masson - Jan 7, 2004
                Case OleDbDataType.OleDbGuid
                    buildDataType = OleDbType.Guid
            End Select

            buildParameter = New Parameter(ParameterName, Value, buildDataType, Size, Direction)
            m_ParameterList.Add(buildParameter)

        End Sub

        Public Class Parameter
            Public ParameterName As String
            Public ParameterValue As Object
            Public ParameterDataType As OleDbDataType
            Public ParameterSize As Integer
            Public ParameterDirectionUsed As ParameterDirection

            Sub New(ByVal passedParameterName As String, _
                    Optional ByVal passedValue As Object = Nothing, _
                    Optional ByVal passedOleDbType As OleDbType = Nothing, _
                    Optional ByVal passedSize As Integer = Nothing, _
                    Optional ByVal passedDirection As ParameterDirection = ParameterDirection.Input)

                ParameterName = passedParameterName
                ParameterValue = passedValue
                ParameterDataType = passedOleDbType
                ParameterSize = passedSize
                ParameterDirectionUsed = passedDirection

            End Sub

        End Class

        Private Function ConvertParameters(ByVal passedParameter As Parameter) As OleDbParameter

            Dim returnOleDbParameter As OleDbParameter = New OleDbParameter

            returnOleDbParameter.ParameterName = passedParameter.ParameterName
            returnOleDbParameter.Value = passedParameter.ParameterValue
            returnOleDbParameter.OleDbType = passedParameter.ParameterDataType
            returnOleDbParameter.Size = passedParameter.ParameterSize
            returnOleDbParameter.Direction = passedParameter.ParameterDirectionUsed

            Return returnOleDbParameter

        End Function

        Public Sub ClearParameters()
            Try
                m_ParameterList.Clear()
            Catch parameterException As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(parameterException)

                Throw New Exception(m_ExceptionMessage & " Parameter List did not clear", parameterException)
            End Try
        End Sub

#End Region

#Region "Exception Logging"
        'We call our eventlog by passing it the exception we caught
        'Private Sub LogException(ByRef ExceptionObject As Exception)

        '    Dim EventLogMessage As String           'this is the Message we will pass to the log

        '    Try
        '        'Create the Message to be passed from the exception 
        '        EventLogMessage = "An error occured in the following module: " & m_ModuleName & _
        '                          " The Source was: " & ExceptionObject.Source & vbCrLf & _
        '                          " With the Message: " & ExceptionObject.Message & vbCrLf & _
        '                          " Stack Tace: " & ExceptionObject.StackTrace & vbCrLf & _
        '                          " Target Site: " & ExceptionObject.TargetSite.ToString
        '        'Define the Eventlog as an Application Log entry
        '        Dim localEventLog As New EventLog("Application")
        '        'Write the entry to the Application Event log, using this Module's name, the message an make it an error message with an ID of 55
        '        EventLog.WriteEntry(m_ModuleName, EventLogMessage, EventLogEntryType.Error, 55)
        '    Catch EventLogException As Exception
         '    	Dim exTracker = new AdvApplicationInsightsInitializer()
        '    	exTracker.TrackExceptionWrapper(EventLogException)

        '        'If the eventlog fails (like forgetting to make the ASPNET user account a member of the debugger group, we pass the error
        '        Throw New Exception(m_ExceptionMessage & " - EventLog Error: " & EventLogException.Message, EventLogException)
        '    End Try

        'End Sub

#End Region

#Region "Validations"
        Private Sub ValidateSQLStatement(ByRef SQLStatement As String)
            'SQL Statement must be at least 10 characters ( "Select * form x" )
            If Len(SQLStatement) < 10 Then
                Throw New Exception(m_ExceptionMessage & " The SQL Statement must be provided and at least 10 characters long")
            End If
        End Sub

#End Region

#Region "Overloaded Dispose and Finalize"
        Public Sub Dispose() Implements IDisposable.Dispose
            If (Not m_DisposedBoolean) Then
                'Call cleanup code in Finalize
                finalize()

                'Record that object has been disposed
                m_DisposedBoolean = True

                'Finalize does not need to be called.
                GC.SuppressFinalize(Me)
            End If
        End Sub
        Protected Overrides Sub finalize()
            'Perform cleanup code here
            If Not m_Connection Is Nothing Then
                'm_Connection.Dispose()
                m_Connection = Nothing
            End If
        End Sub
#End Region


        Private Function GetAdvAppSettings() As AdvAppSettings
            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If
            Return MyAdvAppSettings
        End Function

    End Class
End Namespace
