Imports System.Data
Imports System.Data.OleDb
Imports FluentNHibernate.Conventions.AcceptanceCriteria
Imports FAME.DataAccessLayer
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports FAME.Advantage.Common
Imports System.Web.UI.WebControls
Imports System.Collections

'last update: 11/16/2012 by NZ
Imports System.Activities.Statements

Namespace FAME.Common
    Public Class SDFComponent

#Region "Gets"
        Public Function GetControlCountByType(ByVal ModuleId As String, ByVal ResourceId As Integer, Optional ByVal entityId As Integer = 0) As String
            '**************************************************************************************************
            'Purpose:       This Procedure Determines The Total Count OF Controls Based on The Controls Type
            '               The Controls belong to following three types - Range,List,None
            'Parameters:
            'Returns:       String
            'Notes:         This sub relies on the ResourceId and ModuleCode when
            '               when a request for the page is made.
            '               intRangeCount --- Holds the Total Count for Range Controls
            '               intListCount  --- Holds the Total Count for List Controls
            '               intNoneCount  --- Holds the Total Count for Controls with no validation
            '**************************************************************************************************
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            'Dim ds As New DataSet
            Dim intRangeCount As Integer
            Dim intListCount As Integer
            Dim intNoneCount As Integer

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            Try
                db.ConnectionString = CType(myAdvAppSettings.AppSettings("Constring"), String)
                With sb
                    .Append("select ValTypeID,Count(ValTypeID) as TotalCount from sySDF A,syResourceSDf B where A.sdfid = B.sdfid and B.ResourceId =" & ResourceId & " and B.EntityId =" & entityId & "  group by ValTypeID")
                End With
                'Execute the query
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                While dr.Read()
                    Select Case dr("valtypeid")
                        Case 1
                            intRangeCount = CType(dr("TotalCount"), Integer) 'Get Count of Controls With Ranges
                        Case 2
                            intListCount = CType(dr("TotalCount"), Integer) 'Get Count of Controls with List Controls
                        Case 0
                            intNoneCount = CType(dr("TotalCount"), Integer) 'Get Count of controls with None
                    End Select
                End While
                dr.Close()
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                'db.CloseConnection()
                Return intRangeCount & "," & intListCount & "," & intNoneCount
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            Finally
                db.CloseConnection()
            End Try
            Return String.Empty
        End Function
        Public Function GetModuleByResource(ByVal ResourceId As Integer) As String
            '**************************************************************************************************
            'Purpose:       This Procedure Determines The Module Based On The ResourceID when page opens
            'Parameters:    ResourceID
            'Returns:       String
            'Notes:         This sub relies on the ResourceId when a request for the page is made.

            '**************************************************************************************************
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            'Dim ds As New DataSet
            Dim strModuleID As String = ""

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            Try
                db.ConnectionString = CType(myAdvAppSettings.AppSettings("Constring"), String)
                With sb
                    .Append("select ResourceURL from syResources where ResourceId=? ")
                End With
                'Execute the query
                db.AddParameter("@ResourceID", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                While dr.Read()
                    strModuleID = CType(dr("ResourceURL"), String) 'Get Count of controls with None
                End While
                dr.Close()
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                'db.CloseConnection()
                Return strModuleID

            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            Finally
                db.CloseConnection()
            End Try
            Return Nothing
        End Function

        Public Function GetSDFExists(ByVal ResourceId As Integer, ByVal strModuleId As String) As Integer
            '**************************************************************************************************
            'Purpose:       This Procedure Determines The Module Based On The ResourceID when page opens
            'Parameters:    ResourceID
            'Returns:       String
            'Notes:         This sub relies on the ResourceId when a request for the page is made.
            '**************************************************************************************************
            Dim db As New DataAccess
            Dim sb As New StringBuilder
            'Dim ds As New DataSet
            Dim ModuleId As Integer

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            'Get ControlCountByType

            Select Case strModuleId.ToString
                Case Is = "AD"
                    ModuleId = 189
                Case Is = "AR"
                    ModuleId = 26
                Case Is = "HR"
                    ModuleId = 192
                Case Is = "PL"
                    ModuleId = 193
                Case Is = "SA"
                    ModuleId = 194
                Case Is = "FinAid"
                    ModuleId = 191
            End Select

            Dim intEntityId As Integer = 0
            intEntityId = getEntity(ResourceId, "")

            Try
                db.ConnectionString = CType(myAdvAppSettings.AppSettings("Constring"), String)
                With sb
                    .Append("select Count(*)  from syResourceSDF where ResourceId=? and EntityId=? ")
                End With
                'Execute the query
                db.AddParameter("@ResourceID", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@EntityId", intEntityId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Dim intSdfExists As Integer = CType(db.RunParamSQLScalar(sb.ToString), Integer)
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                Return intSdfExists
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            Finally
                db.CloseConnection()
            End Try
            Return Nothing
        End Function
        Public Function GetAllLabels(ByVal pnl As Panel) As ArrayList
            '**************************************************************************************************
            'Purpose:       This Procedure Determines the SDF Field Values
            'Parameters:    Panel Control
            'Returns:       ArrayList
            '**************************************************************************************************
            Dim ctl As Control
            Dim arr2 As New ArrayList
            For Each ctl In pnl.Controls
                If ctl.GetType Is GetType(Label) Then 'And ctl.ID Like "%lblR%" Then
                    arr2.Add(CType(ctl, Label))
                End If
            Next
            Return arr2
        End Function
        Public Function GetAllValues(ByVal pnl As Panel) As ArrayList
            '**************************************************************************************************
            'Purpose:       This Procedure Gets Values From The Panel
            'Parameters:    Panel Control
            'Returns:       ArrayList
            '**************************************************************************************************
            Dim ctl As Control
            Dim arr3 As New ArrayList

            For Each ctl In pnl.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    arr3.Add(CType(ctl, TextBox).Text)
                End If
            Next
            For Each ctl In pnl.Controls
                If ctl.GetType Is GetType(DropDownList) Then
                    arr3.Add(CType(ctl, DropDownList).SelectedValue)
                End If
            Next
            Return arr3
        End Function

        'Public Sub GetSDFValue(ByVal pgPKID As String)
        '    Dim sb As New StringBuilder
        '    Dim db As New DataAccess

        '    Dim myAdvAppSettings As AdvAppSettings
        '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '        myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        '    Else
        '        myAdvAppSettings = New AdvAppSettings
        '    End If

        '    Try
        '        db.ConnectionString = CType(myAdvAppSettings.AppSettings("Constring"), String)
        '        '   build query
        '        With sb
        '            .Append("Select SDFValue from sySDFModuleValue where pgPKID = ? ")
        '        End With
        '        db.AddParameter("@SDFPKID", pgPKID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
        '        'execute query
        '        db.RunParamSQLExecuteNoneQuery(sb.ToString)
        '        db.ClearParameters()
        '        sb.Remove(0, sb.Length)
        '        'db.CloseConnection()
        '    Catch ex As Exception
         '    	Dim exTracker = new AdvApplicationInsightsInitializer()
        '    	exTracker.TrackExceptionWrapper(ex)

        '    Finally
        '        db.CloseConnection()
        '    End Try
        'End Sub

        Public Function ReadImage(ByVal StudentDocId As String) As SqlDataReader
            Dim sqlConn As SqlConnection

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            'Find the Position of first semi colon
            Dim intconnString As Integer = InStr(CType(myAdvAppSettings.AppSettings("ConString"), String), ";")

            'Modify the connectionstring
            Dim connString As String = Mid(CType(myAdvAppSettings.AppSettings("ConString"), String), intconnString + 1)

            sqlConn = New SqlConnection(connString)
            sqlConn.Open()
            Try

                Dim sqlCom As New SqlCommand("Select Img_data, Img_ContentType From plStudentDocs where StudentDocId=@StudentDocId", sqlConn)
                sqlCom.Parameters.AddWithValue("@StudentDocId", StudentDocId)
                Dim sqlDataReader As SqlDataReader
                sqlDataReader = sqlCom.ExecuteReader 'Execute the SQL command
                Return sqlDataReader
            Finally
                sqlConn.Close()
            End Try

        End Function

        Public Function GetModuleID(ByVal strModuleId As String) As Integer
            Dim moduleId As Integer = 0
            Select Case strModuleId.ToString
                Case Is = "AD"
                    moduleId = 189
                Case Is = "AR"
                    moduleId = 26
                Case Is = "HR"
                    moduleId = 192
                Case Is = "PL"
                    moduleId = 193
                Case Is = "SA"
                    moduleId = 194
                Case Is = "FinAid", "FA"
                    moduleId = 191
            End Select
            Return moduleId
        End Function
        Private Sub getRangeColumn(ByVal ResourceID As Integer, ByVal ModuleId As Integer, ByVal ds As DataSet,
                                   Optional PKID As String = "", Optional intEntityId As Integer = 0)
            Try

                Dim myAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    myAdvAppSettings = New AdvAppSettings
                End If

                Dim sb As StringBuilder = New StringBuilder
                If PKID <> "" Then
                    sb.Append("select distinct A.SDFDescrip,A.SDFID as SDFID,B.SDFVisibilty,(SELECT MinVal FROM sySdfRange where SDFID=A.SDFID) as MinimumValue,(SELECT MaxVal FROM sySdfRange where SDFID=A.SDFID) as MaximumValue,(select SDFValue from sySDFModuleValue where SDFID = A.SDFID and PgPkID =? ) as Value, t.DType, A.IsRequired from sySDF A,syResourceSDf B , sySdfDTypes t where a.DTypeId = t.DTypeId AND A.sdfid = B.sdfid and B.ResourceId = ?  and B.EntityId = ? and valTypeID=1 Order by A.SDFDescrip")
                Else
                    'DE8735: NZ update------------------------------------------
                    sb.Append("SELECT  distinct A.SDFDescrip,A.SDFID AS SDFID ,B.SDFVisibilty ,r.MinVal AS MinimumValue ,r.MaxVal AS MaximumValue,t.DType,A.IsRequired FROM sySDF A INNER JOIN syResourceSDf B ON A.sdfid = B.sdfid INNER JOIN sySdfDTypes t ON a.DTypeId = t.DTypeId LEFT OUTER JOIN sySdfRange r ON r.SDFID = A.SDFID WHERE   B.ResourceId = ? AND B.EntityId = ? AND valTypeID = 1 ORDER BY A.SDFDescrip")
                    '------------------------------------------------------------
                End If
                'Execute the query
                Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))
                If PKID <> "" Then sc.Parameters.Add(New OleDbParameter("@PgPKId", PKID))
                sc.Parameters.Add(New OleDbParameter("@ResourceId", ResourceID))
                sc.Parameters.Add(New OleDbParameter("@EntityId", intEntityId))
                Dim da As New OleDbDataAdapter(sc)
                da.Fill(ds, "RangeColumn")
                sb = Nothing
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            Finally
            End Try
        End Sub
        Private Sub getListColumn(ByVal ResourceID As Integer, ByVal ModuleId As Integer, ByVal ds As DataSet,
                                  Optional PKID As String = "",
                                  Optional intEntityId As Integer = 0)
            Try

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim sb As StringBuilder = New StringBuilder
                If PKID <> "" Then
                    sb.Append("select distinct A.SDFDescrip,A.SDFID as SDFID,B.SDFVisibilty,(select SDFValue from sySDFModuleValue where SDFID = A.SDFID  and SDFValue <> '' and PgPkID =? ) as Value, A.IsRequired from sySDF A,syResourceSDf B where A.sdfid = B.sdfid and B.ResourceId = ?  and B.EntityId = ? and valTypeID=2 Order by A.SDFDescrip")
                Else
                    sb.Append("select distinct A.SDFDescrip,B.SDFVisibilty,A.SDFID as SDFID,A.IsRequired from sySDF A,syResourceSDf B where A.sdfid = B.sdfid and B.ResourceId =? and  B.EntityId =? and valTypeID=2 Order by A.SDFDescrip")
                End If
                'Execute the query
                Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
                If PKID <> "" Then sc.Parameters.Add(New OleDbParameter("@PgPKId", PKID))
                sc.Parameters.Add(New OleDbParameter("@ResourceId", ResourceID))
                sc.Parameters.Add(New OleDbParameter("@EntityId", intEntityId))
                Dim da As New OleDbDataAdapter(sc)
                da.Fill(ds, "ListColumn")
                sb = Nothing
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            Finally
            End Try
        End Sub
        Private Sub getNoneColumn(ByVal ResourceID As Integer, ByVal ModuleId As Integer, ByVal ds As DataSet,
                                  Optional PKID As String = "",
                                  Optional intEntityId As Integer = 0)
            Try

                Dim MyAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    MyAdvAppSettings = New AdvAppSettings
                End If

                Dim sb As StringBuilder = New StringBuilder
                If PKID <> "" Then
                    sb.Append("select distinct A.SDFDescrip,A.SDFID as SDFID,B.SDFVisibilty,(select SDFValue from sySDFModuleValue where SDFID = A.SDFID and SDFValue <> ''  and PgPkID = ? ) as Value, A.IsRequired from sySDF A,syResourceSDf B where A.sdfid = B.sdfid and B.ResourceId = ?  and B.EntityId = ? and valTypeID=0 Order by A.SDFDescrip")
                Else
                    sb.Append("select distinct A.SDFDescrip,B.SDFVisibilty,A.SDFID as SDFID, A.IsRequired from sySDF A,syResourceSDf B where A.sdfid = B.sdfid and B.ResourceId =? and  B.EntityId =? and  valTypeID=0 Order by A.SDFDescrip")
                End If
                'Execute the query
                Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
                If PKID <> "" Then sc.Parameters.Add(New OleDbParameter("@PgPKId", PKID))
                sc.Parameters.Add(New OleDbParameter("@ResourceId", ResourceID))
                sc.Parameters.Add(New OleDbParameter("@EntityId", intEntityId))
                Dim da As New OleDbDataAdapter(sc)
                da.Fill(ds, "NoneColumn")
                sb = Nothing
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            Finally
            End Try
        End Sub

        Private Function getEntity(ByVal ResourceID As Integer, Optional PKID As String = "") As Integer
            Try

                Dim myAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    myAdvAppSettings = New AdvAppSettings
                End If

                Dim db As New DataAccess
                db.ConnectionString = CType(myAdvAppSettings.AppSettings("Constring"), String)
                Dim sb As StringBuilder = New StringBuilder
                Dim intEntityId As Integer = 0
                sb.Append("  Select Top 1 ResourceId from syAdvantageResourceRelations where RelatedResourceId=? ")
                'Execute the query
                db.AddParameter("@RelatedResourceId", ResourceID, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Dim dr As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                While dr.Read()
                    intEntityId = CType(dr("ResourceId"), Integer)
                End While
                dr.Close()
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                'db.CloseConnection()
                Return intEntityId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            Finally
            End Try
            Return Nothing
        End Function

        Private Sub getValueListColumn(ByVal ResourceID As Integer, ByVal ModuleId As Integer, ByVal ds As DataSet,
                                       Optional PKID As String = "",
                                       Optional intEntityId As Integer = 0)
            Try

                Dim myAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    myAdvAppSettings = New AdvAppSettings
                End If

                Dim sb As StringBuilder = New StringBuilder
                If PKID <> "" Then
                    sb.Append(" select sdFListID,valList,C.SDFID as SDFID,C.SDFDescrip,(select SDFValue from sySDFModuleValue where SDFID = C.SDFID and SDFValue <> '' and PgPkID = ? ) as Value from sySDFValList A,syResourceSDF B,sySDF C ")
                    sb.Append(" where A.SDFID = B.SDFID And B.SDFID = C.SDFID ")
                    sb.Append(" and B.ResourceId =? and  B.EntityId=?  and c.valTypeID=2 ")
                    sb.Append("ORDER BY C.SDFDescrip ")
                Else
                    sb.Append(" select sdFListID,valList,C.SDFID as SDFID,C.SDFDescrip from sySDFValList A,syResourceSDF B,sySDF C ")
                    sb.Append(" where A.SDFID = B.SDFID And B.SDFID = C.SDFID ")
                    sb.Append(" and B.ResourceId = ? and  B.EntityId = ? and c.valTypeID = 2 ")
                    sb.Append("ORDER BY C.SDFDescrip ")
                End If
                'Execute the query
                Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("ConString")))
                If PKID <> "" Then sc.Parameters.Add(New OleDbParameter("@PgPKId", PKID))
                sc.Parameters.Add(New OleDbParameter("@ResourceId", ResourceID))
                sc.Parameters.Add(New OleDbParameter("@EntityId", intEntityId))
                Dim da As New OleDbDataAdapter(sc)
                da.Fill(ds, "ValueList")
                sb = Nothing
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            Finally
            End Try
        End Sub
#End Region

#Region "generate New"
        Public Sub GenerateControlsNew(ByVal pnl As Panel, ByVal ResourceID As Integer,
                                       ByVal strModuleId As String)
            '**************************************************************************************************
            'Purpose:       This Procedure Determines the Count Of Controls Available for the Resource(Page)
            '               and Populates the Controls Dynamically.Populates TextBox,Labels,DropDownList
            '               Control and finally adds it to panel control
            'Parameters:    ResourceID,Panel Control
            'Returns:       String
            'Notes:         This sub relies on the ResourceId when a request for the page is made.

            '**************************************************************************************************
            pnl.Controls.Clear()
            Dim intRange, intList, intNone As Integer
            Dim dynText As TextBox, dynList As DropDownList, dynLabel As Label, dynRangeValidator As RangeValidator
            Dim x, y, z As Integer
            Dim ModuleId As Integer = GetModuleID(strModuleId)
            Dim intEntityId As Integer = 0
            intEntityId = getEntity(ResourceID, "")
            'Call function and get the count of controls for a page.
            Dim strResult As String = GetControlCountByType(ModuleId, ResourceID, intEntityId)

            'Use Split Function and get the controls count 
            'in respective variables
            Dim arrRange() As String
            arrRange = strResult.Split(",")
            intRange = arrRange(0)
            intList = arrRange(1)
            intNone = arrRange(2)

            Dim isRequired As Boolean = False


            Dim ds As New DataSet
            getRangeColumn(ResourceID, ModuleId, ds, "", intEntityId) 'table 0 name: RangeColumn
            getListColumn(ResourceID, ModuleId, ds, "", intEntityId) 'table 1 name: ListColumn
            getNoneColumn(ResourceID, ModuleId, ds, "", intEntityId) 'table 2 name: NoneColumn 
            If intList >= 1 Then
                getValueListColumn(ResourceID, ModuleId, ds, "", intEntityId) 'table 3 name: ValueList 
            End If

            pnl.Controls.Clear()
            pnl.Controls.Add(New LiteralControl("<table class=""contenttable"" cellSpacing=""0"" cellPadding=""0"" width=""100%"" align=""center"">"))
            'Create and Load Controls ie Textbox,Label,RangeValidator Control
            'and set the properties for controls and finally Load Controls
            If intRange >= 1 AndAlso ds.Tables("RangeColumn").Rows.Count = intRange Then

                For x = 0 To intRange - 1
                    isRequired = CType(ds.Tables("RangeColumn").Rows(x)("IsRequired"), Boolean)
                    'Create Labels For Ranges
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblR" & ds.Tables("RangeColumn").Rows(x)("SDFID").ToString()
                        .Text = ds.Tables("RangeColumn").Rows(x)("SDFDescrip").ToString()
                        .CssClass = "Label"
                    End With

                    dynText = New TextBox
                    With dynText
                        .ID = "txtR" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                        .CssClass = "TextBox"
                        If (ds.Tables("RangeColumn").Rows(x)("SDFVisibilty").ToString() = "View") Then
                            .ReadOnly = True
                        End If
                        .EnableViewState = True
                        .MaxLength = 50
                    End With

                    'Create Range Validator Control
                    'Create Range Validator Control
                    Dim uMinValue As String = ds.Tables("RangeColumn").Rows(x)("MinimumValue").ToString()
                    Dim uMaxValue As String = ds.Tables("RangeColumn").Rows(x)("MaximumValue").ToString()
                    dynRangeValidator = New RangeValidator
                    With dynRangeValidator
                        .ID = "RangeV" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                        'DE8735: NZ update------------------------------------------
                        Select Case ds.Tables("RangeColumn").Rows(x)("DType").ToString()
                            Case "Character"
                                .Type = ValidationDataType.String
                                uMinValue = "A"
                                uMaxValue = "Z"
                            Case "Numeric"
                                .Type = ValidationDataType.Integer
                                If Not IsNumeric(uMinValue) OrElse Not IsNumeric(uMinValue) Then .Enabled = False
                            Case "Date"
                                .Type = ValidationDataType.Date
                                If Not IsDate(uMinValue) OrElse Not IsDate(uMinValue) Then .Enabled = False
                            Case "Boolean"
                                'duh! does not exist!
                                .Enabled = False
                        End Select
                        '-----------------------------------------------------------
                        .ControlToValidate = dynText.ID.ToString
                        .Display = ValidatorDisplay.None
                        .MinimumValue = uMinValue
                        .MaximumValue = uMaxValue
                        .ErrorMessage = dynLabel.Text & " Should Be Between " & uMinValue & " and " & uMaxValue
                        .ClientIDMode = ClientIDMode.Static
                    End With

                    'Create requried field Validator Control
                    Dim dynRequiredValidator = New RequiredFieldValidator
                    Dim dynRequiredAsterisk = New Literal
                    If isRequired Then
                        With dynRequiredValidator
                            .ID = "reqFieldV" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                            .ControlToValidate = dynText.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                        End With
                        With dynRequiredAsterisk
                            .ID = "reqAsterisk" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If

                    pnl.Controls.Add(New LiteralControl("<tr><td class=""contentcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredAsterisk)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""contentcell4"">"))
                    pnl.Controls.Add(dynText)
                    pnl.Controls.Add(dynRangeValidator)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""emptycell""></td><td class=""contentcell"" nowrap></td><td class=""contentcell4"" nowrap></td></tr>"))
                Next
                'pnl.Controls.Add(New LiteralControl("<br>"))
            End If

            'Create and Load Controls ie Textbox,Label
            'and set the properties for controls and finally Load Controls
            If intNone >= 1 AndAlso ds.Tables("NoneColumn").Rows.Count = intNone Then

                For z = 0 To intNone - 1
                    isRequired = CType(ds.Tables("NoneColumn").Rows(z)("IsRequired"), Boolean)
                    'Create Labels For None Control
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblN" & ds.Tables("NoneColumn").Rows(z)("SDFID").ToString()
                        .Text = ds.Tables("NoneColumn").Rows(z)("SDFDescrip").ToString()

                        .CssClass = "Label"
                    End With

                    'Create TextBoxes For Ranges
                    dynText = New TextBox
                    With dynText
                        .ID = "txtN" & ds.Tables("NoneColumn").Rows(z)("SDFID").ToString()
                        .CssClass = "TextBox"
                        If (ds.Tables("NoneColumn").Rows(z)("SDFVisibilty").ToString() = "View") Then
                            .ReadOnly = True
                        End If
                        .MaxLength = 50
                    End With

                    'Create requried field Validator Control
                    Dim dynRequiredValidator2 = New RequiredFieldValidator
                    Dim dynRequiredAsterisk2 = New Literal
                    If isRequired Then
                        With dynRequiredValidator2
                            .ID = "reqFieldV2" & Replace(ds.Tables("NoneColumn").Rows(z)("SDFID").ToString(), "-", "")
                            .ControlToValidate = dynText.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                        End With
                        With dynRequiredAsterisk2
                            .ID = "reqAsterisk2" & Replace(ds.Tables("NoneColumn").Rows(z)("SDFID").ToString(), "-", "")
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If

                    pnl.Controls.Add(New LiteralControl("<tr><td class=""contentcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredAsterisk2)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""contentcell4"">"))
                    pnl.Controls.Add(dynText)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator2)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""emptycell""></td><td class=""contentcell"" nowrap></td><td class=""contentcell4"" nowrap></td></tr>"))
                Next
            End If
            'Create and Load Controls ie ListBox,Label
            'and set the properties for controls and finally Load Controls
            If intList >= 1 AndAlso ds.Tables("ListColumn").Rows.Count = intList Then

                For y = 0 To intList - 1
                    isRequired = CType(ds.Tables("ListColumn").Rows(y)("IsRequired"), Boolean)
                    'Create Labels For List
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblL" & ds.Tables("ListColumn").Rows(y)("SDFID").ToString()
                        .Text = ds.Tables("ListColumn").Rows(y)("SDFDescrip").ToString()
                        .CssClass = "Label"
                    End With

                    'Create TextBoxes For List
                    Dim strList As String
                    Dim strListArray() As String
                    dynList = New DropDownList
                    With dynList
                        .ID = "ddlL" & ds.Tables("ListColumn").Rows(y)("SDFID").ToString()
                        .CssClass = "DropDownList"
                        If (ds.Tables("ListColumn").Rows(y)("SDFVisibilty").ToString() = "View") Then
                            .Enabled = False
                        End If
                        Try
                            If ds.Tables("ValueList").Rows.Count >= 1 Then
                                strList = ds.Tables("ValueList").Rows(y)("ValList").ToString()
                                strListArray = strList.Split(",")
                            Else
                                strList = ""
                                strListArray = strList.Split(",")
                            End If
                            .DataSource = strListArray
                            .DataBind()
                            If Trim(strList) = "" Then
                                .Items.Clear()
                            End If
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                        End Try
                        .Items.Insert(0, New ListItem("Select", "0"))
                    End With

                    Dim dynRequiredValidator3 = New RequiredFieldValidator
                    Dim dynRequiredasterisk3 = New Literal
                    If isRequired Then
                        With dynRequiredValidator3
                            .ID = "reqFieldV3" & Replace(ds.Tables("ListColumn").Rows(y)("SDFID").ToString(), "-", "")
                            .ControlToValidate = dynList.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                            .InitialValue = "0"
                        End With
                        With dynRequiredasterisk3
                            .ID = "reqAsterisk3" & Replace(ds.Tables("ListColumn").Rows(y)("SDFID").ToString(), "-", "")
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If

                    pnl.Controls.Add(New LiteralControl("<tr><td class=""contentcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredasterisk3)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""contentcell4"">"))
                    pnl.Controls.Add(dynList)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator3)
                    End If

                    pnl.Controls.Add(New LiteralControl("</td><td class=""emptycell""></td><td class=""contentcell"" nowrap></td><td class=""contentcell4"" nowrap></td></tr>"))
                Next
            End If
            pnl.Controls.Add(New LiteralControl("</table>"))
        End Sub

        Public Sub GenerateControlsNewSingleCell(ByVal pnl As Panel, ByVal ResourceID As Integer,
                                                 ByVal strModuleId As String)
            '**************************************************************************************************
            'Purpose:       This Procedure Determines the Count Of Controls Available for the Resource(Page)
            '               and Populates the Controls Dynamically.Populates TextBox,Labels,DropDownList
            '               Control and finally adds it to panel control
            'Parameters:    ResourceID,Panel Control
            'Returns:       String
            'Notes:         This sub relies on the ResourceId when a request for the page is made.

            '**************************************************************************************************
            Dim intRange, intList, intNone As Integer
            Dim dynText As TextBox, dynList As DropDownList, dynLabel As Label, dynRangeValidator As RangeValidator
            Dim x, y, z As Integer
            Dim ModuleId As Integer = GetModuleID(strModuleId)
            Dim intEntityId As Integer = 0
            intEntityId = getEntity(ResourceID, "")
            'Call function and get the count of controls for a page.
            Dim strResult As String = GetControlCountByType(ModuleId, ResourceID, intEntityId)

            'Use Split Function and get the controls count 
            'in respective variables
            Dim arrRange() As String
            arrRange = strResult.Split(",")
            intRange = arrRange(0)
            intList = arrRange(1)
            intNone = arrRange(2)

            Dim isRequired As Boolean = False

            Dim ds As New DataSet
            getRangeColumn(ResourceID, ModuleId, ds, "", intEntityId) 'table 0 name: RangeColumn
            getListColumn(ResourceID, ModuleId, ds, "", intEntityId) 'table 1 name: ListColumn
            getNoneColumn(ResourceID, ModuleId, ds, "", intEntityId) 'table 2 name: NoneColumn 
            If intList >= 1 Then
                getValueListColumn(ResourceID, ModuleId, ds, "", intEntityId) 'table 3 name: ValueList 
            End If

            pnl.Controls.Clear()
            pnl.Controls.Add(New LiteralControl("<table class=""contenttable"" cellSpacing=""0"" cellPadding=""0"" width=""60%"" align=""center"">"))
            'Create and Load Controls ie Textbox,Label,RangeValidator Control
            'and set the properties for controls and finally Load Controls
            If intRange >= 1 Then
                For x = 0 To intRange - 1
                    'Create Labels For Ranges
                    isRequired = CType(ds.Tables("RangeColumn").Rows(x)("IsRequired"), Boolean)
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblR" & ds.Tables("RangeColumn").Rows(x)("SDFID").ToString()
                        .Text = ds.Tables("RangeColumn").Rows(x)("SDFDescrip").ToString()

                        .CssClass = "Label"
                    End With

                    dynText = New TextBox
                    With dynText
                        .ID = "txtR" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                        .CssClass = "TextBox"
                        If (ds.Tables("RangeColumn").Rows(x)("SDFVisibilty").ToString() = "View") Then
                            .ReadOnly = True
                        End If
                        .EnableViewState = True
                    End With

                    'Create Range Validator Control
                    Dim uMinValue As String = ds.Tables("RangeColumn").Rows(x)("MinimumValue").ToString()
                    Dim uMaxValue As String = ds.Tables("RangeColumn").Rows(x)("MaximumValue").ToString()
                    dynRangeValidator = New RangeValidator
                    With dynRangeValidator
                        .ID = "RangeV" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                        'DE8735: NZ update------------------------------------------
                        Select Case ds.Tables("RangeColumn").Rows(x)("DType").ToString()
                            Case "Character"
                                .Type = ValidationDataType.String
                                uMinValue = "A"
                                uMaxValue = "Z"
                            Case "Numeric"
                                .Type = ValidationDataType.Integer
                                If Not IsNumeric(uMinValue) OrElse Not IsNumeric(uMinValue) Then .Enabled = False
                            Case "Date"
                                .Type = ValidationDataType.Date
                                If Not IsDate(uMinValue) OrElse Not IsDate(uMinValue) Then .Enabled = False
                            Case "Boolean"
                                'duh! does not exist!
                                .Enabled = False
                        End Select
                        '-----------------------------------------------------------
                        .ControlToValidate = dynText.ID.ToString
                        .Display = ValidatorDisplay.None
                        .MinimumValue = uMinValue
                        .MaximumValue = uMaxValue
                        .ErrorMessage = dynLabel.Text & " Should Be Between " & uMinValue & " and " & uMaxValue
                        .ClientIDMode = ClientIDMode.Static
                    End With

                    'Create requried field Validator Control
                    Dim dynRequiredValidator = New RequiredFieldValidator
                    Dim dynRequiredAsterisk = New Literal
                    If isRequired Then
                        With dynRequiredValidator
                            .ID = "reqFieldV" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                            .ControlToValidate = dynText.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                        End With
                        With dynRequiredAsterisk
                            .ID = "reqAsterisk" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If


                    pnl.Controls.Add(New LiteralControl("<tr><td class=""twocolumnsdflabelcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredAsterisk)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""twocolumnsdfcontentcell"">"))
                    pnl.Controls.Add(dynText)
                    pnl.Controls.Add(dynRangeValidator)

                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator)
                    End If

                    pnl.Controls.Add(New LiteralControl("</td></tr>"))
                Next
                'pnl.Controls.Add(New LiteralControl("<br>"))
            End If

            'Create and Load Controls ie Textbox,Label
            'and set the properties for controls and finally Load Controls
            If intNone >= 1 Then
                For z = 0 To intNone - 1
                    'Create Labels For None Control
                    isRequired = CType(ds.Tables("NoneColumn").Rows(z)("IsRequired"), Boolean)
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblN" & ds.Tables("NoneColumn").Rows(z)("SDFID").ToString()
                        .Text = ds.Tables("NoneColumn").Rows(z)("SDFDescrip").ToString()

                        .CssClass = "Label"
                    End With

                    'Create TextBoxes For Ranges
                    dynText = New TextBox
                    With dynText
                        .ID = "txtN" & ds.Tables("NoneColumn").Rows(z)("SDFID").ToString()
                        .CssClass = "TextBox"
                        If (ds.Tables("NoneColumn").Rows(z)("SDFVisibilty").ToString() = "View") Then
                            .ReadOnly = True
                        End If
                    End With

                    'Create requried field Validator Control
                    Dim dynRequiredValidator2 = New RequiredFieldValidator
                    Dim dynRequiredAsterisk2 = New Literal
                    If isRequired Then
                        With dynRequiredValidator2
                            .ID = "reqFieldV2" & Replace(ds.Tables("NoneColumn").Rows(z)("SDFID").ToString(), "-", "")
                            .ControlToValidate = dynText.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                        End With
                        With dynRequiredAsterisk2
                            .ID = "reqAsterisk2" & Replace(ds.Tables("NoneColumn").Rows(z)("SDFID").ToString(), "-", "")
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If

                    pnl.Controls.Add(New LiteralControl("<tr><td class=""twocolumnsdflabelcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredAsterisk2)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""twocolumnsdfcontentcell"">"))
                    pnl.Controls.Add(dynText)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator2)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td></tr>"))
                Next
            End If
            'pnl.Controls.Add(New LiteralControl("</table>"))
            'pnl.Controls.Add(New LiteralControl("<br>"))

            'Create and Load Controls ie ListBox,Label
            'and set the properties for controls and finally Load Controls
            If intList >= 1 Then
                For y = 0 To intList - 1
                    'Create Labels For List
                    isRequired = CType(ds.Tables("ListColumn").Rows(y)("IsRequired"), Boolean)
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblL" & ds.Tables("ListColumn").Rows(y)("SDFID").ToString()
                        .Text = ds.Tables("ListColumn").Rows(y)("SDFDescrip").ToString()

                        .CssClass = "Label"
                    End With

                    'Create TextBoxes For List
                    Dim strList As String
                    Dim strListArray() As String
                    dynList = New DropDownList
                    With dynList

                        .ID = "ddlL" & ds.Tables("ListColumn").Rows(y)("SDFID").ToString()
                        .CssClass = "DropDownList"
                        If (ds.Tables("ListColumn").Rows(y)("SDFVisibilty").ToString() = "View") Then
                            .Enabled = False
                        End If
                        Try
                            If ds.Tables("ValueList").Rows.Count >= 1 Then
                                strList = ds.Tables("ValueList").Rows(y)("ValList").ToString()
                                strListArray = strList.Split(",")
                            Else
                                strList = ""
                                strListArray = strList.Split(",")
                            End If
                            .DataSource = strListArray
                            .DataBind()
                            If Trim(strList) = "" Then
                                .Items.Clear()
                            End If
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                        End Try
                        .Items.Insert(0, New ListItem("Select", "0"))
                    End With

                    Dim dynRequiredValidator3 = New RequiredFieldValidator
                    Dim dynRequiredAsterisk3 = New Literal
                    If isRequired Then
                        With dynRequiredValidator3
                            .ID = "reqFieldV3" & Replace(ds.Tables("ListColumn").Rows(y)("SDFID").ToString(), "-", "")
                            .ControlToValidate = dynList.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                            .InitialValue = "0"
                        End With
                        With dynRequiredAsterisk3
                            .ID = "reqAsterisk3" & Replace(ds.Tables("ListColumn").Rows(y)("SDFID").ToString(), "-", "")
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If


                    pnl.Controls.Add(New LiteralControl("<tr><td class=""twocolumnsdflabelcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredAsterisk3)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""twocolumnsdfcontentcell"">"))
                    pnl.Controls.Add(dynList)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator3)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td></tr>"))
                Next
            End If
            pnl.Controls.Add(New LiteralControl("</table>"))
        End Sub
#End Region

#Region "generate Edit"
        Public Sub GenerateControlsEdit(ByVal pnl As Panel, ByVal ResourceID As Integer, ByVal PKID As String,
                                        ByVal strModuleId As String)
            '**************************************************************************************************
            'Purpose:       This Procedure Determines the Count Of Controls Available for the Resource(Page)
            '               and Populates the Controls Dynamically.Populates TextBox,Labels,DropDownList
            '               Control and finally adds it to panel control.Values are Populated From The 
            '               Database.
            'Parameters:    ResourceID,Panel Control
            'Returns:       String
            'Notes:         This sub relies on the ResourceId when a request for the page is made.

            '**************************************************************************************************
            Dim intRange, intList, intNone As Integer
            Dim dynText As TextBox, dynList As DropDownList, dynLabel As Label, dynRangeValidator As RangeValidator
            Dim x, y, z As Integer
            Dim ModuleId As Integer = GetModuleID(strModuleId)
            Dim intEntityId As Integer = 0
            intEntityId = getEntity(ResourceID, PKID)
            Dim arrRange() As String = GetControlCountByType(ModuleId, ResourceID, intEntityId).Split(",")
            intRange = arrRange(0)
            intList = arrRange(1)
            intNone = arrRange(2)

            Dim ds As New DataSet
            getRangeColumn(ResourceID, ModuleId, ds, PKID, intEntityId) 'table 0 name: RangeColumn
            getListColumn(ResourceID, ModuleId, ds, PKID, intEntityId) 'table 1 name: ListColumn
            getNoneColumn(ResourceID, ModuleId, ds, PKID, intEntityId) 'table 2 name: NoneColumn 
            If intList >= 1 Then
                getValueListColumn(ResourceID, ModuleId, ds, PKID, intEntityId) 'table 3 name: ValueList 
            End If

            pnl.Controls.Clear()
            pnl.Controls.Add(New LiteralControl("<table id=""" + Guid.NewGuid().ToString() + """ class=""contenttable"" cellSpacing=""0"" cellPadding=""0"" width=""100%"" align=""center"">"))

            Dim isRequired As Boolean = False


            '------------------------------------------------------------
            'Create Dynamic Controls Here for Range
            If intRange >= 1 AndAlso ds.Tables("RangeColumn").Rows.Count = intRange Then

                pnl.Controls.Add(New LiteralControl("<tr height=2><td></td></tr>"))
                For x = 0 To intRange - 1
                    'Create Labels For Ranges
                    isRequired = CType(ds.Tables("RangeColumn").Rows(x)("IsRequired"), Boolean)
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblR" & ds.Tables("RangeColumn").Rows(x)("SDFID").ToString() & "_" & PKID
                        .Text = ds.Tables("RangeColumn").Rows(x)("SDFDescrip").ToString()
                        .CssClass = "Label"
                        .Style("left") = "5px"
                    End With

                    'Create TextBoxes For Ranges
                    dynText = New TextBox
                    With dynText
                        .ID = "txtR" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "") & "_" & PKID
                        Try
                            .Text = ds.Tables("RangeColumn").Rows(x)("Value").ToString()  'ds.Tables(0).Rows(x)("Value").ToString()
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            .Text = ""
                        End Try
                        .CssClass = "TextBox"
                        If (ds.Tables("RangeColumn").Rows(x)("SDFVisibilty").ToString() = "View") Then
                            .ReadOnly = True
                        End If
                        .EnableViewState = True
                        .ClientIDMode = ClientIDMode.Static
                    End With

                    'Create Range Validator Control
                    Dim uMinValue As String = ds.Tables("RangeColumn").Rows(x)("MinimumValue").ToString()
                    Dim uMaxValue As String = ds.Tables("RangeColumn").Rows(x)("MaximumValue").ToString()
                    dynRangeValidator = New RangeValidator
                    With dynRangeValidator
                        .ID = "RangeV" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "") & "_" & PKID
                        'DE8735: NZ update------------------------------------------
                        Select Case ds.Tables("RangeColumn").Rows(x)("DType").ToString()
                            Case "Character"
                                .Type = ValidationDataType.String
                                uMinValue = "A"
                                uMaxValue = "Z"
                            Case "Numeric"
                                .Type = ValidationDataType.Integer
                                If Not IsNumeric(uMinValue) OrElse Not IsNumeric(uMinValue) Then .Enabled = False
                            Case "Date"
                                .Type = ValidationDataType.Date
                                If Not IsDate(uMinValue) OrElse Not IsDate(uMinValue) Then .Enabled = False
                            Case "Boolean"
                                'duh! does not exist!
                                .Enabled = False
                        End Select
                        '-----------------------------------------------------------
                        .ControlToValidate = dynText.ID.ToString
                        .Display = ValidatorDisplay.None
                        .MinimumValue = uMinValue
                        .MaximumValue = uMaxValue
                        .ErrorMessage = dynLabel.Text & " Should Be Between " & uMinValue & " and " & uMaxValue
                        .ClientIDMode = ClientIDMode.Static
                    End With

                    'Create requried field Validator Control
                    Dim dynRequiredValidator = New RequiredFieldValidator
                    Dim dynRequiredAsterisk = New Literal
                    If isRequired Then
                        With dynRequiredValidator
                            .ID = "reqFieldV" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "") & "_" & PKID
                            .ControlToValidate = dynText.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                        End With
                        With dynRequiredAsterisk
                            .ID = "reqAsterisk" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "") & "_" & PKID
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If


                    pnl.Controls.Add(New LiteralControl("<tr><td class=""contentcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredAsterisk)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""contentcell4"">"))
                    pnl.Controls.Add(dynText)
                    pnl.Controls.Add(dynRangeValidator)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""emptycell""></td><td class=""contentcell"" nowrap></td><td class=""contentcell4"" nowrap></td></tr>"))
                Next
                'pnl.Controls.Add(New LiteralControl("<br>"))
            End If

            'For None Controls
            If intNone >= 1 And ds.Tables("NoneColumn").Rows.Count = intNone Then

                For z = 0 To intNone - 1
                    'Create Labels For Ranges
                    isRequired = CType(ds.Tables("NoneColumn").Rows(z)("IsRequired"), Boolean)
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblN" & ds.Tables("NoneColumn").Rows(z)("SDFID").ToString() & "_" & PKID
                        .Text = ds.Tables("NoneColumn").Rows(z)("SDFDescrip").ToString()
                        .CssClass = "Label"
                    End With

                    'Create TextBoxes For Ranges
                    dynText = New TextBox
                    With dynText
                        .ID = "txtN" & ds.Tables("NoneColumn").Rows(z)("SDFID").ToString() & "_" & PKID
                        .CssClass = "TextBox"
                        .EnableViewState = True
                        Try
                            .Text = ds.Tables("NoneColumn").Rows(z)("Value").ToString()
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            .Text = ""
                        End Try
                        If (ds.Tables("NoneColumn").Rows(z)("SDFVisibilty").ToString() = "View") Then
                            .ReadOnly = True
                        End If
                    End With

                    'Create requried field Validator Control
                    Dim dynRequiredValidator2 = New RequiredFieldValidator
                    Dim dynRequiredAsterisk2 = New LiteralControl
                    If isRequired Then
                        With dynRequiredValidator2
                            .ID = "reqFieldV2" & Replace(ds.Tables("NoneColumn").Rows(z)("SDFID").ToString(), "-", "") & "_" & PKID
                            .ControlToValidate = dynText.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                        End With
                        With dynRequiredAsterisk2
                            .ID = "reqAsterisk2" & Replace(ds.Tables("NoneColumn").Rows(z)("SDFID").ToString(), "-", "") & "_" & PKID
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If

                    pnl.Controls.Add(New LiteralControl("<tr><td class=""contentcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredAsterisk2)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""contentcell4"">"))
                    pnl.Controls.Add(dynText)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator2)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""emptycell""></td><td class=""contentcell"" nowrap></td><td class=""contentcell4"" nowrap></td></tr>"))
                Next
            End If
            'pnl.Controls.Add(New LiteralControl("</table>"))
            'pnl.Controls.Add(New LiteralControl("<br>"))

            'Create Dynamic Controls for List
            If intList >= 1 And ds.Tables("ListColumn").Rows.Count = intList Then

                For y = 0 To intList - 1
                    'Create Labels For List
                    isRequired = CType(ds.Tables("ListColumn").Rows(y)("IsRequired"), Boolean)
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblL" & ds.Tables("ListColumn").Rows(y)("SDFID").ToString() & "_" & PKID
                        .Text = ds.Tables("ListColumn").Rows(y)("SDFDescrip").ToString()
                        .CssClass = "Label"
                    End With

                    'Create TextBoxes For Ranges
                    Dim strList As String
                    Dim strListArray() As String
                    dynList = New DropDownList
                    With dynList
                        Try

                            .ID = "ddlL" & ds.Tables("ListColumn").Rows(y)("SDFID").ToString() & "_" & PKID
                            .CssClass = "DropDownList"
                            If (ds.Tables("ListColumn").Rows(y)("SDFVisibilty").ToString() = "View") Then
                                .Enabled = False
                            End If
                            Try
                                If ds.Tables("ValueList").Rows.Count >= 1 Then
                                    strList = ds.Tables("ValueList").Rows(y)("ValList").ToString()
                                    strListArray = strList.Split(",")
                                Else
                                    strList = ""
                                    strListArray = strList.Split(",")
                                End If
                                .DataSource = strListArray
                                .DataBind()
                                If Trim(strList) = "" Then
                                    .Items.Clear()
                                End If
                            Catch ex As Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                            End Try
                            .Items.Insert(0, New ListItem("Select", "No Value Selected"))
                            If Trim(ds.Tables("ListColumn").Rows(y)("Value").ToString()) = "" Then
                                .SelectedIndex = 0
                            End If
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            .ID = "ddlL" & ds.Tables("ListColumn").Rows(y)("SDFID").ToString() & "_" & PKID
                            .CssClass = "DropDownList"
                            .Items.Insert(0, New ListItem("Select", "No Value Selected"))
                            If (ds.Tables("ListColumn").Rows(y)("SDFVisibilty").ToString() = "View") Then
                                .Enabled = False
                            End If
                        End Try
                    End With
                    If Trim(ds.Tables("ListColumn").Rows(y)("Value").ToString()) <> "" Then
                        Try
                            dynList.SelectedValue = ds.Tables("ListColumn").Rows(y)("Value").ToString()
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            dynList.SelectedIndex = 0
                        End Try
                    End If

                    Dim dynRequiredValidator3 = New RequiredFieldValidator
                    Dim dynRequiredasterisk3 = New Literal
                    If isRequired Then
                        With dynRequiredValidator3
                            .ID = "reqFieldV3" & Replace(ds.Tables("ListColumn").Rows(y)("SDFID").ToString(), "-", "") & "_" & PKID
                            .ControlToValidate = dynList.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                            .InitialValue = "0"
                        End With
                        With dynRequiredasterisk3
                            .ID = "reqAsterisk3" & Replace(ds.Tables("ListColumn").Rows(y)("SDFID").ToString(), "-", "") & "_" & PKID
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If
                    pnl.Controls.Add(New LiteralControl("<tr><td class=""contentcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredasterisk3)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""contentcell4"">"))
                    pnl.Controls.Add(dynList)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator3)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""emptycell""></td><td class=""contentcell"" nowrap></td><td class=""contentcell4"" nowrap></td></tr>"))
                Next
            End If
            pnl.Controls.Add(New LiteralControl("</table>"))
        End Sub
        Public Sub GenerateControlsEditSingleCell(ByVal pnl As Panel, ByVal ResourceID As Integer,
                                                  ByVal PKID As String, ByVal strModuleId As String,
                                                  Optional ByVal intEntityId As Integer = 0)
            '**************************************************************************************************
            'Purpose:       This Procedure Determines the Count Of Controls Available for the Resource(Page)
            '               and Populates the Controls Dynamically.Populates TextBox,Labels,DropDownList
            '               Control and finally adds it to panel control.Values are Populated From The 
            '               Database.
            'Parameters:    ResourceID,Panel Control
            'Returns:       String
            'Notes:         This sub relies on the ResourceId when a request for the page is made.

            '**************************************************************************************************
            Dim intRange, intList, intNone As Integer

            Dim dynText As TextBox, dynList As DropDownList, dynLabel As Label, dynRangeValidator As RangeValidator
            Dim x, y, z As Integer
            Dim ModuleId As Integer = GetModuleID(strModuleId)
            intEntityId = getEntity(ResourceID, PKID)
            Dim strResult As String = GetControlCountByType(ModuleId, ResourceID, intEntityId)
            Dim arrRange() As String
            arrRange = strResult.Split(",")
            intRange = arrRange(0)
            intList = arrRange(1)
            intNone = arrRange(2)

            Dim isRequired As Boolean = False

            Dim ds As New DataSet
            getRangeColumn(ResourceID, ModuleId, ds, PKID, intEntityId) 'table 0 name: "RangeColumn"
            getListColumn(ResourceID, ModuleId, ds, PKID, intEntityId) 'table 1 name: ListColumn
            getNoneColumn(ResourceID, ModuleId, ds, PKID, intEntityId) 'table 2 name: NoneColumn 
            If intList >= 1 Then
                getValueListColumn(ResourceID, ModuleId, ds, PKID, intEntityId) 'table 3 name: ValueList 
            End If

            pnl.Controls.Clear()
            pnl.Controls.Add(New LiteralControl("<table class=""contenttable"" cellSpacing=""0"" cellPadding=""0"" width=""60%"" align=""center"">"))

            If intRange >= 1 AndAlso ds.Tables("RangeColumn").Rows.Count = intRange Then
                pnl.Controls.Add(New LiteralControl("<tr height=2><td></td></tr>"))
                For x = 0 To intRange - 1
                    isRequired = CType(ds.Tables("RangeColumn").Rows(x)("IsRequired"), Boolean)
                    'Create Labels For Ranges
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblR" & ds.Tables("RangeColumn").Rows(x)("SDFID").ToString()
                        .Text = ds.Tables("RangeColumn").Rows(x)("SDFDescrip").ToString()
                        .CssClass = "Label"
                        .Style("left") = "5px"
                    End With

                    'Create TextBoxes For Ranges
                    dynText = New TextBox
                    With dynText
                        .ID = "txtR" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                        .Text = ds.Tables("RangeColumn").Rows(x)("Value").ToString()
                        .CssClass = "TextBox"
                        If (ds.Tables("RangeColumn").Rows(x)("SDFVisibilty").ToString() = "View") Then
                            .ReadOnly = True
                        End If
                        .EnableViewState = True

                    End With

                    'Create Range Validator Control
                    Dim uMinValue As String = ds.Tables("RangeColumn").Rows(x)("MinimumValue").ToString()
                    Dim uMaxValue As String = ds.Tables("RangeColumn").Rows(x)("MaximumValue").ToString()
                    dynRangeValidator = New RangeValidator
                    With dynRangeValidator
                        .ID = "RangeV" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                        'DE8735: NZ update------------------------------------------
                        Select Case ds.Tables("RangeColumn").Rows(x)("DType").ToString()
                            Case "Character"
                                .Type = ValidationDataType.String
                                uMinValue = "A"
                                uMaxValue = "Z"
                            Case "Numeric"
                                .Type = ValidationDataType.Integer
                                If Not IsNumeric(uMinValue) OrElse Not IsNumeric(uMinValue) Then .Enabled = False
                            Case "Date"
                                .Type = ValidationDataType.Date
                                If Not IsDate(uMinValue) OrElse Not IsDate(uMinValue) Then .Enabled = False
                            Case "Boolean"
                                'duh! does not exist!
                                .Enabled = False
                        End Select
                        '-----------------------------------------------------------
                        .ControlToValidate = dynText.ID.ToString
                        .Display = ValidatorDisplay.None
                        .MinimumValue = uMinValue
                        .MaximumValue = uMaxValue
                        .ErrorMessage = dynLabel.Text & " Should Be Between " & uMinValue & " and " & uMaxValue
                        .ClientIDMode = ClientIDMode.Static
                    End With

                    Dim dynRequiredValidator = New RequiredFieldValidator
                    Dim dynRequiredAsterisk = New Literal
                    If isRequired Then
                        With dynRequiredValidator
                            .ID = "reqFieldV" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                            .ControlToValidate = dynText.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                        End With
                        With dynRequiredAsterisk
                            .ID = "reqAsterisk" & Replace(ds.Tables("RangeColumn").Rows(x)("SDFID").ToString(), "-", "")
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If

                    pnl.Controls.Add(New LiteralControl("<tr><td class=""twocolumnsdflabelcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredAsterisk)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""twocolumnsdfcontentcell"">"))
                    pnl.Controls.Add(dynText)
                    pnl.Controls.Add(dynRangeValidator)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td></tr>"))
                Next
                'pnl.Controls.Add(New LiteralControl("<br>"))
            End If

            'For None Controls
            If intNone >= 1 AndAlso ds.Tables("NoneColumn").Rows.Count = intNone Then
                For z = 0 To intNone - 1
                    isRequired = CType(ds.Tables("NoneColumn").Rows(z)("IsRequired"), Boolean)
                    'Create Labels For Ranges
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblN" & ds.Tables("NoneColumn").Rows(z)("SDFID").ToString()
                        .Text = ds.Tables("NoneColumn").Rows(z)("SDFDescrip").ToString()
                        .CssClass = "Label"
                    End With

                    'Create TextBoxes For Ranges
                    dynText = New TextBox
                    With dynText
                        .ID = "txtN" & ds.Tables("NoneColumn").Rows(z)("SDFID").ToString()
                        .CssClass = "TextBox"
                        .EnableViewState = True
                        .Text = ds.Tables("NoneColumn").Rows(z)("Value").ToString()
                        If (ds.Tables("NoneColumn").Rows(z)("SDFVisibilty").ToString() = "View") Then
                            .ReadOnly = True
                        End If
                    End With

                    'Create requried field Validator Control
                    Dim dynRequiredValidator2 = New RequiredFieldValidator
                    Dim dynRequiredAsterisk2 = New LiteralControl
                    If isRequired Then
                        With dynRequiredValidator2
                            .ID = "reqFieldV2" & Replace(ds.Tables("NoneColumn").Rows(z)("SDFID").ToString(), "-", "")
                            .ControlToValidate = dynText.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                        End With
                        With dynRequiredAsterisk2
                            .ID = "reqAsterisk2" & Replace(ds.Tables("NoneColumn").Rows(z)("SDFID").ToString(), "-", "")
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If

                    pnl.Controls.Add(New LiteralControl("<tr><td class=""twocolumnsdflabelcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredAsterisk2)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""twocolumnsdfcontentcell"">"))
                    pnl.Controls.Add(dynText)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator2)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td></tr>"))
                Next
            End If
            'Create Dynamic Controls for List
            If intList >= 1 And ds.Tables("ListColumn").Rows.Count = intList Then
                For y = 0 To intList - 1
                    isRequired = CType(ds.Tables("ListColumn").Rows(y)("IsRequired"), Boolean)
                    'Create Labels For List
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblL" & ds.Tables("ListColumn").Rows(y)("SDFID").ToString()
                        .Text = ds.Tables("ListColumn").Rows(y)("SDFDescrip").ToString()
                        .CssClass = "Label"
                    End With
                    'Create TextBoxes For Ranges
                    Dim strList As String
                    Dim strListArray() As String
                    dynList = New DropDownList
                    With dynList

                        .ID = "ddlL" & ds.Tables("ListColumn").Rows(y)("SDFID").ToString()
                        .CssClass = "DropDownList"
                        If (ds.Tables("ListColumn").Rows(y)("SDFVisibilty").ToString() = "View") Then
                            .Enabled = False
                        End If
                        Try
                            If ds.Tables("ValueList").Rows.Count >= 1 Then
                                strList = ds.Tables("ValueList").Rows(y)("ValList").ToString()
                                strListArray = strList.Split(",")
                            Else
                                strList = ""
                                strListArray = strList.Split(",")
                            End If
                            .DataSource = strListArray
                            .DataBind()
                            If Trim(strList) = "" Then
                                .Items.Clear()
                            End If
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                        End Try
                        .Items.Insert(0, New ListItem("Select", ""))
                    End With
                    If Trim(ds.Tables("ListColumn").Rows(y)("Value").ToString()) <> "" Then
                        dynList.SelectedValue = ds.Tables("ListColumn").Rows(y)("Value").ToString()
                    End If

                    Dim dynRequiredValidator3 = New RequiredFieldValidator
                    Dim dynRequiredasterisk3 = New Literal
                    If isRequired Then
                        With dynRequiredValidator3
                            .ID = "reqFieldV3" & Replace(ds.Tables("ListColumn").Rows(y)("SDFID").ToString(), "-", "")
                            .ControlToValidate = dynList.ID.ToString
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = dynLabel.Text & " is required."
                            .ClientIDMode = ClientIDMode.Static
                            .InitialValue = "0"
                        End With
                        With dynRequiredasterisk3
                            .ID = "reqAsterisk3" & Replace(ds.Tables("ListColumn").Rows(y)("SDFID").ToString(), "-", "")
                            .Text = "<span style='color:red;'>*</span>"
                        End With
                    End If

                    pnl.Controls.Add(New LiteralControl("<tr><td class=""twocolumnsdflabelcell"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredasterisk3)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td><td class=""twocolumnsdfcontentcell"">"))
                    pnl.Controls.Add(dynList)
                    If isRequired Then
                        pnl.Controls.Add(dynRequiredValidator3)
                    End If
                    pnl.Controls.Add(New LiteralControl("</td></tr>"))
                Next
            End If
            pnl.Controls.Add(New LiteralControl("</table>"))
        End Sub
#End Region

        Public Sub DeleteSDFValue(ByVal pgPKID As String)
            '**************************************************************************************************
            'Purpose:       This Procedure Is To Delete The SDF Based on Unique Value
            'Parameters:    Primary Key In the Page
            '**************************************************************************************************
            'Commented on 10/5/2007 to resolve the issue with galen 
            'The InsertValues function has been modified to update and insert the SDF


            'Dim sb As New System.Text.StringBuilder
            'Dim db As New DataAccess
            'Try
            '    db.ConnectionString = SingletonAppSettings.AppSettings("Constring")
            '    '   build query
            '    With sb
            '        .Append("Delete from sySDFModuleValue where pgPKID = ? ")
            '    End With
            '    db.AddParameter("@SDFPKID", pgPKID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
            '    'execute query
            '    db.RunParamSQLExecuteNoneQuery(sb.ToString)
            '    db.ClearParameters()
            '    sb.Remove(0, sb.Length)
            '    'db.CloseConnection()
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            'Finally
            '    db.CloseConnection()
            'End Try
        End Sub
        Public Function InsertValues(ByVal pgPKID As String, ByVal SDFID As String, ByVal SDFValue As String, Optional ByVal ModUser As String = "") As String
            '**************************************************************************************************
            'Purpose:       This Procedure Is To Insert Values in to sySDFModuleValue Table
            'Parameters:    Primary Key On Page,SDFID,SDFValue
            '**************************************************************************************************
            Dim sb As StringBuilder
            Dim db As New DataAccess
            Dim intCheckIfValueExistsForSDF As Integer = 0

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                db.ConnectionString = MyAdvAppSettings.AppSettings("Constring")
                'Check if any value exists for the School Defined Field and Entity
                sb = New StringBuilder
                With sb
                    .Append("Select Count(*) from sySDFModuleValue where pgPKID=? and SDFID=? ")
                End With
                db.ClearParameters()
                db.AddParameter("@pgPKID", pgPKID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                Try
                    intCheckIfValueExistsForSDF = db.RunParamSQLScalar(sb.ToString)
                    If intCheckIfValueExistsForSDF >= 1 Then
                        intCheckIfValueExistsForSDF = 1
                    Else
                        intCheckIfValueExistsForSDF = 0
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    intCheckIfValueExistsForSDF = 0
                End Try
                sb.Remove(0, sb.Length)

                If intCheckIfValueExistsForSDF = 1 Then
                    'If the value selected is "No Value Selected" then we need to delete the existing record.
                    'Otherwise we will add it.
                    sb = New StringBuilder

                    If SDFValue = "No Value Selected" Then
                        With sb
                            .Append("Delete From sySDFModuleValue Where pgPKID=? and SDFID=? ")
                        End With
                        db.ClearParameters()
                        db.AddParameter("@pgPKID", pgPKID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    Else
                        With sb
                            .Append("Update sySDFModuleValue set SDFValue=?,ModUser=?,ModDate=? where pgPKID=? and SDFID=? ")
                        End With

                        'add parameters
                        db.ClearParameters()
                        db.AddParameter("@SDFValue", SDFValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ModUser", ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        db.AddParameter("@pgPKID", pgPKID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                    End If


                    'execute query
                    Try
                        db.RunParamSQLExecuteNoneQuery(sb.ToString)
                        Return ""
                    Catch ex1 As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex1)

                        Dim strSDFDescrip As String
                        sb.Remove(0, sb.Length)
                        sb = New StringBuilder
                        With sb
                            .Append("Select Top 1 SDFDescrip from sySDF where SDFID=? ")
                        End With
                        'add parameters
                        db.ClearParameters()
                        db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Try
                            strSDFDescrip = db.RunParamSQLScalar(sb.ToString)
                            Return "An error occured while updating the UDF " & strSDFDescrip
                        Catch ex2 As Exception
                        	exTracker.TrackExceptionWrapper(ex2)

                            Return "An unexpected error occured while updating the UDF"
                            Exit Function
                        End Try
                    End Try
                Else
                    'We should only insert when the value is not "No Value Selected"
                    If SDFValue <> "No Value Selected" Then
                        sb = New StringBuilder
                        With sb
                            .Append("INSERT INTO sySDFModuleValue(pgPKID,SDFID,SDFValue,ModUser,ModDate) ")
                            .Append("VALUES(?,?,?,?,?)")
                        End With

                        'add parameters
                        db.ClearParameters()
                        db.AddParameter("@pgPKID", pgPKID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@SDFValue", SDFValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ModUser", ModUser, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        db.AddParameter("@ModDate", Date.Now, DataAccess.OleDbDataType.OleDbDateTime, , ParameterDirection.Input)
                        'execute query
                        Try
                            db.RunParamSQLExecuteNoneQuery(sb.ToString)
                            Return ""
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            Dim strSdfDescrip As String
                            sb.Remove(0, sb.Length)
                            sb = New StringBuilder
                            With sb
                                .Append("Select Top 1 SDFDescrip from sySDF where SDFID=? ")
                            End With
                            'add parameters
                            db.ClearParameters()
                            db.AddParameter("@SDFID", SDFID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                            Try
                                strSdfDescrip = CType(db.RunParamSQLScalar(sb.ToString), String)
                                Return "An error occurred while adding the UDF " & strSdfDescrip
                            Catch ex3 As Exception
                            	exTracker.TrackExceptionWrapper(ex3)

                                Return "An unexpected error occurred while adding the UDF"
                            End Try
                        End Try


                    End If

                End If
            Finally
                db.CloseConnection()
            End Try
            Return String.Empty
        End Function

    End Class
End Namespace
