Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.IO
Imports log4net.DateFormatter
Imports System.Data

Public Class RptExport
    Inherits System.Web.UI.Page

#Region "Private data members"

    Private _mFileName As String
    Private _mExportOpts As New ExportOptions
    Private _mDiskOpts As New DiskFileDestinationOptions
    Private _objCommon As New FAME.Common.CommonUtilities

#End Region

#Region "Public methods"

    Public Sub ExportToPdf(ByVal pgContext As HttpContext,
                           ByVal objReport As ReportDocument,
                           ByVal strId As String,
                           Optional ByVal strFileBasename As String = "AdvantageReport")

        strFileBasename = ResetEmptyName(_objCommon.RemoveInvalidFileNameChars(strFileBasename))
        CreateDirectoryIfDoesNotExist(Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host"))
        CreateDirectoryIfDoesNotExist(Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host") & "\Exports\")
        _mFileName = Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host") & "\Exports\" & strId & ".pdf"
        _mDiskOpts.DiskFileName = _mFileName
        _mExportOpts = objReport.ExportOptions
        With _mExportOpts
            .DestinationOptions = _mDiskOpts
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.PortableDocFormat
        End With

        objReport.Export()

        pgContext.Response.ClearContent()
        pgContext.Response.ClearHeaders()
        pgContext.Response.AddHeader("Content-Disposition", "attachment; filename=" & strFileBasename & ".pdf")
        pgContext.Response.ContentType = "application/pdf"
        pgContext.Response.WriteFile(_mFileName)
        pgContext.Response.Flush()

        File.Delete(_mFileName)
    End Sub

    Public Sub ExportToMsWord(ByVal pgContext As HttpContext,
                              ByVal objReport As ReportDocument,
                              ByVal strId As String,
                              Optional ByVal strFileBasename As String = "AdvantageReport")

        strFileBasename = ResetEmptyName(_objCommon.RemoveInvalidFileNameChars(strFileBasename))
        CreateDirectoryIfDoesNotExist(Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host"))
        CreateDirectoryIfDoesNotExist(Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host") & "\Exports\")
        _mFileName = Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host") & "\Exports\" & strId & ".doc"
        _mDiskOpts.DiskFileName = _mFileName
        _mExportOpts = objReport.ExportOptions
        With _mExportOpts
            .DestinationOptions = _mDiskOpts
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.WordForWindows
        End With

        objReport.Export()

        pgContext.Response.ClearContent()
        pgContext.Response.ClearHeaders()
        pgContext.Response.AddHeader("Content-Disposition", "attachment; filename=" & strFileBasename & ".doc")
        pgContext.Response.ContentType = "application/msword"
        pgContext.Response.WriteFile(_mFileName)
        pgContext.Response.Flush()

        File.Delete(_mFileName)
    End Sub

    Public Sub ExportToMsExcel(ByVal pgContext As HttpContext,
                               ByVal objReport As ReportDocument,
                               ByVal strId As String,
                               Optional ByVal strFileBasename As String = "AdvantageReport")

        strFileBasename = ResetEmptyName(_objCommon.RemoveInvalidFileNameChars(strFileBasename))
        CreateDirectoryIfDoesNotExist(Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host"))
        CreateDirectoryIfDoesNotExist(Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host") & "\Exports\")
        _mFileName = Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host") & "\Exports\" & strId & ".xls"
        _mDiskOpts.DiskFileName = _mFileName
        _mExportOpts = objReport.ExportOptions
        Dim excelFormatOpts As New ExcelFormatOptions
        excelFormatOpts.ExcelTabHasColumnHeadings = True
        excelFormatOpts.ExcelUseConstantColumnWidth = False
        With _mExportOpts
            .DestinationOptions = _mDiskOpts
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.Excel
        End With

        objReport.Export()

        pgContext.Response.ClearContent()
        pgContext.Response.ClearHeaders()
        pgContext.Response.AddHeader("Content-Disposition", "attachment; filename=" & strFileBasename & ".xls")
        pgContext.Response.ContentType = "application/vnd.ms-excel"
        pgContext.Response.WriteFile(_mFileName)
        pgContext.Response.Flush()

        File.Delete(_mFileName)
    End Sub

    Public Sub ExportToMsExcelData(ByVal pgContext As HttpContext,
                                   ByVal objReport As ReportDocument,
                                   ByVal strId As String,
                                   Optional ByVal strFileBasename As String = "AdvantageReport")

        strFileBasename = ResetEmptyName(_objCommon.RemoveInvalidFileNameChars(strFileBasename))
        CreateDirectoryIfDoesNotExist(Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host"))
        CreateDirectoryIfDoesNotExist(Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host") & "\Exports\")
        _mFileName = Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host") & "\Exports\" & strId & ".xls"
        _mDiskOpts.DiskFileName = _mFileName
        _mExportOpts = objReport.ExportOptions
        Dim excelFormatOpts As New ExcelFormatOptions
        excelFormatOpts.ExcelTabHasColumnHeadings = True
        excelFormatOpts.ExcelUseConstantColumnWidth = False

        With _mExportOpts
            .DestinationOptions = _mDiskOpts
            .ExportDestinationType = ExportDestinationType.DiskFile
            .ExportFormatType = ExportFormatType.ExcelRecord
        End With

        objReport.Export()

        pgContext.Response.ClearContent()
        pgContext.Response.ClearHeaders()
        pgContext.Response.AddHeader("Content-Disposition", "attachment; filename=" & strFileBasename & ".xls")
        pgContext.Response.ContentType = "application/vnd.ms-excel"
        pgContext.Response.WriteFile(_mFileName)
        pgContext.Response.Flush()

        File.Delete(_mFileName)
    End Sub

    Public Function ExportToDisk(ByRef pgContext As HttpContext,
                            ByVal objReport As ReportDocument,
                            ByVal strId As String,
                            ByVal format As String,
                            ByRef contentType As String,
                            Optional ByVal strFileBasename As String = "AdvantageReport") As String

        objReport.SetDatabaseLogon(CType(Session("DBUserId"), String), CType(Session("DBPassword"), String), CType(Session("ServerName"), String), CType(Session("DBName"), String))

        strFileBasename = ResetEmptyName(_objCommon.RemoveInvalidFileNameChars(strFileBasename))
        CreateDirectoryIfDoesNotExist(Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host"))
        CreateDirectoryIfDoesNotExist(Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host") & "\Exports\")

        _mExportOpts = objReport.ExportOptions
        Dim excelFormatOpts As New ExcelFormatOptions
        excelFormatOpts.ExcelTabHasColumnHeadings = True
        excelFormatOpts.ExcelUseConstantColumnWidth = False

        With _mExportOpts
            .DestinationOptions = _mDiskOpts
            .ExportDestinationType = ExportDestinationType.DiskFile
        End With

        contentType = ""
        Dim extension = ""

        Select Case Trim(format).ToLower
            Case "pdf"
                _mExportOpts.ExportFormatType = ExportFormatType.PortableDocFormat
                contentType = "application/pdf"
                extension = ".pdf"
            Case "xls"
                _mExportOpts.ExportFormatType = ExportFormatType.Excel
                contentType = "application/vnd.ms-excel"
                extension = ".xls"
            Case "doc"
                _mExportOpts.ExportFormatType = ExportFormatType.WordForWindows
                contentType = "application/msword"
                extension = ".doc"
            Case "xls1"
                _mExportOpts.ExportFormatType = ExportFormatType.ExcelRecord
                contentType = "application/vnd.ms-excel"
                extension = ".xls"
        End Select
        _mFileName = Server.MapPath("..") & "\" & pgContext.Request.Headers.Item("host") & "\Exports\" & strId & extension
        _mDiskOpts.DiskFileName = _mFileName

        pgContext.Response.AddHeader("Content-Disposition", "attachment; filename=" & strFileBasename & extension)

        pgContext.Response.ContentType = contentType
        objReport.Export()
        pgContext.Response.WriteFile(_mFileName)
        pgContext.Response.Flush()

        Dim path = _mFileName

        _mExportOpts = Nothing
        _mDiskOpts = Nothing
        _mFileName = Nothing

        If (Not objReport Is Nothing) Then
            objReport.Close()
            objReport.Dispose()
        End If

        Return path
    End Function

    Public Sub ExportToStream(ByRef pgContext As HttpContext,
                              ByVal objReport As ReportDocument,
                              ByVal format As String,
                              Optional ByVal strFileBasename As String = "AdvantageReport")

        objReport.SetDatabaseLogon(CType(Session("DBUserId"), String), CType(Session("DBPassword"), String), CType(Session("ServerName"), String), CType(Session("DBName"), String))

        Dim MyExportOptions As New CrystalDecisions.Shared.ExportOptions

        strFileBasename = ResetEmptyName(_objCommon.RemoveInvalidFileNameChars(strFileBasename))

        Dim extension = ""
        Select Case Trim(format).ToLower
            Case "pdf"
                MyExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat
                extension = ".pdf"
            Case "xls"
                MyExportOptions.ExportFormatType = ExportFormatType.Excel
                extension = ".xls"
            Case "doc"
                MyExportOptions.ExportFormatType = ExportFormatType.WordForWindows
                extension = ".doc"
            Case "xls1"
                MyExportOptions.ExportFormatType = ExportFormatType.ExcelRecord
                extension = ".xls"
        End Select


        Dim stream = objReport.ExportToStream(MyExportOptions.ExportFormatType)
        pgContext.Response.ContentType = "application/octet-stream"
        pgContext.Response.AddHeader("Content-Disposition", "attachment; filename=" & strFileBasename & extension)
        stream.CopyTo(pgContext.Response.OutputStream)
        pgContext.Response.Flush()
        'pgContext.Response.End()

        _mExportOpts = Nothing
        _mDiskOpts = Nothing
        If (Not objReport Is Nothing) Then
            objReport.Close()
            objReport.Dispose()
        End If
    End Sub

#End Region

#Region "Private Methods"

    Private Function ResetEmptyName(ByVal strFileBasename As String) As String
        Return CType(IIf(String.IsNullOrEmpty(strFileBasename), "AdvantageReport", strFileBasename), String)
    End Function

    Private Sub CreateDirectoryIfDoesNotExist(ByVal path As String)
        If (Not Directory.Exists(path)) Then

            Try
                Directory.CreateDirectory(path)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New Exception("Unable to create Export Directory")
            End Try
        End If
    End Sub

#End Region

End Class
