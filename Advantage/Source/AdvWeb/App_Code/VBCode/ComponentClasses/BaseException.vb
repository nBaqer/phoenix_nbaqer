Imports System
Imports System.Web

Namespace FAME.ExceptionLayer
    Public Class BaseException
        Inherits System.ApplicationException
        Private m_PageAddress As String
        Private m_Context As HttpContext
        Public Sub New()
            MyBase.New()

            m_Context = HttpContext.Current
            m_PageAddress = m_Context.Request.Url.AbsolutePath.ToString
        End Sub

        Public Sub New(ByVal message As String)
            MyBase.New(message)
            m_Context = HttpContext.Current
            m_PageAddress = m_Context.Request.Url.AbsolutePath.ToString
        End Sub

        Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
            MyBase.New(message, innerException)
            m_Context = HttpContext.Current
            m_PageAddress = m_Context.Request.Url.AbsolutePath.ToString
        End Sub

        Public ReadOnly Property PageName() As String
            Get
                Return m_PageAddress
            End Get
        End Property

        Private m_AssemblyName As String = System.Reflection.Assembly.GetCallingAssembly.GetName.Name & ".dll"
        Public ReadOnly Property AssemblyName() As String
            Get
                Return m_AssemblyName
            End Get
        End Property
        Private m_AssemblyVersion As String = System.Reflection.Assembly.GetCallingAssembly.GetName.Version.ToString
        Public ReadOnly Property AssemblyVersion() As String
            Get
                Return m_AssemblyVersion
            End Get
        End Property
        Private m_TimeStamp As DateTime = New System.DateTime(System.DateTime.Now.Ticks)
        Public ReadOnly Property TimeStamp() As System.DateTime
            Get
                Return m_TimeStamp
            End Get
        End Property
        Public Overrides ReadOnly Property Message() As String
            Get
                Return MyBase.Message & _
                    "(Page Name: " & Me.PageName & "; Assembly Name: " & Me.AssemblyName & "; Assembly Version: " & Me.AssemblyVersion & "; Time Stamp: " & Me.TimeStamp & ")"
            End Get
        End Property
    End Class
End Namespace

