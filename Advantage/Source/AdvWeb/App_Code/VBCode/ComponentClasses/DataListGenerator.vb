
Imports FAME.ExceptionLayer
Imports System.Data
Imports System.Web
Imports FAME.AdvantageV1.DataAccess

Public Class DataListGenerator

    Private m_Context As HttpContext
    Protected m_Argument As String
    Protected m_DisplayText As String
    Protected m_Id As String
    Protected m_ResourceId As Integer

    Public Sub New()

        m_Context = HttpContext.Current
        m_ResourceId = CInt(m_Context.Items("ResourceId"))

    End Sub
    Public Sub New(ByVal ResourceId As Integer)
        m_ResourceId = ResourceId
    End Sub

    Public Function SummaryListGenerator(ByVal userId As String, ByVal campusId As String) As DataSet
        Dim db As New FAME.DataAccessLayer.DataAccess
        Dim db2 As New FAME.DataAccessLayer.DataAccess
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim ds3 As New DataSet
        Dim sb As New System.Text.StringBuilder
        ' Dim strSQL As String
        Dim strTemplate As String
        Dim dr As DataRow '', dr2
        Dim DisplayField As String = "%DisplayField%"
        Dim ValueField As String = "%ValueField%"
        Dim TableName As String = "%TableName%"
        Dim dtSummList As DataTable
        'Dim strDispFldIDS As String
        'Dim strItem As String
        'Dim strArray() As String
        'Dim strFldIds As String

        'We first need to retrieve the information for the Summary List used
        'on the page.
        With sb
            .Append("SELECT t1.ResourceId,t2.SummListName,t3.TblName,t4.FldName As DispText,t5.FldName as DispValue ")
            .Append("FROM syResources t1,sySummLists t2,syTables t3,syFields t4,syFields t5 ")
            .Append("WHERE t1.ResourceId=? ")
            .Append("AND t1.SummListId=t2.SummListId ")
            .Append("AND t2.TblId=t3.TblId ")
            .Append("AND t2.DispFldId=t4.FldId ")
            .Append("AND t2.ValFldId=t5.FldId")
        End With

        db.AddParameter("@resid", m_ResourceId, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.ConnectionString = "ConStringMaster"
        ds = db.RunParamSQLDataSet(sb.ToString(), "SummList")
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        dtSummList = ds.Tables("SummList")
        dr = dtSummList.Rows(0)
        Try

            'We need to replace the token fields with the actual values
            db2.ConnectionString = "ConString"


            ' Added by Corey Masson July 2, 2004
            ' Added this functionality so that we are only getting Active or Inactive records depending on a optional parameter
            'Dim sStatusId As Guid
            'If RecordType <> "" Then
            '    'Generate the status id
            '    ds2 = StatusIdGenerator()
            '    'Set up the primary key on the datatable
            '    ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}
            '    Dim row As DataRow = ds2.Tables(0).Rows.Find(RecordType)
            '    sStatusId = row("StatusId")
            'End If
            'With strTemplate
            '    .Append("SELECT %ValueField%,%DisplayField%,StatusId ")
            '    .Append("FROM %TableName% ")
            '    If RecordType <> "" Then
            '        .Append("WHERE StatusId = ?")
            '        db.AddParameter("@StatusId", sStatusId, DataAccess.OleDbDataType.OleDbGuid, 16, ParameterDirection.Input)
            '    End If
            '    .Append("ORDER BY %DisplayField%")
            'End With
            ' End addition of logic Corey Masson July 2 2004


            If dr("TblName").ToString().ToLower = "arprograms" Then
                strTemplate = "SELECT %ValueField%,%DisplayField%,%TableName%.StatusId,(select Distinct Status from syStatuses where StatusId=%TableName%.StatusId) as Status, " &
                                "isnull((select ShiftDescrip from arShifts where shiftid=%TableName%.shiftid),'') as ShiftDescrip " &
                         "FROM %TableName% "
            Else
                strTemplate = "SELECT %ValueField%,%DisplayField%,%TableName%.StatusId,(select Distinct Status from syStatuses where StatusId=%TableName%.StatusId) as Status " &
                         "FROM %TableName% "
            End If

            Dim isUserSa As Boolean
            isUserSa = New UserSecurityDB().IsSAorAdvantageSuperUser(userId)

            If Not isUserSa Then
                If (dr("TblName").ToString.Trim.ToLower.Equals("sycampuses")) Then
                    strTemplate &= " WHERE  CampusId = '" & campusId & "' "
                Else
                    strTemplate &= "WHERE CampGrpId IN(SELECT CampGrpId FROM syCmpGrpCmps WHERE CampusId = '" & campusId & "'" & " AND CampGrpId <> (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL')) "
                    strTemplate &= "OR CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL') "
                End If

                If dr("TblName").ToString.Trim.ToLower = "adethcodes" Then
                    strTemplate &= " AND EthCodeDescrip not like 'NonResident%'"
                End If
            Else
                If dr("TblName").ToString.Trim.ToLower = "adethcodes" Then
                    strTemplate &= " WHERE EthCodeDescrip not like 'NonResident%'"
                End If
            End If

            If dr("TblName").ToString().ToLower = "arterm" Then
                strTemplate &= "ORDER BY " & dr("TblName").ToString() & ".StartDate,%DisplayField%"
            Else
                strTemplate &= "ORDER BY %DisplayField%"
            End If




            'Replace the TableName token
            strTemplate = strTemplate.Replace(TableName, dr("TblName").ToString())
            'Replace the DisplayField token. Remember that more than one value might
            'be passed in for the display field.

            strTemplate = strTemplate.Replace(DisplayField, dr("DispText").ToString())
            'Replace the ValueField token
            strTemplate = strTemplate.Replace(ValueField, dr("DispValue").ToString())

            m_Id = dr("DispValue") 'To be used as the commandargument
            m_DisplayText = dr("DispText") 'To be used as the text displayed

            'ds2 = db2.RunParamSQLDataSet(strTemplate)
            ds2 = db2.RunParamSQLDataSet(strTemplate.ToString)

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw New BaseException("Error generating summary list - " & ex.Message & ex.InnerException.Message)
        End Try

        'Next we need to display the info for the status ddl.

        'Return the dataset 
        Return ds2


    End Function
    Public Function SummaryListGeneratorSort() As DataSet
        Dim db As New FAME.DataAccessLayer.DataAccess
        Dim db2 As New FAME.DataAccessLayer.DataAccess
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim ds3 As New DataSet
        Dim sb As New System.Text.StringBuilder
        '  Dim strSQL As String
        Dim strTemplate As String
        Dim dr As DataRow '', dr2
        Dim DisplayField As String = "%DisplayField%"
        Dim SortField As String = "%SortField%"
        Dim ValueField As String = "%ValueField%"
        Dim TableName As String = "%TableName%"
        Dim dtSummList As DataTable
        'Dim strDispFldIDS As String
        'Dim strItem As String
        'Dim strArray() As String
        'Dim strFldIds As String

        'We first need to retrieve the information for the Summary List used
        'on the page.
        With sb
            .Append("SELECT t1.ResourceId,t2.SummListName,t3.TblName,t4.FldName As DispText,t5.FldName as DispValue, ")
            .Append(" (select FldName from syFields where fldname ='StatusId') as StatusId ")
            .Append("FROM syResources t1,sySummLists t2,syTables t3,syFields t4,syFields t5 ")
            .Append("WHERE t1.ResourceId=? ")
            .Append("AND t1.SummListId=t2.SummListId ")
            .Append("AND t2.TblId=t3.TblId ")
            .Append("AND t2.DispFldId=t4.FldId ")
            .Append("AND t2.ValFldId=t5.FldId ")
        End With

        db.AddParameter("@resid", m_ResourceId, FAME.DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
        db.ConnectionString = "ConStringMaster"
        ds = db.RunParamSQLDataSet(sb.ToString(), "SummList")
        db.ClearParameters()
        sb.Remove(0, sb.Length)

        dtSummList = ds.Tables("SummList")
        dr = dtSummList.Rows(0)
        Try
            'We need to replace the token fields with the actual values
            db2.ConnectionString = "ConString"

            If dr("TblName").ToString().ToLower = "arprograms" Then
                strTemplate = "SELECT %ValueField%,%DisplayField%,%TableName%.StatusId,(select Distinct Status from syStatuses where StatusId=%TableName%.StatusId) as Status, " &
                                "isnull((select ShiftDescrip from arShifts where shiftid=%TableName%.shiftid),'') as ShiftDescrip " &
                         "FROM %TableName% " &
                              "ORDER BY %SortField%,%DisplayField%"
            Else
                strTemplate = "SELECT %ValueField%,%DisplayField%,%TableName%.StatusId,(select Distinct Status from syStatuses where StatusId=%TableName%.StatusId) as Status " &
                              "FROM %TableName% " &
                              "ORDER BY %SortField%,%DisplayField%"
            End If

            'Replace the TableName token
            strTemplate = strTemplate.Replace(TableName, dr("TblName").ToString())
            'Replace the DisplayField token. Remember that more than one value might
            'be passed in for the display field.

            strTemplate = strTemplate.Replace(DisplayField, dr("DispText").ToString())
            strTemplate = strTemplate.Replace(SortField, dr("StatusId").ToString())

            'Replace the ValueField token
            strTemplate = strTemplate.Replace(ValueField, dr("DispValue").ToString())

            m_Id = dr("DispValue") 'To be used as the commandargument
            m_DisplayText = dr("DispText") 'To be used as the text displayed

            ds2 = db2.RunParamSQLDataSet(strTemplate.ToString)

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Throw New BaseException("Error generating summary list - " & ex.Message & ex.InnerException.Message)
        End Try

        'Next we need to display the info for the status ddl.

        'Return the dataset 
        Return ds2

    End Function
    Public Function StatusIdGenerator() As DataSet
        Dim db As New FAME.DataAccessLayer.DataAccess
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim ds3 As New DataSet
        Dim sb As New System.Text.StringBuilder
        'Dim strSQL As String
        'Dim strTemplate As String
        'Dim dr, dr2 As DataRow
        Dim DisplayField As String = "%DisplayField%"
        Dim ValueField As String = "%ValueField%"
        Dim TableName As String = "%TableName%"
        'Dim dtStatus As DataTable
        'Dim strDispFldIDS As String
        'Dim strItem As String
        'Dim strArray() As String
        'Dim strFldIds As String
        'Dim m_StatusId As String

        'We first need to retrieve the information for the Summary List used
        'on the page.
        With sb
            .Append("SELECT StatusId, Status ")
            .Append("FROM syStatuses")

        End With

        db.ConnectionString = "ConString"
        ds = db.RunParamSQLDataSet(sb.ToString(), "Status")
        sb.Remove(0, sb.Length)

        Return ds
    End Function
    Public Function StatusIdGeneratorSort() As DataSet
        Dim db As New FAME.DataAccessLayer.DataAccess
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim ds3 As New DataSet
        Dim sb As New System.Text.StringBuilder
        'Dim strSQL As String
        'Dim strTemplate As String
        'Dim dr, dr2 As DataRow
        Dim DisplayField As String = "%DisplayField%"
        Dim ValueField As String = "%ValueField%"
        Dim TableName As String = "%TableName%"
        'Dim dtStatus As DataTable
        'Dim strDispFldIDS As String
        'Dim strItem As String
        'Dim strArray() As String
        'Dim strFldIds As String
        'Dim m_StatusId As String

        'We first need to retrieve the information for the Summary List used
        'on the page.
        With sb
            .Append("SELECT StatusId, Status ")
            .Append("FROM syStatuses order by Status asc")

        End With

        db.ConnectionString = "ConString"
        ds = db.RunParamSQLDataSet(sb.ToString(), "Status")
        sb.Remove(0, sb.Length)

        Return ds
    End Function
End Class
