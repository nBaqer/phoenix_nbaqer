﻿Imports Microsoft.VisualBasic
Imports FAME.AdvantageV1.Common

Public Class FAMELinkFileComparer
    Implements IComparer


    Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
        Dim f1 As FLFileInfo = DirectCast(x, FLFileInfo)
        Dim f2 As FLFileInfo = DirectCast(y, FLFileInfo)

        Dim f1MsgType As String
        Dim f2MsgType As String

        f1MsgType = f1.strMsgType
        f2MsgType = f2.strMsgType

        'If DatePart(DateInterval.Month, f1.CreationDate) <> DatePart(DateInterval.Month, f2.CreationDate) And _
        '   DatePart(DateInterval.Day, f1.CreationDate) <> DatePart(DateInterval.Day, f2.CreationDate) And _
        '   DatePart(DateInterval.Year, f1.CreationDate) <> DatePart(DateInterval.Year, f2.CreationDate) Then
        '    Return f1.CreationDate < f2.CreationDate
        If DatePart(DateInterval.Year, f1.CreationDate) <> DatePart(DateInterval.Year, f2.CreationDate) Then
            Return f1.CreationDate < f2.CreationDate
        ElseIf DatePart(DateInterval.Month, f1.CreationDate) <> DatePart(DateInterval.Month, f2.CreationDate) Then
            Return f1.CreationDate < f2.CreationDate
        ElseIf DatePart(DateInterval.Day, f1.CreationDate) <> DatePart(DateInterval.Day, f2.CreationDate) Then
            Return f1.CreationDate < f2.CreationDate
        Else
            'If for some reason the dates are the same then we want to return the files in the following order
            'FAID, HEAD, DISB, CHNG, RCVD, REFU
            If f1.strMsgType = f2.strMsgType Then
                Return 0
            ElseIf f1.strMsgType = "FAID" Then
                Return -1
            ElseIf f1.strMsgType = "HEAD" And f2.strMsgType <> "FAID" Then
                Return -1
            ElseIf f1.strMsgType = "DISB" And f2.strMsgType <> "FAID" And f2.strMsgType <> "HEAD" Then
                Return -1
            ElseIf f1.strMsgType = "CHNG" And f2.strMsgType <> "FAID" And f2.strMsgType <> "HEAD" And f2.strMsgType <> "DISB" Then
                Return -1
            ElseIf f1.strMsgType = "RCVD" And f2.strMsgType <> "FAID" And f2.strMsgType <> "HEAD" And f2.strMsgType <> "DISB" And f2.strMsgType <> "CHNG" Then
                Return -1
            ElseIf f1.strMsgType = "REFU" And f2.strMsgType <> "FAID" And f2.strMsgType <> "HEAD" And f2.strMsgType <> "DISB" And f2.strMsgType <> "CHNG" And f2.strMsgType <> "RCVD" Then
                Return -1
            Else : Return 1

            End If
        End If

    End Function
End Class
