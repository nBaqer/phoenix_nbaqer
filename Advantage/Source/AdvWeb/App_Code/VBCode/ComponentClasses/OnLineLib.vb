Imports Microsoft.VisualBasic
Imports FAME.AdvantageV1.Common
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Xml
Imports System.Collections.Generic
Imports System.Text
Imports System

Public Class OnLineLib
    Private Shared _listOfOnLineCourses() As String
    Private Shared _onLineCourseAttendanceCollection As List(Of OnLineCourseAttendance)
    Private Shared _onLineCourseAttendanceTerm As Integer
    Private Shared _termList() As String
    <CLSCompliant(False)> Public Shared Function create_user(ByVal onLineInfo As OnLineInfo, ByVal user As String) As String
        Dim facade As New OnLineFacade()

        'insert the record in Advantage DB
        Dim result As String = facade.UpdateOnLineInfo(onLineInfo, user)
        If Not (result = String.Empty) Then Return result
        onLineInfo.IsInDB = True

        'get response from OnLine Server
        Dim xmlStr As String = CommonWebUtilities.getRequest(onLineInfo.Url + "create_user" + onLineInfo.create_user())

        'convert it to xml
        Dim xmlDoc As New XmlDocument()
        xmlDoc.LoadXml(xmlStr)

        'check for errors
        Select Case xmlDoc.SelectSingleNode("xml/status/result").InnerText
            Case "success"
                onLineInfo.IsInDB = True
                onLineInfo.OnLineStudentId = Integer.Parse(xmlDoc.SelectSingleNode("xml/users/user/id").InnerText)
                Return facade.UpdateOnLineInfo(onLineInfo, user)
            Case "error"
                facade.DeleteOnLineInfo(onLineInfo.StuEnrollId)
                Return xmlDoc.SelectSingleNode("xml/status/message").InnerText
        End Select
        Return String.Empty
    End Function
    <CLSCompliant(False)> Public Shared Function get_users(ByVal onLineInfo As OnLineInfo) As String
        Return CommonWebUtilities.getRequest(onLineInfo.Url + "get_users" + onLineInfo.get_users(True, True))
    End Function
    Public Shared Function get_terms() As String
        Return CommonWebUtilities.getRequest(GetOnLineUrl() + "get_terms")
    End Function
    <CLSCompliant(False)> Public Shared Function get_courses(ByVal onLineInfo As OnLineInfo, ByVal student_courses As Boolean) As String
        Return CommonWebUtilities.getRequest(onLineInfo.Url + "get_courses" + onLineInfo.get_courses(student_courses))
    End Function
    Public Shared Function get_courses(ByVal student_courses As Boolean) As String
        Return CommonWebUtilities.getRequest(GetOnLineUrl() + "get_courses" + New OnLineInfo().get_courses(student_courses))
    End Function
    <CLSCompliant(False)> Public Shared Function get_courses(ByVal onLineInfo As OnLineInfo, ByVal courseId As Integer, ByVal student_courses As Boolean) As String
        Return CommonWebUtilities.getRequest(onLineInfo.Url + "get_courses" + onLineInfo.get_courses(courseId, student_courses))
    End Function
    Public Shared Function get_student_attendance_report(ByVal termId As Integer) As String
        Return CommonWebUtilities.getRequest(GetOnLineUrl() + "get_student_attendance_report" + "&termid=" + termId.ToString())
    End Function
    Public Shared Function get_attendance(ByVal termId As Integer) As String
        Return CommonWebUtilities.getRequest(GetOnLineUrl() + "get_attendance" + "&termid=" + termId.ToString())
    End Function
    <CLSCompliant(False)> Public Shared Function update_user(ByVal onLineInfo As OnLineInfo) As String
        Return CommonWebUtilities.getRequest(onLineInfo.Url + "update_user" + onLineInfo.update_user())
    End Function
    <CLSCompliant(False)> Public Shared Function set_user_enrollment(ByVal onLineInfo As OnLineInfo, ByVal return_course_infos As Boolean) As String
        InitializeOnLineConnection()
        Return CommonWebUtilities.getRequest(onLineInfo.Url + "set_user_enrollment" + ConvertUrlWithShortNamesToUrlWithIntegers(onLineInfo.set_user_enrollment(return_course_infos)))
    End Function
    Public Shared Function GetListOfOnLineCourses() As String()
        Dim xmlDoc As New XmlDocument()
        Dim str As String = OnLineLib.get_courses(False)
        If Not str.Substring(0, 88) = "The request URI could not be found or was malformed The remote server returned an error:" Then
            xmlDoc.LoadXml(str)
        Else
            Return (New List(Of String)).ToArray
        End If
        Dim xmlNodes As XmlNodeList = xmlDoc.SelectNodes("/xml/courses/course")
        Dim courseList As New List(Of String)
        For Each node As XmlNode In xmlNodes
            courseList.Add(node.SelectSingleNode("id").InnerText + ";" + node.SelectSingleNode("shortname").InnerText + ";" + node.SelectSingleNode("fullname").InnerText)
        Next
        Return courseList.ToArray()
    End Function
    Public Shared Function GetListOfOnLineTerms() As String()
        If _termList Is Nothing Then
            _termList = GetTermsList()
        Else
            If _termList.Length = 0 Then
                _termList = GetTermsList()
            End If
        End If
        Return _termList
    End Function
    Private Shared Function GetTermsList() As String()
        Dim str As String = OnLineLib.get_terms()
        Dim xmlDoc As New XmlDocument()
        If Not str.Substring(0, 88) = "The request URI could not be found or was malformed The remote server returned an error:" Then
            xmlDoc.LoadXml(str)
        End If
        Dim xmlNodes As XmlNodeList = xmlDoc.SelectNodes("/xml/terms/term")
        Dim termList As New List(Of String)
        For Each node As XmlNode In xmlNodes
            termList.Add(node.SelectSingleNode("id").InnerText + ";" + node.SelectSingleNode("name").InnerText + ";" + node.SelectSingleNode("startdate").InnerText)
        Next
        Return termList.ToArray()
    End Function
    Private Shared Function GetOnLineUrl() As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        GetOnLineUrl = Replace(MyAdvAppSettings.AppSettings("OnLineUrl"), ";", "&")
    End Function
    Private Shared Function IsRegisteredInOnLineDB(ByVal stuEnrollId As String) As String
        Return (New OnLineFacade).GetOnLineInfoByStuEnrollId(stuEnrollId).IsRegisteredInOnLineDB
    End Function
    Public Shared Function IsEnrolledInAnyOnLineCourse(ByVal stuEnrollId As String) As Boolean
        Return (New OnLineFacade).IsEnrolledInAnyOnLineCourse(stuEnrollId)
    End Function
    Public Shared Function IsOnLineStudent(ByVal studentId As String) As Boolean
        Return (New OnLineFacade).IsOnLineStudent(studentId)
    End Function
    <CLSCompliant(False)> Public Shared Function UpdateStudentOnLineEnrollment(ByVal onLineInfo As OnLineInfo, ByVal user As String) As String
        If Not onLineInfo.IsRegisteredInOnLineDB Then
            Dim result As String = create_user(onLineInfo, user)
            If Not (result = String.Empty) Then Return result
        End If
        onLineInfo.OnLineCourses = (New OnLineFacade).GetAllOnLineCoursesByStuEnrollId(onLineInfo.StuEnrollId)
        Return set_user_enrollment(onLineInfo, False)
    End Function
    Public Shared Function IsOnLineClassSection(ByVal clsSectionId As String) As Boolean
        Return (New OnLineFacade).IsOnLineClassSection(clsSectionId)
    End Function
    <CLSCompliant(False)> Public Shared Function SyncOnLineAttendance(ByVal onLineTermAttendance As OnLineTermAttendance, ByVal userId As String) As String
        Return (New OnLineFacade).SyncOnLineAttendance(onLineTermAttendance, userId)
    End Function
    Public Shared Function GetTermGrades(ByVal termId As Integer) As String()
        Dim lines() As String = OnLineLib.get_student_attendance_report(termId).Split(New [Char]() {ChrW(10)})
        Dim results As New List(Of String)
        'extract information from each row
        For i As Integer = 1 To lines.Length - 1
            Dim line As String = lines(i)
            Dim fields() As String = line.Split(",")
            'fields---   --header- -sample-
            'fields(0) = "Course" "MBC100.05.06"
            'fields(1) = "Username" "sandram"
            'fields(2) = "User ID" "220"
            'fields(3) = "First name" "Sandra"
            'fields(4) = "Last name" "Mattison"
            'fields(5) = "Grade" "[969/1000]"
            'fields(6) = "Status" "Active"
            'fields(7) = "Discussion posts" "5"
            'fields(8) = "Last access" "Never"
            'fields(9) = "First access date" "Never"
            'fields(10) = "Last access date" "Never"
            Dim sb As New StringBuilder
            With sb
                For Each field As String In fields
                    .Append(field.Trim(""""))
                    .Append(";")
                Next
            End With
            'results.Add(field(0).Trim("""") + ";" + field(2).Trim("""") + ";" + field(5).Trim("""") + ";" + field(6).Trim(""""))
            results.Add(sb.ToString)
        Next

        Return results.ToArray()
    End Function
    Public Shared Function ConvertOnLineCourseShortNameCodeToIntegerCode(ByVal shortNameCode As String) As Integer
        If _listOfOnLineCourses Is Nothing Then
            _listOfOnLineCourses = GetListOfOnLineCourses()
        End If
        For i As Integer = 0 To _listOfOnLineCourses.Length - 1
            Dim onLineCourse() As String = _listOfOnLineCourses(i).Split(";")
            If shortNameCode = onLineCourse(1) Then Return Integer.Parse(onLineCourse(0))
        Next
        Return 0
    End Function
    Private Shared Function ConvertUrlWithShortNamesToUrlWithIntegers(ByVal urlString As String) As String
        Dim startIdx As Integer = urlString.IndexOf("&courseids=") + 11
        Dim endIdx As Integer = urlString.IndexOf("&return_course_infos=")
        Dim shortNames() As String = urlString.Substring(startIdx, endIdx - startIdx).Split(";")
        Dim sb As New StringBuilder()
        For i As Integer = 0 To shortNames.Length - 1
            If i > 0 Then sb.Append("::")
            sb.Append(ConvertOnLineCourseShortNameCodeToIntegerCode(shortNames(i)))
        Next
        Return urlString.Substring(0, startIdx) + sb.ToString + urlString.Substring(endIdx)
    End Function
    Public Shared Sub InitializeOnLineConnection()
        _listOfOnLineCourses = Nothing
    End Sub
    <CLSCompliant(False)> Public Shared Function GetOnLineAttendanceByTerm(ByVal termId As Integer) As List(Of OnLineCourseAttendance)
        Dim str As String = OnLineLib.get_attendance(termId)
        str = str.Replace("<:", "<D")
        str = str.Replace("</:", "</D")
        str = str.Replace(":>", ">")
        Dim xmlDoc As New XmlDocument()
        xmlDoc.LoadXml(str)
        Dim nodeList As XmlNodeList = xmlDoc.SelectNodes("xml/get_attendance")
        Dim index() As Integer = BuildNodeIndex(nodeList)
        Dim previousCourseShortName As String = String.Empty
        Dim previousStudentId As Integer = 0
        Dim onLineCourseAttendanceCollection As New List(Of OnLineCourseAttendance)
        Dim onLineCourseAttendance As OnLineCourseAttendance = Nothing
        Dim onLineStudentAttendance As OnLineStudentAttendance = Nothing

        For i As Integer = 0 To nodeList.Count - 1
            Dim node As XmlNode = nodeList(index(i))
            Dim studentId As Integer = Integer.Parse(node.SelectSingleNode("id").InnerText)
            Dim lastName As String = node.SelectSingleNode("lastname").InnerText
            Dim firstName As String = node.SelectSingleNode("firstname").InnerText
            Dim courseShortName As String = node.SelectSingleNode("courseshortname").InnerText
            Dim datesAttendedCollection As XmlNodeList = node.SelectNodes("attendance/child::*")
            If Not (courseShortName = previousCourseShortName) Then
                onLineCourseAttendance = New OnLineCourseAttendance(courseShortName, termId)
                onLineCourseAttendance.OnLineStudentAttendanceCollection = New List(Of OnLineStudentAttendance)
                onLineCourseAttendanceCollection.Add(onLineCourseAttendance)
                previousCourseShortName = courseShortName
                previousStudentId = 0
            End If
            If Not (studentId = previousStudentId) Then
                onLineStudentAttendance = New OnLineStudentAttendance(studentId, lastName, firstName, onLineCourseAttendance)
                onLineStudentAttendance.AttendedDatesCollection = New List(Of OnLineAttendedDate)
                onLineCourseAttendance.OnLineStudentAttendanceCollection.Add(onLineStudentAttendance)
                previousStudentId = studentId
            End If
            For Each attendedDate As XmlNode In datesAttendedCollection
                Dim actualDate As Date = Date.Parse(attendedDate.LocalName.Substring(1, 8))
                Dim minutesAttended As Integer = Integer.Parse(attendedDate.InnerText)
                onLineStudentAttendance.AttendedDatesCollection.Add(New OnLineAttendedDate(actualDate, minutesAttended))
            Next
        Next
        Return onLineCourseAttendanceCollection
    End Function
    Private Shared Function BuildNodeIndex(ByVal nodeList As XmlNodeList) As Integer()
        'build index
        Dim keys(nodeList.Count - 1) As String
        Dim index(nodeList.Count - 1) As Integer
        For i As Integer = 0 To nodeList.Count - 1
            Dim node As XmlNode = nodeList(i)
            keys(i) = node.SelectSingleNode("courseshortname").InnerText + ";" + Integer.Parse(node.SelectSingleNode("id").InnerText).ToString("00000")
            index(i) = i
        Next
        Array.Sort(keys, index)
        Return index
    End Function
    <CLSCompliant(False)> Public Shared Function GetOnLineCourseAttendance(ByVal courseShortName As String, ByVal termId As Integer) As OnLineCourseAttendance
        If _onLineCourseAttendanceCollection Is Nothing Or (_onLineCourseAttendanceTerm <> termId) Then
            _onLineCourseAttendanceCollection = GetOnLineAttendanceByTerm(termId)
            _onLineCourseAttendanceTerm = termId
            Dim onLineTermAttendance As New OnLineTermAttendance(_onLineCourseAttendanceTerm, GetOnLineTermStartDate(_onLineCourseAttendanceTerm), _onLineCourseAttendanceCollection)
            Dim str As String = (New OnLineFacade).SyncOnLineAttendance(onLineTermAttendance, "Anatoly")
        End If
        For Each course As OnLineCourseAttendance In _onLineCourseAttendanceCollection
            If course.CourseShortName = courseShortName Then
                If course.OnLineStudentAttendanceCollection.Count > 0 Then Return course
            End If
        Next
        Return Nothing
    End Function
    Public Shared Function GetOnLineTermStartDate(ByVal termId As Integer) As Date
        Dim terms() As String = GetListOfOnLineTerms()
        For i As Integer = 0 To terms.Length - 1
            Dim termItem() As String = terms(i).Split(";")
            If Integer.Parse(termItem(0)) = termId Then
                Return CommonWebUtilities.ConvertUnixTimeStampToDate(Double.Parse(termItem(2)))
            End If
        Next
        Return Nothing
    End Function
    <CLSCompliant(False)> Public Shared Function GetAttendanceStatistics(ByVal onLineStudentAttendance As OnLineStudentAttendance) As String
        Dim statistics() As Integer = {0, 0, 0}
        Dim statString As String = "0;0;0"
        If onLineStudentAttendance.AttendedDatesCollection Is Nothing Then Return statString
        If onLineStudentAttendance.AttendedDatesCollection.Count < 1 Then Return statString
        Dim termStartDate As Date = GetOnLineTermStartDate(onLineStudentAttendance.Parent.OnLineTermId)
        Dim ts As TimeSpan = Date.Today.Subtract(termStartDate)

        'total number of days elapsed
        If ts.Days > 0 Then
            statistics(0) = ts.Days
        Else
            statistics(0) = 0
        End If

        'number of days attended
        statistics(1) = onLineStudentAttendance.AttendedDatesCollection.Count

        'number of minutes attended
        statistics(2) = 0
        For Each attendedDate As OnLineAttendedDate In onLineStudentAttendance.AttendedDatesCollection
            statistics(2) += attendedDate.Minutes
        Next

        'return statistics
        Return statistics(0).ToString() + ";" + statistics(1).ToString() + ";" + statistics(2).ToString()

    End Function
End Class

