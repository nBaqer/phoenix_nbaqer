Imports System.Data
Imports System.Data.OleDb
Imports FAME.DataAccessLayer
Imports FAME.ExceptionLayer
Imports System.Drawing
Imports System.Web.UI
Imports Microsoft.VisualBasic
Imports System.Resources
Imports System.Text.RegularExpressions
Imports System.Text
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports System.Web.UI.HtmlControls
Imports System.Collections
Imports System.Web.UI.WebControls
Imports System.Web
Imports System

Namespace FAME.Common
    Public Class CommonUtilities

        Public strText As String

#Region "Private variables and objects"
        Private m_Context As System.Web.HttpContext
        Private m_PK As String
#End Region
#Region "Public Properties and objects"
        Public Property PagePK() As String
            Get
                Return m_PK
            End Get

            Set(ByVal Value As String)
                m_PK = Value
            End Set
        End Property
#End Region


#Region "Public Constructors"
        Public Sub New()
            MyBase.New()

            m_Context = System.Web.HttpContext.Current

        End Sub
#End Region

#Region "Private Methods"
        Public Sub PageSetupPopups(ByVal Form As HtmlForm, ByVal Mode As String)
            '**************************************************************************************************
            'Purpose:       When a page is loaded this sub is called to color the required fields and to
            '               dynamically add required field validators for fields that are required.
            '               This sub will also add validators to check the length of data entered into
            '               textboxes and also ensure the correct data type is entered as well. It will
            '               add the necessary RegularExpressionValidators for textboxes if they have input
            '               masks associated with them.
            'Parameters:
            '[Form]         The form on the page that contains the asp.net server controls
            '[Mode]         New or PostBack mode. New means that the page is loaded for the first time.
            '               PostBack mode is called when a post back occurs.
            'Returns:       N/A
            'Created:       Troy Richards, 7/3/2003
            'Notes:         This sub relies on the ResourceId placed in  the httpcontext.items collection when
            '               when a request for the page is made. See the Application_BeginRequest sub in the
            '               Global.aspx.vb page.
            '**************************************************************************************************

            '03/2012 Janet Robinson no more coloring of required fields for the streamline project


            Dim db As New DataAccess
            Dim db2 As New DataAccess
            Dim ds As New DataSet
            Dim dt1, dt2, dt3, dt4, dt5 As DataTable
            Dim dr As DataRow '', dr2
            Dim da As OleDbDataAdapter
            Dim sb As New System.Text.StringBuilder
            Dim ctl, ctl2 As Control
            Dim strControl As String
            Dim strPrefix As String
            Dim strFldLen As String
            Dim drMask As DataRow
            Dim strMask As String
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim intDDLCounter As Integer
            Dim ResourceId As Integer
            Dim intCounter As Integer = 0
            Dim sLangName As String

            ResourceId = m_Context.Items("ResourceId")
            sLangName = m_Context.Items("Language")
            sLangName = sLangName.ToUpper

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                'Get the entries from the syResTblFlds table. This is in the
                'AdvantageMaster database
                With sb
                    .Append("SELECT t1.ResDefId,t2.TblFldsId,t2.FldId,t5.TblName,t3.FldName,t1.Required,t3.DDLId,t3.FldLen,t4.FldType,t5.TblPK,t6.Caption,t1.ControlName,t1.UsePageSetup,t1.SchlReq ")
                    .Append("FROM syResTblFlds t1,syTblFlds t2,syFields t3,syFieldTypes t4,syTables t5,syFldCaptions t6,syLangs t7 ")
                    .Append("WHERE t1.TblFldsId=t2.TblFldsId ")
                    .Append("AND t2.FldId=t3.FldId ")
                    .Append("AND t2.TblId=t5.TblId ")
                    .Append("AND t3.FldTypeId=t4.FldTypeId ")
                    .Append("AND t2.FldId=t6.FldId ")
                    .Append("AND t6.LangId=t7.LangId ")
                    .Append("AND t1.ResourceId=? ")
                    .Append("AND t7.LangName=?")
                End With

                db.AddParameter("@resid", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@langid", sLangName, DataAccess.OleDbDataType.OleDbString, 5, ParameterDirection.Input)
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConStringMaster"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                Try
                    db.OpenConnection()
                    da = db.RunParamSQLDataAdapter(sb.ToString())
                    da.Fill(ds, "ResFlds")
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db.CloseConnection()
                    db.Dispose()
                    db = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syResTblFlds table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syResTblFlds table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries for the DDLs that are on the current page
                With sb
                    .Append("SELECT t1.ResDefId,t1.UsePageSetup,t2.TblFldsId,t3.FldName,t4.DDLId,t5.TblName,t6.FldName as DispText,t7.FldName as DispValue,t4.ResourceId as DDLResourceId,t1.ControlName ")
                    .Append("FROM syResTblFlds t1,syTblFlds t2,syFields t3,syDDLS t4,syTables t5,")
                    .Append("syFields t6,syFields t7 ")
                    .Append("WHERE t1.TblFldsId = t2.TblFldsId ")
                    .Append("AND t2.FldId=t3.FldId ")
                    .Append("AND t3.DDLId=t4.DDLId ")
                    .Append("AND t4.TblId=t5.TblId ")
                    .Append("AND t4.DispFldId=t6.FldId ")
                    .Append("AND t4.ValFldId=t7.FldId ")
                    ' Added by Corey Masson on May 27 2004 to exclude all records that are not using pagesetup
                    '.Append("AND t1.UsePageSetup = 1 ")
                    .Append("AND t1.ResourceId=?")
                End With
                db.AddParameter("@resid", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                Try
                    da = db.RunParamSQLDataAdapter(sb.ToString())
                    da.Fill(ds, "DDLDefS")
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db.CloseConnection()
                    db.Dispose()
                    db = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving ddls that are on the current page:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving ddls that are on the current page:" & ex.InnerException.Message)
                    End If
                End Try
                'Cleanup
                db.CloseConnection()
                db.Dispose()
                db = Nothing


                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db2.ConnectionString = "ConString"
                db2.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db2.OpenConnection()

                'Get the entries for the data dictionary school required fields.
                With sb
                    .Append("SELECT FldId ")
                    .Append("FROM syInstFldsDDReq ")
                End With

                Try
                    da = db2.RunParamSQLDataAdapter(sb.ToString)
                    da.Fill(ds, "InstDDReq")
                    db2.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.CloseConnection()
                    db2.Dispose()
                    db2 = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syInstFldsDDReq table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syInstFldsDDReq table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries from the syInstResFldsReq table.
                With sb
                    .Append("SELECT t1.TblFldsId,t2.FldId ")
                    .Append("FROM syInstResFldsReq t1, syTblFlds t2 ")
                    .Append("WHERE t1.TblFldsId=t2.TblFldsId ")
                    .Append("AND ResourceId = ? ")
                End With

                db2.AddParameter("@resid", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Try
                    da = db2.RunParamSQLDataAdapter(sb.ToString)
                    da.Fill(ds, "InstResFldsReq")
                    db2.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.CloseConnection()
                    db2.Dispose()
                    db2 = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syInstResFldsReq table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syInstResFldsReq table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries from the syInstFldsMask table. This is also in the 
                'regular Advantage database.
                With sb
                    .Append("SELECT FldId,Mask ")
                    .Append("FROM syInstFldsMask")
                End With

                Try
                    da = db2.RunParamSQLDataAdapter(sb.ToString)
                    da.Fill(ds, "FldsMask")
                    db2.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.CloseConnection()
                    db2.Dispose()
                    db2 = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syInstFldsMask table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syInstFldsMask table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries from the syRptFields table. This table stores the fields that are needed
                'to support the different reporting agencies such as IPEDS. This code is specific for
                'IPEDS reporting. We only want this code to be executed if the Web.Config file indicates
                'that the school is using IPEDS reporting functionality.
                If MyAdvAppSettings.AppSettings("IPEDS").ToLower = "yes" Then
                    With sb
                        .Append("SELECT distinct t2.FldId ")
                        .Append("FROM syRptAgencyFields t1, syRptFields t2 ")
                        .Append("WHERE t1.RptFldId=t2.RptFldId ")
                        .Append("AND t1.RptAgencyId=1 ")
                        .Append("AND t2.FldId IS NOT NULL ")
                    End With
                    Try
                        da = db2.RunParamSQLDataAdapter(sb.ToString)
                        da.Fill(ds, "IPEDSFields")
                        db2.ClearParameters()
                        sb.Remove(0, sb.Length)
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        db2.CloseConnection()
                        db2.Dispose()
                        db2 = Nothing
                        If ex.InnerException Is Nothing Then
                            Throw New BaseException("Error retrieving entries for IPEDS fields:" & ex.Message)
                        Else
                            Throw New BaseException("Error retrieving entries for IPEDS fields:" & ex.InnerException.Message)
                        End If
                    End Try

                End If

                'Cleanup
                db2.CloseConnection()
                db2.Dispose()
                db2 = Nothing


                'Create primary key
                dt1 = ds.Tables("ResFlds")
                With ds.Tables("ResFlds")
                    .PrimaryKey = New DataColumn() {dt1.Columns("TblFldsId")}
                End With

                dt2 = ds.Tables("InstDDReq")
                With ds.Tables("InstDDReq")
                    .PrimaryKey = New DataColumn() {dt2.Columns("FldId")}
                End With

                dt3 = ds.Tables("InstResFldsReq")
                With ds.Tables("InstResFldsReq")
                    .PrimaryKey = New DataColumn() {dt3.Columns("TblFldsId")}
                End With

                dt4 = ds.Tables("FldsMask")
                With ds.Tables("FldsMask")
                    .PrimaryKey = New DataColumn() {dt4.Columns("FldId")}
                End With

                With ds.Tables("DDLDefs")
                    .PrimaryKey = New DataColumn() {.Columns("DDLId")}
                End With

                dt5 = New DataTable
                If Not ds.Tables("IPEDSFields") Is Nothing Then
                    If ds.Tables("IPEDSFields").Rows.Count > 0 Then
                        dt5 = ds.Tables("IPEDSFields")
                        With ds.Tables("IPEDSFields")
                            .PrimaryKey = New DataColumn() {dt5.Columns("FldId")}
                        End With

                    End If
                End If

                'Add the dt1 DataTable to the httpcontext items collection so it can be 
                'reused by the PopulatePage sub if needed.
                m_Context.Items.Add("ResFlds", dt1)
                m_Context.Items.Add("FldsMask", dt4)
                m_Context.Items.Add("DDLDefs", ds.Tables("DDLDefs"))
                m_Context.Items.Add("IPEDSFields", dt5)

                'Create a ResourceManager to deal with the translated content.
                'ty = GetTypeFromPageName()
                'Dim gStrings As New ResourceManager("AdvantageApp.strings", ty.Assembly)
                'sLang = m_Context.Request.UserLanguages(0)

                'We need to loop through the records in the ResFlds datatable. If a field
                'is a required field we need to color the associated control and add a
                'RequiredFieldValidator.


                'If ResourceId <> 317 Then






                For Each dr In dt1.Rows
                    'If dr("UsePageSetup") = True Then
                    intCounter += 1
                    'If the ControlName field is not null then we have to use the control specified
                    'by that field.
                    Dim ctrlname As String = dr("FldName").ToString
                    Dim fldid As String = dr("FldId").ToString
                    If Not dr.IsNull("ControlName") Then
                        strControl = dr("ControlName").ToString()
                        strPrefix = strControl.Substring(0, 3)
                    Else
                        'If the DDLId associated with the record is null then we are
                        'dealing with a textbox or a checkbox. If not we are dealing with a dropdownlist.
                        'A checkbox normally is associated with a bit field in the database.
                        If dr.IsNull("DDLId") Then
                            If dr("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else 'Textbox
                                strPrefix = "txt"
                            End If
                        Else
                            'This will usually be a ddl. However, if we are on the page that sets up the
                            'ddl itself then it will be a textbox. If the ResouceId associated with the
                            'DDLId is the same as the ResourceId of the current page then it is a textbox.
                            'For example, on the page that sets up the states the StateId will be a textbox
                            'rather than a ddl. On other pages it will be a ddl.

                            drDDLDefs = ds.Tables("DDLDefs").Rows.Find(dr("DDLId"))
                            intResourceId = drDDLDefs("DDLResourceId")
                            If intResourceId = ResourceId Then
                                'We are on the page that sets up the ddl
                                strPrefix = "txt"
                            Else
                                strPrefix = "ddl"
                                'Increment the counter for tracking the number of ddls
                                'on the page.
                                intDDLCounter += 1
                            End If

                        End If
                        strControl = strPrefix & dr("FldName").ToString()
                    End If

                    ctl = Form.FindControl(strControl)
                    If ctl Is Nothing Then
                        strText = "Could not find control:" & strControl
                        'added logic to throw an exception when we do not find a control when we are supposed to
                        'Corey Masson Dec. 5, 2003
                        Throw New System.Exception("Could not find the control: " & strControl)
                    End If
                    ctl2 = Form.FindControl("pnlRequiredFieldValidators")
                    If ctl2 Is Nothing Then
                        Throw New System.Exception("Could not find panel named pnlRequiredFieldValidators")
                    End If
                    strFldLen = dr("FldLen").ToString()

                    'If mode is NEW then we need to set the captions for the label
                    'controls on the page.
                    If Mode = "NEW" Then
                        'If we are dealing with a textbox or a ddl then we need
                        'to check if there is an associated label control.
                        If strPrefix = "txt" Or strPrefix = "ddl" Then
                            Dim ctl3 As Control = Form.FindControl("lbl" & dr("FldName").ToString())
                            'The control might not have a label control associated with it.
                            If Not ctl3 Is Nothing Then
                                Try
                                    CType(ctl3, Label).Text = dr("Caption")
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    Throw New BaseException("Problem casting " & strControl & " to label " & "with caption " & dr("Caption").ToString)
                                End Try
                            End If

                        ElseIf strPrefix = "chk" Then 'Checkbox
                            'We have to find the checkbox itself
                            Dim ctl3 As Control = Form.FindControl("chk" & dr("FldName").ToString())
                            'If the checkbox was not found then we should notify the client
                            If ctl3 Is Nothing Then
                                Throw New System.Exception("Could not find checkbox:" & strControl)
                            End If
                            'The checkbox might be found but we might have a problem
                            'in the casting operation.
                            If Not ctl3 Is Nothing Then
                                Try
                                    CType(ctl3, CheckBox).Text = dr("Caption")
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    Throw New BaseException("Problem casting " & strControl & " to checkbox " & "with caption " & dr("Caption").ToString)
                                End Try
                            End If


                        End If

                    End If  'Mode is NEW and we should populate label controls

                    'Color the required fields.
                    Dim req As Boolean = dr("Required")
                    If (dr("Required") = True Or ValExistsInDt(dt2, dr("FldId")) Or TblFldsIdExistsInDT(dt3, dr("TblFldsId")) Or ValExistsInDt(dt5, dr("FldId"))) Then
                        Try
                            'If strPrefix = "txt" Then
                            '    If TypeOf (ctl) Is TextBox Then
                            '        CType(ctl, TextBox).BackColor = Color.FromName("#ffff99")
                            '        ElseIf TypeOf (ctl) Is Telerik.Web.UI.RadTextBox Then
                            '            CType(ctl, Telerik.Web.UI.RadTextBox).BackColor = Color.FromName("#ffff99")
                            '        ElseIf TypeOf (ctl) Is Telerik.Web.UI.RadDatePicker Then
                            '            CType(ctl, Telerik.Web.UI.RadDatePicker).DateInput.BackColor = Color.FromName("#ffff99")
                            '        End If
                            'ElseIf strPrefix = "ddl" Then
                            '    Try
                            '        If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                            '            CType(ctl, Telerik.Web.UI.RadComboBox).BackColor = Color.FromName("#ffff99")
                            '        Else
                            '            CType(ctl, DropDownList).BackColor = Color.FromName("#ffff99")
                            '        End If

                            '    Catch ex As System.Exception
                             '    	Dim exTracker = new AdvApplicationInsightsInitializer()
                            '    	exTracker.TrackExceptionWrapper(ex)

                            '        CType(ctl, HtmlSelect).Style("background-color") = "Yellow"
                            '    End Try
                            'End If
                            'T.Timochina NOTE: The Control Build by FLDNAME
                            Dim ctl3 As Control = Form.FindControl("lbl" & dr("FldName").ToString())
                            'The control might not have a label control associated with it.
                            If Not ctl3 Is Nothing Then
                                CType(ctl3, Label).Text = dr("Caption") & "<font color=""red"">*</font>"
                            End If
                        Catch ex As System.Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            Throw New System.Exception
                        End Try
                        'Add a RequiredFieldVaidator to monitor the control.
                        If strPrefix = "txt" Then
                            Dim rfv As New RequiredFieldValidator
                            With rfv
                                .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = dr("Caption").ToString & " is required"
                            End With
                            ctl2.Controls.Add(rfv)
                        End If
                        'Add a RequiredFieldVaidator to monitor the control.
                        If strPrefix = "ddl" Then
                            Dim rfv As New RequiredFieldValidator
                            With rfv
                                .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = dr("Caption").ToString & " is required"
                            End With
                            ctl2.Controls.Add(rfv)

                            'add a compare validator
                            Dim cv As New CompareValidator
                            With cv
                                .ID = "cv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = dr("Caption").ToString & " is required"
                                .ValueToCompare = Guid.Empty.ToString
                                .Operator = ValidationCompareOperator.NotEqual
                                .EnableClientScript = True
                            End With
                            ctl2.Controls.Add(cv)
                        End If

                    End If 'Control is required

                    'If the control is a textbox we need to also set the MaxLength property
                    'and also add a CompareValidator to perform a data type check. If it has
                    'an input mask associated with it then we should also add a 
                    'RegularExpressionValidator.
                    If strPrefix = "txt" Then
                        If dr("FldType") = "Varchar" Or dr("FldType") = "Char" Then
                            CType(ctl, TextBox).MaxLength = CInt(dr("FldLen"))
                        ElseIf dr("FldType") = "Smallint" Then 'Changed as per discussion w/ Troy to fix issue: 2483
                            CType(ctl, TextBox).MaxLength = 4
                        ElseIf dr("FldType") = "TinyInt" Then 'Changed as per discussion w/ Troy to fix issue: 2483
                            CType(ctl, TextBox).MaxLength = 3
                        End If
                        'Else '**Changed as per discussion w/ Troy to fix issue: 2483**'
                        '    CType(ctl, TextBox).MaxLength = 50

                        Dim cmv As New CompareValidator
                        With cmv
                            .ID = "cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                            .ControlToValidate = strControl
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = "Incorrect data type for " & dr("Caption").ToString()
                            .[Operator] = ValidationCompareOperator.DataTypeCheck
                            .Type = GetValidatorType(dr("FldType"))
                        End With
                        ctl2.Controls.Add(cmv)


                        drMask = dt4.Rows.Find(dr("FldId"))
                        If Not IsNothing(drMask) Then
                            strMask = drMask("Mask")
                            Dim rev As New RegularExpressionValidator
                            With rev
                                .ID = "rev" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = "Incorrect format for " & strControl
                                .ValidationExpression = GetRegEx(strMask)
                            End With
                            ctl2.Controls.Add(rev)
                        End If
                    End If 'Set MaxLength for TextBox control

                    'If the page has a summary list on the lhs then we need to
                    'call SummListGenerator to populate that control.
                    'End If
                Next

                'End If

                'If the Mode is New we should populate the dropdownlists on the page.
                'On a post back there is no need to populate the ddls on the page as that
                'is handled automatically by ASP.NET. If the page does not contain any
                'ddls then we do not want to call the ListGenerator sub.
                If Mode = "NEW" And intDDLCounter > 0 Then
                    ListGeneratorPopUp(Form)
                End If
                'If the mode is new and the culture is not en-us then we should
                'translate the button captions
                'If Mode = "NEW" And sLang.ToUpper <> "EN-US" Then
                'TranslateButtons(Form)
                'End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error required fields info for ResourceId " & ResourceId & " - " & ex.Message)
            End Try
        End Sub
        Public Sub ListGeneratorPopUp(ByVal Form As HtmlForm)
            'Dim db As New DataAccess
            Dim db2 As New DataAccess
            '   Dim da As OleDb.OleDbDataAdapter
            'Dim ds As New DataSet
            'Dim sb As New System.Text.StringBuilder
            'Dim intCounter As Integer
            'Dim intItem As Integer
            'Dim strInClause As String
            'Dim strSQL As String
            Dim strTemplate As String
            Dim dr As DataRow
            Dim DisplayField As String = "%DisplayField%"
            Dim ValueField As String = "%ValueField%"
            Dim TableName As String = "%TableName%"
            '  Dim objItem As DictionaryEntry
            'Dim ddl As DropDownList
            Dim dtDDLDefs As DataTable
            Dim ctl As Control
            Dim strControl As String
            Dim ResourceId As Integer
            Dim strSelect As String = ""
            Dim ty As Type
            Dim sLang As String
            Dim resourceName As String = "select"

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = CType(m_Context.Items("ResourceId"), Integer)

            'We first need to retrieve the information for the DDLS stored in the
            'DDLDefs item stored in the httpcontext.items collection.
            'We need the values
            dtDDLDefs = CType(m_Context.Items("DDLDefs"), DataTable)

            Try
                'We need to loop through each item in the DDLDefs datatable and replace the token fields
                'with the actual values
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db2.ConnectionString = "ConString"
                db2.ConnectionString = CType(myAdvAppSettings.AppSettings("ConString"), String)
                For Each dr In dtDDLDefs.Rows
                    If dr("UsePageSetup") = True Then
                        'We want to exclude an item if the current page is used to set up
                        'the ddl info for the item.
                        If ResourceId <> dr("DDLResourceId") Then
                            'If UsePageSetup is set to False we want to set up the ddl manually
                            'and so should skip this item
                            If dr("UsePageSetup") = True Then
                                'We can now set up the template
                                strTemplate = "SELECT %ValueField%,%DisplayField% " & _
                                              "FROM %TableName% " & _
                                              "ORDER BY %DisplayField%"
                                'Replace the TableName token
                                strTemplate = strTemplate.Replace(TableName, dr("TblName").ToString())
                                'Replace the DisplayField token
                                strTemplate = strTemplate.Replace(DisplayField, dr("DispText").ToString())
                                'Replace the ValueField token
                                strTemplate = strTemplate.Replace(ValueField, dr("DispValue").ToString())
                                'We can now execute the strTemplate sql statement. All we need to populate the
                                'associated control is to use a DataReader. At this point there is no need to
                                'worry about selecting an entry in the control(ddl or listbox)
                                Dim odr As OleDbDataReader
                                Dim strValFld As String = dr("DispValue").ToString()
                                Dim strTextFld As String = dr("DispText").ToString()

                                odr = db2.RunParamSQLDataReader(strTemplate)
                                'We need the control to be populated
                                If Not dr.IsNull("ControlName") Then
                                    'Added by Michelle R. Rodriguez on 03/09/2005.
                                    strControl = dr("ControlName").ToString()
                                Else
                                    strControl = "ddl" & dr("FldName").ToString()
                                End If

                                ctl = Form.FindControl(strControl)

                                If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                    Dim ddl As Telerik.Web.UI.RadComboBox
                                    ddl = CType(ctl, Telerik.Web.UI.RadComboBox)
                                    With ddl
                                        .DataSource = odr
                                        .DataTextField = strTextFld
                                        .DataValueField = strValFld
                                        .DataBind()
                                        '.Items.Insert(0, New ListItem(strSelect, ""))
                                        '.SelectedIndex = 0
                                    End With
                                Else
                                    Dim ddl As DropDownList
                                    ddl = CType(ctl, DropDownList)
                                    With ddl
                                        .DataSource = odr
                                        .DataTextField = strTextFld
                                        .DataValueField = strValFld
                                        .DataBind()
                                        .Items.Insert(0, New ListItem(strSelect, ""))
                                        .SelectedIndex = 0
                                    End With
                                End If


                                'If the current culture is not en-us then we need to translate
                                'the Select option to the relevant culture.
                                sLang = m_Context.Request.UserLanguages(0)
                                If sLang.ToUpper <> "EN-US" Then
                                    'Create a ResourceManager to deal with the translated content.
                                    ty = GetTypeFromPageName()
                                    Dim gStrings As New ResourceManager("AdvantageApp.strings", ty.Assembly)
                                    strSelect = gStrings.GetString(resourceName)
                                Else
                                    strSelect = "Select"
                                End If

                                If Not odr.IsClosed Then odr.Close()

                            End If
                        End If
                    End If
                Next

            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error building Lists Dataset - " & ex.Message)
            Finally
                If db2.Connection.State = ConnectionState.Open Then
                    db2.CloseConnection()
                End If

                db2.Dispose()
                'db2 = Nothing
            End Try

        End Sub
        Public Function BuildSQLSelect() As System.Collections.ArrayList
            '**************************************************************************************************
            'Purpose:       Uses the data dictionary to construct a sql select statement for the current page.
            '               It relies on two datatables added to the current httpcontext items collection by
            '               the ListGenerator and PageSetup subs.
            'Parameters:    None
            'Returns:       String containing the sql statement
            'Created:       Troy Richards, 7/7/2003
            '**************************************************************************************************
            Dim dtResFlds As DataTable
            Dim dtDefs As DataTable
            Dim dtTables As New DataTable
            Dim dr1, dr3 As DataRow '', dr4
            '' Dim strPrefix As String
            Dim intDDLCounter As Integer = 2
            Dim strTable As String
            Dim intPK As Integer
            Dim strSQL As String
            Dim arrSQL As New System.Collections.ArrayList
            '  Dim intItem As Integer
            '  Dim strInClause As String
            Dim db As New DataAccess
            Dim ds As New DataSet

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)
            dtDefs = CType(m_Context.Items("DDLDefs"), DataTable)

            'Make the DDLId column the primary key for the dtDefs datatable
            With dtDefs
                .PrimaryKey = New DataColumn() {.Columns("DDLId")}
            End With

            'Add a column called TblName and another called TblPK to the dtTables datatable
            Dim col1 As DataColumn = dtTables.Columns.Add("TblName")
            Dim col2 As DataColumn = dtTables.Columns.Add("TblPK", GetType(Integer))
            'Make the TblName column the primary key
            With dtTables
                .PrimaryKey = New DataColumn() {.Columns("TblName")}
            End With

            'Get DISTINCT tables that are used on the page. Loop through dt1 and add the
            'TblName from it to dt3 if it does not exist there already.
            For Each dr1 In dtResFlds.Rows
                If dtTables.Rows.Count = 0 Or Not ValExistsInDt(dtTables, dr1("TblName")) Then
                    Dim dr2 As DataRow = dtTables.NewRow
                    dr2("TblName") = dr1("TblName")
                    dr2("TblPK") = dr1("TblPK")
                    dtTables.Rows.Add(dr2)
                End If
            Next

            'Add dtTables to the items collection of the httpcontext object.
            If m_Context.Items.Contains("Tables") Then
                m_Context.Items.Remove("Tables")
            End If






            m_Context.Items.Add("Tables", dtTables)

            Dim strSELECT As String = ""
            Dim strFROM As String
            Dim strWHERE As String
            ' Dim strOrderBy As String
            'Loop through the dtTables datatable.
            For Each dr1 In dtTables.Rows
                'Find the matching rows in dtResFlds that belong to the current table.
                'It does not matter whether the field needs a lookup from a ddl
                Dim aRows As DataRow()
                strTable = dr1("TblName")
                aRows = dtResFlds.Select("TblName = '" & strTable & "'")
                For Each dr3 In aRows
                    strSELECT &= "t1." & dr3("FldName") & ","
                Next

                strFROM = strTable & " t1"

                'For the WHERE clause we need to know the primary key of the table.
                'We have the id of the primary key field stored in the dtTables datatable.
                'We can use that to search the dtResFlds table to find out the name of the
                'field.
                intPK = dr1("TblPK")
                Dim pkRows As DataRow()
                pkRows = dtResFlds.Select("FldId = " & intPK)
                strWHERE = "t1." & pkRows(0)("FldName").ToString() & " = ? "

                'Find the matching rows in the dtResFlds that have a DDLId
                'Dim aRows2 As DataRow()
                'aRows2 = dtResFlds.Select("DDLId IS NOT NULL AND TblName = '" & strTable & "'")
                'For Each dr3 In aRows2
                'Locate the DispText field in the dtDefs datatable
                'dr4 = dtDefs.Rows.Find(dr3("DDLId"))
                'strPrefix = "t" & intDDLCounter.ToString
                'strSELECT &= strPrefix & "." & dr4("DispText") & ","
                'strFROM &= "," & dr4("TblName") & " " & strPrefix
                'strWHERE &= "AND t1." & dr3("FldName").ToString() & " = " & strPrefix & "." & dr3("FldName") & " "
                'intDDLCounter += 1
                'Next
                'Remove the last comma from the string
                strSELECT = strSELECT.Substring(0, strSELECT.Length - 1)
                strSELECT = "SELECT " & strSELECT

                strFROM = "FROM " & strFROM
                strWHERE = "WHERE " & strWHERE
                strSQL = strSELECT & vbCrLf & strFROM & vbCrLf & strWHERE
                arrSQL.Add(strSQL)
                strText = strSQL
            Next
            Return arrSQL

        End Function

        Public Function ApplyMask(ByVal strMask As String, ByVal strVal As String) As String
            Dim arrMask As New ArrayList
            Dim arrVal As New ArrayList
            Dim chr As Char
            'Dim strMaskChars As String = "#&?AULH\"
            Const strMaskChars As String = "#"
            Dim intCounter As Integer
            Dim intCounter2 As Integer
            Dim strCorrVal As String
            Dim strReturn As String
            Dim strPrev As String = String.Empty

            'If an empty string is passed in then we should simply return the empty string since
            'we cannot apply any mask to an empty string
            If strVal = "" Then
                Return ""
            Else
                'Add each character in strMask to arrMask
                For Each chr In strMask
                    arrMask.Add(chr.ToString)
                Next

                'Add each character in strVal to arrVal
                For Each chr In strVal
                    arrVal.Add(chr)
                Next

                'We need to loop through the arrMask ArrayList and see if each item is a
                'mask character. If it is, we can use intCounter to get the corresponding
                'value from the arrVal ArrayList. Note that we ignore the \ character unless
                'it was preceded by another \. This means that at the end if we have \\ it
                'should be replaced by a \ and if we have a \ then it should be replaced
                'with an empty space.
                For intCounter2 = 0 To arrMask.Count - 1
                    If arrMask(intCounter2).ToString() = "\" And strPrev <> "\" Then
                        'ignore
                    ElseIf strMaskChars.IndexOf(arrMask(intCounter2).ToString()) <> -1 Then
                        strCorrVal = arrVal(intCounter).ToString()
                        arrMask(intCounter2) = strCorrVal
                        intCounter += 1
                    End If
                    strPrev = arrMask(intCounter2).ToString()
                Next
                Dim strChars(arrMask.Count) As String
                arrMask.CopyTo(strChars)
                strReturn = String.Join("", strChars)

                If strReturn.IndexOf("\\") <> -1 Then
                    strReturn = strReturn.Replace("\\", "\")

                ElseIf strReturn.IndexOf("\") <> -1 Then
                    strReturn = strReturn.Replace("\", "")
                Else
                    Return strReturn
                End If
            End If


            Return strReturn
        End Function

        Public Function RemoveMask(ByVal strMask As String, ByVal strVal As String) As String
            Dim arrMask As New System.Collections.ArrayList
            Dim arrVal As New System.Collections.ArrayList
            Dim arrRemovedVals As New System.Collections.ArrayList
            Dim chr As Char
            'Dim strMaskChars As String = "#&?A"
            Dim strMaskChars As String = "#"
            Dim intCounter As Integer
            Dim strCorrVal As String
            '  Dim strPrev As String

            'Add each character in strMask to arrMask
            For Each chr In strMask
                arrMask.Add(chr.ToString)
            Next

            'Add each character in strVal to arrVal
            For Each chr In strVal
                arrVal.Add(chr)
            Next

            'We need to loop through the arrMask ArrayList and see if each item is a
            'mask character. If it is, we can use intCounter to get the corresponding
            'value from the arrVal ArrayList. 
            For intCounter = 0 To arrMask.Count - 1
                If strMaskChars.IndexOf(arrMask(intCounter).ToString()) <> -1 Then
                    strCorrVal = arrVal(intCounter).ToString()
                    arrRemovedVals.Add(strCorrVal)
                End If

            Next
            Dim strChars(arrRemovedVals.Count) As String
            arrRemovedVals.CopyTo(strChars)
            Return String.Join("", strChars)
        End Function

        Public Function GetRegEx(ByVal strMask As String) As String

            Dim arrMask As New System.Collections.ArrayList
            Dim chr As Char
            'Dim strMaskChars As String = "#&?A"
            Dim strMaskChars As String = "#"

            Dim intCounter As Integer

            'Add each character in strMask to arrMask
            For Each chr In strMask
                arrMask.Add(chr.ToString)
            Next

            'We need to loop through the arrMask ArrayList and see if each item is a
            'mask character. If it is we will replace it with the equivalent
            'RegularExpressionValidator character
            For intCounter = 0 To arrMask.Count - 1
                If strMaskChars.IndexOf(arrMask(intCounter).ToString()) <> -1 Then
                    arrMask(intCounter) = GetValidationChar(arrMask(intCounter).ToString())
                End If
            Next
            Dim strChars(arrMask.Count) As String
            arrMask.CopyTo(strChars)
            Return String.Join("", strChars)
        End Function

        Private Function GetValidationChar(ByVal chr As Char) As String

            Select Case chr
                Case "#"
                    Return "\d"
                    'Case "&"
                    'Return "\s"
                    'Case "?"
                    'Return "\D"
                    'Case "A"
                    'Return "\w"
            End Select
            Return String.Empty
        End Function

        Public Function ValidateInputMask(ByVal strMask As String, ByVal strVal As String) As Boolean
            Dim objRegEx As Regex
            Dim regularExpression As String

            regularExpression = GetRegEx(strMask)
            objRegEx = New Regex(regularExpression)
            If objRegEx.IsMatch(strVal) Then
                Return True
            Else
                Return False
            End If
        End Function
#End Region

#Region "Public Methods"

        Public Sub SelValInDDL(ByVal ddl As WebControls.DropDownList, ByVal val As String)
            If val = System.Guid.Empty.ToString Then
                ddl.SelectedIndex = 0
            Else
                Dim i As Integer
                If Not ddl Is Nothing Then
                    For i = 0 To ddl.Items.Count - 1
                        If ddl.Items(i).Value.ToString = val.ToString Then
                            ddl.SelectedIndex = i
                        End If
                    Next
                End If
            End If
        End Sub
        Public Sub SelValInDDL1(ByVal ddl As DropDownList, ByVal val As String, ByVal tblName As String, ByVal dispText As String, ByVal dispValue As String)
            If val = Guid.Empty.ToString Then
                ddl.SelectedIndex = 0
            Else
                Dim i As Integer
                If Not ddl Is Nothing Then
                    For i = 0 To ddl.Items.Count - 1
                        If ddl.Items(i).Value.ToString = val.ToString Then
                            ddl.SelectedIndex = i
                            ''Added by saraswathi on May 22 2009
                            ''to fix issue 16285
                            Exit For
                        Else
                            ddl.SelectedIndex = 0
                        End If
                    Next
                    If ddl.SelectedIndex = 0 Then
                        CommonWebUtilities.SetSelectedValueAloneInAdvantageDDL(ddl, val, tblName, dispText, dispValue)
                    End If
                End If
            End If
        End Sub


        Public Sub SelTextInDDL(ByVal ddl As DropDownList, ByVal val As String)
            Dim i As Integer
            For i = 0 To ddl.Items.Count - 1
                If ddl.Items(i).Text.ToString = val.ToString Then
                    ddl.SelectedIndex = i
                End If
            Next
        End Sub


        Public Shared Function ValExistsInDt(ByVal dt As DataTable, ByVal val As String) As Boolean
            Dim dr As DataRow
            dr = dt.Rows.Find(val)
            Return Not (dr Is Nothing)
            'If dr Is Nothing Then
            '    Return False
            'Else
            '    Return True
            'End If
        End Function

        Public Shared Function ValExistsInDTString(ByVal dt As DataTable, ByVal val As String) As Boolean

            If dt.Rows.Count = 0 Then
                Return False
            Else
                Dim dr As DataRow() = dt.Select("FldId ='" & val & "'")
                If dr.Length <= 0 Then
                    Return False
                Else
                    Return True
                End If
            End If

        End Function

        Public Shared Function ValExistsInDT(ByVal dt As DataTable, ByVal val As Integer) As Boolean

            'Modified on 03/10/2005 By Balaji to solve the 
            'problem with Address Status and Phone Status

            If dt.Rows.Count = 0 Then
                Return False
            Else
                Dim dr As DataRow
                dr = dt.Rows.Find(val)
                If dr Is Nothing Then
                    Return False
                Else
                    Return True
                End If
            End If


            'Dim dr As DataRow() = dt.Select("FldId =" & val)
            'If dr.Length <= 0 Then
            '    Return False
            'Else
            '    Return True
            'End If
        End Function
        Public Shared Function ValExistsInDTSetCaptions(ByVal dt As DataTable, ByVal val As Integer) As Boolean

            'Modified on 03/10/2005 By Balaji to solve the 
            'problem with Address Status and Phone Status

            'Dim dr As DataRow
            'dr = dt.Rows.Find(val)
            'If dr Is Nothing Then
            '    Return False
            'Else
            '    Return True
            'End If

            Dim dr As DataRow() = dt.Select("FldId =" & val)
            If dr.Length <= 0 Then
                Return False
            Else
                Return True
            End If
        End Function
        Public Shared Function TblFldsIdExistsInDT(ByVal dt As DataTable, ByVal val As Integer) As Boolean
            Dim dr As DataRow() = dt.Select("TblFldsId =" & val)
            If dr.Length <= 0 Then
                Return False
            Else
                Return True
            End If
        End Function
        Public Function SetRequiredColorMask(ByVal sfldname As String) As String
            '**************************************************************************************************
            'Purpose:       When a page is loaded this sub is called to color the required fields and to
            '               dynamically add required field validators for fields that are required.
            '               This sub will also add validators to check the length of data entered into
            '               textboxes and also ensure the correct data type is entered as well. It will
            '               add the necessary RegularExpressionValidators for textboxes if they have input
            '               masks associated with them.
            'Parameters:
            '[Form]         The form on the page that contains the asp.net server controls
            '[Mode]         New or PostBack mode. New means that the page is loaded for the first time.
            '               PostBack mode is called when a post back occurs.
            'Returns:       N/A
            'Created:       Troy Richards, 7/3/2003
            'Notes:         This sub relies on the ResourceId placed in  the httpcontext.items collection when
            '               when a request for the page is made. See the Application_BeginRequest sub in the
            '               Global.aspx.vb page.
            '**************************************************************************************************
            Dim db As New DataAccess
            'Dim db2 As New DataAccess
            'Dim ds As New DataSet
            '  Dim dt3, dt4 As DataTable '' dt1, dt2,
            '  Dim dr, dr2 As DataRow
            '  Dim i As Integer
            '  Dim da As OleDbDataAdapter
            Dim sb As New System.Text.StringBuilder
            ' Dim ctl, ctl2 As Control
            '  Dim strControl As String
            ' Dim strPrefix As String
            Dim strValExpression As String = "\S{0,FldLen}"
            '  Dim strFldLen As String
            Dim colHashTable As New System.Collections.Hashtable(20, 0.1)
            '  Dim drMask As DataRow
            ' Dim strMask As String
            '  Dim strRegEx As String
            '  Dim drDDLDefs As DataRow
            'Dim intResourceId As Integer
            'Dim intDDLCounter As Integer
            Dim ResourceId As Integer
            Dim intCounter As Integer = 0
            '  Dim strPageName As String
            '  Dim ty As System.Type
            'Dim sLang As String
            'Dim sResourceName As String
            Dim sLangName As String
            Dim m_Context As HttpContext
            m_Context = HttpContext.Current

            ResourceId = CType(m_Context.Items("ResourceId"), Integer)
            sLangName = m_Context.Items("Language")
            sLangName = sLangName.ToUpper

            Try
                'Get the entries from the syResTblFlds table. This is in the
                'AdvantageMaster database
                With sb
                    .Append("SELECT case t1.Required when 1  then 'Yes' else 'No'  end as  Required  ")
                    .Append("FROM syResTblFlds t1,syTblFlds t2,syFields t3,syFieldTypes t4,syTables t5,syFldCaptions t6,syLangs t7 ")
                    .Append("WHERE t1.TblFldsId=t2.TblFldsId ")
                    .Append("AND t2.FldId=t3.FldId ")
                    .Append("AND t2.TblId=t5.TblId ")
                    .Append("AND t3.FldTypeId=t4.FldTypeId ")
                    .Append("AND t2.FldId=t6.FldId ")
                    .Append("AND t6.LangId=t7.LangId ")
                    .Append("AND t1.ResourceId=? ")
                    .Append("AND t7.LangName=? and t3.fldname=? ")
                End With

                db.AddParameter("@resid", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@langid", sLangName, DataAccess.OleDbDataType.OleDbString, 5, ParameterDirection.Input)
                db.AddParameter("@fldname", sfldname, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                Dim drFldRequired As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim strRequiredValue As String = String.Empty
                While drFldRequired.Read()
                    strRequiredValue = drFldRequired("Required").ToString()
                End While
                If Not drFldRequired.IsClosed Then drFldRequired.Close()
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                With sb
                    .Append(" select Count(*) as Required from syInstResFldsReq t1,syTblFlds t2,syFields t3 ")
                    .Append(" where t1.TblFldsId = t2.TblFldsId and t2.FldID = t3.FldId and t1.ResourceId=? and t3.FldName=? ")
                End With
                db.AddParameter("@ResourceId", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@FldName", sfldname, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                Dim intExist As Integer
                Try
                    intExist = db.RunParamSQLScalar(sb.ToString)
                Catch e As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(e)

                    intExist = 0
                End Try
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                With sb
                    .Append(" select Count(*) as Required from syInstFldsDDReq t1,syFields t2 ")
                    .Append(" where t1.FldId=t2.FldId and t2.FldName = ? ")
                End With
                db.AddParameter("@FldName", sfldname, DataAccess.OleDbDataType.OleDbString, , ParameterDirection.Input)

                Dim intExist2 As Integer
                Try
                    intExist2 = db.RunParamSQLScalar(sb.ToString)
                Catch e As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(e)

                    intExist2 = 0
                End Try
                db.ClearParameters()
                sb.Remove(0, sb.Length)


                If strRequiredValue = "Yes" Or intExist = 1 Or intExist2 = 1 Then
                    Return "Yes"
                Else
                    Return "No"
                End If
            Finally
            End Try
        End Function
        Public Function GetValidatorType(ByVal fieldType As String) As ValidationDataType
            Select Case fieldType.ToUpper
                Case "INT"
                    Return ValidationDataType.Integer
                Case "FLOAT"
                    Return ValidationDataType.Double
                Case "MONEY"
                    Return ValidationDataType.Currency
                Case "CHAR"
                    Return ValidationDataType.String
                Case "DATETIME"
                    Return ValidationDataType.Date
                Case "SMALLDATETIME"
                    Return ValidationDataType.Date
                Case "VARCHAR"
                    Return ValidationDataType.String
                Case "TINYINT"
                    Return ValidationDataType.Integer
                Case "SMALLINT"
                    Return ValidationDataType.Integer
                Case "DECIMAL"
                    Return ValidationDataType.Currency
            End Select
            Return ValidationDataType.String
        End Function

        Public Function FormatSSN(ByVal ssn As String) As String
            Dim str As String
            str = Mid(ssn, 1, 3) & "-" & Mid(ssn, 4, 2) & "-" & Mid(ssn, 6, 4)
            Return str
        End Function

        Public Function FormatPhone(ByVal phone As String) As String
            Dim str As String
            str = "(" & Mid(phone, 1, 3) & ")" & " " & Mid(phone, 4, 3) & "-" & Mid(phone, 7, 4)
            Return str
        End Function

        Public Function FormatError(ByVal errMsg As String) As String
            Dim str As String = errMsg
            If Left(errMsg, 1) = "," Then
                str = Mid(str, 2)
            End If
            Return str
        End Function

        Public Sub ListGenerator(ByVal pgContentPlaceHolder As WebControls.ContentPlaceHolder)
            Dim db As New DataAccess
            Dim db2 As New DataAccess
            '  Dim da As OleDb.OleDbDataAdapter
            Dim ds As New DataSet
            Dim sb As New System.Text.StringBuilder
            '  Dim intCounter As Integer
            '  Dim intItem As Integer
            '  Dim strInClause As String
            ' Dim strSQL As String
            Dim strTemplate As String
            Dim dr As DataRow
            Dim DisplayField As String = "%DisplayField%"
            Dim ValueField As String = "%ValueField%"
            Dim TableName As String = "%TableName%"
            '  Dim objItem As System.Collections.DictionaryEntry
            'Dim ddl As DropDownList
            Dim dtDDLDefs As DataTable
            Dim ctl As Control
            Dim strControl As String
            Dim ResourceId As Integer
            Dim strSelect As String = ""
            Dim ty As Type
            Dim sLang As String
            Dim resourceName As String = "select"

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = CType(m_Context.Items("ResourceId"), Integer)

            'We first need to retrieve the information for the DDLS stored in the
            'DDLDefs item stored in the httpcontext.items collection.
            'We need the values
            dtDDLDefs = CType(m_Context.Items("DDLDefs"), DataTable)

            Try
                'We need to loop through each item in the DDLDefs datatable and replace the token fields
                'with the actual values
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db2.ConnectionString = "ConString"
                db2.ConnectionString = CType(myAdvAppSettings.AppSettings("ConString"), String)
                For Each dr In dtDDLDefs.Rows
                    If dr("UsePageSetup") = True Then
                        'We want to exclude an item if the current page is used to set up
                        'the ddl info for the item.
                        If ResourceId <> dr("DDLResourceId") Then
                            'If UsePageSetup is set to False we want to set up the ddl manually
                            'and so should skip this item
                            If dr("UsePageSetup") = True Then
                                'We can now set up the template
                                strTemplate = "SELECT %ValueField%,%DisplayField% " & _
                                              "FROM %TableName% " & _
                                              "ORDER BY %DisplayField%"
                                'Replace the TableName token
                                strTemplate = strTemplate.Replace(TableName, dr("TblName").ToString())
                                'Replace the DisplayField token
                                strTemplate = strTemplate.Replace(DisplayField, dr("DispText").ToString())
                                'Replace the ValueField token
                                strTemplate = strTemplate.Replace(ValueField, dr("DispValue").ToString())
                                'We can now execute the strTemplate sql statement. All we need to populate the
                                'associated control is to use a DataReader. At this point there is no need to
                                'worry about selecting an entry in the control(ddl or listbox)
                                Dim odr As OleDbDataReader
                                Dim strValFld As String = dr("DispValue").ToString()
                                Dim strTextFld As String = dr("DispText").ToString()

                                odr = db2.RunParamSQLDataReader(strTemplate)
                                'We need the control to be populated
                                If Not dr.IsNull("ControlName") Then
                                    'Added by Michelle R. Rodriguez on 03/09/2005.
                                    strControl = dr("ControlName").ToString()
                                Else
                                    strControl = "ddl" & dr("FldName").ToString()
                                End If

                                ctl = pgContentPlaceHolder.FindControl(strControl)

                                Try
                                    If ResourceId = 145 And dr("FldName").ToString.ToLower = "stateid" Then
                                        Exit Try
                                    End If


                                    If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                        Dim ddl As Telerik.Web.UI.RadComboBox
                                        ddl = CType(ctl, Telerik.Web.UI.RadComboBox)
                                        With ddl
                                            .DataSource = odr
                                            .DataTextField = strTextFld
                                            .DataValueField = strValFld
                                            .DataBind()
                                            '.Items.Insert(0, New ListItem(strSelect, ""))
                                            '.SelectedIndex = 0
                                        End With
                                    Else
                                        Dim ddl As DropDownList
                                        ddl = CType(ctl, DropDownList)
                                        With ddl
                                            .DataSource = odr
                                            .DataTextField = strTextFld
                                            .DataValueField = strValFld
                                            .DataBind()
                                            .Items.Insert(0, New ListItem(strSelect, ""))
                                            .SelectedIndex = 0
                                        End With
                                    End If
                                Catch ex As Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)


                                End Try


                                If Not odr.IsClosed Then odr.Close()


                                'If the current culture is not en-us then we need to translate
                                'the Select option to the relevant culture.
                                sLang = m_Context.Request.UserLanguages(0)
                                If sLang.ToUpper <> "EN-US" Then
                                    'Create a ResourceManager to deal with the translated content.
                                    ty = GetTypeFromPageName()
                                    Dim gStrings As New ResourceManager("AdvantageApp.strings", ty.Assembly)
                                    strSelect = gStrings.GetString(resourceName)
                                Else
                                    strSelect = "Select"
                                End If



                            End If
                        End If
                    End If
                Next

            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error building Lists Dataset - " & ex.Message)
            Finally
                If db2.Connection.State = ConnectionState.Open Then
                    db2.CloseConnection()
                End If

                db2.Dispose()
                'db2 = Nothing
            End Try
        End Sub

        Public Sub ListGenerator(ByVal Form As HtmlForm)
            'Dim db As New DataAccess
            Dim db2 As New DataAccess
            '   Dim da As OleDb.OleDbDataAdapter
            'Dim ds As New DataSet
            'Dim sb As New System.Text.StringBuilder
            Dim strTemplate As String
            Dim dr As DataRow
            Dim DisplayField As String = "%DisplayField%"
            Dim ValueField As String = "%ValueField%"
            Dim TableName As String = "%TableName%"
            '    Dim objItem As DictionaryEntry
            'Dim ddl As DropDownList
            Dim dtDDLDefs As DataTable
            Dim ctl As Control
            Dim strControl As String
            Dim ResourceId As Integer
            Dim strSelect As String = ""
            Dim ty As Type
            Dim sLang As String
            Dim resourceName As String = "select"

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = CType(m_Context.Items("ResourceId"), Integer)

            'We first need to retrieve the information for the DDLS stored in the
            'DDLDefs item stored in the httpcontext.items collection.
            'We need the values
            dtDDLDefs = CType(m_Context.Items("DDLDefs"), DataTable)

            Try
                'We need to loop through each item in the DDLDefs datatable and replace the token fields
                'with the actual values
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db2.ConnectionString = "ConString"
                db2.ConnectionString = CType(myAdvAppSettings.AppSettings("ConString"), String)
                For Each dr In dtDDLDefs.Rows
                    If dr("UsePageSetup") = True Then
                        'We want to exclude an item if the current page is used to set up
                        'the ddl info for the item.
                        If ResourceId <> dr("DDLResourceId") Then
                            'If UsePageSetup is set to False we want to set up the ddl manually
                            'and so should skip this item
                            If dr("UsePageSetup") = True Then
                                'We can now set up the template
                                strTemplate = "SELECT %ValueField%,%DisplayField% " & _
                                              "FROM %TableName% " & _
                                              "ORDER BY %DisplayField%"
                                'Replace the TableName token
                                strTemplate = strTemplate.Replace(TableName, dr("TblName").ToString())
                                'Replace the DisplayField token
                                strTemplate = strTemplate.Replace(DisplayField, dr("DispText").ToString())
                                'Replace the ValueField token
                                strTemplate = strTemplate.Replace(ValueField, dr("DispValue").ToString())
                                'We can now execute the strTemplate sql statement. All we need to populate the
                                'associated control is to use a DataReader. At this point there is no need to
                                'worry about selecting an entry in the control(ddl or listbox)
                                Dim odr As OleDbDataReader
                                Dim strValFld As String = dr("DispValue").ToString()
                                Dim strTextFld As String = dr("DispText").ToString()

                                odr = db2.RunParamSQLDataReader(strTemplate)
                                'We need the control to be populated
                                If Not dr.IsNull("ControlName") Then
                                    'Added by Michelle R. Rodriguez on 03/09/2005.
                                    strControl = dr("ControlName").ToString()
                                Else
                                    strControl = "ddl" & dr("FldName").ToString()
                                End If

                                ctl = Form.FindControl(strControl)

                                If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                    Dim ddl As Telerik.Web.UI.RadComboBox
                                    ddl = CType(ctl, Telerik.Web.UI.RadComboBox)
                                    With ddl
                                        .DataSource = odr
                                        .DataTextField = strTextFld
                                        .DataValueField = strValFld
                                        .DataBind()
                                        '.Items.Insert(0, New ListItem(strSelect, ""))
                                        '.SelectedIndex = 0
                                    End With
                                Else
                                    Dim ddl As DropDownList
                                    ddl = CType(ctl, DropDownList)
                                    With ddl
                                        .DataSource = odr
                                        .DataTextField = strTextFld
                                        .DataValueField = strValFld
                                        .DataBind()
                                        .Items.Insert(0, New ListItem(strSelect, ""))
                                        .SelectedIndex = 0
                                    End With
                                End If

                                If Not odr.IsClosed Then odr.Close()

                                'If the current culture is not en-us then we need to translate
                                'the Select option to the relevant culture.
                                sLang = m_Context.Request.UserLanguages(0)
                                If sLang.ToUpper <> "EN-US" Then
                                    'Create a ResourceManager to deal with the translated content.
                                    ty = GetTypeFromPageName()
                                    Dim gStrings As New ResourceManager("AdvantageApp.strings", ty.Assembly)
                                    strSelect = gStrings.GetString(resourceName)
                                Else
                                    strSelect = "Select"
                                End If



                            End If
                        End If
                    End If
                Next

            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error building Lists Dataset - " & ex.Message)
            Finally
                If db2.Connection.State = ConnectionState.Open Then
                    db2.CloseConnection()
                End If

                db2.Dispose()
                'db2 = Nothing
            End Try

        End Sub
        Public Sub PageSetup(ByVal pgContentPlaceHolder As WebControls.ContentPlaceHolder, ByVal Mode As String, Optional ByVal InternationalAddress As Boolean = False)
            '**************************************************************************************************
            'Purpose:       When a page is loaded this sub is called to color the required fields and to
            '               dynamically add required field validators for fields that are required.
            '               This sub will also add validators to check the length of data entered into
            '               textboxes and also ensure the correct data type is entered as well. It will
            '               add the necessary RegularExpressionValidators for textboxes if they have input
            '               masks associated with them.
            'Parameters:
            '[Form]         The form on the page that contains the asp.net server controls
            '[Mode]         New or PostBack mode. New means that the page is loaded for the first time.
            '               PostBack mode is called when a post back occurs.
            'Returns:       N/A
            'Created:       Troy Richards, 7/3/2003
            'Notes:         This sub relies on the ResourceId placed in  the httpcontext.items collection when
            '               when a request for the page is made. See the Application_BeginRequest sub in the
            '               Global.aspx.vb page.
            '**************************************************************************************************
            Dim db As New DataAccess
            Dim db2 As New DataAccess
            Dim ds As New DataSet
            Dim dt1, dt2, dt3, dt4, dt5 As DataTable
            Dim dr As DataRow '', dr2
            '  Dim i As Integer
            Dim da As OleDbDataAdapter
            Dim sb As New System.Text.StringBuilder
            Dim ctl, ctl2 As New Control
            Dim strControl As String
            Dim strPrefix As String
            Dim strValExpression As String = "\S{0,FldLen}"
            Dim strFldLen As String
            Dim colHashTable As New Hashtable(20, 0.1)
            Dim drMask As DataRow
            Dim strMask As String
            '  Dim strRegEx As String
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim intDDLCounter As Integer
            Dim ResourceId As Integer
            Dim intCounter As Integer = 0
            '   Dim strPageName As String
            '  Dim ty As System.Type
            'Dim sLang As String
            'Dim sResourceName As String
            Dim sLangName As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")
            sLangName = m_Context.Items("Language")
            sLangName = sLangName.ToUpper

            Try
                'Get the entries from the syResTblFlds table. This is in the
                'AdvantageMaster database
                With sb
                    .Append("SELECT t1.ResDefId,t2.TblFldsId,t2.FldId,t5.TblName,t3.FldName,t1.Required,t3.DDLId,t3.FldLen,t4.FldType,t5.TblPK,t6.Caption,t1.ControlName,t1.UsePageSetup,t1.SchlReq ")
                    .Append("FROM syResTblFlds t1,syTblFlds t2,syFields t3,syFieldTypes t4,syTables t5,syFldCaptions t6,syLangs t7 ")
                    .Append("WHERE t1.TblFldsId=t2.TblFldsId ")
                    .Append("AND t2.FldId=t3.FldId ")
                    .Append("AND t2.TblId=t5.TblId ")
                    .Append("AND t3.FldTypeId=t4.FldTypeId ")
                    .Append("AND t2.FldId=t6.FldId ")
                    .Append("AND t6.LangId=t7.LangId ")
                    .Append("AND t1.ResourceId=? ")
                    .Append("AND t7.LangName=?")
                End With

                db.AddParameter("@resid", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@langid", sLangName, DataAccess.OleDbDataType.OleDbString, 5, ParameterDirection.Input)
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConStringMaster"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                Try
                    db.OpenConnection()
                    da = db.RunParamSQLDataAdapter(sb.ToString())
                    da.Fill(ds, "ResFlds")
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db.CloseConnection()
                    db.Dispose()
                    db = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syResTblFlds table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syResTblFlds table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries for the DDLs that are on the current page
                With sb
                    .Append("SELECT t1.ResDefId,t1.UsePageSetup,t2.TblFldsId,t3.FldName,t4.DDLId,t5.TblName,t6.FldName as DispText,t7.FldName as DispValue,t4.ResourceId as DDLResourceId,t1.ControlName ")
                    .Append("FROM syResTblFlds t1,syTblFlds t2,syFields t3,syDDLS t4,syTables t5,")
                    .Append("syFields t6,syFields t7 ")
                    .Append("WHERE t1.TblFldsId = t2.TblFldsId ")
                    .Append("AND t2.FldId=t3.FldId ")
                    .Append("AND t3.DDLId=t4.DDLId ")
                    .Append("AND t4.TblId=t5.TblId ")
                    .Append("AND t4.DispFldId=t6.FldId ")
                    .Append("AND t4.ValFldId=t7.FldId ")
                    ' Added by Corey Masson on May 27 2004 to exclude all records that are not using pagesetup
                    '.Append("AND t1.UsePageSetup = 1 ")
                    .Append("AND t1.ResourceId=?")
                End With
                db.AddParameter("@resid", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                Try
                    da = db.RunParamSQLDataAdapter(sb.ToString())
                    da.Fill(ds, "DDLDefS")
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db.CloseConnection()
                    db.Dispose()
                    db = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving ddls that are on the current page:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving ddls that are on the current page:" & ex.InnerException.Message)
                    End If
                End Try
                'Cleanup
                db.CloseConnection()
                db.Dispose()
                db = Nothing


                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db2.ConnectionString = "ConString"
                db2.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db2.OpenConnection()

                'Get the entries for the data dictionary school required fields.
                With sb
                    .Append("SELECT FldId ")
                    .Append("FROM syInstFldsDDReq ")
                End With

                Try
                    da = db2.RunParamSQLDataAdapter(sb.ToString)
                    da.Fill(ds, "InstDDReq")
                    db2.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.CloseConnection()
                    db2.Dispose()
                    db2 = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syInstFldsDDReq table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syInstFldsDDReq table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries from the syInstResFldsReq table.
                With sb
                    .Append("SELECT t1.TblFldsId,t2.FldId ")
                    .Append("FROM syInstResFldsReq t1, syTblFlds t2 ")
                    .Append("WHERE t1.TblFldsId=t2.TblFldsId ")
                    .Append("AND ResourceId = ? ")
                End With

                db2.AddParameter("@resid", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Try
                    da = db2.RunParamSQLDataAdapter(sb.ToString)
                    da.Fill(ds, "InstResFldsReq")
                    db2.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.CloseConnection()
                    db2.Dispose()
                    db2 = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syInstResFldsReq table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syInstResFldsReq table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries from the syInstFldsMask table. This is also in the 
                'regular Advantage database.
                With sb
                    .Append("SELECT FldId,Mask ")
                    .Append("FROM syInstFldsMask")
                End With

                Try
                    da = db2.RunParamSQLDataAdapter(sb.ToString)
                    da.Fill(ds, "FldsMask")
                    db2.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.CloseConnection()
                    db2.Dispose()
                    db2 = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syInstFldsMask table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syInstFldsMask table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries from the syRptFields table. This table stores the fields that are needed
                'to support the different reporting agencies such as IPEDS. This code is specific for
                'IPEDS reporting. We only want this code to be executed if the Web.Config file indicates
                'that the school is using IPEDS reporting functionality.
                If MyAdvAppSettings.AppSettings("IPEDS").ToLower = "yes" Then
                    With sb
                        .Append("SELECT distinct t2.FldId ")
                        .Append("FROM syRptAgencyFields t1, syRptFields t2 ")
                        .Append("WHERE t1.RptFldId=t2.RptFldId ")
                        .Append("AND t1.RptAgencyId=1 ")
                        .Append("AND t2.FldId IS NOT NULL ")
                    End With
                    Try
                        da = db2.RunParamSQLDataAdapter(sb.ToString)
                        da.Fill(ds, "IPEDSFields")
                        db2.ClearParameters()
                        sb.Remove(0, sb.Length)
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        db2.CloseConnection()
                        db2.Dispose()
                        db2 = Nothing
                        If ex.InnerException Is Nothing Then
                            Throw New BaseException("Error retrieving entries for IPEDS fields:" & ex.Message)
                        Else
                            Throw New BaseException("Error retrieving entries for IPEDS fields:" & ex.InnerException.Message)
                        End If
                    End Try

                End If

                'Cleanup
                db2.CloseConnection()
                db2.Dispose()
                db2 = Nothing


                'Create primary key
                dt1 = ds.Tables("ResFlds")
                With ds.Tables("ResFlds")
                    .PrimaryKey = New DataColumn() {dt1.Columns("TblFldsId")}
                End With

                dt2 = ds.Tables("InstDDReq")
                With ds.Tables("InstDDReq")
                    .PrimaryKey = New DataColumn() {dt2.Columns("FldId")}
                End With

                dt3 = ds.Tables("InstResFldsReq")
                With ds.Tables("InstResFldsReq")
                    .PrimaryKey = New DataColumn() {dt3.Columns("TblFldsId")}
                End With

                dt4 = ds.Tables("FldsMask")
                With ds.Tables("FldsMask")
                    .PrimaryKey = New DataColumn() {dt4.Columns("FldId")}
                End With

                With ds.Tables("DDLDefs")
                    .PrimaryKey = New DataColumn() {.Columns("DDLId")}
                End With

                dt5 = New DataTable
                If Not ds.Tables("IPEDSFields") Is Nothing Then
                    If ds.Tables("IPEDSFields").Rows.Count > 0 Then
                        dt5 = ds.Tables("IPEDSFields")
                        With ds.Tables("IPEDSFields")
                            .PrimaryKey = New DataColumn() {dt5.Columns("FldId")}
                        End With

                    End If
                End If

                'Add the dt1 DataTable to the httpcontext items collection so it can be 
                'reused by the PopulatePage sub if needed.
                If m_Context.Items("ResFlds") Is Nothing Then m_Context.Items.Add("ResFlds", dt1)
                If m_Context.Items("InstResFldsReq") Is Nothing Then m_Context.Items.Add("InstResFldsReq", dt3)
                If m_Context.Items("FldsMask") Is Nothing Then m_Context.Items.Add("FldsMask", dt4)
                If m_Context.Items("DDLDefs") Is Nothing Then m_Context.Items.Add("DDLDefs", ds.Tables("DDLDefs"))
                If m_Context.Items("IPEDSFields") Is Nothing Then m_Context.Items.Add("IPEDSFields", dt5)

                'Create a ResourceManager to deal with the translated content.
                'ty = GetTypeFromPageName()
                'Dim gStrings As New ResourceManager("AdvantageApp.strings", ty.Assembly)
                'sLang = m_Context.Request.UserLanguages(0)

                'We need to loop through the records in the ResFlds datatable. If a field
                'is a required field we need to color the associated control and add a
                'RequiredFieldValidator.

                ' US3134 take out ResourceId = 317 for Terminate Student page
                '  If ResourceId = 317 Or ResourceId = 84 Or ResourceId = 85 Then
                If ResourceId = 84 Or ResourceId = 85 Then
                    '
                Else


                    For Each dr In dt1.Rows
                        'If dr("UsePageSetup") = True Then
                        intCounter += 1
                        'If the ControlName field is not null then we have to use the control specified
                        'by that field.
                        Dim ctrlname As String = dr("FldName").ToString
                        Dim fldid As String = dr("FldId").ToString
                        If Not dr.IsNull("ControlName") Then
                            strControl = dr("ControlName").ToString()
                            strPrefix = strControl.Substring(0, 3)
                        Else
                            'If the DDLId associated with the record is null then we are
                            'dealing with a textbox or a checkbox. If not we are dealing with a dropdownlist.
                            'A checkbox normally is associated with a bit field in the database.
                            If dr.IsNull("DDLId") Then
                                If dr("FldType") = "Bit" Then 'Checkbox
                                    strPrefix = "chk"
                                Else 'Textbox
                                    strPrefix = "txt"
                                End If
                            Else
                                'This will usually be a ddl. However, if we are on the page that sets up the
                                'ddl itself then it will be a textbox. If the ResouceId associated with the
                                'DDLId is the same as the ResourceId of the current page then it is a textbox.
                                'For example, on the page that sets up the states the StateId will be a textbox
                                'rather than a ddl. On other pages it will be a ddl.

                                drDDLDefs = ds.Tables("DDLDefs").Rows.Find(dr("DDLId"))
                                intResourceId = drDDLDefs("DDLResourceId")
                                If intResourceId = ResourceId Then
                                    'We are on the page that sets up the ddl
                                    strPrefix = "txt"
                                Else
                                    strPrefix = "ddl"
                                    'Increment the counter for tracking the number of ddls
                                    'on the page.
                                    intDDLCounter += 1
                                End If

                            End If
                            strControl = strPrefix & dr("FldName").ToString()
                        End If

                        ctl = pgContentPlaceHolder.FindControl(strControl)

                        If ctl Is Nothing Then
                            strText = "Could not find control:" & strControl
                            'added logic to throw an exception when we do not find a control when we are supposed to
                            'Corey Masson Dec. 5, 2003
                            Throw New System.Exception("Could not find the control: " & strControl)
                        End If
                        ctl2 = pgContentPlaceHolder.FindControl("pnlRequiredFieldValidators")
                        If ctl2 Is Nothing Then
                            Throw New System.Exception("Could not find panel named pnlRequiredFieldValidators")
                        End If
                        strFldLen = dr("FldLen").ToString()

                        'If mode is NEW then we need to set the captions for the label
                        'controls on the page.
                        If Mode = "NEW" Then
                            'If we are dealing with a textbox or a ddl then we need
                            'to check if there is an associated label control.
                            If strPrefix = "txt" Or strPrefix = "ddl" Then
                                Dim ctl3 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("FldName").ToString())
                                'The control might not have a label control associated with it.
                                If Not ctl3 Is Nothing Then
                                    Try
                                        CType(ctl3, WebControls.Label).Text = dr("Caption")
                                    Catch ex As System.Exception
                                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                                    	exTracker.TrackExceptionWrapper(ex)

                                        Throw New BaseException("Problem casting " & strControl & " to label " & "with caption " & dr("Caption").ToString)
                                    End Try
                                End If

                            ElseIf strPrefix = "chk" Then 'Checkbox
                                'We have to find the checkbox itself
                                Dim ctl3 As Control = pgContentPlaceHolder.FindControl("chk" & dr("FldName").ToString())
                                'If the checkbox was not found then we should notify the client
                                If ctl3 Is Nothing Then
                                    Throw New System.Exception("Could not find checkbox:" & strControl)
                                End If
                                'The checkbox might be found but we might have a problem
                                'in the casting operation.
                                If Not ctl3 Is Nothing Then
                                    Try
                                        CType(ctl3, WebControls.CheckBox).Text = dr("Caption")
                                    Catch ex As System.Exception
                                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                                    	exTracker.TrackExceptionWrapper(ex)

                                        Throw New BaseException("Problem casting " & strControl & " to checkbox " & "with caption " & dr("Caption").ToString)
                                    End Try
                                End If


                            End If

                        End If  'Mode is NEW and we should populate label controls

                        'Color the required fields.
                        Dim req As Boolean = dr("Required")
                        Try
                            If ResourceId = 75 Then
                                Exit Try 'No validation needed for Setup Bank Codes
                            End If
                            If InternationalAddress And dr("FldName").ToString().ToLower = "stateid" Then
                                Dim ctl3 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("FldName").ToString())
                                'The control might not have a label control associated with it.
                                If Not ctl3 Is Nothing Then
                                    CType(ctl3, WebControls.Label).Text = dr("Caption")
                                End If
                                Exit Try
                            End If

                            If (dr("Required") = True Or ValExistsInDt(dt2, dr("FldId")) Or TblFldsIdExistsInDT(dt3, dr("TblFldsId")) Or ValExistsInDt(dt5, dr("FldId"))) Then
                                Try
                                    'DE9100
                                    '***  DD Suspend the yellow background
                                    'If strPrefix = "txt" Then
                                    '    If TypeOf (ctl) Is TextBox Then
                                    '        CType(ctl, TextBox).BackColor = Color.FromName("#ffff99")
                                    '    ElseIf TypeOf (ctl) Is Telerik.Web.UI.RadTextBox Then
                                    '        CType(ctl, Telerik.Web.UI.RadTextBox).BackColor = Color.FromName("#ffff99")
                                    '    ElseIf TypeOf (ctl) Is Telerik.Web.UI.RadDatePicker Then
                                    '        CType(ctl, Telerik.Web.UI.RadDatePicker).DateInput.BackColor = Color.FromName("#ffff99")
                                    '    End If
                                    'ElseIf strPrefix = "ddl" Then
                                    '    Try
                                    '       
                                    '            CType(ctl, Telerik.Web.UI.RadComboBox).BackColor = Color.FromName("#ffff99")
                                    '        Else
                                    '            CType(ctl, DropDownList).BackColor = Color.FromName("#ffff99")
                                    '        End If

                                    '    Catch ex As System.Exception
                                     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
                                    '    	exTracker.TrackExceptionWrapper(ex)

                                    '        CType(ctl, HtmlSelect).Style("background-color") = "Yellow"
                                    '    End Try
                                    'End If
                                    'making the required for cmsId if teh config setting for 'EnableAFAIntegration' is yes else avoid it as required.
                                    If not (dr("FldName").ToString().ToLower = "cmsid" and MyAdvAppSettings.AppSettings("EnableAFAIntegration").ToLower = "no")
                                    Dim ctl3 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("FldName").ToString())
                                    'The control might not have a label control associated with it.
                                    If Not ctl3 Is Nothing Then
                                        CType(ctl3, WebControls.Label).Text = dr("Caption") & "<font color=""red"">*</font>"
                                    End If
                                    End If
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    Throw New System.Exception
                                End Try
                                'Add a RequiredFieldVaidator to monitor the control.
                                If strPrefix = "txt" Then
                                    If Not ((dr("FldName").ToString.ToLower = "hsid") or ((dr("FldName").ToString().ToLower = "cmsid" and MyAdvAppSettings.AppSettings("EnableAFAIntegration").ToLower = "no")) ) Then 'Ignore HSID from validation
                                        Dim rfv As New WebControls.RequiredFieldValidator
                                        With rfv
                                            .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                            .ControlToValidate = strControl
                                            .Display = WebControls.ValidatorDisplay.None
                                            .ErrorMessage = dr("Caption").ToString & " is required"
                                        End With
                                        ctl2.Controls.Add(rfv)
                                    End If
                                End If
                                'Add a RequiredFieldVaidator to monitor the control.
                                If strPrefix = "ddl" Then
                                    If ResourceId = 21 And dr("FldName").ToString().ToLower = "stateid" Then
                                        'Do nothing as this is handled on the page
                                    Else

                                        Dim rfv As New WebControls.RequiredFieldValidator
                                        With rfv
                                            .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                            .ControlToValidate = strControl
                                            .Display = WebControls.ValidatorDisplay.None
                                            .ErrorMessage = dr("Caption").ToString & " is required"
                                            .InitialValue = ""
                                        End With
                                        ctl2.Controls.Add(rfv)

                                        'add a compare validator
                                        Dim cv As New WebControls.CompareValidator
                                        With cv
                                            .ID = "cv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                            .ControlToValidate = strControl
                                            .Display = WebControls.ValidatorDisplay.None
                                            .ErrorMessage = dr("Caption").ToString & " is required"
                                            .ValueToCompare = System.Guid.Empty.ToString
                                            .Operator = WebControls.ValidationCompareOperator.NotEqual
                                            .EnableClientScript = True
                                        End With
                                        ctl2.Controls.Add(cv)
                                    End If
                                End If

                            End If 'Control is required
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)


                        End Try


                        'If the control is a textbox we need to also set the MaxLength property
                        'and also add a CompareValidator to perform a data type check. If it has
                        'an input mask associated with it then we should also add a 
                        'RegularExpressionValidator.
                        If strPrefix = "txt" Then
                            If Not TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                If dr("FldType") = "Varchar" Or dr("FldType") = "Char" Then
                                    CType(ctl, WebControls.TextBox).MaxLength = CInt(dr("FldLen"))
                                ElseIf dr("FldType") = "Smallint" Then 'Changed as per discussion w/ Troy to fix issue: 2483
                                    CType(ctl, WebControls.TextBox).MaxLength = 4
                                ElseIf dr("FldType") = "TinyInt" Then 'Changed as per discussion w/ Troy to fix issue: 2483
                                    CType(ctl, WebControls.TextBox).MaxLength = 3
                                End If
                                'Else '**Changed as per discussion w/ Troy to fix issue: 2483**'
                                '    CType(ctl, TextBox).MaxLength = 50
                            End If

                            Try
                                If dr("FldName").ToString.ToLower = "hsid" Then
                                    Exit Try
                                End If

                                Dim cmv As New WebControls.CompareValidator
                                With cmv
                                    .ID = "cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                    .ControlToValidate = strControl
                                    .Display = WebControls.ValidatorDisplay.None
                                    .ErrorMessage = "Incorrect data type for " & dr("Caption").ToString()
                                    .[Operator] = WebControls.ValidationCompareOperator.DataTypeCheck
                                    .Type = GetValidatorType(dr("FldType"))
                                End With
                                ctl2.Controls.Add(cmv)
                            Catch ex As Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)


                            End Try



                            drMask = dt4.Rows.Find(dr("FldId"))
                            If Not IsNothing(drMask) Then
                                strMask = drMask("Mask")
                                Dim rev As New WebControls.RegularExpressionValidator
                                With rev
                                    .ID = "rev" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                    .ControlToValidate = strControl
                                    .Display = WebControls.ValidatorDisplay.None
                                    .ErrorMessage = "Incorrect format for " & strControl
                                    .ValidationExpression = GetRegEx(strMask)
                                End With
                                ctl2.Controls.Add(rev)
                            End If
                        End If 'Set MaxLength for TextBox control

                        'If the page has a summary list on the lhs then we need to
                        'call SummListGenerator to populate that control.
                        'End If
                    Next

                End If

                'If the Mode is New we should populate the dropdownlists on the page.
                'On a post back there is no need to populate the ddls on the page as that
                'is handled automatically by ASP.NET. If the page does not contain any
                'ddls then we do not want to call the ListGenerator sub.
                If Mode = "NEW" And intDDLCounter > 0 Then
                    ListGenerator(pgContentPlaceHolder)
                End If
                'If the mode is new and the culture is not en-us then we should
                'translate the button captions
                'If Mode = "NEW" And sLang.ToUpper <> "EN-US" Then
                'TranslateButtons(Form)
                'End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error required fields info for ResourceId " & ResourceId & " - " & ex.Message)
            End Try
        End Sub
        Public Sub PageSetup(ByVal Form As HtmlForm, ByVal Mode As String)
            '**************************************************************************************************
            'Purpose:       When a page is loaded this sub is called to color the required fields and to
            '               dynamically add required field validators for fields that are required.
            '               This sub will also add validators to check the length of data entered into
            '               textboxes and also ensure the correct data type is entered as well. It will
            '               add the necessary RegularExpressionValidators for textboxes if they have input
            '               masks associated with them.
            'Parameters:
            '[Form]         The form on the page that contains the asp.net server controls
            '[Mode]         New or PostBack mode. New means that the page is loaded for the first time.
            '               PostBack mode is called when a post back occurs.
            'Returns:       N/A
            'Created:       Troy Richards, 7/3/2003
            'Notes:         This sub relies on the ResourceId placed in  the httpcontext.items collection when
            '               when a request for the page is made. See the Application_BeginRequest sub in the
            '               Global.aspx.vb page.
            '**************************************************************************************************
            Dim db As New DataAccess
            Dim db2 As New DataAccess
            Dim ds As New DataSet
            Dim dt1, dt2, dt3, dt4, dt5 As DataTable
            Dim dr As DataRow '', dr2
            ' Dim i As Integer
            Dim da As OleDbDataAdapter
            Dim sb As New System.Text.StringBuilder
            Dim ctl, ctl2 As Control
            Dim strControl As String
            Dim strPrefix As String
            Dim strValExpression As String = "\S{0,FldLen}"
            Dim strFldLen As String
            Dim colHashTable As New Hashtable(20, 0.1)
            Dim drMask As DataRow
            Dim strMask As String
            '   Dim strRegEx As String
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim intDDLCounter As Integer
            Dim ResourceId As Integer
            Dim intCounter As Integer = 0
            ' Dim strPageName As String
            ' Dim ty As Type
            'Dim sLang As String
            'Dim sResourceName As String
            Dim sLangName As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")
            sLangName = m_Context.Items("Language")
            sLangName = sLangName.ToUpper

            Try
                'Get the entries from the syResTblFlds table. This is in the
                'AdvantageMaster database
                With sb
                    .Append("SELECT t1.ResDefId,t2.TblFldsId,t2.FldId,t5.TblName,t3.FldName,t1.Required,t3.DDLId,t3.FldLen,t4.FldType,t5.TblPK,t6.Caption,t1.ControlName,t1.UsePageSetup,t1.SchlReq ")
                    .Append("FROM syResTblFlds t1,syTblFlds t2,syFields t3,syFieldTypes t4,syTables t5,syFldCaptions t6,syLangs t7 ")
                    .Append("WHERE t1.TblFldsId=t2.TblFldsId ")
                    .Append("AND t2.FldId=t3.FldId ")
                    .Append("AND t2.TblId=t5.TblId ")
                    .Append("AND t3.FldTypeId=t4.FldTypeId ")
                    .Append("AND t2.FldId=t6.FldId ")
                    .Append("AND t6.LangId=t7.LangId ")
                    .Append("AND t1.ResourceId=? ")
                    .Append("AND t7.LangName=?")
                End With

                db.AddParameter("@resid", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@langid", sLangName, DataAccess.OleDbDataType.OleDbString, 5, ParameterDirection.Input)
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConStringMaster"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                Try
                    db.OpenConnection()
                    da = db.RunParamSQLDataAdapter(sb.ToString())
                    da.Fill(ds, "ResFlds")
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db.CloseConnection()
                    db.Dispose()
                    db = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syResTblFlds table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syResTblFlds table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries for the DDLs that are on the current page
                With sb
                    .Append("SELECT t1.ResDefId,t1.UsePageSetup,t2.TblFldsId,t3.FldName,t4.DDLId,t5.TblName,t6.FldName as DispText,t7.FldName as DispValue,t4.ResourceId as DDLResourceId,t1.ControlName ")
                    .Append("FROM syResTblFlds t1,syTblFlds t2,syFields t3,syDDLS t4,syTables t5,")
                    .Append("syFields t6,syFields t7 ")
                    .Append("WHERE t1.TblFldsId = t2.TblFldsId ")
                    .Append("AND t2.FldId=t3.FldId ")
                    .Append("AND t3.DDLId=t4.DDLId ")
                    .Append("AND t4.TblId=t5.TblId ")
                    .Append("AND t4.DispFldId=t6.FldId ")
                    .Append("AND t4.ValFldId=t7.FldId ")
                    ' Added by Corey Masson on May 27 2004 to exclude all records that are not using pagesetup
                    '.Append("AND t1.UsePageSetup = 1 ")
                    .Append("AND t1.ResourceId=?")
                End With
                db.AddParameter("@resid", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                Try
                    da = db.RunParamSQLDataAdapter(sb.ToString())
                    da.Fill(ds, "DDLDefS")
                    db.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db.CloseConnection()
                    db.Dispose()
                    db = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving ddls that are on the current page:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving ddls that are on the current page:" & ex.InnerException.Message)
                    End If
                End Try
                'Cleanup
                db.CloseConnection()
                db.Dispose()
                db = Nothing


                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db2.ConnectionString = "ConString"
                db2.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db2.OpenConnection()

                'Get the entries for the data dictionary school required fields.
                With sb
                    .Append("SELECT FldId ")
                    .Append("FROM syInstFldsDDReq ")
                End With

                Try
                    da = db2.RunParamSQLDataAdapter(sb.ToString)
                    da.Fill(ds, "InstDDReq")
                    db2.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.CloseConnection()
                    db2.Dispose()
                    db2 = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syInstFldsDDReq table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syInstFldsDDReq table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries from the syInstResFldsReq table.
                With sb
                    .Append("SELECT t1.TblFldsId,t2.FldId ")
                    .Append("FROM syInstResFldsReq t1, syTblFlds t2 ")
                    .Append("WHERE t1.TblFldsId=t2.TblFldsId ")
                    .Append("AND ResourceId = ? ")
                End With

                db2.AddParameter("@resid", ResourceId, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                Try
                    da = db2.RunParamSQLDataAdapter(sb.ToString)
                    da.Fill(ds, "InstResFldsReq")
                    db2.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.CloseConnection()
                    db2.Dispose()
                    db2 = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syInstResFldsReq table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syInstResFldsReq table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries from the syInstFldsMask table. This is also in the 
                'regular Advantage database.
                With sb
                    .Append("SELECT FldId,Mask ")
                    .Append("FROM syInstFldsMask")
                End With

                Try
                    da = db2.RunParamSQLDataAdapter(sb.ToString)
                    da.Fill(ds, "FldsMask")
                    db2.ClearParameters()
                    sb.Remove(0, sb.Length)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.CloseConnection()
                    db2.Dispose()
                    db2 = Nothing
                    If ex.InnerException Is Nothing Then
                        Throw New BaseException("Error retrieving entries from the syInstFldsMask table:" & ex.Message)
                    Else
                        Throw New BaseException("Error retrieving entries from the syInstFldsMask table:" & ex.InnerException.Message)
                    End If
                End Try

                'Get the entries from the syRptFields table. This table stores the fields that are needed
                'to support the different reporting agencies such as IPEDS. This code is specific for
                'IPEDS reporting. We only want this code to be executed if the Web.Config file indicates
                'that the school is using IPEDS reporting functionality.
                If MyAdvAppSettings.AppSettings("IPEDS").ToLower = "yes" Then
                    With sb
                        .Append("SELECT distinct t2.FldId ")
                        .Append("FROM syRptAgencyFields t1, syRptFields t2 ")
                        .Append("WHERE t1.RptFldId=t2.RptFldId ")
                        .Append("AND t1.RptAgencyId=1 ")
                        .Append("AND t2.FldId IS NOT NULL ")
                    End With
                    Try
                        da = db2.RunParamSQLDataAdapter(sb.ToString)
                        da.Fill(ds, "IPEDSFields")
                        db2.ClearParameters()
                        sb.Remove(0, sb.Length)
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        db2.CloseConnection()
                        db2.Dispose()
                        db2 = Nothing
                        If ex.InnerException Is Nothing Then
                            Throw New BaseException("Error retrieving entries for IPEDS fields:" & ex.Message)
                        Else
                            Throw New BaseException("Error retrieving entries for IPEDS fields:" & ex.InnerException.Message)
                        End If
                    End Try

                End If

                'Cleanup
                db2.CloseConnection()
                db2.Dispose()
                db2 = Nothing


                'Create primary key
                dt1 = ds.Tables("ResFlds")
                With ds.Tables("ResFlds")
                    .PrimaryKey = New DataColumn() {dt1.Columns("TblFldsId")}
                End With

                dt2 = ds.Tables("InstDDReq")
                With ds.Tables("InstDDReq")
                    .PrimaryKey = New DataColumn() {dt2.Columns("FldId")}
                End With

                dt3 = ds.Tables("InstResFldsReq")
                With ds.Tables("InstResFldsReq")
                    .PrimaryKey = New DataColumn() {dt3.Columns("TblFldsId")}
                End With

                dt4 = ds.Tables("FldsMask")
                With ds.Tables("FldsMask")
                    .PrimaryKey = New DataColumn() {dt4.Columns("FldId")}
                End With

                With ds.Tables("DDLDefs")
                    .PrimaryKey = New DataColumn() {.Columns("DDLId")}
                End With

                dt5 = New DataTable
                If Not ds.Tables("IPEDSFields") Is Nothing Then
                    If ds.Tables("IPEDSFields").Rows.Count > 0 Then
                        dt5 = ds.Tables("IPEDSFields")
                        With ds.Tables("IPEDSFields")
                            .PrimaryKey = New DataColumn() {dt5.Columns("FldId")}
                        End With

                    End If
                End If

                'Add the dt1 DataTable to the httpcontext items collection so it can be 
                'reused by the PopulatePage sub if needed.
                m_Context.Items.Add("ResFlds", dt1)
                m_Context.Items.Add("FldsMask", dt4)
                m_Context.Items.Add("DDLDefs", ds.Tables("DDLDefs"))
                m_Context.Items.Add("IPEDSFields", dt5)

                'Create a ResourceManager to deal with the translated content.
                'ty = GetTypeFromPageName()
                'Dim gStrings As New ResourceManager("AdvantageApp.strings", ty.Assembly)
                'sLang = m_Context.Request.UserLanguages(0)

                'We need to loop through the records in the ResFlds datatable. If a field
                'is a required field we need to color the associated control and add a
                'RequiredFieldValidator.


                'If ResourceId <> 317 Then






                For Each dr In dt1.Rows
                    'If dr("UsePageSetup") = True Then
                    intCounter += 1
                    'If the ControlName field is not null then we have to use the control specified
                    'by that field.
                    Dim ctrlname As String = dr("FldName").ToString
                    Dim fldid As String = dr("FldId").ToString
                    If Not dr.IsNull("ControlName") Then
                        strControl = dr("ControlName").ToString()
                        strPrefix = strControl.Substring(0, 3)
                    Else
                        'If the DDLId associated with the record is null then we are
                        'dealing with a textbox or a checkbox. If not we are dealing with a dropdownlist.
                        'A checkbox normally is associated with a bit field in the database.
                        If dr.IsNull("DDLId") Then
                            If dr("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else 'Textbox
                                strPrefix = "txt"
                            End If
                        Else
                            'This will usually be a ddl. However, if we are on the page that sets up the
                            'ddl itself then it will be a textbox. If the ResouceId associated with the
                            'DDLId is the same as the ResourceId of the current page then it is a textbox.
                            'For example, on the page that sets up the states the StateId will be a textbox
                            'rather than a ddl. On other pages it will be a ddl.

                            drDDLDefs = ds.Tables("DDLDefs").Rows.Find(dr("DDLId"))
                            intResourceId = drDDLDefs("DDLResourceId")
                            If intResourceId = ResourceId Then
                                'We are on the page that sets up the ddl
                                strPrefix = "txt"
                            Else
                                strPrefix = "ddl"
                                'Increment the counter for tracking the number of ddls
                                'on the page.
                                intDDLCounter += 1
                            End If

                        End If
                        strControl = strPrefix & dr("FldName").ToString()
                    End If

                    ctl = Form.FindControl(strControl)
                    If ctl Is Nothing Then
                        strText = "Could not find control:" & strControl
                        'added logic to throw an exception when we do not find a control when we are supposed to
                        'Corey Masson Dec. 5, 2003
                        Throw New System.Exception("Could not find the control: " & strControl)
                    End If
                    ctl2 = Form.FindControl("pnlRequiredFieldValidators")
                    If ctl2 Is Nothing Then
                        Throw New System.Exception("Could not find panel named pnlRequiredFieldValidators")
                    End If
                    strFldLen = dr("FldLen").ToString()

                    'If mode is NEW then we need to set the captions for the label
                    'controls on the page.
                    If Mode = "NEW" Then
                        'If we are dealing with a textbox or a ddl then we need
                        'to check if there is an associated label control.
                        If strPrefix = "txt" Or strPrefix = "ddl" Then
                            Dim ctl3 As Control = Form.FindControl("lbl" & dr("FldName").ToString())
                            'The control might not have a label control associated with it.
                            If Not ctl3 Is Nothing Then
                                Try
                                    CType(ctl3, Label).Text = dr("Caption")
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    Throw New BaseException("Problem casting " & strControl & " to label " & "with caption " & dr("Caption").ToString)
                                End Try
                            End If

                        ElseIf strPrefix = "chk" Then 'Checkbox
                            'We have to find the checkbox itself
                            Dim ctl3 As Control = Form.FindControl("chk" & dr("FldName").ToString())
                            'If the checkbox was not found then we should notify the client
                            If ctl3 Is Nothing Then
                                Throw New System.Exception("Could not find checkbox:" & strControl)
                            End If
                            'The checkbox might be found but we might have a problem
                            'in the casting operation.
                            If Not ctl3 Is Nothing Then
                                Try
                                    CType(ctl3, CheckBox).Text = dr("Caption")
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    Throw New BaseException("Problem casting " & strControl & " to checkbox " & "with caption " & dr("Caption").ToString)
                                End Try
                            End If


                        End If

                    End If  'Mode is NEW and we should populate label controls

                    'Color the required fields.
                    Dim req As Boolean = dr("Required")
                    If (dr("Required") = True Or ValExistsInDt(dt2, dr("FldId")) Or TblFldsIdExistsInDT(dt3, dr("TblFldsId")) Or ValExistsInDt(dt5, dr("FldId"))) Then
                        Try
                            If strPrefix = "txt" Then
                                If TypeOf (ctl) Is TextBox Then
                                    ' CType(ctl, TextBox).BackColor = Color.FromName("#ffff99")
                                ElseIf TypeOf (ctl) Is Telerik.Web.UI.RadTextBox Then
                                    ' CType(ctl, Telerik.Web.UI.RadTextBox).BackColor = Color.FromName("#ffff99")
                                ElseIf TypeOf (ctl) Is Telerik.Web.UI.RadDatePicker Then
                                    '  CType(ctl, Telerik.Web.UI.RadDatePicker).DateInput.BackColor = Color.FromName("#ffff99")
                                End If
                            ElseIf strPrefix = "ddl" Then
                                Try
                                    If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                        '  CType(ctl, Telerik.Web.UI.RadComboBox).BackColor = Color.FromName("#ffff99")
                                    Else
                                        '   CType(ctl, DropDownList).BackColor = Color.FromName("#ffff99")
                                    End If

                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    '   CType(ctl, HtmlSelect).Style("background-color") = "Yellow"
                                End Try
                            End If
                            Dim ctl3 As Control = Form.FindControl("lbl" & dr("FldName").ToString())
                            'The control might not have a label control associated with it.
                            If Not ctl3 Is Nothing Then
                                CType(ctl3, Label).Text = dr("Caption") & "<font color=""red"">*</font>"
                            End If
                        Catch ex As System.Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            Throw New System.Exception
                        End Try
                        'Add a RequiredFieldVaidator to monitor the control.
                        If strPrefix = "txt" Then
                            Dim rfv As New RequiredFieldValidator
                            With rfv
                                .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = dr("Caption").ToString & " is required"
                            End With
                            ctl2.Controls.Add(rfv)
                        End If
                        'Add a RequiredFieldVaidator to monitor the control.
                        If strPrefix = "ddl" Then
                            Dim rfv As New RequiredFieldValidator
                            With rfv
                                .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = dr("Caption").ToString & " is required"
                            End With
                            ctl2.Controls.Add(rfv)

                            'add a compare validator
                            Dim cv As New CompareValidator
                            With cv
                                .ID = "cv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = dr("Caption").ToString & " is required"
                                .ValueToCompare = Guid.Empty.ToString
                                .Operator = ValidationCompareOperator.NotEqual
                                .EnableClientScript = True
                            End With
                            ctl2.Controls.Add(cv)
                        End If

                    End If 'Control is required

                    'If the control is a textbox we need to also set the MaxLength property
                    'and also add a CompareValidator to perform a data type check. If it has
                    'an input mask associated with it then we should also add a 
                    'RegularExpressionValidator.
                    If strPrefix = "txt" Then
                        If dr("FldType") = "Varchar" Or dr("FldType") = "Char" Then
                            If Not ResourceId = 288 Then
                                CType(ctl, TextBox).MaxLength = CInt(dr("FldLen"))
                            End If
                        ElseIf dr("FldType") = "Smallint" Then 'Changed as per discussion w/ Troy to fix issue: 2483
                            CType(ctl, TextBox).MaxLength = 4
                        ElseIf dr("FldType") = "TinyInt" Then 'Changed as per discussion w/ Troy to fix issue: 2483
                            CType(ctl, TextBox).MaxLength = 3
                        End If
                        'Else '**Changed as per discussion w/ Troy to fix issue: 2483**'
                        '    CType(ctl, TextBox).MaxLength = 50

                        Dim cmv As New CompareValidator
                        With cmv
                            .ID = "cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                            .ControlToValidate = strControl
                            .Display = ValidatorDisplay.None
                            .ErrorMessage = "Incorrect data type for " & dr("Caption").ToString()
                            .[Operator] = ValidationCompareOperator.DataTypeCheck
                            .Type = GetValidatorType(dr("FldType"))
                        End With
                        ctl2.Controls.Add(cmv)


                        drMask = dt4.Rows.Find(dr("FldId"))
                        If Not IsNothing(drMask) Then
                            strMask = drMask("Mask")
                            Dim rev As New RegularExpressionValidator
                            With rev
                                .ID = "rev" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = "Incorrect format for " & strControl
                                .ValidationExpression = GetRegEx(strMask)
                            End With
                            ctl2.Controls.Add(rev)
                        End If
                    End If 'Set MaxLength for TextBox control

                    'If the page has a summary list on the lhs then we need to
                    'call SummListGenerator to populate that control.
                    'End If
                Next

                'End If

                'If the Mode is New we should populate the dropdownlists on the page.
                'On a post back there is no need to populate the ddls on the page as that
                'is handled automatically by ASP.NET. If the page does not contain any
                'ddls then we do not want to call the ListGenerator sub.
                If Mode = "NEW" And intDDLCounter > 0 Then
                    ListGenerator(Form)
                End If
                'If the mode is new and the culture is not en-us then we should
                'translate the button captions
                'If Mode = "NEW" And sLang.ToUpper <> "EN-US" Then
                'TranslateButtons(Form)
                'End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error required fields info for ResourceId " & ResourceId & " - " & ex.Message)
            End Try
        End Sub

        Public Sub SetCaptionsAndColorRequiredFields(ByVal pgContentPlaceHolder As WebControls.ContentPlaceHolder, Optional ByVal LeadGroup As String = "NULL", Optional ByVal InternationalAddress As Boolean = False)
            '**************************************************************************************************
            'Purpose:       When a page is loaded this sub is called to enter the captions and color the
            '               required fields.
            'Parameters:
            'Returns:       N/A
            'Created:       Troy Richards, 2/15/2005
            'Notes:         This sub relies on the ResourceId placed in  the httpcontext.items collection when
            '               when a request for the page is made. See the Application_BeginRequest sub in the
            '               Global.aspx.vb page.
            '**************************************************************************************************
            Dim ds As New DataSet
            Dim dtResDef, dtDDReqFlds, dtResReqFlds, dtIPEDSFields As DataTable
            Dim dr As DataRow '', dr2
            Dim strControl As String
            Dim strPrefix As String
            '  Dim intResourceId As Integer
            Dim ResourceId As Integer
            Dim intCounter As Integer = 0
            Dim sLangName As String
            Dim fac As New PageSetupFacade
            Dim ctl, ctl2 As Control '', ctl1
            Dim strFldLen As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")
            sLangName = m_Context.Items("Language")
            sLangName = sLangName.ToUpper



            Try
                'Get the resource definition table for this page
                dtResDef = fac.GetResourceDef(ResourceId, sLangName)

                'Get the data dictionary required fields
                dtDDReqFlds = fac.GetDataDictionaryReqFields

                'Get the school required fields for the resource being viewed
                dtResReqFlds = fac.GetSchlRequiredFieldsForResource(ResourceId)

                'Get the IPEDS fields
                dtIPEDSFields = New DataTable
                If MyAdvAppSettings.AppSettings("IPEDS").ToLower = "yes" Then
                    dtIPEDSFields = fac.GetIPEDSFields
                End If


                'Create PKs
                'Commented By Balaji on 3/19/2005 to avoid the problem 
                'with Address Status and Phone Status
                'With dtResDef
                '    .PrimaryKey = New DataColumn() {dtResDef.Columns("FldId")}
                'End With

                'With dtDDReqFlds
                '    .PrimaryKey = New DataColumn() {dtDDReqFlds.Columns("FldId")}
                'End With

                'With dtResReqFlds
                '    .PrimaryKey = New DataColumn() {dtResReqFlds.Columns("FldId")}
                'End With

                With dtIPEDSFields
                    .PrimaryKey = New DataColumn() {dtIPEDSFields.Columns("FldId")}
                End With
                'We need to loop through the records in the dtResDef datatable. 
                'First we will set up the captions and then deal with the required fields.                'is a required field we need to color the associated control and add a
                'For required fields we will color them appropriately and set up a RequiredFieldValidator.
                For Each dr In dtResDef.Rows
                    intCounter += 1
                    'If the ControlName field is not null then we have to use the control specified
                    'by that field.
                    Dim ctrlname As String = dr("FldName").ToString
                    Dim fldid As String = dr("FldId").ToString
                    If Not dr.IsNull("ControlName") Then
                        strControl = dr("ControlName").ToString()
                        strPrefix = strControl.Substring(0, 3)
                    Else
                        'If the DDLId associated with the record is null then we are
                        'dealing with a textbox or a checkbox. If not we are dealing with a dropdownlist.
                        'A checkbox normally is associated with a bit field in the database.
                        If dr.IsNull("DDLId") Then
                            If dr("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else 'Textbox
                                strPrefix = "txt"
                            End If
                        Else
                            strPrefix = "ddl"
                        End If
                        strControl = strPrefix & dr("FldName").ToString()
                    End If

                    ctl = pgContentPlaceHolder.FindControl(strControl)
                    If ctl Is Nothing Then
                        Throw New System.Exception("Could not find the control: " & strControl)
                    End If
                    ctl2 = pgContentPlaceHolder.FindControl("pnlRequiredFieldValidators")
                    If ctl2 Is Nothing Then
                        Throw New System.Exception("Could not find panel named pnlRequiredFieldValidators")
                    End If
                    strFldLen = dr("FldLen").ToString()

                    'If we are dealing with a textbox or a ddl then we need
                    'to check if there is an associated label control.
                    If strPrefix = "txt" Or strPrefix = "ddl" Then
                        Dim ctl3 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("FldName").ToString())
                        'The control might not have a label control associated with it.
                        If Not ctl3 Is Nothing Then
                            Try
                                CType(ctl3, WebControls.Label).Text = dr("Caption")
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Throw New BaseException("Problem casting " & strControl & " to label " & "with caption " & dr("Caption").ToString)
                            End Try
                        End If
                        'The label control might be named after the name of the ddl or textbox control.
                        'This should only be performed when the controlname is specified
                        If Not dr.IsNull("ControlName") Then
                            Dim ctl30 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("ControlName").ToString().Substring(3))
                            If Not ctl30 Is Nothing Then
                                Try
                                    CType(ctl30, WebControls.Label).Text = dr("Caption")
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    Throw New BaseException("Problem casting " & strControl & " to label " & "with caption " & dr("Caption").ToString)
                                End Try
                            End If
                        End If

                    ElseIf strPrefix = "chk" Then 'Checkbox
                        'We have to find the checkbox itself
                        Dim ctl3 As Control = pgContentPlaceHolder.FindControl("chk" & dr("FldName").ToString())
                        'If the checkbox was not found then we should notify the client
                        If ctl3 Is Nothing Then
                            Throw New System.Exception("Could not find checkbox:" & strControl)
                        End If
                        'The checkbox might be found but we might have a problem
                        'in the casting operation.
                        If Not ctl3 Is Nothing Then
                            Try
                                '  If Not ctrlname = "LeadGrpId" Then
                                CType(ctl3, WebControls.CheckBox).Text = dr("Caption")
                                '  End If
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Throw New BaseException("Problem casting " & strControl & " to checkbox " & "with caption " & dr("Caption").ToString)
                            End Try
                        End If
                    End If

                    'Color the required fields.
                    Dim req As Boolean = dr("Required")
                    If dr("Required") = True Or ValExistsInDTString(dtDDReqFlds, dr("FldId")) Or TblFldsIdExistsInDT(dtResReqFlds, dr("TblFldsId")) Or ((ResourceId = 174 Or ResourceId = 203 Or ResourceId = 169) And (MyAdvAppSettings.AppSettings("IPEDS").ToLower = "yes" And ValExistsInDt(dtIPEDSFields, dr("FldId")))) Then
                        Try
                            'If international address is checked and its a state field then do not mark it as required
                            'Balaji 03/04/2011
                            If InternationalAddress = True And CInt(dr("FldId")) = 11 Then
                                Exit Try
                            End If

                            If strPrefix = "txt" Then
                                'If (TypeOf ctl Is WebControls.TextBox) Then
                                '    CType(ctl, WebControls.TextBox).BackColor = Color.FromName("#ffff99")
                                'ElseIf (TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox) Then
                                '    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).BackColor = Color.FromName("#ffff99")
                                'End If

                            ElseIf strPrefix = "ddl" Then
                                If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                    ' CType(ctl, Telerik.Web.UI.RadComboBox).BackColor = Color.FromName("#ffff99")
                                Else
                                    'Enroll Leads page, International Address and State Field
                                    'If ResourceId = 174 And InternationalAddress = True And CInt(dr("FldId")) = 11 Then
                                    If ResourceId = 174 And CInt(dr("FldId")) = 11 Then
                                        ' CType(ctl, WebControls.DropDownList).BackColor = Color.FromName("White")
                                    Else
                                        'CType(ctl, WebControls.DropDownList).BackColor = Color.FromName("#ffff99")
                                    End If
                                End If
                            End If

                            Dim ctl3 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("FldName").ToString())
                            'The control might not have a label control associated with it.
                            If Not ctl3 Is Nothing Then
                                If ResourceId = 174 And CInt(dr("FldId")) = 11 Then
                                    CType(ctl3, WebControls.Label).Text = dr("Caption")
                                ElseIf ((ResourceId = 206 Or ResourceId = 170) And dr("FldName") = "Phone") Then
                                    'New Lead and Existing Lead pages
                                    CType(ctl3, WebControls.Label).Text = "Phone 1<font color=""red"">*</font>"
                                Else
                                    CType(ctl3, WebControls.Label).Text = dr("Caption") & "<font color=""red"">*</font>"
                                End If

                            End If
                        Catch ex As System.Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            Throw New System.Exception
                        End Try

                        'Add a RequiredFieldVaidator to monitor the control.
                        If strPrefix = "txt" Or strPrefix = "rad" Then
                            Try
                                If dr("FldName").ToString.ToLower = "adreqid" Then
                                    Exit Try
                                End If
                                Dim rfv As New WebControls.RequiredFieldValidator
                                With rfv
                                    .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                    .ControlToValidate = strControl
                                    .Display = WebControls.ValidatorDisplay.None
                                    .ErrorMessage = dr("Caption").ToString & " is required"
                                End With
                                ctl2.Controls.Add(rfv)
                            Catch ex As Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)


                            End Try

                        End If

                        'Add a regular expression validator to monitor the control.
                        If strPrefix = "ddl" Then
                            'add a required field validator

                            Try
                                'If international address is checked and its a state field then do not mark it as required
                                'Balaji 03/04/2011
                                If InternationalAddress = True And CInt(dr("FldId")) = 11 Then
                                    Exit Try
                                End If
                                If ResourceId = 174 And CInt(dr("FldId")) = 11 Then
                                    Exit Try 'Don't add required validator for State Field - Balaji 03/09/2011
                                End If
                                If ResourceId = 337 And dr("FldName").ToString.ToLower = "adreqid" Then
                                    Exit Try 'Don't add required validator for State Field - Balaji 02/06/2012
                                End If
                                Dim rfv As New WebControls.RequiredFieldValidator
                                With rfv
                                    .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                    .ControlToValidate = strControl
                                    .Display = WebControls.ValidatorDisplay.None
                                    .ErrorMessage = dr("Caption").ToString & " is required"
                                    .InitialValue = ""
                                    .EnableClientScript = True
                                End With
                                ctl2.Controls.Add(rfv)

                                'add a compare validator
                                Dim cv As New WebControls.CompareValidator
                                With cv
                                    .ID = "cv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                    .ControlToValidate = strControl
                                    .Display = WebControls.ValidatorDisplay.None
                                    .ErrorMessage = dr("Caption").ToString & " is required"
                                    .ValueToCompare = System.Guid.Empty.ToString
                                    .Operator = WebControls.ValidationCompareOperator.NotEqual
                                    .EnableClientScript = True
                                End With
                                ctl2.Controls.Add(cv)
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                            End Try
                        End If

                    Else
                        If ((ResourceId = 206 Or ResourceId = 170) And dr("FldName") = "Phone") Then
                            'New Lead and Existing Lead pages
                            'We want the label to just say Phone 1
                            Dim ctl3 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("FldName").ToString())
                            'The control might not have a label control associated with it.
                            If Not ctl3 Is Nothing Then
                                CType(ctl3, WebControls.Label).Text = "Phone 1"
                            End If
                        End If
                    End If 'Control is required

                    'If the control is a textbox we need to also set the MaxLength property
                    'and also add a CompareValidator to perform a data type check. 
                    If strPrefix = "txt" Then
                        If TypeOf ctl Is WebControls.TextBox Then


                            If dr("FldType") = "Varchar" Or dr("FldType") = "Char" Then
                                CType(ctl, WebControls.TextBox).MaxLength = CInt(dr("FldLen"))
                            Else
                                CType(ctl, WebControls.TextBox).MaxLength = 50
                            End If
                        End If
                        Dim cmv As New WebControls.CompareValidator
                        Try
                            'If ctl2.FindControl("cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString()) Is True Then
                            '    ctl2.Controls.Remove("cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString())
                            'End If
                            If dr("FldName").ToString.ToLower = "adreqid" Then
                                Exit Try
                            End If
                            With cmv
                                .ID = "cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = WebControls.ValidatorDisplay.None
                                .ErrorMessage = "Incorrect data type for " & dr("Caption").ToString()
                                .[Operator] = WebControls.ValidationCompareOperator.DataTypeCheck
                                .Type = GetValidatorType(dr("FldType"))
                            End With

                            ctl2.Controls.Add(cmv)
                        Catch ex As System.Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)


                        End Try



                    End If 'Set MaxLength for TextBox control
                Next


            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                If Not ex.InnerException Is Nothing Then
                    Throw New BaseException("Error required fields info for ResourceId " & ResourceId & " - " & ex.InnerException.Message)
                Else
                    Throw New BaseException("Error required fields info for ResourceId " & ResourceId & " - " & ex.Message)
                End If

            End Try

        End Sub

        Public Sub SetCaptionsAndColorRequiredFieldsForQuickLeads(ByVal pgContentPlaceHolder As WebControls.ContentPlaceHolder,
                                                                  Optional ByVal LeadGroup As String = "NULL",
                                                                  Optional ByVal InternationalAddress As Boolean = False,
                                                                  Optional ByVal InternationalPhone1 As Boolean = False)
            '**************************************************************************************************
            'Purpose:       When a page is loaded this sub is called to enter the captions and color the
            '               required fields.
            'Parameters:
            '[Form]         The form on the page that contains the asp.net server controls
            'Returns:       N/A
            'Created:       Troy Richards, 2/15/2005
            'Notes:         This sub relies on the ResourceId placed in  the httpcontext.items collection when
            '               when a request for the page is made. See the Application_BeginRequest sub in the
            '               Global.aspx.vb page.
            '**************************************************************************************************
            Dim ds As New DataSet
            Dim dtResDef, dtDDReqFlds, dtResReqFlds, dtIPEDSFields As DataTable
            Dim dr As DataRow '', dr2
            Dim strControl As String
            Dim strPrefix As String
            '  Dim intResourceId As Integer
            Dim ResourceId As Integer
            Dim intCounter As Integer = 0
            Dim sLangName As String
            Dim fac As New PageSetupFacade
            Dim ctl, ctl2 As Control '', ctl1
            Dim strFldLen As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")
            sLangName = m_Context.Items("Language")
            sLangName = sLangName.ToUpper

            Try
                'Get the resource definition table for this page
                dtResDef = fac.GetResourceForQuickLeads()

                'Get the data dictionary required fields
                dtDDReqFlds = fac.GetDataDictionaryReqFieldsForQuickLeads

                'Get the school required fields for the resource being viewed
                dtResReqFlds = fac.GetSchlRequiredFieldsForResourceForQuickLeads(ResourceId)

                'Get the IPEDS fields
                dtIPEDSFields = New DataTable
                If MyAdvAppSettings.AppSettings("IPEDS").ToLower = "yes" Then
                    dtIPEDSFields = fac.GetIPEDSFieldsForQuickLeads
                End If


                'Create PKs
                'Commented By Balaji on 3/19/2005 to avoid the problem 
                'with Address Status and Phone Status
                'With dtResDef
                '    .PrimaryKey = New DataColumn() {dtResDef.Columns("FldId")}
                'End With

                'With dtDDReqFlds
                '    .PrimaryKey = New DataColumn() {dtDDReqFlds.Columns("FldId")}
                'End With

                'With dtResReqFlds
                '    .PrimaryKey = New DataColumn() {dtResReqFlds.Columns("FldId")}
                'End With

                With dtIPEDSFields
                    .PrimaryKey = New DataColumn() {dtIPEDSFields.Columns("FldId")}
                End With
                'We need to loop through the records in the dtResDef datatable. 
                'First we will set up the captions and then deal with the required fields.                'is a required field we need to color the associated control and add a
                'For required fields we will color them appropriately and set up a RequiredFieldValidator.
                For Each dr In dtResDef.Rows
                    intCounter += 1
                    'If the ControlName field is not null then we have to use the control specified
                    'by that field.
                    Dim ctrlname As String = dr("FldName").ToString
                    Dim fldid As String = dr("FldId").ToString
                    If Not dr.IsNull("ControlName") Then
                        strControl = dr("ControlName").ToString()
                        strPrefix = strControl.Substring(0, 3)
                    Else
                        'If the DDLId associated with the record is null then we are
                        'dealing with a textbox or a checkbox. If not we are dealing with a dropdownlist.
                        'A checkbox normally is associated with a bit field in the database.
                        If dr.IsNull("DDLId") Then
                            If dr("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else 'Textbox
                                strPrefix = "txt"
                            End If
                        Else
                            strPrefix = "ddl"
                        End If
                        strControl = strPrefix & dr("FldName").ToString()
                    End If

                    ctl = pgContentPlaceHolder.FindControl(strControl)
                    If ctl Is Nothing Then
                        Throw New System.Exception("Could not find the control: " & strControl)
                    End If
                    ctl2 = pgContentPlaceHolder.FindControl("pnlRequiredFieldValidators")
                    If ctl2 Is Nothing Then
                        Throw New System.Exception("Could not find panel named pnlRequiredFieldValidators")
                    End If
                    strFldLen = dr("FldLen").ToString()

                    ' Regular Expression Validation
                    Dim rev = New RegularExpressionValidator() With { _
                                  .ID = String.Format("rev{0}{1}", dr("FldName"), dr("TblFldsId")), _
                                  .ControlToValidate = strControl, _
                                  .Display = ValidatorDisplay.None _
                              }
                    If Not InternationalAddress AndAlso dr("FldName").ToString() = "Zip" Then
                        rev.ErrorMessage = "Please enter the proper Zip Code"
                        rev.ValidationExpression = String.Format("{0}{1},{2}{3}", "^(.|" & vbLf & "|" & vbTab & "){", 5, dr("FldLen"), "}$")
                        ctl2.Controls.Add(rev)
                    ElseIf Not InternationalPhone1 AndAlso ("Phone" = dr("FldName").ToString() Or "Phone2" = dr("FldName").ToString()) Then
                        rev.ErrorMessage = String.Format("{0} format is (###) ###-####", dr("Caption"))
                        rev.ValidationExpression = String.Format("{0}{1},{2}{3}", "^(.|" & vbLf & "|" & vbTab & "){", 10, dr("FldLen"), "}$")
                        ctl2.Controls.Add(rev)
                    End If

                    'If we are dealing with a textbox or a ddl then we need
                    'to check if there is an associated label control.
                    If strPrefix = "txt" Or strPrefix = "ddl" Then
                        Dim ctl3 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("FldName").ToString())
                        'The control might not have a label control associated with it.
                        If Not ctl3 Is Nothing Then
                            Try
                                CType(ctl3, WebControls.Label).Text = dr("Caption")
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Throw New BaseException("Problem casting " & strControl & " to label " & "with caption " & dr("Caption").ToString)
                            End Try
                        End If
                        'The label control might be named after the name of the ddl or textbox control.
                        'This should only be performed when the controlname is specified
                        If Not dr.IsNull("ControlName") Then
                            Dim ctl30 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("ControlName").ToString().Substring(3))
                            If Not ctl30 Is Nothing Then
                                Try
                                    CType(ctl30, WebControls.Label).Text = dr("Caption")
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    Throw New BaseException("Problem casting " & strControl & " to label " & "with caption " & dr("Caption").ToString)
                                End Try
                            End If
                        End If

                    ElseIf strPrefix = "chk" Then 'Checkbox
                        'We have to find the checkbox itself
                        Dim ctl3 As Control = pgContentPlaceHolder.FindControl("chk" & dr("FldName").ToString())
                        'If the checkbox was not found then we should notify the client
                        If ctl3 Is Nothing Then
                            Throw New System.Exception("Could not find checkbox:" & strControl)
                        End If
                        'The checkbox might be found but we might have a problem
                        'in the casting operation.
                        If Not ctl3 Is Nothing Then
                            Try
                                '  If Not ctrlname = "LeadGrpId" Then
                                CType(ctl3, WebControls.CheckBox).Text = dr("Caption")
                                '  End If
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Throw New BaseException("Problem casting " & strControl & " to checkbox " & "with caption " & dr("Caption").ToString)
                            End Try
                        End If
                    End If

                    'Color the required fields.
                    Dim req As Boolean = dr("Required")
                    ' If dr("Required") = True Or ValExistsInDTString(dtDDReqFlds, dr("FldId")) Or TblFldsIdExistsInDT(dtResReqFlds, dr("TblFldsId")) Or ValExistsInDT(dtIPEDSFields, dr("FldId")) Then
                    If dr("Required") = True Or ValExistsInDTString(dtDDReqFlds, dr("FldId")) Or TblFldsIdExistsInDT(dtResReqFlds, dr("TblFldsId")) Or
                            ((ResourceId <> 268) And ValExistsInDt(dtIPEDSFields, dr("FldId"))) Then
                        Try
                            'If international address is checked and its a state field then do not mark it as required
                            'Balaji 03/04/2011
                            If InternationalAddress = True And CInt(dr("FldId")) = 11 Then
                                Exit Try
                            End If
                            'If strPrefix = "txt" Then
                            '    CType(ctl, WebControls.TextBox).BackColor = Color.FromName("#ffff99")
                            'ElseIf strPrefix = "ddl" Then
                            '    If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                            '        CType(ctl, Telerik.Web.UI.RadComboBox).BackColor = Color.FromName("#ffff99")
                            '    Else
                            '        CType(ctl, WebControls.DropDownList).BackColor = Color.FromName("#ffff99")
                            '    End If
                            'End If
                            Dim ctl3 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("FldName").ToString())
                            'The control might not have a label control associated with it.
                            If Not ctl3 Is Nothing Then
                                CType(ctl3, WebControls.Label).Text = dr("Caption") & "<font color=""red"">*</font>"
                            End If
                        Catch ex As System.Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            Throw New System.Exception
                        End Try

                        'Add a RequiredFieldVaidator to monitor the control.
                        If strPrefix = "txt" Or strPrefix = "rad" Then
                            Dim rfv As New WebControls.RequiredFieldValidator
                            With rfv
                                .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = WebControls.ValidatorDisplay.None
                                .ErrorMessage = dr("Caption").ToString & " is required"
                            End With
                            ctl2.Controls.Add(rfv)


                        End If



                        'Add a regular expression validator to monitor the control.
                        If strPrefix = "ddl" Then
                            Try
                                'If international address is checked and its a state field then do not mark it as required
                                'Balaji 03/04/2011
                                If InternationalAddress = True And CInt(dr("FldId")) = 11 Then
                                    Exit Try
                                End If
                                'add a required field validator
                                Dim rfv As New WebControls.RequiredFieldValidator
                                With rfv
                                    .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                    .ControlToValidate = strControl
                                    .Display = WebControls.ValidatorDisplay.None
                                    .ErrorMessage = dr("Caption").ToString & " is required"
                                    .InitialValue = ""
                                    .EnableClientScript = True
                                End With
                                ctl2.Controls.Add(rfv)

                                'add a compare validator
                                Dim cv As New WebControls.CompareValidator
                                With cv
                                    .ID = "cv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                    .ControlToValidate = strControl
                                    .Display = WebControls.ValidatorDisplay.None
                                    .ErrorMessage = dr("Caption").ToString & " is required"
                                    .ValueToCompare = System.Guid.Empty.ToString
                                    .Operator = WebControls.ValidationCompareOperator.NotEqual
                                    .EnableClientScript = True
                                End With
                                ctl2.Controls.Add(cv)
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                            End Try
                        End If
                    End If 'Control is required

                    'If the control is a textbox we need to also set the MaxLength property
                    'and also add a CompareValidator to perform a data type check. 
                    If strPrefix = "txt" Then
                        If TypeOf ctl Is WebControls.TextBox Then
                            If dr("FldType") = "Varchar" Or dr("FldType") = "Char" Then
                                CType(ctl, WebControls.TextBox).MaxLength = CInt(dr("FldLen"))
                            Else
                                CType(ctl, WebControls.TextBox).MaxLength = 50
                            End If
                        End If

                        Dim cmv As New WebControls.CompareValidator
                        With cmv
                            .ID = "cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                            .ControlToValidate = strControl
                            .Display = WebControls.ValidatorDisplay.None
                            .ErrorMessage = "Incorrect data type for " & dr("Caption").ToString()
                            .[Operator] = WebControls.ValidationCompareOperator.DataTypeCheck
                            .Type = GetValidatorType(dr("FldType"))
                        End With
                        ctl2.Controls.Add(cmv)

                    End If 'Set MaxLength for TextBox control


                Next


            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                If Not ex.InnerException Is Nothing Then
                    Throw New BaseException("Error required fields info for ResourceId " & ResourceId & " - " & ex.InnerException.Message)
                Else
                    Throw New BaseException("Error required fields info for ResourceId " & ResourceId & " - " & ex.Message)
                End If

            End Try

        End Sub
        Public Function PopulatePage(ByVal pgContentPlaceHolder As ContentPlaceHolder, ByVal strGuid As String) As DataTable
            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim db As New DataAccess
            Dim da As OleDb.OleDbDataAdapter
            ' Dim strSQL As String
            Dim dr As DataRow '', dr2 
            Dim col As DataColumn
            Dim dtResFlds As DataTable
            Dim dtDefs As DataTable
            Dim dtFldsMask As DataTable
            Dim strColName As String
            Dim strPrefix As String
            Dim strControl As String
            Dim ctl As Control
            Dim arrSQL As New ArrayList
            Dim strItem As String
            Dim intCounter As Integer
            Dim strParamName As String
            Dim strTableName As String
            Dim dt As DataTable
            Dim sb As New System.Text.StringBuilder
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim ResourceId As Integer

            Dim boolInternationalPhone As Boolean
            Dim boolInternationalFax As Boolean
            Dim boolInternationalZip As Boolean

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)
            dtDefs = CType(m_Context.Items("DDLDefs"), DataTable)
            dtFldsMask = CType(m_Context.Items("FldsMask"), DataTable)

            'Make the FldName column in the dtResFlds datatable the primary key
            With dtResFlds
                .PrimaryKey = New DataColumn() {.Columns("FldName")}
            End With

            Try
                arrSQL = BuildSQLSelect()
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConString"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.OpenConnection()

                For Each strItem In arrSQL
                    intCounter += 1
                    strParamName = "param" & intCounter.ToString
                    strTableName = "table" & intCounter.ToString
                    db.AddParameter(strParamName, strGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    da = db.RunParamSQLDataAdapter(strItem)
                    da.Fill(ds, strTableName)
                    strText = strItem
                Next

                'Set FldId as the primary key in the dtFldsMask datatable
                With dtFldsMask
                    .PrimaryKey = New DataColumn() {.Columns("FldId")}
                End With
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                If ex.InnerException Is Nothing Then
                    Throw New BaseException(ex.Message)
                Else
                    Throw New BaseException(ex.Message & " " & ex.InnerException.Message)
                End If
            Finally
                db.CloseConnection()
                db.Dispose()
                db = Nothing
            End Try


            'Added by Balaji on 10/05/2005
            For Each dt In ds.Tables
                dr = dt.Rows(0)
                'We need to loop through the record and determine whether each field has
                'a textbox or a ddl on the pgContentPlaceHolder. We can get this information by searching
                'the dtResFlds datatable for the matching FldName
                For Each col In dt.Columns
                    intCounter += 1
                    strColName = col.ColumnName
                    'Check if the column has a textbox or ddl associated with it
                    Dim dr3 As DataRow
                    dr3 = dtResFlds.Rows.Find(strColName)
                    If dr3 Is Nothing Then
                        'Do nothing. This is a lookup field.
                    Else
                        If dr3("DDLId").ToString() = "" Then
                            If dr3("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else                            'Textbox
                                strPrefix = "txt"
                            End If

                        Else
                            'This will usually be a ddl. However, if we are on the page that sets up the
                            'ddl itself then it will be a textbox. If the ResouceId associated with the
                            'DDLId is the same as the ResourceId of the current page then it is a textbox.
                            'For example, on the page that sets up the states the StateId will be a textbox
                            'rather than a ddl. On other pages it will be a ddl.

                            drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                            intResourceId = drDDLDefs("DDLResourceId")
                            If intResourceId = ResourceId Then
                                'We are on the page that sets up the ddl
                                strPrefix = "txt"
                            Else
                                strPrefix = "ddl"

                            End If

                        End If
                        If Not dr3.IsNull("ControlName") Then
                            strControl = dr3("ControlName")
                            strPrefix = strControl.Substring(0, 3)
                        Else
                            strControl = strPrefix & strColName
                        End If

                        ctl = pgContentPlaceHolder.FindControl(strControl)

                        If LCase(strControl) = "chkforeignphone" Then
                            boolInternationalPhone = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignzip" Then
                            boolInternationalZip = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignfax" Then
                            boolInternationalFax = dr(col)
                        End If
                    End If
                Next
            Next



            'Each query returns one record for a specific object such as an employer.
            'We need to loop through each datatable in the dataset, retrieve each field
            'and populate the corresponding field on the pgContentPlaceHolder.
            For Each dt In ds.Tables
                dr = dt.Rows(0)
                'We need to loop through the record and determine whether each field has
                'a textbox or a ddl on the pgContentPlaceHolder. We can get this information by searching
                'the dtResFlds datatable for the matching FldName
                For Each col In dt.Columns
                    intCounter += 1
                    strColName = col.ColumnName
                    'Check if the column has a textbox or ddl associated with it
                    Dim dr3 As DataRow
                    dr3 = dtResFlds.Rows.Find(strColName)
                    If dr3 Is Nothing Then
                        'Do nothing. This is a lookup field.
                    Else
                        If dr3("DDLId").ToString() = "" Then
                            If dr3("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else                            'Textbox
                                strPrefix = "txt"
                            End If

                        Else
                            'This will usually be a ddl. However, if we are on the page that sets up the
                            'ddl itself then it will be a textbox. If the ResouceId associated with the
                            'DDLId is the same as the ResourceId of the current page then it is a textbox.
                            'For example, on the page that sets up the states the StateId will be a textbox
                            'rather than a ddl. On other pages it will be a ddl.

                            drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                            intResourceId = drDDLDefs("DDLResourceId")
                            If intResourceId = ResourceId Then
                                'We are on the page that sets up the ddl
                                strPrefix = "txt"
                            Else
                                strPrefix = "ddl"

                            End If

                        End If
                        If Not dr3.IsNull("ControlName") Then
                            strControl = dr3("ControlName")
                            strPrefix = strControl.Substring(0, 3)
                        Else
                            strControl = strPrefix & strColName
                        End If

                        ctl = pgContentPlaceHolder.FindControl(strControl)

                        If LCase(strControl) = "chkforeignphone" Then
                            boolInternationalPhone = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignzip" Then
                            boolInternationalZip = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignfax" Then
                            boolInternationalFax = dr(col)
                        End If

                        If strPrefix = "txt" Then
                            'If it is a datetime field there is no need to check if
                            'there is an input mask. 
                            If dr3("FldType") = "Datetime" Then
                                If Not dr.IsNull(col) Then
                                    If (dr(col).ToString() = "12:00:00 AM" Or Year(dr(col).ToString()) = "1899") Then
                                        CType(ctl, TextBox).Text = ""
                                    Else
                                        CType(ctl, TextBox).Text = dr(col).toshortdatestring() '- Changed by Bhavana N.
                                    End If

                                End If
                            Else
                                'We need to check if the field has an input mask.
                                'In order to do this we first need to get the fldid.
                                'We can then use it to lookup any input mask in the
                                'FldsMask datatable
                                Dim intFldId As Integer
                                intFldId = dr3("FldId")
                                Dim dr4 As DataRow
                                dr4 = dtFldsMask.Rows.Find(intFldId)
                                If dr4 Is Nothing Then
                                    'There is no input mask for this field
                                    Try
                                        If Not TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            CType(ctl, TextBox).Text = dr(col).ToString()
                                        Else

                                            If InStr(strColName, "Phone") > 0 Then
                                                CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text = dr(col).ToString()
                                                If boolInternationalPhone = True Then
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Mask = "aaaaaaaaaaaaaaaaaaaa"
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).DisplayMask = ""
                                                Else
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Mask = "(###)-###-####"
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).DisplayMask = "(###) ###-####"
                                                End If
                                            End If

                                            If InStr(strColName, "Zip") > 0 Then
                                                CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text = dr(col).ToString()
                                                If boolInternationalZip = True Then
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Mask = ""
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).DisplayMask = ""
                                                Else
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Mask = "#####"
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).DisplayMask = "#####"
                                                End If
                                            End If

                                            If InStr(strColName, "Fax") > 0 Then
                                                CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text = dr(col).ToString()
                                                If boolInternationalFax = True Then
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Mask = "aaaaaaaaaaaaaaaaaaaa"
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).DisplayMask = ""
                                                Else
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Mask = "(###)-###-####"
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).DisplayMask = "(###) ###-####"
                                                End If
                                            End If
                                            If InStr(strColName, "CmsId") > 0 Then
                                                If not String.IsNullOrEmpty(dr(col).ToString())
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text = dr(col).ToString()
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Mask = "####-##"
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).DisplayMask = "####-##"
                                                else
                                                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text = ""
                                                End If
                                            End If
                                        End If

                                    Catch ex As System.Exception
                                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                                    	exTracker.TrackExceptionWrapper(ex)

                                        Throw New System.Exception("Could not find textbox:" & strControl)
                                    End Try

                                Else
                                    Dim strMask As String = dr4("Mask")
                                    Dim strFormatted As String
                                    strFormatted = ApplyMask(strMask, dr(col).ToString())
                                    If Not TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        CType(ctl, TextBox).Text = strFormatted
                                        'Else
                                        '    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text = strFormatted
                                    End If

                                End If

                                If Not TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                    If (strColName.ToString.IndexOf("PhoneType") = -1) Then
                                        If Mid(strControl, 1, 3) = "txt" And (strColName.ToString.IndexOf("Phone") <> -1) Then
                                            Dim facInputMasks As New InputMasksFacade
                                            Dim strMask As String
                                            ' Dim zipMask As String
                                            strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)


                                            '   Dim strMask As String = "(###)-###-####"
                                            Dim strFormatted As String
                                            If strControl.Length > 1 And boolInternationalPhone = False Then
                                                strFormatted = ApplyMask(strMask, dr(col).ToString())
                                                CType(ctl, TextBox).Text = strFormatted
                                            Else
                                                CType(ctl, TextBox).Text = dr(col).ToString()
                                            End If
                                        End If
                                    End If
                                End If

                                If Not TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                    If Mid(strControl, 1, 3) = "txt" And (strColName.ToString.IndexOf("Fax") <> -1) Then
                                        Dim facInputMasks As New InputMasksFacade
                                        Dim strMask As String
                                        ' Dim zipMask As String

                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                        'Dim strMask As String = "(###)-###-####"
                                        Dim strFormatted As String
                                        If strControl.Length > 1 And boolInternationalFax = False Then
                                            strFormatted = ApplyMask(strMask, dr(col).ToString())
                                            CType(ctl, TextBox).Text = strFormatted
                                        Else
                                            CType(ctl, TextBox).Text = dr(col).ToString()
                                        End If
                                    End If
                                End If

                                If Not TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                    If Mid(strControl, 1, 3) = "txt" And (strColName.ToString.IndexOf("Zip") <> -1) Then
                                        Dim facInputMasks As New InputMasksFacade
                                        Dim strMask As String
                                        'Dim zipMask As String

                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

                                        'Dim strMask As String = "#####"
                                        Dim strFormatted As String
                                        If strControl.Length > 1 And boolInternationalZip = False Then
                                            strFormatted = ApplyMask(strMask, dr(col).ToString())
                                            CType(ctl, TextBox).Text = strFormatted
                                        Else
                                            CType(ctl, TextBox).Text = dr(col).ToString()
                                        End If
                                    End If
                                End If

                            End If

                        ElseIf strPrefix = "chk" Then
                            If Not dr.IsNull(col) Then
                                If dr(col) = True Then
                                    CType(ctl, CheckBox).Checked = True
                                Else
                                    CType(ctl, CheckBox).Checked = False
                                End If
                            Else
                                CType(ctl, CheckBox).Checked = False
                            End If
                        ElseIf strPrefix = "ddl" Then
                            'we want to select the appropriate value in the relevant ddl
                            SelValInDDL(ctl, dr(col).ToString())
                        End If

                    End If

                Next
            Next

            Return dtResFlds
        End Function
        Public Function PopulatePage(ByVal Form As HtmlForm, ByVal strGuid As String) As DataTable
            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim db As New DataAccess
            Dim da As OleDb.OleDbDataAdapter
            '   Dim strSQL As String
            Dim dr As DataRow '', dr2
            Dim col As DataColumn
            Dim dtResFlds As DataTable
            Dim dtDefs As DataTable
            Dim dtFldsMask As DataTable
            Dim strColName As String
            Dim strPrefix As String
            Dim strControl As String
            Dim ctl As Control
            Dim arrSQL As New ArrayList
            Dim strItem As String
            Dim intCounter As Integer
            Dim strParamName As String
            Dim strTableName As String
            Dim dt As DataTable
            Dim sb As New System.Text.StringBuilder
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim ResourceId As Integer

            Dim boolInternationalPhone As Boolean
            Dim boolInternationalFax As Boolean
            Dim boolInternationalZip As Boolean

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)
            dtDefs = CType(m_Context.Items("DDLDefs"), DataTable)
            dtFldsMask = CType(m_Context.Items("FldsMask"), DataTable)

            'Make the FldName column in the dtResFlds datatable the primary key
            With dtResFlds
                .PrimaryKey = New DataColumn() {.Columns("FldName")}
            End With

            Try
                arrSQL = BuildSQLSelect()
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConString"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.OpenConnection()

                For Each strItem In arrSQL
                    intCounter += 1
                    strParamName = "param" & intCounter.ToString
                    strTableName = "table" & intCounter.ToString
                    db.AddParameter(strParamName, strGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    da = db.RunParamSQLDataAdapter(strItem)
                    da.Fill(ds, strTableName)
                    strText = strItem
                Next

                'Set FldId as the primary key in the dtFldsMask datatable
                With dtFldsMask
                    .PrimaryKey = New DataColumn() {.Columns("FldId")}
                End With
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                If ex.InnerException Is Nothing Then
                    Throw New BaseException(ex.Message)
                Else
                    Throw New BaseException(ex.Message & " " & ex.InnerException.Message)
                End If
            Finally
                db.CloseConnection()
                db.Dispose()
                db = Nothing
            End Try


            'Added by Balaji on 10/05/2005
            For Each dt In ds.Tables
                dr = dt.Rows(0)
                'We need to loop through the record and determine whether each field has
                'a textbox or a ddl on the form. We can get this information by searching
                'the dtResFlds datatable for the matching FldName
                For Each col In dt.Columns
                    intCounter += 1
                    strColName = col.ColumnName
                    'Check if the column has a textbox or ddl associated with it
                    Dim dr3 As DataRow
                    dr3 = dtResFlds.Rows.Find(strColName)
                    If dr3 Is Nothing Then
                        'Do nothing. This is a lookup field.
                    Else
                        If dr3("DDLId").ToString() = "" Then
                            If dr3("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else                            'Textbox
                                strPrefix = "txt"
                            End If

                        Else
                            'This will usually be a ddl. However, if we are on the page that sets up the
                            'ddl itself then it will be a textbox. If the ResouceId associated with the
                            'DDLId is the same as the ResourceId of the current page then it is a textbox.
                            'For example, on the page that sets up the states the StateId will be a textbox
                            'rather than a ddl. On other pages it will be a ddl.

                            drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                            intResourceId = drDDLDefs("DDLResourceId")
                            If intResourceId = ResourceId Then
                                'We are on the page that sets up the ddl
                                strPrefix = "txt"
                            Else
                                strPrefix = "ddl"

                            End If

                        End If
                        If Not dr3.IsNull("ControlName") Then
                            strControl = dr3("ControlName")
                            strPrefix = strControl.Substring(0, 3)
                        Else
                            strControl = strPrefix & strColName
                        End If

                        ctl = Form.FindControl(strControl)

                        If LCase(strControl) = "chkforeignphone" Then
                            boolInternationalPhone = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignzip" Then
                            boolInternationalZip = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignfax" Then
                            boolInternationalFax = dr(col)
                        End If
                    End If
                Next
            Next



            'Each query returns one record for a specific object such as an employer.
            'We need to loop through each datatable in the dataset, retrieve each field
            'and populate the corresponding field on the form.
            For Each dt In ds.Tables
                dr = dt.Rows(0)
                'We need to loop through the record and determine whether each field has
                'a textbox or a ddl on the form. We can get this information by searching
                'the dtResFlds datatable for the matching FldName
                For Each col In dt.Columns
                    intCounter += 1
                    strColName = col.ColumnName
                    'Check if the column has a textbox or ddl associated with it
                    Dim dr3 As DataRow
                    dr3 = dtResFlds.Rows.Find(strColName)
                    If dr3 Is Nothing Then
                        'Do nothing. This is a lookup field.
                    Else
                        If dr3("DDLId").ToString() = "" Then
                            If dr3("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else                            'Textbox
                                strPrefix = "txt"
                            End If

                        Else
                            'This will usually be a ddl. However, if we are on the page that sets up the
                            'ddl itself then it will be a textbox. If the ResouceId associated with the
                            'DDLId is the same as the ResourceId of the current page then it is a textbox.
                            'For example, on the page that sets up the states the StateId will be a textbox
                            'rather than a ddl. On other pages it will be a ddl.

                            drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                            intResourceId = drDDLDefs("DDLResourceId")
                            If intResourceId = ResourceId Then
                                'We are on the page that sets up the ddl
                                strPrefix = "txt"
                            Else
                                strPrefix = "ddl"

                            End If

                        End If
                        If Not dr3.IsNull("ControlName") Then
                            strControl = dr3("ControlName")
                            strPrefix = strControl.Substring(0, 3)
                        Else
                            strControl = strPrefix & strColName
                        End If

                        ctl = Form.FindControl(strControl)

                        If LCase(strControl) = "chkforeignphone" Then
                            boolInternationalPhone = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignzip" Then
                            boolInternationalZip = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignfax" Then
                            boolInternationalFax = dr(col)
                        End If

                        If strPrefix = "txt" Then
                            'If it is a datetime field there is no need to check if
                            'there is an input mask. 
                            If dr3("FldType") = "Datetime" Then
                                If Not dr.IsNull(col) Then
                                    If (dr(col).ToString() = "12:00:00 AM" Or Year(dr(col).ToString()) = "1899") Then
                                        CType(ctl, TextBox).Text = ""
                                    Else
                                        CType(ctl, TextBox).Text = dr(col).toshortdatestring() '- Changed by Bhavana N.
                                    End If

                                End If
                            Else
                                'We need to check if the field has an input mask.
                                'In order to do this we first need to get the fldid.
                                'We can then use it to lookup any input mask in the
                                'FldsMask datatable
                                Dim intFldId As Integer
                                intFldId = dr3("FldId")
                                Dim dr4 As DataRow
                                dr4 = dtFldsMask.Rows.Find(intFldId)

                                If dr4 Is Nothing Then
                                    'There is no input mask for this field
                                    Try
                                        CType(ctl, TextBox).Text = dr(col).ToString()
                                    Catch ex As System.Exception
                                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                                    	exTracker.TrackExceptionWrapper(ex)

                                        Throw New System.Exception("Could not find textbox:" & strControl)
                                    End Try

                                Else
                                    Dim strMask As String = dr4("Mask")
                                    Dim strFormatted As String
                                    strFormatted = ApplyMask(strMask, dr(col).ToString())
                                    CType(ctl, TextBox).Text = strFormatted
                                End If

                                If (strColName.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (strColName.ToString.IndexOf("Phone") <> -1) Then
                                        Dim facInputMasks As New InputMasksFacade
                                        Dim strMask As String
                                        '  Dim zipMask As String
                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)


                                        '   Dim strMask As String = "(###)-###-####"
                                        Dim strFormatted As String
                                        If strControl.Length > 1 And boolInternationalPhone = False Then
                                            strFormatted = ApplyMask(strMask, dr(col).ToString())
                                            CType(ctl, TextBox).Text = strFormatted
                                        Else
                                            CType(ctl, TextBox).Text = dr(col).ToString()
                                        End If
                                    End If
                                End If

                                If Mid(strControl, 1, 3) = "txt" And (strColName.ToString.IndexOf("Fax") <> -1) Then
                                    Dim facInputMasks As New InputMasksFacade
                                    Dim strMask As String
                                    '  Dim zipMask As String

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                    'Dim strMask As String = "(###)-###-####"
                                    Dim strFormatted As String
                                    If strControl.Length > 1 And boolInternationalFax = False Then
                                        strFormatted = ApplyMask(strMask, dr(col).ToString())
                                        CType(ctl, TextBox).Text = strFormatted
                                    Else
                                        CType(ctl, TextBox).Text = dr(col).ToString()
                                    End If
                                End If

                                If Mid(strControl, 1, 3) = "txt" And (strColName.ToString.IndexOf("Zip") <> -1) Then
                                    Dim facInputMasks As New InputMasksFacade
                                    Dim strMask As String
                                    'Dim zipMask As String

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

                                    'Dim strMask As String = "#####"
                                    Dim strFormatted As String
                                    If strControl.Length > 1 And boolInternationalZip = False Then
                                        strFormatted = ApplyMask(strMask, dr(col).ToString())
                                        CType(ctl, TextBox).Text = strFormatted
                                    Else
                                        CType(ctl, TextBox).Text = dr(col).ToString()
                                    End If
                                End If
                            End If

                        ElseIf strPrefix = "chk" Then
                            If Not dr.IsNull(col) Then
                                If dr(col) = True Then
                                    CType(ctl, CheckBox).Checked = True
                                Else
                                    CType(ctl, CheckBox).Checked = False
                                End If
                            Else
                                CType(ctl, CheckBox).Checked = False
                            End If
                        ElseIf strPrefix = "ddl" Then
                            'we want to select the appropriate value in the relevant ddl
                            SelValInDDL(ctl, dr(col).ToString())
                        End If

                    End If

                Next
            Next

            Return dtResFlds
        End Function

        Public Function PopulatePage1(ByVal pgContentPlaceHolder As ContentPlaceHolder, ByVal strGuid As String) As DataTable
            Dim ds As New DataSet
            'Dim ds2 As New DataSet
            Dim db As New DataAccess
            Dim da As OleDbDataAdapter
            '  Dim strSQL As String
            Dim dr As DataRow '', dr2
            Dim col As DataColumn
            Dim dtResFlds As DataTable
            Dim dtDefs As DataTable
            Dim dtFldsMask As DataTable
            Dim strColName As String
            Dim strPrefix As String
            Dim strControl As String
            Dim ctl As Control
            Dim arrSQL As ArrayList
            Dim strItem As String
            Dim intCounter As Integer
            Dim strParamName As String
            Dim strTableName As String
            Dim dt As DataTable
            'Dim sb As New System.Text.StringBuilder
            Dim drDDLDefs As DataRow = Nothing
            Dim intResourceId As Integer
            Dim ResourceId As Integer

            Dim boolInternationalPhone As Boolean
            Dim boolInternationalFax As Boolean
            Dim boolInternationalZip As Boolean

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)
            dtDefs = CType(m_Context.Items("DDLDefs"), DataTable)
            dtFldsMask = CType(m_Context.Items("FldsMask"), DataTable)

            'Make the FldName column in the dtResFlds datatable the primary key
            With dtResFlds
                .PrimaryKey = New DataColumn() {.Columns("FldName")}
            End With

            Try
                arrSQL = BuildSQLSelect()
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConString"
                db.ConnectionString = myAdvAppSettings.AppSettings("ConString")
                db.OpenConnection()

                For Each strItem In arrSQL
                    intCounter += 1
                    strParamName = "param" & intCounter.ToString
                    strTableName = "table" & intCounter.ToString
                    db.AddParameter(strParamName, strGuid, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                    da = db.RunParamSQLDataAdapter(strItem)
                    da.Fill(ds, strTableName)
                    strText = strItem
                Next

                'Set FldId as the primary key in the dtFldsMask datatable
                With dtFldsMask
                    .PrimaryKey = New DataColumn() {.Columns("FldId")}
                End With
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                If ex.InnerException Is Nothing Then
                    Throw New BaseException(ex.Message)
                Else
                    Throw New BaseException(ex.Message & " " & ex.InnerException.Message)
                End If
            Finally
                db.CloseConnection()
                db.Dispose()
                db = Nothing
            End Try


            'Added by Balaji on 10/05/2005
            For Each dt In ds.Tables
                dr = dt.Rows(0)
                'We need to loop through the record and determine whether each field has
                'a textbox or a ddl on the form. We can get this information by searching
                'the dtResFlds datatable for the matching FldName
                For Each col In dt.Columns
                    intCounter += 1
                    strColName = col.ColumnName
                    'Check if the column has a textbox or ddl associated with it
                    Dim dr3 As DataRow
                    dr3 = dtResFlds.Rows.Find(strColName)
                    If dr3 Is Nothing Then
                        'Do nothing. This is a lookup field.
                    Else
                        If dr3("DDLId").ToString() = "" Then
                            If dr3("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else                            'Textbox
                                strPrefix = "txt"
                            End If

                        Else
                            'This will usually be a ddl. However, if we are on the page that sets up the
                            'ddl itself then it will be a textbox. If the ResouceId associated with the
                            'DDLId is the same as the ResourceId of the current page then it is a textbox.
                            'For example, on the page that sets up the states the StateId will be a textbox
                            'rather than a ddl. On other pages it will be a ddl.

                            drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                            intResourceId = drDDLDefs("DDLResourceId")
                            If intResourceId = ResourceId Then
                                'We are on the page that sets up the ddl
                                strPrefix = "txt"
                            Else
                                strPrefix = "ddl"

                            End If

                        End If
                        If Not dr3.IsNull("ControlName") Then
                            strControl = dr3("ControlName")
                            strPrefix = strControl.Substring(0, 3)
                        Else
                            strControl = strPrefix & strColName
                        End If

                        ctl = pgContentPlaceHolder.FindControl(strControl)

                        If LCase(strControl) = "chkforeignphone" Then
                            boolInternationalPhone = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignzip" Then
                            boolInternationalZip = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignfax" Then
                            boolInternationalFax = dr(col)
                        End If
                    End If
                Next
            Next



            'Each query returns one record for a specific object such as an employer.
            'We need to loop through each datatable in the dataset, retrieve each field
            'and populate the corresponding field on the form.
            For Each dt In ds.Tables
                dr = dt.Rows(0)
                'We need to loop through the record and determine whether each field has
                'a textbox or a ddl on the form. We can get this information by searching
                'the dtResFlds datatable for the matching FldName
                For Each col In dt.Columns
                    intCounter += 1
                    strColName = col.ColumnName
                    'Check if the column has a textbox or ddl associated with it
                    Dim dr3 As DataRow
                    dr3 = dtResFlds.Rows.Find(strColName)
                    If dr3 Is Nothing Then
                        'Do nothing. This is a lookup field.
                    Else
                        If dr3("DDLId").ToString() = "" Then
                            If dr3("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else                            'Textbox
                                strPrefix = "txt"
                            End If

                        Else
                            'This will usually be a ddl. However, if we are on the page that sets up the
                            'ddl itself then it will be a textbox. If the ResouceId associated with the
                            'DDLId is the same as the ResourceId of the current page then it is a textbox.
                            'For example, on the page that sets up the states the StateId will be a textbox
                            'rather than a ddl. On other pages it will be a ddl.

                            drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                            intResourceId = drDDLDefs("DDLResourceId")
                            If intResourceId = ResourceId Then
                                'We are on the page that sets up the ddl
                                strPrefix = "txt"
                            Else
                                strPrefix = "ddl"

                            End If

                        End If
                        If Not dr3.IsNull("ControlName") Then
                            strControl = dr3("ControlName")
                            strPrefix = strControl.Substring(0, 3)
                        Else
                            strControl = strPrefix & strColName
                        End If

                        ctl = pgContentPlaceHolder.FindControl(strControl)

                        If LCase(strControl) = "chkforeignphone" Then
                            boolInternationalPhone = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignzip" Then
                            boolInternationalZip = dr(col)
                        End If

                        If LCase(strControl) = "chkforeignfax" Then
                            boolInternationalFax = dr(col)
                        End If

                        If strPrefix = "txt" Then
                            'If it is a datetime field there is no need to check if
                            'there is an input mask. 
                            If dr3("FldType") = "Datetime" Then
                                If Not dr.IsNull(col) Then
                                    If (dr(col).ToString() = "12:00:00 AM" Or Year(dr(col).ToString()) = "1899") Then
                                        CType(ctl, Telerik.Web.UI.RadDatePicker).SelectedDate = CDate("1/1/1945")
                                    Else
                                        Try
                                            CType(ctl, Telerik.Web.UI.RadDatePicker).SelectedDate = CDate(dr(col).toshortdatestring())
                                        Catch ex As System.Exception
                                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                                        	exTracker.TrackExceptionWrapper(ex)

                                            CType(ctl, Telerik.Web.UI.RadDatePicker).SelectedDate = CDate("1/1/1945")
                                        End Try
                                    End If
                                End If
                            Else
                                'We need to check if the field has an input mask.
                                'In order to do this we first need to get the fldid.
                                'We can then use it to lookup any input mask in the
                                'FldsMask datatable
                                Dim intFldId As Integer
                                intFldId = dr3("FldId")
                                Dim dr4 As DataRow
                                dr4 = dtFldsMask.Rows.Find(intFldId)

                                If dr4 Is Nothing Then
                                    'There is no input mask for this field
                                    Try
                                        CType(ctl, WebControls.TextBox).Text = dr(col).ToString()
                                    Catch ex As System.Exception
                                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                                    	exTracker.TrackExceptionWrapper(ex)

                                        Throw New System.Exception("Could not find textbox:" & strControl)
                                    End Try

                                Else
                                    Dim strMask As String = dr4("Mask")
                                    Dim strFormatted As String
                                    strFormatted = ApplyMask(strMask, dr(col).ToString())
                                    CType(ctl, WebControls.TextBox).Text = strFormatted
                                End If

                                If (strColName.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (strColName.ToString.IndexOf("Phone") <> -1) Then
                                        Dim facInputMasks As New InputMasksFacade
                                        Dim strMask As String
                                        '  Dim zipMask As String
                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)


                                        '   Dim strMask As String = "(###)-###-####"
                                        Dim strFormatted As String
                                        If strControl.Length > 1 And boolInternationalPhone = False Then
                                            strFormatted = ApplyMask(strMask, dr(col).ToString())
                                            CType(ctl, TextBox).Text = strFormatted
                                        Else
                                            CType(ctl, TextBox).Text = dr(col).ToString()
                                        End If
                                    End If
                                End If

                                If Mid(strControl, 1, 3) = "txt" And (strColName.ToString.IndexOf("Fax") <> -1) Then
                                    Dim facInputMasks As New InputMasksFacade
                                    Dim strMask As String
                                    ' Dim zipMask As String

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                    'Dim strMask As String = "(###)-###-####"
                                    Dim strFormatted As String
                                    If strControl.Length > 1 And boolInternationalFax = False Then
                                        strFormatted = ApplyMask(strMask, dr(col).ToString())
                                        CType(ctl, TextBox).Text = strFormatted
                                    Else
                                        CType(ctl, TextBox).Text = dr(col).ToString()
                                    End If
                                End If

                                If Mid(strControl, 1, 3) = "txt" And (strColName.ToString.IndexOf("Zip") <> -1) Then
                                    Dim facInputMasks As New InputMasksFacade
                                    Dim strMask As String
                                    'Dim zipMask As String

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

                                    'Dim strMask As String = "#####"
                                    Dim strFormatted As String
                                    If strControl.Length > 1 And boolInternationalZip = False Then
                                        strFormatted = ApplyMask(strMask, dr(col).ToString())
                                        CType(ctl, TextBox).Text = strFormatted
                                    Else
                                        CType(ctl, TextBox).Text = dr(col).ToString()
                                    End If
                                End If
                            End If

                        ElseIf strPrefix = "chk" Then
                            If Not dr.IsNull(col) Then
                                If dr(col) = True Then
                                    CType(ctl, CheckBox).Checked = True
                                Else
                                    CType(ctl, CheckBox).Checked = False
                                End If
                            Else
                                CType(ctl, CheckBox).Checked = False
                            End If
                        ElseIf strPrefix = "ddl" Then
                            'we want to select the appropriate value in the relevant ddl
                            SelValInDDL1(ctl, dr(col).ToString(), drDDLDefs("TblName"), drDDLDefs("DispText"), drDDLDefs("DispValue"))
                        End If

                    End If

                Next
            Next

            Return dtResFlds
        End Function

        Public Function DoInsert(ByVal pgContentPlaceHolder As ContentPlaceHolder, Optional ByVal InternationalPhone As Boolean = False, Optional ByVal InternationalFax As Boolean = False, Optional ByVal InternationalZip As Boolean = False) As String
            '**************************************************************************************************
            'Purpose:       Uses the data dictionary to construct sql Insert statements for the current page
            '               and execute them. It relies on three datatables added to the current httpcontext
            '               items collection by the PageSetup sub.
            'Parameters:
            '[HtmlForm]     The form for which the insert is to be performed
            'Returns:       NA
            'Created:       Troy Richards, 7/10/2003
            '**************************************************************************************************
            Dim dtResFlds As DataTable
            Dim dtDefs As DataTable
            Dim dtFldsMask As DataTable
            Dim dtTables As New DataTable
            Dim dr1, dr3 As DataRow
            Dim strPrefix As String = ""
            'Dim intDDLCounter As Integer = 2
            Dim strTable As String
            'Dim intPK As Integer
            Dim strSQL As String
            Dim arrSQL As New ArrayList
            'Dim intItem As Integer
            'Dim strInClause As String
            'Dim db As New DataAccess
            Dim db2 As New DataAccess
            'Dim ds As New DataSet
            Dim strFields As String = ""
            Dim strMarkers As String = ""
            Dim intCounter As Integer
            Dim strParamName As String = String.Empty
            'Dim intLen As Integer
            Dim strControl As String
            Dim ctl As Control
            Dim strValue As String = ""
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim ResourceId As Integer
            'Dim msg As String
            Dim controlName As String

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = CType(m_Context.Items("ResourceId"), Integer)

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)
            dtDefs = CType(m_Context.Items("DDLDefs"), DataTable)
            dtFldsMask = CType(m_Context.Items("FldsMask"), DataTable)

            'Make the DDLId column the primary key for the dtDefs datatable
            With dtDefs
                .PrimaryKey = New DataColumn() {.Columns("DDLId")}
            End With

            'Add a column called TblName and another called TblPK to the dtTables datatable
            'Dim col1 As DataColumn = dtTables.Columns.Add("TblName")
            'Dim col2 As DataColumn = dtTables.Columns.Add("TblPK", GetType(Integer))
            dtTables.Columns.Add("TblName")
            dtTables.Columns.Add("TblPK", GetType(Integer))
            dtTables.PrimaryKey = New DataColumn() {dtTables.Columns("TblName")}
            'Make the TblName column the primary key
            'With dtTables
            '    .PrimaryKey = New DataColumn() {.Columns("TblName")}
            'End With

            'Get DISTINCT tables that are used on the page. Loop through dtResFlds and add the
            'TblName from it to dtTables if it does not exist there already.
            For Each dr1 In dtResFlds.Rows
                If dtTables.Rows.Count = 0 Or Not ValExistsInDt(dtTables, dr1("TblName")) Then
                    Dim dr2 As DataRow = dtTables.NewRow
                    dr2("TblName") = dr1("TblName")
                    dr2("TblPK") = dr1("TblPK")
                    dtTables.Rows.Add(dr2)
                End If
            Next

            'Add dtTables to the items collection of the httpcontext object.
            m_Context.Items.Add("Tables", dtTables)

            'Reset the connection string to the regular database
            'Changed by Corey Masson Dec 30, 2003
            'SingletonAppSettings.AppSettings("ConString")
            'db2.ConnectionString = "ConString"
            db2.ConnectionString = CType(myAdvAppSettings.AppSettings("ConString"), String)
            'Loop through the dtTables datatable.
            For Each dr1 In dtTables.Rows
                'Find the matching rows in dtResFlds that belong to the current table.
                Dim aRows As DataRow()
                strTable = CType(dr1("TblName"), String)
                aRows = dtResFlds.Select("TblName = '" & strTable & "'")
                For Each dr3 In aRows
                    'Add a parameter for each field
                    strParamName = "param" & intCounter.ToString
                    If dr3.IsNull("DDLId") Then
                        If dr3("FldType") = "Bit" Then 'Checkbox
                            strControl = "chk" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            If CType(ctl, CheckBox).Checked = True Then
                                strValue = "1"
                            Else
                                strValue = "0"
                            End If
                        Else                            'Textbox
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            'If it is a datetime field there is no need to check if
                            'there is an input mask.
                            If dr3("FldType") = "Datetime" Then
                                If TypeOf (ctl) Is Telerik.Web.UI.RadDatePicker Then
                                    strValue = CType(ctl, Telerik.Web.UI.RadDatePicker).SelectedDate
                                Else
                                    strValue = CType(ctl, TextBox).Text.ToString
                                End If


                            Else
                                'We need to check if the field has an input mask.
                                'In order to do this we first need to get the fldid.
                                'We can then use it to lookup any input mask in the
                                'FldsMask datatable
                                Dim intFldId As Integer
                                intFldId = CType(dr3("FldId"), Integer)
                                Dim dr5 As DataRow
                                dr5 = dtFldsMask.Rows.Find(intFldId)



                                If dr5 Is Nothing Then
                                    'There is no input mask for this field
                                    If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                    Else
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    End If
                                Else
                                    Dim strMask As String
                                    Dim strVal As String
                                    strMask = dr5("Mask")
                                    strVal = CType(ctl, TextBox).Text.ToString
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    End If
                                End If
                                'Modified on 2/25/2005
                                'If intFldId = 99 Then

                                'For Phone
                                If (strControl.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Phone") <> -1) Then
                                        Dim strMask As String
                                        Dim strVal As String
                                        Dim facInputMasks As New InputMasksFacade
                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                        'strMask = "(###)-###-####"
                                        If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            strVal = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        Else
                                            strVal = CType(ctl, TextBox).Text.ToString
                                        End If
                                        'strVal = CType(ctl, TextBox).Text.ToString
                                        '   check also that strVal is not empty. Added by Anatoly 2/11/2004


                                        If Not TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            If strVal.Length > 0 And InternationalPhone = False Then
                                                strValue = RemoveMask(strMask, strVal)
                                            Else
                                                strValue = CType(ctl, TextBox).Text.ToString
                                            End If
                                        Else
                                            strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        End If

                                    End If
                                End If

                                'For Fax
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Fax") <> -1) Then
                                    Dim strMask As String
                                    Dim strVal As String
                                    Dim facInputMasks As New InputMasksFacade

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
                                    'strMask = "(###)-###-####"

                                    strVal = CType(ctl, TextBox).Text.ToString

                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 And InternationalFax = False Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    End If
                                End If

                                'For Zip
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Zip") <> -1) Then
                                    Dim strMask As String
                                    Dim strVal As String
                                    Dim facInputMasks As New InputMasksFacade

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

                                    'strMask = "#####"
                                    If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        strVal = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                    Else
                                        strVal = CType(ctl, TextBox).Text.ToString
                                    End If
                                    'strVal = CType(ctl, TextBox).Text.ToString
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 And InternationalZip = False Then
                                        strValue = RemoveMask(strMask, strVal)
                                        'Else
                                        'If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        '    strVal = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        'Else
                                        '    strVal = CType(ctl, TextBox).Text.ToString
                                        'End If
                                    End If
                                End If

                            End If

                            'If we are dealing with the primary key we need
                            'to generate a guid for it.
                            If dr3("FldId").ToString() = dr1("TblPK").ToString() Then
                                strValue = m_PK 'changed to fix refresh problem. #BN 07/09/04 #
                                'm_PK = strValue #BN 07/09/04 #
                            End If
                        End If

                    Else
                        'This will usually be a ddl. However, if we are on the page that sets up the
                        'ddl itself then it will be a textbox. If the ResouceId associated with the
                        'DDLId is the same as the ResourceId of the current page then it is a textbox.
                        'For example, on the page that sets up the states the StateId will be a textbox
                        'rather than a ddl. On other pages it will be a ddl.
                        drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                        intResourceId = CType(drDDLDefs("DDLResourceId"), Integer)
                        If Not dr3.IsNull("ControlName") Then
                            controlName = dr3("ControlName").ToString
                        Else
                            controlName = ""
                        End If
                        If intResourceId = ResourceId Then
                            'We are on the page that sets up the ddl
                            strPrefix = "txt"
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            strValue = CType(ctl, TextBox).Text.ToString
                            'If we are dealing with the primary key we need
                            'to generate a guid for it.
                            If dr3("FldId").ToString() = dr1("TblPK").ToString() Then
                                ' Commented out by Corey Masson July 29, 2004 - We now create the GUID in the UI
                                'strValue = Guid.NewGuid.ToString()
                                'm_PK = strValue
                                strValue = m_PK
                            End If
                        ElseIf controlName <> "" Then
                            If controlName.Substring(0, 3) = "txt" Then
                                strControl = CType(dr3("ControlName"), String)
                                ctl = pgContentPlaceHolder.FindControl(strControl)
                                strValue = CType(ctl, TextBox).Text.ToString
                                'If we are dealing with the primary key we need
                                'to generate a guid for it.
                                If dr3("FldId").ToString() = dr1("TblPK").ToString() Then
                                    ' Commented out by Corey Masson July 29, 2004 - We now create the GUID in the UI
                                    'strValue = Guid.NewGuid.ToString()
                                    'm_PK = strValue
                                    strValue = m_PK
                                End If
                                '
                            ElseIf controlName.Substring(0, 3) = "ddl" Then
                                'Special case
                                strPrefix = "ddl"
                                strControl = controlName
                                ctl = pgContentPlaceHolder.FindControl(strControl)
                                If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                    strValue = CType(ctl, Telerik.Web.UI.RadComboBox).SelectedItem.Value.ToString
                                Else
                                    strValue = CType(ctl, DropDownList).SelectedItem.Value.ToString
                                End If



                            End If

                        Else
                            strPrefix = "ddl"
                            strControl = "ddl" & dr3("FldName").ToString
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                strValue = CType(ctl, Telerik.Web.UI.RadComboBox).SelectedItem.Value.ToString
                            Else
                                strValue = CType(ctl, DropDownList).SelectedItem.Value.ToString
                            End If
                        End If

                    End If  'Determine type of field and set strValue

                    If strValue <> "" Then

                        strFields &= dr3("FldName") & ","
                        strMarkers &= "?,"

                        If dr3("FldType") = "Datetime" Then
                            db2.AddParameter(strParamName, Convert.ToDateTime(strValue), DataAccess.OleDbDataType.OleDbDateTime, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Bit" Or dr3("FldType") = "TinyInt" Then
                            db2.AddParameter(strParamName, Convert.ToInt16(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Int" Then
                            db2.AddParameter(strParamName, Convert.ToInt32(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Char" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbChar, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Float" Or dr3("FldType") = "Money" Then
                            db2.AddParameter(strParamName, Convert.ToDecimal(strValue), DataAccess.OleDbDataType.OleDbDecimal, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Uniqueidentifier" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, dr3("FldLen"), ParameterDirection.Input)
                        End If

                    End If

                    'Increment the intCounter variable
                    intCounter += 1
                Next

                'Remove the last comma
                strFields = strFields.Substring(0, strFields.Length - 1)
                strMarkers = strMarkers.Substring(0, strMarkers.Length - 1)

                'Add the parentheses and VALUES keyword
                strFields = " (" & strFields & ") "
                strMarkers = "VALUES(" & strMarkers & ")"

                ' Modification by Corey Masson - July 28, 2004
                ' This modification is to add the ModUser and ModDate values to an inserted record
                Dim User As String
                Dim m_Context As HttpContext
                m_Context = HttpContext.Current
                User = CType(HttpContext.Current.Session("UserName"), String)

                Dim newstrFields As New System.Text.StringBuilder
                Dim newstrMarkers As New System.Text.StringBuilder
                With newstrFields
                    .Append(Trim(strFields))
                End With
                With newstrMarkers
                    .Append(Trim(strMarkers))
                End With
                newstrFields.Replace(")", ",ModUser, ModDate)", newstrFields.Length - 1, 1)
                newstrMarkers.Replace(")", ",?,?)", newstrMarkers.Length - 1, 1)
                strFields = newstrFields.ToString
                strMarkers = newstrMarkers.ToString
                db2.AddParameter(strParamName, User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db2.AddParameter(strParamName, Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
                ' End modification - July 28, 2004
                strSQL = "INSERT INTO " & strTable & strFields & strMarkers
                Try
                    db2.RunParamSQLExecuteNoneQuery(strSQL)
                    'Catch ex As UniqueIndexException
                     '	Dim exTracker = new AdvApplicationInsightsInitializer()
                    '	exTracker.TrackExceptionWrapper(ex)


                    '    'DisplayOleDbErrorCollection(ex.GetBaseException())

                    '    '   return Message
                    '    Return DALExceptions.BuildUniqueIndexExceptionMessage(ex)
                Catch ex As OleDbException
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    '   return an error to the client
                    Return DALExceptions.BuildErrorMessage(ex)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.Dispose()
                    'db2 = Nothing
                    Throw New BaseException("Error adding record to " & strTable & " - " & ex.InnerException.Message)
                End Try


                arrSQL.Add(strSQL)

            Next    'Loop through the dtTables.rows

            'Cleanup
            db2.Dispose()
            'db2 = Nothing
            Return String.Empty
        End Function
        Public Function DoInsert(ByVal Form As HtmlForm, Optional ByVal InternationalPhone As Boolean = False, Optional ByVal InternationalFax As Boolean = False, Optional ByVal InternationalZip As Boolean = False) As String
            '**************************************************************************************************
            'Purpose:       Uses the data dictionary to construct sql Insert statements for the current page
            '               and execute them. It relies on three datatables added to the current httpcontext
            '               items collection by the PageSetup sub.
            'Parameters:
            '[HtmlForm]     The form for which the insert is to be performed
            'Returns:       NA
            'Created:       Troy Richards, 7/10/2003
            '**************************************************************************************************
            Dim dtResFlds As DataTable
            Dim dtDefs As DataTable
            Dim dtFldsMask As DataTable
            Dim dtTables As New DataTable
            Dim dr1, dr3 As DataRow
            Dim strPrefix As String
            'Dim intDDLCounter As Integer = 2
            Dim strTable As String
            'Dim intPK As Integer
            Dim strSQL As String
            Dim arrSQL As New ArrayList
            'Dim intItem As Integer
            'Dim strInClause As String
            ' Dim db As New DataAccess
            Dim db2 As New DataAccess
            'Dim ds As New DataSet
            Dim strFields As String = String.Empty
            Dim strMarkers As String = String.Empty
            Dim intCounter As Integer
            Dim strParamName As String = String.Empty
            'Dim intLen As Integer
            Dim strControl As String
            Dim ctl As Control
            Dim strValue As String = String.Empty
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim ResourceId As Integer
            'Dim msg As String
            Dim controlName As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)
            dtDefs = CType(m_Context.Items("DDLDefs"), DataTable)
            dtFldsMask = CType(m_Context.Items("FldsMask"), DataTable)

            'Make the DDLId column the primary key for the dtDefs datatable
            With dtDefs
                .PrimaryKey = New DataColumn() {.Columns("DDLId")}
            End With

            'Add a column called TblName and another called TblPK to the dtTables datatable
            'Dim col1 As DataColumn = dtTables.Columns.Add("TblName")
            'Dim col2 As DataColumn = dtTables.Columns.Add("TblPK", GetType(Integer))
            dtTables.Columns.Add("TblName")
            dtTables.Columns.Add("TblPK", GetType(Integer))
            'Make the TblName column the primary key
            With dtTables
                .PrimaryKey = New DataColumn() {.Columns("TblName")}
            End With

            'Get DISTINCT tables that are used on the page. Loop through dtResFlds and add the
            'TblName from it to dtTables if it does not exist there already.
            For Each dr1 In dtResFlds.Rows
                If dtTables.Rows.Count = 0 Or Not ValExistsInDt(dtTables, dr1("TblName")) Then
                    Dim dr2 As DataRow = dtTables.NewRow
                    dr2("TblName") = dr1("TblName")
                    dr2("TblPK") = dr1("TblPK")
                    dtTables.Rows.Add(dr2)
                End If
            Next

            'Add dtTables to the items collection of the httpcontext object.
            m_Context.Items.Add("Tables", dtTables)

            'Reset the connection string to the regular database
            'Changed by Corey Masson Dec 30, 2003
            'SingletonAppSettings.AppSettings("ConString")
            'db2.ConnectionString = "ConString"
            db2.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            'Loop through the dtTables datatable.
            For Each dr1 In dtTables.Rows
                'Find the matching rows in dtResFlds that belong to the current table.
                Dim aRows As DataRow()
                strTable = dr1("TblName")
                aRows = dtResFlds.Select("TblName = '" & strTable & "'")
                For Each dr3 In aRows
                    'Add a parameter for each field
                    strParamName = "param" & intCounter.ToString
                    If dr3.IsNull("DDLId") Then
                        If dr3("FldType") = "Bit" Then 'Checkbox
                            strControl = "chk" & dr3("FldName").ToString()
                            ctl = Form.FindControl(strControl)
                            If CType(ctl, CheckBox).Checked = True Then
                                strValue = "1"
                            Else
                                strValue = "0"
                            End If
                        Else                            'Textbox
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = Form.FindControl(strControl)
                            'If it is a datetime field there is no need to check if
                            'there is an input mask.
                            If dr3("FldType") = "Datetime" Then
                                strValue = CType(ctl, TextBox).Text.ToString
                            Else
                                'We need to check if the field has an input mask.
                                'In order to do this we first need to get the fldid.
                                'We can then use it to lookup any input mask in the
                                'FldsMask datatable
                                Dim intFldId As Integer
                                intFldId = dr3("FldId")
                                Dim dr5 As DataRow
                                dr5 = dtFldsMask.Rows.Find(intFldId)



                                If dr5 Is Nothing Then
                                    'There is no input mask for this field
                                    strValue = CType(ctl, TextBox).Text.ToString
                                Else
                                    Dim strMask As String
                                    Dim strVal As String
                                    strMask = dr5("Mask")
                                    strVal = CType(ctl, TextBox).Text.ToString
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    End If
                                End If
                                'Modified on 2/25/2005
                                'If intFldId = 99 Then

                                'For Phone
                                If (strControl.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Phone") <> -1) Then
                                        Dim strMask As String
                                        Dim strVal As String
                                        Dim facInputMasks As New InputMasksFacade
                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                        'strMask = "(###)-###-####"
                                        strVal = CType(ctl, TextBox).Text.ToString
                                        '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                        If strVal.Length > 0 And InternationalPhone = False Then
                                            strValue = RemoveMask(strMask, strVal)
                                        Else
                                            strValue = CType(ctl, TextBox).Text.ToString
                                        End If
                                    End If
                                End If

                                'For Fax
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Fax") <> -1) Then
                                    Dim strMask As String
                                    Dim strVal As String
                                    Dim facInputMasks As New InputMasksFacade

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
                                    'strMask = "(###)-###-####"

                                    strVal = CType(ctl, TextBox).Text.ToString

                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 And InternationalFax = False Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    End If
                                End If

                                'For Zip
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Zip") <> -1) Then
                                    Dim strMask As String
                                    Dim strVal As String
                                    Dim facInputMasks As New InputMasksFacade

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

                                    'strMask = "#####"
                                    strVal = CType(ctl, TextBox).Text.ToString
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 And InternationalZip = False Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    End If
                                End If

                            End If

                            'If we are dealing with the primary key we need
                            'to generate a guid for it.
                            If dr3("FldId").ToString() = dr1("TblPK").ToString() Then
                                strValue = m_PK 'changed to fix refresh problem. #BN 07/09/04 #
                                'm_PK = strValue #BN 07/09/04 #
                            End If
                        End If

                    Else
                        'This will usually be a ddl. However, if we are on the page that sets up the
                        'ddl itself then it will be a textbox. If the ResouceId associated with the
                        'DDLId is the same as the ResourceId of the current page then it is a textbox.
                        'For example, on the page that sets up the states the StateId will be a textbox
                        'rather than a ddl. On other pages it will be a ddl.
                        drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                        intResourceId = drDDLDefs("DDLResourceId")
                        If Not dr3.IsNull("ControlName") Then
                            controlName = dr3("ControlName").ToString
                        Else
                            controlName = ""
                        End If
                        If intResourceId = ResourceId Then
                            'We are on the page that sets up the ddl
                            strPrefix = "txt"
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = Form.FindControl(strControl)
                            strValue = CType(ctl, TextBox).Text.ToString
                            'If we are dealing with the primary key we need
                            'to generate a guid for it.
                            If dr3("FldId").ToString() = dr1("TblPK").ToString() Then
                                ' Commented out by Corey Masson July 29, 2004 - We now create the GUID in the UI
                                'strValue = Guid.NewGuid.ToString()
                                'm_PK = strValue
                                strValue = m_PK
                            End If
                        ElseIf controlName <> "" Then
                            If controlName.Substring(0, 3) = "txt" Then
                                strControl = dr3("ControlName")
                                ctl = Form.FindControl(strControl)
                                strValue = CType(ctl, TextBox).Text.ToString
                                'If we are dealing with the primary key we need
                                'to generate a guid for it.
                                If dr3("FldId").ToString() = dr1("TblPK").ToString() Then
                                    ' Commented out by Corey Masson July 29, 2004 - We now create the GUID in the UI
                                    'strValue = Guid.NewGuid.ToString()
                                    'm_PK = strValue
                                    strValue = m_PK
                                End If
                                '
                            ElseIf controlName.Substring(0, 3) = "ddl" Then
                                'Special case
                                strPrefix = "ddl"
                                strControl = controlName
                                ctl = Form.FindControl(strControl)
                                If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                    strValue = CType(ctl, Telerik.Web.UI.RadComboBox).SelectedItem.Value.ToString
                                Else
                                    strValue = CType(ctl, DropDownList).SelectedItem.Value.ToString
                                End If



                            End If

                        Else
                            strPrefix = "ddl"
                            strControl = "ddl" & dr3("FldName").ToString
                            ctl = Form.FindControl(strControl)
                            If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                strValue = CType(ctl, Telerik.Web.UI.RadComboBox).SelectedItem.Value.ToString
                            Else
                                strValue = CType(ctl, DropDownList).SelectedItem.Value.ToString
                            End If
                        End If

                    End If  'Determine type of field and set strValue

                    If strValue <> "" Then

                        strFields &= dr3("FldName") & ","
                        strMarkers &= "?,"

                        If dr3("FldType") = "Datetime" Then
                            db2.AddParameter(strParamName, Convert.ToDateTime(strValue), DataAccess.OleDbDataType.OleDbDateTime, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Bit" Or dr3("FldType") = "TinyInt" Then
                            db2.AddParameter(strParamName, Convert.ToInt16(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Int" Then
                            db2.AddParameter(strParamName, Convert.ToInt32(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Char" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbChar, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Float" Or dr3("FldType") = "Money" Then
                            db2.AddParameter(strParamName, Convert.ToDecimal(strValue), DataAccess.OleDbDataType.OleDbDecimal, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Uniqueidentifier" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, dr3("FldLen"), ParameterDirection.Input)
                        End If

                    End If

                    'Increment the intCounter variable
                    intCounter += 1
                Next

                'Remove the last comma
                strFields = strFields.Substring(0, strFields.Length - 1)
                strMarkers = strMarkers.Substring(0, strMarkers.Length - 1)

                'Add the parentheses and VALUES keyword
                strFields = " (" & strFields & ") "
                strMarkers = "VALUES(" & strMarkers & ")"

                ' Modification by Corey Masson - July 28, 2004
                ' This modification is to add the ModUser and ModDate values to an inserted record
                Dim User As String
                Dim m_Context As HttpContext
                m_Context = HttpContext.Current
                User = CType(HttpContext.Current.Session("UserName"), String)

                Dim newstrFields As New System.Text.StringBuilder
                Dim newstrMarkers As New System.Text.StringBuilder
                With newstrFields
                    .Append(Trim(strFields))
                End With
                With newstrMarkers
                    .Append(Trim(strMarkers))
                End With
                newstrFields.Replace(")", ",ModUser, ModDate)", newstrFields.Length - 1, 1)
                newstrMarkers.Replace(")", ",?,?)", newstrMarkers.Length - 1, 1)
                strFields = newstrFields.ToString
                strMarkers = newstrMarkers.ToString
                db2.AddParameter(strParamName, User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db2.AddParameter(strParamName, Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
                ' End modification - July 28, 2004
                strSQL = "INSERT INTO " & strTable & strFields & strMarkers
                Try
                    db2.RunParamSQLExecuteNoneQuery(strSQL)
                    'Catch ex As UniqueIndexException
                     '	Dim exTracker = new AdvApplicationInsightsInitializer()
                    '	exTracker.TrackExceptionWrapper(ex)


                    '    'DisplayOleDbErrorCollection(ex.GetBaseException())

                    '    '   return Message
                    '    Return DALExceptions.BuildUniqueIndexExceptionMessage(ex)
                Catch ex As OleDbException
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    '   return an error to the client
                    Return DALExceptions.BuildErrorMessage(ex)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.Dispose()
                    'db2 = Nothing
                    Throw New BaseException("Error adding record to " & strTable & " - " & ex.InnerException.Message)
                End Try


                arrSQL.Add(strSQL)

            Next    'Loop through the dtTables.rows

            'Cleanup
            db2.Dispose()
            'db2 = Nothing
            Return String.Empty
        End Function

        Public Function DoInsert1(ByVal Form As HtmlForm) As String
            '**************************************************************************************************
            'Purpose:       Uses the data dictionary to construct sql Insert statements for the current page
            '               and execute them. It relies on three datatables added to the current httpcontext
            '               items collection by the PageSetup sub.
            'Parameters:
            '[HtmlForm]     The form for which the insert is to be performed
            'Returns:       NA
            'Created:       Troy Richards, 7/10/2003
            '**************************************************************************************************
            Dim dtResFlds As DataTable
            Dim dtDefs As DataTable
            Dim dtFldsMask As DataTable
            Dim dtTables As New DataTable
            Dim dr1, dr3 As DataRow
            Dim strPrefix As String
            'Dim intDDLCounter As Integer = 2
            Dim strTable As String
            'Dim intPK As Integer
            Dim strSQL As String
            Dim arrSQL As New ArrayList
            'Dim intItem As Integer
            'Dim strInClause As String
            'Dim db As New DataAccess
            Dim db2 As New DataAccess
            'Dim ds As New DataSet
            Dim strFields As String = String.Empty
            Dim strMarkers As String = String.Empty
            Dim intCounter As Integer
            Dim strParamName As String = String.Empty
            'Dim intLen As Integer
            Dim strControl As String
            Dim ctl As Control
            Dim strValue As String
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim ResourceId As Integer
            'Dim msg As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)
            dtDefs = CType(m_Context.Items("DDLDefs"), DataTable)
            dtFldsMask = CType(m_Context.Items("FldsMask"), DataTable)

            'Make the DDLId column the primary key for the dtDefs datatable
            With dtDefs
                .PrimaryKey = New DataColumn() {.Columns("DDLId")}
            End With

            'Add a column called TblName and another called TblPK to the dtTables datatable
            Dim col1 As DataColumn = dtTables.Columns.Add("TblName")
            Dim col2 As DataColumn = dtTables.Columns.Add("TblPK", GetType(Integer))
            'Make the TblName column the primary key
            With dtTables
                .PrimaryKey = New DataColumn() {.Columns("TblName")}
            End With

            'Get DISTINCT tables that are used on the page. Loop through dtResFlds and add the
            'TblName from it to dtTables if it does not exist there already.
            For Each dr1 In dtResFlds.Rows
                If dtTables.Rows.Count = 0 Or Not ValExistsInDt(dtTables, dr1("TblName")) Then
                    Dim dr2 As DataRow = dtTables.NewRow
                    dr2("TblName") = dr1("TblName")
                    dr2("TblPK") = dr1("TblPK")
                    dtTables.Rows.Add(dr2)
                End If
            Next

            'Add dtTables to the items collection of the httpcontext object.
            m_Context.Items.Add("Tables", dtTables)

            'Reset the connection string to the regular database
            'Changed by Corey Masson Dec 30, 2003
            'SingletonAppSettings.AppSettings("ConString")
            'db2.ConnectionString = "ConString"
            db2.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            'Loop through the dtTables datatable.
            For Each dr1 In dtTables.Rows
                'Find the matching rows in dtResFlds that belong to the current table.
                Dim aRows As DataRow()
                strTable = dr1("TblName")
                aRows = dtResFlds.Select("TblName = '" & strTable & "'")
                For Each dr3 In aRows
                    'Add a parameter for each field
                    strParamName = "param" & intCounter.ToString
                    If dr3.IsNull("DDLId") Then
                        If dr3("FldType") = "Bit" Then 'Checkbox
                            strControl = "chk" & dr3("FldName").ToString()
                            ctl = Form.FindControl(strControl)
                            If CType(ctl, WebControls.CheckBox).Checked = True Then
                                strValue = "1"
                            Else
                                strValue = "0"
                            End If
                        Else                            'Textbox
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = Form.FindControl(strControl)
                            'If it is a datetime field there is no need to check if
                            'there is an input mask.
                            If dr3("FldType") = "Datetime" Then
                                strValue = CType(ctl, WebControls.TextBox).Text.ToString
                            Else
                                'We need to check if the field has an input mask.
                                'In order to do this we first need to get the fldid.
                                'We can then use it to lookup any input mask in the
                                'FldsMask datatable
                                Dim intFldId As Integer
                                intFldId = dr3("FldId")
                                Dim dr5 As DataRow
                                dr5 = dtFldsMask.Rows.Find(intFldId)

                                If dr5 Is Nothing Then
                                    'There is no input mask for this field
                                    strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                Else
                                    Dim strMask As String
                                    Dim strVal As String
                                    strMask = dr5("Mask")
                                    strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                    End If
                                End If

                            End If

                            'If we are dealing with the primary key we need
                            'to generate a guid for it.
                            If dr3("FldId").ToString() = dr1("TblPK").ToString() Then
                                strValue = m_PK 'changed to fix refresh problem. #BN 07/09/04 #
                                'm_PK = strValue #BN 07/09/04 #
                            End If
                        End If

                    Else
                        'This will usually be a ddl. However, if we are on the page that sets up the
                        'ddl itself then it will be a textbox. If the ResouceId associated with the
                        'DDLId is the same as the ResourceId of the current page then it is a textbox.
                        'For example, on the page that sets up the states the StateId will be a textbox
                        'rather than a ddl. On other pages it will be a ddl.
                        drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                        intResourceId = drDDLDefs("DDLResourceId")
                        If intResourceId = ResourceId Then
                            'We are on the page that sets up the ddl
                            strPrefix = "txt"
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = Form.FindControl(strControl)
                            strValue = CType(ctl, WebControls.TextBox).Text.ToString
                            'If we are dealing with the primary key we need
                            'to generate a guid for it.
                            If dr3("FldId").ToString() = dr1("TblPK").ToString() Then
                                ' Commented out by Corey Masson July 29, 2004 - We now create the GUID in the UI
                                'strValue = Guid.NewGuid.ToString()
                                'm_PK = strValue
                                strValue = m_PK
                            End If
                        Else
                            strPrefix = "ddl"
                            strControl = "ddl" & dr3("FldName").ToString
                            ctl = Form.FindControl(strControl)
                            If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                strValue = CType(ctl, Telerik.Web.UI.RadComboBox).SelectedItem.Value.ToString
                            Else
                                strValue = CType(ctl, WebControls.DropDownList).SelectedItem.Value.ToString
                            End If
                        End If

                    End If  'Determine type of field and set strValue

                    If strValue <> "" Then

                        strFields &= dr3("FldName") & ","
                        strMarkers &= "?,"

                        If dr3("FldType") = "Datetime" Then
                            db2.AddParameter(strParamName, System.Convert.ToDateTime(strValue), DataAccess.OleDbDataType.OleDbDateTime, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Bit" Or dr3("FldType") = "TinyInt" Then
                            db2.AddParameter(strParamName, System.Convert.ToInt16(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Int" Then
                            db2.AddParameter(strParamName, System.Convert.ToInt32(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Char" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbChar, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Float" Or dr3("FldType") = "Money" Then
                            db2.AddParameter(strParamName, System.Convert.ToDecimal(strValue), DataAccess.OleDbDataType.OleDbDecimal, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Uniqueidentifier" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, dr3("FldLen"), ParameterDirection.Input)
                        End If

                    End If

                    'Increment the intCounter variable
                    intCounter += 1
                Next

                'Remove the last comma
                strFields = strFields.Substring(0, strFields.Length - 1)
                strMarkers = strMarkers.Substring(0, strMarkers.Length - 1)

                'Add the parentheses and VALUES keyword
                strFields = " (" & strFields & ") "
                strMarkers = "VALUES(" & strMarkers & ")"

                ' Modification by Corey Masson - July 28, 2004
                ' This modification is to add the ModUser and ModDate values to an inserted record
                Dim User As String
                Dim m_Context As System.Web.HttpContext
                m_Context = System.Web.HttpContext.Current
                User = CType(System.Web.HttpContext.Current.Session("UserName"), String)

                Dim newstrFields As New System.Text.StringBuilder
                Dim newstrMarkers As New System.Text.StringBuilder
                With newstrFields
                    .Append(Trim(strFields))
                End With
                With newstrMarkers
                    .Append(Trim(strMarkers))
                End With
                newstrFields.Replace(")", ",ModUser, ModDate)", newstrFields.Length - 1, 1)
                newstrMarkers.Replace(")", ",?,?)", newstrMarkers.Length - 1, 1)
                strFields = newstrFields.ToString
                strMarkers = newstrMarkers.ToString
                db2.AddParameter(strParamName, User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db2.AddParameter(strParamName, Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
                ' End modification - July 28, 2004
                strSQL = "INSERT INTO " & strTable & strFields & strMarkers
                Try
                    db2.RunParamSQLExecuteNoneQuery(strSQL)
                Catch ex As OleDbException
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    '   return an error to the client
                    Return DALExceptions.BuildErrorMessage(ex)

                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.Dispose()
                    'db2 = Nothing
                    Throw New BaseException("Error adding record to " & strTable & " - " & ex.InnerException.Message)
                End Try


                arrSQL.Add(strSQL)

            Next    'Loop through the dtTables.rows

            'Cleanup
            db2.Dispose()
            'db2 = Nothing
            Return String.Empty
        End Function

        Public Function DoUpdate(ByVal pgContentPlaceHolder As ContentPlaceHolder, Optional ByVal InternationalPhone As Boolean = False, Optional ByVal InternationalFax As Boolean = False, Optional ByVal InternationalZip As Boolean = False) As String
            '**************************************************************************************************
            'Purpose:       Uses the data dictionary to construct sql Update statements for the current page
            '               and execute them. It relies on three datatables added to the current httpcontext
            '               items collection by the PageSetup sub.
            'Parameters:
            '[HtmlForm]     The form for which the Update is to be performed
            'Returns:       NA
            'Created:       Troy Richards, 7/21/2003
            '**************************************************************************************************
            Dim dtResFlds As DataTable
            Dim dtDefs As DataTable
            Dim dtFldsMask As DataTable
            Dim dtTables As New DataTable
            Dim dr1, dr3 As DataRow
            Dim strPrefix As String = ""
            'Dim intDDLCounter As Integer = 2
            Dim strTable As String
            'Dim intPK As Integer
            Dim strSQL As String
            'Dim arrSql As ArrayList
            'Dim intItem As Integer
            'Dim db As New DataAccess
            Dim db2 As New DataAccess
            'Dim ds As New DataSet
            Dim strFields As String = ""
            Dim strWhere As String
            Dim intCounter As Integer
            Dim strParamName As String = String.Empty
            'Dim intLen As Integer
            Dim strControl As String
            Dim ctl As Control
            Dim strValue As String = ""
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim ResourceId As Integer
            Dim blnPK As Boolean
            Dim strPK As String
            Dim strPKValue As String
            Dim controlName As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)
            dtDefs = CType(m_Context.Items("DDLDefs"), DataTable)
            dtFldsMask = CType(m_Context.Items("FldsMask"), DataTable)

            'Make the DDLId column the primary key for the dtDefs datatable
            With dtDefs
                .PrimaryKey = New DataColumn() {.Columns("DDLId")}
            End With

            'Add a column called TblName and another called TblPK to the dtTables datatable
            Dim col1 As DataColumn = dtTables.Columns.Add("TblName")
            Dim col2 As DataColumn = dtTables.Columns.Add("TblPK", GetType(Integer))
            'Make the TblName column the primary key
            With dtTables
                .PrimaryKey = New DataColumn() {.Columns("TblName")}
            End With

            'Get DISTINCT tables that are used on the page. Loop through dtResFlds and add the
            'TblName from it to dtTables if it does not exist there already.
            For Each dr1 In dtResFlds.Rows
                If dtTables.Rows.Count = 0 Or Not ValExistsInDt(dtTables, dr1("TblName")) Then
                    Dim dr2 As DataRow = dtTables.NewRow
                    dr2("TblName") = dr1("TblName")
                    dr2("TblPK") = dr1("TblPK")
                    strText &= "TblName:" & dr2("TblName").ToString & " TblPk:" & dr2("TblPK").ToString
                    dtTables.Rows.Add(dr2)
                End If
            Next

            'Add dtTables to the items collection of the httpcontext object.
            'm_Context.Items.Add("Tables", dtTables)

            'Reset the connection string to the regular database
            'Changed by Corey Masson Dec 30, 2003
            'SingletonAppSettings.AppSettings("ConString")
            'db2.ConnectionString = "ConString"
            db2.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            'Loop through the dtTables datatable.
            For Each dr1 In dtTables.Rows
                strPK = ""
                strPKValue = ""
                'Find the matching rows in dtResFlds that belong to the current table.
                Dim aRows As DataRow()
                strTable = dr1("TblName")
                aRows = dtResFlds.Select("TblName = '" & strTable & "'")
                For Each dr3 In aRows
                    'Add a parameter for each field
                    strParamName = "param" & intCounter.ToString
                    blnPK = False
                    If dr3.IsNull("DDLId") Then
                        If dr3("FldType") = "Bit" Then 'Checkbox
                            strControl = "chk" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            If CType(ctl, CheckBox).Checked = True Then
                                strValue = "1"
                            Else
                                strValue = "0"
                            End If
                        Else                            'Textbox
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            'If it is a datetime field there is no need to check if
                            'there is an input mask.
                            If dr3("FldType") = "Datetime" Then
                                If TypeOf (ctl) Is Telerik.Web.UI.RadDatePicker Then
                                    strValue = CType(ctl, Telerik.Web.UI.RadDatePicker).SelectedDate
                                Else
                                    strValue = CType(ctl, TextBox).Text.ToString
                                End If
                            Else
                                'We need to check if the field has an input mask.
                                'In order to do this we first need to get the fldid.
                                'We can then use it to lookup any input mask in the
                                'FldsMask datatable
                                Dim intFldId As Integer
                                intFldId = dr3("FldId")
                                Dim dr5 As DataRow
                                dr5 = dtFldsMask.Rows.Find(intFldId)



                                If dr5 Is Nothing Then
                                    'There is no input mask for this field


                                    If Not TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    Else
                                        strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text
                                    End If


                                Else
                                    Dim strMask As String
                                    Dim strVal As String
                                    strMask = dr5("Mask")
                                    strVal = CType(ctl, TextBox).Text.ToString
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    End If
                                End If

                                'Modified on 1/21/2005
                                'For Phone
                                If (strControl.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Phone") <> -1) Then
                                        Dim facInputMasks As New InputMasksFacade
                                        Dim strMask As String
                                        Dim strVal As String
                                        'Dim zipMask As String

                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                        If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            strVal = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        Else
                                            strVal = CType(ctl, TextBox).Text.ToString
                                        End If

                                        'Dim strVal As String
                                        ''strMask = "(###)-###-####"
                                        'strVal = CType(ctl, TextBox).Text.ToString
                                        ''   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                        'If strVal.Length > 0 And InternationalPhone = False Then
                                        '    strValue = RemoveMask(strMask, strVal)
                                        'Else
                                        '    strValue = CType(ctl, TextBox).Text.ToString
                                        'End If

                                        If Not TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            If strVal.Length > 0 And InternationalPhone = False Then
                                                strValue = RemoveMask(strMask, strVal)
                                            Else
                                                strValue = CType(ctl, TextBox).Text.ToString
                                            End If
                                        Else
                                            strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        End If


                                    End If
                                End If

                                'For Fax
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Fax") <> -1) Then
                                    Dim strMask As String
                                    Dim strVal As String
                                    Dim facInputMasks As New InputMasksFacade

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                    'strMask = "(###)-###-####"
                                    strVal = CType(ctl, TextBox).Text.ToString
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 And InternationalFax = False Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    End If
                                End If

                                'For Zip
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Zip") <> -1) Then
                                    Dim strVal As String
                                    Dim facInputMasks As New InputMasksFacade
                                    Dim strMask As String

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
                                    'strMask = "(###)-###-####"
                                    'strMask = "#####"

                                    If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        strVal = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                    Else
                                        strVal = CType(ctl, TextBox).Text.ToString
                                    End If

                                    'strVal = CType(ctl, TextBox).Text.ToString
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 And InternationalZip = False Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            strVal = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        Else
                                            strVal = CType(ctl, TextBox).Text.ToString
                                        End If
                                    End If
                                End If

                            End If

                            'If we are dealing with the primary key we need
                            'to set blnPK to True.
                            If dr3("FldId") = dr1("TblPK") Then
                                blnPK = True
                                strPK = dr3("FldName")
                                strPKValue = strValue
                            End If
                        End If

                    Else
                        'This will usually be a ddl. However, if we are on the page that sets up the
                        'ddl itself then it will be a textbox. If the ResouceId associated with the
                        'DDLId is the same as the ResourceId of the current page then it is a textbox.
                        'For example, on the page that sets up the states the StateId will be a textbox
                        'rather than a ddl. On other pages it will be a ddl.
                        drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                        intResourceId = drDDLDefs("DDLResourceId")
                        If Not dr3.IsNull("ControlName") Then
                            controlName = dr3("ControlName").ToString
                        Else
                            controlName = ""
                        End If
                        If intResourceId = ResourceId Then
                            'We are on the page that sets up the ddl
                            strPrefix = "txt"
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            strValue = CType(ctl, TextBox).Text.ToString
                            'If we are dealing with the primary key we need
                            'to set blnPK to True.
                            If dr3("FldId") = dr1("TblPK") Then
                                blnPK = True
                                strPK = dr3("FldName")
                                strPKValue = strValue
                            End If
                        ElseIf controlName <> "" Then
                            If controlName.Substring(0, 3) = "txt" Then
                                strControl = dr3("ControlName")
                                ctl = pgContentPlaceHolder.FindControl(strControl)
                                strValue = CType(ctl, TextBox).Text.ToString
                                'If we are dealing with the primary key we need
                                'to set blnPK to True.
                                If dr3("FldId") = dr1("TblPK") Then
                                    blnPK = True
                                    strPK = dr3("FldName")
                                    strPKValue = strValue
                                End If
                                '
                            ElseIf controlName.Substring(0, 3) = "ddl" Then
                                'Special case
                                strPrefix = "ddl"
                                strControl = controlName
                                ctl = pgContentPlaceHolder.FindControl(strControl)
                                If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                    strValue = CType(ctl, Telerik.Web.UI.RadComboBox).SelectedItem.Value.ToString
                                Else
                                    strValue = CType(ctl, DropDownList).SelectedItem.Value.ToString
                                End If
                            End If

                        Else
                            strPrefix = "ddl"
                            strControl = "ddl" & dr3("FldName").ToString
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                strValue = CType(ctl, Telerik.Web.UI.RadComboBox).SelectedItem.Value.ToString
                            Else
                                strValue = CType(ctl, DropDownList).SelectedItem.Value.ToString
                            End If
                        End If

                    End If  'Determine type of field and set strValue

                    'We do not want to include the primary key in the fields statement.
                    'Also, we have to make sure that we add the parameter for it as the
                    'last parameter.
                    If blnPK = False Then
                        strFields &= dr3("FldName") & " = ?,"

                        If strValue = "" Then
                            db2.AddParameter(strParamName, , , , ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Datetime" Then
                            db2.AddParameter(strParamName, Convert.ToDateTime(strValue), DataAccess.OleDbDataType.OleDbDateTime, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Bit" Or dr3("FldType") = "TinyInt" Then
                            db2.AddParameter(strParamName, Convert.ToInt16(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Int" Then
                            db2.AddParameter(strParamName, Convert.ToInt32(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Char" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbChar, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Float" Or dr3("FldType") = "Money" Then
                            db2.AddParameter(strParamName, Convert.ToDecimal(strValue), DataAccess.OleDbDataType.OleDbDecimal, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Uniqueidentifier" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, dr3("FldLen"), ParameterDirection.Input)
                        End If

                    End If
                    'Increment the intCounter variable
                    intCounter += 1
                Next

                'We have to set the Where clause here so we end up with the pk being
                'the last parameter added
                strFields = strFields.Substring(0, strFields.Length - 1)
                ' Modification by Corey Masson - July 28, 2004
                ' This modification is to add the ModUser and ModDate values to an inserted record
                Dim User As String
                Dim m_Context As HttpContext
                m_Context = HttpContext.Current
                User = HttpContext.Current.Session("UserName")

                Dim newstrFields As New System.Text.StringBuilder
                With newstrFields
                    .Append(Trim(strFields))
                    .Append(", ModUser = ?, ModDate = ?")
                End With
                strFields = newstrFields.ToString
                db2.AddParameter(strParamName, User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db2.AddParameter(strParamName, Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
                ' End modification - July 28, 2004
                strWhere = " WHERE " & strPK & " = ?"
                db2.AddParameter("@pk", strPKValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'Remove the last comma from strFields

                'Build sql update statement
                strSQL = "UPDATE " & strTable & " SET " & strFields & strWhere

                Try
                    db2.RunParamSQLExecuteNoneQuery(strSQL)
                    Return ""
                Catch ex As OleDbException
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Return DALExceptions.BuildErrorMessage(ex)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New BaseException("Error updating record to " & strTable & " - " & ex.InnerException.Message)
                Finally
                    db2.Dispose()
                    'db2 = Nothing
                End Try

                'arrSql.Add(strSQL)

            Next    'Loop through the dtTables.rows



            Return String.Empty

        End Function

        Public Function DoUpdate(ByVal Form As HtmlForm, Optional ByVal InternationalPhone As Boolean = False, Optional ByVal InternationalFax As Boolean = False, Optional ByVal InternationalZip As Boolean = False) As String
            '**************************************************************************************************
            'Purpose:       Uses the data dictionary to construct sql Update statements for the current page
            '               and execute them. It relies on three datatables added to the current httpcontext
            '               items collection by the PageSetup sub.
            'Parameters:
            '[HtmlForm]     The form for which the Update is to be performed
            'Returns:       NA
            'Created:       Troy Richards, 7/21/2003
            '**************************************************************************************************
            Dim dtResFlds As DataTable
            Dim dtDefs As DataTable
            Dim dtFldsMask As DataTable
            Dim dtTables As New DataTable
            Dim dr1, dr3 As DataRow
            Dim strPrefix As String
            'Dim intDDLCounter As Integer = 2
            Dim strTable As String
            'Dim intPK As Integer
            Dim strSQL As String
            Dim arrSQL As New ArrayList
            'Dim intItem As Integer
            'Dim db As New DataAccess
            Dim db2 As New DataAccess
            'Dim ds As New DataSet
            Dim strFields As String = String.Empty
            Dim strWhere As String
            Dim intCounter As Integer
            Dim strParamName As String = String.Empty
            'Dim intLen As Integer
            Dim strControl As String
            Dim ctl As Control
            Dim strValue As String = String.Empty
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim ResourceId As Integer
            Dim blnPK As Boolean
            Dim strPK As String
            Dim strPKValue As String
            Dim controlName As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)
            dtDefs = CType(m_Context.Items("DDLDefs"), DataTable)
            dtFldsMask = CType(m_Context.Items("FldsMask"), DataTable)

            'Make the DDLId column the primary key for the dtDefs datatable
            With dtDefs
                .PrimaryKey = New DataColumn() {.Columns("DDLId")}
            End With

            'Add a column called TblName and another called TblPK to the dtTables datatable
            Dim col1 As DataColumn = dtTables.Columns.Add("TblName")
            Dim col2 As DataColumn = dtTables.Columns.Add("TblPK", GetType(Integer))
            'Make the TblName column the primary key
            With dtTables
                .PrimaryKey = New DataColumn() {.Columns("TblName")}
            End With

            'Get DISTINCT tables that are used on the page. Loop through dtResFlds and add the
            'TblName from it to dtTables if it does not exist there already.
            For Each dr1 In dtResFlds.Rows
                If dtTables.Rows.Count = 0 Or Not ValExistsInDt(dtTables, dr1("TblName")) Then
                    Dim dr2 As DataRow = dtTables.NewRow
                    dr2("TblName") = dr1("TblName")
                    dr2("TblPK") = dr1("TblPK")
                    strText &= "TblName:" & dr2("TblName").ToString & " TblPk:" & dr2("TblPK").ToString
                    dtTables.Rows.Add(dr2)
                End If
            Next

            'Add dtTables to the items collection of the httpcontext object.
            'm_Context.Items.Add("Tables", dtTables)

            'Reset the connection string to the regular database
            'Changed by Corey Masson Dec 30, 2003
            'SingletonAppSettings.AppSettings("ConString")
            'db2.ConnectionString = "ConString"
            db2.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            'Loop through the dtTables datatable.
            For Each dr1 In dtTables.Rows
                strPK = ""
                strPKValue = ""
                'Find the matching rows in dtResFlds that belong to the current table.
                Dim aRows As DataRow()
                strTable = dr1("TblName")
                aRows = dtResFlds.Select("TblName = '" & strTable & "'")
                For Each dr3 In aRows
                    'Add a parameter for each field
                    strParamName = "param" & intCounter.ToString
                    blnPK = False
                    If dr3.IsNull("DDLId") Then
                        If dr3("FldType") = "Bit" Then 'Checkbox
                            strControl = "chk" & dr3("FldName").ToString()
                            ctl = Form.FindControl(strControl)
                            If CType(ctl, CheckBox).Checked = True Then
                                strValue = "1"
                            Else
                                strValue = "0"
                            End If
                        Else                            'Textbox
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = Form.FindControl(strControl)
                            'If it is a datetime field there is no need to check if
                            'there is an input mask.
                            If dr3("FldType") = "Datetime" Then
                                strValue = CType(ctl, TextBox).Text.ToString
                            Else
                                'We need to check if the field has an input mask.
                                'In order to do this we first need to get the fldid.
                                'We can then use it to lookup any input mask in the
                                'FldsMask datatable
                                Dim intFldId As Integer
                                intFldId = dr3("FldId")
                                Dim dr5 As DataRow
                                dr5 = dtFldsMask.Rows.Find(intFldId)



                                If dr5 Is Nothing Then
                                    'There is no input mask for this field
                                    strValue = CType(ctl, TextBox).Text.ToString
                                Else
                                    Dim strMask As String
                                    Dim strVal As String
                                    strMask = dr5("Mask")
                                    strVal = CType(ctl, TextBox).Text.ToString
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    End If
                                End If

                                'Modified on 1/21/2005
                                'For Phone
                                If (strControl.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Phone") <> -1) Then
                                        Dim facInputMasks As New InputMasksFacade
                                        Dim strMask As String
                                        'Dim zipMask As String

                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                        Dim strVal As String
                                        'strMask = "(###)-###-####"
                                        strVal = CType(ctl, TextBox).Text.ToString
                                        '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                        If strVal.Length > 0 And InternationalPhone = False Then
                                            strValue = RemoveMask(strMask, strVal)
                                        Else
                                            strValue = CType(ctl, TextBox).Text.ToString
                                        End If
                                    End If
                                End If

                                'For Fax
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Fax") <> -1) Then
                                    Dim strMask As String
                                    Dim strVal As String
                                    Dim facInputMasks As New InputMasksFacade

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                    'strMask = "(###)-###-####"
                                    strVal = CType(ctl, TextBox).Text.ToString
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 And InternationalFax = False Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    End If
                                End If

                                'For Zip
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Zip") <> -1) Then
                                    Dim strVal As String
                                    Dim facInputMasks As New InputMasksFacade
                                    Dim strMask As String

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
                                    'strMask = "(###)-###-####"
                                    'strMask = "#####"
                                    strVal = CType(ctl, TextBox).Text.ToString
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    If strVal.Length > 0 And InternationalZip = False Then
                                        strValue = RemoveMask(strMask, strVal)
                                    Else
                                        strValue = CType(ctl, TextBox).Text.ToString
                                    End If
                                End If

                            End If

                            'If we are dealing with the primary key we need
                            'to set blnPK to True.
                            If dr3("FldId") = dr1("TblPK") Then
                                blnPK = True
                                strPK = dr3("FldName")
                                strPKValue = strValue
                            End If
                        End If

                    Else
                        'This will usually be a ddl. However, if we are on the page that sets up the
                        'ddl itself then it will be a textbox. If the ResouceId associated with the
                        'DDLId is the same as the ResourceId of the current page then it is a textbox.
                        'For example, on the page that sets up the states the StateId will be a textbox
                        'rather than a ddl. On other pages it will be a ddl.
                        drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                        intResourceId = drDDLDefs("DDLResourceId")
                        If Not dr3.IsNull("ControlName") Then
                            controlName = dr3("ControlName").ToString
                        Else
                            controlName = ""
                        End If
                        If intResourceId = ResourceId Then
                            'We are on the page that sets up the ddl
                            strPrefix = "txt"
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = Form.FindControl(strControl)
                            strValue = CType(ctl, TextBox).Text.ToString
                            'If we are dealing with the primary key we need
                            'to set blnPK to True.
                            If dr3("FldId") = dr1("TblPK") Then
                                blnPK = True
                                strPK = dr3("FldName")
                                strPKValue = strValue
                            End If
                        ElseIf controlName <> "" Then
                            If controlName.Substring(0, 3) = "txt" Then
                                strControl = dr3("ControlName")
                                ctl = Form.FindControl(strControl)
                                strValue = CType(ctl, TextBox).Text.ToString
                                'If we are dealing with the primary key we need
                                'to set blnPK to True.
                                If dr3("FldId") = dr1("TblPK") Then
                                    blnPK = True
                                    strPK = dr3("FldName")
                                    strPKValue = strValue
                                End If
                                '
                            ElseIf controlName.Substring(0, 3) = "ddl" Then
                                'Special case
                                strPrefix = "ddl"
                                strControl = controlName
                                ctl = Form.FindControl(strControl)
                                If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                    strValue = CType(ctl, Telerik.Web.UI.RadComboBox).SelectedItem.Value.ToString
                                Else
                                    strValue = CType(ctl, DropDownList).SelectedItem.Value.ToString
                                End If
                            End If

                        Else
                            strPrefix = "ddl"
                            strControl = "ddl" & dr3("FldName").ToString
                            ctl = Form.FindControl(strControl)
                            If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                strValue = CType(ctl, Telerik.Web.UI.RadComboBox).SelectedItem.Value.ToString
                            Else
                                strValue = CType(ctl, DropDownList).SelectedItem.Value.ToString
                            End If
                        End If

                    End If  'Determine type of field and set strValue

                    'We do not want to include the primary key in the fields statement.
                    'Also, we have to make sure that we add the parameter for it as the
                    'last parameter.
                    If blnPK = False Then
                        strFields &= dr3("FldName") & " = ?,"

                        If strValue = "" Then
                            db2.AddParameter(strParamName, , , , ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Datetime" Then
                            db2.AddParameter(strParamName, Convert.ToDateTime(strValue), DataAccess.OleDbDataType.OleDbDateTime, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Bit" Or dr3("FldType") = "TinyInt" Then
                            db2.AddParameter(strParamName, Convert.ToInt16(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Int" Then
                            db2.AddParameter(strParamName, Convert.ToInt32(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Char" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbChar, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Float" Or dr3("FldType") = "Money" Then
                            db2.AddParameter(strParamName, Convert.ToDecimal(strValue), DataAccess.OleDbDataType.OleDbDecimal, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Uniqueidentifier" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, dr3("FldLen"), ParameterDirection.Input)
                        End If

                    End If
                    'Increment the intCounter variable
                    intCounter += 1
                Next

                'We have to set the Where clause here so we end up with the pk being
                'the last parameter added
                strFields = strFields.Substring(0, strFields.Length - 1)
                ' Modification by Corey Masson - July 28, 2004
                ' This modification is to add the ModUser and ModDate values to an inserted record
                Dim User As String
                Dim m_Context As HttpContext
                m_Context = HttpContext.Current
                User = CType(HttpContext.Current.Session("UserName"), String)

                Dim newstrFields As New System.Text.StringBuilder
                With newstrFields
                    .Append(Trim(strFields))
                    .Append(", ModUser = ?, ModDate = ?")
                End With
                strFields = newstrFields.ToString
                db2.AddParameter(strParamName, User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db2.AddParameter(strParamName, Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
                strWhere = " WHERE " & strPK & " = ?"
                db2.AddParameter("@pk", strPKValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                strSQL = "UPDATE " & strTable & " SET " & strFields & strWhere

                Try
                    db2.RunParamSQLExecuteNoneQuery(strSQL)

                    Return ""
                Catch ex As OleDbException
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Return DALExceptions.BuildErrorMessage(ex)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New BaseException("Error updating record to " & strTable & " - " & ex.InnerException.Message)
                Finally
                    db2.Dispose()
                    'db2 = Nothing
                End Try

                'arrSQL.Add(strSQL)

            Next    'Loop through the dtTables.rows

            Return String.Empty


        End Function

        Public Function DoDelete(ByVal Form As HtmlForm) As String
            '**************************************************************************************************
            'Purpose:       Uses the data dictionary to construct sql Delete statements for the current page
            '               and execute them. It relies on three datatables added to the current httpcontext
            '               items collection by the PageSetup sub.
            'Parameters:
            '[HtmlForm]     The form for which the insert is to be performed
            'Returns:       NA
            'Created:       Troy Richards, 7/22/2003
            '**************************************************************************************************
            Dim dtResFlds As DataTable
            Dim dtTables As New DataTable
            Dim dr1, dr3 As DataRow
            Dim strTable As String
            'Dim intPK As Integer
            Dim strSQL As String = String.Empty
            'Dim arrSql As  ArrayList
            'Dim db As New DataAccess
            Dim db2 As New DataAccess
            'Dim ds As New DataSet
            'Dim intCounter As Integer
            Dim strControl As String
            Dim ctl As Control
            Dim strValue As String
            'Dim intResourceId As Integer
            Dim ResourceId As Integer
            Dim strPK As String

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = CType(m_Context.Items("ResourceId"), Integer)

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)

            'Make the FldId column the primary key for the dtResFlds datatable
            With dtResFlds
                .PrimaryKey = New DataColumn() {.Columns("FldId")}
            End With

            'Add a column called TblName and another called TblPK to the dtTables datatable
            'Dim col1 As DataColumn = dtTables.Columns.Add("TblName")
            'Dim col2 As DataColumn = dtTables.Columns.Add("TblPK", GetType(Integer))
            dtTables.Columns.Add("TblName")
            dtTables.Columns.Add("TblPK", GetType(Integer))
            'Make the TblName column the primary key
            With dtTables
                .PrimaryKey = New DataColumn() {.Columns("TblName")}
            End With

            'Get DISTINCT tables that are used on the page. Loop through dtResFlds and add the
            'TblName from it to dtTables if it does not exist there already.
            For Each dr1 In dtResFlds.Rows
                If dtTables.Rows.Count = 0 Or Not ValExistsInDt(dtTables, dr1("TblName")) Then
                    Dim dr2 As DataRow = dtTables.NewRow
                    dr2("TblName") = dr1("TblName")
                    dr2("TblPK") = dr1("TblPK")
                    dtTables.Rows.Add(dr2)
                End If
            Next

            'Set the connection string to the regular database
            'Changed by Corey Masson Dec 30, 2003
            'SingletonAppSettings.AppSettings("ConString")
            'db2.ConnectionString = "ConString"
            db2.ConnectionString = CType(myAdvAppSettings.AppSettings("ConString"), String)
            'Loop through the dtTables datatable.
            For Each dr1 In dtTables.Rows
                'Get table name
                strTable = dr1("TblName")
                'We need the name of the primary key. We can obtain this from
                'the dtResFlds datatable
                dr3 = dtResFlds.Rows.Find(dr1("TblPK"))
                strPK = dr3("FldName")
                'Get the value of the pk from the form
                strControl = "txt" & strPK
                ctl = Form.FindControl(strControl)
                strValue = CType(ctl, TextBox).Text

                strSQL = "DELETE FROM " & strTable & " WHERE " & strPK & " = ?"
                db2.AddParameter("@id", strValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                Try
                    db2.RunParamSQLExecuteNoneQuery(strSQL)
                    Return ""
                Catch ex As OleDbException
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Return DALExceptions.BuildErrorMessage(ex)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New BaseException("Error deleting record from " & strTable & " - " & ex.InnerException.Message)
                Finally
                    db2.Dispose()
                    'db2 = Nothing
                End Try

                'arrSql.Add(strSQL)

            Next    'Loop through the dtTables.rows
            strText = strSQL

            'Cleanup
            db2.Dispose()
            'db2 = Nothing
            Return strText
        End Function

        Public Function DoDelete(ByVal pgContentPlaceHolder As ContentPlaceHolder) As String
            '**************************************************************************************************
            'Purpose:       Uses the data dictionary to construct sql Delete statements for the current page
            '               and execute them. It relies on three datatables added to the current httpcontext
            '               items collection by the PageSetup sub.
            'Parameters:
            '[HtmlForm]     The form for which the insert is to be performed
            'Returns:       NA
            'Created:       Troy Richards, 7/22/2003
            '**************************************************************************************************
            Dim dtResFlds As DataTable
            Dim dtTables As New DataTable
            Dim dr1, dr3 As DataRow
            Dim strTable As String
            'Dim intPK As Integer
            Dim strSQL As String = String.Empty
            'Dim arrSQL As New ArrayList
            'Dim db As New DataAccess
            Dim db2 As New DataAccess
            'Dim ds As New DataSet
            'Dim intCounter As Integer
            Dim strControl As String
            Dim ctl As Control
            Dim strValue As String
            'Dim intResourceId As Integer
            Dim ResourceId As Integer
            Dim strPK As String

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)

            'Make the FldId column the primary key for the dtResFlds datatable
            With dtResFlds
                .PrimaryKey = New DataColumn() {.Columns("FldId")}
            End With

            'Add a column called TblName and another called TblPK to the dtTables datatable
            Dim col1 As DataColumn = dtTables.Columns.Add("TblName")
            Dim col2 As DataColumn = dtTables.Columns.Add("TblPK", GetType(Integer))
            'Make the TblName column the primary key
            With dtTables
                .PrimaryKey = New DataColumn() {.Columns("TblName")}
            End With

            'Get DISTINCT tables that are used on the page. Loop through dtResFlds and add the
            'TblName from it to dtTables if it does not exist there already.
            For Each dr1 In dtResFlds.Rows
                If dtTables.Rows.Count = 0 Or Not ValExistsInDt(dtTables, dr1("TblName")) Then
                    Dim dr2 As DataRow = dtTables.NewRow
                    dr2("TblName") = dr1("TblName")
                    dr2("TblPK") = dr1("TblPK")
                    dtTables.Rows.Add(dr2)
                End If
            Next

            'Set the connection string to the regular database
            'Changed by Corey Masson Dec 30, 2003
            'SingletonAppSettings.AppSettings("ConString")
            'db2.ConnectionString = "ConString"
            db2.ConnectionString = myAdvAppSettings.AppSettings("ConString")
            'Loop through the dtTables datatable.
            For Each dr1 In dtTables.Rows
                'Get table name
                strTable = dr1("TblName")
                'We need the name of the primary key. We can obtain this from
                'the dtResFlds datatable
                dr3 = dtResFlds.Rows.Find(dr1("TblPK"))
                strPK = dr3("FldName")
                'Get the value of the pk from the form
                strControl = "txt" & strPK
                ctl = pgContentPlaceHolder.FindControl(strControl)
                strValue = CType(ctl, TextBox).Text

                strSQL = "DELETE FROM " & strTable & " WHERE " & strPK & " = ?"
                db2.AddParameter("@id", strValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                Try
                    db2.RunParamSQLExecuteNoneQuery(strSQL)
                    Return ""
                Catch ex As OleDbException
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Return DALExceptions.BuildErrorMessage(ex)
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New BaseException("Error deleting record from " & strTable & " - " & ex.InnerException.Message)
                Finally
                    db2.Dispose()
                    'db2 = Nothing
                End Try

                'arrSQL.Add(strSQL)

            Next    'Loop through the dtTables.rows
            strText = strSQL

            'Cleanup
            db2.Dispose()
            'db2 = Nothing

            Return strText

        End Function
        'Public Function DoDelete(ByVal Form As HtmlForm) As String
        '    '**************************************************************************************************
        '    'Purpose:       Uses the data dictionary to construct sql Delete statements for the current page
        '    '               and execute them. It relies on three datatables added to the current httpcontext
        '    '               items collection by the PageSetup sub.
        '    'Parameters:
        '    '[HtmlForm]     The form for which the insert is to be performed
        '    'Returns:       NA
        '    'Created:       Troy Richards, 7/22/2003
        '    '**************************************************************************************************
        '    Dim dtResFlds As DataTable
        '    Dim dtTables As New DataTable
        '    Dim dr1, dr3 As DataRow
        '    Dim strTable As String
        '    Dim intPK As Integer
        '    Dim strSQL As String
        '    Dim arrSQL As New ArrayList
        '    Dim db As New DataAccess
        '    Dim db2 As New DataAccess
        '    Dim ds As New DataSet
        '    Dim intCounter As Integer
        '    Dim strControl As String
        '    Dim ctl As Control
        '    Dim strValue As String
        '    Dim intResourceId As Integer
        '    Dim ResourceId As Integer
        '    Dim strPK As String

        '    ResourceId = m_Context.Items("ResourceId")

        '    dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)

        '    'Make the FldId column the primary key for the dtResFlds datatable
        '    With dtResFlds
        '        .PrimaryKey = New DataColumn() {.Columns("FldId")}
        '    End With

        '    'Add a column called TblName and another called TblPK to the dtTables datatable
        '    Dim col1 As DataColumn = dtTables.Columns.Add("TblName")
        '    Dim col2 As DataColumn = dtTables.Columns.Add("TblPK", GetType(Integer))
        '    'Make the TblName column the primary key
        '    With dtTables
        '        .PrimaryKey = New DataColumn() {.Columns("TblName")}
        '    End With

        '    'Get DISTINCT tables that are used on the page. Loop through dtResFlds and add the
        '    'TblName from it to dtTables if it does not exist there already.
        '    For Each dr1 In dtResFlds.Rows
        '        If dtTables.Rows.Count = 0 Or Not ValExistsInDT(dtTables, dr1("TblName")) Then
        '            Dim dr2 As DataRow = dtTables.NewRow
        '            dr2("TblName") = dr1("TblName")
        '            dr2("TblPK") = dr1("TblPK")
        '            dtTables.Rows.Add(dr2)
        '        End If
        '    Next

        '    'Set the connection string to the regular database
        '    'Changed by Corey Masson Dec 30, 2003
        '    'SingletonAppSettings.AppSettings("ConString")
        '    'db2.ConnectionString = "ConString"
        '    db2.ConnectionString = SingletonAppSettings.AppSettings("ConString")
        '    'Loop through the dtTables datatable.
        '    For Each dr1 In dtTables.Rows
        '        'Get table name
        '        strTable = dr1("TblName")
        '        'We need the name of the primary key. We can obtain this from
        '        'the dtResFlds datatable
        '        dr3 = dtResFlds.Rows.Find(dr1("TblPK"))
        '        strPK = dr3("FldName")
        '        'Get the value of the pk from the form
        '        strControl = "txt" & strPK
        '        ctl = Form.FindControl(strControl)
        '        strValue = CType(ctl, WebControls.TextBox).Text

        '        strSQL = "DELETE FROM " & strTable & " WHERE " & strPK & " = ?"
        '        db2.AddParameter("@id", strValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

        '        Try
        '            db2.RunParamSQLExecuteNoneQuery(strSQL)
        '            Return ""
        '        Catch ex As OleDbException
         '        	Dim exTracker = new AdvApplicationInsightsInitializer()
        '        	exTracker.TrackExceptionWrapper(ex)

        '            Return DALExceptions.BuildErrorMessage(ex)
        '        Catch ex As System.Exception
         '        	Dim exTracker = new AdvApplicationInsightsInitializer()
        '        	exTracker.TrackExceptionWrapper(ex)

        '            Throw New BaseException("Error deleting record from " & strTable & " - " & ex.InnerException.Message)
        '        Finally
        '            db2.Dispose()
        '            db2 = Nothing
        '        End Try

        '        arrSQL.Add(strSQL)

        '    Next    'Loop through the dtTables.rows
        '    strText = strSQL

        '    'Cleanup
        '    db2.Dispose()
        '    db2 = Nothing



        'End Function
        Public Function DoInsertCampus(ByVal pgContentPlaceHolder As ContentPlaceHolder, Optional ByVal InternationalPhone As Boolean = False, Optional ByVal InternationalFax As Boolean = False, Optional ByVal InternationalZip As Boolean = False) As String
            '**************************************************************************************************
            'Purpose:       Uses the data dictionary to construct sql Insert statements for the current page
            '               and execute them. It relies on three datatables added to the current httpcontext
            '               items collection by the PageSetup sub.
            'Parameters:
            '[HtmlForm]     The form for which the insert is to be performed
            'Returns:       NA
            'Created:       Troy Richards, 7/10/2003
            '**************************************************************************************************
            Dim dtResFlds As DataTable
            Dim dtDefs As DataTable
            Dim dtFldsMask As DataTable
            Dim dtTables As New DataTable
            Dim dr1, dr3 As DataRow
            Dim strPrefix As String
            'Dim intDDLCounter As Integer = 2
            Dim strTable As String
            'Dim intPK As Integer
            Dim strSQL As String
            Dim arrSql As New ArrayList
            'Dim intItem As Integer
            'Dim strInClause As String
            'Dim db As New DataAccess
            Dim db2 As New DataAccess
            'Dim ds As New DataSet
            Dim strFields As String = String.Empty
            Dim strMarkers As String = String.Empty
            Dim intCounter As Integer
            Dim strParamName As String = String.Empty
            'Dim intLen As Integer
            Dim strControl As String
            Dim ctl As Control
            Dim strValue As String = String.Empty
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim ResourceId As Integer
            'Dim msg As String
            Dim controlName As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = CType(m_Context.Items("ResourceId"), Integer)

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)
            dtDefs = CType(m_Context.Items("DDLDefs"), DataTable)
            dtFldsMask = CType(m_Context.Items("FldsMask"), DataTable)

            'Make the DDLId column the primary key for the dtDefs datatable
            With dtDefs
                .PrimaryKey = New DataColumn() {.Columns("DDLId")}
            End With

            'Add a column called TblName and another called TblPK to the dtTables datatable
            Dim col1 As DataColumn = dtTables.Columns.Add("TblName")
            Dim col2 As DataColumn = dtTables.Columns.Add("TblPK", GetType(Integer))
            'Make the TblName column the primary key
            With dtTables
                .PrimaryKey = New DataColumn() {.Columns("TblName")}
            End With

            'Get DISTINCT tables that are used on the page. Loop through dtResFlds and add the
            'TblName from it to dtTables if it does not exist there already.
            For Each dr1 In dtResFlds.Rows
                If dtTables.Rows.Count = 0 Or Not ValExistsInDt(dtTables, dr1("TblName")) Then
                    Dim dr2 As DataRow = dtTables.NewRow
                    dr2("TblName") = dr1("TblName")
                    dr2("TblPK") = dr1("TblPK")
                    dtTables.Rows.Add(dr2)
                End If
            Next

            'Add dtTables to the items collection of the httpcontext object.

            If m_Context.Items.Contains("Tables") Then
                m_Context.Items.Remove("Tables")
            End If

            m_Context.Items.Add("Tables", dtTables)

            'Reset the connection string to the regular database
            'Changed by Corey Masson Dec 30, 2003
            'SingletonAppSettings.AppSettings("ConString")
            'db2.ConnectionString = "ConString"
            db2.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
            'Loop through the dtTables datatable.
            For Each dr1 In dtTables.Rows
                'Find the matching rows in dtResFlds that belong to the current table.
                Dim aRows As DataRow()
                strTable = dr1("TblName")
                aRows = dtResFlds.Select("TblName = '" & strTable & "'")
                For Each dr3 In aRows
                    'Add a parameter for each field
                    strParamName = "param" & intCounter.ToString
                    If dr3.IsNull("DDLId") Then
                        If dr3("FldType") = "Bit" Then 'Checkbox
                            strControl = "chk" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            If CType(ctl, WebControls.CheckBox).Checked = True Then
                                strValue = "1"
                            Else
                                strValue = "0"
                            End If
                        Else                            'Textbox
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            'If it is a datetime field there is no need to check if
                            'there is an input mask.
                            If dr3("FldType") = "Datetime" Then
                                strValue = CType(ctl, WebControls.TextBox).Text.ToString
                            Else
                                'We need to check if the field has an input mask.
                                'In order to do this we first need to get the fldid.
                                'We can then use it to lookup any input mask in the
                                'FldsMask datatable
                                Dim intFldId As Integer
                                intFldId = dr3("FldId")
                                Dim dr5 As DataRow
                                dr5 = dtFldsMask.Rows.Find(intFldId)

                                If dr5 Is Nothing And Not TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                    'There is no input mask for this field
                                    strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                Else
                                    Dim strMask As String
                                    Dim strVal As String
                                    If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        ' do nothing here - handle in phone/fax or zip logic below
                                    Else
                                        strMask = dr5("Mask")
                                        strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                        '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                        If strVal.Length > 0 Then
                                            strValue = RemoveMask(strMask, strVal)
                                        Else
                                            strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                        End If
                                    End If

                                End If
                                'Modified on 2/25/2005
                                'If intFldId = 99 Then

                                'For Phone1
                                If (strControl.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Phone1") <> -1) Then
                                        Dim strMask As String
                                        Dim strVal As String
                                        Dim facInputMasks As New InputMasksFacade
                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                        'strMask = "(###)-###-####"
                                        If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        Else
                                            strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                            If strVal.Length > 0 And InternationalPhone = False And Not dr5 Is Nothing Then
                                                strValue = RemoveMask(strMask, strVal)
                                            Else
                                                strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                            End If
                                        End If
                                        '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                        'If strVal.Length > 0 And InternationalPhone = False Then
                                        '    strValue = RemoveMask(strMask, strVal)
                                        'Else
                                        '    strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                        'End If
                                    End If
                                End If

                                'For Phone2
                                If (strControl.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Phone2") <> -1) Then
                                        Dim strMask As String
                                        Dim strVal As String
                                        Dim facInputMasks As New InputMasksFacade
                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                        'strMask = "(###)-###-####"
                                        If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        Else
                                            strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                            If strVal.Length > 0 And InternationalPhone = False Then
                                                strValue = RemoveMask(strMask, strVal)
                                            Else
                                                strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                            End If
                                        End If
                                        '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                        'If strVal.Length > 0 And InternationalPhone = False Then
                                        '    strValue = RemoveMask(strMask, strVal)
                                        'Else
                                        '    strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                        'End If
                                    End If
                                End If

                                'For Phone3
                                If (strControl.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Phone3") <> -1) Then
                                        Dim strMask As String
                                        Dim strVal As String
                                        Dim facInputMasks As New InputMasksFacade
                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                        'strMask = "(###)-###-####"
                                        If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        Else
                                            strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                            If strVal.Length > 0 And InternationalPhone = False Then
                                                strValue = RemoveMask(strMask, strVal)
                                            Else
                                                strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                            End If
                                        End If
                                        '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                        'If strVal.Length > 0 And InternationalPhone = False Then
                                        '    strValue = RemoveMask(strMask, strVal)
                                        'Else
                                        '    strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                        'End If
                                    End If
                                End If

                                'For Fax
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Fax") <> -1) Then
                                    Dim strMask As String
                                    Dim strVal As String
                                    Dim facInputMasks As New InputMasksFacade

                                    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
                                    'strMask = "(###)-###-####"

                                    If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                    Else
                                        strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                        If strVal.Length > 0 And InternationalPhone = False And Not dr5 Is Nothing Then
                                            strValue = RemoveMask(strMask, strVal)
                                        Else
                                            strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                        End If
                                    End If
                                    '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                    'If strVal.Length > 0 And InternationalPhone = False Then
                                    '    strValue = RemoveMask(strMask, strVal)
                                    'Else
                                    '    strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                    'End If
                                End If

                                'For Zip
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Zip") <> -1) Then
                                    Dim strMask As String
                                    Dim strVal As String
                                    Dim facInputMasks As New InputMasksFacade

                                    If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                    Else
                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
                                        'strMask = "#####"
                                        strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                        '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                        If strVal.Length > 0 And InternationalZip = False Then
                                            strValue = RemoveMask(strMask, strVal)
                                        Else
                                            strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                        End If
                                    End If
                                End If

                            End If

                            'If we are dealing with the primary key we need
                            'to generate a guid for it.
                            If dr3("FldId").ToString() = dr1("TblPK").ToString() Then
                                strValue = m_PK 'changed to fix refresh problem. #BN 07/09/04 #
                                'm_PK = strValue #BN 07/09/04 #
                            End If
                        End If

                    Else
                        'This will usually be a ddl. However, if we are on the page that sets up the
                        'ddl itself then it will be a textbox. If the ResouceId associated with the
                        'DDLId is the same as the ResourceId of the current page then it is a textbox.
                        'For example, on the page that sets up the states the StateId will be a textbox
                        'rather than a ddl. On other pages it will be a ddl.
                        drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                        intResourceId = drDDLDefs("DDLResourceId")
                        If Not dr3.IsNull("ControlName") Then
                            controlName = dr3("ControlName").ToString
                        Else
                            controlName = ""
                        End If
                        If intResourceId = ResourceId Then
                            'We are on the page that sets up the ddl
                            strPrefix = "txt"
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            strValue = CType(ctl, WebControls.TextBox).Text.ToString
                            'If we are dealing with the primary key we need
                            'to generate a guid for it.
                            If dr3("FldId").ToString() = dr1("TblPK").ToString() Then
                                ' Commented out by Corey Masson July 29, 2004 - We now create the GUID in the UI
                                'strValue = Guid.NewGuid.ToString()
                                'm_PK = strValue
                                strValue = m_PK
                            End If
                        ElseIf controlName <> "" Then
                            If controlName.Substring(0, 3) = "txt" Then
                                strControl = dr3("ControlName")
                                ctl = pgContentPlaceHolder.FindControl(strControl)
                                strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                'If we are dealing with the primary key we need
                                'to generate a guid for it.
                                If dr3("FldId").ToString() = dr1("TblPK").ToString() Then
                                    ' Commented out by Corey Masson July 29, 2004 - We now create the GUID in the UI
                                    'strValue = Guid.NewGuid.ToString()
                                    'm_PK = strValue
                                    strValue = m_PK
                                End If
                            End If
                        Else
                            strPrefix = "ddl"
                            strControl = "ddl" & dr3("FldName").ToString
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                strValue = CType(ctl, Telerik.Web.UI.RadComboBox).SelectedItem.Value.ToString
                            Else
                                strValue = CType(ctl, WebControls.DropDownList).SelectedItem.Value.ToString
                            End If
                        End If

                    End If  'Determine type of field and set strValue

                    If strValue <> "" Then

                        strFields &= dr3("FldName") & ","
                        strMarkers &= "?,"

                        If dr3("FldType") = "Datetime" Then
                            db2.AddParameter(strParamName, System.Convert.ToDateTime(strValue), DataAccess.OleDbDataType.OleDbDateTime, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Bit" Or dr3("FldType") = "TinyInt" Then
                            db2.AddParameter(strParamName, System.Convert.ToInt16(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Int" Then
                            db2.AddParameter(strParamName, System.Convert.ToInt32(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Char" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbChar, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Float" Or dr3("FldType") = "Money" Then
                            db2.AddParameter(strParamName, System.Convert.ToDecimal(strValue), DataAccess.OleDbDataType.OleDbDecimal, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Uniqueidentifier" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, dr3("FldLen"), ParameterDirection.Input)
                        End If

                    End If

                    'Increment the intCounter variable
                    intCounter += 1
                Next

                'Remove the last comma
                strFields = strFields.Substring(0, strFields.Length - 1)
                strMarkers = strMarkers.Substring(0, strMarkers.Length - 1)

                'Add the parentheses and VALUES keyword
                strFields = " (" & strFields & ") "
                strMarkers = "VALUES(" & strMarkers & ")"

                ' Modification by Corey Masson - July 28, 2004
                ' This modification is to add the ModUser and ModDate values to an inserted record
                Dim User As String
                Dim m_Context As System.Web.HttpContext
                m_Context = System.Web.HttpContext.Current
                User = System.Web.HttpContext.Current.Session("UserName")

                Dim newstrFields As New System.Text.StringBuilder
                Dim newstrMarkers As New System.Text.StringBuilder
                With newstrFields
                    .Append(Trim(strFields))
                End With
                With newstrMarkers
                    .Append(Trim(strMarkers))
                End With
                newstrFields.Replace(")", ",ModUser, ModDate)", newstrFields.Length - 1, 1)
                newstrMarkers.Replace(")", ",?,?)", newstrMarkers.Length - 1, 1)
                strFields = newstrFields.ToString
                strMarkers = newstrMarkers.ToString
                db2.AddParameter(strParamName, User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db2.AddParameter(strParamName, Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
                ' End modification - July 28, 2004
                strSQL = "INSERT INTO " & strTable & strFields & strMarkers
                Try
                    db2.RunParamSQLExecuteNoneQuery(strSQL)
                    'Catch ex As UniqueIndexException
                     '	Dim exTracker = new AdvApplicationInsightsInitializer()
                    '	exTracker.TrackExceptionWrapper(ex)


                    '    'DisplayOleDbErrorCollection(ex.GetBaseException())

                    '    '   return Message
                    '    Return DALExceptions.BuildUniqueIndexExceptionMessage(ex)
                Catch ex As OleDbException
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    '   return an error to the client
                    Return DALExceptions.BuildErrorMessage(ex)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    db2.Dispose()
                    'db2 = Nothing
                    Throw New BaseException("Error adding record to " & strTable & " - " & ex.InnerException.Message)
                End Try


                arrSql.Add(strSQL)

            Next    'Loop through the dtTables.rows

            'Cleanup
            db2.Dispose()
            'db2 = Nothing
            Return Nothing
        End Function
        Public Function DoUpdateCampus(ByVal pgContentPlaceHolder As ContentPlaceHolder, Optional ByVal InternationalPhone As Boolean = False, Optional ByVal InternationalFax As Boolean = False, Optional ByVal InternationalZip As Boolean = False) As String
            '**************************************************************************************************
            'Purpose:       Uses the data dictionary to construct sql Update statements for the current page
            '               and execute them. It relies on three datatables added to the current httpcontext
            '               items collection by the PageSetup sub.
            'Parameters:
            '[HtmlForm]     The form for which the Update is to be performed
            'Returns:       NA
            'Created:       Troy Richards, 7/21/2003
            '**************************************************************************************************
            Dim dtResFlds As DataTable
            Dim dtDefs As DataTable
            Dim dtFldsMask As DataTable
            Dim dtTables As New DataTable
            Dim dr1, dr3 As DataRow
            Dim strPrefix As String
            'Dim intDDLCounter As Integer = 2
            Dim strTable As String
            'Dim intPK As Integer
            Dim strSQL As String
            Dim arrSQL As New ArrayList
            'Dim intItem As Integer
            Dim db As New DataAccess
            Dim db2 As New DataAccess
            'Dim ds As New DataSet
            Dim strFields As String = String.Empty
            Dim strWhere As String
            Dim intCounter As Integer
            Dim strParamName As String = String.Empty
            'Dim intLen As Integer
            Dim strControl As String
            Dim ctl As Control
            Dim strValue As String = String.Empty
            Dim drDDLDefs As DataRow
            Dim intResourceId As Integer
            Dim ResourceId As Integer
            Dim blnPK As Boolean
            Dim strPK As String
            Dim strPKValue As String
            Dim controlName As String

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")

            dtResFlds = CType(m_Context.Items("ResFlds"), DataTable)
            dtDefs = CType(m_Context.Items("DDLDefs"), DataTable)
            dtFldsMask = CType(m_Context.Items("FldsMask"), DataTable)

            'Make the DDLId column the primary key for the dtDefs datatable
            With dtDefs
                .PrimaryKey = New DataColumn() {.Columns("DDLId")}
            End With

            'Add a column called TblName and another called TblPK to the dtTables datatable
            Dim col1 As DataColumn = dtTables.Columns.Add("TblName")
            Dim col2 As DataColumn = dtTables.Columns.Add("TblPK", GetType(Integer))
            'Make the TblName column the primary key
            With dtTables
                .PrimaryKey = New DataColumn() {.Columns("TblName")}
            End With

            'Get DISTINCT tables that are used on the page. Loop through dtResFlds and add the
            'TblName from it to dtTables if it does not exist there already.
            For Each dr1 In dtResFlds.Rows
                If dtTables.Rows.Count = 0 Or Not ValExistsInDt(dtTables, dr1("TblName")) Then
                    Dim dr2 As DataRow = dtTables.NewRow
                    dr2("TblName") = dr1("TblName")
                    dr2("TblPK") = dr1("TblPK")
                    strText &= "TblName:" & dr2("TblName").ToString & " TblPk:" & dr2("TblPK").ToString
                    dtTables.Rows.Add(dr2)
                End If
            Next

            'Add dtTables to the items collection of the httpcontext object.
            'm_Context.Items.Add("Tables", dtTables)

            'Reset the connection string to the regular database
            'Changed by Corey Masson Dec 30, 2003
            'SingletonAppSettings.AppSettings("ConString")
            'db2.ConnectionString = "ConString"
            db2.ConnectionString = CType(myAdvAppSettings.AppSettings("ConString"), String)
            'Loop through the dtTables datatable.
            For Each dr1 In dtTables.Rows
                strPK = ""
                strPKValue = ""
                'Find the matching rows in dtResFlds that belong to the current table.
                Dim aRows As DataRow()
                strTable = dr1("TblName")
                aRows = dtResFlds.Select("TblName = '" & strTable & "'")
                For Each dr3 In aRows
                    'Add a parameter for each field
                    strParamName = "param" & intCounter.ToString
                    blnPK = False
                    If dr3.IsNull("DDLId") Then
                        If dr3("FldType") = "Bit" Then 'Checkbox
                            strControl = "chk" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            If CType(ctl, WebControls.CheckBox).Checked = True Then
                                strValue = "1"
                            Else
                                strValue = "0"
                            End If
                        Else                            'Textbox
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            'If it is a datetime field there is no need to check if
                            'there is an input mask.
                            If dr3("FldType") = "Datetime" Then
                                strValue = CType(ctl, WebControls.TextBox).Text.ToString
                            Else
                                'We need to check if the field has an input mask.
                                'In order to do this we first need to get the fldid.
                                'We can then use it to lookup any input mask in the
                                'FldsMask datatable
                                Dim intFldId As Integer
                                intFldId = dr3("FldId")
                                Dim dr5 As DataRow
                                dr5 = dtFldsMask.Rows.Find(intFldId)



                                If dr5 Is Nothing And Not TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                    'There is no input mask for this field
                                    strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                Else
                                    If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                    Else
                                        Dim strMask As String
                                        Dim strVal As String
                                        strMask = dr5("Mask")
                                        strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                        '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                        If strVal.Length > 0 Then
                                            strValue = RemoveMask(strMask, strVal)
                                        Else
                                            strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                        End If
                                    End If
                                End If

                                'Modified on 1/21/2005
                                'For Phone1
                                If (strControl.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Phone1") <> -1) Then
                                        If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        Else
                                            Dim facInputMasks As New InputMasksFacade
                                            Dim strMask As String
                                            'Dim zipMask As String

                                            strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                            Dim strVal As String
                                            'strMask = "(###)-###-####"
                                            strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                            '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                            If strVal.Length > 0 And InternationalPhone = False Then
                                                strValue = RemoveMask(strMask, strVal)
                                            Else
                                                strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                            End If
                                        End If
                                    End If
                                End If

                                'Phone2
                                If (strControl.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Phone2") <> -1) Then
                                        If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        Else
                                            Dim facInputMasks As New InputMasksFacade
                                            Dim strMask As String
                                            'Dim zipMask As String

                                            strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                            Dim strVal As String
                                            'strMask = "(###)-###-####"
                                            strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                            '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                            If strVal.Length > 0 And InternationalPhone = False Then
                                                strValue = RemoveMask(strMask, strVal)
                                            Else
                                                strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                            End If
                                        End If
                                    End If
                                End If

                                'Phone3
                                If (strControl.ToString.IndexOf("PhoneType") = -1) Then
                                    If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Phone3") <> -1) Then
                                        If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                            strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                        Else
                                            Dim facInputMasks As New InputMasksFacade
                                            Dim strMask As String
                                            'Dim zipMask As String

                                            strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                            Dim strVal As String
                                            'strMask = "(###)-###-####"
                                            strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                            '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                            If strVal.Length > 0 And InternationalPhone = False Then
                                                strValue = RemoveMask(strMask, strVal)
                                            Else
                                                strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                            End If
                                        End If
                                    End If
                                End If

                                'For Fax
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Fax") <> -1) Then
                                    If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                    Else
                                        Dim strMask As String
                                        Dim strVal As String
                                        Dim facInputMasks As New InputMasksFacade

                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)

                                        'strMask = "(###)-###-####"
                                        strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                        '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                        If strVal.Length > 0 And InternationalFax = False Then
                                            strValue = RemoveMask(strMask, strVal)
                                        Else
                                            strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                        End If
                                    End If
                                End If

                                'For Zip
                                If Mid(strControl, 1, 3) = "txt" And (dr3("FldName").ToString.IndexOf("Zip") <> -1) Then
                                    If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                                        strValue = CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text.ToString
                                    Else
                                        Dim strVal As String
                                        Dim facInputMasks As New InputMasksFacade
                                        Dim strMask As String

                                        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
                                        'strMask = "(###)-###-####"
                                        'strMask = "#####"
                                        strVal = CType(ctl, WebControls.TextBox).Text.ToString
                                        '   check also that strVal is not empty. Added by Anatoly 2/11/2004
                                        If strVal.Length > 0 And InternationalZip = False Then
                                            strValue = RemoveMask(strMask, strVal)
                                        Else
                                            strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                        End If
                                    End If

                                End If

                            End If

                            'If we are dealing with the primary key we need
                            'to set blnPK to True.
                            If dr3("FldId") = dr1("TblPK") Then
                                blnPK = True
                                strPK = dr3("FldName")
                                strPKValue = strValue
                            End If
                        End If

                    Else
                        'This will usually be a ddl. However, if we are on the page that sets up the
                        'ddl itself then it will be a textbox. If the ResouceId associated with the
                        'DDLId is the same as the ResourceId of the current page then it is a textbox.
                        'For example, on the page that sets up the states the StateId will be a textbox
                        'rather than a ddl. On other pages it will be a ddl.
                        drDDLDefs = dtDefs.Rows.Find(dr3("DDLId"))
                        intResourceId = drDDLDefs("DDLResourceId")
                        If Not dr3.IsNull("ControlName") Then
                            controlName = dr3("ControlName").ToString
                        Else
                            controlName = ""
                        End If
                        If intResourceId = ResourceId Then
                            'We are on the page that sets up the ddl
                            strPrefix = "txt"
                            strControl = "txt" & dr3("FldName").ToString()
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            strValue = CType(ctl, WebControls.TextBox).Text.ToString
                            'If we are dealing with the primary key we need
                            'to set blnPK to True.
                            If dr3("FldId") = dr1("TblPK") Then
                                blnPK = True
                                strPK = dr3("FldName")
                                strPKValue = strValue
                            End If
                        ElseIf controlName <> "" Then
                            If controlName.Substring(0, 3) = "txt" Then
                                strControl = dr3("ControlName")
                                ctl = pgContentPlaceHolder.FindControl(strControl)
                                strValue = CType(ctl, WebControls.TextBox).Text.ToString
                                'If we are dealing with the primary key we need
                                'to set blnPK to True.
                                If dr3("FldId") = dr1("TblPK") Then
                                    blnPK = True
                                    strPK = dr3("FldName")
                                    strPKValue = strValue
                                End If
                            End If

                        Else
                            strPrefix = "ddl"
                            strControl = "ddl" & dr3("FldName").ToString
                            ctl = pgContentPlaceHolder.FindControl(strControl)
                            If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                strValue = CType(ctl, Telerik.Web.UI.RadComboBox).SelectedItem.Value.ToString
                            Else
                                strValue = CType(ctl, WebControls.DropDownList).SelectedItem.Value.ToString
                            End If
                        End If

                    End If  'Determine type of field and set strValue

                    'We do not want to include the primary key in the fields statement.
                    'Also, we have to make sure that we add the parameter for it as the
                    'last parameter.
                    If blnPK = False Then
                        strFields &= dr3("FldName") & " = ?,"

                        If strValue = "" Then
                            db2.AddParameter(strParamName, , , , ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Datetime" Then
                            db2.AddParameter(strParamName, System.Convert.ToDateTime(strValue), DataAccess.OleDbDataType.OleDbDateTime, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Bit" Or dr3("FldType") = "TinyInt" Then
                            db2.AddParameter(strParamName, System.Convert.ToInt16(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Int" Then
                            db2.AddParameter(strParamName, System.Convert.ToInt32(strValue), DataAccess.OleDbDataType.OleDbInteger, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Char" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbChar, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Float" Or dr3("FldType") = "Money" Then
                            db2.AddParameter(strParamName, System.Convert.ToDecimal(strValue), DataAccess.OleDbDataType.OleDbDecimal, dr3("FldLen"), ParameterDirection.Input)
                        ElseIf dr3("FldType") = "Uniqueidentifier" Then
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                        Else
                            db2.AddParameter(strParamName, strValue, DataAccess.OleDbDataType.OleDbString, dr3("FldLen"), ParameterDirection.Input)
                        End If

                    End If
                    'Increment the intCounter variable
                    intCounter += 1
                Next

                'We have to set the Where clause here so we end up with the pk being
                'the last parameter added
                strFields = strFields.Substring(0, strFields.Length - 1)
                ' Modification by Corey Masson - July 28, 2004
                ' This modification is to add the ModUser and ModDate values to an inserted record
                Dim User As String
                Dim m_Context As System.Web.HttpContext
                m_Context = System.Web.HttpContext.Current
                User = System.Web.HttpContext.Current.Session("UserName")

                Dim newstrFields As New System.Text.StringBuilder
                With newstrFields
                    .Append(Trim(strFields))
                    .Append(", ModUser = ?, ModDate = ?")
                End With
                strFields = newstrFields.ToString
                db2.AddParameter(strParamName, User, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db2.AddParameter(strParamName, Now, DataAccess.OleDbDataType.OleDbDateTime, 8, ParameterDirection.Input)
                ' End modification - July 28, 2004
                strWhere = " WHERE " & strPK & " = ?"
                db2.AddParameter("@pk", strPKValue, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                'Remove the last comma from strFields

                'Build sql update statement
                strSQL = "UPDATE " & strTable & " SET " & strFields & strWhere

                Try
                    db2.RunParamSQLExecuteNoneQuery(strSQL)
                    'Return ""
                Catch ex As OleDbException
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Return DALExceptions.BuildErrorMessage(ex)
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    Throw New BaseException("Error updating record to " & strTable & " - " & ex.InnerException.Message)
                Finally
                    db2.Dispose()
                    db2 = Nothing
                End Try

                arrSQL.Add(strSQL)

            Next    'Loop through the dtTables.rows
            Return Nothing
        End Function

        Public Sub DeleteAllSDFVisibily(ByVal ID As String)
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim dtm As System.DateTime = System.DateTime.Now

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConString"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                With sb
                    .Append("DELETE FROM sySdfModVis ")
                    .Append("WHERE SDFId = ?")
                End With

                db.AddParameter("@sdfid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error deleting visibility info for sdf - " & ex.InnerException.Message)
            Finally
                db.Dispose()
                db = Nothing
            End Try
        End Sub

        Public Sub DeleteAllSDFRange(ByVal ID As String)
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim dtm As System.DateTime = System.DateTime.Now

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConString"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

                With sb
                    .Append("DELETE FROM sySdfRange ")
                    .Append("WHERE SDFId = ?")
                End With

                db.AddParameter("@sdfid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error deleting range info for sdf - " & ex.InnerException.Message)
            Finally
                db.Dispose()
                db = Nothing
            End Try
        End Sub

        Public Sub DeleteAllSDFValList(ByVal ID As String)
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim dtm As System.DateTime = System.DateTime.Now

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConString"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                With sb
                    .Append("DELETE FROM sySdfValList ")
                    .Append("WHERE SDFId = ?")
                End With

                db.AddParameter("@sdfid", ID, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.RunParamSQLExecuteNoneQuery(sb.ToString)

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error deleting value list info for sdf - " & ex.InnerException.Message)
            Finally
                db.Dispose()
                db = Nothing
            End Try
        End Sub

        Public Function GetPageName() As String
            Dim iPosition As Integer
            Dim sPath As String
            sPath = m_Context.Request.Url.AbsolutePath.ToString
            'Find the start position of the .aspx extension
            iPosition = sPath.LastIndexOf(".aspx")
            sPath = sPath.Substring(0, iPosition)
            'Find the position of the last /
            iPosition = sPath.LastIndexOf("/")
            sPath = sPath.Substring(iPosition + 1)

            Return sPath
        End Function
        Public Function GetTypeFromPageName() As System.Type
            '**************************************************************************************************
            'Purpose:       Returns the Type (Class) of the .aspx page that is executing. Remember that each .aspx page is
            '               actually a Type (Class) in .NET.
            'Parameters:    None
            'Returns:       Type of the executing .aspx page
            'Created:       Troy Richards, 8/28/2003            
            '**************************************************************************************************
            Dim strPageName As String
            Dim asm As Reflection.Assembly
            Dim ty As Type

            asm = Reflection.Assembly.GetExecutingAssembly
            strPageName = asm.GetName.Name & "." & GetPageName()

            ty = asm.GetType(strPageName)
            Return ty
        End Function

        Public Function GetInstInputMasks() As DataTable
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim ds As DataSet

            With sb
                .Append("SELECT FldId,Mask ")
                .Append("FROM syInstFldsMask")
            End With

            Try
                db.ConnectionString = "ConString"
                ds = db.RunParamSQLDataSet(sb.ToString)
                Return ds.Tables(0)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error retrieving institution input masks:" & ex.InnerException.Message)
            Finally
                db.Dispose()
                'db = Nothing
            End Try
        End Function

        Public Function GetComparisonOps() As DataTable
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim ds As DataSet
            db.ConnectionString = "ConString"

            With sb
                .Append("SELECT CompOpId,CompOpText,AllowDateTime,AllowString,AllowNumber ")
                .Append("FROM syCompOps")
            End With

            Try
                ds = db.RunParamSQLDataSet(sb.ToString)
                Return ds.Tables(0)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error retrieving comparison operators:" & ex.InnerException.Message)
            Finally
                db.Dispose()
                'db = Nothing
            End Try
        End Function

        Public Sub SetBtnState(ByVal pgContentPlaceHolder As ContentPlaceHolder, ByVal strMode As String, Optional ByVal pObj As UserPagePermissionInfo = Nothing)
            Dim ctrlNew As Control
            Dim ctrlDelete As Control
            Dim ctrlSave As Control

            Select Case strMode

                Case "NEW"
                    'Try
                    '    ctrlNew = pgContentPlaceHolder.FindControl("btnNew")
                    'Catch ex As System.Exception
                     '	Dim exTracker = new AdvApplicationInsightsInitializer()
                    '	exTracker.TrackExceptionWrapper(ex)

                    '    Throw New BaseException("Could not find Control:" & ex.Message)
                    'End Try
                    'DirectCast(ctrlNew, Button).Enabled = False
                    Try
                        ctrlDelete = pgContentPlaceHolder.FindControl("btnDelete")
                        ctrlNew = pgContentPlaceHolder.FindControl("btnNew")
                        ctrlSave = pgContentPlaceHolder.FindControl("btnSave")
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        Throw New BaseException("Could not find Control:" & ex.Message)
                    End Try

                    'This is here just to keep the old code from crashing. Eventually we can
                    'remove it
                    If pObj Is Nothing Then
                        DirectCast(ctrlDelete, Button).Enabled = False
                    End If

                    'This is the new section of code that has the permissions handling
                    If Not pObj Is Nothing Then
                        If pObj.HasFull Or pObj.HasAdd Then
                            DirectCast(ctrlSave, Button).Enabled = True
                            DirectCast(ctrlNew, Button).Enabled = True
                        Else
                            DirectCast(ctrlSave, Button).Enabled = False
                            DirectCast(ctrlNew, Button).Enabled = False
                        End If

                        DirectCast(ctrlDelete, Button).Enabled = False
                    End If

                Case "EDIT"
                    Try
                        ctrlDelete = pgContentPlaceHolder.FindControl("btnDelete")
                        ctrlNew = pgContentPlaceHolder.FindControl("btnNew")
                        ctrlSave = pgContentPlaceHolder.FindControl("btnSave")
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        Throw New BaseException("Could not find Control:" & ex.Message)
                    End Try

                    'This is here just to keep the old code from crashing. Eventually we can
                    'remove it
                    If pObj Is Nothing Then
                        DirectCast(ctrlNew, Button).Enabled = True
                    End If

                    If pObj Is Nothing Then
                        DirectCast(ctrlDelete, Button).Enabled = True
                    End If

                    'This is the new section of code that has the permissions handling
                    If Not pObj Is Nothing Then
                        If pObj.HasFull Or pObj.HasEdit Then
                            DirectCast(ctrlSave, Button).Enabled = True
                        Else
                            DirectCast(ctrlSave, Button).Enabled = False
                        End If

                        If pObj.HasFull Or pObj.HasDelete Then
                            DirectCast(ctrlDelete, Button).Enabled = True
                        Else
                            DirectCast(ctrlDelete, Button).Enabled = False
                        End If

                        If pObj.HasFull Or pObj.HasAdd Then
                            DirectCast(ctrlNew, Button).Enabled = True
                        Else
                            DirectCast(ctrlNew, Button).Enabled = False
                        End If
                    End If


            End Select
        End Sub
        Public Sub SetBtnState(ByVal Form As HtmlForm, ByVal strMode As String, Optional ByVal pObj As UserPagePermissionInfo = Nothing)
            Dim ctrlNew As Control
            Dim ctrlDelete As Control
            Dim ctrlSave As Control

            Select Case strMode

                Case "NEW"
                    'Try
                    '    ctrlNew = Form.FindControl("btnNew")
                    'Catch ex As System.Exception
                     '	Dim exTracker = new AdvApplicationInsightsInitializer()
                    '	exTracker.TrackExceptionWrapper(ex)

                    '    Throw New BaseException("Could not find Control:" & ex.Message)
                    'End Try
                    'DirectCast(ctrlNew, Button).Enabled = False
                    Try
                        ctrlDelete = Form.FindControl("btnDelete")
                        ctrlNew = Form.FindControl("btnNew")
                        ctrlSave = Form.FindControl("btnSave")
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        Throw New BaseException("Could not find Control:" & ex.Message)
                    End Try

                    'This is here just to keep the old code from crashing. Eventually we can
                    'remove it
                    If pObj Is Nothing Then
                        DirectCast(ctrlDelete, Button).Enabled = False
                    End If

                    'This is the new section of code that has the permissions handling
                    If Not pObj Is Nothing Then
                        If pObj.HasFull Or pObj.HasAdd Then
                            DirectCast(ctrlSave, Button).Enabled = True
                            DirectCast(ctrlNew, Button).Enabled = True
                        Else
                            DirectCast(ctrlSave, Button).Enabled = False
                            DirectCast(ctrlNew, Button).Enabled = False
                        End If

                        DirectCast(ctrlDelete, Button).Enabled = False
                    End If

                Case "EDIT"
                    Try
                        ctrlDelete = Form.FindControl("btnDelete")
                        ctrlNew = Form.FindControl("btnNew")
                        ctrlSave = Form.FindControl("btnSave")
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        Throw New BaseException("Could not find Control:" & ex.Message)
                    End Try

                    'This is here just to keep the old code from crashing. Eventually we can
                    'remove it
                    If pObj Is Nothing Then
                        DirectCast(ctrlNew, Button).Enabled = True
                    End If

                    If pObj Is Nothing Then
                        DirectCast(ctrlDelete, Button).Enabled = True
                    End If

                    'This is the new section of code that has the permissions handling
                    If Not pObj Is Nothing Then
                        If pObj.HasFull Or pObj.HasEdit Then
                            DirectCast(ctrlSave, Button).Enabled = True
                        Else
                            DirectCast(ctrlSave, Button).Enabled = False
                        End If

                        If pObj.HasFull Or pObj.HasDelete Then
                            DirectCast(ctrlDelete, Button).Enabled = True
                        Else
                            DirectCast(ctrlDelete, Button).Enabled = False
                        End If

                        If pObj.HasFull Or pObj.HasAdd Then
                            DirectCast(ctrlNew, Button).Enabled = True
                        Else
                            DirectCast(ctrlNew, Button).Enabled = False
                        End If
                    End If


            End Select
        End Sub

        Public Function GetNumberOfOccurencesOfCharInString(ByVal stringToCheck As String, ByVal charToCheckFor As Char) As Integer
            Dim intCounter As Integer
            Dim arrCharsToCheck As New ArrayList
            Dim chr As Char
            Dim numFound As Integer

            For Each chr In stringToCheck
                arrCharsToCheck.Add(chr.ToString)
            Next

            numFound = 0
            For intCounter = 0 To arrCharsToCheck.Count - 1
                If arrCharsToCheck(intCounter) = charToCheckFor Then
                    numFound += 1
                End If
            Next

            Return numFound

        End Function

        Public Function StringContainsInvalidCharacters(ByVal stringToCheck As String, ByVal validCharacters As String) As Boolean
            Dim arrToCheck As New ArrayList
            Dim arrValidChars As New ArrayList
            Dim chr As Char
            Dim blnFound As Boolean
            Dim iCounter1 As Integer
            Dim iCounter2 As Integer

            For Each chr In stringToCheck
                arrToCheck.Add(chr.ToString)
            Next

            For Each chr In validCharacters
                arrValidChars.Add(chr.ToString)
            Next

            'We want to loop through each char in the string to check. If we find a char that is not in the
            'valid chars then we should return false.

            For iCounter1 = 0 To arrToCheck.Count - 1
                blnFound = False
                For iCounter2 = 0 To arrValidChars.Count - 1
                    If arrToCheck(iCounter1) = arrValidChars(iCounter2) Then
                        blnFound = True
                    End If
                Next
                If blnFound = False Then
                    Return True
                End If

            Next

            Return False

        End Function

        ''Added by Saraswathi Lakshmanan on May 07 2009
        ''To fetch the campgrpId for All Group
        ''Added to fix issue 16041

        Public Function GetCampGrpIdforAll() As DataTable
            Dim db As New DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim ds As New DataSet

            With sb
                .Append("Select CampGrpId from syCampGrps where CampGrpCode='All' and CampGrpDescrip='All' and StatusId in (Select StatusId from syStatuses where Status='Active'); ")
            End With

            Try

                db.ConnectionString = "ConString"
                ds = db.RunParamSQLDataSet(sb.ToString)
                Return ds.Tables(0)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error retrieving CampGrpId for 'All' Group:" & ex.InnerException.Message)
            Finally
                db.Dispose()
                db = Nothing
            End Try
        End Function

        Public Function RemoveInvalidFileNameChars(ByVal filename As String) As String
            filename = Replace(filename, " ", "")
            Return IO.Path.GetInvalidFileNameChars.Aggregate(filename, Function(current, invalidChar) current.Replace(invalidChar, "")).Trim()
        End Function

#End Region

        Public Sub SetCaptionsAndColorRequiredFieldsPopUps(ByVal Form As HtmlForm, Optional ByVal LeadGroup As String = "NULL", Optional ByVal InternationalAddress As Boolean = False)
            '**************************************************************************************************
            'Purpose:       When a page is loaded this sub is called to enter the captions and color the
            '               required fields.
            'Parameters:
            '[Form]         The form on the page that contains the asp.net server controls
            'Returns:       N/A
            'Created:       Troy Richards, 2/15/2005
            'Notes:         This sub relies on the ResourceId placed in  the httpcontext.items collection when
            '               when a request for the page is made. See the Application_BeginRequest sub in the
            '               Global.aspx.vb page.
            '**************************************************************************************************
            Dim ds As New DataSet
            Dim dtResDef, dtDDReqFlds, dtResReqFlds, dtIPEDSFields As DataTable
            Dim dr As DataRow
            Dim strControl As String
            Dim strPrefix As String
            'Dim intResourceId As Integer
            Dim ResourceId As Integer
            Dim intCounter As Integer = 0
            Dim sLangName As String
            Dim fac As New PageSetupFacade
            Dim ctl, ctl2 As Control
            Dim strFldLen As String

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")
            sLangName = m_Context.Items("Language")
            sLangName = sLangName.ToUpper



            Try
                'Get the resource definition table for this page
                dtResDef = fac.GetResourceDef(ResourceId, sLangName)

                'Get the data dictionary required fields
                dtDDReqFlds = fac.GetDataDictionaryReqFields

                'Get the school required fields for the resource being viewed
                dtResReqFlds = fac.GetSchlRequiredFieldsForResource(ResourceId)

                'Get the IPEDS fields
                dtIPEDSFields = New DataTable
                If MyAdvAppSettings.AppSettings("IPEDS").ToLower = "yes" Then
                    dtIPEDSFields = fac.GetIPEDSFields
                End If


                'Create PKs
                'Commented By Balaji on 3/19/2005 to avoid the problem 
                'with Address Status and Phone Status
                'With dtResDef
                '    .PrimaryKey = New DataColumn() {dtResDef.Columns("FldId")}
                'End With

                'With dtDDReqFlds
                '    .PrimaryKey = New DataColumn() {dtDDReqFlds.Columns("FldId")}
                'End With

                'With dtResReqFlds
                '    .PrimaryKey = New DataColumn() {dtResReqFlds.Columns("FldId")}
                'End With

                With dtIPEDSFields
                    .PrimaryKey = New DataColumn() {dtIPEDSFields.Columns("FldId")}
                End With
                'We need to loop through the records in the dtResDef datatable. 
                'First we will set up the captions and then deal with the required fields.                'is a required field we need to color the associated control and add a
                'For required fields we will color them appropriately and set up a RequiredFieldValidator.
                For Each dr In dtResDef.Rows
                    intCounter += 1
                    'If the ControlName field is not null then we have to use the control specified
                    'by that field.
                    Dim ctrlname As String = dr("FldName").ToString
                    Dim fldid As String = dr("FldId").ToString
                    If Not dr.IsNull("ControlName") Then
                        strControl = dr("ControlName").ToString()
                        strPrefix = strControl.Substring(0, 3)
                    Else
                        'If the DDLId associated with the record is null then we are
                        'dealing with a textbox or a checkbox. If not we are dealing with a dropdownlist.
                        'A checkbox normally is associated with a bit field in the database.
                        If dr.IsNull("DDLId") Then
                            If dr("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else 'Textbox
                                strPrefix = "txt"
                            End If
                        Else
                            strPrefix = "ddl"
                        End If
                        strControl = strPrefix & dr("FldName").ToString()
                    End If

                    ctl = Form.FindControl(strControl)
                    If ctl Is Nothing Then
                        Throw New System.Exception("Could not find the control: " & strControl)
                    End If
                    ctl2 = Form.FindControl("pnlRequiredFieldValidators")
                    If ctl2 Is Nothing Then
                        Throw New System.Exception("Could not find panel named pnlRequiredFieldValidators")
                    End If
                    strFldLen = dr("FldLen").ToString()

                    'If we are dealing with a textbox or a ddl then we need
                    'to check if there is an associated label control.
                    If strPrefix = "txt" Or strPrefix = "ddl" Then
                        Dim ctl3 As Control = Form.FindControl("lbl" & dr("FldName").ToString())
                        'The control might not have a label control associated with it.
                        If Not ctl3 Is Nothing Then
                            Try
                                CType(ctl3, Label).Text = dr("Caption")
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Throw New BaseException("Problem casting " & strControl & " to label " & "with caption " & dr("Caption").ToString)
                            End Try
                        End If
                        'The label control might be named after the name of the ddl or textbox control.
                        'This should only be performed when the controlname is specified
                        If Not dr.IsNull("ControlName") Then
                            Dim ctl30 As Control = Form.FindControl("lbl" & dr("ControlName").ToString().Substring(3))
                            If Not ctl30 Is Nothing Then
                                Try
                                    CType(ctl30, Label).Text = dr("Caption")
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    Throw New BaseException("Problem casting " & strControl & " to label " & "with caption " & dr("Caption").ToString)
                                End Try
                            End If
                        End If

                    ElseIf strPrefix = "chk" Then 'Checkbox
                        'We have to find the checkbox itself
                        Dim ctl3 As Control = Form.FindControl("chk" & dr("FldName").ToString())
                        'If the checkbox was not found then we should notify the client
                        If ctl3 Is Nothing Then
                            Throw New System.Exception("Could not find checkbox:" & strControl)
                        End If
                        'The checkbox might be found but we might have a problem
                        'in the casting operation.
                        If Not ctl3 Is Nothing Then
                            Try
                                '  If Not ctrlname = "LeadGrpId" Then
                                CType(ctl3, CheckBox).Text = dr("Caption")
                                '  End If
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Throw New BaseException("Problem casting " & strControl & " to checkbox " & "with caption " & dr("Caption").ToString)
                            End Try
                        End If
                    End If

                    'Color the required fields.
                    Dim req As Boolean = dr("Required")
                    If dr("Required") = True Or ValExistsInDTString(dtDDReqFlds, dr("FldId")) Or TblFldsIdExistsInDT(dtResReqFlds, dr("TblFldsId")) Or ((ResourceId = 174 Or ResourceId = 203 Or ResourceId = 169) And (MyAdvAppSettings.AppSettings("IPEDS").ToLower = "yes" And ValExistsInDt(dtIPEDSFields, dr("FldId")))) Then
                        Try
                            'If international address is checked and its a state field then do not mark it as required
                            'Balaji 03/04/2011
                            If InternationalAddress = True And CInt(dr("FldId")) = 11 Then
                                Exit Try
                            End If

                            If strPrefix = "txt" Then
                                ' CType(ctl, TextBox).BackColor = Color.FromName("#ffff99")
                            ElseIf strPrefix = "ddl" Then
                                If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                    ' CType(ctl, Telerik.Web.UI.RadComboBox).BackColor = Color.FromName("#ffff99")
                                Else
                                    'Enroll Leads page, International Address and State Field
                                    'If ResourceId = 174 And InternationalAddress = True And CInt(dr("FldId")) = 11 Then
                                    'DE5166 Janet Robinson 05/18/2011 Added ResourceID 203 (Student Master)
                                    If (ResourceId = 174 OrElse ResourceId = 203) And CInt(dr("FldId")) = 11 Then
                                        CType(ctl, DropDownList).BackColor = Color.FromName("White")
                                    Else
                                        ' CType(ctl, DropDownList).BackColor = Color.FromName("#ffff99")
                                    End If
                                End If
                            End If

                            Dim ctl3 As Control = Form.FindControl("lbl" & dr("FldName").ToString())
                            'The control might not have a label control associated with it.
                            If Not ctl3 Is Nothing Then
                                'DE5166 Janet Robinson 05/18/2011 Added ResourceID 203 (Student Master)
                                If (ResourceId = 174 OrElse ResourceId = 203) And CInt(dr("FldId")) = 11 Then
                                    CType(ctl3, Label).Text = dr("Caption")
                                Else
                                    CType(ctl3, Label).Text = dr("Caption") & "<font color=""red"">*</font>"
                                End If

                            End If
                        Catch ex As System.Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            Throw New System.Exception
                        End Try

                        'Add a RequiredFieldVaidator to monitor the control.
                        If strPrefix = "txt" Or strPrefix = "rad" Then
                            Dim rfv As New RequiredFieldValidator
                            With rfv
                                .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = dr("Caption").ToString & " is required"
                            End With
                            ctl2.Controls.Add(rfv)
                        End If

                        'Add a regular expression validator to monitor the control.
                        If strPrefix = "ddl" Then
                            'add a required field validator

                            Try
                                'If international address is checked and its a state field then do not mark it as required
                                'Balaji 03/04/2011
                                If InternationalAddress = True And CInt(dr("FldId")) = 11 Then
                                    Exit Try
                                End If
                                If ResourceId = 174 And CInt(dr("FldId")) = 11 Then
                                    Exit Try 'Don't add required validator for State Field - Balaji 03/09/2011
                                End If
                                Dim rfv As New RequiredFieldValidator
                                With rfv
                                    .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                    .ControlToValidate = strControl
                                    .Display = ValidatorDisplay.None
                                    .ErrorMessage = dr("Caption").ToString & " is required"
                                    .InitialValue = ""
                                    .EnableClientScript = True
                                End With
                                ctl2.Controls.Add(rfv)

                                'add a compare validator
                                Dim cv As New CompareValidator
                                With cv
                                    .ID = "cv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                    .ControlToValidate = strControl
                                    .Display = ValidatorDisplay.None
                                    .ErrorMessage = dr("Caption").ToString & " is required"
                                    .ValueToCompare = Guid.Empty.ToString
                                    .Operator = ValidationCompareOperator.NotEqual
                                    .EnableClientScript = True
                                End With
                                ctl2.Controls.Add(cv)
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                            End Try
                        End If

                    End If 'Control is required

                    'If the control is a textbox we need to also set the MaxLength property
                    'and also add a CompareValidator to perform a data type check. 
                    If strPrefix = "txt" Then
                        If dr("FldType") = "Varchar" Or dr("FldType") = "Char" Then
                            CType(ctl, TextBox).MaxLength = CInt(dr("FldLen"))
                        Else
                            CType(ctl, TextBox).MaxLength = 50
                        End If

                        Dim cmv As New CompareValidator
                        Try
                            'If ctl2.FindControl("cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString()) Is True Then
                            '    ctl2.Controls.Remove("cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString())
                            'End If
                            With cmv
                                .ID = "cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = "Incorrect data type for " & dr("Caption").ToString()
                                .[Operator] = ValidationCompareOperator.DataTypeCheck
                                .Type = GetValidatorType(dr("FldType"))
                            End With

                            ctl2.Controls.Add(cmv)
                        Catch ex As System.Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)


                        End Try



                    End If 'Set MaxLength for TextBox control
                Next


            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                If Not ex.InnerException Is Nothing Then
                    Throw New BaseException("Error required fields info for ResourceId " & ResourceId & " - " & ex.InnerException.Message)
                Else
                    Throw New BaseException("Error required fields info for ResourceId " & ResourceId & " - " & ex.Message)
                End If

            End Try

        End Sub
        Public Sub SetBtnStatePopups(ByVal Form As HtmlForm, ByVal strMode As String, Optional ByVal pObj As UserPagePermissionInfo = Nothing)
            Dim ctrlNew As Control
            Dim ctrlDelete As Control
            Dim ctrlSave As Control

            Select Case strMode

                Case "NEW"
                    'Try
                    '    ctrlNew = Form.FindControl("btnNew")
                    'Catch ex As System.Exception
                     '	Dim exTracker = new AdvApplicationInsightsInitializer()
                    '	exTracker.TrackExceptionWrapper(ex)

                    '    Throw New BaseException("Could not find Control:" & ex.Message)
                    'End Try
                    'DirectCast(ctrlNew, Button).Enabled = False
                    Try
                        ctrlDelete = Form.FindControl("btnDelete")
                        ctrlNew = Form.FindControl("btnNew")
                        ctrlSave = Form.FindControl("btnSave")
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        Throw New BaseException("Could not find Control:" & ex.Message)
                    End Try

                    'This is here just to keep the old code from crashing. Eventually we can
                    'remove it
                    If pObj Is Nothing Then
                        DirectCast(ctrlDelete, Button).Enabled = False
                    End If

                    'This is the new section of code that has the permissions handling
                    If Not pObj Is Nothing Then
                        If pObj.HasFull Or pObj.HasAdd Then
                            DirectCast(ctrlSave, Button).Enabled = True
                            DirectCast(ctrlNew, Button).Enabled = True
                        Else
                            DirectCast(ctrlSave, Button).Enabled = False
                            DirectCast(ctrlNew, Button).Enabled = False
                        End If

                        DirectCast(ctrlDelete, Button).Enabled = False
                    End If

                Case "EDIT"
                    Try
                        ctrlDelete = Form.FindControl("btnDelete")
                        ctrlNew = Form.FindControl("btnNew")
                        ctrlSave = Form.FindControl("btnSave")
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        Throw New BaseException("Could not find Control:" & ex.Message)
                    End Try

                    'This is here just to keep the old code from crashing. Eventually we can
                    'remove it
                    If pObj Is Nothing Then
                        DirectCast(ctrlNew, Button).Enabled = True
                    End If

                    If pObj Is Nothing Then
                        DirectCast(ctrlDelete, Button).Enabled = True
                    End If

                    'This is the new section of code that has the permissions handling
                    If Not pObj Is Nothing Then
                        If pObj.HasFull Or pObj.HasEdit Then
                            DirectCast(ctrlSave, Button).Enabled = True
                        Else
                            DirectCast(ctrlSave, Button).Enabled = False
                        End If

                        If pObj.HasFull Or pObj.HasDelete Then
                            DirectCast(ctrlDelete, Button).Enabled = True
                        Else
                            DirectCast(ctrlDelete, Button).Enabled = False
                        End If

                        If pObj.HasFull Or pObj.HasAdd Then
                            DirectCast(ctrlNew, Button).Enabled = True
                        Else
                            DirectCast(ctrlNew, Button).Enabled = False
                        End If
                    End If


            End Select
        End Sub


        ' DE6156 01/12/2012 Janet Robinson Added new method to handle special processing for Enroll Leads
        Public Sub SetCaptionsAndColorRequiredFieldsForEnrollLeads(ByVal pgContentPlaceHolder As WebControls.ContentPlaceHolder, Optional ByVal LeadGroup As String = "NULL", Optional ByVal InternationalAddress As Boolean = False)
            '**************************************************************************************************
            'Purpose:       When a page is loaded this sub is called to enter the captions and color the
            '               required fields.
            'Parameters:
            '[Form]         The form on the page that contains the asp.net server controls
            'Returns:       N/A
            'Created:       Troy Richards, 2/15/2005
            'Notes:         This sub relies on the ResourceId placed in  the httpcontext.items collection when
            '               when a request for the page is made. See the Application_BeginRequest sub in the
            '               Global.aspx.vb page.
            '**************************************************************************************************
            'Dim ds As New DataSet
            Dim dtResDef, dtDDReqFlds, dtResReqFlds, dtIPEDSFields As DataTable
            Dim dr As DataRow
            Dim strControl As String
            Dim strPrefix As String
            'Dim intResourceId As Integer
            Dim ResourceId As Integer
            Dim intCounter As Integer = 0
            Dim sLangName As String
            Dim fac As New PageSetupFacade
            Dim ctl, ctl2 As Control
            Dim strFldLen As String
            Dim strCaption As String = String.Empty
            Dim intIndx As Integer = 0

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            ResourceId = m_Context.Items("ResourceId")
            sLangName = m_Context.Items("Language")
            sLangName = sLangName.ToUpper



            Try
                'Get the resource definition table for this page
                dtResDef = fac.GetResourceDef(ResourceId, sLangName)

                'Get the data dictionary required fields
                dtDDReqFlds = fac.GetDataDictionaryReqFields

                'Get the school required fields for the resource being viewed
                dtResReqFlds = fac.GetSchlRequiredFieldsForResource(ResourceId)

                'Get the IPEDS fields
                dtIPEDSFields = New DataTable
                If MyAdvAppSettings.AppSettings("IPEDS").ToLower = "yes" Then
                    dtIPEDSFields = fac.GetIPEDSFields
                End If


                'Create PKs
                'Commented By Balaji on 3/19/2005 to avoid the problem 
                'with Address Status and Phone Status
                'With dtResDef
                '    .PrimaryKey = New DataColumn() {dtResDef.Columns("FldId")}
                'End With

                'With dtDDReqFlds
                '    .PrimaryKey = New DataColumn() {dtDDReqFlds.Columns("FldId")}
                'End With

                'With dtResReqFlds
                '    .PrimaryKey = New DataColumn() {dtResReqFlds.Columns("FldId")}
                'End With

                With dtIPEDSFields
                    .PrimaryKey = New DataColumn() {dtIPEDSFields.Columns("FldId")}
                End With
                'We need to loop through the records in the dtResDef datatable. 
                'First we will set up the captions and then deal with the required fields.                'is a required field we need to color the associated control and add a
                'For required fields we will color them appropriately and set up a RequiredFieldValidator.
                For Each dr In dtResDef.Rows
                    intCounter += 1
                    'If the ControlName field is not null then we have to use the control specified
                    'by that field.
                    Dim ctrlname As String = dr("FldName").ToString
                    Dim fldid As String = dr("FldId").ToString
                    If Not dr.IsNull("ControlName") Then
                        strControl = dr("ControlName").ToString()
                        strPrefix = strControl.Substring(0, 3)
                    Else
                        'If the DDLId associated with the record is null then we are
                        'dealing with a textbox or a checkbox. If not we are dealing with a dropdownlist.
                        'A checkbox normally is associated with a bit field in the database.
                        If dr.IsNull("DDLId") Then
                            If dr("FldType") = "Bit" Then 'Checkbox
                                strPrefix = "chk"
                            Else 'Textbox
                                strPrefix = "txt"
                            End If
                        Else
                            strPrefix = "ddl"
                        End If
                        strControl = strPrefix & dr("FldName").ToString()
                    End If

                    ctl = pgContentPlaceHolder.FindControl(strControl)
                    If ctl Is Nothing Then
                        Throw New System.Exception("Could not find the control: " & strControl)
                    End If
                    ctl2 = pgContentPlaceHolder.FindControl("pnlRequiredFieldValidators")
                    If ctl2 Is Nothing Then
                        Throw New System.Exception("Could not find panel named pnlRequiredFieldValidators")
                    End If
                    strFldLen = dr("FldLen").ToString()

                    'If we are dealing with a textbox or a ddl then we need
                    'to check if there is an associated label control.
                    If strPrefix = "txt" Or strPrefix = "ddl" Then
                        Dim ctl3 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("FldName").ToString())
                        'The control might not have a label control associated with it.
                        If Not ctl3 Is Nothing Then
                            Try
                                If ResourceId = 174 And fldid = 81 Then
                                    'DE7172 2/16/2012 Janet Robinson had to override caption because ipeds using different field
                                ElseIf ResourceId = 174 And fldid = 728 Then
                                    CType(ctl3, Label).Text = "Family Income"
                                Else
                                    CType(ctl3, Label).Text = Trim(dr("Caption"))
                                End If

                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Throw New BaseException("Problem casting " & strControl & " to label " & "with caption " & dr("Caption").ToString)
                            End Try
                        End If
                        'The label control might be named after the name of the ddl or textbox control.
                        'This should only be performed when the controlname is specified
                        If Not dr.IsNull("ControlName") Then
                            Dim ctl30 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("ControlName").ToString().Substring(3))
                            If Not ctl30 Is Nothing Then
                                Try
                                    If ResourceId = 174 And fldid = 81 Then
                                        'DE7172 2/16/2012 Janet Robinson had to override caption because ipeds using different field
                                    ElseIf ResourceId = 174 And fldid = 728 Then
                                        CType(ctl30, Label).Text = "Family Income"
                                    Else
                                        CType(ctl30, Label).Text = Trim(dr("Caption"))
                                    End If
                                Catch ex As System.Exception
                                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                                	exTracker.TrackExceptionWrapper(ex)

                                    Throw New BaseException("Problem casting " & strControl & " to label " & "with caption " & dr("Caption").ToString)
                                End Try
                            End If
                        End If

                    ElseIf strPrefix = "chk" Then 'Checkbox
                        'We have to find the checkbox itself
                        Dim ctl3 As Control = pgContentPlaceHolder.FindControl("chk" & dr("FldName").ToString())
                        'If the checkbox was not found then we should notify the client
                        If ctl3 Is Nothing Then
                            Throw New System.Exception("Could not find checkbox:" & strControl)
                        End If
                        'The checkbox might be found but we might have a problem
                        'in the casting operation.
                        If Not ctl3 Is Nothing Then
                            Try
                                '  If Not ctrlname = "LeadGrpId" Then
                                CType(ctl3, CheckBox).Text = dr("Caption")
                                '  End If
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                                Throw New BaseException("Problem casting " & strControl & " to checkbox " & "with caption " & dr("Caption").ToString)
                            End Try
                        End If
                    End If

                    'Color the required fields.
                    Dim req As Boolean = dr("Required")

                    'If dr("Required") = True Or ValExistsInDTString(dtDDReqFlds, dr("FldId")) Or TblFldsIdExistsInDT(dtResReqFlds, dr("TblFldsId")) Or ((ResourceId = 203 Or ResourceId = 169 Or ResourceId = 206 Or ResourceId = 170) And (SingletonAppSettings.AppSettings("IPEDS").ToLower = "yes" And ValExistsInDT(dtIPEDSFields, dr("FldId")))) Then
                    'DE6156 2/28/2012 Janet Robinson new logic for handling required fields based on ipeds setting
                    'If dr("Required") = True Or ValExistsInDTString(dtDDReqFlds, dr("FldId")) Or (SingletonAppSettings.AppSettings("IPEDS").ToLower = "no" And TblFldsIdExistsInDT(dtResReqFlds, dr("TblFldsId"))) Or (SingletonAppSettings.AppSettings("IPEDS").ToLower = "yes" And ValExistsInDT(dtIPEDSFields, dr("FldId"))) Then
                    'B. Shanblatt 12/12/2012
                    'Make sure all school defined fields are set for validation
                    'DE8768
                    If dr("Required") = True _
                        Or ValExistsInDTString(dtDDReqFlds, dr("FldId")) _
                        Or ValExistsInDTString(dtResReqFlds, dr("FldId")) _
                        Or (MyAdvAppSettings.AppSettings("IPEDS").ToLower = "no" And TblFldsIdExistsInDT(dtResReqFlds, dr("TblFldsId"))) _
                        Or (MyAdvAppSettings.AppSettings("IPEDS").ToLower = "yes" And ValExistsInDt(dtIPEDSFields, dr("FldId"))) Then
                        Try
                            'If international address is checked and its a state field then do not mark it as required
                            'Balaji 03/04/2011
                            If InternationalAddress = True And CInt(dr("FldId")) = 11 Then
                                Exit Try
                            End If
                            If strPrefix = "txt" Then
                                If TypeOf ctl Is Telerik.Web.UI.RadDatePicker Then
                                ElseIf TypeOf ctl Is TextBox Then
                                    '  CType(ctl, TextBox).BackColor = Color.FromName("#ffff99")
                                End If

                            ElseIf strPrefix = "ddl" Then
                                If TypeOf ctl Is Telerik.Web.UI.RadComboBox Then
                                    '  CType(ctl, Telerik.Web.UI.RadComboBox).BackColor = Color.FromName("#ffff99")
                                Else
                                    'Enroll Leads page, International Address and State Field
                                    'If ResourceId = 174 And InternationalAddress = True And CInt(dr("FldId")) = 11 Then
                                    'DE5166 Janet Robinson 05/18/2011 Added ResourceID 203 (Student Master)
                                    'If (ResourceId = 174 Or ResourceId = 203) And CInt(dr("FldId")) = 11 Then
                                    'If (ResourceId = 203) And CInt(dr("FldId")) = 11 Then
                                    '    CType(ctl, DropDownList).BackColor = Color.FromName("White")
                                    'Else
                                    '   CType(ctl, DropDownList).BackColor = Color.FromName("#ffff99")
                                    'End If
                                End If
                            End If

                            Dim ctl3 As Control = pgContentPlaceHolder.FindControl("lbl" & dr("FldName").ToString())
                            'The control might not have a label control associated with it.
                            If Not ctl3 Is Nothing Then
                                If (ResourceId = 174) And CInt(dr("FldId")) = 81 Then
                                    strCaption = CType(ctl3, Label).Text
                                    intIndx = strCaption.IndexOf("<")
                                    If intIndx > -1 Then
                                        strCaption = strCaption.Substring(0, intIndx)
                                    End If
                                    'DE7172 2/16/2012 Janet Robinson had to override caption because ipeds using different field
                                ElseIf ResourceId = 174 And fldid = 728 Then
                                    strCaption = "Family Income"
                                Else
                                    strCaption = dr("Caption")
                                End If
                                CType(ctl3, Label).Text = Trim(strCaption) & "<font color=""red"">*</font>"

                            End If
                        Catch ex As System.Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            Throw New System.Exception
                        End Try

                        'Add a RequiredFieldVaidator to monitor the control.
                        If strPrefix = "txt" Or strPrefix = "rad" Then
                            Dim rfv As New RequiredFieldValidator
                            With rfv
                                .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = dr("Caption").ToString & " is required"
                            End With
                            ctl2.Controls.Add(rfv)
                        End If

                        'Add a regular expression validator to monitor the control.
                        If strPrefix = "ddl" Then
                            'add a required field validator

                            Try
                                'If international address is checked and its a state field then do not mark it as required
                                'Balaji 03/04/2011
                                If InternationalAddress = True And CInt(dr("FldId")) = 11 Then
                                    Exit Try
                                End If
                                'If ResourceId = 174 And CInt(dr("FldId")) = 11 Then
                                '    Exit Try 'Don't add required validator for State Field - Balaji 03/09/2011
                                'End If

                                Dim rfv As New RequiredFieldValidator
                                With rfv
                                    If CInt(dr("FldId")) = 11 Then
                                        .ID = "rfv" & dr("FldName").ToString()
                                    Else
                                        .ID = "rfv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                    End If
                                    .ControlToValidate = strControl
                                    .Display = ValidatorDisplay.None

                                    If (ResourceId = 174) And (CInt(dr("FldId")) = 81) Then
                                        .ErrorMessage = strCaption & " is required"
                                        'DE7172 2/16/2012 Janet Robinson had to override caption because ipeds using different field
                                    ElseIf (ResourceId = 174) And (CInt(dr("FldId")) = 728) Then
                                        .ErrorMessage = "Family Income is required"
                                    Else
                                        .ErrorMessage = Trim(dr("Caption")).ToString & " is required"
                                    End If

                                    .InitialValue = ""
                                    .EnableClientScript = True
                                End With
                                ctl2.Controls.Add(rfv)

                                'add a compare validator
                                Dim cv As New CompareValidator
                                With cv
                                    If CInt(dr("FldId")) = 11 Then
                                        .ID = "cv" & dr("FldName").ToString()
                                    Else
                                        .ID = "cv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                    End If
                                    .ControlToValidate = strControl
                                    .Display = ValidatorDisplay.None
                                    'DE7172 2/16/2012 Janet Robinson had to override caption because ipeds using different field
                                    If (ResourceId = 174) And (CInt(dr("FldId")) = 81) Then
                                        .ErrorMessage = strCaption & " is required"
                                        'DE7172 2/16/2012 Janet Robinson had to override caption because ipeds using different field
                                    ElseIf (ResourceId = 174) And (CInt(dr("FldId")) = 728) Then
                                        .ErrorMessage = "Family Income is required"
                                    Else
                                        .ErrorMessage = dr("Caption").ToString & " is required"
                                    End If
                                    .ValueToCompare = Guid.Empty.ToString
                                    .Operator = ValidationCompareOperator.NotEqual
                                    .EnableClientScript = True
                                End With
                                ctl2.Controls.Add(cv)
                            Catch ex As System.Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                            End Try
                        End If

                    End If 'Control is required

                    'If the control is a textbox we need to also set the MaxLength property
                    'and also add a CompareValidator to perform a data type check. 
                    If strPrefix = "txt" Then
                        If TypeOf ctl Is WebControls.TextBox Then
                            If dr("FldType") = "Varchar" Or dr("FldType") = "Char" Then
                                CType(ctl, TextBox).MaxLength = CInt(dr("FldLen"))
                            ElseIf TypeOf (ctl) Is Telerik.Web.UI.RadDatePicker Then

                            Else
                                CType(ctl, TextBox).MaxLength = 50
                            End If
                        End If
                        Dim cmv As New CompareValidator
                        Try
                            'If ctl2.FindControl("cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString()) Is True Then
                            '    ctl2.Controls.Remove("cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString())
                            'End If
                            With cmv
                                .ID = "cmv" & dr("FldName").ToString() & dr("TblFldsId").ToString()
                                .ControlToValidate = strControl
                                .Display = ValidatorDisplay.None
                                .ErrorMessage = "Incorrect data type for " & dr("Caption").ToString()
                                .[Operator] = ValidationCompareOperator.DataTypeCheck
                                .Type = GetValidatorType(dr("FldType"))
                            End With

                            ctl2.Controls.Add(cmv)
                        Catch ex As System.Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)


                        End Try



                    End If 'Set MaxLength for TextBox control
                Next


            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                If Not ex.InnerException Is Nothing Then
                    Throw New BaseException("Error required fields info for ResourceId " & ResourceId & " - " & ex.InnerException.Message)
                Else
                    Throw New BaseException("Error required fields info for ResourceId " & ResourceId & " - " & ex.Message)
                End If

            End Try

        End Sub

    End Class
    Public Class UniqueIndexException
        Inherits System.Exception
        Sub New()
            MyBase.New()
        End Sub

        Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        Sub New(ByVal message As String, ByVal inner As System.Exception)
            MyBase.New(message, inner)
        End Sub
    End Class
    Public Class ReferentialIntegrityException
        Inherits System.Exception
        Sub New()
            MyBase.New()
        End Sub

        Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        Sub New(ByVal message As String, ByVal inner As System.Exception)
            MyBase.New(message, inner)
        End Sub
    End Class
    Public Class DALExceptions
        Public Shared Function ExceptionType(ByVal ex As OleDb.OleDbException) As String

            '   check for a Referential Integrity Exception
            If ex.Errors(ex.Errors.Count - 1).NativeError.ToString = "547" Then Return "547"

            '   check for a Unique Index Violation Exception
            If ex.Errors(ex.Errors.Count - 1).NativeError.ToString = "2601" Then Return "2601"

            '   It is other type of exception
            Return "otherError"

        End Function
        'Public Shared Function BuildReferentialIntegrityExceptionMessage(ByVal ex As ReferentialIntegrityException) As String

        '    '   build message
        '    Dim message As New StringBuilder
        '    message.Append("You can not delete this item because it is being referenced in " + ChildTable(ex) + " Table.")

        '    '   return message
        '    Return message.ToString

        'End Function
        Public Shared Function BuildUniqueIndexExceptionMessage(ByVal ex As UniqueIndexException) As String

            '   build message
            Dim message As New StringBuilder
            message.Append(GetIndexName(ex))

            '   return message
            Return message.ToString

        End Function

        Private Shared Function GetIndexName(ByVal ex As UniqueIndexException) As String
            Dim i As Integer = ex.GetBaseException.Message.LastIndexOf("' with unique index '") + 22
            Dim j As Integer = ex.GetBaseException.Message.IndexOf("]", i + 1)
            Return ex.GetBaseException.Message.Substring(i, j - i)
        End Function
        Public Shared Function BuildErrorMessage(ByVal ex As OleDb.OleDbException) As String

            '   this error is specific for the OLeDb Provider of MSSQL. 
            If ex.ErrorCode = -2147217873 Then
                '   get error type
                Select Case ExceptionType(ex)
                    Case "547"  '   this is a referential integrity exception
                        Return BuildReferentialIntegrityExceptionMessage(New ReferentialIntegrityException("Referential Integrity Exception", ex))
                    Case "2601" '   this is a unique index violation exception
                        Return DALExceptions.BuildUniqueIndexExceptionMessage(New UniqueIndexException("Unique Index Exception", ex))
                    Case "2627" '   this is a Violation of Primary Key constraint.
                        Return DALExceptions.BuildPrimaryKeyConstraintExceptionMessage(New PrimaryKeyConstraintException("Primary Key Violation Exception", ex))
                    Case Else   '   this is other OleDB exception.
                        Return ex.Message
                End Select
            Else
                Return ex.Message
            End If
        End Function
        Public Shared Function BuildReferentialIntegrityExceptionMessage(ByVal ex As ReferentialIntegrityException) As String

            '   build message
            Dim message As New StringBuilder
            message.Append("You can not delete this item because it is being referenced in " + ChildTable(ex) + " Table.")

            '   return message
            Return message.ToString

        End Function
        Private Shared Function BuildPrimaryKeyConstraintExceptionMessage(ByVal ex As PrimaryKeyConstraintException) As String

            '   build message
            Dim message As New StringBuilder
            message.Append("The record that you are trying to add to the " + GetTableName(ex) + " table already exists.")

            '   return message
            Return message.ToString

        End Function
        Private Shared Function ChildTable(ByVal ex As ReferentialIntegrityException) As String
            Dim i As Integer = ex.GetBaseException.Message.LastIndexOf("', table '") + 12
            Dim j As Integer = ex.GetBaseException.Message.IndexOf("'", i + 1)
            Return ex.GetBaseException.Message.Substring(i, j - i)
        End Function
        Private Shared Function GetTableName(ByVal ex As PrimaryKeyConstraintException) As String
            Dim i As Integer = ex.GetBaseException.Message.LastIndexOf(" key in object '") + 18
            Dim j As Integer = ex.GetBaseException.Message.IndexOf("'", i + 1)
            Return ex.GetBaseException.Message.Substring(i, j - i)
        End Function
    End Class
    Public Class PrimaryKeyConstraintException
        Inherits System.Exception
        Sub New()
            MyBase.New()
        End Sub

        Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub

        Sub New(ByVal message As String, ByVal inner As System.Exception)
            MyBase.New(message, inner)
        End Sub
    End Class
End Namespace
