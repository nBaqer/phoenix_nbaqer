' ===============================================================================
'
' FAME AdvantageV1
'
' CommonWebUtilities.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports System.Xml
Imports System.IO
Imports System.Net
Imports System.Data
Imports System.Data.OleDb
Imports System.Web.Configuration
Imports FAME.DataAccessLayer
Imports System.Reflection
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.BusinessFacade.MSG
Imports System.Net.Mail
Imports Telerik.Web.UI


Public Class CommonWebUtilities
    Public Shared moduleNames() As String = {"Academic Records", "Admissions", "Faculty", "Financial Aid", "Human Resources", "Placement", "Student Accounts", "System", "Choose Module"}
    Public Shared moduleCodes() As String = {"AR", "AD", "FC", "FA", "HR", "PL", "SA", "SY", "UK"}

    'Public Shared Sub SetStyleToSelectedItem(ByVal dataList As DataList, ByVal selectedItem As String, ByVal viewState As StateBag, Optional ByVal header As Header = Nothing)
    Public Shared Sub SetStyleToSelectedItem(ByVal dataList As DataList, ByVal selectedItem As String, ByVal viewState As StateBag)
        '  Dim context As HttpContext
        '   check if a previous item exists
        If Not viewState(dataList.ID) Is Nothing Then
            '   restore values of the previous selected item
            RestoreItemValues(dataList, CType(viewState(dataList.ID), String))
        End If

        '   set the style to the the selectedItem and save itemSelected in the viewstate
        SetItemValues(dataList, selectedItem)
        viewState(dataList.ID) = selectedItem

    End Sub



    Public Shared Sub SetFocus(ByVal mypage As Page, ByVal ctrl As Control)
        ' Define the JavaScript function for the specified control.
        Dim cs As ClientScriptManager = mypage.ClientScript
        Dim csType As Type = mypage.GetType

        Dim focusScript As String = "<script language='javascript'>" & _
          "document.getElementById('" + ctrl.ClientID & _
          "').focus();</script>"

        ' Add the JavaScript code to the page.
        'start comment by balaji on 10/4/2006 to use RegisterClientScriptBlock instead of old RegisterStartupScript
        'mypage.RegisterStartupScript("FocusScript", focusScript)
        'end comment 
        cs.RegisterStartupScript(csType, "FocusScript", focusScript)
    End Sub
    Private Shared Sub SetItemValues(ByVal dataList As DataList, ByVal selectedItem As String)
        Dim linkButton As LinkButton
        Dim idxsOfLinkButtons(10) As Integer
        Dim k = 0
        '   loop through all datalist items 
        '   check only linkButtons
        For i As Integer = 0 To dataList.Items.Count - 1
            '   the first time get an array with the indexes of all linkbuttons
            If i = 0 Then
                For j = 0 To dataList.Items(i).Controls.Count - 1
                    If dataList.Items(i).Controls(j).GetType.ToString = "System.Web.UI.WebControls.LinkButton" Then
                        idxsOfLinkButtons(k) = j
                        k += 1
                    End If
                Next
                'ReDim idxsOfLinkButtons(k - 1)
            End If
            For j = 0 To k - 1
                linkButton = CType(dataList.Items(i).Controls(idxsOfLinkButtons(j)), LinkButton)
                If linkButton.CommandArgument = selectedItem Then
                    linkButton.CssClass = dataList.ItemStyle.CssClass
                    '   apply style to all controls in that item
                    For Each ctl As Control In dataList.Items(i).Controls
                        '   do not change CssClass to Repeaters
                        Try
                            If ctl.GetType.ToString.IndexOf(".WebControls.") > 0 And ctl.GetType.ToString.IndexOf(".WebControls.Repeater") < 0 Then
                                CType(ctl, WebControl).CssClass = dataList.SelectedItemStyle.CssClass
                            End If
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                        End Try

                    Next
                    Exit Sub
                End If
            Next
        Next
    End Sub
    Public Shared Sub RestoreItemValues(ByVal dataList As DataList, ByVal selectedItem As String)
        Dim linkButton As LinkButton
        Dim idxsOfLinkButtons(10) As Integer
        Dim k As Integer = 0
        '   loop through all datalist items 
        '   check only linkButtons
        For i As Integer = 0 To dataList.Items.Count - 1
            '   the first time get an array with the indexes of all linkbuttons
            If i = 0 Then
                For j As Integer = 0 To dataList.Items(i).Controls.Count - 1
                    If dataList.Items(i).Controls(j).GetType.ToString = "System.Web.UI.WebControls.LinkButton" Then
                        idxsOfLinkButtons(k) = j
                        k += 1
                    End If
                Next
                'ReDim idxsOfLinkButtons(k - 1)
            End If
            For j As Integer = 0 To k - 1
                linkButton = CType(dataList.Items(i).Controls(idxsOfLinkButtons(j)), LinkButton)
                If linkButton.CommandArgument = selectedItem Then
                    linkButton.CssClass = dataList.ItemStyle.CssClass

                    '   apply style to all controls in that item
                    For Each ctl As Control In dataList.Items(i).Controls
                        '   do not change CssClass to Repeaters
                        Try
                            If Not (ctl.GetType() = GetType(HiddenField)) Then
                                If ctl.GetType.ToString.IndexOf(".WebControls.", StringComparison.Ordinal) > 0 _
                                    And ctl.GetType.ToString.IndexOf(".WebControls.Repeater", StringComparison.Ordinal) < 0 Then
                                    CType(ctl, WebControl).CssClass = dataList.ItemStyle.CssClass
                                End If
                            End If
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)


                        End Try

                    Next
                    'linkButton.ForeColor = Color.Red
                    linkButton.Font.Bold = True
                    'Exit Sub
                Else
                    linkButton.Font.Bold = False
                End If
            Next
        Next
    End Sub
    Public Shared Sub DisplayInfoInMessageBox(ByVal page As Page, ByVal infoMessage As String)
        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType()

        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=DisplayError();function DisplayError(){MasterPage.SHOW_INFO_WINDOW('"
            Dim scriptEnd As String = "');}</script>"
            cs.RegisterStartupScript(csType, "ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(infoMessage) + scriptEnd)
        End If
    End Sub
    Public Shared Sub DisplayWarningInMessageBox(ByVal page As Page, ByVal warningMessage As String)
        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType()

        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=DisplayError();function DisplayError(){MasterPage.SHOW_WARNING_WINDOW('"
            Dim scriptEnd As String = "');}</script>"
            cs.RegisterStartupScript(csType, "ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(warningMessage) + scriptEnd)
        End If
    End Sub
    Public Shared Sub DisplayErrorInMessageBox(ByVal page As Page, ByVal errorMessage As String)
        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType()

        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=DisplayError();function DisplayError(){MasterPage.SHOW_ERROR_WINDOW('"
            Dim scriptEnd As String = "');}</script>"
            cs.RegisterStartupScript(csType, "ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + scriptEnd)
        End If
    End Sub
    Public Shared Sub AlertAjax(msg As String, ByRef ajaxManager As RadAjaxManager)
        msg = msg.Replace(Environment.NewLine, "\n")
        msg = msg.Replace("'", String.Empty)
        msg = "window.alert('" & msg & "')"
        ajaxManager.ResponseScripts.Add(msg)
    End Sub


    Public Shared Sub DisplayErrorInClientMessageBox(ByVal page As Page, ByVal errorMessage As String)
        'Modified By Balaji on 09/29/2006 to test Atlas
        'as DisplayInMessageBox was not working with Atlas and page.RegisterStartupScript has become obsolete
        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType

        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=DisplayError();function DisplayError(){alert('"
            Dim scriptEnd As String = "');}</script>"

            'Start Comment --- Code commented by balaji on 09/29/2006
            ' page.RegisterStartupScript("ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + scriptEnd)
            'End Comment
            'cs.RegisterStartupScript(csType, "ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + scriptEnd)
            cs.RegisterClientScriptBlock(csType, "ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + scriptEnd)
        End If
    End Sub
    Public Shared Sub DisplayInMessageBox(ByVal page As Page, ByVal errorMessage As String, ByVal DocumentType As String)
        'Modified By Balaji on 09/29/2006 to test Atlas
        'as DisplayInMessageBox was not working with Atlas and page.RegisterStartupScript has become obsolete
        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType

        'ByVal DocumentType As String, ByVal StudentName As String, ByVal StudentId As String, ByVal Extension As String
        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=OpenDocuments1();function OpenDocuments1(){window.open("
            Dim scriptEnd As String = "');}</script>"
            '		var stralert = '../PL/Test1.aspx?DocumentType=' + strDocumentType +'&StudentName=' + strStudentName + '&StudentId=' + strStudentId + '&Extension=' + strExtension;
            Dim strOpenString As String = "'Test1.aspx?FileName="
            Dim strFileName As String = HttpUtility.UrlEncode(errorMessage)
            Dim strDocParam As String = "&DocumentType="
            Dim strDocumentType As String = DocumentType
            Dim strWindowName As String = "','HistWin','width=700,height=600,resizable=yes"
            '   Register a javascript to display error message
            cs.RegisterStartupScript(csType, "ErrorMessage", scriptBegin + strOpenString + strFileName + strDocParam + strDocumentType + strWindowName + scriptEnd)
        End If
    End Sub


    Public Shared Sub PromptTextBox(ByVal page As Page, ByVal promptMessage As String, ByVal formName As String, ByVal txtBoxName As String)
        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType

        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            Dim sb As New StringBuilder("<script type='text/javascript'>window.Onload=SaveUserInput();")
            With sb
                .Append("function SaveUserInput(){ var strUserInput = prompt('")
                .Append(promptMessage)
                .Append("','');if ((strUserInput) && (strUserInput != '')){")
                .Append("document.forms['")
                .Append(formName)
                .Append("'].elements['")
                .Append(txtBoxName)
                .Append("'].value = strUserInput;__doPostBack('lbtPrompt','');}")
                .Append("}</script>")
            End With

            '   Register a javascript to prompt user
            cs.RegisterStartupScript(csType, "PromptUser", sb.ToString)
            'page.RegisterStartupScript("PromptUser", sb.ToString)
            'page.RegisterStartupScript("ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + scriptEnd)
        End If
    End Sub
    Public Shared Sub PromptDefaultValueTextBox(ByVal page As Page, ByVal promptMessage As String, ByVal formName As String, ByVal txtBoxName As String)
        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType
        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            Dim sb As New StringBuilder("<script type='text/javascript'>window.Onload=SaveUserInput();")
            With sb
                .Append("function SaveUserInput(){")
                .Append("var mydefault = ")
                .Append("document.forms['")
                .Append(formName)
                .Append("'].elements['")
                .Append(txtBoxName)
                .Append("'].value" & ";")
                '.Append("alert('Default value is '+ mydefault);")
                .Append("var strUserInput = prompt('")
                .Append(promptMessage)
                .Append("', mydefault);")
                .Append("if ((strUserInput) && (strUserInput != '') && (strUserInput != mydefault)){")
                '.Append("alert('New value is '+ strUserInput);")
                .Append("document.forms['")
                .Append(formName)
                .Append("'].elements['")
                .Append(txtBoxName)
                .Append("'].value = strUserInput;")
                .Append("__doPostBack('lbtPrompt','');}")
                .Append("else{")
                .Append("if ((strUserInput == mydefault)){")
                '.Append("alert('Default value did not change');")
                .Append("__doPostBack('lbtPrompt','');}}")
                .Append("}</script>")
            End With

            '   Register a javascript to prompt user
            cs.RegisterStartupScript(csType, "PromptUserWithDefaultValue", sb.ToString)
            'page.RegisterStartupScript("PromptUserWithDefaultValue", sb.ToString)
            'page.RegisterStartupScript("ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + scriptEnd)
        End If
    End Sub

    Public Shared Sub CloseWindow(ByVal page As Page, ByVal metype As Type)
        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            '   this is the beginning of the javascript code  
            Const scriptBegin As String = "<script type='text/javascript'>window.Onload=CloseWindow();function CloseWindow(){"

            '   this is the end of the javascript code
            Const scriptEnd As String = "window.close()}</script>"

            '   Register the javascript code
            page.ClientScript.RegisterStartupScript(metype, "CloseWindow", scriptBegin + scriptEnd, False)

            'page.RegisterStartupScript("CloseWindow", scriptBegin + scriptEnd)

        End If
    End Sub

    Public Shared Sub OpenWindowAndPassParameters(ByVal page As Page, ByVal url As String, ByVal name As String, ByVal winSettings As String, ByVal parametersArray() As String)
        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType

        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            '   this is the beginning of the javascript
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=OpenChild();function OpenChild(){theChild=window.open('" + url

            '   this is the middle of the javascript
            '   build the parameters list to be send to the child page in the querystring
            Dim scriptMiddle As String = ""
            If Not parametersArray Is Nothing Then
                For i As Integer = 0 To parametersArray.Length - 1
                    scriptMiddle += "&" + parametersArray(i) + "="
                Next
            End If

            '   this is the end of the javascript
            Dim scriptEnd As String = "','" + name + "','" + winSettings + "');}</script>"
            cs.RegisterClientScriptBlock(csType, "OpenWindowAndPassParameters", scriptBegin + scriptMiddle + scriptEnd)
        End If
    End Sub


    Public Shared Sub OpenChildWindow2(ByVal page As Page, ByVal url As String, ByVal name As String, ByVal winSettings As String, request As HttpRequest)

        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType
        Dim myBrowserCaps As HttpBrowserCapabilities = request.Browser
        If (CType(myBrowserCaps, HttpCapabilitiesBase)).EcmaScriptVersion.Major > 1 Then

            '   this is the beginning of the javascript
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=OpenChild();function OpenChild(){theChild=window.open('" + ReplaceSpecialCharactersInJavascriptMessage(url)

            '   this is the end of the javascript
            Dim scriptEnd As String = "','" + name + "','" + winSettings + "');theChild.focus();}</script>"

            '   Register the javascript code
            cs.RegisterStartupScript(csType, "OpenChildWindow", scriptBegin + scriptEnd)

        End If
    End Sub

    Public Shared Sub OpenChildWindow(ByVal page As Page, ByVal url As String, ByVal name As String, ByVal winSettings As String)

        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType
        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            '   this is the beginning of the javascript
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=OpenChild();function OpenChild(){theChild=window.open('" + ReplaceSpecialCharactersInJavascriptMessage(url)

            '   this is the end of the javascript
            'Dim scriptEnd As String = "','" + name + "','" + winSettings + "');theChild.focus();return false;}</script>"
            Dim scriptEnd As String = "','" + name + "','" + winSettings + "');theChild.focus();}</script>"

            '   Register the javascript code
            'page.RegisterStartupScript("OpenChildWindow", scriptBegin + scriptEnd)
            cs.RegisterStartupScript(csType, "OpenChildWindow", scriptBegin + scriptEnd)

            '   add javascript to the Unload event of the body tag to close the child window
            Dim bodyTag As HtmlGenericControl = GetBodyTag(page)
            If Not bodyTag Is Nothing Then
                'bodyTag.Attributes.Add("OnUnload", "javascript:theChild=window.open('','" + name + "','" + "toolbar=no,status=no,resizable=no,width=0px,height=0px" + "');if(!(!theChild || theChild.closed))theChild.close()")
            End If
        End If
    End Sub
    Public Shared Sub OpenClientChildWindow(ByVal page As Page, ByVal url As String, ByVal name As String, ByVal winSettings As String)
        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType
        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            '   this is the beginning of the javascript
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=OpenChild();function OpenChild(){theChild=window.open('" + ReplaceSpecialCharactersInJavascriptMessage(url)

            '   this is the end of the javascript
            Dim scriptEnd As String = "','" + name + "','" + winSettings + "')}</script>"

            '   Register the javascript code
            'page.RegisterStartupScript("OpenChildWindow", scriptBegin + scriptEnd)
            cs.RegisterClientScriptBlock(csType, "OpenChildWindow1", scriptBegin + scriptEnd)

            '   add javascript to the Unload event of the body tag to close the child window
            Dim bodyTag As HtmlGenericControl = GetBodyTag(page)
            If Not bodyTag Is Nothing Then
                'bodyTag.Attributes.Add("OnUnload", "javascript:theChild=window.open('','" + name + "','" + "toolbar=no,status=no,resizable=no,width=0px,height=0px" + "');if(!(!theChild || theChild.closed))theChild.close()")
            End If
        End If
    End Sub
    Public Shared Sub CheckUncheckAllCheckBoxes(ByVal metype As Type, ByVal page As Page, ByVal masterCheckBox As CheckBox, ByVal aspCheckBoxID As String)
        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            '   this is the javascript
            Dim script As New StringBuilder
            With script
                .Append("<script type='text/javascript'>")
                .Append(vbCrLf)
                .Append("<!-- ")
                .Append(vbCrLf)
                .Append("function ChkUnchkAllCheckBoxes(aspCheckBoxID, checkVal) {re = new RegExp( aspCheckBoxID + '$'); for(i = 0; i < document.forms[0].elements.length; i++) {elm = document.forms[0].elements[i]; if (elm.type == 'checkbox') {if (re.test(elm.name)) {elm.checked = checkVal}}}} ")
                .Append(vbCrLf)
                .Append("//  End --> ")
                .Append(vbCrLf)
                .Append("</script>")
            End With

            '   Register the javascript code
            page.ClientScript.RegisterStartupScript(metype, "ChkUnchkAllCheckBoxes", script.ToString, False)
            'page.RegisterStartupScript("ChkUnchkAllCheckBoxes", script.ToString)

            '   add javascript to the OnClick event of the master CheckBox
            masterCheckBox.Attributes.Add("OnClick", "javascript:ChkUnchkAllCheckBoxes('" + aspCheckBoxID + "'," + masterCheckBox.ClientID + ".checked);")
        End If
    End Sub
    Private Shared Function GetBodyTag(ByVal page As Page) As HtmlGenericControl

        '   In order to locate the body tag it must have the attribute runat=server
        For Each control As Object In page.Controls
            If control.GetType Is GetType(HtmlGenericControl) Then
                If CType(control, HtmlGenericControl).TagName.ToLower = "body" Then Return CType(control, HtmlGenericControl)
            End If
        Next

        '   there is no body tag in the page or the body tag doesn't have the attribute runat=server
        Return Nothing

    End Function

    Private Shared Function ReplaceSpecialCharactersInJavascriptMessage(ByVal s As String) As String

        '   replace \ by \\, ' by \', " by "", CR by \r, LF by \n
        Return s.Trim().Replace("\", "\\").Replace("'", "\'").Replace("""", "\""").Replace(vbCr, "\r").Replace(vbLf, "\n")

    End Function
    Public Shared Function ConvertModuleNameToModuleCode(ByVal moduleName As String) As String
        For i As Integer = 0 To moduleNames.Length - 1
            If moduleName = moduleNames(i) Then Return moduleCodes(i)
        Next
        Return "UK"
    End Function
    Public Shared Function ConvertModuleNameToModuleId(ByVal moduleName As String) As Integer
        For i As Integer = 1 To moduleNames.Length
            If moduleName = moduleNames(i - 1) Then Return i
        Next
        Return 10
    End Function
    Public Shared Function ConvertModuleCodeToModuleId(ByVal moduleCode As String) As Integer
        For i As Integer = 1 To moduleCodes.Length
            If moduleCode = moduleCodes(i - 1) Then Return i
        Next
        Return 10
    End Function
    Public Shared Function ConvertModuleCodeToModuleName(ByVal moduleCode As String) As String
        For i As Integer = 0 To moduleNames.Length - 1
            If moduleCode = moduleCodes(i) Then Return moduleNames(i)
        Next
        Return "Choose Module"
    End Function
    <CLSCompliant(False)>
    Public Sub ProcessAddressChangeMessage(ByVal sender As Object, ByVal e As AdvantageMessagingEventArgs)
        Call (New FAME.AdvantageV1.BusinessFacade.MessagingFacade).AddAdvantageMessage(sender, e)
        'Call (New MessagingFacade).AddAdvantageMessage(sender, e)
    End Sub
    Public Shared Function GetFameXmlNamespaceManager(ByVal schemaPath As String) As XmlNamespaceManager

        'add xs: schemas Namespace and NamespaceManager
        Dim nsManager As XmlNamespaceManager = New XmlNamespaceManager(CType(New XmlTextReader(schemaPath), XmlTextReader).NameTable)
        nsManager.AddNamespace("fame", GetFAMEGLDistributionsURI())
        nsManager.AddNamespace("xs", "http://www.w3.org/2001/XMLSchema")

        '   return XmlNamespaceManager
        Return nsManager

    End Function
    Public Shared Function GetFAMEGLDistributionsURI() As String

        '   this is the FAME GLDistributions Schema location
        'Return "http://localhost/AdvantageV1App/localhost/Xml/Schemas/GLDistributionsSchema.xsd"

        '   this is the URI address in the localcomputer (usually "AdvantageDemo")
        Return GetProtocol() + HttpContext.Current.Request.Headers.Item("host") + HttpContext.Current.Request.ApplicationPath + "/" + HttpContext.Current.Request.Headers.Item("host") + "/Xml/Schemas/GLDistributionsSchema.xsd"

    End Function
    Public Shared Function IsValidGuid(ByVal guid As String) As Boolean
        '   if it is nothing it is not a guid
        If guid Is Nothing Then Return False

        '   if it is all zeroes is invalid
        If guid = "00000000-0000-0000-0000-000000000000" Then Return False

        '   these is the regular expression used to validate guids
        Dim guidRegEx As New Regex("[A-Fa-f0-9]{8}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{4}-[A-Fa-f0-9]{12}")

        '   validate guid
        If guidRegEx.Match(guid).Success Then Return True Else Return False
    End Function
    Public Shared Function IsValidDate(ByVal str As Date?) As Boolean
        Try
            Date.Parse(str)
            Return True
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    'Public Shared Function SendEmailMessage(ByRef emailMessage As MailMessage) As String

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    Try
    '        Dim smtp As New SmtpClient
    '        'smtp.EnableSsl = False
    '        'smtp.Port = 465 ''SingletonAppSettings.AppSettings("FameSMTPPort")
    '        'smtp.Host = "smtp.gmail.com" ''SingletonAppSettings.AppSettings("FameSMTPServer")
    '        smtp.Port = MyAdvAppSettings.AppSettings("FameSMTPPort")
    '        smtp.Host = MyAdvAppSettings.AppSettings("FameSMTPServer")
    '        'US3519: check for extra settings 
    '        If Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPUsername")) AndAlso Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPPassword")) Then
    '            smtp.Credentials = New NetworkCredential(MyAdvAppSettings.AppSettings("FameSMTPUsername").ToString, MyAdvAppSettings.AppSettings("FameSMTPPassword").ToString)
    '        End If

    '        If Not IsNothing(MyAdvAppSettings.AppSettings("FameSMTPSSL")) Then
    '            If UCase(MyAdvAppSettings.AppSettings("FameSMTPSSL")) = "YES" Then
    '                smtp.EnableSsl = True
    '            Else
    '                smtp.EnableSsl = False
    '            End If
    '        Else
    '            smtp.EnableSsl = False
    '        End If
    '        smtp.Send(emailMessage)
    '    Catch e As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(e)

    '        Return CType(("SMTP Server is not configured properly or SMTP " + vbCrLf + "Server: " + MyAdvAppSettings.AppSettings("FameSMTPServer") + _
    '                      " is not allowing relay of Emails." + vbCrLf + "No Email(s) sent."), String)

    '    End Try

    '    Return ""

    'End Function


    ' ''' <summary>
    ' ''' Send a Email using the Email Service. The type of email is selected based in the flag OAuth
    ' ''' in the Application Setting. This is resolved in the service itself.
    ' ''' </summary>
    ' ''' <param name="smtpObject">
    ' ''' Configuration parameters.
    ' ''' </param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>

    Public Shared Function SendSMSMessage(ByVal payload As String) As String
        '   return 
        Return GetPage("http://api.clickatell.com/xml/xml", payload)
        'Return getPage("http://localhost/AdvantageV1App/Test/WebForm1.aspx", payload)

    End Function

    Public Shared Function GetPage(ByVal url As String, ByVal payload As String) As String
        Dim result As WebResponse = Nothing

        Try
            Dim req As WebRequest
            Dim RequestStream As Stream
            Dim ReceiveStream As Stream
            Dim encode As Encoding
            Dim sr As StreamReader

            req = WebRequest.Create(url)
            req.Method = "POST"
            req.ContentType = "application/x-www-form-urlencoded"
            Dim someBytes() As Byte
            Dim urlEncoded As New StringBuilder
            Dim reserved() As Char = {ChrW(63), ChrW(61), ChrW(38)}

            If payload <> Nothing Then
                Dim i As Integer = 0
                Dim j As Integer
                While i < payload.Length
                    j = payload.IndexOfAny(reserved, i)
                    If j = -1 Then
                        urlEncoded.Append(HttpUtility.UrlEncode(payload.Substring(i, payload.Length - i)))
                        Exit While
                    End If
                    urlEncoded.Append(HttpUtility.UrlEncode(payload.Substring(i, j - i)))
                    urlEncoded.Append(payload.Substring(j, 1))
                    i = j + 1
                End While
                someBytes = Encoding.UTF8.GetBytes(urlEncoded.ToString())
                req.ContentLength = someBytes.Length
                RequestStream = req.GetRequestStream()
                RequestStream.Write(someBytes, 0, someBytes.Length)
                RequestStream.Close()
            Else
                req.ContentLength = 0
            End If
            result = req.GetResponse()
            ReceiveStream = result.GetResponseStream()
            encode = Encoding.GetEncoding("utf-8")
            sr = New StreamReader(ReceiveStream, encode)

            Dim read(256) As Char
            Dim count As Integer = sr.Read(read, 0, 256)

            Dim sb As New StringBuilder
            Do While count > 0
                Dim str As String = New String(read, 0, count)
                sb.Append(str)
                count = sr.Read(read, 0, 256)
            Loop
            Return sb.ToString

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return "The request URI could not be found or was malformed " + ex.Message
        Finally
            If Not result Is Nothing Then
                result.Close()
            End If
        End Try
    End Function

    Public Shared Function BuildEmailMessage(ByVal fromEmailAddress As String, ByVal toEmailAddress As String, ByRef subject As String, ByRef body As String, ByVal attachments As ArrayList) As MailMessage

        '   build the message


        Dim insMail As New MailMessage(New MailAddress(fromEmailAddress), New MailAddress(toEmailAddress))
        'insMail.IsBodyHtml = False
        'US3519: set as HTML 
        insMail.IsBodyHtml = True
        insMail.Subject = subject
        insMail.Body = body

        ' .BodyFormat = System.Web.Net.MailFormat.Html
        'attach all attachments
        If Not attachments Is Nothing Then
            For Each obj As Object In attachments
                insMail.Attachments.Add(CType(obj, Attachment))
            Next
        End If


        '   return the message
        Return insMail

    End Function
    Public Shared Function BuildSMSMessage(ByVal fromEmailAddress As String, ByVal cellPhoneNumber As String, ByRef body As String) As String

        '   build the message
        Dim sb As New StringBuilder
        sb.Append("data=<clickAPI>")
        sb.Append("<sendMsg>")
        sb.Append("<api_id>710799</api_id>")
        sb.Append("<user>asljussar</user>")
        sb.Append("<password>@pend1ce</password>")
        sb.Append("<to>1" + cellPhoneNumber + "</to>")
        sb.Append("<text>" + body + "</text>")
        sb.Append("<from>fromEmailAddress</from>")
        sb.Append("</sendMsg>")
        sb.Append("</clickAPI>")

        '   return SMS message
        Return sb.ToString

    End Function

    Public Shared Function LoadAdvantageSessionState(ByVal context As HttpContext) As AdvantageSessionState
        If context.Session("AdvantageSessionState") Is Nothing Then
            Return New AdvantageSessionState
        Else
            Try
                Return CType(context.Session("AdvantageSessionState"), AdvantageSessionState)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Return New AdvantageSessionState
            End Try
        End If
    End Function
    Public Shared Sub SaveAdvantageSessionState(ByVal context As HttpContext, ByVal state As AdvantageSessionState)
        context.Session("AdvantageSessionState") = state
    End Sub

    <CLSCompliant(False)>
    Public Shared Sub BuildAdvantageDropDownLists(ByVal ddlList As List(Of AdvantageDDLDefinition))

        Dim nList As New List(Of AdvantageDropDownListMetadata)
        For Each ddl As AdvantageDDLDefinition In ddlList
            'instantiate a metadata object for each ddl
            ddl.Instance = Activator.CreateInstance(GetType(AdvantageDropDownListMetadata).Module.Assembly.GetType("FAME.AdvantageV1.Common." + ddl.DDLName + "DDLMetadata", True))
            ddl.Instance.PullActiveItemsOnly = ddl.PullActiveItemsOnly
            ddl.Instance.CampusId = ddl.CampusId
            'If we are dealing with the Import Leads page we want to exclude any lead status that is mapped
            'to the Advantage system status of Enrolled (DE12596)
            If ddl.Source = "ImportLeads" Then
                ddl.Instance.SetSQLStatementForImportLeadsPage()
            End If

            ddl.WebControl.Items.Clear()
            nList.Add(ddl.Instance)
        Next

        'get ds with dropdownlists tables
        Dim ds As DataSet = (New DropDownListsFacade).GetDropDownLists(nList)

        'bind ddl's
        For Each ddl As AdvantageDDLDefinition In ddlList
            ddl.Instance.Bind(ddl.WebControl, ds, ddl.InsertSelect, ddl.SelectValue)
        Next

    End Sub
    Public Shared Sub SetSelectedValueInAdvantageDDL(ByVal ddl As DropDownList, ByVal selectedValue As String, ByVal selectedText As String)
        'clean ddl
        CleanAdvantageListItemCollection(ddl.Items)
        If selectedValue <> "" And Not selectedValue = Guid.Empty.ToString Then
            'check lower case
            Dim selItem As ListItem = ddl.Items.FindByValue(selectedValue.ToLower)
            If selItem Is Nothing Then
                'check upper case
                selItem = ddl.Items.FindByValue(selectedValue.ToUpper)
            End If

            If selItem Is Nothing Then
                'To prevent the ddl to just show (X) we need to check if there is selectedText is available
                'If Not selectedText = "" Then
                selItem = New ListItem(AdvantageCommonValues.StringToIndicateInactiveItemsInAdvantageDDLs + selectedText, selectedValue)
                ddl.Items.Add(selItem)
                'End If
            End If

            'set selected value
            selItem.Enabled = True
            ddl.SelectedValue = selectedValue
        Else
            If ddl.Items.Count > 0 Then
                ddl.SelectedIndex = 0
            End If
        End If
    End Sub
    'added by Theresa G on 5/14/2009 for 
    Public Shared Sub SetSelectedValueAloneInAdvantageDDL(ByVal ddl As DropDownList, ByVal selectedValue As String, ByVal tblName As String, ByVal dispText As String, ByVal dispValue As String)
        'clean ddl
        CleanAdvantageListItemCollection(ddl.Items)
        If selectedValue <> "" And Not selectedValue = Guid.Empty.ToString Then
            'check lower case
            Dim selItem As ListItem = ddl.Items.FindByValue(selectedValue.ToLower)
            If selItem Is Nothing Then
                'check upper case
                selItem = ddl.Items.FindByValue(selectedValue.ToUpper)
            End If

            If selItem Is Nothing Then
                Dim db As New DataAccess
                Dim sb As New StringBuilder
                With sb
                    .Append("Select " + dispText + " from " + tblName + " where " + dispValue + " = '" + selectedValue + "'")
                End With
                Dim drFldRequired As OleDbDataReader = db.RunParamSQLDataReader(sb.ToString)
                Dim selectedText As String = ""
                While drFldRequired.Read()
                    selectedText = drFldRequired(0).ToString
                End While
                If Not drFldRequired.IsClosed Then drFldRequired.Close()
                selItem = New ListItem(AdvantageCommonValues.StringToIndicateInactiveItemsInAdvantageDDLs + selectedText, selectedValue)
                ddl.Items.Add(selItem)
                'End If
            End If

            'set selected value
            selItem.Enabled = True
            ddl.SelectedValue = selectedValue
        Else
            ddl.SelectedIndex = 0
        End If
    End Sub
    Public Shared Function GetDefaultCountryIndexInDDL(ByVal ddl As DropDownList) As Integer
        'get default country from Configuration file

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        Dim country As String = CType(myAdvAppSettings.AppSettings("DefaultCountry"), String)

        'ignore if there is no value in the configuration file
        If Not country Is Nothing Then
            If Not ddl.Items.FindByText(country) Is Nothing Then
                Return ddl.Items.IndexOf(ddl.Items.FindByText(country))
            Else
                Return 0
            End If
        Else
            Return 0
        End If
    End Function
    Public Shared Function GetFiscalYearEnd() As String
        'get default country from Configuration file

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        Dim fiscalYearEnd As String = CType(myAdvAppSettings.AppSettings("FiscalYearEnd"), String)

        'ignore if there is no value in the configuration file
        If Not fiscalYearEnd Is Nothing Then
            Return fiscalYearEnd
        Else
            Return "10/31"
        End If
    End Function
    Public Shared Sub AddClientCodeToConfirmExitAfterNonSavedChanges(ByVal page As Page, ByVal controlsToIgnore As ArrayList)

        '    'add javascript code to each control that will ignore the exit confirmation
        '    AddJavascriptToEachControlThatWillIgnoreExitConfirmation(page, controlsToIgnore)

        '    'all navigation controls inside containers must be disabled
        '    DisableNavigationControlsInsideContainers(page.Controls, False)

        '    'get all controls ids and values
        '    Dim ids As String = ""
        '    Dim values As String = ""
        '    GetControls(page.Controls, ids, values)

        '    'register javascripts
        '    'Start Comment by balaji on 09/29/2006
        '    'page.ClientScript.RegisterStartupScript(GetType(String), "js1", CreateJavascript())
        '    'End Comment by balaji on 09/29/2006
        '    page.ClientScript.RegisterClientScriptBlock(GetType(String), "js1", CreateJavascript())
        '    page.ClientScript.RegisterArrayDeclaration("ids", ids)
        '    page.ClientScript.RegisterArrayDeclaration("values", values)
    End Sub
    ''This new overloaded function was added to fire Warning Messages for Atlas
    ''This function uses the original AddClientCodeToConfirmExitAfterNonSavedChanges and has a new parameter
    ''updatepanel
    ''need to import Microsoft.Web.UI.Controls and Microsoft.Web.UI namespaces

    Public Shared Function CleanAdvantageListItemCollection(ByVal listItemColl As ListItemCollection) As ListItemCollection
        For Each item As ListItem In listItemColl
            If item.Text.IndexOf(AdvantageCommonValues.StringToIndicateInactiveItemsInAdvantageDDLs) >= 0 Then
                listItemColl.Remove(item)
                Exit For
            End If
        Next
        'Return listItemCollection
        Return listItemColl
    End Function
    <CLSCompliant(False)>
    Public Shared Function GetSchedulingMethod(ByVal schedulingMethod As String) As AdvantageCommonValues.SchedulingMethods
        Select Case schedulingMethod
            Case "Traditional"
                Return AdvantageCommonValues.SchedulingMethods.Traditional
            Case "ModuleStart"
                Return AdvantageCommonValues.SchedulingMethods.ModuleStart
            Case "TraditionalClassesRestrictedByStartDate"
                Return AdvantageCommonValues.SchedulingMethods.TraditionalClassesRestrictedByStartDate
            Case Else
                Return CType((AdvantageCommonValues.SchedulingMethods.Traditional + AdvantageCommonValues.SchedulingMethods.ModuleStart + AdvantageCommonValues.SchedulingMethods.TraditionalClassesRestrictedByStartDate), AdvantageCommonValues.SchedulingMethods)
        End Select
    End Function


    Public Shared Function IsModuleStart() As Boolean

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        If Not String.IsNullOrEmpty(CType(myAdvAppSettings.AppSettings("SchedulingMethod"), String)) Then
            If GetSchedulingMethod(CType(myAdvAppSettings.AppSettings("SchedulingMethod"), String)) = AdvantageCommonValues.SchedulingMethods.ModuleStart Then Return True
        End If
        Return False
    End Function
    <CLSCompliant(False)>
    Public Shared Function SchoolSelectedOptions(ByVal schedulingMethod As String) As AdvantageCommonValues.SchoolOptions
        Dim selectedOptions As Integer = 0

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        'scheduling methods options
        Select Case schedulingMethod.ToLower()
            Case "traditional"
                selectedOptions += AdvantageCommonValues.SchedulingMethods.Traditional
            Case "modulestart"
                selectedOptions += AdvantageCommonValues.SchedulingMethods.ModuleStart
            Case "traditionalclassesrestrictedbystartdate"
                selectedOptions += AdvantageCommonValues.SchedulingMethods.TraditionalClassesRestrictedByStartDate
            Case Else
                selectedOptions += AdvantageCommonValues.SchedulingMethods.Traditional + AdvantageCommonValues.SchedulingMethods.ModuleStart + AdvantageCommonValues.SchedulingMethods.TraditionalClassesRestrictedByStartDate
        End Select

        'OnLineOptions
        If Not (myAdvAppSettings.AppSettings("OnLineUrl") Is Nothing) Then selectedOptions += 8

        'FAMEESP Option
        If Not (myAdvAppSettings.AppSettings("FAMEESP") Is Nothing) Then
            If CType(myAdvAppSettings.AppSettings("FAMEESP"), String).ToLower = "yes" Then
                selectedOptions += 16
            End If
        End If

        'ShowConversionTabs (this option is only for Milwaukee Career College)
        If Not (myAdvAppSettings.AppSettings("UseConversionAttendanceAndResultsTabs") Is Nothing) Then
            If CType(myAdvAppSettings.AppSettings("UseConversionAttendanceAndResultsTabs"), String).ToLower = "yes" Then
                selectedOptions += 32
            End If
        End If

        'Numeric Grade (this option is only for Numeric Grade schools)
        If myAdvAppSettings.AppSettings("GradesFormat") Is Nothing Then
            selectedOptions += 64
        Else
            Dim gradesFormat As String = CType(myAdvAppSettings.AppSettings("GradesFormat"), String).ToLower
            Select Case gradesFormat.ToLower()
                Case "letter"
                    selectedOptions += 64
                Case "numeric"
                    selectedOptions += 128
                Case Else
                    selectedOptions += 64
            End Select
        End If

        'Numeric Grade (this option is only for Numeric Grade schools)
        If myAdvAppSettings.AppSettings("TrackSapAttendance") Is Nothing Then
            selectedOptions += 256
        Else
            Dim trackSapAttendance As String = CType(myAdvAppSettings.AppSettings("TrackSapAttendance"), String)
            Select Case trackSapAttendance.ToLower()
                Case "byclass"
                    selectedOptions += 256
                Case "byday"
                    selectedOptions += 512
                Case Else
                    selectedOptions += 256
            End Select
        End If
        'Progress Report should be only shown on Numeric and byDay schools
        If (selectedOptions And 640) = 640 Then selectedOptions += 1024
        'return selected options
        Return CType(selectedOptions, AdvantageCommonValues.SchoolOptions)
    End Function

    Public Shared Sub AddClientMessageToWebControl(ByVal wc As WebControl, ByVal clientEvent As String, ByVal message As String)
        wc.Attributes.Add(clientEvent, "if(confirm('" + ReplaceSpecialCharactersInJavascriptMessage(message) + "')){}else{return false}")
    End Sub
    Public Shared Sub RemoveClientMessageFromWebControl(ByVal wc As WebControl, ByVal clientEvent As String)
        wc.Attributes.Remove(clientEvent)
    End Sub
    Public Shared Function getRequest(ByVal url As String) As String
        Dim result As WebResponse = Nothing

        Try
            Dim req As WebRequest
            Dim receiveStream As Stream
            Dim encode As Encoding
            Dim sr As StreamReader

            req = WebRequest.Create(url)
            result = req.GetResponse()
            receiveStream = result.GetResponseStream()
            encode = Encoding.GetEncoding("utf-8")
            sr = New StreamReader(receiveStream, encode)

            Dim read(256) As Char
            Dim count As Integer = sr.Read(read, 0, 256)

            Dim sb As New StringBuilder
            Do While count > 0
                Dim str As String = New String(read, 0, count)
                sb.Append(str)
                count = sr.Read(read, 0, 256)
            Loop
            Return sb.ToString

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return "The request URI could not be found or was malformed " + ex.Message
        Finally
            If Not result Is Nothing Then
                result.Close()
            End If
        End Try
    End Function

    Public Shared Function SelectCurrentAcademicYear(ByVal items As ListItemCollection) As Integer
        Dim indexWithMostpoints As Integer = 0
        Dim maxPoints = 0
        For i As Integer = 0 To items.Count - 1
            Dim item As ListItem = items(i)
            Dim points As Integer = CalculatePoints(item.Text)
            If points > maxPoints Then
                indexWithMostpoints = i
                maxPoints = points
            End If
        Next
        Return indexWithMostpoints
    End Function
    Public Shared Function SelectCurrentAcademicYear(ByVal items As RadComboBoxItemCollection) As Integer
        Dim indexWithMostpoints As Integer = 0
        Dim maxPoints = 0
        For i As Integer = 0 To items.Count - 1
            Dim item As RadComboBoxItem = items(i)
            Dim points As Integer = CalculatePoints(item.Text)
            If points > maxPoints Then
                indexWithMostpoints = i
                maxPoints = points
            End If
        Next
        Return indexWithMostpoints
    End Function

    Private Shared Function CalculatePoints(ByVal s As String) As Integer
        Dim points As Integer = 0
        Dim currentYear As Integer = Date.Now.Year

        'points for the previous year
        If s.IndexOf((currentYear - 1).ToString) >= 0 Then points += 6
        If s.IndexOf((currentYear - 1).ToString.Substring(2)) >= 0 Then points += 3

        'points for the current year
        If s.IndexOf((currentYear).ToString) >= 0 Then points += 8
        If s.IndexOf((currentYear).ToString.Substring(2)) >= 0 Then points += 4

        'points for the next year
        If s.IndexOf((currentYear + 1).ToString) >= 0 Then points += 10
        If s.IndexOf((currentYear + 1).ToString.Substring(2)) >= 0 Then points += 5

        Return points
    End Function
    Public Shared Function GetAdvantageBuildNumber() As String
        ' Dim assembly As System.Reflection.Assembly
        Dim version As String = Assembly.GetExecutingAssembly().GetName().Version.ToString()
        Dim firstDot As Integer = version.IndexOf(".")
        Dim secondDot As Integer = version.IndexOf(".", firstDot + 1) + 1
        Dim lastDot As Integer = version.LastIndexOf(".")
        Return version.Substring(secondDot, lastDot - secondDot)
    End Function
    Public Shared Function ConvertUnixTimeStampToDate(ByVal timeStamp As Double) As Date
        Dim origin As New DateTime(1970, 1, 1, 0, 0, 0, 0)
        Return origin.AddSeconds(timeStamp)
    End Function
    Public Shared Function GetProtocol() As String
        If HttpContext.Current.Request.IsSecureConnection Then Return "https://" Else Return "http://"
    End Function

    ''Added by Saraswathi to open a modal window
    ''Dec 9 2008
    Public Shared Sub OpenModalWindow(ByVal page As Page, ByVal url As String, ByVal name As String, ByVal winSettings As String)

        Dim cs As ClientScriptManager = page.ClientScript
        Dim csType As Type = page.GetType
        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            '   this is the beginning of the javascript
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=OpenChild();function OpenChild(){theChild=window.open('" + ReplaceSpecialCharactersInJavascriptMessage(url)

            '   this is the end of the javascript
            Dim scriptEnd As String = "','" + name + "','" + winSettings + "')}</script>"

            '   Register the javascript code
            'page.RegisterStartupScript("OpenChildWindow", scriptBegin + scriptEnd)
            cs.RegisterStartupScript(csType, "OpenModalWindow", scriptBegin + scriptEnd)

            '   add javascript to the Unload event of the body tag to close the child window
            Dim bodyTag As HtmlGenericControl = GetBodyTag(page)
            If Not bodyTag Is Nothing Then
                'bodyTag.Attributes.Add("OnUnload", "javascript:theChild=window.open('','" + name + "','" + "toolbar=no,status=no,resizable=no,width=0px,height=0px" + "');if(!(!theChild || theChild.closed))theChild.close()")
            End If
        End If
    End Sub
    Public Shared Sub OpenFERPAPopUP(ByVal page As Page, ByVal studentId As String, ByVal resourceId As String)
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsLarge
        Dim name = "FERPAPermission"
        Dim url As String = "../AR/" + name + ".aspx?resid=" + resourceId.ToString + "&studentId=" + studentId

        '   open new window and pass parameters
        OpenChildWindow(page, url, name, winSettings)
    End Sub

#Region "RadEditor methods"
    Public Shared Sub SetupRadEditor(ByRef ed As RadEditor, ByVal strToolBar As String, Optional entityID As String = "")

        'common settings
        ed.Tools.Clear()
        ed.ToolsFile = "~/MSG/xml/RadEditorToolsFile.xml"
        ed.ToolbarMode = EditorToolbarMode.Default
        ed.EnableResize = True
        ed.Visible = True


        ' filter the fields categories by entityID
        If entityID <> "" Then
            Dim ds As New DataSet
            Try
                Dim FameToolBar As New EditorToolGroup()
                'add a custom dropdown and set its items and dimension attributes
                Dim ddn As New EditorDropDown("FameTags")
                ddn.Text = "Insert Special Field"

                'Set the popup width and height
                ddn.Attributes("width") = "110px"
                ddn.Attributes("popupwidth") = "240px"
                ' ddn.Attributes("popupheight") = "100px"
                ddn.Attributes("text-align") = "left"

                ds = EntitiesFieldGroupsFacade.GetAll(entityID, True, False)
                For Each dr As DataRow In ds.Tables(0).Rows
                    ddn.Items.Add(HttpContext.Current.Server.HtmlEncode(dr("FieldGroupName")), HttpContext.Current.Server.HtmlEncode(dr("FieldGroupName")))
                Next
                FameToolBar.Tools.Add(ddn)
                ed.Tools.Add(FameToolBar)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            Finally
                ds.Dispose()
                ds = Nothing
            End Try
        End If
    End Sub
#End Region


End Class
<Serializable()>
Public Class AdvantageSessionState
    Inherits DictionaryBase
    Public Sub New()
        MyBase.New()
    End Sub
    Default Public Property Item(ByVal key As [String]) As Object
        Get
            Return Dictionary(key)
        End Get
        Set(ByVal Value As Object)
            If Dictionary.Contains(key) Then
                Dictionary(key) = Value
            Else
                Dictionary.Add(key, Value)
            End If
        End Set
    End Property
    Public ReadOnly Property Keys() As ICollection
        Get
            Return Dictionary.Keys
        End Get
    End Property
    Public ReadOnly Property Values() As ICollection
        Get
            Return Dictionary.Values
        End Get
    End Property
    Public Sub Add(ByVal key As [String], ByVal value As Object)
        Dictionary.Add(key, value)
    End Sub 'Add
    Public Function Contains(ByVal key As [String]) As Boolean
        Return Dictionary.Contains(key)
    End Function 'Contains
    Public Sub Remove(ByVal key As [String])
        Dictionary.Remove(key)
    End Sub 'Remove


End Class
