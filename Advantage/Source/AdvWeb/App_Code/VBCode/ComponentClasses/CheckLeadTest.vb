Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Microsoft.VisualBasic
Public Class CheckLeadTest

    'Public Function CheckRequiredTest(ByVal LeadId As String, ByVal PrgVerId As String) As String
    '    Dim test As New LeadEnrollmentFacade
    '    Dim intLeadHasTaken As Integer
    '    'Dim intLeadHasPassed As Integer
    '    'Dim intLeadTestRequired As Integer
    '    Dim strPassCount As String
    '    Dim intFirstOccurance As Integer
    '    'Dim intLastOccurance As Integer
    '    Dim strTest As String
    '    Dim strFailedTest As String = String.Empty
    '    Dim strLength As String

    '    'Check If Lead Has Taken a Test
    '    'If Count is greater  than zero then lead has taken a test
    '    intLeadHasTaken = test.CheckLeadTakenTest(LeadId)
    '    If intLeadHasTaken >= 1 Then
    '        'Check If the Lead Has Passed a Test Marked as Required
    '        'If Count is Greater than or equal to zero then
    '        'Lead has failed a Required Test

    '        strTest = test.CheckLeadHasPassed(LeadId)

    '        intFirstOccurance = InStr(strTest, ",")
    '        'intLastOccurance = InStrRev(strTest, ",", -1)

    '        strPassCount = Mid(strTest, 1, intFirstOccurance - 1)
    '        strLength = CType(Len(Mid(strTest, intFirstOccurance + 1)), String)

    '        'If there is any test available
    '        If strLength >= 1 Then
    '            strFailedTest = Mid(strTest, intFirstOccurance + 1, CType((strLength - 1), Integer))
    '        End If


    '        'intLeadHasPassed = Test.CheckLeadHasPassed(LeadId)
    '        If strPassCount >= 1 Then
    '            Dim strMessage As String = "Enrollment failed as the lead failed the following test " & Environment.NewLine
    '            strMessage &= strFailedTest
    '            Return strMessage
    '        Else
    '            Return "Enroll"
    '        End If
    '    Else
    '        'Check if The ProgramVersion to which Lead Is Enrolled
    '        'has a Required Test


    '        strTest = test.CheckLeadNoTestTaken(LeadId, PrgVerId)
    '        intFirstOccurance = InStr(strTest, ",")
    '        'intLastOccurance = InStrRev(strTest, ",", -1)

    '        strPassCount = Mid(strTest, 1, intFirstOccurance - 1)
    '        strLength = Len(Mid(strTest, intFirstOccurance + 1))
    '        If strLength < 1 Then
    '            Return "Enroll"
    '            Exit Function
    '        End If

    '        If strLength >= 1 Then
    '            strFailedTest = Mid(strTest, intFirstOccurance + 1, strLength - 1)
    '        End If
    '        'intLeadHasPassed = Test.CheckLeadHasPassed(LeadId)
    '        If strPassCount >= 1 Then
    '            Dim strMessage As String
    '            strMessage = "Enrollment failed as the lead needs to take the following required test " & vbLf
    '            strMessage &= strFailedTest & vbLf
    '            strMessage &= vbLf
    '            strMessage &= "The entrance tests generally shows up based on the program version the lead is interested in" & vbLf
    '            strMessage &= "and on the previous education of the lead.So Please make sure the program version has been selected " & vbLf
    '            strMessage &= "in the intrested in section of the existing leads information page. "
    '            Return strMessage
    '        Else
    '            Return "Enroll"
    '        End If
    '    End If
    'End Function
    Public Function CheckRequiredDocuments(ByVal LeadId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As String
        'Dim Test As New LeadEnrollmentFacade
        Dim intLeadHasTaken As Integer
        Dim leadFacade As New LeadEntranceFacade

        intLeadHasTaken = leadFacade.CheckAllApprovedDocumentsWithPrgVersion(LeadId, PrgVerId, CampusId)

        If intLeadHasTaken >= 1 Then
            Return "Not Enroll"
        Else
            Return "Enroll"
        End If

        'Check If Lead Has Taken a Test
        'If Count is greater  than zero then lead has taken a test
        'intLeadHasTaken = Test.CheckLeadTakenDocs(LeadId)
        'If intLeadHasTaken >= 1 Then
        '    intHasNotApprovedDocs = Test.CheckLeadHasNotApprovedDocs(LeadId)
        '    If intHasNotApprovedDocs >= 1 Then
        '        Return "Enrollment failed"
        '    Else
        '        Return "Enroll"
        '    End If
        'Else
        '    'Check if The ProgramVersion to which Lead Is Enrolled
        '    'has a Required Test
        '    intLeadHasRequiredDocs = Test.CheckIfLeadHasRequiredDocs(LeadId, PrgVerId)
        '    If intLeadHasRequiredDocs < 1 Then
        '        Return "Enroll"
        '        Exit Function
        '    Else
        '        Return "Not Enrolled"
        '    End If
        'End If
    End Function
    Public Function CheckRequiredDocumentsNoProgramVersion(ByVal LeadId As String) As String
        'Dim Test As New LeadEnrollmentFacade
        Dim intLeadHasTaken As Integer
        Dim leadFacade As New LeadEntranceFacade

        intLeadHasTaken = leadFacade.CheckIfLeadHasApprovedRequiredDocumentsNoProgramVersion(LeadId)
        If intLeadHasTaken >= 1 Then
            Return "Not Enroll"
        Else
            Return "Enroll"
        End If
        'Check If Lead Has Taken a Test
        'If Count is greater  than zero then lead has taken a test
        'intLeadHasTaken = Test.CheckLeadTakenDocs(LeadId)
        'If intLeadHasTaken >= 1 Then
        '    intHasNotApprovedDocs = Test.CheckLeadHasNotApprovedDocs(LeadId)
        '    If intHasNotApprovedDocs >= 1 Then
        '        Return "Enrollment failed"
        '    Else
        '        Return "Enroll"
        '    End If
        'Else
        '    'Check if The ProgramVersion to which Lead Is Enrolled
        '    'has a Required Test
        '    intLeadHasRequiredDocs = Test.CheckIfLeadHasRequiredDocsNoProgramVersion(LeadId)
        '    If intLeadHasRequiredDocs < 1 Then
        '        Return "Enroll"
        '        Exit Function
        '    Else
        '        Return "Not Enrolled"
        '    End If
        'End If
    End Function
    Public Function CheckIfRequiredTest(ByVal LeadId As String, ByVal PrgVerId As String, Optional ByVal CampusId As String = "") As String
        Dim intLeadHasTaken As Integer
        Dim leadFacade As New LeadEntranceFacade

        intLeadHasTaken = leadFacade.CheckIfLeadHasPassedRequiredTestWithProgramVersion(LeadId, PrgVerId, CampusId)
        If intLeadHasTaken >= 1 Then
            Return "Not Enroll"
        Else
            Return "Enroll"
        End If
        'Check If Lead Has Taken a Test
        'If Count is greater  than zero then lead has taken a test
        'Dim test As New LeadEnrollmentFacade
        'intLeadHasTaken = test.CheckIfLeadHasTakenTest(LeadId)
        'If intLeadHasTaken >= 1 Then
        '    Dim intHasNotPassedTest = test.CheckIfLeadHasNotPassedTest(LeadId)
        '    If intHasNotPassedTest >= 1 Then
        '        Return "Enrollment failed"
        '    Else
        '        Return "Enroll"
        '    End If
        'Else
        '    'Check if The ProgramVersion to which Lead Is Enrolled
        '    'has a Required Test
        '    Dim intLeadHasRequiredTest As String = test.CheckIfLeadHasRequiredTest(LeadId, PrgVerId)
        '    If intLeadHasRequiredTest < 1 Then
        '        Return "Enroll"
        '    Else
        '        Return "Not Enrolled"
        '    End If
        'End If
    End Function
    Public Function CheckIfRequiredTestNoProgramVersion(ByVal leadId As String) As String
        'Dim Test As New LeadEnrollmentFacade
        Dim intLeadHasTaken As Integer
        'Dim intLeadHasPassed As Integer
        'Dim intLeadTestRequired As Integer
        'Dim strPassCount As String
        'Dim intFirstOccurance As Integer
        'Dim intLastOccurance As Integer
        'Dim strTest As String
        'Dim strFailedTest As String
        'Dim strLength As String
        'Dim intHasNotPassedTest As Integer
        'Dim intLeadHasRequiredTest As Integer
        Dim leadfacade As New LeadEntranceFacade

        'Check If Lead Has Taken a Test
        'If Count is greater  than zero then lead has taken a test
        intLeadHasTaken = leadfacade.CheckIfLeadHasPassedRequiredTestNoProgramVersion(leadId)
        If intLeadHasTaken >= 1 Then
            Return "Not Enroll"
        Else
            Return "Enroll"
        End If
        'If intLeadHasTaken >= 1 Then
        '    intHasNotPassedTest = Test.CheckIfLeadHasNotPassedTest(LeadId)
        '    If intHasNotPassedTest >= 1 Then
        '        Return "Enrollment failed"
        '    Else
        '        Return "Enroll"
        '    End If
        'Else
        '    'Check if The ProgramVersion to which Lead Is Enrolled
        '    'has a Required Test
        '    intLeadHasRequiredTest = Test.CheckIfLeadHasRequiredTestNoProgramVersion(LeadId)
        '    If intLeadHasRequiredTest < 1 Then
        '        Return "Enroll"
        '        Exit Function
        '    Else
        '        Return "Not Enrolled"
        '    End If
        'End If
    End Function

    ''Added by Saraswathi lakshmanan on Sept 22 2010 For document tracking
    ''This function helps to find if the required test dcuments are passed by the student
    ''For use in post payments
    Public Function CheckIfRequiredTest_ForFinAid(ByVal stuEnrollId As String) As String
        Dim intStudentHasTaken As Integer
        Dim studentFacade As New StudentsAccountsFacade
        intStudentHasTaken = studentFacade.CheckIfStudentHasPassedRequiredTestWithProgramVersion_FinAid(stuEnrollId)

        If intStudentHasTaken >= 1 Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If

    End Function
    Public Function CheckRequiredDocuments_ForFinAid(ByVal stuEnrollID As String) As String
        Dim intStudentHasTaken As Integer
        Dim studentFacade As New StudentsAccountsFacade

        intStudentHasTaken = studentFacade.CheckAllApprovedDocumentsWithPrgVersion_FinAid_Student(stuEnrollID)

        If intStudentHasTaken >= 1 Then
            Return "Not Eligible"
        Else
            Return "Eligible"
        End If
    End Function

    Public Function CheckIfRequiredProgramVersion(ByVal selectedValue As String, ByVal leadId As String) As String
        Dim leadFacade As New LeadEntranceFacade()
        Dim requerimentList As List(Of AdRequerimentsProgramVersion) = leadFacade.GetProgramVersionRequerimentsStatus(selectedValue, leadId)
       For Each ar As AdRequerimentsProgramVersion In requerimentList
            If (ar.TypeReq = 1 And ar.TestPassed = False) Then
                Return "UnEnroll"
            End If
            If (ar.TypeReq = 3 And ar.DocPassed = False) Then
                Return "UnEnroll"
            End If
        Next

        Return "Enroll"
    End Function
End Class
