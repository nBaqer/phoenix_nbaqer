Imports System
Imports System.Web
Imports System.Data
Imports Microsoft.VisualBasic
Imports FAME.ExceptionLayer
Imports FAME.Advantage.Common

Namespace FAME.Advantage.DataDictionary
    Public Class DataDictUtilities


#Region "Private variables and objects"

#End Region

#Region "Public Constructors"
        Public Sub New()
            MyBase.New()

        End Sub
#End Region

#Region "Private methods"
        Private Sub AddTable(ByVal TableId As Integer, ByVal TableName As String, ByVal PKey As Integer, _
        Optional ByVal Description As String = Nothing)
            '*************************************************************************************************
            'Purpose:       Helper sub that actually adds table information to syTables table
            'Parameters:
            '[TableId]      The id of the table to be added
            '[TableName]    The name of the table to be added
            '[Pkey]         The Primary Key of the table to be added
            '[Description]  Optional description of purpose of table
            'Returns:       NA
            'Created:       Troy Richards, 5/20/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                If Description Is Nothing Then
                    With sb
                        .Append("INSERT INTO syTables(TblId,TblName,TblPK) ")
                        .Append("VALUES(?,?,?)")
                    End With
                Else
                    With sb
                        .Append("INSERT INTO syTables(TblId,TblName,TblPK,TblDescrip) ")
                        .Append("VALUES(?,?,?,?)")
                    End With
                End If

                db.AddParameter("@tblid", TableId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@tblname", TableName, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@pkey", PKey, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                If Not IsNothing(Description) Then
                    db.AddParameter("@descrip", Description, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 80, ParameterDirection.Input)
                End If
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = SingletonAppSettings.AppSettings("ConString")
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")

                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error adding table to Tables table - " & ex.InnerException.Message)
            End Try
        End Sub

        Private Sub DropTable(ByVal tablename As String)
            '*************************************************************************************************
            'Purpose:       Helper sub that actually removes table information from Tables table
            'Parameters:
            '[TableName]    The name of the table to be removed
            'Returns:       NA
            'Created:       Troy Richards, 5/20/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("DELETE FROM syTables ")
                    .Append("WHERE TblName = ?")
                End With
                db.AddParameter("@tblname", tablename, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConStringMaster"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error removing table from Tables table - " & ex.InnerException.Message)
            End Try
        End Sub

        Private Sub AddField(ByVal FldId As Integer, ByVal FldName As String, _
        ByVal FldTypeId As Integer, ByVal FldLen As Integer)
            '*************************************************************************************************
            'Purpose:       Adds field information to Fields table
            'Parameters:
            '[FldId]      The id of the field to be added
            '[FldName]    The name of the field to be added
            '[FldTypeId]  The FldTypeId of the field to be added
            '[FldLen]     The length of the field to be added
            'Returns:       NA
            'Created:       Troy Richards, 5/21/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("INSERT INTO syFields(FldId,FldName,FldTypeId,FldLen) ")
                    .Append("VALUES(?,?,?,?)")
                End With

                db.AddParameter("@fldid", FldId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@fldname", FldName, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@fldtypeid", FldTypeId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@fldlen", FldLen, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConStringMaster"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error adding field to Fields table - " & ex.InnerException.Message)
            End Try
        End Sub

        Private Sub AddTableField(ByVal TblFldsId As Integer, ByVal TblId As Integer, _
        ByVal FldId As Integer)
            '*************************************************************************************************
            'Purpose:     Adds table-field combination to the TblFlds table
            'Parameters:
            '[TblFldsId]  The id of combination being added
            '[TblId]      The id of the table in the combination
            '[FldId]      The id of the field in the combination
            'Returns:       NA
            'Created:       Troy Richards, 5/21/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("INSERT INTO syTblFlds(TblFldsId,TblId,FldId) ")
                    .Append("VALUES(?,?,?)")
                End With

                db.AddParameter("@tblfldsid", TblFldsId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@tblid", TblId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@fldid", FldId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConStringMaster"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error adding table-field combination to TblFlds table - " & ex.InnerException.Message)
            End Try
        End Sub

        Private Function TblExists(ByVal TableName As String) As Boolean
            '*************************************************************************************************
            'Purpose:       To ascertain if a specified table already exists in the syTables table
            'Parameters:
            '[TableName]    The name of the table to be tested
            'Returns:       True if table exists and False if not
            'Created:       Troy Richards, 5/20/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()
            Dim numTables As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT COUNT(TblName) As NumTables ")
                    .Append("FROM syTables ")
                    .Append("WHERE TblName = ?")
                End With
                db.AddParameter("@tblname", TableName, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConStringMaster"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                numTables = Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                If numTables > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error determining if table exists  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function FldTypeIdExists(ByVal FldTypeId As Integer) As Boolean
            '*************************************************************************************************
            'Purpose:       To ascertain if a specified FldTypeId already exists in the FieldTypes table
            'Parameters:
            '[FldTypeId]    The id of the Field type to be tested
            'Returns:       True if FldTypeId exists and False if not
            'Created:       Troy Richards, 5/21/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()
            Dim numFldType As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT COUNT(FldType) As NumFldType ")
                    .Append("FROM syFieldTypes ")
                    .Append("WHERE FldTypeId = ?")
                End With
                db.AddParameter("@fldtypeid", FldTypeId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                'Changed by Corey Masson Dec 30, 2003
                'SingletonAppSettings.AppSettings("ConString")
                'db.ConnectionString = "ConStringMaster"
                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                numFldType = Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                If numFldType > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error determining if FldTypeId exists  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function FldNameExists(ByVal FldName As String) As Boolean
            '*************************************************************************************************
            'Purpose:       To ascertain if a specified FldName already exists in the Fields table
            'Parameters:
            '[FldName]    The name of the field to be tested
            'Returns:       True if FldName exists and False if not
            'Created:       Troy Richards, 5/21/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()
            Dim numFldName As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT COUNT(FldName) As NumFldName ")
                    .Append("FROM syFields ")
                    .Append("WHERE FldName = ?")
                End With
                db.AddParameter("@fldname", FldName, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                numFldName = Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                If numFldName > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error determining if FldName exists  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function TblIdFldIdExists(ByVal TblId As Integer, ByVal FldId As Integer) As Boolean
            '*************************************************************************************************
            'Purpose:     To ascertain if a specified TblId and FldId already exists in the TblFlds table
            'Parameters:
            '[TblId]      The id of the table in the combination
            '[FldId]      The id of the field in the combination
            'Returns:     True if combination exists and False if not
            'Created:     Troy Richards, 5/21/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()
            Dim numCount As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT COUNT(TblFldsId) As Num ")
                    .Append("FROM syTblFlds ")
                    .Append("WHERE TblId = ? ")
                    .Append("AND FldId = ?")
                End With
                db.AddParameter("@tblid", TblId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@fldid", FldId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                numCount = Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                If numCount > 0 Then
                    Return True
                Else
                    Return False
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error determining if TblId-FldId combination exists  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function GetFieldId(ByVal FieldName As String) As Integer
            '*************************************************************************************************
            'Purpose:       Gets the FldId of the specified FldName from the Fields table. If an id is not 
            '               found -1 is returned.
            'Parameters:
            '[TableName]    The name of the field
            'Returns:       The field id
            'Created:       Troy Richards, 5/20/2003
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT FldId ")
                    .Append("FROM syFields ")
                    .Append("WHERE FldName = ?")
                End With
                db.AddParameter("@tblname", FieldName, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                If Not IsNothing(db.RunParamSQLScalar(sb.ToString())) Then
                    Return Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                Else
                    Return -1
                End If
                db.ClearParameters()
                sb.Remove(0, sb.Length)

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error determining id of field  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function GetMaxTableId() As Integer
            '*************************************************************************************************
            'Purpose:       To get the highest Table id from the Tables table. 
            'Parameters:    None
            'Returns:       Integer indicating the highest id
            'Created:       Troy Richards, 5/20/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT MAX(TblId) As MaxTblId ")
                    .Append("FROM syTables")
                End With

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                If Not IsDBNull(db.RunParamSQLScalar(sb.ToString())) Then
                    Return Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                Else
                    Return 0
                End If
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error obtaining max table id  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function GetMaxFieldId() As Integer
            '*************************************************************************************************
            'Purpose:       To get the highest Field id from the Fields table. 
            'Parameters:    None
            'Returns:       Integer indicating the highest id
            'Created:       Troy Richards, 5/20/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT MAX(FldId) As MaxFldId ")
                    .Append("FROM syFields")
                End With

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                If Not IsDBNull(db.RunParamSQLScalar(sb.ToString())) Then
                    Return Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                Else
                    Return 0
                End If
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error obtaining max field id  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function GetTableId(ByVal TblName As String) As Integer
            '*************************************************************************************************
            'Purpose:       To get the table id for the specified table name. 
            'Parameters:    
            '[TblName]      Name of the table
            'Returns:       Integer representing the id for the specified table name
            'Created:       Troy Richards, 5/21/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()
            Dim TblId As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT TblId ")
                    .Append("FROM syTables ")
                    .Append("WHERE TblName = ?")
                End With
                db.AddParameter("@tbname", TblName, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                TblId = Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                Return TblId
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error obtaining table name  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function GetMaxTblFldsId() As Integer
            '*************************************************************************************************
            'Purpose:       To get the highest TblFldsId from the TblFlds table. 
            'Parameters:    None
            'Returns:       Integer indicating the highest id
            'Created:       Troy Richards, 5/21/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT MAX(TblFldsId) As MaxFldId ")
                    .Append("FROM syTblFlds")
                End With

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                If Not IsDBNull(db.RunParamSQLScalar(sb.ToString())) Then
                    Return Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                Else
                    Return 0
                End If
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error obtaining max TblFldsId  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function GetMaxDDLId() As Integer
            '*************************************************************************************************
            'Purpose:       To get the highest DDLId from the DDLS table. 
            'Parameters:    None
            'Returns:       Integer indicating the highest id
            'Created:       Troy Richards, 5/27/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT MAX(DDLId) As MaxDDLId ")
                    .Append("FROM syDDLS")
                End With

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                If Not IsDBNull(db.RunParamSQLScalar(sb.ToString())) Then
                    Return Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                Else
                    Return 0
                End If
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error obtaining max DDLId  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function GetMaxResourceDefId() As Integer
            '*************************************************************************************************
            'Purpose:       To get the highest ResDefId from the ResTblFlds table. 
            'Parameters:    None
            'Returns:       Integer indicating the highest id
            'Created:       Troy Richards, 6/2/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT MAX(ResDefId) As MaxID ")
                    .Append("FROM syResTblFlds")
                End With

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                If Not IsDBNull(db.RunParamSQLScalar(sb.ToString())) Then
                    Return Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                Else
                    Return 0
                End If
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error obtaining max ResourceDefId  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function GetMaxRptParamId() As Integer
            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New System.Text.StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT MAX(RptParamId) As MaxParamId ")
                    .Append("FROM syRptParams")
                End With

                db.ConnectionString = "ConString"
                If Not IsDBNull(db.RunParamSQLScalar(sb.ToString())) Then
                    Return Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                Else
                    Return 0
                End If
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error obtaining max rptparam id  - " & ex.InnerException.Message)
            End Try
        End Function

        Private Function GetMaxResourceRelationId() As Integer
            '*************************************************************************************************
            'Purpose:       To get the highest ResRelId from the ResTblFlds table. 
            'Parameters:    None
            'Returns:       Integer indicating the highest id
            'Created:       Troy Richards, 8/21/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New System.Text.StringBuilder

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("SELECT MAX(ResRelId) As MaxID ")
                    .Append("FROM syResourceRelations")
                End With

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                If Not IsDBNull(db.RunParamSQLScalar(sb.ToString())) Then
                    Return Convert.ToInt16(db.RunParamSQLScalar(sb.ToString()))
                Else
                    Return 0
                End If
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error obtaining max ResRelfId  - " & ex.InnerException.Message)
            End Try
        End Function

#End Region

#Region "Public methods"
        Public Sub AddTables(ByVal Tables As String, Optional ByVal Description As String = Nothing)
            '**************************************************************************************************
            'Purpose:       (1)Adds the fields of the table to the Fields table if any of them are not
            '               already there.
            '               (2)Adds table information to the Tables table. It will add the TblId, TblName and TblPK.
            '               It will optionally add the TblDescrip if used to add a single table and a description
            '               is supplied in the UI.
            '               (3)Adds the relevent entries to the TblFlds table
            'Parameters:
            '[Tables]       Comma-separated list of table names
            'Returns:       N/A
            'Created:       Troy Richards, 5/20/2003
            'Notes:         In order to use this sub you have to disable the PK on the Tables table which in
            '               turn requires that you remove any relationship with other tables. After running the
            '               sub you must manually enter the TblId and ModuleId and then enable the PK and add
            '               the relationships back.
            '**************************************************************************************************
            Dim TablesArray() As String
            Dim NumTable As Integer
            Dim NumTables As Integer
            Dim TblId As Integer
            Dim TblName As String
            Dim Pkey As String = ""
            Dim NumPKey As Integer
            Dim dt As DataTable
            ' Dim col As DataColumn
            Dim row As DataRow
            Dim db As New DataAccessLayer.DataAccess()
            Dim db2 As New DataAccessLayer.DataAccess()
            Dim sSQL As String
            Dim ColName As String
            Dim FldId As Integer
            Dim FldTypeId As Integer
            Dim FldLen As Integer
            Dim MaxTblId As Integer
            Dim MaxFldId As Integer
            Dim MaxTblFldsId As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                'Put the table list into an array
                TablesArray = Tables.Split(",")
                NumTables = TablesArray.GetUpperBound(0)

                For NumTable = 0 To NumTables
                    'Set number of primary keys to 0
                    NumPKey = 0
                    TblName = Trim(TablesArray(NumTable))
                    'Changed by Corey Masson Dec 30, 2003
                    'SingletonAppSettings.AppSettings("ConString")
                    db2.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                    'Get datatable with schema information for current table
                    sSQL = "SELECT * FROM " & TblName
                    dt = db2.RunSchemaInfo(sSQL)

                    'Only add tables with single-column primary keys
                    For Each row In dt.Rows
                        If row("IsKey") = True Then
                            NumPKey += 1
                            Pkey = row("ColumnName")
                        End If
                    Next
                    If NumPKey = 1 Then
                        'Add the columns for this table to the Fields table if they
                        'do not already exist in that table.
                        For Each row In dt.Rows
                            'We want to verify that the provider type exists in 
                            FldTypeId = CInt(row("ProviderType"))
                            If Not FldTypeIdExists(FldTypeId) Then
                                Throw New System.Exception("FldTypeId does not exist for ProviderType:" & FldTypeId & " Field name:" & row("ColumnName") & " Table name:" & TblName)
                            Else
                                'Only add if field name does not exist in the Fields table
                                ColName = row("ColumnName")
                                If Not FldNameExists(ColName) Then
                                    'Get the max FldId from the Fields table
                                    MaxFldId = GetMaxFieldId()
                                    'Insert FldId,FldName,FldTypeId and FldLen in the Fields table
                                    FldLen = CInt(row("ColumnSize"))
                                    AddField(MaxFldId + 1, ColName, FldTypeId, FldLen)
                                End If 'Field name does not exist in the Fields table

                            End If 'provider type exists in FieldTypes table
                        Next 'Add the columns for this table to the Fields table


                        'Get the FldId for the primary key from the Fields table.
                        'If the there is no FldId then an error must be thrown.
                        'The GetFieldId function returns -1 when an id is not found.
                        FldId = GetFieldId(Pkey)
                        If FldId = -1 Then
                            Throw New System.Exception("No field id was found for the primary key:" & Pkey)
                        Else
                            'Only add table if it is not already in the Tables table
                            If Not TblExists(TblName) Then
                                'Get max table id from the Tables table
                                MaxTblId = GetMaxTableId()
                                'Insert the table id, name and PK in the Tables table
                                If Description Is Nothing Then
                                    AddTable(MaxTblId + 1, TblName, FldId)
                                Else
                                    AddTable(MaxTblId + 1, TblName, FldId, Description)
                                End If

                            End If 'table does not exist already
                        End If 'Field id of primary key is found

                        'We can now insert the entries for this table in the TblFlds table
                        'We cannot have the same TblId and FldId combination twice so we have
                        'to check if each combination already exists.
                        'First get the TblId of the table.
                        TblId = GetTableId(TblName)
                        For Each row In dt.Rows
                            ColName = row("ColumnName")
                            FldId = GetFieldId(ColName)
                            'Only add if combination does not exist already
                            If Not TblIdFldIdExists(TblId, FldId) Then
                                MaxTblFldsId = GetMaxTblFldsId()
                                AddTableField(MaxTblFldsId + 1, TblId, FldId)
                            End If 'table-field combination does not exist in TblFlds
                        Next

                    End If 'single-column primary key
                Next

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                If ex.InnerException Is Nothing Then
                    Throw New System.Exception("Problem occurred adding tables:" & ex.Message)
                Else
                    Throw New System.Exception("Problem occurred adding tables:" & ex.InnerException.Message)
                End If
            End Try
        End Sub

        Public Sub DropTables(ByVal Tables As String)
            '*************************************************************************************************
            'Purpose:       Remove table information from the Tables table
            'Parameters:
            '[Tables]       Comma-separated list of table names
            'Returns:       N/A
            'Created:       Troy Richards, 5/20/2003            
            '*************************************************************************************************
            Dim TablesArray() As String
            Dim NumTable As Integer
            Dim NumTables As Integer
            Dim TblName As String
            Dim db As New DataAccessLayer.DataAccess()
            '  Dim sSQL As String

            Try
                'Put the table list into an array
                TablesArray = Tables.Split(",")
                NumTables = TablesArray.GetUpperBound(0)

                For NumTable = 0 To NumTables
                    TblName = Trim(TablesArray(NumTable))
                    DropTable(TblName)
                Next

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New System.Exception("Problem occurred dropping tables:" & ex.Message)
            End Try
        End Sub

        Public Sub UpdateTblDescrip(ByVal TblId As Integer, ByVal TblDescrip As String)
            '*************************************************************************************************
            'Purpose:       Update description of specified table
            'Parameters:
            '[TblId]        Id of the table to be updated
            '[TblDescrip]   The new table description
            'Returns:       N/A
            'Created:       Troy Richards, 5/22/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("UPDATE syTables ")
                    .Append("SET TblDescrip = ? ")
                    .Append("WHERE TblId = ?")
                End With

                db.AddParameter("@tbldescrip", TblDescrip, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@tblid", TblId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error updating table description - " & ex.InnerException.Message)
            End Try
        End Sub

        Public Sub UpdateTblModule(ByVal TblId As Integer, ByVal ModId As Integer)
            '*************************************************************************************************
            'Purpose:       Update module id of specified table
            'Parameters:
            '[TblId]        Id of the table to be updated
            '[ModId]        The new module id
            'Returns:       N/A
            'Created:       Troy Richards, 5/23/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("UPDATE syTables ")
                    .Append("SET ModuleId = ? ")
                    .Append("WHERE TblId = ?")
                End With

                db.AddParameter("@modid", ModId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@tblid", TblId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error updating table module - " & ex.InnerException.Message)
            End Try
        End Sub

        Public Function AddDDL(ByVal DDLName As String, ByVal TblId As Integer, _
        ByVal DispFldId As Integer, ByVal ValFldId As Integer) As Integer
            '*************************************************************************************************
            'Purpose:       Add 
            'Parameters:
            '[DDLName]      Name of the ddl to be added
            '[TblId]        Id of the table that the ddl is based on
            '[DispFldId]    Id of the field that the display text of the ddl is based on
            '[ValFldId]     Id of the field that the value field of the ddl is based on
            'Returns:       Integer of the max DDLId
            'Created:       Troy Richards, 5/27/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()
            Dim DDLId As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("INSERT INTO syDDLS(DDLId,DDLName,TblId,DispFldId,ValFldId) ")
                    .Append("VALUES(?,?,?,?,?)")
                End With

                DDLId = GetMaxDDLId()

                db.AddParameter("@ddlid", DDLId + 1, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ddlname", DDLName, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@tblid", TblId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@dispfldid", DispFldId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@valfldid", ValFldId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)
                Return DDLId + 1
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error adding DDL to data dictionary - " & ex.InnerException.Message)
            End Try
        End Function

        Public Sub UpdateDDL(ByVal DDLId As Integer, ByVal DDLName As String, _
        ByVal TblId As Integer, ByVal DispFldId As Integer, ByVal ValFldId As Integer)
            '*************************************************************************************************
            'Purpose:       Update the ddl with the specified id 
            'Parameters:
            '[DDLId]        Id of the ddl to be updated
            '[DDLName]      Name of the ddl
            '[TblId]        Id of the table that the ddl is based on
            '[DispFldId]    Id of the field that the display text of the ddl is based on
            '[ValFldId]     Id of the field that the value field of the ddl is based on
            'Returns:       N/A
            'Created:       Troy Richards, 5/28/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("UPDATE syDDLS ")
                    .Append("SET DDLName = ?,")
                    .Append("TblId = ?,")
                    .Append("DispFldId = ?,")
                    .Append("ValFldId = ? ")
                    .Append("WHERE DDLId = ?")
                End With

                db.AddParameter("@ddlname", DDLName, DataAccessLayer.DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)
                db.AddParameter("@tblid", TblId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@dispfldid", DispFldId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@valfldid", ValFldId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@ddlid", DDLId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error updating DDL in data dictionary - " & ex.InnerException.Message)
            End Try
        End Sub

        Public Sub DeleteDDL(ByVal DDLId As Integer)
            '*************************************************************************************************
            'Purpose:       Delete ddl with the specified id 
            'Parameters:
            '[DDLId]        Id of the ddl to be updated            
            'Returns:       N/A
            'Created:       Troy Richards, 5/28/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("DELETE FROM syDDLS ")
                    .Append("WHERE DDLId = ?")
                End With

                db.AddParameter("@ddlid", DDLId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error deleting DDL from data dictionary - " & ex.InnerException.Message)
            End Try
        End Sub

        Public Sub UpdateField(ByVal FldId As Integer, ByVal FldReq As Integer, _
            ByVal FldAud As Integer, ByVal FldMask As String, ByVal DDLId As Integer)
            '*************************************************************************************************
            'Purpose:       Update the field with the specified id 
            'Parameters:
            '[FldReq]       Integer indicating if the field is required or not
            '[FldAud]       Integer indicating if changes to the field should be tracked
            '[FldMask]      Input mask to be used when validating entries in the field
            '[DDLId]        If the field gets its value from a DDL in the UI, this represents the id of that
            '               DDL. If -1 is passed in for this parameter then it means that this should be null.
            'Returns:       N/A
            'Created:       Troy Richards, 5/28/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("UPDATE syFields ")
                    .Append("SET ")
                End With

                If DDLId = -1 Then
                    sb.Append("DDLId = NULL ")
                Else
                    sb.Append("DDLID = ? ")
                End If

                sb.Append("WHERE FldId = ?")

                'db.AddParameter("@fldreq", FldReq, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                'db.AddParameter("@fldaud", FldAud, DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                'db.AddParameter("@fldmask", FldMask, DataAccess.OleDbDataType.OleDbString, 50, ParameterDirection.Input)

                If DDLId <> -1 Then
                    db.AddParameter("@ddlid", DDLId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                End If

                db.AddParameter("@fldid", FldId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error updating field in data dictionary - " & ex.InnerException.Message)
            End Try
        End Sub

        Public Function AddResourceDef(ByVal ResourceId As Integer, ByVal TblFldsId As Integer, _
            ByVal Required As Integer) As Integer
            '*************************************************************************************************
            'Purpose:       Add a new ResourceDef to the data dictionary 
            'Parameters:
            '[ResourceId]   The id of the resource associated with this ResourceDef
            '[TblFldsId]        The id of the Table/Field combination to be added
            '[Reqquired]    Integer indicating whether the field is Advantaged required or not.
            '               (1 means Advantage required and 0 means assigned but not required by Advantage.)
            'Returns:       N/A
            'Created:       Troy Richards, 6/2/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()
            Dim MaxResourceDef As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try

                MaxResourceDef = GetMaxResourceDefId()

                With sb
                    .Append("INSERT INTO syResTblFlds(ResDefId,ResourceId,TblFldsId,Required) ")
                    .Append("VALUES(?,?,?,?)")
                End With

                db.AddParameter("@defid", MaxResourceDef + 1, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@resid", ResourceId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@tblfld", TblFldsId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@reqid", Required, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)

                Return MaxResourceDef + 1

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error adding new ResourceDef to data dictionary - " & ex.InnerException.Message)
            End Try
        End Function

        Public Sub DeleteResourceDef(ByVal ResourceId As Integer, ByVal TblFldsId As Integer)
            '*************************************************************************************************
            'Purpose:       Delete the specified ResourceDefId 
            'Parameters:
            '[ResourceId]   The id of the Resource to be deleted
            '[TblFldsId]    The table/field combination for the resource to be deleted
            'Returns:       N/A
            'Created:       Troy Richards, 6/2/2003            
            '*************************************************************************************************
            Dim db As New DataAccessLayer.DataAccess()
            Dim sb As New System.Text.StringBuilder()

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                With sb
                    .Append("DELETE FROM syResTblFlds ")
                    .Append("WHERE ResourceId = ? ")
                    .Append("AND TblFldsId = ?")

                End With

                db.AddParameter("@res", ResourceId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@fldid", TblFldsId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error deleting record from data dictionary - " & ex.InnerException.Message)
            End Try
        End Sub

        Public Sub AddRptParamID(ByVal resourceId As Integer, ByVal tblFldsId As Integer, ByVal req As Boolean, _
                ByVal sortSec As Boolean, ByVal filterListSec As Boolean, ByVal filterOtherSec As Boolean)
            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim MaxRptParamId As Integer

            Try
                MaxRptParamId = GetMaxRptParamId()

                With sb
                    .Append("INSERT INTO syRptParams(RptParamId,ResourceId,TblFldsId,Required,SortSec,FilterListSec,FilterOtherSec) ")
                    .Append("VALUES(?,?,?,?,?,?,?)")
                End With

                db.AddParameter("@rptparamid", MaxRptParamId + 1, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@resid", resourceId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@tblfld", tblFldsId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@reqid", req, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@sortsec", sortSec, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@filterlistsec", filterListSec, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@filterothersec", filterOtherSec, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = "ConString"
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error adding new rptparamid to data dictionary - " & ex.InnerException.Message)
            End Try

        End Sub

        Public Sub DeleteRptParam(ByVal resourceId As Integer, ByVal tblFldsId As Integer)
            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New System.Text.StringBuilder

            Try
                With sb
                    .Append("DELETE FROM syRptParams ")
                    .Append("WHERE ResourceId = ? ")
                    .Append("AND TblFldsId = ?")
                End With

                db.AddParameter("@resid", resourceId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@tblfldsid", tblFldsId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = "ConString"
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error deleting report parameter from data dictionary - " & ex.InnerException.Message)
            End Try
        End Sub

        Public Sub UpdateRptParam(ByVal resourceId As Integer, ByVal tblFldsId As Integer, ByVal Req As Boolean, _
            ByVal sortSec As Boolean, ByVal filterListSec As Boolean, ByVal filterOtherSec As Boolean)
            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New System.Text.StringBuilder

            Try
                With sb
                    .Append("UPDATE syRptParams ")
                    .Append("SET Required = ?, ")
                    .Append("SortSec = ?, ")
                    .Append("FilterListSec = ?,")
                    .Append("FilterOtherSec = ? ")
                    .Append("WHERE ResourceId = ? ")
                    .Append("AND TblFldsId = ?")
                End With

                db.AddParameter("@reqid", Req, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@sortsec", sortSec, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@filterlistsec", filterListSec, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@filterothersec", filterOtherSec, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@resid", resourceId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@tblfld", tblFldsId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = "ConString"
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error updating rptparamid - " & ex.InnerException.Message)
            End Try
        End Sub

        Public Sub AddResourceRelation(ByVal resourceId As Integer, ByVal parentId As Integer, ByVal seq As Integer)
            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New System.Text.StringBuilder
            Dim MaxResRelId As Integer

            Dim MyAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                MyAdvAppSettings = New AdvAppSettings
            End If

            Try
                MaxResRelId = GetMaxResourceRelationId()

                With sb
                    .Append("INSERT INTO syResourceRelations(ResRelId,ResourceId,ParentId,Sequence) ")
                    .Append("VALUES(?,?,?,?)")
                End With

                db.AddParameter("@defid", MaxResRelId + 1, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@resid", resourceId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@parid", parentId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@seq", seq, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = MyAdvAppSettings.AppSettings("ConString")
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error adding new Resource Relation to data dictionary - " & ex.InnerException.Message)
            End Try
        End Sub

        Public Sub UpdateResourceRelation(ByVal resourceId As Integer, ByVal parentId As Integer, ByVal seq As Integer)
            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New System.Text.StringBuilder

            Try
                With sb
                    .Append("UPDATE syResourceRelations ")
                    .Append("SET Sequence = ? ")
                    .Append("WHERE ResourceId = ? ")
                    .Append("AND ParentId = ?")
                End With

                db.AddParameter("@seq", seq, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@res", resourceId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@parent", parentId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = "ConString"
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error updating resource relation - " & ex.InnerException.Message)
            End Try
        End Sub

        Public Sub DeleteResourceRelation(ByVal resourceId As Integer, ByVal parentId As Integer)
            Dim db As New DataAccessLayer.DataAccess
            Dim sb As New System.Text.StringBuilder

            Try
                With sb
                    .Append("DELETE FROM syResourceRelations ")
                    .Append("WHERE ResourceId = ? ")
                    .Append("AND ParentId = ?")
                End With

                db.AddParameter("@resid", resourceId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)
                db.AddParameter("@parentid", parentId, DataAccessLayer.DataAccess.OleDbDataType.OleDbInteger, , ParameterDirection.Input)

                db.ConnectionString = "ConString"
                db.RunParamSQLExecuteNoneQuery(sb.ToString())
                db.ClearParameters()
                sb.Remove(0, sb.Length)

            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New BaseException("Error deleting resource relation from data dictionary - " & ex.InnerException.Message)
            End Try
        End Sub
#End Region
    End Class

End Namespace

