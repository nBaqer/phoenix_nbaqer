Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.OleDb
Imports FAME.DataAccessLayer
Imports System.Web.UI
Imports System.Text
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Web.UI.WebControls
Imports System.Collections
Imports System

Public Class ReportingAgencies
    Public Sub GenerateReporingAgencies(ByVal pnl As Panel, ByVal intRptFldId As Integer, Optional ByVal strValueId As String = "", Optional ByVal PageName As String = "")
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the Count Of Controls Available for the Resource(Page)
        '               and Populates the Controls Dynamically.Populates TextBox,Labels,DropDownList
        '               Control and finally adds it to panel control
        'Parameters:    ResourceID,Panel Control
        'Returns:       String
        'Notes:         This sub relies on the ResourceId when a request for the page is made.

        '**************************************************************************************************
        pnl.Controls.Clear()
        Dim dynList As DropDownList, dynLabel As Label ', dynText As TextBox

        Dim arrRange() As String
        Dim intPopulateControls As Integer

        Dim db As New DataAccess
        Dim sb As New System.Text.StringBuilder
        Dim ds As New DataSet

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Get Count Of Reporting Agencies Applicable to school
        Dim intCountOfRptAgenciesAppToSchool As Integer = 0
        Dim strRptAgencies As String = AdvantageCommonValues.GetReportingAgenciesApplicableToSchool
        intCountOfRptAgenciesAppToSchool = AdvantageCommonValues.GetCountofReportingAgenciesApplicableToSchool

        'Use Split Function and get the controls count 
        'in respective variables
        arrRange = strRptAgencies.Split(",")

        If intCountOfRptAgenciesAppToSchool >= 1 Then
            pnl.Controls.Add(New LiteralControl("<table class=""contenttable"" cellSpacing=""0"" cellPadding=""0"" width=""100%"" align=""center"">"))
            For intPopulateControls = 0 To intCountOfRptAgenciesAppToSchool - 1
                If Not arrRange(intPopulateControls) = "5" Then
                    Try
                        With sb
                            .Append("   select Distinct ")
                            .Append("       t4.RptAgencyFldValId, ")
                            .Append("       t4.AgencyDescrip    ")
                            If Not strValueId = "" And Not PageName.ToString.ToLower = "term" Then
                                .Append("       ,(select   Distinct           ")
                                .Append("           RptAgencyFldValId ")
                                .Append("       from                  ")
                                .Append("           syRptAgencySchoolMapping ")
                                .Append("       where                 ")
                                .Append("           RptAgencyFldValId=t4.RptAgencyFldValId and ")
                                .Append("           SchoolDescripId= ? ")
                                .Append("       ) as SelectedAgencyValue ")
                            ElseIf Not strValueId = "" And PageName.ToString.ToLower = "Term" Then
                                .Append(", (select Top 1 regentTerm from arTerm where TermId=?) ")
                            End If
                            .Append("   from                    ")
                            .Append("       syRptAgencyFields t1,  ")
                            .Append("       syRptFields t2,        ")
                            .Append("       syRptAgencies t3,      ")
                            .Append("       syRptAgencyFldValues t4 ")
                            .Append("   where                       ")
                            .Append("       t1.RptFldId = t2.RptFldId and ")
                            .Append("       t1.RptAgencyId = t3.RptAgencyId and ")
                            .Append("       t1.RptAgencyFldId = t4.RptAgencyFldId and ")
                            .Append("       t3.RptAgencyId = ? And t1.RptFldId = ? ")
                            'If arrRange(intPopulateControls) = 1 Then
                            .Append(" and t4.RptAgencyFldValId <> 29 ")
                            ' End If

                        End With
                        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(MyAdvAppSettings.AppSettings("ConString")))
                        sc.Parameters.Add(New OleDbParameter("@SchoolDescripId", strValueId))
                        sc.Parameters.Add(New OleDbParameter("@RptAgencyId", arrRange(intPopulateControls)))
                        sc.Parameters.Add(New OleDbParameter("@RptFldId", intRptFldId))

                        Dim da As New OleDbDataAdapter(sc)
                        da.Fill(ds, "ListValue")
                        sb.Remove(0, sb.Length)
                    Finally
                        ' db.CloseConnection()
                    End Try


                    'Create Labels For Agencies
                    dynLabel = New Label
                    With dynLabel
                        .ID = "lblRpt" & AdvantageCommonValues.GetReportingAgencyNames(arrRange(intPopulateControls)).ToString.ToUpper
                        .Text = AdvantageCommonValues.GetReportingAgencyNames(arrRange(intPopulateControls)).ToString.ToUpper
                        .CssClass = "Label"
                    End With

                    dynList = New DropDownList
                    With dynList
                        .ID = "ddlRpt" & AdvantageCommonValues.GetReportingAgencyNames(arrRange(intPopulateControls)).ToString.ToUpper
                        .CssClass = "dropdownlist"
                        .DataTextField = "AgencyDescrip"
                        .DataValueField = "RptAgencyFldValId"
                        .Width = 200
                        .DataSource = ds.Tables(0)
                        .DataBind()
                        .Items.Insert(0, New ListItem("Select", 0))
                        Dim dr As DataRow
                        Dim strDDLSelectedValue As String = ""
                        'Iterate through the records and get the selected value
                        For Each dr In ds.Tables(0).Rows
                            If Not dr("SelectedAgencyValue") Is System.DBNull.Value Then
                                strDDLSelectedValue = dr("SelectedAgencyValue")
                            End If
                        Next
                        Try
                            .SelectedValue = strDDLSelectedValue
                        Catch ex As SystemException
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            .SelectedIndex = 0
                        End Try
                    End With
                    'End If
                    ds.Clear()

                    'Rally Case : DE832 Marital Status, Dependency Types and Geographic Types  are not required for IPEDS
                    'Need to hide the IPEDS Mapping dropdown on these maintenance pages
                    'The following code is what populates the controls in the maintenance pages
                    'The below logic will hide the IPEDS controls in Marital Status, Dependency Types and Geographic Types maintenance pages

                    If arrRange(intPopulateControls).ToString.Trim = "1" And (intRptFldId = 2 Or intRptFldId = 4 Or intRptFldId = 19) Then
                        'Hide the controls in Marital Status, Dependency Types and Geographic Types pages for IPEDS
                    Else
                        pnl.Controls.Add(New LiteralControl("<tr><td class=""twocolumnlabelcell"" nowrap>"))
                        pnl.Controls.Add(dynLabel)
                        pnl.Controls.Add(New LiteralControl("</td><td class=""twocolumncontentcell"">"))
                        pnl.Controls.Add(dynList)
                        pnl.Controls.Add(New LiteralControl("<br/>"))
                        'pnl.Controls.Add(New LiteralControl("</td></tr></table>"))
                    End If
                End If
            Next
        End If
        pnl.Controls.Add(New LiteralControl("</table>"))
    End Sub
    Public Function GetAllValues(ByVal pnl As Panel) As ArrayList
        '**************************************************************************************************
        'Purpose:       This Procedure Gets Values From The Panel
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim arr3 As New ArrayList

        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(DropDownList) Then
                arr3.Add(CType(ctl, DropDownList).SelectedValue)
            End If
        Next
        Return arr3
    End Function
    Public Function InsertValues(ByVal strValueId As String, Optional ByVal selectedRptAgencyFldValId As ArrayList = Nothing, Optional ByVal UpdTable As String = "", Optional bUpdIPEDS As Boolean = True) As Integer
        '   connect to the database
        Dim RptAgencyFacade As New ReportingAgencyFacade
        '  DE1131 Janet Robinson 04/25/2011
        Return RptAgencyFacade.InsertValues(strValueId, selectedRptAgencyFldValId, UpdTable, bupdIPEDS)
    End Function
    Public Function BuildSession(ByVal ddlName As String, ByVal strValueId As String) As DataSet
        'Dim strSQL As String = ""
        Dim sb As New StringBuilder
        Dim strRegentTblName As String = ""
        Dim strDBName As String = ""
        Dim strWhereClause As String = ""

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
      

        strDBName = myAdvAppSettings.AppSettings("RegentDBName").ToString.Trim.ToLower
        If Not strDBName.Trim = "" Then
            Select Case ddlName
                Case Is = "ActivityCode"
                    strDBName = strDBName & ".ally.SAF_ACTIVITY"
                Case Is = "Session"
                    strDBName = strDBName & ".ally.SAF_YR_SES_MAS"
                Case Else
                    strDBName = strDBName & ".CNTL.CTL_VAL_TABLE_VALUES"
            End Select
        End If
        Select Case ddlName.ToString.Trim
            Case Is = "Session"
                strRegentTblName = "SAF_YR_SES_MAS"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "SessionStart"
                strRegentTblName = "SAF_YR_SES_START"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "SessionEnd"
                strRegentTblName = "SAF_YR_SES_END"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "AwardCode"
                strRegentTblName = "SAF_AWD_CODE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
                'strRegentTblName = "SAF_AWD_CODE"
                'strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "AwardSubCode"
                strRegentTblName = "SAF_AWD_SUB_CODE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "AwardStatus"
                strRegentTblName = "SAF_AWD_STAT"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "ActivityCode"
                strRegentTblName = "SAF_ACTIVITY_CODE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "DegreeCode"
                strRegentTblName = "SAF_DEGREE_CODE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "CurrCode"
                strRegentTblName = "SAF_CURRIC_CODE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "LenderCode"
                strRegentTblName = "SAF_LENDER_ID"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "TrackCode"
                strRegentTblName = "SAF_TRACK_CODE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "DueDate"
                strRegentTblName = "SAF_DATE_DUE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "TransDate"
                strRegentTblName = "SAF_TRANS_DATE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "CompletedDate"
                strRegentTblName = "SAF_COMPL_DATE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "NotificationCode"
                strRegentTblName = "SAF_NOTATION_CODE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "ACADSTATUS"
                strRegentTblName = "SAF_ACAD_STAT"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "Genders"
                strRegentTblName = "IDE_SEX"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "MaritalStatus"
                strRegentTblName = "IDE_MARITAL_STAT"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "Ethnicity"
                strRegentTblName = "IDE_RACE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "Citizenship"
                strRegentTblName = "IDE_CITIZENSHIP"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "Prefix"
                strRegentTblName = "IDE_NAME_PREFIX"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "Suffix"
                strRegentTblName = "IDE_NAME_SUFFIX"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "AddressType"
                strRegentTblName = "IDE_ADDR_TYPE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "AddressCode"
                strRegentTblName = "IDE_ADDR_CODE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "Country"
                strRegentTblName = "IDE_COUNTRY_CODE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "County"
                strRegentTblName = "IDE_COUNTY_CODE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
            Case Is = "State"
                strRegentTblName = "IDE_STATE"
                strWhereClause = " WHERE CTL_TABLE_NAME='" & strRegentTblName & "'"
        End Select
        If Not ddlName.ToString.Trim = "ActivityCode" And Not ddlName.ToString.Trim = "Session" Then
            With sb
                .Append(" select distinct CTL_TABLE_VALUE as RptAgencyFldValId, " & vbCrLf)
                .Append(" CTL_TABLE_VALUE as AgencyDescrip " & vbCrLf)
                .Append(" from " & strDBName & strWhereClause & vbCrLf)
            End With
        ElseIf ddlName.ToString.Trim = "ActivityCode" Then
            With sb
                .Append(" select distinct SAF_ACTIVITY_CODE as RptAgencyFldValId, " & vbCrLf)
                .Append(" SAF_ACTIVITY_CODE as AgencyDescrip " & vbCrLf)
                .Append(" from " & strDBName & vbCrLf)
            End With
        End If
        If ddlName.ToString.Trim = "Session" Then
            With sb
                .Append(" select distinct SAF_YR_SES as RptAgencyFldValId, " & vbCrLf)
                .Append(" SAF_YR_SES + ' - ' + SAF_YR_SES_DESC + ' (' + SAF_DATE_BEGIN + '-' + SAF_Date_END + ') ' as AgencyDescrip " & vbCrLf)
                .Append(" from " & strDBName & vbCrLf)
            End With
        End If
        Dim sc As New OleDbCommand(sb.ToString, New OleDbConnection(myAdvAppSettings.AppSettings("RegentConnectionString")))
        Dim da As New OleDbDataAdapter(sc)
        Dim ds As New DataSet
        da.Fill(ds, "ListValue")
        sb.Remove(0, sb.Length)
        Return ds
    End Function
End Class
