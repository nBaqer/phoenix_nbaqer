﻿Imports Microsoft.VisualBasic


Public Enum AdvantageSystemModuleId
    Admissions = 189
    Academics = 26
    FinancialAid = 191
    HumanResources = 192
    Placement = 193
    StudentAccounts = 194
End Enum


Public Enum AdvantageEntityId
    Lead = 395
    Student = 394
    Employers = 397
    Employees = 396
    Lenders = 434
End Enum

''' <summary>
''' Enums for Academic Calendars
''' </summary>
Public Enum AdvantageSystemAcademicCalendars
    NonStandardTerm = 1
    Quarter = 2
    Semester = 3
    Trimester = 4
    ClockHour = 5
    NonTerm = 6
End Enum


''' <summary>
''' Enums for setting base size for reports 
''' </summary>
Public Enum BaseReportSize
    GradCompletionReport = 1500

End Enum


