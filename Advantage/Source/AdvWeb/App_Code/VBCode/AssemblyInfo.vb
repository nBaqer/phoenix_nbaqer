Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Financial Aid Management for Education Inc.")> 
<Assembly: AssemblyProduct("Advantage")> 
<Assembly: AssemblyCopyright("Copyright (c) 2003, 2004. All rights reserved.")> 
<Assembly: AssemblyTrademark("")> 
'<Assembly: CLSCompliant(False)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("A67F93F2-FCB4-4A7F-A980-7DBC5B398E91")> 



' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.2.3.1975")> 
'<Assembly: AssemblyDelaySign(True)> 


