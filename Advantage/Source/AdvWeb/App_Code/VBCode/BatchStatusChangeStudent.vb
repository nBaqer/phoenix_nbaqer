﻿

Namespace AdvWeb.VBCode

    Public Class BatchStatusChangeStudent
    
        ''' <summary>
        ''' 
        ''' Hold Student Id Key
        ''' </summary>
        ''' <value>String</value>
        ''' <returns>String</returns>
        Property StuEnrollmentId  As String

        ''' <summary>
        ''' Student Name.
        ''' </summary>
        ''' <value>String</value>
        ''' <returns>String</returns>
        ''' <remarks></remarks>
        Property StudentName  As String

        ''' <summary>
        ''' Program Version Description
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks>we use the description becuase the id is not available.</remarks>
        Property ProgramVersionDescription As String
        

    End Class
End NameSpace