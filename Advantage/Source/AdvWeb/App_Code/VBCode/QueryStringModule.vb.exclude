﻿Imports System
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Imports System.Web

''' <summary>
''' Summary description for QueryStringModule
''' </summary>
Public Class QueryStringModule
    Implements IHttpModule
    '  private ILog m_Logger = LogManager.GetLogger(typeof(QueryStringModule));
#Region "IHttpModule Members"


    Public Sub Dispose() Implements IHttpModule.Dispose
        ' Nothing to dispose
    End Sub


    Public Sub Init(context As HttpApplication) Implements IHttpModule.Init
        AddHandler context.BeginRequest, AddressOf context_BeginRequest
    End Sub


#End Region


    Private Const PARAMETER_NAME As String = "enc="
    Private Const ENCRYPTION_KEY As String = "key"


    Private Sub context_BeginRequest(sender As Object, e As EventArgs)
        Dim context As HttpContext = HttpContext.Current
        Dim query As String 
        Dim path As String 


        Try
            If context.Request.Url.OriginalString.Contains("aspx") AndAlso context.Request.RawUrl.Contains("?") Then
                query = ExtractQuery(context.Request.RawUrl)
                path = GetVirtualPath()


                If query.StartsWith(PARAMETER_NAME, StringComparison.OrdinalIgnoreCase) Then
                    ' Decrypts the query string and rewrites the path.
                    Dim rawQuery As String = query.Replace(PARAMETER_NAME, String.Empty)
                    Dim decryptedQuery As String = Decrypt(rawQuery)
                    context.RewritePath(path, String.Empty, decryptedQuery)
                ElseIf context.Request.HttpMethod = "GET" Then
                    ' Encrypt the query string and redirects to the encrypted URL.
                    ' Remove if you don't want all query strings to be encrypted automatically.
                    Dim encryptedQuery As String = Encrypt(query)
                    context.Response.Redirect(path + encryptedQuery, False)
                End If
            End If
        Catch ex As Exception
            ' m_Logger.Error("An error occurred while parsing the query string in the URL: " + path, ex);
            context.Response.Redirect("~/Home.aspx")
        End Try


    End Sub


    ''' <summary>
    ''' Parses the current URL and extracts the virtual path without query string.
    ''' </summary>
    ''' <returns>The virtual path of the current URL.</returns>
    Private Shared Function GetVirtualPath() As String
        Dim path As String = HttpContext.Current.Request.RawUrl
        path = path.Substring(0, path.IndexOf("?"))
        path = path.Substring(path.LastIndexOf("/") + 1)
        Return path
    End Function


    ''' <summary>
    ''' Parses a URL and returns the query string.
    ''' </summary>
    ''' <param name="url">The URL to parse.</param>
    ''' <returns>The query string without the question mark.</returns>
    Private Shared Function ExtractQuery(url As String) As String
        Dim index As Integer = url.IndexOf("?") + 1
        Return url.Substring(index)
    End Function


#Region "Encryption/decryption"


    ''' <summary>
    ''' The salt value used to strengthen the encryption.
    ''' </summary>
    Private Shared ReadOnly SALT As Byte() = Encoding.ASCII.GetBytes(ENCRYPTION_KEY.Length.ToString())


    ''' <summary>
    ''' Encrypts any string using the Rijndael algorithm.
    ''' </summary>
    ''' <param name="inputText">The string to encrypt.</param>
    ''' <returns>A Base64 encrypted string.</returns>
    Private Shared Function Encrypt(inputText As String) As String
        Dim rijndaelCipher As New RijndaelManaged()
        Dim plainText As Byte() = Encoding.Unicode.GetBytes(inputText)
        Dim SecretKey As New PasswordDeriveBytes(ENCRYPTION_KEY, SALT)


        Using encryptor As ICryptoTransform = rijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16))
            Using memoryStream As New MemoryStream()
                Using cryptoStream As New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
                    cryptoStream.Write(plainText, 0, plainText.Length)
                    cryptoStream.FlushFinalBlock()
                    Return "?" + PARAMETER_NAME + Convert.ToBase64String(memoryStream.ToArray())
                End Using
            End Using
        End Using
    End Function


    ''' <summary>
    ''' Decrypts a previously encrypted string.
    ''' </summary>
    ''' <param name="inputText">The encrypted string to decrypt.</param>
    ''' <returns>A decrypted string.</returns>
    Private Shared Function Decrypt(inputText As String) As String
        Dim rijndaelCipher As New RijndaelManaged()


        Dim encryptedData As Byte() = Convert.FromBase64String(inputText)
        Dim secretKey As New PasswordDeriveBytes(ENCRYPTION_KEY, SALT)


        Using decryptor As ICryptoTransform = rijndaelCipher.CreateDecryptor(secretKey.GetBytes(32), secretKey.GetBytes(16))
            Using memoryStream As New MemoryStream(encryptedData)
                Using cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)
                    Dim plainText As Byte() = New Byte(encryptedData.Length - 1) {}
                    Dim decryptedCount As Integer = cryptoStream.Read(plainText, 0, plainText.Length)
                    Return Encoding.Unicode.GetString(plainText, 0, decryptedCount)
                End Using
            End Using
        End Using
    End Function


#End Region


End Class


