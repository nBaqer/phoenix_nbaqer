﻿Imports System
Imports System.Diagnostics
Imports System.Web.UI
Imports System.Reflection
Imports Advantage.Business.Objects
Imports System.Activities.Expressions
Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports System.Text
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common
Imports System.Web.Services

Imports Advantage.Business.Logic.Layer
Imports FAME.AdvantageV1.BusinessFacade

Public MustInherit Class BasePage
    Inherits Page


#Region "Enums"
    Protected Enum CallbackType As Integer
        [Postback] = 0
        [RadAjaxCallback] = 1
    End Enum
    Public Enum PageTheme As Integer
        [None] = 0
        [Blue_Theme] = 1
    End Enum
#End Region

#Region "Properties"

    Private _radNotification As RadNotification
    Public Property RadPopupContol() As RadNotification
        Get
            Return _radNotification
        End Get
        Set(value As RadNotification)
            _radNotification = value
        End Set
    End Property

#End Region

#Region "Base Page Messages"
    ''' <summary>
    ''' This method calls a RAD Alert window from an ASP.NET postback or RAD AJAX callback
    ''' </summary>
    ''' <param name="callback">Select whether the message is being triggered by a postback or an RAD AJAX callback</param>
    ''' <param name="key">The key used to register the startup script</param>
    ''' <param name="message">The message to be displayed</param>
    ''' <param name="width">The width of the RAD Alert Window in pixels</param>
    ''' <param name="height">The height of the RAD Alert Window in pixels</param>
    ''' <param name="title">The title to display for the RAD Alert Window</param>
    ''' <remarks></remarks>
    Protected Overridable Sub DisplayRADAlert(callback As CallbackType, key As String, message As String, title As String)
        Dim radalertscript As String
        message = message.Replace(Environment.NewLine, "<br />")
        Dim width As Integer
        Dim height As Integer
        'changing the width and/or height will effect  ~187 DisplayRADAlerts
        width = 450
        height = 200

        Select Case callback
            Case CallbackType.Postback
                radalertscript = String.Format("<script language='javascript'>function f(){{radalert('{0}',{1},{2},'{3}'); Sys.Application.remove_load(f);}}; Sys.Application.add_load(f);</script>", message.Trim(), width, height, title.Trim())
                Page.ClientScript.RegisterStartupScript([GetType](), key.Trim(), radalertscript)
            Case CallbackType.RadAjaxCallback
                radalertscript = String.Format("radalert('{0}',{1},{2},'{3}');", message.Trim, width, height, title.Trim)
                ScriptManager.RegisterStartupScript(Me, Me.[GetType](), key.Trim, radalertscript, True)
            Case Else
                Throw New Exception("Unknown CallbackType")
        End Select
    End Sub

    Protected Overridable Sub DisplayRADAlertCustomSize(callback As CallbackType, key As String, message As String, width As Integer, height As Integer, title As String)
        Dim radalertscript As String
        message = message.Replace(Environment.NewLine, "<br />")
        'message = message.Replace(vbCr, "<br />")
        'message = message.Replace(vbLf, "<br />")

        Select Case callback
            Case CallbackType.Postback
                radalertscript = String.Format("<script language='javascript'>function f(){{radalert('{0}',{1},{2},'{3}'); Sys.Application.remove_load(f);}}; Sys.Application.add_load(f);</script>", message.Trim(), width, height, title.Trim())
                Page.ClientScript.RegisterStartupScript([GetType](), key.Trim(), radalertscript)
            Case CallbackType.RadAjaxCallback
                radalertscript = String.Format("radalert('{0}',{1},{2},'{3}');", message.Trim, width, height, title.Trim)
                ScriptManager.RegisterStartupScript(Me, Me.[GetType](), key.Trim, radalertscript, True)
            Case Else
                Throw New Exception("Unknown CallbackType")
        End Select
    End Sub

    Protected Overridable Sub DisplayRadNotification(msgTitle As String, message As String, Optional width As Integer = 0, Optional height As Integer = 0)

        Dim notCtl As RadNotification = _radNotification
        If notCtl Is Nothing Then
            Throw New Exception("Display RadNotification error - control object not set")
        Else
            Try
                message = message.Replace(Environment.NewLine, "<br />")
                notCtl.Title = msgTitle
                notCtl.Text = message

                If width > 0 Then
                    notCtl.Width = Unit.Pixel(width)
                End If

                If height > 0 Then
                    notCtl.Height = Unit.Pixel(height)
                End If

                notCtl.Show()

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                Throw
            End Try

        End If



    End Sub


#End Region

#Region "TenantConnection"
    Protected Function GetConnectionStringFromAdvAppSetting(connectionname As String) As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Dim connectionString = myAdvAppSettings.AppSettings(connectionname).ToString
        If connectionString <> String.Empty Then
            Return connectionString
        Else
            Throw New Exception("Connection string is blank")
        End If

    End Function
#End Region

#Region "Base Page Utils"

    Public Function GetObjStudentState(Optional entityType As Integer = -1) As StudentMRU

        Dim objStudentState As New StudentMRU
        If (Not Page.IsPostBack Or Session("ShouldLoadFromMru") = True) Then

            Session("ShouldLoadFromMru") = False

            Dim studentGuidStr As String
            Dim leadGuidStr As String

            Try
                If Request.QueryString("switchCampus") Is Nothing Then
                    GlobalSearchHandler(entityType)
                ElseIf Request.QueryString("switchCampus").ToLower().Trim() = "true" Then
                    RefreshSessionStateWithMru(Request.QueryString("cmpid").ToString(), entityType)
                End If

                If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                    studentGuidStr = (Guid.Empty).ToString()
                Else
                    studentGuidStr = AdvantageSession.MasterStudentId
                End If

                If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                    leadGuidStr = (Guid.Empty).ToString()
                Else
                    leadGuidStr = AdvantageSession.MasterLeadId
                End If

                Dim shadowLead = MRUFacade.GetShadowLead(studentGuidStr)

                With objStudentState
                    .StudentId = New Guid(studentGuidStr)
                    .LeadId = New Guid(leadGuidStr)
                    .ShadowLead = shadowLead
                    .Name = AdvantageSession.MasterName
                End With

                HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
                HttpContext.Current.Items("Language") = "En-US"

                ViewState("StudentMRU") = objStudentState


            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                Dim strSearchUrl As String
                strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
                Response.Redirect(strSearchUrl)
            End Try
        Else
            If (Not ViewState("StudentMRU") Is Nothing) Then
                objStudentState = CType(ViewState("StudentMRU"), StudentMRU)

            End If
        End If

        Return objStudentState

    End Function


    Private Sub RefreshSessionStateWithMru(ByVal campusId As String, ByVal entityType As Integer)
        If (entityType = 1) Then
            Dim mruProvider = New MRURoutines(Me.Context, GetAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)
            'Dim studentMru = mruProvider.BuildStudentMRU(AdvantageSession.UserState)
            Dim leadMru = mruProvider.BuildLeadMRU(AdvantageSession.UserState)
            If leadMru IsNot Nothing Then
                If leadMru.Count > 0 Then
                    AdvantageSession.MasterLeadId = leadMru.FirstOrDefault().LeadId.ToString()
                    Dim name = leadMru.FirstOrDefault().Name
                    AdvantageSession.MasterName = name
                    ViewState("LeadId") = name
                    Session("MasterName1") = name
                    Me.Context.Session("MasterName1") = name
                End If
            End If
        Else
            'Place logic here to load entity from the MRU for other Entity by using the campusId
        End If
    End Sub

    ''' <summary>
    ''' Find Control starting at Page level
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overridable Function FindControlRecursive(id As String) As Control
        Return FindControlRecursive(id, Me)
    End Function
    ''' <summary>
    ''' Find Control within a parent control
    ''' </summary>
    ''' <param name="id"></param>
    ''' <param name="parent"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overridable Function FindControlRecursive(id As String, parent As Control) As Control
        ' If parent is the control we're looking for, return it
        If String.Compare(parent.ID, id, True) = 0 Then
            Return parent
        End If
        ' Search through children
        For Each ctrl As Control In parent.Controls
            Dim match As Control = FindControlRecursive(id, ctrl)
            If Not match Is Nothing Then
                Return match
            End If
        Next
        'If we reach here then no control with id was found
        Return Nothing
    End Function

    Public Overridable Sub SetPageTitle()
        If Not Request.QueryString("resid") Is Nothing Then

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            Dim da As New ResourceDA(myAdvAppSettings.AppSettings("ConnectionString").ToString)
            Dim result = da.GetResourceByResourceID(CInt(Request.QueryString("resid")))
            If Not result Is Nothing Then
                If String.IsNullOrWhiteSpace(result.DisplayName) = True Then
                    Header.Title = result.Resource
                Else
                    Header.Title = result.DisplayName
                End If
                AdvantageSession.PageBreadCrumb = CurrentPageAsFriendlyPath()
            End If
        End If
    End Sub

    Private Sub CheckUserState()
        If Not Page.Title = "Login" Then
            If AdvantageSession.UserState Is Nothing Then
                Response.Redirect(FormsAuthentication.LoginUrl)
            End If
        End If
    End Sub
    Public Sub SetFocusOnControl(ByVal ctrlName As String)
        'Set the focus on control
        Dim mycontrol As Control = FindControlRecursive(ctrlName)
        Page.SetFocus(mycontrol)
    End Sub

    Public Function CurrentPageAsFriendlyPath() As String

        Dim currentModule As String
        Try

            Select Case Request.Params("mod")
                Case "AD"
                    currentModule = "Admissions"
                Case "AR"
                    currentModule = "Academics"
                Case "SA"
                    currentModule = "Student Accounts"
                Case "FC"
                    currentModule = "Faculty"
                Case "FA"
                    currentModule = "Financial Aid"
                Case "PL"
                    currentModule = "Placement"
                Case "HR"
                    currentModule = "Human Resources"
                Case "SY"
                    currentModule = "System"
                Case Else
                    currentModule = ""
            End Select

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            currentModule = "System"
        End Try
        Dim currentPage = Header.Title

        'Build Friendly Path
        Dim myPath As New StringBuilder
        myPath.Append(currentModule)
        myPath.Append(" / ")
        myPath.Append(currentPage)
        Return myPath.ToString
    End Function

    Public Sub ReloadUrl(strVID As String)
        Dim nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString())
        nameValues.[Set]("VID", strVID)
        Dim url As String = Request.Url.AbsolutePath
        Dim updatedQueryString As String = "?" + nameValues.ToString()
        Response.Redirect(url + updatedQueryString, False)
    End Sub
    Public Sub RedirectToLeadSearchPage(ByVal campusId As String)
        Dim strSearchUrl = "~/PL/SearchLead.aspx?resid=153&mod=AD&cmpid=" + campusId + "&desc=View Existing Leads"
        Response.Redirect(strSearchUrl, False)
    End Sub
    Public Sub RedirectToStudentSearchPage(ByVal modulecode As String, ByVal campusId As String)
        Dim strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + modulecode + "&cmpid=" + campusId + "&desc=View Existing Students"
        Response.Redirect(strSearchUrl, False)
        HttpContext.Current.ApplicationInstance.CompleteRequest()
    End Sub
    Public Sub RedirectToEmployerInfoPage(ByVal campusId As String)
        Dim strSearchUrl = "~/PL/EmployerInfo.aspx?resid=79&mod=PL&cmpid=" + campusId + "&desc=View Existing Employer"
        Response.Redirect(strSearchUrl, False)
    End Sub

    Public Sub RedirectToEmployeeSearchPage(ByVal campusId As String)
        Dim strSearchUrl = "~/SY/EmployeeSearch.aspx?resid=309&mod=HR&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Employee"
        Response.Redirect(strSearchUrl, False)
    End Sub

    Public Sub RedirectToLeadInfoPage(ByVal campusId As String)
        Dim strSearchUrl = "~/AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + campusId + "&desc=View Lead"
        Response.Redirect(strSearchUrl, False)
    End Sub
    Public Sub RedirectToStudentInfoPage(ByVal modulecode As String, ByVal campusId As String)
        Dim strSearchUrl = "~/PL/StudentMaster.aspx?resid=203&mod=" + modulecode + "&cmpid=" + campusId + "&desc=View Student"
        Response.Redirect(strSearchUrl, False)
        HttpContext.Current.ApplicationInstance.CompleteRequest()
    End Sub
    Public Sub RedirectToEmployerSearchPage(ByVal campusId As String)
        Dim strSearchUrl = "~/PL/EmployerInfo.aspx?resid=79&mod=PL&cmpid=" + campusId + "&desc=View Employer"
        Response.Redirect(strSearchUrl, False)
    End Sub

    Public Sub RedirectToEmployeeInfoPage(ByVal campusId As String)
        Dim strSearchUrl = "~/SY/EmployeeInfo.aspx?resid=52&mod=HR&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Employee"
        Response.Redirect(strSearchUrl, False)
    End Sub

    Private Sub SetMinimumSelectableDate()
        Dim datePickers As List(Of RadDatePicker)
        datePickers = Controls.GetAllControlsOfType(Of RadDatePicker)()
        For Each dPicker As RadDatePicker In datePickers
            dPicker.MinDate = CDate("01/01/1900")
        Next
    End Sub


    Public uSearchEntityControlId As HiddenField = New HiddenField()

    Public Sub GlobalSearchHandler(entitySearchTypeId As Integer, Optional ByVal mruProvider As MRURoutines = Nothing)

        Dim uSearchEntityType As HiddenField = CType(Page.Master.FindControl("hdnEntityType"), HiddenField)
        If uSearchEntityType Is Nothing Then
            uSearchEntityType = CType(Page.Master.Master.FindControl("hdnEntityType"), HiddenField)
        End If

        ' Remove the new from query string
        If Request.QueryString("new") IsNot Nothing Then
            RemoveFromQueryString("new")
        End If

        Dim entityName As String = String.Empty
        If (mruProvider Is Nothing) Then
            mruProvider = New MRURoutines(Context, GetAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)
        End If
        Dim uSearchTypeStr As String = Nothing

        If uSearchEntityType Is Nothing Or String.IsNullOrEmpty(uSearchEntityType.Value) Then
            If Request.QueryString("entityType") IsNot Nothing Then
                uSearchTypeStr = Request.QueryString("entityType").ToString()
                RemoveFromQueryString("entityType")
            End If
        Else
            uSearchTypeStr = uSearchEntityType.Value
        End If

        Dim uSearchEntityStr As String = Nothing
        uSearchEntityControlId = CType(Page.Master.FindControl("hdnCurrentEntityId"), HiddenField)
        If uSearchEntityControlId Is Nothing Then
            uSearchEntityControlId = CType(Page.Master.Master.FindControl("hdnCurrentEntityId"), HiddenField)
        End If
        If uSearchEntityControlId Is Nothing Or String.IsNullOrEmpty(uSearchEntityControlId.Value) Then
            If Request.QueryString("entity") IsNot Nothing Then
                uSearchEntityStr = Request.QueryString("entity")
                RemoveFromQueryString("entity")
            End If
        Else
            uSearchEntityStr = uSearchEntityControlId.Value
        End If

        If String.IsNullOrEmpty(uSearchEntityStr) Then
            If (entitySearchTypeId = -1) Then
                Exit Sub
            End If
            uSearchEntityStr = entitySearchTypeId.ToString()
        End If

        If (IsNothing(mruProvider)) Then
            mruProvider = New MRURoutines(Me.Context, GetAdvAppSettings.AppSettings("AdvantageConnectionString").ToString)
        End If

        If Not String.IsNullOrEmpty(uSearchTypeStr) Then
            'student
            If uSearchTypeStr = "0" Then
                If uSearchEntityStr IsNot Nothing Then
                    If Not String.IsNullOrEmpty(uSearchEntityStr) Then

                        Dim guidEntity As Guid
                        If (Guid.TryParse(uSearchEntityStr, guidEntity)) Then
                            AdvantageSession.MasterStudentId = uSearchEntityStr
                            ViewState("StudentdId") = uSearchEntityStr  'Update the view state of the page

                            mruProvider.InsertMRU(1, uSearchEntityStr, AdvantageSession.UserId, AdvantageSession.UserState.CampusId.ToString)

                            entityName = mruProvider.GetEntityNameByEntityType(New Guid(uSearchEntityStr), "STUDENT")
                        End If
                    End If

                    If Not String.IsNullOrEmpty(entityName) Then
                        AdvantageSession.MasterName = entityName

                        Me.Context.Session("MasterName1") = entityName
                    End If

                    If entitySearchTypeId <> uSearchTypeStr Then
                        RedirectBasedOnEntityType(0)
                    End If

                End If
            End If

            'lead
            If uSearchTypeStr = "1" Then

                If uSearchEntityStr IsNot Nothing Then
                    If Not String.IsNullOrEmpty(uSearchEntityStr) Then
                        AdvantageSession.MasterLeadId = uSearchEntityStr
                        mruProvider.InsertMRU(4, uSearchEntityStr, AdvantageSession.UserId, AdvantageSession.UserState.CampusId.ToString)
                    End If
                End If


                entityName = mruProvider.GetEntityNameByEntityType(New Guid(uSearchEntityStr), "LEAD")
                If entityName IsNot Nothing Then
                    If Not String.IsNullOrEmpty(entityName) Then
                        AdvantageSession.MasterName = entityName
                        ViewState("LeadId") = uSearchEntityStr
                        Session("MasterName1") = AdvantageSession.MasterName

                        Me.Context.Session("MasterName1") = entityName
                    End If
                End If

                If entitySearchTypeId <> uSearchTypeStr Then
                    RedirectBasedOnEntityType(1)
                End If

            End If

            'employer
            If uSearchTypeStr = "2" Then

                If uSearchEntityStr IsNot Nothing Then
                    If Not String.IsNullOrEmpty(uSearchEntityStr) Then
                        AdvantageSession.MasterEmployerId = uSearchEntityStr
                        mruProvider.InsertMRU(2, uSearchEntityStr, AdvantageSession.UserId, AdvantageSession.UserState.CampusId.ToString)
                    End If
                End If

                entityName = mruProvider.GetEntityNameByEntityType(New Guid(uSearchEntityStr), "EMPLOYER")
                If entityName IsNot Nothing Then
                    If Not String.IsNullOrEmpty(entityName) Then
                        AdvantageSession.MasterName = entityName
                        Session("MasterName1") = AdvantageSession.MasterName

                        Me.Context.Session("MasterName1") = entityName
                    End If
                End If

                If entitySearchTypeId <> uSearchTypeStr Then
                    RedirectBasedOnEntityType(2)
                End If

            End If

            'employee
            If uSearchTypeStr = "3" Then

                If uSearchEntityStr IsNot Nothing Then
                    If Not String.IsNullOrEmpty(uSearchEntityStr) Then
                        Dim guidEntity As Guid
                        If (Guid.TryParse(uSearchEntityStr, guidEntity)) Then
                            AdvantageSession.MasterEmployeeId = uSearchEntityStr
                            mruProvider.InsertMRU(3, uSearchEntityStr, AdvantageSession.UserId, AdvantageSession.UserState.CampusId.ToString)
                            entityName = mruProvider.GetEntityNameByEntityType(New Guid(uSearchEntityStr), "EMPLOYEE")
                        End If
                    End If
                End If

                If entityName IsNot Nothing Then
                    If Not String.IsNullOrEmpty(entityName) Then
                        AdvantageSession.MasterName = entityName
                        Session("MasterName1") = AdvantageSession.MasterName

                        Context.Session("MasterName1") = entityName
                    End If
                End If

                If entitySearchTypeId <> uSearchTypeStr Then
                    RedirectBasedOnEntityType(3)
                End If


            End If

        End If
    End Sub

    Private Sub RemoveFromQueryString(val As String)

        Dim isreadonly As PropertyInfo = GetType(NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance Or BindingFlags.NonPublic)

        ' make collection editable
        isreadonly.SetValue(Request.QueryString, False, Nothing)

        ' remove
        Request.QueryString.Remove(val)

        ' make collection read-only again
        isreadonly.SetValue(Request.QueryString, True, Nothing)

    End Sub

    Private Sub RedirectBasedOnEntityType(ByVal entityType As Integer)

        If entityType = 0 Then
            RedirectToStudentInfoPage("AR", AdvantageSession.UserState.CampusId.ToString)
        ElseIf entityType = 1 Then
            RedirectToLeadInfoPage(AdvantageSession.UserState.CampusId.ToString)
        ElseIf entityType = 2 Then
            RedirectToEmployerInfoPage(AdvantageSession.UserState.CampusId.ToString)
        ElseIf entityType = 3 Then
            RedirectToEmployeeInfoPage(AdvantageSession.UserState.CampusId.ToString)
        Else
            RedirectToStudentInfoPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
        End If

    End Sub

    Protected Function GetAdvAppSettings() As AdvAppSettings
        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If
        Return myAdvAppSettings
    End Function

#End Region

#Region "Base Page Events"
    Protected Overrides Sub OnInit(e As EventArgs)
        CheckUserState()
        MyBase.OnInit(e)
    End Sub
    Protected Overrides Sub OnLoad(ByVal e As EventArgs)
        Dim resid As Integer = CType(Request.QueryString("resid"), Integer)
        If resid <> 170 Then
            SetPageTitle()
            SetMinimumSelectableDate()
        End If
        Session("ShouldLoadFromMru") = False
        If (Not Request("__EVENTTARGET") Is Nothing) Then

            If (Request("__EVENTTARGET").ToString().Equals("mruChanged", StringComparison.InvariantCultureIgnoreCase)) Then
                Session("RedirectFromMru") = String.Empty
                Session("ShouldLoadFromMru") = True
                Session("CurrentEnrollmentId") = Nothing

                Session("UseSelectedEnrollment") = false
            ElseIf (Request("__EVENTTARGET").ToString().Equals("mruChanged_and_redirect_542", StringComparison.InvariantCultureIgnoreCase)) Then
                Session("ShouldLoadFromMru") = True
                Session("CurrentEnrollmentId") = Nothing
                Session("RedirectFromMru") = Request.Url.ToString()
                ' Response.Redirect(request.Url.ToString())
            End If
        End If
        'Be sure to call the base class's OnLoad method!
        MyBase.OnLoad(e)
    End Sub
    Protected Overrides Sub OnPreInit(e As EventArgs)
        ' LoadPageTheme()
        MyBase.OnPreInit(e)
    End Sub
    Protected Overrides Sub OnPreRender(e As EventArgs)
        BindToolTip()
        MyBase.OnPreRender(e)
    End Sub
    Protected Overrides Sub OnInitComplete(e As EventArgs)
        MyBase.OnInitComplete(e)
    End Sub
    Protected Overrides Sub OnLoadComplete(e As EventArgs)
        MyBase.OnLoadComplete(e)
    End Sub
    Protected Overrides Sub OnPreLoad(e As EventArgs)
        MyBase.OnPreLoad(e)
    End Sub
    Protected Overrides Sub OnPreRenderComplete(e As EventArgs)
        MyBase.OnPreRenderComplete(e)
    End Sub
    Protected Overrides Sub OnUnload(e As EventArgs)
        MyBase.OnUnload(e)
    End Sub
#End Region

#Region "Bind Tooltips Subs"

    Protected Sub BindToolTip()
        Dim placeholder = FindControlRecursive("ContentMain2")

        If Not placeholder Is Nothing Then
            For Each ctl As Control In placeholder.Controls
                If TypeOf ctl Is ListControl Or TypeOf ctl Is DropDownList Or TypeOf ctl Is ListBox Then
                    For i As Integer = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                        DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                    Next
                End If
                If TypeOf ctl Is Panel Then
                    BindToolTipForControlsInsideaPanel(ctl)
                End If
                If TypeOf ctl Is DataGrid Then
                    BindToolTipForControlsInsideaGrid(ctl)
                End If

                If TypeOf ctl Is RadSplitter Then
                    'Find the Radpane
                    Dim radpane As RadPane

                    Dim ctl2 As Control
                    For Each ctl2 In ctl.Controls
                        If TypeOf ctl2 Is RadPane Then
                            'Get to the radpane
                            radpane = ctl2
                            BindToolTipForControlsInsideaRadpane(radpane)
                        End If
                    Next
                End If
            Next
        End If
    End Sub
    Public Sub BindToolTip(ctrlName As String)
        Dim ctrl = FindControlRecursive(ctrlName)

        If TypeOf ctrl Is ListControl Then
            Dim mylist As ListControl = DirectCast(ctrl, ListControl)
            For Each myitem As ListItem In mylist.Items
                myitem.Attributes.Add("title", myitem.Text)
            Next
        ElseIf TypeOf ctrl Is RadComboBox Then
            Dim radbox As RadComboBox = DirectCast(ctrl, RadComboBox)
            For Each myitem As RadComboBoxItem In radbox.Items
                myitem.ToolTip = myitem.Text
            Next
        End If
    End Sub

    Public Sub BindToolTipForControlsInsideaRadpane(ByVal Ctrlpanel As RadPane)
        Dim j As Integer
        For Each ctrl As Control In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim j As Integer
        For Each ctrl As Control In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub

#End Region

#Region "WebMethods"
    <WebMethod(EnableSession:=True)>
    Public Shared Sub PokePage()
        ' called by client to refresh session
        ' MiscUtilities.ODS("Server: I am poked")
        Console.Write("poked")
    End Sub
#End Region
End Class
