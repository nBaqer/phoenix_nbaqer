﻿
Imports System.Web.UI
Imports System

Public Module advExtensions
    <System.Runtime.CompilerServices.Extension()>
    Public Function GetAllControlsOfType(Of T As Control)(ByVal controls As Web.UI.ControlCollection) As List(Of T)
        Dim controlsArray(controls.Count - 1) As Control
        controls.CopyTo(controlsArray, 0)
        Dim oldControlSieveList As New List(Of Control)(controlsArray)
        Dim newControlSieveList As List(Of Control)
        Dim targetControlList As New List(Of T)
        While oldControlSieveList.Count > 0
            newControlSieveList = New List(Of Control)
            For Each parentControl As Control In oldControlSieveList
                If parentControl.HasControls Then
                    For Each childControl As Control In parentControl.Controls
                        newControlSieveList.Add(childControl)
                    Next
                End If
                If TypeOf parentControl Is T Then targetControlList.Add(CType(parentControl, T))
            Next
            oldControlSieveList = newControlSieveList
        End While
        'Return targetControlList.ToArray
        Return targetControlList.ToList
    End Function
End Module
