﻿Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.Advantage.Site.Lib.Infrastruct.Helpers
Imports Microsoft.Practices.ServiceLocation
Imports FAME.Advantage.Common.Services

Partial Class Template
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <Diagnostics.DebuggerStepThrough> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(sender As System.Object, e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region
    Protected campusId As String
    Private Sub Page_Load(sender As System.Object, e As EventArgs) Handles MyBase.Load

        campusId = Master.CurrentCampusId

        If Me.Master.IsSwitchedCampus = True Then
            CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
        End If

    End Sub

End Class

