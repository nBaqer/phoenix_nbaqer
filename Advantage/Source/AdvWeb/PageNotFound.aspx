﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PageNotFound.aspx.vb" Inherits="PageNotFound" %>
<%@ Register Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Page Not Found</title>
	<link rel="Stylesheet" type="text/css" href="css/Login.css" />
	<link rel="Stylesheet" type="text/css" href="css/localhost_lowercase.css" />
	<style type="text/css">
		.style1
		{
			height: 17px;
		}
	</style>
   </head>
<body>
  <form id="form1" runat="server" enableviewstate="False">
	<telerik:RadScriptManager ID="scriptmanager1" runat="server"></telerik:RadScriptManager>
	  <telerik:RadFormDecorator ID="RadFormDecorator1" Runat="server" DecoratedControls="Default, ValidationSummary" />

	  <div class="clwyPageHeader"> 
		   
			 <div class="clwyToolbarHeaderInfo">           
			<div class="clwyToolbarHeaderVersionInfo">
				<asp:Label ID="lblApplicationHeaderVersion" runat="server" />
			</div>
			<div class="clwyToolbarHeaderActionsInfo">&nbsp;</div>
		</div>
		</div>
		<div class="clwyLoginContent">
		
		 <div>
						<asp:Label ID="lblText" runat="server" style=" font-family:Arial; font-size:15pt; font-weight:bold; text-align:left; color:Navy" Visible="false">Error Occured</asp:Label>
						<asp:HiddenField ID="hdnpreviousURL" runat="server" />
					</div>
	
	<div>
   
			<asp:Image ID="Image1" ImageUrl="~/images/warningIcon.png" runat="server" />
					 </div>
		
				<div style="padding:5px 0px 5px 0px;">
			  
				<asp:Label ID="lblErrorReason" CssClass="ErrorMessageheader"  runat="server" Text="Label"></asp:Label>
		  </div>
			 
		 <div style="padding:5px 0px 5px 0px;">

	<asp:Button ID="btnSendEmail" runat="server" style="vertical-align: middle;font: normal 12px ;color: #000066;"
							Text="Send notification to Advantage Customer Support" />
					</div>
			 
	  
			 <div class="ErrorMessageheader" style="padding:5px 0px 5px 0px;">
			   To continue, go to <asp:LinkButton ID="lnkPreviousPage" runat="server" CssClass="ErrorMessageheader" Text="Previous Page"></asp:LinkButton>  or  <asp:LinkButton ID="lnkLogin" runat="server" CssClass="ErrorMessageheader" Text="Log In"></asp:LinkButton> page 
		
			 </div>
							
				
				
			<div style="padding:5px 0px 5px 0px;"  >
			<asp:Label ID="lblErrorheader" runat="server" CssClass="labelheader" Text="Detailed error for FAME"></asp:Label>
			</div>
				
		<div style="padding:5px 0px 5px 0px;">
			<asp:Label ID="lblerror" runat="server"  Text="Label" CssClass="label"  BackColor="#FFFFCC"></asp:Label>
		</div>
				
			
		
		</div>
	  <div class="clwyLoginPageFooterLabel">
		<table Height="40" width="100%" style="background-color:#3C62A0; padding-bottom:2px;">
		<tr>
			<td>
				<asp:Label ID="lblfooter" runat="server" style="width:250px;margin:auto;color:white;font-size:x-small;">
					Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>
				</asp:Label>
			</td>
		</tr>
	 </table>
		</div>
 
	</form>
</body>
</html>
