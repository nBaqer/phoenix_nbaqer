﻿
Imports Telerik.Web.UI

Partial Class TEST
    Inherits BasePage
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim StudentTabBar_Master As RadTabStrip = Master.FindControl("StudentTabBar")
        StudentTabBar_Master.DataBind()

        Dim RadPanel1_Master As RadPane = Master.FindControl("RadPane1")
        RadPanel1_Master.Width = 240
        Page.Title = "TEST - ROOT1 "
    End Sub
End Class
