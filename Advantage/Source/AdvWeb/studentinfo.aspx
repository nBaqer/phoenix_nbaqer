﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentInfo.aspx.vb" Inherits="StudentInfo" %>
<%@ Register TagPrefix="fame" TagName="WonderBar" Src="~/UserControls/WonderBar.ascx" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 



<asp:Content ID="StudentInfo_Content1" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <fame:WonderBar ID="MyWonderBar" runat="server" />
</asp:Content>

<asp:Content ID="StudentInfo_Content3" ContentPlaceHolderID="ContentError" Runat="Server">
        <asp:ValidationSummary ID="AdvantageErrorDisplay" runat="server" 
        EnableViewState="true" style="margin: 10px;" ValidationGroup="Main" />
</asp:Content>

<asp:Content ID="StudentInfo_Content2" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:Panel ID="MainPanel" runat="server">

        <telerik:RadTabStrip ID="RadStudentTabs" runat="server" MultiPageID="RadMultiPage1" 
        OnClientTabSelecting="onTabSelecting" OnTabClick="RadStudentTabs_TabClick" 
        ScrollChildren="True" SelectedIndex="0" ShowBaseLine="True">

            <tabs>
            <telerik:RadTab runat="server" Text="Profile" Value="StudentInfo">
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Contacts" Value="StudentContacts">
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Docs" Value="StudentDocs">
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Notes" Value="StudentNotes">
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Academics" Value="StudentAcademics">
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Account" Value="StudentAccounts">
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Fin Aid" Value="StudentFinAid">
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Disclosure" Value="StudentDisclosure">
            </telerik:RadTab> 
            </tabs>

        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0">
        </telerik:RadMultiPage>
    </asp:Panel>
</asp:Content>



