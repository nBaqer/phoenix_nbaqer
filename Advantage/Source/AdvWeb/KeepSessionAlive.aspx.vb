﻿
Namespace AdvWeb

    Partial Class KeepSessionAlive
        Inherits Page

        Protected WindowStatusText As String = String.Empty

        Protected Sub Page_Load(sender As Object, e As EventArgs)
            If User.Identity.IsAuthenticated Then
                ' Refresh this page 60 seconds before session timeout, effectively resetting the session timeout counter.
                MetaRefresh.Attributes("content") = CType((Convert.ToString((Session.Timeout * 60) - 60) + ";url=KeepSessionAlive.aspx?q=" + DateTime.Now.Ticks), String)
                WindowStatusText = "Last refresh " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString()
            End If
        End Sub

    End Class
End Namespace