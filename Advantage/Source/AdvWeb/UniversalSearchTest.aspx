﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UniversalSearchTest.aspx.vb" Inherits="UniversalSearchTest" MasterPageFile="~/NewSiteSearchTest.master" %>
<%@ MasterType  virtualPath="~/NewSiteSearchTEst.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
        <asp:ValidationSummary ID="AdvantageErrorDisplay" runat="server" 
        EnableViewState="true" style="margin: 10px;" ValidationGroup="Main" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">

  <telerik:RadToolBar ID="AdvToolBar" runat="server" Width="100%" AutoPostBack="true">
               <Items>
  <telerik:RadToolBarButton runat="server" Text="sep1" IsSeparator="true">
    </telerik:RadToolBarButton> 
                  
</Items>
  </telerik:RadToolBar>
            <p><b>Current Entity ID:</b><asp:Label runat="server" ID="lblEntityId"></asp:Label></p>
            <p><b>Current Entity Name:</b><asp:Label runat="server" ID="lblEntityName"></asp:Label></p>
 
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>
