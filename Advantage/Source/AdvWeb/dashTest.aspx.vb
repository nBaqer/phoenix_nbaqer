﻿
Imports Telerik.Web.UI

Partial Class dashTest
    Inherits BasePage


    Public VoyantUrl As String

#Region "Events"
    Protected Sub BindNavigation(ByVal sender As Object, ByVal e As RadSiteMapNodeEventArgs)

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        'gZipCompression.GZipEncodePage()

        Dim resId As Integer
        Try
            'resId = CType(Request.QueryString.Get("resid"), Integer)
            resId = CInt(HttpUtility.ParseQueryString(Request.UrlReferrer.Query())("resid"))

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            resId = 0
        End Try
        'Happens only when user switches campus and page is either not part of new campus/insufficient permission
        If Request.QueryString.ToString.Contains("&redirect") AndAlso resId > 0 AndAlso Not resId = 264 Then
            CampusObjects.ShowNotificationWhileSwitchingCampus(6, "")
        End If

        'If user gets to dashboard reset session variables that hold VID
        'reason: if not reset Advantage pulls in wrong entity while switching campuses 
        Session("LeadObjectPointer") = ""
        Session("StudentObjectPointer") = ""
        Session("EmployerObjectPointer") = ""
        Session("EmployeeObjectPointer") = ""

        If Session("UserId") IsNot Nothing Then
            hdnUserId.Value = Session("UserId").ToString()
        End If


    End Sub
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit

    End Sub
    Protected Sub RadDock_Command(sender As Object, e As DockCommandEventArgs)

    End Sub
#End Region


End Class
