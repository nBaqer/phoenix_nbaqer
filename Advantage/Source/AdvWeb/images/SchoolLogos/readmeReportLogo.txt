To enter a logo in Ad Hoc Report, change the ReportLogo.jpg file, with your file logo. Rename your file as ReportLogo.jpg.
The logo has a limited width of 120px, and appear left over the report.