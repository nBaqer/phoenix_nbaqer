/*
**************************************************************************************
	FAME
	Copyright (c) 2005
	by Benjamin Strum of ThinkTron Corporation (www.thinktron.com)
	Description: Registers toolbars and the command handlers needed to integrate
		AdvMessaging with the FCKEditor
***************************************************************************************	

/*
	main entry point functions for FCKEditor plugins
*/
function regOpenFromWordCommandButton() 
{
	//FCKCommands.RegisterCommand( 'OpenWord', new FCKAdvMsgDialogCommand(''));
	FCKCommands.RegisterCommand( 'OpenWord', new FCKDialogCommand( 'OpenWord', FCKLang.OpenWordDlgTitle,  
		FCKConfig.PluginsPath + 'OpenWord/openword.html', 340, 250 ) ) ; 
	var oItem		= new FCKToolbarButton( 'OpenWord', 
		FCKLang['OpenWordDlgTitle'], FCKLang['OpenWordDlgTitle_Tip'], FCK_TOOLBARITEM_ICONTEXT ) ;
	
	oItem.IconPath	= FCKConfig.PluginsPath + '/Plugins/OpenWord/word.gif' ;
	FCKToolbarItems.RegisterItem( 'OpenWord', oItem ) ;
}


// Call all the entry point functions
regOpenFromWordCommandButton();
