OpenWord-plugin voor FCKeditor 2.0

This plugin only works in Microsoft Internet Explorer, also ActiveX has to be enabled and, of course, Microsoft Word has to be installed on the client-computer.

To install this plugin unzip the zip-file and copy the folder 'OpenWord' and all of its contents to your 'plugins'-folder.


In case the browser is not Internet Explorer FCKEditor has to load with it's original method of copying and pasting the content of a Word-document.
So in your 'fckconfig.js' you should build in a check for the presence of Internet Explorer.
Put the following lines into 'fckconfig.js', just before the line  'FCKConfig.ToolbarSets["Default"] = [ .... ]


			var OP   = (navigator.userAgent.indexOf("Opera") != -1);
			var IE   = (navigator.userAgent.indexOf("MSIE") != -1) && !OP;
			if (IE)														/* we have IE on the client-computer */
			{
				try
				{
					var Word = new ActiveXObject('Word.Application');	/* can we open Microsoft Word? */
					var PasteWord = 'OpenWord';
					Word.Quit();
				}
				catch(e)
				{
					//alert('enable ActiveX');
					var PasteWord = 'PasteWord';
				}
			}
			else														/* we have Gecko or some other superior browser */
			{
				var PasteWord = 'PasteWord';
			}


and in the ToolbarSets unquote 'PasteWord'.
It wil look like:

			FCKConfig.ToolbarSets["Default"] = [
				['Source','DocProps','-','Save','NewPage','Preview','-','Templates'],
				['Cut','Copy','Paste','PasteText',PasteWord,'-','Print','SpellCheck'],
				['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
				['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
				['OrderedList','UnorderedList','-','Outdent','Indent'],
				['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
				['Link','Unlink','Anchor'],
				['Image','Flash','Table','Rule','Smiley','SpecialChar','UniversalKey'],
				['Form','Checkbox','Radio','TextField','Textarea','Select','Button','ImageButton','HiddenField'],
				'/',
				['Style','FontFormat','FontName','FontSize'],
				['TextColor','BGColor'],
				['About']
			] ;


To enable ActiveX in Internet Explorer do the following:
Click on 'Tools'  and on 'Internet Options' and select the tab labeled as 'Security'.
Select 'Trusted Sites' and click on the 'Sites'-button.
Write the URL of your website (e.g. http://www.domein.com) in the box labeled as 'Add this Web site to the zone' and click on 'Add'.
Now you have to uncheck the box that reads 'Require server verification (https://) for web sites in this zone' and hit 'OK'.
At the end, click on the button labeled as 'Custom Level' and, from  the next window enable the 5 ActiveX-settings.

Koen
kwillems@zonnet.nl
