
function Ok ()
{
	var strSelection = document.getElementById('lbFields').value;
	if(strSelection == '')
	{
		alert (FCKLang.InsertFieldAlert);				/* It's not a Word-document! */
	}
	else												/* Wow, it's a Word-document! */
	{
		oEditor.FCK.InsertHtml( strSelection ) ;		/* put the content into FCK */
		window.close();									/* and close the window */
	}
}
