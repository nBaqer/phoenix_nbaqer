﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="dash.aspx.vb" Inherits="dash" %>

<%@ Register TagPrefix="fame" TagName="StatusBar" Src="~/StatusBar.ascx" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Import Namespace="System.Web.Optimization" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <%--<link rel="stylesheet" href="Kendo/styles/kendo.common.min.css" />--%>
    <%--<link href="Kendo/styles/kendo.dataviz.blueopal.min.css" rel="stylesheet" />--%>
    <%--<link rel="stylesheet" href="Kendo/styles/kendo.default.min.css" />--%>
    <%--<link rel="stylesheet" href="Kendo/styles/kendo.dataviz.min.css" />
    <link rel="stylesheet" href="Kendo/styles/kendo.dataviz.default.min.css" />--%>
    <%--    <link href="css/AD/DashBoard1.css" rel="stylesheet" />--%>
    <%: Scripts.Render(System.Web.Optimization.BundleTable.Bundles.ResolveBundleUrl("~/bundles/ApiClientScripts", true)) %>
    <script type="text/javascript" src="Scripts/FileSaver.js"></script>
    <script type="text/javascript" src="Scripts/Storage/storageCache.js"></script>
    <script type="text/javascript" src="Scripts/common-util.js"></script>
    <script type="text/javascript" src="Scripts/viewModels/transactions.js"></script>
    <script type="text/javascript" src="Scripts/common/kendoControlSetup.js"></script>
    <%--<script type="text/javascript" src="Scripts/ElementQueries.min.js"></script>
    <script type="text/javascript" src="Scripts/ResizeSensor.min.js"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ValidationSummary ID="AdvantageErrorDisplay" runat="server"
        EnableViewState="true" Style="margin: 10px;" ValidationGroup="Main" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <div id="KendoContainerLogger" style="height: 100%; overflow: auto">


        <fame:StatusBar ID="MyStatusBar1" runat="server" Visible="false" />

        <style type="text/css">
            .toolbarColor {
                background-color: #355b99;
            }

            #toolbar .k-toolbar, .k-toolbar .k-button {
            }

            #toolbar .k-button:active, .k-button.k-state-active {
                color: #fff;
                background-color: #355b99;
                border-color: #355b99;
            }

            #toolbar .k-button:hover {
                color: #fff;
                background-color: #355b99;
                border-color: #355b99;
            }

            #toolbar .k-button:checked {
                color: #fff;
                background-color: #355b99;
                border-color: #355b99;
            }

            .k-block, .k-header .k-toolbar, .k-grouping-header, .k-pager-wrap, .k-draghandle,
            .k-treemap-tile, html .km-pane-wrapper .k-header {
                background-color: white;
                border-color: white;
            }

            #toolbar {
                display: table;
                margin: 0 auto;
            }

            #projectionToolbar {
                display: table;
                margin: 0 auto;
            }

            .layerwidget {
                border: 2px solid;
                border-color: #355b99;
                border-radius: 8px;
                margin: 10px;
                padding: 5px 5px;
            }

            .handler {
                padding: 1px;
                cursor: move;
            }

            #UnAssignedWidget > svg > g > g {
                cursor: pointer;
            }

            #MyLeadWidget > svg > g > g {
                cursor: pointer;
            }

            #example {
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            .dash-head {
                width: 100%;
                height: 50px;
                background: #222222;
            }

            .panel-wrap {
                display: table;
                margin: 0 0 20px;
                width: 100%;
                background-color: white;
            }

            #sidebar {
                display: table-cell;
                margin: 0;
                padding: 1% 1% 1% 1%;
                width: 98%;
                vertical-align: top;
            }

            #main-content {
                margin: 0;
                padding: 20px;
                width: 60%;
                vertical-align: top;
            }

            .widget.placeholder {
                opacity: 0.4;
                border: 1px dashed #a6a6a6;
            }

            /* WIDGETS */
            .widget {
                /*margin: 0 0 20px;*/
                padding: 0;
                background-color: #ffffff;
                display: block;
                border: 1px solid #e7e7e7;
                border-radius: 3px;
                cursor: move;
                float: left;
                margin-right: 1%;
            }

                .widget:hover {
                    background-color: #fcfcfc;
                    border-color: #cccccc;
                }

                .widget div {
                    /*padding: 10px;*/
                    /*padding: 0;*/
                    /*min-height: 50px;*/
                }

                .widget h3 {
                    font-size: 12px;
                    margin-top: 0px;
                    margin-bottom: 0px;
                    padding: 8px 10px;
                    text-transform: uppercase;
                    /*border-bottom: 1px solid #e7e7e7;*/
                }

                    .widget h3 span {
                        float: right;
                    }

                        .widget h3 span:hover {
                            cursor: pointer;
                            background-color: #e7e7e7;
                            border-radius: 20px;
                        }

            #titleIVNoticesGrid {
                border-width: 0;
                /*height: 100%;*/ /* DO NOT USE !important for setting the Grid height! */
            }

            .hint {
                width: 250px;
                height: 100px;
                overflow: hidden;
            }

                .hint > h3 {
                    padding-left: 20px;
                }

            .widget:last-child {
                margin-left: 1%;
                margin-right: 0px;
                width: 49% !important;
            }

            .widgetFullSize {
                width: 48% !important;
                height: 300px;
            }

            #titleIVSapNoticesWidget .widgetContent {
                height: 230px;
                min-height: 150px;
            }

            .k-grid .k-grid-header .k-header .k-link {
                overflow: visible;
                white-space: normal;
            }
        </style>

        <asp:HiddenField runat="server" ID="hdnUserId" ClientIDMode="Static" />

        <div id="example" style="height: 100%" runat="server">

            <div class="panel-wrap" style="height: 100%;">
                <div id="sidebar" style="height: 100%">
                    <div id="transactionContainer" class='widget widgetFullSize' style="display: none;">
                        <h3 class="handler">Payment Periods
								<span class="collapse k-icon k-i-arrowhead-n" id="payPeriodCollapse"></span>
                        </h3>
                        <div class="k-content">
                            <div style="display: none; text-align: center;" class="k-content" id="noAuth"><span style="font-size: 14px;">To access this feature, please contact your System Administrator.</span></div>
                            <div id="tabstrip" style='display: table; background-color: white; width: 98%; border-color: white;'>
                                <ul>
                                    <li id="singleStudentTab" class="k-state-active">Charges Auto Posted to Ledger</li>
                                    <li>Payment Period Projections</li>
                                </ul>
                                <div id="chartContent">
                                    <div id="toolbar"></div>
                                    <div id="chart" style="width: 600px; height: 275px;" data-bind="source: transactionSummaryDataSource"></div>
                                    <div style="display: table; margin: 0 auto;" class="k-content" id="moreDetail">Click a bar or details button to see more detail</div>

                                </div>
                                <div id="chartProjectionContent">
                                    <div id="projectionToolbar"></div>
                                    <div id="projectionChart" style="width: 600px; height: 257px;" data-bind="source: projectionSummaryDataSource"></div>
                                    <br />
                                    <div style="display: table; margin: 0 auto; font-weight: bold;" class="k-content" id="morePrDetail">Click a bar or details button to see more detail</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="titleIVSapNoticesWidget" class="widget widgetFullSize" style="display: none">
                        <h3 class="k-header" id="sapNoticeHeader" style="height: 15px;">Title IV SAP Notices <%--<span class="collapse k-icon k-i-sort-asc-sm"></span>--%></h3>
                        <div class="widgetContent">
                            <div id="titleIVNoticesGrid"></div>
                        </div>
                        <div class="k-footer" style="width: 100%; height: 35px;">
                            <button class="k-button" type="button" style="float: right; margin-right: 5px; margin-top: 1%;" data-bind="enabled: canPrint, click: print">Print</button>
                        </div>
                    </div>

                    <div id="studentsReadyForGraduationWidget" class="widget widgetFullSize" style="display: none">
                        <h3 class="k-header" id="studentsReadyForGraduationHeader" style="height: 15px;">Students that have met program length 
                             <span id="gradRefreshBtn" class="k-icon k-i-refresh" ></span>
                        </h3>
                        <div class="widgetContent">
                            <div id="studentsReadyForGraduationGrid"></div>
                        </div>
                        <div class="k-footer" style="width: 99%; padding-bottom: 5px; padding-top: 5px; padding-left: 1%;">
                            <span>Filter To:</span>
                            <input type="text" id="ddlInSchoolStatuses" name="ddlInSchoolStatuses" />
                            <input type="text" id="ddlHaveMet"  name="ddlHaveMet"/>
                            <span>Set all To:</span>
                            <input type="text" id="ddlGraduationStatuses" name="ddlGraduationStatuses" />
                            <button class="k-button" type="button" style="float: right; margin-right: 5px;" data-bind="enabled: canGrad, click: changeStatusSelectedStudents">Change Status Selected Students</button>
                        </div>
                    </div>
                </div>
                
            </div>
<div id="main-content" style="height: 100%">
                    <div id="DashBoardVoyant" class="widget" style="display: none;">
                        <h3 class="handler">Voyant Predictions 
							<%--<span class="closed k-icon k-i-close"></span>--%>
                            <span class="collapse k-icon k-i-arrowhead-n"></span>
                            <span id="openWinTt">
                                <span class="openwin k-icon k-i-maximize" style="visibility: hidden"></span>
                            </span>
                            <span class="refresh k-icon k-i-refresh" style="visibility: hidden"></span>
                        </h3>

                        <div id="Dash">
                            <iframe id="VoyantFrame" src="" style="width: 100%; height: 330px; border: 0" sandbox="allow-scripts"></iframe>
                        </div>
                        <div id="winVoyant" style="display: none;"></div>
                        <!-- Windows Control to Show On Demand waiting Operation -->
                        <div id="WindowsWapiDashOnDemand" style="display: none">
                            <div class="image-type">
                                <img alt="Waiting for On demand Operation" src="images/Gearstar.gif" />
                            </div>
                            <div class="button-type">
                                <button id="btnDashCancelOnDemand" class="k-button" type="button" style="width: 130px; height: 50px">Cancel On Demand</button>
                            </div>
                        </div>
                    </div>


                </div>
        </div>

        <div id="trDetails" style="display: none;">
            <div class="layerwidget" style="display: block; width: 700px;">
                <span class="tblLabels">Start Date:&nbsp;&nbsp;</span><input id='startDatePicker' data-role='datepicker' style="width: 125px" />&nbsp;&nbsp;<span class="tblLabels">End Date:&nbsp;&nbsp;</span><input id="endDatePicker" data-role='datepicker' style="width: 125px" />&nbsp;
                <input id="searchTransactions" type="submit" data-bind="click: searchTransactions" value="Search" class="k-button" />&nbsp;
                <input id="exportToExcel" type="submit" data-bind="click: exportExcel" value="Export To Excel" class="k-button" />
            </div>

            <div class="layerwidget" style="display: block; width: 700px;">
                <div id="transactionGrid" data-bind="source: transactionsDataSource"></div>
            </div>
        </div>

        <div id="prDetails" style="display: none;">
            <div class="layerwidget" style="display: block; width: 770px;">
                <span class="tblLabels">Less than&nbsp;</span>
                <input id='threshValue' data-role='number' style="width: 80px" />&nbsp;
				<input id="ddlIncrementType" style="width: 150px" class="k-dropdown" />
                <span class="tblLabels">until next payment period charge.&nbsp;&nbsp;</span>
                <input id="searchProjections" type="submit" value="Search" class="k-button" data-bind="click: searchProjections" />
                <input id="exportToExcelProjections" type="submit" data-bind="click: saveGridCsvProjections" value="Export To Excel" class="k-button" />
            </div>
            <div class="layerwidget" style="display: block; width: 770px;">
                <div id="projectionGrid" data-bind="source: projectionDataSource"></div>
            </div>
        </div>
        <%--     </telerik:RadPane>
    </telerik:RadSplitter>--%>
    </div>
    <script>
        $(document).ready(function () {
            var manager = new Dash1.DashBoard1();
            manager.initialize();

        });

    </script>





</asp:Content>
