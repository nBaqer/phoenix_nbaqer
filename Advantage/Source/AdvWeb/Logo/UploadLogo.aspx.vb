
Imports Fame.AdvantageV1.BusinessFacade
Imports System.IO
Imports Fame.Advantage.Api.Library.SystemCatalog
Imports Fame.Advantage.Api.Library.Models

Namespace AdvWeb.Logo

    Partial Class LogoUploadLogo
        Inherits BasePage
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
            If Not IsPostBack Then
                Dim facade As New SchoolLogoFacade
                If facade.ConnectionString = "" Then
                    pnlWarning.Visible = True
                    pnlFileInput.Visible = False
                Else
                    pnlWarning.Visible = False
                    pnlFileInput.Visible = True
                End If
            End If
            headerTitle.Text = Header.Title
        End Sub

        Private Sub Submit1_ServerClick(ByVal sender As Object, ByVal e As EventArgs) Handles Submit1.ServerClick
            If File1.PostedFile.FileName.Length > 0 Then

                'reset and hide the message label
                lblMessage.Text = "Result: "
                lblMessage.Visible = False
                'btnPreviewImage.Visible = False
                PictureBox1.Visible = False

                'get size of selected file
                Dim len As Integer = File1.PostedFile.ContentLength

                Dim imageStr(len) As Byte

                Dim image As HttpPostedFile = File1.PostedFile

                'read image into array of bytes
                image.InputStream.Read(imageStr, 0, File1.PostedFile.ContentLength)

                'Get FileName
                Dim strImgFile As String = Path.GetFileNameWithoutExtension(File1.PostedFile.FileName)
                'insert image in DB
                Dim result As String = (New SchoolLogoFacade).InsertImage(imageStr, File1.PostedFile.ContentLength, strImgFile, chkIsOfficial.Checked, tbCode.Text)

                'inform user
                If result = "" Then
                    lblMessage.Text &= "School logo was successfully uploaded."

                    'Show the image
                    Dim base64String As String = Convert.ToBase64String(imageStr, 0, imageStr.Length)
                    PictureBox1.ImageUrl = "data:image/png;base64," + base64String
                    PictureBox1.Visible = True

                    'btnPreviewImage.Visible = True
                Else
                    lblMessage.Text &= result
                    'btnPreviewImage.Visible = False 
                    PictureBox1.Visible = False
                End If
                lblMessage.Visible = True
                chkIsOfficial.Checked = False
            End If
            'End If

        End Sub

        '        Private Sub btnPreviewImage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreviewImage.Click





        'Session("SchoolLogo") = (New SchoolLogoFacade).GetImage()


        '            Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
        '            Dim name As String = "UploadLogo"
        '            Dim url As String = "../Logo/ReportViewer.aspx"

        '            '   open new window and pass parameters
        '            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

        '        End Sub


        ''' <summary>
        ''' Convert a byte array to an Image
        ''' </summary>
        ''' <param name="newImage">Image to be returned</param>
        ''' <param name="byteArr">Contains bytes to be converted</param>
        ''' <remarks></remarks>
        Public Sub Byte2Image(ByRef newImage As Drawing.Image, ByVal byteArr() As Byte)
            '
            Dim imageStream As MemoryStream

            Try
                If byteArr.GetUpperBound(0) > 0 Then
                    imageStream = New MemoryStream(byteArr)
                    newImage = Drawing.Image.FromStream(imageStream)

                Else
                    newImage = Nothing
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                newImage = Nothing
            End Try

        End Sub



    End Class
End Namespace