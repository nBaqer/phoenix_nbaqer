<%@ Page Language="VB" AutoEventWireup="false"  MasterPageFile="~/NewSite.master"  CodeFile="UploadLogo.aspx.vb" Inherits="AdvWeb.Logo.LogoUploadLogo" %>
<%@ mastertype virtualpath="~/NewSite.master"%> 
<asp:content id="content1" contentplaceholderid="additional_head" runat="server">
    <script src="../js/checkall.js" type="text/javascript"></script>
    <script type="text/javascript">
        function OldPageResized(sender) {
            window.$telerik.repaintChildren(sender);
        }
    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <asp:ScriptManagerProxy ID="ScriptManagerProxy2" runat="server">
            </asp:ScriptManagerProxy>
            <p></p>
            <table id="table1" style="width: 98%; border: 0;">
                <tr>
                    <td>
                        <asp:Panel ID="pnlWarning" runat="server">
                            <asp:Label ID="lblWarning" Font-Bold="True" ForeColor="#ff0000" runat="server">Warning! The connection string is missing.</asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="boxContainer">
                            <asp:Panel ID="pnlFileInput" runat="server" Width="100%">
                                <h3>
                                    <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                </h3>
                                <table style="width: 98%; border: 0">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblInfo" Font-Bold="True" runat="server">Upload School Logo (Image logo must be a .jgp or .bmp file)</asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%">
                                            <input id="File1" style="width: 100%" type="file" size="106" name="File1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkIsOfficial" runat="server" Text="Is Official Logo" Checked="False"></asp:CheckBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Enter Code for Logo identification:
                                        <asp:TextBox class="textbox" ID="tbCode" runat="server"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input id="Submit1" type="submit" value="Upload Logo" name="Submit1" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblMessage" Font-Bold="True" runat="server" Visible="False">Results: </asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Image ID="PictureBox1" runat="server" Visible="False" AlternateText="Preview Logo"></asp:Image>
                                            <%--<asp:LinkButton id="btnPreviewImage" runat="server" Visible="False" Text="Preview Logo"></asp:LinkButton>--%>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>


                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>


</asp:Content>

