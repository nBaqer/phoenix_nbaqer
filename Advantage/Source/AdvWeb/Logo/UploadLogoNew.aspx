<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NewSite.master" CodeFile="UploadLogoNew.aspx.vb" Inherits="Logo_UploadLogo" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">

    <script src="../js/checkall.js" type="text/javascript"></script>
    <script type="text/javascript">
        function OldPageResized(sender) {
            window.$telerik.repaintChildren(sender);
        }
    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <style scoped="scoped">
        .config-section a {
            color: #e15613;
            text-decoration: none;
        }

        .configHead {
            display: block;
            font-size: 13px;
            font-weight: bold;
            text-indent: 0;
            margin-top: 10px;
            margin-bottom: 5px;
        }
    </style>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="98%" Orientation="HorizontalTop">
            <asp:ScriptManagerProxy ID="ScriptManagerProxy2" runat="server">
                <Scripts>
                    <asp:ScriptReference Path="../Scripts/Advantage.Client.Logo.js"></asp:ScriptReference>
                </Scripts>
            </asp:ScriptManagerProxy>
            <div id="LogoViewModelRegion">
                <script id="UploadLogo_grid_rowTemplate" type="text/x-kendo-tmpl">
	        <tr data-uid="#= uid #">
                <td style="visibility: hidden; width:1">#: id #  </td>
                <td>    
                    <a class="k-button k-button-icontext k-grid-edit"  href="\#"><span class="k-icon k-edit"></span>Edit</a></td> 
                 <td>   
                     <a class="k-button k-button-icontext k-grid-delete"  href="\#"><span class="k-icon k-delete"></span>Delete</a>
                  </td> 
 		        <td class="photo">
                    <%--  Hola <img src="../../content/web/Employees/#:data.EmployeeID#.jpg" alt="#: data.EmployeeID #" />--%>
                       <img src="data:image/jpg;base64, #: ImagenBytes #" alt="#: ImageCode#" height=80px />
		        </td>
		        <td class="details">
			           <span class="description"><b>Image Code : </b>#: ImageCode#</span>
			           <span class="description"><b>File Name  : </b>#: ImageFile# </span>
			           <span class="description"><b>File Length: </b> #: ImgLenth# &nbsp; &nbsp; &nbsp; <b>Official Use:</b> #: OfficialUse# </span>
		         </td>
		         <td class="description1"> #: Description# </td>
	           </tr>
                </script>
                <script id="UploadLogo_grid_altrowTemplate" type="text/x-kendo-tmpl">
	            <tr class="k-alt"  data-uid="#= uid #">
                    <td style="visibility: hidden; width:1">#: id #  </td>
                      <td>    
                    <a class="k-button k-button-icontext k-grid-edit"  href="\#"><span class="k-icon k-edit"></span>Edit</a></td> 
                 <td>   
                     <a class="k-button k-button-icontext k-grid-delete"  href="\#"><span class="k-icon k-delete"></span>Delete</a>
                  </td> 
		            <td class="photo">
                          <img src="data:image/jpg;base64, #: ImagenBytes #" alt="#: ImageCode#" height=80 "/>
		            </td>
		            <td class="details">
			           <span class="description"><b>Image Code : </b>#: ImageCode#</span>
			           <span class="description"><b>File Name  : </b>#: ImageFile# </span>
			           <span class="description"><b>File Length: </b> #: ImgLenth#  &nbsp; &nbsp; &nbsp; <b>Official Use:</b> #: OfficialUse# </span>
		            </td>
		            <td class="description1">#: Description# </td>
	           </tr>
                </script>
                <div style="align-content: center; text-align: center; width: 100%">
                    <p style="font-weight: bold">Loaded Images</p>
                </div>
                <div style="align-content: center; text-align: center; width: 90%">
                    <p></p>
                    <!-- Table with images Section -->
                    
                    <div id="example" style="align-content: center; width: 100%; margin: 0 80px 0 80px">
                        <table id="UploadLogo_grid">
                            <colgroup>
                                <col style="visibility: hidden; width: 1px" />
                                <col class="button_Column" />
                                <col class="button_Column" />
                                <col class="photo" />
                                <col class="details" />
                                <col />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th id="th1" style="visibility: hidden; width: 1px"></th>
                                    <th id="th2">Edit</th>
                                    <th id="th3">Delete</th>
                                    <th id="th4">Picture</th>
                                    <th id="th5">Details</th>
                                    <th id="th6">Observations</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="6"></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                 <!-- Windows Control to Edit Image -->
                <div id="EditImageWindows">
                    <table>
                         <tr>
                             <td rowspan="3" style="vertical-align: top;">
                                 <img id="wImage" src="data:image/jpg;base64, #: ImagenBytes #" alt="#: ImageCode#" width="80" style="max-height: 80px"/>
                             </td>
                             <td style="vertical-align: top; width:150px; text-align: right"><b>Image Code:</b></td>
                             <td style="vertical-align: top;">
                                 <input id="tbImageCode" class="k-textbox" maxlength="13"/>
                             </td>
                         </tr>
                         <tr>
                             
                             <td style="vertical-align: top; width:150px; text-align: right"><b>File Name:</b></td>
                             <td style="vertical-align: top;" >
                                 <span id="labelFileName"></span>
                             </td>
                         </tr>
                          <tr>
                             
                             <td style="vertical-align: top; width:150px; text-align: right"><b>Official Logo:</b></td>
                             <td style="vertical-align: top;" >
                                 <input id="cbOfficialLogo" type="checkbox" />
                             </td>
                         </tr>
                         <tr >
                             <td style="vertical-align: top"><b>Observations:</b></td>
                             <td colspan="2"><textarea id="tbObservations"  rows="3" maxlength="300" placeholder="Up to 300 characters" style="width: 100%"></textarea></td>
                             
                         </tr>
                        <tr style="padding-top: 10px">
                            <td style="padding-top: 10px"></td><td style="padding-top: 10px"></td>
                            <td style="text-align: right; padding-top: 10px ">
                                 <button type="button" id="btnEditSave" style="width: 80px" >Save</button>
                            </td>
                        </tr>
                    </table>
                
                </div>
                <!-- Windows Control to Delete Image -->
                <div id="DeleteImageWindows">
                    <table>
                         <tr>
                             <td rowspan="2" style="vertical-align: top; width:100px">
                                 <img id="deleteImage" src="data:image/jpg;base64, #: ImagenBytes #" alt="#: ImageCode#" width="80" style="max-height: 80px"/>
                             </td>
                             <td style="vertical-align: top; text-align: center; width:100%"><b>Are you sure to delete image</b></td>
                         </tr>
                         <tr>
                             
                            
                             <td style="vertical-align: top; text-align: center" >
                                 <span id="deleteFileName"></span>?
                             </td>
                         </tr>
                      <tr>
                          <td style="height:50px; width: 100px"></td>
                            <td style="height:50px"></td>
                      </tr>
                        <tr style="padding-top: 10px">
                            <td style="width:100px" ></td>
                           <td style="text-align: right; padding-top: 10px ">
                                 <button type="button" id="btnDeleteImage" style="width: 80px" >Delete</button>
                            </td>
                        </tr>
                    </table>
                
                </div>
              
                <!-- Windows Control to Upload Image Async -->
                <div id="UploadWindows">
                      <p>Use select files to get your images. The images upload automatically.</p>
                    <div style="width: 95%">
                        <div class="demo-section k-header">
                            <input name="files" id="files" type="file" />
                        </div>
                    </div>
                </div>
                <p></p>
                <!-- Insert Button Section -->
                <div style="width: 98%; margin: auto; text-align: center">
                    <button type="button" id="UploadLogo_btnInsert" style="margin: 0 auto; align-self: center">Insert Images </button>
                </div>

                <!-- Error Message -->
                <div id="ajaxWindowError"></div>

            </div>
        </telerik:RadPane>

    </telerik:RadSplitter>

    <style scoped>
         .k-button .k-image {
                    height: 16px;
                }
         .button_Column {
             width: 90px;
         }

        .photo {
            width: 170px;
        }

        .details {
            width: 350px;
            text-align: left;
        }

        .description {
            display: block;
            padding-top: 5px;
        }

        .description1 {
            padding-top: 5px;
        }

        td.photo {
            text-align: center;
        }

        .k-grid-header .k-header {
            padding: 5px 10px;
        }

        .k-grid td {
            background: -moz-linear-gradient(top, rgba(0,0,0,0.05) 0, rgba(0,0,0,0.15) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0,rgba(0,0,0,0.05)), color-stop(100%,rgba(0,0,0,0.15)));
            background: -webkit-linear-gradient(top, rgba(0,0,0,0.05) 0,rgba(0,0,0,0.15) 100%);
            background: -o-linear-gradient(top, rgba(0,0,0,0.05) 0,rgba(0,0,0,0.15) 100%);
            background: -ms-linear-gradient(top, rgba(0,0,0,0.05) 0,rgba(0,0,0,0.15) 100%);
            background: linear-gradient(to bottom, rgba(0,0,0,0.05) 0,rgba(0,0,0,0.15) 100%);
            padding: 5px;
        }

        .k-grid .k-alt td {
            background: -moz-linear-gradient(top, rgba(0,0,0,0.2) 0, rgba(0,0,0,0.1) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0,rgba(0,0,0,0.2)), color-stop(100%,rgba(0,0,0,0.1)));
            background: -webkit-linear-gradient(top, rgba(0,0,0,0.2) 0,rgba(0,0,0,0.1) 100%);
            background: -o-linear-gradient(top, rgba(0,0,0,0.2) 0,rgba(0,0,0,0.1) 100%);
            background: -ms-linear-gradient(top, rgba(0,0,0,0.2) 0,rgba(0,0,0,0.1) 100%);
            background: linear-gradient(to bottom, rgba(0,0,0,0.2) 0,rgba(0,0,0,0.1) 100%);
        }

        .k-grid-content {
            max-height: 500px;
        }
    </style>

</asp:Content>

