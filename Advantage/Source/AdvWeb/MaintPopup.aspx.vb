' ===============================================================================
' MaintPopup
' Generic webform used to display a popup maintenance screen with 2 panes.
' The LHS has a datalist and the RHS is a form that is an ASCX file implementing
' IMaintFormBase.
' Parameters to the page include:
'   cmpid: CampusId
'   resid: ResourceID
'   pid:   ParentId - used to populate the lhs
'   objid: object id used to populate the rhs
' ===============================================================================
' Copyright (C) 2006 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================

Imports System.Data
Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.Common

Partial Class MaintPopup
    Inherits BasePage
    Private formRHS As System.Web.UI.UserControl
    Private userPermissionInfo As New FAME.AdvantageV1.Common.UserPagePermissionInfo

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Init
        ' load the RHS control and add it the the RHS panel
        Try
            Dim controlFileName As String = Request.Params("ascx")
            formRHS = Me.LoadControl(controlFileName)
            Me.pnlRhs.Controls.Add(formRHS)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim lbl As Label = New Label()
            lbl.Text = "<div>Error loading ascx file.</div><div>" + ex.Message + "</div>"
            lbl.ID = "lblError"
            Me.pnlRhs.Controls.Add(lbl)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            userPermissionInfo = LoadUserPagePermissions()
            ' update the IE title and the header that goes at the top
            ' Do this regardless of it is is a postback
            If GetIMaintFormBase() IsNot Nothing Then
                Me.Title = GetIMaintFormBase().Title
            End If

            ' stop the page from showing if the user does not have display permissions
            If userPermissionInfo.HasNone = True And Request.Params("source") <> "studentsummary" Then
                Dim js As String = "<script language='javascript'>alert('You do not have permission to view this page.'); window.close();</script>"
                Me.ClientScript.RegisterStartupScript(Me.Page.GetType(), "onstartup", js)
                Return
            End If

            If Not Page.IsPostBack Then
                If GetIMaintFormBase() IsNot Nothing Then
                    GetIMaintFormBase().ParentId = Request.Params("pid")
                    GetIMaintFormBase().ObjId = Request.Params("objid")
                    lblHeader.Text = Me.Title

                    ' add javascript to the delete button
                    btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

                    InitButtonsForLoad()
                    'remove the Status Bar in LHS for Setup Grade book Weightings Page from course
                    BindLHS()
                    If (Request.Params("resid") = "1") Then
                        tblStatus.Visible = False
                    End If

                    ResetForm()
                    If Request.Params("objid") IsNot Nothing AndAlso Request.Params("objid") <> "" Then
                        BindRHS(Request.Params("objid"))
                    End If
                End If
            End If
            'this happens when schedule popup is open from Attendance Summary Page (AR/StudentSummary.aspx)
            If Request.Params("perm") = "readonly" Then
                btnSave.Enabled = False
                btnNew.Enabled = False
                btnDelete.Enabled = False
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub


    Protected Sub BindLHS()
        Try
            Dim Id As String = GetIMaintFormBase().ParentId
            Dim ds As DataSet = Nothing
            If radStatus.SelectedIndex = 0 Then
                ds = Me.GetIMaintFormBase().GetLHSDataSet(Id, True, False)
            ElseIf radStatus.SelectedIndex = 1 Then
                ds = Me.GetIMaintFormBase().GetLHSDataSet(Id, False, True)
            Else
                ds = Me.GetIMaintFormBase().GetLHSDataSet(Id, True, False)
            End If
            Me.dlLHS.DataSource = ds
            Me.dlLHS.DataBind()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub
    Protected Sub BindRHS(ByVal id As String)
        Try
            ' get the selected userid and store it in the viewstate object
            GetIMaintFormBase().ObjId = id
            ' do some error checking
            If GetIMaintFormBase().ObjId Is Nothing Or GetIMaintFormBase().ObjId = "" Then
                Alert("There was a problem displaying the details for this record.")
                Return
            End If
            ' bind the RHS form
            Dim res As String
            If Request.Params("perm") = "readonly" Then
                res = GetIMaintFormBase().BindForm(GetIMaintFormBase().ObjId, , "readonly")
            Else
                res = GetIMaintFormBase().BindForm(GetIMaintFormBase().ObjId)
            End If
            If res <> "" Then
                Alert(res)
            End If
            ' Advantage specific stuff
            CommonWebUtilities.SetStyleToSelectedItem(dlLHS, GetIMaintFormBase().ObjId, ViewState)
            InitButtonsForEdit()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Protected Sub ResetForm()
        GetIMaintFormBase().Handle_New()
    End Sub

#Region "Advantage Permissions"
    Private Function LoadUserPagePermissions() As FAME.AdvantageV1.Common.UserPagePermissionInfo
        ' load the user permissions
        Dim resourceId As String = ""
        Dim campusId As String = ""
        Dim userId As String = ""

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        Try
            If HttpContext.Current.Request.Params("resid") IsNot Nothing Then
                resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        Try
            If HttpContext.Current.Request.Params("cmpid") IsNot Nothing Then
                campusId = AdvantageSession.UserState.CampusId.ToString
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        Try
            If HttpContext.Current.Session("UserId") IsNot Nothing Then
                userId = AdvantageSession.UserState.UserId.ToString
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        Try

            Dim up As UserPagePermissionInfo = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
            Return up
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return Nothing
        End Try
    End Function

    Private Sub InitButtonsForLoad()
        ' if userPermissionInfo is nothing, assume this is a superuser
        If userPermissionInfo Is Nothing Then
            btnSave.Enabled = True
            btnNew.Enabled = True
            btnDelete.Enabled = False
            Return
        End If

        If userPermissionInfo.HasFull Or userPermissionInfo.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If userPermissionInfo.HasFull Or userPermissionInfo.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        ' if userPermissionInfo is nothing, assume this is a superuser
        If userPermissionInfo Is Nothing Then
            btnSave.Enabled = True
            btnDelete.Enabled = True
            btnNew.Enabled = True
            Return
        End If

        If userPermissionInfo.HasFull Or userPermissionInfo.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If userPermissionInfo.HasFull Or userPermissionInfo.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If userPermissionInfo.HasFull Or userPermissionInfo.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
#End Region

#Region "Helpers"
    Protected Function GetIMaintFormBase() As IMaintFormBase
        Return CType(formRHS, IMaintFormBase)
    End Function

    Public Sub Alert(ByVal msg As String)
        msg = msg.Replace("'", "")
        msg = msg.Replace(vbCrLf, "\n")
        ClientScript.RegisterStartupScript(Page.GetType(), "sendJS", "<script language='javascript'>window.alert('" & msg.Replace("'", "") & "')</script>")
    End Sub
#End Region

#Region "Event Handlers"
    ''' <summary>
    ''' Called when the user click "Active", "Inactive" or "All"
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        BindLHS()
    End Sub

    ''' <summary>
    ''' Called for every item in the LHS datalist.
    ''' Gives us a chance to add a tooltip to the items,
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlLHS_ItemDataBound(ByVal sender As System.Object, ByVal e As DataListItemEventArgs) Handles dlLHS.ItemDataBound
        If e.Item.FindControl("lbLHSItem") IsNot Nothing Then
            Dim lb As LinkButton = e.Item.FindControl("lbLHSItem")
            Dim sbToolTip As New StringBuilder

            Try
                sbToolTip.AppendFormat("Code: {0}{1}", e.Item.DataItem("Code"), vbCrLf)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            Try
                sbToolTip.AppendFormat("Modified: {0}{1}", e.Item.DataItem("ModDate"), vbCrLf)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            Try
                sbToolTip.AppendFormat("By: {0}{1}", e.Item.DataItem("ModUser"), "")
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            lb.ToolTip = sbToolTip.ToString
        End If
    End Sub
    ''' <summary>
    ''' Called when the user click on an item in the LHS datalist.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub dlLHS_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlLHS.ItemCommand
        ' get the selected userid and store it in the viewstate object
        GetIMaintFormBase().ObjId = e.CommandArgument.ToString
        ViewState("objid") = GetIMaintFormBase.ObjId
        dlLHS.SelectedIndex = e.Item.ItemIndex

        ' do some error checking
        If GetIMaintFormBase().ObjId Is Nothing Or GetIMaintFormBase().ObjId = "" Then
            Alert("There was a problem displaying the details for this record.")
            Return
        End If

        ' bind the RHS form
        Dim res As String = GetIMaintFormBase().BindForm(GetIMaintFormBase().ObjId)
        If res <> "" Then
            Alert(res)
        End If

        ' Advantage specific stuff
        CommonWebUtilities.SetStyleToSelectedItem(dlLHS, GetIMaintFormBase().ObjId, ViewState)
        InitButtonsForEdit()

        'this happens when schedule popup is open from Attendance Summary Page (AR/StudentSummary.aspx)
        If Request.Params("perm") = "readonly" Then
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Called when the user clicks the "Save" button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim res As String = Me.GetIMaintFormBase().Handle_Save()
            If res = "" Then
                BindLHS()
                Alert("Record was saved successfully")
                CommonWebUtilities.SetStyleToSelectedItem(dllhs, Me.GetIMaintFormBase().ObjId, ViewState)
                InitButtonsForEdit()
            ElseIf res.ToLower.Contains("notice") Then
                BindLHS()
                Alert("Record was saved successfully " & vbCrLf & vbCrLf & res)
                CommonWebUtilities.SetStyleToSelectedItem(dllhs, Me.GetIMaintFormBase().ObjId, ViewState)
                InitButtonsForEdit()
            Else
                Alert(res)
                BindLHS()
                CommonWebUtilities.SetStyleToSelectedItem(dllhs, Me.GetIMaintFormBase().ObjId, ViewState)
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Alert(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Called when the user clicks the "New" button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ResetForm()
        CommonWebUtilities.SetStyleToSelectedItem(dlLHS, Guid.Empty.ToString, ViewState)
        InitButtonsForLoad()
    End Sub

    ''' <summary>
    ''' Called when the user clicks the "Delete" button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim curid As String = GetIMaintFormBase.ObjId
            If curid IsNot Nothing AndAlso curid <> "" Then
                Dim res As String = GetIMaintFormBase.Handle_Delete()
                If res <> "" Then
                    Alert(res)
                Else
                    ResetForm()
                    BindLHS()
                    CommonWebUtilities.SetStyleToSelectedItem(dlLHS, Guid.Empty.ToString, ViewState)
                    InitButtonsForLoad()
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Alert("Error.  Record could not be deleted.")
        End Try
    End Sub
#End Region

End Class
