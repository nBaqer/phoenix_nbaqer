<%@ Page Language="VB" %>
<%@ Register TagName="SysHeader" TagPrefix="Header" Src="~/usercontrols/TMHeader.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        lblUserName.Text = Session("username")
        lblDate.Text = Date.Now.ToString()
    End Sub
</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Welcome to Advantage Messaging and Task Manager</title>
    <link href="css/tm.css" rel="stylesheet" type="text/css" />    
</head>
<body>
    <form id="form2" runat="server">
        <div class="MenuFramediv">&nbsp;</div>
        <div class="AlignedDiv">
            <table width="100%" class="tableborder" cellpadding="0" cellspacing="0">
                <tr>
                    <th colspan="2">
                        <h1>
                            Choose what you want to do:</h1>
                    </th>
                </tr>
                <tr>
                    <td class="LabelBig">
                        <ul>
                            <li><a class="message" href="MSG/AddMessage.aspx">Write official letters to leads, students or employers</a></li>
                            <li><a class="message" href="MSG/ViewMessages.aspx?source=0">View and process messages waiting in the
                                Outbox</a></li>
                            <li><a class="message" href="TM/ViewUserTasks.aspx">Create and view tasks relating to leads, students
                                or employers</a></li>
                            <li><a class="message" href="TM/ViewCalendar_Day.aspx">View my calendar</a></li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
<div><asp:Label ID="lblUserName" runat="server" />
<asp:Label ID="lblDate" runat="server" /></div>
</html>
