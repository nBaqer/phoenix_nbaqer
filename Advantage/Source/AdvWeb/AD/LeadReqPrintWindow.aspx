﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LeadReqPrintWindow.aspx.vb" Inherits="AD_LeadReqPrintWindow" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" moznomarginboxes mozdisallowselectionprint>
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" media="screen" />
    <script src="../Kendo/js/jquery.min.js"></script>
    <script src="../Scripts/common-util.js"></script>
    <script src="../Scripts/Storage/storageCache.js"></script>
    <style type="text/css">
        @page {margin:0 -6cm}
        @media print {
            html {margin:0 6cm}
            tr.vendorListHeading {
                background-color: lightgrey;
                /*color: #000066;*/
                font-family: arial;
                font-weight: bold !important;
                -webkit-print-color-adjust: exact;
            }

            h2.vendorListHeading {
                /*background-color: lightgrey;*/
                /*color: #000066;*/
                font-weight: bold !important;
                text-align: center;
                -webkit-print-color-adjust: exact;
            }
        }
    </style>
</head>
<body>
   
    <div style="margin: 30px;">
        <table style="width: 100%;">
            <tr>
                <td style="text-align: center; font-family: arial; font-size: 10pt"><span id="schoolName" style="font-weight: bold;">school name</span></td>
            </tr>
            <tr>
                <td style="text-align: center; font-family: arial; font-size: 10pt"><span id="address" >address</span></td>
            </tr>
            <tr>
                <td style="text-align:center; font-family: arial; font-size: 10pt"><span id="citystatezip" >citystatezip</span></td>
            </tr>
        </table>
        </div>
    <h2 style="width: 98%; border: 0px; font-family: arial; font-size: 10pt; color: black; background-color: white;" class="vendorListHeading">RECEIPT</h2>
    <br />
    <div style="margin: 10px;">
        
        <table style="width: 100%; ">
            <tr>
                <td style="font-weight: bold;font-family: arial;  font-size: 9pt;">Received from:</td>
            </tr>

            <tr>
                <td style="font-family: arial; font-size: 9pt;"><span id="leadName" >Lead Name</span></td>
            </tr>
            <tr>
                <td style="font-family: arial; font-size: 9pt;"><span id="leadAddress" >Lead Address</span></td>
            </tr>
            <tr>
                <td style="font-family: arial; font-size: 9pt; "><span id="leadAddress2" >Lead City, State, Zip and Country</span></td>
            </tr>
        </table>
        <br />
    </div>
    <div style="margin: 10px;padding: 10px; border: black solid 1px;">
        <table id="receiptTable" style="width: 100%; border-collapse: collapse ">
            <tr style="background-color: lightgrey; font-family: arial; font-weight: bold;" class="vendorListHeading">
                <td style="font-size: 9pt;text-align: center">Date</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr style="padding-top: 5px;">
                <td style="padding-bottom: 12px;font-size: 9pt; font-family: arial;text-align: center"><span id="tranDate"></span></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr style="background-color: lightgrey; font-family: arial; font-weight: bold;  font-size: 9pt;" class="vendorListHeading">
                <td style="text-align: center">Transaction ID</td>
                <td style="text-align: center">Transaction Code</td>
                <td style="text-align: center">Payment Type</td>
                <td style="text-align: center">Document ID</td>
                <td style="text-align: center">Amount Received</td>
            </tr>
        </table>

    </div>
    <div style="text-align: center">
        <button onclick="printReceipt()" id="btnPrintReceipt" style="width: 150px;">Print Receipt</button>
    </div>
    <script type="text/javascript">
        $(function () {

            var transactionId = $.QueryString["transaction"];
            var campusId = $.QueryString["campus"];
            var lead = $.QueryString["lead"];

            var url2 = "../proxy/api/Leads/Requirements/GetLeadTransactionReceipt";
            var filter = { CampusId: campusId, TransactionId: transactionId, leadId: lead };
            var requestData2 = $.getJSON(url2, filter, function (data) {
                if (data !== undefined && data !== null) {
                    $("#schoolName").text(data.SchoolName);
                    $("#address").text(data.Address);
                    $("#citystatezip").text(data.CityStateZip);
                    $("#leadName").text(data.LeadName);
                    $("#leadAddress").text(data.LeadAddress);
                    $("#leadAddress2").text(data.LeadAddress2);
                    $("#tranDate").text(data.TransDate);
                    $(data.Transactions).each(function (index, element) {
                        if (element !== undefined && element !== null) {
                            var row = "<tr style = 'font-family: arial;font-size: 9pt '>" +
                                "<td style = 'text-align: center;'><span id='transactionId'>" +
                                element.TransactionId +
                                "</span></td>" +
                                "<td style = 'text-align: center;'><span id='transactionCode'>" +
                                element.TransactionCode +
                                "</span></td>";

                            row += "<td style = 'text-align: center;'><span id='paymentType'>" + element.PaymentType + "</span></td>";

                            row += "<td style = 'text-align: center;'><span id='documentId'>" +
                                element.DocumentId +
                                "</span></td>";
                            if (element.Voided === true && index === 1) {
                                row += "<td style = 'text-align: center;'><span id='amountReceived'>" + "(" + formatCurrency(element.AmountReceived) + ")" + "</span></td>";
                            } else {
                                row += "<td style = 'text-align: center;'><span id='amountReceived'>&nbsp;" + formatCurrency(element.AmountReceived) + "</span></td>";
                            }
                            row += "<td></td>" +
                                "</tr>";
                            $("#receiptTable").append(row);
                        }
                    });
                }
            });

            function formatCurrency(total) {
                var neg = false;
                if (total < 0) {
                    neg = true;
                    total = Math.abs(total);
                }
                return (neg ? "-$" : '$') + parseFloat(total, 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
            }
        });
        function printReceipt() {
            $("#btnPrintReceipt").hide();
            window.print();
            $("#btnPrintReceipt").show();
        }
    </script>
</body>
</html>
