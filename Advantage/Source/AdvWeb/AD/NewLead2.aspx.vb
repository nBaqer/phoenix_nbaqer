﻿Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common.TM
Imports FAME.AdvantageV1.BusinessFacade.TM

Partial Class NewLead2
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtEmployerContactId As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEmployerId As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblTitleId As System.Web.UI.WebControls.Label
    Protected WithEvents ddlTitleId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblWorkPhone As System.Web.UI.WebControls.Label
    Protected WithEvents txtWorkPhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblHomePhone As System.Web.UI.WebControls.Label
    Protected WithEvents lblCellPhone As System.Web.UI.WebControls.Label
    Protected WithEvents txtCellPhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEmails As System.Web.UI.WebControls.Label
    Protected WithEvents lblBeeper As System.Web.UI.WebControls.Label
    Protected WithEvents txtBeeper As System.Web.UI.WebControls.TextBox


    ' Protected EmployerContactId As String = "03D26D83-7814-4BCF-AB80-0F429C691D9B"
    Protected WithEvents ChkStatus As System.Web.UI.WebControls.CheckBox
    Protected WithEvents dlstEmployerContact As System.Web.UI.WebControls.DataList

    Protected WithEvents RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator3 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator4 As System.Web.UI.WebControls.RegularExpressionValidator

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblState As System.Web.UI.WebControls.Label
    Protected WithEvents txtState As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblExt As System.Web.UI.WebControls.Label
    Protected WithEvents txtExtension As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblBestTime As System.Web.UI.WebControls.Label
    Protected WithEvents txtBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblPinNumber As System.Web.UI.WebControls.Label
    Protected WithEvents txtPinNumber As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblWorkExt As System.Web.UI.WebControls.Label
    Protected WithEvents txtWorkExt As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblWorkBestTime As System.Web.UI.WebControls.Label
    Protected WithEvents txtWorkBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblHomeBestTime As System.Web.UI.WebControls.Label
    Protected WithEvents txtHomeBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCellBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRowIds As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtResourceId As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents Dropdownlist1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblAdmissionsRepID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlAdmissionsRepID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblPhoneStatusID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlPhoneStatusID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblNationalityID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlNationalityID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblDriverLicenseState As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCitizenID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlDriverLicenseNumber As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblCountyID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCountyID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlAddressStateID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtPhone1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblType As System.Web.UI.WebControls.Label
    Protected WithEvents ddlPhoneTypeID As System.Web.UI.WebControls.DropDownList
    Protected CampusId As String
    Protected WithEvents lbl2 As System.Web.UI.WebControls.Label
    Protected WithEvents chkForeign As System.Web.UI.WebControls.CheckBox
    Protected WithEvents valCompare As System.Web.UI.WebControls.CompareValidator
    Protected m_context As HttpContext
    Protected strSourceDetails As String
    Protected strMask As String
    Protected zipMask As String
    Dim ssnMAsk As String
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region




    'Dim newBox As Messagebox = New Messagebox()
    Private useSaveNext As Boolean = False
    Private pObj As New UserPagePermissionInfo
    Dim userId As String
    Protected ModuleId As String
    Protected ResourceID As Integer
    Public strScript1, strScript As String
    Protected strDefaultCountry As String
    Protected isIPEDSApplicable As String
    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim SDFControls As New SDFComponent
        Dim objCommon As New CommonUtilities
        Dim campusId2 As String
        '  Dim m_Context2 As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId2 As Integer
        Dim facInputMasks As New FAME.AdvantageV1.BusinessFacade.InputMasksFacade

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        ''''' Code Commented on 18th August to fix mantis issue id 19555
        ''CampusId = XmlConvert.ToGuid(m_context.Current.Request.Params("cmpid")).ToString
        '''''''

        ''''' Code Added on 18th August to fix mantis issue id 19555
        If Not ViewState("CampusID") = Nothing Then
            CampusId = ViewState("CampusID").ToString
        Else
            ViewState("CampusID") = Master.CurrentCampusId
           campusid = Master.CurrentCampusId 
        End If
        ''''''''''

        resourceId2 = CInt(HttpContext.Current.Request.Params("resid"))
        ResourceID = CInt(HttpContext.Current.Request.Params("resid"))

        campusId2 = Master.CurrentCampusId

        ''''' Code Added on 18th August to fix mantis issue id 19555
        ddlCampusId.Attributes.Add("onchange", "verifycampuschange('" + ddlCampusId.ClientID + "','" + CampusId + "')")
        '''''''''''''''''

        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        'pObj = fac.GetUserResourcePermissions(userId, resourceId2, campusId2)

        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceID, CampusId)

        txtUserId.Text = userId
        ''''' Code Commented on 18th August to fix mantis issue id 19555
        ''txtCampusId.Text = campusId2
        ''''''''''''''''''''
        'btnCheckDuplicates.Attributes.Add("onclick", "CheckDuplicates1();return false;")

        txtDate.Text = Date.Now.ToShortDateString
        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString
        strMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.Zip)
        ssnMAsk = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.SSN)

        If Not Page.IsPostBack Then
            'ddlPrefix.Focus()
            chkIsInDB.Checked = False
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder))
            lblPhone.Text = "Phone 1"
            ViewState("MODE") = "NEW"

            ''''' Code Modified on 18th August to fix mantis issue id 19555
           campusid = Master.CurrentCampusId 
            BuildCampusDDL()
            '''''''''''''''''''''''
            BuildDropDownLists()

            ''''' Code Modified on 18th August to fix mantis issue id 19555
            Try
                ddlCampusId.SelectedValue = CampusId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)


            End Try

            txtCampusId.Text = CampusId
            ''''''''

            'bind an empty new LeadMasterInfo
            BindLeadMasterDataItemCommand(New LeadMasterInfo)

            txtAssignedDate.SelectedDate = FormatDateTime(Date.Now, DateFormat.ShortDate)
            txtDateApplied.SelectedDate = FormatDateTime(Date.Now, DateFormat.ShortDate)
            txtModDate.Text = Date.Today.ToString
            Session("ScrollValue") = 0
            Try
                'Code changes by Atul Kamble on 08-Oct-2010
                If AdvantageSession.UserState.IsUserSA Or AdvantageSession.UserState.IsUserSupport Or AdvantageSession.UserState.IsUserSuper Then
                    ddlAdmissionsRep.SelectedIndex = 0
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlAdmissionsRep.SelectedIndex = 0
            End Try

            Try
                ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText("Unknown"))
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlGender.SelectedIndex = 0
            End Try

            Try
                ddlAddressType.SelectedIndex = ddlAddressType.Items.IndexOf(ddlAddressType.Items.FindByText("Home"))
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlAddressType.SelectedIndex = 0
            End Try

            Try
                ddlAdmissionsRep.SelectedValue = userId
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlAdmissionsRep.SelectedIndex = 0
            End Try

            If pObj.HasFull Or pObj.HasAdd Then
                btnNew.Enabled = True
                btnSave.Enabled = True
                btnSaveNext.Enabled = True
            End If
        Else
            'objCommon.SetCaptionsAndColorRequiredFields(Form1, , chkForeignZip.Checked)
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), , chkForeignZip.Checked)
        End If

        GetInputMaskValue()

        'Check if School is using IPEDS
        'CodeConstructType commented by balaji on 04/15/2009 to fix issue 12013
        'IPEDSRequirements()

        'For DDLs to work
        'If Not IsPostBack Then
        '    BuildProgramGroupDDL()
        '    BuildSourceCatagoryDDL()
        '    Me.__hiddensubmit.Value = "yes"
        '    Session("loadfirsttime") = "no"
        'Else
        '    If Not ddlAreaId.Value = "" Then
        '        Session("AreaId") = ddlAreaId.Value
        '    End If
        '    Session("programid") = Me.__programid.Value
        '    Session("prgverid") = Me.__prgverid.Value

        '    Session("sourcecategoryid") = ddlSourceCategoryId.Value
        '    Session("sourcetypeid") = Me.__sourcetypeid.Value
        '    Session("sourceadvid") = Me.__sourceadvid.Value
        'End If
        ''save ddl value to hidden field 
        'Me.ddlProgramID.Attributes.Add("onchange", "setHiddenfieldValue('__programid',this.value);")
        'Me.ddlPrgVerId.Attributes.Add("onchange", "setHiddenfieldValue('__prgverid',this.value);")

        'Me.ddlSourceTypeId.Attributes.Add("onchange", "setHiddenfieldValue('__sourcetypeid',this.value);")
        'Me.ddlSourceAdvertisement.Attributes.Add("onchange", "setHiddenfieldValue('__sourceadvid',this.value);")

        If chkForeignZip.Checked = True Then
            txtOtherState.Visible = True
            lblOtherState.Visible = True
            ddlStateID.Enabled = False
            lblStateId.Visible = False
            ddlStateID.Visible = False
        Else
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            ddlStateID.Enabled = True
            lblStateId.Visible = True
            ddlStateID.Visible = True
        End If

        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            lblEntranceInterviewDate.Visible = True
            txtEntranceInterviewDate.Visible = True
            'Img7.Visible = True
            lblHighSchoolProgramCode.Visible = True
            txtHighSchoolProgramCode.Visible = True
            SetRegentRequiredFields()
        End If


        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = SDFControls.GetSDFExists(ResourceID, ModuleId)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtPKID.Text) <> "" Then
            SDFControls.GenerateControlsEdit(pnlSDF, ResourceID, txtPKID.Text, ModuleId)
        Else
            SDFControls.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)
        End If

        ''''' Code Commented on 18th August to fix mantis issue id 19555
        ''ddlCampusId.SelectedValue = campusId2
        '''''''''
        'Header1.EnableHistoryButton(False)



        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        'If dup.Value = "yes" And useSaveNext = False Then
        '    AddLead()
        '    Exit Sub
        'ElseIf dup.Value = "yes" And useSaveNext = True Then
        '    AddLead()
        '    Exit Sub
        'End If

        txtFirstName.BackColor = Color.White
        txtLastName.BackColor = Color.White
        ddlLeadStatus.BackColor = Color.White
        ddlCampusId.BackColor = Color.White
        ddlGender.BackColor = Color.White
        ddlAdmissionsRep.BackColor = Color.White
        ddlCountry.BackColor = Color.White
        ddlAddressType.BackColor = Color.White

        pnlMessage.Visible = False

    End Sub
    Private Sub SetRegentRequiredFields()
        'txtSSN.BackColor = Color.FromName("#ffff99")
        lblSSN.Text = lblSSN.Text & "<font color=""red"">*</font>"
        'txtBirthDate.BackColor = Color.FromName("#ffff99")
        lblBirthDate.Text = lblBirthDate.Text & "<font color=""red"">*</font>"
    End Sub
    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub GetInputMaskValue()

        'Dim correctFormat As Boolean

        'Dim errorMessage As String
        Dim strPhoneReq As String
        Dim strZipReq As String
        '  Dim strFaxReq As String
        Dim objCommon As New CommonUtilities


        txtSSN.Mask = Replace(ssnMAsk, "#", "9")
        lblSSN.ToolTip = ssnMAsk

        'Replace The Mask Character from # to 9 as Masked Edit TextBox 
        'accepts only certain characters as mask characters
        If chkForeignPhone.Checked = False Then
            txtPhone.Mask = Replace(strMask, "#", "9")
            lblPhone.ToolTip = strMask
        Else
            txtPhone.Mask = ""
            lblPhone.ToolTip = ""
        End If
        If chkForeignPhone2.Checked = False Then
            txtPhone2.Mask = Replace(strMask, "#", "9")
            lblPhone2.ToolTip = strMask
        Else
            txtPhone2.Mask = ""
            lblPhone2.ToolTip = ""
        End If
        If chkForeignZip.Checked = False Then
            txtZip.Mask = Replace(zipMask, "#", "9")
            lblZip.ToolTip = zipMask
        Else
            txtZip.Mask = ""
            lblZip.ToolTip = ""
        End If

        'Get The RequiredField Value
        Dim strSSNReq As String
        strPhoneReq = objCommon.SetRequiredColorMask("Phone")
        strZipReq = objCommon.SetRequiredColorMask("Zip")
        strSSNReq = objCommon.SetRequiredColorMask("SSN")
        
        Try
            
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

    Private Function ValidateFieldsWithInputMasks() As String
        Dim facInputMasks As New Fame.AdvantageV1.BusinessFacade.InputMasksFacade
        Dim correctFormat As Boolean
        Dim strMask As String
        Dim zipMask As String
        Dim errorMessage As String = String.Empty
        Dim ssnMask As String

        strMask = facInputMasks.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.SSN)
        'Validate the phone field format. If the field is empty we should not apply the mask
        'agaist it.

        If txtSSN.Text <> "" Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(ssnMask, txtSSN.Text)
            If correctFormat = False Then
                errorMessage = "Incorrect format for  ssn field." & vbCr
            End If
        End If

        If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtPhone.Text)
            If correctFormat = False Then
                errorMessage &= "Incorrect format for  phone field." & vbCr
            End If
        End If
        If txtPhone2.Text <> "" And chkForeignPhone2.Checked = False Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtPhone2.Text)
            If correctFormat = False Then
                errorMessage &= "Incorrect format for  phone field." & vbCr
            End If
        End If
        'Validate the zip field format. If the field is empty we should not apply the mask
        'against it.
        If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtZip.Text)
            If correctFormat = False Then
                errorMessage &= "Incorrect format for zip field."
            End If
        End If

        Return errorMessage
    End Function
    Private Sub BuildCampusDDL()
        'Bind the CampusGroups DrowDownList
        Dim campusGroups As New CampusGroupsFacade
        With ddlCampusId
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            ''Code Modified by Kamalesh Ahuja on August 26, 2010 Mantis Id 19559
            .DataSource = campusGroups.GetCampusesByUser(userId)
            '' '' Code Modified by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
            ''If SingletonAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "ownleads" Then
            ''    .DataSource = campusGroups.GetCampusesByUser(userId)
            ''ElseIf SingletonAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "permissiblecampuses" Then
            ''    .DataSource = campusGroups.GetCampusesByUser(userId)
            ''ElseIf SingletonAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "allcampuses" Then
            ''    .DataSource = campusGroups.GetAllCampuses()
            ''Else
            ''    .DataSource = campusGroups.GetAllCampusEnrollmentByCampus(CampusId)
            ''End If
            ''Code Modified by Kamalesh Ahuja on August 26, 2010 Mantis Id 19559
            '' '''''''''''''''''''''
            .DataBind()
        End With
    End Sub
    Private Sub BuildDropDownLists()

        'BuildPrefixDDL()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        'Prefixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlPrefix, AdvantageDropDownListName.Prefixes, CampusId, True, True, String.Empty))
        'Suffixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlSuffix, AdvantageDropDownListName.Suffixes, CampusId, True, True, String.Empty))
        'Country DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlCountry, AdvantageDropDownListName.Countries, CampusId, True, True, String.Empty))
        'Address Type
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressType, AdvantageDropDownListName.Address_Types, CampusId, True, True, String.Empty))
        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlDrivLicStateID, AdvantageDropDownListName.States, CampusId, True, True, String.Empty))
        'Races
        ddlList.Add(New AdvantageDDLDefinition(ddlRace, AdvantageDropDownListName.Races, CampusId, True, True, String.Empty))
        'Genders
        ddlList.Add(New AdvantageDDLDefinition(ddlGender, AdvantageDropDownListName.Genders, CampusId, True, True, String.Empty))
        'Marital Status
        ddlList.Add(New AdvantageDDLDefinition(ddlMaritalStatus, AdvantageDropDownListName.MaritalStatus, CampusId, True, True, String.Empty))
        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlStateID, AdvantageDropDownListName.States, CampusId, True, True, String.Empty))
        'Citizenships
        ddlList.Add(New AdvantageDDLDefinition(ddlCitizen, AdvantageDropDownListName.Citizen, CampusId, True, True, String.Empty))
        'Nationality
        ddlList.Add(New AdvantageDDLDefinition(ddlNationality, AdvantageDropDownListName.Nationality, CampusId, True, True, String.Empty))
        'Address States 
        'ddlList.Add(New AdvantageDDLDefinition(ddlAddrStateId, AdvantageDropDownListName.States, CampusId,True,True,String.Empty))
        'Shifts
        ddlList.Add(New AdvantageDDLDefinition(ddlShiftID, AdvantageDropDownListName.Shifts, CampusId, True, True, String.Empty))
        'County
        ddlList.Add(New AdvantageDDLDefinition(ddlCounty, AdvantageDropDownListName.Counties, CampusId, True, True, String.Empty))
        'Family Income
        ddlList.Add(New AdvantageDDLDefinition(ddlFamilyIncome, AdvantageDropDownListName.FamilyIncome, CampusId, True, True, String.Empty))
        'Previous Education
        ddlList.Add(New AdvantageDDLDefinition(ddlPreviousEducation, AdvantageDropDownListName.EducationLvl, CampusId, True, True, String.Empty))
        'Phone Types
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneType, AdvantageDropDownListName.Phone_Types, CampusId, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneType2, AdvantageDropDownListName.Phone_Types, CampusId, True, True, String.Empty))
        'Phone Statuses
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneStatus, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Address Status
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressStatus, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Admission Reps
        'ddlList.Add(New AdvantageDDLDefinition(ddlAdmissionsRep, AdvantageDropDownListName.AdmissionsRep, Nothing))

        'Dependency Type
        ddlList.Add(New AdvantageDDLDefinition(ddlDependencyTypeId, AdvantageDropDownListName.DependencyType, CampusId, True, True, String.Empty))

        'Geographic Type
        ddlList.Add(New AdvantageDDLDefinition(ddlGeographicTypeId, AdvantageDropDownListName.GeographicType, CampusId, True, True, String.Empty))

        'Sponsor
        ddlList.Add(New AdvantageDDLDefinition(ddlSponsor, AdvantageDropDownListName.Sponsors, CampusId, True, True, String.Empty))

        'Admin Criteria
        ddlList.Add(New AdvantageDDLDefinition(ddlAdminCriteriaId, AdvantageDropDownListName.AdminCriteriaType, CampusId, True, True, String.Empty))

        'Housing Type
        ddlList.Add(New AdvantageDDLDefinition(ddlHousingId, AdvantageDropDownListName.HousingType, CampusId, True, True, String.Empty))

        'Degree Certificate Seeking Type
        ddlList.Add(New AdvantageDDLDefinition(ddlDegCertSeekingId, AdvantageDropDownListName.DegCertSeekingType, CampusId, True, True, String.Empty))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

        BuildStatusDDL("")
        ''''' Code Modified on 18th August to fix mantis issue id 19555
        ''BuildCampusDDL()
        '''''''''''''
        BuildSourceCatagoryDDL()
        BuildProgramGroupDDL()
        BuildLeadGroupsDDL()
        BuildAdmissionRepsDDL()
    End Sub
    Private Sub BuildLeadGroupsDDL()
        Dim facade As New AdReqsFacade
        With chkLeadGrpId
            .DataTextField = "Descrip"
            .DataValueField = "LeadGrpId"
            .DataSource = facade.GetAllLeadGroups(CampusId)
            .DataBind()
        End With
    End Sub
    Private Function UpdateLeadGroups(ByVal LeadId As String) As Integer
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), chkLeadGrpId.Items.Count)
        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In chkLeadGrpId.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        If i <= 0 Then
            Return -1
        End If

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)

        'update Selected Jobs
        Dim leadGrpFacade As New AdReqsFacade
        If leadGrpFacade.UpdateLeadGroups(LeadId, AdvantageSession.UserState.UserName, selectedDegrees) < 0 Then
            'DisplayErrorMessage("A related record exists,you can not perform this operation")
            DisplayRADAlert(CallbackType.Postback, "Error1", "A related record exists,you can not perform this operation ", "Update Error")
        End If
        Return 0
    End Function
    Private Sub BuildPrgVersionDDL()
        ddlPrgVerId.Items.Clear()
        Dim facade As New LeadFacade
        With ddlPrgVerId
            .DataTextField = "PrgVerDescrip"
            '.DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = facade.GetAllProgramVersionByCampusAndUser(ddlProgramID.SelectedValue, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramGroupDDL()
        Dim PrgGrp As New LeadFacade
        With ddlAreaID
            .DataTextField = "PrgGrpDescrip"
            .DataValueField = "PrgGrpID"
            .DataSource = PrgGrp.GetAllProgramsByGroup()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlProgramID
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlPrgVerId
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceCatagoryDDL()
        Dim sourceCatagory As New LeadFacade
        With ddlSourceCategoryId
            .DataTextField = "SourceCatagoryDescrip"
            .DataValueField = "SourceCatagoryId"
            .DataSource = sourceCatagory.GetAllSourceCategoryByCampus(CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSourceTypeId
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSourceAdvertisement
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceTypeDDL(ByVal SourceCatagoryID As String)
        Dim SourceType As New LeadFacade
        ddlSourceTypeId.Items.Clear()
        With ddlSourceTypeId
            .DataTextField = "SourceTypeDescrip"
            .DataValueField = "SourceTypeId"
            .DataSource = SourceType.GetAllSourceTypeByCampus(SourceCatagoryID, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceAdvDDL(ByVal SourceTypeID As String)
        Dim SourceType As New LeadFacade
        ddlSourceAdvertisement.Items.Clear()
        With ddlSourceAdvertisement
            .DataTextField = "SourceAdvDescrip"
            .DataValueField = "SourceAdvId"
            .DataSource = SourceType.GetAllSourceAdvertisementByCampus(SourceTypeID, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramsDDL(ByVal ProgramGrpID As String)
        Dim programTypes As New LeadFacade
        ddlProgramID.Items.Clear()
        With ddlProgramID
            .DataTextField = "ProgDescrip"
            .DataValueField = "ProgId"
            .DataSource = programTypes.GetAllProgramsByCampusAndUser(ProgramGrpID, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildAdmissionRepsDDL()

        ''Dim LeadAdmissionReps As New LeadFacade
        ''With ddlAdmissionsRep
        ''    .DataTextField = "fullname"
        ''    .DataValueField = "userId"
        ''    .DataSource = LeadAdmissionReps.GetAllAdmissionRepsByCampusAndUserId(CampusId, userId)
        ''    .DataBind()
        ''    .Items.Insert(0, New ListItem("Select", ""))
        ''    .SelectedIndex = 0
        ''End With

        ''MOdified By Saraswathi Lakshmanan to fix issue 14385
        ''When entering a new lead information SA, Front Desk and Director
        '' of Admissions should be able to assign them to admission rep 
        ''Used the available function from AD/LeadMaster1.aspx

        Dim userName As String
        userName = AdvantageSession.UserState.UserName


        Dim leadAdmissionReps As New LeadFacade
        Dim directorAdmissionsorFrontDeskCheck As New UserSecurityFacade
        ''Find if the User is DirectorOfAdmission or FrontDesk

        Dim boolDirOrFDeskCheck As Boolean = directorAdmissionsorFrontDeskCheck.IsDirectorOfAdmissionsorFrontDesk(userId)
        With ddlAdmissionsRep
            .DataTextField = "fullname"
            .DataValueField = "userid"
            If boolDirOrFDeskCheck Then
                .DataSource = leadAdmissionReps.GetAllAdmissionRepsByCampusUserIdUserRoles(CampusId, userId, userName, boolDirOrFDeskCheck)
            Else
                .DataSource = leadAdmissionReps.GetAllAdmissionRepsByCampusAndUserId(CampusId, userId)
            End If
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Function ValidateLeadGroups() As Integer
        Dim strGroups As String = String.Empty
        ' Dim dsUseScheduled As DataSet
        'In For Loop Check The Number of Items Selected
        For Each item As ListItem In chkLeadGrpId.Items
            If item.Selected Then
                If strGroups = String.Empty Then
                    strGroups = item.Value.ToString
                Else
                    strGroups &= "," & item.Value.ToString
                End If

            End If
        Next

        If strGroups = String.Empty Then
            Return -1
        ElseIf strGroups.Split(",").Length = 1 Then
            Return 1
        Else
            Return (New StudentEnrollmentFacade).GetSchedLeadGrpCnt_SP(strGroups)
        End If
    End Function
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim leadMasterUpdate As New LeadFacade
        Dim Result As String
        'Call Update Function in the plEmployerInfoFacade
        Dim errorMessage As String
        Dim strAddressType As String = String.Empty
        Dim intAge As Integer

        Dim pgContentPlaceHolder As  ContentPlaceHolder
        pgContentPlaceHolder = CType(Master.FindControl("ContentMain1"), ContentPlaceHolder)
        Dim ltMess As  Literal
        ltMess = CType(pgContentPlaceHolder.FindControl("ltMessage"), Literal)

        'ddlProgramID.Value = Me.__programid.Value
        'ddlPrgVerId.Value = Me.__prgverid.Value

        'ddlSourceTypeId.Value = Me.__sourcetypeid.Value
        'ddlSourceAdvertisement.Value = Me.__sourceadvid.Value

        If Not txtBirthDate.SelectedDate Is Nothing Then
            If DateDiff(DateInterval.Year, CDate(txtBirthDate.SelectedDate), CDate(txtDate.Text)) < 1 Then
                'DisplayErrorMessage("The DOB cannot be greater than or equal to today's date ")
                DisplayRADAlert(CallbackType.Postback, "Error2", "The DOB cannot be greater than or equal to today's date ", "Update Error")
                txtAge.Text = ""
                Exit Sub
            End If

            Dim intConfigAge As Integer = CType(MyAdvAppSettings.AppSettings("StudentAgeLimit"), Integer)
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year
            Dim dob As Date = txtBirthDate.SelectedDate
            dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso _
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If
            If intAge < intConfigAge Then
                'DisplayErrorMessage("The minimum age requirement is " & intConfigAge)
                DisplayRADAlert(CallbackType.Postback, "Error3", "The minimum age requirement is " & intConfigAge, "Update Error")
                txtAge.Text = ""
                Exit Sub
            Else
                txtAge.Text = intAge
            End If
        End If
        ''Added by Saraswathi lakshmanan to validate the entry in childern field. only numbers can be entered
        ''Added on Feb 24 2008
        If txtChildren.Text <> "" Then
            If Not IsNumeric(txtChildren.Text) Then
                'DisplayErrorMessage("The field children should contain numbers")
                DisplayRADAlert(CallbackType.Postback, "Error4", "The field children should contain numbers ", "Update Error")
                Exit Sub
            End If
        End If

        If chkForeignZip.Checked = False Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not ddlStateID.SelectedValue = "" Or Not txtZip.Text = "" Or Not ddlCounty.SelectedValue = "" Then
                If ddlAddressType.SelectedValue = "" Or ddlAddressType.SelectedValue = Guid.Empty.ToString Then
                    strAddressType = "Address type is required" & vbLf
                End If
            End If
        End If

        If chkForeignZip.Checked = True Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not txtOtherState.Text = "" Or Not txtZip.Text = "" Or Not ddlCounty.SelectedValue = "" Then
                If ddlAddressType.SelectedValue = "" Or ddlAddressType.SelectedValue = Guid.Empty.ToString Then
                    strAddressType = "Address type is required" & vbLf
                End If
            End If
        End If

        'Code commented by Balaji on 04/15/2009 to fix issue 12013
        'Dim strValidateIPEDS As String = ValidateIPEDS()
        'If Not strValidateIPEDS = "" Then
        '    strAddressType &= strValidateIPEDS & vbLf
        'End If


        If Not txtPhone.Text = "" Then
            If ddlPhoneType.SelectedValue = "" Or ddlPhoneType.SelectedValue = "" Then
                strAddressType &= "Phone type is required" & vbLf
            End If
        End If

        If Not ddlPhoneType.SelectedValue = "" Then
            If txtPhone.Text = "" Then
                strAddressType &= "Phone is required" & vbLf
            End If
        End If
        If Not txtPhone2.Text = "" Then
            If ddlPhoneType2.SelectedValue = "" Or ddlPhoneType2.SelectedValue = "" Then
                strAddressType &= "Phone type is required" & vbLf
            End If
        End If

        If Not ddlPhoneType2.SelectedValue = "" Then
            If txtPhone2.Text = "" Then
                strAddressType &= "Phone is required" & vbLf
            End If
        End If
        If rdoPhone2.Checked = True Then
            If txtPhone2.Text = "" And ddlPhoneType.SelectedValue = "" Then
                strAddressType &= "Phone and phone type are required to make it a default Phone" & vbLf
            End If

        End If
        If Not strAddressType = "" Then
            'DisplayErrorMessage(strAddressType)
            DisplayRADAlert(CallbackType.Postback, "Error5", strAddressType, "Update Error")
            'Session("areaid_changed") = "no"
            'Session("programid_changed") = "no"
            'Session("sourcecatagoryid_changed") = "no"
            'Session("sourcetypeid_changed") = "no"
            Exit Sub
        End If

        Dim inquiryTime As String = Now.Date + " " + txtInquiryTime.Text
        If Not CommonWebUtilities.IsValidDate(inquiryTime) Then
            'DisplayErrorMessage("Enter Proper Inquiry Time Format")
            DisplayRADAlert(CallbackType.Postback, "Error6", "Enter Proper Inquiry Time Format ", "Update Error")
            Exit Sub
        End If
        'Check If Mask is successful only if Foreign Is not checked
        If chkForeignPhone.Checked = False Or chkForeignPhone2.Checked = False Or chkForeignZip.Checked = False Then
            errorMessage = ValidateFieldsWithInputMasks()
            If errorMessage <> "" Then
                'DisplayErrorMessage(errorMessage)
                DisplayRADAlert(CallbackType.Postback, "Error7", errorMessage, "Update Error")
                Exit Sub
            End If
        Else
            errorMessage = ""
        End If

        Try
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                Dim sRegMessage As String = ""
                If txtSSN.Text.ToString.Trim = "" Then
                    sRegMessage = "SSN is required for Regent" & vbCrLf
                End If
                If txtBirthDate.SelectedDate Is Nothing Then
                    sRegMessage &= "DOB is required for Regent" & vbCrLf
                End If
                If Not sRegMessage = "" Then
                    'DisplayErrorMessage(sRegMessage)
                    DisplayRADAlert(CallbackType.Postback, "Error8", sRegMessage, "Update Error")
                    Exit Sub
                End If
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        'Check for Duplicate Leads
        Dim facInputMask As New Fame.AdvantageV1.BusinessFacade.InputMasksFacade
        Dim phoneMask As String
        Dim zipMask As String
        Dim ssnMask As String
        Dim strSSN As String
        If txtSSN.Text <> "" Then
            ssnMask = facInputMask.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.SSN)
            strSSN = facInputMask.RemoveMask(ssnMask, txtSSN.Text)
        Else
            strSSN = txtSSN.Text
        End If


        Dim intValidateLeadGroups As Integer
        intValidateLeadGroups = ValidateLeadGroups()
        If intValidateLeadGroups = -1 Then
            ''Message changed from Student group to lead group.
            ''Changed by Saraswathi Lakshmanan on Feb 10 2011 for rally case de5033.
            'DisplayErrorMessage("Please select a lead group")
            DisplayRADAlert(CallbackType.Postback, "Error9", "Please select a lead group ", "Update Error")
            Exit Sub
            '**This validation has been temporarily suspended 4/3/09 DD
        ElseIf intValidateLeadGroups >= 2 Then
            'More that one Student Group with the "Use For Scheduling" setting
            'has been selected.
            'DisplayErrorMessage("Please select only one Student Group that is to be used for scheduling")
            DisplayRADAlert(CallbackType.Postback, "Error10", "Please select only one Student Group that is to be used for scheduling ", "Update Error")
            Exit Sub
        End If

        'Dim winSettings As String = "toolbar=no,status=no,width=550px,height=150px,left=250px,top=200px,modal=yes"
        'Dim name As String = "AdmReqSummary1"
        'Dim Message, Message1, URL As String
        Dim strDate As String = String.Empty
        If Not (txtBirthDate.SelectedDate Is Nothing) Then
            strDate = txtBirthDate.SelectedDate.ToString
        End If
        Dim intDuplicateLeadCount As Integer = leadMasterUpdate.CheckDuplicateLeads(txtFirstName.Text, txtLastName.Text, strSSN, strDate, txtMiddleName.Text, ddlCampusId.SelectedValue)
        'If intDuplicateLeadCount >= 1 Then
        '    If Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
        '        Message = "A Lead already exist with same First Name and Last Name."
        '        Message1 = "Do you want to proceed and add the lead ?"
        '        URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
        '        CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        '        Exit Sub
        '    ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
        '        Message = "A Lead already exist with same First Name,Middle Name and Last Name."
        '        Message1 = "Do you want to proceed and add the lead ?"
        '        URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
        '        CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        '        Exit Sub
        '    ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
        '        Message = "A Lead already exist with same First Name,Last Name and SSN."
        '        Message1 = "Do you want to proceed and add the lead ?"
        '        URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
        '        CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        '        Exit Sub
        '    ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
        '        Message = "A Lead already exist with same First Name,Middle Name,Last Name and SSN."
        '        Message1 = "Do you want to proceed and add the lead ?"
        '        URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
        '        CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        '        Exit Sub
        '    ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
        '        Message = "A Lead already exist with same First Name,Middle Name,Last Name and DOB."
        '        Message1 = "Do you want to proceed and add the lead ?"
        '        URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
        '        CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        '        Exit Sub
        '    ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
        '        Message = "A Lead already exist with same First Name,Last Name and DOB."
        '        Message1 = "Do you want to proceed and add the lead ?"
        '        URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
        '        CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        '        Exit Sub
        '    ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
        '        Message = "A Lead already exist with same First Name,Last Name,MiddleName,SSN and DOB."
        '        Message1 = "Do you want to proceed and add the lead ?"
        '        URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
        '        CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        '        Exit Sub
        '    ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
        '        Message = "A Lead already exist with same First Name,Last Name,SSN and DOB."
        '        Message1 = "Do you want to proceed and add the lead ?"
        '        URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
        '        CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        '        Exit Sub
        '    End If
        'End If

        If intDuplicateLeadCount >= 1 Then

            pnlMessage.Visible = True

            If Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                ltMess.Text = "&nbsp;&nbsp;&nbsp;&nbsp;A Lead already exists with same First Name and Last Name. <br />&nbsp;&nbsp;&nbsp;&nbsp;Do you want to proceed and add the lead ?"
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                ltMess.Text = "&nbsp;&nbsp;&nbsp;&nbsp;A Lead already exists with same First Name,Middle Name and Last Name. <br />&nbsp;&nbsp;&nbsp;&nbsp;Do you want to proceed and add the lead ?"
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                ltMess.Text = "&nbsp;&nbsp;&nbsp;&nbsp;A Lead already exists with same First Name,Last Name and SSN. <br />&nbsp;&nbsp;&nbsp;&nbsp;Do you want to proceed and add the lead ?"
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                ltMess.Text = "&nbsp;&nbsp;&nbsp;&nbsp;A Lead already exists with same First Name,Middle Name,Last Name and SSN. <br />&nbsp;&nbsp;&nbsp;&nbsp;Do you want to proceed and add the lead ?"
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                ltMess.Text = "&nbsp;&nbsp;&nbsp;&nbsp;A Lead already exists with same First Name,Middle Name,Last Name and DOB. <br />&nbsp;&nbsp;&nbsp;&nbsp;Do you want to proceed and add the lead ?"
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                ltMess.Text = "&nbsp;&nbsp;&nbsp;&nbsp;A Lead already exists with same First Name,Last Name and DOB. <br />&nbsp;&nbsp;&nbsp;&nbsp;Do you want to proceed and add the lead ?"
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                ltMess.Text = "&nbsp;&nbsp;&nbsp;&nbsp;A Lead already exists with same First Name,Last Name,MiddleName,SSN and DOB. <br />&nbsp;&nbsp;&nbsp;&nbsp;Do you want to proceed and add the lead ?"
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                ltMess.Text = "&nbsp;&nbsp;&nbsp;&nbsp;A Lead already exists with same First Name,Last Name,SSN and DOB. <br />&nbsp;&nbsp;&nbsp;&nbsp;Do you want to proceed and add the lead ?"
                Exit Sub
            End If
        End If


        Dim strPhone As String
        Dim strPhone2 As String
        Dim strZip As String

        phoneMask = facInputMask.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.Phone)
        If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
            strPhone = facInputMask.RemoveMask(phoneMask, txtPhone.Text)
        Else
            strPhone = txtPhone.Text
        End If
        If txtPhone2.Text <> "" And chkForeignPhone2.Checked = False Then
            strPhone2 = facInputMask.RemoveMask(phoneMask, txtPhone2.Text)
        Else
            strPhone2 = txtPhone2.Text
        End If

        'Get Zip
        If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            zipMask = facInputMask.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.Zip)
            strZip = facInputMask.RemoveMask(zipMask, txtZip.Text)
        Else
            strZip = txtZip.Text
        End If




        'Check for Duplicate Phone
        Dim strMessage As String = ""
        Dim intCheckDuplicatePhone As Integer = 0
        Dim intCheckDuplicateEmail As Integer = 0
        Dim intCheckDuplicateAddress As Integer = 0
        If Not strPhone.ToString.Trim = "" Then
            intCheckDuplicatePhone = (New LeadFacade).CheckDuplicateLeadsWithSamePhoneNumber(CampusId, strPhone, strPhone2)
            If intCheckDuplicatePhone >= 1 Then
                strMessage = "&nbsp;&nbsp;&nbsp;&nbsp;A Lead already exists with the same Phone Number"
            End If
        End If
        If Not txtWorkEmail.Text = "" Or Not txtHomeEmail.Text = "" Then
            intCheckDuplicateEmail = (New LeadFacade).CheckDuplicateLeadsWithSameEmail(CampusId, txtWorkEmail.Text, txtHomeEmail.Text)
        End If
        If Not txtAddress1.Text.ToString.Trim = "" Or Not txtAddress2.Text.ToString.Trim = "" Or Not txtCity.Text.Trim = "" Or Not strZip = "" Then
            intCheckDuplicateAddress = (New LeadFacade).CheckDuplicateLeadsWithSameAddress(CampusId, txtAddress1.Text, txtAddress2.Text, txtCity.Text, ddlStateID.Text, strZip)
        End If
        If intCheckDuplicateEmail >= 1 Then
            If intCheckDuplicatePhone >= 1 Then
                strMessage &= ", Email Address"
            Else
                strMessage = "&nbsp;&nbsp;&nbsp;&nbsp;A Lead already exists with the same Email Address"
            End If
        End If
        If intCheckDuplicateAddress >= 1 Then
            If intCheckDuplicateEmail >= 1 Then
                strMessage &= " and Address."
            Else
                If intCheckDuplicatePhone = 0 And intCheckDuplicateEmail = 0 Then
                    strMessage = "&nbsp;&nbsp;&nbsp;&nbsp;A Lead already exists with the same Address."
                End If
            End If
        End If

        'If Not strMessage = "" Then
        '    Message = strMessage
        '    Message1 = "Do you want to proceed and add the lead ?"
        '    URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
        '    CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        '    Exit Sub
        'End If
        If Not strMessage = "" Then
            pnlMessage.Visible = True
            ltMess.Text = strMessage + "<br />&nbsp;&nbsp;&nbsp;&nbsp;Do you want to proceed and add the lead ?"
            Exit Sub
        End If

        If errorMessage = "" Then
            Dim boolRegent As Boolean = False
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                boolRegent = True
            End If
            Result = leadMasterUpdate.UpdateLeadMaster(BuildLeadMaster(txtLeadMasterID.Text), AdvantageSession.UserState.UserName, txtResumeObjective.Text, boolRegent)

            If Result = "" Then
                Dim j As Integer
                j = UpdateLeadGroups(txtLeadMasterID.Text)

                If Not chkIsInDB.Checked Then
                    Result = AddNewTask()
                End If
                '   populate page fields with data just saved in DB
                ' BindLeadMasterData(LeadMasterUpdate.GetLeadsInfo(txtLeadMasterID.Text))

                '   save on syLeadStatusesChanges only if txtLeadStatusId (oldStatus) <> ddlLeadStatus.SelectedValue (newStatus)
                If ddlLeadStatus.SelectedValue <> txtLeadStatusId.Text Then
                    Dim statusChanged As New LeadStatusesInfo
                    With statusChanged
                        .LeadId = txtLeadMasterID.Text
                        .OrigStatusId = IIf(txtLeadStatusId.Text = "", System.Guid.Empty.ToString, txtLeadStatusId.Text)
                        .NewStatusId = ddlLeadStatus.SelectedValue
                        .ModDate = Date.Parse(txtModDate.Text)
                    End With
                    Result = (New LeadStatusChangeFacade).InsertLeadStatusChange(statusChanged, AdvantageSession.UserState.UserName)
                    If Not Result = "" Then
                        'display error inserting into syLeadStatusesChanges table
                        'DisplayErrorMessage(Result)
                        DisplayRADAlert(CallbackType.Postback, "Error11", Result, "Update Error")
                        Exit Sub
                    End If


                    ''''' Code Modified on 19th August to fix mantis issue id 19555
                    ViewState("CampusID") = Nothing
                   campusid = Master.CurrentCampusId 

                    Try
                        ddlCampusId.SelectedValue = CampusId
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)


                    End Try
                    ''''''''''''''''''''''''''

                    txtLeadStatusId.Text = ddlLeadStatus.SelectedValue
                    BuildStatusDDL(ddlLeadStatus.SelectedValue)
                    ddlLeadStatus.SelectedValue = txtLeadStatusId.Text
                End If


                Dim SDFID As ArrayList
                Dim SDFIDValue As ArrayList
                '                Dim newArr As ArrayList
                Dim z As Integer
                Dim SDFControl As New SDFComponent
                Try
                    txtPKID.Text = txtLeadMasterID.Text
                    SDFControl.DeleteSDFValue(txtPKID.Text)
                    SDFID = SDFControl.GetAllLabels(pnlSDF)
                    SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                    For z = 0 To SDFID.Count - 1
                        SDFControl.InsertValues(txtPKID.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                    Next
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
                If useSaveNext Then
                    AddLeadToMRU()
                    btnNew_Click(Me, New System.EventArgs())
                Else
                    chkIsInDB.Checked = True
                End If
                InitButtonsForEdit()
                If useSaveNext = False Then
                    NavigateToInfo()
                End If
                Exit Sub
            Else
                'DisplayErrorMessage(Result)
                DisplayRADAlert(CallbackType.Postback, "Error12", Result, "Update Error")
                Exit Sub
            End If
        Else
            DisplayErrorMessageMask(errorMessage)
        End If

        'This is to keep the values in AJAX DDLs when form is submitted
        'Session("areaid_changed") = "no"
        'Session("programid_changed") = "no"
        'Session("sourcecatagoryid_changed") = "no"
        'Session("sourcetypeid_changed") = "no"
    End Sub
    Private Function AddNewTask() As String
        Dim utInfo As New UserTaskInfo
        Dim rtn As String = String.Empty
        ' determine if this is an update or a new task by looking
        ' at the ViewState("usertaskid") which is set by passing a parameter to
        ' the page.

        utInfo.IsInDB = False
        utInfo.AssignedById = TMCommon.GetCurrentUserId()
        utInfo.EndDate = Utilities.GetAdvantageDBDateTime(Date.Now)
        utInfo.StartDate = Utilities.GetAdvantageDBDateTime(Date.Now)
        utInfo.Message = "Notice : A new Lead was added." & vbLf
        utInfo.Priority = 1
        utInfo.OwnerId = ddlAdmissionsRep.SelectedValue
        utInfo.ReId = txtLeadMasterID.Text
        utInfo.ModUser = TMCommon.GetCurrentUserId()
        utInfo.Status = TaskStatus.Pending
        utInfo.CampGrpID = Master.CurrentCampusId
        'utInfo.Status = ddlLeadStatus.SelectedValue
        ''utInfo.ResultId = txtStuEnrollmentId.Text


        ' Update the task and check the result
        If Not UserTasksFacade.AddTaskFromStatusCode(ddlLeadStatus.SelectedValue, utInfo) Then
            rtn = "Unable to add a reminder to the Task Manager. " & vbLf
        End If
        Return rtn
    End Function

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
            btnSaveNext.Enabled = True
        Else
            btnSave.Enabled = False
            btnSaveNext.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Function BuildLeadMaster(ByVal LeadMasterId As String) As LeadMasterInfo
        Dim LeadMaster As New LeadMasterInfo
        Dim facInputMask As New Fame.AdvantageV1.BusinessFacade.InputMasksFacade
        Dim phoneMask As String
        Dim zipMask As String
        Dim ssnMask As String

        With LeadMaster
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get LeadMasterID
            .LeadMasterID = txtLeadMasterID.Text

            'Get LastName
            .LastName = txtLastName.Text

            'Get FirstName
            .FirstName = txtFirstName.Text

            'Get MiddleName
            .MiddleName = txtMiddleName.Text

            'Get StatusId 
            .Status = ddlLeadStatus.SelectedValue

            'Get PrefixId 
            .Prefix = ddlPrefix.SelectedValue

            'Get SuffixId 
            .Suffix = ddlSuffix.SelectedValue

            'Get Title 
            '.Title = ddlTitleId.SelectedValue

            'Get BirthDate
            '.BirthDate = txtBirthDate.SelectedDate
            If txtBirthDate.SelectedDate Is Nothing Then
                .BirthDate = ""
            Else
                .BirthDate = Date.Parse(txtBirthDate.SelectedDate)
            End If

            'Get Age
            ' .Age = txtAge.Text

            'Get sponsor
            .Sponsor = ddlSponsor.SelectedValue

            'Get AdmissionsRep
            .AdmissionsRep = ddlAdmissionsRep.SelectedValue

            'Get Gender
            .Gender = ddlGender.SelectedValue

            'Get Race
            .Race = ddlRace.SelectedValue

            'Get MaritalStatus
            .MaritalStatus = ddlMaritalStatus.SelectedValue

            'Get Children
            .Children = txtChildren.Text

            'Get FamilyIncome
            .FamilyIncome = ddlFamilyIncome.SelectedValue

            'Get Phone
            phoneMask = facInputMask.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.Phone)
            If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
                .Phone = facInputMask.RemoveMask(phoneMask, txtPhone.Text)
            Else
                .Phone = txtPhone.Text
            End If
            If txtPhone2.Text <> "" And chkForeignPhone2.Checked = False Then
                .Phone2 = facInputMask.RemoveMask(phoneMask, txtPhone2.Text)
            Else
                .Phone2 = txtPhone2.Text
            End If
            'Get PhoneType
            .PhoneType = ddlPhoneType.SelectedValue
            .PhoneType2 = ddlPhoneType2.SelectedValue
            'Driver License State
            .DriverLicState = ddlDrivLicStateID.SelectedValue

            'Get HomeEmail
            .HomeEmail = txtHomeEmail.Text

            'Get WorkEmail
            .WorkEmail = txtWorkEmail.Text

            'Get Address1
            .Address1 = txtAddress1.Text

            'Get Address2
            .Address2 = txtAddress2.Text

            'Get City
            .City = txtCity.Text

            'Get State
            .State = ddlStateID.SelectedValue

            'Get Zip
            If txtZip.Text <> "" And chkForeignZip.Checked = False Then
                zipMask = facInputMask.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.Zip)
                .Zip = facInputMask.RemoveMask(zipMask, txtZip.Text)
            Else
                .Zip = txtZip.Text
            End If

            'Get Country 
            .Country = ddlCountry.SelectedValue

            'Get County
            .County = ddlCounty.SelectedValue


            'Get SourceDate 
            '.SourceDate = txtSourceDate.SelectedDate
            If txtSourceDate.SelectedDate Is Nothing Then
                .SourceDate = ""
            Else
                .SourceDate = Date.Parse(txtSourceDate.SelectedDate)
            End If

            '.Area = ddlAreaId.Value
            '.ProgramID = Me.__programid.Value
            '.PrgVerId = Me.__prgverid.Value

            '.SourceCategory = ddlSourceCategoryId.Value
            '.SourceType = Me.__sourcetypeid.Value
            '.SourceAdvertisement = Me.__sourceadvid.Value

            'Get Area,Program and Program version
            .Area = ddlAreaID.SelectedValue
            .ProgramID = ddlProgramID.SelectedValue
            .PrgVerId = ddlPrgVerId.SelectedValue


            'Get SourceCategory,Source Type and SourceAdvertisement
            .SourceCategory = ddlSourceCategoryId.SelectedValue
            .SourceType = ddlSourceTypeId.SelectedValue
            .SourceAdvertisement = ddlSourceAdvertisement.SelectedValue

            'Get ExpectedStart
            '.ExpectedStart = txtExpectedStart.SelectedDate
            If txtExpectedStart.SelectedDate Is Nothing Then
                .ExpectedStart = ""
            Else
                .ExpectedStart = Date.Parse(txtExpectedStart.SelectedDate)
            End If

            'Get ShiftID
            .ShiftID = ddlShiftID.SelectedValue

            'Get Nationality
            .Nationality = ddlNationality.SelectedValue

            'Get Citizen
            .Citizen = ddlCitizen.SelectedValue

            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                .EntranceInterviewDate = txtEntranceInterviewDate.SelectedDate
                .HighSchoolProgramCode = txtHighSchoolProgramCode.Text
            End If

            'Get SSN 
            If txtSSN.Text <> "" Then
                ssnMask = facInputMask.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.SSN)
                .SSN = facInputMask.RemoveMask(ssnMask, txtSSN.Text)
            Else
                .SSN = txtSSN.Text
            End If

            'Get DriverLicState 
            .DriverLicState = ddlStateID.SelectedValue

            'Get DriverLicNumber
            .DriverLicNumber = txtDrivLicNumber.Text

            'Get AlienNumber
            .AlienNumber = txtAlienNumber.Text

            'Notes
            .Notes = txtComments.Text

            'Get AddressTypes
            .AddressType = ddlAddressType.SelectedValue

            'Get Assignment Date
            '.AssignmentDate = txtAssignedDate.SelectedDate
            If txtAssignedDate.SelectedDate Is Nothing Then
                .AssignmentDate = ""
            Else
                .AssignmentDate = Date.Parse(txtAssignedDate.SelectedDate)
            End If

            '.AppliedDate = txtDateApplied.SelectedDate
            If txtDateApplied.SelectedDate Is Nothing Then
                .AppliedDate = ""
            Else
                .AppliedDate = Date.Parse(txtDateApplied.SelectedDate)
            End If

            .PreviousEducation = ddlPreviousEducation.SelectedValue


            .CampusId = ddlCampusId.SelectedValue

            .OtherState = txtOtherState.Text

            If chkForeignPhone.Checked = True Then
                .ForeignPhone = 1
            Else
                .ForeignPhone = 0
            End If
            If chkForeignPhone2.Checked = True Then
                .ForeignPhone2 = 1
            Else
                .ForeignPhone2 = 0
            End If
            If chkForeignZip.Checked = True Then
                .ForeignZip = 1
            Else
                .ForeignZip = 0
            End If
            ' If rdoPhone1.Checked Then .DefaultPhone = 1
            If rdoPhone1.Checked Then
                If .Phone = "" And .Phone2 <> "" Then
                    .DefaultPhone = 2
                Else
                    .DefaultPhone = 1
                End If

            End If
            If rdoPhone2.Checked Then .DefaultPhone = 2

            '.LeadGrpId = ddlLeadGrpId.SelectedValue

            .DependencyTypeId = ddlDependencyTypeId.SelectedValue
            .GeographicTypeId = ddlGeographicTypeId.SelectedValue
            .AdminCriteriaId = ddlAdminCriteriaId.SelectedValue
            .HousingTypeId = ddlHousingId.SelectedValue
            .DegCertSeekingId = ddlDegCertSeekingId.SelectedValue
            'Dim ActValue As New AdvantageCommonValues

            If chkForeignZip.Checked = False Then
                If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not ddlStateID.SelectedValue = "" Or Not txtZip.Text = "" Then
                    If ddlAddressStatus.SelectedValue = "" Or ddlAddressStatus.SelectedValue = Guid.Empty.ToString Then
                        .AddressStatus = AdvantageCommonValues.ActiveGuid
                    End If
                End If
            End If

            If chkForeignZip.Checked = True Then
                If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not txtOtherState.Text = "" Or Not txtZip.Text = "" Then
                    If ddlAddressStatus.SelectedValue = "" Or ddlAddressStatus.SelectedValue = Guid.Empty.ToString Then
                        .AddressStatus = AdvantageCommonValues.ActiveGuid
                    End If
                End If
            End If

            If Not txtPhone.Text = "" Then
                If ddlPhoneStatus.SelectedValue = "" Or ddlPhoneStatus.SelectedValue = Guid.Empty.ToString Then
                    .PhoneStatus = AdvantageCommonValues.ActiveGuid
                End If
            End If

            .InquiryTime = txtInquiryTime.Text
            .AdvertisementNote = txtAdvertisementNote.Text
        End With
        Return LeadMaster
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'Assign A New Value For Primary Key Column
        txtLeadMasterID.Text = Guid.NewGuid.ToString()

        ''''' Code Modified on 19th August to fix mantis issue id 19555
        ViewState("CampusID") = Nothing
       campusid = Master.CurrentCampusId 

        Try
            ddlCampusId.SelectedValue = CampusId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
        '''''''''''''''''''''

        'Create a Empty Object and Initialize the Object
        BindLeadMasterDataItemCommand(New LeadMasterInfo)

        'Reset The Value of chkIsInDb Checkbox To Identify an Insert
        chkIsInDB.Checked = False

        chkLeadGrpId.ClearSelection()



        Dim sdfControls As New SDFComponent
        sdfControls.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)

        txtAssignedDate.SelectedDate = Date.Now.ToShortDateString

        Try
            ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText("Unknown"))
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlGender.SelectedIndex = 0
        End Try

        Try
            ddlAddressType.SelectedIndex = ddlAddressType.Items.IndexOf(ddlAddressType.Items.FindByText("Home"))
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlAddressType.SelectedIndex = 0
        End Try

        Try
            ddlAdmissionsRep.SelectedValue = userId
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlAdmissionsRep.SelectedIndex = 0
        End Try

        'Initialize Buttons
        InitButtonsForLoad()

        'Initialize SourceDate and ExpectedStart Date
        txtSourceDate.Clear()
        txtExpectedStart.Clear()
        txtDateApplied.SelectedDate = Date.Now.ToShortDateString
        txtAdvertisementNote.Text = ""

        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            txtEntranceInterviewDate.Clear()
            txtHighSchoolProgramCode.Text = ""
        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
            btnSaveNext.Enabled = True
            btnNew.Enabled = True
        Else
            btnSave.Enabled = False
            btnNew.Enabled = False
        End If
        btnDelete.Enabled = False
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not (txtLeadMasterID.Text = Guid.Empty.ToString) Then
            'instantiate component
            Dim LeadMaster As New LeadFacade

            'Delete The Row Based on EmployerId Date.Parse()
            Dim result As String = LeadMaster.DeleteLeadMaster(txtLeadMasterID.Text, txtModDate.Text)

            If result <> "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error13", result, "Update Error")
                Exit Sub

            Else
                chkIsInDB.Checked = False

                Dim lscfac As New LeadStatusChangeFacade
                lscfac.DeleteLeadStatusChange(txtLeadMasterID.Text)

                'bind an empty new LeadMasterInfo
                BindLeadMasterDataItemCommand(New LeadMasterInfo)

                'initialize buttons
                InitButtonsForLoad()

                'Initialize SourceDate and ExpectedStart Date
                txtSourceDate.Clear()
                txtExpectedStart.Clear()
                txtAdvertisementNote.Text = ""

                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    txtEntranceInterviewDate.Clear()
                    txtHighSchoolProgramCode.Text = ""
                End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim SDFControl As New SDFComponent
                SDFControl.DeleteSDFValue(txtLeadMasterID.Text)
                SDFControl.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End If
        End If
    End Sub
    Private Sub BindLeadMasterDataItemCommand(ByVal LeadMaster As LeadMasterInfo)
        ' BuildDropDownLists()
        ''Bind The EmployerInfo Data From The Database
        With LeadMaster
            'get IsInDB
            chkIsInDB.Checked = .IsInDB

            'get LeadMasterID
            txtLeadMasterID.Text = .LeadMasterID

            'Get LastName
            txtLastName.Text = .LastName

            'Get FirstName
            txtFirstName.Text = .FirstName

            'Get MiddleName
            txtMiddleName.Text = .MiddleName

            'Get BirthDate
            'txtBirthDate.Text = .BirthDate
            Try
                txtBirthDate.SelectedDate = .BirthDate
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtBirthDate.Clear()
            End Try

            'Get Age
            txtAge.Text = ""

            'Get Phone
            txtPhone.Text = .Phone
            txtPhone.Mask = Replace(strMask, "#", "9")

            txtPhone2.Text = .Phone2
            txtPhone2.Mask = Replace(strMask, "#", "9")
            'Get HomeEmail
            txtHomeEmail.Text = .HomeEmail

            'Get WorkEmail
            txtWorkEmail.Text = .WorkEmail

            'Get Address1
            txtAddress1.Text = .Address1

            'Get Address2
            txtAddress2.Text = .Address2

            'Get City
            txtCity.Text = .City

            'Get State
            'txtBirthDate.Text = ""
            ddlStateID.SelectedValue = .State

            'Get Zip
            txtZip.Text = .Zip

            'Get SourceDate 
            'txtSourceDate.SelectedDate = .SourceDate
            Try
                txtSourceDate.SelectedDate = .SourceDate
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtSourceDate.Clear()
            End Try

            'Get ExpectedStart
            'txtExpectedStart.SelectedDate = .ExpectedStart
            Try
                txtExpectedStart.SelectedDate = .ExpectedStart
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtExpectedStart.Clear()
            End Try

            'Assignment Date
            'txtAssignedDate.SelectedDate = .AssignmentDate
            Try
                txtAssignedDate.SelectedDate = .AssignmentDate
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtAssignedDate.Clear()
            End Try

            'txtDateApplied.SelectedDate = .AppliedDate
            Try
                txtDateApplied.SelectedDate = .AppliedDate
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtDateApplied.Clear()
            End Try

            'Get SSN 
            txtSSN.Text = .SSN

            'Get DriverLicNumber
            txtDrivLicNumber.Text = .DriverLicNumber

            'Get AlienNumber
            txtAlienNumber.Text = .AlienNumber

            'Notes
            txtComments.Text = .Notes

            chkForeignPhone.Checked = False
            chkForeignPhone2.Checked = False
            chkForeignZip.Checked = False

            'Status Code
            ' BuildStatusDDL(.StatusCodeId)
            Dim strDefaultStatusCodeId As String = (New LeadFacade).GetNewDefaultLeadStatusCodeId(CampusId, userId)
            ''Code added by Atul Kamble on Nov 02,2010 for RallyID DE1201
            ''If Not strDefaultStatusCodeId = "" Then
            Try
                ddlLeadStatus.SelectedValue = strDefaultStatusCodeId.ToString
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlLeadStatus.SelectedValue = .StatusCodeId
            End Try
            ''End If
            ''Code added by Atul Kamble on Nov 02,2010 for RallyID DE1201
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                txtEntranceInterviewDate.SelectedDate = .EntranceInterviewDate
                txtHighSchoolProgramCode.Text = .HighSchoolProgramCode
            End If
            txtLeadStatusId.Text = ddlLeadStatus.SelectedValue

            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrefix, .Prefix, .PrefixDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSuffix, .Suffix, .SuffixDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdmissionsRep, .AdmissionsRep, .AdmissionRepsDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGender, .Gender, .GenderDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRace, .Race, .EthCodeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlMaritalStatus, .MaritalStatus, .MaritalStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFamilyIncome, .FamilyIncome, .FamilyIncomeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneType, .PhoneType, .PhoneTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneType2, .PhoneType2, .PhoneTypeDescrip2)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus, .Status, .StatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateID, .State, .StateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountry, .Country, .CountryDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCounty, .County, .CountyDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPreviousEducation, .PreviousEducation, .EdLvlDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressStatus, .Status, .StatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlShiftID, .ShiftID, .ShiftDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlNationality, .Nationality, .NationalityDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCitizen, .Citizen, .CitizenDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDrivLicStateID, .DriverLicState, .DriverLicenseStateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressType, .AddressType, .AddressTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus, .PhoneStatus, .PhoneStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus, .PhoneStatus2, .PhoneStatusDescrip2)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressStatus, .AddressStatus, .AddressStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDependencyTypeId, .DependencyTypeId, .DependencyTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGeographicTypeId, .GeographicTypeId, .GeographicTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSponsor, .Sponsor, .SponsorTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdminCriteriaId, .AdminCriteriaId, .AdminCriteriaDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlHousingId, .HousingTypeId, .HousingTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDegCertSeekingId, .DegCertSeekingId, .DegCertSeekingDescrip)

            'Always make sure the default country is selected
            'if the default country is marked as inactive then select the default value "select"
            Try
                ddlCountry.SelectedValue = strDefaultCountry
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlCountry.SelectedIndex = 0
            End Try

            'Get Area
            ddlAreaID.SelectedValue = .Area

            'Get ProgramID
            If Not .Area = "" Then
                BuildProgramsDDL(ddlAreaID.SelectedValue)
                ddlProgramID.SelectedValue = .ProgramID
            Else
                ddlProgramID.SelectedIndex = 0
            End If

            'Get Program Version
            If Not .ProgramID = "" Then
                BuildPrgVersionDDL()
                ddlPrgVerId.SelectedValue = .PrgVerId
            Else
                ddlPrgVerId.SelectedIndex = 0
            End If

            'Get SourceCategory
            ddlSourceCategoryId.SelectedValue = .SourceCategory

            'Get SourceType
            If Not .SourceCategory = "" Then
                BuildSourceTypeDDL(ddlSourceCategoryId.SelectedValue)
                ddlSourceTypeId.SelectedValue = .SourceType
            Else
                ddlSourceTypeId.SelectedIndex = 0
            End If

            'SourceAdvertisement
            If Not .SourceType = "" Then        'Or
                BuildSourceAdvDDL(ddlSourceTypeId.SelectedValue)
                ddlSourceAdvertisement.SelectedValue = .SourceAdvertisement
            Else
                ddlSourceAdvertisement.SelectedIndex = 0
            End If
            txtInquiryTime.Text = .InquiryTime
            txtAdvertisementNote.Text = .AdvertisementNote

            ''Code added by Atul Kamble on Nov 02,2010 for RallyID DE1201
            txtChildren.Text = .Children
            ''Code added by Atul Kamble on Nov 02,2010 for RallyID DE1201
        End With
    End Sub
    Private Sub txtBirthDate_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBirthDate.SelectedDateChanged
        Dim intAge As Integer
        If Not txtBirthDate.SelectedDate Is Nothing And IsDate(txtBirthDate.SelectedDate) Then
            If DateDiff(DateInterval.Year, CDate(txtBirthDate.SelectedDate), CDate(txtDate.Text)) < 1 Then
                'DisplayErrorMessage("The minimum age requirement for student is 18 ")
                DisplayRADAlert(CallbackType.Postback, "Error14", "The minimum age requirement for student is 18 ", "Update Error")
                txtAge.Text = ""
                Exit Sub
            End If
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year - 1
            Dim dob As Date = CType(txtBirthDate.SelectedDate, Date)
            'dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso _
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year - 1
            txtAge.Text = intAge
        End If
    End Sub
    Private Sub txtSourceDate_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSourceDate.SelectedDateChanged
    End Sub
    Private Sub BuildStatusDDL(ByVal leadStatusId As String)
        'Dim Status As New LeadFacade
        Dim facade As New LeadStatusChangeFacade
        With ddlLeadStatus
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            .DataSource = facade.GetAvailLeadStatuses("", CampusId, userId) 'Status.GetNewLeadStatus()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub btnCheckDuplicates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckDuplicates.Click
        Dim winSettings As String = "toolbar=no, status=no, resizable=yes,width=900px,height=470px"
        Dim name As String = "DuplicateLead"
        Dim strDate As String = String.Empty
        If Not (txtBirthDate.SelectedDate Is Nothing) Then
            strDate = txtBirthDate.SelectedDate.ToString
        End If
        '''' Code modified by kamalesh Ahuja on May 18 2010 to fix mantis issues id 18800 
        ''Dim url As String = "DisplayDuplicateLeads.aspx?FirstName=" + Trim(txtFirstName.Text) + "&LastName=" + Trim(txtLastName.Text) + "&SSN=" + Trim(txtSSN.Text) + "&DOB=" + Trim(txtBirthDate.Text) + "&repid=" + Trim(txtUserId.Text) + "&CampusId=" + txtCampusId.Text 
        Dim url As String = "DisplayDuplicateLeads.aspx?FirstName=" + Trim(txtFirstName.Text) + "&LastName=" + Trim(txtLastName.Text) + "&SSN=" + Trim(txtSSN.Text) + "&DOB=" + Trim(strDate) + "&repid=" + Trim(txtUserId.Text) + "&CampusId=" + txtCampusId.Text + "&Address=" + txtAddress1.Text + "&Phone=" + txtPhone.Text + "&Phone2=" + txtPhone2.Text
        '''''''''''''''
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)

    '    '   Set error condition
    '    Customvalidator1.ErrorMessage = errorMessage
    '    Customvalidator1.IsValid = False

    '    If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
    '        '   Display error in message box in the client
    '        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    '    End If

    'End Sub
    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPhone.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        txtPhone.Text = ""
        CommonWebUtilities.SetFocus(Me.Page, txtPhone)
    End Sub
    Private Sub chkForeignPhone2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPhone2.CheckedChanged
        'Dim objCommon As New CommonWebUtilities
        txtPhone2.Text = ""
        CommonWebUtilities.SetFocus(Me.Page, txtPhone2)
    End Sub

    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignZip.CheckedChanged
        'Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtAddress1)
        If chkForeignZip.Checked = True Then
            txtOtherState.Enabled = True
        End If
    End Sub
    Private Sub ddlStateId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStateID.SelectedIndexChanged
    End Sub

    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOtherState.TextChanged
        'Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtZip)
    End Sub

    Private Sub txtCity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCity.TextChanged
        If chkForeignZip.Checked = True Then
            'Dim objCommon As New CommonWebUtilities
            CommonWebUtilities.SetFocus(Me.Page, txtOtherState)
        End If
    End Sub
    Private Sub txtBirthDate_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBirthDate.PreRender
    End Sub
    Protected Sub btnSaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNext.Click
        useSaveNext = True
        btnSave_Click(Me, New EventArgs())
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.

        If Not Trim(Request.Form("scrollposition")) = "" Then
            Dim i As Integer = CInt(Trim(Request.Form("scrollposition")))
            Session("ScrollValue") = i
        End If

        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnCheckDuplicates)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub ddlSourceCategoryId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSourceCategoryId.SelectedIndexChanged
        If Not ddlSourceCategoryId.SelectedValue = "" Then
            BuildSourceTypeDDL(ddlSourceCategoryId.SelectedValue)
            ddlSourceAdvertisement.Items.Clear()
            With ddlSourceAdvertisement
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            ddlSourceTypeId.Items.Clear()
            With ddlSourceTypeId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            ddlSourceAdvertisement.Items.Clear()
            With ddlSourceAdvertisement
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Private Sub ddlSourceTypeId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSourceTypeId.SelectedIndexChanged
        If Not ddlSourceTypeId.SelectedIndex = 0 Then
            BuildSourceAdvDDL(ddlSourceTypeId.SelectedValue)
        Else
            With ddlSourceAdvertisement
                .Items.Clear()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Private Sub ddlAreaID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAreaID.SelectedIndexChanged
        If Not ddlAreaID.SelectedValue = "" Then
            BuildProgramsDDL(ddlAreaID.SelectedValue)
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            ddlProgramID.Items.Clear()
            With ddlProgramID
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Protected Sub ddlProgramID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProgramID.SelectedIndexChanged
        If Not ddlProgramID.SelectedIndex = 0 Then
            BuildPrgVersionDDL()
        Else
            With ddlPrgVerId
                .Items.Clear()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Private Sub NavigateToInfo()
        Dim studentName As New StudentSearchFacade
        Dim defaultCampusId As String
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String
        Dim state As AdvantageSessionState
        Dim fac As New UserSecurityFacade
        Dim arrUpp As  ArrayList
        Dim pURL As String

        defaultCampusId = Master.CurrentCampusId
        defaultCampusId = XmlConvert.ToGuid(defaultCampusId).ToString

        'This is used to load lead into the MRU
        Session("SEARCH") = 1

        'Set relevant properties on the state object
        objStateInfo.LeadId = txtLeadMasterID.Text
        objStateInfo.NameCaption = "Lead : "
        objStateInfo.NameValue = studentName.GetLeadNameByID(txtLeadMasterID.Text)

        'Create a new guid to be associated with the lead pages
        strVID = Guid.NewGuid.ToString

        'Add an entry to AdvantageSessionState for this guid and object load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo

        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        arrUpp = fac.GetUserResourcePermissionsForSubModule(userId, 395, "Admissions", Master.CurrentCampusId)
        If arrUpp.Count = 0 Then
            'User does not have permission to any resource for this submodule
            'Session("Error") = "The Lead was successfully added, But you do not have permission to any of the pages for existing leads<br> for the campus that you are logged in to."
            'Response.Redirect("../ErrorPage.aspx")
            RadNotification1.Show()
            RadNotification1.Text = "The Lead was successfully added, But you do not have permission to any of the pages for existing leads for the campus that you are logged in to."

        ElseIf fac.DoesUserHasAccessToSubModuleResource(arrUpp, 170) Then
            Response.Redirect("../AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)

        Else
            'redirect to the first page that the user has permission to for the submodule
            pURL = BuildPartialURL(arrUpp(0))
            Response.Redirect(pURL & "&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)
        End If
    End Sub
    Private Function BuildPartialURL(ByVal uppInfo As UserPagePermissionInfo) As String
        Return uppInfo.URL & "?resid=" & uppInfo.ResourceId
    End Function
    Private Sub AddLeadToMRU()
        Dim objMRUFac As New MRUFacade
        Dim ds As  DataSet
        Dim objStateInfo As New AdvantageStateInfo

        ds = objMRUFac.LoadAndUpdateMRU("Leads", txtLeadMasterID.Text.ToString, txtUserId.Text.ToString, txtCampusId.Text.ToString)
        objStateInfo.MRUDS = ds

    End Sub
    'Private Sub IPEDSRequirements()
    '    'isIPEDSApplicable = SingletonAppSettings.AppSettings("IPEDS")
    '    'If isIPEDSApplicable.ToLower = "yes" Then
    '    '    'Make Gender,Ethnic Code,Previous Education,Citizenship,BirthDate,state,Nationality Reqd
    '    '    ddlGender.BackColor = Color.FromName("#ffff99")
    '    '    ddlRace.BackColor = Color.FromName("#ffff99")
    '    '    ddlPreviousEducation.BackColor = Color.FromName("#ffff99")
    '    '    ddlCitizen.BackColor = Color.FromName("#ffff99")
    '    '    txtBirthDate.BackColor = Color.FromName("#ffff99")
    '    '    ddlStateID.BackColor = Color.FromName("#ffff99")
    '    '    ddlNationality.BackColor = Color.FromName("#ffff99")
    '    'End If
    'End Sub
    'Private Function ValidateIPEDS() As String
    '    Dim strValidateIPEDS As String = ""
    '    isIPEDSApplicable = MyAdvAppSettings.AppSettings("IPEDS")
    '    If isIPEDSApplicable.ToLower = "yes" Then
    '        If ddlGender.SelectedValue = "" Then
    '            strValidateIPEDS = "Gender is required"
    '        End If
    '        If ddlRace.SelectedValue = "" Then
    '            strValidateIPEDS &= "Race is required" & vbLf
    '        End If
    '        If ddlPreviousEducation.SelectedValue = "" Then
    '            strValidateIPEDS &= "Previous Education is required" & vbLf
    '        End If
    '        If ddlCitizen.SelectedValue = "" Then
    '            strValidateIPEDS &= "Citizen is required" & vbLf
    '        End If
    '        If txtBirthDate.SelectedDate Is Nothing Then
    '            strValidateIPEDS &= "DOB is required" & vbLf
    '        End If
    '        'If ddlStateID.SelectedValue = "" Then
    '        '    strValidateIPEDS &= "State is required" & vbLf
    '        'End If
    '        If ddlNationality.SelectedValue = "" Then
    '            strValidateIPEDS &= "Nationality is required" & vbLf
    '        End If
    '        Return strValidateIPEDS
    '    Else
    '        Return strValidateIPEDS
    '    End If
    'End Function
    'Private Sub DisplayMessageBox(ByVal Message As String)
    '    newBox.MessageBoxButton = 3
    '    newBox.MessageBoxTop = 150
    '    newBox.MessageBoxLeft = 250
    '    newBox.MessageBoxWidth = 450
    '    newBox.MessageBoxHeight = 150
    '    newBox.MessageBoxButtonWidth = 50
    '    newBox.MessageBoxIDYes = "yes"
    '    newBox.MessageBoxIDNo = "no"
    '    newBox.MessageBoxIDCancel = "cancel"
    '    newBox.MessageBoxButtonYesText = "Yes"
    '    newBox.MessageBoxButtonNoText = "No"
    '    newBox.MessageBoxButtonCancelText = "Cancel"
    '    newBox.MessageBoxTitle = "Duplicate Leads Check"
    '    newBox.MessageBoxMessage = Message
    '    newBox.MessageBoxImage = "Information.gif"
    '    PlaceHolder1.Controls.Add(newBox)
    'End Sub
    'Private Sub CheckYesNo()
    '    If dup.Value = "yes" Then
    '        AddLead()
    '        Exit Sub
    '    End If
    'End Sub
    Private Sub AddLead()
        Dim LeadMasterUpdate As New LeadFacade
        Dim Result As String
        Result = LeadMasterUpdate.UpdateLeadMaster(BuildLeadMaster(txtLeadMasterID.Text), AdvantageSession.UserState.UserName, txtResumeObjective.Text)
        If Result = "" Then
            'dup.Value = ""
            If Not chkIsInDB.Checked Then
                Result = AddNewTask()
            End If
            '   populate page fields with data just saved in DB
            ' BindLeadMasterData(LeadMasterUpdate.GetLeadsInfo(txtLeadMasterID.Text))

            '   save on syLeadStatusesChanges only if txtLeadStatusId (oldStatus) <> ddlLeadStatus.SelectedValue (newStatus)
            If ddlLeadStatus.SelectedValue <> txtLeadStatusId.Text Then
                Dim statusChanged As New LeadStatusesInfo
                With statusChanged
                    .LeadId = txtLeadMasterID.Text
                    .OrigStatusId = IIf(txtLeadStatusId.Text = "", System.Guid.Empty.ToString, txtLeadStatusId.Text)
                    .NewStatusId = ddlLeadStatus.SelectedValue
                    .ModDate = Date.Parse(txtModDate.Text)
                End With
                Result = (New LeadStatusChangeFacade).InsertLeadStatusChange(statusChanged, AdvantageSession.UserState.UserName)
                If Not Result = "" Then
                    'display error inserting into syLeadStatusesChanges table
                    'DisplayErrorMessage(Result)
                    DisplayRADAlert(CallbackType.Postback, "Error15", Result, "Update Error")
                    Exit Sub
                End If
                txtLeadStatusId.Text = ddlLeadStatus.SelectedValue
                BuildStatusDDL(ddlLeadStatus.SelectedValue)
                ddlLeadStatus.SelectedValue = txtLeadStatusId.Text
            End If


            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                txtPKID.Text = txtLeadMasterID.Text
                SDFControl.DeleteSDFValue(txtPKID.Text)
                SDFID = SDFControl.GetAllLabels(pnlSDF)
                SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtPKID.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            If useSaveNext Then
                AddLeadToMRU()
                btnNew_Click(Me, New EventArgs())
            Else
                chkIsInDB.Checked = True
            End If
            InitButtonsForEdit()
            If useSaveNext = False Then
                NavigateToInfo()
            End If
            Exit Sub
        Else
            'DisplayErrorMessage(Result)
            DisplayRADAlert(CallbackType.Postback, "Error16", Result, "Update Error")
            Exit Sub
        End If
    End Sub
    'Private Sub CheckForDuplicates()

    'End Sub
    Protected Sub ddlCampusId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampusId.SelectedIndexChanged
        ''''' Code Modified on 18th August to fix mantis issue id 19555
        ViewState("CampusID") = ddlCampusId.SelectedValue
        CampusId = ddlCampusId.SelectedValue
        ''''''''

        BuildDropDownLists()
    End Sub

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        pnlMessage.Visible = False
        If useSaveNext = False Then
            AddLead()
            Exit Sub
        ElseIf useSaveNext = True Then
            AddLead()
            Exit Sub
        End If
    End Sub

End Class
