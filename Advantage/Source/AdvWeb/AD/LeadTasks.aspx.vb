﻿Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.Common.TM
Imports Advantage.Business.Logic.Layer
Imports FAME.AdvantageV1.BusinessFacade.TM

Partial Class LeadTasks
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected LeadId As String
    Protected UserId As String


#End Region




    Private pObj As New UserPagePermissionInfo

    Private mruProvider As MRURoutines
    Protected StudentId As String
    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function GetLeadFromStateObject(ByVal paramResourceId As Integer) As Advantage.Business.Objects.StudentMRU

        Dim objStudentState As New Advantage.Business.Objects.StudentMRU

        Try
            MyBase.GlobalSearchHandler(1)

            boolSwitchCampus = Master.Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If


            Master.Master.ShowHideStatusBarControl(True)
            Master.Master.PageObjectId = LeadId
            Master.Master.PageResourceId = Request.QueryString("resid")
            Master.Master.setHiddenControlForAudit()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function



#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim campusId As String

        '  Dim m_Context2 As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'UserId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        UserId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState = GetLeadFromStateObject(484) 'Pass resourceid

        If objStudentState Is Nothing Then
            MyBase.RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''



        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            Dim reid As String = LeadId

            ' check if the reid paramter was passed and if not, return an error
            If reid Is Nothing Or reid = "" Then
                TMCommon.DisplayErrorMessage(Me, "The required paramter reid was not passed to the page.")
                Return
            End If

            dgdLeadTasks.DataSource = UserTasksFacade.GetUserTasks(TMCommon.GetCampusID(), Nothing, Nothing, reid, Nothing, TaskStatus.None, Nothing, Nothing)
            dgdLeadTasks.DataBind()

            Me.Title = "User Task History for " + UserTasksFacade.GetContactName(reid)

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""

        End If
    End Sub

    Protected Sub dgdLeadTasks_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgdLeadTasks.ItemDataBound
        Dim lb As Label = e.Item.FindControl("lblStatus")
        If Not lb Is Nothing Then
            If lb.Text = TaskStatus.Cancelled Then
                lb.Text = "Cancelled"
            ElseIf lb.Text = TaskStatus.Completed Then
                lb.Text = "Completed"
            ElseIf lb.Text = TaskStatus.Pending Then
                lb.Text = "Pending"
            Else
                lb.Text = "N/A"
            End If
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'BIndToolTip()
    End Sub
End Class
