Imports FAME.Common
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.Common

Partial Class LeadGroups
    Inherits BasePage
    Protected WithEvents btnhistory As Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Protected userId As String
    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim sw As New IO.StringWriter
        Dim campusId As String
        Dim userId As String
        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If



        'Check if this page still exists in the menu while switching campus
        Dim boolPageStillPartofMenu As Boolean = False

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            If Not Page.IsPostBack Then
                m_Context = HttpContext.Current
                If (Not ClientScript.IsStartupScriptRegistered("refreshSession")) Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "refreshSession", "refreshLeadInfoSession();", True)
                End If
                txtResourceId.Text = CInt(m_Context.Items("ResourceId"))
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                txtLeadGrpId.Text = Guid.NewGuid.ToString()
PopulateDataList()
                
                ViewState("ds") = sw.ToString()

            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim sw As New IO.StringWriter
        Dim msg As String
        If campusId Is Nothing Then
            campusId = AdvantageSession.UserState.CampusId.ToString
        End If
        Try
            If ViewState("MODE") = "NEW" Then
                'Set the Primary Key value, so it can be used later by the DoUpdate method
                objCommon.PagePK = txtLeadGrpId.Text
                msg = objCommon.DoInsert(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If

                txtRowIds.Text = objCommon.PagePK

                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"
                dlstAdmissionReqGroups.SelectedIndex = -1
                PopulateDataList()
                ViewState("ds") = sw.ToString()
            ElseIf ViewState("MODE") = "EDIT" Then
                msg = objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg = "" Then
                    dlstAdmissionReqGroups.SelectedIndex = -1
                    PopulateDataList()
                    ViewState("ds") = sw.ToString()
                Else
                    DisplayErrorMessage(msg)
                End If       
            End If
            '   set Style to Selected Item
            ' CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, txtLeadGrpId.Text, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, txtLeadGrpId.Text)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim sw As New IO.StringWriter
        Dim msg As String
        If campusId Is Nothing Then
            campusId = AdvantageSession.UserState.CampusId.ToString
        End If
        Try
            msg = objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            If msg = "" Then
                ClearRHS()
                dlstAdmissionReqGroups.SelectedIndex = -1
                PopulateDataList()
                ViewState("ds") = sw.ToString()
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                txtLeadGrpId.Text = Guid.NewGuid.ToString()
            Else
                DisplayErrorMessage(msg)
            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlrhs.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        'Dim ds As New DataSet
        Dim sr As New IO.StringReader(CStr(ViewState("ds")))

        Try
            ClearRHS()
            PopulateDataList()

            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            txtLeadGrpId.Text = Guid.NewGuid.ToString()
            'Reset Style in the Datalist
            ' CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, Guid.Empty.ToString, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub


    Private Sub PopulateDataList()

        Dim statuses = New List(Of String)

        If (radstatus.SelectedItem.Text = "Active") Then
            statuses.Add("Active")
        ElseIf (radstatus.SelectedItem.Text = "Inactive") Then   'Show InActive Only
            statuses.Add("Inactive")
        Else
            statuses.Add("Active")
            statuses.Add("Inactive")
        End If
         If campusId Is Nothing Then
            campusId = AdvantageSession.UserState.CampusId.ToString
        End If
        Dim campusIdGuid = Guid.Parse(campusId)
        Dim leadgroupDA = New LeadGroupDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString)
        dim leadGroupsDs = leadgroupDA.GetLeadGroupsByCampus(campusIdGuid, statuses)
    
       
        Dim leadGroupsDv = New DataView()
        Dim leadGroupDt= New DataTable()
        leadGroupDt.Columns.Add("Id")
             leadGroupDt.Columns.Add("Description")
          leadGroupDt.Columns.Add("status")
        For Each item  In leadGroupsDs  
            Dim row As DataRow = leadGroupDt.NewRow()  
    row("Id") = item.Id
              row("Description") = item.Description
              row("status") = item.Status
            leadGroupDt.Rows.Add(row)  
        Next  


        With dlstAdmissionReqGroups
            .DataSource = leadGroupDt
            .DataBind()
        End With
        dlstAdmissionReqGroups.SelectedIndex = -1

    End Sub


    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radstatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim objCommon As New CommonUtilities
        campusId = AdvantageSession.UserState.CampusId.ToString
        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtLeadGrpId.Text = Guid.NewGuid.ToString
            PopulateDataList()
            CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnSave_Command(ByVal sender As Object, ByVal e As CommandEventArgs) Handles btnSave.Command

    End Sub

    Private Sub dlstAdmissionReqGroups_ItemCommand1(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstAdmissionReqGroups.ItemCommand
        Dim selIndex As Integer
        Dim sr As New IO.StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Try
            strGUID = dlstAdmissionReqGroups.DataKeys(e.Item.ItemIndex).ToString()
            txtRowIds.Text = e.CommandArgument.ToString
            objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGUID)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = e.Item.ItemIndex
            dlstAdmissionReqGroups.SelectedIndex = selIndex
            PopulateDataList()
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, e.CommandArgument, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, e.CommandArgument)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstCounties_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstCounties_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Overloads Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(CType(ctl, Panel))
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(CType(ctl, DataGrid))
            End If
        Next
    End Sub
    Public Overloads Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Overloads Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub
End Class
