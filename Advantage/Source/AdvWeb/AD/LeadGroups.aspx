<%@ Page Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    Inherits="LeadGroups" CodeFile="LeadGroups.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script type="text/javascript">      
		
      window.onload = function(){
        var strCook = document.cookie;
        if(strCook.indexOf("!~")!=0){
          var intS = strCook.indexOf("!~");
          var intE = strCook.indexOf("~!");
          var strPos = strCook.substring(intS+2,intE);
          document.getElementById("grdWithScroll").scrollTop = strPos;
        }
      }
      function SetDivPosition(){
        var intY = document.getElementById("grdWithScroll").scrollTop;
            document.cookie = "yPos=!~" + intY + "~!";
      }
        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }
        function refreshLeadInfoSession() {
            sessionStorage.setItem("captionRequirementdb", "{}");
        }
    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%"
            Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized"
            Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350"
                Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                    </td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server"
                                            RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstAdmissionReqGroups" runat="server" Width="100%" DataKeyField="Id">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server"
                                            CausesValidation="False" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>'>
                                        </asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" CausesValidation="False"
                                            Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>'>
                                        </asp:ImageButton>
                                        <asp:LinkButton Text='<%# Container.DataItem("Description")%> ' runat="server" CssClass="itemstyle"
                                            CommandArgument='<%# Container.DataItem("Id")%>' ID="Linkbutton1" CausesValidation="False" />
                                    </ItemTemplate>
                                </asp:DataList></div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both"
                orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                            <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False">
                            </asp:Button>
                            <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                            </asp:Button>
                        </td>
                        
                    </tr>
                </table>
                <!-- end top menu (save,new,reset,delete,history)-->
                <!--begin right column-->
                <table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="100%"
                    border="0">
                    <tr>
                        <td class="detailsframe">
                             <asp:Panel ID="pnlrhs" runat="server">
                            <div class="scrollright2">
                                <!-- begin table content-->
                                <!-- Two column template with no spacer in between cells -->
                                <!-- used in placement module for maintenance pages -->
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblCode" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:TextBox ID="txtCode" runat="server" CssClass="textbox" Width="85%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlStatusId" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblDescrip" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:TextBox ID="txtDescrip" runat="server" CssClass="textbox" Width="85%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlCampGrpId" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:TextBox ID="txtLeadGrpId" runat="server" Visible="false"></asp:TextBox>
                                        </td>
                                        <td class="twocolumncontentcell">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                        </td>
                                        <td class="twocolumncontentcell">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:CheckBox ID="chkUseForScheduling" CssClass="label" runat="server" Text="Use for Scheduling" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                        </td>
                                        <td class="twocolumncontentcell">
                                            <asp:CheckBox ID="chkUseForStudentGroupTracking" CssClass="label" runat="server" Text="Use for Student Group Tracking" />
                                        </td>
                                    </tr>
                                </table>
                                <!--end table content-->
                                <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="label"
                                    Width="10px"></asp:TextBox>
                                <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="label"
                                    Width="10px"></asp:TextBox>
                                    </div>
                                     </asp:Panel>
                        </td>
                    </tr>
                </table>
               
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>
</asp:Content>
