﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects

Partial Class viewstudentgroups
    Inherits BasePage
    Protected StudentId As String
    Protected UserId, campusId As String
    Protected pObj As New UserPagePermissionInfo
    Protected resourceId As Integer
#Region " Web Form Designer Generated Code "
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub
#End Region

    Private mruProvider As MRURoutines
    Protected state As AdvantageSessionState
    Protected LeadId As String
    Protected boolSwitchCampus As Boolean = False


    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub

    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
    Private Function GetLastEntityFromStateObject() As BO.StudentMRU
        Dim objStateInfo As AdvantageStateInfo
        Dim strVID As String
        'Dim facInputMasks As New InputMasksFacade
        Dim strStudentId As String = ""
        Dim objEntityState As New BO.StudentMRU

        strStudentId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                               1, _
                                                               AdvantageSession.UserState.CampusId.ToString)


        objStateInfo = mruProvider.BuildStudentStatusBar(strStudentId, AdvantageSession.UserState.CampusId.ToString)
        With objStateInfo
            If .StudentIdentifierCaption.ToLower = "ssn" Then
                Dim ssnMask As String
                Dim facInputMasks As New InputMasksFacade
                ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
                If .StudentIdentifier <> "" Then
                    .StudentIdentifier = facInputMasks.ApplyMask(ssnMask, .StudentIdentifier)
                End If
            End If
            If .LeadId Is Nothing OrElse .LeadId = "" Then
                .LeadId = Guid.Empty.ToString
            End If
        End With


        'Create a new guid to be associated with the employer pages
        strVID = Guid.NewGuid.ToString

        'Add an entry to AdvantageSessionState for this guid and object
        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        'UpdateMRUTable
        'Reason: We need to keep track of the last student record the user worked with
        'Scenario1 : User can log in and click on a student page without using MRU 
        'and we need to display the data of the last student the user worked with
        'If the user is a first time user, we will load the student who was last added to advantage
        mruProvider.UpdateMRUList(strStudentId, AdvantageSession.UserState.UserId.ToString, _
                                  AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

        With objEntityState
            .StudentId = New Guid(objStateInfo.StudentId.ToString)
            .LeadId = New Guid(objStateInfo.LeadId.ToString)
            .ChildId = New Guid(strVID)
        End With
        Return objEntityState
    End Function
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        UserId = AdvantageSession.UserState.UserId.ToString


        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(628) 'Pass resourceid so that user can be redirected to same page while swtiching students
        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString
        End With

        'There are scenarios when a user may migrate from a student page but the student may not have any lead
        'associated data. In that case, the behavior is to load the lead user previously accessed instead of 
        'redirecting user to an error page
        If StudentId.Trim = "" Or StudentId = Guid.Empty.ToString Then
            objStudentState = GetLastEntityFromStateObject() 'Get Last Lead User accessed
            With objStudentState
                StudentId = .StudentId.ToString
                LeadId = .LeadId.ToString
            End With
        End If

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'pObj = Header1.UserPagePermission

        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        End If

        'Delete Should Always Be Disabled
        btnDelete.Enabled = False

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            BindAllEnrollments()
            BindGroupsDLL(advantageUserState.IsUserSA)
            BindDataGrid()

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""


        End If
    End Sub
    Private Sub BindDataGrid()
        Dim StuFacade As New AdStudentGroupsFacade
        Dim ds As DataSet = StuFacade.GetAllStudentGroup(StudentId)
        dgrdStudentGroup.DataSource = ds.Tables(0).DefaultView
        dgrdStudentGroup.DataBind()
        If dgrdStudentGroup.Items.Count > 0 Then
            lblNoRecord.Visible = False
        Else
            lblNoRecord.Visible = True
        End If
        ' ClearActualScore()
    End Sub
    Private Sub BindGroupsDLL(ByVal isUserSa As Boolean)
        Dim StuFacade As New AdStudentGroupsFacade
        ''New Code Added By Vijay Ramteke On June 20, 2010
        ''Dim ds As DataSet = StuFacade.GetStudentGroupsList(UserId)
        Dim ds As DataSet = StuFacade.GetStudentGroupsList(UserId, campusId, isUserSa)
        ''New Code Added By Vijay Ramteke On June 20, 2010
        Dim dr As DataRow = ds.Tables(0).NewRow
        dr("StuGrpId") = Guid.Empty.ToString
        dr("Descrip") = "-- Select --"
        ds.Tables(0).Rows.InsertAt(dr, 0)
        ddlGroup.DataValueField = "StuGrpId"
        ddlGroup.DataTextField = "Descrip"
        ddlGroup.DataSource = ds.Tables(0).DefaultView
        ddlGroup.DataBind()
    End Sub
    Private Sub BindAllEnrollments()
        Dim StuFacade As New AdStudentGroupsFacade
        Dim ds As DataSet = StuFacade.GetAllStudentEnrollments(StudentId)
        Dim dr As DataRow = ds.Tables(0).NewRow
        dr("StuEnrollId") = Guid.Empty.ToString
        dr("PrgVerDescrip") = "-- Select --"
        ds.Tables(0).Rows.InsertAt(dr, 0)
        ddlEnrollmentId.DataValueField = "StuEnrollId"
        ddlEnrollmentId.DataTextField = "PrgVerDescrip"
        ddlEnrollmentId.DataSource = ds.Tables(0).DefaultView
        ddlEnrollmentId.DataBind()
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ''BindDataGrid()
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub

    Protected Sub btnAddGroup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddGroup.Click
        If Not ddlGroup.SelectedValue.ToString = Guid.Empty.ToString Or Not ddlEnrollmentId.SelectedValue.ToString = Guid.Empty.ToString Then
            Dim StuFacade As New AdStudentGroupsFacade
            Dim result As String = StuFacade.ValidateStudentGroupStudent(StudentId, ddlGroup.SelectedValue, ddlEnrollmentId.SelectedValue)
            If result = "" Then
                Dim stuGrpInfo As New AdReqGrpInfo
                stuGrpInfo.StudentId = StudentId
                stuGrpInfo.StuGrpId = ddlGroup.SelectedValue
                stuGrpInfo.StuEnrollId = ddlEnrollmentId.SelectedValue
                stuGrpInfo.ModDate = DateTime.Now
                StuFacade.AddStudentGroupStudent(stuGrpInfo, Session("UserName").ToString)
                BindDataGrid()
            Else
                DisplayErrorMessage(result)
            End If
        Else
            If ddlGroup.SelectedValue.ToString = Guid.Empty.ToString Then
                DisplayErrorMessage("Please Select Student Group to Add.")
            ElseIf ddlGroup.SelectedValue.ToString = Guid.Empty.ToString Then
                DisplayErrorMessage("Please Select Student Enrollment to Add the Student Group.")
            End If
        End If

    End Sub
End Class