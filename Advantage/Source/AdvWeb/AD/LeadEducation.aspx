﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="LeadEducation.aspx.vb" Inherits="LeadEducation" %>

<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <script src="../Scripts/Advantage.Client.AD.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" Runat="Server">
   <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
     <%-- Add class ListFrameTop2 to the table below --%>
        <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="listframetop2">
                    <br />
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td>
                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="listframetop">
                                        <div  align="left" >
                                          <asp:Label ID="lblSelectType" runat="server" CssClass="label">Filter By College/School</asp:Label>
                                        </div>
                                          <div  align="left" style="padding-top: 10px;">
                                        
                                            <asp:DropDownList ID="ddlSchoolType" runat="server"  Width="300px" CssClass="dropdownlist"  AutoPostBack="True">
                                                <asp:ListItem Value="College">Show Colleges Only</asp:ListItem>
                                                <asp:ListItem Value="Schools">Show Schools Only</asp:ListItem>
                                            </asp:DropDownList>
                                              </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="listframebottom">
                                            <div class="scrollleftfltr1row">
                                                <asp:DataList ID="dlstEmployerContact" runat="server">
                                                    <SelectedItemStyle CssClass="selecteditem"></SelectedItemStyle>
                                                    <SelectedItemTemplate>
                                                    </SelectedItemTemplate>
                                                    <ItemStyle CssClass="nonselecteditem"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:LinkButton Text='<%# Container.DataItem("CollegeName")%>' runat="server" CssClass="nonselecteditem"
                                                            CommandArgument='<%# Container.DataItem("LeadEducationId")%>' ID="Linkbutton2"
                                                            CausesValidation="False" />
                                                    </ItemTemplate>
                                                </asp:DataList></div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                       
                    </table>
                </td>
            </tr>
        </table>
    </telerik:RadPane>
    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                        ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                                    </asp:Button>
                                    </td>
                            
                        </tr>
                    </table>
        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
            <tr>
                <td class="detailsframe">
                    <div class="scrollright2">
                        <!-- begin table content-->
                        <!-- 4 column template - 9 row, 4 column template with bottom row containing 2 columns -->
                        <!-- used in placement 'employer' section of common tasks-->
                        <table class="contenttable" cellspacing="0" cellpadding="0" width="98%">
                            <tr>
                                <td class="contentcell" style="white-space: normal">
                                    <asp:Label ID="lblEducationInstId" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlEducationInstId" TabIndex="1" runat="server" AutoPostBack="true"
                                        CssClass="dropdownlist">
                                    </asp:DropDownList>
                                </td>
                                <td class="emptycell">
                                </td>
                                <td class="contentcell" style="white-space: normal">
                                    <asp:Label ID="lblGraduatedDate" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <%--<asp:TextBox ID="txtGraduatedDate" TabIndex="4" runat="server" CssClass="textboxdate"></asp:TextBox>&nbsp;<a
                                        onclick="javascript:OpenCalendar('ClsSect','txtGraduatedDate', true, 1945)"><img
                                            id="IMG1" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                            align="absMiddle"></a>--%>
                                     <telerik:RadDatePicker ID="txtGraduatedDate" MinDate="1/1/1945" runat="server">
                                                        </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcell" style="white-space: normal">
                                    <asp:Label ID="lblOther" runat="server" CssClass="label" Enabled="False"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtOther" TabIndex="2" runat="server" CssClass="textbox" Enabled="False"></asp:TextBox>
                                </td>
                                <td class="emptycell">
                                </td>
                                <td class="contentcell" style="white-space: normal">
                                    <asp:Label ID="lblMajor" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtMajor" TabIndex="5" runat="server" CssClass="textbox"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcell" style="white-space: normal">
                                    <asp:Label ID="lblFinalGrade" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtFinalGrade" TabIndex="2" runat="server" CssClass="textbox"></asp:TextBox>
                                </td>
                                <td class="emptycell">
                                </td>
                                <td class="contentcell" style="white-space: normal">
                                </td>
                                <td class="contentcell4">
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcell" style="white-space: normal">
                                    <asp:Label ID="lblCertificateId" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlCertificateId" TabIndex="3" runat="server" CssClass="dropdownlist">
                                    </asp:DropDownList>
                                </td>
                                <td class="emptycell">
                                </td>
                                <td class="contentcell" style="white-space: normal">
                                </td>
                                <td class="contentcell4">
                                </td>
                            </tr>
                     
                            <tr>
                                <td class="contentcell" style="white-space: normal">
                                    <asp:Label ID="lblComments" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4" colspan="4">
                                    <asp:TextBox ID="txtComments" TabIndex="6"  CssClass="tocommentsnowrap" runat="server"
                            MaxLength="300" Columns="60"  Rows="3" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <div class="contentcellheader" style="width:80%;"><asp:Label ID="lblAddress" runat="server" Visible="True" Font-Bold="true" CssClass="label">Address</asp:Label></div>
                        <table class="contenttable" cellspacing="0" cellpadding="0" width="90%">
                            <tr>
                                <td class="spacertables">
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcell" valign="top" nowrap>
                                    <asp:Label ID="lblAddress1" runat="server" CssClass="label" Visible="true">Address1</asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtAddress1" TabIndex="7" runat="server" CssClass="textbox" ReadOnly="True"
                                        Visible="True"></asp:TextBox>
                                </td>
                                <td class="emptycell">
                                </td>
                                <td class="contentcell" style="white-space: normal">
                                    <asp:Label ID="lblStateId" runat="server" CssClass="label" Visible="True">State</asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtStateId" TabIndex="10" runat="server" CssClass="textbox" ReadOnly="True"
                                        Visible="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcell" style="white-space: normal">
                                    <asp:Label ID="lblAddress2" runat="server" CssClass="label" Visible="true">Address2</asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtAddress2" TabIndex="8" runat="server" CssClass="textbox" ReadOnly="True"
                                        Visible="true"></asp:TextBox>
                                </td>
                                <td class="emptycell">
                                </td>
                                <td class="contentcell" style="white-space: normal">
                                    <asp:Label ID="lblZip" runat="server" CssClass="label" Visible="true">Zip</asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtZip" TabIndex="11" runat="server" CssClass="textbox" ReadOnly="True"
                                        Visible="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="contentcell" style="white-space: normal">
                                    <asp:Label ID="lblCity" runat="server" CssClass="label" Visible="True">City</asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtcity" TabIndex="9" runat="server" CssClass="textbox" ReadOnly="True"
                                        Visible="true"></asp:TextBox>
                                </td>
                                <td class="emptycell">
                                </td>
                                <td class="contentcell" style="white-space: normal">
                                </td>
                                <td class="contentcell4">
                                </td>
                            </tr>
                            <asp:TextBox ID="txtPKID" runat="server" Visible="false"></asp:TextBox><asp:TextBox
                                ID="txtStudentId" runat="server" CssClass="label" Visible="false"></asp:TextBox><asp:TextBox
                                    ID="txtLeadID" runat="server" CssClass="label" Visible="false"></asp:TextBox><asp:CheckBox
                                        ID="ChkIsInDB" runat="server" Checked="False" Visible="false"></asp:CheckBox></table>
                        <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="98%">
                                <tr>
                                    <td class="spacertables">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcellheader" nowrap colspan="6">
                                        <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="98%">
                                <tr>
                                    <td class="spacertables">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="98%">
                                        <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" Width="98%" EnableViewState="false">
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <!--end table content-->
                    </div>
                </td>
            </tr>
        </table>
        <asp:textbox id="txtRowIds" style="VISIBILITY: hidden" runat="server" CssClass="donothing"></asp:textbox>
		<asp:textbox id="txtResourceId" style="VISIBILITY: hidden" runat="server" CssClass="donothing"></asp:textbox>
		<asp:TextBox ID="txtLeadEducationId" Runat="server" style="VISIBILITY: hidden" CssClass="donothing"></asp:TextBox>
		<asp:TextBox ID="txtModDate" Runat="server" style="VISIBILITY: hidden" CssClass="donothing"></asp:TextBox>
    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>   
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">


  
</asp:Content>

