﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="AleadExtracurricular.aspx.vb" Inherits="AdvWeb.AD.AdAleadExtracurricular" %>
<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" runat="Server">
    <!-- Use this to put content -->
    <%--<script type="text/x-kendo-template" id="leadExtraGridtemplate">--%>
    <div id="containerExtraCurricular" class="kendo-page">
            <section class="title-normal">
                <label><b>Extracurricular</b></label>
                <a  class="lnkAdd" id="extraPlusImage" href="javascript:void(0);">
                    <span class="k-icon k-i-plus-circle font-green"></span>
                </a>
            </section>
        <%--</script>--%>
        <link href="../css/AD/LeadExtra.css" rel="stylesheet" />
        <div id="extraWrapper">
            <div id="extraMainPanel">
                <div id="extraGrid"></div>
            </div>
        </div>
        </div>

    <script type="text/javascript">
        $(document).ready(function () {
            var leadElement = document.getElementById('leadId');
            var leadId = leadElement.value;

    <%-- ReSharper disable once UnusedLocals --%>
            var manager = new AD.LeadExtraCurricular(leadId);
        });
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

