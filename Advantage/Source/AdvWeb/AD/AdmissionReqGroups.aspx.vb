﻿Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class AdmissionReqGroups
    Inherits BasePage
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Dim campusId, UserId As String
    Protected WithEvents allDataset As System.Data.DataSet
    Protected WithEvents dataGridTable As System.Data.DataTable
    Protected WithEvents dataListTable As System.Data.DataTable
    Protected WithEvents dropDownListTable As System.Data.DataTable

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set the Delete Button so it prompts the user for confirmation when clicked
        'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim ds2 As New DataSet
        '        Dim sStatusId As String
        Dim userId As String = ""
        '   Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        '     Header1.EnableHistoryButton(False)

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        dgrdLeadGroups.Visible = False

        Try
            If Not Page.IsPostBack Then
                ViewState("MODE") = "NEW"
                objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

                '    objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

                'Disable the new and delete buttons
                'objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                Session("ReqGrpId") = Nothing

                '   build dropdownlists
                BuildDropDownLists()

                '   get allDataset from the DB
                'With New AdReqsFacade
                '    allDataset = .GetReqGroupsDS()
                'End With

                ''   save it on the session
                'Session("ReqGrpLeadGrpDS") = allDataset

                BindDataList()


                '   initialize buttons
                InitButtonsForLoad()

                txtReqGrpId.Text = Guid.NewGuid.ToString

            Else
                objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                '    objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                'objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            End If

            '   create Dataset and Tables
            'CreateDatasetAndTables()

            '   the first time we have to bind an empty datagrid
            If Not IsPostBack Then PrepareForNewData()

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    'Private Sub CreateDatasetAndTables()
    '    '   Get dataGrid dataset from the session and create tables
    '    allDataset = Session("ReqGrpLeadGrpDS")
    '    dataListTable = allDataset.Tables("ReqGroups")
    '    dataGridTable = allDataset.Tables("LeadGrpReqGroups")
    '    dropDownListTable = allDataset.Tables("LeadGroups")
    'End Sub
    Private Sub PrepareForNewData()
        '   bind AdReqGrps datalist
        'BindDataList()

        ''   save ReqGrpId in session
        'Session("ReqGrpId") = Nothing

        ''   bind DataGrid
        'BindDataGrid()

        ''   bind an empty new AdReqGrpInfo
        'BindAdReqGrpData(New AdReqGrpInfo)

        ''   initialize buttons
        'InitButtonsForLoad()

        'dgrdLeadGroups.ShowFooter = True
    End Sub

    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String
        Dim status As String = String.Empty
        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = "Descrip"
                status = "Active"
            Case 1
                rowFilter = "Status=0"
                sortExpression = "Descrip"
                status = "Inactive"
            Case Else
                rowFilter = Nothing
                sortExpression = "Status DESC, Descrip "
        End Select

        Dim facade As New LeadFacade

        '   bind dlstAdmissionReqGroups datalist
        UserId = AdvantageSession.UserState.UserId.ToString
        Dim resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        Dim campusId = AdvantageSession.UserState.CampusId.ToString
        Dim facadeCamp As New CampusGroupsFacade
        Dim originalDataSet = facade.GetReqGroupsByStatus(status)

        dlstAdmissionReqGroups.DataSource = facadeCamp.GetCampusGroupsItems(UserId, resourceId, campusId, originalDataSet)
        dlstAdmissionReqGroups.DataBind()

        'If Not Session("ReqGrpId") Is Nothing Then
        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, Session("ReqGrpId"), ViewState, Header1)
        'End If
    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampGrpDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With
    End Sub
    'Private Sub BuildCampGrpDDL()
    '    '   bind the Campus Group DropDownList
    '    Dim facade As New CampusGroupsFacade
    '    With ddlCampGrpId
    '        .DataTextField = "CampGrpDescrip"
    '        .DataValueField = "CampGrpId"
    '        .DataSource = facade.GetAllCampusGroups()
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", System.Guid.Empty.ToString))
    '        .SelectedIndex = 0
    '    End With
    'End Sub


    Private Sub BuildCampGrpDDL()
        '   bind the Campus Group DropDownList
        Dim resourceId As Integer
        UserId = AdvantageSession.UserState.UserId.ToString
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        Dim facade As New CampusGroupsFacade
        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = facade.GetAllIndividualCampusGroupsByUserAndResourceID(UserId, resourceId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", System.Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub GetReqGrpsDS()
        '   get allDataset from the DB
        'With New AdReqsFacade
        '    allDataset = .GetReqGroupsDS()
        'End With

        ''   save it on the session
        'Session("ReqGrpLeadGrpDS") = allDataset

        '   create Dataset and Tables
        ' CreateDatasetAndTables()
    End Sub
    Private Sub GetRequirementInfo(ByVal ReqGrpId As String)
        Dim reqInfo As New LeadFacade
        BindRequirements(reqInfo.GetReqGroupInfo(ReqGrpId))
    End Sub
    Private Sub BindRequirements(ByVal ReqGrpInfo As AdReqGrpInfo)
        'Bind The StudentInfo Data From The Database
        With ReqGrpInfo
            txtCode.Text = .Code
            txtDescrip.Text = .Descrip
            ddlStatusId.SelectedValue = .StatusId
            ddlCampGrpId.SelectedValue = .CampGrpId
            chkIsMandatoryReqGrp.Checked = .IsMandatoryReqGrp
            txtModDate.Text = .ModDate
            txtModUser.Text = .ModUser
        End With
    End Sub
    Private Sub dlstAdmissionReqGroups_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstAdmissionReqGroups.ItemCommand

        GetRequirementInfo(e.CommandArgument)
        chkIsInDB.Checked = True
        txtReqGrpId.Text = e.CommandArgument
        lnkReqGrpIdDef.Visible = True
        lnkReqGrpIdDef.Enabled = True

        '   this portion of code added to refresh data from the database
        'GetReqGrpsDS()

        ''   save AdReqGrpId in session
        'Dim strId As String = dlstAdmissionReqGroups.DataKeys(e.Item.ItemIndex).ToString()
        'Session("ReqGrpId") = strId

        ''   get the row with the data to be displayed
        'Dim row() As DataRow = dataListTable.Select("ReqGrpId=" + "'" + CType(Session("ReqGrpId"), String) + "'")

        ''   populate controls with row data
        'txtReqGrpId.Text = CType(row(0)("ReqGrpId"), Guid).ToString
        'txtCode.Text = row(0)("Code")
        'txtDescrip.Text = row(0)("Descrip")
        'ddlStatusId.SelectedValue = CType(row(0)("StatusId"), Guid).ToString
        'ddlCampGrpId.SelectedValue = CType(row(0)("CampGrpId"), Guid).ToString
        'chkIsMandatoryReqGrp.Checked = row(0)("IsMandatoryReqGrp")
        'txtModUser.Text = row(0)("ModUser")
        'txtModDate.Text = row(0)("ModDate").ToString

        ''   bind DataGrid
        'dgrdLeadGroups.EditItemIndex = -1
        'BindDataGrid()

        ''   show footer
        'dgrdLeadGroups.ShowFooter = True

        ''   disable ReqGrpLeadGrp link button
        'dgrdLeadGroups.Enabled = True

        '   initialize buttons
        InitButtonsForEdit()

        '   set Style to Selected Item

        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, e.CommandArgument)
    End Sub
    Private Sub BindAdReqGrpData(ByVal AdReqGrp As AdReqGrpInfo)
        With AdReqGrp
            chkIsInDB.Checked = .IsInDB
            txtReqGrpId.Text = .ReqGrpId
            txtCode.Text = .Code
            txtDescrip.Text = .Descrip
            ddlStatusId.SelectedValue = .StatusId
            ddlCampGrpId.SelectedValue = .CampGrpId
            chkIsMandatoryReqGrp.Checked = .IsMandatoryReqGrp
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim facade As New LeadFacade
        Dim result As String
        Dim validateMandatory As New AdReqsFacade
        Dim validateMandatoryMessage As String

        'Validate MandatoryRequirement Group
        If chkIsMandatoryReqGrp.Checked = True Then
            validateMandatoryMessage = validateMandatory.ValidateMandatoryReqGroup(txtReqGrpId.Text)
            If Not validateMandatoryMessage = "" Then
                DisplayErrorMessage(validateMandatoryMessage)
                Exit Sub
            End If
        End If

        'Validate Insert/Update
        result = facade.AddReqGrp(BuildRequirementGroups(), AdvantageSession.UserState.UserName)
        If Not result = "" Then
            DisplayErrorMessage(result)
            Exit Sub
        End If

        chkIsInDB.Checked = True

        BindDataList()

        'Call Update Function in the plEmployerInfoFacade


        ''   if any of the footer fields of the datagrid is not blank.. send an error message 
        'If Not AreFooterFieldsBlank() Then
        '    '   Display Error Message
        '    DisplayErrorMessage("To use ""Save"" all fields in the Lead Groups footer must be blank")
        '    Exit Sub
        'End If

        ''   if AdReqGrpId does not exist do an insert else do an update
        'If Session("ReqGrpId") Is Nothing Then
        '    '   do an insert
        '    BuildNewRowInDataList(Guid.NewGuid.ToString)

        'Else
        '    '   do an update
        '    '   get the row with the data to be displayed
        '    Dim row() As DataRow = dataListTable.Select("ReqGrpId=" + "'" + CType(Session("ReqGrpId"), String) + "'")

        '    '   update row data
        '    UpdateRowData(row(0))

        'End If

        ''   try to update the DB and show any error message
        'UpdateDB()

        ''   if there were no errors then bind and set selected style to the datalist and initialize buttons 
        'If Customvalidator1.IsValid Then

        '    '   bind datalist
        '    BindDataList()

        '    '   initialize buttons
        '    InitButtonsForEdit()
        'End If
        InitButtonsForEdit()
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, txtReqGrpId.Text)
    End Sub

    Private Function BuildAdReqGrpInfo(ByVal AdReqGrpId As String) As AdReqGrpInfo
        'instantiate class
        Dim AdReqGrpInfo As New AdReqGrpInfo

        With AdReqGrpInfo
            'IsInDB
            .IsInDB = chkIsInDB.Checked

            'ReqGrpId
            .ReqGrpId = txtReqGrpId.Text

            'Code
            .Code = txtCode.Text

            'Descrip
            .Descrip = txtDescrip.Text

            'StatusId
            .StatusId = ddlStatusId.SelectedValue

            'CampGrpId
            .CampGrpId = ddlCampGrpId.SelectedValue

            'ModUser
            .ModUser = txtModUser.Text

            'ModDate
            .ModDate = Date.Parse(txtModDate.Text)
        End With

        'return data
        Return AdReqGrpInfo
    End Function

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        txtReqGrpId.Text = Guid.NewGuid.ToString
        txtCode.Text = ""
        txtDescrip.Text = ""
        ddlStatusId.SelectedIndex = 0
        ddlCampGrpId.SelectedIndex = 0
        txtModDate.Text = ""
        txtModUser.Text = ""
        chkIsMandatoryReqGrp.Checked = False
        chkIsInDB.Checked = False



        '   reset dataset to previous state. delete all changes
        'allDataset.RejectChanges()
        'PrepareForNewData()
        'txtReqGrpId.Text = Guid.NewGuid.ToString

        '   initialize buttons
        InitButtonsForLoad()

        '   reset Style to Selected Item
        ' CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, Guid.Empty.ToString, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim facade As New LeadEntranceFacade
        Dim result As String
        result = facade.DeleteRequirementGroup(txtReqGrpId.Text)
        If Not result = "" Then
            DisplayErrorMessage("​Unable to delete Requirement Group. There are active requirement group definitions in use for this group.")
            Exit Sub
        End If
        BindDataList()

        '   get all rows to be deleted
        'Dim rows() As DataRow = dataGridTable.Select("ReqGrpId=" + "'" + CType(Session("ReqGrpId"), String) + "'")

        ''   delete all rows from dataGrid table
        'Dim i As Integer
        'If rows.Length > 0 Then
        '    For i = 0 To rows.Length - 1
        '        rows(i).Delete()
        '    Next
        'End If

        ''   get the row to be deleted in ReqGrpId table
        'Dim row() As DataRow = dataListTable.Select("ReqGrpId=" + "'" + CType(Session("ReqGrpId"), String) + "'")

        ''   delete row from the table
        'row(0).Delete()

        ''   try to update the DB and show any error message
        'UpdateDB()

        ''   if there were no errors prepare screen for new data
        'If Customvalidator1.IsValid Then
        '    '   Prepare screen for new data
        '    PrepareForNewData()
        'End If

        '   initialize buttons
        InitButtonsForLoad()
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
            dgrdLeadGroups.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
        lnkReqGrpIdDef.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
            dgrdLeadGroups.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        lnkReqGrpIdDef.Enabled = True
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub
    Private Function AreFooterFieldsBlank() As Boolean
        Dim footer As Control = dgrdLeadGroups.Controls(0).Controls(dgrdLeadGroups.Controls(0).Controls.Count - 1)
        If Not CType(footer.FindControl("ddlFooterLeadGrpId"), DropDownList).SelectedValue = System.Guid.Empty.ToString And
                Not CType(footer.FindControl("txtFooterNumReqs"), TextBox).Text = "" Then
            Return False
        End If
        'If Not CType(footer.FindControl("ddlFooterLeadGrpId"), DropDownList).SelectedValue = System.Guid.Empty.ToString Then Return False
        'If Not CType(footer.FindControl("txtFooterNumReqs"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Sub BuildNewRowInDataList(ByVal theGuid As String)
        '   get a new row
        'Dim newRow As DataRow = dataListTable.NewRow

        ''   create a Guid for DataList if it has not been created 
        'Session("ReqGrpId") = theGuid
        'txtReqGrpId.Text = theGuid
        'newRow("ReqGrpId") = New Guid(theGuid)

        ''   update row with web controls data
        'UpdateRowData(newRow)

        ''   add row to the table
        'newRow.Table.Rows.Add(newRow)
    End Sub
    Private Sub UpdateRowData(ByVal row As DataRow)
        '   update row data
        'row("Code") = txtCode.Text
        'row("Descrip") = txtDescrip.Text
        'row("StatusId") = New Guid(ddlStatusId.SelectedValue)
        'row("CampGrpId") = New Guid(ddlCampGrpId.SelectedValue)
        'row("IsMandatoryReqGrp") = chkIsMandatoryReqGrp.Checked
        'row("ModUser") = Session("UserName")
        'row("ModDate") = Date.Parse(Date.Now.ToString)
    End Sub

    Private Sub UpdateDB()
        'Dim result As String

        ''   Try update DB
        'With New AdReqsFacade
        '    If chkIsMandatoryReqGrp.Checked Then
        '        '   Validate that there is only ONE requirement group marked as Mandatory
        'res'ult = .ValidateMandatoryReqGroup(txtReqGrpId.Text)
        '    End If
        '    If Not result = "" Then
        '        '   Display Error Message
        '        DisplayErrorMessage(result)

        '    Else
        '        '   Proceed to update Requirement Group
        '        result = .UpdateReqGroupsDS(allDataset)
        '        If Not result = "" Then
        '            '   Display Error Message
        '            DisplayErrorMessage(result)

        '        Else
        '            '   Delete requirement group definition
        '            result = .DeleteReqGrpDef(txtReqGrpId.Text)
        '            If Not result = "" Then
        '                '   Display Error Message
        '                DisplayErrorMessage(result)
        '            End If

        '            '   Get a new version of AdReqGrps dataset because some other users may have added,
        '            '   updated or deleted records from the DB
        '            allDataset = .GetReqGroupsDS()
        '            Session("ReqGrpLeadGrpDS") = allDataset
        '            CreateDatasetAndTables()
        '        End If
        '    End If
        'End With
    End Sub

    Private Sub dgrdLeadGroups_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdLeadGroups.ItemCommand
        '   process postbacks from the datagrid
        'Select Case e.CommandName

        '    '   user hit "Edit"
        'Case "Edit"
        '        '   edit selected item
        '        dgrdLeadGroups.EditItemIndex = e.Item.ItemIndex

        '        '    do not show footer
        '        dgrdLeadGroups.ShowFooter = False


        '        '   user hit "Update" inside the datagrid
        '    Case "Update"
        '        '   process only if the edit textboxes are nonblank and valid
        '        If IsDataInEditTextboxesValid(e) And Not AreEditFieldsBlank(e) Then
        '            '   get the row to be updated
        '            Dim row() As DataRow = dataGridTable.Select("LeadGrpReqGrpId=" + "'" + New Guid(CType(e.Item.FindControl("lblEditLeadGrpReqGrpId"), Label).Text).ToString + "'")
        '            '   fill new values in the row
        '            If Not CType(e.Item.FindControl("ddlEditLeadGrpId"), DropDownList).SelectedValue = System.Guid.Empty.ToString Then row(0)("LeadGrpId") = New Guid(CType(e.Item.FindControl("ddlEditLeadGrpId"), DropDownList).SelectedValue)
        '            If Not CType(e.Item.FindControl("txtEditNumReqs"), TextBox).Text = "" Then row(0)("NumReqs") = Integer.Parse(CType(e.Item.FindControl("txtEditNumReqs"), TextBox).Text)
        '            row(0)("ModUser") = Session("UserName")
        '            row(0)("ModDate") = Date.Parse(Date.Now.ToString)

        '            '   no record is selected
        '            dgrdLeadGroups.EditItemIndex = -1

        '            '   show footer
        '            dgrdLeadGroups.ShowFooter = True

        '            '   Bind DataList
        '            BindDataList()
        '        Else
        '            DisplayErrorMessage("Invalid data during the edit of an existing row of Lead Groups")
        '            Exit Sub
        '        End If


        '        '   user hit "Cancel"
        '    Case "Cancel"
        '        '   set no record selected
        '        dgrdLeadGroups.EditItemIndex = -1

        '        '   show footer
        '        dgrdLeadGroups.ShowFooter = True


        '        '   user hit "Delete" inside the datagrid
        '    Case "Delete"
        '        '   get the row to be deleted
        '        Dim row() As DataRow = dataGridTable.Select("LeadGrpReqGrpId=" + "'" + CType(e.Item.FindControl("lblEditLeadGrpReqGrpId"), Label).Text + "'")

        '        '   delete row 
        '        row(0).Delete()

        '        '   set no record selected
        '        dgrdLeadGroups.EditItemIndex = -1

        '        '   show footer
        '        dgrdLeadGroups.ShowFooter = True


        '    Case "AddNewRow"
        '        '   process only if the footer textboxes are nonblank and valid
        '        If IsDataInFooterTextboxesValid(e) And Not AreFooterFieldsBlank() Then
        '            '   If the dataList item doesn't have an Id assigned.. create one and assign it.
        '            If Session("ReqGrpId") Is Nothing Then
        '                '   build a new row in dataList table
        '                BuildNewRowInDataList(Guid.NewGuid.ToString)
        '                '   bind datalist
        '                BindDataList()
        '            End If

        '            '   get a new row from the dataGridTable
        '            Dim newRow As DataRow = dataGridTable.NewRow

        '            '   fill the new row with values
        '            newRow("LeadGrpReqGrpId") = Guid.NewGuid
        '            newRow("ReqGrpId") = New Guid(CType(Session("ReqGrpId"), String))
        '            If Not CType(e.Item.FindControl("ddlFooterLeadGrpId"), DropDownList).SelectedValue = Guid.Empty.ToString Then newRow("LeadGrpId") = New Guid(CType(e.Item.FindControl("ddlFooterLeadGrpId"), DropDownList).SelectedValue)
        '            If Not CType(e.Item.FindControl("txtFooterNumReqs"), TextBox).Text = "" Then newRow("NumReqs") = Integer.Parse(CType(e.Item.FindControl("txtFooterNumReqs"), TextBox).Text)
        '            newRow("ModUser") = Session("UserName")
        '            newRow("ModDate") = Date.Parse(Date.Now.ToString)

        '            '   add row to the table
        '            newRow.Table.Rows.Add(newRow)

        '        Else
        '            DisplayErrorMessage("Invalid data during adding of a new row of Lead Groups")
        '            Exit Sub
        '        End If
        'End Select

        ''   Bind DataGrid
        'BindDataGrid()
    End Sub

    Private Sub BindDataGrid()
        '   bind datagrid to dataGridTable
        'dgrdLeadGroups.DataSource = New DataView(dataGridTable, "ReqGrpId=" + "'" + CType(Session("ReqGrpId"), String) + "'", "LeadGrpDescrip ASC", DataViewRowState.CurrentRows)
        'dgrdLeadGroups.DataBind()
    End Sub

    'Private Function AreEditFieldsBlank(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
    '    'If Not CType(e.Item.FindControl("ddlEditLeadGrpId"), DropDownList).SelectedValue = System.Guid.Empty.ToString And _
    '    '        Not CType(e.Item.FindControl("txtEditNumReqs"), TextBox).Text = "" Then
    '    '    Return False
    '    'End If
    '    ''If Not CType(e.Item.FindControl("ddlEditLeadGrpId"), DropDownList).SelectedValue = System.Guid.Empty.ToString Then Return False
    '    ''If Not CType(e.Item.FindControl("txtEditNumReqs"), TextBox).Text = "" Then Return False
    '    'Return True
    'End Function

    'Private Function IsDataInFooterTextboxesValid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
    '    'If Not IsTextBoxValidInteger(e.Item.FindControl("txtFooterNumReqs")) Then Return False
    '    'Return True
    'End Function

    'Private Function IsDataInEditTextboxesValid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
    '    'If Not IsTextBoxValidInteger(e.Item.FindControl("txtEditNumReqs")) Then Return False
    '    'Return True
    'End Function

    'Private Function IsTextBoxValidInteger(ByVal textbox As TextBox) As Boolean
    '    'If textbox.Text = "" Then Return True
    '    'Try
    '    '    Dim d As Integer = Integer.Parse(textbox.Text)
    '    '    Return True
    '    'Catch ex As System.Exception
    '    '    Return False
    '    'End Try
    'End Function

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        '   if there are changes to the dataset we must confirm that the user is aware that all changes will be lost.
        'If allDataset.HasChanges() Then
        '    'Set the Delete Button so it prompts the user for confirmation when clicked
        '    btnNew.Attributes.Add("onclick", "if(confirm('All changes made to this record will be lost.  Are you sure you want a New record?')){}else{return false}")
        'Else
        '    btnNew.Attributes.Remove("onclick")
        'End If

        ''   save dataset systems in session
        'Session("ReqGrpLeadGrpDS") = allDataset

        ''   disable Lead Groups datagrid when Req Grp is mandatory
        'If chkIsMandatoryReqGrp.Checked Then
        '    dgrdLeadGroups.Enabled = False
        '    dgrdLeadGroups.ShowFooter = False
        'Else
        '    dgrdLeadGroups.Enabled = True
        'End If
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(lnkReqGrpIdDef)
        'Add javascript code to warn the user about non saved changes 
        '        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub

    Private Sub dgrdLeadGroups_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdLeadGroups.ItemDataBound

        '   process only rows in the items sections
        Select Case e.Item.ItemType
            Case WebControls.ListItemType.EditItem
                '   bind LeadGrpId dropdownlist
                Dim ddlEditLeadGrpId As DropDownList = CType(e.Item.FindControl("ddlEditLeadGrpId"), DropDownList)
                With ddlEditLeadGrpId
                    .DataTextField = "LeadGrpDescrip"
                    .DataValueField = "LeadGrpId"
                    .DataSource = New DataView(dropDownListTable, "", "LeadGrpDescrip ASC ", DataViewRowState.CurrentRows)
                    .DataBind()
                    .SelectedValue = CType(e.Item.FindControl("lblEditLeadGrpId"), Label).Text
                End With

            Case WebControls.ListItemType.Footer
                '   bind LeadGrpId dropdownlist
                Dim ddlFooterLeadGrpId As DropDownList = CType(e.Item.FindControl("ddlFooterLeadGrpId"), DropDownList)
                With ddlFooterLeadGrpId
                    .DataTextField = "LeadGrpDescrip"
                    .DataValueField = "LeadGrpId"
                    .DataSource = New DataView(dropDownListTable, "", "LeadGrpDescrip ASC ", DataViewRowState.CurrentRows)
                    .DataBind()
                    .Items.Insert(0, New ListItem("Select", System.Guid.Empty.ToString))
                    .SelectedIndex = 0
                End With
        End Select
    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged

        txtReqGrpId.Text = Guid.NewGuid.ToString
        txtCode.Text = ""
        txtDescrip.Text = ""
        ddlStatusId.SelectedIndex = 0
        ddlCampGrpId.SelectedIndex = 0
        txtModDate.Text = ""
        txtModUser.Text = ""
        chkIsMandatoryReqGrp.Checked = False
        chkIsInDB.Checked = False
        '   Bind datalist
        BindDataList()
        InitButtonsForLoad()
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
    End Sub

    Private Sub lnkReqGrpIdDef_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkReqGrpIdDef.Click
        If Page.IsValid Then

            '   Dim dtChanges As DataTable = dataGridTable.GetChanges(DataRowState.Added Or DataRowState.Modified Or DataRowState.Deleted)

            '  If IsNothing(dtChanges) Then
            Dim sb As New System.Text.StringBuilder

            '   append ReqGrpId to the querystring
            sb.Append("&ReqGrpId=" + txtReqGrpId.Text)
            '   append ReqGrpId Description to the querystring
            sb.Append("&ReqGrpDescrip=" + txtDescrip.Text)
            '   append IsMandatoryReqGrp to the querystring
            sb.Append("&IsMandatoryReqGrp=" + chkIsMandatoryReqGrp.Checked.ToString)

            '   setup the properties of the new window
            Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsMedium
            'Dim name As String = "RequirementGrpDefs"
            Dim name As String = "AddReqsToReqGroups"
            Dim url As String = "../AD/" + name + ".aspx?resid=341&mod=AD&cmpid=" + campusId + sb.ToString

            '   open new window and pass parameters
            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)

            'Else
            '    If dtChanges.Rows.Count > 0 Then
            '        DisplayErrorMessage("Please save the changes just made to the Lead Groups before proceeding to the Requirement Group Definition.")
            '    End If
        End If
        ' End If
    End Sub

    Private Sub chkIsMandatoryReqGrp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIsMandatoryReqGrp.CheckedChanged
        'If chkIsMandatoryReqGrp.Checked Then
        '    Dim arrRows() As DataRow
        '    arrRows = dataGridTable.Select("ReqGrpId=" + "'" + CType(Session("ReqGrpId"), String) + "'")
        '    If arrRows.GetLength(0) > 0 Then
        '        For Each row As DataRow In arrRows
        '            If row.RowState <> DataRowState.Added Then
        '                row.Delete()
        '            Else
        '                dataGridTable.Rows.Remove(row)
        '            End If
        '        Next
        '        '   bind datagrid, so no rows are shown.
        '        BindDataGrid()
        '    End If
        'Else
        '    '   show datagrid footer
        '    dgrdLeadGroups.ShowFooter = True
        'End If
    End Sub
    Private Function BuildRequirementGroups() As AdReqGrpInfo
        Dim ReqGrp As New AdReqGrpInfo
        With ReqGrp
            .IsInDB = chkIsInDB.Checked
            .Code = txtCode.Text
            .Descrip = txtDescrip.Text
            .StatusId = ddlStatusId.SelectedValue
            .CampGrpId = ddlCampGrpId.SelectedValue
            .ReqGrpId = txtReqGrpId.Text
            If chkIsMandatoryReqGrp.Checked Then
                .IsMandatoryReqGrp = True
            Else
                .IsMandatoryReqGrp = False
            End If
            .ModDate = Date.Now
        End With
        Return ReqGrp
    End Function
    Public Function CheckIfMandatory(ByVal MandatoryReq As Integer) As String
        If MandatoryReq = 1 Then
            Return " *"
        Else
            Return ""
        End If
    End Function
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Overloads Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            Dim ctrlpanel As Panel = TryCast(ctl, Panel)
            If (ctrlpanel IsNot Nothing) Then
                BindToolTipForControlsInsideaPanel(ctrlpanel)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(CType(ctl, DataGrid))
            End If
        Next
    End Sub
    Public Overloads Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Overloads Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub
End Class
