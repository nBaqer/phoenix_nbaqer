
Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections

Partial Class OverRideAdmissionReqs
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents LblLeadStatus As Label
    Protected WithEvents DDLLeadStatus As DropDownList


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    ' Dim intCount As Integer
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Dim campusId, userId As String
    ' Dim LeadId As String
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        '        Dim m_Context As HttpContext
        'Put user code to initialize the page here
        Dim mContext As HttpContext
        mContext = HttpContext.Current

        Dim advantageUserState As User = AdvantageSession.UserState

        ResourceId = HttpContext.Current.Request.Params("resid")
        campusId = advantageUserState.CampusId.ToString
        userId = advantageUserState.UserId.ToString
        moduleId = HttpContext.Current.Request.Params("Mod").ToString
        Session("UserId") = userId.ToString
        ' m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        'Check if this page still exists in the menu while switching campus
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            BuildLeads()
            BuildStatusDDL()
            If Not txtLeadId.Text = "" Then
                BindDataGrid(txtLeadId.Text)
            End If
            'Call InitButtonsForLoad
            InitButtonsForLoad()
        Else
            'Call the procedure for edit
            InitButtonsForEdit()
        End If

    End Sub
    'Private Sub ClearDropDown(ByVal pnl As Panel)
    '    Dim ctl As Control
    '    For Each ctl In pnl.Controls
    '        If ctl.GetType Is GetType(DropDownList) Then
    '            CType(ctl, DropDownList).SelectedIndex = 0
    '        End If
    '    Next
    'End Sub
    Private Sub BuildLeads()
        Dim leadStatus As New LeadFacade
        'Dim userId As String
        ' userId = userId - NZ 11/19 assignment has no effect
        With ddlLeadId
            .DataTextField = "fullname"
            .DataValueField = "LeadId"
            .DataSource = leadStatus.GetAllUnEnrolledLeadsByCampus("", campusId, userId, txtFirstName.Text, txtLastName.Text, "", ddlLeadStatusId.SelectedValue) ' LeadName.GetAllUnEnrolledLeadsByCampus("", CampusId, userId, "", "", "", "") 'LeadName.GetAllFailedLeadNames(CampusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    'Private Sub GenerateControlsScoreByLead(ByVal pnl As Panel)
    '    Dim getDB As New LeadEntranceFacade
    '    Dim intTestCount As Integer = getDB.GetRequiredRequirements(txtLeadId.Text)
    '    Session("CountVal") = intTestCount
    '    Dim ds As New DataSet
    '    Dim dynLabel As Label
    '    'Dim dynText As TextBox
    '    'Dim dynCheckRequired As CheckBox
    '    'Dim dynCheckPass As CheckBox
    '    'Dim dynTextTaken As TextBox
    '    'Dim dynTextActual As TextBox
    '    'Dim dynLabelMinScore As Label
    '    'Dim dynLabelMaxScore As Label
    '    'Dim dynTextComments As TextBox
    '    'Dim dynReqValidator As RequiredFieldValidator
    '    Dim x As Integer
    '    Dim dynRegularExpValidator As New RegularExpressionValidator
    '    'Dim dynNumeric As eWorld.UI.NumericBox
    '    'Dim dynReqValidator1 As RequiredFieldValidator
    '    'Dim dynOverRide As CheckBox
    '    Dim dynDDL As DropDownList

    '    ds = getDB.GetOverRideScores(txtLeadId.Text)
    '    pnl.Controls.Clear()
    '    Try
    '        pnl.Controls.Add(New LiteralControl("<br><table class=""contenttable"" cellspacing=0 cellpadding=0 width=80% align=center border=0 >"))
    '        'Create and Load Controls ie Textbox,Label,RangeValidator Control
    '        'and set the properties for controls and finally Load Controls

    '        If intTestCount >= 1 Then
    '            'pnl.Controls.Add(New LiteralControl("<tr height=2><td></td></tr>"))

    '            For x = 0 To intTestCount - 1
    '                'Create Labels For Test
    '                dynLabel = New Label
    '                Try
    '                    With dynLabel
    '                        .ID = ds.Tables(0).Rows(x)("EntrTestId").ToString()
    '                        .Text = ds.Tables(0).Rows(x)("EntrTestDescrip").ToString()
    '                        .CssClass = "checkboxstyle"
    '                    End With
    '                    pnlControls.Visible = True
    '                Catch ex As System.Exception
     '                	Dim exTracker = new AdvApplicationInsightsInitializer()
    '                	exTracker.TrackExceptionWrapper(ex)

    '                End Try


    '                dynDDL = New DropDownList
    '                With dynDDL
    '                    .ID = "dynDDL" & x
    '                    .CssClass = "dropdownlist"
    '                    .Style("Width") = "100px"
    '                    .Style("Align") = "center"
    '                    .Items.Insert(0, New ListItem("Select", ""))
    '                    .Items.Insert(1, New ListItem("Yes", "Yes"))
    '                    .Items.Insert(2, New ListItem("No", "No"))
    '                    If ds.Tables(0).Rows(x)("OverRide").ToString() = "Yes" Then
    '                        .SelectedIndex = 1
    '                    ElseIf ds.Tables(0).Rows(x)("OverRide").ToString() = "No" Then
    '                        .SelectedIndex = 2
    '                    Else
    '                        .SelectedIndex = 0
    '                    End If
    '                End With

    '                pnl.Controls.Add(New LiteralControl("<tr><td width=""60%"" nowrap cssclass=""datagridheader3"">"))
    '                pnl.Controls.Add(dynLabel)
    '                pnl.Controls.Add(New LiteralControl("</td><td width=""20%"" nowrap cssclass=""datagridheader2"">"))
    '                pnl.Controls.Add(dynDDL)
    '                pnl.Controls.Add(New LiteralControl("</td></tr>"))
    '                pnl.Controls.Add(New LiteralControl("<p></p>"))
    '            Next
    '            pnl.Controls.Add(New LiteralControl("</table>"))
    '        End If
    '    Finally
    '    End Try
    'End Sub
    'Private Sub GenerateEditControlsScoreByLead(ByVal pnl As Panel)
    '    Dim getDB As New LeadEntranceFacade
    '    Dim intTestCount As Integer = getDB.GetRequiredRequirements(txtLeadId.Text)  'getDB.GetRequiredTestCount(txtLeadId.text)
    '    Session("CountVal") = intTestCount
    '    Dim ds As New DataSet
    '    Dim dynLabel As Label
    '    'Dim dynText As TextBox
    '    'Dim dynCheckRequired As CheckBox
    '    'Dim dynCheckPass As CheckBox
    '    'Dim dynTextTaken As TextBox
    '    'Dim dynTextActual As TextBox
    '    'Dim dynLabelMinScore As Label
    '    'Dim dynLabelMaxScore As Label
    '    'Dim dynTextComments As TextBox
    '    'Dim dynReqValidator As RequiredFieldValidator
    '    Dim x As Integer
    '    Dim dynRegularExpValidator As New RegularExpressionValidator
    '    'Dim dynNumeric As eWorld.UI.NumericBox
    '    'Dim dynReqValidator1 As RequiredFieldValidator
    '    'Dim dynOverRide As CheckBox
    '    Dim dynDDL As DropDownList

    '    ds = getDB.GetOverRideScores(txtLeadId.Text) 'getDB.GetScores(txtLeadId.text)
    '    pnl.Controls.Clear()
    '    Try
    '        pnl.Controls.Add(New LiteralControl("<br><table class=""contenttable"" cellspacing=0 cellpadding=0 width=80% align=center border=0 >"))
    '        'Create and Load Controls ie Textbox,Label,RangeValidator Control
    '        'and set the properties for controls and finally Load Controls
    '        If intTestCount >= 1 Then
    '            'pnl.Controls.Add(New LiteralControl("<tr height=2><td></td></tr>"))

    '            For x = 0 To intTestCount - 1

    '                'Create Labels For Test
    '                dynLabel = New Label
    '                Try
    '                    With dynLabel
    '                        .ID = ds.Tables(0).Rows(x)("EntrTestId").ToString()
    '                        .Text = ds.Tables(0).Rows(x)("EntrTestDescrip").ToString()
    '                        .CssClass = "checkboxstyle"
    '                    End With
    '                Catch ex As System.Exception
     '                	Dim exTracker = new AdvApplicationInsightsInitializer()
    '                	exTracker.TrackExceptionWrapper(ex)

    '                End Try


    '                dynDDL = New DropDownList
    '                'dynDDL.Items.Clear()
    '                With dynDDL
    '                    .ID = "dynDDL" & x
    '                    .CssClass = "checkboxstyle"
    '                    .Style("Width") = "100px"
    '                    .Style("Align") = "center"
    '                    .Items.Insert(0, New ListItem("Select", ""))
    '                    .Items.Insert(1, New ListItem("Yes", "Yes"))
    '                    .Items.Insert(2, New ListItem("No", "No"))
    '                    If Trim(ds.Tables(0).Rows(x)("OverRide").ToString()) = "Yes" Then
    '                        .SelectedIndex = 1
    '                    ElseIf Trim(ds.Tables(0).Rows(x)("OverRide").ToString()) = "No" Then
    '                        .SelectedIndex = 2
    '                    Else
    '                        .SelectedIndex = 0
    '                    End If
    '                End With

    '                pnl.Controls.Add(New LiteralControl("<tr><td width=""60%"" nowrap cssclass=""datagridheader3"">"))
    '                pnl.Controls.Add(dynLabel)
    '                pnl.Controls.Add(New LiteralControl("</td><td width=""20%"" nowrap cssclass=""datagridheader2"">"))
    '                pnl.Controls.Add(dynDDL)
    '                pnl.Controls.Add(New LiteralControl("</td></tr>"))
    '                pnl.Controls.Add(New LiteralControl("<p></p>"))
    '            Next
    '            pnl.Controls.Add(New LiteralControl("</table>"))
    '        End If
    '    Finally
    '    End Try
    'End Sub
    Private Sub ddlLeadId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLeadId.SelectedIndexChanged
        If Not ddlLeadId.SelectedValue = "" Then
            BuildLeads(ddlLeadId.SelectedValue)
        End If
        'If Not LeadId = Guid.Empty.ToString Then
        '    ClearDropDown(pnlControls)
        '    If intCount >= 1 Then
        '        GenerateEditControlsScoreByLead(pnlControls)
        '    Else
        '        GenerateControlsScoreByLead(pnlControls)
        '    End If
        'End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsave.Click

        If txtLeadId.Text = "" Then Exit Sub
        'Dim TestLabel As Array
        'Dim OverRide As Array

        'TestLabel = GetAllTestLabels(pnlControls)
        'OverRide = GetAllOverRide(pnlControls)

        'Dim InsertData As New LeadEntranceFacade
        'Dim strResult As Integer = InsertData.EntranceTestOverRide(txtLeadId.Text, TestLabel, OverRide, Session("UserName"))
        ''If strResult = 0 Then
        ''    GenerateEditControlsScoreByLead(pnlControls)
        ''End If
        'Dim LeadName As New LeadEntranceFacade
        'Dim intCount As Integer = LeadName.GetOverRideCount(txtLeadId.Text)
        'If intCount >= 1 Then
        '    GenerateEditControlsScoreByLead(pnlControls)
        'End If
        SetUpEntranceTest()
        BindDataGrid(txtLeadId.Text)

    End Sub
    Public Function GetAllTestLabels(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        Dim getDB As New LeadEntranceFacade
        'Dim intTestCount As Integer = getDB.GetArrayCount(txtLeadId.text)
        'Dim intTestCount As Integer = getDB.GetRequiredTestCount(txtLeadId.text)
        Dim intTestCount As Integer = getDB.GetRequiredRequirements(txtLeadId.Text)
        Dim testLabel() As String = Array.CreateInstance(GetType(String), intTestCount)
        i = 0
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(Label) Then
                testLabel.SetValue(CType(ctl, Label).ID, i)
                i += 1
            End If
        Next
        Return testLabel
    End Function

    Public Function GetAllOverRide(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        Dim getDB As New LeadEntranceFacade
        'Dim intTestCount As Integer = getDB.GetArrayCount(txtLeadId.text)
        Dim intTestCount As Integer = getDB.GetRequiredRequirements(txtLeadId.Text)  'getDB.GetRequiredTestCount(txtLeadId.text)
        Dim testRequired() As String = Array.CreateInstance(GetType(String), intTestCount)
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(DropDownList) Then
                If CType(ctl, DropDownList).SelectedIndex = 1 Then
                    testRequired.SetValue("Yes", i)
                Else
                    testRequired.SetValue("No", i)
                End If
                i += 1
            End If
        Next
        Return testRequired
    End Function
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSearch.Click
        If Not txtFirstName.Text = "" Or Not txtLastName.Text = "" Or Not ddlLeadId.SelectedValue = "" Or Not txtSSN.Text = "" Or Not ddlLeadStatusId.SelectedValue = "" Then
            BuildLeads(ddlLeadId.SelectedValue)
            dgrdTransactionSearch.Visible = False
        End If
        lblLeadName.Visible = False
    End Sub
    Private Sub BuildStatusDDL()
        Dim strLeadStatusId As String = ""
        Dim facade As New LeadStatusChangeFacade
        With ddlLeadStatusId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            .DataSource = facade.GetAvailLeadStatuses(strLeadStatusId, campusId, userId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Function RemoveHypen(ByVal strSSN As String) As String
        Dim strRemoveHypenSSN As String = String.Empty
        Dim chr As Char
        For Each chr In strSSN
            If Not chr = "-" Then
                strRemoveHypenSSN &= chr
            End If
        Next
        Return strRemoveHypenSSN
    End Function
    Private Sub BuildLeads(ByVal strLeadId As String)
        Dim leadStatus As New LeadFacade
        Dim strSSN As String
        If txtSSN.Text.Length >= 1 Then
            strSSN = RemoveHypen(txtSSN.Text)
        Else
            strSSN = ""
        End If
        If Len(txtSSN.Text) >= 1 And Len(txtSSN.Text) < 9 Then
            DisplayErrorMessage("Please enter the full SSN")
            Exit Sub
        End If
        With dlstLeadNames
            .DataSource = leadStatus.GetAllUnEnrolledLeadsByCampus(strLeadId, campusId, userId, txtFirstName.Text, txtLastName.Text, strSSN, ddlLeadStatusId.SelectedValue)
            .DataBind()
        End With
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub
    Private Sub BindDataGrid(ByVal leadId As String)
        Dim getDB As New LeadEntranceFacade
        Dim prgVerId As String
        prgVerId = getDB.GetPrgVersionByLead(leadId)
        If prgVerId <> "" Then
            'dgrdTransactionSearch.DataSource = getDB.GetGridRequirementDetailsByEffectiveDatesForOverRide(LeadId, prgVerId)  'getDB.GetGridRequirementDetails(Leadid, prgVerId)
            dgrdTransactionSearch.DataSource = getDB.GetPendingReqsByLeadandPrgVersion(leadId)
            dgrdTransactionSearch.DataBind()
        Else
            'dgrdTransactionSearch.DataSource = getDB.GetAllStandardRequirementsByEffectiveDatesForOverride(LeadId)
            dgrdTransactionSearch.DataSource = getDB.GetPendingReqsByLeadandPrgVersion(leadId)
            dgrdTransactionSearch.DataBind()
        End If
    End Sub
    Public Sub SetUpEntranceTest()
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        'Dim strEntrTestId As String
        'Dim intRequired As Integer
        Dim intArrCount As Integer
        'Dim intResult As Integer
        Dim z As Integer
        Dim facade As New LeadFacade

        ' Save the datagrid items in a collection.
        iitems = dgrdTransactionSearch.Items
        Try
            'Loop thru the collection to get the Array Size
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                ' If CType(iitem.FindControl("chkOverRide"), CheckBox).Checked = True Then
                intArrCount += 1
                'End If
            Next

            If intArrCount >= 1 Then
                Dim selectedEntranceTest() As String = Array.CreateInstance(GetType(String), intArrCount)
                'Dim selectedRequired() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedPass() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedOverRide() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedDocumentStatus() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedReqTypeDescrip() As String = Array.CreateInstance(GetType(String), intArrCount)

                'Loop Through The Collection To Get ClassSection
                For i = 0 To iitems.Count - 1
                    iitem = iitems.Item(i)
                    'retrieve clssection id from the datagrid
                    'If CType(iitem.FindControl("chkOverRide"), CheckBox).Checked = True Then
                    selectedEntranceTest.SetValue(CType(iitem.FindControl("lblEntrTestId"), Label).Text, z)
                    'If (CType(iitem.FindControl("chkRequired"), CheckBox).Checked = True) Then
                    '    selectedRequired.SetValue("Yes", z)
                    'Else
                    '    selectedRequired.SetValue("No", z)
                    'End If
                    If (CType(iitem.FindControl("chkPass"), CheckBox).Checked = True) Then
                        selectedPass.SetValue("Yes", z)
                    Else
                        selectedPass.SetValue("No", z)
                    End If
                    selectedDocumentStatus.SetValue(CType(iitem.FindControl("lblDocStatus"), Label).Text, z)
                    selectedReqTypeDescrip.SetValue(CType(iitem.FindControl("lblReqTypeDescrip"), Label).Text, z)
                    If (CType(iitem.FindControl("chkOverRide"), CheckBox).Checked = True) Then
                        selectedOverRide.SetValue("Yes", z)
                    Else
                        selectedOverRide.SetValue("No", z)
                    End If
                    z += 1
                    ' End If
                Next

                'resize the array
                'If z > 0 Then ReDim Preserve selectedClsSection(z - 1)
                'If z > 0 Then ReDim Preserve selectedRequired(z - 1)

                facade.UpdateOverRide(txtLeadId.Text, selectedEntranceTest, selectedPass, selectedDocumentStatus, selectedReqTypeDescrip, selectedOverRide, Session("UserName"))
            End If
        Finally
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        controlsToIgnore.Add(btnSearch)
        'Add javascript code to warn the user about non saved changes 
        '        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub InitButtonsForLoad()
        'If pObj.HasFull Or pObj.HasAdd Then
        '    btnSave.Enabled = True
        'Else
        '    btnSave.Enabled = False
        'End If


        btnnew.Enabled = False

        btndelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        'If pObj.HasFull Or pObj.HasDelete Then
        '    btndelete.Enabled = True
        'Else
        btndelete.Enabled = False
        'End If

        'If pObj.HasFull Or pObj.HasAdd Then
        '    btnnew.Enabled = True
        'Else
        btnnew.Enabled = False
        'End If
    End Sub

    Protected Sub dlstLeadNames_ItemCommand(source As Object, e As DataListCommandEventArgs) Handles dlstLeadNames.ItemCommand
        'Dim LeadName As New LeadEntranceFacade
        pnlControls.Controls.Clear()
        pnlHeader.Visible = True
        dgrdTransactionSearch.Visible = True
        txtLeadId.Text = CType(e.CommandArgument, String)
        lblLeadName.Text = CType((" Select the requirement to override for the Lead : " & e.CommandSource.text), String)
        lblLeadName.Visible = True
        BindDataGrid(txtLeadId.Text)
        Dim strGUID As String = dlstLeadNames.DataKeys(e.Item.ItemIndex).ToString()
        CommonWebUtilities.RestoreItemValues(dlstLeadNames, strGUID)
    End Sub
End Class
