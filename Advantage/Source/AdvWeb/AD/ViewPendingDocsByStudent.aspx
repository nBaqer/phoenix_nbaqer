<%@ Page Language="vb" AutoEventWireup="false" Inherits="ViewPendingDocsByStudent"
    CodeFile="ViewPendingDocsByStudent.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Documents By Program Version</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../CSS/localhost_lowercase.css" type="text/css" rel="stylesheet" />
    <link href="../css/systememail.css" type="text/css" rel="stylesheet" />
</head>
<body id="Body1" leftmargin="0" topmargin="0" runat="server" style="overflow:auto;">
    <form id="Form1" method="post" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td class="toppopupheader">
                <img src="../images/advantage_docstatus.jpg">
            </td>
            <td class="topemail">
                <a class="close" onclick="top.close()" href="#">X Close</a>
            </td>
        </tr>
    </table>
    <!--begin right column-->
        <table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="100%" border="0">
           <tr>
                <td class="detailsframe">
                    <div class="scrollwholedocs">
                        <!-- begin content table-->
                        <table width="60%" align="center">
                            <tr>
                                <td>
                                    <asp:Label ID="lblStudent" runat="server" CssClass="label">Student:</asp:Label>
                                </td>
                                <td nowrap>
                                    <asp:Label ID="lblHeading" runat="server" CssClass="labelbold"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <table width="90%" align="center">
                            <tr>
                                <td nowrap width="100%">
                                    <asp:DataGrid ID="dgrdTransactionSearch" runat="server" BorderColor="#E0E0E0" AllowSorting="True"
                                        AutoGenerateColumns="False" BorderStyle="Solid" ShowFooter="False" BorderWidth="1px"
                                        Width="100%" Visible="True">
                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                        <ItemStyle CssClass="datagriditemstyle" HorizontalAlign="Center"></ItemStyle>
                                        <HeaderStyle HorizontalAlign="Center" CssClass="datagridheaderstyle"></HeaderStyle>
                                        <FooterStyle CssClass="datagridpadding"></FooterStyle>
                                        <EditItemStyle CssClass="datagridpadding"></EditItemStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Documents" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-Width="35%" ItemStyle-Width="35%">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%# Container.DataItem("DocumentDescrip")%> ' runat="server" CssClass="label"
                                                        ID="label4" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Requirement Group" HeaderStyle-HorizontalAlign="Left"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="35%" ItemStyle-Width="35%">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblReqGrp" Text='<%# Container.DataItem("ReqGrpDescrip")%>'
                                                        CssClass="label"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Required" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center"
                                                HeaderStyle-Width="10%" ItemStyle-Width="10%">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%# GetBool(Container.DataItem("Required")) %> ' runat="server"
                                                        CssClass="label" ID="label5" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Document Status" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%# Container.DataItem("DocStatusDescrip")%> ' runat="server" CssClass="label"
                                                        ID="label7" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Req for Enrollment" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%# GetBoolforRequirement(Container.DataItem("ReqforEnrollment"))%> '
                                                        runat="server" CssClass="label" ID="label8" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Req for FinancialAid" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%# GetBoolforRequirement(Container.DataItem("ReqforFinancialAid"))%> '
                                                        runat="server" CssClass="label" ID="label9" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Req for Graduation" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:Label Text='<%# GetBoolforRequirement(Container.DataItem("ReqforGraduation"))%> '
                                                        runat="server" CssClass="label" ID="label10" />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                            <tr>
                                <td class="spacertables2">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblMsg" runat="server" Visible="false" CssClass="label" Font-Bold="true"
                                        Text="Note: Between parenthesis is the minimum number of requirements to complete in order to satisfy a Requirement Group."></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
      
        </table>
    <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
    </form>
</body>
</html>
