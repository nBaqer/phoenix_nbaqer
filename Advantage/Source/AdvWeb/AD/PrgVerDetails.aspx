<%@ Page Language="vb" AutoEventWireup="false" Title="Requirement Groups" MasterPageFile="~/NewSite.master" Inherits="PrgVerDetails" CodeFile="PrgVerDetails.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>Admissions Requirements For Program Version</title>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="ContentMain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="ContentError" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="ContentMain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="ddlPrgVerId">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="ddlPrgVerId" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <asp:Panel ID="pnlrhs" runat="server">
                <table width="98%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                                <!-- begin top menu (save,new,reset,delete,history)-->
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button>
                                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                            Enabled="False"></asp:Button>
                                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"></asp:Button>
                                    </td>

                                </tr>
                            </table>
                            <!-- end top menu (save,new,reset,delete,history)-->

                            <!--begin right column-->
                            <div class="boxContainer">
                                <h3>
                                    <asp:Label ID="headerTitle" runat="server"></asp:Label></h3>
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable">
                                    <tr>
                                        <td class="detailsframe">
                                            <div>
                                                <!-- begin table content-->
                                                <table width="50%" border="0" cellpadding="0" align="center" cellspacing="0" class="contenttable">
                                                    <tr>
                                                        <td class="twocolumnlabelcell" style="text-align: right">
                                                            <asp:Label ID="lblPrgVerIdTitle" runat="server" CssClass="Label">Program Version</asp:Label>
                                                        </td>
                                                        <td class="twocolumncontentcell" style="text-align: left">
                                                            <asp:DropDownList ID="ddlPrgVerId" runat="server" AutoPostBack="True" TabIndex="1">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lblPrgVerDescrip" CssClass="LabelBold" Visible="False" runat="server">PrgVerDescrip</asp:Label>
                                                            <asp:Label ID="lblPrgVerId" Visible="False" runat="server">PrgVerId</asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div>
                                    <asp:Panel ID="PnlHrsCrdts" runat="server" CssClass="Frame" Width="100%">

                                        <table width="1200px" border="0" cellpadding="0" cellspacing="0" align="center">
                                            <tr>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblGrdOverride" Visible="False" runat="server" CssClass="Label">Override Min.Grd Required</asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtGrdOverride" Visible="false" runat="server" CssClass="Textbox" MaxLength="6" Width="50"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="fivecolumnspacer">&nbsp;
                                                </td>
                                                <td class="fivecolumnspacer">&nbsp;
                                                </td>
                                                <td class="fivecolumnspacer">&nbsp;
                                                </td>
                                                <td class="fivecolumnspacer">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fivecolumnheader">
                                                    <asp:Label ID="Label2" runat="server" CssClass="label" Font-Bold="True">Available Requirements</asp:Label>
                                                </td>
                                                <td class="fivecolumnspacer">&nbsp;
                                                </td>
                                                <td align="center" width="32%" style="background-color: #e9edf2; border: 1px solid #ebebeb;">
                                                    <asp:Label ID="Label3" runat="server" CssClass="label" Style="text-align: center" Font-Bold="True">Available Requirement Groups</asp:Label>
                                                </td>
                                                <td class="fivecolumnspacer">&nbsp;
                                                </td>
                                                <td class="fivecolumnheader">
                                                    <asp:Label ID="lblSelected" runat="server" CssClass="label" Font-Bold="True">Selected Requirement/Requirement Groups </asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fivecolumncontent">
                                                    <asp:ListBox ID="lbxAvailReqs" runat="server" CssClass="listboxes" TabIndex="2" AutoPostBack="True"
                                                        Rows="12"></asp:ListBox>
                                                </td>
                                                <td class="fivecolumnspacer">&nbsp;
                                                </td>
                                                <td class="fivecolumncontent">
                                                    <asp:ListBox ID="lbxAvailReqGrps" runat="server" TabIndex="4" CssClass="listboxes"
                                                        AutoPostBack="False" Rows="12"></asp:ListBox>
                                                </td>
                                                <td class="fivecolumnspacer">&nbsp;
                                                </td>
                                                <td class="fivecolumncontent">
                                                    <asp:ListBox ID="lbxSelected" runat="server" CssClass="listboxes" TabIndex="6" Rows="12"></asp:ListBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="fivecolumnfooter" align="center">
                                                    <asp:Button ID="btnAdd1" runat="server" TabIndex="3" Width="80px"
                                                        Text="Add"></asp:Button>
                                                </td>
                                                <td class="fivecolumnspacer">&nbsp;
                                                </td>
                                                <td class="fivecolumnfooter" align="center">
                                                    <asp:Button ID="btnAdd" runat="server" TabIndex="5" Width="80px"
                                                        Text="Add"></asp:Button>
                                                </td>
                                                <td class="fivecolumnspacer">&nbsp;
                                                </td>
                                                <td class="fivecolumnfooter" align="center">
                                                    <asp:Button ID="btnRemove" runat="server" TabIndex="7" Width="80px"
                                                        Text="Remove"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>

                                    </asp:Panel>
                                    <asp:TextBox ID="txtProgVerId" Visible="false" MaxLength="128" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtPrgVerTestDetailId" Visible="false" MaxLength="128" runat="server"></asp:TextBox>
                                    <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" Width="0" CssClass="donothing"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" Width="0" CssClass="donothing"></asp:TextBox>
                                    <!--end table content-->
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>

                <%--  </td> --%>
                <!-- end rightcolumn -->
                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                <%--   </tr>
         </table>--%>
            </asp:Panel>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
        ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
            runat="server">
        </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
        ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
</asp:Content>


