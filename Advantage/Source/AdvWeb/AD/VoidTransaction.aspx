﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VoidTransaction.aspx.vb" Inherits="AD_VoidTransaction" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow)
                oWindow = window.RadWindow; //Will work in Moz in all cases, including clasic dialog       
            else if (window.frameElement.radWindow)
                oWindow = window.frameElement.radWindow; //IE (and Moz as well)       
            return oWindow;
        }
        function Close() {
            GetRadWindow().BrowserWindow.location.reload();
            GetRadWindow().Close();
            //GetRadWindow().BrowserWindow.document.forms[0].submit();
        }
        function Cancel() {
            GetRadWindow().Close();
        }
            </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
           <telerik:RadScriptManager ID="radScriptManager1" runat="server"></telerik:RadScriptManager>
           <table>
               <tr>
                   <td><asp:Label ID="lblMessage" runat="server" style="font-family:Arial; font-size:12px; padding-left:20px;">Do you want to void this transaction?</asp:Label></td>
               </tr>
               <tr>
                   <td>
                        <asp:Button ID="btnOk" runat="server" OnClientClick="Close();"  text="Ok" style="font-family:Arial; font-size:10px;margin-left:40px;background-color:#6788be;border-color:#6788be;color:white" />
                       <asp:Button ID="btnCancel" runat="server" OnClientClick="Cancel();" Text="Cancel" style="font-family:Arial; font-size:10px;margin-left:40px;background-color:#6788be;border-color:#6788be;color:white" />

                    </td>
               </tr>

           </table>
    </div>
    </form>
</body>
</html>
