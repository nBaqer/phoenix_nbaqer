

<%@ Page  Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" Inherits="StudentResumes" CodeFile="StudentResume.aspx.vb" %>
<%@ MasterType  VirtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
        <link href="../css/localhost_lowercase.css" type="text/css" rel="stylesheet">
    <link href="../css/resume.css" type="text/css" rel="stylesheet" media="screen">
    <link href="../css/print.css" type="text/css" rel="stylesheet" media="print">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Y" width="100%"  Orientation="HorizontalTop">
        <!-- end header -->
        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
            <tr>
                <td class="detailsframetop">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4" class="menutable">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False">
                                </asp:Button></td>
                           
                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                    <!--begin right column-->
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable">
                        <tr>
                            <td class="detailsframe">
                               
                                    <!-- begin table content-->
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="resumecontainer">
                                        <tr>
                                            <td>
                                                <asp:DataList ID="dlstStudentName" HorizontalAlign="Center" Width="100%" runat="server">
                                                    <ItemTemplate>
                                                        <h1>
                                                            <%# Container.DataItem( "firstname" ) & "  " & Container.DataItem( "lastname" )%>
                                                        </h1>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:DataList ID="dlstStudentAddress" HorizontalAlign="Center" Width="100%" runat="server">
                                                    <ItemTemplate>
                                                        <ul class="resumeheader">
                                                           
                                                                <%# Container.DataItem( "Address" )%>
                                                                </br>
                                                       
                                                                <%# Container.DataItem( "city" )%>
                                                                ,
                                                                <%# Container.DataItem( "state" )%>
                                                                <%# Container.DataItem( "zip" )%>
                                                                </br>
                                                          <strong>Email:</strong>
                                                                <%# Container.DataItem( "HomeEmail" )%>
                                                        
                                                        </ul>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <asp:DataList ID="dlstPhone" HorizontalAlign="Center" Width="100%" runat="server">
                                                    <ItemTemplate>
                                                        <ul class="resumeheader">
                                                           <strong>Phone No:</strong>
                                                                <%# Container.DataItem( "Phone" )%>
                                                           
                                                        </ul>
                                                    </ItemTemplate>
                                                </asp:DataList>
                                                <h2>
                                                    Objective</h2>
                                            
                                                  
                                                        <asp:Label ID="lblobj" runat="server"></asp:Label><br/>
                                                <!--</ul>-->
                                                <h2>
                                                    Experience</h2>
                                                <ul>
                                                   
                                                        <asp:Label ID="lblExperience" runat="server"></asp:Label><br/>
                                                </ul>
                                                <h2>
                                                    Education</h2>
                                                <ul>
                                                   
                                                        <asp:DataList ID="dlstCollegeHistory" Width="100%" runat="server">
                                                            <ItemStyle Font-Size="11px" Font-Names="verdana" Font-Bold="True"></ItemStyle>
                                                            <HeaderStyle Font-Size="11px" Font-Names="verdana" Font-Bold="True"></HeaderStyle>
                                                            <HeaderTemplate>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="educationdescription" style="width: 50%; text-align: left">
                                                                            <strong><u>College/School Name</u></strong></td>
                                                                        <td class="educationdescription" style="width: 25%; text-align: left">
                                                                            <strong><u>Major</u></strong></td>
                                                                        <td class="educationdescription" style="width: 25%; text-align: left">
                                                                            <strong><u>Graduated Date</u></strong></td>
                                                                    </tr>
                                                                </table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td class="educationdescription" style="width: 50%; text-align: left">
                                                                            <%# StrConv(Container.DataItem( "CollegeName" ),VbStrConv.ProperCase)%>
                                                                        </td>
                                                                        <td class="educationdescription" style="width: 25%; text-align: left">
                                                                            <%# Container.DataItem( "Major" ) %>
                                                                        </td>
                                                                        <td class="educationdescription" style="width: 25%; text-align: left">
                                                                            <asp:Label ID="Label1" Text='<%# DataBinder.Eval(Container.DataItem,"GraduatedDate", "{0:d}") %>'
                                                                                runat="server"> </asp:Label></td>
                                                                    </tr>
                                                                </table>
                                                            </ItemTemplate>
                                                        </asp:DataList><br/>
                                                </ul>
                                                <h2>
                                                    Skills</h2>
                                                <ul>
                                                    
                                                        <asp:Label ID="lblOutput" runat="server"></asp:Label><br/>
                                                </ul>
                                                <h2>
                                                    Extracurriculars</h2>
                                                <ul>
                                                    
                                                        <asp:Label ID="lblExtracurroutput" runat="server"></asp:Label><br/>
                                                </ul>
                                                <asp:TextBox ID="txtStudentId" runat="server" Visible="False"></asp:TextBox>
                                                <!--end table content-->
                                            </td>
                                        </tr>
                                    </table>
                               
                            </td>
                        </tr>
                    </table>
                </td>
                <!-- end rightcolumn -->
            </tr>
        </table>
     
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
    </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>