﻿
Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports Advantage.Business.Objects

Imports FAME.Advantage.Reporting

Partial Class LeadRequirement
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    ' ReSharper disable InconsistentNaming
    Protected WithEvents btnhistory As Button
    Protected WithEvents lblReassignToAdmissionReps As Label
    Protected WithEvents lstReassignToAdmissionReps As ListBox
    Protected WithEvents lblAssignmentResult As Label
    Protected WithEvents lstAssignmentResult As ListBox
    Protected WithEvents lstGrpName As ListBox
    Protected WithEvents lstPossibleSkills As ListBox
    Protected WithEvents listbox2 As ListBox
    Protected WithEvents img3 As HtmlImage
    Protected WithEvents btnSubmit As Button
    Protected WithEvents txtLeadsInfo As TextBox
    Protected WithEvents lblLeads As Label
    Protected WithEvents lstAssignCampus As ListBox
    Protected campusId As String
    ' ReSharper restore InconsistentNaming Protected mContext As HttpContext


    Private pObj As New UserPagePermissionInfo
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

    Dim userId As String
    Dim resourceId As Integer
    Protected LeadId As String
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Get LeadId
        Dim objStudentState As StudentMRU = GetObjStudentState()
        Dim advantageUserState As User = AdvantageSession.UserState
        campusId = Me.Master.Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString
        hdnUserId.Value = userId
        hdnLeadId.Value = AdvantageSession.MasterLeadId

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

        'Check if this page still exists in the menu while switching campus
        If Me.Master.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If
        End If
        'If postedTransactionId IsNot Nothing And Not String.IsNullOrEmpty(postedTransactionId.Value) Then
        '    Dim uId As String = Integer.Parse(postedTransactionId.Value.Substring(0, 6), System.Globalization.NumberStyles.HexNumber).ToString()
        '    PrintReceipt(postedTransactionId.Value, uId)
        'End If

    End Sub


    'Protected Sub PrintReceipt(ByVal transactionId As String, ByVal uId As String)
    '    Dim getReportAsBytes As [Byte]()
    '    Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder") '"/Advantage Reports/" + SingletonAppSettings.AppSettings("SSRS Deployment Environment").ToString.Trim
    '    getReportAsBytes = (New Logic.PrintLeadReceipt).RenderReport("pdf", _
    '                                                                TransactionId, _
    '                                                                uId, _
    '                                                                AdvantageSession.UserState.UserName.ToString, _
    '                                                                strReportPath, _
    '                                                                AdvantageSession.UserState.CampusId.ToString)
    '    ExportReport("pdf", getReportAsBytes)
    'End Sub

    'Private Sub ExportReport(ByVal exportFormat As String, ByVal getReportAsBytes As [Byte]())
    '    Dim strExtension, strMimeType As String
    '    Select Case ExportFormat.ToLower
    '        Case Is = "pdf"
    '            strExtension = "pdf"
    '            strMimeType = "application/pdf"
    '            Exit Select
    '        Case "excel"
    '            strExtension = "xls"
    '            strMimeType = "application/vnd.excel"
    '            Exit Select
    '            'Case "WORD"
    '            '    strExtension = "doc"
    '            '    strMimeType = "application/vnd.ms-word"
    '        Case "csv"
    '            strExtension = "csv"
    '            strMimeType = "text/csv"
    '        Case Else
    '            Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
    '    End Select
    '    Session("SSRS_FileExtension") = strExtension
    '    Session("SSRS_MimeType") = strMimeType
    '    Session("SSRS_ReportOutput") = getReportAsBytes
    '    'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
    '    Dim script As String = String.Format("window.open('../SY/DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString())
    '    ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    'End Sub

End Class