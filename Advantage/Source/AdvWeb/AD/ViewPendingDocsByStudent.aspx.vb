Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data

Partial Class ViewPendingDocsByStudent
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected getDocs As New LeadFacade
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Put user code to initialize the page here
        Dim DocumentList As New LeadDocsFacade
        Dim docsFacade As New LeadEntranceFacade

        Dim strStudentId As String = Trim(Request.QueryString("studentid"))
        Dim strStudentName As String = Trim(Request.QueryString("StudentName"))
        Dim campusid As String = Trim(Request.QueryString("campusid"))
        lblHeading.Text = strStudentName

        Dim ds As New DataSet
        ds = docsFacade.GetPendingDocsByStudentandPrgVersion(strStudentId, campusid)

        dgrdTransactionSearch.DataSource = ds
        dgrdTransactionSearch.DataBind()

        'If ds is nothing, lblMsg remains hidden.
        If Not (ds Is Nothing) Then
            Dim temp As DataView
            temp = New DataView(ds.Tables(0), "ReqGrpDescrip<>''", "", DataViewRowState.CurrentRows)
            'Show message only if there are requirement groups in the table
            If temp.Count > 0 Then
                lblMsg.Visible = True
            Else
                lblMsg.Visible = False
            End If
        End If

    End Sub
    Public Function GetBool(ByVal intVal As Integer) As String
        If intVal = 1 Then
            Return "Yes"
        Else
            Return "No"
        End If

    End Function
    Public Function GetBoolforRequirement(ByVal Value As Boolean) As String
        If Value = True Then
            Return "Yes"
        Else
            Return "No"
        End If

    End Function

End Class
