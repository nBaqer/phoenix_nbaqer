﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common

Partial Class LeadExtracurriculars
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblSkillGroups As System.Web.UI.WebControls.Label
    Protected WithEvents lblPossibleSkills As System.Web.UI.WebControls.Label
    Protected WithEvents lblStudentSkills As System.Web.UI.WebControls.Label
    Protected WithEvents txtcheck As System.Web.UI.HtmlControls.HtmlInputText
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    'Protected WithEvents CustomValidator1 As System.Web.UI.WebControls.CustomValidator
    Protected LeadId As String
    Private pObj As New UserPagePermissionInfo


    Protected StudentId As String
    Protected state As AdvantageSessionState
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub



#End Region
    Dim userId As String
    Dim resourceId As Integer
    Dim campusId As String
    Protected boolSwitchCampus As Boolean = False
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function GetLeadFromStateObject(ByVal paramResourceId As Integer) As Advantage.Business.Objects.StudentMRU

        Dim objStudentState As New Advantage.Business.Objects.StudentMRU

        Try

            MyBase.GlobalSearchHandler(1)

            boolSwitchCampus = Master.Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.Master.ShowHideStatusBarControl(True)

            Master.Master.PageObjectId = LeadId
            Master.Master.PageResourceId = Request.QueryString("resid")
            Master.Master.setHiddenControlForAudit()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Dim fac As New UserSecurityFacade

        Dim advantageUserState As User = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        userId = AdvantageSession.UserState.UserId.ToString
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As StudentMRU = GetLeadFromStateObject(148) 'Pass resourceid so that user can be redirected to same page while switching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        txtLeadId.Text = LeadId
        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            'Call Procedure to Bind Extracurricular Groups
            BindExtracurricularGroup()
            BindStudentExtracurricularsByGroup(campusId, LeadId)

            If boolSwitchCampus = True Then 'if lead is from a different campus show notification
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""
        End If
        'Code to disable all buttons if lead was already enrolled
        'to prevent data being changed after lead was enrolled
        Dim chkLeadWasEnrolled As Boolean
        Dim leadFacade As New LeadEnrollmentFacade
        chkLeadWasEnrolled = leadFacade.CheckIfLeadWasEnrolled(LeadId)
        If chkLeadWasEnrolled = True Then
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
            btnNew.ToolTip = "Cannot add new extracurriculars as the lead was already enrolled"
            btnSave.ToolTip = "Cannot modify extracurriculars information as the lead was already enrolled"
            btnDelete.ToolTip = "Cannot delete extracurriculars information as the lead was already enrolled"
        Else
            InitButtonsForEdit()
            btnSave.ToolTip = ""
            btnDelete.ToolTip = ""
            btnNew.ToolTip = ""
        End If
    End Sub
    Private Sub BindExtracurricularGroup()
        ' This Procedure is to bind all Extracurricular Groups
        ' Calls GetAllExtracurricularGroups Procedure in PlacementFacade.vb

        Dim skillGroup As New PlacementFacade
        With lstGrpName
            .DataSource = skillGroup.GetAllExtracurricularGroups(LeadId, campusId)
            .DataTextField = "ExtraCurricularGrpName"
            .DataValueField = "ExtraCurricularGrpId"
            .DataBind()
        End With
    End Sub
    Private Sub BindExtracurricularByGroup(ByVal ExtracurricularGrpId As String, ByVal LeadID As String)

        ' This Procedure is to bind all Possible Extracurricular By Group
        ' Gets Extracurriculars that doesn't exist in Student Extracurriculars
        ' Calls GetAllExtraCurricularsByGroup Procedure in PlacementFacade.vb
        Dim ExtracurricularByGroup As New PlacementFacade

        'Get Extracurricular GroupId
        txtExtracurricularGrpId.Text = ExtracurricularGrpId

        'Get Student Id
        txtLeadId.Text = LeadID

        With lstPossibleExtracurriculars
            .DataSource = ExtracurricularByGroup.GetAllLeadExtraCurricularsByGroup(txtExtracurricularGrpId.Text, txtLeadId.Text, campusId)
            .DataTextField = "ExtracurrDescrip"
            .DataValueField = "ExtracurrId"
            .DataBind()
        End With
    End Sub
    Private Sub BindStudentExtracurricularsByGroup(ByVal ExtracurricularGrpId As String, ByVal LeadID As String)
        ' This Procedure is to bind all Possible Student Extracurriculars By Group
        ' Gets Extracurriculars Assigned To Students Based on GroupId and LeadID
        ' Calls GetStudentExtracurricularsByGroup Procedure in PlacementFacade.vb
        Dim ExtracurricularByGroup As New PlacementFacade
        txtExtracurricularGrpId.Text = ExtracurricularGrpId
        txtLeadId.Text = LeadID
        lstStudentExtracurriculars.Items.Clear()
        With lstStudentExtracurriculars
            .DataSource = ExtracurricularByGroup.GetLeadExtracurricularsByGroup("", txtLeadId.Text, campusId)
            .DataTextField = "ExtracurrDescrip"
            .DataValueField = "ExtracurrId"
            .DataBind()
        End With
    End Sub
    Private Sub lstGrpName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstGrpName.SelectedIndexChanged
        'Get The ExtracurricularGroupId Based On Selected Group
        txtExtracurricularGrpId.Text = lstGrpName.SelectedItem.Value

        'Clear The ListBox
        lstStudentExtracurriculars.Items.Clear()

        lstStudentExtracurriculars.SelectedIndex = -1
        lstPossibleExtracurriculars.SelectedIndex = -1

        'Bind The Possible Extracurricular List and Student List
        BindExtracurricularByGroup(txtExtracurricularGrpId.Text, txtLeadId.Text)
        BindStudentExtracurricularsByGroup(txtExtracurricularGrpId.Text, txtLeadId.Text)

        'Enable the Add all button if there are any items in the possible skills list box
        'This should only happen if the user has permission to add items
        If pObj.HasAdd Or pObj.HasFull Then
            If lstPossibleExtracurriculars.Items.Count > 0 Then
                btnAddAll.Enabled = True
            Else
                btnAddAll.Enabled = False
            End If
        End If

        'Enable the Remove All button if there any items in the selected skills list box
        'This should only happen if the user has permission to remove items
        If pObj.HasDelete Or pObj.HasFull Then
            If lstStudentExtracurriculars.Items.Count > 0 Then
                btnRemoveAll.Enabled = True
            Else
                btnRemoveAll.Enabled = False
            End If
        End If

        'Disable the Remove button if it is enabled
        If btnRemove.Enabled = True Then
            btnRemove.Enabled = False
        End If

        'Disable the Add button if it is enabled
        If btnAdd.Enabled = True Then
            btnAdd.Enabled = False
        End If
    End Sub
    'Private Function BuildSkill(ByVal StrGrpId As String) As PlacementInfo
    '    Dim StudentSkill As New PlacementInfo
    '    With StudentSkill
    '        'get Student Group Id
    '        .StrGrpId = StrGrpId
    '    End With
    'End Function

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim strSelectedText As String
        Dim strSelectedValue As String

        'If there are no items in possible extracurricular list
        If lstPossibleExtracurriculars.Items.Count = 0 Then
            'DisplayErrorMessage("Unable to find an Extracurricular. Please select one from the Extracurricular Group")
            DisplayRADAlert(CallbackType.Postback, "Error1", "Unable to find an Extracurricular. Please select one from the Extracurricular Group ", "Add Error")
            Exit Sub
        End If

        'If no items are selected in possible extracurricular list
        If lstPossibleExtracurriculars.SelectedIndex = -1 Then
            'DisplayErrorMessage("Please select an item from the possible Extracurriculars list")
            DisplayRADAlert(CallbackType.Postback, "Error2", "Please select an item from the possible Extracurriculars list ", "Add Error")
            Exit Sub
        End If

        'Get The Selected Text And Value
        strSelectedText = lstPossibleExtracurriculars.SelectedItem.Text
        strSelectedValue = lstPossibleExtracurriculars.SelectedItem.Value

        'Add and Remove Values From The ListBoxes
        Dim i, count As Integer
        If lstStudentExtracurriculars.Items.Count >= 1 Then
            For i = 0 To lstStudentExtracurriculars.Items.Count - 1
                If lstStudentExtracurriculars.Items(i).Text = strSelectedText Then
                    count = 1
                End If
            Next
            If Not (count = 1) Then
                lstStudentExtracurriculars.Items.Add(New ListItem(strSelectedText, strSelectedValue))
                lstPossibleExtracurriculars.Items.RemoveAt(lstPossibleExtracurriculars.SelectedIndex)
            End If
        Else
            lstStudentExtracurriculars.Items.Add(New ListItem(strSelectedText, strSelectedValue))
            lstPossibleExtracurriculars.Items.RemoveAt(lstPossibleExtracurriculars.SelectedIndex)
        End If
        'Enable the Remove All button if the user has permission to delete
        If pObj.HasFull Or pObj.HasDelete Then
            btnRemoveAll.Enabled = True
        End If

    End Sub
    Private Sub btnAddAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddAll.Click
        ''lblError.Visible = False
        If lstPossibleExtracurriculars.Items.Count = 0 Then
            'DisplayErrorMessage("Unable to find Extracurricular in the list")
            DisplayRADAlert(CallbackType.Postback, "Error3", "Unable to find Extracurricular in the list ", "Add Error")
            Exit Sub
        End If

        'Add All Items From Possible Extracurricular Skills To Student Listbox
        Dim i As Integer ', counter
        For i = 0 To lstPossibleExtracurriculars.Items.Count - 1
            lstStudentExtracurriculars.Items.Add(New ListItem(lstPossibleExtracurriculars.Items(i).Text, lstPossibleExtracurriculars.Items(i).Value))
        Next
        lstPossibleExtracurriculars.Items.Clear()
        'Enable the Remove All button if the user has permission to delete
        If pObj.HasFull Or pObj.HasDelete Then
            btnRemoveAll.Enabled = True
        End If
    End Sub
    Private Sub btnRemoveAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveAll.Click
        ''lblError.Visible = False
        Dim i As Integer ', counter

        If lstStudentExtracurriculars.Items.Count = 0 Then
            'DisplayErrorMessage("Unable to find Extracurricular in student's list")
            DisplayRADAlert(CallbackType.Postback, "Error4", "Unable to find Extracurricular in student's list ", "Remove Error")
            Exit Sub
        End If
        'Remove All Item From the Student Extracurricular ListBox
        For i = 0 To lstStudentExtracurriculars.Items.Count - 1
            lstPossibleExtracurriculars.Items.Add(New ListItem(lstStudentExtracurriculars.Items(i).Text, lstStudentExtracurriculars.Items(i).Value))
        Next
        lstStudentExtracurriculars.Items.Clear()

        'Enable the Add All button if the user has permission to add
        If pObj.HasFull Or pObj.HasAdd Then
            btnAddAll.Enabled = True
        End If

    End Sub
    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        'Dim strRemoveItem As String
        Dim StrSelectedText As String
        Dim strSelectedValue As String

        ''lblError.Visible = False
        If lstStudentExtracurriculars.Items.Count = 0 Then
            'DisplayErrorMessage("The list is empty")
            DisplayRADAlert(CallbackType.Postback, "Error5", "The list is empty ", "Remove Error")
            Exit Sub
        End If

        If (lstStudentExtracurriculars.SelectedIndex = -1) Then
            'DisplayErrorMessage("Please select student's Extracurriculars from the list")
            DisplayRADAlert(CallbackType.Postback, "Error6", "Please select student's Extracurriculars from the list ", "Remove Error")
            Exit Sub
        Else
            StrSelectedText = lstStudentExtracurriculars.SelectedItem.Text
            strSelectedValue = lstStudentExtracurriculars.SelectedItem.Value
            lstPossibleExtracurriculars.Items.Add(New ListItem(StrSelectedText, strSelectedValue))
            lstStudentExtracurriculars.Items.RemoveAt(lstStudentExtracurriculars.SelectedIndex)
        End If

        'Enable the Add All button if the user has permission to add
        If pObj.HasFull Or pObj.HasAdd Then
            btnAddAll.Enabled = True
        End If

    End Sub
    Private Sub UpdateExtracurricularSkills(ByVal LeadID As String, ByVal ExtracurricularGrpId As String)
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), lstStudentExtracurriculars.Items.Count)
        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In lstStudentExtracurriculars.Items
            selectedDegrees.SetValue(item.Value.ToString, i)
            i += 1
        Next

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)

        'update Selected Jobs
        Dim employerJobs As New PlEmployerInfoFacade
        Dim StudentExtracurriculars As New PlacementFacade
        StudentExtracurriculars.UpdateLeadExtracurriculars(LeadID, txtExtracurricularGrpId.Text, Session("UserName"), selectedDegrees)
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click


        ''''' Code changes to Fix mantis issue id 19549 by kamalesh Ahuja on 23 Aug 2010 '''
        Dim userId As String
        userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim LeadMasterUpdate As New LeadFacade
        If MyAdvAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "no" Then
            If LeadMasterUpdate.CheckLeadUser(userId, LeadId) = 0 Then
                'DisplayErrorMessage("You do not have rights to edit this lead")
                DisplayRADAlert(CallbackType.Postback, "Error7", "You do not have rights to edit this lead ", "Save Error")
                Exit Sub
            End If
        End If
        ''''''''''''''''''

        Try
            UpdateExtracurricularSkills(txtLeadId.Text, txtExtracurricularGrpId.Text)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Exit Sub
        End Try
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)
    '    'Set error condition
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub

    Private Sub lstStudentExtracurriculars_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstStudentExtracurriculars.SelectedIndexChanged
        'Enable the Remove button if the user has permission to delete
        If pObj.HasFull Or pObj.HasDelete Then
            btnRemove.Enabled = True
        End If

        'Disable the Add button if it is enabled
        If btnAdd.Enabled = True Then
            btnAdd.Enabled = False
        End If
        'Disable the Add All button is if is enabled
        If btnAddAll.Enabled = True Then
            btnAddAll.Enabled = False
        End If

    End Sub

    Private Sub lstPossibleExtracurriculars_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstPossibleExtracurriculars.SelectedIndexChanged
        'Enable the Add button
        btnAdd.Enabled = True
        'Disable the Remove and Remove All buttons
        If btnRemove.Enabled = True Then
            btnRemove.Enabled = False
        End If
        If btnRemoveAll.Enabled = True Then
            btnRemoveAll.Enabled = False
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnAddAll)
        controlsToIgnore.Add(btnRemove)
        controlsToIgnore.Add(btnRemoveAll)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    Private Sub InitButtonsForEdit()
        'In this case the New button is not relevant
        If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
    End Sub

End Class
