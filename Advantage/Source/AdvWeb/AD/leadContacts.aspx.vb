﻿
Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports Advantage.Business.Objects

Partial Class AD_leadContacts
    Inherits BasePage

    Dim userId As String
    Dim resourceId As Integer
    Protected LeadId As String
    Protected campusId As String
    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Me.Page.Title = hfPageTitle.Value
        Dim advantageUserState As User = AdvantageSession.UserState
        campusId = Me.Master.Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString
        hdnUserId.Value = userId
        hdnLeadId.Value = AdvantageSession.MasterLeadId
        'Get LeadId
        Dim objStudentState As StudentMRU = GetObjStudentState()
        If objStudentState Is Nothing Then
            RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        Master.Master.PageObjectId = objStudentState.LeadId.ToString()
        Master.Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
        Master.Master.SetHiddenControlForAudit()

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)
        If Me.Master.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If
        End If

    End Sub
End Class
