﻿<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentGroups.aspx.vb" Inherits="AD_StudentGroups" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
    <script type="text/javascript">
        window.onload = function () {
            var strCook = document.cookie;
            if (strCook.indexOf("!~") != 0) {
                var intS = strCook.indexOf("!~");
                var intE = strCook.indexOf("~!");
                var strPos = strCook.substring(intS + 2, intE);
                document.getElementById("grdWithScroll").scrollTop = strPos;
            }
        }
        function SetDivPosition() {
            var intY = document.getElementById("grdWithScroll").scrollTop;
            document.cookie = "yPos=!~" + intY + "~!";
        }
    </script>
    <script language="javascript">
        window.__enter = false;
    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstAdmissionReqGroups">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstAdmissionReqGroups" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstAdmissionReqGroups" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstAdmissionReqGroups" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="radstatus">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                    <telerik:AjaxUpdatedControl ControlID="dlstAdmissionReqGroups" />
                    <telerik:AjaxUpdatedControl ControlID="radstatus" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstAdmissionReqGroups" runat="server" DataKeyField="StuGrpId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("StuGrpId")%>' Text='<%# container.dataitem("Descrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>

                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCode" runat="server" CssClass="label" Text="Code<span style='color: #b71c1c;'>*</span>"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCode" runat="server" CssClass="textbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label" Text="Status<span style='color: #b71c1c;'>*</span>"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblDescrip" runat="server" CssClass="label" Text="Description<span style='color: #b71c1c;'>*</span>"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtDescrip" runat="server" CssClass="textbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label" Text="Campus Group<span style='color: #b71c1c;'>*</span>"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="Label2" runat="server" CssClass="label" Text="Student Group Type<span style='color: #b71c1c;'>*</span>"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlGroupType" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="Label1" runat="server" CssClass="label" Text="Owner<span style='color: #b71c1c;'>*</span>"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlOwnerId" runat="server" CssClass="dropdownlist"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:TextBox ID="txtStuGrpId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4" align="left">
                                                <asp:CheckBox ID="chkPublic" CssClass="label" runat="server" Text="Public" /></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4" align="left">
                                                <asp:CheckBox ID="ChkRegistrationHold" CssClass="label" runat="server" Text="Registration Hold" /></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4" align="left">
                                                <asp:CheckBox ID="ChkTranscriptHold" CssClass="label" runat="server" Text="Transcript Hold" /></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4">
                                                <asp:Button ID="btnViewStudents" runat="server" Text=" View Students " Enabled="false" Width="125px" Visible="false" /></td>
                                        </tr>
                                    </table>
                                    <table class="maincontenttable" id="Table6" cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td>
                                                <telerik:RadPanelBar runat="server" ID="rpbStudentSearch" ExpandMode="SingleExpandedItem" Width="100%" Visible="false">
                                                    <Items>
                                                        <telerik:RadPanelItem Text="Student Search" runat="server" Expanded="false">
                                                            <Items>
                                                                <telerik:RadPanelItem Value="StudentSearch" runat="server">
                                                                    <ItemTemplate>
                                                                        <table width="100%" align="center" border="0">
                                                                            <tr height="20">
                                                                                <td colspan="6">&nbsp;</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="6">
                                                                                    <span class="labelBold">Please enter your search criteria, and then press the 'Search' button.</span>
                                                                                </td>
                                                                            </tr>
                                                                            <tr height="5">
                                                                                <td colspan="6"></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lblLastName" runat="Server" CssClass="label">Last Name</asp:Label></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtLastName" runat="Server" CssClass="textbox" Width="115px" TabIndex="1"></asp:TextBox></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblProgram" runat="Server" CssClass="label">Program</asp:Label></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlStudentPrgVerId" runat="Server" CssClass="dropdownlist" Width="150px" TabIndex="3"></asp:DropDownList></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblSSN" runat="Server" CssClass="label">SSN</asp:Label></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtSSN" runat="Server" CssClass="textbox" Width="80px" TabIndex="5"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:Label ID="lblFirstName" runat="Server" CssClass="label">First Name</asp:Label></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtFirstName" runat="Server" CssClass="textbox" Width="115px" TabIndex="2"></asp:TextBox></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblStatus" runat="Server" CssClass="label">Status</asp:Label></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlStatusId" runat="Server" CssClass="dropdownlist" TabIndex="4" Width="150px"></asp:DropDownList></td>
                                                                                <td>
                                                                                    <asp:Label ID="lblEnrollment" runat="Server" CssClass="label">Enrollment #</asp:Label></td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtEnrollment" runat="Server" CssClass="textbox" Width="80px" TabIndex="6"></asp:TextBox></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td>
                                                                                    <asp:DropDownList ID="ddlTermId" runat="server" Visible="False"></asp:DropDownList>
                                                                                    <asp:DropDownList ID="ddlAcademicYearId" runat="server" Visible="False"></asp:DropDownList>
                                                                                </td>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td></td>
                                                                                <td align="center">
                                                                                    <asp:Button ID="btnSearch" runat="Server" Width="60px" Text="Search" OnClick="btnSearch_Click"></asp:Button></td>
                                                                                <td></td>
                                                                                <td>
                                                                                    <asp:Button ID="btnAddStudents" runat="Server" Width="150px" Text="Add Students to Group" OnClick="btnAddStudents_Click" Enabled="false"></asp:Button></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="6">
                                                                                    <telerik:RadGrid ID="rgStudentSearch" runat="server" Width="100%" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="10" AllowSorting="True" AllowMultiRowSelection="False" AllowFilteringByColumn="true" GroupingSettings-CaseSensitive="false" AllowPaging="True" OnNeedDataSource="rgStudentSearch_NeedDataSource" OnItemDataBound="rgStudentSearch_ItemDataBound">
                                                                                        <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                                        <ExportSettings IgnorePaging="true" OpenInNewWindow="true"></ExportSettings>
                                                                                        <MasterTableView Width="100%" Name="StudentSearch" AllowMultiColumnSorting="false">
                                                                                            <Columns>
                                                                                                <telerik:GridBoundColumn AllowSorting="false" HeaderText="SID" HeaderButtonType="TextButton" DataField="StudentId" Visible="false" UniqueName="STUID"></telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn AllowSorting="false" HeaderText="SEID" HeaderButtonType="TextButton" DataField="EnrollmentId" Visible="false" UniqueName="SEID"></telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn AllowFiltering="true" SortExpression="LastName" HeaderText="Last Name" HeaderButtonType="TextButton" DataField="LastName"></telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn AllowFiltering="true" SortExpression="FirstName" HeaderText="First Name" HeaderButtonType="TextButton" DataField="FirstName"></telerik:GridBoundColumn>
                                                                                                <telerik:GridBoundColumn AllowFiltering="true" SortExpression="SSN" HeaderText="SSN" HeaderButtonType="TextButton" DataField="SSN"></telerik:GridBoundColumn>
                                                                                                <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Enrollments" UniqueName="Enrollments">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:DropDownList ID="ddlEnrollmentsId" runat="server" Width="150px" CssClass="DropDownList"></asp:DropDownList>
                                                                                                    </ItemTemplate>
                                                                                                </telerik:GridTemplateColumn>
                                                                                                <telerik:GridTemplateColumn AllowFiltering="false" DataField="StuEnrollmentId" HeaderText="" UniqueName="Delete">
                                                                                                    <HeaderTemplate>
                                                                                                        <asp:CheckBox ID="ChkAll" runat="server" AutoPostBack="true" OnCheckedChanged="ChkAll_CheckChanged" Text="Select All" TextAlign="Right" />
                                                                                                    </HeaderTemplate>
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton ID="lnkbutDelete" Visible="false" Text="<img border=0 src=../images/delete.gif alt=Delete>" CausesValidation="False" runat="server" CommandName="Delete"></asp:LinkButton>
                                                                                                        <asp:CheckBox ID="ChkStu" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"chkDelete") %>' />
                                                                                                    </ItemTemplate>
                                                                                                </telerik:GridTemplateColumn>
                                                                                            </Columns>
                                                                                        </MasterTableView>
                                                                                    </telerik:RadGrid>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </ItemTemplate>
                                                                </telerik:RadPanelItem>
                                                            </Items>
                                                        </telerik:RadPanelItem>
                                                        <telerik:RadPanelItem Text="View Students" Expanded="false" runat="server">
                                                            <Items>
                                                                <telerik:RadPanelItem Value="ViewStudents" runat="server">
                                                                    <ItemTemplate>
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="right">
                                                                                    <asp:Button ID="btnRemoveStudent" runat="Server" Width="175px" Text="Remove Students from Group" OnClick="btnRemoveStudent_Click" Enabled="false"></asp:Button></td>
                                                                            </tr>
                                                                        </table>
                                                                        <telerik:RadGrid ID="rgGroupStudents" runat="server" Width="100%" ShowStatusBar="true" AutoGenerateColumns="False" PageSize="10" AllowSorting="True" AllowMultiRowSelection="False" AllowFilteringByColumn="true" GroupingSettings-CaseSensitive="false" AllowPaging="True" OnDeleteCommand="GroupStudents_Delete" OnNeedDataSource="rgGroupStudents_NeedDataSource" OnItemDataBound="rgGroupStudents_ItemDataBound">
                                                                            <PagerStyle Mode="NumericPages"></PagerStyle>
                                                                            <ExportSettings IgnorePaging="true" OpenInNewWindow="true"></ExportSettings>
                                                                            <MasterTableView Width="100%" Name="GroupStudent" AllowMultiColumnSorting="false">
                                                                                <Columns>
                                                                                    <telerik:GridBoundColumn AllowSorting="false" HeaderText="ASID" HeaderButtonType="TextButton" DataField="StuGrpStuId" Display="false" UniqueName="STDID"></telerik:GridBoundColumn>
                                                                                    <telerik:GridBoundColumn AllowSorting="false" HeaderText="ASID" HeaderButtonType="TextButton" DataField="StudentId" Display="false" UniqueName="STUID"></telerik:GridBoundColumn>
                                                                                    <telerik:GridBoundColumn AllowFiltering="true" SortExpression="LastName" HeaderText="Last Name" HeaderButtonType="TextButton" DataField="LastName"></telerik:GridBoundColumn>
                                                                                    <telerik:GridBoundColumn AllowFiltering="true" SortExpression="FirstName" HeaderText="First Name" HeaderButtonType="TextButton" DataField="FirstName"></telerik:GridBoundColumn>
                                                                                    <telerik:GridBoundColumn AllowFiltering="true" SortExpression="Program" HeaderText="Program Version" HeaderButtonType="TextButton" DataField="Program"></telerik:GridBoundColumn>
                                                                                    <telerik:GridBoundColumn AllowFiltering="true" SortExpression="SSN" HeaderText="Enrollment Status" HeaderButtonType="TextButton" DataField="Status"></telerik:GridBoundColumn>
                                                                                    <telerik:GridBoundColumn AllowFiltering="true" SortExpression="CampusName" HeaderText="Campus" HeaderButtonType="TextButton" DataField="CampusName"></telerik:GridBoundColumn>
                                                                                    <telerik:GridTemplateColumn AllowFiltering="false" DataField="StuEnrollmentId" HeaderText="Delete" UniqueName="Delete">
                                                                                        <HeaderTemplate>
                                                                                            <asp:CheckBox ID="chkDeleteALL" runat="server" AutoPostBack="true" Text="Select All" TextAlign="Right" OnCheckedChanged="SetAllChecked" />
                                                                                        </HeaderTemplate>
                                                                                        <ItemTemplate>
                                                                                            <asp:LinkButton ID="lnkbutDelete" Visible="false" Text="<img border=0 src=../images/delete.gif alt=Delete>" CausesValidation="False" runat="server" CommandName="Delete"></asp:LinkButton>
                                                                                            <asp:CheckBox ID="chkDeleteStu" runat="server" Checked='<%# DataBinder.Eval(Container.DataItem,"chkDelete") %>' />
                                                                                        </ItemTemplate>
                                                                                    </telerik:GridTemplateColumn>
                                                                                </Columns>
                                                                            </MasterTableView>
                                                                        </telerik:RadGrid>
                                                                    </ItemTemplate>
                                                                </telerik:RadPanelItem>
                                                            </Items>
                                                        </telerik:RadPanelItem>
                                                    </Items>
                                                </telerik:RadPanelBar>
                                            </td>
                                        </tr>
                                    </table>
                                    <!--end table content-->
                                    <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="label" Width="10%"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="label" Width="10%"></asp:TextBox>
                                </div>
                                <!--end table content-->
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>
</asp:Content>


