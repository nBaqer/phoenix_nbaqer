﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="LeadAssignment.aspx.vb" Inherits="LeadAssignment" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../js/CheckAll.js" type="text/javascript"></script>
    <link href="../css/AD/LeadAssignment.css" rel="stylesheet" />
    <%--  <link href="../Kendo/styles/kendo.dataviz.blueopal.min.css" rel="stylesheet" />
  <script type="text/javascript" src="../Scripts/FileSaver.js"></script>
    <script type="text/javascript" src="../Scripts/Storage/storageCache.js"></script>
    <script type="text/javascript" src="../Scripts/common-util.js"></script>
    <script type="text/javascript" src="../Scripts/common/kendoControlSetup.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/leadVendorDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/campusOfinterestDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/campusesDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/admissionsRepDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/admissionsReRepDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/leadUnassignedDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/leadReassignDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/campusWithActiveLeadsDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/viewModels/leadAssignment.js"></script>--%>

 
    <script id="infoTemplate" type="text/x-kendo-template">
        <div class="infoMessageTemplate">
            <img width="32px" height="32px" src=#= src #   />
            <h3>#= title #</h3>
            <p>#= message #</p>
        </div>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Width="100%" Orientation="HorizontalTop" Scrolling="Both">
            <div id="AdmissionPageWrapper" style="margin: 10px;">
                <div id="tabstrip" style='display: table; width: 100%; '>
                    <ul>
                        <li id="assignToCampusRep" class="k-state-active">
                            <span id="assignHeader">Assign Leads</span>
                        </li>
                        <li id="duplicateLeadTab">
                            <span>Duplicates</span>
                        </li>

                        <li id="ReassignToCampsuRep">
                            <span>Reassign Leads</span>
                        </li>

                    </ul>
                    <div>
                        <br />
                        <div class="leadcontainer" style="width: 74%">
                            <div class="leadheader">
                                <table cellpadding="1" style="width: 100%">
                                    <tr>
                                        <td valign="middle" width="20%">
                                            <h4 >1. &nbsp;&nbsp;Select Task</h4>
                                        </td>

                                        <td valign="middle" width="80%">
                                            <table width="100%">
                                                <tr>
                                                    <td style="width: 25%; text-align: left;">
                                                        <input type="radio" name="radio" id="rdoCampus" value="rCampus" checked="checked" />&nbsp;
                                                        <span class="blueLabels2">Assign Leads to Campus</span>&nbsp;
                                                        <span style="font-weight: bold">(</span>
                                                        <label style="color: red; vertical-align: text-top; font-weight: bold;" id="rCampus"></label>
                                                        <span style="font-weight: bold">)</span>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input type="radio" name="radio" id="rdoRep" value="rRep" />&nbsp;<span class="blueLabels2">Assign Leads to Admissions Rep</span>&nbsp;<span style="font-weight: bold">(</span><label style="color: red; vertical-align: text-top; font-weight: bold;" id="rRep"></label><span style="font-weight: bold">)</span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="width: 650px; height: 40px;" class="leadmainbody">
                                <table width="650px" cellpadding="2" cellspacing="2">
                                    <tr>
                                        <td width="50px" valign="middle" align="right"><span class="blueLabels2">Vendor</span></td>
                                        <td width="200px" valign="middle">
                                            <input id="ddlVendor" style="width: 175px" /></td>
                                        <td width="120px" valign="middle" id="tdCampus" align="right">
                                            <span class="blueLabels2" id="lblCampus">Area of Interest</span></td>
                                        <td width="225px" valign="middle">
                                            <input id="ddlCampuses" style="width: 250px" /><input id="ddlCampusesReal" style="width: 250px; display: none;" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <section>
                            <div id="oneDiv">
                                <div class="leadcontainer">
                                    <div class="leadheader">
                                        <table cellpadding="1">
                                            <tr>
                                                <td valign="middle">
                                                    <h4 >2.&nbsp;&nbsp;Select Leads</h4>
                                                </td>

                                            </tr>
                                        </table>
                                    </div>
                                    <div style="height: 425px; position: relative;" class="leadmainbody">
                                        <div id="leadsGrid" data-bind="source: leadsDataSource" style="position: relative; width: 99%; height: 99%"></div>
                                    </div>

                                </div>
                            </div>
                            <div id="twoDiv">

                                <div class="leadcontainer2">
                                    <div class="leadheader">
                                        <table cellpadding="1">
                                            <tr>
                                                <td valign="middle">
                                                    <h4 id='step3Header'>3.&nbsp;&nbsp;Select Campus</h4>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="height: 441px;" class="leadmainbody2">
                                        <div id="Step3Tables" style="height: 425px; position:relative" class="leadmainbody2">
                                            <div id="campusGrid" data-bind="source: campusGridDataSource" style="width: 99%; height:89%"></div>
                                            <div id="campusRepGrid" data-bind="source: admissionRepSource" style="width: 99%; height:89%; display: none"></div>
                                            <div style="text-align: center">
                                                <button id="btnAssignLeadsToCampus" class="k-button" type="button" style="width: 200px; margin-top:10px">Assign Leads to this Campus</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <br />
                    </div>

                    <%--Duplicate Leads This is for Jose G to work on once he has had his morning coffee--%>
                    <div id="leadDuplicatePagebind" style="overflow: hidden; height: 600px">
                        <script type="text/kendo-x-tmpl" id="templateDuplicateLeadListView">
                            <div class="k-readonly leadDuplicateNameListItem ">
                                #:PosibleDuplicateFullName#
                            </div>
                        </script>
                        <div class="leadPageColumn" style="display: inline; float: left; width: 160px">
                            <div class="leadheaderColumn">1. Select Existing Lead</div>

                            <div id="duplicateLeadlistView" style="cursor: pointer; border: 0"></div>

                        </div>

                        <!-- Template for ListView with the duplicate information -->
                        <script type="text/kendo-x-tmpl" id="templateduplicateHeaderLeadlistView">
                            <div class="leadDuplicateComparationList" >
                               <div class="leadDuplicateHeader">#:Header#</div> 
                               <div class="item1 leadDuplicateItem" >#:AcquiredDate#</div>
                               <div class="item2 leadDuplicateItem" >#:From#</div>
                               <div class="item3 leadDuplicateItem" >#:FullName#</div>
                               <div class="item4 leadDuplicateItem" >#:Address1#</div>
                               <div class="item5 leadDuplicateItem" >#:Address2#</div>
                               <div class="item6 leadDuplicateItem" >#:City#</div>
                               <div class="item7 leadDuplicateItem" >#:State#</div>
                               <div class="item8 leadDuplicateItem" >#:Zip#</div>
                               <div class="item9 leadDuplicateItem" >#:Status#</div>
                               <div class="item10 leadDuplicateItem" >#:Phone#</div>
                               <div class="item11 leadDuplicateItem" >#:Email#</div>
                               <div class="item12 leadDuplicateItem" >#:SourceInfo#</div>
                               <div class="item13 leadDuplicateItem" >#:AdmissionsRep#</div>
                               <div class="item14 leadDuplicateItem" >#:Campus#</div>
                               <div class="item15 leadDuplicateItem" >#:ProgramOfInterest#</div>
                               <div class="item16 leadDuplicateItem" >#:Comments#</div>
                        </div>
                        </script>
                        <div class="leadPageColumn" style="min-width: 800px; margin-left: 190px; min-height: 500px;">

                            <div class="leadheaderColumn">2. Resolve Duplicate Lead</div>
                            <div id="duplicateHeaderLeadlistView" style="border: 0"></div>

                        </div>
                        <br style="clear: both;" />
                        <div style="margin: 0; min-width: 800px; margin-left: 190px; min-height: 500px;">
                            <button id="buttonDupUpdateLeadB" class="k-button leadDuplicateButton" type="button">Update Advantage with Lead A</button>
                            <button id="buttonDupDeleteLeadA" class="k-button leadDuplicateButton" type="button">Delete Lead A</button>
                            <button id="buttonDupCreateLeadA" class="k-button leadDuplicateButton" type="button">Create New Lead from Lead A </button>
                            <button id="buttonDupCreateLeadB" class="k-button leadDuplicateButton" type="button">Create New Lead from Lead B </button>
                        </div>
                    </div>

                    <%--RE-assign Leads--%>
                    <div>
                        <br />
                        <div class="leadcontainer" style="width: 74%">
                            <div class="leadheader">
                                <table cellpadding="1" width="100%">
                                    <tr>
                                        <td valign="middle" width="20%">
                                            <h4 style="font-size: 14px;">1.&nbsp;&nbsp;Select Task</h4>
                                        </td>
                                        <td valign="middle" width="80%">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td align="left">
                                                        <input type="radio" name="reradio" id="rdoReCampus" value="reCampus" checked="checked" />
                                                        &nbsp;<span class="blueLabels2">Reassign to another Campus</span>&nbsp;
                                                        <label style="color: red; vertical-align: text-top; font-weight: bold;" id="reCampus"></label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <input type="radio" name="reradio" id="rdoReRep" value="reRep" />
                                                        &nbsp;<span class="blueLabels2">Reassign to another Admissions Rep</span>
                                                        <label style="color: red; vertical-align: text-top; font-weight: bold;" id="reRep"></label>
                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="width: 100%; height: 40px;" class="HeaderReassign">

                                <div id="qZoneLeft">
                                    <label id="blueLabels2" class="blueLabels2">Current Campus</label>
                                    <input id="ddlReCampus" style="width: 250px; display: none;" />

                                </div>
                                <div id="qZoneCenter" style="display: inline">
                                    <label id="blueLabels3" class="blueLabels2">Last Date Modified</label>
                                    <input id="ddlReDate" style="width: 80px; display: none;" />
                                </div>
                                <div id="qZoneRight" style="display: none;">
                                    <label id="lblReRep" class="blueLabels2">Current Admission Rep</label>
                                    <input id="ddlReRep" style="width: 155px" />
                                </div>

                            </div>
                        </div>
                        <section>
                            <div id="oneR">
                                <div class="leadcontainer">
                                    <div class="leadheader">
                                        <table cellpadding="1">
                                            <tr>
                                                <td valign="middle">
                                                    <h4 style="font-size: 14px;">2.&nbsp;&nbsp;Select Leads</h4>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="height: 425px; position: relative;" class="leadmainbody">
                                        <div id="leadsReGrid" data-bind="source: leadsReDataSource" style="position: relative; width: 99%; height: 99%"></div>
                                    </div>

                                </div>
                            </div>
                            <div id="twoR">
                                <div class="leadcontainer2">
                                    <div class="leadheader">
                                        <table cellpadding="1">
                                            <tr>
                                                <td valign="middle">
                                                    <h4 style="font-size: 14px;" id="step3ReHeader">3.&nbsp;&nbsp;Select New Campus</h4>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="height: 441px;" class="leadmainbody2">
                                        <div style="height: 425px;" class="leadmainbody2">
                                            <div id="reCampusRepGrid" data-bind="source: reCampusGridDataSource" style="height:89%; width: 99%;"></div>
                                            <div id="reAssignedRepGrid" data-bind="source: reCampusGridDataSource" style="height:89%; width: 99%; display: none"></div>
                                            <div style="text-align: center">
                                                <button id="btnReAssignLeadsToCampus" class="k-button" type="button" style="margin-top:10px; width: 200px;">Assign Leads to this Campus</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <br />
                    </div>
                </div>
            </div>

            <span id="kNotification"></span>
            <asp:HiddenField runat="server" ID="hdnUserId" ClientIDMode="Static" />
        </telerik:RadPane>
    </telerik:RadSplitter>
    <script type="text/javascript" src="../Scripts/Advantage.Client.AD.js"></script>
    <%-- ReSharper disable UnusedLocals --%>
    <script type="text/javascript">
        $(document).ready(function () {
            var admission = new AD.LeadAssignment();
            var manager = new AD.LeadDuplicate();
        });
    </script>
    <%-- ReSharper restore UnusedLocals --%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>


