﻿
Imports Advantage.Business.Objects

Namespace AdvWeb.AD

    Partial Class AdAleadSkills
        Inherits BasePage

        Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

            'Get LeadId
            Dim objStudentState As StudentMRU = GetObjStudentState()
            If objStudentState Is Nothing Then
                RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
                Exit Sub
            End If
            'With objStudentState
            '    StudentId = .StudentId.ToString
            '    leadID = .LeadId.ToString
            '    'if lead is from a different campus show notification

            'End With
            Master.Master.PageObjectId = objStudentState.LeadId.ToString()
            Master.Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
            Master.Master.SetHiddenControlForAudit()

            Dim advantageUserState As User = AdvantageSession.UserState
            Dim campusId as String= Me.Master.Master.CurrentCampusId
            Dim resourceId as Integer = CInt(HttpContext.Current.Request.Params("resid"))

            Dim pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

            'Check if this page still exists in the menu while switching campus
            If Me.Master.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
                End If
            End If
        End Sub

    End Class
End Namespace