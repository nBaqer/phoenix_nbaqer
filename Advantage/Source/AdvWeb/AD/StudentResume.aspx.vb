
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects

Partial Class StudentResumes
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    'Protected WithEvents dlstEmploymentHistory As System.Web.UI.WebControls.DataList
    Protected WithEvents dlstSchoolHistory As System.Web.UI.WebControls.DataList
    'Protected WithEvents dlstSchoolPlaced As System.Web.UI.WebControls.DataList
    Protected WithEvents DgTestRules As System.Web.UI.WebControls.DataGrid
    Private designerPlaceholderDeclaration As System.Object
    Protected WithEvents txtLeadID As System.Web.UI.WebControls.TextBox
    Protected WithEvents DgObjective As System.Web.UI.WebControls.DataGrid
    Protected WithEvents txtobj As System.Web.UI.WebControls.TextBox
    Protected StudentId As String
    Protected ResourceID As Integer
    Protected ModuleId As String
    Protected state As AdvantageSessionState


    Protected LeadId As String = Guid.Empty.ToString
    Protected boolSwitchCampus As Boolean = False



#End Region

#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()


    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU


        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'Dim SearchLeadID As New EmployerSearchFacade
        'LeadID = SearchLeadID.GetLeadID(Session("SSN"))
        Dim userId, campusId, moduleId As String
        Dim resourceId As Integer
        Dim advantageUserState As User = AdvantageSession.UserState
        Dim pObj As New UserPagePermissionInfo

        userId = AdvantageSession.UserState.UserId.ToString
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        ViewState("CampusId") = campusId
        moduleId = AdvantageSession.UserState.ModuleCode.ToString



        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New StudentMRU
        objStudentState = GetStudentFromStateObject(114) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        ' advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        txtStudentId.Text = StudentId
        Dim plFacade As New PlacementFacade
        Dim intAddressExist As Integer = plFacade.intCheckAddressExists(StudentId)
        Dim intPhoneExist As Integer = plFacade.intCheckPhoneExists(StudentId)

        If intAddressExist >= 1 Then
            BuildStudentAddressDDL()
            dlstStudentAddress.Visible = True
        Else
            dlstStudentAddress.Visible = False
        End If

        If intPhoneExist >= 1 Then
            BuildStudentPhoneDataList()
            dlstPhone.Visible = True
        Else
            dlstPhone.Visible = False
        End If
        BuildStudentNameDataList()
        BuildSkills()
        BuildStudentCollgeHistory()
        BuildExtracurriculars()
        BuildSchoolPlacementHistory()
        EditButtons()
        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            BuildObjective()
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010

            'If (New StudentFERPA).HasFERPAPermission(ResourceID, StudentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, ResourceID.ToString)
            'End If

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""


        End If
        BuildExperience()
    End Sub
    Private Sub BuildExperience()
        Dim output As New PlacementFacade
        lblExperience.Text = output.StudentExperience(StudentId)
        If lblExperience.Text = "None" Then
            lblExperience.Text = ""
        End If
    End Sub
    Private Sub EditButtons()
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub
    Private Sub BuildStudentNameDataList()
        Dim StudentName As New PlacementFacade
        With dlstStudentName
            .DataSource = StudentName.GetStudentName(StudentId)
            .DataBind()
        End With
    End Sub
    Private Sub BuildStudentPhoneDataList()
        Dim StudentAddress As New PlacementFacade
        With dlstPhone
            .DataSource = StudentAddress.GetResumeStudentPhone(Trim(StudentId))
            .DataBind()
        End With
    End Sub
    Private Sub BuildStudentAddressDDL()
        'Bind the Certificate DropDownList
        'Based on Degrees DropDownList
        Dim StudentAddress As New PlacementFacade
        With dlstStudentAddress
            .DataSource = StudentAddress.GetResumeStudentAddress(Trim(StudentId))
            .DataBind()
        End With

    End Sub
    Private Sub BuildObjective()
        'Bind the Certificate DropDownList
        'Based on Degrees DropDownList
        Dim StudentAddress As New LeadFacade
        lblobj.Text = StudentAddress.GetLeadObjective(txtStudentId.Text)
    End Sub
    Private Sub BuildStudentCollgeHistory()
        'Bind the Certificate DropDownList
        'Based on Degrees DropDownList
        Dim StudentCollege As New PlacementFacade
        With dlstCollegeHistory
            .DataSource = StudentCollege.ResumeStudentEducationHistory(txtStudentId.Text)
            .DataBind()
        End With
    End Sub
    Private Sub BuildSchoolPlacementHistory()

    End Sub
    Private Sub BuildSkills()
        Dim output As New PlacementFacade
        lblOutput.Text = output.SkillGroup(StudentId)
        If lblOutput.Text = "None" Then
            lblOutput.Text = ""
        End If
    End Sub
    Private Sub BuildExtracurriculars()
        Dim ExtracurrOutput As New PlacementFacade
        lblExtracurroutput.Text = ExtracurrOutput.GetStudentExtracurriculars(StudentId)
        If lblExtracurroutput.Text = "None" Then
            lblExtracurroutput.Text = ""
        End If
    End Sub
    Private Sub dlstStudentAddress_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlstStudentAddress.ItemDataBound

    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim Exp As New LeadFacade
        Dim result As Integer
        Dim strObj As String = Trim(txtobj.Text)
        result = Exp.AddObjective(StudentId, strObj)
    End Sub

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        '        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub


End Class
