﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="AleadEducation.aspx.vb" Inherits="AD_AleadEducation" %>

<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Width="100%" Orientation="HorizontalTop" Scrolling="Both">
            <!-- Use this to put content -->
            <div id="leadPriorEducation" class="kendo-page" style="width: 98%" >
                <section class="title-normal">
                    <label class="leadTitles" style="font-weight: bold !important; vertical-align: top; font-size: 11pt !important;">Prior Education</label>
                    <a id="lnkAddPriorEducation" href="javascript:void(0);">
                        <span class="k-icon k-i-plus-circle font-green"></span>
                    </a>
                </section>
                <section class="main-grid-section">
                    <div id="leadEducationGrid" class="main-grid"></div>
                </section>
                <section id="institutionSection" class="grid-sub-edit hidden">
                    <section class="title2">
                        <label class="institutionName bold"><span></span></label>
                    </section>
                    <section class="title">
                        <label><b>Phones</b></label>
                        <a id="lnkAddPhone" href="javascript:void(0);" class="lnkAdd">
                            <img src="../images/icon/Add.gif" alt="Add Institution Phone" /></a>
                    </section>
                    <div id="institutionPhonesGrid" style="width: 500px;"></div>
                    <section class="title">
                        <label><b>Addresses</b></label>
                        <a id="lnkAddAddress" href="javascript:void(0);" class="lnkAdd">
                            <img src="../images/icon/Add.gif" alt="Add Institution Address" /></a>
                    </section>
                    <div id="institutionAddressGrid" style="width: 950px"></div>
                    <section class="title">
                        <label><b>Contact</b></label>
                        <a id="lnkAddContact" href="javascript:void(0);" class="lnkAdd">
                            <img src="../images/icon/Add.gif" alt="Add Contact Address" /></a>
                    </section>
                    <div id="institutionContactGrid" style="width: 850px"></div>
                    <div class="clear"></div>

                    <section class="title">
                        <label><b>Comments</b></label>
                    </section>
                    <div id="prioEducationComents" style="width: 80%;">
                        <textarea id="priorEducationComentsText" class="leadComments" maxlength="300"></textarea>
                        <div class="commentsAction">
                            <a href="javascript:void(0)" id="lnkSaveComments">
                                <img src="../images/icon/save.png" /></a>
                            <a href="javascript:void(0)" id="lnkDeleteComments">
                                <img src="../images/icon/action_delete.png" /></a>
                        </div>
                    </div>

                    <div class="clear"></div>
                </section>
                <br />
                <div id="dvCustomBar"style="display: none">
                    <label><b>Custom</b></label>
                </div>
                <div id="dvCustomFields" class="grid-sub-edit "  style="display: none;"></div>
                <div id ="dvCustomSave" ><button id="sdfSave" type="button" class="k-button " style="float: right; margin-top: 10px;display: none " title="Save">Save</button></div>
            </div>
            <script id="institutionPhoneForm" type="text/x-kendo-template">
                <div id="institutionPhoneFormValidator" class="popup">
                     <div class="popup-row">
                        <div class="templateLeft">
                            <label>Type</label><span class="red">&nbsp;*</span>
                        </div>
                        <div class="templateRight">
                            <select id="ddlPhoneType" name="ddlPhoneType" class="ddl" style="width: 150px;" required></select>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Is International?</label>
                        </div>
                        <div class="templateRight">
                            <input id="chkPhoneIsInternational" name="chkPhoneIsInternational" type="checkbox"/>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Phone</label><span class="red">&nbsp;*</span>
                        </div>
                        <div class="templateRight">
                            <input id="txtPhone" name="txtPhone" class="k-textbox" style="width: 150px;" required/>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </script>

            <script id="institutionAddressForm" type="text/x-kendo-template">
                <div id="institutionAddressFormValidator" class="popup">
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Type</label><span class="red">&nbsp;*</span>
                        </div>
                        <div class="templateRight">
                            <select id="ddlAddressType" name="ddlAddressType" class="ddl" style="width: 150px; height: 30px;" required></select>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Is International?</label>
                        </div>
                        <div class="templateRight">
                            <input id="chkPhoneIsInternational" name="chkPhoneIsInternational" type="checkbox"/>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Address</label><span class="red">&nbsp;*</span>
                        </div>
                        <div class="templateRight">
                            <input id="txtAddress" name="txtAddress" class="k-textbox" style="width: 250px;" maxlength="250"  required/>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>City</label><span class="red">&nbsp;*</span>
                        </div>
                        <div class="templateRight">
                            <input id="txtCity" name="txtCity" class="k-textbox" style="width: 200px;" maxlength="250"  required />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>State</label><span class="red">&nbsp;*</span>
                        </div>
                        <div class="templateRight">
                            <select id="ddlAddressState" name="ddlAddressState" class="ddl" style="width: 200px; height: 30px;"></select>
                            <input id="txtAddressState" name="txtAddressState" class="k-textbox hidden" maxlength="50"  style="width: 200px;" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Zip</label><span class="red">&nbsp;*</span>
                        </div>
                        <div class="templateRight">
                            <input id="txtAddressZip" name="txtAddressZip" class="k-textbox" style="width: 100px;"  maxlength="10" required/>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Country</label><span class="red">&nbsp;*</span>
                        </div>
                        <div class="templateRight">
                            <select id="ddlAddressCountry" name="ddlAddressCountry" class="ddl" style="width: 200px; height: 30px;"></select>
                            <input id="txtAddressCountry" name="txtAddressCountry" class="k-textbox hidden" style="width: 200px;" maxlength="100"  />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>County</label>
                        </div>
                        <div class="templateRight">
                            <select id="ddlAddressCounty" name="ddlAddressCounty" class="ddl" style="width: 200px; height: 30px;"></select>
                            <input id="txtAddressCounty" name="txtAddressCounty" class="k-textbox hidden" style="width: 200px;" maxlength="100"  />
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </script>
            <script id="institutionContactForm" type="text/x-kendo-template">
            <div id="institutionContactFormValidator" class="popup">
                <div class="popup-row">
                    <div class="templateLeft">
                        <label>Title</label>
                    </div>
                    <div class="templateRight">
                        <input id="txtTitle" name="txtTitle" class="k-textbox" style="width: 200px;"  maxlength="100" />
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup-row">
                    <div class="templateLeft">
                        <label>Prefix</label>
                    </div>
                    <div class="templateRight">
                        <select id="ddlPrefix" name="ddlPrefix" class="ddl" style="width: 200px; height: 30px;"></select>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup-row">
                    <div class="templateLeft">
                        <label>First Name</label><span class="red">&nbsp;*</span>
                    </div>
                    <div class="templateRight">
                        <input id="txtFirstName" name="txtFirstName" class="k-textbox" style="width: 200px;" maxlength="50"  required />
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup-row">
                    <div class="templateLeft">
                        <label>Last Name</label><span class="red">&nbsp;*</span>
                    </div>
                    <div class="templateRight">
                        <input id="txtLastName" name="txtLastName" class="k-textbox" style="width: 200px;" maxlength="50"  required />
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup-row">
                    <div class="templateLeft">
                        <label>Middle Name</label>
                    </div>
                    <div class="templateRight">
                        <input id="txtMiddleName" name="txtMiddleName" class="k-textbox" style="width: 200px;" maxlength="50"  />
                    </div>
                    <div class="clear"></div>
                </div>
                
                <div class="popup-row">
                    <div class="templateLeft">
                        <label>Suffix</label>
                    </div>
                    <div class="templateRight">
                        <select id="ddlSuffix" name="ddlSuffix" class="ddl" style="width: 200px; height: 30px;"></select>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup-row">
                    <div class="templateLeft">
                        <label>Phone</label>
                    </div>
                    <div class="templateRight">
                        <input id="txtContactPhone" name="txtContactPhone" class="k-textbox" style="width: 200px;" maxlength="50"  />
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="popup-row">
                    <div class="templateLeft">
                        <label>Email</label>
                    </div>
                    <div class="templateRight">
                        <input id="txtContactEmail" name="txtContactEmail" type="email" class="k-textbox" style="width: 200px;" maxlength="50"  />
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
            </script>

            <script id="leadEducationForm" type="text/x-kendo-template">
                <div id="leadEducationFormValidator" class="popup">
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Level</label><span class="red">&nbsp;*</span>
                        </div>
                        <div class="templateRight">
                            <select id="ddlLevel" name="ddlLevel" class="ddl" style="width: 150px; height: 30px;" required></select>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Select</label><span class="red">&nbsp;*</span>
                        </div>
                        <div class="templateRight">
                            <div id="rlSelect"  data-type="radioGroup" data-name="rgSelect" required="required" >
                                <input id="rlSchoolDefined" name="rgSelect" type="radio"  value="1" />School Defined &nbsp;
                                <input id="rlNational" name="rgSelect" type="radio"  value="2"/>National &nbsp;
                                <input id="rlNew" name="rgSelect" type="radio" value="3"/>New &nbsp;
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Institution Name</label><span class="red">*</span>
                        </div>
                        <div class="templateRight">
                            <input id="acDdlInstitutionName" name="acDdlInstitutionName" style="width: 350px" maxlength="100"  required />
                            <input id="txtInstitutionName" name="txtInstitutionName" style="width: 350px" class="hidden k-textbox fieldlabel1" maxlength="100"  />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Major</label>
                        </div>
                        <div class="templateRight">
                            <input id="txtMajor" name="txtMajor" class="k-textbox" style="width: 250px;" maxlength="50"  />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Graduate</label>
                        </div>
                        <div class="templateRight">
                            <select id="ddlGraduate" name="ddlGraduate" class="k-textbox" style="width: 75px; height: 30px;">
                                <option value="">Select</option>
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Grad Date</label>
                        </div>
                        <div class="templateRight">
                            <input id="txtGradDate" name="txtGradDate" data-validation="date" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Certificate/Degree</label>
                        </div>
                        <div class="templateRight">
                            <input id="txtCertificate" name="txtCertificate" class="k-textbox fieldlabel1" style="width: 250px" maxlength="50" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Final Grade</label>
                        </div>
                        <div class="templateRight">
                            <input id="txtFinalGrade" name="txtFinalGrade" class="k-textbox fieldlabel1" data-validation="lettersnumbers" style="width: 75px" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>GPA</label>
                        </div>
                        <div class="templateRight">
                            <input id="txtGpa" name="txtGpa" class="k-textbox fieldlabel1" style="width: 75px" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Rank</label>
                        </div>
                        <div class="templateRight">
                            <input id="txtRank" name="txtRank" class="k-textbox fieldlabel1" style="width: 75px" />
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Percentile</label>
                        </div>
                        <div class="templateRight">
                            <input id="txtPercentile" name="txtPercentile" class="k-textbox fieldlabel1" style="width: 75px" /> %
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="popup-row">
                        <div class="templateLeft">
                            <label>Comments</label>
                        </div>
                        <div class="templateRight">
                            <textarea id="txtComments" name="txtComments" class="k-textbox fieldlabel1" style="width: 300px" maxlength="300" ></textarea>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </script>
        </telerik:RadPane>
    </telerik:RadSplitter>

    <asp:HiddenField runat="server" ID="hdnUserId" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnCampusId" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdnLeadId" ClientIDMode="Static" />

    <script type="text/javascript">
        $(document).ready(function () {
            var leadId = $("#hdnLeadId").val();
            var campusId = $("#hdnCampusId").val();
            var userId = $("#hdnUserId").val();

            var leadEducation = new AD.LeadEducation({ CampusId: campusId, LeadId: leadId, UserId: userId });
            leadEducation.initialize("leadEducationGrid", "leadEducationForm", "lnkAddPriorEducation");
        });
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

