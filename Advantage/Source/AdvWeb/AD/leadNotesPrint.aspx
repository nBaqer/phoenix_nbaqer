﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LeadNotesPrint.aspx.vb" Inherits="AdvWeb.AD.LeadNotesPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Lead Notes</title>

    <link href="../css/LeadNotes.css" rel="stylesheet" />
    <script src="../Kendo/js/jquery.min.js"></script>
    <script src="../Kendo/js/kendo.all.min.js"></script>
    <script src="../Scripts/Advantage.Client.AD.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <article id="printInfoPage" style="width: 7.5in;">
            <div id="printWrapper">
                <div id="printHeaderRight">
                    <div id="printCampus">Campus</div>
                    <div id="printModule">LEAD NOTES</div>
                    <div id="printDateTime">--/--/--</div>
                </div>

                <div class="clearfix"></div>
                <br />
                <label class="hrcol1">Lead:</label>
                <input class="hrcol2" id="hrLeadFullName" readonly="readonly" />
                <label class="hrcol3">Admissions Rep:</label>
                <input class="hrcol4" id="hrAdmissionsRep" readonly="readonly" />
                <div class="clearfix"></div>
                <label class="hrcol1">Status:</label>
                <input class="hrcol2" id="hrStatus" readonly="readonly" />
                <label class="hrcol3">Date Assigned</label>
                <input class="hrcol4" id="hrDateAssigned" readonly="readonly" />
                <div class="clearfix"></div>
                <label class="hrcol1">Program Version:</label>
                <input class="hrcol2" id="hrProgramVersion" readonly="readonly" />
                <div class="clearfix"></div>
                <label class="hrcol1">Expected Start:</label>
                <input class="hrcol2" id="hrExpectedStartDate" readonly="readonly" />
                <div class="clearfix"></div>
                <br />

                <div id="corpusReport" style="width: 7.5in; border: 2px solid gainsboro">
                    <table id="LeadNotes" class="LNStyle">
                    </table>
                </div>
            </div>
        </article>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {   
            var moduleCode = "AD";
            var manager = new AD.LeadNotesPrint(moduleCode);
        });

    </script>
</body>
</html>
