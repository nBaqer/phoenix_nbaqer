﻿<%@ Page Title="Post Applicant Fee" Language="VB" MasterPageFile="~/NewSite.master"
    AutoEventWireup="false" CodeFile="PostApplicantFee.aspx.vb" Inherits="PostApplicantFees" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register TagPrefix="FAME" TagName="LeadSearch" Src="../UserControls/SearchControls/LeadSearchControl.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link href="../CSS/StudentSearchControl.css" rel="stylesheet" type="text/css" />
    <title>Post Fee</title>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="both"
            Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False">
                                    
                                    </asp:Button>
                                  
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                </td>
                                
                            </tr>
                        </table>                      
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                            <table style="margin-left: 100px;" width="800px" cellpadding="0" cellspacing="0"
                                                class="contenttable" border="0">
                                                <tr>
                                                    <td colspan="2">                                                            
                                                        <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                                        <asp:CheckBox ID="chkIsEnrolled" runat="server" Visible="False"></asp:CheckBox>
                                                        <asp:CheckBox ID="chkIsVoided" runat="server" Visible="False"></asp:CheckBox>
                                                        <asp:TextBox ID="txtLeadTransactionId" runat="server" Visible="False" Wrap="False">txtLeadTransactionId</asp:TextBox>
                                                        <asp:TextBox ID="txtLeadId" runat="server" Visible="False" Wrap="False">txtLeadId</asp:TextBox>
                                                        <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                                        <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                                        <asp:TextBox ID="txtLeadName" runat="server" Visible="False">LeadName</asp:TextBox>
                                                        <asp:TextBox ID="txtTransTypeId" runat="server" Visible="False"></asp:TextBox>
                                                        <asp:TextBox ID="txtPaymentReference" runat="server" Visible="False"></asp:TextBox>
                                                        <asp:TextBox ID="txtCampusId" runat="server" Visible="False"></asp:TextBox>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                     
                                                    <td valign="top" style="width: 650px;">
                                                        <div class="contentmargin">        
                                                                                                          
                                                                <asp:Label ID="lblLead" runat="server" CssClass="label">Lead Name<font color="red">*</font></asp:Label>                                                            
                                                        </div>
                                                        <div class="contentmargin">                                                            
                                                                <FAME:LeadSearch ID="LeadSearch" runat="server" OnTransferLeadSearch="TransferLeadSearch" />                                                           
                                                        </div>                                                       
                                                        <div class="contentmargin">                                                           
                                                                <asp:Label ID="lblTransCodeId" runat="server" CssClass="label">Transaction Code<font color="red">*</font></asp:Label>                                                            
                                                        </div>
                                                        <div class="contentmargin">                                                            
                                                                <telerik:RadComboBox ID="ddlTransCodeId" TabIndex="3" runat="server" Width="325px"
                                                                    ForeColor="Black" BackColor="White">
                                                                </telerik:RadComboBox>
                                                                <asp:CompareValidator ID="TransCodeCompareValidator" runat="server" Display="None"
                                                                    ErrorMessage="Transaction Code is Required" ControlToValidate="ddlTransCodeId"
                                                                    Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Transaction Code is Required</asp:CompareValidator>                                                           
                                                        </div>
                                                        <div class="contentmargin">                                                            
                                                                <asp:Label ID="lblTransDescrip" runat="server" CssClass="label">Description<font color="red">*</font></asp:Label>                                                            
                                                        </div>
                                                        <div class="contentmargin">                                                            
                                                                <asp:TextBox ID="txtDescrip" runat="server" TabIndex="4" Width="330px" CssClass="textbox"></asp:TextBox>                                                                                                                         
                                                        </div>                                                                                                            
                                                        <div class="contentmargin">                                                            
                                                                <asp:Label ID="lblTransReference" runat="server" CssClass="label">Reference</asp:Label>                                                            
                                                        </div>
                                                        <div class="contentmargin">                                                            
                                                                <asp:TextBox ID="txtTransReference" runat="server" TabIndex="5" Width="330px" CssClass="textbox"></asp:TextBox>                                                           
                                                        </div>                                                       
                                                        <div class="contentmargin">                                                            
                                                                <asp:Label ID="lblPaymentTypeId" runat="server" CssClass="label">Payment Type<font color="red">*</font></asp:Label>                                                            
                                                        </div>
                                                        <div class="contentmargin">                                                            
                                                                <telerik:RadComboBox ID="ddlPaymentTypeId" runat="server" Width="325px" AutoPostBack="True"
                                                                    ForeColor="Black" BackColor="White" TabIndex="11">
                                                                </telerik:RadComboBox>
                                                                <asp:CompareValidator ID="PaymentTypeCompareValidator" runat="server" Display="None"
                                                                    ErrorMessage="Payment Type is Required" ControlToValidate="ddlPaymentTypeId"
                                                                    Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Payment Type is Required</asp:CompareValidator>
                                                         </div>                                                         
                                                        <div class="contentmargin" runat="server" visible="false" id="divlbReference">                                                            
                                                                <asp:Label ID="lblTypeOfReference" runat="server" CssClass="label">Reference Type</asp:Label>                                                            
                                                        </div>
                                                        <div class="contentmargin" runat="server" visible="false" id="divtxtReference">                                                            
                                                                <asp:TextBox ID="txtPaymentReferenceNumber" runat="server" MaxLength="50" CssClass="textboxAdv"
                                                                    TabIndex="13" Width="330px"></asp:TextBox>
                                                                <%--   <asp:RequiredFieldValidator ID="rfvPaymentReferenceNumber" runat="server" Display="None" ErrorMessage="Payment Type Reference is Required" 
                                                                        ControlToValidate="txtPaymentReferenceNumber">Payment Type Reference is Required</asp:RequiredFieldValidator>--%>                                                            
                                                        </div>
                                                        <div class="contentmargin">                                                            
                                                                <asp:Label ID="lblTransDate" runat="server" CssClass="label">Transaction Date<font color="red">*</font></asp:Label>                                                            
                                                        </div>
                                                        <div class="contentmargin">                                                            
                                                                <telerik:RadDatePicker runat="server" ID="txtTransDate" DatePopupButton-ToolTip=""
                                                                    TabIndex="14" Calendar-ShowRowHeaders="false">
                                                                </telerik:RadDatePicker>
                                                                <asp:RangeValidator ID="drvTransactionDate" runat="server" Display="None" ErrorMessage="Date is Invalid or outside allowed range"
                                                                    ControlToValidate="txtTransDate" Type="Date" MaximumValue="2049/12/31" MinimumValue="1945/01/01">Date is Invalid or outside allowed range</asp:RangeValidator>                                                           
                                                        </div>
                                                        <div class="titlemargin">                                                            
                                                                <asp:Label ID="lblTransAmount" runat="server" CssClass="label">Amount<font color="red">*</font></asp:Label>                                                            
                                                        </div>
                                                        <div class="contentmargin">                                                            
                                                                <asp:TextBox ID="txtTransAmount" runat="server" CssClass="textbox" AutoPostBack="true"
                                                                    TabIndex="15"></asp:TextBox>
                                                                <%-- <telerik:RadTextBox ID="txtTransAmount" runat="server" CssClass="textboxamountreqfield"
                                                                                    AutoPostBack="true" TabIndex="9">
                                                                                </telerik:RadTextBox>  --%>
                                                                <span class="label">
                                                                    <%=MyAdvAppSettings.AppSettings("Currency")%>
                                                                </span>
                                                                <asp:RequiredFieldValidator ID="rfvTransAmount" runat="server" Display="None" ErrorMessage="Transaction Amount is Required"
                                                                    ControlToValidate="txtTransAmount">Transaction Amount is Required</asp:RequiredFieldValidator>
                                                                </div>
                                                        <br />
                                                        <div class="contentmargin">                                                            
                                                                <asp:Button ID="btnSaveAndPrint" runat="server" Width="160px" Text="Save and Print Receipt" TabIndex="18" />                                                            
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <!--end validation panel-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>
