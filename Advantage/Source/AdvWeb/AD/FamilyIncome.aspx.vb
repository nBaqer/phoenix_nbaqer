Imports System.Diagnostics
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports System.IO
Imports Advantage.Business.Objects
Imports System.Collections

Partial Class FamilyIncome
    Inherits BasePage
    Protected WithEvents btnhistory As Button
    Protected WithEvents rvViewOrder As RangeValidator
    Protected viewOrderLabel As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Dim ReadOnly cmpCallReportingAgencies As New ReportingAgencies
    Protected intRptFldId, intResultsAffected As Integer
    Protected boolRptAgencyAppToSchool As Boolean

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim objCommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        Dim ds As  DataSet
        Dim sw As New StringWriter
        Dim campusId As String
        'Dim userId As String
        Dim mContext As HttpContext
        Dim resourceId As Integer
        Dim advantageUserState As User = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        'userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'Check if this page still exists in the menu while switching campus
        'Dim boolSwitchCampus As Boolean = False
        'Dim boolPageStillPartofMenu As Boolean = False
 
        Try
            If Not Page.IsPostBack Then
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                mContext = HttpContext.Current
                txtResourceId.Text = CInt(mContext.Items("ResourceId"))
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                txtFamilyIncomeId.Text = Guid.NewGuid.ToString()
                ds = (New LeadFacade).GetAllFamilyIncome("All")

                PopulateDataList(ds)

                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            End If

            If lblViewOrder.Text.IndexOf("*") <> -1 Then
                'Remove * character from lblViewOrder.Text
                viewOrderLabel = lblViewOrder.Text.Substring(0, lblViewOrder.Text.Length - 1)
            Else
                viewOrderLabel = lblViewOrder.Text
            End If

            boolRptAgencyAppToSchool = AdvantageCommonValues.CheckIfReportingAgenciesExistForSchool
            If boolRptAgencyAppToSchool = True Then
                intRptFldId = AdvantageCommonValues.IncomeLevel

                pnlReportingAgenciesHeader.Visible = True

                'Call the Generate Reporting Agencies function in Reporting Agencies component
                cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtFamilyIncomeId.Text, "IncomeLevel")
            Else
                pnlReportingAgenciesHeader.Visible = False
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load: " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load: " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BtnSaveClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim objCommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        Dim ds As  DataSet
        Dim sw As New StringWriter
        Dim msg As String = String.Empty
        Dim leadFac As New LeadFacade
        Dim dtCounter As  DataTable
        Dim dtList As  DataTable
        Dim strList As String = String.Empty
        Dim dsDup As DataSet = leadFac.CheckDuplicateFamilyIncome(Convert.ToInt32(txtViewOrder.Text))
        Dim instrLtTag As Integer = InStr(lblViewOrder.Text, "<")

        'Validate ViewOrder.
        If dsDup.Tables.Count = 2 Then
            dtCounter = dsDup.Tables(0)
            dtList = dsDup.Tables(1)
            If dtList.Rows.Count > 0 Then
                For Each dr As DataRow In dtList.Rows
                    strList &= CType((dr("ViewOrder") & ", "), String)
                Next
                strList = strList.Substring(0, strList.Length - 2)
            End If
            If dtCounter.Rows.Count > 0 Then
                msg = Mid(lblViewOrder.Text, 1, instrLtTag - 1) & "  " & txtViewOrder.Text & " already exist." & vbCr & _
                      "Please select " & Mid(lblViewOrder.Text, 1, instrLtTag - 1) & " NOT in list (" & strList & ")."
            End If
            '
            Try
                If ViewState("MODE") = "NEW" Then
                    If dtCounter.Rows.Count <> 0 Then
                        'View Order already exists, so it cannot be used.
                        DisplayErrorMessage(msg)
                        'Dim objWebUtil As New CommonWebUtilities
                        CommonWebUtilities.SetFocus(Page, txtViewOrder)
                        Exit Sub
                    End If

                    'Set the Primary Key value, so it can be used later by the DoUpdate method
                    objCommon.PagePK = txtFamilyIncomeId.Text
                    msg = objCommon.DoInsert(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                    If msg <> "" Then
                        DisplayErrorMessage(msg)
                    End If

                    txtRowIds.Text = objCommon.PagePK

                    'Set Mode to EDIT since a record has been inserted into the db.
                    objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                    ViewState("MODE") = "EDIT"
                    ds = leadFac.GetAllFamilyIncome("All")
                    dlstIncomes.SelectedIndex = -1
                    PopulateDataList(ds)
                    ds.WriteXml(sw)
                    ViewState("ds") = sw.ToString()


                ElseIf viewstate("MODE") = "EDIT" Then
                    If dtCounter.Rows.Count <> 0 Then
                        If dtCounter.Rows(0)("FamilyIncomeId").ToString <> txtFamilyIncomeId.Text Then
                            'View Order already exists, so it cannot be used.
                            DisplayErrorMessage(msg)
                            'Dim objWebUtil As New CommonWebUtilities
                            CommonWebUtilities.SetFocus(Page, txtViewOrder)
                            Exit Sub
                        End If
                    End If
                    '
                    msg = objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                    If msg = "" Then
                        ds = (New LeadFacade).GetAllFamilyIncome("All")
                        dlstIncomes.SelectedIndex = -1
                        PopulateDataList(ds)
                        ds.WriteXml(sw)
                        ViewState("ds") = sw.ToString()
                    Else
                        DisplayErrorMessage(msg)
                    End If
                End If
                Dim arrReportingAgencyValue As ArrayList
                arrReportingAgencyValue = cmpCallReportingAgencies.GetAllValues(pnlReportingAgencies)
                ' DE 1131 Janet Robinson 04/25/2011
                Try
                    'DE8804
                    'intResultsAffected = cmpCallReportingAgencies.InsertValues(txtFamilyIncomeId.Text, arrReportingAgencyValue, "syFamilyIncome")
                    intResultsAffected = cmpCallReportingAgencies.InsertValues(txtFamilyIncomeId.Text, arrReportingAgencyValue, "syFamilyIncome", False)
                    Dim sReportAgencies As String
                    'Dim iResult As Integer
                    sReportAgencies = AdvantageCommonValues.GetReportingAgenciesApplicableToSchool
                    If sReportAgencies.Contains(CType(1, String)) Then '1 is Agency value for IPEDS 
                        Dim buildIpeDsDB As New ReportingAgencyFacade
                        buildIpeDsDB.UpdateIPEDSValues(CType(arrReportingAgencyValue(0).ToString, Integer), txtFamilyIncomeId.Text, "syFamilyIncome")
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
                CommonWebUtilities.RestoreItemValues(dlstIncomes, txtFamilyIncomeId.Text)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub btnSave_Click " & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End If
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub

    Private Sub BtnDeleteClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim sw As New StringWriter
        Dim msg As String
        Dim strValueId As String
        Try
            msg = objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            If msg = "" Then
                ' DE9570 6/4/2013 Janet Robinson
                strValueId = txtFamilyIncomeId.Text
                ClearRhs()
                ds = (New LeadFacade).GetAllFamilyIncome("All")
                dlstIncomes.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, strValueId)
                txtFamilyIncomeId.Text = Guid.NewGuid.ToString()
            Else
                DisplayErrorMessage(msg)
            End If
            CommonWebUtilities.RestoreItemValues(dlstIncomes, Guid.Empty.ToString)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRhs()
        Dim ctl As Control

        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BtnNewClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New StringReader(CStr(ViewState("ds")))

        Try
            ClearRhs()
            ds.ReadXml(sr)
            ' DE1131 Janet Robinson 06/20/2011 Stop transfer to error page if no family income records exist and New is pressed
            If ds.Tables.Count > 0 Then
                PopulateDataList(ds)
            End If

            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"

            txtFamilyIncomeId.Text = Guid.NewGuid.ToString()
            cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, txtFamilyIncomeId.Text)
            CommonWebUtilities.RestoreItemValues(dlstIncomes, Guid.Empty.ToString)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub DlstIncomesItemCommand(ByVal sender As Object, ByVal e As DataListCommandEventArgs) Handles dlstIncomes.ItemCommand
        Dim ds As  DataSet
        'Dim sr As New StringReader(CStr(ViewState("ds")))
        Dim strGuid As String
        Dim objCommon As New CommonUtilities

        Try
            strGuid = dlstIncomes.DataKeys(e.Item.ItemIndex).ToString()
            txtRowIds.Text = e.CommandArgument.ToString
            objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGuid)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            dlstIncomes.SelectedIndex = e.Item.ItemIndex
            ds = (New LeadFacade).GetAllFamilyIncome("All")
            PopulateDataList(ds)

            Try
                cmpCallReportingAgencies.GenerateReporingAgencies(pnlReportingAgencies, intRptFldId, strGuid, "FamilyIncome")
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            CommonWebUtilities.RestoreItemValues(dlstIncomes, CType(e.CommandArgument, String))
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstIncomes_ItemCommand " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstIncomes_ItemCommand " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    'Private Sub ChkStatusCheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim objListGen As New DataListGenerator
    '    Dim ds As New DataSet
    '    Dim objCommon As New CommonUtilities

    '    Try
    '        ClearRhs()
    '        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
    '        Viewstate("MODE") = "NEW"
    '        txtFamilyIncomeId.Text = Guid.NewGuid.ToString()
    '        ds = (New LeadFacade).GetAllFamilyIncome("All")
    '        PopulateDataList(ds)

    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        'Redirect to error page.
    '        If ex.InnerException Is Nothing Then
    '            Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
    '        Else
    '            Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
    '        End If
    '        Response.Redirect("../ErrorPage.aspx")
    '    End Try
    'End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was refactored by AVP on 6/22/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radStatus.SelectedItem.Text.ToLower = "active") Or (radStatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radStatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        Dim dv As New DataView(ds.Tables(0), sDataFilter, "ViewOrder", DataViewRowState.CurrentRows)

        With dlstIncomes
            .DataSource = dv
            .DataBind()
        End With
        dlstIncomes.SelectedIndex = -1
    End Sub

    Private Sub RadStatusSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radStatus.SelectedIndexChanged
        'Dim objListGen As New DataListGenerator
        Dim ds As  DataSet
        Dim objCommon As New CommonUtilities

        Try
            ClearRhs()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            Viewstate("MODE") = "NEW"
            txtFamilyIncomeId.Text = Guid.NewGuid.ToString
            ds = (New LeadFacade).GetAllFamilyIncome("All")
            PopulateDataList(ds)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender 
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList() 
        'add save button 
        controlsToIgnore.Add(btnSave)
       'Add javascript code to warn the user about non saved changes 
        'CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BIndToolTip()
    End Sub

End Class
