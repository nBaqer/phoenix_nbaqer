﻿
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports FAME.Advantage.Common

Partial Class ImportLeads
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Dim userId As String
    Dim resourceId As Integer
    Protected mContext As HttpContext
    Protected myAdvAppSettings As AdvAppSettings
    
    Protected Sub BtnUploadClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim regDB As New LeadEnrollmentFacade
        Dim strResult As String = ""
        Dim errorMessage As String = ""
        Dim boolDistribute As Boolean = False
        Dim rand As New Random
        Dim strRnd As String = rand.Next.ToString
        If radAssignLeads.SelectedValue = "0" Then
            boolDistribute = False
        Else
            boolDistribute = True
        End If
        ''Added by Saraswathi Lakshmanan on Nov 11 2008
        ''Distribute the leads to the currently online admission reps
        ''If there are no admiReps Online do not import the leads
        If boolDistribute = True Then
            ''Added by Saraswathi Lakshmanan on Nov 5th 2008
            Dim intAdmRepCount As Integer
            intAdmRepCount = regDB.GetonlineAdmissionRepsCount(campusId)
            If intAdmRepCount = 0 Then
                errorMessage = "There are no Admission Reps Currently Online. Data not Imported."
                'DisplayErrorMessage(errorMessage)
                DisplayRADAlert(CallbackType.Postback, "Error1", errorMessage, "Upload Error")
                Return
            End If
        End If

        'If Not txtUpLoad.PostedFile Is Nothing And Not txtUpLoad.Value = "" Then
        '    strResult = (regDB).ImportAllLeads(txtUpLoad.Value, Session("UserName"), campusId, userId, boolDistribute, strRnd)
        'End If

        Dim strFileName As String = ""
        If Not uploadExcelFile.PostedFile Is Nothing And Not uploadExcelFile.FileName = "" Then
            Try
                strFileName = myAdvAppSettings.AppSettings("ImportLeadsFilePath") + uploadExcelFile.FileName
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                DisplayErrorMessage("The file path was not found")
                DisplayRADAlert(CallbackType.Postback, "Error2", "The file path was not found", "Upload Error")
                Exit Sub
            End Try
        End If

        If Not strFileName = "" Then
            If myAdvAppSettings.AppSettings("ImportLeadsForUnitek") = "no" Then
                strResult = (regDB).ImportAllLeads(strFileName, AdvantageSession.UserState.UserName, campusId, userId, boolDistribute, strRnd)
            Else
                strResult = (regDB).ImportAllLeadsforUnitek(strFileName, AdvantageSession.UserState.UserName, campusId, userId, boolDistribute, strRnd)
            End If

        End If


        'Dim result() As String
        'Dim int1 As Integer = 0
        'Dim res1 As String = ""
        'Dim res2 As String = ""
        'Dim res3 As String = ""
        'Dim strRemaining As String = ""
        'int1 = InStr(strResult.ToString, ":")
        'res1 = Mid(strResult.ToString, int1, 2)
        Try
            Dim dt As DataTable = (New LeadEnrollmentFacade).GetLeadSummaryByAdmissionsRep(strRnd)
            'res2 = dt.Rows.Count
            DataGrid1.Visible = True
            DataGrid1.DataSource = dt
            DataGrid1.DataBind()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DataGrid1.Visible = False
        End Try

        'lblSummary.Text &= "Number of leads in the import file:" & res1 & vbCrLf
        'lblSummary.Text &= "Number of leads added to advantage:" & res2 & vbCrLf
        'lblSummary.Text &= "Number of leads not added to advantage:" & (res1 - res2) & vbCrLf
    End Sub
    Private Sub BindDatagrid(ByVal strRnd As String)

    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Sub ImportLeads_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        '  If Not Page.IsPostBack Then
        'campusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'userId = Session("UserId")
        ' End If

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState


        campusid = Master.CurrentCampusId 
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId2)

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub

End Class