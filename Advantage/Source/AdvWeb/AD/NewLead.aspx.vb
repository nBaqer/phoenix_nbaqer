Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common.TM
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports System.Data
Imports System.Collections
Imports System.Collections.Generic
Imports Microsoft.Ajax.Utilities
Imports System.Windows.Forms.VisualStyles
Imports BO = Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer
Imports FAME.AdvantageV1.BusinessFacade.TM

Partial Class NewLead
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtEmployerContactId As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEmployerId As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblTitleId As System.Web.UI.WebControls.Label
    Protected WithEvents ddlTitleId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblWorkPhone As System.Web.UI.WebControls.Label
    Protected WithEvents txtWorkPhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblHomePhone As System.Web.UI.WebControls.Label
    Protected WithEvents lblCellPhone As System.Web.UI.WebControls.Label
    Protected WithEvents txtCellPhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEmails As System.Web.UI.WebControls.Label
    Protected WithEvents lblBeeper As System.Web.UI.WebControls.Label
    Protected WithEvents txtBeeper As System.Web.UI.WebControls.TextBox

    ' Protected EmployerContactId As String = "03D26D83-7814-4BCF-AB80-0F429C691D9B"
    Protected WithEvents ChkStatus As System.Web.UI.WebControls.CheckBox
    Protected WithEvents dlstEmployerContact As System.Web.UI.WebControls.DataList

    Protected WithEvents RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator3 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator4 As System.Web.UI.WebControls.RegularExpressionValidator

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblState As System.Web.UI.WebControls.Label
    Protected WithEvents txtState As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblExt As System.Web.UI.WebControls.Label
    Protected WithEvents txtExtension As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblBestTime As System.Web.UI.WebControls.Label
    Protected WithEvents txtBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblPinNumber As System.Web.UI.WebControls.Label
    Protected WithEvents txtPinNumber As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblWorkExt As System.Web.UI.WebControls.Label
    Protected WithEvents txtWorkExt As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblWorkBestTime As System.Web.UI.WebControls.Label
    Protected WithEvents txtWorkBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblHomeBestTime As System.Web.UI.WebControls.Label
    Protected WithEvents txtHomeBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCellBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRowIds As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtResourceId As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents Dropdownlist1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblAdmissionsRepID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlAdmissionsRepID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblPhoneStatusID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlPhoneStatusID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblNationalityID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlNationalityID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblDriverLicenseState As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCitizenID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlDriverLicenseNumber As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblCountyID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCountyID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlAddressStateID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtPhone1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblType As System.Web.UI.WebControls.Label
    Protected WithEvents ddlPhoneTypeID As System.Web.UI.WebControls.DropDownList
    Protected CampusId As String
    Protected WithEvents lbl2 As System.Web.UI.WebControls.Label
    Protected WithEvents chkForeign As System.Web.UI.WebControls.CheckBox
    Protected WithEvents valCompare As System.Web.UI.WebControls.CompareValidator
    Protected m_context As HttpContext
    Protected strSourceDetails As String
    Protected strMask As String
    Protected zipMask As String
    Dim ssnMAsk As String
#End Region

    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    'Dim newBox As Messagebox = New Messagebox()
    Private useSaveNext As Boolean = False
    Private pObj As New UserPagePermissionInfo
    Dim userId As String
    Protected ModuleId As String
    Protected ResourceID As Integer
    Public strScript1, strScript As String
    Protected strDefaultCountry As String
    Protected isIPEDSApplicable As String

    Private mruProvider As MRURoutines
    Protected LeadId, StudentId As String

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub





    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sdfControls As New SDFComponent
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        'Dim objCommon As New CommonUtilities
        'Dim campusId2 As String
        '  Dim m_Context2 As HttpContext
        'Dim fac As New FAME.AdvantageV1.BusinessFacade.UserSecurityFacade
        'Dim resourceId2 As Integer
        'Dim facInputMasks As New FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade

        CampusId = Master.CurrentCampusId ' AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ViewState("CampusID") = CampusId
        
        ResourceID = CInt(HttpContext.Current.Request.Params("resid"))

        'campusId2 = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString

        ''''' Code Added on 18th August to fix mantis issue id 19555
        ddlCampusId.Attributes.Add("onchange", "verifycampuschange('" + CampusId + "')")
        '''''''''''''''''

        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString
        'pObj = fac.GetUserResourcePermissions(userId, resourceId2, campusId2)
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceID, CampusId)

        txtUserId.Value = userId
        ''''' Code Commented on 18th August to fix mantis issue id 19555
        ''txtCampusId.Value = campusId2
        ''''''''''''''''''''
        'btnCheckDuplicates.Attributes.Add("onclick", "CheckDuplicates1();return false;")

        txtDate.Value = Date.Now.ToShortDateString
        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString

        'Check if this page still exists in the menu while switching campus
        Dim boolPageStillPartofMenu As Boolean = False

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            chkIsInDB.Checked = False
            CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).ApplyValidation()
            'lblPhone.Text = "Phone 1"
            ViewState("MODE") = "NEW"

            ''''' Code Modified on 18th August to fix mantis issue id 19555
            'campusid = Master.CurrentCampusId 
            Me.BuildCampusDdl()
            '''''''''''''''''''''''
            BuildDropDownLists()

            'Dim vsiValue As String
            'Try
            '    vsiValue = Request.QueryString("VSI")
            'Catch ex As Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    vsiValue = ""
            'End Try

            ''''' Code Modified on 18th August to fix mantis issue id 19555
            'Try
            '    Try
            '        If Session(vsiValue.ToString & "CurrentCampus") Is Nothing Or _
            '         String.IsNullOrEmpty(Session(vsiValue & "CurrentCampus").ToString) Then
            '            ddlCampusId.SelectedValue = CampusId
            '        Else
            '            ddlCampusId.SelectedValue = Session(vsiValue & "CurrentCampus").ToString
            '        End If
            '    Catch ex As Exception
             '    	Dim exTracker = new AdvApplicationInsightsInitializer()
            '    	exTracker.TrackExceptionWrapper(ex)

            '        ddlCampusId.SelectedValue = CampusId
            '        Session(vsiValue.ToString & "CurrentCampus") = CampusId
            '    End Try

            'Catch ex As Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)


            'End Try
            ddlCampusId.SelectedValue = Master.CurrentCampusId
            txtCampusId.Value = CampusId
            ''''''''

            'bind an empty new LeadMasterInfo
            BindLeadMasterDataItemCommand(New LeadMasterInfo)

            txtAssignedDate.SelectedDate = Date.Now.ToShortDateString
            txtDateApplied.SelectedDate = Date.Now.ToShortDateString
            txtModDate.Value = Date.Today.ToString
            Session("ScrollValue") = 0
            Try
                'Code changes by Atul Kamble on 08-Oct-2010
                If AdvantageSession.UserState.IsUserSA Or AdvantageSession.UserState.IsUserSupport Or AdvantageSession.UserState.IsUserSuper Then
                    ddlAdmissionsRep.SelectedIndex = 0
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlAdmissionsRep.SelectedIndex = 0
            End Try

            Try
                ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText("Unknown"))
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlGender.SelectedIndex = 0
            End Try

            Try
                ddlAddressType.SelectedIndex = ddlAddressType.Items.IndexOf(ddlAddressType.Items.FindByText("Home"))
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlAddressType.SelectedIndex = 0
            End Try

            Try
                ddlAdmissionsRep.SelectedValue = userId
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlAdmissionsRep.SelectedIndex = 0
            End Try

            'If pObj.HasFull Or pObj.HasAdd Then
            '    btnNew.Enabled = True
            '    btnSave.Enabled = True
            '    btnSaveNext.Enabled = True
            'End If
            InitButtonsForLoad()
        Else
            CType(Master.FindControl("ContentMain2"), ContentPlaceHolder).ApplyValidation(chkForeignZip.Checked, chkForeignPhone.Checked, chkForeignPhone2.Checked)
        End If

        'GetInputMaskValue()

        'Check if School is using IPEDS
        'CodeConstructType commented by balaji on 04/15/2009 to fix issue 12013
        'IPEDSRequirements()

        'For DDLs to work
        'If Not IsPostBack Then
        '    BuildProgramGroupDDL()
        '    BuildSourceCatagoryDDL()
        '    Me.__hiddensubmit.Value = "yes"
        '    Session("loadfirsttime") = "no"
        'Else
        '    If Not ddlAreaId.Value = "" Then
        '        Session("AreaId") = ddlAreaId.Value
        '    End If
        '    Session("programid") = Me.__programid.Value
        '    Session("prgverid") = Me.__prgverid.Value

        '    Session("sourcecategoryid") = ddlSourceCategoryId.Value
        '    Session("sourcetypeid") = Me.__sourcetypeid.Value
        '    Session("sourceadvid") = Me.__sourceadvid.Value
        'End If
        ''save ddl value to hidden field 
        'Me.ddlProgramID.Attributes.Add("onchange", "setHiddenfieldValue('__programid',this.value);")
        'Me.ddlPrgVerId.Attributes.Add("onchange", "setHiddenfieldValue('__prgverid',this.value);")

        'Me.ddlSourceTypeId.Attributes.Add("onchange", "setHiddenfieldValue('__sourcetypeid',this.value);")
        'Me.ddlSourceAdvertisement.Attributes.Add("onchange", "setHiddenfieldValue('__sourceadvid',this.value);")

        If chkForeignZip.Checked = True Then
            txtOtherState.Visible = True
            lblOtherState.Visible = True
            ddlStateID.Enabled = False
            ddlStateID.SelectedIndex = 0
        Else
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            ddlStateID.Enabled = True
        End If

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            lblEntranceInterviewDate.Visible = True
            txtEntranceInterviewDate.Visible = True
            ' Img7.Visible = True
            lblHighSchoolProgramCode.Visible = True
            txtHighSchoolProgramCode.Visible = True
            SetRegentRequiredFields()
        End If


        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfControls.GetSDFExists(ResourceID, ModuleId)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtPKID.Value) <> "" Then
            sdfControls.GenerateControlsEdit(MyBase.FindControlRecursive("pnlSDF"), ResourceID, txtPKID.Value, "AD")
        Else
            sdfControls.GenerateControlsNew(MyBase.FindControlRecursive("pnlSDF"), ResourceID, "AD")
        End If

        ''''' Code Commented on 18th August to fix mantis issue id 19555
        ''ddlCampusId.SelectedValue = campusId2
        '''''''''
        'Header1.EnableHistoryButton(False)



        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        If dup.Value = "yes" And useSaveNext = False Then
            AddLead()
            Exit Sub
        ElseIf dup.Value = "yes" And useSaveNext = True Then
            AddLead()
            Exit Sub
        End If
    End Sub
    'Private Function ReloadURLForCampus(ByVal VSI As String, ByVal OriginalURL As String, ByVal URLPath As String) As String
    '    Dim nameValues = HttpUtility.ParseQueryString(OriginalURL)
    '    nameValues.[Set]("VSI", VSI)
    '    Dim url As String = URLPath
    '    Dim updatedQueryString As String = "?" + nameValues.ToString()
    '    Dim newURL As String = url + updatedQueryString
    '    Return newURL
    'End Function

    Private Sub SetRegentRequiredFields()
        'txtSSN.BackColor = Color.FromName("#ffff99")
        lblSSN.Text = lblSSN.Text & "<font color=""red"">*</font>"
        'txtBirthDate.BackColor = Color.FromName("#ffff99")
        lblBirthDate.Text = lblBirthDate.Text & "<font color=""red"">*</font>"
    End Sub

    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    'Private Function GetInputMaskValue() As String
    '    Dim facInputMasks As New FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade
    '    '        Dim correctFormat As Boolean
    '    Dim strMask As String
    '    Dim zipMask As String
    '    '   Dim errorMessage As String
    '    '   Dim strPhoneReq As String
    '    '   Dim strZipReq As String
    '    '   Dim strFaxReq As String
    '    Dim objCommon As New CommonUtilities
    '    Dim ssnMAsk As String

    '    'Get The Input Mask for Phone/Fax and Zip
    '    strMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Phone)
    '    zipMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Zip)
    '    ssnMAsk = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.SSN)


    '    ' txtSSN.Mask = Replace(ssnMAsk, "#", "9")
    '    lblSSN.ToolTip = ssnMAsk

    '    'Replace The Mask Character from # to 9 as Masked Edit TextBox 
    '    'accepts only certain characters as mask characters
    '    If chkForeignPhone.Checked = False Then
    '        ' txtPhone.Mask = Replace(strMask, "#", "9")
    '        'txtphone.Mask = "(###)-###-####"
    '        'txtphone.DisplayMask = "(###)-###-####"
    '        'lblPhone.ToolTip = strMask
    '    Else
    '        'txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
    '        'txtphone.DisplayMask = ""
    '        'lblPhone.ToolTip = ""
    '    End If

    '    If chkForeignPhone2.Checked = False Then
    '        'txtPhone2.Mask = "(###)-###-####'
    '        'txtPhone2.DisplayMask = "(###)-###-####"
    '        'lblPhone2.ToolTip = strMask
    '    Else
    '        'txtPhone2.Mask = "aaaaaaaaaaaaaaaaaaaa"
    '        'txtPhone2.DisplayMask = ""
    '        'lblPhone2.ToolTip = ""
    '    End If

    '    If chkForeignZip.Checked = False Then
    '        ' txtZip.Mask = Replace(zipMask, "#", "9")
    '        'txtzip.Mask = "#####"
    '        'lblZip.ToolTip = zipMask
    '    Else
    '        'txtzip.Mask = "aaaaaaaaaaaaaaaaaaaa"
    '        'lblZip.ToolTip = ""
    '    End If

    '    'Get The RequiredField Value
    '    'Dim strSSNReq As String
    '    'strPhoneReq = objCommon.SetRequiredColorMask("Phone")
    '    'strZipReq = objCommon.SetRequiredColorMask("Zip")
    '    'strSSNReq = objCommon.SetRequiredColorMask("SSN")

    '    'If The Field Is Required Field Then Color The Masked
    '    'Edit Control
    '    'If strSSNReq = "Yes" Then
    '    '    txtSSN.BackColor = Color.FromName("#ffff99")
    '    'End If
    '    'If strPhoneReq = "Yes" Then
    '    '    txtPhone.BackColor = Color.FromName("#ffff99")
    '    'End If
    '    ''If InStr(strZipReq, "Y") >= 1 Then
    '    'If strZipReq = "Yes" Then
    '    '    txtZip.BackColor = Color.FromName("#ffff99")
    '    'End If

    '    'Try
    '    '    If SingletonAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
    '    '        txtSSN.BackColor = Color.FromName("#ffff99")
    '    '        txtBirthDate.BackColor = Color.FromName("#ffff99")
    '    '    End If
    '    'Catch ex As System.Exception
     '    '	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    '	exTracker.TrackExceptionWrapper(ex)

    '    'End Try
    'End Function
    'Private Function ValidateFieldsWithInputMasks() As String
    '    Dim facInputMasks As New FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade
    '    Dim correctFormat As Boolean
    '    Dim strMask As String
    '    Dim zipMask As String
    '    Dim errorMessage As String = ""
    '    Dim ssnMask As String

    '    strMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Phone)
    '    zipMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Zip)
    '    ssnMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.SSN)
    '    'Validate the phone field format. If the field is empty we should not apply the mask
    '    'agaist it.

    '    If txtSSN.Text <> "" Then
    '        correctFormat = facInputMasks.ValidateStringWithInputMask(ssnMask, txtSSN.Text)
    '        If correctFormat = False Then
    '            errorMessage = "Incorrect format for  ssn field." & vbCr
    '        End If
    '    End If

    '    If txtphone.Text <> "" And chkForeignPhone.Checked = False Then
    '        correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtphone.Text)
    '        If correctFormat = False Then
    '            errorMessage &= "Incorrect format for  phone field." & vbCr
    '        End If
    '    End If
    '    If txtPhone2.Text <> "" And chkForeignPhone2.Checked = False Then
    '        correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtPhone2.Text)
    '        If correctFormat = False Then
    '            errorMessage &= "Incorrect format for  phone field." & vbCr
    '        End If
    '    End If
    '    'Validate the zip field format. If the field is empty we should not apply the mask
    '    'against it.
    '    If txtzip.Text <> "" And chkForeignZip.Checked = False Then
    '        correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtzip.Text)
    '        If correctFormat = False Then
    '            errorMessage &= "Incorrect format for zip field."
    '        End If
    '    End If

    '    Return errorMessage
    'End Function

    Private Sub BuildCampusDdl()
        'Bind the CampusGroups DrowDownList
        Dim campusGroups As New CampusGroupsFacade
        With ddlCampusId
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            ''Code Modified by Kamalesh Ahuja on August 26, 2010 Mantis Id 19559
            .DataSource = campusGroups.GetCampusesByUser(userId)
            '' '' Code Modified by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
            ''If SingletonAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "ownleads" Then
            ''    .DataSource = campusGroups.GetCampusesByUser(userId)
            ''ElseIf SingletonAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "permissiblecampuses" Then
            ''    .DataSource = campusGroups.GetCampusesByUser(userId)
            ''ElseIf SingletonAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "allcampuses" Then
            ''    .DataSource = campusGroups.GetAllCampuses()
            ''Else
            ''    .DataSource = campusGroups.GetAllCampusEnrollmentByCampus(CampusId)
            ''End If
            ''Code Modified by Kamalesh Ahuja on August 26, 2010 Mantis Id 19559
            '' '''''''''''''''''''''
            .DataBind()
        End With
    End Sub
    Private Sub BuildDropDownLists()

        'BuildPrefixDDL()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        'Prefixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlPrefix, AdvantageDropDownListName.Prefixes, CampusId, True, True, String.Empty))
        'Suffixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlSuffix, AdvantageDropDownListName.Suffixes, CampusId, True, True, String.Empty))
        'Country DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlCountry, AdvantageDropDownListName.Countries, CampusId, True, True, String.Empty))
        'Address Type
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressType, AdvantageDropDownListName.Address_Types, CampusId, True, True, String.Empty))
        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlDrivLicStateID, AdvantageDropDownListName.States, CampusId, True, True, String.Empty))
        'Races
        ddlList.Add(New AdvantageDDLDefinition(ddlRace, AdvantageDropDownListName.Races, CampusId, True, True, String.Empty))
        'Genders
        ddlList.Add(New AdvantageDDLDefinition(ddlGender, AdvantageDropDownListName.Genders, CampusId, True, True, String.Empty))
        'Marital Status
        ddlList.Add(New AdvantageDDLDefinition(ddlMaritalStatus, AdvantageDropDownListName.MaritalStatus, CampusId, True, True, String.Empty))
        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlStateID, AdvantageDropDownListName.States, CampusId, True, True, String.Empty))
        'Citizenships
        ddlList.Add(New AdvantageDDLDefinition(ddlCitizen, AdvantageDropDownListName.Citizen, CampusId, True, True, String.Empty))
        'Nationality
        ddlList.Add(New AdvantageDDLDefinition(ddlNationality, AdvantageDropDownListName.Nationality, CampusId, True, True, String.Empty))
        'Address States 
        'ddlList.Add(New AdvantageDDLDefinition(ddlAddrStateId, AdvantageDropDownListName.States, CampusId,True,True,String.Empty))
        'Shifts
        ddlList.Add(New AdvantageDDLDefinition(ddlShiftID, AdvantageDropDownListName.Shifts, CampusId, True, True, String.Empty))
        'County
        ddlList.Add(New AdvantageDDLDefinition(ddlCounty, AdvantageDropDownListName.Counties, CampusId, True, True, String.Empty))
        'Family Income
        ddlList.Add(New AdvantageDDLDefinition(ddlFamilyIncome, AdvantageDropDownListName.FamilyIncome, CampusId, True, True, String.Empty))
        'Previous Education
        ddlList.Add(New AdvantageDDLDefinition(ddlPreviousEducation, AdvantageDropDownListName.EducationLvl, CampusId, True, True, String.Empty))
        'Phone Types
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneType, AdvantageDropDownListName.Phone_Types, CampusId, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneType2, AdvantageDropDownListName.Phone_Types, CampusId, True, True, String.Empty))
        'Phone Statuses
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneStatus, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Address Status
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressStatus, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Admission Reps
        'ddlList.Add(New AdvantageDDLDefinition(ddlAdmissionsRep, AdvantageDropDownListName.AdmissionsRep, Nothing))

        'Dependency Type
        ddlList.Add(New AdvantageDDLDefinition(ddlDependencyTypeId, AdvantageDropDownListName.DependencyType, CampusId, True, True, String.Empty))

        'Geographic Type
        ddlList.Add(New AdvantageDDLDefinition(ddlGeographicTypeId, AdvantageDropDownListName.GeographicType, CampusId, True, True, String.Empty))

        'Sponsor
        ddlList.Add(New AdvantageDDLDefinition(ddlSponsor, AdvantageDropDownListName.Sponsors, CampusId, True, True, String.Empty))

        'Admin Criteria
        ddlList.Add(New AdvantageDDLDefinition(ddlAdminCriteriaId, AdvantageDropDownListName.AdminCriteriaType, CampusId, True, True, String.Empty))

        'Housing Type
        ddlList.Add(New AdvantageDDLDefinition(ddlHousingId, AdvantageDropDownListName.HousingType, CampusId, True, True, String.Empty))

        'Degree Certificate Seeking Type
        ddlList.Add(New AdvantageDDLDefinition(ddlDegCertSeekingId, AdvantageDropDownListName.DegCertSeekingType, CampusId, True, True, String.Empty))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

        BuildStatusDDL("")
        ''''' Code Modified on 18th August to fix mantis issue id 19555
        ''BuildCampusDDL()
        '''''''''''''
        BuildSourceCatagoryDDL()
        BuildProgramGroupDDL()
        BuildLeadGroupsDDL()
        BuildAdmissionRepsDDL()
    End Sub
    Private Sub BuildLeadGroupsDDL()
        Dim facade As New AdReqsFacade
        With chkLeadGrpId
            .DataTextField = "Descrip"
            .DataValueField = "LeadGrpId"
            .DataSource = facade.GetAllLeadGroups(CampusId)
            .DataBind()
        End With
    End Sub
    Private Function UpdateLeadGroups(ByVal LeadId As String) As Integer
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), chkLeadGrpId.Items.Count)
        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In chkLeadGrpId.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        If i <= 0 Then
            Return -1
        End If

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)

        'update Selected Jobs
        Dim leadGrpFacade As New AdReqsFacade
        If leadGrpFacade.UpdateLeadGroups(LeadId, AdvantageSession.UserState.UserName, selectedDegrees) < 0 Then
            DisplayErrorMessage("A related record exists,you can not perform this operation")
            'DisplayRADAlert(CallbackType.Postback, "Error1", "A related record exists,you can not perform this operation", 350, 100, "Update Lead Groups Error")

        End If
        Return 0
    End Function

    Private Sub BuildPrgVersionDDL()
        ddlPrgVerId.Items.Clear()
        Dim facade As New LeadFacade
        With ddlPrgVerId
            .DataTextField = "PrgVerDescrip"
            '.DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = facade.GetAllProgramVersionByCampusAndUser(ddlProgramID.SelectedValue, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramGroupDDL()
        Dim PrgGrp As New LeadFacade
        With ddlAreaID
            .DataTextField = "PrgGrpDescrip"
            .DataValueField = "PrgGrpID"
            .DataSource = PrgGrp.GetAllProgramsByGroup()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlProgramID
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlPrgVerId
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceCatagoryDDL()
        Dim SourceCatagory As New LeadFacade
        With ddlSourceCategoryId
            .DataTextField = "SourceCatagoryDescrip"
            .DataValueField = "SourceCatagoryId"
            .DataSource = SourceCatagory.GetAllSourceCategoryByCampus(CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSourceTypeId
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSourceAdvertisement
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceTypeDDL(ByVal SourceCatagoryID As String)
        Dim SourceType As New LeadFacade
        ddlSourceTypeId.Items.Clear()
        With ddlSourceTypeId
            .DataTextField = "SourceTypeDescrip"
            .DataValueField = "SourceTypeId"
            .DataSource = SourceType.GetAllSourceTypeByCampus(SourceCatagoryID, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceAdvDDL(ByVal SourceTypeID As String)
        Dim SourceType As New LeadFacade
        ddlSourceAdvertisement.Items.Clear()
        With ddlSourceAdvertisement
            .DataTextField = "SourceAdvDescrip"
            .DataValueField = "SourceAdvId"
            .DataSource = SourceType.GetAllSourceAdvertisementByCampus(SourceTypeID, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramsDDL(ByVal ProgramGrpID As String)
        Dim ProgramTypes As New LeadFacade
        ddlProgramID.Items.Clear()
        With ddlProgramID
            .DataTextField = "ProgDescrip"
            .DataValueField = "ProgId"
            .DataSource = ProgramTypes.GetAllProgramsByCampusAndUser(ProgramGrpID, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildAdmissionRepsDDL()

        ''Dim LeadAdmissionReps As New LeadFacade
        ''With ddlAdmissionsRep
        ''    .DataTextField = "fullname"
        ''    .DataValueField = "userId"
        ''    .DataSource = LeadAdmissionReps.GetAllAdmissionRepsByCampusAndUserId(CampusId, userId)
        ''    .DataBind()
        ''    .Items.Insert(0, New ListItem("Select", ""))
        ''    .SelectedIndex = 0
        ''End With

        ''MOdified By Saraswathi Lakshmanan to fix issue 14385
        ''When entering a new lead information SA, Front Desk and Director
        '' of Admissions should be able to assign them to admission rep 
        ''Used the available function from AD/LeadMaster1.aspx

        Dim userName As String
        userName = AdvantageSession.UserState.UserName


        Dim LeadAdmissionReps As New LeadFacade
        Dim DirectorAdmissionsorFrontDeskCheck As New UserSecurityFacade
        ''Find if the User is DirectorOfAdmission or FrontDesk

        Dim boolDirOrFDeskCheck As Boolean = DirectorAdmissionsorFrontDeskCheck.IsDirectorOfAdmissionsorFrontDesk(userId)
        With ddlAdmissionsRep
            .DataTextField = "fullname"
            .DataValueField = "userid"
            If boolDirOrFDeskCheck Then
                .DataSource = LeadAdmissionReps.GetAllAdmissionRepsByCampusUserIdUserRoles(CampusId, userId, userName, boolDirOrFDeskCheck)
            Else
                .DataSource = LeadAdmissionReps.GetAllAdmissionRepsByCampusAndUserId(CampusId, userId)
            End If
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Function ValidateLeadGroups() As Integer
        Dim strGroups As String = String.Empty
        ' Dim dsUseScheduled As DataSet
        'In For Loop Check The Number of Items Selected
        For Each item As ListItem In chkLeadGrpId.Items
            If item.Selected Then
                If strGroups = String.Empty Then
                    strGroups = item.Value.ToString
                Else
                    strGroups &= "," & item.Value.ToString
                End If

            End If
        Next

        If strGroups = String.Empty Then
            Return -1
        ElseIf strGroups.Split(",").Length = 1 Then
            Return 1
        Else
            Return (New StudentEnrollmentFacade).GetSchedLeadGrpCnt_SP(strGroups)
        End If
    End Function
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim LeadMasterUpdate As New LeadFacade
        Dim Result As String
        'Call Update Function in the plEmployerInfoFacade
        Dim errorMessage As String = String.Empty
        Dim strAddressType As String = String.Empty
        Dim intAge As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If Not txtBirthDate.SelectedDate Is Nothing And IsDate(txtBirthDate.SelectedDate) Then
            If DateDiff(DateInterval.Year, CDate(txtBirthDate.SelectedDate), CDate(txtDate.Value)) < 1 Then
                DisplayErrorMessage("The DOB cannot be greater than or equal to today's date ")
                'DisplayRADAlert(CallbackType.Postback, "Error2", "The DOB cannot be greater than or equal to today's date ", 350, 100, "Save Error")
                txtAge.Text = ""
                Exit Sub
            End If

            Dim intConfigAge As Integer = MyAdvAppSettings.AppSettings("StudentAgeLimit")
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year
            Dim dob As Date = txtBirthDate.SelectedDate
            dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso _
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If
            If intAge < intConfigAge Then
                DisplayErrorMessage("The minimum age requirement is " & intConfigAge)
                'DisplayRADAlert(CallbackType.Postback, "Error3", "The minimum age requirement is " & intConfigAge, 350, 100, "Save Error")
                txtAge.Text = ""
                Exit Sub
            Else
                txtAge.Text = intAge
            End If
        End If
        ''Added by Saraswathi lakshmanan to validate the entry in childern field. only numbers can be entered
        ''Added on Feb 24 2008
        If txtChildren.Text <> "" Then
            If Not IsNumeric(txtChildren.Text) Then
                DisplayErrorMessage("The field children should contain numbers")
                'DisplayRADAlert(CallbackType.Postback, "Error4", "The field children should contain numbers", 350, 100, "Save Error")
                Exit Sub
            End If
        End If

        If chkForeignZip.Checked = False Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not ddlStateID.SelectedValue = "" Or Not txtzip.Text = "" Or Not ddlCounty.SelectedValue = "" Then
                If ddlAddressType.SelectedValue = "" Or ddlAddressType.SelectedValue = Guid.Empty.ToString Then
                    strAddressType = "Address type is required" & vbLf
                End If
            End If
        End If

        If chkForeignZip.Checked = True Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not txtOtherState.Text = "" Or Not txtzip.Text = "" Or Not ddlCounty.SelectedValue = "" Then
                If ddlAddressType.SelectedValue = "" Or ddlAddressType.SelectedValue = Guid.Empty.ToString Then
                    strAddressType = "Address type is required" & vbLf
                End If
            End If
        End If

        'Code commented by Balaji on 04/15/2009 to fix issue 12013
        'Dim strValidateIPEDS As String = ValidateIPEDS()
        'If Not strValidateIPEDS = "" Then
        '    strAddressType &= strValidateIPEDS & vbLf
        'End If

        If Not txtphone.Text = "" Then
            If Not chkForeignPhone.Checked And txtphone.Text.Length < 10 Then
                strAddressType &= "Domestic phone numbers must include the area code and phone number and be 10 characters in length (Phone field)" & vbLf
            End If
        End If

        If Not txtphone.Text = "" Then
            If ddlPhoneType.SelectedValue = "" Or ddlPhoneType.SelectedValue = "" Then
                strAddressType &= "Phone type is required" & vbLf
            End If
        End If

        If Not ddlPhoneType.SelectedValue = "" Then
            If txtphone.Text = "" Then
                strAddressType &= "Phone is required" & vbLf
            End If
        End If

        If Not txtPhone2.Text = "" Then
            If Not chkForeignPhone2.Checked And txtPhone2.Text.Length < 10 Then
                strAddressType &= "Domestic phone numbers must include the area code and phone number and be 10 characters in length (Phone2 field)" & vbLf
            End If
        End If

        If Not txtPhone2.Text = "" Then
            If ddlPhoneType2.SelectedValue = "" Or ddlPhoneType2.SelectedValue = "" Then
                strAddressType &= "Phone type is required" & vbLf
            End If
        End If

        If Not ddlPhoneType2.SelectedValue = "" Then
            If txtPhone2.Text = "" Then
                strAddressType &= "Phone is required" & vbLf
            End If
        End If
        If rdoPhone2.Checked = True Then
            If txtPhone2.Text = "" And ddlPhoneType.SelectedValue = "" Then
                strAddressType &= "Phone and phone type are required to make it a default Phone" & vbLf
            End If

        End If
        If Not strAddressType = "" Then
            DisplayErrorMessage(strAddressType)
            'DisplayRADAlert(CallbackType.Postback, "Error5", strAddressType, 350, 100, "Save Error")
            'Session("areaid_changed") = "no"
            'Session("programid_changed") = "no"
            'Session("sourcecatagoryid_changed") = "no"
            'Session("sourcetypeid_changed") = "no"
            Exit Sub
        End If

        Dim inquiryTime As String = Now.Date + " " + txtInquiryTime.Text
        If Not CommonWebUtilities.IsValidDate(inquiryTime) Then
            DisplayErrorMessage("Enter Proper Inquiry Time Format")
            'DisplayRADAlert(CallbackType.Postback, "Error6", "Enter Proper Inquiry Time Format", 350, 100, "Save Error")
            Exit Sub
        End If
        'Check If Mask is successful only if Foreign Is not checked
        'If chkForeignPhone.Checked = False Or chkForeignPhone2.Checked = False Or chkForeignZip.Checked = False Then
        '    errorMessage = ValidateFieldsWithInputMasks()
        '    If errorMessage <> "" Then
        '        'DisplayErrorMessage(errorMessage)
        '        DisplayRADAlert(CallbackType.Postback, "Error7", errorMessage, 350, 100, "Save Error")
        '        Exit Sub
        '    End If
        'Else
        '    errorMessage = ""
        'End If

        Try
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                Dim sRegMessage As String = ""
                If txtSSN.Text.ToString.Trim = "" Then
                    sRegMessage = "SSN is required for Regent" & vbCrLf
                End If
                If txtBirthDate.SelectedDate Is Nothing Then
                    sRegMessage &= "DOB is required for Regent" & vbCrLf
                End If
                If Not sRegMessage = "" Then
                    DisplayErrorMessage(sRegMessage)
                    'DisplayRADAlert(CallbackType.Postback, "Error8", sRegMessage, 350, 100, "Save Error")
                    Exit Sub
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        'Check for Duplicate Leads
        'Dim facInputMask As New Fame.AdvantageV1.BusinessFacade.TM.InputMasksFacade
        'Dim phoneMask As String
        'Dim zipMask As String
        'Dim ssnMask As String
        Dim strSSN As String
        'If txtSSN.Text <> "" Then
        '    ssnMask = facInputMask.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.SSN)
        '    strSSN = facInputMask.RemoveMask(ssnMask, txtSSN.Text)
        'Else
        strSSN = txtSSN.Text
        'End If

        If Not String.IsNullOrEmpty(strSSN) Then
            Dim strDupSsnMsg As String

            strDupSsnMsg = LeadMasterUpdate.CheckDuplicateSSN(txtFirstName.Text, txtLastName.Text, strSSN)
            If Not String.IsNullOrEmpty(strDupSsnMsg) Then
                DisplayErrorMessage(strDupSsnMsg)
                Exit Sub
            End If
        End If



        Dim intValidateLeadGroups As Integer
        intValidateLeadGroups = ValidateLeadGroups()
        If intValidateLeadGroups = -1 Then
            ''Message changed from Student group to lead group.
            ''Changed by Saraswathi Lakshmanan on Feb 10 2011 for rally case de5033.
            DisplayErrorMessage("Please select a lead group")
            'DisplayRADAlert(CallbackType.Postback, "Error9", "Please select a lead group", 350, 100, "Save Error")
            Exit Sub
            '**This validation has been temporarily suspended 4/3/09 DD
        ElseIf intValidateLeadGroups >= 2 Then
            'More that one Student Group with the "Use For Scheduling" setting
            'has been selected.
            DisplayErrorMessage("Please select only one Student Group that is to be used for scheduling")
            'DisplayRADAlert(CallbackType.Postback, "Error10", "Please select only one Student Group that is to be used for scheduling", 350, 100, "Save Error")
            Exit Sub
        End If





        Dim winSettings As String = "toolbar=no, status=no, resizable=yes,width=1200px,height=470px"
        Dim name As String = "DuplicateLead"
        Dim url As String = String.Empty
        'Dim winSettings As String = "toolbar=no,status=no,width=550px,height=150px,left=250px,top=200px,modal=yes"
        'Dim name As String = "AdmReqSummary1"
        Dim message1 As String = String.Empty
        Dim message2 As String = String.Empty
        Dim intDuplicateLeadCount As Integer
        If Not txtBirthDate.SelectedDate Is Nothing Then
            intDuplicateLeadCount = LeadMasterUpdate.CheckDuplicateLeads(txtFirstName.Text, txtLastName.Text, strSSN, txtBirthDate.SelectedDate, txtMiddleName.Text, ddlCampusId.SelectedValue)
        Else
            intDuplicateLeadCount = LeadMasterUpdate.CheckDuplicateLeads(txtFirstName.Text, txtLastName.Text, strSSN, "", txtMiddleName.Text, ddlCampusId.SelectedValue)
        End If


        If intDuplicateLeadCount >= 1 Then
            If Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                message1 = "A Lead already exists with same First Name and Last Name."
                message2 = "Do you want to proceed and add the lead ?"
            End If
            If Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                message1 = "A Lead already exists with same First Name,Middle Name and Last Name."
                message2 = "Do you want to proceed and add the lead ?"
            End If
            If Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                message1 = "A Lead already exists with same First Name,Last Name and SSN."
                message2 = "Do you want to proceed and add the lead ?"
            End If
            If Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                message1 = "A Lead already exists with same First Name,Middle Name,Last Name and SSN."
                message2 = "Do you want to proceed and add the lead ?"
            End If
            If Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                message1 = "A Lead already exists with same First Name,Middle Name,Last Name and DOB."
                message2 = "Do you want to proceed and add the lead ?"
            End If
            If Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                message1 = "A Lead already exists with same First Name,Last Name and DOB."
                message2 = "Do you want to proceed and add the lead ?"
            End If

            If Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                message1 = "A Lead already exists with same First Name,Last Name,MiddleName,SSN and DOB."
                message2 = "Do you want to proceed and add the lead ?"
            End If

            If Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                message1 = "A Lead already exists with same First Name,Last Name,SSN and DOB."
                message2 = "Do you want to proceed and add the lead ?"
            End If

            If Not txtBirthDate.SelectedDate Is Nothing Then
                url = "DisplayDuplicateLeads.aspx?FirstName=" + Trim(txtFirstName.Text) + "&LastName=" + Trim(txtLastName.Text) + "&SSN=" + Trim(txtSSN.Text) + "&DOB=" + Trim(txtBirthDate.SelectedDate) + "&repid=" + Trim(txtUserId.Value) + "&CampusId=" + txtCampusId.Value + "&Address=" + txtAddress1.Text + "&Phone=" + txtphone.Text + "&Phone2=" + txtPhone2.Text + "&Message1=" + message1 + "&Message2=" + message2 + "&RequestType=" + "save"
            Else
                url = "DisplayDuplicateLeads.aspx?FirstName=" + Trim(txtFirstName.Text) + "&LastName=" + Trim(txtLastName.Text) + "&SSN=" + Trim(txtSSN.Text) + "&DOB=" + "" + "&repid=" + Trim(txtUserId.Value) + "&CampusId=" + txtCampusId.Value + "&Address=" + txtAddress1.Text + "&Phone=" + txtphone.Text + "&Phone2=" + txtPhone2.Text + "&Message1=" + message1 + "&Message2=" + message2 + "&RequestType=" + "save"
            End If
            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
            Exit Sub

        End If


        Dim strPhone As String
        Dim strPhone2 As String
        Dim strZip As String

        strPhone = txtphone.Text
        strPhone2 = txtPhone2.Text
        strZip = txtzip.Text

        'Check for Duplicate Phone
        Dim strMessage As String = ""
        Dim intCheckDuplicatePhone As Integer = 0
        Dim intCheckDuplicateEmail As Integer = 0
        Dim intCheckDuplicateAddress As Integer = 0
        If Not strPhone.ToString.Trim = "" Then
            intCheckDuplicatePhone = (New LeadFacade).CheckDuplicateLeadsWithSamePhoneNumber(CampusId, strPhone, strPhone2)
            If intCheckDuplicatePhone >= 1 Then
                strMessage = " A Lead already exists with the same Phone Number"
            End If
        End If
        If Not txtWorkEmail.Text = "" Or Not txtHomeEmail.Text = "" Then
            intCheckDuplicateEmail = (New LeadFacade).CheckDuplicateLeadsWithSameEmail(CampusId, txtWorkEmail.Text, txtHomeEmail.Text)
        End If
        If Not txtAddress1.Text.ToString.Trim = "" Or Not txtAddress2.Text.ToString.Trim = "" Or Not txtCity.Text.Trim = "" Or Not strZip = "" Then
            intCheckDuplicateAddress = (New LeadFacade).CheckDuplicateLeadsWithSameAddress(CampusId, txtAddress1.Text, txtAddress2.Text, txtCity.Text, ddlStateID.SelectedValue, strZip)
        End If
        If intCheckDuplicateEmail >= 1 Then
            If intCheckDuplicatePhone >= 1 Then
                strMessage &= ", Email Address"
            Else
                strMessage = " A Lead already exists with the same Email Address"
            End If
        End If
        If intCheckDuplicateAddress >= 1 Then
            If intCheckDuplicateEmail >= 1 Then
                strMessage &= " and Address."
            Else
                If intCheckDuplicatePhone = 0 And intCheckDuplicateEmail = 0 Then
                    strMessage = " A Lead already exists with the same Address."
                End If
            End If
        End If

        If Not strMessage = "" Then
            message1 = strMessage
            message2 = "Do you want to proceed and add the lead ?"
            url = "DisplayLeadMessage.aspx?Message1=" + message1 + "&Message2=" + message2
            CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
            Exit Sub
        End If


        If errorMessage = "" Then
            Dim boolRegent As Boolean = False
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                boolRegent = True
            End If
            Result = LeadMasterUpdate.UpdateLeadMaster(BuildLeadMaster(txtLeadMasterID.Value), AdvantageSession.UserState.UserName, txtResumeObjective.Text, boolRegent)

            If Result = "" Then
                Dim j As Integer
                j = UpdateLeadGroups(txtLeadMasterID.Value)

                If Not chkIsInDB.Checked Then
                    Result = AddNewTask()
                End If
                '   populate page fields with data just saved in DB
                ' BindLeadMasterData(LeadMasterUpdate.GetLeadsInfo(txtLeadMasterID.Value))

                '   save on syLeadStatusesChanges only if txtLeadStatusId (oldStatus) <> ddlLeadStatus.SelectedValue (newStatus)
                If ddlLeadStatus.SelectedValue <> txtLeadStatusId.Value Then
                    Dim statusChanged As New LeadStatusesInfo
                    With statusChanged
                        .LeadId = txtLeadMasterID.Value
                        .OrigStatusId = IIf(txtLeadStatusId.Value = "", System.Guid.Empty.ToString, txtLeadStatusId.Value)
                        .NewStatusId = ddlLeadStatus.SelectedValue
                        .ModDate = Date.Parse(txtModDate.Value)
                    End With
                    Result = (New LeadStatusChangeFacade).InsertLeadStatusChange(statusChanged, AdvantageSession.UserState.UserName)
                    If Not Result = "" Then
                        'display error inserting into syLeadStatusesChanges table
                        DisplayErrorMessage(Result)
                        'DisplayRADAlert(CallbackType.Postback, "Error11", Result, 350, 100, "Save Error")
                        Exit Sub
                    End If


                    ''''' Code Modified on 19th August to fix mantis issue id 19555
                    ViewState("CampusID") = Nothing
                    CampusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString

                    Try
                        ddlCampusId.SelectedValue = CampusId
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)


                    End Try
                    ''''''''''''''''''''''''''

                    txtLeadStatusId.Value = ddlLeadStatus.SelectedValue
                    BuildStatusDDL(ddlLeadStatus.SelectedValue)
                    ddlLeadStatus.SelectedValue = txtLeadStatusId.Value
                End If


                Dim sdfid As ArrayList
                Dim sdfidValue As ArrayList
                '                Dim newArr As ArrayList
                Dim z As Integer
                Dim sdfControl As New SDFComponent
                Try
                    txtPKID.Value = txtLeadMasterID.Value
                    sdfControl.DeleteSDFValue(txtPKID.Value)
                    sdfid = sdfControl.GetAllLabels(pnlSDF)
                    sdfidValue = sdfControl.GetAllValues(pnlSDF)
                    For z = 0 To sdfid.Count - 1
                        sdfControl.InsertValues(txtPKID.Value, Mid(sdfid(z).id, 5), sdfidValue(z))
                    Next
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try
                If useSaveNext Then
                    btnNew_Click(Me, New System.EventArgs())
                Else
                    chkIsInDB.Checked = True
                End If
                InitButtonsForEdit()
                If useSaveNext = False Then
                    BuildLeadStateObject() 'Add MRU
                End If
                Exit Sub
            Else
                DisplayErrorMessage(Result)
                'DisplayRADAlert(CallbackType.Postback, "Error12", Result, 350, 100, "Save Error")
                Exit Sub
            End If
        Else
            DisplayErrorMessageMask(errorMessage)
            'DisplayRADAlert(CallbackType.Postback, "Error13", errorMessage, 350, 100, "Save Error")
        End If

        'This is to keep the values in AJAX DDLs when form is submitted
        'Session("areaid_changed") = "no"
        'Session("programid_changed") = "no"
        'Session("sourcecatagoryid_changed") = "no"
        'Session("sourcetypeid_changed") = "no"
    End Sub
    Public Sub BuildLeadStateObject()
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String
        Dim strLeadId As String = ""
        Dim strLeadObjectPointer As String = ""

        strLeadId = txtLeadMasterID.Value
        If Not strLeadId.Trim = "" Then
            objStateInfo = mruProvider.BuildLeadStatusBar(strLeadId)

            strVID = Guid.NewGuid.ToString

            strLeadObjectPointer = strVID 'Set VID to Lead Object Pointer

            Session("LeadObjectPointer") = strVID ' This step is required, as master page loses values stored in class variables

            If objStateInfo.SystemStatus = 6 Then 'If lead is enrolled set the student pointer
                Session("StudentObjectPointer") = strVID
            Else
                Session("StudentObjectPointer") = ""
            End If

            'Add an entry to AdvantageSessionState for this guid and object
            'load Advantage state
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            state(strVID) = objStateInfo
            'save current State
            CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
            Try
                Dim advResetUserState As New BO.User
                advResetUserState = AdvantageSession.UserState
                With advResetUserState
                    .CampusId = New Guid(objStateInfo.CampusId.ToString)
                End With
                AdvantageSession.UserState = advResetUserState
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New ArgumentException(ex.Message.ToString)
            End Try

            mruProvider.InsertMRU(4, strLeadId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

            AdvantageSession.MasterLeadId = strLeadId

            RedirectBasedOnPermission(AdvantageSession.UserState.CampusId.ToString, strVID)
        End If
    End Sub
    Private Sub RedirectBasedOnPermission(ByVal defaultCampusId As String, ByVal strVID As String)
        Dim arrUPP As New ArrayList
        Dim pURL As String
        Dim fac As New UserSecurityFacade
        arrUPP = fac.GetUserResourcePermissionsForSubModule(HttpContext.Current.Session("UserId"), 395, "Admissions", Master.CurrentCampusId)
        If arrUPP.Count = 0 Then
            'User does not have permission to any resource for this submodule
            'Session("Error") = "The Lead was successfully added, But you do not have permission to any of the pages for existing leads<br> for the campus that you are logged in to."
            'Response.Redirect("../ErrorPage.aspx")
            RadNotification1.Show()
            RadNotification1.Text = "The Lead was successfully added, But you do not have permission to any of the pages for existing leads for the campus that you are logged in to."

        ElseIf fac.DoesUserHasAccessToSubModuleResource(arrUPP, 170) Then
            Response.Redirect("../AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)

        Else
            'redirect to the first page that the user has permission to for the submodule
            pURL = BuildPartialURL(arrUPP(0))
            Response.Redirect(pURL & "&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)
        End If
    End Sub
    Private Function AddNewTask() As String
        Dim utInfo As New UserTaskInfo
        Dim rtn As String = String.Empty
        ' determine if this is an update or a new task by looking
        ' at the ViewState("usertaskid") which is set by passing a parameter to
        ' the page.

        utInfo.IsInDB = False
        utInfo.AssignedById = TMCommon.GetCurrentUserId()
        utInfo.EndDate = Utilities.GetAdvantageDBDateTime(Date.Now)
        utInfo.StartDate = Utilities.GetAdvantageDBDateTime(Date.Now)
        utInfo.Message = "Notice : A new Lead was added." & vbLf
        utInfo.Priority = 1
        utInfo.OwnerId = ddlAdmissionsRep.SelectedValue
        utInfo.ReId = txtLeadMasterID.Value
        utInfo.ModUser = TMCommon.GetCurrentUserId()
        utInfo.Status = TaskStatus.Pending
        utInfo.CampGrpID = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'utInfo.Status = ddlLeadStatus.SelectedValue
        ''utInfo.ResultId = txtStuEnrollmentId.Text


        ' Update the task and check the result
        If Not UserTasksFacade.AddTaskFromStatusCode(ddlLeadStatus.SelectedValue, utInfo) Then
            rtn = "Unable to add a reminder to the Task Manager. " & vbLf
        End If
        Return rtn
    End Function

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
            btnSaveNext.Enabled = True
        Else
            btnSave.Enabled = False
            btnSaveNext.Enabled = False
        End If

        'If pObj.HasFull Or pObj.HasDelete Then
        '    btnDelete.Enabled = True
        'Else
        '    btnDelete.Enabled = False
        'End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Function BuildLeadMaster(ByVal LeadMasterId As String) As LeadMasterInfo
        Dim LeadMaster As New LeadMasterInfo
        Dim facInputMask As New FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade
        'Dim phoneMask As String
        'Dim zipMask As String
        'Dim ssnMask As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        With LeadMaster
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get LeadMasterID
            .LeadMasterID = txtLeadMasterID.Value

            'Get LastName
            .LastName = txtLastName.Text

            'Get FirstName
            .FirstName = txtFirstName.Text

            'Get MiddleName
            .MiddleName = txtMiddleName.Text

            'Get StatusId 
            .Status = ddlLeadStatus.SelectedValue

            'Get PrefixId 
            .Prefix = ddlPrefix.SelectedValue

            'Get SuffixId 
            .Suffix = ddlSuffix.SelectedValue

            'Get Title 
            '.Title = ddlTitleId.SelectedValue

            'Get BirthDate
            '.BirthDate = txtBirthDate.SelectedDate
            If txtBirthDate.SelectedDate Is Nothing Then
                .BirthDate = ""
            Else
                .BirthDate = Date.Parse(txtBirthDate.SelectedDate)
            End If

            'Get Age
            ' .Age = txtAge.Text

            'Get sponsor
            .Sponsor = ddlSponsor.SelectedValue

            'Get AdmissionsRep
            .AdmissionsRep = ddlAdmissionsRep.SelectedValue

            'Get Gender
            .Gender = ddlGender.SelectedValue

            'Get Race
            .Race = ddlRace.SelectedValue

            'Get MaritalStatus
            .MaritalStatus = ddlMaritalStatus.SelectedValue

            'Get Children
            .Children = txtChildren.Text

            'Get FamilyIncome
            .FamilyIncome = ddlFamilyIncome.SelectedValue

            'Get Phone
            'phoneMask = facInputMask.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Phone)
            'If txtphone.Text <> "" And chkForeignPhone.Checked = False Then
            '    .Phone = facInputMask.RemoveMask(phoneMask, txtphone.Text)
            'Else
            .Phone = txtphone.Text
            'End If
            'If txtPhone2.Text <> "" And chkForeignPhone2.Checked = False Then
            '    .Phone2 = facInputMask.RemoveMask(phoneMask, txtPhone2.Text)
            'Else
            .Phone2 = txtPhone2.Text
            ' End If
            'Get PhoneType
            .PhoneType = ddlPhoneType.SelectedValue
            .PhoneType2 = ddlPhoneType2.SelectedValue
            'Driver License State
            .DriverLicState = ddlDrivLicStateID.SelectedValue

            'Get HomeEmail
            .HomeEmail = txtHomeEmail.Text

            'Get WorkEmail
            .WorkEmail = txtWorkEmail.Text

            'Get Address1
            .Address1 = txtAddress1.Text

            'Get Address2
            .Address2 = txtAddress2.Text

            'Get City
            .City = txtCity.Text

            'Get State
            .State = ddlStateID.SelectedValue

            'Get Zip
            'If txtzip.Text <> "" And chkForeignZip.Checked = False Then
            '    zipMask = facInputMask.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Zip)
            '    .Zip = facInputMask.RemoveMask(zipMask, txtzip.Text)
            'Else
            .Zip = txtzip.Text
            'End If

            'Get Country 
            .Country = ddlCountry.SelectedValue

            'Get County
            .County = ddlCounty.SelectedValue


            'Get SourceDate 
            '.SourceDate = txtSourceDate.SelectedDate
            If txtSourceDate.SelectedDate Is Nothing Then
                .SourceDate = ""
            Else
                .SourceDate = Date.Parse(txtSourceDate.SelectedDate)
            End If

            '.Area = ddlAreaId.Value
            '.ProgramID = Me.__programid.Value
            '.PrgVerId = Me.__prgverid.Value

            '.SourceCategory = ddlSourceCategoryId.Value
            '.SourceType = Me.__sourcetypeid.Value
            '.SourceAdvertisement = Me.__sourceadvid.Value

            'Get Area,Program and Program version
            .Area = ddlAreaID.SelectedValue
            .ProgramID = ddlProgramID.SelectedValue
            .PrgVerId = ddlPrgVerId.SelectedValue


            'Get SourceCategory,Source Type and SourceAdvertisement
            .SourceCategory = ddlSourceCategoryId.SelectedValue
            .SourceType = ddlSourceTypeId.SelectedValue
            .SourceAdvertisement = ddlSourceAdvertisement.SelectedValue

            'Get ExpectedStart
            '.ExpectedStart = txtExpectedStart.SelectedDate
            If txtExpectedStart.SelectedDate Is Nothing Then
                .ExpectedStart = ""
            Else
                .ExpectedStart = Date.Parse(txtExpectedStart.SelectedDate)
            End If

            'Get ShiftID
            .ShiftID = ddlShiftID.SelectedValue

            'Get Nationality
            .Nationality = ddlNationality.SelectedValue

            'Get Citizen
            .Citizen = ddlCitizen.SelectedValue

            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                .EntranceInterviewDate = txtEntranceInterviewDate.SelectedDate
                .HighSchoolProgramCode = txtHighSchoolProgramCode.Text
            End If

            'Get SSN 
            'If txtSSN.Text <> "" Then
            '    ssnMask = facInputMask.GetInputMaskForItem(Fame.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.SSN)
            '    .SSN = facInputMask.RemoveMask(ssnMask, txtSSN.Text)
            'Else
            .SSN = txtSSN.Text
            'End If

            'Get DriverLicState 
            .DriverLicState = ddlStateID.SelectedValue

            'Get DriverLicNumber
            .DriverLicNumber = txtDrivLicNumber.Text

            'Get AlienNumber
            .AlienNumber = txtAlienNumber.Text

            'Notes
            .Notes = txtComments.Text

            'Get AddressTypes
            .AddressType = ddlAddressType.SelectedValue

            '.AssignmentDate = txtAssignedDate.SelectedDate
            If txtAssignedDate.SelectedDate Is Nothing Then
                .AssignmentDate = ""
            Else
                .AssignmentDate = Date.Parse(txtAssignedDate.SelectedDate)
            End If

            '.AppliedDate = txtDateApplied.SelectedDate
            If txtDateApplied.SelectedDate Is Nothing Then
                .AppliedDate = ""
            Else
                .AppliedDate = Date.Parse(txtDateApplied.SelectedDate)
            End If

            .PreviousEducation = ddlPreviousEducation.SelectedValue


            .CampusId = ddlCampusId.SelectedValue

            .OtherState = txtOtherState.Text

            If chkForeignPhone.Checked = True Then
                .ForeignPhone = 1
            Else
                .ForeignPhone = 0
            End If
            If chkForeignPhone2.Checked = True Then
                .ForeignPhone2 = 1
            Else
                .ForeignPhone2 = 0
            End If
            If chkForeignZip.Checked = True Then
                .ForeignZip = 1
            Else
                .ForeignZip = 0
            End If
            ' If rdoPhone1.Checked Then .DefaultPhone = 1
            If rdoPhone1.Checked Then
                If .Phone = "" And .Phone2 <> "" Then
                    .DefaultPhone = 2
                Else
                    .DefaultPhone = 1
                End If

            End If
            If rdoPhone2.Checked Then .DefaultPhone = 2

            '.LeadGrpId = ddlLeadGrpId.SelectedValue

            .DependencyTypeId = ddlDependencyTypeId.SelectedValue
            .GeographicTypeId = ddlGeographicTypeId.SelectedValue
            .AdminCriteriaId = ddlAdminCriteriaId.SelectedValue
            .HousingTypeId = ddlHousingId.SelectedValue
            .DegCertSeekingId = ddlDegCertSeekingId.SelectedValue
            Dim ActValue As New AdvantageCommonValues

            If chkForeignZip.Checked = False Then
                If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not ddlStateID.SelectedValue = "" Or Not txtzip.Text = "" Then
                    If ddlAddressStatus.SelectedValue = "" Or ddlAddressStatus.SelectedValue = Guid.Empty.ToString Then
                        .AddressStatus = AdvantageCommonValues.ActiveGuid
                    End If
                End If
            End If

            If chkForeignZip.Checked = True Then
                If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not txtOtherState.Text = "" Or Not txtzip.Text = "" Then
                    If ddlAddressStatus.SelectedValue = "" Or ddlAddressStatus.SelectedValue = Guid.Empty.ToString Then
                        .AddressStatus = AdvantageCommonValues.ActiveGuid
                    End If
                End If
            End If

            If Not txtphone.Text = "" Then
                If ddlPhoneStatus.SelectedValue = "" Or ddlPhoneStatus.SelectedValue = Guid.Empty.ToString Then
                    .PhoneStatus = AdvantageCommonValues.ActiveGuid
                End If
            End If

            .InquiryTime = txtInquiryTime.Text
            .AdvertisementNote = txtAdvertisementNote.Text
        End With
        Return LeadMaster
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Assign A New Value For Primary Key Column
        txtLeadMasterID.Value = Guid.NewGuid.ToString()

        ''''' Code Modified on 19th August to fix mantis issue id 19555
        ViewState("CampusID") = Nothing
        CampusId = Master.CurrentCampusId

        Try
            ddlCampusId.SelectedValue = CampusId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
        '''''''''''''''''''''

        'Create a Empty Object and Initialize the Object
        BindLeadMasterDataItemCommand(New LeadMasterInfo)

        'Reset The Value of chkIsInDb Checkbox To Identify an Insert
        chkIsInDB.Checked = False

        chkLeadGrpId.ClearSelection()



        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)

        txtAssignedDate.SelectedDate = Date.Now.ToShortDateString

        Try
            ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText("Unknown"))
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlGender.SelectedIndex = 0
        End Try

        Try
            ddlAddressType.SelectedIndex = ddlAddressType.Items.IndexOf(ddlAddressType.Items.FindByText("Home"))
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlAddressType.SelectedIndex = 0
        End Try

        Try
            ddlAdmissionsRep.SelectedValue = userId
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlAdmissionsRep.SelectedIndex = 0
        End Try

        'Initialize Buttons
        InitButtonsForLoad()

        'Initialize SourceDate and ExpectedStart Date
        txtSourceDate.Clear()
        txtExpectedStart.Clear()
        txtDateApplied.SelectedDate = Date.Now.ToShortDateString
        txtAdvertisementNote.Text = ""

        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            txtEntranceInterviewDate.Clear()
            txtHighSchoolProgramCode.Text = ""
        End If
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
            btnSaveNext.Enabled = True
            btnNew.Enabled = True
        Else
            btnSave.Enabled = False
            btnNew.Enabled = False
        End If
        btnDelete.Enabled = False
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If Not (txtLeadMasterID.Value = Guid.Empty.ToString) Then
            'instantiate component
            Dim LeadMaster As New LeadFacade

            'Delete The Row Based on EmployerId Date.Parse()
            Dim result As String = LeadMaster.DeleteLeadMaster(txtLeadMasterID.Value, txtModDate.Value)

            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                'DisplayRADAlert(CallbackType.Postback, "Error14", result, 350, 100, "Delete Error")
                Exit Sub

            Else
                chkIsInDB.Checked = False

                Dim lscfac As New LeadStatusChangeFacade
                lscfac.DeleteLeadStatusChange(txtLeadMasterID.Value)

                'bind an empty new LeadMasterInfo
                BindLeadMasterDataItemCommand(New LeadMasterInfo)

                'initialize buttons
                InitButtonsForLoad()

                'Initialize SourceDate and ExpectedStart Date
                txtSourceDate.Clear()
                txtExpectedStart.Clear()
                txtAdvertisementNote.Text = ""

                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    txtEntranceInterviewDate.Clear()
                    txtHighSchoolProgramCode.Text = ""
                End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim SDFControl As New SDFComponent
                SDFControl.DeleteSDFValue(txtLeadMasterID.Value)
                SDFControl.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            End If
        End If
    End Sub
    Private Sub BindLeadMasterDataItemCommand(ByVal LeadMaster As LeadMasterInfo)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        ' BuildDropDownLists()
        ''Bind The EmployerInfo Data From The Database
        Dim facInputMasks As New FAME.AdvantageV1.BusinessFacade.InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        Dim ssnMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.InputMasksFacade.InputMaskItem.SSN)

        With LeadMaster
            'get IsInDB
            chkIsInDB.Checked = .IsInDB

            'get LeadMasterID
            txtLeadMasterID.Value = .LeadMasterID

            'Get LastName
            txtLastName.Text = .LastName

            'Get FirstName
            txtFirstName.Text = .FirstName

            'Get MiddleName
            txtMiddleName.Text = .MiddleName

            'Get BirthDate
            'txtBirthDate.Text = .BirthDate

            Dim temp As DateTime
            If (DateTime.TryParse(.BirthDate, temp)) Then
                txtBirthDate.SelectedDate = temp
            Else
                txtBirthDate.Clear()
            End If

            'Try
            '    txtBirthDate.SelectedDate = .BirthDate
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    txtBirthDate.Clear()
            'End Try

            'Get Age
            txtAge.Text = ""

            'Get Phone
            If .ForeignPhone = 0 Then
                txtphone.Mask = "(###)-###-####"
                txtphone.DisplayMask = "(###)-###-####"
                txtphone.DisplayPromptChar = ""
            Else
                txtphone.Text = ""
                txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
                txtphone.DisplayMask = ""
                txtphone.DisplayPromptChar = ""
            End If
            If .ForeignPhone2 = 0 Then
                txtPhone2.Mask = "(###)-###-####"
                txtPhone2.DisplayMask = "(###)-###-####"
                lblPhone2.ToolTip = strMask
            Else
                txtPhone2.Mask = "aaaaaaaaaaaaaaaaaaaa"
                txtPhone2.DisplayMask = ""
                lblPhone2.ToolTip = ""
            End If
            txtphone.Text = .Phone
            txtPhone2.Text = .Phone2

            'Get HomeEmail
            txtHomeEmail.Text = .HomeEmail

            'Get WorkEmail
            txtWorkEmail.Text = .WorkEmail

            'Get Address1
            txtAddress1.Text = .Address1

            'Get Address2
            txtAddress2.Text = .Address2

            'Get City
            txtCity.Text = .City

            'Get State
            'txtBirthDate.Text = ""

            'Get Zip
            If .ForeignZip = 0 Then
                txtzip.Mask = "#####"
                lblZip.ToolTip = zipMask
                lblOtherState.Visible = False
                txtOtherState.Visible = False
            Else
                txtzip.Mask = "aaaaaaaaaaaaaaaaaaaa"
                lblZip.ToolTip = ""
                lblOtherState.Visible = True
                txtOtherState.Visible = True
            End If
            txtzip.Text = .Zip

            'Get SourceDate 
            'txtSourceDate.SelectedDate = .SourceDate
            Dim temp1 As DateTime
            If (DateTime.TryParse(.SourceDate, temp1)) Then
                txtSourceDate.SelectedDate = temp1
            Else
                txtSourceDate.Clear()
            End If
            'Try
            '    txtSourceDate.SelectedDate = .SourceDate
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    txtSourceDate.Clear()
            'End Try

            'Get ExpectedStart
            'txtExpectedStart.SelectedDate = .ExpectedStart
            Dim temp2 As DateTime
            If (DateTime.TryParse(.ExpectedStart, temp2)) Then
                txtExpectedStart.SelectedDate = temp2
            Else
                txtExpectedStart.Clear()
            End If

            'Try
            '    txtExpectedStart.SelectedDate = .ExpectedStart
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    txtExpectedStart.Clear()
            'End Try

            'Assignment Date
            'txtAssignedDate.SelectedDate = .AssignmentDate
            Dim temp3 As DateTime
            If (DateTime.TryParse(.AssignmentDate, temp3)) Then
                txtAssignedDate.SelectedDate = temp3
            Else
                txtAssignedDate.Clear()
            End If

            'Try
            '    txtAssignedDate.SelectedDate = .AssignmentDate
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    txtAssignedDate.Clear()
            'End Try

            If Not txtDateApplied.SelectedDate Is Nothing Then
                txtDateApplied.SelectedDate = CType(FormatDateTime(CType(txtDateApplied.SelectedDate, Date), DateFormat.ShortDate), Date?)
            End If

            If Not txtAssignedDate.SelectedDate Is Nothing Then
                txtAssignedDate.SelectedDate = FormatDateTime(txtAssignedDate.SelectedDate, DateFormat.ShortDate)
            End If

            'Get SSN 
            txtSSN.Text = .SSN
            'If txtSSN.Text <> "" Then
            '    txtSSN.Text = facInputMasks.ApplyMask(ssnMask, txtSSN.Text)
            'End If

            'Get DriverLicNumber
            txtDrivLicNumber.Text = .DriverLicNumber

            'Get AlienNumber
            txtAlienNumber.Text = .AlienNumber

            'Notes
            txtComments.Text = .Notes

            chkForeignPhone.Checked = False
            chkForeignPhone2.Checked = False
            chkForeignZip.Checked = False

            'Status Code
            ' BuildStatusDDL(.StatusCodeId)
            Dim strDefaultStatusCodeId As String = (New LeadFacade).GetNewDefaultLeadStatusCodeId(CampusId, userId)
            ''Code added by Atul Kamble on Nov 02,2010 for RallyID DE1201
            ''If Not strDefaultStatusCodeId = "" Then
            Try
                ddlLeadStatus.SelectedValue = strDefaultStatusCodeId.ToString
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlLeadStatus.SelectedValue = .StatusCodeId
            End Try
            ''End If
            ''Code added by Atul Kamble on Nov 02,2010 for RallyID DE1201
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                txtEntranceInterviewDate.SelectedDate = .EntranceInterviewDate
                txtHighSchoolProgramCode.Text = .HighSchoolProgramCode
            End If
            txtLeadStatusId.Value = ddlLeadStatus.SelectedValue

            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrefix, .Prefix, .PrefixDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSuffix, .Suffix, .SuffixDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdmissionsRep, .AdmissionsRep, .AdmissionRepsDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGender, .Gender, .GenderDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRace, .Race, .EthCodeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlMaritalStatus, .MaritalStatus, .MaritalStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFamilyIncome, .FamilyIncome, .FamilyIncomeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneType, .PhoneType, .PhoneTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneType2, .PhoneType2, .PhoneTypeDescrip2)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus, .Status, .StatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateID, .State, .StateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountry, .Country, .CountryDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCounty, .County, .CountyDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPreviousEducation, .PreviousEducation, .EdLvlDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressStatus, .Status, .StatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlShiftID, .ShiftID, .ShiftDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlNationality, .Nationality, .NationalityDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCitizen, .Citizen, .CitizenDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDrivLicStateID, .DriverLicState, .DriverLicenseStateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressType, .AddressType, .AddressTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus, .PhoneStatus, .PhoneStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus, .PhoneStatus2, .PhoneStatusDescrip2)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressStatus, .AddressStatus, .AddressStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDependencyTypeId, .DependencyTypeId, .DependencyTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGeographicTypeId, .GeographicTypeId, .GeographicTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSponsor, .Sponsor, .SponsorTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdminCriteriaId, .AdminCriteriaId, .AdminCriteriaDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlHousingId, .HousingTypeId, .HousingTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDegCertSeekingId, .DegCertSeekingId, .DegCertSeekingDescrip)

            'Always make sure the default country is selected
            'if the default country is marked as inactive then select the default value "select"
            Try
                ddlCountry.SelectedValue = strDefaultCountry
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlCountry.SelectedIndex = 0
            End Try

            'Get Area
            ddlAreaID.SelectedValue = .Area

            'Get ProgramID
            If Not .Area = "" Then
                BuildProgramsDDL(ddlAreaID.SelectedValue)
                ddlProgramID.SelectedValue = .ProgramID
            Else
                ddlProgramID.SelectedIndex = 0
            End If

            'Get Program Version
            If Not .ProgramID = "" Then
                BuildPrgVersionDDL()
                ddlPrgVerId.SelectedValue = .PrgVerId
            Else
                ddlPrgVerId.SelectedIndex = 0
            End If

            'Get SourceCategory
            ddlSourceCategoryId.SelectedValue = .SourceCategory

            'Get SourceType
            If Not .SourceCategory = "" Then
                BuildSourceTypeDDL(ddlSourceCategoryId.SelectedValue)
                ddlSourceTypeId.SelectedValue = .SourceType
            Else
                ddlSourceTypeId.SelectedIndex = 0
            End If

            'SourceAdvertisement
            If Not .SourceType = "" Then        'Or
                BuildSourceAdvDDL(ddlSourceTypeId.SelectedValue)
                ddlSourceAdvertisement.SelectedValue = .SourceAdvertisement
            Else
                ddlSourceAdvertisement.SelectedIndex = 0
            End If
            txtInquiryTime.Text = .InquiryTime
            txtAdvertisementNote.Text = .AdvertisementNote

            ''Code added by Atul Kamble on Nov 02,2010 for RallyID DE1201
            txtChildren.Text = .Children
            ''Code added by Atul Kamble on Nov 02,2010 for RallyID DE1201
        End With
    End Sub
    Private Sub txtBirthDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBirthDate.SelectedDateChanged
        Dim intAge As Integer
        If Not txtBirthDate.SelectedDate Is Nothing Then
            If DateDiff(DateInterval.Year, CDate(txtBirthDate.SelectedDate), CDate(txtDate.Value)) < 1 Then
                DisplayErrorMessage("The minimum age requirement for student is 18 ")
                'DisplayRADAlert(CallbackType.Postback, "Error15", "The minimum age requirement for student is 18 ", 350, 100, "Date Error")
                txtAge.Text = ""
                Exit Sub
            End If
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year - 1
            Dim dob As Date = txtBirthDate.SelectedDate
            dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso _
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year - 1
            txtAge.Text = intAge
        End If
    End Sub
    Private Sub txtSourceDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSourceDate.SelectedDateChanged
    End Sub
    Private Sub BuildStatusDDL(ByVal leadStatusId As String)
        'Dim Status As New LeadFacade
        Dim facade As New LeadStatusChangeFacade
        With ddlLeadStatus
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            .DataSource = facade.GetAvailLeadStatuses("", CampusId, userId) 'Status.GetNewLeadStatus()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub btnCheckDuplicates_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckDuplicates.Click
        Dim winSettings As String = "toolbar=no, status=no, resizable=yes,width=1200px,height=470px"
        'Dim winSettings As String = "toolbar=no, status=no, resizable=yes,width=900px,height=470px"
        Dim name As String = "DuplicateLead"
        Dim url As String = String.Empty
        '''' Code modified by kamalesh Ahuja on May 18 2010 to fix mantis issues id 18800 
        ''Dim url As String = "DisplayDuplicateLeads.aspx?FirstName=" + Trim(txtFirstName.Text) + "&LastName=" + Trim(txtLastName.Text) + "&SSN=" + Trim(txtSSN.Text) + "&DOB=" + Trim(txtBirthDate.Text) + "&repid=" + Trim(txtUserId.Value) + "&CampusId=" + txtCampusId.Value 
        'DE7696 6/1/2012 Janet Robinson added because DOB was changed to a raddatepicker
        If Not txtBirthDate.SelectedDate Is Nothing Then
            url = "DisplayDuplicateLeads.aspx?FirstName=" + Trim(txtFirstName.Text) + "&LastName=" + Trim(txtLastName.Text) + "&SSN=" + Trim(txtSSN.Text) + "&DOB=" + Trim(txtBirthDate.SelectedDate) + "&repid=" + Trim(txtUserId.Value) + "&CampusId=" + txtCampusId.Value + "&Address=" + txtAddress1.Text + "&Phone=" + txtphone.Text + "&Phone2=" + txtPhone2.Text + "&RequestType=" + "checkdup"
        Else
            url = "DisplayDuplicateLeads.aspx?FirstName=" + Trim(txtFirstName.Text) + "&LastName=" + Trim(txtLastName.Text) + "&SSN=" + Trim(txtSSN.Text) + "&DOB=" + "" + "&repid=" + Trim(txtUserId.Value) + "&CampusId=" + txtCampusId.Value + "&Address=" + txtAddress1.Text + "&Phone=" + txtphone.Text + "&Phone2=" + txtPhone2.Text + "&RequestType=" + "checkdup"
        End If

        '''''''''''''''
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPhone.CheckedChanged
        If chkForeignPhone.Checked = False Then
            txtphone.Mask = "(###)-###-####"
            txtphone.DisplayMask = "(###)-###-####"
            txtphone.DisplayPromptChar = ""
        Else
            txtphone.Text = ""
            txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtphone.DisplayMask = ""
            txtphone.DisplayPromptChar = ""
        End If
        Dim objCommon As New CommonWebUtilities
        txtphone.Text = ""
        'CommonWebUtilities.SetFocus(Me.Page, txtphone)
        txtphone.Focus()
    End Sub
    Private Sub chkForeignPhone2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPhone2.CheckedChanged
        If chkForeignPhone2.Checked = False Then
            txtPhone2.Mask = "(###)-###-####"
            txtPhone2.DisplayMask = "(###)-###-####"
        Else
            txtPhone2.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtPhone2.DisplayMask = ""
            lblPhone2.ToolTip = ""
        End If
        Dim objCommon As New CommonWebUtilities
        txtPhone2.Text = ""
        'CommonWebUtilities.SetFocus(Me.Page, txtPhone2)
        txtPhone2.Focus()
    End Sub

    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignZip.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        'CommonWebUtilities.SetFocus(Me.Page, txtAddress1)
        txtAddress1.Focus()
        If chkForeignZip.Checked = True Then
            txtOtherState.Enabled = True
            txtzip.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtzip.DisplayMask = ""
            txtzip.Text = String.Empty
            ddlStateID.SelectedIndex = 0
            txtzip.MaxLength = 20
        Else
            txtzip.Mask = "#####"
            'txtzip.DisplayMask = "#####"
            txtzip.Text = String.Empty
            txtzip.MaxLength = 5
        End If
    End Sub
    Private Sub ddlStateId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStateID.SelectedIndexChanged
    End Sub

    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOtherState.TextChanged
        Dim objCommon As New CommonWebUtilities
        'CommonWebUtilities.SetFocus(Me.Page, txtzip)
        txtzip.Focus()
    End Sub

    Private Sub txtCity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCity.TextChanged
        If chkForeignZip.Checked = True Then
            Dim objCommon As New CommonWebUtilities
            'CommonWebUtilities.SetFocus(Me.Page, txtOtherState)
            txtOtherState.Focus()
        End If
    End Sub
    Private Sub txtBirthDate_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBirthDate.PreRender
    End Sub
    Protected Sub btnSaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnSaveNext.Click
        useSaveNext = True
        btnSave_Click(Me, New System.EventArgs())
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.

        If Not Trim(Request.Form("scrollposition")) = "" Then
            Dim i As Integer = CInt(Trim(Request.Form("scrollposition")))
            Session("ScrollValue") = i
        End If

        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnCheckDuplicates)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub ddlSourceCategoryId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSourceCategoryId.SelectedIndexChanged
        If Not ddlSourceCategoryId.SelectedValue = "" Then
            BuildSourceTypeDDL(ddlSourceCategoryId.SelectedValue)
            ddlSourceAdvertisement.Items.Clear()
            With ddlSourceAdvertisement
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            ddlSourceTypeId.Items.Clear()
            With ddlSourceTypeId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            ddlSourceAdvertisement.Items.Clear()
            With ddlSourceAdvertisement
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Private Sub ddlSourceTypeId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSourceTypeId.SelectedIndexChanged
        If Not ddlSourceTypeId.SelectedIndex = 0 Then
            BuildSourceAdvDDL(ddlSourceTypeId.SelectedValue)
        Else
            With ddlSourceAdvertisement
                .Items.Clear()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Private Sub ddlAreaID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAreaID.SelectedIndexChanged
        If Not ddlAreaID.SelectedValue = "" Then
            BuildProgramsDDL(ddlAreaID.SelectedValue)
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            ddlProgramID.Items.Clear()
            With ddlProgramID
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
        ddlProgramID.Focus()
    End Sub
    Protected Sub ddlProgramID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProgramID.SelectedIndexChanged
        If Not ddlProgramID.SelectedIndex = 0 Then
            BuildPrgVersionDDL()
        Else
            With ddlPrgVerId
                .Items.Clear()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
        ddlPrgVerId.Focus()
    End Sub
    Private Sub NavigateToInfo()
        Dim StudentName As New StudentSearchFacade
        Dim defaultCampusId As String
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String = ""
        '  Dim state As AdvantageSessionState
        Dim fac As New UserSecurityFacade
        Dim arrUPP As New ArrayList
        Dim pURL As String

        defaultCampusId = Master.CurrentCampusId
        defaultCampusId = XmlConvert.ToGuid(defaultCampusId).ToString

        'This is used to load lead into the MRU
        Session("SEARCH") = 1

        ''Set relevant properties on the state object
        'objStateInfo.LeadId = txtLeadMasterID.Value
        'objStateInfo.NameCaption = "Lead : "
        'objStateInfo.NameValue = StudentName.GetLeadNameByID(txtLeadMasterID.Value)

        ''Create a new guid to be associated with the lead pages
        'strVID = Guid.NewGuid.ToString

        ''Add an entry to AdvantageSessionState for this guid and object load Advantage state
        'state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        'state(strVID) = objStateInfo

        ''save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        strVID = BuildLeadStateObjectRedirectToInfo(txtLeadMasterID.Value.ToString)
        Dim strCampusId As String = Master.CurrentCampusId
        arrUPP = mruProvider.checkEntityPageSecurity(AdvantageSession.UserState, 189, strCampusId, 395)
        If arrUPP.Count = 0 Then
            'User does not have permission to any resource for this submodule
            'Session("Error") = "You do not have permission to any of the pages for existing student<br> for the campus that you are logged in to."
            'Response.Redirect("../ErrorPage.aspx")
            RadNotification1.Show()
            RadNotification1.Text = "You do not have permission to any of the pages for existing student for the campus that you are logged in to."

        ElseIf mruProvider.DoesUserHasAccessToSubModuleResource(arrUPP, 170) Then
            Response.Redirect("../AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=4", True)
        Else
            'redirect to the first page that the user has permission to for the submodule
            pURL = BuildPartialURL(arrUPP(0))
            Response.Redirect(pURL & "&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=4", True)
        End If


        'arrUPP = fac.GetUserResourcePermissionsForSubModule(HttpContext.Current.Session("UserId"), 395, "Admissions", HttpContext.Current.Request.Params("cmpid"))
        'If arrUPP.Count = 0 Then
        '    'User does not have permission to any resource for this submodule
        '    Session("Error") = "The Lead was successfully added, But you do not have permission to any of the pages for existing leads<br> for the campus that you are logged in to."
        '    Response.Redirect("../ErrorPage.aspx")

        'ElseIf fac.DoesUserHasAccessToSubModuleResource(arrUPP, 170) Then
        '    Response.Redirect("../AD/LeadMaster1.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)

        'Else
        '    'redirect to the first page that the user has permission to for the submodule
        '    pURL = BuildPartialURL(arrUPP(0))
        '    Response.Redirect(pURL & "&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)
        'End If
    End Sub
    Private Function BuildPartialURL(ByVal uppInfo As UserPagePermissionInfo) As String
        Return uppInfo.Url & "?resid=" & uppInfo.ResourceId
    End Function
    Private Sub AddLeadToMRU()
        Dim objMRUFac As New MRUFacade
        Dim ds As New DataSet
        Dim objStateInfo As New AdvantageStateInfo

        ds = objMRUFac.LoadAndUpdateMRU("Leads", txtLeadMasterID.Value.ToString, txtUserId.Value.ToString, txtCampusId.Value.ToString)
        objStateInfo.MRUDS = ds

    End Sub
    Private Sub IPEDSRequirements()

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        isIPEDSApplicable = MyAdvAppSettings.AppSettings("IPEDS")
        If isIPEDSApplicable.ToLower = "yes" Then
            'Make Gender,Ethnic Code,Previous Education,Citizenship,BirthDate,state,Nationality Reqd
            ddlGender.BackColor = Color.FromName("#ffff99")
            ddlRace.BackColor = Color.FromName("#ffff99")
            ddlPreviousEducation.BackColor = Color.FromName("#ffff99")
            ddlCitizen.BackColor = Color.FromName("#ffff99")
            txtBirthDate.BackColor = Color.FromName("#ffff99")
            ddlStateID.BackColor = Color.FromName("#ffff99")
            ddlNationality.BackColor = Color.FromName("#ffff99")
        End If
    End Sub
    Private Function ValidateIPEDS() As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim strValidateIPEDS As String = ""
        isIPEDSApplicable = MyAdvAppSettings.AppSettings("IPEDS")
        If isIPEDSApplicable.ToLower = "yes" Then
            If ddlGender.SelectedValue = "" Then
                strValidateIPEDS = "Gender is required"
            End If
            If ddlRace.SelectedValue = "" Then
                strValidateIPEDS &= "Race is required" & vbLf
            End If
            If ddlPreviousEducation.SelectedValue = "" Then
                strValidateIPEDS &= "Previous Education is required" & vbLf
            End If
            If ddlCitizen.SelectedValue = "" Then
                strValidateIPEDS &= "Citizen is required" & vbLf
            End If
            If txtBirthDate.SelectedDate = "" Then
                strValidateIPEDS &= "DOB is required" & vbLf
            End If
            'If ddlStateID.SelectedValue = "" Then
            '    strValidateIPEDS &= "State is required" & vbLf
            'End If
            If ddlNationality.SelectedValue = "" Then
                strValidateIPEDS &= "Nationality is required" & vbLf
            End If
            Return strValidateIPEDS
        Else
            Return strValidateIPEDS
        End If
    End Function
    'Private Sub DisplayMessageBox(ByVal Message As String)
    '    newBox.MessageBoxButton = 3
    '    newBox.MessageBoxTop = 150
    '    newBox.MessageBoxLeft = 250
    '    newBox.MessageBoxWidth = 450
    '    newBox.MessageBoxHeight = 150
    '    newBox.MessageBoxButtonWidth = 50
    '    newBox.MessageBoxIDYes = "yes"
    '    newBox.MessageBoxIDNo = "no"
    '    newBox.MessageBoxIDCancel = "cancel"
    '    newBox.MessageBoxButtonYesText = "Yes"
    '    newBox.MessageBoxButtonNoText = "No"
    '    newBox.MessageBoxButtonCancelText = "Cancel"
    '    newBox.MessageBoxTitle = "Duplicate Leads Check"
    '    newBox.MessageBoxMessage = Message
    '    newBox.MessageBoxImage = "Information.gif"
    '    PlaceHolder1.Controls.Add(newBox)
    'End Sub
    Private Sub CheckYesNo()
        If dup.Value = "yes" Then
            AddLead()
            Exit Sub
        End If
    End Sub
    Private Sub AddLead()
        Dim LeadMasterUpdate As New LeadFacade
        Dim Result As String
        Result = LeadMasterUpdate.UpdateLeadMaster(BuildLeadMaster(txtLeadMasterID.Value), AdvantageSession.UserState.UserName, txtResumeObjective.Text)
        If Result = "" Then
            dup.Value = ""
            If Not chkIsInDB.Checked Then
                Result = AddNewTask()
            End If
            '   populate page fields with data just saved in DB
            ' BindLeadMasterData(LeadMasterUpdate.GetLeadsInfo(txtLeadMasterID.Value))

            Dim j As Integer
            j = UpdateLeadGroups(txtLeadMasterID.Value)

            '   save on syLeadStatusesChanges only if txtLeadStatusId (oldStatus) <> ddlLeadStatus.SelectedValue (newStatus)
            If ddlLeadStatus.SelectedValue <> txtLeadStatusId.Value Then
                Dim statusChanged As New LeadStatusesInfo
                With statusChanged
                    .LeadId = txtLeadMasterID.Value
                    .OrigStatusId = IIf(txtLeadStatusId.Value = "", System.Guid.Empty.ToString, txtLeadStatusId.Value)
                    .NewStatusId = ddlLeadStatus.SelectedValue
                    .ModDate = Date.Parse(txtModDate.Value)
                End With
                Result = (New LeadStatusChangeFacade).InsertLeadStatusChange(statusChanged, AdvantageSession.UserState.UserName)
                If Not Result = "" Then
                    'display error inserting into syLeadStatusesChanges table
                    DisplayErrorMessage(Result)
                    'DisplayRADAlert(CallbackType.Postback, "Error16", Result, 350, 100, "Save Error")
                    Exit Sub
                End If
                txtLeadStatusId.Value = ddlLeadStatus.SelectedValue
                BuildStatusDDL(ddlLeadStatus.SelectedValue)
                ddlLeadStatus.SelectedValue = txtLeadStatusId.Value
            End If


            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                txtPKID.Value = txtLeadMasterID.Value
                SDFControl.DeleteSDFValue(txtPKID.Value)
                SDFID = SDFControl.GetAllLabels(pnlSDF)
                SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtPKID.Value, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            If useSaveNext Then
                AddLeadToMRU()
                btnNew_Click(Me, New System.EventArgs())
            Else
                chkIsInDB.Checked = True
            End If
            InitButtonsForEdit()
            If useSaveNext = False Then
                BuildLeadStateObject()
            End If
            Exit Sub
        Else
            DisplayErrorMessage(Result)
            'DisplayRADAlert(CallbackType.Postback, "Error17", Result, 350, 100, "Save Error")
            Exit Sub
        End If
    End Sub
    Private Sub CheckForDuplicates()

    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

    Protected Sub ddlCampusId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampusId.SelectedIndexChanged
        ''''' Code Modified on 18th August to fix mantis issue id 19555
        ViewState("CampusID") = ddlCampusId.SelectedValue
        CampusId = ddlCampusId.SelectedValue
        ''''''''

        BuildDropDownLists()
    End Sub
    Public Sub BuildLeadStateObject(ByVal LeadId As String)
        Dim objStateInfo As New AdvantageStateInfo
        Dim objGetStudentStatusBar As New AdvantageStateInfo
        Dim strVID As String = ""
        Dim strLeadId As String = ""


        strLeadId = LeadId
        mruProvider.InsertMRU(4, strLeadId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)
    End Sub
    Public Function BuildLeadStateObjectRedirectToInfo(ByVal LeadId As String) As String
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String = ""
        Dim strLeadId As String = ""

        If LeadId.ToString.Trim = "" Then 'No Student was selected from MRU
            strLeadId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                                   4, _
                                                                   AdvantageSession.UserState.CampusId.ToString)
        Else
            strLeadId = LeadId
        End If

        objStateInfo = mruProvider.BuildLeadStatusBar(strLeadId)

        'Create a new guid to be associated with the employer pages
        strVID = Guid.NewGuid.ToString

        'Add an entry to AdvantageSessionState for this guid and object
        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        'UpdateMRUTable
        'Reason: We need to keep track of the last student record the user worked with
        'Scenario1 : User can log in and click on a student page without using MRU 
        'and we need to display the data of the last student the user worked with
        'If the user is a first time user, we will load the student who was last added to advantage
        'mruProvider.UpdateMRUList(strLeadId, AdvantageSession.UserState.UserId.ToString, _
        '                          AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

        mruProvider.InsertMRU(4, strLeadId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

        Return strVID

    End Function

    Protected Sub __fieldchanges_Load(sender As Object, e As System.EventArgs) Handles __fieldchanges.Load

    End Sub
End Class
