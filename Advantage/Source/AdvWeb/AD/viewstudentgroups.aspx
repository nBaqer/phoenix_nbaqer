﻿<%@ Page Title="Student Hold Groups" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="viewstudentgroups.aspx.vb" Inherits="viewstudentgroups" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button><asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                                        Enabled="False"></asp:Button></td>

                            </tr>
                        </table>
                        <div class="boxContainer full">
                            <h3><%=Header.Title  %></h3>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                <tr align="center">
                                    <td nowrap width="100%">
                                        <asp:Label ID="lblNoRecord" Text="No Group is Attached" CssClass="LabelBold" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr align="center">
                                    <td width="100%" class="DataGridHeader">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td align="center">
                                                    <table border="0" cellpadding="0" cellspacing="2">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblEnrollments" Text="Enrollments" CssClass="Label" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblEnrollmentC" Text=":" CssClass="Label" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlEnrollmentId" runat="server" CssClass="DropDownLists" Width="205px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>&nbsp;&nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="center">
                                                    <table border="0" cellpadding="0" cellspacing="2">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblGroup" Text="Group" CssClass="Label" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblGroupC" Text=":" CssClass="Label" runat="server"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlGroup" runat="server" CssClass="DropDownLists" Width="205px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>&nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnAddGroup" runat="Server" Text=" Add " CssClass="rfdSkinnedButton" Width="75px" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <div class="boxContainer noBorder">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>

                                        <table width="100%" align="center" role="grid">
                                            <tr>
                                                <td width="100%" class="k-grid ">
                                                    <asp:DataGrid ID="dgrdStudentGroup" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="False" BorderStyle="Solid" ShowFooter="False" BorderWidth="1px"
                                                        BorderColor="#E0E0E0"
                                                        Width="99%">
                                                        <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                        <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                                        <FooterStyle CssClass="datagridpadding"></FooterStyle>
                                                        <EditItemStyle CssClass="datagridpadding"></EditItemStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Enrollment">
                                                                <HeaderStyle CssClass="k-grid-header" Width="20%"></HeaderStyle>
                                                                <ItemStyle></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEnrollment" Text='<%# Container.DataItem("EnrollMents") %>' CssClass="Label"
                                                                        runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Group">
                                                                <HeaderStyle CssClass="k-grid-header" Width="20%"></HeaderStyle>
                                                                <ItemStyle></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblGroupDescrip" Text='<%# Container.DataItem("Descrip") %>' CssClass="Label"
                                                                        runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Date Added">
                                                                <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>
                                                                <ItemStyle></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDateAdded" Text='<%# DataBinder.Eval(Container.DataItem,"DateAdded","{0:d}") %>'
                                                                        CssClass="Label" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="User">
                                                                <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>
                                                                <ItemStyle></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOwnerName" Text='<%# Container.DataItem("OwnerName") %>' CssClass="Label"
                                                                        runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Date Removed">
                                                                <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>
                                                                <ItemStyle></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDateRemoved" Text='<%# DataBinder.Eval(Container.DataItem,"DateRemoved","{0:d}") %>'
                                                                        CssClass="Label" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="User">
                                                                <HeaderStyle CssClass="k-grid-header" Width="10%"></HeaderStyle>
                                                                <ItemStyle></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblModUser" Text='<%# Container.DataItem("ModUser") %>' CssClass="Label"
                                                                        runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

