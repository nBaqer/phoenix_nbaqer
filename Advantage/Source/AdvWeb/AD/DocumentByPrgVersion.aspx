
<%@ Page Language="vb" AutoEventWireup="false" Inherits="DocumentByPrgVersion" CodeFile="DocumentByPrgVersion.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Documents By Program Version</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
    <link rel="stylesheet" type="text/css" href="../css/systememail.css" />
</head>
<body id="Body1" leftmargin="0" topmargin="0" runat="server">
    <form id="Form1" method="post" runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="toppopupheader">
                <img src="../images/advantage_docstatus.jpg" alt="doc status"/>
            </td>
            <td class="topemail">
                <a class="close" onclick="top.close()" href="#">X Close</a>
            </td>
        </tr>
    </table>
    <!--begin right column-->
    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="90%" border="0">
        <tr>
            <td class="detailsframe">
                <div class="scrollwholedocs">
                    <!-- begin content table-->
                    <table width="90%" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="twocolumnlabelcell">
                                <asp:Label ID="lblLead" runat="server" CssClass="label" Visible="False">Lead:</asp:Label>
                            </td>
                            <td class="twocolumncontentcell" style="padding-bottom: 20px">
                                <asp:Label ID="lblHeading" runat="server" CssClass="labelbold" Visible="False"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table width="90%" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <asp:DataGrid ID="dgrdTransactionSearch" runat="server" BorderColor="#E0E0E0" AllowSorting="True"
                                    AutoGenerateColumns="False" BorderStyle="Solid" ShowFooter="False" BorderWidth="1px"
                                    Width="90%">
                                    <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                    <HeaderStyle CssClass="DataGridHeaderStyle"></HeaderStyle>
                                    <FooterStyle CssClass="datagridpadding"></FooterStyle>
                                    <EditItemStyle CssClass="datagridpadding"></EditItemStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Documents">
                                            <HeaderStyle CssClass="DataGridHeaderStyle" Width="60%"></HeaderStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label Text='<%# Container.DataItem("DocumentDescrip")%> ' runat="server" CssClass="label"
                                                    ID="Label4" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Required">
                                            <HeaderStyle CssClass="DataGridHeaderStyle" Width="10%"></HeaderStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label Text='<%# GetBool(Container.DataItem("Required")) %> ' runat="server"
                                                    CssClass="label" ID="Label5" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Document Status">
                                            <HeaderStyle CssClass="DataGridHeaderStyle" Width="30%"></HeaderStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Label Text='<%# Container.DataItem("DocStatusDescrip")%> ' runat="server" CssClass="label"
                                                    ID="Label7" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                    <!--end content table-->
                </div>
            </td>
        </tr>
    </table>
    <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
    </form>
</body>
</html>
