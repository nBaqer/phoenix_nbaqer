Imports System.Diagnostics
Imports FAME.Advantage.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Advantage.Business.Logic.Layer

Partial Class ViewLeadEntranceTest
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
#End Region

    Dim Leadid As String
    Dim campusId As String
    Dim userId As String
    Private pObj As New UserPagePermissionInfo


    Protected StudentId As String
    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()


    End Sub
    Private Function GetLeadFromStateObject(ByVal paramResourceId As Integer) As StudentMRU

        Dim objStudentState As New StudentMRU

        Try

            MyBase.GlobalSearchHandler(1)

            boolSwitchCampus = Master.Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                Leadid = Guid.Empty.ToString()
            Else
                Leadid = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(Leadid)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.Master.ShowHideStatusBarControl(True)

            Master.Master.PageObjectId = Leadid
            Master.Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
            Master.Master.setHiddenControlForAudit()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here

        '        Dim m_Context As HttpContext
        Dim resourceId As Integer
        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        userId = advantageUserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New StudentMRU
        objStudentState = GetLeadFromStateObject(225) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            StudentId = .StudentId.ToString
            Leadid = .LeadId.ToString
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If
        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''



        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'If pObj.HasFull Or pObj.HasDelete Then
        '    btnDelete.Enabled = True
        'End If

        'Delete Should Always Be Disabled
        btnDelete.Enabled = False
        btnNew.Enabled = False

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            BindDataGrid()

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""

        End If
        'Code to disable all buttons if lead was already enrolled
        'to prevent data being changed after lead was enrolled
        Dim chkLeadWasEnrolled As Boolean
        Dim leadFacade As New LeadEnrollmentFacade
        chkLeadWasEnrolled = leadFacade.CheckIfLeadWasEnrolled(Leadid)
        If chkLeadWasEnrolled = True Then
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
            'btnNew.ToolTip = "Cannot add lead employment information as the lead was already enrolled"
            btnSave.ToolTip = "Cannot modify entrance test information as the lead was already enrolled"
            btnDelete.ToolTip = "Cannot delete entrance test as the lead was already enrolled"
        Else
            btnNew.ToolTip = ""
            btnSave.ToolTip = ""
            btnDelete.ToolTip = ""
        End If
    End Sub
    Private Sub BindDataGrid()
        Dim getDB As New LeadEntranceFacade
        Dim prgVerId As String
        prgVerId = getDB.GetPrgVersionByLead(Leadid)
        If prgVerId <> "" Then
            dgrdTransactionSearch.DataSource = getDB.GetGridRequirementDetailsByEffectiveDates(Leadid, prgVerId, campusId) 'getDB.GetGridRequirementDetails(Leadid, prgVerId)
            dgrdTransactionSearch.DataBind()
        Else
            Dim ds As New DataSet
            ds = getDB.GetAllStandardRequirementsByEffectiveDates(Leadid, campusId)
            ds.Tables(0).DefaultView.Sort = " Descrip asc"
            'If Not ds Is Nothing Then
            '    ds = ds.Tables(0).Select(" Descrip Asc ")
            'End If
            dgrdTransactionSearch.DataSource = ds.Tables(0).DefaultView
            dgrdTransactionSearch.DataBind()
        End If
    End Sub
    Public Sub SetUpEntranceTest()
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        'Dim strEntrTestId As String
        'Dim intRequired As Integer
        Dim intArrCount As Integer
        '  Dim intOverRideCount As Integer
        Dim intResult As Integer
        Dim z As Integer
        Dim Facade As New LeadFacade
        Dim sMessage As String

        ' Save the datagrid items in a collection.
        iitems = dgrdTransactionSearch.Items
        Try
            'Loop thru the collection to get the Array Size

            Dim strAlpha As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            strAlpha = strAlpha.ToLower.ToString
            sMessage = ""
            Dim sMessageCharacters As String = ""

            Dim strActualValue As String
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                Dim z1 As Integer
                If Not Trim(CType(iitem.FindControl("txtActualScore"), TextBox).Text) = "" Then
                    For z1 = 1 To CType(iitem.FindControl("txtActualScore"), TextBox).Text.Length
                        strActualValue = Mid(CType(iitem.FindControl("txtActualScore"), TextBox).Text.ToLower.ToString, z1, 1)
                        If InStr(strAlpha, strActualValue) >= 1 Then
                            sMessageCharacters &= "No characters are allowed in Actual Score for " & CType(iitem.FindControl("lblEntrTestDescrip"), Label).Text & vbLf
                            Exit For
                        End If
                    Next
                End If
            Next
            If Not sMessageCharacters = "" Then
                DisplayErrorMessage(sMessageCharacters)
                Exit Sub
            End If
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                ' If Not CType(iitem.FindControl("chkOverRide"), CheckBox).Checked = True Then
                If Not Trim(CType(iitem.FindControl("txtActualScore"), TextBox).Text) = "" Then
                    If CInt(CType(iitem.FindControl("txtActualScore"), TextBox).Text) >= 1 Then
                        'If CType(iitem.FindControl("txtTestTaken"), TextBox).Text Then
                        If Trim(CType(iitem.FindControl("txtTestTaken"), TextBox).Text) = "" Then
                            sMessage &= "Completed date is required for " & CType(iitem.FindControl("lblEntrTestDescrip"), Label).Text & vbLf
                        End If
                        'End If
                    End If
                End If
                'End If
            Next

            If Not sMessage = "" Then
                DisplayErrorMessage(sMessage)
                Exit Sub
            End If

            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                ' If Not (CType(iitem.FindControl("txtTestTaken"), TextBox).Text) = "" Or CType(iitem.FindControl("chkOverRide"), CheckBox).Checked = True Then
                intArrCount += 1
                'End If
            Next

            'For i = 0 To iitems.Count - 1
            '    iitem = iitems.Item(i)
            '    intOverRideCount += 1
            'Next

            'If intOverRideCount >= 1 Then
            '    Dim overrideselectedEntranceTest() As String = overrideselectedEntranceTest.CreateInstance(GetType(String), intOverRideCount)
            '    Dim overselectedOverRide() As String = selectedOverRide.CreateInstance(GetType(String), intArrCount)
            'End If

            If intArrCount >= 1 Then
                Dim selectedEntranceTest() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedRequired() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedPass() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedTestTaken() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedActualScore() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedMinScore() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedComments() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedOverRide() As String = Array.CreateInstance(GetType(String), intArrCount)

                'Loop Through The Collection To Get ClassSection

                For i = 0 To iitems.Count - 1
                    iitem = iitems.Item(i)
                    'retrieve clssection id from the datagrid

                    'Modified by balaji on 10/13/2005
                    '  If Not (CType(iitem.FindControl("txtTestTaken"), TextBox).Text) = "" Or CType(iitem.FindControl("chkOverRide"), CheckBox).Checked = True Then
                    selectedEntranceTest.SetValue(CType(iitem.FindControl("lblEntrTestId"), Label).Text, z)
                    If (CType(iitem.FindControl("chkRequired"), CheckBox).Checked = True) Then
                        selectedRequired.SetValue("Yes", z)
                    Else
                        selectedRequired.SetValue("No", z)
                    End If
                    If (CType(iitem.FindControl("chkPass"), CheckBox).Checked = True) Then
                        selectedPass.SetValue("Yes", z)
                    Else
                        selectedPass.SetValue("No", z)
                    End If
                    selectedTestTaken.SetValue(CType(iitem.FindControl("txtTestTaken"), TextBox).Text, z)
                    selectedActualScore.SetValue(CType(iitem.FindControl("txtActualScore"), TextBox).Text, z)
                    selectedMinScore.SetValue(CType(iitem.FindControl("lblMinscore"), Label).Text, z)
                    selectedComments.SetValue(CType(iitem.FindControl("txtComments"), TextBox).Text, z)
                    If (CType(iitem.FindControl("chkPass"), CheckBox).Checked = True) Then
                        selectedOverRide.SetValue("No", z)
                    Else
                        If (CType(iitem.FindControl("chkOverRide"), CheckBox).Checked = True) Then
                            selectedOverRide.SetValue("Yes", z)
                        Else
                            selectedOverRide.SetValue("No", z)
                        End If
                    End If
                    z += 1
                    ' End If
                Next

                'resize the array
                'If z > 0 Then ReDim Preserve selectedClsSection(z - 1)
                'If z > 0 Then ReDim Preserve selectedRequired(z - 1)
                intResult = Facade.InsertEntranceTest(Leadid, selectedEntranceTest, selectedRequired, selectedPass, selectedTestTaken, selectedActualScore, selectedMinScore, selectedComments, selectedOverRide, AdvantageSession.UserState.UserName)
            End If
            BindDataGrid()
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click


        ''''' Code changes to Fix mantis issue id 19549 by kamalesh Ahuja on 23 Aug 2010 '''
        Dim userId As String
        userId = XmlConvert.ToGuid(AdvantageSession.UserState.UserId.ToString).ToString

        Dim LeadMasterUpdate As New LeadFacade
        If MyAdvAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "no" Then
            If LeadMasterUpdate.CheckLeadUser(userId, Leadid) = 0 Then
                DisplayErrorMessage("You do not have rights to edit this lead")
                Exit Sub
            End If
        End If
        ''''''''''''''''''

        SetUpEntranceTest()
        BindDataGrid()
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub dgrdTransactionSearch_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdTransactionSearch.ItemDataBound
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        iitems = dgrdTransactionSearch.Items
        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
            '' Code Changes by Atul Kamble on 20 October to Fix Rally Issue id DE1055
            If Trim(CType(iitem.FindControl("txtActualScore"), TextBox).Text) = "0.00" Then
                ''If Trim(CType(iitem.FindControl("txtActualScore"), TextBox).Text) = "0" Then
                ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
                CType(iitem.FindControl("txtActualScore"), TextBox).Text = ""
            End If
        Next
        ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim txtActualScore As TextBox = CType(e.Item.FindControl("txtActualScore"), TextBox)
            txtActualScore.Attributes.Add("onkeypress", "return DecimalNumbers(" + txtActualScore.ClientID + ", 3)")
        End If
        ''New Code Added By Vijay Ramteke On September 30, 2010 For Mantis Id 17767
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        '        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
End Class
