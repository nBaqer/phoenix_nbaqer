Imports Fame.Common
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class Colleges
    Inherits BasePage
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected strDefaultCountry As String
    Dim resourceId As Integer
    Dim campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim campusId As String
        'Dim userId As String
        Dim mContext As HttpContext

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString
        campusId = Master.CurrentCampusId 'AdvantageSession.UserState.CampusId.ToString
        'userId = AdvantageSession.UserState.UserId.ToString

        mContext = HttpContext.Current
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = resourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        'Check if this page still exists in the menu while switching campus

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString
        Dim objListGen As New DataListGenerator(resourceId)
        Try
            If Not Page.IsPostBack Then
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                txtcollegeid.Text = System.Guid.NewGuid.ToString

                'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                ds = objListGen.SummaryListGenerator(userId, campusId)

                PopulateDataList(ds)

                ds.WriteXml(sw)
                Try
                    ddlcountryid.SelectedValue = strDefaultCountry
                Catch ex As System.Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlcountryid.SelectedIndex = 0
                End Try
                ViewState("ds") = sw.ToString()
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")

                If chkforeignzip.Checked = True Then
                    'txtzip.Text = String.Empty
                    ddlstateid.Enabled = False
                    txtotherstate.Visible = True
                    lblotherstate.Visible = True
                Else
                    ddlstateid.Enabled = True
                    txtotherstate.Visible = False
                    lblotherstate.Visible = False
                End If
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Function ValidateFieldsWithInputMasks() As String
        Dim facInputMasks As New InputMasksFacade
        Dim correctFormat1 As Boolean '' correctFormat,, correctFormat2, correctFormat3
        Dim strMask As String
        Dim zipMask As String
        Dim errorMessage As String

        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        'Validate the phone field format. If the field is empty we should not apply the mask
        'agaist it.
        errorMessage = ""

        If txtphone.Text <> "" And chkforeignphone.Checked = False Then
            correctFormat1 = facInputMasks.ValidateStringWithInputMask(strMask, txtphone.Text)
            If correctFormat1 = False Then
                errorMessage &= "Incorrect format for phone field." & vbLf
            End If
        End If

        If txtzip.Text <> "" And chkforeignzip.Checked = False Then
            correctFormat1 = facInputMasks.ValidateStringWithInputMask(zipMask, txtzip.Text)
            If correctFormat1 = False Then
                errorMessage &= "Incorrect format for zip field." & vbLf
            End If
        End If

        Return Trim(errorMessage)
    End Function

    Private Sub BtnSaveClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New System.IO.StringWriter
        Dim msg As String

        Try
            If ViewState("MODE") = "NEW" Then
                'Set the Primary Key value, so it can be used later by the DoUpdate method
                objCommon.PagePK = txtcollegeid.Text
                msg = objCommon.DoInsert(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), chkforeignphone.Checked, False, chkforeignzip.Checked)
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If

                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstcolleges.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"

            ElseIf ViewState("MODE") = "EDIT" Then
                msg = objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), chkforeignphone.Checked, False, chkforeignzip.Checked)
                If msg = "" Then
                    ds = objListGen.SummaryListGenerator(userId, campusId)
                    dlstcolleges.SelectedIndex = -1
                    PopulateDataList(ds)
                    ds.WriteXml(sw)
                    ViewState("ds") = sw.ToString()
                Else
                    DisplayErrorMessage(msg)
                End If

            End If

            '   set Style to Selected Item
            CommonWebUtilities.RestoreItemValues(dlstcolleges, txtcollegeid.Text)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub BtnNewClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))

        Try
            ClearRhs()
            ds.ReadXml(sr)
            PopulateDataList(ds)
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtcollegeid.Text = System.Guid.NewGuid.ToString
            txtphone.Text = ""
            txtzip.Text = ""
            txtphone.Mask = "(###)-###-####"
            txtphone.DisplayMask = "(###)-###-####"
            txtzip.Mask = "#####"
            txtzip.DisplayMask = "#####"

            Try
                ddlcountryid.SelectedValue = strDefaultCountry
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                ddlcountryid.SelectedIndex = 0
            End Try
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstColleges, System.Guid.Empty.ToString, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstcolleges, Guid.Empty.ToString)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub BtnDeleteClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim objcommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim intCheckChild As Integer
        Dim studentfacade As New StudentMasterFacade
        Dim msg As String
        Try
            'Check For Child Records 
            intCheckChild = studentfacade.checkChildExists(txtcollegeid.Text)

            If intCheckChild >= 1 Then
                DisplayErrorMessage("You can not delete this item because it is being referenced in another page")
                Exit Sub
            End If

            'Delete from database and clear the screen as well. 
            'And set the mode to "NEW" since the record has been deleted from the db.
            msg = objcommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            If msg = "" Then
                ClearRhs()
                txtphone.Mask = "(###)-###-####"
                txtphone.DisplayMask = "(###)-###-####"
                txtzip.Mask = "#####"
                txtzip.DisplayMask = "#####"
                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstcolleges.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                txtcollegeid.Text = System.Guid.NewGuid.ToString
                Try
                    ddlcountryid.SelectedValue = strDefaultCountry
                Catch ex As System.Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlcountryid.SelectedIndex = 0
                End Try
                CommonWebUtilities.RestoreItemValues(dlstcolleges, Guid.Empty.ToString)
            Else
                DisplayErrorMessage(msg)
            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRhs()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
                If TypeOf ctl Is Telerik.Web.UI.RadMaskedTextBox Then
                    CType(ctl, Telerik.Web.UI.RadMaskedTextBox).Text = String.Empty
                End If
            Next
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub DlstCollegesItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstcolleges.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim objListGenerator As New DataListGenerator
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Dim strGuid As String
        Dim objCommon As New CommonUtilities
        Try
            Master.PageObjectId = e.CommandArgument
            Master.PageResourceId = resourceId
            Master.SetHiddenControlForAudit()

            strGuid = dlstcolleges.DataKeys(e.Item.ItemIndex).ToString()
            objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGuid)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"

            selIndex = e.Item.ItemIndex
            dlstcolleges.SelectedIndex = selIndex
            ds.ReadXml(sr)
            PopulateDataList(ds)

            If chkforeignzip.Checked = True Then
                ddlstateid.Enabled = False
                txtotherstate.Visible = True
                lblotherstate.Visible = True
            Else
                ddlstateid.Enabled = True
                txtotherstate.Visible = False
                lblotherstate.Visible = False
            End If

            CommonWebUtilities.RestoreItemValues(dlstcolleges, e.CommandArgument)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstColleges_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstColleges_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ChkStatusCheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim objCommon As New CommonUtilities

        Try
            ClearRhs()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"
            txtcollegeid.Text = System.Guid.NewGuid.ToString

            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was refactored by AVP on 6/22/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radstatus.SelectedItem.Text.ToLower = "active") Or (radstatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radstatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        Dim dv As New DataView(ds.Tables(0), sDataFilter, "CollegeName", DataViewRowState.CurrentRows)

        With dlstcolleges
            .DataSource = dv
            .DataBind()
        End With
        dlstcolleges.SelectedIndex = -1

    End Sub

    Private Sub RadStatusSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim objCommon As New CommonUtilities

        Try
            ClearRhs()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtcollegeid.Text = Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
            'Header1.EnableHistoryButton(False)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ChkForeignZipCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkforeignzip.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtaddress1)
        If chkforeignzip.Checked = True Then
            ddlstateid.Enabled = False
            txtotherstate.Visible = True
            lblotherstate.Visible = True
            txtzip.Mask = "####################"
            txtzip.DisplayMask = "####################"
            txtzip.Text = String.Empty

            Dim ctrl As New Control
            Dim ctrl2 As New Control
            'cvStateId106
            If Not MyBase.FindControlRecursive("rfvStateId106") Is Nothing Then
                ctrl = MyBase.FindControlRecursive("rfvStateId106")
                Dim stateVal As RequiredFieldValidator = CType(ctrl, RequiredFieldValidator)
                stateVal.Enabled = False
            End If
            If Not MyBase.FindControlRecursive("cvStateId106") Is Nothing Then
                ctrl = MyBase.FindControlRecursive("cvStateId106")
                Dim stateVal2 As CompareValidator = CType(ctrl, CompareValidator)
                stateVal2.Enabled = False
            End If
        Else
            ddlstateid.Enabled = True
            txtotherstate.Visible = False
            lblotherstate.Visible = False
            txtzip.Mask = "#####"
            txtzip.DisplayMask = "#####"
            txtzip.Text = String.Empty
        End If
    End Sub

    Protected Sub ChkforeignphoneCheckedChanged(sender As Object, e As System.EventArgs) Handles chkforeignphone.CheckedChanged
        If chkforeignphone.Checked = True Then
            txtphone.Text = ""
            txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtphone.DisplayMask = ""
            txtphone.DisplayPromptChar = ""
        Else
            txtphone.Mask = "(###)-###-####"
            txtphone.DisplayMask = "(###)-###-####"
            txtphone.DisplayPromptChar = ""
        End If

    End Sub

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        '     CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

End Class