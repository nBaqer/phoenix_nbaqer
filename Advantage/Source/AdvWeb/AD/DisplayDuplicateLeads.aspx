<%@ Page Language="vb" AutoEventWireup="false" Inherits="DisplayDuplicateLeads" CodeFile="DisplayDuplicateLeads.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title>Display Leads</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR"/>
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE"/>
    <meta content="JavaScript" name="vs_defaultClientScript"/>
    <link rel="stylesheet" type="text/css" href="../css/systememail.css"/>
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema"/>
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
    <link href="../css/systememail.css" type="text/css" rel="stylesheet"/>
    <script language="javascript" type="text/javascript">
        function CloseWindow() {
            window.close();
        }
        function windowfocus() {
            window.focus();
        }
        function yesclick() {
            var dupValue = window.opener.document.getElementById("dup");
            dupValue.value = "yes";
            //window.opener.document.forms("form1").submit();
            window.opener.document.forms[0].submit();
            window.close()
        }

        function noclick() {
            var dupValue = window.opener.document.getElementById("dup");
            dupValue.value = "no";
            window.close();
        }

        function cancelclick() {
            window.close();
        } 
    </script>
</head>
<body id="Body1" leftmargin="0" topmargin="0" language="javascript" onload="windowfocus();">
    <form id="Form1" method="post" runat="server">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img src="../images/advantage_check_duplicate.jpg">
            </td>
            <td class="topemail">
                <a class="close" onclick="top.close()" href="#">X Close</a>
            </td>
        </tr>
    </table>
    <!--begin right column-->
    <table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="100%"
        border="0">
        <tbody>
            <tr>
                <td class="detailsframe">
                    <div class="scrollwholedocs">
                        <!-- begin content table-->
                        <table width="100%" align="center" border="0">
                            <asp:Button ID="btnCheckDuplicates" Text="Close" CssClass="delete" runat="server"
                                Visible="false"></asp:Button>
                            <tr>
                                <td>
                                    <asp:DataGrid ID="dgrdTransactionSearch" CellPadding="0" BorderWidth="1px" BorderStyle="Solid" 
                                        BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true"
                                        EditItemStyle-Wrap="false" GridLines="Horizontal" Width="100%" runat="server">
                                        <EditItemStyle Wrap="False"></EditItemStyle>
                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                         <Columns>
                                            <asp:TemplateColumn HeaderText="Last Name" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLastName" runat="server" Text='<%# Container.DataItem("LastName") %>'>
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="First Name" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDgFirstName" Text='<%# Container.DataItem("FirstName") %>' runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="SSN" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" Text='<%# Container.DataItem("SSN") %>' runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="DOB" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"BirthDate", "{0:d}")  %>'
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Address" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"Address1") %>'
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Phone" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"Phone") %>'
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Phone 2" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"Phone2") %>'
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Campus" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"CampDescrip") %>'
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Admission Rep" HeaderStyle-HorizontalAlign="left"
                                                ItemStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="left"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:Label ID="Label2" Text='<%# DataBinder.Eval(Container.DataItem,"FullName") %>'
                                                        runat="server">
                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>                            
                        </table>
                        <table width="50%">
                            <tr align="right">
                                <td>
                                    <asp:Label runat="server" ID="lblMessage1" Width="1000px" CssClass="Label"></asp:Label>
                                </td>
                            </tr>
                            <tr align="right">
                                <td>
                                    <asp:Label runat="server" ID="lblMessage2" Width="1000px" CssClass="Label"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <table runat="server" id="tblButtons" width="50%" visible="false">
                            <tr>
                                <td>
                                    <asp:Button ID="btnyes" Text="Yes" runat="server"  Width="75px" />
                                </td>
                                <td>
                                    <asp:Button ID="btnno" Text="No" runat="server"  Width="75px" />
                                </td>
                                <td>
                                    <asp:Button ID="btncancel" Text="Cancel" runat="server"  Width="75px" />
                                </td>
                            </tr>
                        </table>
                        <!-- end content table -->
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
   <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
    </form>
</body>
</html>
