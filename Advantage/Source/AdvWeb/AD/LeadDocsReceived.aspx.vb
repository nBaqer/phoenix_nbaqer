﻿Imports System.Diagnostics
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Advantage.Business.Objects
Imports Telerik.Web.UI
Imports System.Collections
Imports FAME.Advantage.Common
Imports System.IO

Partial Class LeadDocsReceived
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents ddlScannedId As DropDownList
    Protected WithEvents txtStudentDocs As TextBox
    Protected strCount As String
    Protected WithEvents btnhistory As Button
    Protected WithEvents lblDocumentModule As Label
    Protected WithEvents ddlDocumentModule As DropDownList
    Protected WithEvents Label1 As Label
    'Protected WithEvents Label2 As System.Web.UI.WebControls.Label
    Protected WithEvents ddlModulesId As DropDownList
    Protected WithEvents ddlDocumentTypeId As DropDownList
    Protected WithEvents lblStudentName As Label
    Protected WithEvents ddlStudentName As DropDownList
    Protected WithEvents txtStudentName As TextBox
    Protected WithEvents lblStudentId As Label
    Protected WithEvents ddlStudentId As DropDownList
    Protected StudentId As String '= "14094BD8-18CA-4B95-9195-069D47774306"
    Protected WithEvents lblModuleId As Label
    Protected WithEvents lnkPrgVersion As LinkButton
#End Region

    Private pObj As New UserPagePermissionInfo
    Protected selectedModule As Integer
    Protected Leadid As String
    Protected PrgVerId As String
    Protected strLeadName As String
    Dim campusId As String
    Dim userId As String
    Dim resouceId As Integer

    'Private mruProvider As MRURoutines
    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
   
    End Sub
    Private Function GetLeadFromStateObject(ByVal paramResourceId As Integer) As StudentMRU

        Dim objStudentState As New StudentMRU

        Try

            MyBase.GlobalSearchHandler(1)

            boolSwitchCampus = Master.Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                Leadid = Guid.Empty.ToString()
            Else
                Leadid = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(Leadid)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.Master.ShowHideStatusBarControl(True)

            Master.Master.PageObjectId = Leadid
            Master.Master.PageResourceId = Request.QueryString("resid")
            Master.Master.setHiddenControlForAudit()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function


#End Region
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        '  Dim m_Context As HttpContext
        ' Dim fac As New UserSecurityFacade
        'Dim strVID As String
        'Dim state As AdvantageSessionState

        'Put user code to initialize the page here
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        resouceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString
        Dim advantageUserState As User = AdvantageSession.UserState
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resouceId, campusId)

        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As StudentMRU = GetLeadFromStateObject(313) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            StudentId = .StudentId.ToString
            Leadid = .LeadId.ToString

        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''
        Dim getDB As New LeadEntranceFacade
        PrgVerId = getDB.GetPrgVersionByLead(Leadid)

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            strCount = 0
            Dim content As ContentPlaceHolder = CType(DirectCast(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder)
            objCommon.SetCaptionsAndColorRequiredFields(content)
            'objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

            'Disable the new and delete buttons
            objCommon.SetBtnState(content, "NEW", pObj)
            'objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            'Bind The DropDownList
            BuildDropDownLists()

            'Get PK Value
            txtStudentDocId.Text = Guid.NewGuid.ToString()

            'Get Default Document when page is loaded for first time
            ddlDocumentId.Enabled = True
            BindDataList(Leadid)

           
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            If Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value) Then
                RadGrid1.Visible = False
            End If

            uSearchEntityControlId.Value = ""


        Else
            Dim content As ContentPlaceHolder = CType(DirectCast(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder)
            objCommon.SetCaptionsAndColorRequiredFields(content)
            'objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
        End If

        'Code to disable all buttons if lead was already enrolled
        'to prevent data being changed after lead was enrolled
        Dim chkLeadWasEnrolled As Boolean
        Dim leadFacade As New LeadEnrollmentFacade
        chkLeadWasEnrolled = leadFacade.CheckIfLeadWasEnrolled(Leadid)
        ViewState("chkLeadWasEnrolled") = chkLeadWasEnrolled
        If chkLeadWasEnrolled = True Then
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
            btnNew.ToolTip = "Cannot add new document as the lead was already enrolled"
            btnSave.ToolTip = "Cannot modify document as the lead was already enrolled"
            btnDelete.ToolTip = "Cannot delete document as the lead was already enrolled"
        Else
            btnNew.ToolTip = ""
            btnSave.ToolTip = ""
            btnDelete.ToolTip = ""
            If Not Page.IsPostBack Then
                InitButtonsForLoad()
            Else
                '  InitButtonsForEdit()
            End If
        End If

        ddlDocumentId.BackColor = Color.White
        ddlDocStatusId.BackColor = Color.White

    End Sub
    Private Sub EnableBtnStudentType()
    End Sub
    'Private Sub BuildSession()
    '    With ddlTrackCode
    '        .DataTextField = "AgencyDescrip"
    '        .DataValueField = "RptAgencyFldValId"
    '        .DataSource = (New ReportingAgencies).BuildSession("TrackCode", txtStudentDocId.Text)
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    '    With ddlDueDate
    '        .DataTextField = "AgencyDescrip"
    '        .DataValueField = "RptAgencyFldValId"
    '        .DataSource = (New ReportingAgencies).BuildSession("DueDate", txtStudentDocId.Text)
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", FormatDateTime(Date.Now, DateFormat.ShortDate)))
    '        .SelectedIndex = 0
    '    End With
    '    With ddlTransDate
    '        .DataTextField = "AgencyDescrip"
    '        .DataValueField = "RptAgencyFldValId"
    '        .DataSource = (New ReportingAgencies).BuildSession("TransDate", txtStudentDocId.Text)
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", FormatDateTime(Date.Now, DateFormat.ShortDate)))
    '        .SelectedIndex = 0
    '    End With
    '    With ddlCompletedDate
    '        .DataTextField = "AgencyDescrip"
    '        .DataValueField = "RptAgencyFldValId"
    '        .DataSource = (New ReportingAgencies).BuildSession("CompletedDate", txtStudentDocId.Text)
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", FormatDateTime(Date.Now, DateFormat.ShortDate)))
    '        .SelectedIndex = 0
    '    End With
    '    With ddlNotificationCode
    '        .DataTextField = "AgencyDescrip"
    '        .DataValueField = "RptAgencyFldValId"
    '        .DataSource = (New ReportingAgencies).BuildSession("NotificationCode", txtStudentDocId.Text)
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildExistingDocumentListDDL(ByVal leadDocId As String)
        'Bind the Document DropDownList
        Dim DocumentList As New LeadDocsFacade
        Dim docsFacade As New LeadEntranceFacade
        ddlDocumentId.Items.Clear()
        With ddlDocumentId
            .DataTextField = "DocumentDescrip"
            .DataValueField = "DocumentId"
            If PrgVerId <> "" Then
                .DataSource = docsFacade.GetAllExistingStandardDocumentsByEffectiveDatesAndPrgVersion(Leadid, PrgVerId, leadDocId, campusId)
            Else
                'Modified on 09/02/2005 by balaji
                .DataSource = docsFacade.GetAllExistingStandardDocumentsByEffectiveDates(Leadid, campusId, leadDocId)
            End If
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildDocumentListDDL()
        'Bind the Document DropDownList
        'Dim DocumentList As New LeadDocsFacade
        Dim docsFacade As New LeadEntranceFacade
        'If Not Session("PrgVerId") = "" Then
        '    PrgVerId = Trim(CType(Session("PrgVerId"), String))
        'Else
        '    PrgVerId = ""
        'End If
        With ddlDocumentId
            .DataTextField = "DocumentDescrip"
            .DataValueField = "DocumentId"
            Dim noused As Guid
            If Guid.TryParse(PrgVerId, noused) Then
                .DataSource = docsFacade.GetAllStandardDocumentsByEffectiveDatesAndPrgVersion(Leadid, PrgVerId, campusId)
            Else
                'Modified on 09/02/2005 by balaji
                .DataSource = docsFacade.GetAllStandardDocumentsByEffectiveDates(Leadid, campusId)
            End If
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildDropDownLists()
        'Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        'ddlList.Add(New AdvantageDDLDefinition(ddlDocStatusId, AdvantageDropDownListName.DocStatuses, campusId, True, False, String.Empty))
        'ddlList.Add(New AdvantageDDLDefinition(ddlDocFilterStatus, AdvantageDropDownListName.DocStatuses, campusId, True, False, String.Empty))
        'CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
        BuildDocumentListDDL()
        BuildDocumentStatusDDL()
        BuildDocumentFilterStatusDDL()
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim StudentDocs As New LeadDocsFacade
        Dim Result As String
        '        Dim StudentDocsId As String
        Dim message As String = ""
        strCount = 0

        ''''' Code changes to Fix mantis issue id 19549 by kamalesh Ahuja on 23 Aug 2010 '''
        'Dim userId As String
        'userId = XmlConvert.ToGuid(AdvantageSession.UserState.UserId.ToString).ToString

        Dim LeadMasterUpdate As New LeadFacade
        If MyAdvAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "no" Then
            If LeadMasterUpdate.CheckLeadUser(userId, Leadid) = 0 Then
                'DisplayErrorMessage("You do not have rights to edit this lead")
                DisplayRADAlert(CallbackType.Postback, "Error1", "You do not have rights to edit this lead", "Save Error")
                Exit Sub
            End If
        End If
        ''''''''''''''''''

        If ddlDocumentId.SelectedValue = "" Then
            message &= "Document is required" & vbLf
        End If

        If ddlDocStatusId.SelectedValue = "" Then
            message &= "Document status is required" & vbLf
        End If

        If Not message = "" Then
            'DisplayErrorMessage(message)
            DisplayRADAlert(CallbackType.Postback, "Error2", message, "Save Error")
            Exit Sub
        End If


        ''Call Update Function in the plEmployerInfoFacade
        Result = StudentDocs.UpdateStudentDocs(BuildStudentDocsInfo(), AdvantageSession.UserState.UserName, txtStudentDocId.Text)

        ''  If DML is not successful then Prompt Error Message
        If Not Result = "" Then
            'DisplayErrorMessage(Result)
            Result = Result.Replace("'", " ")
            If Result.Contains("Cannot insert duplicate key row") Then
                Result = "Cannot insert the document since it already exists for the lead"
            End If
            DisplayRADAlert(CallbackType.Postback, "Error3", Result, "Save Error")        'Result
            Exit Sub
        End If


        ''Reset The Checked Property Of CheckBox To True
        ChkIsInDB.Checked = True

        ''Bind The DataList Based On StudentId and Education Institution Type.
        If ddlDocFilterStatus.SelectedIndex = -1 Then
            BindDataList(Leadid)
        Else
            GetDocumentsByStatus(ddlDocFilterStatus.SelectedValue, Leadid)
        End If
        'Reset The Filter Dropdown List
        'ddlDocumentModule.SelectedValue = ddlModulesId.SelectedValue
        'ddlDocFilterStatus.SelectedIndex = 0
        UploadFile()
        'CommonWebUtilities.SetStyleToSelectedItem(dlstDocumentStatus, txtStudentDocId.Text, ViewState, Header1)

        'If Page is free of errors Show Edit Buttons
        If Page.IsValid Then
            InitButtonsForEdit()
        End If
        RadGrid1.Visible = True
        'btnSave.Enabled = True
        'btnNew.Enabled = True
        'btnDelete.Enabled = True
        CommonWebUtilities.RestoreItemValues(dlstDocumentStatus, txtStudentDocId.Text)
    End Sub
    Private Function BuildStudentDocsInfo() As StudentDocsInfo

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim StudentDocsInfo As New StudentDocsInfo
        Dim intDocHasBeenApproved As Integer = 0
        intDocHasBeenApproved = (New LeadDocsFacade).CheckIfDocHasBeenApproved(ddlDocStatusId.SelectedValue)
        With StudentDocsInfo
            .IsInDb = ChkIsInDB.Checked
            .LeadId = Leadid
            .DocumentId = ddlDocumentId.SelectedValue
            .DocStatusId = ddlDocStatusId.SelectedValue
            '.ReceiveDate = txtReceiveDate.Text
            If txtReceiveDate.SelectedDate Is Nothing Then
                .ReceiveDate = ""
            Else
                .ReceiveDate = Date.Parse(txtReceiveDate.SelectedDate)
            End If
            .StudentDocsId = txtStudentDocId.Text
            .DocDescrip = ddlDocumentId.SelectedItem.Text
            'If Document status maps to approved status uncheck the override checkbox
            If intDocHasBeenApproved >= 1 Then
                chkOverride.Checked = False
                .Override = False
            Else
                .Override = chkOverride.Checked
            End If
            'If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            '    .TrackCode = ddlTrackCode.SelectedValue
            '    .DueDate = ddlDueDate.SelectedValue
            '    .TransDate = ddlTransDate.SelectedValue
            '    .CompletedDate = ddlCompletedDate.SelectedValue
            '    .NotificationCode = ddlNotificationCode.SelectedValue
            '    .DocDescrip = ddlDocumentId.SelectedItem.Text
            'End If

        End With
        Return StudentDocsInfo
    End Function
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            If ViewState("chkLeadWasEnrolled") = False Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Sub BindDataList(ByVal leadId As String)
        With New LeadDocsFacade
            dlstDocumentStatus.DataSource = .GetDocsLeadByLeadId(leadId, campusId)
            dlstDocumentStatus.DataBind()
        End With
    End Sub
    Private Sub dlstDocumentStatus_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstDocumentStatus.ItemCommand
        ChkIsInDB.Checked = True
        txtStudentDocId.Text = e.CommandArgument
        GetStudentInfo(e.CommandArgument)

        'set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstDocumentStatus, e.CommandArgument, ViewState, Header1)
        Master.Master.PageObjectId = e.CommandArgument
        Master.Master.PageResourceId = Request.QueryString("resid")
        Master.Master.setHiddenControlForAudit()
        Dim facade As New LeadDocsFacade
        strLeadName = facade.GetLeadNamesById(Leadid)
        Dim strFileNameOnly As String = Server.UrlEncode(strLeadName & "_" & Leadid).ToLower
        txtExtension.Text = facade.GetDocumentExtension(strFileNameOnly, ddlDocumentId.SelectedItem.Text)
        txtDocumentName.Text = ddlDocumentId.SelectedItem.Text
        txtLeadName.Text = strLeadName
        txtStudentId.Text = Leadid
        ddlDocumentId.Enabled = True
        'If ViewState("chkLeadWasEnrolled") = False Then
        '    btnSave.Enabled = True
        'End If
        'btnSave.Enabled = False
        'pnlViewDoc.Visible = False

        'Refresh The DataGrid With Document Types
        BuildFileDataGrid(strFileNameOnly)
        
        CommonWebUtilities.RestoreItemValues(dlstDocumentStatus, e.CommandArgument)
        InitButtonsForEdit()
        RadGrid1.Visible = True
    End Sub
    Private Sub GetStudentInfo(ByVal stStudentDocId As String)
        Dim studentinfo As New LeadDocsFacade
        BindStudentInfoExist(studentinfo.GetStudentDocs(stStudentDocId, txtLeadId.Text), stStudentDocId)
    End Sub
    Private Sub BindStudentInfoExist(ByVal studentDocsInfo As StudentDocsInfo, ByVal leadDocId As String)

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Bind The StudentInfo Data From The Database
        With studentDocsInfo
            'txtReceiveDate.Text = .ReceiveDate
            Try
                txtReceiveDate.SelectedDate = .ReceiveDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtReceiveDate.Clear()
            End Try
            Try
                BuildExistingDocumentListDDL(leadDocId)
                ddlDocumentId.SelectedValue = .DocumentId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlDocumentId.SelectedIndex = 0
            End Try

            Try
                BuildExistingDocumentStatusDDL(leadDocId)
                ddlDocStatusId.SelectedValue = .DocStatusId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlDocStatusId.SelectedIndex = 0
            End Try
            txtLeadId.Text = Leadid
            chkOverride.Checked = .Override

          
        End With
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        BindEmploymentInfo()
        txtStudentDocId.Text = Guid.NewGuid.ToString()
        InitButtonsForLoad()
        'dgrdDocs.Visible = False
        Dim dt As New DataTable
        dt.Columns.Add("DisplayName")

        RadGrid1.DataSource = dt
        RadGrid1.DataBind()
        RadGrid1.Visible = False
        'pnlViewDoc.Visible = False
        btnDelete.Enabled = False
        chkOverride.Checked = False
        BuildDocumentListDDL()
        BuildDocumentStatusDDL()
        'If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
        '    pnlRegent.Visible = True
        '    BuildSession()
        'End If
        CommonWebUtilities.RestoreItemValues(dlstDocumentStatus, Guid.Empty.ToString)
    End Sub
    Private Sub InitButtonsForLoad()
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False
        If ViewState("chkLeadWasEnrolled") = False Then
            If pObj.HasFull Or pObj.HasAdd Then
                btnSave.Enabled = True
                btnNew.Enabled = True
            End If
        End If
    End Sub
    Private Sub BindEmploymentInfo()
        txtReceiveDate.Clear()
        txtLeadId.Text = Leadid
        ddlDocumentId.SelectedIndex = 0
        ddlDocStatusId.SelectedIndex = 0
        ChkIsInDB.Checked = False
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If Not (Leadid = Guid.Empty.ToString) Then
            '    '   instantiate component
            Dim StudentDocs As New LeadDocsFacade

            '    'Delete The Row Based on LeadId
            Dim result As Integer = StudentDocs.DeleteLeadDocs(txtStudentDocId.Text, ddlDocumentId.SelectedValue, Leadid)

            '    'If Delete Operation was unsuccessful
            If result < 0 Then
                Customvalidator1.ErrorMessage = "There was a problem with Referential Integrity.<BR> You can not perform this operation."
                Customvalidator1.IsValid = False
            End If
            BindEmploymentInfo()
            ChkIsInDB.Checked = False
            GetDocumentsByStatus(ddlDocFilterStatus.SelectedValue, Leadid)
            'initialize buttons
            InitButtonsForLoad()
            'If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            '    pnlRegent.Visible = True
            '    BuildSession()
            'End If
        End If
        CommonWebUtilities.RestoreItemValues(dlstDocumentStatus, Guid.Empty.ToString)
    End Sub
    Private Sub btnApplyFilter_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnApplyFilter.Click
        BindEmploymentInfo()
        If ddlDocFilterStatus.SelectedIndex = 0 Then
            BindDataList(Leadid)
        Else
            GetDocumentsByStatus(ddlDocFilterStatus.SelectedValue, Leadid)
        End If
        InitButtonsForLoad()
        'pnlViewDoc.Visible = False
        Dim dt As New DataTable
        dt.Columns.Add("DisplayName")
        RadGrid1.DataSource = dt
        RadGrid1.DataBind()
        RadGrid1.Visible = False
        'dgrdDocs.Visible = False
    End Sub
    Private Sub GetDocumentsByStatus(ByVal documentStatusId As String, ByVal leadId As String)
        With New LeadDocsFacade
            dlstDocumentStatus.DataSource = .GetAllDocsStudentByStatus(ddlDocFilterStatus.SelectedValue, leadId, campusId)
            dlstDocumentStatus.DataBind()
        End With
    End Sub
    Private Function CreateDirectory(ByVal folderName As String) As String

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim strDirectoryPath = MyAdvAppSettings.AppSettings("CreateDocumentPath")
        Dim oDirectoryInfo As DirectoryInfo
        '        Dim oSubDirectoryInfo As DirectoryInfo
        Dim strDirectory As String
        If Directory.Exists(strDirectoryPath & folderName) = False Then
            'oDirectoryInfo = Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\Docs\" + FolderName)
            oDirectoryInfo = Directory.CreateDirectory(strDirectoryPath + folderName)
            strDirectory = strDirectoryPath & folderName
        Else
            strDirectory = strDirectoryPath & folderName
        End If
        Return strDirectory
    End Function
   

    Private Sub UploadFile()
    End Sub
    'Private Sub lnkViewDoc_Click(ByVal sender As Object, ByVal e As EventArgs)
    '    Session("DocumentName") = txtDocumentName.Text
    '    Session("StudentName") = txtLeadName.Text
    '    Session("LeadId") = Leadid
    '    Session("Path") = AppDomain.CurrentDomain.BaseDirectory
    '    Session("DocumentType") = ddlDocumentId.SelectedItem.Text
    '    Session("Extension") = txtExtension.Text
    '    txtDocumentType.Text = Session("DocumentType")
    'End Sub
    Private Sub BuildFileDataGrid(Optional ByVal fileName As String = "")
        'Dim File As New LeadDocsFacade
        'dgrdDocs.DataSource = File.GetAllDocumentsByStudentDocType(FileName, ddlDocumentId.SelectedItem.Text)
        'dgrdDocs.DataBind()

        Dim File As New LeadDocsFacade
        Dim dsnew As New DataSet
        If fileName <> "" Then
            dsnew = File.GetAllDocumentsByStudentDocType(fileName, ddlDocumentId.SelectedItem.Text)
            'dgrdDocs.DataSource = dsnew
            'dgrdDocs.DataBind()
        End If


        If ddlDocumentId.SelectedValue <> "" Then
            dsnew = File.GetAllDocumentsByLeadIdandDocumentID(Leadid, ddlDocumentId.SelectedValue)
        End If

        RadGrid1.DataSource = dsnew
        RadGrid1.DataBind()
    End Sub
    'Private Sub dgrdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdDocs.ItemCommand
    '    Dim FileName As String = e.CommandArgument
    '    Dim DocumentType As String = ddlDocumentId.SelectedItem.Text
    '    Dim result As String
    '    Dim docfacade As New DocumentManagementFacade
    '    Dim modulefacade As New LeadDocsFacade
    '    Dim intModuleId As Integer
    '    Dim intStudentCount As Integer

    '    Dim MyAdvAppSettings As AdvAppSettings
    '    If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
    '        MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
    '    Else
    '        MyAdvAppSettings = New AdvAppSettings
    '    End If

    '    If Not ddlDocumentId.SelectedValue = "" Then
    '        intModuleId = modulefacade.GetModuleId(ddlDocumentId.SelectedValue)
    '    Else
    '        intModuleId = 10
    '    End If

    '    Dim docMessage As String = String.Empty

    '    If e.CommandName = "DeleteFile" Then
    '        'Check If Lead Has Been Enrolled
    '        intStudentCount = modulefacade.GetEnrollmentCountByLeadId(Leadid)

    '        If intStudentCount = 0 Then
    '            Dim strPath As String = CType(MyAdvAppSettings.AppSettings("DocumentPath"), String)
    '            Dim strFileNameWithExtension As String = CType(e.Item.FindControl("Linkbutton1"), LinkButton).CommandArgument
    '            Dim strExtension As String = Mid(strFileNameWithExtension, InStrRev(strFileNameWithExtension, ".") + 1)
    '            Dim strFullPath As String = strPath + DocumentType + "\" + FileName + "." + strExtension
    '            File.Delete(strFullPath)
    '            'Delete The File,ReBind The DataList and Exit Procedure
    '            result = docfacade.DeleteDocument(FileName, intModuleId, ddlDocumentId.SelectedValue)
    '            If Not result = "" Then
    '                'DisplayErrorMessage(result)
    '                DisplayRADAlert(CallbackType.Postback, "Error9", result, 350, 100, "Doc Error")
    '            End If
    '            BuildFileDataGrid(FileName)
    '            chkScannedId.Checked = False
    '            Exit Sub
    '        Else
    '            docMessage &= "This lead has been enrolled in a program version so document cannot be deleted in this page " & vbLf
    '            docMessage &= "To delete this document,go to PLACEMENT === COMMON TASKS and  click View Existing Student " & vbLf
    '            docMessage &= "Select the student and go to Student Docs tab and then delete the document"
    '            'DisplayErrorMessage(docMessage)
    '            DisplayRADAlert(CallbackType.Postback, "Error10", docMessage, 350, 100, "Doc Error")
    '            Exit Sub
    '        End If
    '    End If

    '    Session("File2Browse") = MyAdvAppSettings.AppSettings("DocumentPath") & DocumentType.Replace("/", "\") & "\" & FileName

    '    Dim popupScript As String = "<script type='text/javascript'>window.Onload=OpenDocuments1();function OpenDocuments1(){window.open('../FileBrowser.aspx','HistWin','width=700,height=600,resizable=yes');}</script>"
    '    ClientScript.RegisterStartupScript(Me.GetType(), "PopUpScript", popupScript)

    'End Sub
    Private Sub ddlDocFilterStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlDocFilterStatus.SelectedIndexChanged
    End Sub
    Private Sub lnkPrgVersion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkPrgVersion.Click
    End Sub
    Private Sub btnViewDocsByStatus_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewDocsByStatus.Click
        Dim winSettings As String = "toolbar=no, status=no, resizable=yes,width=900px,height=470px"
        Dim name As String = "DocsByPrgVersion"
        Dim url As String = "DocumentByPrgVersion.aspx?LeadId=" + Leadid + "&prgverid=" + PrgVerId + "&LeadName=" + strLeadName + "&CampusId=" + campusId
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnApplyFilter)
        'controlsToIgnore.Add(btnUpload)
        controlsToIgnore.Add(btnViewDocsByStatus)
        'controlsToIgnore.Add(Img2)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    Private Sub BuildDocumentStatusDDL()
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlDocStatusId
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = DocumentStatus.GetAllDocumentStatus(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildDocumentFilterStatusDDL()
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlDocFilterStatus
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = DocumentStatus.GetAllDocumentStatus(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildExistingDocumentStatusDDL(ByVal leadDocId As String)
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlDocStatusId
            .DataTextField = "DocStatusDescrip"
            .DataValueField = "DocStatusId"
            .DataSource = DocumentStatus.GetAllLeadExistingDocumentStatus(leadDocId, campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub


    Protected Sub RadGrid1_DeleteCommand(sender As Object, e As GridCommandEventArgs)

        Dim DocumentType As String = ddlDocumentId.SelectedItem.Text
        Dim result As String
        Dim docfacade As New LeadDocsFacade

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim ID As String = TryCast(e.Item, GridDataItem).OwnerTableView.DataKeyValues(e.Item.ItemIndex)("FileID").ToString()

        Dim DeleteItem As GridDataItem = TryCast(e.Item, GridDataItem)

        Dim strPath As String = MyAdvAppSettings.AppSettings("DocumentPath")
        Dim imageName As String = TryCast(DeleteItem("DisplayName").FindControl("lblName"), Label).Text
        ' Dim strFileNameWithExtension As String = DeleteItem.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("FileName").ToString()
        '  Dim strExtension As String = Mid(strFileNameWithExtension, InStrRev(strFileNameWithExtension, ".") + 1)
        '  Dim StrExtension As String = TryCast(e.Item, GridDataItem).OwnerTableView.DataKeyValues(e.Item.ItemIndex)("DocumentCategory").ToString()
        'Dim strFullPath As String = strPath + DocumentType + "\" + imageName + ".gif" '' + strExtension

        Dim extnposition As Integer

        extnposition = imageName.LastIndexOf(".")

        Dim extn As String
        extn = imageName.Substring(extnposition, imageName.Length - extnposition)


        Dim strFullPath As String = strPath + DocumentType + "\" + ID + extn



        If File.Exists(strFullPath) Then
            File.Delete(strFullPath)
        End If

        'Delete The File,ReBind The DataList and Exit Procedure
        result = docfacade.DeleteDocumentbyID(ID)
        If Not result = "" Then
            'DisplayErrorMessage(result)
            DisplayRADAlert(CallbackType.Postback, "Error11", result, "Doc Error")
        End If
        BuildFileDataGrid()
        chkScannedId.Checked = False
        Exit Sub

    End Sub
    Private Function ValidatePage() As String
        Dim strDocumentInsert As New DocumentManagementFacade
        Dim sMessage As String = ""

        If ddlDocumentId.SelectedValue = "" Then
            sMessage &= "Document is required" & vbLf
        End If

        If ddlDocStatusId.SelectedValue = "" Then
            sMessage &= "Document Status is required" & vbLf
        End If

        If Not sMessage = "" Then
            '   DisplayErrorMessage(sMessage)
            Return sMessage
            '  Exit Function
        End If

        ''Check if the Lead record is saved
        ''If the record is not saved then force the user to save the document info before uploading the document.
        Dim isLeadDocumentRecordCreated As Boolean
        isLeadDocumentRecordCreated = strDocumentInsert.IsLeadDocumentAlreadyCreated(Leadid, Trim(ddlDocumentId.SelectedValue))
        If Not isLeadDocumentRecordCreated Then
            Return (" Please save the information to upload the document ")
            Exit Function
        End If
        Return ""
    End Function
    Protected Sub RadGrid1_InsertCommand(sender As Object, e As GridCommandEventArgs)

        Dim strDirectory As String
        Dim smessage As String
        Dim result As String
        smessage = ValidatePage()
        If Not smessage = "" Then
            'DisplayErrorMessage(smessage)
            DisplayRADAlert(CallbackType.Postback, "Error12", smessage, "Doc Error")
            e.Canceled = True
            RadGrid1.MasterTableView.IsItemInserted = False
            RadGrid1.MasterTableView.ClearSelectedItems()
            RadGrid1.Rebind()
            Exit Sub
        End If


        Dim insertItem As GridEditFormInsertItem = TryCast(e.Item, GridEditFormInsertItem)
        '    Dim imageName As String = TryCast(insertItem("DisplayName").FindControl("txbName"), RadTextBox).Text
        Dim radAsyncUpload As RadAsyncUpload = TryCast(insertItem("Upload").FindControl("AsyncUpload1"), RadAsyncUpload)
        '' Dim file As UploadedFile = radAsyncUpload.UploadedFiles(0)
        If radAsyncUpload.UploadedFiles.Count > 0 Then
            For Each File As UploadedFile In radAsyncUpload.UploadedFiles
                'Get Directory
                Dim strDocumentType As String = ddlDocumentId.SelectedItem.Text
                strDirectory = CreateDirectory(strDocumentType)

                'Get FileName from the client machine
                Dim strExt As String = File.FileName.Substring(File.FileName.LastIndexOf(".")).ToLower()

                'Get Friendly Name for storing
                Dim slashposition As Integer = File.FileName.LastIndexOf("\")
                Dim strDisplayName As String = File.FileName.Substring(slashposition + 1)
                strDisplayName = strDisplayName.ToString.Split(".")(0)

                Dim intBackSlashPos As Integer = InStrRev(File.FileName, "\")
                If intBackSlashPos < 1 Then
                    intBackSlashPos = InStrRev(File.FileName, "/")
                End If
                Dim strOrigFileName As String = Mid(File.FileName, intBackSlashPos + 1)
                Dim strFindExtPos As Integer = InStr(strOrigFileName, ".")
                Dim strUserFileName As String = Mid(strOrigFileName, 1, strFindExtPos - 1)

                'Get Size of file
                Dim filesize As Integer = File.ContentLength

                'Get type of file
                '  Dim facade As New DocumentManagementFacade
                Dim facade As New LeadDocsFacade
                '  Dim strStudentId As String = Trim(ddlStudentId.SelectedValue)
                Dim strLeadName As String = facade.GetLeadNamesById(Leadid)
                '  Dim strStudentName As String = facade.GetStudentNamesById(strStudentId)
                Dim filetype As String = File.ContentType
                Dim strFileNameOnly As String = Server.UrlEncode(strLeadName & "_" & Leadid & "_" & strUserFileName).ToLower()
                Dim strFileName As String = strLeadName & "_" & Leadid & "_" & strUserFileName & strExt


                If filesize <= 0 Then
                    'DisplayErrorMessage("Document upload process failed")
                    DisplayRADAlert(CallbackType.Postback, "Error13", "Document upload process failed", "Doc Error")
                Else

                    result = facade.InsertDocumentbyID(strFileNameOnly, strExt, Session("UserName"), ddlDocumentId.SelectedItem.Text, Leadid, ddlDocumentId.SelectedValue, strDisplayName)

                    If result <> "" Then
                        File.SaveAs(strDirectory + "\" + result + strExt)
                        'DisplayErrorMessage("Document uploaded successfully")
                        DisplayRADAlert(CallbackType.Postback, "Error14", "Document uploaded successfully", "Doc Error")
                    Else
                        'DisplayErrorMessage("Data not saved")
                        DisplayRADAlert(CallbackType.Postback, "Error15", "Data not saved", "Doc Error")
                    End If
                End If
            Next
        End If
        e.Canceled = True
        RadGrid1.MasterTableView.IsItemInserted = False
        RadGrid1.MasterTableView.ClearSelectedItems()
        RadGrid1.Rebind()



    End Sub

    Protected Sub RadGrid1_ItemCommand(ByVal source As Object, ByVal e As GridCommandEventArgs)
        If e.CommandName = RadGrid.EditCommandName Then
            ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "SetEditMode", "isEditMode = true;", True)
        End If
        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked
            Dim strDocumentInsert As New DocumentManagementFacade
            Dim isLeadDocumentRecordCreated As Boolean
            isLeadDocumentRecordCreated = strDocumentInsert.IsLeadDocumentAlreadyCreated(Leadid, Trim(ddlDocumentId.SelectedValue))
            If Not isLeadDocumentRecordCreated Then
                e.Canceled = True
                RadGrid1.MasterTableView.IsItemInserted = False
                RadGrid1.MasterTableView.ClearSelectedItems()
                RadGrid1.Rebind()
            Else
                e.Canceled = True
                Dim newValues As ListDictionary = New ListDictionary()
                newValues("DisplayName") = ""
                e.Item.OwnerTableView.InsertItem(newValues)
            End If
        End If
        If e.CommandName = "StudentSearch" Then
            Dim FileName As String = e.CommandArgument
            Session("File2Browse") = e.CommandArgument


            If (TypeOf e.Item Is GridDataItem) Then
                Dim gridItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                'Dim img As New LinkButton
                Dim img As LinkButton = DirectCast(gridItem("Upload").FindControl("Linkbutton1"), LinkButton)
                ' Dim img As LinkButton = TryCast(e.Item.FindControl("Linkbutton1"), LinkButton)
                img.Attributes.Add("target", "_blank")
                Dim popupScript As String = "<script type='text/javascript'>window.Onload=OpenDocuments1();function OpenDocuments1(){window.open('../FileBrowser.aspx','_blank','HistWin','width=700,height=600,resizable=yes'); }</script>"
                ScriptManager.RegisterStartupScript(RadGrid1, RadGrid1.GetType(), "PopUpScript", popupScript, True)
            End If


        End If

    End Sub



    Protected Sub RadGrid1_ItemCreated(sender As Object, e As GridItemEventArgs)
        If TypeOf e.Item Is GridEditableItem AndAlso e.Item.IsInEditMode Then
            Dim upload As RadAsyncUpload = TryCast(DirectCast(e.Item, GridEditableItem)("Upload").FindControl("AsyncUpload1"), RadAsyncUpload)
            Dim cell As TableCell = DirectCast(upload.Parent, TableCell)

            Dim validator As New CustomValidator()
            validator.ErrorMessage = "Please select file to be uploaded"
            validator.ClientValidationFunction = "validateRadUpload"
            validator.Display = ValidatorDisplay.Dynamic
            cell.Controls.Add(validator)
        End If
    End Sub

    Protected Sub RadGrid1_NeedDataSource(sender As Object, e As GridNeedDataSourceEventArgs)
        If ddlDocumentId.SelectedValue <> "" Then
            Dim File As New LeadDocsFacade
            Dim dsnew As New DataSet

            dsnew = File.GetAllDocumentsByLeadIdandDocumentID(Leadid, ddlDocumentId.SelectedValue)

            RadGrid1.DataSource = dsnew
        End If
    End Sub

    Protected Sub RadGrid1_UpdateCommand(sender As Object, e As GridCommandEventArgs)
        Dim strDirectory As String
        '   Dim strDocumentInsert As New DocumentManagementFacade
        Dim smessage As String
        Dim result As Integer
        smessage = ValidatePage()
        If Not smessage = "" Then
            'DisplayErrorMessage(smessage)
            DisplayRADAlert(CallbackType.Postback, "Error16", smessage, "Doc Error")
            Exit Sub
        End If

        Dim editedItem As GridEditableItem = TryCast(e.Item, GridEditableItem)
        Dim ID As String = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("FileID").ToString()
        '   Dim imageName As String = TryCast(editedItem("DisplayName").FindControl("txbName"), RadTextBox).Text
        '   Dim description As String = TryCast(editedItem("Description").FindControl("txbDescription"), RadTextBox).Text
        Dim radAsyncUpload As RadAsyncUpload = TryCast(editedItem("Upload").FindControl("AsyncUpload1"), RadAsyncUpload)


        '   If Not txtUpLoad.PostedFile Is Nothing And Not txtUpLoad.Value = "" Then
        If radAsyncUpload.UploadedFiles.Count = 1 Then
            'Get Directory
            Dim strDocumentType As String = ddlDocumentId.SelectedItem.Text
            strDirectory = CreateDirectory(strDocumentType)

            'Get FileName from the client machine
            Dim strExt As String = radAsyncUpload.UploadedFiles(0).FileName.Substring(radAsyncUpload.UploadedFiles(0).FileName.LastIndexOf(".")).ToLower()

            'Get Friendly Name for storing
            Dim slashposition As Integer = radAsyncUpload.UploadedFiles(0).FileName.LastIndexOf("\")
            Dim strDisplayName As String = radAsyncUpload.UploadedFiles(0).FileName.Substring(slashposition + 1)
            strDisplayName = strDisplayName.ToString.Split(".")(0)

            Dim intBackSlashPos As Integer = InStrRev(radAsyncUpload.UploadedFiles(0).FileName, "\")
            If intBackSlashPos < 1 Then
                intBackSlashPos = InStrRev(radAsyncUpload.UploadedFiles(0).FileName, "/")
            End If
            Dim strOrigFileName As String = Mid(radAsyncUpload.UploadedFiles(0).FileName, intBackSlashPos + 1)
            Dim strFindExtPos As Integer = InStr(strOrigFileName, ".")
            Dim strUserFileName As String = Mid(strOrigFileName, 1, strFindExtPos - 1)

            'Get Size of file
            Dim filesize As Integer = radAsyncUpload.UploadedFiles(0).ContentLength

            'Get type of file
            ' Dim facade As New DocumentManagementFacade
            Dim facade As New LeadDocsFacade
            '  Dim strStudentId As String = Trim(ddlStudentId.SelectedValue)
            '  Dim strStudentName As String = facade.GetLeadNamesById(Leadid)
            Dim strLeadName As String = facade.GetLeadNamesById(Leadid)
            Dim filetype As String = radAsyncUpload.UploadedFiles(0).ContentType
            Dim strFileNameOnly As String = Server.UrlEncode(strLeadName & "_" & Leadid & "_" & strUserFileName).ToLower()
            Dim strFileName As String = strLeadName & "_" & Leadid & "_" & strUserFileName & strExt

            If filesize <= 0 Then
                'DisplayErrorMessage("Document upload process failed")
                DisplayRADAlert(CallbackType.Postback, "Error17", "Document upload process failed", "Doc Error")
            Else
                ' radAsyncUpload.UploadedFiles(0).SaveAs(strDirectory + "\" + strFileNameOnly + strExt)
                result = facade.UpdateDocument(ID, strFileNameOnly, strExt, Session("UserName"), ddlDocumentId.SelectedItem.Text, Leadid, ddlDocumentId.SelectedValue, strDisplayName)
                radAsyncUpload.UploadedFiles(0).SaveAs(strDirectory + "\" + ID + strExt)
                If result = 0 Then
                    'DisplayErrorMessage("Document uploaded successfully")
                    DisplayRADAlert(CallbackType.Postback, "Error18", "Document uploaded successfully", "Doc Error")
                End If
            End If
        ElseIf radAsyncUpload.UploadedFiles.Count > 1 Then
            'DisplayErrorMessage("Only one document can be uploaded")
            DisplayRADAlert(CallbackType.Postback, "Error19", "Only one document can be uploaded", "Doc Error")
        End If
        'RadGrid1.DataBind()
        e.Canceled = True
        RadGrid1.MasterTableView.IsItemInserted = False
        RadGrid1.MasterTableView.ClearEditItems()
        RadGrid1.Rebind()
    End Sub



    Protected Sub RadGrid1_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles RadGrid1.ItemDataBound


        ''Dim img As LinkButton = TryCast(e.Item.FindControl("Linkbutton1"), LinkButton)
        ''img.Attributes.Add("target", "_blank")
        ''Dim popupScript As String = "<script type='text/javascript'>window.Onload=OpenDocuments1();function OpenDocuments1(){window.open('../FileBrowser.aspx','_blank','HistWin','width=700,height=600,resizable=yes'); }</script>"
        ''ScriptManager.RegisterStartupScript(RadGrid1, RadGrid1.GetType(), "PopUpScript", popupScript, True)
        ''  If TypeOf e.Item Is GridItem Then


        Dim bEdit As Boolean
        Dim bAdd As Boolean
        Dim bDelete As Boolean
        Dim bEditLead As Boolean

        Dim myAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        bEdit = False
        bAdd = False
        bDelete = False
        bEditLead = False

        If ViewState("chkLeadWasEnrolled") = False Then
            Dim leadMasterUpdate As New LeadFacade
            If myAdvAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "no" Then
                If leadMasterUpdate.CheckLeadUser(userId, Leadid) = 0 Then
                    bEditLead = False
                Else
                    bEditLead = True
                End If
            Else
                bEditLead = True
            End If
        End If

        If bEditLead Then
            If pObj.HasEdit Then
                bEdit = True
                bAdd = True
            End If
            If pObj.HasAdd Then
                bAdd = True
            End If
            If pObj.HasDelete Then
                bDelete = True
            End If
        End If

        If bAdd Then
            RadGrid1.AllowAutomaticInserts = True
        ElseIf Not bAdd Then
            If TypeOf e.Item Is GridCommandItem Then
                DisableGridAdd(e)
            End If
        End If
        If bEdit Then
            RadGrid1.AllowAutomaticUpdates = True
        ElseIf bEdit = False And bAdd = False Then
            If TypeOf e.Item Is GridDataItem Then
                DisableGridLink(e, "EditCommandColumn", "edit")
            End If
        ElseIf bEdit = False And bAdd = True Then
            If TypeOf e.Item Is GridDataItem Then 'And e.Item.OwnerTableView.IsItemInserted = False Then
                DisableGridLink(e, "EditCommandColumn", "edit")
            End If
        End If
        If Not bDelete Then
            If TypeOf e.Item Is GridDataItem Then
                DisableGridLink(e, "DeleteColumn", "delete")
            End If
        End If
        If (TypeOf e.Item Is GridDataItem) Then
            Dim gridItem As GridDataItem = DirectCast(e.Item, GridDataItem)
            'Dim img As New LinkButton
            Dim img As LinkButton = DirectCast(gridItem("Upload").FindControl("Linkbutton1"), LinkButton)
            'img = TryCast(e.Item.FindControl("Linkbutton1"), LinkButton)
            If Not img Is Nothing Then
                Dim javascripttoopenfile As String
                '    '  Dim parsequerystring As String

                '    'parsequerystring = "  function ParseQueryString(queryString) {    if(queryString == 'undefined' || queryString == '') {        return false;    } else {      if(queryString.substr(0, 1) == '?') { queryString = queryString.substr(1); }        var components = queryString.split('&');       var finalObject = new Object();        var parts;       for (var i = 0; i < components.length; i++) {      parts = components[i].split('=');            finalObject[parts[0]] = decodeURI(parts[1]);        }        return finalObject;    }} "
                '    'javascripttoopenfile = "var pqs = new ParseQueryString();var pn = pqs.param('" + Server.UrlEncode(img.CommandArgument.ToString()) + "');window.open('../FileBrowser.aspx?fileurl= pn','HistWin','width=700,height=600,resizable=yes');"



                '    '    parsequerystring = "  function ParseQueryString(queryString) {    if(queryString == 'undefined' || queryString == '') {        return false;    } else {      if(queryString.substr(0, 1) == '?') { queryString = queryString.substr(1); }        var components = queryString.split('&');       var finalObject = new Object();        var parts;       for (var i = 0; i < components.length; i++) {      parts = components[i].split('=');            finalObject[parts[0]] = decodeURI(parts[1]);        }        return finalObject;    }} "
                javascripttoopenfile = "window.open('../FileBrowser.aspx?fileurl=" + Server.UrlEncode(img.CommandArgument.ToString()) + "','HistWin','width=700,height=600,resizable=yes');"
                '    '' "window.Onload=OpenDocuments1();function OpenDocuments1(){window.open('../FileBrowser.aspx?fileurl='" + Server.UrlEncode(img.CommandArgument.ToString()) + "','_blank','HistWin','width=700,height=600,resizable=yes')} "

                img.Attributes.Add("onclick", javascripttoopenfile)
            End If

        End If
    End Sub

    Private Sub DisableGridLink(ByVal e As GridItemEventArgs,
                          ByVal sCmdName As String,
                          ByVal sDesc As String)

        Dim sToolTip As String = "User does not have the permission to " & sDesc & " record"
        Dim cell As TableCell = CType(e.Item, GridDataItem)(sCmdName)
        Dim lnk As ImageButton = CType(cell.Controls(0), ImageButton)

        'lnk.Enabled = False
        'lnk.ToolTip = sToolTip
        lnk.Visible = False
        cell.ToolTip = sToolTip


    End Sub

    Private Sub DisableGridAdd(ByVal e As GridItemEventArgs)

        Dim cmditm As GridCommandItem = DirectCast(e.Item, GridCommandItem)
        Dim btn1 As Button = DirectCast(cmditm.FindControl("btn1"), Button)

        Try
            'btn1.Enabled = False
            btn1.Visible = False
            btn1.ToolTip = "User does not have permission to add new record"
            Dim lnkbtn1 As LinkButton = DirectCast(cmditm.FindControl("linkbuttionInitInsert"), LinkButton)
            'lnkbtn1.Enabled = False
            lnkbtn1.ToolTip = "User does not have permission to add new record"
            lnkbtn1.Visible = False
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

    End Sub
End Class
