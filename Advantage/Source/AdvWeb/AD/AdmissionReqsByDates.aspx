<%@ Page Title="Requirements" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="AdmissionReqsByDates.aspx.vb" Inherits="AdmissionReqsByDates" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Reference Page="~/CM/DocumentStatus.aspx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <script language="javascript" src="../AuditHist.js" type="text/javascript"></script>

    <script language="javascript" src="../js/checkall.js" type="text/javascript"></script>
    <script language="javascript" src="../js/NumberValidations.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.onload = function () {
            var strCook = document.cookie;
            if (strCook.indexOf("!~") != 0) {
                var intS = strCook.indexOf("!~");
                var intE = strCook.indexOf("~!");
                var strPos = strCook.substring(intS + 2, intE);
                document.getElementById("grdWithScroll").scrollTop = strPos;
            }
        }
        function SetDivPosition() {
            var intY = document.getElementById("grdWithScroll").scrollTop;
            document.cookie = "yPos=!~" + intY + "~!";
        }
    </script>
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">


            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td nowrap align="left" width="15%">
                                    <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                <td nowrap width="85%">
                                    <asp:RadioButtonList ID="radStatus" runat="Server" RepeatDirection="Horizontal" AutoPostBack="true"
                                        CssClass="label">
                                        <asp:ListItem Text="Active" Selected="True" />
                                        <asp:ListItem Text="Inactive" />
                                        <asp:ListItem Text="All" />
                                    </asp:RadioButtonList></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframetop">

                        <table id="AutoNumber6" cellspacing="0" cellpadding="2" width="100%">
                            <tr id="trMOdule" runat="server">
                                <td class="employersearch">
                                    <asp:Label ID="lblModule" runat="server" CssClass="label">Module</asp:Label></td>
                                <td class="employersearch2">
                                    <asp:DropDownList ID="ddlmodule" runat="server" CssClass="dropdownlist" Width="80%" AutoPostBack="False"></asp:DropDownList></td>
                            </tr>
                            <tr id="trReqType" runat="server">
                                <td class="employersearch">
                                    <asp:Label ID="Label4" runat="server" CssClass="label">Required For</asp:Label></td>
                                <td class="employersearch2">
                                    <asp:DropDownList ID="dllreqtype" runat="server" AutoPostBack="false" Width="80%" CssClass="dropdownlist">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">Required for Enrollment</asp:ListItem>
                                        <asp:ListItem Value="2">Required for Financial Aid Disbursements</asp:ListItem>
                                        <asp:ListItem Value="3">Required for Graduation</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>

                            <tr>
                                <td class="employersearch"></td>
                                <td class="employersearch2" style="text-align: center">
                                    <asp:Button ID="btnBuildList" runat="server" Text="Build List" CausesValidation="False"></asp:Button></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div id="grdWithScroll" class="scrollleftfilters" style="height: expression(document.body.clientHeight -300 + 'px');" onscroll="SetDivPosition()">
                            <asp:DataList ID="dlstAdmissionReqGroups" runat="server" DataKeyField="adReqId" Width="100%">
                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
                                        CausesValidation="False" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>'
                                        CommandArgument='<%# Container.DataItem("adReqId")%>'></asp:ImageButton>
                                    <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" CausesValidation="False"
                                        Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' CommandArgument='<%# Container.DataItem("adReqId")%>'></asp:ImageButton>
                                    <asp:LinkButton ID="Linkbutton1" runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("adReqId")%>'
                                        Text='<%# Container.DataItem("Descrip") & "" & CheckIfMandatory(Ctype(Container.DataItem("MandatoryRequirement"), Boolean)) %> '
                                        CausesValidation="False" />
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>



        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">


            <asp:Panel ID="pnlRHS" runat="server">
                <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                            <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                            <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button></td>

                    </tr>
                </table>
                <!-- end top menu (save,new,reset,delete,history)-->
                <!--begin right column-->
                <table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="100%" style="padding-top: 10px"
                    border="0">
                    <tr>
                        <td class="detailsframe">
                            <div class="boxContainer">
                                <h3><%=Header.Title  %></h3>
                                <!-- begin table content-->
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                    <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                    <asp:TextBox ID="txtModUser" runat="server" Visible="False"></asp:TextBox>
                                    <asp:TextBox ID="txtModDate" runat="server" Visible="False"></asp:TextBox>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblCode" runat="server" CssClass="label"></asp:Label></td>
                                        <td class="twocolumncontentcell">
                                            <asp:TextBox ID="txtCode" TabIndex="1" runat="server" Width="59%" CssClass="textbox"></asp:TextBox></td>
                                        <td class="twocolumncontentcell">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label></td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlStatusId" TabIndex="2" Width="60%" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList></td>
                                        <td class="twocolumncontentcell" style="padding-bottom: 20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblDescrip" runat="server" CssClass="label"></asp:Label></td>
                                        <td class="twocolumncontentcell">
                                            <asp:TextBox ID="txtDescrip" TabIndex="3" runat="server" Width="59%" CssClass="textbox" MaxLength="50"></asp:TextBox></td>
                                        <td class="twocolumncontentcell">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lbladReqTypeId" runat="server" CssClass="label"></asp:Label></td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddladReqTypeId" TabIndex="4" Width="60%" runat="server" CssClass="dropdownlist"
                                                AutoPostBack="True">
                                            </asp:DropDownList></td>
                                        <td class="twocolumncontentcell" style="padding-bottom: 20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblModuleId" runat="server" CssClass="label"></asp:Label></td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlModuleId" TabIndex="5" Width="60%" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList></td>
                                        <td class="twocolumncontentcell">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label></td>
                                        <td class="twocolumncontentcell">
                                            <asp:DropDownList ID="ddlCampGrpId" TabIndex="6" Width="60%" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>

                                        </td>
                                        <td class="twocolumncontentcell" style="padding-bottom: 20px">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">&nbsp;</td>
                                        <td>
                                            <asp:RadioButtonList ID="rbtnrequirement" runat="server"
                                                CssClass="radiobuttonlist">
                                                <asp:ListItem Value="1">Required for Enrollment</asp:ListItem>
                                                <asp:ListItem Value="2">Required for Financial Aid Disbursements</asp:ListItem>
                                                <asp:ListItem Value="3">Required for Graduation</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>

                                        <td class="twocolumnlabelcell">&nbsp;</td>
                                        <td>&nbsp;</td>

                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">&nbsp;</td>
                                        <td class="twocolumnlabelcell">
                                            <asp:CheckBox ID="chkAppliesToAll" runat="server" AutoPostBack="True"
                                                CssClass="checkbox" TabIndex="7" Text="Applies To All" Visible="False" />
                                        </td>

                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell">
                                            <asp:TextBox ID="txtadReqId" runat="server" CssClass="textbox" Visible="false"></asp:TextBox></td>
                                        <td class="twocolumncontentcell"></td>
                                        <td class="twocolumncontentcell">&nbsp;</td>
                                    </tr>
                                </table>
                                <table width="100%" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="contentcellheader">
                                            <asp:Label ID="lblHeading" CssClass="labelbold" runat="server">Select Mandatory to make an item a school level requirement</asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 90%; text-align: center; padding-top: 20px">
                                            <asp:DataGrid ID="dgrdLeadGroups" TabIndex="6" runat="server" Visible="True" Width="95%"
                                                BorderWidth="1px" ShowFooter="True" BorderStyle="Solid" AutoGenerateColumns="False"
                                                AllowSorting="True" BorderColor="#E0E0E0">
                                                <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                <ItemStyle CssClass="datagriditemstyle" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="datagridheaderstyle"></HeaderStyle>
                                                <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                                <EditItemStyle CssClass="datagriditemstyle"></EditItemStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Start Date">
                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="18%"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStartDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>'> </asp:Label>
                                                            <asp:Label ID="lbladReqId" runat="server" Text='<%# Container.DataItem("adReqId") %>'
                                                                Visible="False"> </asp:Label>
                                                            <asp:Label ID="lbladReqEffectiveDateId" runat="server" Text='<%# Container.DataItem("adReqEffectiveDateId") %>'
                                                                Visible="False"> </asp:Label>
                                                        </ItemTemplate>
                                                        <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtStartDate" CssClass="TextBox" runat="server"></asp:TextBox>
                                                            <asp:CompareValidator ID="cvStartDate" runat="server" ControlToValidate="txtStartDate"
                                                                Display="None" ErrorMessage="Incorrect Start Date" Operator="DataTypeCheck" Type="Date" />
                                                        </FooterTemplate>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtEditStartDate" CssClass="TextBox" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"StartDate", "{0:d}") %>'> </asp:TextBox>
                                                            <asp:CompareValidator ID="cvEditStartDate" runat="server" ControlToValidate="txtEditStartDate"
                                                                Display="None" ErrorMessage="Incorrect Start Date" Operator="DataTypeCheck" Type="Date" />
                                                            <asp:TextBox ID="txtEditadReqId" runat="server" Text='<%# Container.DataItem("adReqId") %>'
                                                                Visible="False" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="End Date">
                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="18%"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEndDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container.DataItem,"EndDate", "{0:d}") %>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <FooterStyle HorizontalAlign="Center" CssClass="datagriditemstyle"></FooterStyle>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtEndDate" CssClass="TextBox" runat="server"></asp:TextBox>
                                                            <asp:CompareValidator ID="cvEndDate" runat="server" ControlToValidate="txtEndDate"
                                                                Display="None" ErrorMessage="Incorrect End Date" Operator="DataTypeCheck" Type="Date" />
                                                        </FooterTemplate>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtEditEndDate" CssClass="TextBox" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"EndDate", "{0:d}") %>'> </asp:TextBox>
                                                            <asp:CompareValidator ID="cvEditEndDate" runat="server" ControlToValidate="txtEditEndDate"
                                                                Display="None" ErrorMessage="Incorrect End Date" Operator="DataTypeCheck" Type="Date" />
                                                            <asp:Label ID="lblEditadReqEffectiveDateId" runat="server" Text='<%# Container.DataItem("adReqEffectiveDateId") %>'
                                                                Visible="False"> </asp:Label>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Min Score">
                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMinScore" runat="server" CssClass="label" Text='<%# Container.DataItem("MinScore") %>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <FooterStyle HorizontalAlign="Center" CssClass="datagriditemstyle"></FooterStyle>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtMinScore" CssClass="TextBox" runat="server"></asp:TextBox>
                                                        </FooterTemplate>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtEditMinScore" CssClass="TextBox" runat="server" Text='<%# Container.DataItem("MinScore") %>'> </asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Valid For (days)">
                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblValidFor" runat="server" CssClass="label" Text='<%# Container.DataItem("ValidDays") %>'> </asp:Label>
                                                        </ItemTemplate>
                                                        <FooterStyle HorizontalAlign="Center" CssClass="datagriditemstyle"></FooterStyle>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtValidFor" CssClass="TextBox" runat="server"></asp:TextBox>
                                                            <asp:CompareValidator ID="cvFooterValidFor" runat="server" Operator="DataTypeCheck" Type="Integer" ControlToValidate="txtValidFor" Display="none" ErrorMessage="Invalid Valid For (days)"></asp:CompareValidator>
                                                        </FooterTemplate>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtEditValidFor" CssClass="TextBox" runat="server" Text='<%# Container.DataItem("ValidDays") %>'> </asp:TextBox>
                                                            <asp:CompareValidator ID="cvEditValidFor" runat="server" Operator="DataTypeCheck" Type="integer" ControlToValidate="txtEditValidFor" Display="None" ErrorMessage="Invalid Valid For (days)"></asp:CompareValidator>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Mandatory">
                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle" HorizontalAlign="Center"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkMandatoryRequirement" TabIndex="5" runat="server" CssClass="checkbox"
                                                                Enabled="False" Checked='<%# TrueOrFalse(Container.DataItem("MandatoryRequirement")) %>' />
                                                        </ItemTemplate>
                                                        <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                                        <FooterTemplate>
                                                            <asp:CheckBox ID="chkFooterMandatoryRequirement" TabIndex="5" runat="server" CssClass="checkbox"
                                                                AutoPostBack="false" />
                                                        </FooterTemplate>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <EditItemTemplate>
                                                            <asp:CheckBox ID="chkEditMandatoryRequirement" TabIndex="5" runat="server" CssClass="checkbox"
                                                                Checked='<%# TrueOrFalse(Container.DataItem("MandatoryRequirement")) %>' AutoPostBack="false" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn>
                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="15%"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnlButEdit" Text="<img border=0 src=../images//im_edit.gif alt= edit>"
                                                                CausesValidation="False" runat="server" CommandName="Edit"> <img border="0" src="../images/im_edit.gif" alt="edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <FooterStyle CssClass="DataGridItemStyle"></FooterStyle>
                                                        <FooterTemplate>
                                                            <asp:Button ID="btnAddRow" Text="Add" runat="server" CommandName="AddNewRow"></asp:Button>
                                                        </FooterTemplate>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkbutUpdate" Text="<img border=0 src=../images/im_update.gif alt=update>"
                                                                runat="server" CommandName="Update" CausesValidation="true"> <img border="0" src="../images/im_update.gif" alt="update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkbutDelete" Text="<img border=0 src=../images/delete.gif alt=delete>"
                                                                runat="server" CommandName="Delete" CausesValidation="False"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkbutCancel" Text="<img border=0 src=../images//im_delete.gif alt=Cancel>"
                                                                CausesValidation="False" runat="server" CommandName="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn>
                                                        <HeaderStyle CssClass="datagridheaderstyle" Width="19%"></HeaderStyle>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkLeadGroups" runat="server" CommandName="SetupLead" CausesValidation="False">Setup Groups</asp:LinkButton>
                                                        </ItemTemplate>
                                                        <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                                        <FooterTemplate>
                                                            <asp:Label runat="server" Text="Setup Groups" Enabled="False" CssClass="label"
                                                                ID="Hyperlink2"></asp:Label>
                                                        </FooterTemplate>
                                                        <ItemStyle CssClass="datagriditemstyle" VerticalAlign="middle"></ItemStyle>
                                                        <EditItemTemplate>
                                                            <asp:Label runat="server" Text="Setup Groups" Enabled="False" CssClass="label"
                                                                ID="Label2"></asp:Label>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid></td>
                                    </tr>
                                </table>
                                <table width="100%" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td nowrap width="100%">
                                            <asp:DataGrid ID="dgrdTransactionSearch" runat="server" Width="100%" BorderWidth="1px"
                                                ShowFooter="False" BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True"
                                                BorderColor="#E0E0E0">
                                                <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                                <ItemStyle CssClass="DataGridItemStyle" HorizontalAlign="Center"></ItemStyle>
                                                <HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeaderStyle"></HeaderStyle>
                                                <FooterStyle CssClass="datagridpadding"></FooterStyle>
                                                <EditItemStyle CssClass="datagridpadding"></EditItemStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="All">
                                                        <HeaderStyle CssClass="DataGridHeaderStyle" Width="10%"></HeaderStyle>
                                                        <HeaderTemplate>
                                                            <input id="chkAllItems" type="checkbox" value="check all" onclick="CheckAllDataGridCheckBoxes('chkUnschedule',
        document.forms[0].chkAllItems.checked)" />
                                                        </HeaderTemplate>
                                                        <ItemStyle CssClass="DataGridItemStyle" VerticalAlign="middle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkUnschedule" runat="server" Text='' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="LeadGroup">
                                                        <HeaderStyle CssClass="DataGridHeaderStyle" Width="80%"></HeaderStyle>
                                                        <ItemStyle CssClass="DataGridItemStyle" VerticalAlign="middle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" Text='<%# Container.DataItem("Descrip") %>' CssClass="Label"
                                                                runat="server"> </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Required">
                                                        <HeaderStyle CssClass="DataGridHeaderStyle" Width="10%"></HeaderStyle>
                                                        <ItemStyle CssClass="DataGridItemStyle" VerticalAlign="middle"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkRequired" Checked='<%# Container.DataItem("Required") %>' CssClass="CheckBoxStyle"
                                                                runat="server"></asp:CheckBox>
                                                            <asp:TextBox ID="txtLeadGrpId" runat="server" Visible="False" Text='<%# Container.DataItem("LeadGrpId") %>'> </asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid></td>
                                    </tr>
                                </table>
                                <!--<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="80%" align="center">
															<TR>
																<TD class="DataGridHeader3" width="10%">Check All</TD>
																<TD class="DataGridHeader3" width="60%">Lead Groups</TD>
																<TD class="DataGridHeader3" style="BORDER-RIGHT: #ebebeb 1px solid; WIDTH: 10%">Required</TD>
															</TR>
														</TABLE>
														<TABLE>
															<TR>
																<TD>
																	<asp:Panel id="pnlLeadGroups" Runat="server"></asp:Panel></TD>
															</TR>
														</TABLE>-->
                                <asp:Panel ID="pnlTerm" runat="server" Visible="true">
                                </asp:Panel>
                                <table width="100%" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="spacertables"></td>
                                    </tr>
                                    <tr>
                                        <td class="twocolumnlabelcell" align="center" colspan="2">
                                            <asp:LinkButton ID="lnkReqGrpIdDef" TabIndex="7" runat="server" CssClass="Label"
                                                Visible="False">Requirement Group Definition</asp:LinkButton></td>
                                    </tr>
                                </table>
                                <!--end table content-->
                                <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="Label"
                                    Width="10%"></asp:TextBox>
                                <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="Label"
                                    Width="10%"></asp:TextBox>
                                <!--end table content-->
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:Panel>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
        ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
            runat="server">
        </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <!--end validation panel-->
</asp:Content>
