﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false"
    CodeFile="LeadDocsReceived.aspx.vb" Inherits="LeadDocsReceived" %>

<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>Doc Management</title>
    <script src="../Scripts/Advantage.Client.AD.js"></script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var uploadedFilesCount = 0;
            var isEditMode;
            function validateRadUpload(source, e) {
                if (isEditMode == null || isEditMode == undefined) {
                    e.IsValid = false;

                    if (uploadedFilesCount > 0) {
                        e.IsValid = true;
                    }
                }
                isEditMode = null;
            }

            function OnClientFileUploaded(sender, eventArgs) {
                uploadedFilesCount++;
            }

            function openFileBrowser(fileurl) {
                alert(fileurl);
                //   var fileurl = e.CommandArgument;
                window.open('../FileBrowser.aspx?fileurl=' + fileurl, '_blank', 'HistWin', 'width=700,height=600,resizable=yes');
            }
            
            
        </script>
    </telerik:RadCodeBlock>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" runat="Server">
     <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

   
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelContent" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0" BackColor="#FAFAFA">
                <tr>
                    <td class="listframetop2">
                        <br />
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td>
                                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                        <tr>
                                            <td class="listframetop" nowrap>
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0" >
                                                    <tr>
                                                        <td nowrap>
                                                            <asp:Label ID="lblDocumentStatus" runat="server" CssClass="label">Document Status</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlDocFilterStatus" runat="server" CssClass="dropdownlist">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap>
                                                        </td>
                                                        <td>
                                                            <%--<asp:Button ID="btnApplyFilter" runat="server" CssClass="buttontopfilter" Text="Apply Filter"
                                                                CausesValidation="False"></asp:Button>--%>
                                                            <telerik:RadButton ID="btnApplyFilter" runat="server"  Text="Apply Filter"
                                                                CausesValidation="False">
                                                            </telerik:RadButton>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="listframebottom">
                                                <div class="scrollleftfltr2rows">
                                                    <asp:DataList ID="dlstDocumentStatus" runat="server">
                                                        <SelectedItemStyle CssClass="selecteditem"></SelectedItemStyle>
                                                        <SelectedItemTemplate>
                                                        </SelectedItemTemplate>
                                                        <ItemStyle CssClass="nonselecteditem"></ItemStyle>
                                                        <ItemTemplate>
                                                            <asp:LinkButton Text='<%# Container.DataItem("fullname") %>' runat="server" CssClass="nonselecteditem"
                                                                CommandArgument='<%# Container.DataItem("LeadDocId")%>' ID="Linkbutton2" CausesValidation="False" />
                                                        </ItemTemplate>
                                                    </asp:DataList></div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="true">
                        </asp:Button><asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" Enabled="true" CausesValidation="false">
                        </asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete"
                            CausesValidation="true" Enabled="False"></asp:Button>
                    </td>
                    
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="scrollright2" style="padding-left: 10px;">
                            <!--begin content table-->
                            <asp:Panel ID="pnlRHS" runat="server">
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="60%">
                                    <asp:TextBox ID="txtStEmploymentId" runat="server" Visible="false"></asp:TextBox>
                                    <asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblDocumentId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlDocumentId" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="contentcell4right">
                                            <asp:CheckBox ID="chkOverride" runat="server" CssClass="checkboxstyle" Text="Override" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblDocStatusId" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4">
                                            <asp:DropDownList ID="ddlDocStatusId" runat="server" CssClass="dropdownlist">
                                            </asp:DropDownList>
                                        </td>
                                        <td class="contentcell4right">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            <asp:Label ID="lblReceiveDate" runat="server" CssClass="label"></asp:Label>
                                        </td>
                                        <td class="contentcell4" style="text-align: left">
                                            <%--<asp:TextBox ID="txtReceiveDate" runat="server" CssClass="textboxdate"></asp:TextBox>
                                    <a onclick="javascript:OpenCalendar('ClsSect','txtReceiveDate', true, 1945)">
                                        <img id="Img2" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                            align="absMiddle"></a>--%>
                                            <telerik:RadDatePicker ID="txtReceiveDate" MinDate="1/1/1945" runat="server">
                                            </telerik:RadDatePicker>
                                        </td>
                                        <td class="contentcell4right">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell">
                                            &nbsp;
                                        </td>
                                        <td class="contentcell4">
                                            <telerik:RadButton ID="btnViewDocsByStatus" runat="server"  Text="View docs by status" CausesValidation="false">
                                            </telerik:RadButton>
                                        </td>
                                        <td class="contentcell4right">
                                            &nbsp;
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label3" runat="server" class="labelbold" Text="Upload Documents"></asp:Label>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <asp:CheckBox ID="chkScannedId" runat="server" AutoPostBack="True" Visible="False">
                                    </asp:CheckBox>
                                    <asp:Label ID="lblScanned" runat="server" Visible="False"></asp:Label>
                                    <asp:LinkButton ID="lnkViewDoc" runat="server" Visible="False"></asp:LinkButton>
                                </table>

                                <telerik:RadGrid runat="server" ID="RadGrid1" AllowPaging="True" AllowSorting="True" Visible="true"
                                AutoGenerateColumns="False" Width="97%" ShowStatusBar="True" GridLines="None"
                                OnItemCreated="RadGrid1_ItemCreated" PageSize="5" OnInsertCommand="RadGrid1_InsertCommand"
                                OnNeedDataSource="RadGrid1_NeedDataSource" OnDeleteCommand="RadGrid1_DeleteCommand"
                                OnUpdateCommand="RadGrid1_UpdateCommand" OnItemCommand="RadGrid1_ItemCommand" >
                                <PagerStyle Mode="NumericPages" AlwaysVisible="true" />
                                <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="FileID" InsertItemPageIndexAction="ShowItemOnFirstPage">
                                  <CommandItemSettings AddNewRecordText="Add New Record" RefreshText="View All" ShowRefreshButton="false" />
                                    <CommandItemTemplate>
                                        <div style="height: 20px;">
                                            <div style="float: left; vertical-align: top;">
                                                <asp:Button runat="server" ID="btn1" CommandName="InitInsert" CssClass="rgAdd" Text=" " />
                                                <asp:LinkButton runat="server" ID="linkbuttionInitInsert" CommandName="InitInsert"
                                                    Text="Add New Record"></asp:LinkButton>
                                            </div>                                                   
                                        </div>
                                    </CommandItemTemplate>
                                    <Columns>
                                        <telerik:GridEditCommandColumn ButtonType="ImageButton" EditText="Replace">
                                            <HeaderStyle Width="3%" />
                                        </telerik:GridEditCommandColumn>
                                        <telerik:GridTemplateColumn HeaderText="Document Name" EditFormHeaderTextFormat="Document to upload" 
                                            UniqueName="DisplayName" SortExpression="DisplayName">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblName" Text='<%# Eval("DisplayName") & Eval("FileExtension") %>' />
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                            </EditItemTemplate>
                                            <HeaderStyle Width="30%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn DataField="Data" HeaderText="" EditFormHeaderTextFormat=""
                                            UniqueName="Upload">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Linkbutton1" runat="server" Text='click here to view the document'
                                                    CssClass="Label" CausesValidation="False" CommandArgument='<%# Eval("FileUrl") %>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <telerik:RadAsyncUpload runat="server" ID="AsyncUpload1" OnClientFileUploaded="OnClientFileUploaded" DisablePlugins="true" 
                                                    AllowedFileExtensions="jpg,jpeg,png,gif,pdf,txt,doc,docx,xls,xlsx">
                                                </telerik:RadAsyncUpload>
                                            </EditItemTemplate>
                                        </telerik:GridTemplateColumn>
                                      <%--  <telerik:GridButtonColumn Text="Delete" CommandName="Delete" ButtonType="ImageButton">
                                            <HeaderStyle Width="2%" />
                                        </telerik:GridButtonColumn>--%>
                                         <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" ConfirmDialogType="Classic"
                                            ConfirmText="Delete this item?" ConfirmTitle="Delete" Text="Delete" UniqueName="DeleteColumn"
                                            HeaderStyle-Width="2%">
                                            <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" />
                                        </telerik:GridButtonColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn ButtonType="ImageButton" />
                                    </EditFormSettings>
                                    <PagerStyle AlwaysVisible="True" />
                                </MasterTableView>
                            </telerik:RadGrid>
                                
                                <%--JG-07-23-2014 COMMENTED CODE BELOW RELATES TO REGENT WHICH IS OBSELETE, AS PER LORI. --%>

                               <%-- <asp:Panel ID="pnlUpload" runat="server" Visible="false" Width="100%" Height="1px">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%">
                                        <tr>
                                            <td class="spacertables" colspan="3">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" colspan="3">
                                                <asp:Label ID="lblHeading" CssClass="labelbold" runat="server">Upload an electronic document:</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables" colspan="3">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblFileName" CssClass="label" runat="server">File</asp:Label>
                                            </td>
                                            <td class="contentcell4" style="text-align: left">
                                                <input class="textbox" id="txtUpLoad" type="file" name="txtUpLoad" runat="server"
                                                    width="100%" />
                                            </td>
                                            <td class="contentcell4right">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                &nbsp;
                                            </td>
                                            <td class="contentcell4" style="text-align: left">
                                                <telerik:RadButton ID="btnUpload" runat="server"  Text="Upload Document">
                                                </telerik:RadButton>
                                            </td>
                                            <td class="contentcell4right">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                        
                        
                        
                            
                        
                                </asp:Panel>
                                <asp:Panel ID="pnlViewDoc" runat="server" Visible="False">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%">
                                        <tr>
                                            <td class="contentcellheader" nowrap>
                                                <asp:Label ID="lblgeneral" runat="server" CssClass="label" Font-Bold="true">View 
                                                        Documents</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlRegent" runat="server" Visible="false">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" style="border: 1px solid #ebebeb;">
                                        <tr>
                                            <td class="contentcellheader" nowrap style="border-top: 0px; border-right: 0px; border-left: 0px">
                                                <asp:Label ID="lblMapToRegent" runat="server" Font-Bold="true" CssClass="label">Map To Regent</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellcitizenship" style="padding-top: 12px">
                                                <asp:Panel ID="pnlRegentAwardChild" TabIndex="12" runat="server" Width="100%">
                                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="70%" align="center"
                                                        border="0">
                                                        <tr>
                                                            <td class="contentcell">
                                                                <asp:Label ID="lblTrackCode" runat="server" CssClass="label">Track Code</asp:Label>
                                                            </td>
                                                            <td class="contentcell4">
                                                                <asp:DropDownList ID="ddlTrackCode" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="contentcell">
                                                                <asp:Label ID="lblDueDate" runat="server" CssClass="label">Due Date</asp:Label>
                                                            </td>
                                                            <td class="contentcell4">
                                                                <asp:DropDownList ID="ddlDueDate" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="contentcell">
                                                                <asp:Label ID="lblNotifiedDate" runat="server" CssClass="label">Notified Date</asp:Label>
                                                            </td>
                                                            <td class="contentcell4">
                                                                <asp:DropDownList ID="ddlTransDate" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="contentcell">
                                                                <asp:Label ID="lblCompletedDate" runat="server" CssClass="label">Completed Date</asp:Label>
                                                            </td>
                                                            <td class="contentcell4">
                                                                <asp:DropDownList ID="ddlCompletedDate" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="contentcell">
                                                                <asp:Label ID="lblNotificationCode" runat="server" CssClass="label">Notification Code</asp:Label>
                                                            </td>
                                                            <td class="contentcell4">
                                                                <asp:DropDownList ID="ddlNotificationCode" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="contentcell">
                                                                <asp:Label ID="lblNotes" runat="server" CssClass="label">Note</asp:Label>
                                                            </td>
                                                            <td class="contentcell4">
                                                                <asp:TextBox ID="txtNotes" CssClass="textbox" runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                               
                                <div id="OldStudentDocGrid" visible="false" runat="server">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%">
                                        <tr>
                                            <td>
                                                <asp:DataGrid ID="dgrdDocs" runat="server" Width="90%" align="center" GridLines="Horizontal"
                                                    EditItemStyle-Wrap="false" HeaderStyle-Wrap="true" AllowSorting="True" AutoGenerateColumns="False"
                                                    BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0" CellPadding="0">
                                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="FileName">
                                                            <HeaderStyle CssClass="datagridheader" Width="40%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="Linkbutton1" runat="server" Text='<%# Container.DataItem("DisplayName")%>'
                                                                    CssClass="label" CausesValidation="False" CommandArgument='<%# Container.DataItem("FileName") & Container.DataItem("FileExtension")%>'
                                                                    CommandName="StudentSearch">
                                                                </asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="DocumentType">
                                                            <HeaderStyle CssClass="datagridheader" Width="40%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocumentType" Text='<%# Container.DataItem("DocumentCategory") %>'
                                                                    CssClass="label" runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="">
                                                            <HeaderStyle CssClass="datagridheader" Width="20%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Button ID="btnDeleteFile" Text="Delete"  runat="server" CommandArgument='<%# Container.DataItem("FileName")%>'
                                                                    CommandName="DeleteFile" />
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </div>--%>

                        </asp:Panel>
                        <!--end content table-->
                        </div>
                        
                        <asp:Panel ID="Panel2" runat="server" Visible="False">
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" Visible="False" ShowMessageBox="True"
                                ShowSummary="False"></asp:ValidationSummary>
                            <asp:CustomValidator ID="CustomValidator2" runat="server" Visible="False" Display="None"></asp:CustomValidator>
                        </asp:Panel>
                        <asp:TextBox ID="txtStudentDocId" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtDocumentName" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtLeadId" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtLeadName" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtPath" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtDocumentType" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtExtension" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="txtStudentId" runat="server" Visible="False"></asp:TextBox>
                        <div>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">



</asp:Content>
