﻿<%@ Page Title="Lead Information" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="AleadInfoPage.aspx.vb" Inherits="AdvWeb.AD.AdAleadInfoPage" %>

<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" runat="Server">
    <link href="../css/AD/LeadInfoPage.css" rel="stylesheet" />
    <script src="../Kendo/js/jszip.min.js"></script>
    <%-- <div style="overflow: scroll; height: 1024px;">--%>
    <div id="wrapperLead" style="overflow: auto; position: fixed; max-height: 840px; width: 100%; top: 215px;" class="hidden">
        <div id="containerInfoPage" data-role="validator">
            <div id="personalInformation">
                <label id="firstName" class="textLabelleft">First Name</label>
                <input id="txtFirstName" name="FirstName" class="k-textbox fieldlabel1" maxlength="28" />
                <%--<span class="k-invalid-msg" data-for="FirstName"></span>--%>
                <label id="nickname" class="textLabelright">Nickname</label>
                <input id="tbNickname" class="k-textbox fieldlabel2" maxlength="12" />
                <label id="middleName" class="textLabelleft">Middle Name</label>
                <input id="txtMiddleName" class="k-textbox fieldlabel1 " maxlength="28" />
                <label id="prefix" class="textLabelright">Prefix</label>
                <span class="cb-small">
                    <input id="cbprefix" />
                </span>
                <label id="lastName" class="textLabelleft">Last Name</label>
                <input id="txtLastName" name="txtLastName" class="k-textbox fieldlabel1" maxlength="28" />
                <%-- <span class="k-invalid-msg" data-for="txtLastName"></span>--%>


                <label id="suffix" class="textLabelright">Suffix</label>
                <span class="cb-small">
                    <input id="cbsuffix" />
                </span>

                <div class="clearfix"></div>
                <label id="prevlast" class="textLabelleft">Prior Last Name</label>
                <input id="cbPrevlast" />
                <%--<input id="txtprevlast" class="k-textbox fieldlabel1" />--%>
                <label id="gender" class="textLabelright">Gender</label>
                <span class="cb-small">
                    <input id="cbgender" /></span>
                <div class="clearfix"></div>


                <label id="leadStatus" class="textLabelleft">Lead Status</label>
                <input id="cbLeadStatus" />
                <label id="lblRace" class="textLabelright">Race</label>
                <span class="cb-small">
                    <input id="cbRace" /></span>

                <div class="clearfix"></div>

                <label id="leadAssign" class="textLabelleft">Lead Assigned to</label>
                <input id="cbLeadAssign" />
                <div class="clearfix"></div>

                <label id="interestArea" class="textLabelleft">Interest Area</label>
                <input id="cbInterestArea" />
                <div class="clearfix"></div>
                <label id="programLabel" class="textLabelleft">Program</label>
                <input id="cbprogramLabel" />
                <div class="clearfix"></div>
                <label id="programVersion" class="textLabelleft">Program Version</label>
                <input id="cbprogramVersion" />
                <div class="clearfix"></div>
                <label id="expectedStart" class="textLabelleft">Expected Start</label>
                <input id="cbExpectedStart" style="display: none" />
                <input id="dpExpectedStart" style="display: none" name="dpExpectedStart" data-validation="date" />

                <div class="clearfix"></div>
                <label id="attend" class="textLabelleft">Attend</label>
                <span class="cb-small">
                    <input id="cbattend" /></span>
                <label id="schedule" class="textLabelfree">Schedule</label>
                <span class="cb-small">
                    <input id="cbschedule" />
                </span>
                <div class="clearfix"></div>
                <%--<label id="grad" class="textLabelleft">Expected Grad</label>
                <input id="dtGrad" data-validation="date" />--%>
            </div>
            <div id="personalInformation2" style="margin-bottom: 5px">
                <label id="socialSecurity" class="textLabelleftDemog2">SSN</label>
                <input id="maskedsocialSecurity" class="fieldlabel2_1" type="text" data-validation="ssn" name="maskedsocialSecurity"
                    data-role="maskedtextbox" data-mask="000-00-0000" />
                <span data-for="maskedsocialSecurity" class="k-invalid-msg"></span>
                <div class="clearfix"></div>
                <label id="dob" class="textLabelleftDemog2">DOB</label>
                <input id="dtDob" name="dtDob" data-validation="date" />
                <label id="age" class="textLabelfree">Age</label>

                <input id="textage" class="k-textbox fieldlabel5" name="textage" data-validation="number" maxlength="3" />

                <label id="citizenship" class="textLabelleftDemog2">Citizenship</label>
                <span class="cb-medium">
                    <input id="cbcitizenship" /></span>
                <div class="clearfix"></div>

                <label id="aliennumber" class="textLabelleftDemog2">Alien Number</label>
                <input id="tbaliennumber" class="k-textbox fieldlabel4" />
                <br />
                <br />
                <label id="dependency" class="textLabelleftDemog2">Dependency</label>
                <span class="cb-medium">
                    <input id="cbdependency" /></span>
                <div class="clearfix"></div>

                <label id="maritalstatus" class="textLabelleftDemog2">Marital Status</label>
                <span class="cb-small">
                    <input id="cbmaritalstatus" /></span>
                <div class="clearfix"></div>
                <label id="dependants" class="textLabelleftDemog2">Dependants</label>
                <span class="cb-xx-small">
                    <input id="itextdependants" style="width: 43px; border-width: 1px; border-radius: 0" />
                </span>
                <div class="clearfix"></div>
                <label id="familyincome" class="textLabelleftDemog2">Family Income</label>
                <span class="cb-medium">
                    <input id="cbfamilyincome" /></span>
                <div class="clearfix"></div>

                <label id="housingtype" class="textLabelleftDemog2">Housing Type</label>
                <span class="cb-medium">
                    <input id="cbhousingtype" /></span>
                <div class="clearfix"></div>

                <label id="driverlicensestate" class="textLabelleftDemog2">Driv Lic State</label>
                <span class="cb-small">
                    <input id="cbdriverlicensestate" /></span>
                <div class="clearfix"></div>
                <label id="driverlicensenumber" class="textLabelleftDemog2">Driv Lic Number</label>
                <input id="tdriverlicensenumber" class="k-textbox fieldlabel4" maxlength="17" />
                <div class="clearfix"></div>

                <label id="transportation" class="textLabelleftDemog2">Transportation</label>
                <span class="cb-medium">
                    <input id="cbtransportation" /></span>
                <button id="goMoreTransportation" type="button" class="k-button" style="display: inline-block; margin-top: 4px; height: 24px; vertical-align: text-top;">
                    More
                </button>
                <div class="clearfix"></div>
                <label id="timeTo" class="textLabelleftDemog2">Distance to School</label>
                <span class="cb-x-small">
                    <input id="tpTimeTo" style="width: 60px; border-width: 1px; border-radius: 0" maxlength="4" />

                </span>
                <label id="measureUnit" class="textLabelleftDemog2" style="margin-left: 0;">miles</label>
            </div>
            <div id="contactInformation">
                <div class="textLabelfree">
                    <label id="preferredContact">Preferred Contact</label>
                    <input id="preferredPhone" type="radio" name="contact" checked="checked" style="vertical-align: top;" />Phone
                        <input id="preferredEmail" type="radio" name="contact" />Email
                        <input id="preferredText" type="radio" name="contact" />Text
                </div>
                <div class="clearfix"></div>
                <label id="bestime" class="textLabelfree">
                    Best Time
                </label>
                <span class="cb-small" style="margin: 3px">
                    <input id="tpBestTime" name="tpBestTime" style="width: 100px;" data-validation="time" /></span>
                <div class="clearfix"></div>
                <!-- Phone -->
                <label id="phone" class="textLabelleftContact">Phone - Best</label>
                <input type="hidden" id="hiddenPhoneId" value="0">
                <span class="cb-x-small">
                    <input id="cbphone" name="cbphone" data-validation="dependRequired" data-dependable="txtPhone" data-empty="Type" />
                </span>
                <input id="txtPhone" class=" fieldlabel21" name="txtPhone" type="tel" />
                <input id="tbphoneext" class="k-textbox fieldlabel6" maxlength="10" />
                <label class="checkphone">
                    <input id="checkphone" type="checkbox"><span id="txtcheckphone">Int'l</span></label>
                <div class="clearfix"></div>

                <!-- Phone 1 -->
                <label id="phone1" class="textLabelleftContact">Phone</label>
                <input type="hidden" id="hiddenPhone1Id" value="0">
                <span class="cb-x-small">
                    <input id="cbphone1" name="cbphone1" data-validation="dependRequired" data-dependable="txtPhone1" data-empty="Type" />
                </span>
                <input id="txtPhone1" class=" fieldlabel21" name="txtPhone1" type="tel" />
                <input id="tbphone1ext" class="k-textbox fieldlabel6" maxlength="10" />
                <label class="checkphone">
                    <input id="checkphone1" type="checkbox"><span id="txtcheckphone1">Int'l</span></label>
                <div class="clearfix"></div>

                <!-- Phone 2 -->
                <label id="phone2" class="textLabelleftContact">Phone</label>
                <input type="hidden" id="hiddenPhone2Id" value="0">
                <span class="cb-x-small">
                    <input id="cbphone2" name="cbphone2" data-validation="dependRequired" data-dependable="txtPhone2" data-empty="Type" />
                </span>
                <input id="txtPhone2" class=" fieldlabel21" name="txtPhone2" type="tel" />
                <input id="tbphone2ext" class="k-textbox fieldlabel6" maxlength="10" />
                <label class="checkphone">
                    <input id="checkphone2" type="checkbox"><span id="txtcheckphone2">Int'l</span></label>
                <div class="clearfix"></div>
                <label id="emailprimary" class="textLabelleftContact">Email - Best</label>
                <span class="cb-x-small">
                    <input id="cbemailprimary" name="cbemailprimary" data-validation="dependRequired" data-dependable="txtemailprimary" data-empty="Type" />
                </span>
                <input id="txtemailprimary" class="k-textbox fieldlabelEmail " maxlength="100" type="email" name="txtemailprimary" />
                <input type="hidden" id="hiddenEmailPrimaryId" value="0">
                <label class="checkphone">
                    <input id="checkNoEmail" type="checkbox"><span id="noEmail">None</span></label>
                <div class="clearfix"></div>
                <label id="email" class="textLabelleftContact">Email</label>
                <input type="hidden" id="hiddenEmailId" value="0">
                <span class="cb-x-small">
                    <input id="cbemail" name="cbemail" data-validation="dependRequired" data-dependable="txtemail" data-empty="Type" />
                </span>
                <input id="txtemail" class="k-textbox fieldlabelEmail " maxlength="100" type="email" name="txtemail" />

                <div class="clearfix"></div>
                <label id="address1" class="textLabelleftContact">Address 1</label>
                <span class="cb-x-small">
                    <input id="cbAddress" name="cbAddress" data-validation="dependRequired" data-dependable="txtAddress1" data-empty="Type" />
                </span>
                <input id="txtAddress1" class="k-textbox fieldlabel9" maxlength="250" />
                <input id="txtApart" class="k-textbox fieldlabel6" />
                <label class="checkphone">
                    <input id="checkIntAddress" type="checkbox">Int'l</label>
                <div class="clearfix"></div>
                <label id="address2" class="textLabelleftContact">Address 2</label>
                <input id="txtAddress2" class="k-textbox fieldlabel7" maxlength="250" />

                <label id="city" class="textLabelleftContact">City, State, Zip</label>
                <input id="txtCity" class="k-textbox fieldlabel8" maxlength="150" />

                <span class="cb-small">
                    <input id="cbState" />
                </span>
                <input id="otherStates" class="k-textbox fieldlabel2" style="display: none" />
                <input id="txtZipCode" class=" fieldlabel6" name="txtZipCode" data-validation="zip" />
                <div class="clearfix"></div>
                <label class="textLabelleftContact">
                    <span id="county">County</span>
                </label>

                <span class="cb-medium">
                    <input id="cbCounty" /></span>

                <div class="clearfix"></div>
                <label class="textLabelleftContact">
                    <span id="country">Country</span>
                </label>
                <span class="cb-large">
                    <input id="cbcountry" /></span>

                <div class="clearfix"></div>
                <label class="textLabelleftContact"></label>
                <button id="gocontactInformation" type="button" class="k-button">More Contacts</button>
            </div>
            <div class="clearfix"></div>
            <div id="infobanner">
                <span style="margin-left: 5px"><b>Source</b></span>
                <span style="margin-left: 260px"><b>Other</b></span>
                <span id="leadGroupHeader" style="margin-left: 668px"><b>Lead Groups</b><span class="IsRequiredAsterisk">*</span></span>
            </div>
            <div id="subContainerInfoPage">
                <!-- Subsection Source.......................................................... -->
                <div id="source">
                    <!--Category -->
                    <label id="categorySource" class="textLabelleftSource">Category</label>
                    <input id="cbcategorySource" />
                    <!--Type -->
                    <label id="typeSource" class="textLabelleftSource">Type</label>
                    <input id="cbtypeSource" />

                    <!--Vendor -->
                    <label id="vendorSource" class="textLabelleftSource">Vendor</label>
                    <input id="txtVendorSource" class="k-textbox fieldlabel1 k-valid" disabled="disabled" />

                    <!--Advertisement -->
                    <label id="advertisementSource" class="textLabelleftSource">Advertisement</label>
                    <input id="cbAdvertisementSource" />
                    <label id="DateSource" class="textLabelleftSource">Date/Time</label>
                    <input id="dtDateSource" style="width: 167px" name="dtDateSource" data-validation="datetime" />
                    <label id="noteSource" class="textLabelleftSource">Note</label>
                    <textarea class="fieldlabelNote" id="txtNoteSource" maxlength="50" rows="4"></textarea>

                </div>
                <div id="other">
                    <label id="otherDateApplied" class="textLabelOtherleft">Date Applied</label>
                    <input id="dtotherDateApplied" name="dtotherDateApplied" data-validation="date" />
                    <div class="separatorOther"></div>
                    <label id="otherPreviousEducation" class="textLabelOtherleft2">Previous Education</label>
                    <input id="cbotherPreviousEducation" />
                    <div class="clearfix"></div>

                    <label id="otherAdmRepAssignDate" class="textLabelOtherleft">Adm Rep Assigned</label>
                    <input id="dtotherAdmRepAssignDate" name="dtotherAdmRepAssignDate" data-validation="date" />
                    <div class="separatorOther"></div>
                    <label id="otherHighSchool" class="textLabelOtherleft2">High School</label>
                    <input id="cbOtherHighSchool" />
                    <div class="clearfix"></div>

                    <label id="otherAdmRep" class="textLabelOtherleft">Admissions Rep</label>
                    <input id="cbotherAdmRep" name='Admission Rep' />
                    <div class="separatorOther"></div>

                    <label id="HsGradDate" class="textLabelOtherleft2">HS Grad Date</label>
                    <input id="dtHsGradDate" name="dtHsGradDate" data-validation="date" />
                    <div class="clearfix"></div>

                    <label id="otherSponsor" class="textLabelOtherleft">Company Sponsor</label>
                    <input id="cbotherSponsor" />

                    <div class="separatorOther"></div>

                    <label id="AttendingHs" class="textLabelOtherleft2">Attending HS</label>
                    <input id="ddAttendingHs" />
                    <div class="clearfix"></div>

                    <label id="otherReasonNot" class="textLabelOtherleft">Reason not enrolled</label>
                    <input id="cbotherReasonNot" />

                    <div class="separatorOther"></div>

                    <label id="otherAdmCriteria" class="textLabelOtherleft2">Admin Criteria</label>
                    <input id="cbotherAdmCriteria" />
                    <div class="clearfix"></div>

                    <label id="otherComment" class="textLabelOtherleft">Comments</label>
                    <textarea class="biggerComment" id="tbOtherComment" maxlength="100" rows="2"></textarea>
                    <%-- <div id="tbOtherComment" class="biggerComment"></div>--%>
                </div>
                <div class="clearfix"></div>
                <!-- UDF Area -->
                <div id="infobanner2" class="CaptionLabel"><b>Custom</b></div>
                <br />
                <div id="custom">
                </div>
            </div>
            <div id="leadGroups" style="margin-left: 5px; padding-left: 10px" data-validation="oneChecked">
            </div>
        </div>
        <div id="WindowsDuplicate" style="display: none">
            <span class="k-toolbar">The following potential duplicates was/where found:</span>
            <div id="infoPageDupGrid"></div>
            <div id="buttonPanel" class="downpanel">
                <button id="insertLead" type="button" class="k-button">Save New Lead</button>
                <button id="cancelLead" type="button" class="k-button">Cancel Save</button>
            </div>
        </div>
        <section id="windowsVehicle" style="display: none">
            <section id="vehicle1">
                <header>Vehicle 1</header>
                <br />
                <label id="">Parking Permit</label>
                <input id="vehicleParkingPermit1" class="k-textbox" maxlength="49" />
                <div class="clearfix"></div>
                <label id="">Vehicle Make</label>
                <input id="vehicleMaker1" class="k-textbox" name="vehicleMaker1" data-validation="lettersnumbers" title="Special symbol are not allowed" maxlength="49" />
                <div class="clearfix"></div>
                <label id="">Vehicle Model</label>
                <input id="vehicleModel1" class="k-textbox" name="vehicleModel1" data-validation="lettersnumbers" title="Special symbol are not allowed" maxlength="49" />
                <div class="clearfix"></div>
                <label id="">Vehicle Color</label>
                <input id="vehicleColor1" name="nvehicleColor1" class="k-textbox" data-validation="letters" title="Please enter only letters" maxlength="49" />
                <div class="clearfix"></div>
                <label id="">License Plate</label>
                <input id="vehiclePlate1" class="k-textbox" name="vehiclePlate1" data-validation="lettersnumbers" title="Special symbol are not allowed" maxlength="49" />

                <div class="clearfix"></div>
            </section>
            <section id="vehicle2">
                <header>Vehicle 2</header>
                <br />
                <label id="">Parking Permit</label>
                <input id="vehicleParkingPermit2" class="k-textbox" maxlength="49" />
                <div class="clearfix"></div>
                <label id="">Vehicle Make</label>
                <input id="vehicleMaker2" class="k-textbox" name="vehicleMaker2" data-validation="lettersnumbers" title="Special symbol are not allowed" maxlength="49" />
                <div class="clearfix"></div>
                <label id="">Vehicle Model</label>
                <input id="vehicleModel2" class="k-textbox" name="vehicleModel2" data-validation="lettersnumbers" title="Special symbol are not allowed" maxlength="49" />
                <div class="clearfix"></div>
                <label id="">Vehicle Color</label>
                <input id="vehicleColor2" class="k-textbox" name="nvehicleColor2" data-validation="letters" title="Please enter only letters" maxlength="49" />
                <div class="clearfix"></div>
                <label id="">License Plate</label>
                <input id="vehiclePlate2" class="k-textbox" name="vehiclePlate2" data-validation="lettersnumbers" title="Special symbol are not allowed" maxlength="49" />
                <div class="clearfix"></div>
            </section>
            <div class="clearfix"></div>
            <div id="vehiclePanel" class="downpanel">
                <button id="vehicleSave" type="button" class="k-button" style="margin-right: 5px; width: 100px;">Done</button>
                <button id="vehicleCancel" type="button" class="k-button" style="margin-right: 10px; width: 100px">Cancel</button>
            </div>

        </section>
        <%--<div id="bottom">bottom</div>--%>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            var manager = new AD.LeadInfoPage();
        });

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

