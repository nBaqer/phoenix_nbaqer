
Imports System.Data
Imports System.Diagnostics
Imports FAME.AdvantageV1.BusinessFacade


Partial Class AssignLeadGroups
    Inherits Page
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblRequirement As Label
    Protected WithEvents lblEffectiveDates As Label
    Protected WithEvents label2 As Label


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected WithEvents AllDataset As DataSet
    Protected WithEvents DataGridTable As DataTable
    Protected WithEvents DataListTable As DataTable
    Protected WithEvents LeadGroupTable As DataTable
    Dim strEffectiveDateId As String
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        'CreateDatasetAndTables()
        Dim chkMandatory As Boolean
        strEffectiveDateId = Request.QueryString("adReqEffectiveDateId")
        chkMandatory = CType(Request.QueryString("MandatoryRequirement"), Boolean)
        'label3.Text = Request.QueryString("Requirement")

        Dim sMessage As String = "For : <strong>" & Request.QueryString("Requirement") & "</strong>"
        If Not Request.QueryString("EndDate") = "" Then
            sMessage &= " <br><br>Valid From: <strong>" & Request.QueryString("StartDate") & " </strong> Through <strong>" & Request.QueryString("EndDate") & "</strong>." & vbLf
        Else
            sMessage &= " <br><br>Valid From: <strong>" & Request.QueryString("StartDate") & vbLf
        End If
        sMessage &= "<br><br>Please select the appropriate lead groups below"

        If chkMandatory = True Then
            'sMessage = ""
            sMessage = "For mandatory school requirements,the system will automatically flag all lead groups." & vbLf
            label3.Text = sMessage
        Else
            label3.Text = sMessage
        End If

        label3.Text = sMessage



        If Not Page.IsPostBack Then
            BindDataGridList()
        End If
        If chkMandatory = True Then
            CheckAll()
        End If
        If chkMandatory Then
            dgrdTransactionSearch.Enabled = False
            btnAssignLeadGrp.Enabled = False
        Else
            dgrdTransactionSearch.Enabled = True
            btnAssignLeadGrp.Enabled = True
        End If
    End Sub

    Public Sub SetUpLeadGroups()
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        Dim strLeadGrpId As String
        '        Dim intRequired As Integer
        Dim intArrCount, intChkCount As Integer
        'Dim intResult As Integer
        Dim z As Integer
        Dim facade As New LeadFacade

        ' Save the datagrid items in a collection.
        iitems = dgrdTransactionSearch.Items
        Try
            'Loop thru the collection to get the Array Size
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If (CType(iitem.FindControl("chkRequired"), CheckBox).Checked = True) Then
                    If (CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = False) Then
                        CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True
                    End If
                End If
                'Modified by balaji on 11/3/2005 for mantis issue : 5641
                If (CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True) Then
                    intArrCount += 1
                End If
            Next

            'to be modified by balaji on 11/3/2005
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If (CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = False) Then
                    intChkCount = 0
                Else
                    intChkCount = 1
                End If
            Next

            'If none of the checkbox has been selected
            If intChkCount = 0 And intArrCount = 0 Then
                Dim intDeleteAllLeadGrps As Integer = facade.DeleteLeadGroups(strEffectiveDateId)
                If intDeleteAllLeadGrps = 0 Then
                    Exit Sub
                End If
            End If

            If intArrCount >= 1 Then
                Dim selectedClsSection() As String = CType(Array.CreateInstance(GetType(String), intArrCount), String())
                Dim selectedRequired() As String = CType(Array.CreateInstance(GetType(String), intArrCount), String())

                'Loop Through The Collection To Get ClassSection
                For i = 0 To iitems.Count - 1
                    iitem = iitems.Item(i)
                    'retrieve clssection id from the datagrid
                    strLeadGrpId = CType(iitem.FindControl("txtLeadGrpId"), TextBox).Text

                    If (CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True) Then
                        selectedClsSection.SetValue(strLeadGrpId, z)
                        If (CType(iitem.FindControl("chkRequired"), CheckBox).Checked = True) Then
                            selectedRequired.SetValue("Yes", z)
                        Else
                            selectedRequired.SetValue("No", z)
                        End If
                        z += 1
                    End If

                Next

                'resize the array
                If z > 0 Then ReDim Preserve selectedClsSection(z - 1)
                If z > 0 Then ReDim Preserve selectedRequired(z - 1)

                facade.InsertLeadGroupsByRequirement(strEffectiveDateId, selectedClsSection, selectedRequired, CType(Session("UserName"), String))
            End If
        Finally
        End Try
    End Sub
    Public Sub CheckAll()
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer

        ' Save the datagrid items in a collection.
        iitems = dgrdTransactionSearch.Items
        'Loop thru the collection to get the Array Size
        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True
            CType(iitem.FindControl("chkUnschedule"), CheckBox).Enabled = False

            CType(iitem.FindControl("chkRequired"), CheckBox).Checked = True
            CType(iitem.FindControl("chkRequired"), CheckBox).Enabled = False
        Next
    End Sub

    Public Sub BindDataGridList()
        Dim facade As New LeadFacade
        dgrdTransactionSearch.DataSource = facade.GetAllLeadGroupsByRequirement(strEffectiveDateId)
        dgrdTransactionSearch.DataBind()
    End Sub
    Private Sub BtnAssignLeadGrpClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnAssignLeadGrp.Click
        SetUpLeadGroups()
        CallMessage("The changes made to lead groups were saved successfully")
    End Sub
    '    Public Shared Sub CloseWindowInMessageBox(ByVal page As Page, ByVal errorMessage As String)
    '        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
    '            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=DisplayError();function DisplayError(){alert('"
    '            Dim scriptEnd As String = "');window.close();}</script>"

    '            '   Register a javascript to display error message
    '            page.ClientScript.RegisterStartupScript(Me.GetType(), 
    ').RegisterStartupScript("ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + scriptEnd)
    '        End If
    '    End Sub
    Private Shared Function ReplaceSpecialCharactersInJavascriptMessage(ByVal s As String) As String
        '   replace \ by \\, ' by \', " by "", CR by \r, LF by \n
        Return s.Trim().Replace("\", "\\").Replace("'", "\'").Replace("""", "\""").Replace(vbCr, "\r").Replace(vbLf, "\n")
    End Function
    Private Sub CallMessage(ByVal errorMessage As String)
        'Set error condition
        'Display error in message box in the client
        If Page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            Const scriptBegin As String = "window.Onload=DisplayError();function DisplayError(){alert('"
            Const scriptEnd As String = "');window.close();}"

            '   Register a javascript to display error message
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + scriptEnd, True)
        End If

        'CloseWindowInMessageBox(Me.Page, errorMessage)
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(CType(ctl, Panel))
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(CType(ctl, DataGrid))
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(CType(ctrl, Panel))
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(CType(ctrl, DataGrid))
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal ctrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In ctrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub
End Class
