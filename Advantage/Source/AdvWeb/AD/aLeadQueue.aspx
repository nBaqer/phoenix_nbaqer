﻿<%@ Page Title="Lead Queue" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="aLeadQueue.aspx.vb" Inherits="AD_aleadQueue" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../Scripts/Advantage.Client.AD.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <link href="../css/AD/LeadQueue.css" rel="stylesheet" />
   <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Width="100%" Orientation="HorizontalTop" Scrolling="Both">
 
        <div id="queueContainer">
            <div id="queueBanner">
                <div id="qZoneLeft">
                    <label id="qlabelLeft" class="labelAlign">Queue</label>
                    <label id="queueQuantity" class="labelAlign"></label>
                </div>
                <div id="qZoneCenter">
                    <label id="qlabelAdmm" class="labelAlign">Admission Rep</label>
                    <input id="ddlqueueAdmm" />
                </div>
                <div id="qZoneRight">
                    <label id="qlabelRight">Refresh:</label>
                    <label id="queueRefresh">00:00</label>
                </div>
            </div>
            <div id="queueGridwrapper">
                <div id="queueGrid"></div>
            </div>
            <div id="queueWindowsPhone" style="display: none">
                <span class="k-toolbar">Please Edit the Phone Number:</span>
                <br />
                <div style="vertical-align: middle">
                    <input id="queuePhoneEdit" class="k-textbox" />
                    <input id="queueInternationalPhone" type="checkbox"><span id="queueTxtcheckphone">International</span>
                </div>
                <br />
                <div id="buttonPanel" class="downpanel">
                    <button id="queuesavePhone" type="button" class="k-button">Save</button>
                    <button id="queuecancelPhone" type="button" class="k-button">Cancel</button>
                </div>
            </div>
            <iframe id="KeepAliveFrame" src="..\KeepSessionAlive.aspx" width="0" height="0" runat="server"></iframe>

        </div>
  </telerik:RadPane>
</telerik:RadSplitter>
    <script type="text/javascript">
        $(document).ready(function () {
    <%-- ReSharper disable once UnusedLocals --%>
            var manager = new AD.LeadQueue();
        });
    </script>
    <script type="text/x-kendo-template" id="queueGridStatusTemplate">
        #if( StatusSysId == 1){#
            <strong>#: Status # </strong>
        #}else{#
            #: Status # 
        #}#
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

