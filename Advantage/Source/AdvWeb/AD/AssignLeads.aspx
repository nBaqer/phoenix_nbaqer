﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="AssignLeads.aspx.vb" Inherits="AssignLeads" %>
<%@ MasterType  virtualPath="~/NewSite.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" width="100%" Orientation="HorizontalTop" Scrolling="Both">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
				<!-- begin rightcolumn -->
				<tr>
					<td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                        ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                </td>
                               
                            </tr>
                        </table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
                        <!-- Open Table For Split -->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="detailsframe">
                                <div class="scrollsingleframe">
                                    <!--begin content here-->
                                    <table width="80%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                        <tr>
                                            <td style="padding: 0 20px 20px 20px; text-align: center">
                                                <asp:Label ID="lblMessage" runat="server" CssClass="labelbold">Please select Admissions Rep, Leads to be assigned and campus. Press "Assign Leads" to see your results</asp:Label></td>
                                        </tr>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" width="40%" border="0" align="center">
                                        <tr>
                                            <td class="contentcell" style="padding-bottom: 20px">
                                                <asp:Label ID="lblCurrentAdmissionsRep" runat="server" CssClass="label">Admissions Rep</asp:Label></td>
                                            <td class="contentcell4" style="padding-bottom: 20px">
                                                <asp:DropDownList ID="ddlCurrentAdmissionRepID" CssClass="dropdownlist" AutoPostBack="true"
                                                    runat="server">
                                                </asp:DropDownList></td>
                                        </tr>
                                    </table>
                                    <table cellspacing="0" cellpadding="0" width="900px" border="0" align="center" >
                                        <tr>
                                            <td width="5%">
                                                &nbsp;</td>
                                            <td width="30%">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td class="fourcolumnheader1" nowrap>
                                                            <asp:Label ID="Label2" CssClass="label" runat="server" Font-Bold="True">Select Leads to be assigned</asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fourcolumncontent1" nowrap>
                                                            <asp:ListBox ID="lstLeads" runat="server" SelectionMode="Multiple" CssClass="listboxes" Rows="10">
                                                            </asp:ListBox></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="2%">
                                                &nbsp;</td>
                                            <td width="30%">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td class="fourcolumnheader1" nowrap>
                                                            <asp:Label ID="lblAssignCampus" CssClass="label" runat="server" Font-Bold="True">Select campus</asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="fourcolumncontent1" nowrap>
                                                            <asp:ListBox ID="lstCampusId" runat="server" SelectionMode="Multiple" CssClass="listboxes" Rows="10">
                                                            </asp:ListBox></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="8%">
                                                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                    <tr>
                                                        <td style="padding: 6px">
                                                            
                                                               
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                        <table width="80%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                            <tr>
                                                <td style="width: 100%; text-align: center; padding: 20px">
                                                <asp:Button ID="btnAssign" Text="Assign Lead" runat="server"  Enabled="False"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="40%" align="center">
                                        <tr>
                                            <td style="padding: 20px">
                                                <asp:Label ID="lblAddress" runat="server" CssClass="label" Font-Bold="true">Results:</asp:Label></td>
                                        </tr>
                                    </table>
                                    <table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">
                                        <tr>
                                            <td>
                                                <asp:DataGrid ID="dgrdTransactionSearch" CellPadding="3" AutoGenerateColumns="False"
                                                    AllowSorting="True" HeaderStyle-Wrap="true" EditItemStyle-Wrap="false" GridLines="Horizontal"
                                                    Width="100%" runat="server" BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0">
                                                    <EditItemStyle Wrap="False"></EditItemStyle>
                                                    <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                    <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Lead(s)">
                                                            <HeaderStyle CssClass="datagridheader" Width="50%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLeadName" runat="server" Text='<%# Container.DataItem("FullName") %>'
                                                                    CssClass="label"> </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Campus">
                                                            <HeaderStyle CssClass="datagridheader" Width="50%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDgFirstName" Text='<%# Container.DataItem("CampusDescrip") %>'
                                                                    CssClass="label" runat="server"> </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:TextBox ID="txtAdmissionReps" runat="server" Visible="false"></asp:TextBox>
                                    <asp:ListBox ID="lstAssignedFlds" runat="server" Rows="10" Visible="false"></asp:ListBox>
                                    <asp:TextBox ID="txtCampusId" runat="server" Visible="False"></asp:TextBox>
                                    <asp:TextBox ID="TextBox2" runat="server" Visible="false"></asp:TextBox>
                                    <asp:TextBox ID="txtLeads" runat="server" Visible="False"></asp:TextBox>
                                    <asp:Label ID="Label1" CssClass="label" runat="server" Font-Bold="True" Visible="false"></asp:Label>
                                    <asp:ListBox ID="lstAssignmentResults" runat="server" CssClass="assignmentresult"
                                        Visible="false"></asp:ListBox>
                                    <!--end content here-->
                                    <!-- end footer -->
                                </div>
                            </td>
                        </tr>
                    </table>
					</td>
					<!-- end rightcolumn --></tr>
			</table>           
			<!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

