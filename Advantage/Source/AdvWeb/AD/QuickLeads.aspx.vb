Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common.TM
Imports FAME.AdvantageV1.BusinessFacade.TM
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Drawing
Imports System.Data
Imports Advantage.Business.Logic.Layer
Partial Class QuickLeads
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtEmployerContactId As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtEmployerId As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblTitleId As System.Web.UI.WebControls.Label
    Protected WithEvents ddlTitleId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblWorkPhone As System.Web.UI.WebControls.Label
    Protected WithEvents txtWorkPhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblHomePhone As System.Web.UI.WebControls.Label
    Protected WithEvents lblCellPhone As System.Web.UI.WebControls.Label
    Protected WithEvents txtCellPhone As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblEmails As System.Web.UI.WebControls.Label
    Protected WithEvents lblBeeper As System.Web.UI.WebControls.Label
    Protected WithEvents txtBeeper As System.Web.UI.WebControls.TextBox


    ' Protected EmployerContactId As String = "03D26D83-7814-4BCF-AB80-0F429C691D9B"
    Protected WithEvents ChkStatus As System.Web.UI.WebControls.CheckBox
    Protected WithEvents dlstEmployerContact As System.Web.UI.WebControls.DataList

    Protected WithEvents RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator2 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator3 As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator4 As System.Web.UI.WebControls.RegularExpressionValidator

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblState As System.Web.UI.WebControls.Label
    Protected WithEvents txtState As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblExt As System.Web.UI.WebControls.Label
    Protected WithEvents txtExtension As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblBestTime As System.Web.UI.WebControls.Label
    Protected WithEvents txtBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox4 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblPinNumber As System.Web.UI.WebControls.Label
    Protected WithEvents txtPinNumber As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblWorkExt As System.Web.UI.WebControls.Label
    Protected WithEvents txtWorkExt As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblWorkBestTime As System.Web.UI.WebControls.Label
    Protected WithEvents txtWorkBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblHomeBestTime As System.Web.UI.WebControls.Label
    Protected WithEvents txtHomeBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtCellBestTime As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtRowIds As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtResourceId As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents Dropdownlist1 As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblAdmissionsRepID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlAdmissionsRepID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblPhoneStatusID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlPhoneStatusID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblNationalityID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlNationalityID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblDriverLicenseState As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCitizenID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlDriverLicenseNumber As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblCountyID As System.Web.UI.WebControls.Label
    Protected WithEvents ddlCountyID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents ddlAddressStateID As System.Web.UI.WebControls.DropDownList
    Protected WithEvents txtPhone1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblType As System.Web.UI.WebControls.Label
    Protected WithEvents ddlPhoneTypeID As System.Web.UI.WebControls.DropDownList
    Protected CampusId As String
    Protected WithEvents lbl2 As System.Web.UI.WebControls.Label
    Protected WithEvents chkForeign As System.Web.UI.WebControls.CheckBox
    Protected WithEvents valCompare As System.Web.UI.WebControls.CompareValidator
    Protected m_context As HttpContext
    Protected strSourceDetails As String

#End Region

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub

    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    'Dim newBox As Messagebox = New Messagebox()
    Private useSaveNext As Boolean = False
    Private pObj As New UserPagePermissionInfo
    Dim userId As String
    Protected moduleId As String
    Protected ResourceID As Integer
    Public strScript1, strScript As String
    Protected strDefaultCountry As String
    Protected isIPEDSApplicable As String

    Private mruProvider As MRURoutines
    Protected LeadId, StudentId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        CampusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ViewState("CampusID") = CampusId

        ResourceID = CInt(HttpContext.Current.Request.Params("resid"))

        ''''' Code Added on 18th August to fix mantis issue id 19555
        ddlCampusId.Attributes.Add("onchange", "verifycampuschange('" + CampusId + "')")
        '''''''''''''''''
        moduleId = HttpContext.Current.Request.Params("Mod").ToString
        'pObj = fac.GetUserResourcePermissions(userId, resourceId2, campusId2)
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceID, CampusId)

        txtUserId.Value = userId
        txtDate.Value = Date.Now.ToShortDateString
        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString

        'Check if this page still exists in the menu while switching campus
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not Page.IsPostBack Then
            ShowHideControls()
            chkIsInDB.Checked = False
            objCommon.SetCaptionsAndColorRequiredFieldsForQuickLeads(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            ViewState("MODE") = "NEW"
            ''''' Code Modified on 18th August to fix mantis issue id 19555

            BuildCampusDdl()
            '''''''''''''''''''''''
            BuildDropDownLists()
            '' jguirado This should resolve the problem of two or more open tab. 
            If ddlCampusId.Items.FindByValue(CampusId).Value = CampusId Then
                ddlCampusId.SelectedValue = CampusId
            Else
                ddlCampusId.SelectedValue = AdvantageSession.UserState.CampusId.ToString()
            End If

            txtCampusId.Value = ddlCampusId.SelectedValue
            ''''''''

            'bind an empty new LeadMasterInfo
            BindLeadMasterDataItemCommand(New LeadMasterInfo)

            txtAssignedDate.SelectedDate = Date.Now.ToShortDateString
            txtDateApplied.SelectedDate = Date.Now.ToShortDateString
            txtModDate.Value = Date.Today.ToString
            Session("ScrollValue") = 0
            Try
                'Code changes by Atul Kamble on 08-Oct-2010
                If AdvantageSession.UserState.IsUserSA Or AdvantageSession.UserState.IsUserSupport Or AdvantageSession.UserState.IsUserSuper Then
                    ddlAdmissionsRep.SelectedIndex = 0
                End If
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlAdmissionsRep.SelectedIndex = 0
            End Try

            Try
                ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText("Unknown"))
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlGender.SelectedIndex = 0
            End Try

            Try
                ddlAddressType.SelectedIndex = ddlAddressType.Items.IndexOf(ddlAddressType.Items.FindByText("Home"))
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlAddressType.SelectedIndex = 0
            End Try

            'Code changes by Atul Kamble on 08-Oct-2010
            Try
                ddlAdmissionsRep.SelectedValue = userId
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlAdmissionsRep.SelectedIndex = 0
            End Try
            'If pObj.HasFull Or pObj.HasAdd Then
            '    btnNew.Enabled = True
            '    btnSave.Enabled = True
            '    btnSaveNext.Enabled = True
            'End If
            InitButtonsForLoad()
        Else
            Session("txtPhone") = txtphone.Text
            objCommon.SetCaptionsAndColorRequiredFieldsForQuickLeads(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), , chkForeignZip.Checked, chkForeignPhone.Checked)
            'Code commented by Atul Kamble on 08-Oct-2010
            'Try
            '    If Session("UserName").ToString() = "sa" Or Session("UserName").ToString().ToLower() = "support" Or Session("UserName").ToString().ToLower() = "super" Then
            '        ddlAdmissionsRep.SelectedIndex = 0
            '    End If
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    ddlAdmissionsRep.SelectedIndex = 0
            'End Try

            'Code commented by Atul Kamble on Nov 01,2010 for Rally ID DE1168
            'Try
            '    ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText("Unknown"))
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    ddlGender.SelectedIndex = 0
            'End Try

            'Try
            '    ddlAddressType.SelectedIndex = ddlAddressType.Items.IndexOf(ddlAddressType.Items.FindByText("Home"))
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    ddlAddressType.SelectedIndex = 0
            'End Try
            'Code commented by Atul Kamble on Nov 01,2010 for Rally ID DE1168
            Session("txtPhone") = txtphone.Text


        End If
        GetInputMaskValue()

        'DE6156 2/27/2012 Janet Robinson comment out ipeds
        'Check if School is using IPEDS
        'IPEDSRequirements()
        If chkForeignZip.Checked = True Then
            txtOtherState.Visible = True
            lblOtherState.Visible = True
            ddlStateID.Enabled = False
            ddlStateID.BackColor = Color.FromName("White")
            ddlStateID.SelectedIndex = 0
        Else
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            ddlStateID.Enabled = True
            'ddlStateID.BackColor = Color.FromName("#ffff99")
        End If

        ''''' Code Commented on 18th August to fix mantis issue id 19555
        ''ddlCampusId.SelectedValue = campusId2
        '''''''''
        'Header1.EnableHistoryButton(False)
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        useSaveNext = Session("useSaveNext")
        If dup.Value = "yes" And useSaveNext = False Then
            AddLeadInfo()
            Exit Sub
        ElseIf dup.Value = "yes" And useSaveNext = True Then
            AddLead()
            Exit Sub
        End If
        BindToolTip()
    End Sub
    Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Function GetInputMaskValue() As String
        Dim facInputMasks As New FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade
        '        Dim correctFormat As Boolean
        Dim strMask As String
        Dim zipMask As String
        '   Dim errorMessage As String
        '   Dim strPhoneReq As String
        '   Dim strZipReq As String
        '   Dim strFaxReq As String
        Dim objCommon As New CommonUtilities
        Dim ssnMAsk As String

        'Get The Input Mask for Phone/Fax and Zip
        strMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Zip)
        ssnMAsk = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.SSN)

        'txtSSN.Mask = Replace(ssnMAsk, "#", "9")
        lblSSN.ToolTip = ssnMAsk

        'Replace The Mask Character from # to 9 as Masked Edit TextBox 
        'accepts only certain characters as mask characters
        If chkForeignPhone.Checked = False Then
            ' txtPhone.Mask = Replace(strMask, "#", "9")
            txtphone.Mask = "(###)-###-####"
            txtphone.DisplayMask = "(###)-###-####"
            txtphone.DisplayPromptChar = ""
        Else
            txtphone.Text = ""
            txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtphone.DisplayMask = ""
            txtphone.DisplayPromptChar = ""
        End If

        If chkForeignZip.Checked = False Then
            ' txtZip.Mask = Replace(zipMask, "#", "9")
            txtzip.Mask = "#####"
            lblZip.ToolTip = zipMask
        Else
            txtzip.Mask = "aaaaaaaaaaaaaaaaaaaa"
            lblZip.ToolTip = ""
        End If

        'Get The RequiredField Value
        'Dim strSSNReq As String
        'strPhoneReq = objCommon.SetRequiredColorMask("Phone")
        'strZipReq = objCommon.SetRequiredColorMask("Zip")
        'strSSNReq = objCommon.SetRequiredColorMask("SSN")

        ''If The Field Is Required Field Then Color The Masked
        ''Edit Control
        'If strSSNReq = "Yes" Then
        '    txtSSN.BackColor = Color.FromName("#ffff99")
        'End If
        'If strPhoneReq = "Yes" Then
        '    txtPhone.BackColor = Color.FromName("#ffff99")
        'End If
        ''If InStr(strZipReq, "Y") >= 1 Then
        'If strZipReq = "Yes" Then
        '    txtZip.BackColor = Color.FromName("#ffff99")
        'End If
        Return ""
    End Function
    Private Function ValidateFieldsWithInputMasks() As String
        Dim facInputMasks As New FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade
        Dim correctFormat As Boolean
        Dim strMask As String
        Dim zipMask As String
        Dim errorMessage As String = ""
        Dim ssnMask As String

        strMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.SSN)
        'Validate the phone field format. If the field is empty we should not apply the mask
        'agaist it.

        If txtSSN.Text <> "" Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(ssnMask, txtSSN.Text)
            If correctFormat = False Then
                errorMessage = "Incorrect format for  ssn field." & vbCr
            End If
        End If

        If txtphone.Text <> "" And chkForeignPhone.Checked = False Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtphone.Text)
            If correctFormat = False Then
                errorMessage &= "Incorrect format for  phone field." & vbCr
            End If
        End If

        'Validate the zip field format. If the field is empty we should not apply the mask
        'against it.
        If txtzip.Text <> "" And chkForeignZip.Checked = False Then
            correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtzip.Text)
            If correctFormat = False Then
                errorMessage &= "Incorrect format for zip field."
            End If
        End If

        Return errorMessage
    End Function
    Private Sub BuildCampusDdl()
        Dim campusGroups As New CampusGroupsFacade
        With ddlCampusId
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            ''Code Modified by Kamalesh Ahuja on August 26, 2010 Mantis Id 19559
            .DataSource = campusGroups.GetCampusesByUser(userId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildDropDownLists()

        'BuildPrefixDDL()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
        'Prefixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlPrefix, AdvantageDropDownListName.Prefixes, CampusId, True, True, String.Empty))
        'Suffixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlSuffix, AdvantageDropDownListName.Suffixes, CampusId, True, True, String.Empty))
        'Country DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlCountry, AdvantageDropDownListName.Countries, CampusId, True, True, String.Empty))
        'Address Type
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressType, AdvantageDropDownListName.Address_Types, CampusId, True, True, String.Empty))
        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlDrivLicStateID, AdvantageDropDownListName.States, CampusId, True, True, String.Empty))
        'Races
        ddlList.Add(New AdvantageDDLDefinition(ddlRace, AdvantageDropDownListName.Races, CampusId, True, True, String.Empty))
        'Genders
        ddlList.Add(New AdvantageDDLDefinition(ddlGender, AdvantageDropDownListName.Genders, CampusId, True, True, String.Empty))
        'Marital Status
        ddlList.Add(New AdvantageDDLDefinition(ddlMaritalStatus, AdvantageDropDownListName.MaritalStatus, CampusId, True, True, String.Empty))
        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlStateID, AdvantageDropDownListName.States, CampusId, True, True, String.Empty))
        'Citizenships
        ddlList.Add(New AdvantageDDLDefinition(ddlCitizen, AdvantageDropDownListName.Citizen, CampusId, True, True, String.Empty))
        'Nationality
        ddlList.Add(New AdvantageDDLDefinition(ddlNationality, AdvantageDropDownListName.Nationality, CampusId, True, True, String.Empty))
        'Address States 
        'ddlList.Add(New AdvantageDDLDefinition(ddlAddrStateId, AdvantageDropDownListName.States, CampusId,True,True,String.Empty))
        'Shifts
        ddlList.Add(New AdvantageDDLDefinition(ddlShiftID, AdvantageDropDownListName.Shifts, CampusId, True, True, String.Empty))
        'County
        ddlList.Add(New AdvantageDDLDefinition(ddlCounty, AdvantageDropDownListName.Counties, CampusId, True, True, String.Empty))
        'Family Income
        ddlList.Add(New AdvantageDDLDefinition(ddlFamilyIncome, AdvantageDropDownListName.FamilyIncome, CampusId, True, True, String.Empty))
        'Previous Education
        ddlList.Add(New AdvantageDDLDefinition(ddlPreviousEducation, AdvantageDropDownListName.EducationLvl, CampusId, True, True, String.Empty))
        'Phone Types
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneType, AdvantageDropDownListName.Phone_Types, CampusId, True, True, String.Empty))
        'Phone Statuses
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneStatus, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Address Status
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressStatus, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Admission Reps
        'ddlList.Add(New AdvantageDDLDefinition(ddlAdmissionsRep, AdvantageDropDownListName.AdmissionsRep, Nothing))

        'Dependency Type
        ddlList.Add(New AdvantageDDLDefinition(ddlDependencyTypeId, AdvantageDropDownListName.DependencyType, CampusId, True, True, String.Empty))

        'Geographic Type
        ddlList.Add(New AdvantageDDLDefinition(ddlGeographicTypeId, AdvantageDropDownListName.GeographicType, CampusId, True, True, String.Empty))

        'Sponsor
        ddlList.Add(New AdvantageDDLDefinition(ddlSponsor, AdvantageDropDownListName.Sponsors, CampusId, True, True, String.Empty))

        'Admin Criteria
        ddlList.Add(New AdvantageDDLDefinition(ddlAdminCriteriaId, AdvantageDropDownListName.AdminCriteriaType, CampusId, True, True, String.Empty))

        'Housing Type
        ddlList.Add(New AdvantageDDLDefinition(ddlHousingId, AdvantageDropDownListName.HousingType, CampusId, True, True, String.Empty))

        'Degree Certificate Seeking Type
        ddlList.Add(New AdvantageDDLDefinition(ddlDegCertSeekingId, AdvantageDropDownListName.DegCertSeekingType, CampusId, True, True, String.Empty))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
        '' --Code Modified to fix issue DE8222
        ''QA: IDTI-Getting an error page when changing the Campus dropdown in the Quick Leads page from "Long Island" to "Select"
        If Not CampusId = Guid.Empty.ToString Then
            BuildStatusDDL("")
            ''''' Code Modified on 18th August to fix mantis issue id 19555
            ''BuildCampusDDL()
            '''''''''''''
            BuildSourceCatagoryDDL()
            BuildProgramGroupDDL()
            BuildLeadGroupsDDL()
            BuildAdmissionRepsDDL()
        End If

    End Sub
    Private Sub BuildLeadGroupsDDL()
        Dim facade As New AdReqsFacade
        With chkLeadGrpId
            .DataTextField = "Descrip"
            .DataValueField = "LeadGrpId"
            .DataSource = facade.GetAllLeadGroups(CampusId)
            .DataBind()
        End With
    End Sub
    Private Sub UpdateLeadGroups(ByVal LeadId As String)
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), chkLeadGrpId.Items.Count)
        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In chkLeadGrpId.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        'resize the array
        If i > 0 Then
            ReDim Preserve selectedDegrees(i - 1)
            'update Selected Jobs
            Dim LeadGrpFacade As New AdReqsFacade
            If LeadGrpFacade.UpdateLeadGroups(LeadId, AdvantageSession.UserState.UserName, selectedDegrees) < 0 Then
                DisplayErrorMessage("A related record exists,you can not perform this operation")
            End If
        End If

    End Sub
    Private Sub BuildPrgVersionDDL()
        ddlPrgVerId.Items.Clear()
        Dim facade As New LeadFacade
        With ddlPrgVerId
            '.DataTextField = "PrgVerDescrip"
            .DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = facade.GetAllProgramVersionByCampusAndUser(ddlProgramID.SelectedValue, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramGroupDDL()
        Dim PrgGrp As New LeadFacade
        With ddlAreaID
            .DataTextField = "PrgGrpDescrip"
            .DataValueField = "PrgGrpID"
            .DataSource = PrgGrp.GetAllProgramsByGroup()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlProgramID
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlPrgVerId
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceCatagoryDDL()
        Dim SourceCatagory As New LeadFacade
        With ddlSourceCategoryId
            .DataTextField = "SourceCatagoryDescrip"
            .DataValueField = "SourceCatagoryId"
            .DataSource = SourceCatagory.GetAllSourceCategoryByCampus(CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSourceTypeId
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSourceAdvertisement
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceTypeDDL(ByVal SourceCatagoryID As String)
        Dim SourceType As New LeadFacade
        ddlSourceTypeId.Items.Clear()
        With ddlSourceTypeId
            .DataTextField = "SourceTypeDescrip"
            .DataValueField = "SourceTypeId"
            .DataSource = SourceType.GetAllSourceTypeByCampus(SourceCatagoryID, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceAdvDDL(ByVal SourceTypeID As String)
        Dim SourceType As New LeadFacade
        ddlSourceAdvertisement.Items.Clear()
        With ddlSourceAdvertisement
            .DataTextField = "SourceAdvDescrip"
            .DataValueField = "SourceAdvId"
            .DataSource = SourceType.GetAllSourceAdvertisementByCampus(SourceTypeID, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramsDDL(ByVal ProgramGrpID As String)
        Dim ProgramTypes As New LeadFacade
        ddlProgramID.Items.Clear()
        With ddlProgramID
            .DataTextField = "ProgDescrip"
            .DataValueField = "ProgId"
            .DataSource = ProgramTypes.GetAllProgramsByCampusAndUser(ProgramGrpID, CampusId, "")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildAdmissionRepsDDL()

        'code modified by balaji on 06/10/2009

        ''When entering a new lead information SA, Front Desk and Director
        '' of Admissions should be able to assign them to admission rep 
        ''Used the available function from AD/LeadMaster1.aspx

        Dim userName As String
        userName = AdvantageSession.UserState.UserName

        Dim LeadAdmissionReps As New LeadFacade
        Dim DirectorAdmissionsorFrontDeskCheck As New UserSecurityFacade

        ''Find if the User is DirectorOfAdmission or FrontDesk
        Dim boolDirOrFDeskCheck As Boolean = DirectorAdmissionsorFrontDeskCheck.IsDirectorOfAdmissionsorFrontDesk(userId)

        ''''' Code changes by kamalesh Ahuja on 23 rd August to fix mantis issue id 19607
        ddlAdmissionsRep.SelectedIndex = -1
        ''''''

        With ddlAdmissionsRep
            .DataTextField = "fullname"
            .DataValueField = "userid"
            If boolDirOrFDeskCheck Then

                .DataSource = LeadAdmissionReps.GetAllAdmissionRepsByCampusUserIdUserRoles(CampusId, userId, userName, boolDirOrFDeskCheck)
            Else
                .DataSource = LeadAdmissionReps.GetAllAdmissionRepsByCampusAndUserId(CampusId, userId)
            End If
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        'Dim LeadAdmissionReps As New LeadFacade
        'With ddlAdmissionsRep
        '    .DataTextField = "fullname"
        '    .DataValueField = "userId"
        '    .DataSource = LeadAdmissionReps.GetAllAdmissionRepsByCampusAndUserId(CampusId, userId)
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", ""))
        '    .SelectedIndex = 0
        'End With
    End Sub
    Private Function ValidateLeadGroups() As Integer
        Dim strGroups As String = String.Empty
        ' Dim dsUseScheduled As DataSet
        'In For Loop Check The Number of Items Selected
        For Each item As ListItem In chkLeadGrpId.Items
            If item.Selected Then
                If strGroups = String.Empty Then
                    strGroups = item.Value.ToString
                Else
                    strGroups &= "," & item.Value.ToString
                End If

            End If
        Next

        If strGroups = String.Empty Then
            Return -1
        ElseIf strGroups.Split(",").Length = 1 Then
            Return 1
        Else
            Return (New StudentEnrollmentFacade).GetSchedLeadGrpCnt_SP(strGroups)
        End If

    End Function
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim LeadMasterUpdate As New LeadFacade
        Dim Result As String
        'Call Update Function in the plEmployerInfoFacade
        Dim errorMessage As String = ""
        Dim strAddressType As String = ""
        Dim intAge As Integer

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If Not txtBirthDate.SelectedDate Is Nothing And IsDate(txtBirthDate.SelectedDate) Then
            If DateDiff(DateInterval.Year, CDate(txtBirthDate.SelectedDate), CDate(txtDate.Value)) < 1 Then
                DisplayErrorMessage("The DOB cannot be greater than or equal to today's date ")
                txtAge.Text = ""
                Exit Sub
            End If

            Dim intConfigAge As Integer = MyAdvAppSettings.AppSettings("StudentAgeLimit")
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year
            Dim dob As Date = txtBirthDate.SelectedDate
            dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If
            If intAge < intConfigAge Then
                DisplayErrorMessage("The minimum age requirement is " & intConfigAge)
                txtAge.Text = ""
                Exit Sub
            Else
                txtAge.Text = intAge
            End If
        End If


        If Not txtChildren.Text = "" Then
            Dim numOfChildren As Integer
            If (Integer.TryParse(txtChildren.Text, numOfChildren)) Then
                If (numOfChildren > 0 AndAlso numOfChildren < 100) Then
                    txtChildren.Text = numOfChildren.ToString()
                Else
                    DisplayErrorMessage("Children value should be a numeric, maximun two digits. ")
                    txtChildren.Text = ""
                    Exit Sub
                End If
            Else
                DisplayErrorMessage("The field children should contain numbers. ")
                txtChildren.Text = ""
                Exit Sub
            End If
        End If

        If Not txtzip.Text = "" Then
            If Not chkForeignZip.Checked And txtzip.Text.Length < 5 Then
                strAddressType &= "Please enter the proper Zip Code" & vbLf
            End If
        End If


        If chkForeignZip.Checked = False Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not ddlStateID.SelectedValue = "" Or Not txtzip.Text = "" Or Not ddlCounty.SelectedValue = "" Then
                If ddlAddressType.SelectedValue = "" Or ddlAddressType.SelectedValue = Guid.Empty.ToString Then
                    strAddressType = "Address type is required" & vbLf
                End If
            End If
        End If

        If chkForeignZip.Checked = True Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not txtOtherState.Text = "" Or Not txtzip.Text = "" Or Not ddlCounty.SelectedValue = "" Then
                If ddlAddressType.SelectedValue = "" Or ddlAddressType.SelectedValue = Guid.Empty.ToString Then
                    strAddressType = "Address type is required" & vbLf
                End If
            End If
        End If

        'Dim strValidateIPEDS As String = ValidateIPEDS()
        'If Not strValidateIPEDS = "" Then
        '    strAddressType &= strValidateIPEDS & vbLf
        'End If

        If Not txtphone.Text = "" Then
            If Not chkForeignPhone.Checked And txtphone.Text.Length < 10 Then
                strAddressType &= "Domestic phone numbers must include the area code and phone number and be 10 characters in length (Phone field)" & vbLf
            End If
        End If

        If Not txtphone.Text = "" Then
            If Not chkForeignPhone.Checked And txtphone.Text.Length < 10 Then
                strAddressType &= "Domestic phone numbers must include the area code and phone number and be 10 characters in length (Phone field)" & vbLf
            End If
        End If

        If Not ddlPhoneType.SelectedValue = "" Then
            If Session("txtPhone") = "" Or Session("txtPhone") Is Nothing Then
                strAddressType &= "Phone is required" & vbLf
            End If
        End If

        If Not strAddressType = "" Then
            DisplayErrorMessage(strAddressType)
            Exit Sub
        End If

        Dim inquiryTime As String = Now.Date + " " + txtInquiryTime.Text
        If Not CommonWebUtilities.IsValidDate(inquiryTime) Then
            DisplayErrorMessage("Enter Proper Inquiry Time Format")
            Exit Sub
        End If

        'Check If Mask is successful only if Foreign Is not checked
        'If chkForeignPhone.Checked = False Or chkForeignZip.Checked = False Then
        '    errorMessage = ValidateFieldsWithInputMasks()
        '    '' New Code Added By Vijay Ramteke On November 02, 2010
        '    If errorMessage <> "" Then
        '        DisplayErrorMessage(errorMessage)
        '        Exit Sub
        '    End If
        '    '' New Code Added By Vijay Ramteke On November 02, 2010
        'Else
        '    errorMessage = ""
        'End If

        'Check for Duplicate Leads
        'Dim facInputMask As New FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade
        'Dim phoneMask As String
        'Dim zipMask As String
        'Dim ssnMask As String
        Dim strSSN As String
        'If txtSSN.Text <> "" Then
        '    ssnMask = facInputMask.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.SSN)
        '    strSSN = facInputMask.RemoveMask(ssnMask, txtSSN.Text)
        'Else
        strSSN = txtSSN.Text
        'End If

        Dim intValidateLeadGroups As Integer

        intValidateLeadGroups = ValidateLeadGroups()
        'If intValidateLeadGroups = -1 Then
        '    DisplayErrorMessage("Please select a lead group")
        '    Exit Sub
        'End If
        If intValidateLeadGroups = -1 Then
            DisplayErrorMessage("Please select a lead group")
            Exit Sub
            '**This validation has been temporarily suspended 4/3/09 DD
        ElseIf intValidateLeadGroups >= 2 Then
            'More that one Student Group with the "Use For Scheduling" setting
            'has been selected.
            DisplayErrorMessage("Please select only one lead Group that is to be used for scheduling")
            Exit Sub
        End If

        Dim winSettings As String = "toolbar=no,status=no,width=550px,height=150px,left=250px,top=200px,modal=yes"
        Dim name As String = "AdmReqSummary1"
        Dim Message, Message1, URL As String
        Dim intDuplicateLeadCount As Integer = 0
        If Not txtBirthDate.SelectedDate Is Nothing Then
            intDuplicateLeadCount = LeadMasterUpdate.CheckDuplicateLeads(txtFirstName.Text, txtLastName.Text, strSSN, txtBirthDate.SelectedDate, txtMiddleName.Text, ddlCampusId.SelectedValue)
        Else
            intDuplicateLeadCount = LeadMasterUpdate.CheckDuplicateLeads(txtFirstName.Text, txtLastName.Text, strSSN, "", txtMiddleName.Text, ddlCampusId.SelectedValue)
        End If
        If intDuplicateLeadCount >= 1 Then
            If Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                Message = "A Lead already exists with same First Name and Last Name."
                Message1 = "Do you want to proceed and add the lead ?"
                URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
                CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                Message = "A Lead already exists with same First Name,Middle Name and Last Name."
                Message1 = "Do you want to proceed and add the lead ?"
                URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
                CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                Message = "A Lead already exists with same First Name,Last Name and SSN."
                Message1 = "Do you want to proceed and add the lead ?"
                URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
                CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                Message = "A Lead already exists with same First Name,Middle Name,Last Name and SSN."
                Message1 = "Do you want to proceed and add the lead ?"
                URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
                CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                Message = "A Lead already exists with same First Name,Middle Name,Last Name and DOB."
                Message1 = "Do you want to proceed and add the lead ?"
                URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
                CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                Message = "A Lead already exists with same First Name,Last Name and DOB."
                Message1 = "Do you want to proceed and add the lead ?"
                URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
                CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And Not txtMiddleName.Text = "" Then
                Message = "A Lead already exists with same First Name,Last Name,MiddleName,SSN and DOB."
                Message1 = "Do you want to proceed and add the lead ?"
                URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
                CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
                Exit Sub
            ElseIf Not txtFirstName.Text = "" And Not txtLastName.Text = "" And Not txtSSN.Text = "" And Not txtBirthDate.SelectedDate Is Nothing And txtMiddleName.Text = "" Then
                Message = "A Lead already exists with same First Name,Last Name,SSN and DOB."
                Message1 = "Do you want to proceed and add the lead ?"
                URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
                CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
                Exit Sub
            End If
        End If

        Dim strPhone As String
        '        Dim strPhone2 As String
        Dim strZip As String

        'phoneMask = facInputMask.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Phone)
        'If txtphone.Text <> "" And chkForeignPhone.Checked = False Then
        '    strPhone = facInputMask.RemoveMask(phoneMask, txtphone.Text)
        'Else
        strPhone = txtphone.Text
        'End If

        'Get Zip
        'If txtZip.Text <> "" And chkForeignZip.Checked = False Then
        '    zipMask = facInputMask.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Zip)
        '    strZip = facInputMask.RemoveMask(zipMask, txtZip.Text)
        'Else
        strZip = txtzip.Text
        'End If

        Dim strMessage As String = ""
        Dim intCheckDuplicatePhone As Integer = 0
        Dim intCheckDuplicateEmail As Integer = 0
        Dim intCheckDuplicateAddress As Integer = 0
        If Not strPhone.ToString.Trim = "" Then
            intCheckDuplicatePhone = (New LeadFacade).CheckDuplicateLeadsWithSamePhoneNumber(CampusId, strPhone, "")
            If intCheckDuplicatePhone >= 1 Then
                strMessage = " A Lead already exists with the same Phone Number"
            End If
        End If
        If Not txtWorkEmail.Text = "" Or Not txtHomeEmail.Text = "" Then
            intCheckDuplicateEmail = (New LeadFacade).CheckDuplicateLeadsWithSameEmail(CampusId, txtWorkEmail.Text, txtHomeEmail.Text)
        End If
        If Not txtAddress1.Text.ToString.Trim = "" Or Not txtAddress2.Text.ToString.Trim = "" Or Not txtCity.Text.Trim = "" Or Not strZip = "" Then
            intCheckDuplicateAddress = (New LeadFacade).CheckDuplicateLeadsWithSameAddress(CampusId, txtAddress1.Text, txtAddress2.Text, txtCity.Text, ddlStateID.Text, strZip)
        End If
        If intCheckDuplicateEmail >= 1 Then
            If intCheckDuplicatePhone >= 1 Then
                strMessage &= ", Email Address"
            Else
                strMessage = " A Lead already exists with the same Email Address"
            End If
        End If
        If intCheckDuplicateAddress >= 1 Then
            If intCheckDuplicateEmail >= 1 Then
                strMessage &= " and Address."
            Else
                If intCheckDuplicatePhone = 0 And intCheckDuplicateEmail = 0 Then
                    strMessage = " A Lead already exists with the same Address."
                End If
            End If
        End If

        If Not strMessage = "" Then
            Message = strMessage
            Message1 = "Do you want to proceed and add the lead ?"
            URL = "DisplayLeadMessage.aspx?Message=" + Message + "&Message1=" + Message1
            CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
            Exit Sub
        End If

        If errorMessage = "" Then
            Result = LeadMasterUpdate.UpdateLeadMaster(BuildLeadMaster(txtLeadMasterID.Value), AdvantageSession.UserState.UserName, "")

            If Result = "" Then
                UpdateLeadGroups(txtLeadMasterID.Value)

                If Not chkIsInDB.Checked Then
                    Result = AddNewTask()
                End If
                '   populate page fields with data just saved in DB
                ' BindLeadMasterData(LeadMasterUpdate.GetLeadsInfo(txtLeadMasterID.Value))

                '   save on syLeadStatusesChanges only if txtLeadStatusId (oldStatus) <> ddlLeadStatus.SelectedValue (newStatus)
                If ddlLeadStatus.SelectedValue <> txtLeadStatusId.Value Then
                    Dim statusChanged As New LeadStatusesInfo
                    With statusChanged
                        .LeadId = txtLeadMasterID.Value
                        .OrigStatusId = IIf(txtLeadStatusId.Value = "", System.Guid.Empty.ToString, txtLeadStatusId.Value)
                        .NewStatusId = ddlLeadStatus.SelectedValue
                        .ModDate = Date.Parse(txtModDate.Value)
                    End With
                    Result = (New LeadStatusChangeFacade).InsertLeadStatusChange(statusChanged, AdvantageSession.UserState.UserName)
                    If Not Result = "" Then
                        'display error inserting into syLeadStatusesChanges table
                        DisplayErrorMessage(Result)
                        Exit Sub
                    End If

                    ''''' Code Modified on 19th August to fix mantis issue id 19555
                    ViewState("CampusID") = Nothing
                    CampusId = Master.CurrentCampusId

                    Try
                        ddlCampusId.SelectedValue = CampusId
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)


                    End Try
                    ''''''''''''''''''''''''''

                    txtLeadStatusId.Value = ddlLeadStatus.SelectedValue
                    BuildStatusDDL(ddlLeadStatus.SelectedValue)
                    ddlLeadStatus.SelectedValue = txtLeadStatusId.Value
                End If


                If useSaveNext Then
                    'AddLeadToMRU()
                    DisplayErrorMessage("The Lead was successfully added")
                    btnNew_Click(Me, New System.EventArgs())
                    chkLeadGrpId.ClearSelection()
                    'Code added by Atul Kamble on Nov 01,2010 for Rally ID DE1165
                    useSaveNext = False
                    Session("useSaveNext") = False
                    ' BuildLeadStateObjectNoRedirect()
                    'Code added by Atul Kamble on Nov 01,2010 for Rally ID DE1165
                    Exit Sub
                    btnNew_Click(Me, New System.EventArgs())
                Else
                    chkIsInDB.Checked = True
                End If
                InitButtonsForEdit()
                If useSaveNext = False Then
                    BuildLeadStateObject()
                End If
                Exit Sub
            Else
                DisplayErrorMessage(Result)
                Exit Sub
            End If
        Else
            DisplayErrorMessageMask(errorMessage)
        End If
    End Sub
    Public Sub BuildLeadStateObjectNoRedirect()
        Dim objStateInfo As New AdvantageStateInfo
        'Dim objGetStudentStatusBar As New AdvantageStateInfo
        Dim strVid As String
        Dim strLeadId As String = ""
        Dim strLeadObjectPointer As String = ""

        strLeadId = txtLeadMasterID.Value
        If Not strLeadId.Trim = "" Then
            objStateInfo = mruProvider.BuildLeadStatusBar(strLeadId)

            'Create a new guid to be associated with the employer pages
            strVid = Guid.NewGuid.ToString

            strLeadObjectPointer = strVid 'Set VID to Lead Object Pointer

            Session("LeadObjectPointer") = strVid ' This step is required, as master page loses values stored in class variables

            If objStateInfo.SystemStatus = 6 Then 'If lead is enrolled set the student pointer
                Session("StudentObjectPointer") = strVid
            Else
                Session("StudentObjectPointer") = ""
            End If

            'Add an entry to AdvantageSessionState for this guid and object
            'load Advantage state
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            state(strVid) = objStateInfo
            'save current State
            CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
            Try
                Dim advResetUserState As New BO.User
                advResetUserState = AdvantageSession.UserState
                With advResetUserState
                    .CampusId = New Guid(objStateInfo.CampusId.ToString)
                End With
                AdvantageSession.UserState = advResetUserState
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New ArgumentException(ex.Message.ToString)
            End Try

            Try
                Dim advResetUserState As New BO.User
                advResetUserState = AdvantageSession.UserState
                With advResetUserState
                    .CampusId = New Guid(objStateInfo.CampusId.ToString)
                End With
                AdvantageSession.UserState = advResetUserState
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New ArgumentException(ex.Message.ToString)
            End Try
            mruProvider.InsertMRU(4, strLeadId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

        End If
    End Sub
    Public Sub BuildLeadStateObject()
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String
        Dim facInputMasks As New FAME.AdvantageV1.BusinessFacade.InputMasksFacade
        Dim strLeadId As String = ""
        Dim strLeadObjectPointer As String = ""

        strLeadId = txtLeadMasterID.Value
        If Not strLeadId.Trim = "" Then
            objStateInfo = mruProvider.BuildLeadStatusBar(strLeadId)

            'Create a new guid to be associated with the employer pages
            strVID = Guid.NewGuid.ToString

            strLeadObjectPointer = strVID 'Set VID to Lead Object Pointer

            Session("LeadObjectPointer") = strVID ' This step is required, as master page loses values stored in class variables

            If objStateInfo.SystemStatus = 6 Then 'If lead is enrolled set the student pointer
                Session("StudentObjectPointer") = strVID
            Else
                Session("StudentObjectPointer") = ""
            End If

            'Add an entry to AdvantageSessionState for this guid and object
            'load Advantage state
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            state(strVID) = objStateInfo
            'save current State
            CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
            'add to the session
            AdvantageSession.MasterLeadId = strLeadId
            AdvantageSession.MasterName = objStateInfo.NameValue

            Try
                Dim advResetUserState As New BO.User
                advResetUserState = AdvantageSession.UserState
                With advResetUserState
                    .CampusId = New Guid(objStateInfo.CampusId.ToString)
                End With
                AdvantageSession.UserState = advResetUserState
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                Throw New ArgumentException(ex.Message.ToString)
            End Try

            mruProvider.InsertMRU(4, strLeadId, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

            AdvantageSession.LeadMRU = Nothing
            'Master.BuildCampusList()
            'CType(FindControlRecursive("CampusSelector"), Telerik.Web.UI.RadComboBox).SelectedValue = AdvantageSession.UserState.CampusId.ToString

            RedirectBasedOnPermission(AdvantageSession.UserState.CampusId.ToString, strVID)
        End If
    End Sub
    Private Sub RedirectBasedOnPermission(ByVal defaultCampusId As String, ByVal strVID As String)
        Dim arrUpp As New ArrayList
        Dim pUrl As String
        Dim fac As New UserSecurityFacade
        arrUpp = fac.GetUserResourcePermissionsForSubModule(HttpContext.Current.Session("UserId"), 395, "Admissions", Master.CurrentCampusId)
        If arrUpp.Count = 0 Then
            'User does not have permission to any resource for this submodule
            'Session("Error") = "The Lead was successfully added, But you do not have permission to any of the pages for existing leads<br> for the campus that you are logged in to."
            'Response.Redirect("../ErrorPage.aspx")
            RadNotification1.Show()
            RadNotification1.Text = "The Lead was successfully added, But you do not have permission to any of the pages for existing leads for the campus that you are logged in to."

        ElseIf fac.DoesUserHasAccessToSubModuleResource(arrUpp, 170) Then
            Response.Redirect("../AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)

        Else
            'redirect to the first page that the user has permission to for the submodule
            pUrl = BuildPartialURL(arrUpp(0))
            Response.Redirect(pUrl & "&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)
        End If
    End Sub
    Private Function AddNewTask() As String
        Dim utInfo As New UserTaskInfo
        Dim rtn As String = String.Empty
        ' determine if this is an update or a new task by looking
        ' at the ViewState("usertaskid") which is set by passing a parameter to
        ' the page.

        utInfo.IsInDB = False
        utInfo.AssignedById = TMCommon.GetCurrentUserId()
        utInfo.EndDate = Utilities.GetAdvantageDBDateTime(Date.Now)
        utInfo.StartDate = Utilities.GetAdvantageDBDateTime(Date.Now)
        utInfo.Message = "Notice : A new Lead was added." & vbLf
        utInfo.Priority = 1
        utInfo.OwnerId = ddlAdmissionsRep.SelectedValue
        utInfo.ReId = txtLeadMasterID.Value
        utInfo.ModUser = TMCommon.GetCurrentUserId()
        utInfo.Status = TaskStatus.Pending
        utInfo.CampGrpID = Master.CurrentCampusId
        'utInfo.Status = ddlLeadStatus.SelectedValue
        ''utInfo.ResultId = txtStuEnrollmentId.Text


        ' Update the task and check the result
        If Not UserTasksFacade.AddTaskFromStatusCode(ddlLeadStatus.SelectedValue, utInfo) Then
            rtn = "Unable to add a reminder to the Task Manager. " & vbLf
        End If
        Return rtn
    End Function

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
            btnSaveNext.Enabled = True
        Else
            btnSave.Enabled = False
            btnSaveNext.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Function BuildLeadMaster(ByVal LeadMasterId As String) As LeadMasterInfo
        Dim LeadMaster As New LeadMasterInfo
        Dim facInputMask As New FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade
        'Dim phoneMask As String
        'Dim zipMask As String
        'Dim ssnMask As String

        With LeadMaster
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get LeadMasterID
            .LeadMasterID = txtLeadMasterID.Value

            'Get LastName
            .LastName = txtLastName.Text

            'Get FirstName
            .FirstName = txtFirstName.Text

            'Get MiddleName
            .MiddleName = txtMiddleName.Text

            'Get StatusId 
            .Status = ddlLeadStatus.SelectedValue

            'Get PrefixId 
            .Prefix = ddlPrefix.SelectedValue

            'Get SuffixId 
            .Suffix = ddlSuffix.SelectedValue

            'Get Title 
            '.Title = ddlTitleId.SelectedValue

            'Get BirthDate
            '.BirthDate = txtBirthDate.SelectedDate
            If txtBirthDate.SelectedDate Is Nothing Then
                .BirthDate = ""
            Else
                .BirthDate = Date.Parse(txtBirthDate.SelectedDate)
            End If

            'Get Age
            ' .Age = txtAge.Text

            'Get sponsor
            .Sponsor = ddlSponsor.SelectedValue

            'Get AdmissionsRep
            .AdmissionsRep = ddlAdmissionsRep.SelectedValue

            'Get Gender
            .Gender = ddlGender.SelectedValue

            'Get Race
            .Race = ddlRace.SelectedValue

            'Get MaritalStatus
            .MaritalStatus = ddlMaritalStatus.SelectedValue

            'Get Children
            .Children = txtChildren.Text

            'Get FamilyIncome
            .FamilyIncome = ddlFamilyIncome.SelectedValue

            'Get Phone
            'phoneMask = facInputMask.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Phone)
            'If txtphone.Text <> "" And chkForeignPhone.Checked = False Then
            '    .Phone = facInputMask.RemoveMask(phoneMask, txtphone.Text)
            'Else
            '.Phone = txtphone.Text
            'End If
            .Phone = Session("txtPhone")

            'Get PhoneType
            .PhoneType = ddlPhoneType.SelectedValue

            'Driver License State
            .DriverLicState = ddlDrivLicStateID.SelectedValue

            'Get HomeEmail
            .HomeEmail = txtHomeEmail.Text

            'Get WorkEmail
            .WorkEmail = txtWorkEmail.Text

            'Get Address1
            .Address1 = txtAddress1.Text

            'Get Address2
            .Address2 = txtAddress2.Text

            'Get City
            .City = txtCity.Text

            'Get State
            .State = ddlStateID.SelectedValue

            'Get Zip
            'If txtzip.Text <> "" And chkForeignZip.Checked = False Then
            '    zipMask = facInputMask.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Zip)
            '    .Zip = facInputMask.RemoveMask(zipMask, txtzip.Text)
            'Else
            .Zip = txtzip.Text
            'End If

            'Get Country 
            .Country = ddlCountry.SelectedValue

            'Get County
            .County = ddlCounty.SelectedValue


            '.SourceDate = txtSourceDate.SelectedDate
            If txtSourceDate.SelectedDate Is Nothing Then
                .SourceDate = ""
            Else
                .SourceDate = Date.Parse(txtSourceDate.SelectedDate)
            End If

            'Get Area,Program and Program version
            .Area = ddlAreaID.SelectedValue
            .ProgramID = ddlProgramID.SelectedValue
            .PrgVerId = ddlPrgVerId.SelectedValue
            .DegCertSeekingId = ddlDegCertSeekingId.SelectedValue

            'Get SourceCategory,Source Type and SourceAdvertisement
            .SourceCategory = ddlSourceCategoryId.SelectedValue
            .SourceType = ddlSourceTypeId.SelectedValue
            .SourceAdvertisement = ddlSourceAdvertisement.SelectedValue

            'Get ExpectedStart
            '.ExpectedStart = txtExpectedStart.SelectedDate
            If txtExpectedStart.SelectedDate Is Nothing Then
                .ExpectedStart = ""
            Else
                .ExpectedStart = Date.Parse(txtExpectedStart.SelectedDate)
            End If

            'Get ShiftID
            .ShiftID = ddlShiftID.SelectedValue

            'Get Nationality
            .Nationality = ddlNationality.SelectedValue

            'Get Citizen
            .Citizen = ddlCitizen.SelectedValue

            'Get SSN 
            'If txtSSN.Text <> "" Then
            '    ssnMask = facInputMask.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.SSN)
            '    .SSN = facInputMask.RemoveMask(ssnMask, txtSSN.Text)
            'Else
            .SSN = txtSSN.Text
            'End If

            'Get DriverLicState 
            .DriverLicState = ddlStateID.SelectedValue

            'Get DriverLicNumber
            .DriverLicNumber = txtDrivLicNumber.Text

            'Get AlienNumber
            .AlienNumber = txtAlienNumber.Text

            'Notes
            .Notes = txtComments.Text

            'Get AddressTypes
            .AddressType = ddlAddressType.SelectedValue

            'Assignment Date
            '.AssignmentDate = txtAssignedDate.SelectedDate
            If txtAssignedDate.SelectedDate Is Nothing Then
                .AssignmentDate = ""
            Else
                .AssignmentDate = Date.Parse(txtAssignedDate.SelectedDate)
            End If

            '.AppliedDate = txtDateApplied.SelectedDate
            If txtDateApplied.SelectedDate Is Nothing Then
                .AppliedDate = ""
            Else
                .AppliedDate = Date.Parse(txtDateApplied.SelectedDate)
            End If

            .PreviousEducation = ddlPreviousEducation.SelectedValue


            .CampusId = ddlCampusId.SelectedValue

            .OtherState = txtOtherState.Text

            If chkForeignPhone.Checked = True Then
                .ForeignPhone = 1
            Else
                .ForeignPhone = 0
            End If

            If chkForeignZip.Checked = True Then
                .ForeignZip = 1
            Else
                .ForeignZip = 0
            End If

            '.LeadGrpId = ddlLeadGrpId.SelectedValue

            .DependencyTypeId = ddlDependencyTypeId.SelectedValue
            .GeographicTypeId = ddlGeographicTypeId.SelectedValue
            .AdminCriteriaId = ddlAdminCriteriaId.SelectedValue
            .HousingTypeId = ddlHousingId.SelectedValue

            Dim ActValue As New AdvantageCommonValues

            If chkForeignZip.Checked = False Then
                If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not ddlStateID.SelectedValue = "" Or Not txtzip.Text = "" Then
                    If ddlAddressStatus.SelectedValue = "" Or ddlAddressStatus.SelectedValue = Guid.Empty.ToString Then
                        .AddressStatus = AdvantageCommonValues.ActiveGuid
                    End If
                End If
            End If

            If chkForeignZip.Checked = True Then
                If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not txtOtherState.Text = "" Or Not txtzip.Text = "" Then
                    If ddlAddressStatus.SelectedValue = "" Or ddlAddressStatus.SelectedValue = Guid.Empty.ToString Then
                        .AddressStatus = AdvantageCommonValues.ActiveGuid
                    End If
                End If
            End If

            If Not txtphone.Text = "" Then
                If ddlPhoneStatus.SelectedValue = "" Or ddlPhoneStatus.SelectedValue = Guid.Empty.ToString Then
                    .PhoneStatus = AdvantageCommonValues.ActiveGuid
                End If
            End If

            .InquiryTime = txtInquiryTime.Text
            .AdvertisementNote = txtAdvertisementNote.Text
        End With
        Return LeadMaster
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'Assign A New Value For Primary Key Column
        txtLeadMasterID.Value = Guid.NewGuid.ToString()

        ''''' Code Modified on 19th August to fix mantis issue id 19555
        'ViewState("CampusID") = Nothing
        'CampusId = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString

        Try
            ddlCampusId.SelectedValue = CampusId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
        '''''''''''''''''''''

        'Create a Empty Object and Initialize the Object
        BindLeadMasterDataItemCommand(New LeadMasterInfo)

        'Reset The Value of chkIsInDb Checkbox To Identify an Insert
        chkIsInDB.Checked = False

        chkLeadGrpId.ClearSelection()

        txtAssignedDate.SelectedDate = Date.Now.ToShortDateString
        Try
            'Code changes by Atul Kamble on 08-Oct-2010
            If AdvantageSession.UserState.IsUserSA Or AdvantageSession.UserState.IsUserSupport Or AdvantageSession.UserState.IsUserSuper Then
                ddlAdmissionsRep.SelectedIndex = 0
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlAdmissionsRep.SelectedIndex = 0
        End Try

        Try
            ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText("Unknown"))
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlGender.SelectedIndex = 0
        End Try

        Try
            ddlAddressType.SelectedIndex = ddlAddressType.Items.IndexOf(ddlAddressType.Items.FindByText("Home"))
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlAddressType.SelectedIndex = 0
        End Try

        Try
            ddlAdmissionsRep.SelectedValue = userId
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlAdmissionsRep.SelectedIndex = 0
        End Try

        'Initialize Buttons
        InitButtonsForLoad()

        'Initialize SourceDate and ExpectedStart Date
        txtSourceDate.Clear()
        txtExpectedStart.Clear()
        txtDateApplied.SelectedDate = Date.Now.ToShortDateString
        txtAdvertisementNote.Text = ""
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
            btnSaveNext.Enabled = True
            btnNew.Enabled = True
        Else
            btnSave.Enabled = False
            btnNew.Enabled = False
        End If
        btnDelete.Enabled = False
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not (txtLeadMasterID.Value = Guid.Empty.ToString) Then
            'instantiate component
            Dim LeadMaster As New LeadFacade

            'Delete The Row Based on EmployerId Date.Parse()
            Dim result As String = LeadMaster.DeleteLeadMaster(txtLeadMasterID.Value, txtModDate.Value)

            If result <> "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                Exit Sub

            Else
                chkIsInDB.Checked = False

                Dim lscfac As New LeadStatusChangeFacade
                lscfac.DeleteLeadStatusChange(txtLeadMasterID.Value)

                'bind an empty new LeadMasterInfo
                BindLeadMasterDataItemCommand(New LeadMasterInfo)

                'initialize buttons
                InitButtonsForLoad()

                'Initialize SourceDate and ExpectedStart Date
                txtSourceDate.Clear()
                txtExpectedStart.Clear()
                txtAdvertisementNote.Text = ""
            End If
        End If
    End Sub
    Private Sub BindLeadMasterDataItemCommand(ByVal LeadMaster As LeadMasterInfo)
        ' BuildDropDownLists()
        ''Bind The EmployerInfo Data From The Database

        Dim facInputMasks As New FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        Dim ssnMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(FAME.AdvantageV1.BusinessFacade.TM.InputMasksFacade.InputMaskItem.SSN)

        With LeadMaster
            'get IsInDB
            chkIsInDB.Checked = .IsInDB

            'get LeadMasterID
            txtLeadMasterID.Value = .LeadMasterID

            'Get LastName
            txtLastName.Text = .LastName

            'Get FirstName
            txtFirstName.Text = .FirstName

            'Get MiddleName
            txtMiddleName.Text = .MiddleName

            'Get BirthDate
            'txtBirthDate.Text = .BirthDate
            Try
                txtBirthDate.SelectedDate = .BirthDate
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtBirthDate.Clear()
            End Try

            'Get Age
            txtAge.Text = ""

            chkForeignPhone.Checked = False
            chkForeignZip.Checked = False

            'Get Phone
            If .ForeignPhone = 0 Then
                txtphone.Mask = "(###)-###-####"
                txtphone.DisplayMask = "(###)-###-####"
                txtphone.DisplayPromptChar = ""
            Else
                txtphone.Text = ""
                txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
                txtphone.DisplayMask = ""
                txtphone.DisplayPromptChar = ""
            End If
            txtphone.Text = .Phone

            'Get HomeEmail
            txtHomeEmail.Text = .HomeEmail

            'Get WorkEmail
            txtWorkEmail.Text = .WorkEmail

            'Get Address1
            txtAddress1.Text = .Address1

            'Get Address2
            txtAddress2.Text = .Address2

            'Get City
            txtCity.Text = .City

            'Get State
            'txtBirthDate.Text = ""

            'Get Zip
            If .ForeignZip = 0 Then
                txtzip.Mask = "#####"
                lblZip.ToolTip = zipMask
                lblOtherState.Visible = False
                txtOtherState.Visible = False
            Else
                txtzip.Mask = "aaaaaaaaaaaaaaaaaaaa"
                lblZip.ToolTip = ""
                lblOtherState.Visible = True
                txtOtherState.Visible = True
            End If
            txtzip.Text = .Zip

            'Get SourceDate 
            'txtSourceDate.SelectedDate = .SourceDate
            Try
                txtSourceDate.SelectedDate = .SourceDate
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtSourceDate.Clear()
            End Try

            'Get ExpectedStart
            'txtExpectedStart.SelectedDate = .ExpectedStart
            Try
                txtExpectedStart.SelectedDate = .ExpectedStart
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtExpectedStart.Clear()
            End Try

            'Assignment Date
            Try
                txtAssignedDate.SelectedDate = .AssignmentDate
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtAssignedDate.Clear()
            End Try

            If Not txtDateApplied.SelectedDate Is Nothing Then
                txtDateApplied.SelectedDate = FormatDateTime(txtDateApplied.SelectedDate, DateFormat.ShortDate)
            End If

            'Get SSN 
            txtSSN.Text = .SSN

            'Get DriverLicNumber
            txtDrivLicNumber.Text = .DriverLicNumber

            'Get AlienNumber
            txtAlienNumber.Text = .AlienNumber

            'Notes
            txtComments.Text = .Notes

            'Status Code
            ' BuildStatusDDL(.StatusCodeId)
            Dim strDefaultStatusCodeId As String = (New LeadFacade).GetNewDefaultLeadStatusCodeId(CampusId, userId)
            ''New Code Added By Atul Kamble On November 02, 2010 ForRally Id DE1201
            ''If Not strDefaultStatusCodeId = "" Then
            Try
                ddlLeadStatus.SelectedValue = strDefaultStatusCodeId.ToString
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlLeadStatus.SelectedValue = .StatusCodeId
            End Try
            ''End If
            ''New Code Added By Atul Kamble On November 02, 2010 ForRally Id DE1201
            txtLeadStatusId.Value = ddlLeadStatus.SelectedValue
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrefix, .Prefix, .PrefixDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSuffix, .Suffix, .SuffixDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdmissionsRep, .AdmissionsRep, .AdmissionRepsDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGender, .Gender, .GenderDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRace, .Race, .EthCodeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlMaritalStatus, .MaritalStatus, .MaritalStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFamilyIncome, .FamilyIncome, .FamilyIncomeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneType, .PhoneType, .PhoneTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus, .Status, .StatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateID, .State, .StateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountry, .Country, .CountryDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCounty, .County, .CountyDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPreviousEducation, .PreviousEducation, .EdLvlDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressStatus, .Status, .StatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlShiftID, .ShiftID, .ShiftDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlNationality, .Nationality, .NationalityDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCitizen, .Citizen, .CitizenDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDrivLicStateID, .DriverLicState, .DriverLicenseStateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressType, .AddressType, .AddressTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus, .PhoneStatus, .PhoneStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressStatus, .AddressStatus, .AddressStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDependencyTypeId, .DependencyTypeId, .DependencyTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGeographicTypeId, .GeographicTypeId, .GeographicTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSponsor, .Sponsor, .SponsorTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdminCriteriaId, .AdminCriteriaId, .AdminCriteriaDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlHousingId, .HousingTypeId, .HousingTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDegCertSeekingId, .DegCertSeekingId, .DegCertSeekingDescrip)

            'Always make sure the default country is selected
            'if the default country is marked as inactive then select the default value "select"
            Try
                ddlCountry.SelectedValue = strDefaultCountry
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlCountry.SelectedIndex = 0
            End Try

            'Get Area
            ddlAreaID.SelectedValue = .Area

            'Get ProgramID
            If Not .Area = "" Then
                BuildProgramsDDL(ddlAreaID.SelectedValue)
                ddlProgramID.SelectedValue = .ProgramID
            Else
                ddlProgramID.SelectedIndex = 0
            End If

            'Get Program Version
            If Not .ProgramID = "" Then
                BuildPrgVersionDDL()
                ddlPrgVerId.SelectedValue = .PrgVerId
            Else
                ddlPrgVerId.SelectedIndex = 0
            End If

            'Get SourceCategory
            ddlSourceCategoryId.SelectedValue = .SourceCategory

            'Get SourceType
            If Not .SourceCategory = "" Then
                BuildSourceTypeDDL(ddlSourceCategoryId.SelectedValue)
                ddlSourceTypeId.SelectedValue = .SourceType
            Else
                ddlSourceTypeId.SelectedIndex = 0
            End If

            'SourceAdvertisement
            If Not .SourceType = "" Then        'Or
                BuildSourceAdvDDL(ddlSourceTypeId.SelectedValue)
                ddlSourceAdvertisement.SelectedValue = .SourceAdvertisement
            Else
                ddlSourceAdvertisement.SelectedIndex = 0
            End If
            txtInquiryTime.Text = .InquiryTime
            txtAdvertisementNote.Text = .AdvertisementNote

            ''Code added by Atul Kamble on Nov 02,2010 for RallyID DE1201
            txtChildren.Text = .Children
            ''Code added by Atul Kamble on Nov 02,2010 for RallyID DE1201
        End With
    End Sub
    'Private Sub txtBirthDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBirthDate.TextChanged
    '    Dim intAge As Integer
    '    If Not txtBirthDate.Text = "" And IsDate(txtBirthDate.Text) Then
    '        If DateDiff(DateInterval.Year, CDate(txtBirthDate.Text), CDate(txtDate.Value)) < 1 Then
    '            DisplayErrorMessage("The minimum age requirement for student is 18 ")
    '            txtAge.Text = ""
    '            Exit Sub
    '        End If
    '        intAge = Now.Year - CDate(txtBirthDate.Text).Year - 1
    '        txtAge.Text = intAge
    '    End If
    'End Sub
    Private Sub txtBirthDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBirthDate.SelectedDateChanged
        ValidateDOB()
    End Sub
    Private Sub ValidateDOB()
        Dim intAge As Integer
        If Not txtBirthDate.SelectedDate Is Nothing And IsDate(txtBirthDate.SelectedDate) Then
            If DateDiff(DateInterval.Year, CDate(txtBirthDate.SelectedDate), CDate(txtDate.Value)) < 1 Then
                DisplayErrorMessage("The minimum age requirement for student is 18 ")
                txtAge.Text = ""
                Exit Sub
            End If
            Dim dob As Date = txtBirthDate.SelectedDate
            dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year - 1
            txtAge.Text = intAge
        End If
    End Sub
    Private Sub txtSourceDate_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSourceDate.SelectedDateChanged
    End Sub
    Private Sub BuildStatusDDL(ByVal leadStatusId As String)
        'Dim Status As New LeadFacade
        Dim facade As New LeadStatusChangeFacade
        With ddlLeadStatus
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            If Not CampusId = Guid.Empty.ToString Then
                .DataSource = facade.GetAvailLeadStatuses("", CampusId, userId) 'Status.GetNewLeadStatus()
                .DataBind()
            End If

            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPhone.CheckedChanged
        If chkForeignPhone.Checked = False Then
            txtphone.Mask = "(###)-###-####"
            txtphone.DisplayMask = "(###)-###-####"
            txtphone.DisplayPromptChar = ""
        Else
            txtphone.Text = ""
            txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtphone.DisplayMask = ""
            txtphone.DisplayPromptChar = ""
        End If
        Dim objCommon As New CommonWebUtilities
        'CommonWebUtilities.SetFocus(Me.Page, txtphone)
        txtphone.Text = ""
        txtphone.Focus()
    End Sub

    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignZip.CheckedChanged
        If chkForeignZip.Checked = True Then
            txtOtherState.Enabled = True
            txtzip.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtzip.Text = String.Empty
            ddlStateID.SelectedIndex = 0
            txtzip.MaxLength = 20
        Else
            txtzip.Mask = "#####"
            txtzip.Text = String.Empty
            txtzip.MaxLength = 5
        End If
        Dim objCommon As New CommonWebUtilities
        'CommonWebUtilities.SetFocus(Me.Page, txtAddress1)
        txtAddress1.Focus()
    End Sub
    Private Sub ddlStateId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStateID.SelectedIndexChanged
    End Sub
    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOtherState.TextChanged
        Dim objCommon As New CommonWebUtilities
        'CommonWebUtilities.SetFocus(Me.Page, txtzip)
        txtzip.Focus()
    End Sub
    Private Sub txtCity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCity.TextChanged
        If chkForeignZip.Checked = True Then
            Dim objCommon As New CommonWebUtilities
            'CommonWebUtilities.SetFocus(Me.Page, txtOtherState)
            txtOtherState.Focus()
        End If
    End Sub
    Private Sub txtBirthDate_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBirthDate.PreRender
    End Sub
    Protected Sub btnSaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNext.Click
        useSaveNext = True
        Session("useSaveNext") = True
        btnSave_Click(Me, New System.EventArgs())
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.

        If Not Trim(Request.Form("scrollposition")) = "" Then
            Dim i As Integer = CInt(Trim(Request.Form("scrollposition")))
            Session("ScrollValue") = i
        End If

        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)

    End Sub
    Private Sub ddlSourceCategoryId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSourceCategoryId.SelectedIndexChanged
        If Not ddlSourceCategoryId.SelectedValue = "" Then
            BuildSourceTypeDDL(ddlSourceCategoryId.SelectedValue)
            ddlSourceAdvertisement.Items.Clear()
            With ddlSourceAdvertisement
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            ddlSourceTypeId.Items.Clear()
            With ddlSourceTypeId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            ddlSourceAdvertisement.Items.Clear()
            With ddlSourceAdvertisement
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Private Sub ddlSourceTypeId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSourceTypeId.SelectedIndexChanged
        If Not ddlSourceTypeId.SelectedIndex = 0 Then
            BuildSourceAdvDDL(ddlSourceTypeId.SelectedValue)
        Else
            With ddlSourceAdvertisement
                .Items.Clear()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Private Sub ddlAreaID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAreaID.SelectedIndexChanged
        If Not ddlAreaID.SelectedValue = "" Then
            BuildProgramsDDL(ddlAreaID.SelectedValue)
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            ddlProgramID.Items.Clear()
            With ddlProgramID
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Protected Sub ddlProgramID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProgramID.SelectedIndexChanged
        If Not ddlProgramID.SelectedIndex = 0 Then
            BuildPrgVersionDDL()
        Else
            With ddlPrgVerId
                .Items.Clear()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Private Sub NavigateToInfo()
        Dim StudentName As New StudentSearchFacade
        Dim defaultCampusId As String
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String
        Dim state As AdvantageSessionState
        Dim fac As New UserSecurityFacade

        Dim pURL As String

        'defaultCampusId = HttpContext.Current.Request.Params("cmpid")
        'defaultCampusId = XmlConvert.ToGuid(defaultCampusId).ToString
        defaultCampusId = CampusId

        'This is used to load lead into the MRU
        Session("SEARCH") = 1

        'Set relevant properties on the state object
        objStateInfo.LeadId = txtLeadMasterID.Value
        objStateInfo.NameCaption = "Lead : "
        objStateInfo.NameValue = StudentName.GetLeadNameByID(txtLeadMasterID.Value)

        'Create a new guid to be associated with the lead pages
        strVID = Guid.NewGuid.ToString

        'Add an entry to AdvantageSessionState for this guid and object load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo

        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        Dim arrUPP As ArrayList
        arrUPP = fac.GetUserResourcePermissionsForSubModule(HttpContext.Current.Session("UserId"), 395, "Admissions", Master.CurrentCampusId)
        If arrUPP.Count = 0 Then
            'User does not have permission to any resource for this submodule
            'Session("Error") = "The Lead was successfully added, But you do not have permission to any of the pages for existing leads<br> for the campus that you are logged in to."
            'Response.Redirect("../ErrorPage.aspx")
            RadNotification1.Show()
            RadNotification1.Text = "The Lead was successfully added, But you do not have permission to any of the pages for existing leads for the campus that you are logged in to."


        ElseIf fac.DoesUserHasAccessToSubModuleResource(arrUPP, 170) Then
            Response.Redirect("../AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)

        Else
            'redirect to the first page that the user has permission to for the submodule
            pURL = BuildPartialURL(arrUPP(0))
            Response.Redirect(pURL & "&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)
        End If
    End Sub
    Private Function BuildPartialURL(ByVal uppInfo As UserPagePermissionInfo) As String
        Return uppInfo.URL & "?resid=" & uppInfo.ResourceId
    End Function

    Private Sub AddLead()
        Dim LeadMasterUpdate As New LeadFacade
        Dim Result As String
        Result = LeadMasterUpdate.UpdateLeadMaster(BuildLeadMaster(txtLeadMasterID.Value), AdvantageSession.UserState.UserName.ToString(), "")
        If Result = "" Then
            dup.Value = ""
            If Not chkIsInDB.Checked Then
                Result = AddNewTask()
            End If


            UpdateLeadGroups(txtLeadMasterID.Value)
            '   populate page fields with data just saved in DB
            ' BindLeadMasterData(LeadMasterUpdate.GetLeadsInfo(txtLeadMasterID.Value))

            '   save on syLeadStatusesChanges only if txtLeadStatusId (oldStatus) <> ddlLeadStatus.SelectedValue (newStatus)
            If ddlLeadStatus.SelectedValue <> txtLeadStatusId.Value Then
                Dim statusChanged As New LeadStatusesInfo
                With statusChanged
                    .LeadId = txtLeadMasterID.Value
                    .OrigStatusId = IIf(txtLeadStatusId.Value = "", System.Guid.Empty.ToString, txtLeadStatusId.Value)
                    .NewStatusId = ddlLeadStatus.SelectedValue
                    .ModDate = Date.Parse(txtModDate.Value)
                End With
                Result = (New LeadStatusChangeFacade).InsertLeadStatusChange(statusChanged, AdvantageSession.UserState.UserName.ToString())
                If Not Result = "" Then
                    'display error inserting into syLeadStatusesChanges table
                    DisplayErrorMessage(Result)
                    Exit Sub
                End If
                txtLeadStatusId.Value = ddlLeadStatus.SelectedValue
                BuildStatusDDL(ddlLeadStatus.SelectedValue)
                ddlLeadStatus.SelectedValue = txtLeadStatusId.Value
            End If
            If useSaveNext Then
                DisplayErrorMessage("The lead was added")
                Session("useSaveNext") = False
                useSaveNext = False
                btnNew_Click(Me, New System.EventArgs())
                Exit Sub
            Else
                chkIsInDB.Checked = True
            End If
            InitButtonsForEdit()
            If useSaveNext = False Then
                NavigateToInfo()
            End If
            Exit Sub
        Else
            DisplayErrorMessage(Result)
            Exit Sub
        End If
    End Sub
    Private Sub AddLeadInfo()
        Dim LeadMasterUpdate As New LeadFacade
        Dim Result As String
        Result = LeadMasterUpdate.UpdateLeadMaster(BuildLeadMaster(txtLeadMasterID.Value), AdvantageSession.UserState.UserName.ToString(), "")
        If Result = "" Then
            dup.Value = ""
            If Not chkIsInDB.Checked Then
                Result = AddNewTask()
            End If


            UpdateLeadGroups(txtLeadMasterID.Value)

            '   populate page fields with data just saved in DB
            ' BindLeadMasterData(LeadMasterUpdate.GetLeadsInfo(txtLeadMasterID.Value))

            '   save on syLeadStatusesChanges only if txtLeadStatusId (oldStatus) <> ddlLeadStatus.SelectedValue (newStatus)
            If ddlLeadStatus.SelectedValue <> txtLeadStatusId.Value Then
                Dim statusChanged As New LeadStatusesInfo
                With statusChanged
                    .LeadId = txtLeadMasterID.Value
                    .OrigStatusId = IIf(txtLeadStatusId.Value = "", System.Guid.Empty.ToString, txtLeadStatusId.Value)
                    .NewStatusId = ddlLeadStatus.SelectedValue
                    .ModDate = Date.Parse(txtModDate.Value)
                End With
                Result = (New LeadStatusChangeFacade).InsertLeadStatusChange(statusChanged, AdvantageSession.UserState.UserName.ToString())
                If Not Result = "" Then
                    'display error inserting into syLeadStatusesChanges table
                    DisplayErrorMessage(Result)
                    Exit Sub
                End If
                txtLeadStatusId.Value = ddlLeadStatus.SelectedValue
                BuildStatusDDL(ddlLeadStatus.SelectedValue)
                ddlLeadStatus.SelectedValue = txtLeadStatusId.Value
            End If
            If useSaveNext Then
                DisplayErrorMessage("The lead was added")
                Session("useSaveNext") = False
                useSaveNext = False
                btnNew_Click(Me, New System.EventArgs())
            Else
                chkIsInDB.Checked = True
            End If
            InitButtonsForEdit()
            If useSaveNext = False Then
                BuildLeadStateObject()
            End If
            Exit Sub
        Else
            DisplayErrorMessage(Result)
            Exit Sub
        End If
    End Sub

    Private Sub ShowHideControls()
        Dim lstFields As String = ""
        Dim lstSections As String = ""
        Dim ds As DataSet
        ds = (New LeadFacade).GetQuickLeadFields
        For Each dr As DataRow In ds.Tables(0).Rows
            lstFields &= dr("FldName")
            lstFields &= ","

            lstSections &= dr("SectionName")
            lstSections &= ","
        Next
        lstFields = Mid(lstFields, 1, InStrRev(lstFields, ",") - 1)
        lstSections = Mid(lstSections, 1, InStrRev(lstSections, ",") - 1)

        MakeControlVisible(lstFields, "CampusId", ddlCampusId, lblCampusId)
        MakeControlVisible(lstFields, "LastName", txtLastName, lblLastName)
        MakeControlVisible(lstFields, "FirstName", txtFirstName, lblFirstName)
        MakeControlVisible(lstFields, "SSN", txtSSN, lblSSN)
        MakeControlVisible(lstFields, "BirthDate", txtBirthDate, lblBirthDate)
        MakeControlVisible(lstFields, "Comments", txtComments, lblComments)
        MakeControlVisible(lstFields, "MiddleName", txtMiddleName, lblMiddleName)
        MakeControlVisible(lstFields, "Age", txtAge, lblAge)
        MakeControlVisible(lstFields, "LeadStatus", ddlLeadStatus, lblLeadStatus)
        MakeControlVisible(lstFields, "Prefix", ddlPrefix, lblPrefix)
        MakeControlVisible(lstFields, "Suffix", ddlSuffix, lblSuffix)
        MakeControlVisible(lstFields, "Sponsor", ddlSponsor, lblSponsor)
        MakeControlVisible(lstFields, "AdmissionsRep", ddlAdmissionsRep, lblAdmissionsRep)
        MakeControlVisible(lstFields, "Gender", ddlGender, lblGender)
        MakeControlVisible(lstFields, "Race", ddlRace, lblRace)
        MakeControlVisible(lstFields, "MaritalStatus", ddlMaritalStatus, lblMaritalStatus)
        MakeControlVisible(lstFields, "FamilyIncome", ddlFamilyIncome, lblFamilyIncome)
        MakeControlVisible(lstFields, "Children", txtChildren, lblChildren)

        MakeControlVisible(lstFields, "AssignedDate", txtAssignedDate, lblAssignedDate)
        MakeControlVisible(lstFields, "PreviousEducation", ddlPreviousEducation, lblPreviousEducation)
        MakeControlVisible(lstFields, "HousingId", ddlHousingId, lblHousingId)
        MakeControlVisible(lstFields, "admincriteriaid", ddlAdminCriteriaId, lblAdminCriteriaId)
        MakeControlVisible(lstFields, "dateapplied", txtDateApplied, lblDateApplied)
        MakeControlVisible(lstFields, "dependencytypeId", ddlDependencyTypeId, lblDependencyTypeId)
        MakeControlVisible(lstFields, "geographictypeId", ddlGeographicTypeId, lblGeographicTypeId)
        ' DE1170 Janet Robinson 5/5/2011
        MakeControlVisible(lstFields, "DegCertSeekingID", ddlDegCertSeekingId, lblDegCertSeekingId)

        MakePanelsVisible(lstSections, "Add Personal Information", pnlPersonalInfo)
        MakePanelsVisible(lstSections, "Add Phone and Email Information", pnlPhoneInfo)
        MakePanelsVisible(lstSections, "Add Address Information", pnlAddressInfo)
        MakePanelsVisible(lstSections, "Add Interested In Information", pnlInterestedInfo)
        MakePanelsVisible(lstSections, "Add Source Information", pnlSourceInfo)

    End Sub
    Private Sub MakeControlVisible(ByVal lstFields As String,
                                   ByVal FieldName As String,
                                   ByVal ctl As Control,
                                   ByVal lbl As Label)
        If InStr(lstFields.Trim.ToLower, FieldName.Trim.ToLower) >= 1 Then
            ctl.Visible = True
            lbl.Visible = True
        Else
            ctl.Visible = False
            lbl.Visible = False

        End If
        If FieldName.ToString.ToLower = "comments" And InStr(lstFields.Trim.ToLower, FieldName.Trim.ToLower) >= 1 Then
            pnlComments.Visible = True
            ctl.Visible = True
            lbl.Visible = True
        End If
    End Sub
    Private Sub MakePanelsVisible(ByVal lstFields As String,
                                   ByVal FieldName As String,
                                   ByVal pnl As Panel)
        If InStr(lstFields.Trim.ToLower, FieldName.Trim.ToLower) >= 1 Then
            pnl.Visible = True
        Else
            pnl.Visible = False
        End If
    End Sub


    Protected Sub ddlCampusId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCampusId.SelectedIndexChanged
        ''''' Code Modified on 18th August to fix mantis issue id 19555
        ViewState("CampusID") = ddlCampusId.SelectedValue
        CampusId = ddlCampusId.SelectedValue
        ''''''''
        Dim userState As BO.User = AdvantageSession.UserState
        userState.CampusId = Guid.Parse(CampusId)
        AdvantageSession.UserState = userState

        Me.Master.CurrentCampusId = CampusId

        BuildDropDownLists()
    End Sub



End Class
