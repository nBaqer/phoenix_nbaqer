﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="MyLeadManagement.aspx.vb" Inherits="AD_MyLeadManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <style type="text/css">
        .main {
            margin-left: 321px;
             min-width: 900px;
            /*height: 900px;*/
            /*background: #9c9;*/
             /*border:  #94C0D2 solid 1px;*/
        }

        .sidebar {
            float: left;
            width: 320px;
            /*height: 900px;*/
            /*background: #c9c;*/
            /*border: #94C0D2 solid 1px;*/
         }

        .detailC1 {
            font-weight: bold;

        }

        .detailRAlt {
            background-color: ghostwhite;
        }

        #UnAssignedWidget > svg > g > g {
            cursor: pointer;

        }

          #MyLeadWidget > svg > g > g {
            cursor: pointer;

        }

      


    </style>
    <div id="myLeadManagementPage" style="height: 100%; overflow: auto">
        <aside class="sidebar" id="widgetZone">
            <span class="k-toolbar" ><b>Filters:</b></span>
            <table id="filterCriterias" style="margin: 5px;">
                 <tr>
                    <td style="font-size: 12px; color: grey; text-align: center">
                     <span class="k-toolbar">  No Assigned Leads</span>
                        
                    </td>
                </tr>
             <%--   <tr>
                    <td >
                        <div id="UnAssignedWidget"></div>
                        
                    </td>
                </tr>--%>
                 <tr>
                    <td style="font-size: 12px; color: grey; text-align: center">
                     <span class="k-toolbar">  My Assigned Leads</span>
                        
                    </td>
                <tr>
                    <td>
                        <div id="MyLeadWidget"></div>
                    </td>
                </tr>
            </table>
        </aside>
        <section class="main" >
            <article>
               <span class="k-toolbar" ><b>Lead Information:</b></span>
                 <div id="leadGrid" style="margin: 5px;  overflow: auto"></div>
                 <div id="WindowsDetails" style="display: none;">
                     <span class="k-toolbar"><span style="margin-left: 5px"> Lead Details:</span></span>
                     <table id="datailDataTable"></table>
                  </div>
                 <div id="WindowsDuplicate" style="display: none">
                     <span class="k-toolbar"> Lead Possibles Duplicates:</span>
                      <div id="duplicatesLeadGrid"></div>
                 </div>
            </article>
        </section>
       
    </div>
    <script src="../Kendo/js/jszip.min.js"></script>
    <script src="../Scripts/Advantage.Client.AD.js"></script>
    <script>

        $(document).ready(function () {
            var manager = new AD.MyLeadManage();

        });

    </script>

</asp:Content>

