﻿Imports System.Diagnostics
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports FAME.Advantage.Common
Imports Advantage.Business.Logic.Layer

Partial Class LeadEducation
    Inherits BasePage

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents chkStatus As CheckBox
    Protected WithEvents chkSchool As CheckBox
    Protected WithEvents txtCode As TextBox



    Protected WithEvents img3 As HtmlImage

    Protected WithEvents btnhistory As Button

    Protected strFrom As String
    Protected WithEvents form2 As HtmlForm

    'For Test Purpose
    Protected studentId, leadId As String '= "DCAF913F-F4B6-4ED3-B8E1-46C9CDFEAB69"
    Protected WithEvents ddlStateI As TextBox
    Protected moduleId As String
    Protected resourceId As Integer

    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Protected bindFacade As New LeadNotesFacade
    Dim userId As String

    Private mruProvider As MRURoutines
    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    Protected myAdvAppSettings As AdvAppSettings
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(sender As Object, e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function GetLeadFromStateObject(ByVal paramResourceId As Integer) As StudentMRU

        Dim objStudentState As New StudentMRU

        Try


            MyBase.GlobalSearchHandler(1)

            boolSwitchCampus = Master.Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                studentId = Guid.Empty.ToString()
            Else
                studentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                leadId = Guid.Empty.ToString()
            Else
                leadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(studentId)
                .LeadId = New Guid(leadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.Master.ShowHideStatusBarControl(True)

            Master.Master.PageObjectId = leadId
            Master.Master.PageResourceId = Request.QueryString("resid")
            Master.Master.setHiddenControlForAudit()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region
    Private Sub Page_Load(sender As Object, e As EventArgs) Handles MyBase.Load



        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        Dim objCommon As New CommonUtilities
        Dim sdfControls As New SDFComponent

        Dim advantageUserState As User = AdvantageSession.UserState

        ' Dim intReqsByLeadGrpExists As Integer

        'Header1.EnableHistoryButton(False)

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = advantageUserState.UserId.ToString 'XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        campusId = advantageUserState.CampusId.ToString 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        moduleId = HttpContext.Current.Request.Params("Mod").ToString

        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If

        'Get StudentId and LeadId
        Dim objStudentState As StudentMRU
        objStudentState = GetLeadFromStateObject(145) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            studentId = .StudentId.ToString
            leadId = .LeadId.ToString

        End With

        txtLeadID.Text = leadId

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            ClearControls()
            txtResourceId.Text = resourceId

            Dim content As ContentPlaceHolder = CType(DirectCast(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder)
            objCommon.SetCaptionsAndColorRequiredFields(content)


            'objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            ddlEducationInstId.BackColor = Color.White
            'Disable the new and delete buttons
            ViewState("MODE") = "NEW"

            'By Default Set InstType To College
            Dim insttype = "College"

            'Bind The DropDownList
            BuildDropDownLists()

            'Build The DataList
            BindDataList(txtLeadID.Text, insttype)

            'Assign Value For Primary Key
            txtPKID.Text = Guid.NewGuid.ToString

            ChkIsInDB.Checked = False

            lblOther.Text = "New College Name"
            InitButtonsForLoad()

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""

        Else
            'objCommon.PageSetup(Form1, "EDIT")
            'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            'objCommon.SetCaptionsAndColorRequiredFields(Form1)
            Dim content As ContentPlaceHolder = CType(DirectCast(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder)
            objCommon.SetCaptionsAndColorRequiredFields(content)

            'objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            If ddlSchoolType.SelectedItem.Value = "Schools" Then
                Me.lblEducationInstId.Text = "School Name"
                Me.lblOther.Text = "New School Name"
            Else
                Me.lblEducationInstId.Text = "College Name"
                Me.lblOther.Text = "New College Name"
            End If


        End If

        ddlEducationInstId.BackColor = Color.White

        'Check If any UDF exists for this resource
        Dim intSdfExists As Integer = sdfControls.GetSDFExists(resourceId, moduleId)
        If intSdfExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Trim(txtPKID.Text) <> "" Then
            sdfControls.GenerateControlsEdit(pnlSDF, resourceId, txtPKID.Text, moduleId)
        Else
            sdfControls.GenerateControlsNew(pnlSDF, resourceId, moduleId)
        End If

        'Code to disable all buttons if lead was already enrolled
        'to prevent data being changed after lead was enrolled
        Dim chkLeadWasEnrolled As Boolean
        Dim leadFacade As New LeadEnrollmentFacade
        chkLeadWasEnrolled = leadFacade.CheckIfLeadWasEnrolled(leadId)
        If chkLeadWasEnrolled = True Then
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
            btnNew.ToolTip = "Cannot add education information as the lead was already enrolled"
            btnSave.ToolTip = "Cannot modify education information as the lead was already enrolled"
            btnDelete.ToolTip = "Cannot delete education information as the lead was already enrolled"
        Else
            btnSave.ToolTip = ""
            btnDelete.ToolTip = ""
            btnNew.ToolTip = ""
        End If
    End Sub

    Private Sub ClearControls()
        txtAddress1.Text = ""
        txtAddress2.Text = ""
        txtComments.Text = ""
        txtFinalGrade.Text = ""
        txtGraduatedDate.SelectedDate = Nothing
        txtMajor.Text = ""
        txtOther.Text = ""
        txtStateId.Text = ""
        txtZip.Text = ""
        txtcity.Text = ""
    End Sub

    Private Sub BuildDropDownLists()
        'By Default Set InstType To College
        Dim insttype = "College"
        Dim schoolItem As String
        Dim ddlList = New List(Of AdvantageDDLDefinition)()
        ddlList.Add(New AdvantageDDLDefinition(ddlCertificateId, AdvantageDropDownListName.Degrees, campusId, True, True))
        schoolItem = ddlSchoolType.SelectedItem.Value
        'Select Case SchoolItem
        '    Case "College"
        '        ddlList.Add(New AdvantageDDLDefinition(ddlEducationInstId, AdvantageDropDownListName.Colleges, Nothing, True, False, String.Empty))
        '    Case "Schools"
        '        ddlList.Add(New AdvantageDDLDefinition(ddlEducationInstId, AdvantageDropDownListName.HighSchools, Nothing, True, False, String.Empty))
        'End Select
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
        BuildSchoolsDdl(insttype, "")
    End Sub
    Private Sub BuildSchoolsDdl(ByVal schoolType As String, ByVal leadId As String)
        'Bind the Colleges/Schools DropDownList
        'Based on ddlSchoolType Selection and LeadId
        Dim schools As New PlacementFacade
        With ddlEducationInstId
            .DataTextField = "CollegeName"
            .DataValueField = "CollegeId"
            .DataSource = schools.GetAllSchools(schoolType, campusId, leadId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .Items.Insert(1, New ListItem("Add " & schoolType & " not listed", "{BFB523E7-D0F8-4BAF-AD42-040051560737}"))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BindAddress(addressInfo As PlacementInfo)
        'Get The Address Information Based On The SchoolType
        'Selection
        With addressInfo
            txtAddress1.Text = .Address1
            txtAddress2.Text = .Address2
            txtcity.Text = .City
            txtZip.Text = .Zip
            txtStateId.Text = .State
        End With
    End Sub
    Private Sub ClearForm()
        txtAddress1.Text = ""
        txtAddress2.Text = ""
        txtcity.Text = ""
        txtZip.Text = ""
        txtStateId.Text = ""
        txtOther.Text = ""
    End Sub
    Private Sub DisableForm()
        txtAddress1.Enabled = False
        txtAddress2.Enabled = False
        txtcity.Enabled = False
        txtZip.Enabled = False
        txtStateId.Enabled = False
    End Sub
    Private Sub EnableForm()
        txtAddress1.Enabled = True
        txtAddress2.Enabled = True
        txtcity.Enabled = True
        txtZip.Enabled = True
        txtStateId.Enabled = True
    End Sub
    Private Sub BindStudentInfo(addressInfo As PlacementInfo)
        'Get The StudentInfo 
        With addressInfo
            'txtGraduatedDate.SelectedDate = .GraduationDate
            Try
                txtGraduatedDate.SelectedDate = .GraduationDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtGraduatedDate.Clear()
            End Try
            txtFinalGrade.Text = .FinalGrade
            txtComments.Text = .Comments
            txtPKID.Text = .StEmploymentId
            txtAddress1.Text = ""
            txtAddress2.Text = ""
            txtcity.Text = ""
            txtZip.Text = ""
            txtStateId.Text = ""
            ddlEducationInstId.SelectedIndex = 0
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCertificateId, .Certificate, .CertificateDescrip)
        End With
    End Sub
    Private Sub BindStudentInfoExist(addressInfo As PlacementInfo)
        'Bind The StudentInfo Data From The Database
        With addressInfo
            If Not String.IsNullOrEmpty(.GraduationDate) Then
                txtGraduatedDate.SelectedDate = .GraduationDate
            End If

            txtFinalGrade.Text = .FinalGrade
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCertificateId, .Certificate, .CertificateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlEducationInstId, .EducationInstId, .EducationInst)
            txtComments.Text = .Comments
            txtMajor.Text = .Major
            txtModDate.Text = .ModDate
        End With
    End Sub
    Private Sub GetAddress(collegeid As String, schoolType As String)
        'Get Address Based On SchoolType and CollegeId
        Dim addressinfo As New PlacementFacade
        BindAddress(addressinfo.GetAddressBySchool(collegeid, schoolType))
    End Sub
    Private Sub DdlEducationInstIdSelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlEducationInstId.SelectedIndexChanged
        'Get Address Info Based On SchoolName/CollegeName
        Dim strSelectedValue As String
        strSelectedValue = ddlEducationInstId.SelectedItem.Value
        ClearForm()
        If strSelectedValue = "{BFB523E7-D0F8-4BAF-AD42-040051560737}" Then
            txtOther.Enabled = True
            lblOther.Enabled = True
            DisableForm()
            ChkIsInDB.Checked = False
            Exit Sub
        Else
            txtOther.Enabled = False
            lblOther.Enabled = False
            EnableForm()
        End If

        If ddlEducationInstId.SelectedValue = Guid.Empty.ToString Or ddlEducationInstId.SelectedValue = "" Then
            Exit Sub
        End If
        Select Case lblEducationInstId.Text
            Case "College Name"
                GetAddress(strSelectedValue, "College")
            Case "School Name"
                GetAddress(strSelectedValue, "Schools")
            Case Else
                GetAddress(strSelectedValue, "College")
        End Select

    End Sub
    Private Sub BtnSaveClick(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim studentEducation As New LeadFacade
        Dim result As String
        Dim intCheckDuplicates As Integer
        Dim chkStudent As New PlacementFacade

        Dim leadMasterUpdate As New LeadFacade
        If myAdvAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "no" Then
            If leadMasterUpdate.CheckLeadUser(userId, txtLeadID.Text) = 0 Then
                'DisplayErrorMessage("You do not have rights to edit this lead")
                DisplayRADAlert(CallbackType.Postback, "Error1", "You do not have rights to edit this lead", "Save Error")
                Exit Sub
            End If
        End If
        ''''''''''''''''''

        'if the selected school/college is 'other' dont check
        If ddlEducationInstId.SelectedItem.Value <> "{BFB523E7-D0F8-4BAF-AD42-040051560737}" Then
            'Check If School Is Active
            Dim strCheckStatus As String
            strCheckStatus = chkStudent.CheckStudentEducation(ddlEducationInstId.SelectedValue, ddlSchoolType.SelectedValue)
            If strCheckStatus = "Inactive" Then
                'DisplayErrorMessage("The selected college/school is currently inactive")
                DisplayRADAlert(CallbackType.Postback, "Error2", "The selected college/school is currently inactive", "Save Error")
                Exit Sub
            End If

            Dim strDate As String = String.Empty
            If Not (txtGraduatedDate.SelectedDate Is Nothing) Then
                strDate = txtGraduatedDate.SelectedDate.ToString
            End If
            If ChkIsInDB.Checked = False Then
                intCheckDuplicates = studentEducation.CheckDuplicates(ddlEducationInstId.SelectedValue, txtLeadID.Text, strDate, ddlCertificateId.SelectedValue)
                If intCheckDuplicates >= 1 Then
                    'DisplayErrorMessage("The Education information already exists")
                    DisplayRADAlert(CallbackType.Postback, "Error3", "The Education information already exists", "Save Error")
                    Exit Sub
                End If
            End If
        Else
            'Assign Value For Primary Key
            txtPKID.Text = Guid.NewGuid.ToString
        End If

        'Call Update Function in the plEmployerInfoFacade
        result = studentEducation.UpdateStudentEducation(BuildStudentEducation(txtLeadID.Text), AdvantageSession.UserState.UserName, txtMajor.Text)

        ''  If DML is not successful then Prompt Error Message
        If Not result = "" Then
            'DisplayErrorMessage(Result)
            DisplayRADAlert(CallbackType.Postback, "Error3", result, "Save Error")
            Exit Sub
        End If

        'Reset The Checked Property Of CheckBox To True
        ChkIsInDB.Checked = True

        'Update The Extracurricular Offered(CheckBoxes)
        'UpdateStudentExtracurriculars(txtLeadID.Text, ddlEducationInstId.SelectedValue)

        Dim insttype As String
        'Show The Education Institution Type 
        'Based On The Selection From DropDownList Selection
        Select Case ddlSchoolType.SelectedValue
            Case "College"
                insttype = "College"
            Case "Schools"
                insttype = "Schools"
            Case Else
                insttype = "College"
        End Select

        'Bind The DataList Based On LeadId 
        'and Education Institution Type.
        BindDataList(txtLeadID.Text, insttype)
        BuildSchoolsDdl(insttype, txtLeadID.Text)
        InitButtonsForEdit()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim sdfid As ArrayList
        Dim sdfidValue As ArrayList
        '  Dim newArr As ArrayList
        Dim z As Integer
        Dim sdfControl As New SDFComponent
        Try
            sdfControl.DeleteSDFValue(txtPKID.Text)
            sdfid = sdfControl.GetAllLabels(pnlSDF)
            sdfidValue = sdfControl.GetAllValues(pnlSDF)
            For z = 0 To sdfid.Count - 1
                sdfControl.InsertValues(txtPKID.Text, Mid(sdfid(z).id, 5), sdfidValue(z))
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        ''If Page is free of errors Show Edit Buttons
        If Page.IsValid Then
            InitButtonsForEdit()
        End If

        ddlEducationInstId.Enabled = True
        ddlEducationInstId.SelectedValue = ViewState("EducationInstId")
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, txtPKID.Text, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, txtPKID.Text)
        'if the selected school/college is 'other' then auto select the drop down
        If ddlEducationInstId.SelectedItem.Value = "{BFB523E7-D0F8-4BAF-AD42-040051560737}" Then
            ddlEducationInstId.SelectedIndex = ddlEducationInstId.Items.IndexOf(ddlEducationInstId.Items.FindByText(txtOther.Text))
            txtOther.Text = ""
            txtOther.Enabled = False
            lblOther.Enabled = False
        End If
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Function BuildStudentEducation(ByVal leadId As String) As PlacementInfo
        Dim studentEducation As New PlacementInfo
        With studentEducation
            ''get IsInDB
            .IsInDb = ChkIsInDB.Checked

            'Get Primary Key Value
            .StEmploymentId = txtPKID.Text

            'Get EmployerId
            .LeadID = txtLeadID.Text

            .EducationInstId = ddlEducationInstId.SelectedValue

            .Address1 = txtAddress1.Text

            .Address2 = txtAddress2.Text

            .State = txtStateId.Text

            .City = txtcity.Text

            .Zip = Trim(txtZip.Text)

            '.GraduationDate = txtGraduatedDate.SelectedDate
            If txtGraduatedDate.SelectedDate Is Nothing Then
                .GraduationDate = ""
            Else
                .GraduationDate = Date.Parse(txtGraduatedDate.SelectedDate)
            End If

            .FinalGrade = txtFinalGrade.Text

            .Certificate = ddlCertificateId.SelectedValue

            .Comments = txtComments.Text

            Select Case ddlSchoolType.SelectedValue
                Case "College"
                    .EducationInstType = "College"
                    lblEducationInstId.Text = "College Name"
                    lblOther.Text = "New College Name"
                    lblMajor.Visible = True
                    txtMajor.Visible = True
                Case "Schools"
                    .EducationInstType = "Schools"
                    lblEducationInstId.Text = "School Name"
                    lblOther.Text = "New School Name"
                    lblMajor.Visible = False
                    txtMajor.Visible = False
            End Select
            .Major = txtMajor.Text
            txtModDate.Text = Date.Now
            .ModDate = txtModDate.Text
            If txtOther.Text = "" Then
                .OtherCollegeNotListed = ""
            Else
                .OtherCollegeNotListed = txtOther.Text
            End If
        End With
        ViewState("EducationInstId") = ddlEducationInstId.SelectedValue
        Return studentEducation
    End Function

    Private Sub BtnNewClick(sender As Object, e As EventArgs) Handles btnNew.Click
        Dim insttype As String
        ChkIsInDB.Checked = False

        Session("from") = "New"

        'Create a Empty Object and Initialize the Object
        BindStudentInfo(New PlacementInfo)

        Dim resourceID As Integer
        resourceID = Trim(Request.QueryString("resid"))
        Dim sdfControls As New SDFComponent
        sdfControls.GenerateControlsNew(pnlSDF, resourceID, moduleId)

        'Refresh The DropDownList For Schools
        Select Case ddlSchoolType.SelectedValue
            Case "College"
                insttype = "College"
                lblEducationInstId.Text = "College Name"
                lblOther.Text = "New College Name"
            Case "Schools"
                insttype = "Schools"
                lblEducationInstId.Text = "School Name"
                lblOther.Text = "New School Name"
            Case Else
                insttype = "College"
                lblEducationInstId.Text = "College Name"
                lblOther.Text = "New College Name"
        End Select
        BuildSchoolsDdl(insttype, "")

        'chkExtracurricularId.ClearSelection()

        'Initialize Buttons
        InitButtonsForLoad()

        ddlEducationInstId.Enabled = True
        txtMajor.Text = ""

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, Guid.Empty.ToString, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub DlstEmployerContactItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstEmployerContact.ItemCommand
        Dim studentExtracurr As New PlacementFacade
        Dim instType As String

        'Refresh The DropDownList For Schools
        Select Case ddlSchoolType.SelectedValue
            Case "College"
                instType = "College"
            Case "Schools"
                instType = "Schools"
            Case Else
                instType = "College"
        End Select
        ChkIsInDB.Checked = True
        txtPKID.Text = e.CommandArgument
        BuildSchoolsDdl(instType, txtLeadID.Text)
        GetStudentInfo(txtLeadID.Text, e.CommandArgument, instType)
        Master.Master.PageObjectId = e.CommandArgument
        Master.Master.PageResourceId = resourceId
        Master.Master.SetHiddenControlForAudit()
        If Not ddlEducationInstId.SelectedValue = "" Or Not ddlEducationInstId.SelectedValue = Guid.Empty.ToString Then
            GetAddress(ddlEducationInstId.SelectedValue, instType)
        End If
        txtRowIds.Text = e.CommandArgument.ToString

        Dim strEducationInstID As String
        strEducationInstID = studentExtracurr.GetLeadEducationInstID(e.CommandArgument)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim sdfControls As New SDFComponent
        sdfControls.GenerateControlsEdit(pnlSDF, resourceId, e.CommandArgument, moduleId)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        'BindStudentExtracurricular(txtLeadId.Text, strEducationInstID)

        'set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, e.CommandArgument, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, e.CommandArgument)
        'Initialize Buttons For Edit
        InitButtonsForEdit()
        ddlEducationInstId.Enabled = True
    End Sub
    Private Sub GetStudentInfo(leadId As String, collegeId As String, instType As String)
        Dim studentinfo As New LeadFacade
        BindStudentInfoExist(studentinfo.GetStudentInfo(leadId, collegeId, instType))
    End Sub
    Private Sub BindDataList(leadId As String, instType As String)
        With New LeadFacade
            dlstEmployerContact.DataSource = .GetCollegeNamesByLeadID(instType, leadId)
            dlstEmployerContact.DataBind()
        End With
    End Sub

    Private Sub DdlSchoolTypeSelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSchoolType.SelectedIndexChanged
        Dim schoolItem As String
        schoolItem = ddlSchoolType.SelectedItem.Value
        BindDataList(txtLeadID.Text, schoolItem)
        BuildSchoolsDdl(schoolItem, "")
        Select Case schoolItem
            Case "College"
                lblEducationInstId.Text = "College Name"
                lblOther.Text = "New College Name"
                lblMajor.Visible = True
                txtMajor.Visible = True
            Case "Schools"
                lblEducationInstId.Text = "School Name"
                lblOther.Text = "New School Name"
                lblMajor.Visible = False
                txtMajor.Visible = False
            Case Else
                lblEducationInstId.Text = "College Name"
                lblOther.Text = "New College Name"
                lblMajor.Visible = True
                txtMajor.Visible = True
        End Select
        BindStudentInfo(New PlacementInfo)
        ddlEducationInstId.Enabled = True
        'chkExtracurricularId.ClearSelection()
        ChkIsInDB.Checked = False

        Dim sdfControls As New SDFComponent
        sdfControls.GenerateControlsNew(pnlSDF, resourceId, moduleId)

    End Sub
    Private Sub BtnDeleteClick(sender As Object, e As EventArgs) Handles btnDelete.Click
        If Not (txtLeadID.Text = Guid.Empty.ToString) Then
            '   instantiate component
            Dim studentInfo As New PlacementFacade

            'Delete The Row Based on LeadId
            Dim result As String = studentInfo.DeleteLeadEducation(txtPKID.Text, Date.Parse(txtModDate.Text))


            'If Delete Fails
            If result <> "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error3", result, "Delete Error")
                Exit Sub
            Else
                BindStudentInfo(New PlacementInfo)
                ddlEducationInstId.Enabled = True

                ChkIsInDB.Checked = False
                txtMajor.Text = ""
                Dim insttype As String
                Select Case ddlSchoolType.SelectedValue
                    Case "College"
                        insttype = "College"
                    Case "Schools"
                        insttype = "Schools"
                    Case Else
                        insttype = "College"
                End Select
                BindDataList(txtLeadID.Text, insttype)
                BuildSchoolsDdl(ddlSchoolType.SelectedValue, "")
                txtMajor.Text = ""
                InitButtonsForLoad()
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
            'SDF Code Starts Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim sdfControl As New SDFComponent
            sdfControl.DeleteSDFValue(leadId)
            sdfControl.GenerateControlsNew(pnlSDF, resourceId, moduleId)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)
        End If
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(txtGraduatedDate)
        'Add javascript code to warn the user about non saved changes 
        '        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)
        BindToolTip()
    End Sub
End Class
