﻿
Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.Common

Namespace AdvWeb.AD

    Partial Class AD_AleadPriorWork
        Inherits BasePage
        Private pObj As New UserPagePermissionInfo
        Public ObjStudentState As StudentMRU

        ''' <summary>
        ''' Check Permission, fill the Object Student State
        ''' </summary>
        ''' <param name="sender">The page sender</param>
        ''' <param name="e">Event Argument</param>
        Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

            'Get LeadId
            ObjStudentState = GetObjStudentState()
            If ObjStudentState Is Nothing Then
                RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
                Exit Sub
            End If

            Master.Master.PageObjectId = ObjStudentState.LeadId.ToString()
            Master.Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
            Master.Master.SetHiddenControlForAudit()
            Dim advantageUserState As User = AdvantageSession.UserState
            Dim resourceId = HttpContext.Current.Request.Params("resid")
            Dim campusid = Master.Master.CurrentCampusId
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusid)

            If Me.Master.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusid.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(4, ObjStudentState.Name)
                End If
            End If
        End Sub
    End Class
End Namespace