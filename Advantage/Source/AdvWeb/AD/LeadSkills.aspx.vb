﻿
Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports BO = Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports Advantage.Business.Logic.Layer
Partial Class LeadSkills
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtcheck As HtmlInputText
    Protected WithEvents txtStudentId As TextBox
    Protected WithEvents btnhistory As Button
    Protected WithEvents lstGrpName1 As ListBox
    Protected WithEvents lblSkillGroups1 As Label
    Protected WithEvents lblPossibleSkills1 As Label
    Protected WithEvents lblStudentSkills1 As Label
    Protected WithEvents lstPossibleSkills1 As ListBox
    Protected WithEvents lstStudentSkills1 As ListBox


    Private pObj As New UserPagePermissionInfo


#End Region

    Dim userId As String
    Dim resourceId As Integer
    Dim campusId As String
    Protected LeadId As String

    Private mruProvider As MRURoutines
    Protected StudentId As String
    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function GetLeadFromStateObject(ByVal paramResourceId As Integer) As StudentMRU

        Dim objStudentState As New StudentMRU

        Try
            MyBase.GlobalSearchHandler(1)

            boolSwitchCampus = Master.Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = (Guid.Empty).ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = (Guid.Empty).ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.Master.ShowHideStatusBarControl(True)

            Master.Master.PageObjectId = LeadId
            Master.Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
            Master.Master.setHiddenControlForAudit()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function

#End Region
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        Dim leadID As String = String.Empty
        'Dim strVID As String
        'Dim state As AdvantageSessionState
        '        Dim m_Context As HttpContext

        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        userId = AdvantageSession.UserState.UserId.ToString
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New StudentMRU
        objStudentState = GetLeadFromStateObject(147) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            StudentId = .StudentId.ToString
            leadID = .LeadId.ToString
            'if lead is from a different campus show notification

        End With

        txtLeadId.Text = leadID
        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            BindSkillGroup()
            BindStudentSkillsByGroup(campusId, leadID)

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""

        End If

        'Code to disable all buttons if lead was already enrolled
        'to prevent data being changed after lead was enrolled
        Dim chkLeadWasEnrolled As Boolean
        Dim leadFacade As New LeadEnrollmentFacade
        chkLeadWasEnrolled = leadFacade.CheckIfLeadWasEnrolled(leadID)
        If chkLeadWasEnrolled = True Then
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
            btnNew.ToolTip = "Cannot add new skills as the lead was already enrolled"
            btnSave.ToolTip = "Cannot modify skills information as the lead was already enrolled"
            btnDelete.ToolTip = "Cannot delete skills information as the lead was already enrolled"
        Else
            btnSave.ToolTip = ""
            btnDelete.ToolTip = ""
            btnNew.ToolTip = ""
            InitButtonsForEdit()
        End If
    End Sub
    Private Sub BindSkillGroup()
        Dim skillGroup As New PlacementFacade
        With lstGrpName
            .DataSource = skillGroup.GetAllSkillGroups(campusId, txtLeadId.Text)
            .DataTextField = "SkillGrpName"
            .DataValueField = "SkillGrpId"
            .DataBind()
        End With
    End Sub
    Private Sub BindSkillsByGroup(ByVal SkillGrpId As String)
        Dim skillByGroup As New PlacementFacade
        Dim skillCommon As New PlacementInfo
        txtSkillGrpId.Text = SkillGrpId
        With lstPossibleSkills
            .DataSource = skillByGroup.GetAllLeadSkillsByGroup(txtLeadId.Text, campusId, txtSkillGrpId.Text)
            .DataTextField = "SkillDescrip"
            .DataValueField = "SkillId"
            .DataBind()
        End With
    End Sub
    Private Sub BindStudentSkillsByGroup(ByVal skillGrpId As String, ByVal leadID As String)
        Dim skillByGroup As New PlacementFacade
        txtSkillGrpId.Text = skillGrpId
        With lstStudentSkills
            .DataSource = skillByGroup.GetLeadSkillsByGroup(txtLeadId.Text, campusId)
            .DataTextField = "SkillDescrip"
            .DataValueField = "SkillId"
            .DataBind()
        End With
    End Sub
    Private Sub LstGrpNameSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles lstGrpName.SelectedIndexChanged
        txtSkillGrpId.Text = lstGrpName.SelectedItem.Value
        lstStudentSkills.Items.Clear()
        BindSkillsByGroup(txtSkillGrpId.Text)
        BindStudentSkillsByGroup(txtSkillGrpId.Text, txtLeadId.Text)
        'Enable the Add all button if there are any items in the possible skills list box
        'This should only happen if the user has permission to add items
        If pObj.HasAdd Or pObj.HasFull Then
            If lstPossibleSkills.Items.Count > 0 Then
                btnAddAll.Enabled = True
            Else
                btnAddAll.Enabled = False
            End If
        End If

        'Enable the Remove All button if there any items in the selected skills list box
        'This should only happen if the user has permission to remove items
        If pObj.HasDelete Or pObj.HasFull Then
            If lstStudentSkills.Items.Count > 0 Then
                btnRemoveAll.Enabled = True
            Else
                btnRemoveAll.Enabled = False
            End If
        End If

        'Disable the Remove button if it is enabled
        If btnRemove.Enabled = True Then
            btnRemove.Enabled = False
        End If

        'Disable the Add button if it is enabled
        If btnAdd.Enabled = True Then
            btnAdd.Enabled = False
        End If


    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAdd.Click
        Dim strSelectedText As String
        Dim strSelectedValue As String
        'lblError.Visible = False
        If lstPossibleSkills.Items.Count = 0 Then
            'DisplayErrorMessage("Unable to locate a Skill from the Skills Group")
            DisplayRADAlert(CallbackType.Postback, "Error1", "Unable to locate a Skill from the Skills Group ", "Add Error")
            Exit Sub
        End If

        If lstPossibleSkills.SelectedIndex = -1 Then
            'DisplayErrorMessage("Please select an item from the possible Skills list")
            DisplayRADAlert(CallbackType.Postback, "Error2", "Please select an item from the possible Skills list ", "Add Error")
            Exit Sub
        End If

        strSelectedText = lstPossibleSkills.SelectedItem.Text
        strSelectedValue = lstPossibleSkills.SelectedItem.Value

        Dim i, count As Integer
        If lstStudentSkills.Items.Count >= 1 Then
            For i = 0 To lstStudentSkills.Items.Count - 1
                If lstStudentSkills.Items(i).Text = strSelectedText Then
                    count = 1
                End If
            Next
            If Not (count = 1) Then
                lstStudentSkills.Items.Add(New ListItem(strSelectedText, strSelectedValue))
                lstPossibleSkills.Items.RemoveAt(lstPossibleSkills.SelectedIndex)
            End If
        Else
            lstStudentSkills.Items.Add(New ListItem(strSelectedText, strSelectedValue))
            lstPossibleSkills.Items.RemoveAt(lstPossibleSkills.SelectedIndex)
        End If
        'Enable the Remove All button if the user has permission to delete
        If pObj.HasFull Or pObj.HasDelete Then
            btnRemoveAll.Enabled = True
        End If
    End Sub
    Private Sub btnAddAll_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddAll.Click
        ' lblError.Visible = False
        If lstPossibleSkills.Items.Count = 0 Then
            'DisplayErrorMessage("Unable to locate a Skill from the Skills Group ")
            DisplayRADAlert(CallbackType.Postback, "Error3", "Unable to locate a Skill from the Skills Group ", "Add Error")
            Exit Sub
        End If

        Dim i As Integer ', counter
        For i = 0 To lstPossibleSkills.Items.Count - 1
            lstStudentSkills.Items.Add(New ListItem(lstPossibleSkills.Items(i).Text, lstPossibleSkills.Items(i).Value))
        Next
        lstPossibleSkills.Items.Clear()
        'Enable the Remove All button if the user has permission to delete
        If pObj.HasFull Or pObj.HasDelete Then
            btnRemoveAll.Enabled = True
        End If
    End Sub
    Private Sub btnRemoveAll_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemoveAll.Click
        'lblError.Visible = False
        Dim i As Integer ', counter
        If lstStudentSkills.Items.Count = 0 Then
            'DisplayErrorMessage("The student Skills list is empty")
            DisplayRADAlert(CallbackType.Postback, "Error4", "The student Skills list is empty ", "Remove Error")
            Exit Sub
        End If
        For i = 0 To lstStudentSkills.Items.Count - 1
            lstPossibleSkills.Items.Add(New ListItem(lstStudentSkills.Items(i).Text, lstStudentSkills.Items(i).Value))
        Next
        lstStudentSkills.Items.Clear()
        'Enable the Add All button if the user has permission to add
        If pObj.HasFull Or pObj.HasAdd Then
            btnAddAll.Enabled = True
        End If

    End Sub
    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemove.Click
        '        Dim strRemoveItem As String
        Dim StrSelectedText As String
        Dim strSelectedValue As String

        ' lblError.Visible = False
        If lstStudentSkills.Items.Count = 0 Then
            'DisplayErrorMessage("The list is empty - unable to remove the item")
            DisplayRADAlert(CallbackType.Postback, "Error5", "The list is empty - unable to remove the item ", "Remove Error")
            Exit Sub
        End If
        If (lstStudentSkills.SelectedIndex = -1) Then
            'DisplayErrorMessage("Please select student's Skill from the Skill list")
            DisplayRADAlert(CallbackType.Postback, "Error6", "Please select student's Skill from the Skill list ", "Remove Error")
            Exit Sub
        Else
            StrSelectedText = lstStudentSkills.SelectedItem.Text
            strSelectedValue = lstStudentSkills.SelectedItem.Value
            lstPossibleSkills.Items.Add(New ListItem(StrSelectedText, strSelectedValue))
            lstStudentSkills.Items.RemoveAt(lstStudentSkills.SelectedIndex)
        End If
        'Enable the Add All button if the user has permission to add
        If pObj.HasFull Or pObj.HasAdd Then
            btnAddAll.Enabled = True
        End If
    End Sub
    Private Sub UpdateStudentSkills(ByVal LeadID As String, ByVal SkillGrpId As String)
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), lstStudentSkills.Items.Count)
        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In lstStudentSkills.Items
            selectedDegrees.SetValue(item.Value.ToString, i)
            i += 1
        Next

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)

        'update Selected Jobs
        Dim employerJobs As New PlEmployerInfoFacade
        Dim StudentSkills As New PlacementFacade
        StudentSkills.UpdateLeadSkills(LeadID, txtSkillGrpId.Text, AdvantageSession.UserState.UserName, selectedDegrees)
        'If StudentSkills.UpdateStudentSkills(LeadID, Session("UserName"), selectedDegrees) < 0 Then
        'CustomValidator1.ErrorMessage = "There was a problem with Referential Integrity.<BR> You can not perform this operation."
        'CustomValidator1.IsValid = False
        'End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click


        ''''' Code changes to Fix mantis issue id 19549 by kamalesh Ahuja on 23 Aug 2010 '''
        'Dim userId As String
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString

        Dim LeadMasterUpdate As New LeadFacade
        If MyAdvAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "no" Then
            If LeadMasterUpdate.CheckLeadUser(userId, LeadId) = 0 Then
                'DisplayErrorMessage("You do not have rights to edit this lead")
                DisplayRADAlert(CallbackType.Postback, "Error7", "You do not have rights to edit this lead ", "Save Error")
                Exit Sub
            End If
        End If
        ''''''''''''''''''

        Try
            UpdateStudentSkills(txtLeadId.Text, txtSkillGrpId.Text)
            InitButtonsForEdit()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Exit Sub
        End Try
    End Sub
    Private Sub InitButtonsForEdit()
        'In this case the New button is not relevant
        If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'If pObj.HasFull Or pObj.HasDelete Then
        '    btnDelete.Enabled = True
        'Else
        '    btnDelete.Enabled = False
        'End If
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)
    '    'Set error condition
    '    'Display error in message box in the client
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub

    Private Sub lstStudentSkills_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles lstStudentSkills.SelectedIndexChanged
        'Enable the Remove button if the user has permission to delete
        If pObj.HasFull Or pObj.HasDelete Then
            btnRemove.Enabled = True
        End If

        'Disable the Add button if it is enabled
        If btnAdd.Enabled = True Then
            btnAdd.Enabled = False
        End If
        'Disable the Add All button is if is enabled
        If btnAddAll.Enabled = True Then
            btnAddAll.Enabled = False
        End If
    End Sub

    Private Sub lstPossibleSkills_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles lstPossibleSkills.SelectedIndexChanged
        'Enable the Add button
        btnAdd.Enabled = True
        'Disable the Remove and Remove All buttons
        If btnRemove.Enabled = True Then
            btnRemove.Enabled = False
        End If
        If btnRemoveAll.Enabled = True Then
            btnRemoveAll.Enabled = False
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        ' Dim controlsToIgnore As New ArrayList() 
        ' 'add save button 
        ' controlsToIgnore.Add(btnSave)
        ' controlsToIgnore.Add(btnAdd)
        ' controlsToIgnore.Add(btnAddAll)
        ' controlsToIgnore.Add(btnRemove)
        ' controlsToIgnore.Add(btnRemoveAll)
        ''Add javascript code to warn the user about non saved changes 
        ' CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub


End Class
