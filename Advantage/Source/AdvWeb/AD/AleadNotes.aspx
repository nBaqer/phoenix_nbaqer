﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="AleadNotes.aspx.vb" Inherits="AdvWeb.AD.AdAleadNotes" %>
<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" runat="Server">
    <!-- Use this to put content -->
    <link href="../css/AD/LeadNotes.css" rel="stylesheet" />
    <div id="notesWrapper">
        <div id="notesFirstPanel" class="flex-container">
            <div id="notesItem1" class="flex-item" style="left: 0">
                <label class="leadTitles" style="font-weight: bold !important; vertical-align: top; font-size: 11pt !important;">Notes</label>
                <a id="notesPlusImage" href="javascript:void(0);">
                    <span class="k-icon k-i-plus-circle font-green"></span>
                </a>
            </div>
            <div id="notesItem2" class="flex-item">
                <label style="position: relative; top: 7px; margin-right: 10px; font-weight: bold">Module</label>
                <div id="notesModulesDdl"></div>
            </div>
            <div id="notesItem3" class="flex-item">
                <button id="notesPrintbutton" type="button" class="k-button" style="font-size: 12px; font-weight: bold; width:75px">Print</button>
            </div>
        </div>

        <div id="notesMainPanel" style="margin: 10px">
            <div id="notesGrid"></div>
        </div>
        <div id="Edit"></div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var leadElement = document.getElementById('leadId');
            var leadid = leadElement.value;
            var manager = new AD.LeadNotes(leadid, "AD");
            manager.initOperation();
        });
    </script>
      <script type="text/x-kendo-template" id="notesTypeGridStyleTemplate">
        #if( Type == "Confidential"){#
            <span style="color: ##b71c1c">  #: Type #  </span>
        #}else{#
            #: Type # 
        #}#
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

