﻿Imports System.Collections
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports BO = Advantage.Business.Objects

Partial Class AssignLeads
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblReassignToAdmissionReps As System.Web.UI.WebControls.Label
    Protected WithEvents lstReassignToAdmissionReps As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblAssignmentResult As System.Web.UI.WebControls.Label
    Protected WithEvents lstAssignmentResult As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstGrpName As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstPossibleSkills As System.Web.UI.WebControls.ListBox
    Protected WithEvents listbox2 As System.Web.UI.WebControls.ListBox
    Protected WithEvents img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents txtLeadsInfo As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblLeads As System.Web.UI.WebControls.Label
    Protected WithEvents lstAssignCampus As System.Web.UI.WebControls.ListBox
    Protected campusId As String
    Protected mContext As HttpContext


    Private pObj As New UserPagePermissionInfo
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

    Dim userId As String
    Dim resourceId As Integer
    Protected leadId As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        'Dim objCommon As New CommonUtilities
        '        Dim m_Context As HttpContext
        'Dim fac As New UserSecurityFacade
        'Dim campusId2 As String
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        campusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'campusId2 = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId2)

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'Check if this page still exists in the menu while switching campus



        If Me.Master.IsSwitchedCampus = True Then

            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        'End If

        If pObj.HasFull Then
            btnAssign.Enabled = True
        Else
            btnAssign.Enabled = False
        End If

        If Not Page.IsPostBack Then
            BuildAdmissionRepsDdl()
            BuildCampusDdl()
        End If
        btnNew.Enabled = False
        btnDelete.Enabled = False
        btnSave.Enabled = False
    End Sub
    Private Sub BuildAdmissionRepsDdl()
        Dim leadAdmissionReps As New LeadFacade
        With ddlCurrentAdmissionRepID
            .DataTextField = "fullname"
            .DataValueField = "UserId"
            .DataSource = leadAdmissionReps.GetAllAdmissionRepsByCampus(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            '.Items.Insert(1, New ListItem("None", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildLeads(ByVal admissionRepId As String)
        Dim leadStatus As New LeadFacade
        With lstLeads
            .DataTextField = "fullname"
            .DataValueField = "LeadId"
            .DataSource = leadStatus.GenerateUnAssignedLeadByAdmissionRepId(admissionRepId)
            .DataBind()
        End With
    End Sub
    Private Sub BuildCampusDdl()
        Dim campGrp As New CampusGroupsFacade
        With lstCampusId
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            .DataSource = campGrp.GetAllCampusEnrollment()
            .DataBind()
        End With
    End Sub
    Private Sub UpdateAssignment()
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        'Dim selectedDegrees() As String = selectedDegrees.CreateInstance(GetType(String), lstAssignCampus.Items.Count)

        Dim selectedCampus() As String = Array.CreateInstance(GetType(String), lstCampusId.Items.Count)
        'Dim strCurrAdmRepId As String = ddlCurrentAdmissionRepID.SelectedValue
        Dim selectedLeads() As String = Array.CreateInstance(GetType(String), lstLeads.Items.Count)


        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim leadAssignment As New LeadFacade
        Dim result As String

        Dim item As ListItem
        '        Dim strLeadIndex As String

        txtLeads.Text = ""
        txtCampusId.Text = ""

        For Each item In lstLeads.Items
            If item.Selected Then
                txtLeads.Text &= item.Value & "','"
                '  strLeadIndex &= lstLeads.SelectedItem. 
            End If
        Next

        For Each item In lstCampusId.Items
            If item.Selected Then
                txtCampusId.Text &= item.Value & "','"
            End If
        Next

        If txtCampusId.Text = "" Then
            'DisplayErrorMessage("Please select a Campus from the Campus List")
            DisplayRADAlert(CallbackType.Postback, "Error1", "Please select a Campus from the Campus List", "Update Error")
            Exit Sub
        End If

        ''In For Loop Check The Number of Items Selected For Reassign Admission Reps
        For Each item In lstCampusId.Items
            If item.Selected Then
                selectedCampus.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        For Each item In lstLeads.Items
            If item.Selected Then
                selectedLeads.SetValue(item.Value.ToString, j)
                'lstStudentExtracurriculars.Items.RemoveAt(lstStudentExtracurriculars.SelectedIndex)

                j += 1
            End If
        Next



        ''resize the array
        If i > 0 Then ReDim Preserve selectedCampus(i - 1)
        If j > 0 Then ReDim Preserve selectedLeads(j - 1)

        If txtLeads.Text = "" Then
            'DisplayErrorMessage("Please select the leads from the leads listbox ")
            DisplayRADAlert(CallbackType.Postback, "Error2", "Please select the leads from the leads listbox ", "Update Error")
            Exit Sub
        End If


        'update Selected Jobs
        result = leadAssignment.AssignmentLeadToCampus(selectedLeads, ddlCurrentAdmissionRepID.SelectedValue, selectedCampus, AdvantageSession.UserState.UserName)
        If result = 0 Then
            Dim intPosComma As Integer
            Dim strLeads As String ', strStatus

            'For Campus
            intPosComma = InStrRev(txtCampusId.Text, ",", -1)
            'strCampusId = Mid(txtCampusId.Text, 1, intPosComma - 1)

            'For Leads
            intPosComma = InStrRev(txtLeads.Text, ",", -1)
            strLeads = Mid(txtLeads.Text, 1, intPosComma - 2)

            'lstAssignmentResults.Items.Clear()
            'With lstAssignmentResults
            '    .DataTextField = "FullName"
            '    .DataValueField = "LeadId"
            '    .DataSource = LeadAssignment.PopulateAssignLeadsToCampus(strLeads)
            '    .DataBind()
            'End With
            dgrdTransactionSearch.Visible = True
            With dgrdTransactionSearch
                .DataSource = leadAssignment.PopulateAssignLeadsToCampus(strLeads)
                .DataBind()
            End With
            'Refresh the Leads ListBox
            BuildLeads(ddlCurrentAdmissionRepID.SelectedValue)
            lstLeads.SelectedIndex = -1
            lstCampusId.SelectedIndex = -1
            'DisplayErrorMessage("The Leads were successfully assigned to the selected Campus")
            DisplayRADAlert(CallbackType.Postback, "Error3", "The Leads were successfully assigned to the selected Campus", "Update Error")
        End If
    End Sub
    Private Sub BtnSaveClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim selectedLeads() As String = selectedLeads.CreateInstance(GetType(String), lstLeads.Items.Count)

    End Sub
    Private Sub BtnSubmitClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
    End Sub
    Private Sub TxtStartLeadDateTextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub
    Private Sub DdlCurrentAdmissionRepIDSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurrentAdmissionRepID.SelectedIndexChanged
        If Not ddlCurrentAdmissionRepID.SelectedValue = Guid.Empty.ToString Then
            BuildLeads(ddlCurrentAdmissionRepID.SelectedValue)
            lstAssignmentResults.Items.Clear()
            lstCampusId.ClearSelection()
            dgrdTransactionSearch.Visible = False
        End If
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)
    '    'Set error condition
    '    'Display error in message box in the client
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub
    Private Sub BtnAssignLeadClick(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub
    Private Sub BtnAssignClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        If ddlCurrentAdmissionRepID.SelectedValue = Guid.Empty.ToString Then
            'DisplayErrorMessage("Admissions Representative is required")
            DisplayRADAlert(CallbackType.Postback, "Error4", "Admissions Representative is required", "Assign Error")
            Exit Sub
        End If
        If lstLeads.Items.Count = 0 Then
            'DisplayErrorMessage("Unable to find any Leads assigned to this Admissions Representative")
            DisplayRADAlert(CallbackType.Postback, "Error5", "Unable to find any Leads assigned to this Admissions Representative", "Assign Error")
            Exit Sub
        End If
        UpdateAssignment()
        BuildLeads(ddlCurrentAdmissionRepID.SelectedValue)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnAssign)
        'Add javascript code to warn the user about non saved changes 
        '    CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub


End Class