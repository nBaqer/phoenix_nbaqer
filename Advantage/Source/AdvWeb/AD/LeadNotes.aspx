﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="LeadNotes.aspx.vb" Inherits="LeadNotes" %>
<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
	<script src="../Scripts/Advantage.Client.AD.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" Runat="Server">
	   <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
	</asp:ScriptManagerProxy>
	<telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
	VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
	<telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
	 <%-- Add class ListFrameTop2 to the table below --%>
		<table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
			<tr>
				<td class="listframetop2">
					<br />
					 <table cellspacing="0" cellpadding="0" width="100%" border="0">
						<tr>
							<td>
								<table cellSpacing="0" cellPadding="0" width="100%" border="0" style="background-color:transparent;">
										<tr>
											<td class="contentcell" style="background-color:transparent;"><asp:label id="lblModuleName" CssClass="label" Runat="server">Module</asp:label></td>
											<td class="contentcell4" align="left" style="background-color:transparent;"><asp:dropdownlist id="ddlModules" runat="server" CssClass="dropdownlist"></asp:dropdownlist></td>
										</tr>
										<tr>
											<td class="contentcell" style="background-color:transparent;"><asp:label id="lblTransDateFrom" runat="server" CssClass="label">Start Date</asp:label></td>
											<td class="contentcell4" align="left" style="background-color:transparent;">
											<%--<asp:textbox id="txtTransDateFrom" runat="server" CssClass="textboxdate"></asp:textbox>&nbsp;<A id="CalButton" onclick="javascript:OpenCalendar('Form1', 'txtTransDateFrom', false, 2001)"
													runat="server"><IMG id="Img2" alt="Graphic Calendar" src="../UserControls/Calendar/PopUpCalendar.gif"
														align="absMiddle" border="0" runat="server"></A>--%>
												 <telerik:RadDatePicker ID="txtTransDateFrom" MinDate="1/1/1945" runat="server">
														</telerik:RadDatePicker>
											</td>
										</tr>
										<tr>
											<td class="contentcell" style="background-color:transparent;"><asp:label id="lblTransDateTo" runat="server" CssClass="label">End Date</asp:label></td>
											<td class="contentcell4" align="left" style="background-color:transparent;">
										   <%-- <asp:textbox id="txtTransDateTo" runat="server" CssClass="textboxdate"></asp:textbox>&nbsp;<A id="A1" onclick="javascript:OpenCalendar('Form1', 'txtTransDateTo', false, 2001)" runat="server"><IMG id="Img1" alt="Graphic Calendar" src="../UserControls/Calendar/PopUpCalendar.gif"
														border="0" runat="server"></A>--%>
												 <telerik:RadDatePicker ID="txtTransDateTo" MinDate="1/1/1945" runat="server">
														</telerik:RadDatePicker>
											</td>
										</tr>
										<tr>
											<td class="contentcell" style="background-color:transparent;"></td>
											<td class="contentcell4" style="TEXT-ALIGN: left;background-color:transparent;">
												<%--<asp:button id="btnBuildList" runat="server" CssClass="buttontopfilter" CausesValidation="False"
													Text="Build List"></asp:button>--%>
												<telerik:RadButton ID="btnBuildList" runat="server"   CausesValidation="False"
													Text="Build List" style="background-color:transparent;"></telerik:RadButton>
											</td>
										</tr>
									</table>
							</td>
						</tr>
						<tr>
							<td class="listframebottom">
								<div class="scrollleftfilters">&nbsp;
									</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</telerik:RadPane>
	<telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
					<table cellspacing="0" cellpadding="0" width="100%" border="0">
						<!-- begin top menu (save,new,reset,delete,history)-->
						<tr>
							<td class="menuframe" align="right">
								<asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
									ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
										ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
									</asp:Button>
									</td>
							
						</tr>
					</table>
		<table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
			<tr>
				<td class="detailsframe">
					<div class="scrollright2" align="center">
						<!-- begin content table-->
						<table width="60%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
											<tr>
												<td>
													<asp:textbox ID="txtLeadNoteId" Runat="server" Visible="False"></asp:textbox>
													<asp:textbox ID="txtLeadId" Runat="server" Visible="False"></asp:textbox>
													<asp:TextBox ID="txtModUser" Runat="server" Visible="False"></asp:TextBox>
													<asp:TextBox ID="txtModDate" Runat="server" Visible="False"></asp:TextBox>
													<asp:textbox id="txtNote" runat="server" TextMode="MultiLine" Rows="5" Width="100%" CssClass="textbox"></asp:textbox>
													<asp:RequiredFieldValidator id="rfvNote" runat="server" ErrorMessage="Note can not be blank" Display="None"
														ControlToValidate="txtNote">Note can not be blank</asp:RequiredFieldValidator></td>
											</tr>
											<tr>
												<td><asp:dropdownlist id="ddlModules1" runat="server" CssClass="dropdownlist"></asp:dropdownlist></td>
											</tr>
											<tr>
												<td style="text-align: center; padding: 16px">
													<telerik:RadButton ID="btnAddNewNote" runat="server"  Text="Add New Note" Width="100px"></telerik:RadButton>
													<telerik:RadButton id="btnPrint" runat="server"  Text="Print Notes" Width="100px" CausesValidation="false"></telerik:RadButton>
												</td>                                                    
											</tr>
											<%--<tr>
												<td style="text-align: center; padding: 16px">
													<asp:button id="btnAddNewNote" runat="server" CssClass="button" Text="Add New Note" Width="100px"></asp:button>
													<telerik:RadButton ID="btnAddNewNote" runat="server"  Text="Add New Note" Width="100px"></telerik:RadButton>
												</td>
											</tr>--%>

										</table>
										<asp:datagrid id="dgrdLeadNotes" runat="server" HorizontalAlign="Center" width="100%" BorderStyle="Solid"
											AutoGenerateColumns="False" AllowSorting="True" BorderColor="#E0E0E0">
											<AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
											<ItemStyle HorizontalAlign="Center" CssClass="datagriditemstyle"></ItemStyle>
											<HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn SortExpression="CreatedDate asc" HeaderText="Date">
													<HeaderStyle CssClass="datagridheaderstyle" Width="15%"></HeaderStyle>
													<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lblLeadNoteDate" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.CreatedDate") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Note">
												<HeaderStyle CssClass="datagridheaderstyle" Width="55%"></HeaderStyle>
													<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
													<ItemTemplate>
														<asp:LinkButton id="lnkLeadNote" runat="server" Text='<%# TruncateNote(DataBinder.Eval(Container, "DataItem.LeadNoteDescrip")) %>' CausesValidation="False"  Visible="False">
														</asp:LinkButton>
														<asp:Label ID="lblLeadNote" Runat="server" CssClass="label" text='<%# DataBinder.Eval(Container, "DataItem.LeadNoteDescrip") %>'>
														</asp:Label>
													</ItemTemplate>
													<EditItemTemplate>
														<asp:TextBox id="txtLeadNote" runat="server" Width="99%" CssClass="textbox" Text='<%# DataBinder.Eval(Container, "DataItem.LeadNoteDescrip") %>' TextMode="MultiLine" ReadOnly="True">
														</asp:TextBox>
													</EditItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="ModuleCode asc" HeaderText="Module">
												<HeaderStyle CssClass="datagridheaderstyle" Width="15%"></HeaderStyle>
													<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lblModuleCode" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.ModuleCode") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn SortExpression="UserName asc" HeaderText="User">
													<HeaderStyle CssClass="datagridheaderstyle" Width="15%"></HeaderStyle>
													<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="lblUser" runat="server" CssClass="label" Text='<%# DataBinder.Eval(Container, "DataItem.UserName") %>'>
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid>
						<!-- end content table-->
						
					</div>
				</td>
			</tr>
		</table>
	   
	</telerik:RadPane>
	</telerik:RadSplitter>
		<asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
		</asp:Panel>
		<asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
			Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
		<asp:Panel ID="pnlRequiredFieldValidators" runat="server">
		</asp:Panel>
		<asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
			ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary> 
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">

</asp:Content>

