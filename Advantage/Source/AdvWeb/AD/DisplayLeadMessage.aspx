﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DisplayLeadMessage.aspx.vb" Inherits="AD_DisplayLeadMessage" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head id="Head1">
    <title>Duplicate Lead Message</title>
    <link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
    <script language="javascript" type="text/javascript">
        function yesclick() {
            var dupValue = window.opener.document.getElementById("dup");
            dupValue.value = "yes";
            window.opener.document.forms("form1").submit();
            window.close()
        }

        function noclick() {
            var dupValue = window.opener.document.getElementById("dup");
            dupValue.value = "no";
            window.close();           
        }
       
        function cancelclick() {
            window.close();
        }       
    </script>
    <style>
        .LabelBold
            {
	            font: normal 11px verdana;
	            font-weight: bold;
	            color: #000066;
	            background-color: transparent;
	            width: 100%;
	            padding: 2px;
	            vertical-align: bottom;
            }
       .buttons
            {
	            margin: 4px;
	            vertical-align: middle;
	            font: normal 10px verdana;
	            text-align:center;
	            color: #000066;
            }
    </style>
</head>
<body bgcolor="#E9EDF2">
    <form id="form1" runat="server">
    <div>
    <p></p><p></p><p></p><p></p>
        <table width="80%">
            <tr>
                <td nowrap>
                    <asp:Label ID="lblBold" runat="Server" CssClass="LabelBold"><%=Request.QueryString("Message")%></asp:Label>            
                </td>
            </tr>
             <tr>
                <td>
                    <asp:Label ID="Label1" runat="Server" CssClass="LabelBold"><%=Request.QueryString("Message1")%></asp:Label>            
                </td>
            </tr>
        </table>
        <p></p>
        <table width="50%">
            <tr>
                <td><asp:Button ID="btnyes" Text="Yes" runat="server"  width="75px" /></td>
                <td><asp:Button ID="btnno" Text="No" runat="server"  width="75px" /></td>
                <td><asp:Button ID="btncancel" Text="Cancel" runat="server"  width="75px" /></td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
