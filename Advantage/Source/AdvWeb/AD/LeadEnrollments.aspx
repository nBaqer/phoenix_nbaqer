﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="LeadEnrollments.aspx.vb" Inherits="AdvWeb.AD.LeadEnrollments" %>


<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<%@ Register Src="~/usercontrols/AdvantageRadAjaxLoadingControl.ascx" TagPrefix="uc1" TagName="AdvantageRadAjaxLoadingControl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" runat="Server">
	<script language="javascript" src="../js/EnterKey.js" type="text/javascript"></script>
	<script language="javascript" type="text/javascript">
		window.onload = function () {
			var strCook = document.cookie;
			if (strCook.indexOf("!~") !== 0) {
				var intS = strCook.indexOf("!~");
				var intE = strCook.indexOf("~!");
				var strPos = strCook.substring(intS + 2, intE);
				document.getElementById("grdWithScroll").scrollTop = strPos;
			}
		}
	</script>
	<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
	</asp:ScriptManagerProxy>
	<uc1:AdvantageRadAjaxLoadingControl ID="AdvantageRadAjaxLoadingControl1" runat="server" />
	<%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="AdvantageRadAjaxLoadingControl1">--%>
	<telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
		Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
		<telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350"
			Scrolling="Y">
			<%-- Add class ListFrameTop2 to the table below --%>
			<%-- Add class ListFrameTop2 to the table below --%>
			<table id="Table2" class="maincontenttable">
				<tr>
					<td class="listframetop2">
						<br />
						<table class="maincontenttable">
							<tr>
								<td>
									<table class="maincontenttable">
										<tr>
											<td class="employersearch" nowrap="nowrap">
												<asp:Label ID="lblFirstName" runat="server" CssClass="label">First Name</asp:Label>
											</td>
											<td class="employersearch2">
												<asp:TextBox ID="txtFirstName" runat="server" CssClass="textbox"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="employersearch" nowrap="noWrap">
												<asp:Label ID="lblLastName" runat="server" CssClass="label">Last Name</asp:Label>
											</td>
											<td class="employersearch2">
												<asp:TextBox ID="txtLastName" runat="server" CssClass="textbox"></asp:TextBox>
											</td>
										</tr>
										<tr>
											<td class="employersearch" nowrap="nowrap">
												<asp:Label ID="lblLeadStatus" runat="server" CssClass="label">Lead Status</asp:Label>
											</td>
											<td class="employersearch2">
												<asp:DropDownList ID="ddlLeadStatus" runat="server" CssClass="dropdownlist">
												</asp:DropDownList>
											</td>
										</tr>
										<tr>
											<td class="employersearch" nowrap="nowrap">
												<asp:Label ID="lblCampusIDSearch" runat="server" CssClass="label">Campus</asp:Label>
											</td>
											<td class="employersearch2">
												<asp:DropDownList ID="ddlCampusIDSearch" Width="90%" runat="server" CssClass="dropdownlist">
												</asp:DropDownList>
											</td>
										</tr>
										<tr>
											<td class="employersearch"></td>
											<td class="employersearch2" style="text-align: center">
												<asp:Button ID="btnSearch" runat="server" CausesValidation="False" Text="Build Leads"></asp:Button>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="listframebottom">
									<div id="grdWithScroll" class="scrollleftfltr4rows">
										<asp:DataList ID="dlstLeadNames" runat="server" DataKeyField="LeadId" GridLines="Both"
											Width="100%">
											<SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
											<ItemStyle CssClass="itemstyle"></ItemStyle>
											<HeaderTemplate>
												<table style="width: 100%; padding: 0; margin:0">
													<tr>
														<td class="threecolumndatalist" style="border-right: #999 1px solid; border-top: #999 1px solid; border-left: #999 1px solid; width: 100%; border-bottom: #999 1px solid">
															<asp:Label ID="lblLeadName" runat="server" CssClass="labelbold">Leads (SSN)</asp:Label>
														</td>
													</tr>
												</table>
											</HeaderTemplate>
											<SelectedItemTemplate>
												<table style="width: 100%; padding: 0; margin:0">
													<tr>
														<td class="threecolumndatalist" style="width: 100%;">
															<asp:Label ID="label11" runat="server" CssClass="selecteditemstyle" Text='<%# Container.DataItem("FullName")%>'>
															</asp:Label>
														</td>
													</tr>
												</table>
											</SelectedItemTemplate>
											<ItemTemplate>
												<table style="width: 100%; padding: 0; margin:0">
													<tr>
														<td class="threecolumndatalist" style="width: 100%;">
															<asp:LinkButton ID="Linkbutton1" runat="server" CausesValidation="False" CommandArgument='<%# Container.DataItem("LeadId")%>'
																CssClass="itemstyle" Text='<%# Container.DataItem("FullName")%>' />
														</td>
													</tr>
												</table>
											</ItemTemplate>
										</asp:DataList>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</telerik:RadPane>
		<telerik:RadSplitBar ID="RadSplitBar1" runat="server" CollapseMode="Forward"></telerik:RadSplitBar>
		<telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
			Orientation="HorizontalTop" Height="16px">
			<table  style="width:100%; " border="0" cellpadding="0" cellspacing="0" >
				<tr>
					<td class="menuframe" align="right">
						<asp:Button ID="btnSave" runat="server" CssClass="save" Enabled="False" Text="Save"></asp:Button>
						<asp:Button ID="btnNew" runat="server" CausesValidation="False" CssClass="new" Enabled="False"
							Text="New"></asp:Button>
						<asp:Button ID="btnDelete" runat="server" CausesValidation="False" CssClass="delete"
							Enabled="False" Text="Delete"></asp:Button>
					</td>
					
				</tr>
			</table>
			<table  class="maincontenttable" style="width: 98%;">
				<tr>
					<td class="detailsframe">
						<div class="scrollright2">
							<!-- begin content table-->
							<asp:Panel ID="pnlNotes" runat="server">
								<table class="contenttable">
									<tr>
										<td style="text-align: center; white-space: nowrap;" >
											<asp:Label ID="lblNote" runat="server" CssClass="label" Font-Bold="true">Note :</asp:Label>
										</td>
										<td style="text-align: left; white-space: nowrap;" >
											<asp:Label ID="Label12" runat="server" CssClass="label" Font-Bold="true">Please select a lead status from the dropdown list and then click on a lead.</asp:Label>
										</td>
									</tr>
									<tr>
										<td style="text-align: center; white-space: nowrap;" ></td>
										<td style="text-align: left; white-space: nowrap;" >
											<asp:Label ID="lblNote2" runat="server" CssClass="label" Font-Bold="true">Leads shown here are those whose status allows to be changed to 'Enrolled'.</asp:Label>
										</td>
									</tr>
								</table>
							</asp:Panel>
							<asp:Panel ID="pnlLeadEnrollment" runat="server" Height="758px" Width="902px">
								<table class="contenttable" style="width: 98%;">
									<tr>
										<td class="contentcell"></td>
										<td class="contentcell4"></td>
										<td style="padding-left: 50px; "></td>
										<td class="contentcell" style="white-space: normal"></td>
										<td class="contentcell4">
											<img alt="" height="24" src="../Images/admission.gif" width="24" />
											<asp:LinkButton ID="lnkOpenRequirements" runat="server" CausesValidation="False"
												Enabled="false">Admission Requirements</asp:LinkButton>
										</td>
									</tr>
									<tr style="height: 10px;">
										<td class="contentcell"></td>
										<td class="contentcell4"></td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal"></td>
										<td class="contentcell4"></td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblEnrollmentId" runat="server" CssClass="label">Enrollment Id</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:TextBox ID="txtEnrollmentId" runat="server" CssClass="textbox" Enabled="False"
												TabIndex="1"></asp:TextBox>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblStatusCodeId" runat="server" CssClass="label">Status</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlStatusCodeId" runat="server" CssClass="dropdownlist" TabIndex="18">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="Label1" runat="server" CssClass="label">Lead Name</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:TextBox ID="txtLead" runat="server" CssClass="textbox" Enabled="False" TabIndex="2"></asp:TextBox>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblCampusId" runat="server" CssClass="label">Campus</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlCampusId" runat="server" CssClass="dropdownlist" TabIndex="19">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblPrgVerId" runat="server" CssClass="label">Program Version</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlPrgVerId" runat="server" AutoPostBack="True" CssClass="dropdownlist"
												TabIndex="3">
											</asp:DropDownList>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblEmpId" runat="server" CssClass="label">Academic Advisor</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlEmpId" runat="server" CssClass="dropdownlist" TabIndex="20">
											</asp:DropDownList>
										</td>
									</tr>
									<tr id="SuvaStuff" style="visibility: collapse" runat="server">
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="Label2" runat="server" CssClass="label">Program Version Type<span style="color: #b71c1c">*</span></asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlSuvaVersion" runat="server" AutoPostBack="False" CssClass="dropdownlist"
												TabIndex="3">
											</asp:DropDownList>
										</td>
										<td class="emptycell"></td>
										<td class="emptycell"></td>
										<td class="emptycell"></td>
									</tr>

									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblSchedule" runat="server" CssClass="Label">Schedule</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlScheduleID" runat="server" CssClass="dropdownlist" TabIndex="4" />
											<asp:ImageButton ID="ibToMaintPage_Schedule" runat="server" CausesValidation="False"
												ImageUrl="~/images/AR/to_maintpage.gif" ToolTip="Go to maintenance page" />
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblAdminRep" runat="server" CssClass="label">Admissions Rep</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlAdminRep" runat="server" CssClass="dropdownlist" TabIndex="21">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblEnrollDate" runat="server" CssClass="label">Enrollment Date</asp:Label>
										</td>
										<td class="contentcell4">
											<%--<asp:TextBox ID="txtEnrollDate" TabIndex="5" CssClass="TextBoxDate" runat="server"></asp:TextBox>&nbsp;<a
											onclick="javascript:OpenCalendar('ClsSect','txtEnrollDate', true, 1945)"><img id="IMG1"
												src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle" border="0"
												runat="server"></a>--%>
											<telerik:RadDatePicker ID="txtEnrollDate" runat="server" MinDate="1/1/1945" AutoPostBack="true"
												OnSelectedDateChanged="SetConGradDate" Calendar-ShowRowHeaders="false">
											</telerik:RadDatePicker>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblShiftId" runat="server" CssClass="label">Shift</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlShiftId" runat="server" CssClass="dropdownlist" TabIndex="22">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblExpStartDate" runat="server" CssClass="label">Expected Start Date</asp:Label>
										</td>
										<td class="contentcell4">
											<%--<asp:TextBox ID="txtExpStartDate" TabIndex="6" CssClass="TextBoxDate" runat="server"></asp:TextBox>&nbsp;<a
											onclick="javascript:OpenCalendar('ClsSect','txtExpStartDate', true, 1945)"><img id="Img2"
												src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle" border="0"
												runat="server"></a>--%>
											<telerik:RadDatePicker ID="txtExpStartDate" runat="server" MinDate="1/1/1945" AutoPostBack="true"
												OnSelectedDateChanged="SetConGradDate" Calendar-ShowRowHeaders="false">
											</telerik:RadDatePicker>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblEdLvlId" runat="server" CssClass="label">Education Level</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlEdLvlId" runat="server" CssClass="dropdownlist" TabIndex="23">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblStartDate" runat="server" CssClass="label">Version Start Date</asp:Label>
										</td>
										<td class="contentcell4">
											<%-- <asp:TextBox ID="txtStartDate" TabIndex="7" CssClass="TextBoxDate" runat="server"></asp:TextBox>&nbsp;<a
											onclick="javascript:OpenCalendar('ClsSect','txtStartDate', true, 1945)"><img id="Img3"
												src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle" border="0"
												runat="server"></a>--%>
											<telerik:RadDatePicker ID="txtStartDate" runat="server" MinDate="1/1/1945" AutoPostBack="true"
												OnSelectedDateChanged="SetConGradDate" Calendar-ShowRowHeaders="false">
											</telerik:RadDatePicker>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblBillingMethodId" runat="server" CssClass="label">Charging Method</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlBillingMethodId" runat="server" CssClass="dropdownlist"
												TabIndex="24">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblMidPtDate" runat="server" CssClass="label">Mid-Point Date</asp:Label>
										</td>
										<td class="contentcell4">
											<%-- <asp:TextBox ID="txtMidPtDate" TabIndex="8" CssClass="TextBoxDate" runat="server"></asp:TextBox>&nbsp;<a
											onclick="javascript:OpenCalendar('ClsSect','txtMidPtDate', true, 1945)"><img id="Img7"
												src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle" border="0"
												runat="server"></a>--%>
											<telerik:RadDatePicker ID="txtMidPtDate" runat="server" MinDate="1/1/1945" AutoPostBack="true"
												OnSelectedDateChanged="SetConGradDate" Calendar-ShowRowHeaders="false">
											</telerik:RadDatePicker>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblTuitionCategoryId" runat="server" CssClass="label">Tuition Category</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlTuitionCategoryId" runat="server" CssClass="dropdownlist"
												TabIndex="25">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblContractedGradDate" runat="server" CssClass="label">Contracted Graduation Date</asp:Label>
										</td>
										<td class="contentcell4">
											<%-- <asp:TextBox ID="txtContractedGradDate" TabIndex="9" CssClass="TextBoxDate" runat="server"></asp:TextBox>&nbsp;<a
											onclick="javascript:OpenCalendar('ClsSect','txtExpGradDate', true, 1945)"><img id="Img4"
												src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle" border="0"
												runat="server"></a>--%>
											<telerik:RadDatePicker ID="txtContractedGradDate" runat="server" Calendar-ShowRowHeaders="false"
												ToolTip="The Contracted Graduation Date must be greater then the Enrollment, Expected Start, Start, and Mid-Point dates"
												AutoPostBack="true" OnSelectedDateChanged="SetConGradDate" MinDate="1/1/1945">
											</telerik:RadDatePicker>
										</td>
										<td style="padding: 3px 3px 3px 0; vertical-align: top; background-color: White; white-space: nowrap">
											<asp:ImageButton ID="btnCalcGradDate" runat="server" CausesValidation="False" Height="20px"
												ImageUrl="~/Images/Calculate.jpg" Text="" ToolTip="Calculate Expected Grad Date"
												Width="20px" />
										</td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblRace" runat="server" CssClass="label">Race</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlRace" runat="server" CssClass="dropdownlist" TabIndex="26">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblTransferDate" runat="server" CssClass="label">Transfer Date</asp:Label>
										</td>
										<td class="contentcell4">
											<telerik:RadDatePicker ID="txtTransferDate" runat="server" MinDate="1/1/1945" Calendar-ShowRowHeaders="false">
											</telerik:RadDatePicker>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblNationality" runat="server" CssClass="label">Nationality</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<asp:DropDownList ID="ddlNationality" runat="server" CssClass="dropdownlist" TabIndex="27">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblGender" runat="server" CssClass="label"></asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlGender" runat="server" CssClass="dropdownlist" TabIndex="11">
											</asp:DropDownList>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblGeographicTypeId" runat="server" CssClass="label">Geographic Type</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<asp:DropDownList ID="ddlGeographicTypeId" runat="server" CssClass="dropdownlist"
												TabIndex="28">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblBirthDate" runat="server" CssClass="label">DOB</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<telerik:RadDatePicker ID="txtBirthDate" runat="server" MinDate="1/1/1945" Calendar-ShowRowHeaders="false" AutoPostBack="true">
											</telerik:RadDatePicker>
										</td>
										<asp:TextBox ID="txtAge" runat="server" CssClass="textbox" TabIndex="9" Visible="false"
											Width="0px"></asp:TextBox>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblAdminCriteriaId" runat="server" CssClass="label" Visible="true">Admin Criteria</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<asp:DropDownList ID="ddlAdminCriteriaId" runat="server" CssClass="dropdownlist"
												TabIndex="29" Visible="true">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblDependencyTypeId" runat="server" CssClass="label">Dependency Type</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<asp:DropDownList ID="ddlDependencyTypeId" runat="server" CssClass="dropdownlist"
												TabIndex="13">
											</asp:DropDownList>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal;">
											<asp:Label ID="lblStateId" runat="server" CssClass="label">State</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlStateID" runat="server" CssClass="dropdownlist" TabIndex="30">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" style="white-space: normal">
											<asp:Label ID="lblHousingId" runat="server" CssClass="label">Housing Type</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<asp:DropDownList ID="ddlHousingId" runat="server" CssClass="dropdownlist" TabIndex="14">
											</asp:DropDownList>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell" style="white-space: normal;">
											<asp:Label ID="lblFAAdvisorId" runat="server" CssClass="label">Financial Aid Advisor</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlFAAdvisorId" runat="server" AutoPostBack="True" CssClass="dropdownlist"
												TabIndex="31">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell" nowrap="nowrap">
											<asp:Label ID="lblMaritalStatus" runat="server" CssClass="label">Marital Status</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<asp:DropDownList ID="ddlMaritalStatus" runat="server" CssClass="dropdownlist" TabIndex="15">
											</asp:DropDownList>
										</td>
										<td class="emptycell"></td>
										<td class="contentcell">
											<asp:Label ID="lblDegCertSeekingId" runat="server" CssClass="label" Visible="true">Degree/Certificate<br />Seeking Type</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:DropDownList ID="ddlDegCertSeekingId" runat="server" CssClass="dropdownlist"
												TabIndex="32">
											</asp:DropDownList>
										</td>
									</tr>
									<tr>
										<td class="contentcell">
											<asp:Label ID="lblCitizen" runat="server" CssClass="label">Citizen</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<asp:DropDownList ID="ddlCitizen" runat="server" CssClass="dropdownlist" TabIndex="16">
											</asp:DropDownList>
										</td>
										<td class="emptycell" nowrap="nowrap"></td>
										<td class="contentcell">
											<asp:Label ID="lblBadgeNumber" runat="server" CssClass="label">Badge Number</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:TextBox ID="txtBadgeNumber" runat="server" CssClass="textbox" TabIndex="33" />
										</td>
									</tr>
									<tr>
										<td class="contentcell">
											<asp:Label ID="lblAttendTypeId" runat="server" CssClass="label">Attendance Type</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<asp:DropDownList ID="ddlAttendTypeId" runat="server" CssClass="dropdownlist" TabIndex="17">
											</asp:DropDownList>
										</td>
										<td class="emptycell" nowrap="nowrap"></td>
										<td class="contentcell" nowrap="nowrap">
											<asp:Label ID="lblSSN" runat="server" CssClass="Label">
														SSN</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<telerik:RadMaskedTextBox ID="txtSSN" runat="server" Mask="###-##-####">
											</telerik:RadMaskedTextBox>
											<asp:RegularExpressionValidator Display="None" ID="MaskedTextBoxRegularExpressionValidator"
												runat="server" ErrorMessage="SSN format is ###-##-####" ControlToValidate="txtSSN"
												ValidationExpression="^\d{3}-\d{2}-\d{4}$" />
										</td>
									</tr>
									<tr>
										<td class="contentcell">
											<asp:Label ID="lblFamilyIncomeID" runat="server" CssClass="label"></asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<asp:DropDownList ID="ddlFamilyIncome" runat="server" CssClass="dropdownlist" TabIndex="17">
											</asp:DropDownList>
										</td>
										<td class="emptycell" nowrap="nowrap"></td>
										<td class="contentcell">
											<asp:Label ID="lblTransferDate0" runat="server" CssClass="Label">Transfer Hours</asp:Label>
										</td>
										<td class="contentcell4">
											<asp:TextBox ID="txtTransferHours" runat="server" TabIndex="34" CssClass="TextBox" />
										</td>
									</tr>
									<!-- Begin new fields Disabled  First Time in School -->
									<tr>
										<td class="contentcell">
											<asp:Label ID="txtIsDisabled" runat="server" CssClass="label">Disabled</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<asp:DropDownList ID="ddlIsDisabled" DefaultMessage="Select..." runat="server" CssClass="dropdownlist" TabIndex="18">
											   <asp:ListItem Value="-1">Select</asp:ListItem>
												<asp:ListItem Value="1">Yes</asp:ListItem>
												<asp:ListItem Value="0">No</asp:ListItem>
										</asp:DropDownList>
										</td>
										<td class="emptycell" nowrap="nowrap"></td>
										<td class="contentcell">
										</td>
										<td class="contentcell4">
										  <asp:CheckBox ID="chkIsFirstTimeInSchool" runat="server" Checked="False" Visible="true"></asp:CheckBox>
											<asp:Label ID="Label3" runat="server" CssClass="Label">First-time at this school</asp:Label>
										</td>
									</tr>
										<tr>
										<td class="contentcell">
											<asp:Label ID="txtEntranceInterviewDate" runat="server" CssClass="label">Entrance Interview</asp:Label>
										</td>
										<td class="contentcell4" nowrap="nowrap">
											<telerik:RadDatePicker ID="EntranceInterviewDate" runat="server" MinDate="1/1/1945" Calendar-ShowRowHeaders="false">
											</telerik:RadDatePicker>
										</td>
										<td class="emptycell" nowrap="nowrap"></td>
										<td class="contentcell">
											
											
										</td>
										<td class="contentcell4">
											<asp:CheckBox ID="chkIsFirstTimePostSecSchool" runat="server" Checked="False" Visible="true"></asp:CheckBox>
											<asp:Label ID="Label5" runat="server" CssClass="Label">First-time at any post-secondary school</asp:Label>
										</td>
									</tr>
								</table>
							</asp:Panel>
							<!-- end content table -->
							<asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
								<table class="contenttable" style="width: 98%;">
									<tr>
										<td class="spacertables"></td>
									</tr>
									<tr>
										<td class="contentcellheader" nowrap="nowrap">
											<asp:Label ID="lblSDF" runat="server" CssClass="Label" Font-Bold="true">School Defined Fields</asp:Label>
											<asp:TextBox ID="txtProgId" runat="server" Visible="false" />
										</td>
									</tr>
									<tr>
										<td class="spacertables"></td>
									</tr>
								</table>
							</asp:Panel>
							<asp:Panel ID="pnlSDF" runat="server" EnableViewState="false" TabIndex="13" Visible="False">
							</asp:Panel>
							<!-- end content table-->
						</div>
					</td>
				</tr>
			</table>
			<table class="maincontenttable">
				<tr>
					<td class="contentcell" style="white-space: normal">
						<asp:TextBox ID="txtStuEnrollId" runat="server" Visible="false"></asp:TextBox>
						<asp:CheckBox ID="chkIsInDB" runat="server" Checked="False" Visible="false"></asp:CheckBox>
						<asp:TextBox ID="txtGender" runat="server" Visible="false"></asp:TextBox>
						<asp:TextBox ID="txtGenderDescrip" runat="server" Visible="false"></asp:TextBox>
						<asp:TextBox ID="txtMiddleName" runat="server" Visible="false"></asp:TextBox>
					</td>
					<td></td>
				</tr>
				<tr>
					<td class="contentcell" style="white-space: normal"></td>
					<td class="contentcell4">
						<asp:TextBox ID="txtLeadId" runat="server" CssClass="Textbox" Visible="false"></asp:TextBox>
					</td>
					<td>
						<asp:TextBox ID="txtPKId" runat="server" CssClass="Textbox" Visible="false"></asp:TextBox>
					</td>
				</tr>
			</table>
			&nbsp;&nbsp;&nbsp;
		</telerik:RadPane>
	</telerik:RadSplitter>
	<%--</telerik:RadAjaxPanel>--%>
	<asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
	</asp:Panel>
	<asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
		Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
	<asp:Panel ID="pnlRequiredFieldValidators" runat="server">
	</asp:Panel>
	<asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
		ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <script>
        $(function() {
            $("#kendoleadBarControl").hide();
        });
    </script>
</asp:Content>
