﻿Imports System.Diagnostics
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports FAME.Advantage.Common
Imports Advantage.Business.Logic.Layer
Imports AdvWeb.VBCode


Partial Class LeadMaster1
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtEmployerContactId As TextBox
    Protected WithEvents txtEmployerId As TextBox
    Protected WithEvents lblTitleId As Label
    Protected WithEvents ddlTitleId As DropDownList
    Protected WithEvents lblWorkPhone As Label
    Protected WithEvents txtWorkPhone As TextBox
    Protected WithEvents lblHomePhone As Label
    Protected WithEvents lblCellPhone As Label
    Protected WithEvents txtCellPhone As TextBox
    Protected WithEvents lblEmails As Label
    Protected WithEvents lblBeeper As Label
    Protected WithEvents txtBeeper As TextBox

    ' Protected EmployerContactId As String = "03D26D83-7814-4BCF-AB80-0F429C691D9B"
    Protected WithEvents ChkStatus As CheckBox
    Protected WithEvents dlstEmployerContact As DataList

    Protected WithEvents RegularExpressionValidator1 As RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator2 As RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator3 As RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator4 As RegularExpressionValidator

    Protected WithEvents txtState As TextBox
    Protected WithEvents lblExt As Label
    Protected WithEvents txtExtension As TextBox
    Protected WithEvents lblBestTime As Label
    Protected WithEvents txtBestTime As TextBox
    Protected WithEvents Textbox1 As TextBox
    Protected WithEvents Textbox2 As TextBox
    Protected WithEvents Textbox3 As TextBox
    Protected WithEvents Textbox4 As TextBox
    Protected WithEvents lblPinNumber As Label
    Protected WithEvents txtPinNumber As TextBox
    Protected WithEvents lblWorkExt As Label
    Protected WithEvents txtWorkExt As TextBox
    Protected WithEvents lblWorkBestTime As Label
    Protected WithEvents txtWorkBestTime As TextBox
    Protected WithEvents lblHomeBestTime As Label
    Protected WithEvents txtHomeBestTime As TextBox
    Protected WithEvents txtCellBestTime As TextBox
    Protected WithEvents txtRowIds As TextBox
    Protected WithEvents txtResourceId As TextBox
    Protected WithEvents Label7 As Label
    Protected WithEvents Dropdownlist1 As DropDownList
    Protected WithEvents lblAddressStatusID As Label
    Protected WithEvents ddlDriverLicenseNumber As DropDownList
    Protected WithEvents txtPhone1 As TextBox

    Protected CampusId As String
    Protected WithEvents lbl2 As Label
    Protected WithEvents chkForeign As CheckBox
    Protected m_context As HttpContext
    Protected LeadId, StudentId As String

    Private pObj As New UserPagePermissionInfo
    Dim userId As String
    Protected ModuleId As String
    Dim ResourceID As Integer
    'Dim isIPEDSApplicable As String
    Protected strDefaultCountry As String


    Private mruProvider As MRURoutines

    Private bDispSSNbyRoles As Boolean
    Private sHiddenSSNValue As String = "XXX-XX-XXXX"
#End Region
    Public strImageURL As String = ""
    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub

    Private Function GetLeadFromStateObject(ByVal paramResourceId As Integer) As StudentMRU

        Dim objStudentState As New StudentMRU

        Try
            MyBase.GlobalSearchHandler(1)

            boolSwitchCampus = Master.Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.Master.ShowHideStatusBarControl(False)

            Master.Master.PageObjectId = LeadId
            Master.Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
            Master.Master.SetHiddenControlForAudit()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If
        Dim sdfControls As New SDFComponent
        Dim fac As New UserSecurityFacade

        Session("SEARCH") = 0
        txtDate.Text = Date.Now.ToShortDateString
        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        If (Not Page.IsPostBack) Then
            CampusId = Master.Master.CurrentCampusId
            ViewState("CampusID") = CampusId
        ElseIf (Not ViewState("CampusID") Is Nothing) Then
            CampusId = ViewState("CampusID").ToString()
        End If

        ResourceID = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceID, CampusId)

        ddlCampusId.Attributes.Add("onchange", "verifycampuschange('" + CampusId + "')")

        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True AndAlso Not MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "allcampuses" Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        Trace.Warn("Begin MRU")
        Dim objStudentState As New StudentMRU
        objStudentState = GetLeadFromStateObject(170) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

            'boolSwitchCampus = False 'Reset. If not reset, the notification is shown for every student within the campus
        End With
        Trace.Warn("End MRU")
        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''
        bDispSSNbyRoles = fac.GetRolePermissionforSSN(userId, CampusId)
        'Get StudentId and LeadId

        'set the radnotification control to be used for errors
        Me.RadPopupContol = RadNotification1

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            'CType(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).ApplyValidation()
            CType(DirectCast(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder).ApplyValidation()

            ' CType(Master.Master.FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder).ApplyValidation()
            'objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder))
            'lblPhone.Text = "Phone 1"
            ViewState("MODE") = "NEW"
            BuildDropDownLists()

            If ddlCampusId.Items.FindByValue(CampusId).Value = CampusId Then
                ddlCampusId.SelectedValue = CampusId
            Else
                ddlCampusId.SelectedValue = AdvantageSession.UserState.CampusId.ToString()
            End If

            Session("ScrollValue") = 0
            strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString
            If LeadId <> "" Then
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    lblEntranceInterviewDate.Visible = True
                    txtEntranceInterviewDate.Visible = True
                    'Img7.Visible = True
                    lblHighSchoolProgramCode.Visible = True
                    txtHighSchoolProgramCode.Visible = True
                End If
                Trace.Warn("Begin Populate Values")
                GetLeadInfo(LeadId, False)
                BindLeadGroupList(LeadId)
                Trace.Warn("End Populate Values")
                txtLeadMasterID.Text = LeadId
                chkIsInDB.Checked = True
                txtPKID.Text = LeadId
            Else
                ' CType(Master.FindControl("ContentMain1"), ContentPlaceHolder).ApplyValidation()
                CType(DirectCast(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder).ApplyValidation()

                ' CType(Master.FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder).ApplyValidation()
                'objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder))
                chkIsInDB.Checked = False
                txtLeadMasterID.Text = Guid.NewGuid.ToString
                txtAssignedDate.SelectedDate = CType(Date.Now.ToShortDateString(), Date?)
            End If
            'Setup regent required fields
            SetupRegentRequiredFields()

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""

        Else
            CType(DirectCast(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder).ApplyValidation()

            'CType(Master.FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder).ApplyValidation(chkForeignZip.Checked, chkForeignPhone.Checked, chkForeignPhone2.Checked)
            'CType(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).ApplyValidation(chkForeignZip.Checked, chkForeignPhone.Checked, chkForeignPhone2.Checked)
            'objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), , chkForeignZip.Checked)
        End If

        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        'Set the Mask
        'GetInputMaskValue()

        'Check if Lead is eligible for Enrollment
        Dim isLeadEligible As String = IsLeadEligibleForEnrollment()
        Dim strLeadName As String = Trim(txtFirstName.Text) + " " & Trim(txtMiddleName.Text) + " " + Trim(txtLastName.Text)
        If ddlLeadStatus.Enabled = True Then
            If Not isLeadEligible = String.Empty Then
                If isLeadEligible = "Enroll" Then
                    lblEnrollStatus.Text = "<strong>" & strLeadName & " </strong> has satisfied all admission requirements and " & "<strong>" & " is eligible for enrollment." & "</strong>"
                    lblEnrollStatus.Visible = True
                    lnkToEnrollLead.Visible = False
                    lblOpenReqs.Visible = False
                    lblOpenReqs.Text = " and to view Admission Requirements, click "
                    lnkToOpenRequirements.Visible = False
                    lnkToOpenRequirements.Font.Bold = True
                    lnkEnrollLead.Enabled = True
                    lnkOpenRequirements.Enabled = True
                Else
                    lblEnrollStatus.Text = "<strong>" & strLeadName & " </strong> has not satisfied all admission requirements and " & "<strong>" & " is not eligible for enrollment.</strong>"
                    lblEnrollStatus.Visible = True
                    lblOpenReqs.Text = " To view Admission Requirements, please click "
                    lblOpenReqs.Visible = False
                    lnkToOpenRequirements.Visible = False
                    lnkToEnrollLead.Visible = False
                    lnkEnrollLead.Enabled = False
                    lnkOpenRequirements.Enabled = True
                End If
            Else
                lblEnrollStatus.Text = "Please select the program version to check if " & strLeadName & " has satisfied all admission requirements and is eligible for enrollment."
                lblEnrollStatus.Visible = True
                lnkToEnrollLead.Visible = False
                lblOpenReqs.Text = " Please <strong>select program version</strong> to view Admission Requirements."
                lblOpenReqs.Visible = False
                lnkToOpenRequirements.Visible = False
                lnkEnrollLead.Enabled = False
                lnkOpenRequirements.Enabled = True
            End If
        Else
            lblEnrollStatus.Text = "<strong>" & strLeadName & " </strong> has been successfully enrolled."
            lblEnrollStatus.Visible = True
            lnkToEnrollLead.Visible = False
            lblOpenReqs.Visible = True
            lblOpenReqs.Text = " To view Admission Requirements, please click "
            lblOpenReqs.Visible = False
            lnkToOpenRequirements.Visible = False
            lnkOpenRequirements.Enabled = True
            lnkEnrollLead.Enabled = False
        End If
        If chkForeignZip.Checked = True Then
            lblOtherState.Enabled = True
            lblOtherState.Visible = True
            txtOtherState.Visible = True
            ddlStateID.Enabled = False
        Else
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            ddlStateID.Enabled = True
        End If
        'IPEDSRequirements()

        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfControls.GetSDFExists(ResourceID, "AD")
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        'User Defined Fields Code
        If Trim(txtPKID.Text) <> "" Then
            sdfControls.GenerateControlsEdit(MyBase.FindControlRecursive("pnlSDF"), ResourceID, txtPKID.Text, "AD")
        Else
            sdfControls.GenerateControlsNew(MyBase.FindControlRecursive("pnlSDF"), ResourceID, "AD")
        End If

        'Code to disable all buttons if lead was already enrolled
        'to prevent data being changed after lead was enrolled
        Dim chkLeadWasEnrolled As Boolean
        Dim leadFacade As New LeadEnrollmentFacade
        chkLeadWasEnrolled = leadFacade.CheckIfLeadWasEnrolled(LeadId)
        If chkLeadWasEnrolled = True Then
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
            lnkEnrollLead.Enabled = False
            btnSave.ToolTip = "Cannot modify lead info as the lead was already enrolled"
            btnDelete.ToolTip = "Cannot delete lead as the lead was already enrolled"
        Else
            btnSave.ToolTip = ""
            btnDelete.ToolTip = ""
            '  lnkEnrollLead.Enabled = True
        End If

        Session("PrgVerId") = ddlPrgVerId.SelectedValue

        'Display Student Image

        Try

            strImageURL = GetStudentImagePath(LeadId)


            Dim AltMsg As String = String.Empty
            If strImageURL = "" Then
                strImageURL = "../images/Default100.jpg"
                AltMsg = "Photograph not available"
            End If
            myimage.Alt = AltMsg
            myimage.Src = strImageURL
            myimage.CausesValidation = False
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            myimage.Alt = "Photograph not available"
            myimage.Src = "../images/Default100.jpg"
        End Try

        txtFirstName.BackColor = Color.White
        txtLastName.BackColor = Color.White
        ddlLeadStatus.BackColor = Color.White
        ddlCampusId.BackColor = Color.White
        ddlAdmissionsRep.BackColor = Color.White
        ddlCountry.BackColor = Color.White
        txtAssignedDate.BackColor = Color.White
        txtDateApplied.BackColor = Color.White
    End Sub
    Private Sub BuildLeadGroupsDDL()
        Dim facade As New AdReqsFacade
        With chkLeadGrpId
            .DataTextField = "Descrip"
            .DataValueField = "LeadGrpId"
            .DataSource = facade.GetAllLeadGroupsForExistingLeads(CampusId, LeadId)
            .DataBind()
        End With
    End Sub
    Private Sub SetupRegentRequiredFields()
        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            'txtSSN.BackColor = Color.FromName("#ffff99")
            'txtBirthDate.BackColor = Color.FromName("#ffff99")
            lblSSN.Text = lblSSN.Text & "<font color=""red"">*</font>"
            lblBirthDate.Text = lblBirthDate.Text & "<font color=""red"">*</font>"
        End If
    End Sub
    Private Sub GetLeadInfo(ByVal LeadId As String, ByVal isCampusSwitched As Boolean)
        Dim leadInfo As New LeadFacade
        Dim boolRegent As Boolean = False
        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            boolRegent = True
        End If
        BindLeadMasterData(leadInfo.GetLeadsInfo(LeadId, boolRegent), isCampusSwitched)
    End Sub
    '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
    Private Sub BuildCampusDDL()
        'Bind the CampusGroups DrowDownList
        Dim campusGroups As New CampusGroupsFacade
        With ddlCampusId
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"

            If MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "ownleads" Then
                ''''' Code changes by Kamalesh Ahuja on 16 Aug 2010 to resolve mantis issue id 19548
                .DataSource = campusGroups.GetCampusesByUser(userId)
                ''''''
            ElseIf MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "permissiblecampuses" Then
                .DataSource = campusGroups.GetCampusesByUser(userId)
            ElseIf MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "allcampuses" Then
                ''''' Code changes by Kamalesh Ahuja on 17 Aug 2010 to resolve mantis issue id 19549
                'If SingletonAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "yes" Then
                '    .DataSource = campusGroups.GetAllCampuses()
                'Else
                '    .DataSource = campusGroups.GetCampusesByUser(userId)
                'End If
                .DataSource = campusGroups.GetAllCampuses()
                ''''''''''''''
            Else
                .DataSource = campusGroups.GetAllCampusEnrollmentByCampus(CampusId)
            End If

            .DataBind()
        End With
    End Sub
    Private Sub BuildDropDownLists()

        'BuildPrefixDDL()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Lead Status
        ddlList.Add(New AdvantageDDLDefinition(ddlLeadStatus, AdvantageDropDownListName.LeadStatus, CampusId, True, True, String.Empty))

        'Prefixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlPrefix, AdvantageDropDownListName.Prefixes, CampusId, True, True, String.Empty))

        'Campuses DDL
        'ddlList.Add(New AdvantageDDLDefinition(ddlCampusId, AdvantageDropDownListName.Campuses, CampusId,True,True,String.Empty))

        'Suffixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlSuffix, AdvantageDropDownListName.Suffixes, CampusId, True, True, String.Empty))

        'Country DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlCountry, AdvantageDropDownListName.Countries, CampusId, True, True, String.Empty))

        'Address Type
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressType, AdvantageDropDownListName.Address_Types, CampusId, True, True, String.Empty))

        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlDrivLicStateId, AdvantageDropDownListName.States, CampusId, True, True, String.Empty))

        'Races
        ddlList.Add(New AdvantageDDLDefinition(ddlRace, AdvantageDropDownListName.Races, CampusId, True, True, String.Empty))

        'Genders
        ddlList.Add(New AdvantageDDLDefinition(ddlGender, AdvantageDropDownListName.Genders, CampusId, True, True, String.Empty))

        'Marital Status
        ddlList.Add(New AdvantageDDLDefinition(ddlMaritalStatus, AdvantageDropDownListName.MaritalStatus, CampusId, True, True, String.Empty))

        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlStateID, AdvantageDropDownListName.States, CampusId, True, True, String.Empty))

        'Citizenships
        ddlList.Add(New AdvantageDDLDefinition(ddlCitizen, AdvantageDropDownListName.Citizen, CampusId, True, True, String.Empty))

        'Nationality
        ddlList.Add(New AdvantageDDLDefinition(ddlNationality, AdvantageDropDownListName.Nationality, CampusId, True, True, String.Empty))

        'Address States 
        'ddlList.Add(New AdvantageDDLDefinition(ddlAddrStateId, AdvantageDropDownListName.States, CampusId,True,True,String.Empty))

        'Shifts
        ddlList.Add(New AdvantageDDLDefinition(ddlShiftID, AdvantageDropDownListName.Shifts, CampusId, True, True, String.Empty))

        'County
        ddlList.Add(New AdvantageDDLDefinition(ddlCounty, AdvantageDropDownListName.Counties, CampusId, True, True, String.Empty))

        'Family Income
        ddlList.Add(New AdvantageDDLDefinition(ddlFamilyIncome, AdvantageDropDownListName.FamilyIncome, CampusId, True, True, String.Empty))

        'Previous Education
        ddlList.Add(New AdvantageDDLDefinition(ddlPreviousEducation, AdvantageDropDownListName.EducationLvl, CampusId, True, True, String.Empty))

        'Phone Types
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneType, AdvantageDropDownListName.Phone_Types, CampusId, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneType2, AdvantageDropDownListName.Phone_Types, CampusId, True, True, String.Empty))

        'Phone Statuses
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneStatus, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneStatus2, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Address Status
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressStatus, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Admission Reps
        'ddlList.Add(New AdvantageDDLDefinition(ddlAdmissionsRep, AdvantageDropDownListName.AdmissionsRep, Nothing))

        'Dependency Type
        ddlList.Add(New AdvantageDDLDefinition(ddlDependencyTypeId, AdvantageDropDownListName.DependencyType, CampusId, True, True, String.Empty))

        'Geographic Type
        ddlList.Add(New AdvantageDDLDefinition(ddlGeographicTypeId, AdvantageDropDownListName.GeographicType, CampusId, True, True, String.Empty))

        'Sponsor
        ddlList.Add(New AdvantageDDLDefinition(ddlSponsor, AdvantageDropDownListName.Sponsors, CampusId, True, True, String.Empty))

        'Admin Criteria
        ddlList.Add(New AdvantageDDLDefinition(ddlAdminCriteriaId, AdvantageDropDownListName.AdminCriteriaType, CampusId, True, True, String.Empty))

        'Housing Type
        ddlList.Add(New AdvantageDDLDefinition(ddlHousingId, AdvantageDropDownListName.HousingType, CampusId, True, True, String.Empty))

        'Degree Certificate Seeking Type
        ddlList.Add(New AdvantageDDLDefinition(ddlDegCertSeekingId, AdvantageDropDownListName.DegCertSeekingType, CampusId, True, True, String.Empty))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

        BuildSourceCatagoryDDL()
        BuildProgramGroupDDL()
        BuildLeadGroupsDDL()
        BuildAdmissionRepsDDL()
        '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
        If (Not Page.IsPostBack) Then
            BuildCampusDDL()
        End If
    End Sub
    Private Function UpdateLeadGroups(ByVal LeadId As String) As Integer

        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), chkLeadGrpId.Items.Count)

        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In chkLeadGrpId.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        If i <= 0 Then
            Return -1
        End If

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)

        'update Selected Jobs
        Dim leadGrpFacade As New AdReqsFacade
        If leadGrpFacade.UpdateLeadGroups(LeadId, AdvantageSession.UserState.UserName, selectedDegrees) < 0 Then
            'DisplayErrorMessage("A related record exists,you can not perform this operation")
            DisplayRadNotification("Update Error", "A related record exists,you can not perform this operation ")
            'Exit Function
        End If
        Return 0
    End Function
    Private Function ValidateLeadGroups() As Integer
        Dim strGroups As String = String.Empty
        ' Dim dsUseScheduled As DataSet
        'In For Loop Check The Number of Items Selected
        For Each item As ListItem In chkLeadGrpId.Items
            If item.Selected Then
                If strGroups = String.Empty Then
                    strGroups = item.Value.ToString
                Else
                    strGroups &= "," & item.Value.ToString
                End If

            End If
        Next

        If strGroups = String.Empty Then
            Return -1
        ElseIf strGroups.Split(CType(",", Char)).Length = 1 Then
            Return 1
        Else
            Return (New StudentEnrollmentFacade).GetSchedLeadGrpCnt_SP(strGroups)
        End If

    End Function
    Private Sub BuildPrgVersionDDL(Optional ByVal boolDisplayInActive As Boolean = False)
        ddlPrgVerId.Items.Clear()
        Dim facade As New LeadFacade
        With ddlPrgVerId
            '.DataTextField = "PrgVerDescrip"
            .DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = facade.GetAllProgramVersionByCampusAndUser(ddlProgramID.SelectedValue, CampusId, LeadId, boolDisplayInActive)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramGroupDDL()
        Dim prgGrp As New LeadFacade
        With ddlAreaId
            .DataTextField = "PrgGrpDescrip"
            .DataValueField = "PrgGrpID"
            .DataSource = prgGrp.GetAllProgramsByGroupAndLead(LeadId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlProgramID
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlPrgVerId
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceCatagoryDDL()
        Dim sourceCatagory As New LeadFacade
        With ddlSourceCategoryId
            .DataTextField = "SourceCatagoryDescrip"
            .DataValueField = "SourceCatagoryId"
            .DataSource = sourceCatagory.GetAllSourceCategoryByCampus(CampusId, LeadId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSourceTypeId
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSourceAdvertisement
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceTypeDDL(ByVal SourceCatagoryID As String, Optional ByVal boolDisplayInActive As Boolean = False)
        Dim sourceType As New LeadFacade
        ddlSourceTypeId.Items.Clear()
        With ddlSourceTypeId
            .DataTextField = "SourceTypeDescrip"
            .DataValueField = "SourceTypeId"

            Dim tb As DataSet = sourceType.GetAllSourceTypeByCampus(SourceCatagoryID, CampusId, LeadId, boolDisplayInActive)
            If tb.Tables(0).Rows.Count > 0 Then
                .DataSource = tb
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End If

        End With
    End Sub
    Private Sub BuildSourceAdvDDL(ByVal SourceTypeID As String, Optional ByVal boolDisplayInActive As Boolean = False)
        Dim sourceType As New LeadFacade
        ddlSourceAdvertisement.Items.Clear()
        With ddlSourceAdvertisement
            .DataTextField = "SourceAdvDescrip"
            .DataValueField = "SourceAdvId"
            Dim ds As DataSet = sourceType.GetAllSourceAdvertisementByCampus(SourceTypeID, CampusId, LeadId, boolDisplayInActive)
            If ds.Tables(0).Rows.Count > 0 Then
                .DataSource = ds
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End If

        End With
    End Sub
    Private Sub BuildProgramsDDL(ByVal ProgramGrpID As String, Optional ByVal boolDisplayInActive As Boolean = False)
        Dim programTypes As New LeadFacade
        ddlProgramID.Items.Clear()
        With ddlProgramID
            .DataTextField = "ProgDescrip"
            .DataValueField = "ProgId"
            .DataSource = programTypes.GetAllProgramsByCampusAndUser(ProgramGrpID, CampusId, LeadId, boolDisplayInActive)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildAdmissionRepsDDL()
        Dim userName As String
        userName = AdvantageSession.UserState.UserName

        Dim LeadAdmissionReps As New LeadFacade
        Dim DirectorAdmissionsorFrontDeskCheck As New UserSecurityFacade
        ''Find if the User is DirectorOfAdmission or FrontDesk

        Dim boolDirOrFDeskCheck As Boolean = DirectorAdmissionsorFrontDeskCheck.IsDirectorOfAdmissionsorFrontDesk(userId)
        With ddlAdmissionsRep
            .DataTextField = "fullname"
            .DataValueField = "userid"
            If boolDirOrFDeskCheck Then
                .DataSource = LeadAdmissionReps.GetAllAdmissionRepsByCampusUserIdUserRoles(CampusId, userId, userName, boolDirOrFDeskCheck)
            Else
                .DataSource = LeadAdmissionReps.GetAllAdmissionRepsByCampusAndUserId(CampusId, userId)
            End If
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub
    Private Function BuildLeadMaster() As LeadMasterInfo
        Dim leadMaster As New LeadMasterInfo
        With leadMaster
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get LeadMasterID
            .LeadMasterID = txtLeadMasterID.Text

            'Get LastName
            .LastName = txtLastName.Text

            'Get FirstName
            .FirstName = txtFirstName.Text

            'Get MiddleName
            .MiddleName = txtMiddleName.Text

            'Get StatusId 
            .Status = ddlLeadStatus.SelectedValue
            txtLeadStatusIdHidden.Text = .Status


            'Get PrefixId 
            .Prefix = ddlPrefix.SelectedValue

            'Get SuffixId 
            .Suffix = ddlSuffix.SelectedValue


            'Get BirthDate
            '.BirthDate = txtBirthDate.SelectedDate
            If txtBirthDate.SelectedDate Is Nothing Then
                .BirthDate = ""
            Else
                .BirthDate = Date.Parse(txtBirthDate.SelectedDate)
            End If

            'Get Age
            '.Age = txtAge.Text

            'Get sponsor
            .Sponsor = ddlSponsor.SelectedValue

            'Get AdmissionsRep
            .AdmissionsRep = ddlAdmissionsRep.SelectedValue

            'Get Gender
            .Gender = ddlGender.SelectedValue

            'Get Race
            .Race = ddlRace.SelectedValue

            'Get MaritalStatus
            .MaritalStatus = ddlMaritalStatus.SelectedValue

            'Get Children
            .Children = txtChildren.Text

            'Get FamilyIncome
            .FamilyIncome = ddlFamilyIncome.SelectedValue

            'Get Phone
            'phoneMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
            'If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
            '.Phone = facInputMask.RemoveMask(phoneMask, txtphone.Text)
            'Else
            .Phone = txtphone.Text
            'End If
            'If txtPhone2.Text <> "" And chkForeignPhone2.Checked = False Then
            '    .Phone2 = facInputMask.RemoveMask(phoneMask, txtPhone2.Text)
            'Else
            .Phone2 = txtPhone2.Text
            'End If
            'Get Zip
            'If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            '    zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
            '    .Zip = facInputMask.RemoveMask(zipMask, txtZip.Text)
            'Else
            .Zip = txtZip.Text
            'End If

            'Get PhoneType
            .PhoneType = ddlPhoneType.SelectedValue
            .PhoneType2 = ddlPhoneType2.SelectedValue

            If rdoPhone1.Checked Then
                If .Phone = "" And .Phone2 <> "" Then
                    .DefaultPhone = 2
                Else
                    .DefaultPhone = 1
                End If

            End If

            If rdoPhone2.Checked Then
                .DefaultPhone = 2
            End If


            'Driver License State
            .DriverLicState = ddlDrivLicStateId.SelectedValue

            'Get Phone Status
            .PhoneStatus = ddlPhoneStatus.SelectedValue
            .PhoneStatus2 = ddlPhoneStatus2.SelectedValue


            'Get AddressStatus
            .AddressStatus = ddlAddressStatus.SelectedValue

            'Get HomeEmail
            .HomeEmail = txtHomeEmail.Text

            'Get WorkEmail
            .WorkEmail = txtWorkEmail.Text

            'Get Address1
            .Address1 = txtAddress1.Text

            'Get Address2
            .Address2 = txtAddress2.Text

            'Get City
            .City = txtCity.Text

            'Get State
            .State = ddlStateID.SelectedValue

            'Get Country 
            .Country = ddlCountry.SelectedValue

            'Get County
            .County = ddlCounty.SelectedValue

            '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
            .CampusId = ddlCampusId.SelectedValue

            'Get SourceCategory
            '.SourceCategory = ddlSourceCategoryId.Value

            'Get SourceDate 
            '.SourceDate = txtSourceDate.SelectedDate
            If txtSourceDate.SelectedDate Is Nothing Then
                .SourceDate = ""
            Else
                .SourceDate = Date.Parse(txtSourceDate.SelectedDate)
            End If


            ''Get SourceType
            '.SourceType = Me.__sourcetypeid.Value

            ''Get Area
            '.Area = ddlAreaId.Value

            ''SourceAdv
            '.SourceAdvertisement = Me.__sourceadvid.Value

            'Get ExpectedStart
            '.ExpectedStart = txtExpectedStart.SelectedDate
            If txtExpectedStart.SelectedDate Is Nothing Then
                .ExpectedStart = ""
            Else
                .ExpectedStart = Date.Parse(txtExpectedStart.SelectedDate)
            End If


            'Get ProgramID
            '.ProgramID = Me.__programid.Value

            'Get ShiftID
            .ShiftID = ddlShiftID.SelectedValue

            'Get Nationality
            .Nationality = ddlNationality.SelectedValue

            'Get Citizen
            .Citizen = ddlCitizen.SelectedValue

            'Get SSN 
            'ssnMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
            'If txtSSN.Text <> "" Then
            '    .SSN = facInputMask.RemoveMask(ssnMask, txtSSN.Text)
            'Else
            If bDispSSNbyRoles Then
                .SSN = txtSSN.Text
            End If
            'End If

            'Get DriverLicState 
            .DriverLicState = ddlDrivLicStateId.SelectedValue

            'Get DriverLicNumber
            .DriverLicNumber = txtDrivLicNumber.Text

            'Get AlienNumber
            .AlienNumber = txtAlienNumber.Text

            'Notes
            ''Comments saved with linebreakcharacters as \n
            ''Modified by Saraswathi on Nov 10 2008
            .Notes = txtComments.Text.Replace(vbCrLf, " \n ")

            'Get AddressTypes
            .AddressType = ddlAddressType.SelectedValue


            '.AssignmentDate = txtAssignedDate.SelectedDate
            If txtAssignedDate.SelectedDate Is Nothing Then
                .AssignmentDate = ""
            Else
                .AssignmentDate = CType(Date.Parse(CType(txtAssignedDate.SelectedDate, String)), String)
            End If

            '.AppliedDate = txtDateApplied.SelectedDate
            If txtDateApplied.SelectedDate Is Nothing Then
                .AppliedDate = ""
            Else
                .AppliedDate = Date.Parse(txtDateApplied.SelectedDate)
            End If

            .PreviousEducation = ddlPreviousEducation.SelectedValue

            '.PrgVerId = Me.__prgverid.Value

            .OtherState = txtOtherState.Text

            If chkForeignPhone.Checked = True Then
                .ForeignPhone = 1
            Else
                .ForeignPhone = 0
            End If
            If chkForeignPhone2.Checked = True Then
                .ForeignPhone2 = 1
            Else
                .ForeignPhone2 = 0
            End If
            If chkForeignZip.Checked = True Then
                .ForeignZip = 1
            Else
                .ForeignZip = 0
            End If

            'Dependency Type
            .DependencyTypeId = ddlDependencyTypeId.SelectedValue

            'Geographic Type
            .GeographicTypeId = ddlGeographicTypeId.SelectedValue

            'Address Status
            .AddressStatus = ddlAddressStatus.SelectedValue

            'Phone Status
            .PhoneStatus = ddlPhoneStatus.SelectedValue
            .PhoneStatus2 = ddlPhoneStatus2.SelectedValue

            'Get Area,Program and Program version
            .Area = ddlAreaId.SelectedValue
            .ProgramID = ddlProgramID.SelectedValue
            .PrgVerId = ddlPrgVerId.SelectedValue


            'Get SourceCategory,Source Type and SourceAdvertisement
            .SourceCategory = ddlSourceCategoryId.SelectedValue
            .SourceType = ddlSourceTypeId.SelectedValue
            .SourceAdvertisement = ddlSourceAdvertisement.SelectedValue
            .InquiryTime = txtInquiryTime.Text
            .AdvertisementNote = txtAdvertisementNote.Text
            .AdminCriteriaId = ddlAdminCriteriaId.SelectedValue
            .HousingTypeId = ddlHousingId.SelectedValue
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                .EntranceInterviewDate = txtEntranceInterviewDate.SelectedDate
                .HighSchoolProgramCode = txtHighSchoolProgramCode.Text
            End If
            .DegCertSeekingId = ddlDegCertSeekingId.SelectedValue
        End With
        Return leadMaster
    End Function
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim leadMasterUpdate As New LeadFacade
        Dim result As String

        ''''' Code changes to Fix mantis issue id 19549 by kamalesh Ahuja on 16 Aug 2010 '''
        If MyAdvAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "no" Then
            If leadMasterUpdate.CheckLeadUser(userId, txtLeadMasterID.Text) = 0 Then

                DisplayRadNotification("Save Error", "You do not have rights to edit this lead ")
                Exit Sub
            End If
        End If
        '''''''''''''

        If Not txtBirthDate.SelectedDate Is Nothing And IsDate(txtBirthDate.SelectedDate) Then
            If DateDiff(DateInterval.Year, CDate(txtBirthDate.SelectedDate), CDate(txtDate.Text)) < 1 Then
                DisplayRadNotification("Save Error", "The DOB cannot be greater than or equal to today's date ")
                txtAge.Text = ""
                Exit Sub
            End If

            Dim intAge As Integer
            Dim intConfigAge As Integer = CType(MyAdvAppSettings.AppSettings("StudentAgeLimit"), Integer)
            Dim dob As Date = txtBirthDate.SelectedDate
            dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If

            If intAge < intConfigAge Then
                DisplayRadNotification("Save Error", "The minimum age requirement is " & intConfigAge)
                txtAge.Text = ""
                Exit Sub
            Else
                txtAge.Text = intAge
            End If
        End If

        If Not txtChildren.Text = "" Then
            Dim numOfChildren As Integer
            If (Integer.TryParse(txtChildren.Text, numOfChildren)) Then
                If (numOfChildren >= 0 AndAlso numOfChildren < 100) Then
                    txtChildren.Text = numOfChildren.ToString()
                Else
                    DisplayRadNotification("Save Error", "Children accepts only numbers, and must be from 0 to 99 ")

                    txtChildren.Text = ""
                    Exit Sub
                End If
            Else
                DisplayRadNotification("Save Error", "The field children should contain numbers ")

                txtChildren.Text = ""
                Exit Sub
            End If
        End If

        Dim strAddressType As String = String.Empty
        If ddlLeadStatus.SelectedValue = "" Then
            strAddressType = "Status is required" & vbLf
        End If


        If chkForeignZip.Checked = False Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not ddlStateID.SelectedValue = "" Or Not txtZip.Text = "" Or Not ddlCounty.SelectedValue = "" Then
            End If
        End If

        If chkForeignZip.Checked = True Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not txtOtherState.Text = "" Or Not txtZip.Text = "" Or Not ddlCounty.SelectedValue = "" Then
            End If
        End If

        If Not txtphone.Text = "" Then
            If Not chkForeignPhone.Checked And txtphone.Text.Length < 10 Then
                strAddressType &= "Domestic phone numbers must include the area code and phone number and be 10 characters in length (Phone field)" & vbLf
            End If
        End If

        If Not txtphone.Text = "" Then
            If ddlPhoneType.SelectedValue = "" Or ddlPhoneType.SelectedValue = "" Then
                strAddressType &= "Phone type is required" & vbLf
            End If
        End If

        If Not ddlPhoneType.SelectedValue = "" Then
            If txtphone.Text = "" Then
                strAddressType &= "Phone is required" & vbLf
            End If
        End If

        If Not txtPhone2.Text = "" Then
            If Not chkForeignPhone2.Checked And txtPhone2.Text.Length < 10 Then
                strAddressType &= "Domestic phone numbers must include the area code and phone number and be 10 characters in length (Phone2 field)" & vbLf
            End If
        End If

        If Not txtPhone2.Text = "" Then
            If ddlPhoneType2.SelectedValue = "" Or ddlPhoneType2.SelectedValue = "" Then
                strAddressType &= "Phone type is required" & vbLf
            End If
        End If

        If Not ddlPhoneType2.SelectedValue = "" Then
            If txtPhone2.Text = "" Then
                strAddressType &= "Phone is required" & vbLf
            End If
        End If

        If Not strAddressType = "" Then

            DisplayRadNotification("Save Error", strAddressType)
            Exit Sub
        End If



        If txtInquiryTime.Text.IndexOf("-", System.StringComparison.Ordinal) >= 0 Then
            DisplayRadNotification("Save Error", "Enter Proper Inquiry Time Format ")
            Exit Sub
        End If

        Dim inquiryTime As String = DateTime.Today.ToString("d") + " " + txtInquiryTime.Text

        Try

            Dim inqDateTime As DateTime = Convert.ToDateTime(inquiryTime)

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            DisplayRadNotification("Save Error", "Enter Proper Inquiry Time Format ")
            Exit Sub
        End Try

        If Len(txtComments.Text.Replace(vbCrLf, " \n ")) > 240 Then
            Dim lstr As Integer = Len(txtComments.Text.Replace(vbCrLf, " \n "))
            Dim errmsg As String = "Maximum characters for the comments field is 240." & " There are currently " & lstr.ToString & " characters in the field. Please note that this number includes special characters such as spaces and carriage returns"
            DisplayRadNotification("Save Error", errmsg)
            Exit Sub
        End If


        Dim errorMessage As String = String.Empty

        Dim intValidateLeadGroups As Integer

        intValidateLeadGroups = ValidateLeadGroups()

        If intValidateLeadGroups = -1 Then
            ''Student group corected to lead group
            ''Modified bySaraswathi lakshmanan on FEb 10 2011 for Rally case DE5033

            DisplayRadNotification("Save Error", "Please select a lead group")

            Exit Sub
            '**This validation has been temporarily suspended 4/3/09 DD
        ElseIf intValidateLeadGroups >= 2 Then
            'More that one Student Group with the "Use For Scheduling" setting
            'has been selected.

            DisplayRadNotification("Save Error", "Please select only one Student Group that is to be used for scheduling ")
            Exit Sub
        End If

        'Check if an international group is checked.  If so, do not validate the ssn
        Dim isIntnl As Boolean = (From item As ListItem In chkLeadGrpId.Items Let intlFound = item.Text.ToUpper().IndexOf("INT", System.StringComparison.Ordinal) >= 0 Where item.Selected = True And intlFound = True).Any()


        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            Dim strRegentmessage As String = ""
            If txtSSN.Text = "" Then
                strRegentmessage = "SSN is a required for Regent" & vbCrLf
            End If
            If txtBirthDate.SelectedDate Is Nothing Then
                strRegentmessage &= "DOB is a required for Regent" & vbCrLf
            End If
            If Not strRegentmessage = "" Then

                DisplayRadNotification("Save Error", strRegentmessage)
                Exit Sub
            End If
        End If



        If isIntnl = False Then
            If Not String.IsNullOrEmpty(txtSSN.Text) Then
                Dim strDupSsnMsg As String = leadMasterUpdate.CheckDuplicateSSN(txtFirstName.Text, txtLastName.Text, txtSSN.Text)
                If Not String.IsNullOrEmpty(strDupSsnMsg) Then

                    DisplayRadNotification("Save Error", "One or more duplicate SSNs were found.")
                    Exit Sub
                End If
            End If
        End If

        If Len(txtComments.Text) > 240 Then
            DisplayRadNotification("Save Error", "Maximum characters for comments field is 240")
        End If

        If errorMessage = "" Then
            Dim boolRegent As Boolean = False
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                boolRegent = True
            End If
            'Call Update Function in the plEmployerInfoFacade
            result = leadMasterUpdate.UpdateLeadMaster(BuildLeadMaster(), AdvantageSession.UserState.UserName, txtResumeObjective.Text, boolRegent, bDispSSNbyRoles)

            If result = "" Then
                Dim j As Integer
                j = UpdateLeadGroups(txtLeadMasterID.Text)

                '   save on syLeadStatusesChanges only if txtLeadStatusId (oldStatus) <> ddlLeadStatus.SelectedValue (newStatus)
                If ddlLeadStatus.SelectedValue <> txtLeadStatusId.Text Then
                    Dim statusChanged As New LeadStatusesInfo
                    With statusChanged
                        .LeadId = txtLeadMasterID.Text
                        .OrigStatusId = IIf(txtLeadStatusId.Text = "", Guid.Empty.ToString, txtLeadStatusId.Text)
                        .NewStatusId = ddlLeadStatus.SelectedValue
                        .ModDate = Date.Parse(txtModDate.Text)
                    End With
                    result = (New LeadStatusChangeFacade).InsertLeadStatusChange(statusChanged, AdvantageSession.UserState.UserName)
                    If Not result = "" Then
                        'DisplayErrorMessage(Result)

                        DisplayRadNotification("Save Error", result)
                        Exit Sub
                    End If
                    txtLeadStatusId.Text = ddlLeadStatus.SelectedValue
                    BuildStatusDDL(ddlLeadStatus.SelectedValue)
                    'Change made on 08/16/2006 as the lead status was bringing in all lead status
                    'when page was loaded but when data is modified and save button was clicked
                    'behaves differently,the other statuses disappear and only brings in the 
                    'saved status.BuildStatusDDL() is called to keep the behavior consistent.
                    'BuildStatusDDL("")
                    ddlLeadStatus.SelectedValue = txtLeadStatusId.Text
                    txtLeadStatusIdHidden.Text = ddlLeadStatus.SelectedValue
                End If
                BuildLeadStateObject(txtLeadMasterID.Text)
            Else

                DisplayRadNotification("Save Error", result)

                Exit Sub
            End If

            chkIsInDB.Checked = True

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim sdfid As ArrayList
            Dim sdfidValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim sdfControl As New SDFComponent
            Try
                sdfControl.DeleteSDFValue(txtPKID.Text)
                sdfid = sdfControl.GetAllLabels(pnlSDF)
                sdfidValue = sdfControl.GetAllValues(pnlSDF)
                For z = 0 To sdfid.Count - 1
                    sdfControl.InsertValues(txtPKID.Text, Mid(CType(sdfid(z).id, String), 5), CType(sdfidValue(z), String))
                Next
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Else

            DisplayRadNotification("Save Error", errorMessage)
        End If
    End Sub
    Private Sub BindLeadMasterData(leadMaster As LeadMasterInfo, ByVal isCampusSwitched As Boolean)
        ''Bind The EmployerInfo Data From The Database
        Dim facInputMasks As New InputMasksFacade
        'Dim strMask As String
        Dim zipMask As String
        'Dim ssnMask As String

        'Get the mask for phone numbers and zip
        'strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        'ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        With leadMaster

            'get IsInDB
            chkIsInDB.Checked = .IsInDB

            'get LeadMasterID
            txtLeadMasterID.Text = .LeadMasterID

            'Get LastName
            txtLastName.Text = .LastName

            'Get FirstName
            txtFirstName.Text = .FirstName

            'Get MiddleName
            txtMiddleName.Text = .MiddleName

            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrefix, .Prefix, .PrefixDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSuffix, .Suffix, .SuffixDescrip)
            If (Not isCampusSwitched) Then
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdmissionsRep, .AdmissionsRep, .AdmissionRepsDescrip)
            End If
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGender, .Gender, .GenderDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRace, .Race, .EthCodeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlMaritalStatus, .MaritalStatus, .MaritalStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFamilyIncome, .FamilyIncome, .FamilyIncomeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneType, .PhoneType, .PhoneTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneType2, .PhoneType2, .PhoneTypeDescrip2)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateID, .State, .StateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountry, .Country, .CountryDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCounty, .County, .CountyDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPreviousEducation, .PreviousEducation, .EdLvlDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlShiftID, .ShiftID, .ShiftDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlNationality, .Nationality, .NationalityDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCitizen, .Citizen, .CitizenDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDrivLicStateId, .DriverLicState, .DriverLicenseStateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressType, .AddressType, .AddressTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus, .PhoneStatus, .PhoneStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus2, .PhoneStatus2, .PhoneStatusDescrip2)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressStatus, .AddressStatus, .AddressStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDependencyTypeId, .DependencyTypeId, .DependencyTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGeographicTypeId, .GeographicTypeId, .GeographicTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSponsor, .Sponsor, .SponsorTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdminCriteriaId, .AdminCriteriaId, .AdminCriteriaDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlHousingId, .HousingTypeId, .HousingTypeDescrip)
            If (Not Page.IsPostBack) Then
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCampusId, .CampusId, .CampusDescrip)
            End If
            'Get BirthDate
            Dim bdate As DateTime
            If DateTime.TryParse(.BirthDate, bdate) Then
                txtBirthDate.SelectedDate = bdate
            Else
                txtBirthDate.Clear()
            End If

            'txtBirthDate.Text = .BirthDate
            'Try
            '    txtBirthDate.SelectedDate = .BirthDate
            'Catch ex As Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    txtBirthDate.Clear()
            'End Try

            'Get Age
            If Not txtBirthDate.SelectedDate Is Nothing Then
                Dim intAge As Integer
                Dim dob As Date = CType(txtBirthDate.SelectedDate, Date)
                'dob.ToString("yyyy/MM/dd")
                intAge = Today.Year - dob.Year
                If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso
                    dob.Day <> Today.Day) Then
                    'The current year is not yet complete. 
                    intAge -= 1
                End If
                txtAge.Text = CType(intAge, String) 'Math.Floor(DateDiff(DateInterval.Day, CDate(txtBirthDate.Text), Now) / 365)
            Else
                txtAge.Text = ""
            End If

            'Get sponsor
            ddlSponsor.SelectedValue = .Sponsor

            'Get Children
            txtChildren.Text = .Children

            'Get Phone
            txtphone.Text = .Phone
            txtPhone2.Text = .Phone2
            If .ForeignPhone = 0 Then
                txtphone.Mask = "(###)-###-####"
                txtphone.DisplayMask = "(###)-###-####"
                txtphone.DisplayPromptChar = ""
            Else
                txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
                txtphone.DisplayMask = ""
                txtphone.DisplayPromptChar = ""
                lblPhone.ToolTip = ""
            End If
            If .ForeignPhone2 = 0 Then
                txtPhone2.Mask = "(###)-###-####"
                txtPhone2.DisplayMask = "(###)-###-####"
                txtPhone2.DisplayPromptChar = ""
            Else
                txtPhone2.Mask = "aaaaaaaaaaaaaaaaaaaa"
                txtPhone2.DisplayMask = ""
                txtPhone2.DisplayPromptChar = ""
                lblPhone2.ToolTip = ""
            End If
            'Get HomeEmail
            txtHomeEmail.Text = .HomeEmail

            'Get WorkEmail
            txtWorkEmail.Text = .WorkEmail

            'Get Address1
            txtAddress1.Text = .Address1

            'Get Address2
            txtAddress2.Text = .Address2

            'Get City
            txtCity.Text = .City

            'Get Zip
            If .ForeignZip = 0 Then
                txtZip.Mask = "#####"
                lblZip.ToolTip = zipMask
            Else
                txtZip.Mask = "aaaaaaaaaaaaaaaaaaaa"
                lblZip.ToolTip = ""
            End If
            txtZip.Text = .Zip

            'Get SourceDate 
            'txtSourceDate.SelectedDate = .SourceDate
            Dim soDate As DateTime
            If DateTime.TryParse(.SourceDate, soDate) Then
                txtSourceDate.SelectedDate = soDate
            Else
                txtSourceDate.Clear()
            End If

            'Try
            '    txtSourceDate.SelectedDate = .SourceDate
            'Catch ex As Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    txtSourceDate.Clear()
            'End Try

            'Get ExpectedStart
            'txtExpectedStart.SelectedDate = .ExpectedStart
            Dim exDate As DateTime
            If DateTime.TryParse(.ExpectedStart, exDate) Then
                txtExpectedStart.SelectedDate = exDate
            Else
                txtExpectedStart.Clear()
            End If
            'Try
            '    txtExpectedStart.SelectedDate = .ExpectedStart
            'Catch ex As Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    txtExpectedStart.Clear()
            'End Try

            'Assignment Date
            'txtAssignedDate.SelectedDate = .AssignmentDate
            Dim assDate As DateTime
            If DateTime.TryParse(.AssignmentDate, assDate) Then
                txtAssignedDate.SelectedDate = assDate
            Else
                txtAssignedDate.Clear()
            End If


            'Try
            '    txtAssignedDate.SelectedDate = .AssignmentDate
            'Catch ex As Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    txtAssignedDate.Clear()
            'End Try

            'txtDateApplied.SelectedDate = .AppliedDate
            Dim appDate As DateTime
            If DateTime.TryParse(.AppliedDate, appDate) Then
                txtDateApplied.SelectedDate = appDate
            Else
                txtDateApplied.Clear()
            End If


            'Try
            '    txtDateApplied.SelectedDate = .AppliedDate
            'Catch ex As Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    txtDateApplied.Clear()
            'End Try


            If Not txtDateApplied.SelectedDate Is Nothing Then
                txtDateApplied.SelectedDate = CType(FormatDateTime(CType(txtDateApplied.SelectedDate, Date), DateFormat.ShortDate), Date?)
            End If

            If Not txtAssignedDate.SelectedDate Is Nothing Then
                txtAssignedDate.SelectedDate = CType(FormatDateTime(CType(txtAssignedDate.SelectedDate, Date), DateFormat.ShortDate), Date?)
            End If

            'Get SSN 
            If bDispSSNbyRoles Then
                txtSSN.Text = .SSN
            Else
                'txtSSNValid.Text = "Y"
                If .SSN = "" Then
                    'txtSSNValid.Text = ""
                    sHiddenSSNValue = ""
                End If
                HideSSNValue()
            End If

            'Get DriverLicNumber
            txtDrivLicNumber.Text = .DriverLicNumber

            'Get AlienNumber
            txtAlienNumber.Text = .AlienNumber

            'Notes
            ''Comments  with \n characters as linebreakcharacters vbcrlf
            ''and showed in different lines
            ''Modified by Saraswathi on Nov 10 2008
            txtComments.Text = .Notes.Replace("\n", vbCrLf)

            'Status
            'Build Status DDL based on syLeadStatusChanges table + current status.
            If Not .Status = "" Then
                BuildStatusDDL(.Status)
                ddlLeadStatus.SelectedValue = .Status
                txtLeadStatusId.Text = .Status      'store current status on hidden textbox
                txtLeadStatusIdHidden.Text = .Status ' store in hidden status textbox for validating enroll lead
            End If

            If (Not isCampusSwitched) Then
                ddlAreaId.SelectedValue = .Area
                'Get ProgramID
                If Not .Area = "" Then
                    BuildProgramsDDL(ddlAreaId.SelectedValue, True)
                    If (Not isCampusSwitched) Then
                        ddlProgramID.SelectedValue = .ProgramID
                    End If
                End If

                'Get Program Version
                If Not .ProgramID = "" Then
                    BuildPrgVersionDDL(True)
                    If (Not isCampusSwitched) Then
                        ddlPrgVerId.SelectedValue = .PrgVerId
                    End If
                End If
            Else
                ddlProgramID.Items.Clear()
                ddlProgramID.Items.Insert(0, New ListItem("Select", ""))
                ddlProgramID.SelectedIndex = 0

                ddlPrgVerId.Items.Clear()
                ddlPrgVerId.Items.Insert(0, New ListItem("Select", ""))
                ddlPrgVerId.SelectedIndex = 0

            End If


            'Get SourceCategory
            ddlSourceCategoryId.SelectedValue = .SourceCategory

            'Get SourceType
            If Not .SourceCategory = "" Then
                BuildSourceTypeDDL(ddlSourceCategoryId.SelectedValue, True)
                ddlSourceTypeId.SelectedValue = .SourceType
            End If

            'SourceAdvertisement
            If Not .SourceType = "" Then        'Or
                BuildSourceAdvDDL(ddlSourceTypeId.SelectedValue, True)
                ddlSourceAdvertisement.SelectedValue = .SourceAdvertisement
            End If

            If .ForeignPhone = True Then
                chkForeignPhone.Checked = True
            Else
                chkForeignPhone.Checked = False
            End If
            If .ForeignPhone2 = True Then
                chkForeignPhone2.Checked = True
            Else
                chkForeignPhone2.Checked = False
            End If
            If .DefaultPhone = 1 Then rdoPhone1.Checked = True
            If .DefaultPhone = 2 Then rdoPhone2.Checked = True

            If .ForeignZip = True Then
                chkForeignZip.Checked = True
                txtOtherState.Text = .OtherState
                txtOtherState.Visible = True
                lblOtherState.Visible = True
                ddlStateID.Enabled = False
            Else
                chkForeignZip.Checked = False
                txtOtherState.Visible = False
                lblOtherState.Visible = False
                ddlStateID.Enabled = True
            End If
            txtModDate.Text = CType(.ModDate, String)

            'If InStr(.StatusDescrip, "Enroll") >= 1 Then
            If .IsStatusEnrolled = True Then  'if lead was enrolled then disable the drop down
                ddlLeadStatus.Enabled = False
            End If

            If .InquiryTime IsNot Nothing Then
                txtInquiryTime.Text = .InquiryTime
            End If

            txtAdvertisementNote.Text = .AdvertisementNote

            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                txtEntranceInterviewDate.SelectedDate = .EntranceInterviewDate
                txtHighSchoolProgramCode.Text = .HighSchoolProgramCode
            End If

            If ddlDegCertSeekingId.Items.FindByValue(.DegCertSeekingId) IsNot Nothing Then
                ddlDegCertSeekingId.SelectedValue = .DegCertSeekingId
            End If
        End With
        Return
    End Sub
    Private Sub InitButtonsForLoad()
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        If Not (txtLeadMasterID.Text = "") Then
            'instantiate component
            Dim leadMaster As New LeadFacade

            If leadMaster.IsLeadEnrolled(txtLeadMasterID.Text) Then
                'DisplayErrorMessage("Lead cannot be deleted because it has at least one enrollment.")

                DisplayRadNotification("Delete Error", "Lead cannot be deleted because it has at least one enrollment. ")

                Exit Sub
            End If

            'Delete The Row Based on EmployerId 
            Dim result As String = leadMaster.DeleteLeadMaster(txtLeadMasterID.Text, Date.Parse(txtModDate.Text))

            'If Delete Fails
            If result <> "" Then
                '   Display Error Message
                ' DisplayErrorMessage(result)

                DisplayRadNotification("Delete Error", result)
                Exit Sub
            Else
                chkIsInDB.Checked = False

                Dim lscfac As New LeadStatusChangeFacade
                lscfac.DeleteLeadStatusChange(txtLeadMasterID.Text)

                'bind an empty new BankAcctInfo
                BindLeadMasterData(New LeadMasterInfo, False)

                'initialize buttons
                InitButtonsForLoad()

                'Initialize SourceDate and ExpectedStart Date
                txtSourceDate.Clear()
                txtExpectedStart.Clear()

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim SDFControl As New SDFComponent
                SDFControl.DeleteSDFValue(txtLeadMasterID.Text)
                SDFControl.GenerateControlsNew(pnlSDF, ResourceID, "AD")
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                'DisplayErrorMessage("The lead was sucessfully deleted")

                DisplayRadNotification("Delete Error", "The lead was successfully deleted")
                Response.Redirect("../pl/searchlead.aspx?resid=153&mod=AD&cmpid=" & CampusId)
            End If
        End If
    End Sub

    Private Sub txtBirthDate_SelectedDateChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtBirthDate.SelectedDateChanged
        If Not txtBirthDate.SelectedDate Is Nothing Then
            If DateDiff(DateInterval.Year, CDate(txtBirthDate.SelectedDate), CDate(txtDate.Text)) < 1 Then
                DisplayRadNotification("Date Error", "The minimum age requirement for student is 18")
                Exit Sub
            End If

            Dim intAge As Integer

            Dim dob = CType(txtBirthDate.SelectedDate, Date)
            'dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If
            txtAge.Text = CType(intAge, String) 'Math.Floor(DateDiff(DateInterval.Day, CDate(txtBirthDate.Text), Now) / 365)
        End If
    End Sub
    Private Sub BuildStatusDDL(leadStatusId As String)
        Dim facade As New LeadStatusChangeFacade
        With ddlLeadStatus
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            .DataSource = facade.GetExistingLeadStatuses(CampusId, userId, leadStatusId, LeadId) 'facade.GetAvailLeadStatuses(leadStatusId, CampusId, userId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignZip.CheckedChanged
        'CommonWebUtilities.SetFocus(Me.Page, txtAddress1)
        txtAddress1.Focus()
        If chkForeignZip.Checked = True Then
            lblOtherState.Enabled = True
            txtZip.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtZip.Text = String.Empty
            ddlStateID.SelectedIndex = 0
            txtZip.MaxLength = 20
        Else
            txtZip.Mask = "#####"
            'txtZip.DisplayMask = "#####"
            txtZip.Text = String.Empty
            txtZip.MaxLength = 5
        End If
    End Sub
    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignPhone.CheckedChanged
        If chkForeignPhone.Checked = False Then
            txtphone.Mask = "(###) ###-####"
            txtphone.DisplayMask = "(###) ###-####"
        Else
            txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtphone.DisplayMask = ""
            lblPhone.ToolTip = ""
        End If
        'Dim objCommon As New CommonWebUtilities
        'CommonWebUtilities.SetFocus(Me.Page, txtphone)
        txtphone.Focus()
    End Sub
    Private Sub chkForeignPhone2_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignPhone2.CheckedChanged
        If chkForeignPhone2.Checked = False Then
            txtPhone2.Mask = "(###) ###-####"
            txtPhone2.DisplayMask = "(###) ###-####"
        Else
            txtPhone2.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtPhone2.DisplayMask = ""
            lblPhone2.ToolTip = ""
        End If
        txtPhone2.Focus()
    End Sub
    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtOtherState.TextChanged
        txtZip.Focus()
    End Sub
    Private Sub txtCity_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtCity.TextChanged
        If chkForeignZip.Checked = True Then
            txtOtherState.Focus()
        End If
    End Sub
    Private Sub lnkOpenRequirements_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkOpenRequirements.Click
        Dim TestRequired As New CheckLeadTest
        Dim strTestRequiredResult As String
        '        Dim Result As String
        Dim strDocRequired As String


        'Check If Lead Has Not Taken a Required Test or Test Has not been passed
        Dim strTestStatus = "", strDocStatus = "", strEnrollStatus As String

        If Not ddlPrgVerId.SelectedValue = "" Then
            'Check If Lead Has Not Taken a Required Test or Test Has not been passed
            strTestStatus = TestRequired.CheckIfRequiredTest(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            'If the Lead Has not taken a required doc or has an document not  approved
            strDocStatus = TestRequired.CheckRequiredDocuments(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            Dim AdReqs As New AdReqsFacade
            Dim strReqsMetStatus As String
            strReqsMetStatus = AdReqs.CheckIfReqGroupMeetsConditions(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            If strTestStatus = "Enroll" And strDocStatus = "Enroll" And strReqsMetStatus = "Enroll" Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If
            OpenAdmissionRequirementsPagePopUp(LeadId, ddlPrgVerId.SelectedValue, strEnrollStatus)
        Else
            'If The Program Version is blank
            strTestRequiredResult = TestRequired.CheckIfRequiredTestNoProgramVersion(LeadId)
            If strTestRequiredResult = "Enroll" Then
                strTestStatus = "Enroll"
            End If

            'If the Lead Has not taken a required doc or has an document not  approved
            strDocRequired = TestRequired.CheckRequiredDocumentsNoProgramVersion(LeadId)
            If strDocRequired = "Enroll" Then
                strDocStatus = "Enroll"
            End If

            If strTestStatus = "Enroll" And strDocStatus = "Enroll" Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If
            OpenAdmissionRequirementsPagePopUp(LeadId, ddlPrgVerId.SelectedValue, strEnrollStatus)
        End If
    End Sub
    Private Sub OpenAdmissionRequirementsPagePopUp(leadId1 As String, prgVerId As String, strEnrollStatus As String)
        '   setup the properties of the new window
        Dim sb As New StringBuilder

        '   append ReqGrpId to the query string
        sb.Append("&LeadId=" + leadId1)
        '   append ReqGrpId Description to the query string
        If prgVerId = "" Then
            sb.Append("&PrgVerId=None")
        Else
            sb.Append("&PrgVerId=" + prgVerId)
        End If

        Dim strLeadName As String = Trim(txtFirstName.Text) + " " & Trim(txtMiddleName.Text) + " " + Trim(txtLastName.Text)

        sb.Append("&LeadName=" + strLeadName)
        sb.Append("&EnrollStatus=" + strEnrollStatus)
        sb.Append("&PrgVersion=" + ddlPrgVerId.SelectedItem.Text)
        sb.Append("&CampusId=" + CampusId)

        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsAdReqs
        Dim name = "AdmReqSummary1"
        Dim url As String = "../AD/RequirementsDisplay.aspx?resid=283&mod=AD" + sb.ToString
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
    Private Sub BindLeadGroupList(ByVal leadIdl As String)

        'Get Degrees data to bind the CheckBoxList
        Dim facade As New AdReqsFacade
        Dim ds As DataSet = facade.GetLeadgroupsselected(leadIdl)
        Dim jobCount As Integer = facade.GetLeadGroupCount(leadIdl)

        If jobCount >= 1 Then

            'Select the items on the CheckBoxList
            Dim i As Integer
            If Not (ds.Tables(0).Rows.Count < 0) Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim row As DataRow
                    row = ds.Tables(0).Rows(i)
                    Dim item As ListItem
                    For Each item In chkLeadGrpId.Items
                        If item.Value.ToString = DirectCast(row("LeadGrpId"), Guid).ToString Then
                            item.Selected = True
                            Exit For
                        End If
                    Next
                Next
            End If
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            Dim ds As DataSet = (New LeadFacade).GetLeadsByAdmissionsRep(ddlAdmissionsRep.SelectedValue, ddlLeadStatus.SelectedValue)
            ViewState("ListOfLeads") = ds
        End If

        If Not Trim(Request.Form("scrollposition")) = "" Then
            Dim i As Integer = CInt(Trim(Request.Form("scrollposition")))
            Session("ScrollValue") = i
        End If

        'set previous button 
        If Not LeadId Is Nothing Then btnPrevious.Enabled = Not IsFirstLead(LeadId) And Not IsEmpty()

        'set next button
        If Not LeadId Is Nothing Then btnNext.Enabled = Not IsLastLead(LeadId) And Not IsEmpty()

        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(lnkEnrollLead)
        controlsToIgnore.Add(btnNext)
        controlsToIgnore.Add(btnPrevious)
        controlsToIgnore.Add(lnkOpenRequirements)
        controlsToIgnore.Add(txtSourceDate)
        controlsToIgnore.Add(txtBirthDate)
        controlsToIgnore.Add(txtExpectedStart)
        controlsToIgnore.Add(txtAssignedDate)
        'Add javascript code to warn the user about non saved changes 
        '        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Page, controlsToIgnore)


        BindToolTip()
    End Sub
    Private Function IsFirstLead(ByVal leadIdl As String) As Boolean
        If ViewState("ListOfLeads") Is Nothing Then Return False
        Dim ds As DataSet = CType(ViewState("ListOfLeads"), DataSet)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)("LeadId") = New Guid(leadIdl) Then Return True Else Return False
        Else
            Return False
        End If
    End Function
    Private Function IsLastLead(ByVal leadIdl As String) As Boolean
        If ViewState("ListOfLeads") Is Nothing Then Return False
        Dim ds As DataSet = CType(ViewState("ListOfLeads"), DataSet)
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1)("LeadId") = New Guid(leadIdl) Then Return True Else Return False
        Else
            Return False
        End If
    End Function
    Private Function IsEmpty() As Boolean
        If ViewState("ListOfLeads") Is Nothing Then Return True
        Dim ds As DataSet = CType(ViewState("ListOfLeads"), DataSet)
        If ds.Tables(0).Rows.Count > 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrevious.Click
        Session("btnleadid") = LeadId
        LeadId = GetPreviousLead(LeadId)
        SetStateInfo(LeadId)
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click
        Session("btnleadid") = LeadId
        LeadId = GetNextLead(LeadId)
        SetStateInfo(LeadId)
    End Sub

    Private Function GetPreviousLead(ByVal leadIdl As String) As String
        If ViewState("ListOfLeads") Is Nothing Then Return ""
        Dim ds As DataSet = CType(ViewState("ListOfLeads"), DataSet)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim row As DataRow = ds.Tables(0).Rows(i)
                If row("LeadId") = New Guid(leadIdl) Then
                    If i > 0 Then
                        Return CType(ds.Tables(0).Rows(i - 1)("LeadId"), Guid).ToString
                    Else
                        Return ""
                    End If
                End If
            Next
        End If

        'nothing found
        Return ""
    End Function
    Private Function GetNextLead(ByVal leadIdl As String) As String
        If ViewState("ListOfLeads") Is Nothing Then Return ""
        Dim ds As DataSet = CType(ViewState("ListOfLeads"), DataSet)
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim row As DataRow = ds.Tables(0).Rows(i)
                If row("LeadId") = New Guid(leadIdl) Then
                    If i < ds.Tables(0).Rows.Count - 1 Then
                        Return CType(ds.Tables(0).Rows(i + 1)("LeadId"), Guid).ToString
                    Else
                        Return ""
                    End If
                End If
            Next
        End If

        'nothing found
        Return ""
    End Function
    Private Sub SetStateInfo(ByRef leadIdl As String)
        Dim objStateInfo As New AdvantageStateInfo
        'Dim state As AdvantageSessionState
        Dim arrUpp As ArrayList
        Dim pUrl As String
        Dim strVid As String
        Dim fac As New UserSecurityFacade
        Dim defaultCampusId As String

        defaultCampusId = Master.Master.CurrentCampusId

        Session("SEARCH") = 1
        'Set relevant properties on the state object
        objStateInfo.LeadId = leadIdl
        AdvantageSession.MasterLeadId = leadIdl
        objStateInfo.NameCaption = "Lead : "
        objStateInfo.NameValue = (New StudentSearchFacade).GetLeadNameByID(leadIdl)
        strVid = BuildLeadStateObject(leadIdl)

        'TODO Revise this with Balaji...defaultCampusId = AdvantageSession.UserState.CampusId.ToString

        'Redirect To Student Lead Education
        'Response.Redirect("../AD/LeadMaster1.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)
        'arrUPP = fac.GetUserResourcePermissionsForSubModule(userId, 395, "Admissions", HttpContext.Current.Request.Params("cmpid"))
        arrUpp = fac.GetUserResourcePermissionsForSubModule(userId, 395, "Admissions", defaultCampusId)
        If arrUpp.Count = 0 Then
            'User does not have permission to any resource for this submodule
            'Session("Error") = "You do not have permission to any of the pages for existing leads<br> for the campus that you are logged in to."
            'Response.Redirect("../ErrorPage.aspx")
            DisplayRadNotification("Permission Error", "You do not have permission to any of the pages for existing leads for the campus that you are logged in to.")

            leadIdl = CType(Session("btnleadid"), String)
            Session("SEARCH") = 1
            'Set relevant properties on the state object
            objStateInfo.LeadId = leadIdl
            objStateInfo.NameCaption = "Lead : "
            objStateInfo.NameValue = (New StudentSearchFacade).GetLeadNameByID(leadIdl)
            BuildLeadStateObject(leadIdl)
            'defaultCampusId = AdvantageSession.UserState.CampusId.ToString
        ElseIf fac.DoesUserHasAccessToSubModuleResource(arrUpp, 170) Then
            Dim r As New Random(DateTime.Now.Millisecond + DateTime.Now.Second * 1000 + DateTime.Now.Minute * 60000 + DateTime.Now.Minute * 3600000)
            Dim strVsi As String = CType(r.Next(), String)
            mruProvider.InsertMRU(4, leadIdl, AdvantageSession.UserState.UserId.ToString, defaultCampusId)
            Response.Redirect("../AD/LeadMaster1.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVid + "&Type=4" + "&VSI=" + strVsi, True)
        Else
            'redirect to the first page that the user has permission to for the submodule
            pUrl = BuildPartialUrl(CType(arrUpp(0), UserPagePermissionInfo))
            Response.Redirect(pUrl & "&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVid, True)
        End If

    End Sub
    Private Function BuildPartialUrl(ByVal uppInfo As UserPagePermissionInfo) As String
        Return uppInfo.Url & "?resid=" & uppInfo.ResourceId
    End Function
    Protected Sub lnkEnrollLead_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkEnrollLead.Click
        Dim intLeadStatusMappedToEnrollment As Integer
        Dim strMessage As String = ""
        Dim intlLead As String = ""

        intLeadStatusMappedToEnrollment = (New LeadStatusChangeFacade).CheckIfLeadCanBeEnrolledByStatus(userId, CampusId, txtLeadStatusIdHidden.Text)
        If intLeadStatusMappedToEnrollment >= 1 Then
            If chkForeignZip.Checked Then
                intlLead = "true"
            Else
                intlLead = "false"
            End If
            Response.Redirect("../ad/leadenrollments.aspx?resid=174&mod=AD&cmpid=" + CampusId + "&enid=" + txtLeadMasterID.Text + "&intld=" + intlLead)
        Else
            strMessage &= "The Current Lead Status needs to be mapped to Enrolled Status for the lead to be enrolled" & vbLf

            DisplayRadNotification("Enroll Error", strMessage)
            Exit Sub
        End If
    End Sub
    Protected Sub dlstEmployerContact_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstEmployerContact.ItemCommand
    End Sub
    Private Sub DdlSourceCategoryIdSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlSourceCategoryId.SelectedIndexChanged
        If Not ddlSourceCategoryId.SelectedIndex = 0 Then
            BuildSourceTypeDDL(ddlSourceCategoryId.SelectedValue)
            ddlSourceAdvertisement.Items.Clear()
            With ddlSourceAdvertisement
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            ddlSourceTypeId.Items.Clear()
            With ddlSourceTypeId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            ddlSourceAdvertisement.Items.Clear()
            With ddlSourceAdvertisement
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Private Sub DdlSourceTypeIdSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlSourceTypeId.SelectedIndexChanged
        If Not ddlSourceTypeId.SelectedIndex = 0 Then
            BuildSourceAdvDDL(ddlSourceTypeId.SelectedValue)
        End If
    End Sub
    Private Sub DdlAreaIDSelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAreaId.SelectedIndexChanged
        If Not ddlAreaId.SelectedValue = "" Then
            BuildProgramsDDL(ddlAreaId.SelectedValue)
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            ddlProgramID.Items.Clear()
            With ddlProgramID
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Protected Sub ddlProgramID_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlProgramID.SelectedIndexChanged
        If Not ddlProgramID.SelectedIndex = 0 Then
            BuildPrgVersionDDL()
        Else
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Public Function GetStudentImagePath(ByVal strStudentId As String) As String
        Dim facade As New DocumentManagementFacade
        Dim strDocumentType As String = facade.GetLeadPhotoDocumentType(LeadId, "Photo", "LeadId")
        Dim strPath As String = CType(MyAdvAppSettings.AppSettings("StudentImagePath"), String) 'SingletonAppSettings.AppSettings("DocumentPath")
        Dim strFileNameOnly As String = facade.GetLeadPhotoFileName(strStudentId, "Photo")
        Dim strFullPath As String = strPath + strDocumentType + "/" + strFileNameOnly
        Return strFullPath
    End Function
    Protected Sub myimage_ServerClick(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles myimage.ServerClick
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsAdReqs
        Const name As String = "StudentPhoto"
        Dim leadName As String = (New StudentSearchFacade).GetLeadNameByID(LeadId)
        Dim url As String = "../PL/StudentImage.aspx?id=" + LeadId + "&Name=" + leadName + "&Ent=Lead"
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
    Protected Sub lnkEnlarge_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkEnlarge.Click
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsAdReqs
        Const name As String = "StudentsPhoto"
        Dim leadName As String = (New StudentSearchFacade).GetLeadNameByID(LeadId)
        Dim url As String = "../PL/StudentImage.aspx?id=" + LeadId + "&Name=" + leadName + "&Ent=Lead"
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
    Private Function IsLeadEligibleForEnrollment() As String
        Dim testRequired As New CheckLeadTest
        Dim strEnrollStatus As String
        If Not ddlPrgVerId.SelectedValue = "" Then
            Dim strTestStatus As String = testRequired.CheckIfRequiredTest(LeadId, ddlPrgVerId.SelectedValue, CampusId)
            Dim strDocStatus As String = testRequired.CheckRequiredDocuments(LeadId, ddlPrgVerId.SelectedValue, CampusId)
            Dim strReqsMetStatus As String = (New AdReqsFacade).CheckIfReqGroupMeetsConditions(LeadId, ddlPrgVerId.SelectedValue, CampusId)
            If strTestStatus = "Enroll" And strDocStatus = "Enroll" And strReqsMetStatus = "Enroll" Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If
            Return strEnrollStatus
        End If
        Return String.Empty
    End Function

    Protected Sub lnkToOpenRequirements_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkToOpenRequirements.Click
        Dim testRequired As New CheckLeadTest
        Dim strTestRequiredResult As String
        '        Dim Result As String
        Dim strDocRequired As String


        'Check If Lead Has Not Taken a Required Test or Test Has not been passed
        Dim strTestStatus = "", strDocStatus = "", strEnrollStatus As String
        'If The Program Version is not blank
        If ddlPrgVerId.SelectedValue = "" Then
            'isplayErrorMessage("Please select Program version to view the Admission Requirements ")

            DisplayRadNotification("open Req Error", "Please select program version to view the Admission Requirements")
            Exit Sub
        End If

        If Not ddlPrgVerId.SelectedValue = "" Then
            'Check If Lead Has Not Taken a Required Test or Test Has not been passed
            strTestStatus = testRequired.CheckIfRequiredTest(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            'If the Lead Has not taken a required doc or has an document not  approved
            strDocStatus = testRequired.CheckRequiredDocuments(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            Dim adReqs As New AdReqsFacade
            Dim strReqsMetStatus As String
            strReqsMetStatus = adReqs.CheckIfReqGroupMeetsConditions(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            If strTestStatus = "Enroll" And strDocStatus = "Enroll" And strReqsMetStatus = "Enroll" Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If
            OpenAdmissionRequirementsPagePopUp(LeadId, ddlPrgVerId.SelectedValue, strEnrollStatus)
        Else
            'If The Program Version is blank
            strTestRequiredResult = testRequired.CheckIfRequiredTestNoProgramVersion(LeadId)
            If strTestRequiredResult = "Enroll" Then
                strTestStatus = "Enroll"
            End If

            'If the Lead Has not taken a required doc or has an document not  approved
            strDocRequired = testRequired.CheckRequiredDocumentsNoProgramVersion(LeadId)
            If strDocRequired = "Enroll" Then
                strDocStatus = "Enroll"
            End If

            If strTestStatus = "Enroll" And strDocStatus = "Enroll" Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If
            OpenAdmissionRequirementsPagePopUp(LeadId, ddlPrgVerId.SelectedValue, strEnrollStatus)
        End If
    End Sub
    Protected Sub lnkToEnrollLead_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkToEnrollLead.Click
        Dim strCheckIfLeadStatusMapsToEnrollment As String = (New LeadEnrollmentFacade).CheckIfLeadStatusMapsToEnrollment(CampusId, userId, txtLeadStatusIdHidden.Text)
        Dim strMessage As String = ""
        If Not strCheckIfLeadStatusMapsToEnrollment = "" Then
            strMessage &= "The Current Lead Status needs to be mapped to Enrolled Status for the lead to be enrolled" & vbLf
            'DisplayErrorMessage(strMessage)

            DisplayRadNotification("Enroll Error", strMessage)
            Exit Sub
        Else
            Response.Redirect("../ad/leadenrollments.aspx?resid=174&mod=AD&cmpid=" + CampusId + "&enid=" + txtLeadMasterID.Text)
        End If
    End Sub
    'US3433 - SSN field on the Lead Master page to visible to only certain security roles 
    Protected Sub HideSSNValue()
        txtSSN.Enabled = bDispSSNbyRoles
        If Not bDispSSNbyRoles Then
            If sHiddenSSNValue = "" Then
                txtSSN.Mask = ""
                txtSSN.DisplayMask = ""
            Else
                txtSSN.Mask = "aaaaaaaaaaa"
                txtSSN.DisplayMask = "aaaaaaaaaaa"
            End If
            txtSSN.Text = sHiddenSSNValue
            MaskedTextBoxRegularExpressionValidator.Enabled = False
            'SSNRequiredValidation()
        End If
    End Sub
    Public Function BuildLeadStateObject(ByVal leadIdl As String) As String
        Dim objStateInfo As AdvantageStateInfo
        Dim strVID As String
        Dim strLeadId As String

        strLeadId = leadIdl
        objStateInfo = mruProvider.BuildLeadStatusBar(strLeadId)

        'Create a new guid to be associated with the employer pages
        strVID = Guid.NewGuid.ToString

        Session("LeadObjectPointer") = strVID ' This step is required, as master page loses values stored in class variables
        If objStateInfo.SystemStatus = 6 Then 'If lead is enrolled set the student pointer
            Session("StudentObjectPointer") = strVID
        Else
            Session("StudentObjectPointer") = ""
        End If
        Session("StudentMRUFlag") = "false"

        'Add an entry to AdvantageSessionState for this guid and object
        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        Try
            Dim advResetUserState As User = AdvantageSession.UserState
            With advResetUserState
                .CampusId = New Guid(objStateInfo.CampusId.ToString)
            End With
            AdvantageSession.UserState = advResetUserState
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New ArgumentException(ex.Message.ToString)
        End Try

        Return strVID

    End Function

    Private Sub ddlCampusId_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCampusId.SelectedIndexChanged
        CampusId = ddlCampusId.SelectedValue.ToString()
        ViewState("CampusID") = ddlCampusId.SelectedValue
        'Dim userState = AdvantageSession.UserState
        'userState.CampusId = Guid.Parse(CampusId)
        'AdvantageSession.UserState = userState

        'Me.Master.CurrentCampusId = CampusId

        BuildDropDownLists()
        GetLeadInfo(LeadId, True)
        BindLeadGroupList(LeadId)
    End Sub
End Class















