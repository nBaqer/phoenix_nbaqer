<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master" Inherits="Countries" CodeFile="Countries.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script type="text/javascript">

       function OldPageResized(sender, args) {
           $telerik.repaintChildren(sender);
       }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>

                                        <asp:RadioButtonList ID="radStatus" CssClass="label" AutoPostBack="true" runat="Server" RepeatDirection="Horizontal">
                                            <asp:ListItem Text="Active" Selected="True" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstCountries" runat="server" Width="100%" DataKeyField="CountryId">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server" CausesValidation="False" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'></asp:ImageButton>
                                        <asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" CausesValidation="False" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'></asp:ImageButton>
                                        <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Container.DataItem("StatusId")%>' />
                                        <asp:LinkButton Text='<%# Container.DataItem("CountryDescrip")%>' runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("CountryId")%>' ID="Linkbutton1" CausesValidation="False" />
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">


                <asp:Panel ID="pnlRHS" runat="server">
                    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                                <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CommandName="new" CausesValidation="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                            </td>

                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                    <!--begin right column-->
                    <table id="Table5" cellspacing="0" cellpadding="0" width="100%" border="0" class="maincontenttable">
                        <tr>
                            <td class="detailsframe">
                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <!-- begin table content-->
                                    <!-- Two column template with no spacer in between cells -->
                                    <!-- used in placement module for maintenance pages -->

                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                        <br />
                                        <tr>
                                            <td class="contentcell" style="white-space: normal; width: 30%;">
                                                <asp:Label ID="lblCountryCode" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCountryCode" runat="server" CssClass="textbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="white-space: normal">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" AutoPostBack="false" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="white-space: normal">
                                                <asp:Label ID="lblCountryDescrip" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCountryDescrip" runat="server" CssClass="textbox" MaxLength="128" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="white-space: normal">
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="textbox" AutoPostBack="false"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">&nbsp;</td>
                                            <td class="contentcell4">
                                                <asp:CheckBox ID="chkIsDefault" runat="server" Text="DefaultCountry" CssClass="label" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="white-space: normal">
                                                <asp:TextBox ID="txtCountryId" runat="server" CssClass="label" Visible="false"></asp:TextBox></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="white-space: normal"></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                    </table>
                                    <!--end table content-->


                                    <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="Label" Width="10%"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="Label" Width="10%"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <asp:Panel ID="pnlRegent" runat="server" Visible="false">
                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center" style="border: 1px solid #ebebeb;">
                        <tr>
                            <td class="contentcellheader" nowrap colspan="6" style="border-top: 0px; border-right: 0px; border-left: 0px">
                                <asp:Label ID="label2" runat="server" Font-Bold="true" CssClass="label">Map To Regent</asp:Label></td>
                        </tr>
                        <tr>
                            <td class="contentcellcitizenship" colspan="6" style="padding-top: 12px">
                                <asp:Panel ID="pnlRegentAwardChild" TabIndex="12" runat="server" Width="100%">
                                    <table width="100%">
                                        <tr>
                                            <td width="35%" nowrap class="twocolumnlabelcell">
                                                <asp:Label ID="lblregentterm" runat="server" CssClass="label">Country Code<span style="color: red">*</span></asp:Label></td>
                                            <td width="65%" nowrap class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlRegentCode" runat="server" CssClass="Textbox" TabIndex="6"></asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" ErrorMessage="CustomValidator"
            Display="None"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True"
            ShowSummary="False"></asp:ValidationSummary>
    </div>
</asp:Content>
