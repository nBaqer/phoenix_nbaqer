﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="UnassignedLeadsManagement.aspx.vb" Inherits="UnassignedLeadsManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
        <style type="text/css">
        .main {
            margin-left: 321px;
             min-width: 900px;
            /*height: 900px;*/
            /*background: #9c9;*/
             /*border:  #94C0D2 solid 1px;*/
        }

        .sidebar {
            float: left;
            width: 320px;
            /*height: 900px;*/
            /*background: #c9c;*/
            /*border: #94C0D2 solid 1px;*/
         }

        .detailC1 {
            font-weight: bold;

        }

        .detailRAlt {
            background-color: ghostwhite;
        }

        #UnAssignedWidget > svg > g > g {
            cursor: pointer;

        }

          #MyLeadWidget > svg > g > g {
            cursor: pointer;

        }

      


    </style>
    <div id="myLeadManagementPage" style="height: 100%; overflow: auto">
        <aside class="sidebar" id="widgetZone">
            <span class="k-toolbar" ><b>Filters:</b></span>
            <table id="filterCriterias" style="margin: 5px;">
                 <tr>
                    <td style="font-size: 12px; color: grey; text-align: center">
                     <span class="k-toolbar">  No Assigned Leads</span>
                        
                    </td>
                </tr>
                <tr>
                    <td >
                        <div id="UnAssignedWidget"></div>
                        
                    </td>
                </tr>
    <%--             <tr>
                    <td style="font-size: 12px; color: grey; text-align: center">
                     <span class="k-toolbar">  My Assigned Leads</span>
                        
                    </td>
                <tr>
                    <td>
                        <div id="MyLeadWidget"></div>
                    </td>
                </tr>--%>
            </table>
        </aside>
        <section class="main" >
            <article>
               <span class="k-toolbar" ><b>Lead Information:</b></span>
                 <div id="leadGrid" style="margin: 5px;  overflow: auto"></div>
                 <div id="WindowsDetails" style="display: none;">
                     <span class="k-toolbar"><span style="margin-left: 5px"> Lead Details:</span></span>
                     <table id="datailDataTable"></table>
                  </div>
                 <div id="WindowsDuplicate" style="display: none">
                     <span class="k-toolbar"> Lead Possibles Duplicates:</span>
                     <h3> Here the information of the duplicate prospected Lead (complete name, phone, email, Address, Program of interest)</h3>
                     <hr/>
                     <h3>Here the Leads list from Leads those already exists in database that can be matched with the new prospect:</h3>
                      <div id="duplicatesLeadGrid"></div>

                 </div>
            </article>
        </section>
       
    </div>
    <script src="../Kendo/js/jszip.min.js"></script>
    <script src="../Scripts/Advantage.Client.AD.js"></script>
    <script>

        $(document).ready(function () {
            var manager = new AD.UnAssignedLeadManage();

        });

    </script>

</asp:Content>

