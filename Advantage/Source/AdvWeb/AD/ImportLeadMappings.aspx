﻿<%@ Page Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ImportLeadMappings.aspx.vb" Inherits="AD_ImportLeadMappings" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>Import Lead Mappings</title>
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>

<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div>
                                <asp:DataList ID="dlstMappings" runat="server" DataKeyField="MappingId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle" Font-Bold="true" />
                                    <ItemStyle CssClass="itemstyle" />
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("MappingId")%>' Text='<%# container.dataitem("MapName")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>

                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button>
                            <asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false" Enabled="false" OnClientClick="return false;" ></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>
                        
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="scrollright2">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="80%" align="center">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="Textbox" Enabled="False"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblMapName" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtMapName" runat="server" CssClass="Textbox" Width="65%" Enabled="False"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="Textbox" Enabled="False"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:TextBox ID="txtMappingId" runat="server" CssClass="Textbox" Visible="false" ></asp:TextBox></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4"></td>
                                        </tr>

                                    </table>
                                    <!--end table content-->
                                    <asp:Panel ID="pnlRegent" runat="server" Visible="false">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center" style="border: 1px solid #ebebeb;">
                                            <tr>
                                                <td class="contentcellheader" nowrap
                                                    style="border-top: 0px; border-right: 0px; border-left: 0px">
                                                    <asp:Label ID="label2" runat="server" Font-Bold="true" CssClass="label">Map To Regent</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellcitizenship" style="padding-top: 12px">
                                                    <asp:Panel ID="pnlRegentAwardChild" TabIndex="12" runat="server" Width="80%">
                                                        <table width="100%">
                                                            <tr>
                                                                <td width="35%" nowrap class="contentcell">
                                                                    <asp:Label ID="lblregentterm" runat="server" CssClass="label">Session<span style="color: red">*</span></asp:Label></td>
                                                                <td width="65%" nowrap class="contentcell4">
                                                                    <asp:DropDownList ID="ddlSession" runat="server" CssClass="textbox" TabIndex="8"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="35%" nowrap class="contentcell">
                                                                    <asp:Label ID="lblSessionStart" runat="server" CssClass="label">Session Start<span style="color: red">*</span></asp:Label></td>
                                                                <td width="65%" nowrap class="contentcell4">
                                                                    <asp:DropDownList ID="ddlSessionStart" runat="server" CssClass="textbox" TabIndex="6"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td width="35%" nowrap class="contentcell">
                                                                    <asp:Label ID="lblSessionEnd" runat="server" CssClass="label">Session End<span style="color: red">*</span></asp:Label></td>
                                                                <td width="65%" nowrap class="contentcell4">
                                                                    <asp:DropDownList ID="ddlSessionEnd" runat="server" CssClass="textbox" TabIndex="6"></asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:TextBox ID="txtrowIds" Style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:TextBox>
                                    <asp:TextBox ID="txtresourceId" Style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:TextBox>

                                    <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap>
                                                    <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables"></td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="contentcell2">
                                                    <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false"></asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </div>
                            </asp:Panel>
                            <!--end table content-->

                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <asp:TextBox ID="txtOldCampGrpId" runat="server" Width="0px"></asp:TextBox>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
            ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
            ShowMessageBox="true"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>

</asp:Content>
