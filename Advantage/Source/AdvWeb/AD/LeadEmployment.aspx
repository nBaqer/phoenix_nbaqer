﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="LeadEmployment.aspx.vb" Inherits="LeadEmployment" %>
<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <script src="../Scripts/Advantage.Client.AD.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" Runat="Server">
    <script type="text/javascript">
        function OpenResponsibility() {
            window.open("../AD/Responsibility.aspx", "histWin", "width=500,height=150,re-sizable,scrollbars");
        }
        // document.body.onkeyup = ValidateResponsibilities();
    </script>

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
     <%-- Add class ListFrameTop2 to the table below --%>
        <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="listframetop2">
                    <br />
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="listframetop">
                                <asp:Label ID="lblFilter" CssClass="label" runat="server" Text="Filter Not Applicable" />
                            </td>
                        </tr>
                        <tr>
                            <td class="listframebottom">
                                <div class="scrollleftnofilter">
                                    <asp:DataList ID="dlstEmployerContact" runat="server">
                                        <SelectedItemStyle CssClass="selecteditem"></SelectedItemStyle>
                                        <SelectedItemTemplate>
                                        </SelectedItemTemplate>
                                        <ItemStyle CssClass="nonselecteditem"></ItemStyle>
                                        <ItemTemplate>
                                            <asp:LinkButton Text='<%# Container.DataItem("EmployerDescrip") & " - " & Container.DataItem("EmployerJobTitle")%>'
                                                runat="server" CssClass="nonselecteditem" CommandArgument='<%# Container.DataItem("StEmploymentId")%>'
                                                ID="Linkbutton2" CausesValidation="False" />
                                        </ItemTemplate>
                                    </asp:DataList></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </telerik:RadPane>


    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                        ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                                    </asp:Button>
                                    </td>
                            
                        </tr>
                    </table>
        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
            <tr>
                <td class="detailsframe">
                    <div class="scrollright2">
                        <!-- begin table content-->
                        <table width="98%" cellpadding="0" cellspacing="0" class="contenttable">
                            <tr>
                                <td nowrap class="contentcell">
                                    <asp:Label ID="lblEmployerName" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtEmployerName" CssClass="dropdownlist" runat="server" TabIndex="1"></asp:TextBox>
                                </td>
                                <td class="emptycell">
                                </td>
                                <td nowrap class="contentcell">
                                    <asp:Label ID="lblStartDate" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <%--<asp:TextBox ID="txtStartDate" runat="server" CssClass="dateemployment" TabIndex="4"></asp:TextBox>
                                    &nbsp;<a onclick="javascript:OpenCalendar('ClsSect','txtStartDate', true, 1945)"><img
                                        id="Img2" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                        align="absMiddle"></a>--%>
                                    <telerik:RadDatePicker ID="txtStartDate" MinDate="1/1/1945" runat="server">
                                                        </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap class="contentcell">
                                    <asp:Label ID="lblEmployerJobTitle" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:TextBox ID="txtEmployerJobTitle" CssClass="textbox" runat="server" TabIndex="2"></asp:TextBox>
                                </td>
                                <td class="emptycell">
                                </td>
                                <td nowrap class="contentcell">
                                    <asp:Label ID="lblEndDate" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                   <%-- <asp:TextBox ID="txtEndDate" runat="server" CssClass="dateemployment" TabIndex="5"></asp:TextBox>
                                    &nbsp;<a onclick="javascript:OpenCalendar('ClsSect','txtEndDate', true, 1945)"><img
                                        id="Img1" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                        align="absMiddle"></a>--%>
                                    <telerik:RadDatePicker ID="txtEndDate" MinDate="1/1/1945" runat="server">
                                                        </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap class="contentcell">
                                    <asp:Label ID="lblJobTitleId" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlJobTitleId" CssClass="dropdownlist" runat="server" TabIndex="2">
                                    </asp:DropDownList>
                                </td>
                                <td class="emptycell">
                                </td>
                                <td nowrap class="contentcell">
                                    &nbsp;
                                </td>
                                <td class="contentcell4">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td nowrap class="contentcell">
                                    <asp:Label ID="lblJobStatusId" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td class="contentcell4">
                                    <asp:DropDownList ID="ddlJobStatusId" CssClass="dropdownlist" runat="server" TabIndex="3">
                                    </asp:DropDownList>
                                </td>
                                <td class="emptycell">
                                </td>
                                <td nowrap class="contentcell">
                                    &nbsp;
                                </td>
                                <td class="contentcell4">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td nowrap class="contentcell">
                                    <asp:Label ID="lblComments" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td colspan="5" class="contentcell2">
                                    <asp:TextBox runat="server" CssClass="tocommentsnowrap" TextMode="MultiLine" ID="txtComments"
                                        Rows="3" Columns="60" MaxLength="300" TabIndex="6"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap class="contentcell">
                                    <asp:Label ID="lblJobResponsibilities" runat="server" CssClass="label"></asp:Label>
                                </td>
                                <td colspan="5" class="contentcell2">
                                    <asp:TextBox ID="txtJobResponsibilities" CssClass="tocommentsnowrap" runat="server"
                                        Columns="60" Rows="3" TextMode="MultiLine" MaxLength="300" TabIndex="7"></asp:TextBox>
                                   <br /> <asp:Label ID="lblRoleHelp" CssClass="label" runat="server">Please Separate The 'Job Responsibilities' with a Period(.). To See an example <a href="#BuildResume.aspx" onclick="OpenResponsibility();">Click Here</a></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="98%">
                                <tr>
                                    <td class="spacertables">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcellheader" nowrap colspan="6">
                                        <asp:Label ID="lblSDF" runat="server" Font-Bold="true" CssClass="label">School Defined Fields</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables">
                                    </td>
                                </tr>
                            </table>
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="98%">
                                <tr>
                                    <td class="spacertables">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100%">
                                        <asp:Panel ID="pnlSDF" TabIndex="13" runat="server" EnableViewState="false" Width="98%">
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:TextBox ID="txtLeadId" CssClass="label" runat="server" Visible="false"></asp:TextBox>
                        <asp:TextBox ID="txtStEmploymentId" runat="server" Visible="false"></asp:TextBox>
                        <asp:CheckBox ID="ChkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox>
                        <asp:TextBox ID="txtModDate" runat="server" Visible="False"></asp:TextBox>
                        <!--end content tables-->
                    </div>
                </td>
            </tr>
        </table>
        
    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>    
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">


</asp:Content>

