﻿Imports System.Collections
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.Common
Imports Advantage.Business.Objects

Partial Class LeadEntranceTest
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected LeadId As String
    Protected UserId As String
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        Dim strVID As String
        Dim state As AdvantageSessionState
        Dim objStateInfo As New AdvantageStateInfo

        'Get the LeadId from the state object associated with this page
        strVID = HttpContext.Current.Request.Params("VID")
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        If state.Contains(strVID) Then
            LeadId = DirectCast(state(strVID), AdvantageStateInfo).LeadId
            objStateInfo = state(strVID)
        Else
            'Send the user to the standard error page
            Session("Error") = "Your session has expired. Please log in again."
            Response.Redirect("../ErrorPage.aspx")
        End If

        ''Call mru component for leads
        'Dim objMRUFac As New MRUFacade
        'Dim ds As New DataSet

        'If Session("SEARCH") = 1 Then
        '    ds = objMRUFac.LoadAndUpdateMRU("Leads", LeadId.ToString(), Session("UserId").ToString(), HttpContext.Current.Request.Params("cmpid"))
        'Else
        '    ds = objMRUFac.LoadMRU("Leads", Session("UserId").ToString(), HttpContext.Current.Request.Params("cmpid"))
        'End If

        'objStateInfo.MRUDS = ds
        'state(strVID) = objStateInfo
        ''save current State
        'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

    End Sub

#End Region




    Private pObj As New UserPagePermissionInfo
    Protected intTestCount As Integer

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim leadFacade As New LeadEntranceFacade
        Dim getLeadCount As Integer = leadFacade.GetTestCountByLead(LeadId)
        Dim campusId As String

        '  Dim m_Context2 As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer

        Dim advantageUserState As User  = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
       campusid = Master.CurrentCampusId 
        'UserId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString

        UserId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

        ' If Not Page.IsPostBack Then
        If getLeadCount >= 1 Then
            'GenerateControlsLeadTestData(pnl1)
            GenerateControlsScoreByLead(pnl1)
        Else
            GenerateControlsNoLeadTestData(pnl1)
        End If

        Dim strVID As String
        Dim state As AdvantageSessionState
        'Get the LeadId from the state object associated with this page
        strVID = HttpContext.Current.Request.Params("VID")
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        If state.Contains(strVID) Then
            LeadId = DirectCast(state(strVID), AdvantageStateInfo).LeadId
        Else
            'Send the user to the standard error page
            Session("Error") = "Your session has expired. Please log in again."
            Response.Redirect("../ErrorPage.aspx")
        End If

        'pObj = Header1.UserPagePermission

        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        End If

        'Delete Should Always Be Disabled
        btnDelete.Enabled = False
    End Sub
    Private Sub GenerateControlsNoLeadTestData(ByVal pnl As Panel)
        Dim getDB As New LeadEntranceFacade
        'Dim intTestCount As Integer = getDB.GetControlCountAppend(LeadId)

        Dim ds As DataSet
        Dim dynLabel As Label
        '        Dim dynText As TextBox
        Dim dynCheckRequired As CheckBox
        Dim dynCheckPass As CheckBox
        Dim dynTextTaken As TextBox
        '   Dim dynTextActual As TextBox
        Dim dynLabelMinScore As Label
        '  Dim dynLabelMaxScore As Label
        Dim dynTextComments As TextBox
        Dim dynRegularExpValidator As RegularExpressionValidator
        Dim x As Integer
        Dim dynNumeric As eWorld.UI.NumericBox
        Dim dynReqValidator As RequiredFieldValidator
        Dim dynReqValidator1 As RequiredFieldValidator
        Dim prgVerId As String

        prgVerId = getDB.GetPrgVersionByLead(LeadId)
        If prgVerId <> "" Then
            ds = getDB.GetRequirementDetails(LeadId, prgVerId)
            'Get The Number Of Tests
            intTestCount = getDB.GetLeadTestCount(LeadId, prgVerId)
            Session("CountVal") = intTestCount
        Else
            ds = getDB.GetAllStandardRequirementDetails(LeadId)
            'Get The Number Of Tests
            intTestCount = getDB.GetAllStandardLeadTestCount(LeadId)
            If intTestCount = 0 Or intTestCount < 1 Then
                Exit Sub
            End If
            Session("CountVal") = intTestCount
        End If
        Try
            pnl.Controls.Add(New LiteralControl("<table cellspacing=0 cellpadding=0 width=100% align=center border=0 >"))
            'Create and Load Controls ie Textbox,Label,RangeValidator Control
            'and set the properties for controls and finally Load Controls
            If intTestCount >= 1 Then
                pnl.Controls.Add(New LiteralControl("<tr height=2><td></td></tr>"))
                For x = 0 To intTestCount - 1
                    'Create Labels For Test
                    dynLabel = New Label
                    Try
                        With dynLabel
                            .ID = "Test" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                            .Text = ds.Tables(0).Rows(x)("EntrTestDescrip").ToString()
                            .CssClass = "TextBoxStyle"

                        End With
                        pnlHeader.Visible = True
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        lblErrorMessage.Visible = True
                        pnlHeader.Visible = False
                        Dim message As String = "The entrance test generally shows up based on the program version , the lead is interested in and the education level of the lead." & vbLf
                        message &= "No entrance test has been set up for the current education level of this lead"
                        lblErrorMessage.Text = message
                        Exit Sub
                    End Try

                    'Create CheckBox for Required
                    dynCheckRequired = New CheckBox
                    With dynCheckRequired
                        .ID = "radR" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "CheckBoxStyle"
                        If ds.Tables(0).Rows(x)("Required").ToString() = "True" Or ds.Tables(0).Rows(x)("Required").ToString() = "1" Then
                            .Checked = True
                        Else
                            .Checked = False
                        End If
                        .Enabled = False
                    End With

                    'Create CheckBox for Pass
                    dynCheckPass = New CheckBox
                    With dynCheckPass
                        .ID = "radP" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "CheckBoxStyle"
                        .Enabled = False
                    End With

                    'Create TextBox For TestTaken
                    dynTextTaken = New TextBox
                    With dynTextTaken
                        .ID = "txtT" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "ToTheme2leadtestdatescore"
                    End With


                    dynRegularExpValidator = New RegularExpressionValidator
                    With dynRegularExpValidator
                        .ID = "REV" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .ControlToValidate = dynTextTaken.ID
                        '.ValidationExpression = "^(((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00|[048])))$"
                        '.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                        .ValidationExpression = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$"
                        '.ValidationExpression = "(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d"
                        '.ValidationExpression = "^((((0?[13578])|(1[02]))[\/|\-]?((0?[1-9]|[0-2][0-9])|(3[01])))|(((0?[469])|(11))[\/|\-]?((0?[1-9]|[0-2][0-9])|(30)))|(0?[2][\/\-]?(0?[1-9]|[0-2][0-9])))[\/\-]?\d{2,4}$"
                        .Display = ValidatorDisplay.None
                        .ErrorMessage = dynLabel.Text & " has an invalid date format "
                    End With

                    dynReqValidator1 = New RequiredFieldValidator
                    With dynReqValidator1
                        .ID = "ReqA1" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .ControlToValidate = dynTextTaken.ID
                        .Display = ValidatorDisplay.None
                        .ErrorMessage = "Completed Date required for " & dynLabel.Text
                    End With

                    'Create TextBox For Actual Score
                    'dynTextActual = New TextBox
                    'With dynTextActual
                    '    .ID = "txtA" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                    '    .CssClass = "ToTheme2leadtest"
                    'End With


                    dynNumeric = New eWorld.UI.NumericBox
                    With dynNumeric
                        .ID = "txtA" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "ToTheme2leadtestdatescore"
                        .PositiveNumber = True
                        .RealNumber = False
                    End With

                    dynReqValidator = New RequiredFieldValidator
                    With dynReqValidator
                        .ID = "ReqA" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .ControlToValidate = dynNumeric.ID
                        .Display = ValidatorDisplay.None
                        .ErrorMessage = "Actual Score required for " & dynLabel.Text
                    End With

                    'Create Label For MinScore
                    dynLabelMinScore = New Label
                    With dynLabelMinScore
                        .ID = "lblMin" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .Text = ds.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "CheckBoxStyle"

                    End With

                    'Create Label For MaxScore
                    'dynLabelMaxScore = New Label
                    'With dynLabelMaxScore
                    '    .ID = "lblMax" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                    '    .Text = ds.Tables(0).Rows(x)("MaxScore").ToString()
                    '    .CssClass = "CheckBoxStyle"
                    'End With

                    'Create TextBox for Comments
                    dynTextComments = New TextBox
                    With dynTextComments
                        .ID = "txtComments" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "ToTheme2leadtest"
                        .Columns = 50

                    End With


                    pnl.Controls.Add(New LiteralControl("<tr><td width=""20%"" nowrap cssclass=""DataGridHeader3"">"))
                    pnl.Controls.Add(dynLabel)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""6%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynCheckRequired)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""6%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynCheckPass)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""12%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynTextTaken)
                    pnl.Controls.Add(dynRegularExpValidator)
                    'If dynCheckRequired.Checked = True Then
                    '    pnl.Controls.Add(dynReqValidator1)
                    'End If
                    pnl.Controls.Add(New LiteralControl("</td><td width=""12%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynNumeric)
                    'If dynCheckRequired.Checked = True Then
                    '    pnl.Controls.Add(dynReqValidator)
                    'End If
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynLabelMinScore)
                    ' pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    ' pnl.Controls.Add(dynLabelMaxScore)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""28%"" nowrap cssclass=""DataGridHeader4"">"))
                    pnl.Controls.Add(dynTextComments)
                    pnl.Controls.Add(New LiteralControl("</td></tr>"))
                Next
                pnl.Controls.Add(New LiteralControl("</table>"))
                pnlDateFormat.Visible = False
            End If
        Finally
        End Try
    End Sub
    Private Sub GenerateControlsScoreByLead(ByVal pnl As Panel)
        Dim getDB As New LeadEntranceFacade
        ' Dim intTestCount As Integer = getDB.GetControlCountAppend(LeadId)

        Session("CountVal") = intTestCount
        Dim ds As New DataSet
        Dim dynLabel As Label
        '        Dim dynText As TextBox
        Dim dynCheckRequired As CheckBox
        Dim dynCheckPass As CheckBox
        Dim dynTextTaken As TextBox
        '   Dim dynTextActual As TextBox
        Dim dynLabelMinScore As Label
        '  Dim dynLabelMaxScore As Label
        Dim dynTextComments As TextBox
        Dim dynReqValidator As RequiredFieldValidator
        Dim x As Integer
        Dim dynRegularExpValidator As New RegularExpressionValidator
        Dim dynNumeric As eWorld.UI.NumericBox
        Dim dynReqValidator1 As RequiredFieldValidator
        Dim PrgVerId As String
        PrgVerId = getDB.GetPrgVersionByLead(LeadId)

        If PrgVerId <> "" Then
            intTestCount = getDB.GetLeadTestCount(LeadId, PrgVerId)
            Session("CountVal") = intTestCount
            'ds = getDB.GetScoreByTest(LeadId)
            ds = getDB.GetLeadTestAndScores(LeadId, PrgVerId)
        Else
            intTestCount = getDB.GetAllStandardLeadTestCount(LeadId)
            If intTestCount = 0 Or intTestCount < 1 Then
                Exit Sub
            End If
            Session("CountVal") = intTestCount
            ds = getDB.GetStandardLeadTestAndScores(LeadId)
        End If

        Try
            pnl.Controls.Add(New LiteralControl("<table cellspacing=0 cellpadding=0 width=100% align=center border=0 >"))
            'Create and Load Controls ie Textbox,Label,RangeValidator Control
            'and set the properties for controls and finally Load Controls
            If intTestCount >= 1 Then
                pnl.Controls.Add(New LiteralControl("<tr height=2><td></td></tr>"))

                For x = 0 To intTestCount - 1

                    'Create Labels For Test
                    dynLabel = New Label
                    Try
                        With dynLabel
                            .ID = "Test" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                            .Text = ds.Tables(0).Rows(x)("EntrTestDescrip").ToString()
                            .CssClass = "TextBoxStyle"
                        End With
                        pnlHeader.Visible = True
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                    End Try

                    'Create CheckBox for Required
                    dynCheckRequired = New CheckBox
                    With dynCheckRequired
                        .ID = "radR" & x
                        .CssClass = "CheckBoxStyle"
                        If ds.Tables(0).Rows(x)("Required").ToString() = "True" Or ds.Tables(0).Rows(x)("Required").ToString() = "1" Then
                            .Checked = True
                        Else
                            .Checked = False
                        End If
                        .Enabled = False
                    End With

                    'Create CheckBox for Pass
                    dynCheckPass = New CheckBox
                    With dynCheckPass
                        .ID = "radP" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "CheckBoxStyle"
                        If ds.Tables(0).Rows(x)("Pass").ToString() = "True" Then
                            .Checked = True
                        Else
                            .Checked = False
                        End If
                        .Enabled = False
                    End With

                    'Create TextBox For TestTaken
                    dynTextTaken = New TextBox
                    With dynTextTaken
                        .ID = "txtT" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .Text = ds.Tables(0).Rows(x)("TestTaken").ToString()
                        .CssClass = "ToTheme2leadtestdatescore"
                    End With

                    dynRegularExpValidator = New RegularExpressionValidator
                    With dynRegularExpValidator
                        .ID = "REV" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .ControlToValidate = dynTextTaken.ID
                        '.ValidationExpression = "^(((0?[1-9]|[12]\d|3[01])[\.\-\/](0?[13578]|1[02])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|[12]\d|30)[\.\-\/](0?[13456789]|1[012])[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|((0?[1-9]|1\d|2[0-8])[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?\d{2}|\d))|(29[\.\-\/]0?2[\.\-\/]((1[6-9]|[2-9]\d)?(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00)|00|[048])))$"
                        '.ValidationExpression = "(?n:^(?=\d)((?<month>(0?[13578])|1[02]|(0?[469]|11)(?!.31)|0?2(?(.29)(?=.29.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|(16|[2468][048]|[3579][26])00))|(?!.3[01])))(?<sep>[-./])(?<day>0?[1-9]|[12]\d|3[01])\k<sep>(?<year>(1[6-9]|[2-9]\d)\d{2})(?(?=\x20\d)\x20|$))?(?<time>((0?[1-9]|1[012])(:[0-5]\d){0,2}(?i:\x20[AP]M))|([01]\d|2[0-3])(:[0-5]\d){1,2})?$)"
                        .ValidationExpression = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d$"
                        '.ValidationExpression = "^((((0?[13578])|(1[02]))[\/]?((0?[1-9]|[0-2][0-9])|(3[01])))|(((0?[469])|(11))[\/]?((0?[1-9]|[0-2][0-9])|(30)))|(0?[2][\/]?(0?[1-9]|[0-2][0-9])))[\/]?\d{4}$"
                        .Display = ValidatorDisplay.None
                        .ErrorMessage = dynLabel.Text & " has an invalid date format "
                    End With

                    dynReqValidator1 = New RequiredFieldValidator
                    With dynReqValidator1
                        .ID = "ReqA1" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .ControlToValidate = dynTextTaken.ID
                        .Display = ValidatorDisplay.None
                        .ErrorMessage = "Completed Date required for " & dynLabel.Text
                    End With

                    ''Create TextBox For Actual Score
                    'dynTextActual = New TextBox
                    'With dynTextActual
                    '    .ID = "txtA" & ds.Tables(0).Rows(x)("ActualScore").ToString()
                    '    .CssClass = "ToTheme2leadtest"
                    'End With


                    dynNumeric = New eWorld.UI.NumericBox
                    With dynNumeric
                        .ID = "txtA" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "ToTheme2leadtestdatescore"
                        .Text = ds.Tables(0).Rows(x)("ActualScore").ToString()
                        .PositiveNumber = True
                        .RealNumber = False
                    End With

                    dynReqValidator = New RequiredFieldValidator
                    With dynReqValidator
                        .ID = "ReqA" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .ControlToValidate = dynNumeric.ID
                        .Display = ValidatorDisplay.None
                        .ErrorMessage = "Actual Score required for " & dynLabel.Text
                    End With

                    'Create Label For MinScore
                    dynLabelMinScore = New Label
                    With dynLabelMinScore
                        .ID = "lblMin" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .Text = ds.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "CheckBoxStyle"

                    End With

                    'Create Label For MaxScore
                    'dynLabelMaxScore = New Label
                    'With dynLabelMaxScore
                    '    .ID = "lblMax" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                    '    .Text = ds.Tables(0).Rows(x)("MaxScore").ToString()
                    '    .CssClass = "CheckBoxStyle"

                    'End With

                    'Create TextBox for Comments
                    dynTextComments = New TextBox
                    With dynTextComments
                        .ID = "txtComments" & ds.Tables(0).Rows(x)("EntrTestId").ToString() & ds.Tables(0).Rows(x)("MinScore").ToString()
                        .Text = ds.Tables(0).Rows(x)("Comments").ToString()
                        .CssClass = "ToTheme2leadtest"
                        .Columns = 50
                    End With


                    pnl.Controls.Add(New LiteralControl("<tr><td width=""20%"" nowrap cssclass=""DataGridHeader3"">"))
                    pnl.Controls.Add(dynLabel)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""6%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynCheckRequired)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""6%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynCheckPass)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""12%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynTextTaken)
                    pnl.Controls.Add(dynRegularExpValidator)
                    'If dynCheckRequired.Checked = True Then
                    '    pnl.Controls.Add(dynReqValidator1)
                    'End If
                    pnl.Controls.Add(New LiteralControl("</td><td width=""12%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynNumeric)
                    'If dynCheckRequired.Checked = True Then
                    '    pnl.Controls.Add(dynReqValidator)
                    'End If
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynLabelMinScore)
                    ' pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    '  pnl.Controls.Add(dynLabelMaxScore)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""28%"" nowrap cssclass=""DataGridHeader4"">"))
                    pnl.Controls.Add(dynTextComments)
                    pnl.Controls.Add(New LiteralControl("</td></tr>"))
                Next
                pnl.Controls.Add(New LiteralControl("</table>"))
                pnlDateFormat.Visible = False
            End If
        Finally
        End Try
    End Sub
    Private Sub GenerateControlsLeadTestData(ByVal pnl As Panel)
        Dim getDB As New LeadEntranceFacade

        'Check If Leads Exits in adLeadEntranceTest table
        Dim intTestCount As Integer = getDB.GetControlCount(LeadId)

        'Clear the Panel
        pnl.Controls.Clear()

        'Based on the programversion the lead is interested in
        'get the number of test that has not been taken yet
        Dim intTestExcep As Integer = 0


        Session("CountVal") = intTestCount
        Dim ds As New DataSet
        Dim dynLabel As Label
        '    Dim dynText As TextBox
        Dim dynCheckRequired As CheckBox
        Dim dynCheckPass As CheckBox
        Dim dynTextTaken As TextBox
        Dim dynTextActual As TextBox
        Dim dynLabelMinScore As Label
        Dim dynLabelMaxScore As Label
        Dim dynTextComments As TextBox
        '   Dim dynButton As HyperLink
        Dim x As Integer
        Dim ds2 As New DataSet
        Dim ds3 As New DataSet
        Dim dynNumeric As eWorld.UI.NumericBox

        'Get The Test Information from adLeadEntranceTest Table 
        'Including the Marks Scored
        'ds = getDB.GetLeadEntranceTestDetail(LeadId)

        'Get The Test Details For The Program Version the Lead Is Intrested In
        'Returns all test both taken and not yet taken
        'ds2 = getDB.GetTestDetails(LeadId)

        'Get The Test Details For The Program Version the Lead Is Intrested In
        'Returns test that was not taken
        'ds3 = getDB.GetTestDetailsAppend(LeadId)

        Try
            pnl.Controls.Add(New LiteralControl("<table cellspacing=0 cellpadding=0 width=100% align=center border=0>"))

            'Create and Load Controls ie Textbox,Label,RangeValidator Control
            'and set the properties for controls and finally Load Controls
            If intTestCount >= 1 Then
                pnl.Controls.Add(New LiteralControl("<tr height=2><td></td></tr>"))
                For x = 0 To intTestCount - 1
                    'Create Labels For Test
                    dynLabel = New Label
                    With dynLabel
                        .ID = "Test" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("EntrTestDescrip").ToString()
                        .CssClass = "CheckBoxStyle"
                        '.Style("left") = "5px"
                    End With

                    'Create CheckBox for Required
                    dynCheckRequired = New CheckBox
                    With dynCheckRequired
                        .ID = "radR" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        If ds2.Tables(0).Rows(x)("Required").ToString() = "True" Then
                            .Checked = True
                        End If
                        .CssClass = "CheckBoxStyle"
                        .Enabled = False
                    End With

                    'Create CheckBox for Pass
                    dynCheckPass = New CheckBox
                    With dynCheckPass
                        .ID = "radP" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        If ds.Tables(0).Rows(x)("Pass").ToString() = "True" Then
                            .Checked = True
                        End If
                        .CssClass = "CheckBoxStyle"
                        .Enabled = False
                    End With

                    'Create TextBox For TestTaken
                    dynTextTaken = New TextBox
                    With dynTextTaken
                        .ID = "txtT" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("TestTaken").ToString()
                        .CssClass = "ToTheme2leadtest"
                    End With

                    'Create TextBox For Actual Score
                    'dynTextActual = New TextBox
                    'With dynTextActual
                    '    .ID = "txtA" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                    '    .Text = ds.Tables(0).Rows(x)("ActualScore").ToString()
                    '    .CssClass = "ToTheme2leadtest"
                    'End With

                    dynNumeric = New eWorld.UI.NumericBox
                    With dynNumeric
                        .ID = "txtA" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("ActualScore").ToString()
                        .CssClass = "ToTheme2leadtest"
                        .PositiveNumber = True
                        .RealNumber = True
                    End With

                    'Create Label For MinScore
                    dynLabelMinScore = New Label
                    With dynLabelMinScore
                        .ID = "lblMin" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "CheckBoxStyle"
                    End With

                    'Create Label For MaxScore
                    dynLabelMaxScore = New Label
                    With dynLabelMaxScore
                        .ID = "lblMax" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("MaxScore").ToString()
                        .CssClass = "CheckBoxStyle"
                    End With

                    'Create TextBox for Comments
                    dynTextComments = New TextBox
                    With dynTextComments
                        .ID = "txtComments" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("Comments").ToString()
                        .CssClass = "ToTheme2leadtest"
                        .Columns = 30
                    End With

                    pnl.Controls.Add(New LiteralControl("<tr><td width=""16%"" cssclass=""DataGridHeader3"" nowrap>"))
                    pnl.Controls.Add(dynLabel)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynCheckRequired)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynCheckPass)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""12%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynTextTaken)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""12%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynNumeric)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynLabelMinScore)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynLabelMaxScore)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""28%"" nowrap cssclass=""DataGridHeader4"">"))
                    pnl.Controls.Add(dynTextComments)
                    pnl.Controls.Add(New LiteralControl("</td></tr>"))
                Next
                pnl.Controls.Add(New LiteralControl("</table>"))
            End If
            If intTestExcep >= 1 Then
                pnl.Controls.Add(New LiteralControl("<tr height=2><td></td></tr>"))
                For x = 0 To intTestExcep - 1
                    'Create Labels For Test
                    dynLabel = New Label
                    With dynLabel
                        .ID = "Test" & ds3.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds3.Tables(0).Rows(x)("EntrTestDescrip").ToString()
                        .CssClass = "CheckBoxStyle"

                    End With

                    'Create CheckBox for Required
                    dynCheckRequired = New CheckBox
                    With dynCheckRequired
                        .ID = "radR" & ds3.Tables(0).Rows(x)("EntrTestId").ToString()
                        If ds3.Tables(0).Rows(x)("Required").ToString() = "True" Then
                            .Checked = True
                        Else
                            .Checked = False
                        End If
                        .CssClass = "CheckBoxStyle"
                        .Enabled = False
                    End With

                    'Create CheckBox for Pass
                    dynCheckPass = New CheckBox
                    With dynCheckPass
                        .ID = "radP" & ds3.Tables(0).Rows(x)("EntrTestId").ToString()
                        If ds.Tables(0).Rows(x)("Pass").ToString() = "Yes" Then
                            .Checked = True
                        End If
                        .CssClass = "CheckBoxStyle"
                        .Enabled = False
                    End With

                    'Create TextBox For TestTaken
                    dynTextTaken = New TextBox
                    With dynTextTaken
                        .ID = "txtT" & ds3.Tables(0).Rows(x)("EntrTestId").ToString()
                        .CssClass = "ToTheme2leadtest"
                    End With

                    'Create TextBox For Actual Score
                    dynTextActual = New TextBox
                    With dynTextActual
                        .ID = "txtA" & ds3.Tables(0).Rows(x)("EntrTestId").ToString()
                        .CssClass = "ToTheme2leadtest"
                    End With

                    'Create Label For MinScore
                    dynLabelMinScore = New Label
                    With dynLabelMinScore
                        .ID = "lblMin" & ds3.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds3.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "CheckBoxStyle"
                    End With

                    'Create Label For MaxScore
                    dynLabelMaxScore = New Label
                    With dynLabelMaxScore
                        .ID = "lblMax" & ds3.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds3.Tables(0).Rows(x)("MaxScore").ToString()
                        .CssClass = "CheckBoxStyle"
                    End With

                    'Create TextBox for Comments
                    dynTextComments = New TextBox
                    With dynTextComments
                        .ID = "txtComments" & ds3.Tables(0).Rows(x)("EntrTestId").ToString()
                        .CssClass = "ToTheme2leadtest"
                        .Columns = 30
                    End With

                    pnl.Controls.Add(New LiteralControl("<tr><td width=""16%"" nowrap cssclass=""DataGridHeader3"">"))
                    pnl.Controls.Add(dynLabel)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynCheckRequired)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynCheckPass)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""12%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynTextTaken)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""12%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynTextActual)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynLabelMinScore)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynLabelMaxScore)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""28%"" nowrap cssclass=""DataGridHeader4"">"))
                    pnl.Controls.Add(dynTextComments)
                    pnl.Controls.Add(New LiteralControl("</td></tr>"))
                Next
                pnl.Controls.Add(New LiteralControl("</table>"))
            End If

        Finally
        End Try
    End Sub
    Private Sub GenerateControlsExistingLeadTestData(ByVal pnl As Panel)
        Dim getDB As New LeadEntranceFacade
        Dim intTestCount As Integer = getDB.GetControlCount(LeadId)
        Session("CountVal") = intTestCount

        'Clear the Panel
        pnl.Controls.Clear()

        Dim ds As New DataSet
        Dim dynLabel As Label
        '        Dim dynText As TextBox
        Dim dynCheckRequired As CheckBox
        Dim dynCheckPass As CheckBox
        Dim dynTextTaken As TextBox
        Dim dynTextActual As TextBox
        Dim dynLabelMinScore As Label
        Dim dynLabelMaxScore As Label
        Dim dynTextComments As TextBox
        '    Dim dynButton As HyperLink
        Dim x As Integer
        Dim ds2 As New DataSet
        Dim ds3 As New DataSet
        'ds = getDB.GetLeadEntranceTestDetail(LeadId)
        'ds2 = getDB.GetTestDetails(LeadId)

        Try
            pnl.Controls.Add(New LiteralControl("<table width=100% align=center cellspacing=0 cellpadding=0 border=0>"))
            'Create and Load Controls ie Textbox,Label,RangeValidator Control
            'and set the properties for controls and finally Load Controls
            If intTestCount >= 1 Then
                pnl.Controls.Add(New LiteralControl("<tr height=2><td></td></tr>"))
                For x = 0 To intTestCount - 1
                    'Create Labels For Test
                    dynLabel = New Label
                    With dynLabel
                        .ID = "Test" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("EntrTestDescrip").ToString()
                        .CssClass = "CheckBoxStyle"
                        .Style("center") = "5px"
                    End With

                    'Create CheckBox for Required
                    dynCheckRequired = New CheckBox
                    With dynCheckRequired
                        .ID = "radR" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        If ds2.Tables(0).Rows(x)("Required").ToString() = "True" Then
                            .Checked = True
                        End If
                        .CssClass = "CheckBoxStyle"
                        .Enabled = False
                    End With

                    'Create CheckBox for Pass
                    dynCheckPass = New CheckBox
                    With dynCheckPass
                        .ID = "radP" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        If ds.Tables(0).Rows(x)("Pass").ToString() = "True" Then
                            .Checked = True
                        End If
                        .CssClass = "CheckBoxStyle"
                        .Enabled = False
                    End With

                    'Create TextBox For TestTaken
                    dynTextTaken = New TextBox
                    With dynTextTaken
                        .ID = "txtT" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("TestTaken").ToString()
                        .CssClass = "ToTheme2leadtest"

                    End With

                    'Create TextBox For Actual Score
                    dynTextActual = New TextBox
                    With dynTextActual
                        .ID = "txtA" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("ActualScore").ToString()
                        .CssClass = "ToTheme2leadtest"

                    End With

                    'Create Label For MinScore
                    dynLabelMinScore = New Label
                    With dynLabelMinScore
                        .ID = "lblMin" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("MinScore").ToString()
                        .CssClass = "CheckBoxStyle"

                    End With

                    'Create Label For MaxScore
                    dynLabelMaxScore = New Label
                    With dynLabelMaxScore
                        .ID = "lblMax" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("MaxScore").ToString()
                        .CssClass = "CheckBoxStyle"

                    End With

                    'Create TextBox for Comments
                    dynTextComments = New TextBox
                    With dynTextComments
                        .ID = "txtComments" & ds.Tables(0).Rows(x)("EntrTestId").ToString()
                        .Text = ds.Tables(0).Rows(x)("Comments").ToString()
                        .CssClass = "ToTheme2leadtest"
                        .Columns = 30

                    End With

                    pnl.Controls.Add(New LiteralControl("<tr><td width=""16%"" nowrap cssclass=""DataGridHeader3"">"))
                    pnl.Controls.Add(dynLabel)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynCheckRequired)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynCheckPass)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""12%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynTextTaken)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""12%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynTextActual)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynLabelMinScore)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""8%"" nowrap cssclass=""DataGridHeader2"">"))
                    pnl.Controls.Add(dynLabelMaxScore)
                    pnl.Controls.Add(New LiteralControl("</td><td width=""28%"" nowrap cssclass=""DataGridHeader4"">"))
                    pnl.Controls.Add(dynTextComments)
                    pnl.Controls.Add(New LiteralControl("</td></tr>"))
                Next
            End If

            pnl.Controls.Add(New LiteralControl("</table>"))
        Finally
        End Try
    End Sub

    Public Function GetAllTestLabels(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        'Dim getDB As New LeadEntranceFacade
        'Dim intTestCount As Integer = getDB.GetArrayCount(LeadId)
        'Dim intTestCount As Integer = getDB.GetControlCountAppend(LeadId)
        Dim testLabel() As String = Array.CreateInstance(GetType(String), intTestCount)
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(Label) And Mid(ctl.ID, 1, 4) = "Test" Then
                testLabel.SetValue(Mid(CType(ctl, Label).ID, 5), i)
                i += 1
            End If
        Next
        'If i > 0 Then ReDim Preserve TestLabel(i - 1)
        Return testLabel
    End Function
    Public Function GetAllRequired(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        Dim getDB As New LeadEntranceFacade
        'Dim intTestCount As Integer = getDB.GetArrayCount(LeadId)
        ' Dim intTestCount As Integer = getDB.GetControlCountAppend(LeadId)
        Dim TestRequired() As String = Array.CreateInstance(GetType(String), intTestCount)
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(CheckBox) And Mid(ctl.ID, 1, 4) = "radR" Then
                If CType(ctl, CheckBox).Checked = True Then
                    TestRequired.SetValue("1", i)
                Else
                    TestRequired.SetValue("0", i)
                End If
                i += 1
            End If
        Next
        'If i > 0 Then ReDim Preserve TestRequired(i - 1)
        Return TestRequired
    End Function
    Public Function GetAllPass(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        Dim getDB As New LeadEntranceFacade
        'Dim intTestCount As Integer = getDB.GetArrayCount(LeadId)
        '  Dim intTestCount As Integer = getDB.GetControlCountAppend(LeadId)
        Dim TestPass() As String = Array.CreateInstance(GetType(String), intTestCount)
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(CheckBox) And Mid(ctl.ID, 1, 4) = "radP" Then
                If CType(ctl, CheckBox).Checked = True Then
                    TestPass.SetValue("1", i)
                Else
                    TestPass.SetValue("0", i)
                End If
                i += 1
            End If
        Next
        ' If i > 0 Then ReDim Preserve TestPass(i - 1)
        Return TestPass
    End Function
    Public Function GetAllTestTaken(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        Dim getDB As New LeadEntranceFacade
        'Dim intTestCount As Integer = getDB.GetArrayCount(LeadId)
        ' Dim intTestCount As Integer = getDB.GetControlCountAppend(LeadId)
        Dim TestTaken() As String = Array.CreateInstance(GetType(String), intTestCount)
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(TextBox) And Mid(ctl.ID, 1, 4) = "txtT" Then
                TestTaken.SetValue(CType(ctl, TextBox).Text, i)
                i += 1
            End If
        Next
        'If i > 0 Then ReDim Preserve TestTaken(i - 1)
        Return TestTaken
    End Function
    Public Function GetAllMinScore(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        'Dim getDB As New LeadEntranceFacade
        'Dim intTestCount As Integer = getDB.GetArrayCount(LeadId)
        'Dim intTestCount As Integer = getDB.GetControlCountAppend(LeadId)
        Dim MinScore() As String = Array.CreateInstance(GetType(String), intTestCount)
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(Label) And Mid(ctl.ID, 1, 6) = "lblMin" Then
                MinScore.SetValue(CType(ctl, Label).Text, i)
                i += 1
            End If
        Next
        ' ' If i > 0 Then ReDim Preserve MinScore(i - 1)
        Return MinScore
    End Function
    Public Function GetAllMaxScore(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        'Dim getDB As New LeadEntranceFacade
        'Dim intTestCount As Integer = getDB.GetArrayCount(LeadId)
        ' Dim intTestCount As Integer = getDB.GetControlCountAppend(LeadId)
        Dim MaxScore() As String = Array.CreateInstance(GetType(String), intTestCount)
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(Label) And Mid(ctl.ID, 1, 6) = "lblMax" Then
                MaxScore.SetValue(CType(ctl, Label).Text, i)
                i += 1
            End If
        Next
        '   If i > 0 Then ReDim Preserve MaxScore(i - 1)
        Return MaxScore
    End Function
    Public Function GetAllActualScore(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        'Dim getDB As New LeadEntranceFacade
        'Dim intTestCount As Integer = getDB.GetArrayCount(LeadId)
        '  Dim intTestCount As Integer = getDB.GetControlCountAppend(LeadId)
        Dim ActualScore() As String = Array.CreateInstance(GetType(String), intTestCount)
        Dim i As Integer
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(eWorld.UI.NumericBox) And Mid(ctl.ID, 1, 4) = "txtA" Then
                ActualScore.SetValue(CType(ctl, eWorld.UI.NumericBox).Text, i)
                i += 1
            End If
        Next
        Return ActualScore
    End Function
    Public Function GetAllComments(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        'Dim getDB As New LeadEntranceFacade
        'Dim intTestCount As Integer = getDB.GetArrayCount(LeadId)
        '   Dim intTestCount As Integer = getDB.GetControlCountAppend(LeadId)
        Dim Comments() As String = Array.CreateInstance(GetType(String), intTestCount)
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(TextBox) And Mid(ctl.ID, 1, 11) = "txtComments" Then
                Comments.SetValue(CType(ctl, TextBox).Text, i)
                i += 1
            End If
        Next
        ' If i > 0 Then ReDim Preserve Comments(i - 1)
        Return Comments
    End Function
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)
    '    'Set error condition
    '    'Display error in message box in the client
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim testLabel As Array
        Dim Required As Array
        Dim Pass As Array
        Dim TestTaken As Array
        Dim MinScore As Array
        '        Dim MaxScore As Array
        Dim ActualScore As Array
        Dim COmments As Array

        testLabel = GetAllTestLabels(pnl1)
        Required = GetAllRequired(pnl1)
        Pass = GetAllPass(pnl1)
        TestTaken = GetAllTestTaken(pnl1)
        MinScore = GetAllMinScore(pnl1)
        ' MaxScore = GetAllMaxScore(pnl1)
        ActualScore = GetAllActualScore(pnl1)
        COmments = GetAllComments(pnl1)

        Dim InsertData As New LeadEntranceFacade
        Dim strResult As Integer = InsertData.InsertValues(LeadId, testLabel, Required, Pass, TestTaken, ActualScore, MinScore, COmments, UserId)
        If strResult = 0 Then
            pnl1.Controls.Clear()
            GenerateControlsScoreByLead(pnl1)
            'GenerateControlsLeadTestData(pnl1)
            'GenerateControlsExistingLeadTestData(pnl1)
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        '        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
End Class
