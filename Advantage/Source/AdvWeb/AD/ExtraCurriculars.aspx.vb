Imports Fame.Common
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Data
Imports System.Collections
Imports System.Diagnostics
Imports System.IO
Imports Advantage.Business.Objects
Imports Fame.Advantage.Common

Partial Class ExtraCurriculars
    Inherits BasePage
    Protected WithEvents btnhistory As Button


#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected resourceId As String
    Protected moduleId As String
    Protected campusId, userId As String

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim sw As New StringWriter
        Dim mContext As HttpContext

        Dim advantageUserState As User = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        moduleId = HttpContext.Current.Request.Params("Mod").ToString

        mContext = HttpContext.Current
        txtResourceId.Value = resourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = resourceId
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Value, campusId)
        'Check if this page still exists in the menu while switching campus
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            If Not Page.IsPostBack Then
                'Header1.EnableHistoryButton(False)
                Session("NameCaption") = ""
                Session("EmployerCode") = ""
                Session("NameValue") = ""
                Session("IdCaption") = ""
                Session("IdValue") = ""
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"

                'Set the text box to a new guid
                txtExtraCurrId.Text = Guid.NewGuid.ToString

                ds = objListGen.SummaryListGenerator(userId, campusId)

                PopulateDataList(ds)

                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()

                Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
                If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    pnlRegent.Visible = True
                    BuildSession()
                End If
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BuildSession()
        With ddlActivityCode
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("ActivityCode", txtExtraCurrId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BtnSaveClick(sender As Object, e As EventArgs) Handles btnsave.Click
        Dim objCommon As New CommonUtilities
        Dim ds As DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New StringWriter
        Dim msg As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Try
            If ViewState("MODE") = "NEW" Then
                'Set the Primary Key value, so it can be used later by the DoUpdate method
                objCommon.PagePK = txtExtraCurrId.Text
                msg = objCommon.DoInsert(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If
                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstExtraCurr.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    Dim strReturn As String = ""
                    strReturn = (New regentFacade).updateRegentActivityCode(txtExtraCurrId.Text, ddlActivityCode.SelectedValue, Session("User"))
                End If

                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"

            ElseIf ViewState("MODE") = "EDIT" Then
                msg = objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg = "" Then
                    ds = objListGen.SummaryListGenerator(userId, campusId)
                    dlstExtraCurr.SelectedIndex = -1
                    PopulateDataList(ds)
                    ds.WriteXml(sw)
                    ViewState("ds") = sw.ToString()
                    If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                        Dim strReturn = ""
                        strReturn = (New regentFacade).updateRegentActivityCode(txtExtraCurrId.Text, ddlActivityCode.SelectedValue, Session("User"))
                    End If
                Else
                    DisplayErrorMessage(msg)
                End If
            End If
            '   set Style to Selected Item
            CommonWebUtilities.RestoreItemValues(dlstExtraCurr, txtExtraCurrId.Text)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub DisplayErrorMessage(errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Page, errorMessage)
    End Sub

    Private Sub BtnNewClick(sender As Object, e As EventArgs) Handles btnnew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New StringReader(CStr(ViewState("ds")))

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Try
            ClearRhs()
            ds.ReadXml(sr)
            PopulateDataList(ds)
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtExtraCurrId.Text = Guid.NewGuid.ToString
            'Reset Style in the Datalist
            If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                BuildSession()
            End If
            CommonWebUtilities.RestoreItemValues(dlstExtraCurr, Guid.Empty.ToString)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BtnDeleteClick(sender As Object, e As EventArgs) Handles btndelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim ds As DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New StringWriter
        Dim msg As String

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Try
            msg = objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            If msg = "" Then
                ClearRhs()
                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstExtraCurr.SelectedIndex = -1
                PopulateDataList(ds)
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                'Set the text box to a new guid
                txtExtraCurrId.Text = Guid.NewGuid.ToString
                If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    BuildSession()
                End If
                'Header1.EnableHistoryButton(False)
            Else
                DisplayErrorMessage(msg)
            End If
            CommonWebUtilities.RestoreItemValues(dlstExtraCurr, Guid.Empty.ToString)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRhs()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlrhs.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub DlstExtraCurrItemCommand(sender As Object, e As DataListCommandEventArgs) Handles dlstExtraCurr.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim sr As New StringReader(CStr(ViewState("ds")))
        Dim strGuid As String
        Dim objCommon As New CommonUtilities

        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

        Try
            Master.PageObjectId = CType(e.CommandArgument, String)
            Master.PageResourceId = CType(resourceId, Integer)
            Master.SetHiddenControlForAudit()

            strGuid = dlstExtraCurr.DataKeys(e.Item.ItemIndex).ToString()
            txtRowIds.Value = e.CommandArgument.ToString()
            objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGuid)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = e.Item.ItemIndex
            dlstExtraCurr.SelectedIndex = selIndex
            ds.ReadXml(sr)
            PopulateDataList(ds)
            If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                Try
                    ddlActivityCode.SelectedValue = (New regentFacade).getregentActivityCode(strGuid)
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlActivityCode.SelectedValue = ""
                End Try
            End If

            '   set Style to Selected Item
            CommonWebUtilities.RestoreItemValues(dlstExtraCurr, strGuid)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstExtraCurr_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstExtraCurr_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    'Private Sub ChkStatusCheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim objListGen As New DataListGenerator
    '    Dim ds As DataSet
    '    'Dim ds2 As New DataSet
    '    'Dim sw As New StringWriter
    '    Dim objCommon As New CommonUtilities
    '    'Dim dv2 As New DataView
    '    '        Dim sStatusId As String
    '    Try
    '        ClearRhs()
    '        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
    '        ViewState("MODE") = "NEW"
    '        ds = objListGen.SummaryListGenerator()
    '        PopulateDataList(ds)
    '        'Set the text box to a new guid
    '        txtExtraCurrId.Text = Guid.NewGuid.ToString
    '    Catch ex As Exception
    '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        'Redirect to error page.
    '        If ex.InnerException Is Nothing Then
    '            Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
    '        Else
    '            Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
    '        End If
    '        Response.Redirect("../ErrorPage.aspx")
    '    End Try
    'End Sub

    Private Sub PopulateDataList(ds As DataSet)

        Dim ds2 As DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was re factored by AVP on 6/19/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radstatus.SelectedItem.Text.ToLower = "active") Or (radstatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radstatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        Dim dv As New DataView(ds.Tables(0), sDataFilter, "ExtraCurrDescrip", DataViewRowState.CurrentRows)

        With dlstExtraCurr
            .DataSource = dv
            .DataBind()
        End With
        dlstExtraCurr.SelectedIndex = -1

    End Sub

    Private Sub RadStatusSelectedIndexChanged(sender As Object, e As EventArgs) Handles radstatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As DataSet
        Dim objCommon As New CommonUtilities

        Try
            ClearRhs()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtExtraCurrId.Text = Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")

        End Try
    End Sub

    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        BindToolTip()
    End Sub

End Class
