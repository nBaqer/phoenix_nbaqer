Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data

Partial Class DisplayDuplicateLeads
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        btnCheckDuplicates.Attributes.Add("onclick", "CloseWindow();return false;")

        'Put user code to initialize the page here
        Dim strLastName As String = Trim(Request.QueryString("LastName"))
        Dim strFirstName As String = Trim(Request.QueryString("FirstName"))
        Dim strSsn As String = Trim(Request.QueryString("SSN"))
        Dim strDob As String = Trim(Request.QueryString("DOB"))
        Dim strCampusId As String = Trim(Request.QueryString("CampusId"))
        Dim strUserId As String = Trim(Request.QueryString("repid"))
        Dim strMessage1 As String = Trim(Request.QueryString("Message1"))
        Dim strMessage2 As String = Trim(Request.QueryString("Message2"))
        Dim strRequestType As String = Trim(Request.QueryString("RequestType"))

        btnyes.Attributes.Add("onclick", "yesclick()")
        btnno.Attributes.Add("onclick", "noclick()")
        btncancel.Attributes.Add("onclick", "cancelclick()")

        ''''''Code Added by Kamalesh Ahuja on May 18 2010 to fix mantis issues id 18800 
        Try
            Dim strAddress As String = Trim(Request.QueryString("Address"))
            Dim strPhone As String = Trim(Request.QueryString("Phone"))
            Dim strPhone2 As String = Trim(Request.QueryString("Phone2"))

            lblMessage1.Text = strMessage1
            lblMessage2.Text = strMessage2

            Dim searchFacade As New LeadFacade

            Dim ds As DataSet
            ds = searchFacade.CheckDuplicateLead(strLastName, strFirstName, strSsn, strDob, strCampusId, strUserId, strAddress, strPhone, strPhone2)

            If ds.Tables(0).Rows.Count < 1 Then
                lblMessage1.Text = "No Duplicates Found"
                dgrdTransactionSearch.Visible = False
            End If

            dgrdTransactionSearch.DataSource = searchFacade.CheckDuplicateLead(strLastName, strFirstName, strSsn, strDob, strCampusId, strUserId, strAddress, strPhone, strPhone2)
            dgrdTransactionSearch.DataBind()

            If strRequestType = "save" Then tblButtons.Visible = True


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal ctrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In ctrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub
End Class
