﻿
Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.Common

Partial Class AD_aleadQueue
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo
    Public ObjStudentState As StudentMRU

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        'Get LeadId
        ObjStudentState = GetObjStudentState()
        If ObjStudentState Is Nothing Then
            RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        Master.PageObjectId = ObjStudentState.LeadId.ToString()
        Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
        Master.SetHiddenControlForAudit()
        Dim advantageUserState As User = AdvantageSession.UserState
        Dim resourceId = HttpContext.Current.Request.Params("resid")
        Dim campusid = Master.CurrentCampusId
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusid)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusid.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, ObjStudentState.Name)
            End If
        End If
    End Sub
End Class

