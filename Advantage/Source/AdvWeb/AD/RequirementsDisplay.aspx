<%@ Page Language="VB" AutoEventWireup="true" CodeFile="RequirementsDisplay.aspx.vb" Inherits="AD_RequirementsDisplay" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Admission Requirements Summary</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link rel="stylesheet" type="text/css" href="../CSS/localhost_lowercase.css" />
		<LINK rel="stylesheet" type="text/css" href="../css/systememail.css" >
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
				    <td><img src="../images/adreq.jpg" alt="" /></td>
					<td class="topemail"><a class="close" onclick="top.close()" href="#">X Close</a></td>
				</tr>
			</table>
			<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<tr>
					<td class="detailsframetopemail">
						<table id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="menuframe" align="right"><asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:button><asp:button id="btnNew" runat="server" Text="New" CssClass="new" Enabled="False" CausesValidation="False"></asp:button><asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" Enabled="False" CausesValidation="False"></asp:button></td>
							   
							</tr>
						</table>
						    <div class="requirementdisplayscroll">
						    <p style="padding: 20px 0 0 0"><asp:Label ID="lblEnrollStatus" runat="server" cssClass="labelreq"></asp:label></p>
						    <div>
						        <asp:DataList ID="DataList1" runat="server" Width=100%>
               					<ItemTemplate>
									<table cellpadding="0" cellspacing="0" style="border: 1px solid #bebebd" width="100%">
								   		<tr>
											<td style="width: 100%; padding: 6px; background-color: #e3ecf8; text-align: left; border-bottom: 1px solid #bebebd; border-right: 1px solid #bebebd">
											    <asp:Label ID="Label1" Font-Bold="true" runat="server" Text='<%# Container.dataitem("Descrip") %>' cssClass="labelreq"></asp:label>
                                                <asp:Label ID="lbl23" runat="server" Text="[Requirements needed #:" cssClass="labelreq"></asp:label>
											    <asp:Label ID="Label3" Font-Bold="true" runat="server" Text=' <%# Container.dataitem("NumReqs") %>' cssClass="labelreq"></asp:label>] 
											    <asp:Label ID="Label4" runat="server" Text="[Requirements completed/overriden #:"  cssClass="labelreq"></asp:label>
												<asp:Label ID="Label2" runat="server" Text='<%# Container.dataitem("AttemptedReqs") %>'  cssClass="labelreq"></asp:label>]
											</td>
										</tr>
										<tr>
											<td style="width: 100%">
											    <asp:DataGrid ID="dgSchoolLevelRequirements" runat="server" AutoGenerateColumns="False" Width=100% ShowFooter="true" BorderColor="#d4ebfb" BorderStyle="solid" BorderWidth="0">
                                                        <Columns>
                                                            <asp:TemplateColumn>
                                                                    <HeaderStyle CssClass="headerstyle" Width="100%" />
                                                                    <ItemStyle CssClass="itemstyle" width="100%" />
                                                                    <ItemTemplate>
                                                                            <asp:CheckBoxList ID="chkRequirements" runat="Server" RepeatColumns=3 RepeatDirection="Horizontal" CssClass="checkbox"></asp:CheckBoxList>
                                                                    </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                </asp:DataGrid>
                                            </td>
										</tr>
									</table>
						        </ItemTemplate>
                            </asp:DataList>
                           </div>
                           <% If  Trim(Session("PrgVerId")).ToString.ToLower = "none" Then %>
                            <div style="margin: 20px 0">	
                                <asp:DataList ID="DataList3NoPrgVersion" runat="server" Width=100%>
               					    <ItemTemplate>
									    <table cellpadding="0" cellspacing="0" style="border: 1px solid #bebebd" width="100%">
								   		    <tr>
											    <td style="width: 100%; padding: 6px; background-color: #e3ecf8; text-align: left; border-bottom: 1px solid #bebebd; border-right: 1px solid #bebebd">
											        <asp:Label ID="Label5" Font-Bold="true" runat="server" Text='<%# Container.dataitem("Descrip") %>'   cssClass="labelboldreq"></asp:label>
											        <asp:Label ID="Label6" Font-Bold="true" runat="server" Text=' - Requirements Assigned'   cssClass="labelboldreq"></asp:label>
											        <asp:Label ID="lblLeadGrpId" Font-Bold="true" runat="server" Text='<%# Container.dataitem("LeadGrpId") %>' Visible="false"  cssClass="labelreq"></asp:label>
											    </td>
										    </tr>
										    <tr>
											<td style="width: 100%; padding: 0 20px">
											      <asp:DataGrid ID="dgReqsToLeadGrpNoPrgVersion" runat="server" AutoGenerateColumns="False" Width=100% ShowFooter="true" BorderColor="#d4ebfb" BorderStyle="solid" BorderWidth="0px">
                                                        <Columns>
                                                            <asp:TemplateColumn>
                                                                    <ItemStyle CssClass="itemstyle" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBoxList ID="chkReqs3" runat=server RepeatDirection="Horizontal" RepeatColumns=3 CssClass="checkbox"></asp:CheckBoxList>     
                                                                   </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                </asp:DataGrid>
                                              </td>
										</tr>
                                        </table>
						            </ItemTemplate>
                                </asp:DataList> 
                            </div> 
                           <% end if %>
                           
                           <% If Not Trim(Session("PrgVerId")).ToString.ToLower = "none" Then%>
                            <div style="margin: 20px 0">
                           			<table cellpadding="0" cellspacing="0" style="border: 1px solid #bebebd" width="100%">
								   		<tr>
											<td style="width: 100%; padding: 6px; background-color: #e3ecf8; text-align: left; border-bottom: 1px solid #bebebd; border-right: 1px solid #bebebd">
											    <asp:Label ID="Label1" Font-Bold="true" runat="server" Text="Requirements - Interested in Program" cssClass="labelreq"></asp:label>
											</td>
										</tr>
										<tr>
											<td style="width: 100%; padding: 0 20px">
											    <asp:Label ID="lblReqsProgramVersionStatus" runat="Server" CssClass="label" Visible="false"></asp:Label>
											    <asp:DataGrid ID="dgReqsProgramVersion" runat="server" AutoGenerateColumns="False" Width=100% ShowFooter="true" BorderColor="#d4ebfb" BorderStyle="solid" BorderWidth="0px">
                                                        <Columns>
                                                            <asp:TemplateColumn>
                                                                    <HeaderStyle CssClass="headerstyle" Width="40%" />
                                                                    <ItemStyle CssClass="itemstyle" />
                                                                    <ItemTemplate>
                                                                        <asp:CheckBoxList ID="chkRequirementsToProgramVersion" runat="Server" RepeatColumns=3 RepeatDirection="Horizontal" CssClass="checkbox"></asp:CheckBoxList>
                                                                    </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                </asp:DataGrid>  
                                             </td>
										</tr>
									</table>
								</div>	
								<div style="margin: 20px 0">	
                                <asp:DataList ID="DataList3" runat="server" Width=100%>
               					<ItemTemplate>
									<table cellpadding="0" cellspacing="0" style="border: 1px solid #bebebd" width="100%">
								   		<tr>
											<td style="width: 100%; padding: 6px; background-color: #e3ecf8; text-align: left; border-bottom: 1px solid #bebebd; border-right: 1px solid #bebebd">
											    <asp:Label ID="Label1" Font-Bold="true" runat="server" Text='<%# Container.dataitem("Descrip") %>'   cssClass="labelboldreq"></asp:label>
											    <asp:Label ID="lbl1" runat="server" Text=" - Requirements/Requirement Groups" cssClass="labelboldreq"></asp:label>
											    <asp:Label ID="lblLeadGrpId" Font-Bold="true" runat="server" Text='<%# Container.dataitem("LeadGrpId") %>' Visible="false"  cssClass="labelreq"></asp:label>
											</td>
										</tr>
										<tr>
											<td style="width: 100%; padding: 0 20px">
											    <asp:DataGrid ID="dgReqsToReqGroup" runat="server" AutoGenerateColumns="False" Width=100% ShowFooter="true" BorderColor="#d4ebfb" BorderStyle="solid" BorderWidth="0px">
                                                        <Columns>
                                                            <asp:TemplateColumn>
                                                                    <ItemStyle CssClass="itemstyle" />
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDescrip2" runat="server" Text='<%# Container.dataitem("Descrip") %>'   cssClass="labelboldreq"></asp:label>
                                                                        <asp:Label ID="lblReqGrpId" runat="server" Text='<%# Container.dataitem("ReqGrpId") %>' Visible="false" cssClass="labelreq"></asp:label>
                                                                        [Requirements needed #: 
											                            <asp:Label ID="Label3" Font-Bold="true" runat="server" Text='<%# Container.dataitem("NumReqs") %>' cssClass="labelreq"></asp:label>] 
											                            &nbsp;&nbsp;[Requirements completed/overriden #:
												                        <asp:Label ID="Label2" runat="server" Text='<%# Container.dataitem("AttemptedReqs") %>' cssClass="labelreq"></asp:label>]
												                        <asp:datagrid id="dg1" runat="Server" AutoGenerateColumns="false" Width=100% ShowFooter="true" BorderColor="#d4ebfb" BorderStyle="solid" BorderWidth="0px">
                                                                            <Columns>
                                                                                <asp:TemplateColumn>
                                                                                    <ItemTemplate>
                                                                                             <asp:CheckBoxList ID="chkReqs" runat=server RepeatDirection="Horizontal" RepeatColumns=3 CssClass="checkbox"></asp:CheckBoxList>     
                                                                                    </ItemTemplate> 
                                                                                </asp:TemplateColumn>
                                                                            </Columns>
                                                                     </asp:datagrid>
                                                                    </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                </asp:DataGrid>
                                              </td>
										</tr>
										<tr>
											<td style="width: 100%; padding: 0 20px">
											      <asp:DataGrid ID="dgReqsToLeadGrp" runat="server" AutoGenerateColumns="False" Width=100% ShowFooter="true" BorderColor="#d4ebfb" BorderStyle="solid" BorderWidth="0px">
                                                        <Columns>
                                                            <asp:TemplateColumn>
                                                                    <ItemStyle CssClass="itemstyle" />
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="lblHeading" runat="Server" Text="Miscellaneous Requirements"  cssClass="labelboldreq"></asp:label>
                                                                    </HeaderTemplate> 
                                                                    <ItemTemplate>
                                                                        <asp:CheckBoxList ID="chkReqs3" runat=server RepeatDirection="Horizontal" RepeatColumns=3 CssClass="checkbox"></asp:CheckBoxList>     
                                                                   </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                </asp:DataGrid>
                                              </td>
										</tr>
									</table>
						        </ItemTemplate>
                            </asp:DataList> 
                            </div> 
                            <% end if  %>
						</div>
					</td>
				</tr>
			</table>
			<div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
		</form>
	</body>
</HTML>


