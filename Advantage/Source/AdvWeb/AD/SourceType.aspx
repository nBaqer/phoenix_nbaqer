<%@ Page Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    Inherits="SourceType" CodeFile="SourceType.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%"
            Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized"
            Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                    </td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server"
                                            RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:datalist id="dlstSourceType" runat="server" DataKeyField="SourceTypeId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server"
                                            Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>'
                                            CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>'
                                            CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                       <asp:LinkButton text='<%# Container.DataItem("SourceTypeDescrip")%>' Runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("SourceTypeId")%>' ID="Linkbutton1" CausesValidation="False" />
                                    </ItemTemplate>
                                </asp:datalist></div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both"
                orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false">
                            </asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false">
                            </asp:Button>
                        </td>
                       
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="scrollright2">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSourceTypeCode" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtSourceTypeCode" runat="server" CssClass="textbox" Width="83%" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlStatusId" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSourceTypeDescrip" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtSourceTypeDescrip" runat="server"  Width="83%" CssClass="textbox" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="Label" runat="server"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSourceCatagoryId" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlSourceCatagoryId" runat="server" >
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:TextBox ID="txtSourceTypeId" runat="server" CssClass="Label" Visible="false"></asp:TextBox>
                                            </td>
                                            <td class="twocolumncontentcell">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                            </td>
                                            <td class="twocolumncontentcell">
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="Label"
                                        Width="10px"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="Label"
                                        Width="10px"></asp:TextBox><!--end table content--></div>
                            </asp:Panel>
                        </td>
                        <!-- end rightcolumn -->
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>
</asp:Content>
