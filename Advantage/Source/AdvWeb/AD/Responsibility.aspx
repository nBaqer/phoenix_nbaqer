<%@ Page Language="vb" AutoEventWireup="false" Inherits="Responsibility" CodeFile="Responsibility.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Job Responsibility Example</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="../css/localhost.css" type="text/css" rel="stylesheet">
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
		<div style="padding: 16px; width: 90%; margin: 0">
			<table width="100%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="twocolumnlabelcell" style="text-align: right"><asp:label id="lblComments" runat="server" cssClass="label">Job Responsibilities</asp:label></td>
					<td class="twocolumncontentcell"><asp:textbox id="txtComments" cssClass="textbox" Runat="server" Columns="50" Rows="5" ReadOnly="True"
							TextMode="MultiLine" MaxLength="300">Supervised, managed and trained 22 person office staff. Prepared for meetings and corresponded with member representatives on upcoming meetings. Created and configured client databases.</asp:textbox></td>
				</tr>
			</table>
			<table width="100%" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td class="twocolumnlabelcell">&nbsp;</td>
					<td class="twocolumncontentcell" style="text-align: center"><input type="button" language="javascript" class="buttons" value="Close Window" onclick="window.close();"></td>
				</tr>
			</table>
			</div>
		</form>
	</body>
</HTML>
