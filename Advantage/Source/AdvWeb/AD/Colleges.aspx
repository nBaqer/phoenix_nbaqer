<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Colleges.aspx.vb" Inherits="Colleges" %>
<%@ MasterType  virtualPath="~/NewSite.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
       <title>student addresses</title> 
       <script type="text/javascript">

           function OldPageResized(sender, args) {
               $telerik.repaintChildren(sender);
           }

    </script>
    </asp:Content>

    <asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    </asp:Content>
    <asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
    </asp:Content>
    <asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">


        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350"  Scrolling="Y">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td nowrap="nowrap" align="left" width="15%">
                                    <asp:Label ID="Label1" runat="server"><b class="tothemeshow">Show</b></asp:Label></td>
                                <td nowrap="nowrap" width="85%">
                                    <asp:RadioButtonList ID="radstatus" runat="Server" RepeatDirection="Horizontal" AutoPostBack="true"
                                        CssClass="label">
                                        <asp:ListItem Text="Active" Selected="True" />
                                        <asp:ListItem Text="Inactive" />
                                        <asp:ListItem Text="All" />
                                    </asp:RadioButtonList></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfilters">
                            <asp:datalist id="dlstcolleges" runat="server" width="100%" datakeyfield="collegeid">
                            <selecteditemstyle cssclass="selecteditemstyle" />
                                <itemstyle cssclass="itemstyle"></itemstyle>
                                <itemtemplate>
                                    <asp:imagebutton id="imginactive" imageurl="~/images/inactive.gif" runat="server" visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' causesvalidation=false>
                                    </asp:imagebutton>
                                    <asp:imagebutton id="imgactive" imageurl="../images/active.gif" runat="server" visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' causesvalidation=false>
                                    </asp:imagebutton>
                                    <asp:label id="lblid" runat="server" visible="false" text='<%# container.dataitem("statusid")%>' />
                                    <asp:linkbutton text='<%# container.dataitem("collegename")%>' runat="server" cssclass="itemstyle" commandargument='<%# container.dataitem("collegeid")%>' id="linkbutton1" causesvalidation="false" />
                                </itemtemplate>
                            </asp:datalist>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>


        <telerik:radpane id="oldcontentpane" runat="server" borderwidth="0px"  Scrolling="Both" orientation="horizontaltop" left="250px">
        <!-- end leftcolumn -->
        <!-- begin rightcolumn -->
        <asp:panel id="pnlRHS" Runat="server">
            <table id="table4" cellspacing="0" cellpadding="0" width="100%" border="0"> <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:button id="btnsave" runat="server" cssclass="save" text="Save"></asp:button>
                        <asp:button id="btnnew" runat="server" cssclass="new" text="New" causesvalidation="false"></asp:button>
                        <asp:button id="btndelete" runat="server" cssclass="delete" text="Delete" causesvalidation="false"></asp:button></td>
                    
                </tr>
            </table> <!-- end top menu (save,new,reset,delete,history)--> <!--begin right column-->

        <table class="maincontenttable" id="table5" cellspacing="0" cellpadding="0" width="80%" style="padding-left:100px;" border="0">
        <tr>
        <td class="detailsframe">
        <div class="scrollright2"><!-- begin table content--> <!-- two column template with no spacer in between cells --> <!-- used in placement module for maintenance pages -->
            <%--<table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" align="left" border="0">--%>
            <!-- begin content table-->
            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                <%--<td class="contentcell">--%>
                    <td class="detailsframe">
                        <asp:label id="lblcollegecode" runat="server" cssclass="label"></asp:label>
                    </td>
                    <td class="contentcell4">
                        <asp:textbox id="txtcollegecode" runat="server" cssclass="textbox" tabindex="1"></asp:textbox>
                    </td>
                </tr>

                <tr>
                    <td class="contentcell">
                        <asp:label id="lblstatusid" runat="server" cssclass="label"></asp:label>
                    </td>
                    <td class="contentcell4">
                        <asp:dropdownlist id="ddlstatusid" runat="server" cssclass="dropdownlist" tabindex="2"></asp:dropdownlist>
                    </td>
                </tr>

                <tr>
                    <td class="contentcell">
                        <asp:label id="lblcollegename" runat="server" cssclass="label"></asp:label>
                    </td>
                    <td class="contentcell4">
                        <asp:textbox id="txtcollegename" runat="server" cssclass="textbox" tabindex="3" Width="70%"></asp:textbox>
                    </td>
                </tr>

                <tr>
                    <td class="contentcell">
                        <asp:label id="lblcampgrpid" runat="server" cssclass="label"></asp:label>
                    </td>
                    <td class="contentcell4">
                        <asp:dropdownlist id="ddlcampgrpid" runat="server" cssclass="dropdownlist" tabindex="4"></asp:dropdownlist>
                    </td>
                </tr>

                <tr>
                <td class="spacertables"></td>
                </tr>
                <tr>
                <td class="contentcellheader" nowrap="nowrap" colspan="4">
                <asp:label id="lbladdress" runat="server" cssclass="label" font-bold="true">Address</asp:label></td>
                </tr>
                <tr>
                <td class="spacertables"></td>
                </tr>

                <tr>
                    <td class="contentcell">
                    <%--<td class="contentcell4">--%>
                        <asp:checkbox id="chkforeignzip" tabindex="5" runat="server" autopostback="true" cssclass="checkboxinternational"></asp:checkbox>
                    </td>
                </tr>




                <tr>
                    <td class="contentcell">
                        <asp:label id="lbladdress1" runat="server" cssclass="label"></asp:label>
                    </td>
                    <td class="contentcell4">
                        <asp:textbox id="txtaddress1" runat="server" cssclass="textbox" tabindex="6" Width="70%"></asp:textbox>
                    </td>
                </tr>


                <tr>
                    <td class="contentcell">
                        <asp:label id="lbladdress2" runat="server" cssclass="label"></asp:label>
                    </td>
                    <td class="contentcell4">
                        <asp:textbox id="txtaddress2" runat="server" cssclass="textbox" tabindex="7" Width="70%"></asp:textbox>
                    </td>
                </tr>


                <tr>
                    <td class="contentcell">
                        <asp:label id="lblcity" runat="server" cssclass="label"></asp:label></td>
                    <td class="contentcell4">
                        <asp:textbox id="txtcity" runat="server" cssclass="textbox" tabindex="8" Width="70%"></asp:textbox>
                    </td>
                </tr>

                <tr>
                    <td class="contentcell">
                        <asp:label id="lblstateid" runat="server" cssclass="label"></asp:label>
                    </td>
                    <td class="contentcell4">
                        <asp:dropdownlist id="ddlstateid" runat="server" cssclass="dropdownlist" tabindex="9"></asp:dropdownlist>
                    </td>
                </tr>



                <tr>
                    <td class="contentcell">
                        <asp:label id="lblotherstate" runat="server" cssclass="label" visible="false"></asp:label>
                    </td>
                    <td class="contentcell4">
                        <asp:textbox id="txtotherstate" runat="server" autopostback="true" cssclass="textbox" tabindex="10" visible="false"></asp:textbox>
                    </td>
                </tr>



                <tr>
                    <td class="contentcell">
                        <asp:label id="lblzip" runat="server" cssclass="label"></asp:label>
                    </td>
                    <td class="contentcell4">
                        <telerik:RadMaskedTextBox ID="txtzip" tabindex="11" runat="server" cssclass="textbox" Mask="#####" DisplayMask="#####" DisplayPromptChar="" AutoPostBack="false" ></telerik:RadMaskedTextBox>
                    </td>
                </tr>



                <tr>
                    <td class="contentcell">
                        <asp:label id="lblcountryid" runat="server" cssclass="label"></asp:label>
                    </td>
                    <%--<td class="contentcell4">--%>
                    <td class="contentcell">
                        <asp:dropdownlist id="ddlcountryid" runat="server" cssclass="dropdownlist" tabindex="12"></asp:dropdownlist>
                    </td>
                </tr>



                <tr>
                    <td class="spacertables"></td>
                </tr>

                <tr>
                    <td class="contentcellheader" nowrap colspan="6">
                        <asp:label id="lblphoneheading" runat="server" cssclass="label" font-bold="true">Phone</asp:label>
                    </td>
                </tr>

                <tr>
                    <td class="spacertables"></td>
                </tr>

                <tr>
                    <td class="contentcell">
                    <%--<td class="contentcell4">--%>
                        <asp:checkbox id="chkforeignphone" tabindex="13" runat="server" autopostback="true" text="international" cssclass="checkboxinternational"></asp:checkbox>
                    </td>
                </tr>


                <tr>
                    <td class="contentcell">
                        <asp:label id="lblphone" runat="server" cssclass="label"></asp:label>
                    </td>
                    <td class="contentcell4" colspan="4">
                        <telerik:RadMaskedTextBox ID="txtphone" tabindex="14" runat="server" cssclass="textbox"  Width="200px" DisplayFormatPosition="Right"  Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar="" ></telerik:RadMaskedTextBox>
                    </td>
                </tr>


                <tr>
                <td class="contentcell">
                <asp:textbox id="txtcollegeid" runat="server" cssclass="label" visible="false" maxlength="128" tabindex="15"></asp:textbox></td>
                <td class="contentcell4"></td>
                </tr>


            </table> <!--end table content-->
        </div>
        </td>
        </tr>
        </table>

        </asp:panel>







        <%--</asp:panel></td>
        <!-- end rightcolumn --></tr>
        </table>--%>
        </telerik:radpane>
</telerik:RadSplitter>
<!-- start validation panel--><asp:panel id="panel1" runat="server" cssclass="validationsummary"></asp:panel><asp:customvalidator id="customvalidator1" runat="server" cssclass="validationsummary" errormessage="customvalidator"
display="none"></asp:customvalidator><asp:panel id="pnlrequiredfieldvalidators" runat="server"></asp:panel><asp:validationsummary id="validationsummary1" runat="server" cssclass="validationsummary" showmessagebox="true"
showsummary="false"></asp:validationsummary>
</asp:Content>