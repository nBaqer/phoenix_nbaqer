﻿
Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports Advantage.Business.Objects

Partial Class LeadAssignment
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    ' ReSharper disable InconsistentNaming
    Protected WithEvents btnhistory As Button
    Protected WithEvents lblReassignToAdmissionReps As Label
    Protected WithEvents lstReassignToAdmissionReps As ListBox
    Protected WithEvents lblAssignmentResult As Label
    Protected WithEvents lstAssignmentResult As ListBox
    Protected WithEvents lstGrpName As ListBox
    Protected WithEvents lstPossibleSkills As ListBox
    Protected WithEvents listbox2 As ListBox
    Protected WithEvents img3 As HtmlImage
    Protected WithEvents btnSubmit As Button
    Protected WithEvents txtLeadsInfo As TextBox
    Protected WithEvents lblLeads As Label
    Protected WithEvents lstAssignCampus As ListBox
    Protected campusId As String
    ' ReSharper restore InconsistentNaming Protected mContext As HttpContext


    Private pObj As New UserPagePermissionInfo
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

    Dim userId As String
    Dim resourceId As Integer
    Protected LeadId As String
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim advantageUserState As User = AdvantageSession.UserState
        campusId = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString
        hdnUserId.Value = userId
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

        'Check if this page still exists in the menu while switching campus
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
    End Sub

End Class