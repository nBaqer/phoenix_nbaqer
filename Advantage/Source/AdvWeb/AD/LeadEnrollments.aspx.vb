﻿Imports System.Collections
Imports System.Collections.Generic
Imports System.Data
Imports System.Diagnostics
Imports System.IO
Imports System.Text
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.Common.AR
Imports FAME.Common
Imports Telerik.Web.UI
Imports Telerik.Web.UI.Calendar

Namespace AdvWeb.AD

    Partial Class LeadEnrollments
        Inherits BasePage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

#End Region

#Region "Variables"

        Protected WithEvents chkStatus As CheckBox
        Protected WithEvents pnlRHS As Panel
        Protected WithEvents dlstStdEnroll As DataList
        Protected WithEvents ddlLeadId As DropDownList
        Protected ShowStatusLevel As AdvantageCommonValues.IncludeStatus
        Protected boolStatus As String
        Dim ssnMAsk As String

        Public pObj As New UserPagePermissionInfo
        'To get the first name and last name of student to pass into the enrollment id component.

        Dim EnrollmentId As New EnrollmentComponent
        Dim LeadID As String '= "FAAB0903-819A-4A6C-95EE-BDA7C62E8EA1"
        Dim strFirstName, strMiddleName, strLastName As String
        Protected CampusId As String
        Protected UserId As String
        Dim resourceId As Integer
        Protected moduleid As String
        Dim internationalAddr As Boolean
        Dim dateError As Boolean = False
#End Region

#Region "Page Events"

        Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

        Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
            Dim objCommon As New CommonUtilities
            Dim sdfControls As New SDFComponent
            Form.DefaultButton = btnSearch.UniqueID
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

            resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            CampusId = AdvantageSession.UserState.CampusId.ToString
            ViewState("CampusId") = CampusId
            UserId = AdvantageSession.UserState.UserId.ToString
            moduleid = HttpContext.Current.Request.Params("Mod").ToString

            ShowStatusLevel = AdvantageCommonValues.IncludeStatus.ActiveOnly
            boolStatus = "True"

            Dim advantageUserState As User = AdvantageSession.UserState

            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), CampusId)

            If Me.Master.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If
            Try
                If Not Page.IsPostBack Then
                    'DE7912 - we need show the transfer hours for all clock hour program
                    BuildDropDownLists()
                    'Get the setting that decide if it a SUVA School
                    Dim majorMinorActive = GetSettingString("MajorsMinorsConcentrations")
                    ddlSuvaVersion.DataTextField = "Description"
                    ddlSuvaVersion.DataValueField = "IdSuva"
                    If majorMinorActive.ToUpper().ToUpper() = "YES" Then
                        SuvaStuff.Style("visibility") = "visible"
                        ddlSuvaVersion.DataSource = SuvaTableFacade.GetItemsFromSuvaCatalogDB()

                    Else
                        ddlSuvaVersion.DataSource = SuvaTableFacade.GetMockItemsFromSuvaCatalogDB()
                        SuvaStuff.Style("visibility") = "collapse"
                    End If

                    ddlSuvaVersion.DataBind()
                    '............................................

                    BuildCampusGroupsDDL()
                    BuildCampusGroupsSearchDDL()
                    ''''''''''''''''''
                    BuildPrgVersionsDDL()
                    BuildLeadStatus()

                    'Get international lead indicator from the query string in case the request came from the Lead Info tab
                    Dim intlLead As Boolean = False

                    If HttpContext.Current.Request.QueryString("intld") <> "" Then
                        If HttpContext.Current.Request.QueryString("intld") = "true" Then
                            intlLead = True
                        End If
                    End If

                    If intlLead = True Then
                        objCommon.SetCaptionsAndColorRequiredFieldsForEnrollLeads(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), , True)
                    Else
                        objCommon.SetCaptionsAndColorRequiredFieldsForEnrollLeads(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                    End If

                    ViewState("MODE") = "NEW"

                    'Set the text box to a new guid
                    txtStuEnrollId.Text = Guid.NewGuid.ToString

                    'Disable The Panel Control
                    pnlLeadEnrollment.Visible = False

                    'Make The Note Visible
                    pnlNotes.Visible = True

                    'Get LeadId from the query string in case the request came from the Lead Info tab
                    If HttpContext.Current.Request.QueryString("enid") <> "" Then
                        txtLeadId.Text = HttpContext.Current.Request.QueryString("enid")
                    Else
                        txtLeadId.Text = ""
                    End If

                    'Check if Future Start is mapped to Future Start System Status
                    Dim strCheckStatus As String
                    Dim strStatusMessage As String = ""
                    strCheckStatus = (New LeadFacade).CheckIfStatusMappedToFutureStart()
                    If strCheckStatus Is Nothing Then
                        pnlLeadEnrollment.Visible = False
                        DisplayErrorMessage("The lead cannot be enrolled as none of the school defined status codes are mapped to future start system status ")
                        Exit Sub
                    End If
                    If Not strCheckStatus.Trim = "" Then
                        strStatusMessage &= "The lead cannot be enrolled as the Future Start status has been currently mapped to " & strCheckStatus
                        pnlLeadEnrollment.Visible = False
                        DisplayErrorMessage(strStatusMessage)
                        Exit Sub
                    Else
                        pnlLeadEnrollment.Visible = True
                    End If

                    'Populate fields on the RHS based on the LeadId 
                    'Similarly to what is done when an item from the datalist on the LHS is selected
                    If txtLeadId.Text <> "" Then
                        ' NOTE Here is loaded the enrollment data                    
                        ProcessLeadEnrollment()
                    End If
                    'added by Theresa G on sep 08 2010
                    '19691: QA: Loading a lead on the enroll leads page shows the transfer hours field for non clock hour programs. 
                    If ddlPrgVerId.SelectedValue = "" Or ddlPrgVerId.SelectedValue = "00000000-0000-0000-0000-000000000000" Then
                    Else
                        EnableAndDisableForClockHR()
                    End If
                Else
                    objCommon.SetCaptionsAndColorRequiredFieldsForEnrollLeads(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

                    If ViewState("InternationalLead") IsNot Nothing Then
                        Dim intLead = CType(ViewState("InternationalLead"), Boolean)
                        If intLead = False Then
                            lblSSN.Text = "SSN<font color='red'>*</font>"
                        End If
                    End If

                End If
                'IPEDSRequirements()

                'Check If any UDF exists for this resource
                Dim intSdfExists As Integer = sdfControls.GetSDFExists(resourceId, moduleid)
                If intSdfExists >= 1 Then
                    pnlUDFHeader.Visible = True
                Else
                    pnlUDFHeader.Visible = False
                End If

                If Trim(txtPKId.Text) <> "" Then
                    sdfControls.GenerateControlsEdit(pnlSDF, resourceId, txtPKId.Text, moduleid)
                Else
                    sdfControls.GenerateControlsNew(pnlSDF, resourceId, moduleid)
                End If

                Dim myAdvAppSettings As AdvAppSettings
                If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
                Else
                    myAdvAppSettings = New AdvAppSettings
                End If

                'DE6698 - 11/16/2011 Janet Robinson
                If myAdvAppSettings.AppSettings("TimeClockClassSection", CampusId).ToString.ToLower = "yes" Then
                    ddlScheduleID.Enabled = False
                    ibToMaintPage_Schedule.Enabled = False
                    btnCalcGradDate.Visible = False
                Else
                    txtBadgeNumber.Enabled = False
                End If

                If txtLeadId.Text = "" Then
                    If myAdvAppSettings.AppSettings("TrackSAPAttendance", CampusId).ToString.ToLower = "byclass" Then
                        btnCalcGradDate.Visible = False
                    Else
                        btnCalcGradDate.Visible = True
                    End If
                End If
                GetInputMaskValue()
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End Sub

#End Region

        Private Sub GetInputMaskValue()
            'dsfadfasdfasd()

            Dim facInputMasks As New InputMasksFacade

            Dim objCommon As New CommonUtilities

            ssnMAsk = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
            lblSSN.ToolTip = ssnMAsk

            'Replace The Mask Character from # to 9 as Masked Edit TextBox 
            'accepts only certain characters as mask characters


            'Get The RequiredField Value
            Dim strSSNReq As String

            strSSNReq = objCommon.SetRequiredColorMask("SSN")

            'If The Field Is Required Field Then Color The Masked
            'Edit Control
            If strSSNReq = "Yes" Then
                'txtSSN.BackColor = Color.FromName("#ffff99")
                lblSSN.Text = "SSN<font color='red'>*</font>"
            End If
        End Sub

        Private Sub BuildCampusGroupsDDL()

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim campGrp As New CampusGroupsFacade
            With ddlCampusId
                .DataTextField = "CampDescrip"
                .DataValueField = "CampusId"
                If myAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "ownleads" Then
                    .DataSource = campGrp.GetCampusesByUser(UserId)
                ElseIf myAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "permissiblecampuses" Then
                    .DataSource = campGrp.GetCampusesByUser(UserId)
                ElseIf myAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "allcampuses" Then
                    If myAdvAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "yes" Then
                        .DataSource = campGrp.GetAllCampuses()
                    Else
                        .DataSource = campGrp.GetCampusesByUser(UserId)
                    End If
                Else
                    .DataSource = campGrp.GetAllCampusEnrollmentByCampus(CampusId)
                End If
                .DataBind()
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub BuildPrgVersionsDDL()
            '   create row filter and sort expression
            Dim facade As New CopyPrgVerFacade

            With ddlPrgVerId
                '.DataTextField = "PrgVerDescrip"
                .DataTextField = "PrgVerShiftDescrip"
                .DataValueField = "PrgVerId"
                .DataSource = facade.GetPrgVersions(CampusId)
                .DataBind()
                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            End With
        End Sub

        Private Sub BuildDropDownLists()
            Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()
            'ddlList.Add(New AdvantageDDLDefinition(ddlPrgVerId, AdvantageDropDownListName.ProgramVersions, CampusId, True, True, String.Empty))
            'ddlList.Add(New AdvantageDDLDefinition(ddlCampusId, AdvantageDropDownListName.Campus, Nothing, True, True))
            ddlList.Add(New AdvantageDDLDefinition(ddlAdminRep, AdvantageDropDownListName.AdmissionsRep, CampusId, True, True, String.Empty))
            ddlList.Add(New AdvantageDDLDefinition(ddlShiftId, AdvantageDropDownListName.Shifts, CampusId, True, True, String.Empty))
            ddlList.Add(New AdvantageDDLDefinition(ddlEdLvlId, AdvantageDropDownListName.EducationLvl, CampusId, True, True, String.Empty))
            ddlList.Add(New AdvantageDDLDefinition(ddlBillingMethodId, AdvantageDropDownListName.Billing_Methods, CampusId, True, True, String.Empty))
            ddlList.Add(New AdvantageDDLDefinition(ddlTuitionCategoryId, AdvantageDropDownListName.Tuition_Cats, CampusId, True, True, String.Empty))
            ddlList.Add(New AdvantageDDLDefinition(ddlEmpId, AdvantageDropDownListName.Academic_Advisors, CampusId, True, True, String.Empty))
            ddlList.Add(New AdvantageDDLDefinition(ddlStatusCodeId, AdvantageDropDownListName.LeadEnrollStatusCodes, CampusId, True, True, String.Empty))
            'Races
            ddlList.Add(New AdvantageDDLDefinition(ddlRace, AdvantageDropDownListName.Races, CampusId, True, True, String.Empty))
            'Genders
            ddlList.Add(New AdvantageDDLDefinition(ddlGender, AdvantageDropDownListName.Genders, CampusId, True, True, String.Empty))
            'Marital Status
            ddlList.Add(New AdvantageDDLDefinition(ddlMaritalStatus, AdvantageDropDownListName.MaritalStatus, CampusId, True, True, String.Empty))
            'States 
            ddlList.Add(New AdvantageDDLDefinition(ddlStateID, AdvantageDropDownListName.States, CampusId, True, True, String.Empty))
            'Citizenships
            ddlList.Add(New AdvantageDDLDefinition(ddlCitizen, AdvantageDropDownListName.Citizen, CampusId, True, True, String.Empty))
            'Nationality
            ddlList.Add(New AdvantageDDLDefinition(ddlNationality, AdvantageDropDownListName.Nationality, CampusId, True, True, String.Empty))
            'Previous Education
            'ddlList.Add(New AdvantageDDLDefinition(ddlPreviousEducation, AdvantageDropDownListName.EducationLvl, CampusId, True, True, String.Empty))
            'Dependency Type
            ddlList.Add(New AdvantageDDLDefinition(ddlDependencyTypeId, AdvantageDropDownListName.DependencyType, CampusId, True, True, String.Empty))
            'Geographic Type
            ddlList.Add(New AdvantageDDLDefinition(ddlGeographicTypeId, AdvantageDropDownListName.GeographicType, CampusId, True, True, String.Empty))
            'Admin Criteria
            ddlList.Add(New AdvantageDDLDefinition(ddlAdminCriteriaId, AdvantageDropDownListName.AdminCriteriaType, CampusId, True, True, String.Empty))
            'Housing Type
            ddlList.Add(New AdvantageDDLDefinition(ddlHousingId, AdvantageDropDownListName.HousingType, CampusId, True, True, String.Empty))

            'DE7172 2/16/2012 Janet Robinson Add Family Income
            'Family Income
            ddlList.Add(New AdvantageDDLDefinition(ddlFamilyIncome, AdvantageDropDownListName.FamilyIncome, CampusId, True, True, String.Empty))

            'Attendance Types DDL
            ddlList.Add(New AdvantageDDLDefinition(ddlAttendTypeId, AdvantageDropDownListName.AttendanceType, CampusId, True, True, String.Empty))

            'Degree Certificate Seeking Types DDL
            ddlList.Add(New AdvantageDDLDefinition(ddlDegCertSeekingId, AdvantageDropDownListName.DegCertSeekingType, CampusId, True, True, String.Empty))

            'Financial Advisor DDL
            ddlList.Add(New AdvantageDDLDefinition(ddlFAAdvisorId, AdvantageDropDownListName.Financial_Advisors, CampusId, True, True, String.Empty))

            '''''''''''''''''''''''''

            CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
            Try
                For Each item As ListItem In ddlShiftId.Items
                    If item.Text.ToString.Trim.ToLower = "online" Then
                        ddlShiftId.SelectedIndex = ddlShiftId.Items.IndexOf(ddlShiftId.Items.FindByText("Online"))
                    End If
                Next
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        End Sub

        Public Sub BuildLeadsByStatus(strLeadStatus As String)

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()


            Dim leadStatus As New LeadFacade
            UserId = CType(Session("UserId"), String)
            Dim ds As DataSet
            Dim leadCampuses As String
            If myAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "ownleads" Then
                leadCampuses = "ownleads"
            ElseIf myAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "permissiblecampuses" Then
                leadCampuses = "permissiblecampuses"
            ElseIf myAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "allcampuses" Then
                leadCampuses = "allcampuses"
            Else
                leadCampuses = "ownleads"
            End If

            ds = leadStatus.GetAllLeadsByCampusUserAndNameFiltersNew(strLeadStatus, ddlCampusIDSearch.SelectedValue.ToString, UserId, txtFirstName.Text, txtLastName.Text, leadCampuses, CType(IIf(myAdvAppSettings.AppSettings("EditOtherLeads").ToLower = "yes", True, False), Boolean))
            ''''''''''''''''''''''''''''''''''''''
            With dlstLeadNames
                .DataSource = ds
                .DataBind()
            End With
        End Sub

        Private Sub BuildLeadStatus()
            Dim leadStatus As New LeadStatusChangeFacade
            With ddlLeadStatus
                .DataTextField = "StatusCodeDescrip"
                .DataValueField = "StatusCodeID"
                .DataSource = leadStatus.GetLeadStatusMappedToEnrolledStatus(UserId, CampusId)
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End Sub

        Private Sub GetLeadInfo(leadId1 As String)
            Dim leadInfo As New LeadEnrollmentFacade
            BindLeadInfoExist(leadInfo.GetLeadEnrollmentInfo(leadId1))
        End Sub

        Private Sub BindLeadInfoExist(ByVal leadInfo As LeadMasterInfo)

            Dim myAdvAppSettings As AdvAppSettings
            If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
                myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
            Else
                myAdvAppSettings = New AdvAppSettings
            End If

            Dim facInputMasks As New InputMasksFacade
            ssnMAsk = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
            With leadInfo
                chkIsInDB.Checked = False
                If .ExpectedStart = "" Then
                    txtExpStartDate.Clear()
                Else
                    txtExpStartDate.SelectedDate = CType(.ExpectedStart, Date?)
                End If

                If .EnrollmentDate = "" Then
                    txtEnrollDate.Clear()
                Else
                    txtEnrollDate.SelectedDate = CType(.EnrollmentDate, Date?)
                End If
                If .StartDate = "" Then
                    txtStartDate.Clear()
                Else
                    txtStartDate.SelectedDate = CType(.StartDate, Date?)
                End If
                If .MidPointDate = "" Then
                    txtMidPtDate.Clear()
                Else
                    txtMidPtDate.SelectedDate = CType(.MidPointDate, Date?)
                End If
                If .ExpGradDate = "" Then
                    txtContractedGradDate.Clear()
                Else
                    txtContractedGradDate.SelectedDate = CType(.ExpGradDate, Date?)
                End If
                If .TransferDate = "" Then
                    txtTransferDate.Clear()
                Else
                    txtTransferDate.SelectedDate = CType(.TransferDate, Date?)
                End If
                If .BirthDate = "" Then
                    txtBirthDate.Clear()
                Else
                    txtBirthDate.SelectedDate = CType(.BirthDate, Date?)
                End If

                ddlIsDisabled.SelectedValue = CType(.IsDisabled, String)

                chkIsFirstTimeInSchool.Checked = .IsFirstTimeInSchool
                chkIsFirstTimePostSecSchool.Checked = .IsFirstTimePostSecSchool

                If .EntranceInterviewDate Is Nothing Then

                    EntranceInterviewDate.Clear()
                Else
                    EntranceInterviewDate.SelectedDate = .EntranceInterviewDate

                End If

                txtMiddleName.Text = .MiddleName
                txtLead.Text = .FirstName & " " & .MiddleName & " " & .LastName
                txtGender.Text = .Gender
                txtGenderDescrip.Text = .GenderDescrip
                txtProgId.Text = .ProgramID
                'txtProgId.Text = .PrgVerId
                internationalAddr = (.ForeignZip > 0)
                txtSSN.Text = .SSN
                'If txtSSN.Text <> "" Then
                '    txtSSN.Text = facInputMasks.ApplyMask(ssnMAsk, txtSSN.Text)
                'End If

                '--------------------------------------------------------------------------------------------------------------------
                '' JG - 02/23/2015 - Added check for internatioanal student.  
                ''--------------------------------------------------------------------------------------------------------------------
                If .InternationalLead = False Then
                    lblSSN.Text = "SSN<font color='red'>*</font>"
                End If

                ViewState("InternationalLead") = .InternationalLead

                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlShiftId, .ShiftID, .ShiftDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCampusId, .CampusId, .CampusDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdminRep, .AdmissionsRep, .AdmissionRepsDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlEmpId, .AcademicAdvisor, .AcademicAdvisorDescrip)
                'CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrgVerId, .PrgVerId, .PrgVersionDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrgVerId, .PrgVerId, .PrgVerShiftDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlTuitionCategoryId, .TuitionCategory, .TuitionCategoryDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlEdLvlId, .GradeLevel, .EdLvlDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlBillingMethodId, .ChargingMethod, .ChargingMethodDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStatusCodeId, "", "Select")
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGender, .Gender, .GenderDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRace, .Race, .EthCodeDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlMaritalStatus, .MaritalStatus, .MaritalStatusDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateID, .State, .StateDescrip)
                'CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPreviousEducation, .PreviousEducation, .EdLvlDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlNationality, .Nationality, .NationalityDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCitizen, .Citizen, .CitizenDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDependencyTypeId, .DependencyTypeId, .DependencyTypeDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGeographicTypeId, .GeographicTypeId, .GeographicTypeDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdminCriteriaId, .AdminCriteriaId, .AdminCriteriaDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlHousingId, .HousingTypeId, .HousingTypeDescrip)
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDegCertSeekingId, .DegCertSeekingId, .DegCertSeekingDescrip)

                'DE7172 2/16/2012 Janet Robinson add Family Income
                CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFamilyIncome, .FamilyIncome, .FamilyIncomeDescrip)

                Dim ldfacade As New LeadFacade
                Dim IsClockHour As Boolean
                IsClockHour = ldfacade.IsClockHourProg(ddlPrgVerId.SelectedValue)

                If IsClockHour Then
                    '' New Code Added by Vijay Ramteke on June 15, 2010 For Mantis ID 18067
                    ''btnCalcGradDate.Enabled = True
                    btnCalcGradDate.Visible = True
                    '' New Code Added by Vijay Ramteke on June 15, 2010 For Mantis ID 18067
                Else
                    '' New Code Added by Vijay Ramteke on June 15, 2010 For Mantis ID 18067
                    ''btnCalcGradDate.Enabled = False
                    btnCalcGradDate.Visible = False

                    '' New Code Added by Vijay Ramteke on June 15, 2010 For Mantis ID 18067
                End If

                ' DE6698 - 11/29/2011 Janet Robinson hide calculator button
                ' DE6829 - 12/01/2011 Janet Robinson hide calculator for byclass as well
                If myAdvAppSettings.AppSettings("TimeClockClassSection", CampusId).ToString.ToLower = "yes" OrElse myAdvAppSettings.AppSettings("TrackSAPAttendance", CampusId).ToString.ToLower = "byclass" Then
                    btnCalcGradDate.Visible = False
                End If

                'If the school is using module starts then we want to populate the expected grad date field
                'based on the prgverid, expstartdate, campusid and shiftid
                If myAdvAppSettings.AppSettings("SchedulingMethod", CampusId) = "ModuleStart" Then
                    Dim facLE As New LeadEnrollmentFacade
                    txtContractedGradDate.SelectedDate = CType(facLE.GetExpectedGradDateForModuleStart(ddlPrgVerId.SelectedValue, txtExpStartDate.SelectedDate.ToString, ddlCampusId.SelectedValue, ddlShiftId.SelectedValue), Date?)
                End If
                Try
                    For Each item As ListItem In ddlShiftId.Items
                        If item.Text.ToString.Trim.ToLower = "online" Then
                            ddlShiftId.SelectedIndex = ddlShiftId.Items.IndexOf(ddlShiftId.Items.FindByText("Online"))
                        End If
                    Next
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                End Try

                ' 1/31/07 - Added by ThinkTron for Clock Hours project
                BuildSchedulesForPrgVersionDDL(ddlScheduleID, ddlPrgVerId.SelectedValue)
                txtBadgeNumber.Text = "" 'clear out the badge number

                'Get the enrollment id
                txtEnrollmentId.Text = EnrollmentId.GenerateEnrollmentID(.LastName, .FirstName)
            End With
        End Sub

        Private Function BuildLeadInfoExist(leadId1 As String) As LeadMasterInfo
            Dim leadInfo As New LeadMasterInfo
            Dim facade As New StuEnrollFacade
            Dim fullName() As String = Split(facade.GetLeadName(txtLeadId.Text), "#")
            strFirstName = fullName(0)
            strLastName = fullName(1)
            strMiddleName = fullName(2)
            Dim facInputMask As New InputMasksFacade
            Dim getStudentFormat As New FAME.AdvantageV1.BusinessFacade.StudentMasterFacade
            Dim studInfo As FAME.AdvantageV1.Common.StudentMasterInfo = getStudentFormat.GetStudentFormat()
            Dim intYearNumber As Integer
            Dim intMonthNumber As Integer
            Dim intDateNumber As Integer
            Dim intLNameNumber As Integer
            Dim intFNameNumber As Integer
            Dim intSeqNumber As Integer
            Dim intStartSeqNumber As Integer
            Dim strFormatType As String
            Dim strStudentNumber = ""


            With studInfo
                intYearNumber = .YearNumber
                intMonthNumber = .MonthNumber
                intDateNumber = .DateNumber
                intLNameNumber = .LNameNumber
                intFNameNumber = .FNameNumber
                intSeqNumber = .SeqNumber
                intStartSeqNumber = .SeqStartNumber
                strFormatType = .FormatType
            End With


            If strFormatType = 1 Then
            ElseIf strFormatType = 3 Then
                strStudentNumber = EnrollmentId.GenerateStudentId(strLastName, strFirstName, intYearNumber, intMonthNumber, intDateNumber, intLNameNumber, intFNameNumber, intSeqNumber)
            Else
                strStudentNumber = EnrollmentId.GenerateStudentIdSeq(intStartSeqNumber)
            End If

            With leadInfo
                .FirstName = strFirstName
                .LastName = strLastName
                .MiddleName = strMiddleName
                .IsInDB = chkIsInDB.Checked
                .ShiftID = ddlShiftId.SelectedValue
                .CampusId = ddlCampusId.SelectedValue
                .AdmissionsRep = ddlAdminRep.SelectedValue
                .LeadMasterID = leadId1
                .StudentId = Guid.NewGuid.ToString
                .StatusCodeId = ddlStatusCodeId.SelectedValue
                .GradeLevel = ddlEdLvlId.SelectedValue
                .ChargingMethod = ddlBillingMethodId.SelectedValue
                .EnrollmentDate = txtEnrollDate.SelectedDate.ToString
                .ExpectedStart = txtExpStartDate.SelectedDate.ToString
                .StartDate = txtStartDate.SelectedDate.ToString
                .MidPointDate = txtMidPtDate.SelectedDate.ToString
                .ExpGradDate = txtContractedGradDate.SelectedDate.ToString
                .TransferDate = txtTransferDate.SelectedDate.ToString
                .AcademicAdvisor = ddlEmpId.SelectedValue
                .ShiftID = ddlShiftId.SelectedValue
                .ChargingMethod = ddlBillingMethodId.SelectedValue
                .PrgVerId = ddlPrgVerId.SelectedValue
                .EnrollmentId = txtEnrollmentId.Text
                .TuitionCategory = ddlTuitionCategoryId.SelectedValue
                .StudentNumber = strStudentNumber
                .Gender = ddlGender.SelectedValue
                .Race = ddlRace.SelectedValue
                .BirthDate = txtBirthDate.SelectedDate.ToString
                .Nationality = ddlNationality.SelectedValue
                .DependencyTypeId = ddlDependencyTypeId.SelectedValue
                .GeographicTypeId = ddlGeographicTypeId.SelectedValue
                .HousingTypeId = ddlHousingId.SelectedValue
                .AdminCriteriaId = ddlAdminCriteriaId.SelectedValue
                .MaritalStatus = ddlMaritalStatus.SelectedValue
                .Citizen = ddlCitizen.SelectedValue
                .State = ddlStateID.SelectedValue
                .BadgeNumber = txtBadgeNumber.Text.Trim
                .ForeignZip = CType(internationalAddr, Integer)
                '' Suva Stuff jguirado...
                If ddlSuvaVersion.SelectedValue.ToString = String.Empty Then
                    .ProgramVersionType = 0
                Else
                    .ProgramVersionType = CType(ddlSuvaVersion.SelectedValue, Integer)
                End If

                .InternationalLead = CType(ViewState("InternationalLead"), Boolean)
                .FamilyIncome = ddlFamilyIncome.SelectedValue
                ssnMAsk = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
                If txtSSN.Text <> "" Then
                    .SSN = txtSSN.Text
                End If
                .FAAdvisorId = ddlFAAdvisorId.SelectedValue
                .Attendtypeid = ddlAttendTypeId.SelectedValue
                .DegCertSeekingId = ddlDegCertSeekingId.SelectedValue
                If txtTransferHours.Text <> "" Then
                    If IsNumeric(txtTransferHours.Text) Then
                        .TransferHours = CType(txtTransferHours.Text, Decimal)
                    End If

                End If
                If txtTransferDate.SelectedDate.ToString <> "" Then
                    .TransferDate = txtTransferDate.SelectedDate.ToString
                End If
                .IsDisabled = CType(ddlIsDisabled.SelectedValue, Integer)

                .EntranceInterviewDate = EntranceInterviewDate.SelectedDate
                .IsFirstTimeInSchool = chkIsFirstTimeInSchool.Checked
                .IsFirstTimePostSecSchool = chkIsFirstTimePostSecSchool.Checked

            End With
            Return leadInfo
        End Function

        ''' <summary>
        ''' Save the information in lead and migrate it to students and enrollments
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim leadMasterUpdate As New LeadFacade
            Dim testRequired As New CheckLeadTest
            Dim result As String
            Dim fac As New StartDatesFacade

            If txtLeadId.Text = "" Then
                DisplayErrorMessage("Please select a Lead first. A list of Leads can be found in the left frame of this Page.")
                Exit Sub
            End If

            dateError = False
            SetConGradDate()
            If dateError Then
                Exit Sub
            End If

            Const maxlength = 9

            If txtBadgeNumber.Text.Trim.Length > maxlength Then
                DisplayErrorMessage("Badge number should not exceed " + maxlength.ToString + " characters")
                Exit Sub
            End If

            'Validation for Start Date control
            Dim startDate = txtStartDate.SelectedDate
            If (startDate Is Nothing) Then
                DisplayErrorMessage("Start Date is a required field")
                Exit Sub
            End If


            ' 1/31/07 - ThinkTron - check if the badge number entered is in use
            ' and if so, display an error message and stop the enrollment from succeeding
            If txtBadgeNumber.Text.Length > 0 AndAlso _
                SchedulesFacade.IsBadgeNumberInUse(txtBadgeNumber.Text, "",CampusId) Then
                DisplayErrorMessage("The entered badge number is currently used by another student.  Please choose another badge number.")
                Exit Sub
            End If
            If myAdvAppSettings.AppSettings("SchedulingMethod", CampusId).ToString = "ModuleStart" And myAdvAppSettings.AppSettings("ValidateStartForModuleStart").ToLower = "yes" Then
                Dim startDatesShift As String = fac.GetStartDatesShift(ddlPrgVerId.SelectedValue)
                If (startDatesShift = "") Then
                    DisplayErrorMessage("The shift for the lead does not match the " & vbLf & "shift of the start for the program that " & vbLf & "you are trying to enroll him/her in.")
                    Exit Sub
                ElseIf (startDatesShift = "00000000-0000-0000-0000-000000000000") Then
                    DisplayErrorMessage("There are no starts setup for the program" & vbLf & "that you are trying to enroll the lead in." & vbLf & "You need to create atleast one start that " & vbLf & "match the lead's expected start date. ")
                    Exit Sub
                ElseIf (startDatesShift <> ddlShiftId.SelectedValue.ToString) Then
                    DisplayErrorMessage("The shift for the lead does not match the " & vbLf & "shift of the start for the program that " & vbLf & "you are trying to enroll him/her in.")
                    Exit Sub
                End If
                If (fac.GetTermStartDate(ddlPrgVerId.SelectedValue, txtExpStartDate.SelectedDate.ToString) = False) Then
                    DisplayErrorMessage("The expected start date of the lead does" & vbLf & "not match the start date of any starts for the " & "program")
                    Exit Sub
                End If
            End If

            ' Validate if the user select the Program Type Version
            If GetSettingString("MajorsMinorsConcentrations").ToUpper = "YES" Then
                If (ddlSuvaVersion.SelectedValue = String.Empty OrElse ddlSuvaVersion.SelectedValue = 0) Then
                    DisplayErrorMessage("Program Version Type is required")
                    Exit Sub
                End If
            End If

            'Get the interface control info into a LeadMasterInfo object
            Dim myLeadInfo As LeadMasterInfo = BuildLeadInfoExist(txtLeadId.Text)

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Only check for dups if ssn has a value and if the student is NOT and international student
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If myLeadInfo.InternationalLead = False Then
                If String.IsNullOrEmpty(txtSSN.Text) Then
                    DisplayErrorMessage("SSN is required")
                    Exit Sub
                End If
            End If

            If Not String.IsNullOrEmpty(txtSSN.Text) And myLeadInfo.InternationalLead = False Then
                Dim strDupSsnMsg As String = leadMasterUpdate.CheckDuplicateSSN(myLeadInfo.FirstName, myLeadInfo.LastName, txtSSN.Text)
                If Not String.IsNullOrEmpty(strDupSsnMsg) Then
                    DisplayErrorMessage(strDupSsnMsg)
                    Exit Sub
                End If
            End If

            Dim strEnrollStatus As String


            Dim leadEntr As LeadEntranceFacade = New LeadEntranceFacade()
            Dim retVal As Boolean = False
            retVal = leadEntr.GetHasLeadMetRequirements(txtLeadId.Text)


            If retVal = True Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If

            If retVal = False Then
                'OpenAdmissionRequirementsPage(txtLeadId.Text, ddlPrgVerId.SelectedValue)
                OpenAdmissionRequirementsPagePopUp(txtLeadId.Text, ddlPrgVerId.SelectedValue, strEnrollStatus)
                Exit Sub
            End If

            'Need to get the LeadStatus before enrolling
            Dim lscFac As New LeadStatusChangeFacade
            Dim oldLeadStatus As String = lscFac.GetLeadStatus(txtLeadId.Text)

            Dim updatePrgVersion As New LeadEntranceFacade
            updatePrgVersion.InsertPrgVersionForLead(txtLeadId.Text, ddlPrgVerId.SelectedValue)
            updatePrgVersion.UpdateExpectedDateForLead(txtLeadId.Text, txtExpStartDate.SelectedDate.ToString)

            Dim stuEnrollId As String = Guid.NewGuid.ToString
            Dim msg As String

            msg = ""

           ' result = leadMasterUpdate.UpdateLeadStudent(myLeadInfo, AdvantageSession.UserState.UserName, stuEnrollId)

            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                Exit Sub
            Else
                Try
                    'DisplayErrorMessage("The Lead was successfully enrolled")
                    '   save on syLeadStatusesChanges to record status change 
                    Dim statusChanged As New LeadStatusesInfo
                    With statusChanged
                        .LeadId = txtLeadId.Text
                        .OrigStatusId = oldLeadStatus
                        .NewStatusId = ddlStatusCodeId.SelectedValue
                        .ModDate = Date.Parse(Today.ToShortDateString)
                    End With
                    result = lscFac.InsertLeadStatusChange(statusChanged, AdvantageSession.UserState.UserName)
                    If Not result = "" Then
                        msg &= result & vbLf
                        Exit Sub
                    End If

                    Dim tranFac As New TranscriptFacade

                    'Automatically register student for regular traditional schools
                    If myAdvAppSettings.AppSettings("SchedulingMethod", CampusId).ToString.ToLower = "regulartraditional" Then
                        Dim res As String = tranFac.AutoRegisterForRegularTraditionalSchools(CType(txtExpStartDate.SelectedDate.ToString, Date), ddlPrgVerId.SelectedValue, stuEnrollId, AdvantageSession.UserState.UserName)
                        If res.Trim.Length >= 1 Then
                            msg &= res & vbLf
                        End If
                    End If


                    'If Student's start date matches any of the start dates of the program that
                    'he/she is registered for, then Register the student in all classes w/ that 
                    'start date
                    If myAdvAppSettings.AppSettings("SchedulingMethod", CampusId) = "ModuleStart" Then
                        tranFac.RegisterLeadForInitialCourses(ddlPrgVerId.SelectedValue, stuEnrollId, CType(Date.Parse(txtExpStartDate.SelectedDate.ToString), String), AdvantageSession.UserState.UserName, ddlShiftId.SelectedValue)

                        'enroll student in OnLine
                        If OnLineLib.IsEnrolledInAnyOnLineCourse(stuEnrollId) Then
                            Dim onLineInfo As OnLineInfo = (New OnLineFacade).GetOnLineInfoByStuEnrollId(stuEnrollId)
                            OnLineLib.UpdateStudentOnLineEnrollment(onLineInfo, AdvantageSession.UserState.UserName)
                        End If

                    End If

                    If (myAdvAppSettings.AppSettings("SchedulingMethod", CampusId) = "Traditional") And (myAdvAppSettings.AppSettings("UseAutomaticSchedulingForTraditional").ToUpper = "YES") Then
                        Try
                            Dim res As String = tranFac.RegisterLeadForInitialCourses(ddlPrgVerId.SelectedValue, stuEnrollId, CType(Date.Parse(txtExpStartDate.SelectedDate.ToString), String), AdvantageSession.UserState.UserName, ddlShiftId.SelectedValue)
                            If res.Length > 0 Then
                                DisplayErrorMessage("Lead was not registered in the courses because " + res)
                            End If
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            If ex.InnerException Is Nothing Then
                                msg &= (ex.Message.ToString) & vbLf
                            Else
                                msg &= (ex.InnerException.ToString) & vbLf
                            End If

                        End Try

                        'Troy 5/16/2008: Currently we are only catching DAL exceptions. It is possible that the
                        'student was not registered for any classes because of setup issues such as the wrong
                        'shift was selected for the student or classes scheduled. At this point we need to check
                        'if the student is in fact registered for any courses. If not we need to alert the user.
                        'We will be displaying a general message about the possible causes. However, in the next
                        'release we will give specific messages such as the prereqs for a course has not been
                        'scheduled so the student could not be registered in those courses.
                        If Not tranFac.IsStudentScheduledForClasses(stuEnrollId) Then
                            msg &= "The student was not scheduled for any classes." + vbLf
                            msg &= "Possible reasons are:" + vbLf
                            msg &= "-- The shift of the student does not match the shift" + vbLf
                            msg &= "on any of the classes." + vbLf
                            msg &= "-- Some of the classes require a prereq and no class" + vbLf
                            msg &= "has been setup for the prereq." + vbLf
                            msg &= "-- The student start date might be incorrect." + vbLf
                        End If

                    End If

                    'Troy:If the school is module start and also uses numeric grading we want to register the student for the relevant work units
                    If (myAdvAppSettings.AppSettings("SchedulingMethod", CampusId) = "ModuleStart" And myAdvAppSettings.AppSettings("GradesFormat", CampusId) = "Numeric") Then
                        tranFac.RegisterLeadForCoursesGrdBkComponents(ddlPrgVerId.SelectedValue, stuEnrollId, CType(Date.Parse(txtExpStartDate.SelectedDate.ToString), String), AdvantageSession.UserState.UserName, ddlShiftId.SelectedValue)
                    End If


                    If ddlScheduleID.SelectedValue <> "" Then
                        Dim schedInfo As New StudentScheduleInfo()
                        schedInfo.ScheduleId = ddlScheduleID.SelectedValue
                        schedInfo.StuEnrollId = stuEnrollId
                        schedInfo.BadgeNumber = txtBadgeNumber.Text
                        schedInfo.StartDate = Nothing ' TBD on version 2
                        schedInfo.EndDate = Nothing ' TBD on version 2
                        schedInfo.Active = True
                        SchedulesFacade.AssignStudentSchedule(schedInfo, AdvantageSession.UserState.UserName)
                    End If
                    ClearControls()
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    If ex.InnerException Is Nothing Then
                        msg &= (ex.Message.ToString) & vbLf
                    Else
                        msg &= (ex.InnerException.ToString) & vbLf
                    End If
                End Try

                chkIsInDB.Checked = True
                ''''Code Modified to resolve mantis issue id 19502 on 19th August 2010
                If Not txtFirstName.Text = "" Or Not txtLastName.Text = "" Or Not ddlLeadStatus.SelectedValue = "" Or Not ddlCampusIDSearch.SelectedValue = Guid.Empty.ToString Then
                    BuildLeadsByStatus(ddlLeadStatus.SelectedValue)
                End If
                '''''''''''''

                If msg = "" Then
                    lnkOpenRequirements.Enabled = False
                    DisplayErrorMessage("The lead was sucessfully enrolled.")

                Else
                    lnkOpenRequirements.Enabled = False
                    msg = "The lead was successfully enrolled." + vbLf + "However the following problems occured:" + vbLf + msg
                    DisplayErrorMessage(msg)
                End If
            End If
            CommonWebUtilities.RestoreItemValues(dlstLeadNames, txtLeadId.Text)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '************************* START ADD TO MRU **********************************************
            Dim mruProvider As MRURoutines

            mruProvider = New MRURoutines(Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
            mruProvider.InsertMRU(4, myLeadInfo.LeadMasterID.ToString, AdvantageSession.UserState.UserId.ToString, AdvantageSession.UserState.CampusId.ToString)

            AdvantageSession.MasterLeadId = myLeadInfo.LeadMasterID.ToString()
            '************************* END ADD TO MRU ************************************************
            CommonWebUtilities.AlertAjax("Information was saved successfully", CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager))

        End Sub

        'Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
        '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        'End Sub

        Private Sub DisplayErrorMessage(errorMessage As String)
            CommonWebUtilities.AlertAjax(errorMessage, CType(Master.FindControl("RadAjaxManager1"), RadAjaxManager))
        End Sub

        Private Sub btnNew_Click(sender As Object, e As EventArgs) Handles btnNew.Click
        End Sub

        Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
            'Delete from database and clear the screen as well. 
            Dim objCommon As New CommonUtilities
            'Dim objListGen As New DataListGenerator
            Dim ds As DataSet
            Dim sw As New StringWriter
            Dim facade As New StuEnrollFacade
            Try
                objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                ClearRHS()
                dlstLeadNames.SelectedIndex = -1
                ds = facade.PopulateDataList(txtLeadId.Text, ShowStatusLevel)
                With dlstLeadNames
                    .DataSource = ds
                    .DataBind()
                End With
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                ViewState("MODE") = "NEW"
                'Set the text box to a new guid
                txtStuEnrollId.Text = Guid.NewGuid.ToString
                'To get the enrollment Id
                LeadID = txtLeadId.Text

                Dim fullName() As String = Split(facade.GetLeadName(LeadID))
                strFirstName = fullName(0)
                strLastName = fullName(1)

                'To Generate EnrollmentID 
                txtEnrollmentId.Text = EnrollmentId.GenerateEnrollmentID(strLastName, strFirstName)

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Dim SDFControl As New SDFComponent
                'SDFControl.DeleteSDFValue(txtLeadId.Text)
                'SDFControl.GenerateControlsNew(pnlSDF, resourceId, moduleid)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                CommonWebUtilities.RestoreItemValues(dlstLeadNames, Guid.Empty.ToString)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End Sub

        Private Sub ClearRHS()
            Dim ctl As Control
            Try
                '***************************************************************
                'This section clears the Field Properties section of the page
                '***************************************************************
                For Each ctl In pnlRHS.Controls
                    If ctl.GetType Is GetType(TextBox) Then
                        CType(ctl, TextBox).Text = ""
                    End If
                    If ctl.GetType Is GetType(CheckBox) Then
                        CType(ctl, CheckBox).Checked = False
                    End If
                    If ctl.GetType Is GetType(DropDownList) Then
                        CType(ctl, DropDownList).SelectedIndex = 0
                    End If

                Next
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End Sub

        'Private Sub BuildShiftsDDL()
        '    Dim dsShift As New DataSet
        '    Dim Shift As New LeadFacade
        '    Dim intShifts As Integer = 0
        '    Try
        '        dsShift = Shift.GetShifts
        '        intShifts = dsShift.Tables(0).Rows.Count
        '    Catch ex As Exception
         '    	Dim exTracker = new AdvApplicationInsightsInitializer()
        '    	exTracker.TrackExceptionWrapper(ex)

        '        intShifts = 0
        '    End Try


        '    With ddlShiftId
        '        .DataTextField = "ShiftDescrip"
        '        .DataValueField = "ShiftId"
        '        .DataSource = dsShift
        '        .DataBind()
        '        .Items.Insert(0, New ListItem("Select", ""))
        '        .SelectedIndex = 0
        '    End With
        '    For Each item As ListItem In ddlShiftId.Items
        '        If item.Text.ToString.Trim.ToLower = "online" Then
        '            ddlShiftId.SelectedItem.Text = "online"
        '            Exit For
        '        End If
        '    Next
        'End Sub

        Public Sub dlstCategories_ItemCommand(sender As Object, e As DataListCommandEventArgs)
            'Get LeadID
            txtLeadId.Text = CType(e.CommandArgument, String)
            ProcessLeadEnrollment()
        End Sub

        Public Sub ProcessLeadEnrollment()

            'Disable The Panel Control
            lnkOpenRequirements.Enabled = True
            pnlLeadEnrollment.Visible = True

            'Enable the Save button if the user has full permission to the page
            If pObj.HasFull Then
                btnSave.Enabled = True
            Else
                btnSave.Enabled = False
            End If

            'Make The Note InVisible
            pnlNotes.Visible = False

            GetLeadInfo(txtLeadId.Text)

            chkIsInDB.Checked = False

            txtPKId.Text = txtLeadId.Text
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
            'SDF Code Starts Here

            Dim sdfControls As New SDFComponent
            sdfControls.GenerateControlsEdit(pnlSDF, resourceId, txtPKId.Text, moduleid)

            'SDF Code Ends Here
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        End Sub

        Private Sub chkStatus_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkStatus.CheckedChanged
            'Dim objListGen As New DataListGenerator
            Dim ds As DataSet
            'Dim ds2 As New DataSet
            'Dim sw As New StringWriter
            Dim objCommon As New CommonUtilities
            'Dim dv2 As New DataView
            Dim facade As New StuEnrollFacade

            Try
                ClearRHS()
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                ViewState("MODE") = "NEW"
                'Set the text box to a new guid
                txtStuEnrollId.Text = Guid.NewGuid.ToString
                ds = facade.PopulateDataList(txtLeadId.Text, ShowStatusLevel)
                With dlstLeadNames
                    .DataSource = ds
                    .DataBind()
                End With
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Redirect to error page.
                If ex.InnerException Is Nothing Then
                    Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
                Else
                    Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
                End If
                Response.Redirect("../ErrorPage.aspx")
            End Try
        End Sub

        'Private Sub BindDatalist(ByVal ds As DataSet)
        '    Dim sw As New StringWriter
        '    Dim ds2 As New DataSet
        '    Dim dv2 As New DataView
        '    Dim objListGen As New DataListGenerator
        '    Dim sStatusId As String

        '    ds2 = objListGen.StatusIdGenerator()
        '    'Set up the primary key on the datatable
        '    ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        '    If (chkStatus.Checked = True) Then 'Show Active Only

        '        Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
        '        sStatusId = row("StatusId").ToString()
        '        'Create dataview which displays active records only
        '        Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "PrgVerDescrip", DataViewRowState.CurrentRows)
        '        dv2 = dv
        '    ElseIf (chkStatus.Checked = False) Then

        '        Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
        '        sStatusId = row("StatusId").ToString()
        '        'Create dataview which displays inactive records only
        '        Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "PrgVerDescrip", DataViewRowState.CurrentRows)
        '        dv2 = dv
        '    End If
        '    With dlstLeadNames
        '        .DataSource = dv2
        '        .DataBind()
        '    End With
        '    'dtlAssignList.SelectedIndex = -1
        '    ViewState("StoredDs") = ds
        '    'I cannot write the dataset to XML as I lose the date and time formatting - Corey Masson
        '    'ds.WriteXml(sw)
        '    'ViewState("ds") = sw.ToString()
        'End Sub

        Private Sub ddlLeadStatus_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLeadStatus.SelectedIndexChanged
            If Not ddlLeadStatus.SelectedValue = "" Then
                BuildLeadsByStatus(ddlLeadStatus.SelectedValue)
            End If
        End Sub

        'Private Sub ddlLeadStatus_PreRender(sender As Object, e As EventArgs) Handles ddlLeadStatus.PreRender
        '    Try
        '        Dim ss As String = String.Empty

        '    Catch ex As Exception
         '    	Dim exTracker = new AdvApplicationInsightsInitializer()
        '    	exTracker.TrackExceptionWrapper(ex)

        '        Dim ssex As String = ex.InnerException.ToString()
        '    End Try


        'End Sub

        ''' <summary>
        ''' Helper function for BindTimeClockSchedules.  Used to build javascript that we attach
        ''' to the To Maintenance Page button to show Setup Schedules as a popup
        ''' </summary>
        ''' <param name="popupUrl"></param>
        ''' <param name="width"></param>
        ''' <param name="height"></param>
        ''' <param name="retControlId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function GetJavsScriptPopup(ByVal popupUrl As String, _
                                                      ByVal width As Integer, _
                                                      ByVal height As Integer, _
                                                      ByVal retControlId As String) As String
            Dim js As New StringBuilder
            js.Append("var strReturn; " & vbCrLf)
            js.Append("strReturn=window.showModalDialog('")
            js.Append(popupUrl)
            js.Append("',null,'resizable:yes;status:no;scrollbars=yes;minimize:yes; maximize:yes; dialogWidth:")
            js.Append(width.ToString())
            js.Append("px;dialogHeight:")
            js.Append(height.ToString())
            js.Append("px;dialogHide:true;help:no;scroll:yes'); " & vbCrLf)
            If Not retControlId Is Nothing Then
                If retControlId.Length <> 0 Then
                    js.Append("if (strReturn != null) " & vbCrLf)
                    js.Append("     document.getElementById('")
                    js.Append(retControlId)
                    js.Append("').value=strReturn;" & vbCrLf)
                End If
            End If
            Return js.ToString()
        End Function

        ''' <summary>
        ''' Binds the available schedules for the given program version
        ''' 1/31/07 Added by ThinkTron for the Clock Hours project
        ''' </summary>
        ''' <param name="ddl"></param>
        ''' <param name="PrgVerId"></param>
        ''' <remarks></remarks>
        Protected Sub BuildSchedulesForPrgVersionDDL(ByVal ddl As DropDownList, ByVal PrgVerId As String)
            Try
                If PrgVerId Is Nothing Or PrgVerId = "" Then
                    ddl.Items.Clear()
                    ddl.Items.Add(New ListItem("No schedules defined", ""))
                Else
                    ddl.DataSource = SchedulesFacade.GetSchedules(PrgVerId, True, False)
                    ddl.DataTextField = "Descrip"
                    ddl.DataValueField = "ID"
                    ddl.DataBind()
                    If ddl.Items.Count > 0 Then
                        ddl.Items.Insert(0, New ListItem("---Select---", ""))
                    Else
                        ddl.Items.Add(New ListItem("No schedules defined", ""))
                    End If

                    ' add javascript to show the scheudle maintenance page
                    Dim parentId As String = PrgVerId
                    Dim ObjId = ""
                    Dim url As String = String.Format("../MaintPopup.aspx?mod={0}&resid=511&cmpid={1}&pid={2}&objid={3}&ascx={4}", _
                        Session("mod"), Master.Master.CurrentCampusId, ParentId, ObjId, "~/AR/IMaint_SetupSchedule.ascx")
                    Dim js As String = GetJavsScriptPopup(url, 900, 600, Nothing)
                    ibToMaintPage_Schedule.Attributes.Add("onclick", js)
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddl.Items.Clear()
                ddl.Items.Add(New ListItem("No schedules defined", ""))
            End Try
        End Sub
        ''' <summary>
        ''' This gets called after the client side script opens up the setup schedule maintenance page
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Protected Sub ibToMaintPage_Schedule_Click(sender As Object, e As ImageClickEventArgs) Handles ibToMaintPage_Schedule.Click
            BuildSchedulesForPrgVersionDDL(ddlScheduleID, ddlPrgVerId.SelectedValue)
            If (ddlPrgVerId.SelectedValue = "") Then
                DisplayErrorMessage("Please select the program version")
            End If

        End Sub
        Private Sub ddlPrgVerId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlPrgVerId.SelectedIndexChanged
            If Not ddlPrgVerId.SelectedValue = "" Then
                Dim ldfacade As New LeadFacade


                '19691: QA: Loading a lead on the enroll leads page shows the transfer hours field for non clock hour programs. 
                EnableAndDisableForClockHR()
                '    Dim strBillingMethodId As String = ldfacade.GetBillingMethodByPrgVersion(ddlPrgVerId.SelectedValue)
                Dim ds As DataSet
                ds = ldfacade.GetBillingMethodByPrgVersion(ddlPrgVerId.SelectedValue)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        If Not ds.Tables(0).Rows(0)(0).ToString = "" Then
                            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlBillingMethodId, ds.Tables(0).Rows(0)(0).ToString, ds.Tables(0).Rows(0)(1).ToString)
                        End If
                    End If

                End If
                BuildSchedulesForPrgVersionDDL(ddlScheduleID, ddlPrgVerId.SelectedValue)
            End If
        End Sub

        'created new sub routine by Theresa G on sep 08 2010
        '19691: QA: Loading a lead on the enroll leads page shows the transfer hours field for non clock hour programs. 

        Private Sub EnableAndDisableForClockHr()

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            Dim ldfacade As New LeadFacade
            Dim isClockHour As Boolean
            isClockHour = ldfacade.IsClockHourProgram(ddlPrgVerId.SelectedValue)

            If isClockHour Then
                btnCalcGradDate.Visible = True
                If myAdvAppSettings.AppSettings("TrackSapAttendance", CampusId).ToString.ToLower = "byday" Then
                    btnCalcGradDate.Visible = True
                End If
                lblTransferDate0.Visible = True
                txtTransferHours.Visible = True
            Else
                btnCalcGradDate.Visible = False
                lblTransferDate0.Visible = False
                txtTransferHours.Visible = False
            End If

            If myAdvAppSettings.AppSettings("TimeClockClassSection", CampusId).ToString.ToLower = "yes" OrElse myAdvAppSettings.AppSettings("TrackSAPAttendance", CampusId).ToString.ToLower = "byclass" Then
                btnCalcGradDate.Visible = False
            End If

        End Sub
        'Private Sub OpenAdmissionRequirementsPage(ByVal leadId As String, ByVal prgVerId As String)
        '    '   setup the properties of the new window
        '    Dim sb As New StringBuilder

        '    '   append ReqGrpId to the querystring
        '    sb.Append("&LeadId=" + leadId)
        '    '   append ReqGrpId Description to the querystring
        '    sb.Append("&PrgVerId=" + prgVerId)

        '    sb.Append("&LeadName=" + txtLead.Text)

        '    sb.Append("&EnrollStatus=NotEnrolled")

        '    Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsAdReqs
        '    Dim name As String = "AdmReqSummary"
        '    Dim url As String = "../AD/AdmissionRequirementsSummary.aspx?resid=283&mod=AD" + sb.ToString
        '    CommonWebUtilities.OpenChildWindow2(Page, url, name, winSettings, Request)
        '    'CommonWebUtilities.AjaxOpenChildWindow(url,name, winSettings, Me.Master.FindControl("RadAjaxManager1"))
        'End Sub

        Private Sub OpenAdmissionRequirementsPagePopUp(leadId1 As String, prgVerId As String, strEnrollStatus As String)
            '   setup the properties of the new window
            Dim sb As New StringBuilder

            sb.Append("&LeadId=" + leadId1)
            sb.Append("&PrgVerId=" + prgVerId)
            sb.Append("&LeadName=" + txtLead.Text)
            sb.Append("&EnrollStatus=" + strEnrollStatus)
            sb.Append("&EnrollDate=" + txtEnrollDate.SelectedDate.ToString)
            sb.Append("&PrgVersion=" + ddlPrgVerId.SelectedItem.Text)
            sb.Append("&CampusId=" + CampusId)

            Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsAdReqs
            Dim name = "AdmReqSummary1"
            Dim url As String = "../AD/RequirementsDisplay.aspx?resid=283&mod=AD" + sb.ToString
            CommonWebUtilities.OpenChildWindow2(Page, url, name, winSettings, Request)
        End Sub

        'Private Function ConvertModuleNameToModuleCode(ByVal moduleName As String) As String
        '    For i As Integer = 0 To CommonWebUtilities.moduleNames.Length - 1
        '        If moduleName = CommonWebUtilities.moduleNames(i) Then Return CommonWebUtilities.moduleCodes(i)
        '    Next
        '    Return "UK"
        'End Function

        Private Sub btnSave_Command(ByVal sender As Object, ByVal e As CommandEventArgs) Handles btnSave.Command

        End Sub
        Private Sub lnkOpenRequirements_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkOpenRequirements.Click
            If ddlPrgVerId.SelectedValue = "" Then
                DisplayErrorMessage("Please select the program version")
                Exit Sub
            End If
            'Dim LeadMasterUpdate As New LeadFacade
            Dim testRequired As New CheckLeadTest
            Dim strTestStatus, strDocStatus, strEnrollStatus As String

            'Check If Lead Has Not Taken a Required Test or Test Has not been passed
            strTestStatus = testRequired.CheckIfRequiredTest(txtLeadId.Text, ddlPrgVerId.SelectedValue, CampusId)

            'If the Lead Has not taken a required doc or has an document not  approved
            strDocStatus = testRequired.CheckRequiredDocuments(txtLeadId.Text, ddlPrgVerId.SelectedValue, CampusId)


            Dim adReqs As New AdReqsFacade
            Dim strReqsMetStatus As String
            strReqsMetStatus = adReqs.CheckIfReqGroupMeetsConditions(txtLeadId.Text, ddlPrgVerId.SelectedValue, CampusId)

            If strTestStatus = "Enroll" And strDocStatus = "Enroll" And strReqsMetStatus = "Enroll" Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If

            OpenAdmissionRequirementsPagePopUp(txtLeadId.Text, ddlPrgVerId.SelectedValue, strEnrollStatus)
        End Sub

        'Private Sub OpenGender()
        '    '   setup the properties of the new window
        '    Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsSmaller
        '    Dim name As String = "Gender"
        '    Dim url As String = "../AD/GenderMessage.aspx"
        '    'CommonWebUtilities.AjaxOpenChildWindow(url,name, winSettings, Me.Master.FindControl("RadAjaxManager1"))
        '    CommonWebUtilities.OpenChildWindow2(Page, url, name, winSettings, Request)
        'End Sub

        Public Function FormatSSN(ssn As String) As String
            If Not ssn = "" Then
                Return "*****" & Mid(ssn, 6, 4)
            Else
                Return ""
            End If
        End Function

        Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnSave)
            controlsToIgnore.Add(btnSearch)
            'Add javascript code to warn the user about non saved changes. 
            'NOTE:The procedure is empty 
            BindToolTip()
        End Sub

        Protected Sub dlstLeadNames_ItemCommand(source As Object, e As DataListCommandEventArgs) Handles dlstLeadNames.ItemCommand

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            txtLeadId.Text = CType(e.CommandArgument, String)
            Dim strCheckStatus As String
            Dim strStatusMessage = ""
            strCheckStatus = (New LeadFacade).CheckIfStatusMappedToFutureStart()
            If Not strCheckStatus.Trim = "" Then
                strStatusMessage &= "The lead cannot be enrolled as the Future Start status has been currently mapped to " & strCheckStatus
                pnlLeadEnrollment.Visible = False
                DisplayErrorMessage(strStatusMessage)
                Exit Sub
            Else
                pnlLeadEnrollment.Visible = True
            End If

            ProcessLeadEnrollment()
            SetConGradDate()

            Dim ldfacade As New LeadFacade
            Dim isClockHour As Boolean
            isClockHour = ldfacade.IsClockHourProg(ddlPrgVerId.SelectedValue)

            If isClockHour Then
                btnCalcGradDate.Visible = True
            Else
                btnCalcGradDate.Visible = False
            End If

            If myAdvAppSettings.AppSettings("TimeClockClassSection", CampusId).ToString.ToLower = "yes" OrElse myAdvAppSettings.AppSettings("TrackSAPAttendance", CampusId).ToString.ToLower = "byclass" Then
                btnCalcGradDate.Visible = False
            End If
            If ddlPrgVerId.SelectedValue = "" Or ddlPrgVerId.SelectedValue = "00000000-0000-0000-0000-000000000000" Then
            Else
                EnableAndDisableForClockHr()
            End If

            'set Style to Selected Item
            CommonWebUtilities.RestoreItemValues(dlstLeadNames, txtLeadId.Text)

        End Sub

        Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
            '' Code changes made on 18 August 2010 to resolve mantis issue id 19554
            If Not txtFirstName.Text = "" Or Not txtLastName.Text = "" Or Not ddlLeadStatus.SelectedValue = "" Or Not ddlCampusIDSearch.SelectedValue = Guid.Empty.ToString Then
                BuildLeadsByStatus(ddlLeadStatus.SelectedValue)
            End If
            ''''''
        End Sub
        Private Sub ClearControls()
            txtEnrollmentId.Text = ""
            ddlStatusCodeId.SelectedIndex = 0
            txtLead.Text = ""
            ddlCampusId.SelectedIndex = 0
            ddlPrgVerId.SelectedIndex = 0
            ddlEmpId.SelectedIndex = 0
            txtEnrollDate.Clear()
            ddlAdminRep.SelectedIndex = 0
            txtExpStartDate.Clear()
            ddlShiftId.SelectedIndex = 0
            ddlEdLvlId.SelectedIndex = 0
            txtMidPtDate.Clear()
            ddlBillingMethodId.SelectedIndex = 0
            txtContractedGradDate.Clear()
            ddlTuitionCategoryId.SelectedIndex = 0
            txtTransferDate.Clear()
            ddlScheduleID.SelectedIndex = 0
            txtBadgeNumber.Text = ""
            txtStuEnrollId.Text = ""
            txtLeadId.Text = ""
            txtPKId.Text = ""
            btnCalcGradDate.Visible = False
            ddlGender.SelectedIndex = 0
            ddlRace.SelectedIndex = 0
            ddlMaritalStatus.SelectedIndex = 0
            ddlStateID.SelectedIndex = 0
            ddlNationality.SelectedIndex = 0
            ddlCitizen.SelectedIndex = 0
            ddlDependencyTypeId.SelectedIndex = 0
            ddlGeographicTypeId.SelectedIndex = 0
            ddlAdminCriteriaId.SelectedIndex = 0
            ddlHousingId.SelectedIndex = 0
            ddlAttendTypeId.SelectedIndex = 0
            ddlDegCertSeekingId.SelectedIndex = 0
            ddlFAAdvisorId.SelectedIndex = 0
            '''''''''''

            txtBirthDate.Clear()
            txtTransferHours.Text = ""
        End Sub

        Protected Sub btnCalcGradDate_Click(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles btnCalcGradDate.Click

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            If txtLeadId.Text = "" Then
                DisplayErrorMessage("Please select a Lead first. A list of Leads can be found in the left frame of this Page.")
                Exit Sub
            End If
            If ddlPrgVerId.SelectedIndex = 0 Then
                DisplayErrorMessage("Please select a Program Version to calculate Expected Graduation Date.")
                Exit Sub
            End If
            If ddlScheduleID.SelectedIndex = 0 Then
                DisplayErrorMessage("Please select a Schedule to calculate Expected Graduation Date.")
                Exit Sub
            End If
            If txtExpStartDate.SelectedDate.ToString = "" Then
                DisplayErrorMessage("Please Enter Expected Start Date to calculate Expected Graduation Date.")
                Exit Sub
            End If

            If myAdvAppSettings.AppSettings("TrackSapAttendance", CampusId).ToString.ToLower = "byday" Then
                Dim errMsg = ""
                Dim useTimeClock = False
                Dim dtfacade As New GetDateFromHoursBR
                Dim dtExpectedDate As DateTime = dtfacade.GetDateFromHours(ddlPrgVerId.SelectedValue.ToString(), CType(txtExpStartDate.SelectedDate.ToString, Date), ddlScheduleID.SelectedValue, errMsg, useTimeClock, CampusId)
                If errMsg = "" And useTimeClock = True Then

                    If txtContractedGradDate.SelectedDate.ToString = "" Then
                        txtContractedGradDate.SelectedDate = CType(dtExpectedDate.ToString("MM/dd/yyyy"), Date?)
                    Else
                        If Not Convert.ToDateTime(txtContractedGradDate.SelectedDate.ToString) = dtExpectedDate Then
                            DisplayErrorMessage("Revised Expected Graduation Date is " + dtExpectedDate.ToString("MM/dd/yyyy"))
                        End If
                    End If
                ElseIf errMsg = "" And useTimeClock = False Then
                    DisplayErrorMessage("Expected Graduation date cannot be calculated as the program needs to be a clock hour program")
                Else
                    If Not errMsg = "" Then
                        DisplayErrorMessage(errMsg)
                    End If

                End If
            Else
                DisplayErrorMessage("Expected Graduation date cannot be calculated as the Attendence is not set as ByDay")
            End If

        End Sub

        Private Sub BuildCampusGroupsSearchDDL()
            Dim campGrp As New CampusGroupsFacade

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            With ddlCampusIDSearch
                .DataTextField = "CampDescrip"
                .DataValueField = "CampusId"
                If myAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "ownleads" Then
                    .DataSource = campGrp.GetCampusesByUser(UserId)
                ElseIf myAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "permissiblecampuses" Then
                    .DataSource = campGrp.GetCampusesByUser(UserId)
                ElseIf myAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "allcampuses" Then
                    .DataSource = campGrp.GetAllCampuses()
                Else
                    .DataSource = campGrp.GetAllCampusEnrollmentByCampus(CampusId)
                End If

                .DataBind()

                .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
                .SelectedIndex = 0
            End With
        End Sub

        Protected Sub SetConGradDate()

            Dim esd, ed, sd, mpd, cgd As Date

            esd = Date.MinValue
            ed = Date.MinValue
            sd = Date.MinValue
            mpd = Date.MinValue
            cgd = Date.MinValue

            If Not txtExpStartDate.SelectedDate Is Nothing Then esd = CType(txtExpStartDate.SelectedDate, Date)

            If Not txtEnrollDate.SelectedDate Is Nothing Then ed = CType(txtEnrollDate.SelectedDate, Date)

            If Not txtStartDate.SelectedDate Is Nothing Then sd = CType(txtStartDate.SelectedDate, Date)

            If Not txtMidPtDate.SelectedDate Is Nothing Then mpd = CType(txtMidPtDate.SelectedDate, Date)

            If Not txtContractedGradDate.SelectedDate Is Nothing Then cgd = CType(txtContractedGradDate.SelectedDate, Date)

            If cgd <> Date.MinValue Then
                If esd >= cgd OrElse ed >= cgd OrElse sd >= cgd OrElse mpd >= cgd Then
                    DisplayErrorMessage("The Contracted Graduation Date must be greater then the Enrollment, Expected Start, Start, and Mid-Point dates")
                    DateError = True
                    Exit Sub
                End If
            End If

        End Sub

        Protected Sub txtBirthDate_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs) Handles txtBirthDate.SelectedDateChanged

            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()

            If IsDate(txtBirthDate.DbSelectedDate) Then
                Dim dateNow As Date = Date.Now
                Dim dateBorn = CDate(txtBirthDate.SelectedDate)
                If dateNow.Month < dateBorn.Month Or dateNow.Month = dateBorn.Month And dateNow.Day < dateBorn.Day Then
                    dateNow = dateNow.AddYears(-1)
                End If
                txtAge.Text = CType(DateDiff(DateInterval.Year, dateBorn, dateNow), String)
                If txtAge.Text < myAdvAppSettings.AppSettings("StudentAgeLimit", CampusId) Then
                    DisplayErrorMessage("The minimum age requirement for student is " & myAdvAppSettings.AppSettings("StudentAgeLimit", CampusId).ToString)
                    txtBirthDate.SelectedDate = Nothing
                    Exit Sub
                End If
            End If

        End Sub

        ''' <summary>
        ''' Get a setting as String
        ''' </summary>
        ''' <param name="settingName">The name of the setting</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function GetSettingString(settingName As String) As String
            Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
            Dim result As String = myAdvAppSettings.AppSettings(settingName).ToString()
            Return result
        End Function

    End Class
End Namespace