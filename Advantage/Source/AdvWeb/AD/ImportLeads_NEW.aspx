﻿<%@ Page Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="ImportLeads_NEW.aspx.vb" Inherits="AD_ImportLeads_NEW" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <link rel="stylesheet" type="text/css" href="../CSS/ImportLeads.css" />
    <script src="../js/CheckAll.js" type="text/javascript"></script>
    <title>Import Leads</title>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Orientation="HorizontalTop">
            <div style="overflow-x: auto; width: 100%; height: 670px">
                <table style="width: 100%; border: 0; padding: 0; border-spacing: 0; margin: 10px;" id="Table1">
                    <!-- begin right-column -->
                    <tr runat="server" id="trMain">
                        <td class="detailsframetop">
                            <%-- 
                        <table style="width: 100%; border: 0; padding: 0; border-spacing: 0" id="Table2">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" style="text-align: right">
                                    <asp:Button ID="btnSave" runat="server" Enabled="False" CssClass="save" Text="Save"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Enabled="False" CssClass="new"
                                        Text="New" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server"
                                            Enabled="False" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                                </td>
                                
                            </tr>
                        </table>  
                            --%>
                            <telerik:RadTabStrip ID="RadTabStrip1" SelectedIndex="0" runat="server" MultiPageID="RadMultiPage1"
                                 CssClass="NoBg" OnTabClick="RadTabStrip1_TabClick" ClickSelectedTab="true">
                                <Tabs>
                                    <telerik:RadTab runat="server" Text="Map Field Names" PageViewID="PageView1" Selected="true" Enabled="False">
                                    </telerik:RadTab>
                                    <telerik:RadTab runat="server" Text="Exceptions" PageViewID="PageView2" Enabled="false">
                                    </telerik:RadTab>
                                    <telerik:RadTab runat="server" Text="Duplicates" PageViewID="PageView3" Enabled="false">
                                    </telerik:RadTab>
                                    <telerik:RadTab runat="server" Text="Map Field Detail" PageViewID="PageView4" Enabled="false">
                                    </telerik:RadTab>
                                    <telerik:RadTab runat="server" Text="Initial Values" PageViewID="PageView5" Enabled="false">
                                    </telerik:RadTab>
                                    <telerik:RadTab runat="server" Text="Import File" PageViewID="PageView6" Enabled="false">
                                    </telerik:RadTab>
                                </Tabs>
                            </telerik:RadTabStrip>
                            <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0">
                                <telerik:RadPageView ID="Pageview1" runat="server" ToolTip="Map Field Names">
                                    <asp:Table ID="tbSelectFile" Style="margin-right: 250px; width: 900px; text-align: left" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblFileName" runat="Server" CssClass="label">Select File to Import<span style="color: #b71c1c">*</span></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="ddlFileNames" runat="server" CssClass="DropDownLists" AutoPostBack="true" Height="23px">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblSavedMappings" Text="Select Template" runat="server" CssClass="label"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:DropDownList ID="ddlSavedMappings" runat="server" CssClass="DropDownLists" AutoPostBack="true" Height="23px">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblStartRecord" Text="Start at Record" runat="server" CssClass="label"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:TextBox ID="txtStartRecord" runat="server" Text="1" Width="60px"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>
                                                <asp:CheckBox ID="chkFRowColHeaders" runat="server" Text="  If the first row of data contains the column name, select this box. This causes the Import File function to disregard that row of data." />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Button ID="btnReadFile" runat="server" Text=" Read File " CssClass="ProcessFile" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <asp:Table ID="Table3" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <br />
                                                <asp:Label ID="lblWarnMap" runat="server" Visible="False">
                                               <b>The import file field names shown below must be mapped to Advantage field names.</b>
                                               <br /><br />
                                               <b>Instructions:</b>
                                                <br />   
                                                <ol type="1" class="Instructions_Ol"  >
                                                    <li class="Instructions_Li">Click "Select" in order to assign a field name for that column.</li>
                                                    <li class="Instructions_Li">If a field name does not exist in "Select", it must be created in Maintenance.</li>
                                                </ol>
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Style="text-align: left">
                                                <asp:Button ID="btnPrevious" runat="server" Text="Previous Tab" Enabled="false" Visible="false" />
                                                <asp:Button ID="btnNextToExceptions" runat="server" Text="Next Tab" Visible="True" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Style="text-align: left">
                                                <asp:Label ID="lblMap" runat="server" Visible="false">
                                            * Please note in order to map all fields in advantage there must be existing values created in maintenance.
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Table runat="server">
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <telerik:RadGrid ID="rgAdvFields" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                                                Visible="false">
                                                                <HeaderStyle Width="135px" HorizontalAlign="Left" />
                                                            </telerik:RadGrid>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                    <asp:TableRow>
                                                        <asp:TableCell>
                                                            <telerik:RadGrid ID="rgFile" runat="server" Visible="false"  PageSize="25" AllowPaging="true" MasterTableView-TableLayout="Fixed">
                                                                <HeaderStyle Width="135px" HorizontalAlign="Left" />
                                                                <%--<ItemStyle Width="100px" HorizontalAlign="Left" />  --%>
                                                                <MasterTableView Width="100%" HorizontalAlign="NotSet" AutoGenerateColumns="True">
                                                                    <Columns>
                                                                        <telerik:GridBoundColumn UniqueName="Element" DataField="Element" Visible="false">
                                                                        </telerik:GridBoundColumn>
                                                                    </Columns>
                                                                </MasterTableView>
                                                            </telerik:RadGrid>
                                                        </asp:TableCell>
                                                    </asp:TableRow>
                                                </asp:Table>

                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="Pageview2" runat="server" ToolTip="Exceptions">
                                    <asp:Table ID="Table4" Style="width: 80%" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblWarnExcept" runat="server">
                                               <b>There are data exceptions in the file you are trying to import. These exceptions must be fixed or removed, from the file you are importing, before you can continue .</b>
                                                <br /><br />
                                               <b>Instructions:</b>
                                                <br />
                                                <ol type="1" class="Instructions_Ol"  >
                                                    <li class="Instructions_Li" >Locate original file you are trying to import.</li>
                                                    <li class="Instructions_Li" >Correct exceptions listed below in the original import file and save the file.</li>
                                                    <li class="Instructions_Li" >Click "Previous Tab" in order to return to "Map Field Names" tab, then "Select File to Import".</li>
                                                </ol>
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblNoExceptions" runat="server" Visible="false" Font-Bold="True">There are no exceptions to resolve.</asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Panel runat="server">
                                                    <asp:Button ID="btnPrevToMapFields" runat="server" Text="Previous Tab" />
                                                    <asp:Button ID="btnNextToDuplicates" runat="server" Text="Next Tab" />
                                                    <asp:Label Style="float: right" runat="server">
                                                        <asp:Button ID="btnExportExceptionsToExcel" runat="server" Text="Export to Excel" />
                                                    </asp:Label>
                                                </asp:Panel>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadGrid ID="rgExceptions" runat="server"  AutoGenerateColumns="False" PageSize="25" AllowPaging="true">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <AlternatingItemStyle HorizontalAlign="Left" />
                                                    <MasterTableView AutoGenerateColumns="False" EnableNoRecordsTemplate="false" ShowHeadersWhenNoRecords="false">
                                                        <Columns>
                                                            <telerik:GridBoundColumn DataField="Field" HeaderText="Field" UniqueName="Field">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Row" HeaderText="Row" UniqueName="Row">
                                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Value" HeaderText="Value" UniqueName="Value">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Exception" HeaderText="Exception" UniqueName="Exception">
                                                            </telerik:GridBoundColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="Pageview3" runat="server" ToolTip="Duplicates">
                                    <asp:Table runat="server" ID="Table5" Style="width: 80%">
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblWarnDuplicates" runat="server">
                                                    <b>There are duplicates in the file you are trying to import. These duplicates should be resolved before you continue.</b>
                                                    <br /><br />
                                                    <b>Instructions:</b>
                                                    <ol type="1" class="Instructions_Ol" >
                                                        <li class="Instructions_Li" >Review the items identified as duplicate data below.</li>
                                                        <li class="Instructions_Li" >If you want to import the duplicate into Advantage no action is required.</li>
                                                        <li class="Instructions_Li" >If you want to remove a duplicate from import, select the "Remove" check box for that item.</li>
                                                        <li class="Instructions_Li" >Once you have resolved all duplicates, select the "Next Tab" to continue.</li>
                                                    </ol>
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblNoDuplicates" runat="server" Visible="false"><b>There are no duplicates to resolve.</b></asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Panel runat="server">
                                                    <asp:Button ID="btnPrevToExceptions" runat="server" Text="Previous Tab" />
                                                    <asp:Button ID="btnNextToMapLookupValues" runat="server" Text="Next Tab" />
                                                    <asp:Label runat="server" Style="float: right">
                                                        <asp:Button ID="btnExportDuplicatesToExcel" runat="server" Text="Export to Excel" />
                                                    </asp:Label>
                                                </asp:Panel>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadGrid ID="rgDuplicates" runat="server"  AutoGenerateColumns="False" PageSize="25" AllowPaging="true"
                                                    OnItemCreated="RgDuplicatesItemCreated">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <AlternatingItemStyle HorizontalAlign="Left" />
                                                    <MasterTableView AutoGenerateColumns="False">
                                                        <Columns>
                                                            <telerik:GridTemplateColumn UniqueName="Remove">
                                                                <HeaderStyle HorizontalAlign="Center" Width="90px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                                <HeaderTemplate>
                                                                    <asp:Label runat="server" ID="lblRemove" Text="Remove"></asp:Label>
                                                                    <asp:CheckBox UniqueName="chkRemoveAll" ID="chkRemoveAll" runat="server" OnCheckedChanged="ChkRemoveAllChanged" AutoPostBack="true" >
                                                                    </asp:CheckBox>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkRemove" runat="server" OnCheckedChanged="CheckBoxChanged" AutoPostBack="true" />
                                                                </ItemTemplate>
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridBoundColumn DataField="Row" HeaderText="Row" UniqueName="Row">
                                                                <HeaderStyle HorizontalAlign="Center" Width="50px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="LastName" HeaderText="Last Name" UniqueName="LastName">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="FirstName" HeaderText="First Name" UniqueName="FirstName">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="MiddleName" HeaderText="Middle Name" UniqueName="MiddleName">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="SSN" HeaderText="SSN" UniqueName="SSN">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Address1" HeaderText="Address 1" UniqueName="Address1">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Birth Date" HeaderText="Birth Date" UniqueName="BDate">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Phone" HeaderText="Phone" UniqueName="Phone">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Phone2" HeaderText="Phone 2" UniqueName="Phone2">
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="Home Email" HeaderText="Home Email" UniqueName="HEmail">
                                                            </telerik:GridBoundColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="PageView4" runat="server" ToolTip="Map Field Detail">
                                    <asp:Table ID="Table6" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblWarnMapLookup" runat="server">
                                                    <b>There are field names in the import file that must be mapped to defined drop down selections in Advantage.</b>
                                                    <br /><br />
                                                    <b>Instructions:</b>
                                                    <ol type="1" class="Instructions_Ol" >
                                                        <li class="Instructions_Li" >For each Import File Field Name, select an appropriate Advantage Field Name.</li>
                                                        <li class="Instructions_Li" >If an Advantage Field Name is needed, add the field name in Maintenance.</li>
                                                        <li class="Instructions_Li" >When all fields are mapped, select "Next Tab" button.</li>
                                                    </ol>
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Style="text-align: left">
                                                <asp:Button ID="btnPrevToDuplicates" runat="server" Text="Previous Tab" />
                                                <asp:Button ID="btnNextToDefaultValues" runat="server" Text="Next Tab" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                    <asp:Table ID="tblMapLookupValues" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="RadPageView5" runat="server" ToolTip="Initial Values">
                                    <asp:Table ID="tblDefaultValues" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="Label2" runat="server" Width="700px">
                                                    <b>Select the default Campus, Admissions Rep and Lead Status to be applied to your imported data.  Once data is imported,
                                                        use the Lead Assignment module to reassign leads to another Campus or Admissions Rep.</b>
                                                    <br /><br />
                                                    <b>Instructions:</b>
                                                    <ol type="1" class="Instructions_Ol" >
                                                        <li class="Instructions_Li" >Select the Campus for the imported leads.</li>
                                                        <li class="Instructions_Li" >Select the Admissions Rep for the imported leads.</li>
                                                        <li class="Instructions_Li" >Select the Lead Status to be used for the imported leads.</li>
                                                        <li class="Instructions_Li" >Select the "Next Tab" to continue to "Import File".</li>
                                                    </ol>
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="left">
                                                <asp:Button ID="btnPrevToMapLookupValues" runat="server" Text="Previous Tab" />
                                                <asp:Button ID="btnNextToImportFile" runat="server" Text="Next Tab" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label runat="server" ID="lblDefaultCampus" CssClass="label" Width="100px">Campus<span style="color: #b71c1c" >*</span></asp:Label>
                                                <asp:DropDownList runat="server" ID="ddlDefaultCampus" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label runat="server" ID="lblAdmissionsRep" CssClass="label" Width="100px">Admission Rep<span style="color: #b71c1c" >*</span></asp:Label>
                                                <asp:DropDownList runat="server" ID="ddlDefaultAdmissionsRep">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell HorizontalAlign="Left">
                                                <asp:Label runat="server" ID="lblDefaultLeadStatus" CssClass="label" Width="100px">Lead Status<span style="color: #b71c1c" >*</span></asp:Label>
                                                <asp:DropDownList runat="server" ID="ddlDefaultLeadStatus">
                                                </asp:DropDownList>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </telerik:RadPageView>
                                <telerik:RadPageView ID="PageView6" runat="server" ToolTip="Import File">
                                    <asp:Table ID="Table7" runat="server">
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="lblWarnImport" runat="server" Width="700px">
                                                    <b>Import file to Advantage database</b>
                                                    <br /><br />
                                                    <b>Instructions:</b>
                                                    <ol type="1" class="Instructions_Ol" >
                                                        <li class="Instructions_Li" >To re-use the mapped values for future imports, enter a name into "Save Template as".  This saved mapping will be accessible in "Map Field Names" under "Select Template".</li>
                                                        <li class="Instructions_Li" >To import the data into Advantage, select "Import File".</li>
                                                    </ol>
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Style="text-align: left">
                                                <asp:Label ID="lblSaveMapping" runat="server" Text="Save Template as"></asp:Label>
                                                &nbsp;
                                                <asp:TextBox ID="txtMappingName" runat="server"></asp:TextBox>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Style="text-align: left">
                                                <asp:Panel runat="server">
                                                    <asp:Button ID="btnPrevToDefaultValues" runat="server" Text="Previous" />
                                                    <asp:Button ID="btnNext" runat="server" Text="Next" Enabled="false" Visible="False" />
                                                    <asp:Label runat="server" Style="float: right">
                                                        <asp:Button ID="btnExportToExcel" runat="server" Text="Export to Excel" />
                                                        <asp:Button ID="btnImport" runat="server" Text="Import File" />
                                                    </asp:Label>
                                                </asp:Panel>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>&nbsp;</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell Style="text-align: left">
                                                <asp:Label ID="lblNumOfRecords" runat="server"></asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <telerik:RadGrid ID="rgPreview" runat="server"  PageSize="25" AllowPaging="true"
                                                    OnNeedDataSource="RgPreviewNeedDataSource">
                                                    <ItemStyle HorizontalAlign="Left" />
                                                    <AlternatingItemStyle HorizontalAlign="Left" />
                                                    <MasterTableView Width="100%" HorizontalAlign="NotSet" AutoGenerateColumns="True">
                                                        <Columns>
                                                            <telerik:GridBoundColumn UniqueName="Element" DataField="Element" Visible="false">
                                                            </telerik:GridBoundColumn>
                                                        </Columns>
                                                    </MasterTableView>
                                                </telerik:RadGrid>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </telerik:RadPageView>
                            </telerik:RadMultiPage>
                        </td>
                    </tr>
                    <!-- end right-column -->
                    <tr runat="server" id="trerrmsg" align="center" style="visibility: hidden;">
                        <td>
                            <asp:Label runat="server" ID="lblErrMsg"></asp:Label></td>
                    </tr>
                </table>
            </div>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
                Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
                ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>
