<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master" Inherits="NewLead" EnableViewStateMac="True"
	CodeFile="NewLead.aspx.vb" EnableViewState="true" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
	<title>Enter New Lead</title>
	<%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
	<script type="text/javascript">
			function scrollwindow(){
				window.document.Form1.scrollposition.value = window.document.getElementById("scrollwhole").scrollTop;
			}
			function loadbody(){
				window.document.getElementById("scrollwhole").scrollTop = "<%=Session("ScrollValue")%>";
			}
			 function verifycampuschange(oldvalue){
			
			if(confirm('Campus selection change will result in clearing all other selected values.\n Do you want to continue ? ')){
			}else
			{ document.getElementById("ddlCampusId").value=oldvalue;     
			return false;
			}
			} 
	</script>
	<style type="text/css">
		.radfd_fieldset
 {
	  position: relative;
 }
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
   
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">

	<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
	</asp:ScriptManagerProxy>
	<telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
		Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
		<telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
			Width="100%" Orientation="HorizontalTop">
			<table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td class="detailsframetop">
						<table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right">
									<asp:Button ID="btnSaveNext" runat="server" Enabled="False" Text="Save" CssClass="save"
										ToolTip="Clicking this button will enable user to add new leads with out exiting this page"
										OnClick="btnSaveNext_Click"></asp:Button>
									<asp:Button ID="btnNew" runat="server" Enabled="False" Text="New" CssClass="new"
										CausesValidation="False"></asp:Button>
									<asp:Button ID="btnDelete" runat="server" Enabled="False" Text="Delete" CssClass="delete"
										CausesValidation="False"></asp:Button>
								</td>
								
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
					</td>
				</tr>
				<tr>
					<td>
						<table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="100%"
							border="0">
							<tr>
								<td class="detailsframe">
									<div id="scrollwhole" class="scrollsingleframe" onscroll="scrollwindow();">
										<!-- begin content table-->
										<table class="contentleadmastertable" cellspacing="0" cellpadding="0" width="100%">
											<tr>
												<td class="contentcellheader" style="border-top: 0; border-left: 0; border-right: 0"
													colspan="12" nowrap>
													<div style="width: 70%; float: left">
														<asp:Label ID="lblgeneral" runat="server" Font-Bold="true" CssClass="label">General Information</asp:Label></div>
													<div style="width: 12%; left: 70%; text-align: right">
														<asp:Button ID="btnSave" runat="server" Enabled="False" Text="Save and redirect to info page"
															 ToolTip="Clicking this button adds a new lead and redirects user to the Lead Info page">
														</asp:Button></div>
												</td>
											</tr>
											<tr>
												<td class="checkduplicate2" colspan="4">
												</td>
												<td class="newleaddetail" colspan="3">
													<asp:Button ID="btnDetails" Text="Details"  Width="60px" runat="server"
														CausesValidation="false" Enabled="false" Visible="false" />
												</td>
												<td class="checkduplicate2" colspan="1">
													<asp:Button ID="btnCheckDuplicates" Text="Check Duplicate" 
														runat="server" CausesValidation="false"></asp:Button>
												</td>
											</tr>
											<tr>
												<td class="leadcellleft" nowrap>
													<asp:Label ID="lblFirstName" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:TextBox ID="txtFirstName" TabIndex="1" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblGender" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:DropDownList ID="ddlGender" TabIndex="10" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblSSN" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2right" nowrap>                                                    
													<telerik:RadMaskedTextBox ID="txtSSN" TabIndex="18" runat="server" CssClass="textbox"
														Width="200px" DisplayFormatPosition="Right" DisplayPromptChar="" Mask="#########" DisplayMask="###-##-####">
													</telerik:RadMaskedTextBox>
													<asp:RegularExpressionValidator Display="None" ID="MaskedTextBoxRegularExpressionValidator" runat="server"
													 ErrorMessage="SSN format is ###-##-####"  ControlToValidate="txtSSN"  ValidationExpression="^\d{3}\d{2}\d{4}$" />
												</td>
											</tr>
											<tr>
												<td class="leadcellleft" nowrap>
													<asp:Label ID="lblMiddleName" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:TextBox ID="txtMiddleName" TabIndex="2" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblBirthDate" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<%--  <asp:TextBox ID="txtBirthDate" TabIndex="10" CssClass="textboxdate" runat="server"
													AutoPostBack="True"></asp:TextBox>
												&nbsp;<a onclick="javascript:OpenCalendarAge('ClsSect','txtBirthDate',1, true, 1945)"><img
													id="Img2" src="../UserControls/Calendar/PopUpCalendar.gif" align="top"
													border="0" runat="server"></a>--%>
													<telerik:RadDatePicker ID="txtBirthDate" MinDate="1/1/1945" runat="server" TabIndex="11" AutoPostBack="true">
													</telerik:RadDatePicker>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblFamilyIncome" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2right" nowrap>
													<asp:DropDownList ID="ddlFamilyIncome" TabIndex="19" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
											</tr>
											<tr>
												<td class="leadcellleft" nowrap>
													<asp:Label ID="lblLastName" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:TextBox ID="txtLastName" TabIndex="3" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblAge" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:TextBox ID="txtAge" TabIndex="12" CssClass="textbox" runat="server" ReadOnly="True"></asp:TextBox>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblPreviousEducation" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2right" nowrap>
													<asp:DropDownList ID="ddlPreviousEducation" TabIndex="20" CssClass="dropdownlist"
														runat="server">
													</asp:DropDownList>
												</td>
											</tr>
											<tr>
												<td class="leadcellleft" nowrap>
													<asp:Label ID="lblPrefix" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:DropDownList ID="ddlPrefix" TabIndex="4" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblRace" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:DropDownList ID="ddlRace" TabIndex="13" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblSponsor" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2right" nowrap>
													<asp:DropDownList ID="ddlSponsor" TabIndex="21" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
											</tr>
											<tr>
												<td class="leadcellleft" nowrap>
													<asp:Label ID="lblSuffix" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:DropDownList ID="ddlSuffix" TabIndex="5" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblMaritalStatus" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:DropDownList ID="ddlMaritalStatus" TabIndex="14" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblAdmissionsRep" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2right" nowrap>
													<asp:DropDownList ID="ddlAdmissionsRep" TabIndex="22" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
											</tr>
											<tr>
												<td class="leadcellleft" nowrap>
													<asp:Label ID="lblLeadStatus" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:DropDownList ID="ddlLeadStatus" TabIndex="6" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblChildren" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:TextBox ID="txtChildren" TabIndex="15" runat="server" CssClass="textbox"></asp:TextBox>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblAssignedDate" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2right" nowrap>
													<%-- <asp:TextBox ID="txtAssignedDate" TabIndex="21" CssClass="textboxdate" runat="server"></asp:TextBox>
												&nbsp;<a onclick="javascript:OpenCalendar('ClsSect','txtAssignedDate', true, 1945)"><img
													id="Img4" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
													border="0" runat="server"></a>--%>
													<telerik:RadDatePicker ID="txtAssignedDate" MinDate="1/1/1945" runat="server" TabIndex="23">
													</telerik:RadDatePicker>
												</td>
											</tr>
											<tr>
												<td class="leadcellleft" nowrap>
													<asp:Label ID="lblDependencyTypeId" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:DropDownList ID="ddlDependencyTypeId" TabIndex="7" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblGeographicTypeId" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:DropDownList ID="ddlGeographicTypeId" TabIndex="16" CssClass="dropdownlist"
														runat="server">
													</asp:DropDownList>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblDateApplied" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2right" nowrap>
													<%-- <asp:TextBox ID="txtDateApplied" TabIndex="18" CssClass="textboxdate" runat="server"></asp:TextBox>&nbsp;<a
													onclick="javascript:OpenCalendar('ClsSect','txtDateApplied', true, 1945)"><img id="Img5"
														src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server" align="absMiddle"></a>--%>
													<telerik:RadDatePicker ID="txtDateApplied" MinDate="1/1/1945" runat="server" TabIndex="24">
													</telerik:RadDatePicker>
												</td>
												<td>
												</td>
												<td>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
											</tr>
											<tr>
												<td class="leadcellleft" nowrap style="padding-bottom: 16px">
													<asp:Label ID="lblCampusId" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap style="padding-bottom: 16px">
													<asp:DropDownList ID="ddlCampusId" TabIndex="8" CssClass="dropdownlist" runat="server"
														AutoPostBack="true">
													</asp:DropDownList>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblHousingId" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:DropDownList ID="ddlHousingId" TabIndex="17" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblAdminCriteriaId" runat="server" CssClass="label" Visible="true"></asp:Label>
												</td>
												<td class="leadcell2right" nowrap>
													<asp:DropDownList ID="ddlAdminCriteriaId" TabIndex="25" CssClass="dropdownlist" runat="server"
														Visible="true">
													</asp:DropDownList>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
											</tr>
											<tr>
												<td class="leadcellleft" nowrap style="padding-bottom: 16px">
													<asp:Label ID="lblDegCertSeekingId" runat="server" CssClass="label"></asp:Label>
												</td>
												<td class="leadcell2" nowrap style="padding-bottom: 16px">
													<asp:DropDownList ID="ddlDegCertSeekingId" TabIndex="9" CssClass="dropdownlist" runat="server">
													</asp:DropDownList>
												</td>
											</tr>
											<tr>
												<td class="leadcellleft" nowrap>
													<asp:Label ID="lblEntranceInterviewDate" runat="server" CssClass="label" Visible="false"
														Text="Entrance Interview Date"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<%-- <asp:TextBox ID="txtEntranceInterviewDate" TabIndex="18" CssClass="textboxdate" runat="server" Visible=false></asp:TextBox>&nbsp;<a
													onclick="javascript:OpenCalendar('ClsSect','txtEntranceInterviewDate', true, 1945)"><img id="Img7"
														src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server" align="absMiddle" visible=false></a>--%>
													<telerik:RadDatePicker ID="txtEntranceInterviewDate" MinDate="1/1/1945" runat="server" Visible="false" tabIndex="18">
													</telerik:RadDatePicker>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
													<asp:Label ID="lblHighSchoolProgramCode" runat="server" CssClass="label" Visible="false"
														Text="Highschool Program Code"></asp:Label>
												</td>
												<td class="leadcell2" nowrap>
													<asp:TextBox ID="txtHighSchoolProgramCode" runat="Server" CssClass="textbox" Visible="false"></asp:TextBox>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
												<td class="leadcell" nowrap>
												</td>
												<td class="leadcell2right" nowrap>
												</td>
												<td class="leadcellspacer" nowrap>
													&nbsp;
												</td>
											</tr>
											<tr>
												<td class="contentcellheader" nowrap colspan="12" style="border-left: 0; border-right: 0">
													<asp:Label ID="Label1" runat="server" Font-Bold="true" CssClass="label">Select Lead Groups<font color="red">*</font></asp:Label>
												</td>
											</tr>
											<tr>
												<td style="width: 100%; padding: 3px 16px 3px 16px; text-align: left" colspan="8">
													<asp:CheckBoxList ID="chkLeadGrpId" TabIndex="26" runat="Server" CssClass="checkboxstyle"
														RepeatColumns="7">
													</asp:CheckBoxList>
												</td>
											</tr>
										</table>
										<table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
											width="100%">
											<tr>
												<td class="leadcell3">
													<table class="contenttableborder" cellspacing="0" cellpadding="0" width="100%">
														<tr>
															<td class="contentcellheader" nowrap colspan="4" style="border-top: 0; border-left: 0;
																border-right: 0">
																<asp:Label ID="lblAddress" runat="server" Font-Bold="true" CssClass="label">Address</asp:Label>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap style="padding-top: 10px">
															</td>
															<td class="contentcell4column" nowrap style="padding-top: 10px">
																<asp:CheckBox ID="chkForeignZip" TabIndex="27" CssClass="checkboxinternational" runat="server"
																	AutoPostBack="true"></asp:CheckBox>
															</td>
															<td class="leadcell4column" style="padding-top: 10px" nowrap>
																&nbsp;
															</td>
															<td class="contentcell4columnright" style="padding-top: 10px" nowrap>
																&nbsp;
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="lblAddress1" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column">
																<asp:TextBox ID="txtAddress1" TabIndex="28" CssClass="textbox" TextMode="MultiLine"
																	runat="server" MaxLength="50"></asp:TextBox>
															</td>
															<td class="leadcell4column" nowrap>
																<asp:Label ID="lblAddressType" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4columnright">
																<asp:DropDownList ID="ddlAddressType" TabIndex="34" CssClass="dropdownlist" runat="server">
																</asp:DropDownList>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="lblAddress2" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column">
																<asp:TextBox ID="txtAddress2" TabIndex="29" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
															</td>
															<td class="leadcell4column" nowrap>
																<asp:Label ID="lblCounty" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4columnright">
																<asp:DropDownList ID="ddlCounty" TabIndex="35" CssClass="dropdownlist" runat="server">
																</asp:DropDownList>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="lblCity" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column">
																<asp:TextBox ID="txtCity" TabIndex="30" CssClass="textbox" runat="server" AutoPostBack="true"
																	MaxLength="50"></asp:TextBox>
															</td>
															<td class="leadcell4column" nowrap>
																<asp:Label ID="lblAddressStatus" runat="server" CssClass="label" Visible="false">Status</asp:Label>
															</td>
															<td class="contentcell4columnright">
																<asp:DropDownList ID="ddlAddressStatus" TabIndex="33" CssClass="dropdownlist" runat="server"
																	Visible="false">
																</asp:DropDownList>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="lblStateId" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column">
																<asp:DropDownList ID="ddlStateID" TabIndex="31" CssClass="dropdownlist" runat="server">
																</asp:DropDownList>
															</td>
															<td class="leadcell4column" style="white-space: normal">
																<asp:Label ID="lblOtherState" runat="server" CssClass="label" Visible="true" readonly="true"></asp:Label>
															</td>
															<td class="contentcell4columnright">
																<asp:TextBox ID="txtOtherState" CssClass="textbox" TabIndex="31" runat="server" AutoPostBack="true"
																	Visible="true"></asp:TextBox>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="lblZip" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column">                                                                
																<telerik:RadMaskedTextBox ID="txtzip" TabIndex="32" runat="server" Width="210px" 
																	DisplayPromptChar=" " AutoPostBack="false" DisplayFormatPosition="Left">
																</telerik:RadMaskedTextBox>
															</td>
															<td class="leadcell4column" nowrap>
																&nbsp;
															</td>
															<td class="contentcell4columnright">
																&nbsp;
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
																<asp:Label ID="lblCountry" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column" style="padding-bottom: 16px">
																<asp:DropDownList ID="ddlCountry" TabIndex="33" CssClass="dropdownlist" runat="server">
																</asp:DropDownList>
															</td>
															<td class="leadcell4column" nowrap style="padding-bottom: 16px">
																&nbsp;
															</td>
															<td class="contentcell4columnright" style="padding-bottom: 16px">
																&nbsp;
															</td>
														</tr>
													</table>
												</td>
												<td class="leadcell3">
													<table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
														<tr>
															<td class="contentcellheader" nowrap colspan="4" style="border-top: 0; border-right: 0;
																border-left: 0">
																<asp:Label ID="lblPhonelabel" runat="server" Font-Bold="true" CssClass="label">Phone and Email</asp:Label>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap style="padding-top: 10px">
															</td>
															<td class="contentcell4column" nowrap style="padding-top: 10px">
																<asp:CheckBox ID="chkForeignPhone" TabIndex="35" CssClass="checkboxinternational"
																	runat="server" AutoPostBack="true"></asp:CheckBox>
															</td>
															<td class="leadcell4column" style="padding-top: 10px" nowrap>
																&nbsp;
															</td>
															<td class="contentcell4columnright" style="padding-top: 10px" nowrap>
																<asp:CheckBox ID="chkForeignPhone2" TabIndex="35" CssClass="checkboxinternational"
																	runat="server" AutoPostBack="true" Text="International"></asp:CheckBox>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="lblPhone" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column" nowrap>                                                                
																<telerik:RadMaskedTextBox ID="txtphone" TabIndex="36" runat="server" 
																	Width="200px" DisplayFormatPosition="Left" DisplayPromptChar="">
																</telerik:RadMaskedTextBox>
															</td>
															<td class="leadcell4column" nowrap>
																<asp:Label ID="lblPhone2" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4columnright" nowrap>                                                                
																<telerik:RadMaskedTextBox ID="txtPhone2" TabIndex="36" runat="server" 
																	Width="200px" DisplayFormatPosition="Left" DisplayPromptChar="">
																</telerik:RadMaskedTextBox>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="lblPhoneType" CssClass="label" runat="server">Type</asp:Label>
															</td>
															<td class="contentcell4column">
																<asp:DropDownList ID="ddlPhoneType" TabIndex="37" CssClass="dropdownlist" runat="server">
																</asp:DropDownList>
															</td>
															<td class="leadcell4column">
																<asp:Label ID="lblPhoneType2" CssClass="label" runat="server">Type</asp:Label>
															</td>
															<td class="contentcell4columnright">
																<asp:DropDownList ID="ddlPhoneType2" TabIndex="37" CssClass="dropdownlist" runat="server">
																</asp:DropDownList>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
																<asp:Label ID="lblPhoneStatus" runat="server" TabIndex="38" CssClass="label" Visible="False">Status</asp:Label>
															</td>
															<td class="contentcell4column" style="padding-bottom: 16px">
																<asp:DropDownList ID="ddlPhoneStatus" TabIndex="38" CssClass="dropdownlist" runat="server"
																	Visible="False">
																</asp:DropDownList>
															</td>
															<td class="leadcell4column" style="padding-bottom: 16px">
																<asp:Label ID="lblPhoneStatus2" runat="server" TabIndex="38" CssClass="label" Visible="False">Status</asp:Label>
															</td>
															<td class="contentcell4columnright" style="padding-bottom: 16px">
																<asp:DropDownList ID="ddlPhoneStatus2" TabIndex="38" CssClass="dropdownlist" runat="server"
																	Visible="False">
																</asp:DropDownList>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="Label2" CssClass="label" runat="server">Default Phone</asp:Label>
															</td>
															<td class="contentcell4column" style="padding-bottom: 16px">
																<asp:RadioButton ID="rdoPhone1" Text="Phone 1" GroupName="rdoPhone" runat="server"
																	TabIndex="38" CssClass="radiobutton" Checked />
															</td>
															<td>
																&nbsp;
															</td>
															<td class="contentcell4columnright" style="padding-bottom: 16px">
																<asp:RadioButton ID="rdoPhone2" Text="Phone 2" GroupName="rdoPhone" runat="server"
																	TabIndex="38" CssClass="radiobutton" />
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
																<asp:Label ID="lblWorkEmail" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column" style="padding-bottom: 16px">
																<asp:TextBox ID="txtWorkEmail" TabIndex="39" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
																<asp:RegularExpressionValidator ID="Regularexpressionvalidator5" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
																	ControlToValidate="txtWorkEmail" ErrorMessage="Invalid WorkEmail Format" Display="None">Invalid Email Format</asp:RegularExpressionValidator>
															</td>
															<td class="leadcell4column" style="padding-bottom: 16px">
																<asp:Label ID="lblHomeEmail" runat="server" CssClass="label">Home Email</asp:Label>
															</td>
															<td class="contentcell4columnright" style="padding-bottom: 16px">
																<asp:TextBox ID="txtHomeEmail" TabIndex="40" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
																<asp:RegularExpressionValidator ID="Regularexpressionvalidator6" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
																	ControlToValidate="txtHomeEmail" ErrorMessage="Invalid HomeEmail Format" Display="None">Invalid Email Format</asp:RegularExpressionValidator>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
											width="100%">
											<tr>
												<td class="leadcell3">
													<table class="contenttableborder" cellspacing="0" cellpadding="0" width="100%">
														<tr>
															<td class="contentcellheader" nowrap colspan="4" style="border-left: 0; border-right: 0;
																border-top: 0">
																<asp:Label ID="lblSource" runat="server" Font-Bold="true" CssClass="label">Source</asp:Label>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap style="padding-top: 16px">
																<asp:Label ID="lblSourceCategoryId" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column" style="padding-top: 16px">
																<asp:DropDownList ID="ddlSourceCategoryId" TabIndex="41" CssClass="dropdownlist"
																	runat="server" AutoPostBack="true">
																</asp:DropDownList>
															</td>
															<td class="leadcell4column" style="padding-top: 16px">
																<asp:Label ID="lblSourceDate" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4columnright" style="padding-top: 16px" nowrap>
																<%-- <asp:TextBox ID="txtSourceDate" TabIndex="44" CssClass="textboxdate" runat="server"></asp:TextBox>
															&nbsp;<a onclick="javascript:OpenCalendar('ClsSect','txtSourceDate', true, 1945)"><img
																id="IMG1" src="../UserControls/Calendar/PopUpCalendar.gif" align="top"
																border="0" runat="server"></a>--%>
																<telerik:RadDatePicker ID="txtSourceDate" MinDate="1/1/1945" runat="server" TabIndex="44">
																</telerik:RadDatePicker>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap style="height: 26px">
																<asp:Label ID="lblSourceTypeId" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column" style="height: 26px">
																<asp:DropDownList ID="ddlSourceTypeId" TabIndex="42" CssClass="dropdownlist" runat="server"
																	AutoPostBack="true">
																</asp:DropDownList>
															</td>
															<td class="leadcell4column" style="height: 26px">
																<asp:Label ID="lblInquiryTime" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4columnright" style="height: 26px">
																<asp:TextBox ID="txtInquiryTime" runat="server" CssClass="textbox" TabIndex="45"></asp:TextBox>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
																<asp:Label ID="lblSourceAdvertisement" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column" style="padding-bottom: 16px">
																<asp:DropDownList ID="ddlSourceAdvertisement" TabIndex="43" CssClass="dropdownlist"
																	runat="server">
																</asp:DropDownList>
															</td>
															<td class="leadcell4column" style="padding-bottom: 16px">
																<asp:Label ID="lblAdvertisementNote" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4columnright" style="padding-bottom: 16px">
																<asp:TextBox ID="txtAdvertisementNote" runat="server" CssClass="textbox" TabIndex="46"></asp:TextBox>
															</td>
														</tr>
													</table>
												</td>
												<td class="leadcell3">
													<table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
														<tr>
															<td class="contentcellheader" nowrap colspan="4" style="border-left: 0; border-right: 0;
																border-top: 0">
																<asp:Label ID="lblPersonalIdentification" runat="server" Font-Bold="true" CssClass="label">Personal Identification</asp:Label>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" style="padding-top: 16px" nowrap>
																<asp:Label ID="lblNationality" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column" style="padding-top: 16px">
																<asp:DropDownList ID="ddlNationality" TabIndex="47" CssClass="dropdownlist" runat="server">
																</asp:DropDownList>
															</td>
															<td class="leadcell4column" style="padding-top: 16px">
																<asp:Label ID="lblDrivLicStateID" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4columnright" style="padding-top: 16px">
																<asp:DropDownList ID="ddlDrivLicStateID" TabIndex="50" CssClass="dropdownlist" runat="server">
																</asp:DropDownList>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="lblCitizen" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column">
																<asp:DropDownList ID="ddlCitizen" TabIndex="48" CssClass="dropdownlist" runat="server">
																</asp:DropDownList>
															</td>
															<td class="leadcell4column">
																<asp:Label ID="lblDrivLicNumber" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4columnright">
																<asp:TextBox ID="txtDrivLicNumber" TabIndex="51" CssClass="textbox" runat="server"
																	MaxLength="50"></asp:TextBox>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
																<asp:Label ID="lblAlienNumber" runat="server" CssClass="label">Alien Number</asp:Label>
															</td>
															<td class="contentcell4column" style="padding-bottom: 16px">
																<asp:TextBox ID="txtAlienNumber" TabIndex="49" CssClass="textbox" runat="server"
																	MaxLength="50"></asp:TextBox>
															</td>
															<td class="leadcell4column" style="padding-bottom: 16px">
																&nbsp;
															</td>
															<td class="contentcell4columnright" style="padding-bottom: 16px">
																&nbsp;
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<table class="contentleadmastertable" cellspacing="0" style="border-top: 0" cellpadding="0"
											width="100%">
											<tr>
												<td class="leadcell3">
													<table class="contenttableborder" cellspacing="0" cellpadding="0" width="100%">
														<tr>
															<td class="contentcellheader" nowrap colspan="4" style="border-left: 0; border-right: 0;
																border-top: 0">
																<asp:Label ID="lblInterestedIn" runat="server" Font-Bold="true" CssClass="label">Interested In</asp:Label>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" style="padding-top: 16px" nowrap>
																<asp:Label ID="lblAreaID" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column" style="padding-top: 16px">
																<asp:DropDownList ID="ddlAreaID" TabIndex="52" CssClass="dropdownlist" runat="server"
																	AutoPostBack="true" Width="230px">
																</asp:DropDownList>
															</td>
															<td class="leadcell4column" style="padding-bottom: 16px">
																&nbsp;
															</td>
															<td class="contentcell4columnright" style="padding-bottom: 16px">
																&nbsp;
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="lblProgramID" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column" colspan="3">
																<asp:DropDownList ID="ddlProgramID" TabIndex="53" CssClass="dropdownlist" runat="server"
																	AutoPostBack="true">
																</asp:DropDownList>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="lblPrgVerId" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column" colspan="3">
																<asp:DropDownList ID="ddlPrgVerId" TabIndex="54" CssClass="dropdownlist" runat="server">
																</asp:DropDownList>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" nowrap>
																<asp:Label ID="lblShiftID" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4column">
																<asp:DropDownList ID="ddlShiftID" TabIndex="55" CssClass="dropdownlist" runat="server"
																	Width="230px">
																</asp:DropDownList>
															</td>
															<td class="leadcell4column">
																<asp:Label ID="lblExpectedStart" runat="server" CssClass="label"></asp:Label>
															</td>
															<td class="contentcell4columnright" nowrap>
																<%-- <asp:TextBox ID="txtExpectedStart" TabIndex="52" CssClass="textboxdate" runat="server"></asp:TextBox>
															<a onclick="javascript:OpenCalendar('ClsSect','txtExpectedStart', true, 1945)">
																<img id="Img3" src="../UserControls/Calendar/PopUpCalendar.gif" align="top"
																	border="0" runat="server"></a>AAA--%>
																<telerik:RadDatePicker ID="txtExpectedStart" MinDate="1/1/1945" runat="server" TabIndex="56">
																</telerik:RadDatePicker>
															</td>
														</tr>
													</table>
												</td>
												<td class="leadcell3">
													<table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
														<tr>
															<td class="contentcellheader"  colspan="6"  style="border-left: 0; border-right: 0;
																border-top: 0">
																<asp:Label ID="Label5" runat="server" Font-Bold="true" CssClass="label">Comments</asp:Label>
															</td>
														</tr>
														<tr>
															<td class="leadcell4columnleft" style="padding-top: 16px" nowrap>
																<asp:Label ID="lblComments" runat="server" CssClass="label">Comments</asp:Label>
															</td>
															<td class="contentcell4columnright" style="padding-top: 16px; padding-bottom: 16px"
																colspan="5">
																<asp:TextBox ID="txtComments" TabIndex="57" runat="server" CssClass="textbox" Columns="57"
																	TextMode="Multiline" Rows="6"></asp:TextBox>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
										<table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
											<tr>
												<td class="spacertables">
												</td>
											</tr>
											<tr>
												<td>
													<asp:Label ID="Label3" runat="server" Font-Bold="true" CssClass="label" Visible="false"></asp:Label>
												</td>
											</tr>
											<tr>
												<td class="spacertables">
												</td>
											</tr>
										</table>
										<asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
											<table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
												<tr>
													<td class="spacertables">
													</td>
												</tr>
												<tr>
													<td class="contentcellheader" nowrap>
														<asp:Label ID="Label4" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label>
													</td>
												</tr>
												<tr>
													<td class="spacertables">
													</td>
												</tr>
												<tr>
													<td class="contentcell2">
														<asp:Panel ID="pnlSDF" TabIndex="58" runat="server" EnableViewState="false">
														</asp:Panel>
													</td>
												</tr>
											</table>
										</asp:Panel>
										<table class="contenttable" cellspacing="0" runat="server" cellpadding="0" width="100%" visible="false">
											<tr>
															<td class="contentcellheader"  colspan="6"  style="border-left: 0; border-right: 0;
																border-top: 0">
													<asp:Label ID="Label6" runat="server" CssClass="tothemecomments" Visible="False">Objectives</asp:Label>
												</td>
												<td class="contentcell2" colspan="8">
													<asp:TextBox ID="txtResumeObjective" TabIndex="59" runat="server" MaxLength="300"
														Width="0px" Columns="60" TextMode="MultiLine" Rows="3" CssClass="tocommentsnowrap"
														Visible="false"></asp:TextBox>
												</td>
											</tr>
										</table>
										<!-- end content table-->
									</div>
								</td>
							</tr>
						</table>
						<!-- end rightcolumn -->
					</td>
				</tr>
			</table>
			<asp:HiddenField ID="txtDate" runat="server" />
			<asp:HiddenField ID="txtPKID" runat="server" />
			<asp:HiddenField ID="txtLeadMasterID" runat="server" />
			<asp:HiddenField ID="lblSourceTypeID1" runat="server" />
			<asp:HiddenField ID="txtLeadId" runat="server" />
			<asp:HiddenField ID="txtCreatedDate" runat="server" />
			<asp:HiddenField ID="txtModDate" runat="server" />
			<asp:HiddenField ID="txtUserId" runat="server" />
			<asp:HiddenField ID="txtCampusId" runat="server" />
			<asp:HiddenField ID="txtLeadStatusId" runat="server" />
			<asp:HiddenField ID="txtVal" runat="server" />
			<asp:PlaceHolder ID="PlaceHolder1" runat="server" Visible="false"></asp:PlaceHolder>
			
			<asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
			<asp:DropDownList ID="ddlSourceTypeId1" CssClass="dropdownlist" runat="server" Visible="False">
			</asp:DropDownList>
			<input type="hidden" name="scrollposition" id="scrollposition" />
			<input id="__programid" type="hidden" name="__programid" runat="server" />
			<input id="__prgverid" type="hidden" name="__prgverid" runat="server" />
			<input id="__areaid" type="hidden" name="__areaid" runat="server" />
			<input id="__hiddensubmit" type="hidden" name="__hiddensubmit" runat="server" />
			<input id="__fieldchanges" type="hidden" name="__fieldchanges" runat="server" />
			<input id="__sourcetypeid" type="hidden" name="__sourcetypeid" runat="server" />
			<input id="__sourceadvid" type="hidden" name="__sourceadvid" runat="server" />
			<input id="dup" type="hidden" runat="server" value="no" clientidmode="Static" />
			<!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
			 <telerik:RadNotification runat="server" ID="RadNotification1" 
				Text="This is a test" ShowCloseButton="true" 
				Width="400px" Height="125px"
				TitleIcon="" 
				Position="Center" Title="Message" 
				EnableRoundedCorners="true" 
				EnableShadow="true" 
				Animation="Fade" 
				AnimationDuration="1000" 
				   
				style="padding-left:120px; padding-top:5px; word-spacing:2pt;"> 
	</telerik:RadNotification> 	
		</telerik:RadPane>
	</telerik:RadSplitter>
</asp:Content>
