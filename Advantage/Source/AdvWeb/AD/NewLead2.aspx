﻿<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>

<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="NewLead2.aspx.vb" Inherits="NewLead2" %>
<%@ MasterType  virtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
      <AjaxSettings>

            <telerik:AjaxSetting AjaxControlID="AddressSection">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="AddressSection" LoadingPanelID="RadAjaxLoadingPanelContent"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="PhoneEmailSection">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="PhoneEmailSection" LoadingPanelID="RadAjaxLoadingPanelContent"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="SourceSection">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="SourceSection" LoadingPanelID="RadAjaxLoadingPanelContent"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="InterestedInSection">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="InterestedInSection" LoadingPanelID="RadAjaxLoadingPanelContent"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
<link rel="stylesheet" type="text/css" href="../CSS/main.css" />
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
   <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelContent" runat="server"></telerik:RadAjaxLoadingPanel>
<script language="javascript" type="text/javascript">

    function CalcAge(sender, eventArgs) {
        var AgeTextBox = document.getElementById('txtAge')
        var DateString = eventArgs.get_newValue();
        var BirthDate = new Date(DateString);
        var BirthDay = BirthDate.getDate();
        var BirthMonth = BirthDate.getMonth();
        var BirthYear = BirthDate.getFullYear();

        var Today = new Date;
        var TYear = Today.getFullYear();
        var TMonth = Today.getMonth();
        var TDay = Today.getDate();
        var Age = Today.getFullYear() - BirthDate.getFullYear();
        if ((TMonth > BirthMonth) || (TMonth == BirthMonth & TDay >= BirthDay)) {
            // alert(Age);
            AgeTextBox.value = Age;
        } 
        else {
            Age = Age - 1;
            AgeTextBox.value = Age;
           // alert(AgeTextBox.value);
        }
     } 

            function verifycampuschange(clientsideID,oldvalue){
            
            if(confirm('Campus selection change will result in clearing all other selected values.\n Do you want to continue ? ')){
            }else
            { document.getElementById(clientsideID).value=oldvalue;     
            return false;
            }
            }   

</script>

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">

			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								 <td class="menuframe" align="right">
                                <asp:Button ID="btnSaveNext" runat="server" Enabled="False" Text="Save" CssClass="save" ToolTip="Clicking this button will enable user to add new leads with out exiting this page">
                                </asp:Button>
                                <asp:Button ID="btnNew" runat="server" Enabled="False" Text="New" CssClass="new"
                                    CausesValidation="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" Enabled="False" Text="Delete" CssClass="delete"
                                    CausesValidation="False"></asp:Button></td>
                           
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->			
                        <table width="980px" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                    <div id="scrollwhole" class="scrollsingleframe" runat="server" style="margin:10px;">
                                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                            <div class="Label"><asp:Literal ID="ltMessage" runat="server"></asp:Literal></div>
                                            &nbsp;&nbsp;&nbsp;<asp:Button ID="btnYes" runat="server" Text="Yes"  Width="100px" />
                                            <asp:Button ID="btnNo" runat="server" Text="No"  Width="100px"  /><br />
                                        </asp:Panel>
                                        <table cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td colspan="2" align="right" style="padding-right:35px;">
                                                <asp:Button ID="btnCheckDuplicates" Text="Check Duplicate" CssClass="checkduplicate"
                                                runat="server" CausesValidation="false"></asp:Button>
                                                 <telerik:RadButton ID="btnSave" runat="server" Enabled="False" 
                                                 Text="Save and redirect to info page"  
                                                 ToolTip="Clicking this button adds a new lead and redirects user to the Lead Info page">
                                                 </telerik:RadButton>
                                                </td>
                                            </tr>
                                            </table>
                                            <div class="LeadForm" >
                                            <div class="TitleSectionRow">
                                                <div id="caption1" class="CaptionLabel" runat="server"> General Information </div>
                                            </div>
                                            <div class="formRow15">
                                            <div>
                                            <div class="FLabel60"><asp:Label ID="lblPrefix" runat="server" Visible="false" ></asp:Label></div>  
                                            <div class="FLabel135"><asp:Label ID="lblFirstName" runat="server" ></asp:Label></div>
                                            <div class="FLabel135"><asp:Label ID="lblMiddleName" runat="server" ></asp:Label></div>
                                            <div class="FLabel135"><asp:Label ID="lblLastName" runat="server" ></asp:Label></div>
                                            <div class="FLabel60"><asp:Label ID="lblSuffix" runat="server" ></asp:Label></div>
                                            <div class="FLabel140"> <asp:Label ID="lblBirthDate" runat="server" ></asp:Label></div>
                                            <div class="FLabel60"><asp:Label ID="lblAge" runat="server" ></asp:Label> </div>   
                                            </div>
                                            <div class="formControlRow">
                                            <span><asp:DropDownList ID="ddlPrefix"   runat="server" CssClass="dropdownlistAdv" Visible="false"></asp:DropDownList> </span>
                                            <span><asp:TextBox ID="txtFirstName" runat="server" MaxLength="50"></asp:TextBox></span>
                                            <span><asp:TextBox ID="txtMiddleName" runat="server" MaxLength="50"></asp:TextBox></span>
                                            <span><asp:TextBox ID="txtLastName"   runat="server" MaxLength="50"></asp:TextBox></span>
                                            <span><asp:DropDownList ID="ddlSuffix"   runat="server"></asp:DropDownList></span>
                                            <span><telerik:RadDatePicker ID="txtBirthDate" MinDate="1/1/1945" runat="server"> <ClientEvents OnDateSelected="CalcAge" /> </telerik:RadDatePicker></span>
                                            <span><asp:TextBox ID="txtAge" runat="server" ReadOnly="True" TabIndex = "-1" Width="25px" ClientIDMode="Static" ></asp:TextBox></span>
                                            </div>
                                            </div>
                                            <div class="formRow25">
                                            <div>
                                            <div class="FLabel135"><asp:Label ID="lblSSN" runat="server" ></asp:Label></div>
                                            <div class="FLabel150"> <asp:Label ID="lblDependencyTypeId" runat="server" ></asp:Label> </div>
                                            <div class="FLabel80"><asp:Label ID="lblGender" runat="server"></asp:Label></div>
                                            <div class="FLabel150" style="margin-left:100px;"> <asp:Label ID="lblAssignedDate" runat="server" ></asp:Label></div>
                                            <div class="FLabel150"> <asp:Label ID="lblDateApplied" runat="server" ></asp:Label></div>
                                            </div>
                                            <div class="formControlRow">
                                            <span><ew:MaskedTextBox ID="txtSSN"  runat="server" ></ew:MaskedTextBox></span>
                                            <span><asp:DropDownList ID="ddlDependencyTypeId" runat="server"  Width="150px"></asp:DropDownList></span>
                                            <span style="margin-left:5px;"><asp:DropDownList ID="ddlGender"  runat="server"> </asp:DropDownList></span>
                                            <span style="margin-left:100px;"><telerik:RadDatePicker ID="txtAssignedDate" MinDate="1/1/1945" runat="server"> </telerik:RadDatePicker>  </span>
                                            <span > <telerik:RadDatePicker ID="txtDateApplied" MinDate="1/1/1945" runat="server"></telerik:RadDatePicker></span>
                                            </div>
                                             </div>
                                            <div class="formRow25">
                                            <div>
                                            <div class="FLabel300"><asp:Label ID="lblCampusId" runat="server" ></asp:Label></div>
                                            <div class="FLabel300" style="margin-left:175px;"><asp:Label ID="lblLeadStatus" runat="server" ></asp:Label></div>
                                            </div>
                                            <div class="formControlRow">
                                            <span><asp:DropDownList ID="ddlCampusId"   runat="server" AutoPostBack="true" Width="300px"> </asp:DropDownList></span>
                                            <span style="margin-left:175px;"><asp:DropDownList ID="ddlLeadStatus"   runat="server" Width="300px"></asp:DropDownList></span>
                                            </div>
                                             </div>
                                            <div class="formRow15">
                                            <div>
                                             <div class="FLabel300"><asp:Label ID="lblDegCertSeekingId" runat="server" ></asp:Label>  </div>
                                             <div class="FLabel300" style="margin-left:175px;"><asp:Label ID="lblAdmissionsRep" runat="server" ></asp:Label></div>
                                            </div>
                                            <div class="formControlRow">
                                            <span><asp:DropDownList ID="ddlDegCertSeekingId"    runat="server" AutoPostBack="true" Width="300px"> </asp:DropDownList></span>
                                            <span style="margin-left:175px;"><asp:DropDownList ID="ddlAdmissionsRep"    runat="server" Width="300px"> </asp:DropDownList></span>
                                            </div>
                                             </div>
                                            <div class="formRow15">
                                            <div>
                                            <div class="FLabel300" ><asp:Label ID="lblAdminCriteriaId" runat="server"  Visible="true"></asp:Label> </div>
                                            <div class="FLabel300" style="margin-left:175px;"><asp:Label ID="lblSponsor" runat="server" ></asp:Label></div>
                                            </div>
                                            <div class="formControlRow">
                                            <span><asp:DropDownList ID="ddlAdminCriteriaId"    runat="server" Visible="true" Width="300px"></asp:DropDownList>  </span>
                                            <span style="margin-left:175px;"><asp:DropDownList ID="ddlSponsor"   runat="server" Width="300px"></asp:DropDownList></span>
                                            </div>
                                             </div>
                                            <div class="formRow15">
                                            <div>
                                            <div class="FLabel300" ><asp:Label ID="lblPreviousEducation" runat="server" ></asp:Label> </div>
                                            <div class="FLabel300" style="margin-left:175px;"> <asp:Label ID="lblHousingId" runat="server" ></asp:Label></div>
                                            </div>
                                            <div class="formControlRow">
                                            <span><asp:DropDownList ID="ddlPreviousEducation"   runat="server" Width="300px"></asp:DropDownList> </span>
                                            <span style="margin-left:175px;"><asp:DropDownList ID="ddlHousingId"   runat="server" Width="300px"> </asp:DropDownList> </span>
                                            </div>
                                             </div>
                                            <div class="formRow15">
                                            <div>
                                            <div class="FLabel300" ><asp:Label ID="lblGeographicTypeId" runat="server" ></asp:Label> </div>
                                            <div class="FLabel300" style="margin-left:175px;"><asp:Label ID="lblFamilyIncome" runat="server" ></asp:Label> </div>
                                            </div>
                                            <div class="formControlRow">
                                            <span><asp:DropDownList ID="ddlGeographicTypeId"    runat="server" Width="300px"></asp:DropDownList> </span>
                                            <span style="margin-left:175px;"><asp:DropDownList ID="ddlFamilyIncome"    runat="server" Width="300px">  </asp:DropDownList>  </span>
                                            </div>
                                             </div>
                                            <div class="formRow15">
                                            <div>
                                            <div class="FLabel300"><asp:Label ID="lblRace" runat="server" ></asp:Label></div>
                                            <div class="FLabel200" style="margin-left:175px;"><asp:Label ID="lblMaritalStatus" runat="server" ></asp:Label>  </div>
                                            <div class="FLabel80" style="margin-left:15px;"><asp:Label ID="lblChildren" runat="server" ></asp:Label> </div>
                                            </div>
                                            <div class="formControlRow">
                                            <span><asp:DropDownList ID="ddlRace"    runat="server" Width="300px"></asp:DropDownList> </span>
                                            <span style="margin-left:175px;"><asp:DropDownList ID="ddlMaritalStatus"  runat="server" Width="200px"> </asp:DropDownList></span>
                                            <span style="margin-left:15px;"><asp:TextBox ID="txtChildren"  runat="server" Width="75px" ></asp:TextBox></span>
                                            </div>
                                             </div>
                                            <div class="formRow15">
                                            <div >
                                            <span><asp:Label ID="lblHighSchoolProgramCode" runat="server"  Visible="false" Text="Highschool Program Code"></asp:Label> </span>
                                            <span style="margin-left:350px;"> <asp:Label ID="lblEntranceInterviewDate" runat="server"  Visible="false" Text="Entrance Interview Date"></asp:Label>  </span>
                                            </div>
                                            <div class="formControlRow">
                                            <span><asp:TextBox ID="txtHighSchoolProgramCode" runat="Server"  Visible="false"></asp:TextBox> </span>
                                            <span><telerik:RadDatePicker ID="txtEntranceInterviewDate" MinDate="1/1/1945" runat="server" Visible="false"></telerik:RadDatePicker></span>
                                            </div>
                                             </div>
                                            <div class="TitleSectionRow">
                                                <div id="caption2" class="CaptionLabel" runat="server"> Select Lead Groups<span style="color: #b71c1c;font-weight:bold;">*</span> </div>
                                            </div>
                                            <div class="formRow5">
                                            <div >
                                            <span></span>
                                            <span style="margin-left:350px;"></span>
                                            </div>
                                            <div class="formControlRow">
                                            <span><asp:CheckBoxList ID="chkLeadGrpId" CssClass="nothing"  runat="Server" RepeatColumns="4"> </asp:CheckBoxList>  </span>
                                            </div>
                                             </div> 
                                             <div id="AddressSection" runat="server">  
                                            <div class="TitleSectionRow" style="margin-top:25px;">
                                                <div id="caption3" class="CaptionLabel" runat="server">Address</div>
                                            </div> 
                                            
                                            <div class="formRow25">
                                            <div>
                                            <div class="FLabel200"><asp:Label ID="lblAddress1" runat="server" ></asp:Label></div>
                                            <div class="FLabel200" style="margin-left:5px;"><asp:Label ID="lblAddress2" runat="server" ></asp:Label> </div>
                                            <div class="FLabel200" style="margin-left:5px;"><asp:Label ID="lblCity" runat="server" ></asp:Label></div>
                                            </div>
                                            <div class="formControlRow">
                                            <span><asp:TextBox ID="txtAddress1" runat="server" MaxLength="50" Width="200px"></asp:TextBox></span>
                                            <span><asp:TextBox ID="txtAddress2"   runat="server" MaxLength="50" Width="200px"></asp:TextBox> </span>
                                            <span><asp:TextBox ID="txtCity"  runat="server" AutoPostBack="true" MaxLength="50" Width="200px"></asp:TextBox></span>
                                            <span><asp:CheckBox ID="chkForeignZip" runat="server" AutoPostBack="true" /></span>
                                            <span><asp:DropDownList ID="ddlAddressStatus"   runat="server" Visible="false" ></asp:DropDownList></span>
                                            </div>
                                             </div>    
                                             <div class="formRow5">
                                             <div>
                                             <div class="FLabel208"><asp:Label ID="lblAddressStatus" runat="server"  Visible="false">Status</asp:Label></div>
                                             <div class="FLabel208"><asp:Label ID="lblStateId" runat="server" ></asp:Label></div>
                                             <div class="FLabel208"><asp:Label ID="lblOtherState" runat="server"  Visible="true" readonly="true"></asp:Label></div>
                                             <div class="FLabel208"> <asp:Label ID="lblZip" runat="server" ></asp:Label></div>
                                             <div class="FLabel208"><asp:Label ID="lblCountry" runat="server" ></asp:Label></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span><asp:DropDownList ID="ddlStateID"   runat="server" Width="208px" ></asp:DropDownList></span>
                                             <span> <asp:TextBox ID="txtOtherState"   runat="server" AutoPostBack="true" Visible="true" Width="200px"></asp:TextBox></span>
                                             <span><ew:MaskedTextBox ID="txtZip"  runat="server" Width="200px" > </ew:MaskedTextBox></span>     
                                             <span><asp:DropDownList ID="ddlCountry"  runat="server" Width="208px" > </asp:DropDownList></span>                                
                                             </div>
                                             </div>
                                             </div> 
                                             <div class="formRow5">
                                             <div>
                                             <div class="FLabel208"> <asp:Label ID="lblCounty" runat="server" ></asp:Label> </div>
                                             <div class="FLabel125"><asp:Label ID="lblAddressType" runat="server" ></asp:Label></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span> <asp:DropDownList ID="ddlCounty"  runat="server" Width="208px" > </asp:DropDownList></span>   
                                             <span><asp:DropDownList ID="ddlAddressType"   runat="server" Width="125px" ></asp:DropDownList>  </span>                          
                                             </div>
                                             </div>
                                             </div> 
                                             </div>  
                                             <div id="PhoneEmailSection" runat="server">
                                             <div class="TitleSectionRow" style="margin-top:25px;">
                                                <div id="caption4" class="CaptionLabel" runat="server">Phone And Email</div>
                                            </div>
                                             <div class="formRow25">
                                             <div>
                                             <div class="FLabel130"><asp:Label ID="lblPhone" runat="server" ></asp:Label></div>
                                             <div class="FLabel90"><asp:Label ID="lblPhoneType"  runat="server">Type</asp:Label></div>
                                             <div class="FLabel135"><asp:DropDownList ID="ddlPhoneStatus"   runat="server" Visible="False" > </asp:DropDownList></div>
                                             <div class="FLabel135"><asp:Label ID="lblPhoneStatus" runat="server"   Visible="False">Status</asp:Label> </div>
                                             <div class="FLabel135" style="margin-left:240px;"> <asp:Label ID="lblHomeEmail" runat="server" >Home Email</asp:Label></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div> 
                                             <span> <ew:MaskedTextBox ID="txtPhone"  runat="server" > </ew:MaskedTextBox></span>  
                                             <span> <asp:DropDownList ID="ddlPhoneType"   runat="server"> </asp:DropDownList> </span> 
                                             <span> <asp:CheckBox ID="chkForeignPhone"  runat="server" AutoPostBack="true"></asp:CheckBox></span> 
                                             <span style="margin-left:150px;"> <asp:TextBox ID="txtHomeEmail"   runat="server" MaxLength="50" Width="200px"></asp:TextBox>  
                                             <asp:RegularExpressionValidator ID="Regularexpressionvalidator6" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                              ControlToValidate="txtHomeEmail" ErrorMessage="Invalid HomeEmail Format" Display="None">Invalid Email Format</asp:RegularExpressionValidator> 
                                             </span> 
                                             </div>
                                             </div>
                                             </div>       
                                             <div class="formRow5">
                                             <div>
                                             <div class="FLabel130"><asp:Label ID="lblPhone2" runat="server" ></asp:Label> </div>
                                             <div class="FLabel90"> <asp:Label ID="lblPhoneType2"  runat="server">Type</asp:Label> </div>
                                             <div class="FLabel135"><asp:Label ID="lblPhoneStatus2" runat="server"   Visible="False">Status</asp:Label></div>
                                              <div class="FLabel130" style="margin-left:240px;"> <asp:Label ID="lblWorkEmail" runat="server" ></asp:Label></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span>  <ew:MaskedTextBox ID="txtPhone2"  runat="server" > </ew:MaskedTextBox></span>
                                             <span> <asp:DropDownList ID="ddlPhoneType2"   runat="server"></asp:DropDownList></span>
                                             <span><asp:DropDownList ID="ddlPhoneStatus2"   runat="server" Visible="False" > </asp:DropDownList></span>
                                             <span><asp:CheckBox ID="chkForeignPhone2"   runat="server" AutoPostBack="true" Text="International"></asp:CheckBox></span>
                                             <span style="margin-left:150px;">
                                             <asp:TextBox ID="txtWorkEmail"   runat="server" MaxLength="50" Width="200px"></asp:TextBox>
                                             <asp:RegularExpressionValidator ID="Regularexpressionvalidator5" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                             ControlToValidate="txtWorkEmail" ErrorMessage="Invalid WorkEmail Format" Display="None">Invalid Email Format</asp:RegularExpressionValidator>
                                             </span>
                                             </div>
                                             </div>
                                             </div>   
                                             <div class="formRow5">
                                             <div>
                                             <div class="FLabel135"></div>
                                             <div class="FLabel135" style="margin-left:115px;"></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span>Default Phone Number: </span>
                                             <span><asp:RadioButton ID="rdoPhone1" Text="Phone 1"  GroupName="rdoPhone" runat="server"  Checked="true"/></span>
                                             <span><asp:RadioButton ID="rdoPhone2" Text="Phone 2"  GroupName="rdoPhone" runat="server"/></span>
                                             <span> </span>  
                                             <span>  </span> 
                                             <span></span>
                                             <span></span>
                                             <span>  </span>
                                             <span> </span>
                                             <span></span>
                                             </div>
                                             </div>
                                             </div>  
                                             </div>       
                                             <div id="SourceSection" runat="server">                             
                                             <div class="TitleSectionRow" style="margin-top:25px;">
                                                <div id="caption5" class="CaptionLabel" runat="server">Source</div>
                                            </div>
                                             <div class="formRow5">
                                             <div>
                                             <div class="FLabel250"><asp:Label ID="lblSourceCategoryId" runat="server" ></asp:Label></div>
                                             <div class="FLabel250" ><asp:Label ID="lblSourceTypeId" runat="server" ></asp:Label></div>
                                             <div class="FLabel250" > <asp:Label ID="lblSourceAdvertisement" runat="server"></asp:Label></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span>  <asp:dropdownlist id="ddlSourceCategoryId"  Runat="server" AutoPostBack="true" Width="250px" ></asp:dropdownlist></span>                                                                                        
                                             <span> <asp:dropdownlist id="ddlSourceTypeId" Runat="server"  AutoPostBack="true" Width="250px" > </asp:dropdownlist>  </span> 
                                             <span><asp:dropdownlist id="ddlSourceAdvertisement"  Runat="server" Width="250px" ></asp:dropdownlist></span>
                                             </div>
                                             </div>
                                             </div>   
                                             <div class="formRow5">
                                             <div>
                                             <div class="FLabel150" >  <asp:Label ID="lblSourceDate" runat="server" ></asp:Label></div>
                                             <div class="FLabel135" ><asp:Label ID="lblInquiryTime" runat="server" ></asp:Label></div>
                                             <div class="FLabel135"><asp:Label ID="lblAdvertisementNote" runat="server"></asp:Label></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span><telerik:RadDatePicker ID="txtSourceDate" MinDate="1/1/1945" runat="server"> </telerik:RadDatePicker> </span>  
                                             <span><asp:TextBox ID="txtInquiryTime" runat="server" ></asp:TextBox></span>
                                             <span> <asp:TextBox ID="txtAdvertisementNote" runat="server" ></asp:TextBox>  </span>
                                             </div>
                                             </div>
                                             </div> 
                                             </div> 
                                             <div id="PersonalIdSection" runat="server">                                               
                                            <div class="TitleSectionRow" style="margin-top:25px;">
                                                <div id="caption6" class="CaptionLabel" runat="server">Personal Identification</div>
                                             </div>
                                             <div class="formRow25">
                                             <div>                                             
                                             <div class="FLabel150"> <asp:Label ID="lblNationality" runat="server" ></asp:Label></div>
                                             <div class="FLabel150" style="margin-left:5px"> <asp:Label ID="lblCitizen" runat="server" ></asp:Label></div>
                                             <div class="FLabel300" style="margin-left:5px"> <asp:Label ID="lblAlienNumber" runat="server" >Alien Number</asp:Label></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span><asp:DropDownList ID="ddlNationality"   runat="server"  Width="150px"></asp:DropDownList> </span>  
                                             <span> <asp:DropDownList ID="ddlCitizen"   runat="server"  Width="150px"> </asp:DropDownList></span>   
                                             <span> <asp:TextBox ID="txtAlienNumber"  runat="server" MaxLength="50"  Width="300px"></asp:TextBox></span>
                                             </div>
                                             </div>
                                             </div>
                                             <div class="formRow5">
                                             <div>                                             
                                             <div class="FLabel300"><asp:Label ID="lblDrivLicStateID" runat="server" ></asp:Label></div>
                                             <div class="FLabel300" style="margin-left:5px"> <asp:Label ID="lblDrivLicNumber" runat="server" ></asp:Label></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span><asp:DropDownList ID="ddlDrivLicStateID"   runat="server" Width="300px" > </asp:DropDownList>   </span> 
                                             <span><asp:TextBox ID="txtDrivLicNumber"   runat="server" MaxLength="50" Width="300px"></asp:TextBox></span>
                                             </div>
                                             </div>
                                             </div>  
                                             </div>  
                                             <div id="InterestedInSection" runat="server">                                          
                                             <div class="TitleSectionRow" style="margin-top:25px;">
                                                <div id="caption7" class="CaptionLabel" runat="server">Interested In</div>
                                            </div>
                                             <div class="formRow25">
                                             <div>
                                             <div class="FLabel350"><asp:Label ID="lblAreaID" runat="server" ></asp:Label></div>
                                             <div class="FLabel350">  <asp:Label ID="lblProgramID" runat="server" ></asp:Label></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span><asp:dropdownlist id="ddlAreaID"   Runat="server" AutoPostBack="true" Width="350px" ></asp:dropdownlist> </span>   
                                             <span> <asp:dropdownlist id="ddlProgramID"  Runat="server" AutoPostBack="true" Width="350px" ></asp:dropdownlist> </span>  
                                             </div>
                                             </div>
                                             </div> 
                                             <div class="formRow5">
                                             <div>
                                             <div class="FLabel350"><asp:Label ID="lblPrgVerId" runat="server" ></asp:Label></div>
                                             <div class="FLabel70"> <asp:Label ID="lblShiftID" runat="server" ></asp:Label></div>
                                             <div class="FLabel150"> <asp:Label ID="lblExpectedStart" runat="server" ></asp:Label></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span> <asp:dropdownlist id="ddlPrgVerId"   Runat="server" Width="350px" ></asp:dropdownlist>  </span> 
                                             <span><asp:DropDownList ID="ddlShiftID"  runat="server" ></asp:DropDownList></span>
                                             <span><telerik:RadDatePicker ID="txtExpectedStart" MinDate="1/1/1945" runat="server"></telerik:RadDatePicker></span>
                                             </div>
                                             </div>
                                             </div> 
                                             </div>   
                                                                                    
                                           <div class="TitleSectionRow" style="margin-top:25px;">
                                                <div id="caption8" class="CaptionLabel" runat="server">Comments</div>
                                            </div>
                                             <div class="formRow25">
                                             <div>
                                             <div class="FLabel135"> <asp:Label ID="lblComments" runat="server" >Comments</asp:Label></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span> <asp:TextBox ID="txtComments"  runat="server"  Columns="175"  TextMode="Multiline" Rows="6"></asp:TextBox> </span>   

                                             </div>
                                             </div>
                                             </div>  
                                             <div class="formRow5">
                                             <div>
                                             <div class="FLabel135"></div>
                                             <div class="FLabel135" style="margin-left:115px;"></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span> </span>   
                                             <span> </span>  
                                             <span>  </span> 
                                             <span></span>
                                             <span></span>
                                             <span>  </span>
                                             <span> </span>
                                             <span></span>
                                             </div>
                                             </div>
                                             </div>  
                                             <div class="formRow5">
                                             <div>
                                             <div class="FLabel135"></div>
                                             <div class="FLabel135" style="margin-left:115px;"></div>
                                             </div>
                                             <div class="formControlRow">
                                             <div>
                                             <span> </span>   
                                             <span> </span>  
                                             <span>  </span> 
                                             <span></span>
                                             <span></span>
                                             <span>  </span>
                                             <span> </span>
                                             <span></span>
                                             </div>
                                             </div>
                                             </div>                                                                                            
                                            
                                            </div>
                                            <!-- End Lead Form -->
                                            
                                            <!-- Old fields -->
                                        <table id="Table2" style="width:800px;" runat="server" visible="false">
                                            <tr>
                                                <td>

                                                            <asp:Label ID="Label5" runat="server" Font-Bold="true" >Comments</asp:Label>
                                                    <br />
                                                            <br />
                                                           
                                                               
                                             <br />
                                                <asp:Label ID="Label3" runat="server" Font-Bold="true"  Visible="false"></asp:Label>
                                   <br />
                                    <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="spacertables">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcellheader" nowrap>
                                                    <asp:Label ID="Label4" runat="server"  Font-Bold="true">School Defined Fields</asp:Label></td>
                                            </tr>
                                            <tr>
                                                <td class="spacertables">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell2">
                                                    <asp:Panel ID="pnlSDF"  runat="server" EnableViewState="false">
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                  <br />
                                                <asp:Label ID="Label6" runat="server" CssClass="tothemecomments" Visible="False">Objectives</asp:Label>
                                            <br />
                                                <asp:TextBox ID="txtResumeObjective"  runat="server" MaxLength="300"
                                                    Width="0px" Columns="60" TextMode="MultiLine" Rows="3" CssClass="hiddenAdv"></asp:TextBox>
                                      
                                                </td>
                                            </tr>
                                        </table>
                                        </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <asp:TextBox ID="txtDate" runat="server" Visible="False" />
                            <asp:TextBox ID="txtPKID" runat="server" Visible="false"></asp:TextBox>
                            <asp:PlaceHolder id="PlaceHolder1" runat="server"></asp:PlaceHolder>
                            <asp:TextBox ID="txtLeadMasterID" runat="server" Visible="False"></asp:TextBox>
                            <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                            <asp:Label ID="lblSourceTypeID1" runat="server"  Visible="False"></asp:Label>
                            <asp:DropDownList ID="ddlSourceTypeId1"  runat="server" Visible="False" >
                            </asp:DropDownList>
                            <asp:TextBox ID="txtLeadId" runat="server" CssClass="hiddenAdv" Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtCreatedDate" runat="server" CssClass="hiddenAdv" Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtModDate" runat="server" CssClass="hiddenAdv" Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtUserId" runat="server" CssClass="hiddenAdv" Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtCampusId" runat="server" CssClass="hiddenAdv" Visible="False"></asp:TextBox>
                            <asp:TextBox ID="txtLeadStatusId" runat="server" CssClass="hiddenAdv" Visible="False"></asp:TextBox>
                            <input type="hidden" name="scrollposition" id="scrollposition"/>
                            <asp:TextBox ID="txtVal" runat="server" CssClass="hiddenAdv" Visible="False" />
                            <input id="__programid" type="hidden" name="__programid" runat="server" />
                            <input id="__prgverid" type="hidden" name="__prgverid" runat="server" />
                            <input id="__areaid" type="hidden" name="__areaid" runat="server" />
                            <input id="__hiddensubmit" type="hidden" name="__hiddensubmit" runat="server" />
                            <input id="__fieldchanges" type="hidden" name="__fieldchanges" runat="server" />
                            <input id="__sourcetypeid" type="hidden" name="__sourcetypeid" runat="server" />
                            <input id="__sourceadvid" type="hidden" name="__sourceadvid" runat="server" />
                            <input id="dup" type="hidden" runat="server" />
			                <!-- start validation panel-->
			                <asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			                <asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				                ErrorMessage="CustomValidator"></asp:customvalidator>
			                <asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				                ShowMessageBox="True"></asp:validationsummary>
			                <!--end validation panel-->
                            </td>
                        </tr>
                      </table>
                          <telerik:RadNotification runat="server" ID="RadNotification1" 
                Text="This is a test" ShowCloseButton="true" 
                Width="400px" Height="125px"
                TitleIcon="" 
                Position="Center" Title="Message" 
                EnableRoundedCorners="true" 
                EnableShadow="true" 
                Animation="Fade" 
                AnimationDuration="1000" 
                   
                style="padding-left:120px; padding-top:5px; word-spacing:2pt;"> 
    </telerik:RadNotification> 	
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

