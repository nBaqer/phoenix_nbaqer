﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AleadPrint.aspx.vb" Inherits="AdvWeb.AD.AdAleadPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print Report for Lead Profile</title>

    <style type="text/css">
        #printInfoPage input {
            margin-left: 5px;
        }

        #printPhoto {
            width: 2cm;
            display: table;
            float: left;
            position: relative;
        }

        #printImage {
            width: 2cm;
        }

        #printHeaderRight {
            font-family: Arial, sans-serif;
            font-weight: bold;
            font-size: 10pt;
            width: 14cm;
            display: table;
            padding-top: 10px;
            float: left;
            position: relative;
            overflow: hidden;
            text-align: center;
        }

            #printHeaderRight > div {
                margin-top: 5px;
            }

        .clearfix {
            clear: both;
        }


        .hrcol1 {
            font-family: arial, sans-serif;
            font-size: 9pt;
            font-weight: bold;
            display: block;
            position: relative;
            width: 3cm;
            text-align: left;
            float: left;
            overflow: hidden;
        }

        .hrcol2 {
            font-family: arial, sans-serif;
            font-size: 9pt;
            font-weight: normal;
            display: block;
            min-width: 6cm;
            width: 6cm;
            text-align: left;
            position: relative;
            float: left;
            overflow: hidden;
            border: 0;
        }

        .hrcol3 {
            font-family: arial, sans-serif;
            font-size: 9pt;
            font-weight: bold;
            width: 3cm;
            text-align: left;
            float: left;
            margin-left: 0.3cm;
        }

        .hrcol4 {
            font-family: arial, sans-serif;
            font-size: 9pt;
            font-weight: normal;
            width: 4cm;
            text-align: left;
            float: left;
            border: 0;
        }

        #corpusReport input {
            font-family: arial, sans-serif;
            font-size: 9pt;
            font-weight: normal;
            text-align: left;
            float: left;
            height: 0.4cm;
            border: 0;
            /*padding-top: 0.05cm;*/
 
        }

        #corpusReport label {
            background-color: gainsboro !important;
            font-family: arial, sans-serif;
            font-size: 9pt;
            font-weight: normal;
            text-align: left;
            float: left;
            height: 0.4cm;
            border: 1px solid gainsboro;
            display: block;
            /*padding-top: 0.05cm;*/
 
        }
        /* Sections DIV style in corpus report*/
        .corpusTitle {
            width: 100%;
            font-family: arial, sans-serif;
            font-size: 9pt;
            font-weight: bold;
            /*padding-left: 5px;*/
            border: 1px solid gainsboro;
            border-right: 0;
            border-left: 0;
            height: 0.5cm;
            padding-top: 0.1cm;
            display: block;
        }
        
        /* Rows wrapper style in corpus report*/
        .corpusRow {
            border: 1px solid gainsboro;
            border-right: 0;
            border-left: 0;
            page-break-inside: avoid;
       }

        

        /* corpus labels styles ---------------------------*/
        .corpus1 {
           min-width: 3.25cm;
            width: 3.25cm;
        }

        .corpus1x {
            min-width: 4cm;
            width: 4cm;
        }

          .corpus6x {
            min-width: 3.25cm;
            width: 3.25cm;
              height: 1cm;
              min-height: 1cm;
        }

                .corpus3 {
           min-width: 1.75cm;
            width: 1.75cm;
       }

        .corpus3x {
           min-width: 2.5cm;
            width: 2.5cm;
        }

        /* corpus INPUT styles*/
        .corpus1Text {
            min-width: 3cm;
            width: 3cm;
       }

        .corpus2 {
            min-width: 3.25cm;
            width: 3.25cm;
            display: block;
        }

        .corpus2m {
            display: block;
            min-width: 2.50cm;
            width: 2.50cm;
        }

        .corpus3xText {
            min-width: 2.15cm;
            width: 2.15cm;
      }

        .corpus4 {
            min-width: 7.5cm;
        }

        .corpus4m {
            min-width: 7cm;
        }

        .corpuslarge {
            min-width: 13.5cm;
            border: 0;
            overflow: auto;
        }

        .corpuslargeM {
            min-width: 13cm;
        }

        .corpuswide {
            width: 17cm;
        }

        .corpus23 {
            min-width: 8.40cm;
        }

        .corpusCustom {
           min-width: 4cm;
        }
    </style>

    <script src="../Kendo/js/jquery.min.js"></script>
    <script src="../Kendo/js/kendo.all.min.js"></script>
    <script src="../Scripts/Advantage.Client.AD.js"></script>


</head>
<body>
    <form id="form1" runat="server">
        <article id="printInfoPage" style="width: 17.5cm;">
            <div id="printWrapper">
                <div id="printPhoto">
                    <img id="printImage" src="#" />
                </div>
                <div id="printHeaderRight">
                    <div id="printCampus">Campus</div>
                    <div>LEAD PROFILE</div>
                    <div id="printDateTime">--/--/--</div>
                </div>
                <div class="clearfix"></div>
                <br />
                <label class="hrcol1">Lead:</label>
                <input class="hrcol2" id="hr1text2" readonly="readonly" />
                <label class="hrcol3">Admissions Rep:</label>
                <input class="hrcol4" id="hr1text4" readonly="readonly" />
                <div class="clearfix"></div>
                <label class="hrcol1">Status:</label>
                <input class="hrcol2" id="hr2text2" readonly="readonly" />
                <label class="hrcol3">Date Assigned</label>
                <input class="hrcol4" id="hr2text4" readonly="readonly" />
                <div class="clearfix"></div>
                <label class="hrcol1">Program Version:</label>
                <input class="hrcol2" id="hr3text2" readonly="readonly" />
                <label class="hrcol3">Requirements Met:</label>
                <input class="hrcol4" id="hr3text4" readonly="readonly" />
                <div class="clearfix"></div>
                <label class="hrcol1">Expected Start:</label>
                <input class="hrcol2" id="hr4text2" readonly="readonly" />
                <div class="clearfix"></div>
                <br />

                <!-- Corpus report -->
                <div id="corpusReport" style="width: 100%; border: 1px solid gainsboro">
                    <!-- Contact Info -->
                    <div class="corpusTitle">&nbsp;Contact Info</div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Preferred Contact</label>
                        <input class="corpus2" id="valPreferContact" readonly="readonly" />
                        <label class="corpus3">&nbsp;Best Time</label>
                        <input class="corpus4" id="valPhoneBest" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Phone Best</label>
                        <input class="corpus2" id="valPhone" readonly="readonly" />
                        <label class="corpus3">&nbsp;Phone</label>
                        <input class="corpus2" id="valPhone2" readonly="readonly" />
                        <label class="corpus3">&nbsp;Phone</label>
                        <input class="corpus2" id="valPhone3" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Email-Best</label>
                        <input class="corpuslarge" id="valEmailBest" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Email</label>
                        <input class="corpuslarge" id="valEmail" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Address</label>
                        <input class="corpus23" id="valAddress" readonly="readonly" />
                        <label class="corpus3">&nbsp;County</label>
                        <input class="corpus2" id="valCounty" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;City, State, Zip</label>
                        <input class="corpus23" id="valCityStateZip" readonly="readonly" />
                        <label class="corpus3">&nbsp;Country</label>
                        <input class="corpus2" id="valCountry" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <!--  Interest -->
                    <div class="corpusTitle">&nbsp;Interest</div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Area</label>
                        <input class="corpuslarge" id="valArea" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Program</label>
                        <input class="corpuslarge" id="valProgram" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Attend</label>
                        <input class="corpus2" id="valAttend" readonly="readonly" />
                        <label class="corpus3">&nbsp;Schedule</label>
                        <input class="corpus2" id="valSchedule" readonly="readonly" />
                        <%--<label class="corpus3x">&nbsp;Grad Date</label>
                        <input class="corpus2m" id="valExpectedGrad" readonly="readonly" />--%>
                        <div class="clearfix"></div>
                    </div>
                    <!--  Demographics -->
                    <div class="corpusTitle">&nbsp;Demographics</div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;SSN</label>
                        <input class="corpuslarge" id="valSsn" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;DOB-Age</label>
                        <input class="corpus2" id="valDobAge" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Gender</label>
                        <input class="corpus3xText" id="valGender" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Race</label>
                        <input class="corpus1Text" id="valRace" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Citizenship</label>
                        <input class="corpus2" id="valCitizenship" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Dependency</label>
                        <input class="corpus3xText" id="valDependency" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Dependants</label>
                        <input class="corpus1Text" id="valDependents" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Alien Number</label>
                        <input class="corpus2" id="valAlienNumber" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Marital Status</label>
                        <input class="corpus3xText" id="valMaritalStatus" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Family Income</label>
                        <input class="corpus1Text" id="valFamilyIncoming" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Housing</label>
                        <input class="corpus2" id="valHousing" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Driver Lic. St.</label>
                        <input class="corpus3xText" id="valDriverLic" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Driver Lic. No.</label>
                        <input class="corpus1Text" id="valDriverNo" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Transportation</label>
                        <input class="corpus2" id="valTransportation" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Dist. To School</label>
                        <input class="corpus3xText" id="valDistToSchool" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Disabled</label>
                        <input class="corpus1Text" id="valDisabled" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <!-- Source -->
                    <div class="corpusTitle">&nbsp;Source</div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Category</label>
                        <input class="corpus2" id="valCategory" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Advertisement</label>
                        <input class="corpus4" id="valAdvertisement" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Type</label>
                        <input class="corpus2" id="valType" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Date/time</label>
                        <input class="corpus4" id="valCreatedDate" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>

                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Note</label>
                        <input class="corpuslarge" id="valNote" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <!-- Other -->
                    <div class="corpusTitle">&nbsp;Other</div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Date Applied</label>
                        <input class="corpus2" id="valDateApplied" readonly="readonly" />
                        <label class="corpus1">&nbsp;Previous Education</label>
                        <input class="corpus4m" id="valPreviousEducation" readonly="readonly" />

                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Sponsor</label>
                        <input class="corpus2" id="valSponsor" readonly="readonly" />
                        <label class="corpus1">&nbsp;High School</label>
                        <input class="corpus4m" id="valHs" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Admin Criteria</label>
                        <input class="corpus2" id="valAdminCriteria" readonly="readonly" />
                        <label class="corpus1">&nbsp;HS Grade Date</label>
                        <input class="corpus3xText" id="valHsGradeDate" readonly="readonly" />
                        <label class="corpus3x">&nbsp;Attending HS</label>
                        <input class="corpus3xText" id="valHsAttending" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus6x" >&nbsp;Comments</label>
                        <textarea class="corpuslarge" rows="2" id="valComments" readonly="readonly"></textarea>
                        <div class="clearfix"></div>
                    </div>
                    <div class="corpusRow">
                        <label class="corpus1">&nbsp;Reason not Enrolled</label>
                        <input class="corpuslarge" id="valReasonNotEnrolled" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <!-- Lead Groups  -->
                    <div class="corpusTitle">&nbsp;Lead Groups</div>
                    <div class="corpusRow">
                        <input class="corpuswide" id="valGroups" readonly="readonly" />
                        <div class="clearfix"></div>
                    </div>
                    <!-- Custom, this should be dynamic Generated  -->
                    <div class="corpusTitle">&nbsp;Custom</div>
                    <div id="customPlaceHolder">
                    </div>
                </div>


            </div>
        </article>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
    <%-- ReSharper disable once UnusedLocals --%>
            var manager = new AD.LeadPrint();
        });
    </script>
</body>
</html>
