﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="AleadEnrollment.aspx.vb" Inherits="AdvWeb.AD.AdAleadEnrollment" %>
<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <%--<style type="text/css">
      
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" Runat="Server">
       <!-- Use this to put content -->
    <link href="../css/AD/LeadEnrollPage.css" rel="stylesheet" />
    <div style="/*overflow: auto; position: fixed;*/ max-height: 1080px; width: 100%; top:215px;">
        <div id="dvEnrollment" data-role="validator"  class="boxContainer">
            <br />
            <div id="dvBlocker" class="updateBlocker" style="display: none;"></div>
            <div>
                <h3><label class="label-bold-title">Enroll Lead</label></h3>
            </div>

            <br />
            <div id="dvLeft" style="float: left; width: 40%">
                <div class="descriptionLabel">
                    Student Information
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblEnrollmentId">Enrollment Id</label>
                    </div>
                    <div class="enrollControl">
                        <input id="txtEnrollmentId" disabled="disabled" class="ctrlWidth k-textbox" name="EnrollmentId" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblLeadName">Lead Name</label>
                    </div>
                    <div class="enrollControl">
                        <input id="txtLeadName" disabled="disabled" class="ctrlWidth k-textbox" name="LeadName" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblGender">Gender</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlGender" name="Gender" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblDob">DOB</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="txtDob" data-validation="date" name="Dob" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblSsn">SSN</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="txtSsn" class=" k-textbox" data-validation="ssn" 
                            data-role="maskedtextbox" data-mask="000-00-0000" name="Ssn" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblDependencyType">Dependency Type</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlDependencyType" name="DependencyType" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblMaritalStatus">Marital Status</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlMaritalStatus" name="MaritalStatus" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblFamilyIncome">Family Income</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlFamilyIncome" name="FamilyIncome" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblHousingType">Housing Type</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlHousingType" name="HousingType" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblEducationLevel">Education Level</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlEducationLevel" name="EducationLevel" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblAdminCriteria">Admin Criteria</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlAdminCriteria" name="AdminCriteria" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblDegree">Degree/Certificate Seeking Type</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlDegree" name="Degree" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblAttendanceType">Attendance Type</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlAttendanceType" name="AttendanceType" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblRace">Race</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlRace" name="Race" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblNationality">Nationality</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlNationality" name="Nationality" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblCitizenship">Citizenship</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlCitizenship" name="Citizenship" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblGeographicType">GeographicType</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlGeographicType" name="GeographicType" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblState">State of Residence</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlState" name="State" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblDisabled">Disabled</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlDisabled" name="Disabled" />
                    </div>
                </div>
                <div class="clearfix ">
                    <div class="enrollLabel">
                        <label id="lblFirstTime">First Time</label>
                    </div>

                    <div class="enrollControl">
                        <label>
                            <input id="chkSchoolfirst" type="checkbox" name="Schoolfirst" />At this school</label>
                    </div>
                </div>
                
                <div class="clearfix ">
                    <div class="enrollLabel">
                        <label>&nbsp;</label>
                    </div>
                    <div class="enrollControl">
                        <label>
                            <input id="chkSecondaryschool" type="checkbox" name="SecondarySchool" />At any post-secondary school</label>
                    </div>
                </div>
                 <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblThirdPartyContract">Attending through a contract with a third party</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlThirdPartyContract" name="ThirdPartyContract" />
                    </div>
                </div>
            </div>
            <div id="dvRight" style="float: left; width: 40%">
                <div class="descriptionLabel">
                    Program Information
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblCampus">Campus</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlCampus" name="Campus" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblProgVersion">Program Version</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlProgVersion" name="ProgVersion" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblProgVerType">Program Version Type</label>
                    </div>

                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlProgVerType" name="ProgVerType" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblSchedule">Schedule</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlSchedule" name="Schedule" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblShift">Shift</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlShift" name="Shift" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblEnrollmentDate">Enrollment Date</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="txtEnrollmentDate" name="EnrollmentDate" data-validation="date" />
                    </div>
                </div>
                <div class="clearfix bottomSpace hidden" id="dvExpectedStartDate">
                    <div class="enrollLabel">
                        <label id="lblExpStartDate">Expected Start Date</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="txtExpStartDate" maxlength ="12" name="ExpStartDate" data-validation="date" />
                    </div>
                </div>
                <div class="clearfix bottomSpace" id="dvExpectedStartDateDrop">
                    <div class="enrollLabel">
                        <label id="lblExpStartDateDrop">Expected Start Date<span style=color: #b71c1c>*</span></label>
                    </div>
                    <div class="enrollControl">
                        <input id="ddlExpectedStart" class="ctrlWidth" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblStartDate">Start Date</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="txtStartDate" data-validation="date" name="StartDate" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblMidPointDate">Mid-Point Date</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="txtMidPointDate" data-validation="date" name="MidPointDate" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblContractedGradDate">Contracted Graduation Date</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="txtContractedGradDate" data-validation="date" name="ContractedGradDate" />
                    </div>
                    <button id="btnCalcGradDate" type="button" class="k-button hidden" style="float: left;position: relative;margin-top: 5px;margin-bottom: 5px;">Calculate Grad. Date</button>&nbsp;

                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblTransferDate">Transfer Date</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="txtTransferDate" data-validation="date" name="TransferDate" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblEntranceInterview">Entrance Interview</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="txtEntranceInterviewDt" data-validation="date" name="EntranceInterview" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblChargingMethod">Charging Method</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlChargingMethod" name="ChargingMethod" />
                    </div>
                </div>
                <div class="clearfix bottomSpace" id="disableAutoChargeContainer" style="display: none;">
                    <div class="enrollLabel">
                        <label id="lblDisableAutoCharge">Disable Auto Charge</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlDisableAutoCharge" name="DisableAutoCharge" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblTuitionCategory">Tuition Category</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlTuitionCategory" name="TuitionCategory" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblFinAidAdvisor">Financial Aid Advisor</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlFinAidAdvisor" name="FinAidAdvisor" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblAcademicAdvisor">Academic Advisor</label>
                    </div>
                    <div class="enrollControl">
                        <input class="ctrlWidth" id="ddlAcademicAdvisor" name="AcademicAdvisor" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblBadgeNumber">Badge Number</label>
                    </div>
                    <div class="enrollControl">
                        <input id="txtBadgeNumber" maxlength="9" class="ctrlWidth k-textbox" name="BadgeNumber" />
                    </div>
                    <button id="btnNewBadgeNumber" type="button" class="k-button" style="position: relative;margin-top: 5px;margin-bottom: 5px;">Generate Badge ID</button>&nbsp;
                   
                </div>
                <div class="clearfix bottomSpace"></div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <label id="lblTransferHrs">Transfer Hours</label>
                    </div>
                    <div class="enrollControl">
                        <input id="txtTransferHrs" class="ctrlWidth k-textbox" data-validation="decimal" name="TransferHrs" />
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">
                        <%--<div class="enrollLabel">&nbsp;</div>--%>
                    </div>

                    <div class="enrollLabel">
                        <%--<div >&nbsp;</div>--%>
                    </div>
                </div>
                <div class="clearfix bottomSpace">
                    <div class="enrollLabel">&nbsp;</div>
                    <div class="enrollControl">
                        <button id="btnEnrollLead" type="button" class="blue-primary k-button">Enroll Lead</button>&nbsp;
                <button id="btnCancelEnroll" type="button" class="k-button">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
      <script type="text/javascript">
       
        $(document).ready(function () {
           var d = new AD.LeadEnroll();
        });

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>