﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="AleadSkills.aspx.vb" Inherits="AdvWeb.AD.AdAleadSkills" %>

<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" runat="Server">
    <!-- Use this to put content -->
    <div id="containerSkill">
        <div id="extraFirstPanel" class="leadFirstPanel">
            <label class="leadTitles" style="font-weight: bold !important; vertical-align: top; font-size: 11pt !important;">Skills</label>
            <a id="extraPlusImage" href="javascript:void(0);">
                <span class="k-icon k-i-plus-circle font-green"></span>
            </a>
        </div>
        <link href="../css/AD/LeadExtra.css" rel="stylesheet" />
        <div id="extraWrapper">
            <div id="extraMainPanel" style="margin: 10px">
                <div id="extraGrid"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var leadElement = document.getElementById('leadId');
            var leadId = leadElement.value;
     <%-- ReSharper disable once UnusedLocals --%>
            var manager = new AD.LeadSkills(leadId);
        });
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

