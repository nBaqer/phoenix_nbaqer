﻿Imports FAME.AdvantageV1.BusinessFacade

Partial Class AD_VoidTransaction
    Inherits System.Web.UI.Page
    Protected Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Dim strTransactionId As String = Request.QueryString("transactionid").ToString
        Dim facadeObj As New LeadMasterObject
        facadeObj.MakeATransactionVoid(strTransactionId)
    End Sub
End Class
