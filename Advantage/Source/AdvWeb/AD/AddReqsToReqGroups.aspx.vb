
Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports Advantage.Business.Objects
Imports System.Collections

Partial Class AddReqsToReqGroups
    Inherits Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblShow As Label
    'Protected WithEvents radStatus As RadioButtonList
    Protected WithEvents pnlTerm As Panel
    Protected WithEvents lnkReqGrpIdDef As LinkButton
    Protected WithEvents pnlRHS As Panel
    Protected WithEvents Panel1 As Panel
    Protected WithEvents Customvalidator1 As CustomValidator
    Protected WithEvents pnlRequiredFieldValidators As Panel
    Protected WithEvents Validationsummary1 As ValidationSummary
    Protected WithEvents lblHeading As Label


    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Dim Facade As New AdReqsFacade
    Dim boolMandatoryGroup As Boolean
    Protected resourceId As String
    Protected campusid As String
    Private pObj As New UserPagePermissionInfo
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim advantageUserState As User  = AdvantageSession.UserState
        'campusid = XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        campusid = AdvantageSession.UserState.CampusId.ToString
        resourceId = HttpContext.Current.Request.Params("resid")
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusid)
        InitButtonsForLoad()
        If Not Page.IsPostBack Then
            txtReqGrpId.Text = Request.QueryString("ReqGrpId")
            txtReqGrpDescrip.Text = Request.QueryString("ReqGrpDescrip")
            boolMandatoryGroup = Request.QueryString("IsMandatoryReqGrp")

            With ddlNumReqs
                .Items.Insert(0, New ListItem("Select", 0))
            End With
            If boolMandatoryGroup = True Then
                dgrdTransactionSearch.Visible = True
                'Label1.Text = "Add requirements to " & txtReqGrpDescrip.Text & " requirement group  and selected lead group"
                dgrdTransactionSearch.DataSource = Facade.GetsMandatoryRequirementsByLeadGrp(Date.Now.ToShortDateString)
                dgrdTransactionSearch.DataBind()
                checkAllDataGrid()
                dgrdTransactionSearch.Enabled = False
            
                btnNew.Enabled = False
                btnSave.Enabled = False
                btnDelete.Enabled = False

                pnlNumReqs.Visible = False
                pnlMandatory.Visible = True

                lblLeadGrpId.Visible = False
                ddlLeadGrpId.Visible = False

                ddlNumReqs.Visible = False
                lblNumReqs.Visible = False

                ddlStatus.Visible = False
                lblStatus.Visible = False

                pnlGrid.Visible = False

                Exit Sub
            End If
            pnlMandatory.Visible = False
            pnlGrid.Visible = True
            chkIsInDB.Checked = False
            BuildLeadGroups()
            dgrdTransactionSearch.Enabled = False
            BindDataList()
            BuildStatusDDL()
        End If
    End Sub
    Private Sub BuildLeadGroups()
        With ddlLeadGrpId
            .DataTextField = "Descrip"
            .DataValueField = "LeadGrpId"
            .DataSource = Facade.GetAllLeadGroups(campusid)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStatus
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With
    End Sub
    Private Sub ddlLeadGrpId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlLeadGrpId.SelectedIndexChanged
        dgrdTransactionSearch.Enabled = True
        Dim dtCurrentDate As Date = Date.Now.ToShortDateString
        Dim ds As  DataSet

        If Not ddlLeadGrpId.SelectedValue = "" Then
            ds = Facade.GetsReqsByLeadGrp(ddlLeadGrpId.SelectedValue, txtReqGrpId.Text, dtCurrentDate, campusid)
            dgrdTransactionSearch.Visible = True
            dgrdTransactionSearch.DataSource = ds
            dgrdTransactionSearch.DataBind()
            ddlNumReqs.Items.Clear()

            Dim intRowCount As Integer = ds.Tables(0).Rows.Count
            Dim z As Integer
            With ddlNumReqs
                .Items.Insert(0, New ListItem("Select", 0))
            End With
            If intRowCount >= 1 Then
                With ddlNumReqs

                    For z = 1 To intRowCount
                        .Items.Add(New ListItem(z, z))
                        If z = intRowCount Then
                            Exit For
                        End If
                    Next
                End With
            End If
        End If

    End Sub
    Public Sub checkAllDataGrid()
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer

        ' Save the datagrid items in a collection.
        iitems = dgrdTransactionSearch.Items
        Try
            'Loop thru the collection to get the Array Size
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True
            Next
        Finally
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        '        Dim strLeadGrpId As String
        '   Dim intRequired As Integer
        Dim intArrCount As Integer
        Dim intResult As String
        Dim intRequiredCount as Integer 
        Dim z As Integer
        '  Dim intReqCount As Integer
        Dim strMessage As String = String.Empty

        If ddlLeadGrpId.SelectedValue = "" Then
            strMessage = "Lead group is required" & vbLf
        End If

        If ddlNumReqs.SelectedValue = "0" Then
            strMessage &= "Minimum number of requirement is required" & vbLf
        End If

        If Not strMessage = "" Then
            DisplayErrorMessage(strMessage)
            Exit Sub
        End If

        Dim minNumReqs As Integer = CInt(ddlNumReqs.SelectedValue)

        ' Save the datagrid items in a collection.
        iitems = dgrdTransactionSearch.Items
        Try
            'Loop thru the collection to get the Array Size
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If CType(iitem.FindControl("chkRequired"), CheckBox).Checked = True Then
                    CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True
                End If
                If (CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True) Then
                    intArrCount += 1
                End If
                 If (CType(iitem.FindControl("chkRequired"), CheckBox).Checked = True) Then
                    intRequiredCount += 1
                End If
            Next

            If minNumReqs > intArrCount Then
                radErrorNotification.Show("The minimum number of requirements expected should be either equal to or greater than the number of requirements marked as required")
                'DisplayErrorMessage("The selected requirements should be either equal to or greater than the minimum number of requirements expected")
                Exit Try
            End If

             If minNumReqs < intRequiredCount Then
                radErrorNotification.Show("The minimum number of requirements expected should be either equal to or greater than the number of requirements marked as required")
                'DisplayErrorMessage("The minimum number of requirements expected should be either equal to or greater than the number of requirements marked as required")
                Exit Try
            End If

            If intArrCount >= 1 Then
                Dim selectedRequirement() As String = Array.CreateInstance(GetType(String), intArrCount)
                Dim selectedMandatory() As Boolean = Array.CreateInstance(GetType(Boolean), intArrCount)

                'Loop Through The Collection To Get ClassSection
                For i = 0 To iitems.Count - 1
                    iitem = iitems.Item(i)
                    'retrieve clssection id from the datagrid
                    If CType(iitem.FindControl("chkRequired"), CheckBox).Checked = True Then
                        CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True
                    End If
                    If (CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True) Then
                        selectedRequirement.SetValue(CType(iitem.FindControl("lbladReqId"), Label).Text, z)
                        selectedMandatory.SetValue(CType(iitem.FindControl("chkRequired"), CheckBox).Checked, z)
                        z += 1
                    End If
                Next
                If chkIsInDB.Checked = False Then
                    intResult = Facade.InsertReqGrpDef(txtReqGrpId.Text, minNumReqs, ddlLeadGrpId.SelectedValue, selectedRequirement, Session("UserName"), selectedMandatory, ddlStatus.SelectedValue)
                Else
                    intResult = Facade.UpdateReqGrpDef(txtReqGrpId.Text, minNumReqs, ddlLeadGrpId.SelectedValue, selectedRequirement, Session("UserName"), selectedMandatory, ddlStatus.SelectedValue)
                End If
                If Not intResult = "" Then
                    DisplayErrorMessage(intResult)
                    Exit Sub
                End If
                chkIsInDB.Checked = True
            End If
            BindDataList()
        Finally
        End Try
        '  CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, ddlLeadGrpId.SelectedValue, ViewState)
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, ddlLeadGrpId.SelectedValue)
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Public Sub SetUpEntranceTest()
    End Sub
    Private Sub BindDataList()
        Dim status As String = String.Empty

        If (radStatus.SelectedIndex = 0) Then
            status = "Active"
        ElseIf (radStatus.SelectedIndex = 1) Then
            status = "Inactive"
        End If

        '   bind dlstAdmissionReqGroups datalist
        dlstAdmissionReqGroups.DataSource = Facade.GetRequirementGroupByLeadGroups(txtReqGrpId.Text, status)
        dlstAdmissionReqGroups.DataBind()
    End Sub
    Private Sub BindDataGrid()
        Dim status As String = String.Empty

        If (radStatus.SelectedIndex = 0) Then
            status = "Active"
        ElseIf (radStatus.SelectedIndex = 1) Then
            status = "Inactive"
        End If

        Dim dsLeadgroups As New LeadFacade
        Datagrid1.DataSource = dsLeadgroups.GetReqsByRequirementgroup(txtReqGrpId.Text, status)
        Datagrid1.DataBind()

    End Sub

    Private Sub dlstAdmissionReqGroups_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstAdmissionReqGroups.ItemCommand
        Dim dtCurrentDate As Date = Date.Now.ToShortDateString
        dgrdTransactionSearch.Enabled = True

        Dim objValue As AdReqGrpInfo


        'btnNew.Enabled = True
        'btnSave.Enabled = True
        'btnDelete.Enabled = True

        InitButtonsForLoad()

        pnlNumReqs.Visible = True
        pnlMandatory.Visible = False

        lblLeadGrpId.Visible = True
        ddlLeadGrpId.Visible = True

        ddlNumReqs.Visible = True
        lblNumReqs.Visible = True

        ddlStatus.Visible = True
        lblStatus.Visible = True

        pnlGrid.Visible = True

        dgrdTransactionSearch.Visible = True
        dgrdTransactionSearch.Enabled = True

        Dim ds As DataSet
        ds = Facade.GetsReqsByLeadGrp(e.CommandArgument, txtReqGrpId.Text, dtCurrentDate, campusid)

        ddlNumReqs.Items.Clear()
        With ddlNumReqs
            .Items.Insert(0, New ListItem("Select", 0))
        End With

        Dim intRowCount As Integer = ds.Tables(0).Rows.Count
        Dim z As Integer
        If intRowCount >= 1 Then
            With ddlNumReqs
                '.Items.Insert(0, New ListItem("Select", 0))
                For z = 1 To intRowCount
                    .Items.Add(New ListItem(z, z))
                    If z = intRowCount Then
                        Exit For
                    End If
                Next
            End With
        End If

        Datagrid1.Visible = False

        objValue = Facade.GetReqDefByLeadGrp(txtReqGrpId.Text, CType(e.CommandArgument, String))

        CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, CType(e.CommandArgument, String), ViewState)

        With objValue
            Try
                ddlLeadGrpId.SelectedValue = .LeadgrpId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlLeadGrpId.SelectedIndex = 0
            End Try
            Try
                ddlNumReqs.SelectedValue = .NumReqs.ToString
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlNumReqs.SelectedIndex = 0
            End Try
            Try
                ddlStatus.SelectedValue = .StatusId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlStatus.SelectedIndex = 0
            End Try
        End With
        dgrdTransactionSearch.Visible = True
        chkIsInDB.Checked = True

        dgrdTransactionSearch.DataSource = Facade.GetsReqsByLeadGrp(e.CommandArgument, txtReqGrpId.Text, dtCurrentDate, campusid)
        dgrdTransactionSearch.DataBind()

        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, CType(e.CommandArgument, String))
    End Sub
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        BindNewData()
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
    End Sub
    Private Sub BindNewData()
        ddlLeadGrpId.SelectedIndex = 0
        ddlNumReqs.SelectedIndex = 0
        ddlNumReqs.Items.Clear()
        With ddlNumReqs
            .Items.Insert(0, New ListItem("Select", 0))
        End With
       
        dgrdTransactionSearch.Visible = False
        chkIsInDB.Checked = False
    End Sub
    Private Sub lnkAllReqs_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkAllReqs.Click

        pnlAllReqs.Visible = True

        btnNew.Enabled = False
        btnSave.Enabled = False
        btnDelete.Enabled = False

        pnlNumReqs.Visible = False
        pnlMandatory.Visible = False

        lblLeadGrpId.Visible = False
        ddlLeadGrpId.Visible = False

        ddlNumReqs.Visible = False
        lblNumReqs.Visible = False

        ddlStatus.Visible = False
        lblStatus.Visible = False

        pnlGrid.Visible = False
        dgrdTransactionSearch.Visible = False

        Datagrid1.Visible = True

        BindDataGrid()
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        Dim fac As New LeadFacade
        fac.DeleteReqGrpDef(ddlLeadGrpId.SelectedValue, txtReqGrpId.Text)
        BindNewData()
        BindDataList()
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
    End Sub
    Public Function converttoyesno(ByVal boolValue As Boolean) As String
        If boolValue = True Then
            Return "Yes"
        Else
            Return "No"
        End If
    End Function
    Public Function ChangeToBoolean(ByVal boolValue As String) As Boolean
        If boolMandatoryGroup = True Then
            Return True
        End If
        If boolValue.ToLower = "yes" Or boolValue.ToLower = "true" Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes '
        ' CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(CType(ctrl, Panel))
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(CType(ctrl, DataGrid))
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged

        ddlLeadGrpId.SelectedIndex = 0
        ddlNumReqs.SelectedIndex = 0
        If radStatus.SelectedIndex = 1 Then
            ddlStatus.SelectedIndex = 1
        Else
            ddlStatus.SelectedIndex = 0
        End If
        chkIsInDB.Checked = False
        '   Bind datalist
        BindDataList()
        InitButtonsForLoad()
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
        dgrdTransactionSearch.Visible = False
        dgrdTransactionSearch.Enabled = False

        pnlNumReqs.Visible = True
        pnlMandatory.Visible = True

        lblLeadGrpId.Visible = True
        ddlLeadGrpId.Visible = True

        ddlNumReqs.Visible = True
        lblNumReqs.Visible = True

        ddlStatus.Visible = True
        lblStatus.Visible = True

        pnlGrid.Visible = True

        Datagrid1.Visible = False

    End Sub

End Class
