<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Titles.aspx.vb" Inherits="Titles" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    </telerik:RadAjaxManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstTitles" runat="server" DataKeyField="TitleId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# container.dataitem("TitleId")%>' Text='<%# container.dataitem("TitleDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>

            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>

                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                        <tr>
                                            <td class="contentcell" style="width: 30%">
                                                <asp:Label ID="lblTitleCode" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtTitleCode" runat="server" CssClass="textbox"></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblTitleDescrip" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtTitleDescrip" runat="server" CssClass="textbox" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblJobCatId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlJobCatId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:TextBox ID="txtTitleId" runat="server" CssClass="label" Visible="false"></asp:TextBox></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell"></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                    </table>
                                    <!--end table content-->
                                    <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="label" Width="10%"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="label" Width="10%"></asp:TextBox>
                                </div>
                            </asp:Panel>
                            <!--end table content-->

                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" ErrorMessage="CustomValidator"
            Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True"
            ShowSummary="False"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>

</asp:Content>


