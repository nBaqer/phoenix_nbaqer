﻿Imports Advantage.Business.Objects
Namespace AdvWeb.AD
    Partial Class ADAleadQuick
        Inherits BasePage

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            'Get LeadId
            Dim objStudentState As StudentMRU = GetObjStudentState()
            If objStudentState Is Nothing Then
                RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
                Exit Sub
            End If

            Master.Master.PageObjectId = objStudentState.LeadId.ToString()
            Master.Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
            Master.Master.SetHiddenControlForAudit()
        End Sub

    End Class
End Namespace
