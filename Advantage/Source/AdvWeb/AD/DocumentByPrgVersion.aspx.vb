
Imports FAME.AdvantageV1.BusinessFacade

Partial Class DocumentByPrgVersion
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
    Protected getDocs As New LeadFacade
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim documentList As New LeadDocsFacade
        Dim docsFacade As New LeadEntranceFacade

        Dim strLeadId As String = Request.QueryString("LeadId")
        Dim strPrgVerId As String = Request.QueryString("PrgVerId")
        Dim strLeadName As String = Request.QueryString("LeadName")
        Dim strCampusId As String = Request.QueryString("CampusId")

        lblHeading.Text = strLeadName

        If strPrgVerId <> "" Then
            dgrdTransactionSearch.DataSource = docsFacade.GetAllStandardDocumentsByEffectiveDatesAndPrgVersionByStatus(strLeadId, strPrgVerId, strCampusId)
            dgrdTransactionSearch.DataBind()
        Else
            dgrdTransactionSearch.DataSource = docsFacade.GetAllStandardDocumentsByEffectiveDatesByStatus(strLeadId, strCampusId)
            dgrdTransactionSearch.DataBind()
        End If
    End Sub
    Public Function GetBool(ByVal intVal As Integer) As String
        If intVal = 1 Then
            Return "Yes"
        Else
            Return "No"
        End If
    End Function
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal ctrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In ctrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub
End Class
