﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="LeadManager.aspx.vb" Inherits="LeadManager" %>
<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" type="text/javascript">
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
	<asp:validationsummary id="Validationsummary1" runat="server"></asp:validationsummary>
    <telerik:RadSplitter ID="ContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="ContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
        <div style="padding:15px;">
            <telerik:RadTabStrip ID="LeadManagementTabs" runat="server" Width="100%" SelectedIndex="0">
                <Tabs>
                    <telerik:RadTab runat="server" Text="Summary" Value="Summary"></telerik:RadTab>
                    <telerik:RadTab runat="server" Text="Personal" Value="Personal">
                        <Tabs>
                            <telerik:RadTab runat="server" Text="Info" Value="Info" Selected="true">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Address" Value="Address">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Phone" Value="Phone">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Email" Value="Email">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Demographics" Value="Demographics">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Comments" Value="Comments">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="School Defined Fields" Value="UserDefined">
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTab>
                    <telerik:RadTab runat="server" Text="Background" Value="Background ">
                        <Tabs>
                            <telerik:RadTab runat="server" Text="Education" Value="Education">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Skills" Value="Skills">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Employment" Value="Employment">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Extracurriculars" Value="Extracurriculars">
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTab>
                    <telerik:RadTab runat="server" Text="Admissions" Value="Admissions">
                        <Tabs>
                            <telerik:RadTab runat="server" Text="Groups" Value="Groups">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Notes" Value="Notes">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Tasks" Value="Tasks">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Source Tracking" Value="Source">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Requirements" Value="Requirements">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Documents" Value="Documents">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Entrance Test" Value="EntranceTest">
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTab>
                    <telerik:RadTab runat="server" Text="Student Management" Value="StudentManagement">
                        <Tabs>
                            <telerik:RadTab runat="server" Text="Groups" Value="Groups">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Notes" Value="Notes">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Tasks" Value="Tasks">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Source Tracking" Value="Source">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Requirements" Value="Requirements">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Documents" Value="Documents">
                            </telerik:RadTab>
                            <telerik:RadTab runat="server" Text="Entrance Test" Value="EntranceTest">
                            </telerik:RadTab>
                        </Tabs>
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
        <div id="LeadSummary" runat="server">
            <fieldset class="FS1">
                <legend>Personal Information</legend>
                <input runat="server" id="Photo" type="image" width="100" height="100" src="~/images/samplePhoto.jpg" style="float:left;padding:15px;vertical-align:top;" />
                <div class="FormRow">
                <div class="FGroup1">
                    <asp:Label ID="La7" runat="server" Text="Miss Tyesha Allen"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La3" runat="server" Text="123 Main St. Apt. B1132"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La5" runat="server" Text="Fort Lauderdale, FL 33308"  CssClass="TextFieldLabel"></asp:Label>
                </div>
                 <div class="FGroup1">
                    <asp:Label ID="La12" runat="server" Text="Gender: Female"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La6" runat="server" Text="DOB: 01/25/1971"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La8" runat="server" Text="Age: 25"  CssClass="TextFieldLabel"></asp:Label>

                    <asp:Label ID="La9" runat="server" Text="Phone: 954-555-1212"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La10" runat="server" Text="Race: White"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La11" runat="server" Text="Martial Status: Single"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La13" runat="server" Text="Children: 0"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La14" runat="server" Text="Housing Type: Off Campus"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La15" runat="server" Text="Geographic Type: Urban (Over 100,000)"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La16" runat="server" Text="Admissions Rep: Courtney Timm"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La17" runat="server" Text="Date Assigned: 01/01/2012"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La18" runat="server" Text="Tour Date: 01/01/2012"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La19" runat="server" Text="Admin Criteria: High School Grad"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="Label23" runat="server" Text="SSN: 014-55-3888"  CssClass="TextFieldLabel" Visible="true"></asp:Label>
                    <asp:Label ID="Label24" runat="server" Text="Lead Status: Enrolled"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="Label25" runat="server" Text="Dependency Type: Non-degree/certificate seeking"  CssClass="TextFieldLabel"></asp:Label>
                </div>
                 <div class="FGroup1">
                    <asp:Label ID="La20" runat="server" Text="Degree/Certificate Seeking Type: 25"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La21" runat="server" Text="Age: 25"  CssClass="TextFieldLabel"></asp:Label>
                    <asp:Label ID="La22" runat="server" Text="Age: 25"  CssClass="TextFieldLabel"></asp:Label>
                </div>
                 <div class="FGroup1">

                </div>
                 <div class="FGroup1">

                </div>
                </div>
            </fieldset>
        </div>
            <div id="InfoView" runat="server" visible="false">
            <fieldset class="FS1">
                <legend>Name</legend>
                <div class="FormRow">
                <div class="FGroup1">
                    <asp:Label ID="LPrefix" runat="server" Text="Prefix" AssociatedControlID="RadPrefix" CssClass="TextFieldLabel"></asp:Label>
                    <telerik:RadComboBox ID="RadPrefix" runat="server" Width="50px">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--" Value="--" />
                            <telerik:RadComboBoxItem runat="server" Text="Mr." Value="Mr." />
                            <telerik:RadComboBoxItem runat="server" Text="Mrs." Value="Mrs." />
                            <telerik:RadComboBoxItem runat="server" Text="Ms." Value="Ms." />
                            <telerik:RadComboBoxItem runat="server" Text="D.S.S." Value="Ms." />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                 <div class="FGroup1">
                    <asp:Label ID="LFirst" runat="server" Text="First Name" AssociatedControlID="RadTextBox1" CssClass="TextFieldLabel"></asp:Label>
                    <telerik:RadTextBox ID="RadTextBox1" runat="server" Width="150px">
                    </telerik:RadTextBox>
                </div>
                 <div class="FGroup1">
                    <asp:Label ID="LMiddle" runat="server" Text="Middle Name" AssociatedControlID="RadTextBox2" CssClass="TextFieldLabel"></asp:Label>
                    <telerik:RadTextBox ID="RadTextBox2" runat="server">
                    </telerik:RadTextBox>
                </div>
                 <div class="FGroup1">
                    <asp:Label ID="LLast" runat="server" Text="Last Name" AssociatedControlID="RadTextBox5" CssClass="TextFieldLabel"></asp:Label>
                    <telerik:RadTextBox ID="RadTextBox5" runat="server"  Width="150px">
                    </telerik:RadTextBox>
                </div>
                 <div class="FGroup1">
                    <asp:Label ID="LSuffix" runat="server" Text="Suffix" AssociatedControlID="RadSuffix" CssClass="TextFieldLabel"></asp:Label>
                    <telerik:RadComboBox ID="RadSuffix" runat="server" Width="50px">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="--" Value="--" />
                            <telerik:RadComboBoxItem runat="server" Text="Mr." Value="Mr." />
                            <telerik:RadComboBoxItem runat="server" Text="Mrs." Value="Mrs." />
                            <telerik:RadComboBoxItem runat="server" Text="Ms." Value="Ms." />
                        </Items>
                    </telerik:RadComboBox>
                </div>
                </div>
            </fieldset>
            <fieldset class="FS1">
                <legend>Personal Information</legend> 
                <div class="FormRow">
                 <div class="FGroup1">
                    <asp:Label ID="Lab9" runat="server" Text="SSN" AssociatedControlID="RadMaskedTextBox1" CssClass="TextFieldLabel"></asp:Label>
                    <telerik:RadMaskedTextBox ID="RadMaskedTextBox1" runat="server" 
                         Mask="###-##-####">
                    </telerik:RadMaskedTextBox>
                </div>
                <div class="FGroup1">
                    <asp:Label ID="Lab1" runat="server" Text="DOB" AssociatedControlID="RadDatePicker1" CssClass="TextFieldLabel"></asp:Label>
                    <telerik:RadDatePicker ID="RadDatePicker1" runat="server">
                    </telerik:RadDatePicker>
                </div>
                 <div class="FGroup1">
                    <asp:Label ID="Lab2" runat="server" Text="Age" AssociatedControlID="RadTB1" CssClass="TextFieldLabel"></asp:Label>
                    <telerik:RadTextBox ID="RadTB1" runat="server" Width="50px">
                    </telerik:RadTextBox>
                </div>
                </div>                               
            </fieldset>
            </div>
        </div>
        <div id="tempHide" runat="server" visible="false">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1" >
				<!-- begin rightcolumn -->
				<tr>
					<td class="detailsframetop"><table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								 <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" Enabled="False" CssClass="save" Text="Save">
                                </asp:Button><asp:Button ID="btnNew" runat="server" Enabled="False" CssClass="new"
                                    Text="New" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server"
                                        Enabled="False" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button></td>
                            
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                            align="center">
                            <tr>
                                <td class="detailsframe">
                                    <div id="scrollwhole" class="scrollsingleframe" onscroll="scrollwindow();">
                                        <!-- begin table content-->
                                        <table class="contentleadmastertable" cellspacing="0" cellpadding="0" width="100%">
                                            <tr>
                                                <td class="contentcellheader" nowrap colspan="9" style="border-top: 0; border-left: 0;
                                                    border-right: 0">
                                                    <div style="width: 50%; float: left">
                                                        <asp:Label ID="lblgeneral" runat="server" CssClass="label" Font-Bold="true">General Information</asp:Label></div>
                                                    <div style="width: 35%; float: left">
                                                        <asp:LinkButton runat="server" ID="lnkEnrollLead" CausesValidation="false" CssClass="admissionlinks">Enroll Lead</asp:LinkButton>
                                                        <asp:LinkButton ID="lnkOpenRequirements" runat="server" CausesValidation="False"
                                                            CssClass="admissionlinks">Admission Requirements</asp:LinkButton></div>
                                                    <div style="width: 15%; left: 85%; text-align: right">
                                                       <%-- <asp:Button ID="btnPrevious" runat="server"  Text="Previous" CausesValidation="false" />
                                                        <asp:Button ID="btnNext" runat="server"  Text="Next" CausesValidation="false" />--%>
                                                        <telerik:RadButton ID="btnPrevious" runat="server"  Text="Previous" CausesValidation="false"></telerik:RadButton>
                                                        <telerik:RadButton ID="btnNext" runat="server"  Text="Next" CausesValidation="false"></telerik:RadButton>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:10%;padding:15px 0px 0px 15px;">
                                                    <input runat="server" id="myimage" type="image" width="100" height="100" src="<%=strImageURL %>" />

                                                     
                                                    <asp:LinkButton ID="lnkEnlarge" runat="server"  CausesValidation="false" Visible="false">Enlarge</asp:LinkButton>
                                                </td>
                                                <td class="leadcell2" colspan="7" style="text-align: center; padding-right: 5px;
                                                    width: 90%; vertical-align: middle">
                                                    <asp:Label ID="lblEnrollStatus" runat="Server" Visible="false"></asp:Label>
                                                    <asp:LinkButton runat="server" ID="lnkToEnrollLead" CausesValidation="false" Visible="false">here</asp:LinkButton>
                                                    <asp:Label ID="lblOpenReqs" runat="Server" Visible="false"></asp:Label>
                                                    <asp:LinkButton runat="server" ID="lnkToOpenRequirements" CausesValidation="false">here</asp:LinkButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap style="padding-top: 16px">
                                                    <asp:Label ID="lblFirstName" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap style="padding-top: 16px">
                                                    <asp:TextBox ID="txtFirstName" TabIndex="1" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                                                </td>
                                                <td class="leadcellspacer" nowrap style="padding-top: 16px">
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap style="padding-top: 16px">
                                                    <asp:Label ID="lblGender" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap style="padding-top: 16px">
                                                    <asp:DropDownList ID="ddlGender" TabIndex="7" CssClass="dropdownlist" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcellspacer" nowrap style="padding-top: 16px">
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap style="padding-top: 16px">
                                                    <asp:Label ID="lblSSN" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2right" nowrap style="padding-top: 16px">
                                                    <%--<ew:MaskedTextBox ID="txtSSN" TabIndex="13" runat="server" CssClass="textbox">
                                                    </ew:MaskedTextBox>--%>
                                                    <telerik:RadMaskedTextBox ID="txtSSN" TabIndex="13" runat="server" CssClass="textbox"
                                                        Width="200px" DisplayFormatPosition="Right" Mask="#########" DisplayMask="###-##-####"
                                                        DisplayPromptChar="">
                                                    </telerik:RadMaskedTextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblMiddleName" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:TextBox ID="txtMiddleName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblBirthDate" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                   <%-- <asp:TextBox ID="txtBirthDate" TabIndex="8" CssClass="textboxdate" runat="server"
                                                        AutoPostBack="True"></asp:TextBox>&nbsp;<a onclick="javascript:OpenCalendarAge('ClsSect','txtBirthDate',1, true, 1945)"><img
                                                            id="Img2" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                                            align="absMiddle"></a>--%>
                                                        <telerik:RadDatePicker ID="txtBirthDate" MinDate="1/1/1945" runat="server" TabIndex="8">
                                                        </telerik:RadDatePicker>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblFamilyIncome" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2right" nowrap>
                                                    <asp:DropDownList ID="ddlFamilyIncome" TabIndex="14" CssClass="dropdownlist" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblLastName" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:TextBox ID="txtLastName" TabIndex="3" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblAge" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:TextBox ID="txtAge" TabIndex="9" CssClass="textbox" runat="server" ReadOnly="True"></asp:TextBox>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblPreviousEducation" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2right" nowrap>
                                                    <asp:DropDownList ID="ddlPreviousEducation" TabIndex="15" CssClass="dropdownlist"
                                                        runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblPrefix" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlPrefix" TabIndex="4" CssClass="dropdownlist" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblRace" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlRace" TabIndex="10" CssClass="dropdownlist" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblSponsor" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2right" nowrap>
                                                    <asp:DropDownList ID="ddlSponsor" TabIndex="16" CssClass="DropDownList" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblSuffix" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlSuffix" TabIndex="5" CssClass="dropdownlist" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblMaritalStatus" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlMaritalStatus" TabIndex="11" CssClass="dropdownlist" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblAdmissionsRep" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2right" nowrap>
                                                    <asp:DropDownList ID="ddlAdmissionsRep" TabIndex="17" CssClass="dropdownlist" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblLeadStatus" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlLeadStatus" TabIndex="6" CssClass="dropdownlist" runat="server"
                                                        BackColor="#ffff99">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblChildren" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:TextBox ID="txtChildren" TabIndex="12" CssClass="textbox" runat="server"></asp:TextBox>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblAssignedDate" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2right" nowrap>
                                                   <%-- <asp:TextBox ID="txtAssignedDate" TabIndex="18" CssClass="textboxdate" runat="server"></asp:TextBox>&nbsp;<a
                                                        onclick="javascript:OpenCalendar('ClsSect','txtAssignedDate', true, 1945)"><img id="Img4"
                                                            src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server" align="absMiddle"></a>--%>
                                                        <telerik:RadDatePicker ID="txtAssignedDate" MinDate="1/1/1945" runat="server" TabIndex="18">
                                                        </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblDependencyTypeId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlDependencyTypeId" TabIndex="5" CssClass="dropdownlist" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblGeographicTypeId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlGeographicTypeId" TabIndex="11" CssClass="dropdownlist"
                                                        runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblDateApplied" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2right" nowrap>
                                                    <%--<asp:TextBox ID="txtDateApplied" TabIndex="18" CssClass="textboxdate" runat="server"></asp:TextBox>&nbsp;<a
                                                        onclick="javascript:OpenCalendar('ClsSect','txtDateApplied', true, 1945)"><img id="Img5"
                                                            src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server" align="absMiddle"></a>--%>
                                                        <telerik:RadDatePicker ID="txtDateApplied" MinDate="1/1/1945" runat="server" TabIndex="18">
                                                        </telerik:RadDatePicker>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblCampusId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:DropDownList ID="ddlCampusId" CssClass="dropdownlist" runat="server" TabIndex="8">
                                                    </asp:DropDownList>
                                                    <td class="leadcellspacer" nowrap>
                                                        &nbsp;
                                                    </td>
                                                    <td class="leadcell" nowrap>
                                                        <asp:Label ID="lblHousingId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="leadcell2" nowrap>
                                                        <asp:DropDownList ID="ddlHousingId" TabIndex="11" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="leadcellspacer" nowrap>
                                                        &nbsp;
                                                    </td>
                                                    <td class="leadcell" nowrap>
                                                        <asp:Label ID="lblAdminCriteriaId" runat="server" CssClass="label"></asp:Label>
                                                    </td>
                                                    <td class="leadcell2right" nowrap>
                                                        <asp:DropDownList ID="ddlAdminCriteriaId" TabIndex="17" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="leadcellspacer" nowrap>
                                                        &nbsp;
                                                    </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap style="padding-bottom: 16px">
                                                    <asp:Label ID="lblDegCertSeekingId" runat="server" CssClass="label"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap style="padding-bottom: 16px">
                                                    <asp:DropDownList ID="ddlDegCertSeekingId" TabIndex="8" CssClass="dropdownlist" runat="server"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft" nowrap>
                                                    <asp:Label ID="lblEntranceInterviewDate" runat="server" CssClass="label" Visible="false"
                                                        Text="Entrance Interview Date"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                   <%-- <asp:TextBox ID="txtEntranceInterviewDate" TabIndex="18" CssClass="textboxdate" runat="server"
                                                        Visible="false"></asp:TextBox>&nbsp;<a onclick="javascript:OpenCalendar('ClsSect','txtEntranceInterviewDate', true, 1945)"><img
                                                            id="Img7" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                                            align="absMiddle" visible="false"></a>--%>
                                                        <telerik:RadDatePicker ID="txtEntranceInterviewDate" MinDate="1/1/1945" runat="server" TabIndex="18" Visible="false">
                                                        </telerik:RadDatePicker>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                    <asp:Label ID="lblHighSchoolProgramCode" runat="server" CssClass="label" Visible="false"
                                                        Text="Highschool Program Code"></asp:Label>
                                                </td>
                                                <td class="leadcell2" nowrap>
                                                    <asp:TextBox ID="txtHighSchoolProgramCode" runat="Server" CssClass="textbox" Visible="false"></asp:TextBox>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                                <td class="leadcell" nowrap>
                                                </td>
                                                <td class="leadcell2right" nowrap>
                                                </td>
                                                <td class="leadcellspacer" nowrap>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                            width="100%">
                                            <tr>
                                                <td class="contentcellheader" style="border-top: 0; border-left: 0; border-right: 0">
                                                    <asp:Label ID="Label1" runat="server" Font-Bold="true" CssClass="label">Select Lead Groups</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcellleft">
                                                    <asp:CheckBoxList ID="chkLeadGrpId" TabIndex="5" runat="Server" CssClass="checkboxstyle"
                                                        RepeatColumns="7">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                            width="100%">
                                            <tr>
                                                <td class="leadcell3">
                                                    <table class="contenttableborder" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="contentcellheader" nowrap colspan="4" style="border-top: 0; border-left: 0;
                                                                border-right: 0">
                                                                <asp:Label ID="lblAddress" runat="server" CssClass="label" Font-Bold="true">Address</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap style="padding-top: 10px">
                                                                &nbsp;
                                                            </td>
                                                            <td class="contentcell4column" nowrap style="padding-top: 10px">
                                                                <asp:CheckBox ID="chkForeignZip" TabIndex="19" CssClass="checkboxinternational" runat="server"
                                                                    AutoPostBack="true"></asp:CheckBox>
                                                            </td>
                                                            <td class="leadcell4column" nowrap style="padding-top: 10px">
                                                                &nbsp;
                                                            </td>
                                                            <td class="contentcell4columnright" nowrap style="padding-top: 10px">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblAddress1" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column">
                                                                <asp:TextBox ID="txtAddress1" TabIndex="20" CssClass="textbox" TextMode="MultiLine"
                                                                    MaxLength="50" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td class="leadcell4column" nowrap>
                                                                <asp:Label ID="lblAddressType" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright">
                                                                <asp:DropDownList ID="ddlAddressType" TabIndex="26" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblAddress2" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column">
                                                                <asp:TextBox ID="txtAddress2" TabIndex="21" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                                                            </td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblCounty" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright">
                                                                <asp:DropDownList ID="ddlCounty" TabIndex="27" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblCity" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column">
                                                                <asp:TextBox ID="txtCity" TabIndex="22" CssClass="textbox" MaxLength="50" runat="server"
                                                                    AutoPostBack="True"></asp:TextBox>
                                                            </td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblAddressStatus" runat="server" CssClass="label" Visible="true"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright">
                                                                <asp:DropDownList ID="ddlAddressStatus" CssClass="dropdownlist" runat="server" Visible="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblStateId" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column">
                                                                <asp:DropDownList ID="ddlStateID" TabIndex="23" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="leadcell4column" nowrap>
                                                                <asp:Label ID="lblOtherState" runat="server" CssClass="label" Visible="false"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright" nowrap>
                                                                <asp:TextBox ID="txtOtherState" CssClass="textbox" runat="server" AutoPostBack="True"
                                                                    Visible="false"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblZip" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column">
                                                                <%--<ew:MaskedTextBox ID="txtZip" TabIndex="24" runat="server" CssClass="textbox">
                                                                </ew:MaskedTextBox>--%>

                                                                 <telerik:RadMaskedTextBox ID="txtZip" tabindex="24" runat="server" DisplayPromptChar="" AutoPostBack="false" DisplayFormatPosition="Left"></telerik:RadMaskedTextBox>

                                                            </td>
                                                            <td class="leadcell4column">
                                                                &nbsp;
                                                            </td>
                                                            <td class="contentcell4columnright">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" style="padding-bottom: 16px">
                                                                <asp:Label ID="lblCountry" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column" style="padding-bottom: 16px">
                                                                <asp:DropDownList ID="ddlCountry" TabIndex="25" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="leadcell4column" style="padding-bottom: 16px">
                                                                &nbsp;
                                                            </td>
                                                            <td class="contentcell4columnright" style="padding-bottom: 16px">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="leadcell3">
                                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="contentcellheader" nowrap colspan="4" style="border-top: 0; border-right: 0;
                                                                border-left: 0">
                                                                <asp:Label ID="lblPhone1" runat="server" CssClass="label" Font-Bold="true">Phones and Email</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" style="padding-top: 10px" nowrap>
                                                                &nbsp;
                                                            </td>
                                                            <td class="contentcell4column" style="padding-top: 10px" nowrap>
                                                                <asp:CheckBox ID="chkForeignPhone" runat="server" AutoPostBack="true" CssClass="checkboxinternational">
                                                                </asp:CheckBox>
                                                            </td>
                                                            <td class="leadcell4column" style="padding-top: 10px" nowrap>
                                                                &nbsp;
                                                            </td>
                                                            <td class="contentcell4columnright" style="padding-top: 10px" nowrap>
                                                                <asp:CheckBox ID="chkForeignPhone2" TabIndex="35" CssClass="checkboxinternational"
                                                                    runat="server" AutoPostBack="true" Text="International"></asp:CheckBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft">
                                                                <asp:Label ID="lblPhone" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column">
                                                              <%--  <ew:MaskedTextBox ID="txtPhone" TabIndex="28" runat="server" CssClass="textbox">
                                                                </ew:MaskedTextBox>--%>
                                                                 <telerik:RadMaskedTextBox ID="txtphone" TabIndex="28" runat="server"
                                                                    Width="200px" DisplayFormatPosition="Left" DisplayPromptChar="">
                                                                 </telerik:RadMaskedTextBox>
                                                            </td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblPhone2" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright">
                                                               <%-- <ew:MaskedTextBox ID="txtPhone2" TabIndex="36" runat="server" CssClass="textbox">
                                                                </ew:MaskedTextBox>--%>
                                                                  <telerik:RadMaskedTextBox ID="txtPhone2" TabIndex="36" runat="server" 
                                                                    Width="200px" DisplayFormatPosition="Left" DisplayPromptChar="">
                                                                  </telerik:RadMaskedTextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblPhoneType" CssClass="label" runat="server">Type</asp:Label>
                                                            </td>
                                                            <td class="contentcell4column">
                                                                <asp:DropDownList ID="ddlPhoneType" TabIndex="29" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblPhoneType2" CssClass="label" runat="server">Type</asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright">
                                                                <asp:DropDownList ID="ddlPhoneType2" TabIndex="37" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
                                                                <asp:Label ID="lblPhoneStatus" runat="server" TabIndex="38" CssClass="label" Visible="True">Status</asp:Label>
                                                            </td>
                                                            <td class="contentcell4column" style="padding-bottom: 16px">
                                                                <asp:DropDownList ID="ddlPhoneStatus" TabIndex="38" CssClass="dropdownlist" runat="server"
                                                                    Visible="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="leadcell4column" style="padding-bottom: 16px">
                                                                <asp:Label ID="lblPhoneStatus2" runat="server" TabIndex="38" CssClass="label" Visible="True">Status</asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright" style="padding-bottom: 16px">
                                                                <asp:DropDownList ID="ddlPhoneStatus2" TabIndex="38" CssClass="dropdownlist" runat="server"
                                                                    Visible="True">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="Label2" CssClass="label" runat="server">Default Phone</asp:Label>
                                                            </td>
                                                            <td class="contentcell4column" style="padding-bottom: 16px">
                                                                <asp:RadioButton ID="rdoPhone1" Text="Phone 1" GroupName="rdoPhone" runat="server"
                                                                    TabIndex="38" CssClass="radiobutton" Checked />
                                                            </td>
                                                            <td>
                                                                &nbsp;
                                                            </td>
                                                            <td class="contentcell4columnright" style="padding-bottom: 16px">
                                                                <asp:RadioButton ID="rdoPhone2" Text="Phone 2" GroupName="rdoPhone" runat="server"
                                                                    TabIndex="38" CssClass="radiobutton" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
                                                                <asp:Label ID="lblWorkEmail" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column" style="padding-bottom: 16px">
                                                                <asp:TextBox ID="txtWorkEmail" TabIndex="39" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator5" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                    ControlToValidate="txtWorkEmail" ErrorMessage="Invalid WorkEmail Format" Display="None">Invalid Email Format</asp:RegularExpressionValidator>
                                                            </td>
                                                            <td class="leadcell4column" style="padding-bottom: 16px">
                                                                <asp:Label ID="lblHomeEmail" runat="server" CssClass="label">Home Email</asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright" style="padding-bottom: 16px">
                                                                <asp:TextBox ID="txtHomeEmail" TabIndex="40" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
                                                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator6" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                    ControlToValidate="txtHomeEmail" ErrorMessage="Invalid HomeEmail Format" Display="None">Invalid Email Format</asp:RegularExpressionValidator>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                            width="100%">
                                            <tr>
                                                <td class="leadcell3">
                                                    <table class="contenttableborder" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="contentcellheader" nowrap colspan="4" style="border-top: 0; border-left: 0;
                                                                border-right: 0">
                                                                <asp:Label ID="lblSource" runat="server" CssClass="label" Font-Bold="true">Source</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap style="padding-top: 16px">
                                                                <asp:Label ID="lblSourceCategoryId" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column" nowrap style="padding-top: 16px">
                                                                <asp:DropDownList ID="ddlSourceCategoryId" TabIndex="37" CssClass="dropdownlist"
                                                                    runat="server" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="leadcell4column" nowrap style="padding-top: 16px">
                                                                <asp:Label ID="lblSourceDate" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright" nowrap style="padding-top: 16px">
                                                              <%--  <asp:TextBox ID="txtSourceDate" TabIndex="38" CssClass="textboxdate" runat="server"></asp:TextBox>
                                                                &nbsp;<a onclick="javascript:OpenCalendar('ClsSect','txtSourceDate', true, 1945)"><img
                                                                    id="IMG1" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
                                                                    border="0" runat="server"></a>--%>
                                                                    <telerik:RadDatePicker ID="txtSourceDate" MinDate="1/1/1945" runat="server" TabIndex="38">
                                                                        </telerik:RadDatePicker>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblSourceTypeId" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column">
                                                                <asp:DropDownList ID="ddlSourceTypeId" TabIndex="44" CssClass="dropdownlist" runat="server"
                                                                    AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblInquiryTime" runat="server" CssClass="label">Inquiry Time</asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright">
                                                                <asp:TextBox ID="txtInquiryTime" TabIndex="38" CssClass="textboxdate" runat="server"
                                                                    ToolTip="HH:MM AM/PM"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
                                                                <asp:Label ID="lblSourceAdvertisement" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column" style="padding-bottom: 16px">
                                                                <asp:DropDownList ID="ddlSourceAdvertisement" TabIndex="44" CssClass="dropdownlist"
                                                                    runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="leadcell4column" style="padding-bottom: 16px">
                                                                <asp:Label ID="lblAdvertisementNote" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright" style="padding-bottom: 16px">
                                                                <asp:TextBox ID="txtAdvertisementNote" runat="server" CssClass="textbox"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="leadcell3">
                                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="contentcellheader" nowrap colspan="4" style="border-top: 0; border-right: 0;
                                                                border-left: 0">
                                                                <asp:Label ID="lblPersonalIdentification" runat="server" CssClass="label" Font-Bold="true">Personal Identification</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" style="padding-top: 16px" nowrap>
                                                                <asp:Label ID="lblNationality" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column" style="padding-top: 16px">
                                                                <asp:DropDownList ID="ddlNationality" TabIndex="37" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="leadcell4column" style="padding-top: 16px">
                                                                <asp:Label ID="lblDrivLicStateId" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright" style="padding-top: 16px">
                                                                <asp:DropDownList ID="ddlDrivLicStateId" TabIndex="40" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblCitizen" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column">
                                                                <asp:DropDownList ID="ddlCitizen" TabIndex="38" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblDrivLicNumber" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright">
                                                                <asp:TextBox ID="txtDrivLicNumber" TabIndex="41" CssClass="textbox" MaxLength="50"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" style="padding-bottom: 16px" nowrap>
                                                                <asp:Label ID="lblAlienNumber" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column" style="padding-bottom: 16px" nowrap>
                                                                <asp:TextBox ID="txtAlienNumber" TabIndex="39" CssClass="textbox" MaxLength="50"
                                                                    runat="server"></asp:TextBox>
                                                            </td>
                                                            <td class="leadcell4column" style="padding-bottom: 16px" nowrap>
                                                                &nbsp;
                                                            </td>
                                                            <td class="contentcell4columnright" style="padding-bottom: 16px">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                            width="100%">
                                            <tr>
                                                <td class="leadcell3">
                                                    <table class="contenttableborder" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="contentcellheader" nowrap colspan="4" style="border-top: 0; border-left: 0;
                                                                border-right: 0">
                                                                <asp:Label ID="lblInterestedIn" runat="server" CssClass="label" Font-Bold="true">Interested In</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" style="padding-top: 16px" nowrap>
                                                                <asp:Label ID="lblAreaID" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column" style="padding-top: 16px">
                                                                <asp:DropDownList ID="ddlAreaId" CssClass="dropdownlist" runat="server" TabIndex="44"
                                                                    AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="leadcell4column" style="padding-top: 16px">
                                                                <asp:Label ID="lblExpectedStart" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright" style="padding-top: 16px" nowrap>
                                                                <%--<asp:TextBox ID="txtExpectedStart" TabIndex="47" CssClass="textboxdate" runat="server"></asp:TextBox>
                                                                <a onclick="javascript:OpenCalendar('ClsSect','txtExpectedStart', true, 1945)">
                                                                    <img id="Img3" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
                                                                        border="0" runat="server"></a>--%>
                                                                    <telerik:RadDatePicker ID="txtExpectedStart" MinDate="1/1/1945" runat="server" TabIndex="47">
                                                                        </telerik:RadDatePicker>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap>
                                                                <asp:Label ID="lblProgramID" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column">
                                                                <asp:DropDownList ID="ddlProgramID" TabIndex="49" CssClass="dropdownlist" runat="server"
                                                                    AutoPostBack="true">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="leadcell4column">
                                                                <asp:Label ID="lblShiftID" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright">
                                                                <asp:DropDownList ID="ddlShiftID" TabIndex="48" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
                                                                <asp:Label ID="lblPrgVerId" runat="server" CssClass="label"></asp:Label>
                                                            </td>
                                                            <td class="contentcell4column" style="padding-bottom: 16px">
                                                                <asp:DropDownList ID="ddlPrgVerId" TabIndex="46" CssClass="dropdownlist" runat="server">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td class="contentcell4column" style="padding-bottom: 16px">
                                                                &nbsp;
                                                            </td>
                                                            <td class="contentcell4columnright" style="padding-bottom: 16px">
                                                                &nbsp;
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="leadcell3">
                                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td class="contentcellheader" nowrap colspan="6" style="border-top: 0; border-left: 0;
                                                                border-right: 0">
                                                                <asp:Label ID="Label5" runat="server" CssClass="label" Font-Bold="true">Comments</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="leadcell4columnleft" style="padding-top: 16px; padding-bottom: 16px" nowrap>
                                                                <asp:Label ID="lblComments" runat="server" CssClass="label">Comments</asp:Label>
                                                            </td>
                                                            <td class="contentcell4columnright" colspan="3" style="padding-top: 16px; padding-bottom: 16px;
                                                                width: 80%">
                                                                <asp:TextBox ID="txtComments" TabIndex="47" CssClass="textbox" runat="server" Rows="5"
                                                                    TextMode="Multiline" Columns="57"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                            <table class="contentleadmastertable" style="border-top: 0" cellspacing="0" cellpadding="0"
                                                width="100%">
                                                <tr>
                                                    <td class="contentcellheader" nowrap colspan="6" style="border-top: 0; border-left: 0;
                                                        border-right: 0">
                                                        <asp:Label ID="Label4" runat="server" Font-Bold="true" CssClass="label">School Defined Fields</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="contentcell2" colspan="6">
                                                        <asp:Panel ID="pnlSDF" TabIndex="50" runat="server" EnableViewState="false">
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" id="rosTable" visible="false" runat="server">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label3" runat="server" CssClass="label" Font-Bold="true" Visible="False">Resume Objectives Section</asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="contentcell" style="white-space: normal">
                                                    <asp:Label ID="Label6" runat="server" CssClass="tothemecomments" Visible="False">Objectives</asp:Label>
                                                </td>
                                                <td class="contentcell2" colspan="6">
                                                    <asp:TextBox ID="txtResumeObjective" TabIndex="48" MaxLength="300" runat="server"
                                                        Visible="true" Width="0px" CssClass="tocommentsnowrap" Rows="3" TextMode="MultiLine"
                                                        Columns="60"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- end content table-->
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>

        </div>
            <asp:TextBox ID="txtLeadMasterID" runat="server" Visible="False"></asp:TextBox>
        <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
        <asp:TextBox ID="txtCreatedDate" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="txtRecruitmentOffice" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="txtModDate" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="txtDate" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="txtPKID" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="txtLeadStatusId" runat="server" Visible="False"></asp:TextBox>
        <asp:TextBox ID="txtLeadGrpId" runat="server" Visible="False"></asp:TextBox>
        <input type="hidden" name="scrollposition" id="scrollposition" />
        <input id="__programid" type="hidden" name="__programid" runat="server" />
        <input id="__prgverid" type="hidden" name="__prgverid" runat="server" />
        <input id="__areaid" type="hidden" name="__areaid" runat="server" />
        <input id="__hiddensubmit" type="hidden" name="__hiddensubmit" runat="server" />
        <input id="__fieldchanges" type="hidden" name="__fieldchanges" runat="server" />
        <input id="__sourcetypeid" type="hidden" name="__sourcetypeid" runat="server" />
        <input id="__sourceadvid" type="hidden" name="__sourceadvid" runat="server" />
        <asp:TextBox ID="txtLeadStatusIdHidden" runat="server" Visible="false" />
         <td class="leadcellleft" style="padding-bottom: 16px">
               <asp:Label ID="lblLeadGrpId" runat="server" CssClass="label" Visible="False"></asp:Label>
               <asp:DropDownList ID="ddlLeadGrpId" TabIndex="5" CssClass="dropdownlist" Visible="False"
                                                    runat="server"></asp:DropDownList>

                                                     <telerik:RadNotification runat="server" ID="RadNotification1" 
                Text="This is a test" ShowCloseButton="true" 
                Width="400px" Height="125px"
                TitleIcon="" 
                Position="Center" Title="Message" 
                EnableRoundedCorners="true" 
                EnableShadow="true" 
                Animation="Fade" 
                AnimationDuration="1000" 
                   
                style="padding-left:120px; padding-top:5px; word-spacing:2pt;"> 
    </telerik:RadNotification> 
			
        </telerik:RadPane>
    </telerik:RadSplitter>
    <!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel>
			<!--end validation panel-->
</asp:Content>


