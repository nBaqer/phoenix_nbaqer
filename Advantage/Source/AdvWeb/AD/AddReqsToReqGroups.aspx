<%@ Page Language="vb" AutoEventWireup="false" Inherits="AddReqsToReqGroups" CodeFile="AddReqsToReqGroups.aspx.vb" %>
<%@ Register TagPrefix="fame" TagName="header" Src="~/UserControls/Header.ascx" %>
<%--<%@ Register TagPrefix="fame" TagName="footer" Src="../UserControls/Footer.ascx" %>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Requirement Group Definition</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<%--<LINK href="../css/localhost.css" type="text/css" rel="stylesheet">--%>
		<LINK href="../css/systememail.css" type="text/css" rel="stylesheet">
		<script src="../js/CheckAll.js" type="text/javascript"></script>
        <style type="text/css">
            .tablereq {
                width:100%;
                border: 0;
                border-spacing: 0;
                border-collapse: separate;
            }

            table td {
                padding: 0;
 
            }
        </style>
	</HEAD>
	<body leftMargin="0" topMargin="0" runat="server" ID="Body1">
		<form id="Form1" method="post" runat="server">
		<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
			<table class="tablereq">
				<tr>
					<td><img src="../images/advantage_req_Grp_Def.jpg"></td>
					<td class="topemail"><a class="close" href="javascript:" onClick="top.close()">X Close</a></td>
				</tr>
			</table>
			<table class="tablereq" style="height: 100%">
				<tr>
					<!-- begin left column -->
					<td class="listframe">
						<table class="tablereq">
							<tr>
							    <td class="listframetop">
								<table cellspacing="0" cellpadding="0" width="100%" border="0">
									<tr>
										<td nowrap align="left" width="15%">
											<asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
										<td nowrap width="85%">
											<asp:RadioButtonList ID="radStatus" CssClass="label" AutoPostBack="true" runat="Server"
												RepeatDirection="Horizontal">
												<asp:ListItem Text="Active" Selected="True" />
												<asp:ListItem Text="Inactive" />
												<asp:ListItem Text="All" />
											</asp:RadioButtonList></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
                                <td class="listframebottom">
								<div class="scrollleftfilters">
								    <asp:LinkButton ID="lnkAllReqs" Runat="server" CssClass="NonSelectedItem" CommandName="All" CausesValidation="False">View All Requirements</asp:LinkButton>
									<asp:DataList ID="dlstAdmissionReqGroups" runat="server" Width="100%" DataKeyField="LeadGrpId">
										<SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
										<ItemStyle CssClass="itemstyle"></ItemStyle>
										<ItemTemplate>
											<asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
												CausesValidation="False" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>'
												CommandArgument='<%# Container.DataItem("LeadGrpId")%>'></asp:ImageButton>
											<asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" CausesValidation="False"
												Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' CommandArgument='<%# Container.DataItem("LeadGrpId")%>'>
											</asp:ImageButton>
											<asp:LinkButton ID="Linkbutton1" runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("LeadGrpId")%>'
												Text='<%# Container.DataItem("Descrip") %> '
												CausesValidation="False" />
										</ItemTemplate>
									</asp:DataList>
								</div>
							</td>
								<%--<td class="listframetop" nowrap="nowrap"><asp:Label ID="lblNoFilter" Runat="server" CssClass="label"></asp:Label></td>--%>
							</tr>
						<%--	<tr>
								<td class="listframebottom">
									<div class="scrollleftaddreq">
										<asp:LinkButton ID="lnkAllReqs" Runat="server" CssClass="NonSelectedItem" CommandName="All" CausesValidation="False">View All Requirements</asp:LinkButton>
										<asp:datalist id="dlstAdmissionReqGroups" runat="server" DataKeyField="LeadGrpId" Width="100%">
											<SelectedItemStyle CssClass="SelectedItem"></SelectedItemStyle>
											<ItemStyle CssClass="NonSelectedItem"></ItemStyle>
											<ItemTemplate>
												<asp:LinkButton ID="Linkbutton1" Runat="server" CssClass="NonSelectedItem" CommandArgument='<%# Container.DataItem("LeadGrpId")%>' text='<%# Container.DataItem("Descrip")%> ' CausesValidation="False" />
											</ItemTemplate>
										</asp:datalist>
									</div>
								</td>
							</tr>--%>
						</table>
					</td>
					<!-- end left column -->
					<!-- begin right column -->
					<td class="leftside">
						<table class="tablereq" style="width: 9px">
						</table>
					</td>
					<td class="DetailsFrameTop">
						<table class="tablereq">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="MenuFrame" align="right">
								    <asp:button id="btnSave" runat="server" CssClass="save" Text="Save" Enabled="true"></asp:button>
                                    <asp:button id="btnNew" runat="server" CssClass="new" Text="New" Enabled="true"></asp:button>
                                    <asp:button id="btnDelete" runat="server" CssClass="delete hidden" Text="Delete" CausesValidation="true"
										Enabled="False"></asp:button>
								</td>
								
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
                        <div style="width:90%">
						<table class="maincontenttable" cellSpacing="0" cellPadding="0" width="95%" border="0">
							<tr>
								<td class="detailsframe">
									<div style="padding:25px;" >
										<!--begin content table-->
										<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" align="center">
											<TR>
												<TD class="twocolumnlabelcell" style="BORDER-TOP: #ebebeb 1px solid; BORDER-LEFT: #ebebeb 1px solid; WIDTH: 15%; BORDER-BOTTOM: #ebebeb 1px solid">
													<asp:label id="lblReqGrpDescrip" Runat="server" CssClass="labelbold">Requirement Group:</asp:label></TD>
												<TD class="twocolumncontentcell" style="BORDER-RIGHT:#ebebeb 1px solid; BORDER-TOP:#ebebeb 1px solid; WIDTH:85%; BORDER-BOTTOM:#ebebeb 1px solid; TEXT-ALIGN:left">
													<asp:label id="txtReqGrpDescrip" tabIndex="1" Runat="server" CssClass="label" enabled="true"></asp:label>
												</TD>
											</TR>
										</TABLE>
										<asp:Panel ID="pnlNumReqs" Runat="server" Visible="true">
											<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="100%" align="center">
												<TR>
													<TD class="spacertable"></TD>
												</TR>
												<TR>
													<TD class="contentcellheader">
														<asp:Label id="Label3" CssClass="labelbold" Runat="server">Select min requirements that need to be satisfied for each lead group</asp:Label></TD>
												</TR>
												<TR>
													<TD class="spacertables"></TD>
												</TR>
											</TABLE>
										</asp:Panel>
										<asp:Panel ID="pnlMandatory" Runat="server" Visible="False">
											<TABLE class="contenttable, tablereq" style=" width:90%;  text-align: center">
												<TR>
													<TD class="spacertables"></TD>
												</TR>
												<TR>
													<TD class="contentcell2">
														<asp:Label id="Label2" CssClass="labelbold" Runat="server">This mandatory requirement group applies to all lead groups and all mandatory requirement(s) will be selected by default and cannot be edited.</asp:Label></TD>
												</TR>
												<TR>
													<TD class="spacertables"></TD>
												</TR>
											</TABLE>
										</asp:Panel>
										<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="75%" align="center">
											<TR>
												<TD class="spacertables"></TD>
											</TR>
											<TR>
												<TD class="twocolumnlabelcell">
													<asp:label id="lblLeadGrpId" Runat="server" CssClass="label">Lead Groups</asp:label></TD>
												<TD class="twocolumncontentcell">
													<asp:dropdownlist id="ddlLeadGrpId" tabIndex="2" runat="server" CssClass="dropdownlistff" AutoPostBack="True"></asp:dropdownlist></TD>
											</TR>
							                <TR>
												<TD class="twocolumnlabelcell">
													<asp:label id="lblStatus" Runat="server" CssClass="label">Status</asp:label></TD>
												<TD class="twocolumncontentcell">
													<asp:dropdownlist id="ddlStatus" tabIndex="1" Runat="server" CssClass="dropdownlistff" enabled="true"></asp:dropdownlist></TD>
											</TR>
                                            <TR>
												<TD class="twocolumnlabelcell">
													<asp:label id="lblNumReqs" Runat="server" CssClass="label">Min Number Of Requirements</asp:label></TD>
												<TD class="twocolumncontentcell">
													<asp:dropdownlist id="ddlNumReqs" tabIndex="1" Runat="server" CssClass="dropdownlistff" enabled="true"></asp:dropdownlist></TD>
											</TR>

											<asp:textbox id="txtadReqGrpId" runat="server" visible="false" />
											<tr>
												<td>
													<asp:textbox id="txtRowIds" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
													<asp:textbox id="txtResourceId" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
													<asp:textbox id="txtReqGrpId" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
													<asp:CheckBox ID="chkMandatory" Runat="server" Visible="False"></asp:CheckBox>
													<asp:CheckBox id="chkIsInDB" Runat="server" visible="False"></asp:CheckBox>
													<asp:TextBox id="txtModUser" Runat="server" visible="False" Width="0px"></asp:TextBox>
													<asp:TextBox id="txtModDate" Runat="server" visible="False" Width="0px"></asp:TextBox>
												</td>
											</tr>
										</TABLE>
										<asp:Panel ID="pnlGrid" Runat="server" Visible="true">
											<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="90%" align="center">
												<TR>
													<TD class="spacertables"></TD>
												</TR>
												<TR>
													<TD class="contentcellheader">
														<asp:Label id="Label1" CssClass="labelbold" Runat="server">Select items from the list below to satisfy the "Min number of requirements"</asp:Label></TD>
												</TR>
												<TR>
													<TD class="spacertables"></TD>
												</TR>
											</TABLE>
										</asp:Panel>
										<TABLE width="90%" align="center" cellpadding="0" cellspacing="0">
											<TR>
												<TD noWrap width="100%">
													<asp:datagrid id="dgrdTransactionSearch" Runat="server" width="100%" BorderWidth="1px" ShowFooter="False"
														BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True" BorderColor="#E0E0E0">
														<AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
														<ItemStyle CssClass="datagriditemstyle" HorizontalAlign="Center"></ItemStyle>
														<HeaderStyle HorizontalAlign="Left" CssClass="labelbold"></HeaderStyle>
														<FooterStyle CssClass="datagriditemstyle"></FooterStyle>
														<EditItemStyle CssClass="datagriditemstyle"></EditItemStyle>
														<Columns>
															<asp:TemplateColumn HeaderText="All">
															<HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
																	<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																<HeaderTemplate>
																	<input id="chkAllItems" type="checkbox" value="check all" onclick="CheckAllDataGridCheckBoxes('chkUnschedule',
																							document.forms[0].chkAllItems.checked)" />
																</HeaderTemplate>
																<ItemTemplate>
																	<asp:checkbox Id="chkUnschedule" Runat="server" Checked='<%# Container.DataItem("ReqSelected") %>' />
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Requirement">
																<HeaderStyle CssClass="datagridheaderstyle" Width="60%"></HeaderStyle>
																	<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lblDescrip" Text='<%# Container.DataItem("Descrip") %>' cssClass="Label" Runat="server">
																	</asp:Label>
																	<asp:Label id="lbladReqId" Text='<%# Container.DataItem("adReqId") %>' cssClass="Label" Runat="server" Visible=False>
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Type">
																<HeaderStyle CssClass="datagridheaderstyle" Width="20%"></HeaderStyle>
																	<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																<ItemTemplate>
																	<asp:Label id="lblReqTypeDescrip" Text='<%# Container.DataItem("ReqTypeDescrip") %>' cssClass="Label" Runat="server">
																	</asp:Label>
																</ItemTemplate>
															</asp:TemplateColumn>
															<asp:TemplateColumn HeaderText="Required">
																<HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
																<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																<ItemTemplate>
																    <asp:checkbox id="chkRequired" Checked='<%# ChangeToBoolean(Container.DataItem("Required")) %>' cssClass="CheckBoxStyle" Runat="server">
														            </asp:checkbox>
																</ItemTemplate>
															</asp:TemplateColumn>
														</Columns>
													</asp:datagrid></TD>
											</TR>
										</TABLE>
										<asp:Panel ID="pnlAllReqs" Runat="server">
											<TABLE cellSpacing="0" cellPadding="0" width="90%" align="center">
												<TR>
													<TD noWrap width="100%">
														<asp:datagrid id="Datagrid1" Runat="server" BorderColor="#E0E0E0" AllowSorting="True" AutoGenerateColumns="False"
															BorderStyle="Solid" ShowFooter="False" BorderWidth="1px" width="100%">
															<AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
															<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
															<HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
															<FooterStyle CssClass="datagriditemstyle"></FooterStyle>
															<EditItemStyle CssClass="datagriditemstyle"></EditItemStyle>
															<Columns>
																<asp:TemplateColumn HeaderText="Requirement">
																	<HeaderStyle CssClass="datagridheaderstyle" Width="50%"></HeaderStyle>
																	<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																	<ItemTemplate>
																		<asp:Label id="Label4" Text='<%# Container.DataItem("Descrip") %>' cssClass="Label" Runat="server">
																		</asp:Label>
																		<asp:Label id="Label5" Text='<%# Container.DataItem("adReqId") %>' cssClass="Label" Runat="server" Visible=False>
																		</asp:Label>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Type">
																	<HeaderStyle CssClass="datagridheaderstyle" Width="20%"></HeaderStyle>
																	<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																	<ItemTemplate>
																		<asp:Label id="Label6" Text='<%# Container.DataItem("ReqTypeDescrip") %>' cssClass="Label" Runat="server">
																		</asp:Label>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Required">
																	<HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
																	<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																	<ItemTemplate>
																		<asp:Label id="Label7" Text='<%# converttoyesno(Container.DataItem("Required")) %>' cssClass="Label" Runat="server">
																		</asp:Label>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:TemplateColumn HeaderText="Lead Group">
																	<HeaderStyle CssClass="datagridheaderstyle" Width="20%"></HeaderStyle>
																	<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																	<ItemTemplate>
																		<asp:Label id="Label8" Text='<%# Container.DataItem("LeadgroupDescrip") %>' cssClass="Label" Runat="server">
																		</asp:Label>
																	</ItemTemplate>
																</asp:TemplateColumn>
															</Columns>
														</asp:datagrid></TD>
												</TR>
											</TABLE>
										</asp:Panel>
										<!-- end content table -->
									</div>
								</td>
							</tr>
						</table>
                        </div>
					</td>
					<!-- end rightcolumn --></tr>
			</table>
            <telerik:RadNotification runat="server" id="radErrorNotification" width="600px" CssClass="textbox" Title="Validation" BorderStyle="Solid" Position="Center" AutoCloseDelay="4000"></telerik:RadNotification>
			<%--<div id="footer">Copyright  FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>--%>
		</form>
	</body>
</HTML>


