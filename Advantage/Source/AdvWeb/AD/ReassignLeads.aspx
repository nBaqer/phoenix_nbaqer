﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ReassignLeads.aspx.vb" Inherits="ReassignLeads" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
				<!-- begin rightcolumn -->
				<tr>
					<td class="detailsframetop"><table width="100%" border="0" cellpadding="0" cellspacing="0">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" Enabled="false">
                                </asp:Button><asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"
                                    Enabled="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"
                                    Enabled="False"></asp:Button></td>
                            
                        </tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                            align="center">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                        <table width="80%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                            <tr>
                                                <td style="text-align: center; padding: 6px 20px 20px 20px">
                                                    <asp:Label ID="lblMessage" runat="server" CssClass="labelbold">Please select the desired criteria and then press "Show Leads" to see the list of leads to reassign.<br>
        Press "Assign Leads" to see the results. </asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="85%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                            <tr>
                                                <td class="leadcell4columnleft" nowrap style="padding-left: 0">
                                                    <asp:Label ID="lblCurrentAdmissionsRep" runat="server" CssClass="label">Admissions Rep</asp:Label>
                                                </td>
                                                <td class="contentcell4column" style="padding-right: 20px">
                                                    <asp:DropDownList ID="ddlCurrentAdmissionRepID" TabIndex="1" CssClass="dropdownlist"
                                                        runat="server" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                                <td class="leadcell4column" nowrap style="padding-left: 20px">
                                                    <asp:Label ID="lblStartLeadDate" runat="server" CssClass="label">Lead Start Date</asp:Label>
                                                </td>
                                                <td class="contentcell4columnright">
                                                   <%-- <asp:TextBox ID="txtStartLeadDate" TabIndex="3" CssClass="textboxdate" runat="server"></asp:TextBox>
                                                    &nbsp;<a onclick="javascript:OpenCalendar('ClsSect','txtStartLeadDate', true, 1945)"><img
                                                        id="Img2" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" align="absmiddle"
                                                        runat="server"></a>--%>
                                                    <telerik:RadDatePicker ID="txtStartLeadDate" MinDate="1/1/1945" runat="server">
                                                        </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="leadcell4columnleft" nowrap style="padding-left: 0">
                                                    <asp:Label ID="lblLeadStatuses" CssClass="label" runat="server">Lead Statuses</asp:Label>
                                                </td>
                                                <td class="contentcell4column" style="padding-right: 20px">
                                                    <asp:ListBox ID="lstLeadStatuses" runat="server" CssClass="textbox" TabIndex="2"
                                                        Rows="4" SelectionMode="Multiple" Height="75px"></asp:ListBox>
                                                </td>
                                                <td class="leadcell4column" nowrap style="padding-left: 20px">
                                                    <asp:Label ID="lblEndLeadDate" runat="server" CssClass="label">Lead End Date</asp:Label>
                                                </td>
                                                <td class="contentcell4columnright">
                                                   <%-- <asp:TextBox ID="txtEndLeadDate" TabIndex="4" CssClass="textboxdate" runat="server"></asp:TextBox>
                                                    &nbsp;<a onclick="javascript:OpenCalendar('ClsSect','txtEndLeadDate', true, 1945)"><img
                                                        id="Img1" src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server"
                                                        align="absmiddle"></a>--%>
                                                    <telerik:RadDatePicker ID="txtEndLeadDate" MinDate="1/1/1945" runat="server">
                                                        </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:CompareValidator ID="cvStartLeadDate" runat="server" ControlToValidate="txtStartLeadDate"
                                                        Display="None" ErrorMessage="Incorrect data type for Start Date" Operator="DataTypeCheck"
                                                        Type="Date" />
                                                </td>
                                                <td colspan="2">
                                                    <asp:CompareValidator ID="cvEndLeadDate" runat="server" ControlToValidate="txtEndLeadDate"
                                                        Display="None" ErrorMessage="Incorrect data type for End Date" Operator="DataTypeCheck"
                                                        Type="Date" />
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="80%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                            <tr>
                                                <td style="width: 100%; text-align: center; padding: 20px">
                                                    <asp:Button ID="btnSearch" runat="Server" TabIndex="5" Text="Show Leads" Width="100px"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="600px" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                            <tr>
                                                <td width="5%">
                                                    &nbsp;
                                                </td>
                                                <td width="30%">
                                                    <table cellspacing="0" cellpadding="0" width="100%" border="0" align="center">
                                                        <tr>
                                                            <td class="fourcolumnheader1" nowrap>
                                                                <asp:Label ID="Label2" CssClass="label" runat="server" Font-Bold="True">Select Leads to be assigned</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="fourcolumncontent1" nowrap>
                                                                <asp:ListBox ID="lstAssignedFlds" runat="server" TabIndex="6" CssClass="listboxes"
                                                                    Rows="10" SelectionMode="Multiple"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="2%">
                                                    &nbsp;
                                                </td>
                                                <td width="30%">
                                                    <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                                        <tr>
                                                            <td class="fourcolumnheader1" nowrap>
                                                                <asp:Label ID="lblAssignCampus" CssClass="label" runat="server" Font-Bold="True">Select Admissions Rep</asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="fourcolumncontent1" nowrap>
                                                                <asp:ListBox ID="lstReassignAdmissionReps" TabIndex="7" runat="server" CssClass="listboxes"
                                                                    Rows="10"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="8%">
                                                    <table width="100%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                                        <tr>
                                                            <td style="padding: 6px">
                                                                
                                                                    
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="80%" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                            <tr>
                                                <td style="width: 100%; text-align: center; padding: 20px">
                                                 <asp:Button ID="btnAdd" Text="Assign Leads" TabIndex="8" runat="server" Enabled="False"></asp:Button>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="40%" align="center">
                                            <tr>
                                                <td style="padding: 20px">
                                                    <asp:Label ID="lblAddress" runat="server" CssClass="label" Font-Bold="true">Results:</asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <table width="80%" border="0" cellpadding="0" cellspacing="0" align="center">
                                            <tr>
                                                <td>
                                                    <asp:DataGrid ID="dgrdTransactionSearch" CellPadding="0" AutoGenerateColumns="False"
                                                        AllowSorting="True" HeaderStyle-Wrap="true" EditItemStyle-Wrap="false" GridLines="Horizontal"
                                                        Width="100%" runat="server" BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0">
                                                        <EditItemStyle Wrap="False"></EditItemStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Lead(s)" HeaderStyle-HorizontalAlign="left" ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle CssClass="datagridheader" Width="50%"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLeadName" runat="server" Text='<%# Container.DataItem("FullName") %>'
                                                                        CssClass="label"> </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Admissions Rep" HeaderStyle-HorizontalAlign="left"
                                                                ItemStyle-HorizontalAlign="Left">
                                                                <HeaderStyle CssClass="datagridheader" Width="50%"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDgFirstName" Text='<%# Container.DataItem("AdmissionrepFullName") %>'
                                                                        CssClass="label" runat="server"> </asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
					</td>
					<!-- end rightcolumn --></tr>
			</table>
            <asp:TextBox ID="txtAdmissionReps" runat="server" Visible="False"></asp:TextBox>
            <asp:TextBox ID="txtLeadsInfo" runat="server" Visible="False"></asp:TextBox>
            <asp:TextBox ID="txtStatus" runat="server" Visible="false"></asp:TextBox>
			<!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

