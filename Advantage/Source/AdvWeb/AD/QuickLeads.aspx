<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master"
    Inherits="QuickLeads" EnableViewStateMac="True" CodeFile="QuickLeads.aspx.vb"
    EnableViewState="true" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>Quick Leads</title>
    <%--<script language="javascript" src="../UserControls/FullCalendar/script.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
			function scrollwindow(){
				window.document.Form1.scrollposition.value = window.document.getElementById("scrollwhole").scrollTop;
			}
			function loadbody(){
				window.document.getElementById("scrollwhole").scrollTop = "<%=Session("ScrollValue")%>";
			}
            function verifycampuschange(oldvalue){
            
            if(confirm('Campus selection change will result in clearing all other selected values.\n Do you want to continue ? ')){
            }else
            { document.getElementById("ddlCampusId").value=oldvalue;     
            return false;
            }
            } 
           
 </script>


    
    
</asp:Content>
<asp:Content ID="studentaddresses_content1" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="studentaddresses_content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="studentaddresses_content2" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>    
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Width="100%" Orientation="HorizontalTop">
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="detailsframetop">
                        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSaveNext" runat="server" Enabled="False" Text="Save" CssClass="save" 
                                        ToolTip="Clicking this button will enable user to add new leads with out exiting this page">
                                    </asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Enabled="False" Text="New" CssClass="new"
                                        CausesValidation="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Enabled="False" Text="Delete" CssClass="delete"
                                        CausesValidation="False"></asp:Button>
                                </td>
                                
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="100%"
                            border="0">
                            <tr>
                                <td class="detailsframe">
                                    <div id="scrollwhole" class="scrollsingleframe" onscroll="scrollwindow();">
                                        <!-- begin content table-->
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                            <tr>
                                                <td class="contentcellheader" style="border-top: 0; border-left: 0; border-right: 0"
                                                    colspan="8">
                                                    <div style="width: 70%; float: left">
                                                        <asp:Label ID="lblgeneral" runat="server" Font-Bold="true" CssClass="label">General Information</asp:Label></div>
                                                    <div style="width: 25%; left: 70%; text-align: right">
                                                        <asp:Button ID="btnSave" runat="server" Enabled="False" Text="Save and redirect to info page"
                                                             ToolTip="Clicking this button adds a new lead and redirects user to the Lead Info page">
                                                        </asp:Button></div>
                                                </td>
                                            </tr>
                                        </table>
                                        <p>
                                        </p>
                                        <table class="contenttable" cellspacing="0" cellpadding="2" width="60%" align="center">
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblFirstName" runat="server" CssClass="label" Text="First Name"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:TextBox ID="txtFirstName" TabIndex="1" CssClass="textbox" runat="server" MaxLength="50"
                                                        Width="300px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblMiddleName" runat="server" CssClass="label" Text="Middle Name"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:TextBox ID="txtMiddleName" TabIndex="2" CssClass="textbox" runat="server" MaxLength="50"
                                                        Width="300px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblLastName" runat="server" CssClass="label" Text="Last Name"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:TextBox ID="txtLastName" TabIndex="3" CssClass="textbox" runat="server" MaxLength="50"
                                                        Width="300px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblSSN" runat="server" CssClass="label" Text="SSN"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <%--<ew:MaskedTextBox ID="txtSSN" TabIndex="16" runat="server" CssClass="textbox" Width="300px">
                                                </ew:MaskedTextBox>--%>
                                                    <telerik:RadMaskedTextBox ID="txtSSN" TabIndex="4" runat="server" CssClass="textbox"
                                                        Width="200px" DisplayFormatPosition="Right"  Mask ="###-##-####"
                                                        DisplayPromptChar="">
                                                    </telerik:RadMaskedTextBox>
                                                    <asp:RegularExpressionValidator Display="None" ID="MaskedTextBoxRegularExpressionValidator" runat="server"
                                                     ErrorMessage="SSN format is ###-##-####"  ControlToValidate="txtSSN"  ValidationExpression="^\d{3}-\d{2}-\d{4}$" /> 
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblLeadStatus" runat="server" CssClass="label" Text="Lead Status"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlLeadStatus" TabIndex="6" CssClass="dropdownlist" runat="server"
                                                        Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblAdmissionsRep" runat="server" CssClass="label" Text="Admissions Rep"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlAdmissionsRep" TabIndex="7" CssClass="dropdownlist" runat="server"
                                                        Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblCampusId" runat="server" CssClass="label" Text="Campus"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlCampusId" TabIndex="8" CssClass="dropdownlist" runat="server"
                                                        Width="300px" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblGender" runat="server" CssClass="label" Text="Gender"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlGender" TabIndex="9" CssClass="dropdownlist" runat="server"
                                                        Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblBirthDate" runat="server" CssClass="label" Text="DOB"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell">
                                                    <%-- <asp:TextBox ID="txtBirthDate" TabIndex="10" CssClass="textbox" runat="server"
                                                    AutoPostBack="True" Width="280px"></asp:TextBox>
                                                &nbsp;<a onclick="javascript:OpenCalendarAge('ClsSect','txtBirthDate',1, true, 1945)"><img
                                                    id="Img2" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
                                                    border="0" runat="server"></a>--%>
                                                    <telerik:RadDatePicker ID="txtBirthDate" MinDate="1/1/1945" runat="server" TabIndex="10"
                                                        AutoPostBack="True">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblAge" runat="server" CssClass="label" Text="Age"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:TextBox ID="txtAge" TabIndex="11" CssClass="textbox" runat="server" ReadOnly="True"
                                                        Width="300px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblFamilyIncome" runat="server" CssClass="label" Text="Family Income"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlFamilyIncome" TabIndex="12" CssClass="dropdownlist" runat="server"
                                                        Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblPreviousEducation" runat="server" CssClass="label" Text="Previous Education"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlPreviousEducation" TabIndex="13" CssClass="dropdownlist"
                                                        runat="server" Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblPrefix" runat="server" CssClass="label" Text="Prefix"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlPrefix" TabIndex="14" CssClass="dropdownlist" runat="server"
                                                        Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblSuffix" runat="server" CssClass="label" Text="Suffix"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlSuffix" TabIndex="15" CssClass="dropdownlist" runat="server"
                                                        Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblRace" runat="server" CssClass="label" Text="Race"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlRace" TabIndex="16" CssClass="dropdownlist" runat="server"
                                                        Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblSponsor" runat="server" CssClass="label" Text="Sponsor"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlSponsor" TabIndex="17" CssClass="dropdownlist" runat="server"
                                                        Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblMaritalStatus" runat="server" CssClass="label" Text="Marital Status"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlMaritalStatus" TabIndex="18" CssClass="dropdownlist" runat="server"
                                                        Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblChildren" runat="server" CssClass="label" Text="Children"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:TextBox ID="txtChildren" TabIndex="19" runat="server" CssClass="textbox" Width="300px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblAssignedDate" runat="server" CssClass="label" Text="Assigned Date"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <%-- <asp:TextBox ID="txtAssignedDate" TabIndex="21" CssClass="textbox" runat="server" Width="280px"></asp:TextBox>
                                                &nbsp;<a onclick="javascript:OpenCalendar('ClsSect','txtAssignedDate', true, 1945)"><img
                                                    id="Img4" src="../UserControls/Calendar/PopUpCalendar.gif" align="top"
                                                    border="0" runat="server"></a>--%>
                                                    <telerik:RadDatePicker ID="txtAssignedDate" MinDate="1/1/1945" runat="server" TabIndex="21">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblDependencyTypeId" runat="server" CssClass="label" Text="Dependency Type"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlDependencyTypeId" TabIndex="22" CssClass="dropdownlist" runat="server"
                                                        Width="300px">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblGeographicTypeId" runat="server" CssClass="label" Text="Geographic Type"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlGeographicTypeId" TabIndex="23" CssClass="dropdownlist"
                                                        Width="300px" runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblDateApplied" runat="server" CssClass="label" Text="Date Applied"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <%--  <asp:TextBox ID="txtDateApplied" TabIndex="18" CssClass="textbox" runat="server" Width="280px"></asp:TextBox>&nbsp;<a
                                                    onclick="javascript:OpenCalendar('ClsSect','txtDateApplied', true, 1945)"><img id="Img5"
                                                        src="../UserControls/Calendar/PopUpCalendar.gif" border="0" runat="server" align="top"></a>--%>
                                                    <telerik:RadDatePicker ID="txtDateApplied" MinDate="1/1/1945" runat="server" TabIndex="24">
                                                    </telerik:RadDatePicker>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblHousingId" runat="server" CssClass="label" Text="Housing Type"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlHousingId" TabIndex="25" CssClass="dropdownlist" Width="300px"
                                                        runat="server">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblAdminCriteriaId" runat="server" CssClass="label" Visible="true"
                                                        Text="Admin Criteria"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlAdminCriteriaId" TabIndex="26" CssClass="dropdownlist" runat="server"
                                                        Width="300px" Visible="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell" nowrap>
                                                    <asp:Label ID="lblDegCertSeekingId" runat="server" CssClass="label" Visible="true"
                                                        Text="Degree/Certicate Seeking Type"></asp:Label>
                                                </td>
                                                <td class="twocolumncontentcell" nowrap>
                                                    <asp:DropDownList ID="ddlDegCertSeekingId" TabIndex="27" CssClass="dropdownlist"
                                                        runat="server" Width="300px" Visible="true">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                        <p>
                                        </p>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                            <tr>
                                                <td class="contentcellheader" style="border-top: 0; border-left: 0; border-right: 0"
                                                    colspan="8">
                                                    <div style="width: 88%; float: left">
                                                        <asp:Label ID="Label2" runat="server" Font-Bold="true" CssClass="label">Select Lead Groups<font color="red">*</font></asp:Label></div>
                                                    <div style="width: 12%; left: 88%; text-align: right">
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" align="center">
                                            <tr>
                                                <td style="width: 100%; padding: 3px 16px 3px 16px; text-align: left;" colspan="8"
                                                    nowrap>
                                                    <asp:CheckBoxList ID="chkLeadGrpId" TabIndex="28" runat="Server" CssClass="checkboxstyle"
                                                        RepeatColumns="7">
                                                    </asp:CheckBoxList>
                                                </td>
                                            </tr>
                                        </table>
                                        <p>
                                        </p>
                                        <asp:Panel ID="pnlAddressInfo" runat="server" Visible="false">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="contentcellheader" nowrap colspan="4" style="border-top: 0; border-left: 0;
                                                        border-right: 0">
                                                        <asp:Label ID="lblAddress" runat="server" Font-Bold="true" CssClass="label">Address</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap style="padding-top: 10px">
                                                    </td>
                                                    <td class="contentcell4column" nowrap style="padding-top: 10px">
                                                        <asp:CheckBox ID="chkForeignZip" TabIndex="30" CssClass="checkboxinternational" runat="server"
                                                            AutoPostBack="true" Text="International"></asp:CheckBox>
                                                    </td>
                                                    <td class="leadcell4column" style="padding-top: 10px" nowrap>
                                                        &nbsp;
                                                    </td>
                                                    <td class="contentcell4columnright" style="padding-top: 10px" nowrap>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap>
                                                        <asp:Label ID="lblAddress1" runat="server" CssClass="label" Text="Address 1"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <asp:TextBox ID="txtAddress1" TabIndex="31" CssClass="textbox" TextMode="MultiLine"
                                                            runat="server" MaxLength="50"></asp:TextBox>
                                                    </td>
                                                    <td class="leadcell4column" nowrap>
                                                        <asp:Label ID="lblAddressType" runat="server" CssClass="label" Text="Address Type"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright">
                                                        <asp:DropDownList ID="ddlAddressType" TabIndex="32" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap>
                                                        <asp:Label ID="lblAddress2" runat="server" CssClass="label" Text="Address 2"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <asp:TextBox ID="txtAddress2" TabIndex="33" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
                                                    </td>
                                                    <td class="leadcell4column" nowrap>
                                                        <asp:Label ID="lblCounty" runat="server" CssClass="label" Text="County"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright">
                                                        <asp:DropDownList ID="ddlCounty" TabIndex="34" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap>
                                                        <asp:Label ID="lblCity" runat="server" CssClass="label" Text="City"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <asp:TextBox ID="txtCity" TabIndex="35" CssClass="textbox" runat="server" AutoPostBack="true"
                                                            MaxLength="50"></asp:TextBox>
                                                    </td>
                                                    <td class="leadcell4column" nowrap>
                                                        <asp:Label ID="lblAddressStatus" runat="server" CssClass="label" Visible="false">Status</asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright">
                                                        <asp:DropDownList ID="ddlAddressStatus" TabIndex="36" CssClass="dropdownlist" runat="server"
                                                            Visible="false">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap>
                                                        <asp:Label ID="lblStateId" runat="server" CssClass="label" Text="State"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <asp:DropDownList ID="ddlStateID" TabIndex="37" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="leadcell4column" style="white-space: normal">
                                                        <asp:Label ID="lblOtherState" runat="server" CssClass="label" Visible="true" readonly="true"
                                                            Text="Other State"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright">
                                                        <asp:TextBox ID="txtOtherState" CssClass="textbox" TabIndex="38" runat="server" AutoPostBack="true"
                                                            Visible="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap>
                                                        <asp:Label ID="lblZip" runat="server" CssClass="label" Text="Zip"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <%--<ew:MaskedTextBox ID="txtZip" TabIndex="29" runat="server" CssClass="textbox">
                                                        </ew:MaskedTextBox>--%>
                                                        <telerik:RadMaskedTextBox ID="txtzip" TabIndex="39" runat="server" width="210px" cssclass="textbox" DisplayFormatPosition="Left"
                                                                    DisplayPromptChar=" " AutoPostBack="false">
                                                        </telerik:RadMaskedTextBox>
                                                        <%--  <asp:RegularExpressionValidator Display="None" ID="RegularExpressionValidator1" runat="server"
                                                     ErrorMessage="Zip format is #####"  ControlToValidate="txtzip"  ValidationExpression="^\d{5}$" /> --%>
                                                    </td>
                                                    <td class="leadcell4column" nowrap>
                                                        &nbsp;
                                                    </td>
                                                    <td class="contentcell4columnright">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
                                                        <asp:Label ID="lblCountry" runat="server" CssClass="label" Text="Country"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column" style="padding-bottom: 16px">
                                                        <asp:DropDownList ID="ddlCountry" TabIndex="40" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="leadcell4column" nowrap style="padding-bottom: 16px">
                                                        &nbsp;
                                                    </td>
                                                    <td class="contentcell4columnright" style="padding-bottom: 16px">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <p>
                                        </p>
                                        <asp:Panel ID="pnlPhoneInfo" runat="Server" Visible="false">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="contentcellheader" nowrap colspan="4" style="border-top: 0; border-right: 0;
                                                        border-left: 0">
                                                        <asp:Label ID="lblPhonelabel" runat="server" Font-Bold="true" CssClass="label">Phone and Email</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap style="padding-top: 10px">
                                                    </td>
                                                    <td class="contentcell4column" nowrap style="padding-top: 10px">
                                                        <asp:CheckBox ID="chkForeignPhone" TabIndex="41" CssClass="checkboxinternational"
                                                            runat="server" AutoPostBack="true" Text="International"></asp:CheckBox>
                                                    </td>
                                                    <td class="leadcell4column" style="padding-top: 10px" nowrap>
                                                        &nbsp;
                                                    </td>
                                                    <td class="contentcell4columnright" style="padding-top: 10px" nowrap>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap>
                                                        <asp:Label ID="lblPhone" runat="server" CssClass="label" Text="Phone"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column" nowrap>
                                                        <%--<ew:MaskedTextBox ID="txtPhone" TabIndex="36" runat="server" CssClass="textbox">
                                                            </ew:MaskedTextBox>--%>
                                                        <telerik:RadMaskedTextBox ID="txtphone" TabIndex="42" runat="server" 
                                                            Width="200px" DisplayFormatPosition="Left" Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar="">
                                                        </telerik:RadMaskedTextBox>
                                                    </td>
                                                    <td class="leadcell4column" nowrap>
                                                        <asp:Label ID="lblWorkEmail" runat="server" CssClass="label" Text="Work Email"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright" nowrap>
                                                        <asp:TextBox ID="txtWorkEmail" TabIndex="43" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator5" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                            ControlToValidate="txtWorkEmail" ErrorMessage="Invalid WorkEmail Format" Display="None">Invalid Email Format</asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap>
                                                        <asp:Label ID="lblPhoneType" CssClass="label" runat="server" Text="Phone Type">Type</asp:Label>
                                                    </td>
                                                    <td class="contentcell4column">
                                                        <asp:DropDownList ID="ddlPhoneType" TabIndex="44" CssClass="dropdownlist" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="leadcell4column">
                                                        <asp:Label ID="lblHomeEmail" runat="server" CssClass="label">Home Email</asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright">
                                                        <asp:TextBox ID="txtHomeEmail" TabIndex="45" CssClass="textbox" runat="server" MaxLength="50"></asp:TextBox>
                                                        <asp:RegularExpressionValidator ID="Regularexpressionvalidator6" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                            ControlToValidate="txtHomeEmail" ErrorMessage="Invalid HomeEmail Format" Display="None">Invalid Email Format</asp:RegularExpressionValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
                                                        <asp:Label ID="lblPhoneStatus" runat="server" TabIndex="46" CssClass="label" Visible="False">Status</asp:Label>
                                                    </td>
                                                    <td class="contentcell4column" style="padding-bottom: 16px">
                                                        <asp:DropDownList ID="ddlPhoneStatus" TabIndex="46" CssClass="dropdownlist" runat="server"
                                                            Visible="False">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="leadcell4column" style="padding-bottom: 16px">
                                                        &nbsp;
                                                    </td>
                                                    <td class="contentcell4columnright" style="padding-bottom: 16px">
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <asp:Panel ID="pnlSourceInfo" runat="Server" Visible="false">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                                <tr>
                                                    <td class="contentcellheader" nowrap colspan="4" style="border-left: 0; border-right: 0;
                                                        border-top: 0">
                                                        <asp:Label ID="lblSource" runat="server" Font-Bold="true" CssClass="label" Text="Source">Source</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="leadcell4columnleft" nowrap style="padding-top: 16px">
                                                        <asp:Label ID="lblSourceCategoryId" runat="server" CssClass="label" Text="Source Category"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4column" style="padding-top: 16px">
                                                        <asp:DropDownList ID="ddlSourceCategoryId" TabIndex="47" CssClass="dropdownlist"
                                                            runat="server" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="leadcell4column" style="padding-top: 16px">
                                                        <asp:Label ID="lblSourceDate" runat="server" CssClass="label" Text="Source Date"></asp:Label>
                                                    </td>
                                                    <td class="contentcell4columnright" style="padding-top: 16px" nowrap>
                                                        <%-- <asp:TextBox ID="txtSourceDate" TabIndex="44" CssClass="textboxdate" runat="server"></asp:TextBox>
                                                            &nbsp;<a onclick="javascript:OpenCalendar('ClsSect','txtSourceDate', true, 1945)"><img
                                                                id="IMG1" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
                                                                border="0" runat="server"></a>--%>
                                                        <telerik:RadDatePicker ID="txtSourceDate" MinDate="1/1/1945" runat="server" TabIndex="48">
                                                        </telerik:RadDatePicker>
                                                    </td>
                            </tr>
                            <tr>
                                <td class="leadcell4columnleft" nowrap>
                                    <asp:Label ID="lblSourceTypeId" runat="server" CssClass="label" Text="Source Type"></asp:Label>
                                </td>
                                <td class="contentcell4column">
                                    <asp:DropDownList ID="ddlSourceTypeId" TabIndex="49" CssClass="dropdownlist" runat="server"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                <td class="leadcell4column">
                                    <asp:Label ID="lblInquiryTime" runat="server" CssClass="label" Text="Inquiry Time"></asp:Label>
                                </td>
                                <td class="contentcell4columnright">
                                    <asp:TextBox ID="txtInquiryTime" runat="server" CssClass="textbox" TabIndex="50"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
                                    <asp:Label ID="lblSourceAdvertisement" runat="server" CssClass="label" Text="Source Advertisement"></asp:Label>
                                </td>
                                <td class="contentcell4column" style="padding-bottom: 16px">
                                    <asp:DropDownList ID="ddlSourceAdvertisement" TabIndex="51" CssClass="dropdownlist"
                                        runat="server">
                                    </asp:DropDownList>
                                </td>
                                <td class="leadcell4column" style="padding-bottom: 16px">
                                    <asp:Label ID="lblAdvertisementNote" runat="server" CssClass="label" Text="Note"></asp:Label>
                                </td>
                                <td class="contentcell4columnright" style="padding-bottom: 16px">
                                    <asp:TextBox ID="txtAdvertisementNote" runat="server" CssClass="textbox" TabIndex="52"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlPersonalInfo" runat="Server" Visible="False">
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="contentcellheader" nowrap colspan="4" style="border-left: 0; border-right: 0;
                                        border-top: 0">
                                        <asp:Label ID="lblPersonalIdentification" runat="server" Font-Bold="true" CssClass="label">Personal Identification</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="leadcell4columnleft" style="padding-top: 16px" nowrap>
                                        <asp:Label ID="lblNationality" runat="server" CssClass="label" Text="Nationality"></asp:Label>
                                    </td>
                                    <td class="contentcell4column" style="padding-top: 16px">
                                        <asp:DropDownList ID="ddlNationality" TabIndex="53" CssClass="dropdownlist" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="leadcell4column" style="padding-top: 16px">
                                        <asp:Label ID="lblDrivLicStateID" runat="server" CssClass="label" Text="Driv Lic State"></asp:Label>
                                    </td>
                                    <td class="contentcell4columnright" style="padding-top: 16px">
                                        <asp:DropDownList ID="ddlDrivLicStateID" TabIndex="54" CssClass="dropdownlist" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="leadcell4columnleft" nowrap>
                                        <asp:Label ID="lblCitizen" runat="server" CssClass="label" Text="Citizenship"></asp:Label>
                                    </td>
                                    <td class="contentcell4column">
                                        <asp:DropDownList ID="ddlCitizen" TabIndex="55" CssClass="dropdownlist" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="leadcell4column">
                                        <asp:Label ID="lblDrivLicNumber" runat="server" CssClass="label" Text="Driv Lic Number"></asp:Label>
                                    </td>
                                    <td class="contentcell4columnright">
                                        <asp:TextBox ID="txtDrivLicNumber" TabIndex="56" CssClass="textbox" runat="server"
                                            MaxLength="50"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
                                        <asp:Label ID="lblAlienNumber" runat="server" CssClass="label">Alien Number</asp:Label>
                                    </td>
                                    <td class="contentcell4column" style="padding-bottom: 16px">
                                        <asp:TextBox ID="txtAlienNumber" TabIndex="57" CssClass="textbox" runat="server"
                                            MaxLength="50"></asp:TextBox>
                                    </td>
                                    <td class="leadcell4column" style="padding-bottom: 16px">
                                        &nbsp;
                                    </td>
                                    <td class="contentcell4columnright" style="padding-bottom: 16px">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlInterestedInfo" runat="Server" Visible="false">
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="contentcellheader" nowrap colspan="4" style="border-left: 0; border-right: 0;
                                        border-top: 0">
                                        <asp:Label ID="lblInterestedIn" runat="server" Font-Bold="true" CssClass="label">Interested In</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="leadcell4columnleft" style="padding-top: 16px" nowrap>
                                        <asp:Label ID="lblAreaID" runat="server" CssClass="label" Text="Area"></asp:Label>
                                    </td>
                                    <td class="contentcell4column" style="padding-top: 16px">
                                        <asp:DropDownList ID="ddlAreaID" TabIndex="58" CssClass="dropdownlist" runat="server"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="leadcell4column" style="padding-top: 16px">
                                        <asp:Label ID="lblExpectedStart" runat="server" CssClass="label" Text="Expected Start"></asp:Label>
                                    </td>
                                    <td class="contentcell4columnright" style="padding-top: 16px" nowrap>
                                        <%-- <asp:TextBox ID="txtExpectedStart" TabIndex="51" CssClass="Textboxdate" runat="server"></asp:TextBox>
                                                            <a onclick="javascript:OpenCalendar('ClsSect','txtExpectedStart', true, 1945)">
                                                                <img id="Img3" src="../UserControls/Calendar/PopUpCalendar.gif" align="absMiddle"
                                                                    border="0" runat="server"></a>--%>
                                        <telerik:RadDatePicker ID="txtExpectedStart" MinDate="1/1/1945" runat="server" TabIndex="59">
                                        </telerik:RadDatePicker>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="leadcell4columnleft" nowrap>
                                        <asp:Label ID="lblProgramID" runat="server" CssClass="label" Text="Interested Program"></asp:Label>
                                    </td>
                                    <td class="contentcell4column">
                                        <asp:DropDownList ID="ddlProgramID" TabIndex="60" CssClass="dropdownlist" runat="server"
                                            AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="leadcell4column">
                                        <asp:Label ID="lblShiftID" runat="server" CssClass="label" Text="Shift"></asp:Label>
                                    </td>
                                    <td class="contentcell4columnright">
                                        <asp:DropDownList ID="ddlShiftID" TabIndex="62" CssClass="dropdownlist" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="leadcell4columnleft" nowrap style="padding-bottom: 16px">
                                        <asp:Label ID="lblPrgVerId" runat="server" CssClass="label" Text="Program Version"></asp:Label>
                                    </td>
                                    <td class="contentcell4column" style="padding-bottom: 16px">
                                        <asp:DropDownList ID="ddlPrgVerId" TabIndex="63" CssClass="dropdownlist" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="leadcell4column" style="padding-bottom: 16px">
                                        &nbsp;
                                    </td>
                                    <td class="contentcell4columnright" style="padding-bottom: 16px">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pnlComments" runat="Server" Visible="false">
                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                <tr>
                                    <td class="contentcellheader" nowrap colspan="2" style="border-left: 0; border-right: 0;
                                        border-top: 0">
                                        <asp:Label ID="Label5" runat="server" Font-Bold="true" CssClass="label">Comments</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="leadcell4columnleft" style="padding-top: 16px" nowrap>
                                        <asp:Label ID="lblComments" runat="server" CssClass="label">Comments</asp:Label>
                                    </td>
                                    <td class="contentcell4columnright" style="padding-top: 16px; padding-bottom: 16px">
                                        <asp:TextBox ID="txtComments" TabIndex="64" runat="server" CssClass="textbox" Columns="57"
                                            TextMode="Multiline" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td class="spacertables">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label3" runat="server" Font-Bold="true" CssClass="label" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="spacertables">
                                </td>
                            </tr>
                        </table>
                        <!-- end content table-->
                        </div>
                    </td>
                    </tr> </table> </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
             <telerik:RadNotification runat="server" ID="RadNotification1" 
                Text="This is a test" ShowCloseButton="true" 
                Width="400px" Height="125px"
                TitleIcon="" 
                Position="Center" Title="Message" 
                EnableRoundedCorners="true" 
                EnableShadow="true" 
                Animation="Fade" 
                AnimationDuration="1000" 
                   
                style="padding-left:120px; padding-top:5px; word-spacing:2pt;"> 
    </telerik:RadNotification> 	
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:HiddenField ID="txtDate" runat="server" />
    <asp:HiddenField ID="txtPKID" runat="server" />
    <asp:HiddenField ID="txtLeadMasterID" runat="server" />
    <asp:HiddenField ID="lblSourceTypeID1" runat="server" />
    <asp:HiddenField ID="txtLeadId" runat="server" />
    <asp:HiddenField ID="txtCreatedDate" runat="server" />
    <asp:HiddenField ID="txtModDate" runat="server" />
    <asp:HiddenField ID="txtUserId" runat="server" />
    <asp:HiddenField ID="txtCampusId" runat="server" />
    <asp:HiddenField ID="txtLeadStatusId" runat="server" />
    <asp:HiddenField ID="txtVal" runat="server" />
    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
        ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
    <!--end validation panel-->
    <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
    <asp:DropDownList ID="ddlSourceTypeId1" CssClass="dropdownlist" runat="server" Visible="False">
    </asp:DropDownList>
    <input type="hidden" name="scrollposition" id="scrollposition" />
    <input id="__programid" type="hidden" name="__programid" runat="server" />
    <input id="__prgverid" type="hidden" name="__prgverid" runat="server" />
    <input id="__areaid" type="hidden" name="__areaid" runat="server" />
    <input id="__hiddensubmit" type="hidden" name="__hiddensubmit" runat="server" />
    <input id="__fieldchanges" type="hidden" name="__fieldchanges" runat="server" />
    <input id="__sourcetypeid" type="hidden" name="__sourcetypeid" runat="server" />
    <input id="__sourceadvid" type="hidden" name="__sourceadvid" runat="server" />
    <input id="dup" type="hidden" runat="server" value="no" clientidmode="Static" />
  
      
</asp:Content>
