<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" 
    CodeFile="OverRideAdmissionReqs.aspx.vb" Inherits="OverRideAdmissionReqs" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
     <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy2" runat="server">
        <AjaxSettings>       
        </AjaxSettings> 
</telerik:RadAjaxManagerProxy>

        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%"
            Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350"
                Scrolling="Y">
                <table class="Table100">
                    <tr>
                        <td class="listframetop">
                            <table class="Table100">
                                <tr>
                                    <td class="employersearch" style="white-space: nowrap; text-align: left">
                                        <asp:Label ID="lblFirstName" CssClass="label" runat="server">First Name</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:TextBox ID="txtFirstName" CssClass="textbox" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch" style="white-space: nowrap; text-align: left">
                                        <asp:Label ID="lblLastName" CssClass="label" runat="server">Last Name</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:TextBox ID="txtLastName" CssClass="textbox" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch" style="white-space: nowrap; text-align: left">
                                        <asp:Label ID="lblSSN" runat="Server" CssClass="label">SSN</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <telerik:RadMaskedTextBox ID="txtSSN" runat="server" CssClass="textbox" Mask="###-##-####">
                                        </telerik:RadMaskedTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch" style="white-space: nowrap; text-align: left">
                                        <asp:Label ID="lblLeadStatusId" CssClass="label" runat="server">Lead Status</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlLeadStatusId" CssClass="dropdownlist" runat="server" Width="240px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch" style="white-space: nowrap; text-align: left">
                                        <asp:Label ID="lblLeadId" CssClass="label" runat="server">Leads</asp:Label>
                                    </td>
                                    <td class="employersearch2">
                                        <asp:DropDownList ID="ddlLeadId" CssClass="dropdownlist" runat="server" Width="240px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="employersearch">
                                        &nbsp;
                                    </td>
                                    <td class="employersearch2" style="text-align: center">
                                        <asp:Button ID="btnSearch" runat="server" Text="Build Leads" CausesValidation="False">
                                        </asp:Button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    </table>
                      <table class="Table100">
                    <tr>
                        <td class="listframebottom">
                           
                                <asp:DataList ID="dlstLeadNames" runat="server" Width="100%" DataKeyField="LeadId">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>                                              
                                    <ItemTemplate>
                                        <asp:LinkButton Text='<%# Container.DataItem("FullName")%>' runat="server" CssClass="itemstyle"
                                            CommandArgument='<%# Container.DataItem("LeadId")%>'
                                            ID="Linkbutton1" CausesValidation="false" />
                                    </ItemTemplate>
                                </asp:DataList>
                           
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both"
                orientation="horizontaltop">
                <table  class="Table100">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" style="text-align: right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false">
                            </asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false">
                            </asp:Button>
                        </td>
                        
                    </tr>
                </table>
                <table class="maincontenttable Table100" >
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlHeader" runat="server">
                                <div class="scrollright2">
                                    <table class="contenttable Table100" width="100%">
                                        <tr>
                                            <td class="contentcellheader">
                                                <asp:Label ID="lblLeadName" runat="server" CssClass="labelbold" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Panel ID="pnlControls" runat="server" Visible="True">
                                    </asp:Panel>
                                    <table class="contenttable Table100" style="text-align:center;" >
                                        <tr>
                                            <td style="padding-top: 10px; white-space: nowrap; width: 100%">
                                                <asp:DataGrid ID="dgrdTransactionSearch" runat="server" Width="100%" BorderWidth="1px"
                                                    ShowFooter="False" BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True"
                                                    BorderColor="#E0E0E0">
                                                    <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="datagriditemstyle" HorizontalAlign="center"></ItemStyle>
                                                    <HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
                                                    <FooterStyle CssClass="datagridpadding"></FooterStyle>
                                                    <EditItemStyle CssClass="datagridpadding"></EditItemStyle>
                                                    <Columns>
                                                        <asp:TemplateColumn HeaderText="Requirements">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="50%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEntrTestDescrip" Text='<%# Container.DataItem("Descrip") %>' CssClass="label"
                                                                    runat="server">
                                                                </asp:Label>
                                                                <asp:Label ID="lblEntrTestId" Text='<%# Container.DataItem("adReqId") %>' CssClass="label"
                                                                    runat="server" Visible="false">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Type">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="20%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblReqTypeDescrip" Text='<%# Container.DataItem("ReqTypeDescrip") %>'
                                                                    CssClass="label" runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Test Passed">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle" HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="ChkPass" Checked='<%# Container.DataItem("Pass") %>' Enabled="False"
                                                                    CssClass="checkboxstyle" runat="server"></asp:CheckBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Document Approved">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle" HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDocStatus" Text='<%# Container.DataItem("DocStatusDescrip") %>'
                                                                    Enabled="False" CssClass="checkboxstyle" runat="server">
                                                                </asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                        <asp:TemplateColumn HeaderText="Override">
                                                            <HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
                                                            <ItemStyle CssClass="datagriditemstyle" HorizontalAlign="Center"></ItemStyle>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkOverRide" Checked='<%# Container.DataItem("OverRide") %>' Enabled="true"
                                                                    CssClass="checkboxstyle" runat="server"></asp:CheckBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtLeadId" runat="server" Visible="False" Width="0px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <!--end table content-->
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
       
        <!-- start validation panel-->
        <asp:Panel ID="panel1" runat="server" >
        </asp:Panel>
        <asp:CustomValidator ID="customvalidator1" runat="server" 
            ErrorMessage="customvalidator" Display="none"></asp:CustomValidator>
        <asp:Panel ID="pnlrequiredfieldvalidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="validationsummary1" runat="server" 
            ShowMessageBox="true" ShowSummary="false"></asp:ValidationSummary>
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
</asp:Content>
