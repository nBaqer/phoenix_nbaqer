Imports FAME.common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Drawing
Imports Telerik.Web.UI
Imports System.Text.RegularExpressions

Partial Class HighSchools
    Inherits BasePage
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Protected strDefaultCountry As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        Dim campusId As String
        Dim userId As String
        '        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString

        'Check if this page still exists in the menu while switching campus
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not IsPostBack Then
            ' Header1.EnableHistoryButton(False)

            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

            'Disable the new and delete buttons
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDDLs()

            '   bind datalist
            BindDataList(0,"")

            '   bind an empty page
            BindEmptySchool()


            Try
                ddlCountryId.SelectedValue = strDefaultCountry
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlCountryId.SelectedIndex = 0
            End Try

        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            'Call the procedure for edit
        End If
        '   GetInputMaskValue()
        If chkForeignZip.Checked = True Then
            ddlStateId.Enabled = False
            lblStateId.Text = "State"
            txtzip.Mask = "aaaaaaaaaa"
            txtzip.DisplayMask = "aaaaaaaaaa"
            txtOtherState.Visible = True
            lblOtherState.Visible = True
        Else
            ddlStateId.Enabled = True
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            txtzip.Mask = "#####"
            txtzip.DisplayMask = "#####"
        End If

        If chkForeignPhone.Checked = True Then
            txtPhone.Mask = "+###############"
            txtPhone.DisplayMask = "+###############"
        Else
            txtPhone.Mask = "(###)-###-####"
            txtPhone.DisplayMask = "(###)-###-####"
        End If

    End Sub
    Private Sub GetInputMaskValue()
        Dim facInputMasks As New InputMasksFacade
        '  Dim correctFormat As Boolean
        Dim strMask As String
        Dim zipMask As String
        '   Dim errorMessage As String
        Dim strPhoneReq As String
        Dim strZipReq As String

        '  Dim strFaxReq As String
        Dim objCommon As New CommonUtilities

        'Get The Input Mask for Phone/Fax and Zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        'Replace The Mask Character from # to 9 as Masked Edit TextBox 
        'accepts only certain characters as mask characters
        If chkForeignPhone.Checked = False Then
            txtPhone.Mask = Replace(strMask, "#", "9")
            lblPhone.ToolTip = strMask
        Else
            txtPhone.Mask = ""
            lblPhone.ToolTip = ""
        End If

        If chkForeignZip.Checked = False Then
            txtzip.Mask = Replace(zipMask, "#", "9")
            lblZip.ToolTip = zipMask
        Else
            txtzip.Mask = ""
            lblZip.ToolTip = ""
        End If

        'Get The RequiredField Value
        strPhoneReq = objCommon.SetRequiredColorMask("Phone")
        strZipReq = objCommon.SetRequiredColorMask("Zip")


        'If The Field Is Required Field Then Color The Masked
        'Edit Control
        If strPhoneReq = "Yes" Then
            txtPhone.BackColor = Color.FromName("#ffff99")
        End If
        If strZipReq = "Yes" Then
            txtzip.BackColor = Color.FromName("#ffff99")
        End If
    End Sub

    Private Function ValidateFieldsWithInputMasks() As String
        Dim facInputMasks As New InputMasksFacade
        ''correctFormat,, correctFormat2, correctFormat3
        Dim correctFormat1 As Boolean
        Dim strMask As String
        Dim zipMask As String
        Dim errorMessage As String

        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        'Validate the phone field format. If the field is empty we should not apply the mask
        'agaist it.
        errorMessage = ""

        If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
            correctFormat1 = facInputMasks.ValidateStringWithInputMask(strMask, txtPhone.Text)
            If correctFormat1 = False Then
                errorMessage &= "Incorrect format for phone field." & vbLf
            End If
        End If

        If txtZip.Text <> "" And chkForeignZip.Checked = False Then
            correctFormat1 = facInputMasks.ValidateStringWithInputMask(zipMask, txtZip.Text)
            If correctFormat1 = False Then
                errorMessage &= "Incorrect format for zip field." & vbLf
            End If
        End If

        Return Trim(errorMessage)
    End Function
    Private Sub BtnSaveClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim result As String
        Dim errorMessage As String = ""

        'Check If Mask is successful only if Foreign Is not checked
        'If chkForeignPhone.Checked = False Or chkForeignZip.Checked = False Then
        '    errorMessage = ValidateFieldsWithInputMasks()
        'Else
        '    errorMessage = ""
        'End If
        If Not chkForeignZip.Checked Then
            If ddlStateId.SelectedIndex = 0 Then
                errorMessage = "- State is required"
                DisplayErrorMessage(errorMessage)
                Exit Sub
            End If
            If Not String.IsNullOrEmpty(txtzip.Text) Then
                Dim rgx As New Regex("^\d{5}$")
                If Not rgx.IsMatch(txtzip.Text) Then
                    errorMessage = "Invalid Zip Code Format!"
                    DisplayErrorMessage(errorMessage)
                    Exit Sub
                End If
            End If
        End If
            If errorMessage = "" Then
            With New HighSchoolFacade
                result = .UpdateHighSchool(BuildSchool(txtHSId.Text), AdvantageSession.UserState.UserName)
            End With

            If Not result = "" Then
                DisplayErrorMessage(result)
                Exit Sub
            End If

            '   bind the datalist
            BindDataList(0, "")

            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstHighSchools, txtHSId.Text, ViewState)



            '   if there are no errors bind a new entity and init buttons
            If Page.IsValid Then
                chkIsInDB.Checked = True
                InitButtonsForLoad()
                InitButtonsForEdit()
            End If
        Else
            DisplayErrorMessage(errorMessage)
        End If

       ' CommonWebUtilities.RestoreItemValues(dlstHighSchools, txtHSId.Text)
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub BtnDeleteClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim hsFacade As New HighSchoolFacade
        If Not (txtHSId.Text = Guid.Empty.ToString) Then
            Dim result As String

            'check first if HS can be deleted
            result = hsFacade.VerifyIfHighSchoolCanBeDeleted(txtHSId.Text)

            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage("The high school you are trying to delete is Active/In Use and cannot be deleted")

            Else

                'update AcademicYear Info 
                result = hsFacade.DeleteHighSchoolInfo(txtHSId.Text, Date.Parse(txtModDate.Text))
                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                Else
                    '   bind datalist
                    BindDataList(0,"")

                    '   bind an empty page
                    BindEmptySchool()

                    Try
                        ddlCountryId.SelectedValue = strDefaultCountry
                    Catch ex As System.Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        ddlCountryId.SelectedIndex = 0
                    End Try

                    '   initialize buttons
                    InitButtonsForLoad()
                End If
            End If
        End If

       ' CommonWebUtilities.RestoreItemValues(dlstHighSchools, Guid.Empty.ToString)
    End Sub

    Private Sub BtnNewClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty page
        BindEmptySchool()

        Try
            ddlCountryId.SelectedValue = strDefaultCountry
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlCountryId.SelectedIndex = 0
        End Try

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstHighSchools, Guid.Empty.ToString, ViewState)

        'initialize buttons
        InitButtonsForLoad()
       ' CommonWebUtilities.RestoreItemValues(dlstHighSchools, Guid.Empty.ToString)

    End Sub
    'Private Sub InitButtonsForLoad()
    '    If pObj.HasFull Or pObj.HasAdd Then
    '        btnSave.Enabled = True
    '    Else
    '        btnSave.Enabled = False
    '    End If

    '    If pObj.HasFull Or pObj.HasAdd Then
    '        btnNew.Enabled = True
    '    Else
    '        btnNew.Enabled = True
    '    End If

    '    btnDelete.Enabled = False
    'End Sub
    'Private Sub InitButtonsForEdit()
    '    If pObj.HasFull Or pObj.HasEdit Then
    '        btnSave.Enabled = True
    '    Else
    '        btnSave.Enabled = False
    '    End If

    '    If pObj.HasFull Or pObj.HasDelete Then
    '        btnDelete.Enabled = True
    '        'Set the Delete Button so it prompts the user for confirmation when clicked
    '        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
    '    Else
    '        btnDelete.Enabled = False
    '    End If

    '    If pObj.HasFull Or pObj.HasAdd Then
    '        btnNew.Enabled = True
    '    Else
    '        btnNew.Enabled = True
    '    End If
    'End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnnew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btndelete.Enabled = True
        Else
            btndelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub

    Private Sub ClearRhs()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    'Private Sub DlstHighSchoolsItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstHighSchools.ItemCommand
    '    '   get the AcademicYearId from the backend and display it
    '    Dim hsFacade As New HighSchoolFacade
    '    BindSchool(hsFacade.GetHighSchoolInfo(e.CommandArgument))

    '    'Enable/Disable State and Make OtherState Visible/InVisible
    '    If chkForeignZip.Checked = True Then
    '        ddlStateId.Enabled = False
    '        txtOtherState.Visible = True
    '        lblOtherState.Visible = True
    '    Else
    '        ddlStateId.Enabled = True
    '        txtOtherState.Visible = False
    '        lblOtherState.Visible = False
    '    End If

    '    '   set Style to Selected Item
    '    '    CommonWebUtilities.SetStyleToSelectedItem(dlstHighSchools, e.CommandArgument, ViewState, Header1)

    '    '   initialize buttons
    '    InitButtonsForEdit()
    '    CommonWebUtilities.RestoreItemValues(dlstHighSchools, e.CommandArgument)
    'End Sub
    Private Sub RadStatusSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        InitButtonsForLoad()
        BindEmptySchool()
        BindDataList(radStatus.SelectedIndex,"")
    End Sub
    Protected Sub RadGrdPostScores_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrdPostScores.NeedDataSource
        Try
             Dim hsFacade As New HighSchoolFacade
        ' If Not RadGrdPostScores.MasterTableView.FilterExpression Is String.Empty Then
        '    Dim filterFunction As String  = RadGrdPostScores.MasterTableView.GetColumn(filter).CurrentFilterFunction.ToString()
        '    Dim filterValue = RadGrdPostScores.MasterTableView.GetColumn(filter).CurrentFilterValue
        '    RadGrdPostScores.DataSource = hsFacade.GetHighSchoolDataList(0,filterValue)
        'Else
            RadGrdPostScores.DataSource = hsFacade.GetHighSchoolDataList(0,"")
        'End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New Exception(ex.Message.ToString)
        End Try
     
    End Sub
    Private Sub BindDataList(ByVal strSelectedIndex As Integer, Optional ByVal HsName As String="")
        Dim hsFacade As New HighSchoolFacade
        Dim dt As System.Data.DataTable = hsFacade.GetHighSchoolDataList(strSelectedIndex,HsName,ddlInstitutionType.SelectedValue)
        RadGrdPostScores.DataSource = dt
        RadGrdPostScores.DataBind()
    End Sub
    Private Sub BuildStatesDdl()
        'Bind the Status DropDownList
        Dim states As New StatesFacade
        With ddlStateId
            .DataTextField = "StateDescrip"
            .DataValueField = "StateID"
            .DataSource = states.GetAllStates()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BuildCountryDdl()
        Dim country As New PrefixesFacade
        With ddlCountryId
            .DataTextField = "CountryDescrip"
            .DataValueField = "CountryId"
            .DataSource = country.GetAllCountries()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            'selected index must be the default country if it is defined in configuration file
            '.SelectedIndex = CommonWebUtilities.GetDefaultCountryIndexInDDL(ddlCountryId)
        End With
    End Sub

    Private Sub BuildCountyDdl()
        Dim county As New JobSearchFacade
        With ddlCountyId
            .DataTextField = "CountyDescrip"
            .DataValueField = "CountyId"
            .DataSource = county.GetCountyCodes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            'selected index must be the default country if it is defined in configuration file
            '.SelectedIndex = CommonWebUtilities.GetDefaultCountryIndexInDDL(ddlCountryId)
        End With
    End Sub

     Private Sub BuildPrefixDdl()
        Dim prefix As New PrefixesFacade
        With ddlPrefix
            .DataTextField = "PrefixDescrip"
            .DataValueField = "PrefixId"
            .DataSource = prefix.GetAllPrefixes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            'selected index must be the default country if it is defined in configuration file
            '.SelectedIndex = CommonWebUtilities.GetDefaultCountryIndexInDDL(ddlCountryId)
        End With
    End Sub
    Private Sub BuildSuffixDdl()
        Dim suffix As New SuffixesFacade
        With ddlSuffix 
            .DataTextField = "SuffixDescrip"
            .DataValueField = "SuffixId"
            .DataSource = suffix.GetAllSuffixes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            'selected index must be the default country if it is defined in configuration file
            '.SelectedIndex = CommonWebUtilities.GetDefaultCountryIndexInDDL(ddlCountryId)
        End With
    End Sub


    Private Sub BuildCampusGroupsDdl()

        'Bind the CampusGroups DrowDownList
        Dim campusGroups As New CampusGroupsFacade
        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = campusGroups.GetAllCampusGroups()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildStatusDdl()
        'Bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With

         With ddlContactStatus
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With
        
    End Sub
    Private Sub BuildDDLs()
        BuildStatesDdl()
        BuildStatusDdl()
        BuildCampusGroupsDdl()
        BuildCountryDdl()
        BuildCountyDdl()
        BuildPrefixDdl()
        BuildSuffixDdl()
    End Sub
    Private Sub BindSchool(ByVal hs As HighSchool)
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        With hs

            If .ForeignPhone = 1 Then
                chkForeignPhone.Checked = True
                txtPhone.Mask = "+###############"
                txtPhone.DisplayMask = "+###############"
            Else
                chkForeignPhone.Checked = False
                txtPhone.Mask = strMask
                txtPhone.DisplayMask = strMask
            End If
            If .ForeignZip = 1 Then
                lblStateId.Text = "State"
                chkForeignZip.Checked = True
                txtOtherState.Text = .OtherState
                ddlStateId.Enabled = False
                txtOtherState.Visible = True
                lblOtherState.Visible = True
                txtzip.Mask = "aaaaaaaaaa"
                txtzip.DisplayMask = "aaaaaaaaaa"
            Else
                chkForeignZip.Checked = False
                ddlStateId.Enabled = True
                txtOtherState.Visible = False
                lblOtherState.Visible = False
                txtzip.Mask = zipMask
                txtzip.DisplayMask = zipMask
            End If

            chkIsInDB.Checked = .IsInDB
            txtHSCode.Text = .Code
            txtHSId.Text = .HsId
            ddlStatusId.SelectedValue = .StatusId
            txtHSName.Text = .Name
            txtAddress1.Text = .Address1
            txtAddress2.Text = .Address2
            txtCity.Text = .City
            ddlStateId.SelectedValue = .StateId
            ddlCountryId.SelectedValue = .CountryId
            txtPhone.Text = .Phone
            ddlInstitutionTypeList.SelectedValue = .InstitutionType
            txtAddressId.Text = .InstitutionAddressId
            txtPhoneId.Text = .InstitutionPhoneId
            txtContactId.Text = .InstitutionContactId
            txtAddDefault.Text = .IsDefaultAdd.ToString()
            txtPhoneDefault.Text = .IsDefaultPhone.ToString()
            txtContactDefault.Text = .IsDefaultContact.ToString()
            Try
                ddlCampGrpId.SelectedValue = .CampusGroup
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlCampGrpId.SelectedIndex = 0
            End Try
            txtModDate.Text = .modDate.ToString

            txtPhone.Text = .Phone
            If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
                txtPhone.Text = facInputMasks.ApplyMask(strMask, txtPhone.Text)
            End If

            'Get Zip
            txtZip.Text = .Zip
            If txtZip.Text <> "" And chkForeignZip.Checked = False Then
                txtZip.Text = facInputMasks.ApplyMask(zipMask, txtZip.Text)
            End If

            txtOtherState.Text = .OtherState

            radSource.SelectedValue = .Source

              Try
                ddlCountyId.SelectedValue = .CountyId
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlCountyId.SelectedIndex = 0
            End Try

                Try
                ddlContactStatus.SelectedValue = .ContactStatus
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlContactStatus.SelectedIndex = 0
            End Try

            txtContactTitle.Text = .ContactTitle
            txtFirstName.Text = .ContactFirstName
            txtLastName.Text  = .ContactLastName
            txtMiddleName.Text = .ContactMiddleName
            If txtMiddleName.Text <> "" Then
                txtMiddleName.Text = txtMiddleName.Text.Substring(0, 1)
            End If
            txtContactPhone.Text = .ContactPhone
            txtContactEmail.Text = .ContactEmail
            
              Try
                ddlPrefix.SelectedValue = .PrefixId
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlPrefix.SelectedIndex = 0
            End Try

                 Try
                ddlSuffix.SelectedValue = .SuffixId
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlSuffix.SelectedIndex = 0
            End Try
        End With
    End Sub
    Private Sub BindEmptySchool()
        chkForeignPhone.Checked = False
        chkForeignZip.Checked = False
        chkIsInDB.Checked = False
        txtHSCode.Text = ""
        txtHSName.Text = ""
        txtAddress1.Text = ""
        txtAddress2.Text = ""
        txtCity.Text = ""
        txtPhone.Text = ""
        txtModDate.Text = ""
        txtPhone.Text = ""
        txtZip.Text = ""
        txtOtherState.Text = ""
        txtHSId.Text = Guid.NewGuid.ToString
        ddlStatusId.SelectedIndex = 0
        ddlStateId.SelectedIndex = 0
        ddlCountryId.SelectedIndex = 0
        ddlCampGrpId.SelectedIndex = 0
        txtOtherState.Visible = False
        lblOtherState.Visible = False
        radSource.SelectedValue = 1
        ddlInstitutionType.SelectedValue = 2
        ddlInstitutionTypeList.SelectedValue = 2
        ddlCountyId.SelectedIndex = 0
        ddlCountyId.SelectedIndex = 0
        ddlContactStatus.SelectedIndex = 0 
        ddlPrefix.SelectedIndex = 0
        ddlSuffix.SelectedIndex = 0      

        txtContactTitle.Text = ""
        txtFirstName.Text = ""
        txtLastName.Text  = ""
        txtMiddleName.Text = ""
        txtContactPhone.Text = ""
        txtContactEmail.Text = ""
         Try
            ddlCountryId.Items.FindByValue("USA").Selected = true
         Catch ex As Exception
          	Dim exTracker = new AdvApplicationInsightsInitializer()
         	exTracker.TrackExceptionWrapper(ex)

            ddlCountryId.SelectedIndex=0
         End Try
        
    End Sub
    Private Function BuildSchool(ByVal hSId As String) As HighSchool
        'instantiate class
        Dim hs As New HighSchool


        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        With hs
            'get IsInDB
            If chkForeignPhone.Checked = True Then
                .ForeignPhone = 1
            Else
                .ForeignPhone = 0
            End If

            If chkForeignZip.Checked = True Then
                .ForeignZip = 1
            Else
                .ForeignZip = 0
            End If

            .IsInDB = chkIsInDB.Checked

            .HsId = txtHSId.Text
            .InstitutionAddressId = txtAddressId.Text
            .InstitutionContactId = txtContactId.Text
            .InstitutionPhoneId = txtPhoneId.Text
            '.IsDefaultPhone = Convert.ToInt32(txtPhoneDefault.Text)
            .IsDefaultPhone = 1
            '.IsDefaultAdd = Convert.ToInt32(txtAddDefault.Text)
            .IsDefaultAdd = 1
            .IsDefaultContact = 1
            '.IsDefaultContact = Convert.ToInt32(txtContactDefault.Text)
            'get Code
            .Code = txtHSCode.Text.Trim

            'get StatusId
            .StatusId = ddlStatusId.SelectedValue

            'get Name
            .Name = txtHSName.Text

            'get Campus Group
            .CampusGroup = ddlCampGrpId.SelectedValue

            'Address1
            .Address1 = txtAddress1.Text

            'Address2
            .Address2 = txtAddress2.Text

            'City
            .City = txtCity.Text

            'StateId
            .StateId = ddlStateId.SelectedValue

            'Country
            .CountryId = ddlCountryId.SelectedValue

            'Phone 
            'If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
            '    '  .Phone = facInputMasks.RemoveMask(strMask, txtPhone.Text)
            'Else
            .Phone = txtPhone.Text
            ' End If

            'Get Zip
            'If txtzip.Text <> "" And chkForeignZip.Checked = False Then
            '    .Zip = facInputMasks.RemoveMask(zipMask, txtzip.Text)
            'Else
            .Zip = txtzip.Text
            ' End If
            .OtherState = txtOtherState.Text
            txtModDate.Text = Date.Now
            .modDate = txtModDate.Text

            .Source = radSource.SelectedValue

            .InstitutionType = ddlInstitutionTypeList.SelectedValue

          
            .CountyId = ddlCountyId.SelectedValue
            .ContactTitle = txtContactTitle.Text
            .ContactFirstName = txtFirstName.Text
            .ContactLastName = txtLastName.Text 
            .ContactMiddleName = txtMiddleName.Text
            .ContactPhone=txtContactPhone.Text 
            .ContactEmail=txtContactEmail.Text
            .ContactStatus=ddlContactStatus.SelectedValue

            .PrefixId = ddlPrefix.SelectedValue
            .SuffixId = ddlSuffix.SelectedValue
            
        End With
        'return data
        Return hs
    End Function

    Private Sub TxtCityTextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCity.TextChanged
        Dim objCommon As New CommonWebUtilities
        If chkForeignZip.Checked = True Then
            CommonWebUtilities.SetFocus(Me.Page, txtOtherState)
        End If
    End Sub

    Private Sub ChkForeignZipCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignZip.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtAddress1)
    End Sub

    Private Sub TxtZipTextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtZip.TextChanged

    End Sub

    Private Sub TxtOtherStateTextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOtherState.TextChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtZip)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        ' CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    public filter As String= string.Empty
    Protected Sub RadGrdPostScores_ItemCommand(sender As Object, e As GridCommandEventArgs) 
        If e.CommandName.ToString.ToLower = "rowclick"
            Dim item As GridDataItem = Ctype(e.Item,GridDataItem) 
           Dim strHsID As string  = Ctype(item.FindControl("lblHsID"),Label).Text.ToString()
           BindForm(strHsID)
        ElseIf  e.CommandName.ToString.ToLower = "filter"
            Dim filterPair As Pair = Ctype(e.CommandArgument,Pair)
            filter = filterPair.Second.ToString()
            Dim filterFunction As String  = RadGrdPostScores.MasterTableView.GetColumn(filter).CurrentFilterFunction.ToString()
            Dim filterValue As String  = RadGrdPostScores.MasterTableView.GetColumn(filter).CurrentFilterValue
           If Not filterValue Is String.Empty Then
                BindDataList(0, filterValue)
            End If
        End If
     End Sub
     Protected Sub BindForm(ByVal HSID As String)
         Dim hsFacade As New HighSchoolFacade
        BindSchool(hsFacade.GetHighSchoolInfo(HSID))
        'Enable/Disable State and Make OtherState Visible/InVisible
        If chkForeignZip.Checked = True Then
            ddlStateId.Enabled = False
            txtOtherState.Visible = True
            lblOtherState.Visible = True
        Else
            ddlStateId.Enabled = True
            txtOtherState.Visible = False
            lblOtherState.Visible = False
        End If

        '   set Style to Selected Item
        '    CommonWebUtilities.SetStyleToSelectedItem(dlstHighSchools, e.CommandArgument, ViewState, Header1)

        '   initialize buttons
        InitButtonsForEdit()
     End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        If Not String.IsNullOrEmpty(txtSearch.Text) Then
            BindDataList(0,Replace(txtSearch.Text,"""",""))
        Else
            BindDataList(0,"")
        End If
        Dim header As GridHeaderItem = TryCast(RadGrdPostScores.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
        If ddlInstitutionType.SelectedValue = 2 Then 
            CType(header.FindControl("lblHeaderDescription"),Label).Text = "High Schools"
        Else
            CType(header.FindControl("lblHeaderDescription"),Label).Text = "Colleges"
        End If
    End Sub

End Class 
