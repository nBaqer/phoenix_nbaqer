﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="AleadPriorWork.aspx.vb" Inherits="AdvWeb.AD.AD_AleadPriorWork" %>
<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" runat="Server">
    <link href="../css/AD/LeadPriorWork.css" rel="stylesheet" />
    <!-- Use this to put content -->
    <div style="overflow: scroll; height: 1024px;">
        <main id="priorWorkWrapper">
            <script id="priorGridEditor" type="text/x-kendo-template">
           <!--   Template for Edition Grid  -->
         <div id="priorGridTemplateWrapper">
          <div  class="templateLeft"> <label >Employer<span class="mandatory"> *</span></label> </div>
          <div  class="templateRight"> 
                <input id="priorEmployer" required data-bind="value:Employer" type="text" class="k-textbox" maxlength="50" /> </div>
          <div class="clearfix"></div>
          <div  class="templateLeft"> <label >Job Title<span class="mandatory"> *</span></label> </div>
          <div  class="templateRight"> <input required data-bind="value:JobTitle" type="text" class="k-textbox"  maxlength="50"/> </div>
          <div class="clearfix"></div>
          <div  class="templateLeft"> <label >School Job Title</label> </div>
            <div id="schoolJobTitlePlaceHolder" class="templateRight"> 
              <input name="SchoolJobTitle" 
				data-bind="value:SchoolJobTitle"
 				data-value-field="ID" 
				data-text-field="Description" 
				data-source= "newPriorSchoolJobTitleDataSource"
				data-role="dropdownlist"
                data-option-label = "Select"
            />
          </div>
          <div class="clearfix"></div>
          <div  class="templateLeft"> <label >Job Status</label></div>
          <div  class="templateRight"> 
               <input name="JobStatus" 
				data-bind="value:JobStatus" 
				data-value-field="ID" 
				data-text-field="Description" 
				data-role="dropdownlist"
                data-option-label = "Select"
            />
          </div>
          <div class="clearfix"></div>
          <div  class="templateLeft"> <label >Start Date</label> </div>
          <div  class="templateRight"> 
             <input name="dpPriorStartDate" 
                data-bind="value:StartDate" 
                data-validation="date"
                type="date" 
                data-role="datepicker"
            /> 
           </div>
          <div class="clearfix"></div>
          <div  class="templateLeft"> <label >End Date</label> </div>
          <div  class="templateRight"> 
           <input 
           data-bind="value:EndDate" 
           data-validation="date"
           type="date" 
           data-role="datepicker"/> 
           </div>
          <div class="clearfix"></div>
          <div  class="templateLeft"> <label >Address 1 </label> </div>
          <div  class="templateRight"> 
           <input data-bind="value:AddressOutputModel.Address1" 
                type="text" class="k-textbox"  
                maxlength="200" /> 
          </div>
          <div class="templateExtra">
                <label>
                   <input id="priorInternational" type="checkbox"
                   data-bind="value:AddressOutputModel.IsInternational" 
                /> Int'l
                </label>
          </div>
          <div class="clearfix"></div>

         <div  class="templateLeft"> <label >Address 2</label> </div>
          <div  class="templateRight"> <input data-bind="value:AddressOutputModel.Address2" type="text" class="k-textbox"  maxlength="100" /> </div>
          <div class="clearfix"></div>
          <div  class="templateLeft"> <label >City</label> </div>
          <div  class="templateRight"> 
           <input data-bind="value:AddressOutputModel.City" type="text" class="k-textbox"  maxlength="50" /> </div>
          <div class="clearfix"></div>
          <div  class="templateLeft"> <label >State</label> </div>
          <div  class="templateRight"> 
                    <input id="StateInternational"  name="StateInternational" 
                     maxlength="50" 
                     data-bind="value:AddressOutputModel.StateInternational" 
                     type="text" 
                     class="k-textbox toggle-inv " /> 
                <div id="priorStateUsa" class="toggle-vis" >
                <input name="AddressOutputModel" 
                    data-bind="value:AddressOutputModel.State" 
				    data-value-field="ID" 
				    data-text-field="Description" 
				    data-role="dropdownlist"
                    data-option-label = "Select"
                 />
                </div>
          </div>
          <div class="clearfix"></div>
          <div  class="templateLeft"> <label >Country</label> </div>
          <div  class="templateRight"> 
          <input name="newPriorCountry" 
				data-bind="value:AddressOutputModel.Country" 
				data-value-field="ID" 
				data-text-field="Description" 
				data-role="dropdownlist"
                data-option-label = "Select"
            />
          </div>
          <div class="clearfix"></div>
          <div  class="templateLeft"> <label >Zip</label> </div>
          <div  class="templateRight"> 
           <input id=priorZipCode data-bind="value:AddressOutputModel.ZipCode" type="text" data-validation="zip" /> </div>
          <div class="clearfix"></div>
           <hr/>
  
       </div>      
       
 </div
            </script>

            <div id="priorFirstPanel" class="leadFirstPanel">
                <label class="leadTitles" style="font-weight: bold !important; vertical-align: top; font-size: 11pt !important;">Prior Work</label>
                <a id="priorPlusImage" href="javascript:void(0);">
                    <span class="k-icon k-i-plus-circle font-green"></span>
                </a>
            </div>
            <section id="MainSection" class="marco">
                <div id="priorWorkGrid"></div>
            </section>
            <section id="WorkDetailSection" class="marco" style="display:none">
                <label class="leadTitles">Work Details</label>
                <div id="priorDetailContainer" class="workDetailsFrame">
                    <label id="priorSelecteddEntity" class="leadTitles">Selected Entity</label>
                    <br/> <br/>
                   <%-- <div id="priorContactPanel" class="leadFirstPanel"> </div>--%>
                        <label class="leadTitles">Contacts</label>
                        <img id="priorContactPlusImage" src="../images/icon/Add.gif" alt="Add Prior Work" />
                   <br/> <br/>
                    <div id="priorWorkContactGrid" class="grid80"></div>
                     <br/>
                   <p><label id="lpriorJobResponsabilities" class="leadTitles">Job Responsibilities</label></p>
                   
                    <input id="priorJobResponsabilities" class="k-textbox priorWorkTextBox" type="text" />
                    <button id="priorJobResponsabilitiesSave" type="button" class="k-button largeTextbutton" title="Save"></button>
                    <%--<button id="priorJobResponsabilitiesCleanSave" type="button" class="k-button largeTextbutton"></button>--%>
                    <br />
                    <p><label id="lpriorComments" class="leadTitles">Comments</label></p>
                    <input id="priorComment" class="k-textbox priorWorkTextBox" type="text"/>
                    <button id="priorCommentSave" type="button" class="k-button largeTextbutton" title="Save"></button>
                    <%--<button id="priorCommentCleanSave"   type="button" class="k-button largeTextbutton"></button>--%>
                </div>
            </section>
                <div id="dvCustomBar" style="display: none" class="leadFirstPanel"><label class="leadTitles">Custom</label></div>
                <div id="dvCustomFields" class="marco workDetailsFrame" style="display: none"></div>
                <div id ="dvCustomSave" ><button id="sdfSave" type="button" class="k-button largeTextbutton" style="float: right; margin-right: 9px; display: none;" title="Save">Save</button></div>
        </main>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var leadElement = document.getElementById('leadId');
            var leadId = leadElement.value;
            window.manager = new AD.LeadPriorWorks(leadId);
            window.manager.initialization();
        });

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

