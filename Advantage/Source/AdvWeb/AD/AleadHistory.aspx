﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="AleadHistory.aspx.vb" Inherits="AdvWeb.AD.AdALeadHistory" %>
<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" Runat="Server">
       <!-- Use this to put content -->
    <link href="../css/AD/LeadHistory.css" rel="stylesheet" />
    <div id="historyWrapper">
        <div id="historyFirstPanel" class="flex-container">
            <div id="historyItem1" class="flex-item" style="left: 0">
                <label class="leadTitles" style="font-weight: bold !important; vertical-align: top; font-size: 11pt !important;">History</label>
            </div>
            <div id="historyItem2" class="flex-item">
                <label style="position: relative; top: 7px; margin-right: 10px; font-weight: bold">Module</label>
                <div id="historyModulesDdl"></div>
            </div>
            <div id="historyItem3" class="flex-item">
                <button id="historyPrintbutton" type="button" class="k-button" style="font-size: 12px; font-weight: bold; width:75px">Print</button>
            </div>
        </div>

        <div id="historyMainPanel" style="margin: 10px">
            <div id="historyGrid"></div>
        </div>
        <div id="Edit"></div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
           var leadElement = document.getElementById('leadId');
           var leadid = leadElement.value;
           var manager = new AD.LeadHistory(leadid, "AD");
            manager.initOperation(); 
        });
    </script>
      <script type="text/x-kendo-template" id="historyTypeGridStyleTemplate">
       #
            #: Type # 
       #
    </script>
    <style type="text/css">
        .email-link {
                text-decoration: underline !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

