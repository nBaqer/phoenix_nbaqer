<%@ page title="" language="vb" MasterPageFile="~/NewSite.master" autoeventwireup="false" codefile="SetupQuickLeadFields.aspx.vb" inherits="SetupQuickLeadFields" %>
<%@ mastertype virtualpath="~/NewSite.master"%> 
<asp:content id="content1" contentplaceholderid="additional_head" runat="server">
<script language="javascript" src="../js/checkall.js" type="text/javascript"/>
<script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="98%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>

                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="boxContainer">
                                    <h3>
                                        <asp:Label ID="headerTitle" runat="server"></asp:Label>
                                    </h3>
                                    <!-- end top menu (save,new,reset,delete,history)-->
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td class="detailsframe">
                                                <!--begin content table-->
                                                <table width="800px" cellpadding="0" cellspacing="0" class="contenttable" align="center">
                                                    <asp:Label ID="lblError" CssClass="label" runat="server" Visible="false" ForeColor="red"></asp:Label>
                                                    <tr>
                                                        <td class="threecolumnheader1">
                                                            <asp:Label ID="lblAvailableFields" runat="server" CssClass="labelbold" Font-Bold="True">Available Fields</asp:Label></td>
                                                        <td class="threecolumnspacer1"></td>
                                                        <td class="threecolumnheader1">
                                                            <asp:Label ID="lblSelectedFields" runat="server" CssClass="labelbold" Font-Bold="True">Selected Fields</asp:Label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="threecolumncontent">
                                                            <asp:ListBox ID="lstAvailableFields" runat="server" CssClass="listboxes" Rows="20"></asp:ListBox></td>
                                                        <td class="threecolumnbuttons">
                                                            <div style="margin: 10px;">
                                                                <telerik:RadButton ID="btnAdd" Text="Add >" runat="server" CssClass="buttons1" Width="100px"></telerik:RadButton>
                                                            </div>
                                                            <div style="margin: 10px;">
                                                                <telerik:RadButton ID="btnRemove" Text="< Remove" runat="server" CssClass="buttons1" Width="100px"></telerik:RadButton>
                                                            </div>
                                                        </td>
                                                        <td class="threecolumncontent">
                                                            <asp:ListBox ID="lstSelectedFields" runat="server" CssClass="listboxes" Rows="20"></asp:ListBox></td>
                                                    </tr>
                                                </table>
                                                <!--end content table-->
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <!--end table content-->
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:TextBox ID="txtSkillGrpId" runat="server" Visible="False"></asp:TextBox>
        <asp:Button ID="btncheck" runat="server" Text="Check" Visible="false" />
        <input type="hidden" name="txtcheck" runat="server" id="Hidden1">
        <asp:TextBox ID="txtStudentId" runat="server" Visible="False" />
        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
    </div>

</asp:Content>


