﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ReversalReason.aspx.vb" Inherits="AD_ReversalReason" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript">
        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow)
                oWindow = window.RadWindow; //Will work in Moz in all cases, including clasic dialog       
            else if (window.frameElement.radWindow)
                oWindow = window.frameElement.radWindow; //IE (and Moz as well)       
            return oWindow;
        }
        function Close() {
            Page_ClientValidate("Reason");
            if (Page_IsValid) {
                GetRadWindow().Close();
                GetRadWindow().BrowserWindow.location.reload();
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lblReason" runat="server" style="font-family:Arial; font-size:12px">Adjustment Reason <font color="red">*</font></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="txtReverseReason" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="reqReason" runat="server" ControlToValidate="txtReverseReason" ErrorMessage="Adjustment Reason is required" ValidationGroup="Reason" 
                        style="font-family:Arial; font-size:12px; color: #b71c1c;"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClientClick="Close();" style=" font-family:Arial; font-size:12px" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
