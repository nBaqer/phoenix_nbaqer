<%@ Page Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    Inherits="SourceAdv" CodeFile="SourceAdv.aspx.vb" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%"
            Orientation="vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized"
            Style="overflow: auto;">
            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label>
                                    </td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server"
                                            RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:datalist id="dlstSourceAdv" runat="server" DataKeyField="SourceAdvId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server"
                                            Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "inactive" %>'
                                            CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# ctype(container.dataitem("status"), string).tostring.tolower = "active" %>'
                                            CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                       <asp:LinkButton text='<%# Container.DataItem("SourceAdvDescrip")%>' Runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("SourceAdvId")%>' ID="Linkbutton1" CausesValidation="False" />
                                    </ItemTemplate>
                                </asp:DataList></div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both"
                orientation="horizontaltop">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <!-- begin top menu (save,new,reset,delete,history)-->
                    <tr>
                        <td class="menuframe" align="right">
                            <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false">
                            </asp:Button>
                            <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false">
                            </asp:Button>
                        </td>
                        
                    </tr>
                </table>
                <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="detailsframe">
                            <!-- begin content table-->
                            <asp:Panel ID="pnlrhs" runat="server">
                                <div class="scrollright2">
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSourceAdvCode" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtSourceAdvCode" runat="server" CssClass="Textbox" Width="85%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlStatusId" runat="server" >
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSourceAdvDescrip" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:TextBox ID="txtSourceAdvDescrip" runat="server" CssClass="Textbox" Width="85%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCampGrpId" CssClass="Label" runat="server"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlCampGrpId" runat="server" >
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblSourceTypeId" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlSourceTypeId" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblStartDate" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" style="text-align: left">
                                                 <telerik:RadDatePicker ID="txtStartdate" MinDate="1/1/1945" runat="server" >
                                                 </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblEndDate" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" style="text-align: left">
                                                  <telerik:RadDatePicker ID="txtEndDate" MinDate="1/1/1945" runat="server" >
                                                  </telerik:RadDatePicker>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblCost" runat="server" CssClass="Label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell" nowrap>
                                                <asp:TextBox ID="txtCost" runat="server" CssClass="TextBox"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:Label ID="lblAdvIntervalId" runat="server" CssClass="label"></asp:Label>
                                            </td>
                                            <td class="twocolumncontentcell">
                                                <asp:DropDownList ID="ddlAdvIntervalId" runat="server">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="twocolumnlabelcell">
                                                <asp:TextBox ID="txtSourceAdvId" runat="server" CssClass="Textbox" Visible="false"></asp:TextBox>
                                            </td>
                                            <td class="twocolumncontentcell">
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="donothing"></asp:TextBox>
                                    <asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="donothing"></asp:TextBox><!--end table content--></div>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>
</asp:Content>
