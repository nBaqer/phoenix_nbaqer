Imports System.Diagnostics
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections

Partial Class AdmissionReqsByDates
    Inherits BasePage
    Protected WithEvents btnhistory As Button
    Protected WithEvents txtReqGrpId As TextBox
    Protected WithEvents chkIsMandatoryReqGrp As CheckBox
    Dim campusId As String
    Protected WithEvents allDataset As DataSet
    Protected WithEvents dataGridTable As DataTable
    Protected WithEvents dataListTable As DataTable
    Protected WithEvents leadGroupTable As DataTable
    Protected WithEvents dropDownListTable As DataTable
    Dim intTestCount As Integer
    Dim LeadGrpFacade As New LeadFacade

#Region " Web Form Designer Generated Code "

    ' This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        ' CODEGEN: This method call is required by the Web Form Designer
        ' Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        Dim objCommon As New CommonUtilities
        Dim resourceId As Integer
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        ViewState("CampusId") = campusId

        Dim advantageUserState As User = AdvantageSession.UserState
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, CType(resourceId, String), campusId)

        intTestCount = LeadGrpFacade.GetLeadGrpCount()
        'Check if this page still exists in the menu while switching campus
        Dim boolPageStillPartofMenu As Boolean = False
        If Me.Master.IsSwitchedCampus Then

            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        'End If
        'Try
        If Not Page.IsPostBack Then
            ViewState("MODE") = "NEW"
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

            Session("adReqIdByDate") = Nothing

            '   build dropdownlists
            BuildDropDownLists()

            '   get allDataset from the DB
            With New AdReqsFacade
                allDataset = .GetRequirementsAndEffectiveDatesDS()
            End With

            '   save it on the session
            Session("RequirementsAndDatesDs") = allDataset

            '   initialize buttons
            InitButtonsForLoad()

            'GenerateControlsNoLeadGrpSelected(pnlLeadGroups)

            txtadReqId.Text = Guid.NewGuid.ToString
            Session("adReqIdByDate") = txtadReqId.Text
            BindDataGridList()
        Else
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
        End If



        '   create Dataset and Tables
        CreateDatasetAndTables()



        '   the first time we have to bind an empty datagrid
        If Not IsPostBack Then PrepareForNewData()

        'By Default Select Admissions Module
        ' ddlModuleId.SelectedIndex = ddlModuleId.Items.IndexOf(ddlModuleId.Items.FindByText("Admissions"))
        ddlModuleId.Enabled = True
        'test by theresa
    End Sub

    Private Sub CreateDatasetAndTables()
        '   Get dataGrid dataset from the session and create tables
        If Not Session("RequirementsAndDatesDs") Is Nothing Then
            allDataset = CType(Session("RequirementsAndDatesDs"), DataSet)
            dataListTable = allDataset.Tables("Requirements")
            dataGridTable = allDataset.Tables("EffectiveDates")
            'LeadGroupTable = allDataset.Tables("LeadGroupsByEffectiveDates")
        End If

    End Sub
    Private Sub PrepareForNewData()
        '   bind AdReqGrps datalist
        BindDataList()

        '   save ReqGrpId in session
        Session("adReqIdByDate") = Nothing

        '   bind DataGrid
        BindDataGrid()

        '   bind an empty new AdReqGrpInfo
        BindAdReqGrpData(New AdReqGrpInfo)

        '   initialize buttons
        InitButtonsForLoad()

        dgrdLeadGroups.ShowFooter = True
    End Sub
    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter, sortExpression As String

        Select Case radStatus.SelectedIndex
            Case 0
                rowFilter = "Status=1"
                sortExpression = "Descrip"
            Case 1
                rowFilter = "Status=0"
                sortExpression = "Descrip"
            Case Else
                rowFilter = Nothing
                sortExpression = "Status DESC, Descrip "
        End Select


        If Not ddlModule.SelectedItem.Text = "Select" Then
            If rowFilter Is Nothing Then
                rowFilter = " ModuleID= " + ddlModule.SelectedValue
            Else
                rowFilter = rowFilter + " AND ModuleID= " + ddlModule.SelectedValue
            End If
        End If
        If dllreqtype.SelectedValue = "1" Then
            rowFilter = rowFilter + " AND ReqforEnrollment=1 "
        ElseIf dllreqtype.SelectedValue = "2" Then
            rowFilter = rowFilter + " AND ReqforFinancialAid=1 "
        ElseIf dllreqtype.SelectedValue = "3" Then
            rowFilter = rowFilter + " AND ReqforGraduation=1 "
        End If

        Dim originalDataSet = dataListTable.DataSet()


        Dim UserId = AdvantageSession.UserState.UserId.ToString
        Dim resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        Dim campusId = AdvantageSession.UserState.CampusId.ToString
        Dim facadeCamp As New CampusGroupsFacade
        Dim updatedDataSet = facadeCamp.GetCampusGroupsItems(UserId, resourceId, campusId, originalDataSet)
        Dim updatedDataTable = updatedDataSet.Tables(0)

        '   bind dlstAdmissionReqGroups datalist
        dlstAdmissionReqGroups.DataSource = New DataView(updatedDataTable, rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstAdmissionReqGroups.DataBind()

        If Not Session("adReqIdByDate") Is Nothing Then
            '   set Style to Selected Item
            ' CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, Session("adReqIdByDate"), ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Session("adReqIdByDate"))
        End If
    End Sub
    Private Sub BuildModuleDDL()
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddlModuleId
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleId"
            .DataSource = DocumentStatus.GetAllDocumentModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
        End With
        With ddlModule
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleId"
            .DataSource = DocumentStatus.GetAllDocumentModules()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
        End With
        'With ddlModuleId
        '    .SelectedIndex = 
        '    .Enabled = False
        'End With
    End Sub
    Private Sub BuildReqTypesDDL()
        'Bind the Document DropDownList
        Dim DocumentStatus As New StudentDocsFacade
        With ddladReqTypeId
            .DataTextField = "Descrip"
            .DataValueField = "adReqTypeId"
            .DataSource = DocumentStatus.GetAllReqTypes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampGrpDDL()
        BuildModuleDDL()
        BuildReqTypesDDL()
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
        End With
    End Sub
    Private Sub BuildCampGrpDDL()
        '   bind the Campus Group DropDownList
        Dim resourceId As Integer
        Dim UserId = AdvantageSession.UserState.UserId.ToString
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        Dim facade As New CampusGroupsFacade
        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = facade.GetAllIndividualCampusGroupsByUserAndResourceID(UserId, resourceId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub GetReqGrpsDS()
        '   get allDataset from the DB
        With New AdReqsFacade
            allDataset = .GetRequirementsAndEffectiveDatesDS()
        End With

        '   save it on the session
        Session("RequirementsAndDatesDs") = allDataset

        '   create Dataset and Tables
        CreateDatasetAndTables()
    End Sub
    Private Sub dlstAdmissionReqGroups_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstAdmissionReqGroups.ItemCommand
        '   this portion of code added to refresh data from the database
        GetReqGrpsDS()

        '   save adReqId in session
        Dim strId As String = dlstAdmissionReqGroups.DataKeys(e.Item.ItemIndex).ToString()
        Session("adReqIdByDate") = strId

        '   get the row with the data to be displayed
        Dim row() As DataRow = dataListTable.Select("adReqId=" + "'" + CType(Session("adReqIdByDate"), String) + "'")

        '   populate controls with row data
        txtadReqId.Text = CType(row(0)("adReqId"), Guid).ToString
        txtCode.Text = row(0)("Code")
        txtDescrip.Text = row(0)("Descrip")
        ddlStatusId.SelectedValue = CType(row(0)("StatusId"), Guid).ToString
        ddlCampGrpId.SelectedValue = CType(row(0)("CampGrpId"), Guid).ToString
        ddladReqTypeId.SelectedValue = row(0)("adReqTypeId")
        'chkAppliesToAll.Checked = row(0)("AppliesToAll")
        txtModUser.Text = CType(row(0)("ModUser"), String)
        txtModDate.Text = row(0)("ModDate").ToString
        ddlModuleId.SelectedValue = CType(row(0)("ModuleId"), String)

        If Not row(0)("ReqforEnrollment") Is DBNull.Value And row(0)("ReqforEnrollment") = True Then
            rbtnrequirement.SelectedValue = 1
        End If
        If Not row(0)("ReqforFinancialAid") Is DBNull.Value And row(0)("ReqforFinancialAid") = True Then
            rbtnrequirement.SelectedValue = 2
        End If
        If Not row(0)("ReqforGraduation") Is DBNull.Value And row(0)("ReqforGraduation") = True Then
            rbtnrequirement.SelectedValue = 3
            'ElseIf Not row(0)("ReqforEnrollment") Is DBNull.Value And row(0)("ReqforEnrollment") = False And Not row(0)("ReqforFinancialAid") Is DBNull.Value And row(0)("ReqforFinancialAid") = False And Not row(0)("ReqforGraduation") Is DBNull.Value And row(0)("ReqforGraduation") = False Then
            '    rbtnrequirement.SelectedValue = 4
        End If

        '   bind DataGrid
        dgrdLeadGroups.EditItemIndex = -1
        BindDataGrid()

        '   show footer
        dgrdLeadGroups.ShowFooter = True

        '   disable ReqGrpLeadGrp link button
        dgrdLeadGroups.Enabled = True

        '   initialize buttons
        InitButtonsForEdit()

        Session("Requirement") = txtDescrip.Text

        'BindDataGridList()
        ClearMinScoreIfReqIsDocument()

        '   set Style to Selected Item
        '  CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, e.CommandArgument, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, CType(e.CommandArgument, String))
    End Sub

    Private Sub BindAdReqGrpData(ByVal AdReqGrp As AdReqGrpInfo)
        With AdReqGrp
            chkIsInDB.Checked = .IsInDB
            txtadReqId.Text = .ReqGrpId
            txtCode.Text = .Code
            txtDescrip.Text = .Descrip
            ddlStatusId.SelectedValue = .StatusId
            ddlCampGrpId.SelectedValue = .CampGrpId
            Try
                ddladReqTypeId.SelectedValue = .adReqTypeId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddladReqTypeId.SelectedIndex = 0
            End Try
            'chkAppliesToAll.Checked = .IsMandatoryReqGrp
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
            'By Default Select Admissions Module
            '     ddlModuleId.SelectedIndex = ddlModuleId.Items.IndexOf(ddlModuleId.Items.FindByText("Admissions"))

        End With
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        ' Validate that exists a requeriment selected
        If rbtnrequirement.SelectedIndex = -1 Then
            DisplayErrorMessage("Please select one of the requirement options")
            Exit Sub
        End If

        '   if any of the footer fields of the datagrid is not blank.. send an error message 
        If Not AreFooterFieldsBlank() Then
            '   Display Error Message
            DisplayErrorMessage("To use ""Save"" all fields in the Lead Groups footer must be blank")
            Exit Sub
        End If

        '   if AdReqGrpId does not exist do an insert else do an update
        If Session("adReqIdByDate") Is Nothing Then
            '   do an insert
            BuildNewRowInDataList(Guid.NewGuid.ToString)

        Else
            '   do an update
            '   get the row with the data to be displayed
            Dim row() As DataRow = dataListTable.Select("adReqId=" + "'" + CType(Session("adReqIdByDate"), String) + "'")

            '   update row data
            UpdateRowData(row(0))

        End If

        '   try to update the DB and show any error message
        UpdateDB()


        '   if there were no errors then bind and set selected style to the datalist and initialize buttons 
        If Customvalidator1.IsValid Then
            '   bind datalist
            BindDataList()

            '   initialize buttons
            InitButtonsForEdit()
        End If
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, txtadReqId.Text)
    End Sub


    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        '   reset dataset to previous state. delete all changes
        allDataset.RejectChanges()
        rbtnrequirement.SelectedIndex = -1
        ddlModuleId.SelectedIndex = 0
        PrepareForNewData()

        '   initialize buttons
        InitButtonsForLoad()

        '   reset Style to Selected Item
        '  CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, Guid.Empty.ToString, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        '   get all rows to be deleted
        Dim rows() As DataRow = dataGridTable.Select("adReqId=" + "'" + CType(Session("adReqIdByDate"), String) + "'")

        '   delete all rows from dataGrid table
        Dim i As Integer
        If rows.Length > 0 Then
            For i = 0 To rows.Length - 1
                rows(i).Delete()
            Next
        End If

        '   get the row to be deleted in ReqGrpId table
        Dim row() As DataRow = dataListTable.Select("adReqId=" + "'" + CType(Session("adReqIdByDate"), String) + "'")

        '   delete row from the table
        row(0).Delete()

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   Prepare screen for new data
            ddlModuleId.SelectedIndex = 0
            rbtnrequirement.SelectedIndex = -1
            PrepareForNewData()
        End If

        '   initialize buttons
        InitButtonsForLoad()
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
            dgrdLeadGroups.Enabled = True
        Else
            btnNew.Enabled = False
            dgrdLeadGroups.Enabled = False
        End If

        btnDelete.Enabled = False
        lnkReqGrpIdDef.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
            dgrdLeadGroups.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        lnkReqGrpIdDef.Enabled = True
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub
    Private Function AreFooterFieldsBlank() As Boolean
        Dim footer As Control = dgrdLeadGroups.Controls(0).Controls(dgrdLeadGroups.Controls(0).Controls.Count - 1)

        'If ddlReqTypeId.selectedvalue = 1 then
        'minscore should not be blank

        If Not CType(footer.FindControl("txtStartDate"), TextBox).Text = "" And ddladReqTypeId.SelectedValue = 1 And Not CType(footer.FindControl("txtMinScore"), TextBox).Text = "" Then
            Return False
        ElseIf Not CType(footer.FindControl("txtStartDate"), TextBox).Text = "" And ddladReqTypeId.SelectedValue = 3 Then
            Return False
        End If
        'If Not CType(footer.FindControl("ddlFooterLeadGrpId"), DropDownList).SelectedValue = System.Guid.Empty.ToString Then Return False
        'If Not CType(footer.FindControl("txtFooterNumReqs"), TextBox).Text = "" Then Return False
        Return True
    End Function
    Private Sub BuildNewRowInDataList(ByVal theGuid As String)
        '   get a new row
        Dim newRow As DataRow = dataListTable.NewRow

        '   create a Guid for DataList if it has not been created 
        txtadReqId.Text = theGuid
        newRow("adReqId") = New Guid(theGuid)
        Session("adReqIdByDate") = theGuid

        '   update row with web controls data
        UpdateRowData(newRow)

        '   add row to the table
        newRow.Table.Rows.Add(newRow)
    End Sub
    Private Sub UpdateRowData(ByVal row As DataRow)
        '   update row data
        row("Code") = txtCode.Text
        row("Descrip") = txtDescrip.Text
        row("adReqId") = New Guid(txtadReqId.Text)
        row("StatusId") = New Guid(ddlStatusId.SelectedValue)
        row("CampGrpId") = New Guid(ddlCampGrpId.SelectedValue)
        row("ModuleId") = ddlModuleId.SelectedValue
        row("adReqTypeId") = ddladReqTypeId.SelectedValue
        'row("AppliesToAll") = chkAppliesToAll.Checked
        row("ModUser") = AdvantageSession.UserState.UserName
        row("ModDate") = Date.Parse(Date.Now.ToString)


        ''Fixed for rally case DE1035
        ''QA: Saving an admission requirement record with the first and last radio option selected is not saving correctly.
        If rbtnrequirement.SelectedValue = 1 Then
            row("ReqforEnrollment") = 1
            row("ReqforFinancialAid") = 0
            row("ReqforGraduation") = 0
        ElseIf rbtnrequirement.SelectedValue = 2 Then
            row("ReqforEnrollment") = 0
            row("ReqforFinancialAid") = 1
            row("ReqforGraduation") = 0
        ElseIf rbtnrequirement.SelectedValue = 3 Then
            row("ReqforEnrollment") = 0
            row("ReqforFinancialAid") = 0
            row("ReqforGraduation") = 1

        End If

    End Sub

    Private Sub UpdateDB()
        Dim result As String

        '   Try update DB
        With New AdReqsFacade

            '   Proceed to update Requirement Group
            result = .UpdateRequirementsandEffectiveDatesDS(allDataset)
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   Get a new version of AdReqGrps dataset because some other users may have added,
                '   updated or deleted records from the DB
                allDataset = .GetRequirementsAndEffectiveDatesDS()
                Session("RequirementsAndDatesDs") = allDataset
                CreateDatasetAndTables()
            End If
            'End If
        End With
    End Sub
    Private Sub dgrdLeadGroups_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgrdLeadGroups.ItemCommand
        '   process postbacks from the datagrid
        If rbtnrequirement.SelectedIndex = -1 Then
            DisplayErrorMessage("Please select the document required For")
            Exit Sub
        End If

        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                '   edit selected item
                dgrdLeadGroups.EditItemIndex = e.Item.ItemIndex

                '    do not show footer
                dgrdLeadGroups.ShowFooter = False


                If ddladReqTypeId.SelectedValue = 3 Then
                    Try
                        CType(e.Item.FindControl("txtEditMinScore"), TextBox).Text = ""
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                    End Try

                    Try
                        CType(e.Item.FindControl("lblMinScore"), Label).Text = ""
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                    End Try

                    Try
                        CType(e.Item.FindControl("txtMinScore"), TextBox).Text = ""
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                    End Try

                End If
                '   user hit "Update" inside the datagrid
            Case "Update"
                '   process only if the edit textboxes are nonblank and valid
                'If IsDataInEditTextboxesValid(e) And Not AreEditFieldsBlank(e) Then
                '   get the row to be updated

                Dim row() As DataRow = dataGridTable.Select("adReqEffectiveDateId=" + "'" + New Guid(CType(e.Item.FindControl("lblEditadReqEffectiveDateId"), Label).Text).ToString + "'")

                Dim dtStartDate, dtEndDate As Date
                Dim strDateMessage As String
                Dim stradReqEffectiveDateId As String
                Dim strMinScore As String

                stradReqEffectiveDateId = CType(CType(e.Item.FindControl("lblEditadReqEffectiveDateId"), Label).Text, String).ToString




                'Check If StartDate is not empty
                If Not CType(e.Item.FindControl("txtEditStartDate"), TextBox).Text = "" Then
                    Try
                        dtStartDate = CType(CType(e.Item.FindControl("txtEditStartDate"), TextBox).Text, Date)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisplayErrorMessage("Incorrect Start Date")
                        Exit Sub
                    End Try
                Else
                    strDateMessage = "The start date cannot be empty"
                    DisplayErrorMessage(strDateMessage)
                    Exit Sub
                End If

                'Check If EndDate is not empty
                If Not CType(e.Item.FindControl("txtEditEndDate"), TextBox).Text = "" Then
                    Try
                        dtEndDate = CType(CType(e.Item.FindControl("txtEditEndDate"), TextBox).Text, Date)
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisplayErrorMessage("Incorrect End Date")
                        Exit Sub
                    End Try
                    If DateDiff(DateInterval.Day, dtEnddate, dtStartDate) >= 1 Then
                        DisplayErrorMessage("The end date should be greater than the start date")
                        Exit Sub
                    End If

                End If



                'Check if adReqEffectiveDateId is not for the current row
                Dim dtRowStartDate, dtRowEndDate As Date
                Dim dr As DataRow
                Dim dtadReqEffectiveDateId As String

                Dim drCount As Integer
                Dim strStartDate As Date
                Dim strLastRecord As String = String.Empty
                Dim strEndDate As String
                Dim intCounter As Integer
                Dim iitems As DataGridItemCollection
                Dim iitem As DataGridItem
                Dim i As Integer
                Dim strPreviousStartDate As Date

                ' Save the datagrid items in a collection.
                iitems = dgrdLeadGroups.Items

                'Get The Start Date of the Currentky Edited Row
                If Not CType(e.Item.FindControl("txtEditStartDate"), TextBox).Text = "" Then
                    strStartDate = CType(CType(e.Item.FindControl("txtEditStartDate"), TextBox).Text, Date)
                End If

                'Check If the StartDate of the currently edited row's start date is greater than 
                'other start dates
                Try
                    'Loop thru the collection to get the Array Size
                    For i = 0 To iitems.Count - 1
                        iitem = iitems.Item(i)
                        Try
                            strPreviousStartDate = CType(CType(iitem.FindControl("lblStartDate"), Label).Text, Date)
                        Catch ex As Exception
                         	Dim exTracker = new AdvApplicationInsightsInitializer()
                        	exTracker.TrackExceptionWrapper(ex)

                            strStartDate = CType(CType(iitem.FindControl("txtEditStartDate"), TextBox).Text, Date)
                        End Try
                        If DateDiff(DateInterval.Day, strPreviousStartDate, strStartDate) >= 1 Then
                            strLastRecord = "Yes"
                        ElseIf DateDiff(DateInterval.Day, strPreviousStartDate, strStartDate) = 0 Then
                            strLastRecord = "Yes"
                        Else
                            strLastRecord = "No"
                        End If
                    Next
                Finally
                End Try

                For Each dr In dataGridTable.Select("adReqId=" + "'" + CType(Session("adReqIdByDate"), String) + "'")
                    drCount += 1
                Next

                For Each dr In dataGridTable.Select("adReqId=" + "'" + CType(Session("adReqIdByDate"), String) + "'")
                    dtRowStartDate = CType(dr("StartDate"), Date)
                    dtadReqEffectiveDateId = CType(dr("adReqEffectiveDateId"), Guid).ToString
                    intCounter += 1
                    'If condt
                    If Not dr("EndDate") Is DBNull.Value And Not CType(e.Item.FindControl("txtEditEndDate"), TextBox).Text = "" Then
                        dtRowEndDate = dr("EndDate")
                    Else
                        If Not CType(e.Item.FindControl("txtEditEndDate"), TextBox).Text = "" Then
                            dtRowEndDate = CType(CType(e.Item.FindControl("txtEditEndDate"), TextBox).Text, Date)
                            dtRowStartDate = CType(CType(e.Item.FindControl("txtEditStartDate"), TextBox).Text, Date)
                            strEndDate = "NA"
                        Else
                            strEndDate = ""
                            dtRowStartDate = CType(CType(e.Item.FindControl("txtEditStartDate"), TextBox).Text, Date)
                        End If
                        If strEndDate = "" And Not drCount = 1 And Not strLastRecord = "Yes" Then
                            strDateMessage = ""
                            strDateMessage &= "A new effective date can be added only if the previous end date is filled up" & vbLf
                            strDateMessage &= "Please put an end date for the date range starting from " & dtRowStartDate & vbLf
                            DisplayErrorMessage(strDateMessage)
                            Exit Sub
                        End If
                    End If

                    If Not Trim(stradReqEffectiveDateId) = Trim(dtadReqEffectiveDateId) Then
                        If Not dr("EndDate") Is DBNull.Value Then
                            strDateMessage = ""
                            If dtStartDate >= dtRowStartDate And dtStartDate <= dtRowEndDate Then
                                strDateMessage &= "Start date should not overlap with an existing date range" & vbLf
                                strDateMessage &= "The start date overlaps with the date range " & dtRowStartDate & " - " & dtRowEndDate & vbLf
                            End If
                            If dtEndDate >= dtRowStartDate And dtEndDate <= dtRowEndDate Then
                                strDateMessage &= "End date should not overlap with an existing date range" & vbLf
                                strDateMessage &= "The end date overlaps with the date range " & dtRowStartDate & " - " & dtRowEndDate & vbLf
                            End If
                            If Not strDateMessage = "" Then
                                DisplayErrorMessage(strDateMessage)
                                Exit Sub
                            End If
                        End If
                    End If
                Next

                If ddladReqTypeId.SelectedValue = 1 And CType(e.Item.FindControl("txtEditMinScore"), TextBox).Text = "" Then
                    DisplayErrorMessage("Min Score is required for " & txtDescrip.Text)
                    Exit Sub
                End If

                strMinScore = CType(e.Item.FindControl("txtEditMinScore"), TextBox).Text
                Dim myPos As Integer
                If ddladReqTypeId.SelectedValue = 1 And CType(e.Item.FindControl("txtEditMinScore"), TextBox).Text <> "" Then
                    Try
                        myPos = CInt(CType(e.Item.FindControl("txtEditMinScore"), TextBox).Text)
                        If Not myPos >= 1 Then
                            DisplayErrorMessage("Min Score is invalid")
                            Exit Sub
                        End If
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisplayErrorMessage("Characters not allowed for Min Score")
                        Exit Sub
                    End Try
                End If

                '   fill new values in the row
                If Not CType(e.Item.FindControl("txtEditStartDate"), TextBox).Text = "" Then row(0)("StartDate") = CDate(CType(e.Item.FindControl("txtEditStartDate"), TextBox).Text).ToShortDateString
                If Not CType(e.Item.FindControl("txtEditEndDate"), TextBox).Text = "" Then row(0)("EndDate") = CDate(CType(e.Item.FindControl("txtEditEndDate"), TextBox).Text).ToShortDateString Else row(0)("EndDate") = DBNull.Value

                '' Code Changes by Kamalesh Ahuja on 13 October to Fix Rally Issue id DE1032
                ''If Not CType(e.Item.FindControl("txtEditMinScore"), TextBox).Text = "" Then row(0)("MinScore") = (CType(e.Item.FindControl("txtEditMinScore"), TextBox).Text) Else row(0)("MinScore") = ""
                If Not CType(e.Item.FindControl("txtEditMinScore"), TextBox).Text = "" Then row(0)("MinScore") = (CType(e.Item.FindControl("txtEditMinScore"), TextBox).Text) Else row(0)("MinScore") = "0.00"
                '''''''''
                If CType(e.Item.FindControl("chkEditMandatoryRequirement"), CheckBox).Checked = True Then row(0)("MandatoryRequirement") = 1 Else row(0)("MandatoryRequirement") = 0
                If Not CType(e.Item.FindControl("txtEditadReqId"), TextBox).Text = "" Then row(0)("adReqId") = New Guid(CType(e.Item.FindControl("txtEditadReqId"), TextBox).Text)
                If Not CType(e.Item.FindControl("lblEditadReqEffectiveDateId"), Label).Text = "" Then row(0)("adReqEffectiveDateId") = New Guid(CType(e.Item.FindControl("lblEditadReqEffectiveDateId"), Label).Text)
                If Not CType(e.Item.FindControl("txtEditValidFor"), TextBox).Text = "" Then row(0)("ValidDays") = Integer.Parse((CType(e.Item.FindControl("txtEditValidFor"), TextBox).Text)) Else row(0)("ValidDays") = DBNull.Value

                If ddladReqTypeId.SelectedValue = 3 Then
                    '' Code Changes by Kamalesh Ahuja on 13 October to Fix Rally Issue id DE1032
                    ''row(0)("MinScore") = ""
                    row(0)("MinScore") = "0.00"
                End If

                row(0)("ModUser") = AdvantageSession.UserState.UserName
                row(0)("ModDate") = Date.Parse(Date.Now.ToString)

                '   no record is selected
                dgrdLeadGroups.EditItemIndex = -1

                '   show footer
                dgrdLeadGroups.ShowFooter = True

                '   Bind DataList
                BindDataList()
                'Else
                '    DisplayErrorMessage("Invalid data during the edit of an existing row of Lead Groups")
                '    Exit Sub
                ' End If
            Case "SetupLead"
                Dim strReqs As String = txtDescrip.Text
                Dim stradReqEffectiveDateId As String = CType(e.Item.FindControl("lbladReqEffectiveDateId"), Label).Text
                Dim strStartDate As String = CType(e.Item.FindControl("lblStartDate"), Label).Text
                Dim strEndDate As String = CType(e.Item.FindControl("lblEndDate"), Label).Text
                '  Dim boolMandatoryRequirement As Boolean

                '   setup the properties of the new window
                Dim sb As New StringBuilder

                'Append Campus Id
                sb.Append("&campusid=" + campusId)

                '   append ReqGrpId to the querystring
                sb.Append("&adReqEffectiveDateId=" + stradReqEffectiveDateId)
                '   append ReqGrpId Description to the querystring

                sb.Append("&StartDate=" + strStartDate)

                sb.Append("&EndDate=" + strEndDate)

                If CType(e.Item.FindControl("chkMandatoryRequirement"), CheckBox).Checked = True Then
                    sb.Append("&MandatoryRequirement=True")
                Else
                    sb.Append("&MandatoryRequirement=False")
                End If

                sb.Append("&Requirement=" + strReqs)

                Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsAdReqs
                Dim name As String = "AdmReqSummary1"
                Dim url As String = "../AD/AssignLeadGroups.aspx?resid=283&mod=AD" + sb.ToString
                CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
                '   user hit "Cancel"
            Case "Cancel"
                '   set no record selected
                dgrdLeadGroups.EditItemIndex = -1

                '   show footer
                dgrdLeadGroups.ShowFooter = True


                '   user hit "Delete" inside the datagrid
            Case "Delete"
                '   get the row to be deleted
                Dim row() As DataRow = dataGridTable.Select("adReqEffectiveDateId=" + "'" + CType(e.Item.FindControl("lblEditadReqEffectiveDateId"), Label).Text + "'")

                '   delete row 
                row(0).Delete()

                '   set no record selected
                dgrdLeadGroups.EditItemIndex = -1

                '   show footer
                dgrdLeadGroups.ShowFooter = True


            Case "AddNewRow"
                '   process only if the footer textboxes are nonblank and valid
                ' If IsDataInFooterTextboxesValid(e) And Not AreFooterFieldsBlank() Then
                '   If the dataList item doesn't have an Id assigned.. create one and assign it.
                Dim strMinScore As String

                If Session("adReqIdByDate") Is Nothing Then
                    '   build a new row in dataList table
                    BuildNewRowInDataList(Guid.NewGuid.ToString)
                    '   bind datalist
                    BindDataList()
                End If

                ' ClearMinScoreIfReqIsDocument()

                'Dim intCheckselected As Integer = checkItemsSelected()

                'If intCheckselected = 0 And Not CType(e.Item.FindControl("chkFooterMandatoryRequirement"), CheckBox).Checked Then
                '    DisplayErrorMessage("The Lead Group was not selected")
                '    Exit Sub
                'End If
                '   get a new row from the dataGridTable
                Dim newRow As DataRow = dataGridTable.NewRow

                Dim adReqEffectiveDateId As Guid = New Guid(Guid.NewGuid.ToString)
                Dim strDateMessage As String
                Dim dtStartDate As Date

                'Check If StartDate is not empty
                If Not CType(e.Item.FindControl("txtStartDate"), TextBox).Text = "" Then
                    dtStartDate = CType(CType(e.Item.FindControl("txtStartDate"), TextBox).Text, Date)
                Else
                    strDateMessage = "The start date cannot be empty"
                    DisplayErrorMessage(strDateMessage)
                    Exit Sub
                End If

                'Check If EndDate is not empty
                If Not CType(e.Item.FindControl("txtEndDate"), TextBox).Text = "" Then
                    Dim dtEnddate As Date = CType(CType(e.Item.FindControl("txtEndDate"), TextBox).Text, Date)
                    If DateDiff(DateInterval.Day, dtEnddate, dtStartDate) >= 1 Then
                        DisplayErrorMessage("The end date should be greater than the start date")
                        Exit Sub
                    End If
                End If

                Dim dtRowStartDate, dtRowEndDate As Date
                Dim dr As DataRow
                Dim dradReqEffectiveDateId As String
                Dim stradReqEffectiveDateId As String

                stradReqEffectiveDateId = CType(adReqEffectiveDateId, Guid).ToString


                For Each dr In dataGridTable.Select("adReqId=" + "'" + CType(Session("adReqIdByDate"), String) + "'")
                    dtRowStartDate = dr("StartDate")
                    dradReqEffectiveDateId = CType(dr("adReqEffectiveDateId"), Guid).ToString

                    If Not dr("EndDate") Is DBNull.Value Then
                        dtRowEndDate = dr("EndDate")
                    Else
                        strDateMessage = ""
                        strDateMessage &= "A new effective date can be added only if the previous end date is filled up" & vbLf
                        strDateMessage &= "Please put an end date for the date range starting from " & dtRowStartDate & vbLf
                        DisplayErrorMessage(strDateMessage)
                        Exit Sub
                    End If

                    If Not Trim(dradReqEffectiveDateId) = Trim(stradReqEffectiveDateId) Then
                        If dtStartDate >= dtRowStartDate And dtStartDate <= dtRowEndDate Then
                            strDateMessage = ""
                            strDateMessage &= "Start date should not be in an existing date range" & vbLf
                            strDateMessage &= "The start date overlaps with the date range " & dtRowStartDate & " - " & dtRowEndDate & vbLf
                            DisplayErrorMessage(strDateMessage)
                            Exit Sub
                        End If
                    End If
                Next

                '   fill the new row with values
                newRow("adReqEffectiveDateId") = adReqEffectiveDateId
                newRow("adReqId") = New Guid(CType(Session("adReqIdByDate"), String))
                If Not CType(e.Item.FindControl("txtStartDate"), TextBox).Text = "" Then newRow("StartDate") = CDate(CType(e.Item.FindControl("txtStartDate"), TextBox).Text).ToShortDateString
                If Not CType(e.Item.FindControl("txtEndDate"), TextBox).Text = "" Then newRow("EndDate") = CDate(CType(e.Item.FindControl("txtEndDate"), TextBox).Text).ToShortDateString

                If ddladReqTypeId.SelectedValue = 1 And CType(e.Item.FindControl("txtMinScore"), TextBox).Text = "" Then
                    DisplayErrorMessage("Min Score is required for " & txtDescrip.Text)
                    Exit Sub
                End If


                strMinScore = CType(e.Item.FindControl("txtMinScore"), TextBox).Text
                Dim myPos As Integer
                If ddladReqTypeId.SelectedValue = 1 And CType(e.Item.FindControl("txtMinScore"), TextBox).Text <> "" Then
                    Try
                        myPos = CInt(CType(e.Item.FindControl("txtMinScore"), TextBox).Text)
                        If Not myPos >= 1 Then
                            DisplayErrorMessage("Min Score is invalid")
                            Exit Sub
                        End If
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        DisplayErrorMessage("Characters not allowed for Min Score")
                        Exit Sub
                    End Try
                End If

                '' Code Changes by Kamalesh Ahuja on 13 October to Fix Rally Issue id DE1032
                ''If Not CType(e.Item.FindControl("txtMinScore"), TextBox).Text = "" Then newRow("MinScore") = CType(e.Item.FindControl("txtMinScore"), TextBox).Text Else newRow("MinScore") = ""
                If Not CType(e.Item.FindControl("txtMinScore"), TextBox).Text = "" Then newRow("MinScore") = CType(e.Item.FindControl("txtMinScore"), TextBox).Text Else newRow("MinScore") = "0.00"
                '''''''''
                If ddladReqTypeId.SelectedValue = 3 Then
                    '' Code Changes by Kamalesh Ahuja on 13 October to Fix Rally Issue id DE1032
                    ''newRow("MinScore") = ""
                    newRow("MinScore") = "0.00"
                End If

                If CType(e.Item.FindControl("chkFooterMandatoryRequirement"), CheckBox).Checked = True Then newRow("MandatoryRequirement") = 1 Else newRow("MandatoryRequirement") = 0
                newRow("ModUser") = AdvantageSession.UserState.UserName
                newRow("ModDate") = Date.Parse(Date.Now.ToString)

                If Not CType(e.Item.FindControl("txtValidFor"), TextBox).Text = "" Then newRow("ValidDays") = Integer.Parse(CType(e.Item.FindControl("txtValidFor"), TextBox).Text)

                '   add row to the table
                newRow.Table.Rows.Add(newRow)

                Session("Requirement") = txtDescrip.Text

                BindDataList()

        End Select

        '   Bind DataGrid
        BindDataGrid()
    End Sub
    Private Sub BindDataGrid()
        '   bind datagrid to dataGridTable
        dgrdLeadGroups.DataSource = New DataView(dataGridTable, "adReqId=" + "'" + CType(Session("adReqIdByDate"), String) + "'", "StartDate ASC", DataViewRowState.CurrentRows)
        dgrdLeadGroups.DataBind()
    End Sub


    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreRender
        Try
            '   if there are changes to the dataset we must confirm that the user is aware that all changes will be lost.
            If allDataset.HasChanges() Then
                'Set the Delete Button so it prompts the user for confirmation when clicked
                'btnNew.Attributes.Add("onclick", "if(confirm('All changes made to this record will be lost.  Are you sure you want a New record?')){}else{return false}")
            Else
                btnNew.Attributes.Remove("onclick")
            End If

            '   save dataset systems in session
            Session("RequirementsandDatesDs") = allDataset

            ''   disable Lead Groups datagrid when Req Grp is mandatory
            'If chkIsMandatoryReqGrp.Checked Then
            '    dgrdLeadGroups.Enabled = False
            '    dgrdLeadGroups.ShowFooter = False
            'Else
            '    dgrdLeadGroups.Enabled = True
            'End If
            'add to this list any button or link that should ignore the Confirm Exit Warning.
            Dim controlsToIgnore As New ArrayList()
            'add save button 
            controlsToIgnore.Add(btnSave)
            'Add javascript code to warn the user about non saved changes 
            CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
            BindToolTip()
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try

    End Sub

    Protected Sub dgrdLeadGroups_ItemCreated(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdLeadGroups.ItemCreated

    End Sub
    Private Sub dgrdLeadGroups_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdLeadGroups.ItemDataBound

        '' New Code Added By Vijay Ramteke On September 23, 2010 Form Mantis Id 17767
        Dim bEdit As Boolean
        Dim bAdd As Boolean
        Dim bDelete As Boolean

        bEdit = False
        bAdd = False
        bDelete = False

        If pObj.HasEdit Then
            bEdit = True
            bAdd = True
        End If
        If pObj.HasAdd Then
            bAdd = True
        End If
        If pObj.HasDelete Then
            bDelete = True
        End If

        Select Case e.Item.ItemType
            '' Code Changes by Kamalesh Ahuja on 13 October to Fix Rally Issue id DE1032
            'Case ListItemType.EditItem
            '    Dim txtEditMinScore As TextBox = CType(e.Item.FindControl("txtEditMinScore"), TextBox)
            '    txtEditMinScore.Attributes.Add("onkeypress", "return DecimalNumbers(" + txtEditMinScore.ClientID + ", 3)")
            'Case ListItemType.Footer
            '    '   bind LeadGrpId dropdownlist
            '    Dim txtMinScore As TextBox = CType(e.Item.FindControl("txtMinScore"), TextBox)
            '    txtMinScore.Attributes.Add("onkeypress", "return DecimalNumbers(" + txtMinScore.ClientID + ", 3)")
            Case ListItemType.AlternatingItem
                Dim lblMinScore As Label = CType(e.Item.FindControl("lblMinScore"), Label)
                If lblMinScore.Text = "0.00" Then
                    lblMinScore.Text = ""
                End If
                Dim lnk As LinkButton = CType(e.Item.FindControl("lnlButEdit"), LinkButton)
                If Not bEdit Then
                    lnk.Enabled = False
                    lnk.ToolTip = "do not have permission to edit this record"
                End If
            Case ListItemType.Item
                Dim lblMinScore As Label = CType(e.Item.FindControl("lblMinScore"), Label)
                If lblMinScore.Text = "0.00" Then
                    lblMinScore.Text = ""
                End If
                Dim lnk1 As LinkButton = CType(e.Item.FindControl("lnlButEdit"), LinkButton)
                If Not bEdit Then
                    lnk1.Enabled = False
                    lnk1.ToolTip = "do not have permission to edit this record"
                End If
            Case ListItemType.EditItem
                Dim txtEditMinScore As TextBox = CType(e.Item.FindControl("txtEditMinScore"), TextBox)
                If txtEditMinScore.Text = "0.00" Then
                    txtEditMinScore.Text = ""
                End If
                txtEditMinScore.Attributes.Add("onkeypress", "return DecimalNumbers(" + txtEditMinScore.ClientID + ", 3)")
                Dim lnkDelete As LinkButton = CType(e.Item.FindControl("lnkbutDelete"), LinkButton)
                If Not bDelete Then
                    lnkDelete.Enabled = False
                    lnkDelete.ToolTip = "do not have permission to delete this record"
                End If
            Case ListItemType.Footer
                '   bind LeadGrpId dropdownlist
                Dim txtMinScore As TextBox = CType(e.Item.FindControl("txtMinScore"), TextBox)
                If txtMinScore.Text = "0.00" Then
                    txtMinScore.Text = ""
                End If
                txtMinScore.Attributes.Add("onkeypress", "return DecimalNumbers(" + txtMinScore.ClientID + ", 3)")
                ''''''''''''
                Dim btnAdd As Button = CType(e.Item.FindControl("btnAddRow"), Button)
                If Not bAdd Then
                    btnAdd.Enabled = False
                    btnAdd.ToolTip = "do not have permission to add this record"
                End If
        End Select

    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radStatus.SelectedIndexChanged
        allDataset.RejectChanges()
        ddlModuleId.SelectedIndex = 0
        rbtnrequirement.SelectedIndex = -1
        PrepareForNewData()
        InitButtonsForLoad()
        '   Bind datalist
        BindDataList()
    End Sub

    'Private Sub lnkReqGrpIdDef_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkReqGrpIdDef.Click
    '    'If Page.IsValid Then
    '    '    Dim sb As New System.Text.StringBuilder

    '    '    '   append ReqGrpId to the querystring
    '    '    sb.Append("&ReqGrpId=" + txtReqGrpId.Text)
    '    '    '   append ReqGrpId Description to the querystring
    '    '    sb.Append("&ReqGrpDescrip=" + txtDescrip.Text)
    '    '    '   append IsMandatoryReqGrp to the querystring
    '    '    sb.Append("&IsMandatoryReqGrp=" + chkIsMandatoryReqGrp.Checked.ToString)

    '    '    '   setup the properties of the new window
    '    '    Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsLarge
    '    '    Dim name As String = "RequirementGrpDefs"
    '    '    Dim url As String = "../AD/" + name + ".aspx?resid=341&mod=AD&cmpid=" + campusId + sb.ToString

    '    '    '   open new window and pass parameters
    '    '    CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    '    'End If

    'End Sub
    'Private Sub chkIsMandatoryReqGrp_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkIsMandatoryReqGrp.CheckedChanged
    '    If chkIsMandatoryReqGrp.Checked Then
    '        Dim arrRows() As DataRow
    '        arrRows = dataGridTable.Select("ReqGrpId=" + "'" + CType(Session("ReqGrpId"), String) + "'")
    '        If arrRows.GetLength(0) > 0 Then
    '            For Each row As DataRow In arrRows
    '                If row.RowState <> DataRowState.Added Then
    '                    row.Delete()
    '                Else
    '                    dataGridTable.Rows.Remove(row)
    '                End If
    '            Next
    '            '   bind datagrid, so no rows are shown.
    '            BindDataGrid()
    '        End If
    '    Else
    '        '   show datagrid footer
    '        dgrdLeadGroups.ShowFooter = True
    '    End If
    'End Sub
    'Private Sub GenerateControlsNoLeadGrpSelected(ByVal pnl As Panel)
    '    ' Dim getDB As New LeadEntranceFacade
    '    'Dim intTestCount As Integer = getDB.GetControlCountAppend(LeadId)

    '    Dim ds As New DataSet
    '    Dim dynLabel As Label
    '    '  Dim dynText As TextBox
    '    Dim dynCheckRequired As CheckBox
    '    'Dim dynCheckPass As CheckBox
    '    'Dim dynTextTaken As TextBox
    '    'Dim dynTextActual As TextBox
    '    'Dim dynLabelMinScore As Label
    '    'Dim dynLabelMaxScore As Label
    '    'Dim dynTextComments As TextBox
    '    'Dim dynRegularExpValidator As RegularExpressionValidator
    '    Dim x As Integer
    '    'Dim dynNumeric As eWorld.UI.NumericBox
    '    'Dim dynReqValidator As RequiredFieldValidator
    '    'Dim dynReqValidator1 As RequiredFieldValidator
    '    'Dim PrgVerId As String

    '    Dim dynSelectRequired As CheckBox
    '    ds = LeadGrpFacade.GetAllAdmissionLeadGroups()
    '    pnl.Controls.Clear()
    '    Try
    '        pnl.Controls.Add(New LiteralControl("<table cellspacing=0 cellpadding=0 width=80%  border=1>"))
    '        'Create and Load Controls ie Textbox,Label,RangeValidator Control
    '        'and set the properties for controls and finally Load Controls
    '        If intTestCount >= 1 Then
    '            pnl.Controls.Add(New LiteralControl("<tr height=2><td></td></tr>"))
    '            For x = 0 To intTestCount - 1
    '                'Create Labels For Test
    '                dynLabel = New Label
    '                Try
    '                    With dynLabel
    '                        .ID = ds.Tables(0).Rows(x)("LeadGrpId").ToString()
    '                        .Text = ds.Tables(0).Rows(x)("Descrip").ToString()
    '                        .CssClass = "TextBoxStyle"
    '                    End With
    '                Catch ex As Exception
     '                	Dim exTracker = new AdvApplicationInsightsInitializer()
    '                	exTracker.TrackExceptionWrapper(ex)

    '                    Exit Sub
    '                End Try

    '                'Create CheckBox for Required
    '                dynSelectRequired = New CheckBox
    '                With dynSelectRequired
    '                    .ID = "radSelect" & ds.Tables(0).Rows(x)("LeadGrpId").ToString()
    '                    .CssClass = "CheckBoxStyle"
    '                    .Enabled = True
    '                End With

    '                'Create CheckBox for Required
    '                dynCheckRequired = New CheckBox
    '                With dynCheckRequired
    '                    .ID = "radR" & ds.Tables(0).Rows(x)("LeadGrpId").ToString()
    '                    .CssClass = "CheckBoxStyle"
    '                    .Enabled = True
    '                End With


    '                pnl.Controls.Add(New LiteralControl("<tr><td width=""10%"" nowrap cssclass=""DataGridHeader2"">"))
    '                pnl.Controls.Add(dynCheckRequired)
    '                pnl.Controls.Add(New LiteralControl("</td><td width=""60%"" nowrap cssclass=""DataGridHeader3"">"))
    '                pnl.Controls.Add(dynLabel)
    '                pnl.Controls.Add(New LiteralControl("</td><td width=""10%"" nowrap cssclass=""DataGridHeader2"">"))
    '                pnl.Controls.Add(dynSelectRequired)
    '                pnl.Controls.Add(New LiteralControl("</td></tr>"))
    '            Next
    '            pnl.Controls.Add(New LiteralControl("</table>"))
    '        End If
    '    Finally
    '    End Try
    'End Sub
    'Private Sub GenerateControlsEditLeadGrpSelected(ByVal pnl As Panel)
    '    '  Dim getDB As New LeadEntranceFacade
    '    Dim getReqs As New LeadFacade

    '    Dim ds As New DataSet
    '    Dim dynLabel As Label
    '    'Dim dynText As TextBox
    '    Dim dynCheckRequired As CheckBox
    '    'Dim dynCheckPass As CheckBox
    '    'Dim dynTextTaken As TextBox
    '    'Dim dynTextActual As TextBox
    '    'Dim dynLabelMinScore As Label
    '    'Dim dynLabelMaxScore As Label
    '    'Dim dynTextComments As TextBox
    '    'Dim dynRegularExpValidator As RegularExpressionValidator
    '    Dim x As Integer
    '    'Dim dynNumeric As eWorld.UI.NumericBox
    '    'Dim dynReqValidator As RequiredFieldValidator
    '    'Dim dynReqValidator1 As RequiredFieldValidator
    '    'Dim PrgVerId As String
    '    Dim ds1 As New DataSet

    '    Dim dynSelectRequired As CheckBox
    '    ds = LeadGrpFacade.GetAllAdmissionLeadGroups()
    '    ds1 = getReqs.GetAllRequiredReqsLeadGroups(txtadReqId.Text)
    '    pnl.Controls.Clear()
    '    Try
    '        pnl.Controls.Add(New LiteralControl("<table cellspacing=0 cellpadding=0 width=80%  border=1>"))
    '        'Create and Load Controls ie Textbox,Label,RangeValidator Control
    '        'and set the properties for controls and finally Load Controls
    '        If intTestCount >= 1 Then
    '            pnl.Controls.Add(New LiteralControl("<tr height=2><td></td></tr>"))
    '            For x = 0 To intTestCount - 1
    '                'Create Labels For Test
    '                dynLabel = New Label
    '                Try
    '                    With dynLabel
    '                        .ID = ds.Tables(0).Rows(x)("LeadGrpId").ToString()
    '                        .Text = ds.Tables(0).Rows(x)("Descrip").ToString()
    '                        .CssClass = "TextBoxStyle"
    '                    End With
    '                Catch ex As Exception
     '                	Dim exTracker = new AdvApplicationInsightsInitializer()
    '                	exTracker.TrackExceptionWrapper(ex)

    '                    Exit Sub
    '                End Try

    '                'Create CheckBox for Required
    '                dynSelectRequired = New CheckBox
    '                With dynSelectRequired
    '                    .ID = "radSelect" & ds.Tables(0).Rows(x)("LeadGrpId").ToString()
    '                    .CssClass = "CheckBoxStyle"
    '                    .Enabled = True
    '                    If (ds.Tables(0).Rows(x)("LeadGrpId").ToString()) = (ds1.Tables(0).Rows(x)("LeadGrpId").ToString()) Then
    '                        .Checked = True
    '                    Else
    '                        .Checked = False
    '                    End If
    '                End With

    '                'Create CheckBox for Required
    '                dynCheckRequired = New CheckBox
    '                With dynCheckRequired
    '                    .ID = "radR" & ds.Tables(0).Rows(x)("LeadGrpId").ToString()
    '                    .CssClass = "CheckBoxStyle"
    '                    .Enabled = True
    '                    If ds1.Tables(0).Rows(x)("IsRequired").ToString() = "1" Then
    '                        .Checked = True
    '                    Else
    '                        .Checked = False
    '                    End If
    '                End With

    '                pnl.Controls.Add(New LiteralControl("<tr><td width=""10%"" nowrap cssclass=""DataGridHeader2"">"))
    '                pnl.Controls.Add(dynCheckRequired)
    '                pnl.Controls.Add(New LiteralControl("</td><td width=""60%"" nowrap cssclass=""DataGridHeader3"">"))
    '                pnl.Controls.Add(dynLabel)
    '                pnl.Controls.Add(New LiteralControl("</td><td width=""10%"" nowrap cssclass=""DataGridHeader2"">"))
    '                pnl.Controls.Add(dynSelectRequired)
    '                pnl.Controls.Add(New LiteralControl("</td></tr>"))
    '            Next
    '            pnl.Controls.Add(New LiteralControl("</table>"))
    '        End If
    '    Finally
    '    End Try
    'End Sub

    Public Function GetAllTestLabels(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        ' Dim getDB As New LeadEntranceFacade
        Dim TestLabel() As String = Array.CreateInstance(GetType(String), intTestCount)
        i = 0
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(Label) Then
                TestLabel.SetValue(CType(ctl, Label).ID, i)
                i += 1
            End If
        Next
        Return TestLabel
    End Function
    Public Function GetAllRequired(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        'Dim getDB As New LeadEntranceFacade
        Dim TestRequired() As String = Array.CreateInstance(GetType(String), intTestCount)
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(CheckBox) Then
                If CType(ctl, CheckBox).Checked = True Then
                    TestRequired.SetValue("Yes", i)
                Else
                    TestRequired.SetValue("No", i)
                End If
                i += 1
            End If
        Next
        Return TestRequired
    End Function
    Public Function GetAllSelected(ByVal pnl As Panel) As Array
        '**************************************************************************************************
        'Purpose:       This Procedure Determines the SDF Field Values
        'Parameters:    Panel Control
        'Returns:       ArrayList
        '**************************************************************************************************
        Dim ctl As Control
        Dim i As Integer
        ' Dim getDB As New LeadEntranceFacade
        Dim LeadGrpSelected() As String = Array.CreateInstance(GetType(String), intTestCount)
        For Each ctl In pnl.Controls
            If ctl.GetType Is GetType(CheckBox) Then
                If CType(ctl, CheckBox).Checked = True Then
                    LeadGrpSelected.SetValue("Yes", i)
                Else
                    LeadGrpSelected.SetValue("No", i)
                End If
                i += 1
            End If
        Next
        Return LeadGrpSelected
    End Function
    Protected Function TrueOrFalse(ByVal Bool As Boolean) As Boolean

        If Bool Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Sub BindDataGridList()
        'Dim Facade As New LeadFacade
        'dgrdTransactionSearch.DataSource = Facade.GetAllLeadGroupsByRequirement(txtadReqId.Text)
        'dgrdTransactionSearch.DataBind()
    End Sub

    Private Sub dlstAdmissionReqGroups_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles dlstAdmissionReqGroups.ItemDataBound
        'Dim iitems As DataGridItemCollection
        'Dim iitem As DataGridItem
        'Dim i As Integer
        'Dim txtCheckIsGraded As TextBox
        'Dim GradeLabel As Label

        'iitems = dgrdTransactionSearch.Items
        'For i = 0 To iitems.Count - 1
        '    iitem = iitems.Item(i)
        '    txtCheckIsGraded = CType(iitem.FindControl("txtIsGraded"), TextBox)
        '    GradeLabel = CType(iitem.FindControl("blIsGraded"), Label)
        '    'If txtCheckIsGraded.Text = True Then
        '    '    GradeLabel.Text = "Yes"
        '    'ElseIf txtCheckIsGraded.Text = False Then
        '    '    GradeLabel.Text = "No"
        '    'End If
        'Next
    End Sub
    Public Function checkItemsSelected() As Integer
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer
        Dim intArrCount As Integer
        ' Dim Facade As New LeadFacade

        ' Save the datagrid items in a collection.
        iitems = dgrdTransactionSearch.Items
        Try
            'Loop thru the collection to get the Array Size
            For i = 0 To iitems.Count - 1
                iitem = iitems.Item(i)
                If (CType(iitem.FindControl("chkUnschedule"), CheckBox).Checked = True) Then
                    intArrCount += 1
                End If
            Next
        Finally
        End Try
        If intArrCount >= 1 Then
            Return intArrCount
        Else
            Return 0
        End If
    End Function

    Public Function CheckIfMandatory(ByVal MandatoryReq As Boolean) As String
        If MandatoryReq = True Then
            Return " <font color=""red"">*"
        Else
            Return ""
        End If
    End Function

    Private Sub ClearMinScoreIfReqIsDocument()
        Dim iitems As DataGridItemCollection
        Dim iitem As DataGridItem
        Dim i As Integer

        ' Save the data-grid items in a collection.
        iitems = dgrdLeadGroups.Items
        Try
            'Loop through the collection to get the Array Size
            If ddladReqTypeId.SelectedValue = 3 Then
                For i = 0 To iitems.Count - 1
                    iitem = iitems.Item(i)
                    If Not (IsNothing(iitem.FindControl("txtMinScore"))) Then
                        Dim t As TextBox = CType(iitem.FindControl("txtMinScore"), TextBox)
                        t.Text = String.Empty
                    End If
                    If Not (IsNothing(iitem.FindControl("lblMinScore"))) Then
                        Dim t As Label = CType(iitem.FindControl("lblMinScore"), Label)
                        t.Text = String.Empty
                    End If
                    If Not (IsNothing(iitem.FindControl("txtEditMinScore"))) Then
                        Dim t As TextBox = CType(iitem.FindControl("txtEditMinScore"), TextBox)
                        t.Text = String.Empty
                    End If


                    'Try
                    '    CType(iitem.FindControl("txtMinScore"), TextBox).Text = ""
                    'Catch ex As Exception
                     '	Dim exTracker = new AdvApplicationInsightsInitializer()
                    '	exTracker.TrackExceptionWrapper(ex)

                    'End Try

                    'Try
                    '    CType(iitem.FindControl("lblMinScore"), Label).Text = ""
                    'Catch ex As Exception
                     '	Dim exTracker = new AdvApplicationInsightsInitializer()
                    '	exTracker.TrackExceptionWrapper(ex)

                    'End Try

                    'Try
                    '    CType(iitem.FindControl("txtEditMinScore"), TextBox).Text = ""
                    'Catch ex As Exception
                     '	Dim exTracker = new AdvApplicationInsightsInitializer()
                    '	exTracker.TrackExceptionWrapper(ex)

                    'End Try
                Next
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

    Private Sub ddladReqTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddladReqTypeId.SelectedIndexChanged
        ClearMinScoreIfReqIsDocument()
    End Sub


    ''To fix rally case DE1085
    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuildList.Click
        BindDataList()

        '   save ReqGrpId in session
        Session("adReqIdByDate") = Nothing
        ddlModuleId.SelectedIndex = 0
        rbtnrequirement.SelectedIndex = -1
        '   bind DataGrid
        BindDataGrid()

        '   bind an empty new AdReqGrpInfo
        BindAdReqGrpData(New AdReqGrpInfo)

        '   initialize buttons
        InitButtonsForLoad()
        CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
        dgrdLeadGroups.ShowFooter = True

    End Sub
End Class
