Imports System.Xml
Imports System.Diagnostics
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Advantage.Business.Objects
Imports System.Globalization
Imports FAME.Advantage.Reporting.Logic
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections
'Imports FAME.AdvantageV1.BusinessFacade.TM
Imports Advantage.Business.Logic.Layer

Partial Class ApplicantLedger
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
#End Region

    Dim leadid As String
    Protected campusId As String
    Dim userId As String
    Private pObj As New UserPagePermissionInfo

    Private mruProvider As MRURoutines
    Protected studentId As String
    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    Protected leadname As String = ""
    Protected strTransacId As String = ""
    Protected chkLeadWasEnrolled As Boolean = False
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()


        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))


    End Sub

    Private Function GetLeadFromStateObject(ByVal paramResourceId As Integer) As StudentMRU

        Dim objStudentState As New StudentMRU

        Try
            MyBase.GlobalSearchHandler(1)

            boolSwitchCampus = Master.Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                studentId = Guid.Empty.ToString()
            Else
                studentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                leadid = Guid.Empty.ToString()
            Else
                leadid = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(studentId)
                .LeadId = New Guid(leadid)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"


            Master.Master.ShowHideStatusBarControl(True) 'Show Lead Bar only when Lead is not blank


            Master.Master.PageObjectId = leadid
            Master.Master.PageResourceId = Request.QueryString("resid")
            Master.Master.setHiddenControlForAudit()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        '        Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        userId = advantageUserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState = GetLeadFromStateObject(484) 'Pass resourceid

        If objStudentState Is Nothing Then
            MyBase.RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            studentId = .StudentId.ToString
            leadid = .LeadId.ToString


            leadname = objStudentState.Name
        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''


        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'If pObj.HasFull Or pObj.HasDelete Then
        '    btnDelete.Enabled = True
        'End If

        'Delete Should Always Be Disabled
        btnDelete.Enabled = False
        btnNew.Enabled = False


        Dim leadFacade As New LeadEnrollmentFacade
        chkLeadWasEnrolled = leadFacade.CheckIfLeadWasEnrolled(leadid)

        'Disable all save/new/delete buttons
        btnNew.Enabled = False
        btnSave.Enabled = False
        btnDelete.Enabled = False

        If chkLeadWasEnrolled = True Then
            radApplicantLedger.MasterTableView.NoMasterRecordsText = "Ledger details not shown as the applicant was enrolled. Please go to Student Ledger page to view details."

            If Not String.IsNullOrEmpty(leadname) Then
                radApplicantLedger.MasterTableView.Caption = "Applicant Ledger  -  " & leadname.ToString.ToProperCase
            End If

            Exit Sub
        End If

        If (Not Page.IsPostBack AndAlso chkLeadWasEnrolled = False) Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            BuildGridData()

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""
        End If

        If String.IsNullOrEmpty(leadname) Then
            radApplicantLedger.MasterTableView.Caption = "Applicant Ledger "
        Else
            radApplicantLedger.MasterTableView.Caption = "Applicant Ledger  -  " & leadname.ToString.ToProperCase
        End If

    End Sub
    Private Function BuildRadGrid() As DataSet
        Dim ds As New DataSet
        ds = (New LeadMasterObject).getTransactionsforApplicantLedger(leadid)
        Return ds
    End Function
    Private Sub BuildGridData()
        lblNoRowsMessage.Visible = False
        radApplicantLedger.DataSource = BuildRadGrid()
        radApplicantLedger.DataBind()

    End Sub
    Protected Sub RadApplicantLedgerDeleteCommand(sender As Object, e As GridCommandEventArgs)
        'Response.Write("Reversal")
    End Sub
    Protected Sub RadApplicantLedgerItemCommand(sender As Object, e As GridCommandEventArgs)
        If e.CommandName.ToLower = "void" Then
            Dim strTransactionId As String = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("TransactionId").ToString 'e.CommandArgument.ToString
            Dim facadeObj As New LeadMasterObject
            facadeObj.MakeATransactionVoid(strTransactionId)
            BuildGridData()
            'ElseIf e.CommandName.ToLower = "reverse" Then
            '    'Call Reversal funtion
            '    Dim strTransactionId As String = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("TransactionId").ToString
            '    Dim strLeadId As String = CType(e.Item.OwnerTableView.FindControl("lblLeadId"), Label).Text.ToString
        ElseIf e.CommandName.ToLower = "print" Then
            'Call the SSRS report
            Dim strTransactionId As String = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("TransactionId").ToString
            Dim isVoided As String = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("Voided").ToString
            Dim strTransDescription As String = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("ReversalReason").ToString
            Dim isReversed As Boolean = False
            If Not String.IsNullOrEmpty(strTransDescription) Then
                isReversed = True
            End If
            PrintReceipt(strTransactionId, "111111", isVoided, isReversed)
        End If

    End Sub
    Protected Sub RadApplicantLedgerItemCreated(sender As Object, e As GridItemEventArgs) Handles radApplicantLedger.ItemCreated
        If TypeOf e.Item Is GridDataItem Then
            'Dim btnReverse As LinkButton = DirectCast(e.Item.FindControl("lnkReverse"), LinkButton)
            Dim btnVoid As LinkButton = DirectCast(e.Item.FindControl("lnkVoid"), LinkButton)
            Dim btnPrint As LinkButton = DirectCast(e.Item.FindControl("lnkPrint"), LinkButton)

            'btnReverse.Attributes("href") = "#"
            btnVoid.Attributes("href") = "#"

            Dim gridedititem As GridDataItem = CType(e.Item, GridDataItem)
            Dim strLeadId As String = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("LeadId").ToString
            Dim strTransCodeId As String = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("TransCodeId").ToString
            Dim strTransReference As String = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("TransReference").ToString
            Dim strTransDescrip As String = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("TransDescription").ToString
            Dim strTransAmount As String = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("TransactionAmount").ToString
            Dim boolIsEnrolled As Boolean = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("IsEnrolled")
            strTransacId = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("TransactionId").ToString
            Dim intEnrolled As Integer = 0
            If boolIsEnrolled = False Then intEnrolled = 0 Else intEnrolled = 1

            'btnVoid.Attributes("onclick") = [String].Format("return showConfirm('{0}','{1});", e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("TransactionId"), _
            '                                                   e.Item.ItemIndex)

            btnVoid.Attributes("onclick") = [String].Format("return voidtransaction('{0}','{1}');", e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("TransactionId"), e.Item.ItemIndex)

            'btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

            'btnVoid.Attributes.Add("onclick", "radconfirm('Do you want to void this transaction? .\n\nClick OK to confirm.', callBackFn, null, 150, '', 'Void');return false;")

            'btnReverse.Attributes("onclick") = [String].Format("return ShowEditForm('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7});", _
            '                                                   e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("TransactionId"), _
            '                                                   e.Item.ItemIndex, _
            '                                                   strLeadId, _
            '                                                   strTransCodeId, _
            '                                                   strTransReference, _
            '                                                   strTransDescrip, _
            '                                                   strTransAmount, _
            '                                               intEnrolled)
        End If
    End Sub

    Protected Sub RadApplicantLedgerItemDataBound(sender As Object, e As GridItemEventArgs) Handles radApplicantLedger.ItemDataBound
        If TypeOf e.Item Is GridDataItem Then
            Dim boolVoid As Boolean = False
            Try
                boolVoid = DirectCast(e.Item.FindControl("lblVoid"), Label).Text
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                boolVoid = False
            End Try


            Dim strReversalReason As String = ""
            Try
                strReversalReason = DirectCast(e.Item.FindControl("lblReversalReason"), Label).Text
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                strReversalReason = ""
            End Try

            Dim strTransType As String = ""
            Try
                strTransType = DirectCast(e.Item.FindControl("lblItemTransactionType"), Label).Text
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                strTransType = ""
            End Try


            If boolVoid = True Then
                CType(e.Item.FindControl("lnkVoid"), LinkButton).BackColor = Color.SteelBlue
                CType(e.Item.FindControl("lnkVoid"), LinkButton).ForeColor = Color.White
                CType(e.Item.FindControl("lnkVoid"), LinkButton).Font.Bold = True
                CType(e.Item.FindControl("lnkVoid"), LinkButton).Enabled = False
                CType(e.Item.FindControl("lnkVoid"), LinkButton).ToolTip = "This payment was voided"
                CType(e.Item.FindControl("lnkVoid"), LinkButton).Attributes.Add("onclick", "return false")
                CType(e.Item.FindControl("lnkVoid"), LinkButton).Attributes.Add("onclientclick", "return false")
                ' CType(e.Item.FindControl("lnkReverse"), LinkButton).Visible = False
            Else
                CType(e.Item.FindControl("lnkVoid"), LinkButton).BackColor = Color.White
                CType(e.Item.FindControl("lnkVoid"), LinkButton).ForeColor = Color.Black
                CType(e.Item.FindControl("lnkVoid"), LinkButton).Font.Bold = False
                CType(e.Item.FindControl("lnkVoid"), LinkButton).Enabled = True
                CType(e.Item.FindControl("lnkVoid"), LinkButton).ToolTip = "Void Transaction"
                ' CType(e.Item.FindControl("lnkReverse"), LinkButton).Visible = True
                'CType(e.Item.FindControl("lnkVoid"), LinkButton).Attributes.Add("onclick", "if(confirm('Are you sure you want to void this transaction?')){}else{return false}")
                If Not String.IsNullOrEmpty(strReversalReason) Then
                    'CType(e.Item.FindControl("lnkReverse"), LinkButton).BackColor = Color.MediumSeaGreen
                    'CType(e.Item.FindControl("lnkReverse"), LinkButton).ForeColor = Color.White
                    'CType(e.Item.FindControl("lnkReverse"), LinkButton).Font.Bold = True
                    'CType(e.Item.FindControl("lnkReverse"), LinkButton).Enabled = False
                    'CType(e.Item.FindControl("lnkReverse"), LinkButton).ToolTip = strReversalReason
                    'CType(e.Item.FindControl("lnkReverse"), LinkButton).Attributes.Add("onclick", "return false")
                    CType(e.Item.FindControl("lnkVoid"), LinkButton).Visible = False
                End If
            End If

            If strTransType.ToLower = "adjustment" Or _
                strTransType.ToLower = "charge" Or _
                strTransType.ToLower = "charge (adjusted)" Or _
                strTransType.ToLower = "payment (adjusted)" Then
                CType(e.Item.FindControl("lnkVoid"), LinkButton).Visible = False
                'CType(e.Item.FindControl("lnkReverse"), LinkButton).Visible = False
                CType(e.Item.FindControl("lnkPrint"), LinkButton).Visible = False
            End If

            If Not String.IsNullOrEmpty(strReversalReason) Then
                CType(e.Item.FindControl("lnkVoid"), LinkButton).Visible = True
            End If
        End If
    End Sub
    Protected Sub RadApplicantLedgerNeedDataSource(ByVal sender As Object, ByVal e As GridNeedDataSourceEventArgs) Handles radApplicantLedger.NeedDataSource
        If chkLeadWasEnrolled = False Then
            radApplicantLedger.Visible = True
            radApplicantLedger.DataSource = BuildRadGrid()
            lblNoRowsMessage.Visible = False
        Else
            radApplicantLedger.Visible = False
            lblNoRowsMessage.Visible = True
            lblNoRowsMessage.Text = "Ledger details not shown as the applicant was enrolled. Please go to Student Ledger page to view details."
        End If
    End Sub
    Private Sub BtnSaveClick(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        ''''' Code changes to Fix mantis issue id 19549 by kamalesh Ahuja on 23 Aug 2010 '''
        Dim userId As String
        userId = XmlConvert.ToGuid(AdvantageSession.UserState.UserId.ToString).ToString
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    'TO DO Change report file name
    Protected Sub PrintReceipt(ByVal transactionId As String, ByVal uniqueId As String, ByVal isVoided As String, ByVal isReversed As Boolean)
        Dim getReportAsBytes As [Byte]()
        Dim strTransactionStatus As String = ""

        If isVoided.ToLower = "true" Then
            strTransactionStatus = "** Transaction Void"
        ElseIf isReversed = True Then
            strTransactionStatus = "** Transaction Adjusted"
        End If

        Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder") '"/Advantage Reports/" + SingletonAppSettings.AppSettings("SSRS Deployment Environment").ToString.Trim
        getReportAsBytes = (New PrintLeadReceipt).RenderReport("pdf", _
                                                                    transactionId, _
                                                                    Integer.Parse(transactionId.Substring(0, 6), NumberStyles.HexNumber).ToString(), _
                                                                    AdvantageSession.UserState.UserName.ToString, _
                                                                    strReportPath, _
                                                                    AdvantageSession.UserState.CampusId.ToString,
                                                                    strTransactionStatus)
        ExportReport("pdf", getReportAsBytes)
    End Sub

    Private Sub ExportReport(ByVal exportFormat As String, ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case exportFormat.ToLower
            Case Is = "pdf"
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case "excel"
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
                'Case "WORD"
                '    strExtension = "doc"
                '    strMimeType = "application/vnd.ms-word"
            Case "csv"
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select
        Session("SSRS_FileExtension") = strExtension
        Session("SSRS_MimeType") = strMimeType
        Session("SSRS_ReportOutput") = getReportAsBytes
        'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        Dim script As String = String.Format("window.open('../SY/DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString())
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    End Sub

End Class
