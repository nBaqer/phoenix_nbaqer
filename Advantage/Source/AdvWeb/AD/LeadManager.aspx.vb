﻿Imports System.Diagnostics
Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports Advantage.Business.Objects
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports FAME.Advantage.Common
Imports Advantage.Business.Logic.Layer
Imports System.IO

Partial Class LeadManager
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtEmployerContactId As TextBox
    Protected WithEvents txtEmployerId As TextBox
    Protected WithEvents lblTitleId As Label
    Protected WithEvents ddlTitleId As DropDownList
    Protected WithEvents lblWorkPhone As Label
    Protected WithEvents txtWorkPhone As TextBox
    Protected WithEvents lblHomePhone As Label
    Protected WithEvents lblCellPhone As Label
    Protected WithEvents txtCellPhone As TextBox
    Protected WithEvents lblEmails As Label
    Protected WithEvents lblBeeper As Label
    Protected WithEvents txtBeeper As TextBox

    ' Protected EmployerContactId As String = "03D26D83-7814-4BCF-AB80-0F429C691D9B"
    Protected WithEvents ChkStatus As CheckBox
    Protected WithEvents dlstEmployerContact As DataList

    Protected WithEvents RegularExpressionValidator1 As RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator2 As RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator3 As RegularExpressionValidator
    Protected WithEvents Regularexpressionvalidator4 As RegularExpressionValidator

    Protected WithEvents txtState As TextBox
    Protected WithEvents lblExt As Label
    Protected WithEvents txtExtension As TextBox
    Protected WithEvents lblBestTime As Label
    Protected WithEvents txtBestTime As TextBox
    Protected WithEvents Textbox1 As TextBox
    Protected WithEvents Textbox2 As TextBox
    Protected WithEvents Textbox3 As TextBox
    Protected WithEvents Textbox4 As TextBox
    Protected WithEvents lblPinNumber As Label
    Protected WithEvents txtPinNumber As TextBox
    Protected WithEvents lblWorkExt As Label
    Protected WithEvents txtWorkExt As TextBox
    Protected WithEvents lblWorkBestTime As Label
    Protected WithEvents txtWorkBestTime As TextBox
    Protected WithEvents lblHomeBestTime As Label
    Protected WithEvents txtHomeBestTime As TextBox
    Protected WithEvents txtCellBestTime As TextBox
    Protected WithEvents txtRowIds As TextBox
    Protected WithEvents txtResourceId As TextBox
    Protected WithEvents Label7 As Label
    Protected WithEvents Dropdownlist1 As DropDownList
    Protected WithEvents lblAddressStatusID As Label
    Protected WithEvents ddlDriverLicenseNumber As DropDownList
    Protected WithEvents txtPhone1 As TextBox

    Protected CampusId As String
    Protected WithEvents lbl2 As Label
    Protected WithEvents chkForeign As CheckBox
    Protected m_context As HttpContext
    Protected LeadId, StudentId As String

    Private pObj As New UserPagePermissionInfo
    Dim userId As String
    Protected ModuleId As String
    Dim ResourceID As Integer
    Dim isIPEDSApplicable As String
    Protected strDefaultCountry As String


    Private mruProvider As MRURoutines
#End Region
    Public strImageURL As String = ""
    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    Protected MyAdvAppSettings As AdvAppSettings
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

        mruProvider = New MRURoutines(Me.Context, GetConnectionStringFromAdvAppSetting("AdvantageConnectionString"))
    End Sub
    Private Function GetLeadFromStateObject(ByVal paramResourceId As Integer) As StudentMRU

        Dim objStudentState As New StudentMRU

        Try
            MyBase.GlobalSearchHandler(1)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)

            Master.PageObjectId = LeadId
            Master.PageResourceId = Request.QueryString("resid")
            Master.setHiddenControlForAudit()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
    Private Function getLastEntityFromStateObject() As StudentMRU
        Dim objStateInfo As New AdvantageStateInfo
        Dim strVID As String
        Dim strLeadId As String = ""
        Dim objEntityState As New StudentMRU

        strLeadId = mruProvider.getLastEntityUserWorkedWith(AdvantageSession.UserState.UserId.ToString, _
                                                               4, _
                                                               AdvantageSession.UserState.CampusId.ToString)

        If String.IsNullOrEmpty(strLeadId) Then
            MyBase.RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
        ElseIf strLeadId = Guid.Empty.ToString Then
            MyBase.RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
        End If
        objStateInfo = mruProvider.BuildLeadStatusBar(strLeadId)
        With objStateInfo
            If .StudentId Is Nothing OrElse .StudentId = "" Then
                .StudentId = Guid.Empty.ToString
            End If
        End With

        'Create a new guid to be associated with the employer pages
        strVID = Guid.NewGuid.ToString

        'Add an entry to AdvantageSessionState for this guid and object
        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        'UpdateMRUTable
        'Reason: We need to keep track of the last student record the user worked with
        'Scenario1 : User can log in and click on a student page without using MRU 
        'and we need to display the data of the last student the user worked with
        'If the user is a first time user, we will load the student who was last added to advantage
        mruProvider.UpdateMRUList(strLeadId, AdvantageSession.UserState.UserId.ToString, _
                                  AdvantageSession.UserState.CampusId.ToString, AdvantageSession.UserState.UserId.ToString)

        With objEntityState
            .StudentId = New Guid(objStateInfo.StudentId.ToString)
            .LeadId = New Guid(objStateInfo.LeadId.ToString)
            .ChildId = New Guid(strVID)
            .Name = objStateInfo.NameValue
        End With
        Return objEntityState
    End Function
#End Region
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim sdfControls As New SDFComponent
        Dim objCommon As New CommonUtilities
        'Dim fac As New UserSecurityFacade
        'Dim strVID As String
        ' Dim state As AdvantageSessionState

        Session("SEARCH") = 0
        txtDate.Text = Date.Now.ToShortDateString
        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        CampusId = Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        ResourceID = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceID, CampusId)

        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If


        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        Dim objStudentState As New StudentMRU
        objStudentState = GetLeadFromStateObject(170) 'Pass resourceid so that user can be redirected to same page while swtiching students
        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            'boolSwitchCampus = False 'Reset. If not reset, the notification is shown for every student within the campus
        End With

        'There are scenarios when a user may migrate from a student page but the student may not have any lead
        'associated data. In that case, the behavior is to load the lead user previously accessed instead of 
        'redirecting user to an error page
        If LeadId.Trim = "" Or LeadId = Guid.Empty.ToString Then
            objStudentState = getLastEntityFromStateObject() 'Get Last Lead User accessed
            With objStudentState
                StudentId = .StudentId.ToString
                LeadId = .LeadId.ToString
            End With
            'ObjStudentState.ChildId property holds strVID
            If LeadId.Trim <> "" Or LeadId <> Guid.Empty.ToString Then
                ' Master.ShowHideStatusBarPassObjectPointer(True, objStudentState.ChildId.ToString) 'Show Lead Bar only when Lead is not blank
            Else
                '    Master.ShowHideStatusBarPassObjectPointer(False, objStudentState.ChildId.ToString) 'Hide Lead Bar only when Lead is blank
            End If

        End If

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        'Get StudentId and LeadId
        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder))
            lblPhone.Text = "Phone 1"
            ViewState("MODE") = "NEW"
            BuildDropDownLists()
            Session("ScrollValue") = 0
            strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString
            If LeadId <> "" Then
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    lblEntranceInterviewDate.Visible = True
                    txtEntranceInterviewDate.Visible = True
                    'Img7.Visible = True
                    lblHighSchoolProgramCode.Visible = True
                    txtHighSchoolProgramCode.Visible = True
                End If
                GetLeadInfo(LeadId)
                BindLeadGroupList(LeadId)
                txtLeadMasterID.Text = LeadId
                chkIsInDB.Checked = True
            Else
                objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder))
                chkIsInDB.Checked = False
                txtLeadMasterID.Text = Guid.NewGuid.ToString
                txtAssignedDate.SelectedDate = Date.Now.ToShortDateString()
            End If
            'Setup regent required fields
            SetupRegentRequiredFields()

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""
        Else
            objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), , chkForeignZip.Checked)
        End If

        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        'Set the Mask
        GetInputMaskValue()

        'Check if Lead is eligible for Enrollment
        Dim isLeadEligible As String = IsLeadEligibleForEnrollment()
        Dim strLeadName As String = Trim(txtFirstName.Text) + " " & Trim(txtMiddleName.Text) + " " + Trim(txtLastName.Text)
        If ddlLeadStatus.Enabled = True Then
            If Not isLeadEligible = "" Then
                If isLeadEligible = "Enroll" Then
                    lblEnrollStatus.Text = "<strong>" & strLeadName & " </strong> has satisfied all admission requirements and " & "<strong>" & " is eligible for enrollment." & "</strong>"
                    lblEnrollStatus.Visible = True
                    lnkToEnrollLead.Visible = False
                    lblOpenReqs.Visible = False
                    lblOpenReqs.Text = " and to view Admission Requirements, click "
                    lnkToOpenRequirements.Visible = False
                    lnkToOpenRequirements.Font.Bold = True
                    lnkEnrollLead.Enabled = True
                    lnkOpenRequirements.Enabled = True
                Else
                    lblEnrollStatus.Text = "<strong>" & strLeadName & " </strong> has not satisfied all admission requirements and " & "<strong>" & " is not eligible for enrollment.</strong>"
                    lblEnrollStatus.Visible = True
                    lblOpenReqs.Text = " To view Admission Requirements, please click "
                    lblOpenReqs.Visible = False
                    lnkToOpenRequirements.Visible = False
                    lnkToEnrollLead.Visible = False
                    lnkEnrollLead.Enabled = False
                    lnkOpenRequirements.Enabled = True
                End If
            Else
                lblEnrollStatus.Text = "Please select the program version to check if " & strLeadName & " has satisfied all admission requirements and is eligible for enrollment."
                lblEnrollStatus.Visible = True
                lnkToEnrollLead.Visible = False
                lblOpenReqs.Text = " Please <strong>select program version</strong> to view Admission Requirements."
                lblOpenReqs.Visible = False
                lnkToOpenRequirements.Visible = False
                lnkEnrollLead.Enabled = False
                lnkOpenRequirements.Enabled = True
            End If
        Else
            lblEnrollStatus.Text = "<strong>" & strLeadName & " </strong> has been successfully enrolled."
            lblEnrollStatus.Visible = True
            lnkToEnrollLead.Visible = False
            lblOpenReqs.Visible = True
            lblOpenReqs.Text = " To view Admission Requirements, please click "
            lblOpenReqs.Visible = False
            lnkToOpenRequirements.Visible = False
            lnkOpenRequirements.Enabled = True
            lnkEnrollLead.Enabled = False
        End If
        If chkForeignZip.Checked = True Then
            lblOtherState.Enabled = True
            lblOtherState.Visible = True
            txtOtherState.Visible = True
            ddlStateID.Enabled = False
        Else
            txtOtherState.Visible = False
            lblOtherState.Visible = False
            ddlStateID.Enabled = True
        End If
        'IPEDSRequirements()

        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfControls.GetSDFExists(ResourceID, "AD")
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        'User Defined Fields Code
        If Trim(txtPKID.Text) <> "" Then
            sdfControls.GenerateControlsEdit(MyBase.FindControlRecursive("pnlSDF"), ResourceID, txtPKID.Text, "AD")
        Else
            sdfControls.GenerateControlsNew(MyBase.FindControlRecursive("pnlSDF"), ResourceID, "AD")
        End If

        'Code to disable all buttons if lead was already enrolled
        'to prevent data being changed after lead was enrolled
        Dim chkLeadWasEnrolled As Boolean
        Dim leadFacade As New LeadEnrollmentFacade
        chkLeadWasEnrolled = leadFacade.CheckIfLeadWasEnrolled(LeadId)
        If chkLeadWasEnrolled = True Then
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
            lnkEnrollLead.Enabled = False
            btnSave.ToolTip = "Cannot modify lead info as the lead was already enrolled"
            btnDelete.ToolTip = "Cannot delete lead as the lead was already enrolled"
        Else
            btnSave.ToolTip = ""
            btnDelete.ToolTip = ""
            '  lnkEnrollLead.Enabled = True
        End If

        Session("PrgVerId") = ddlPrgVerId.SelectedValue

        'Display Student Image

        Try
            strImageURL = GetStudentImagePath(LeadId)
            Dim AltMsg As String = String.Empty
            If Not strImageURL = "" Then
                If Not File.Exists(strImageURL) Then
                    strImageURL = "../images/Default100.jpg"
                    AltMsg = "Photograph not available"
                End If
            Else
                strImageURL = "../images/Default100.jpg"
                AltMsg = "Photograph not available"
            End If
            myimage.Alt = AltMsg
            myimage.Src = strImageURL
            myimage.CausesValidation = False
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            myimage.Alt = "Photograph not available"
            myimage.Src = "../images/Default100.jpg"
        End Try
        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            SetupRegentRequiredFields()
        End If

        txtFirstName.BackColor = Color.White
        txtLastName.BackColor = Color.White
        ddlLeadStatus.BackColor = Color.White
        ddlCampusId.BackColor = Color.White
        ddlAdmissionsRep.BackColor = Color.White
        ddlCountry.BackColor = Color.White
        txtAssignedDate.BackColor = Color.White
        txtDateApplied.BackColor = Color.White

    End Sub
    Private Sub BuildLeadGroupsDDL()
        Dim facade As New AdReqsFacade
        With chkLeadGrpId
            .DataTextField = "Descrip"
            .DataValueField = "LeadGrpId"
            .DataSource = facade.GetAllLeadGroupsForExistingLeads(CampusId, LeadId)
            .DataBind()
        End With
    End Sub
    Private Sub SetupRegentRequiredFields()
        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            'txtSSN.BackColor = Color.FromName("#ffff99")
            'txtBirthDate.BackColor = Color.FromName("#ffff99")
            lblSSN.Text = lblSSN.Text & "<font color=""red"">*</font>"
            lblBirthDate.Text = lblBirthDate.Text & "<font color=""red"">*</font>"
        End If
    End Sub
    'Private Function ValidateRegentFields() As String
    '    'For regent firstname,lastname,dob and ssn are required fields
    '    If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
    '        Dim strMessage As String = ""
    '        If txtSSN.Text = "" Then
    '            strMessage = "SSN is required for student creation in regent application"
    '        End If
    '        If txtBirthDate.SelectedDate Is Nothing Then
    '            strMessage &= "DOB is required for student creation in regent application"
    '        End If
    '        Return strMessage
    '    End If
    'End Function
    Private Sub GetLeadInfo(ByVal LeadId As String)
        Dim leadInfo As New LeadFacade
        Dim boolRegent = MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes"
        BindLeadMasterData(leadInfo.GetLeadsInfo(LeadId, boolRegent))
    End Sub
    Private Sub GetInputMaskValue()
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        Dim ssnMask As String

        'Get The Input Mask for Phone/Fax and Zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)

        'Replace The Mask Character from # to 9 as Masked Edit TextBox 
        'accepts only certain characters as mask characters
        'txtSSN.Mask = Replace(ssnMask, "#", "9")
        lblSSN.ToolTip = ssnMask

        If chkForeignPhone.Checked = False Then
            ' txtPhone.Mask = Replace(strMask, "#", "9")
            txtphone.Mask = "(###)-###-####"
            txtphone.DisplayMask = "(###)-###-####"
            txtphone.DisplayPromptChar = ""
        Else
            txtphone.Text = ""
            txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtphone.DisplayMask = ""
            txtphone.DisplayPromptChar = ""
        End If

        If chkForeignPhone2.Checked = False Then
            txtPhone2.Mask = "(###)-###-####"
            txtPhone2.DisplayMask = "(###)-###-####"
            lblPhone2.ToolTip = strMask
        Else
            txtPhone2.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtPhone2.DisplayMask = ""
            lblPhone2.ToolTip = ""
        End If

        If chkForeignZip.Checked = False Then
            ' txtZip.Mask = Replace(zipMask, "#", "9")
            txtZip.Mask = "#####"
            lblZip.ToolTip = zipMask
        Else
            txtZip.Mask = "aaaaaaaaaaaaaaaaaaaa"
            lblZip.ToolTip = ""
        End If

    End Sub

    '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
    Private Sub BuildCampusDDL()
        'Bind the CampusGroups DrowDownList
        Dim campusGroups As New CampusGroupsFacade
        With ddlCampusId
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"

            If MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "ownleads" Then
                ''''' Code changes by Kamalesh Ahuja on 16 Aug 2010 to resolve mantis issue id 19548
                .DataSource = campusGroups.GetCampusesByUser(userId)
                ''''''
            ElseIf MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "permissiblecampuses" Then
                .DataSource = campusGroups.GetCampusesByUser(userId)
            ElseIf MyAdvAppSettings.AppSettings("ShowLeadsToUsers").ToString.Trim.ToLower = "allcampuses" Then
                ''''' Code changes by Kamalesh Ahuja on 17 Aug 2010 to resolve mantis issue id 19549
                'If SingletonAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "yes" Then
                '    .DataSource = campusGroups.GetAllCampuses()
                'Else
                '    .DataSource = campusGroups.GetCampusesByUser(userId)
                'End If
                .DataSource = campusGroups.GetAllCampuses()
                ''''''''''''''
            Else
                .DataSource = campusGroups.GetAllCampusEnrollmentByCampus(CampusId)
            End If

            .DataBind()
        End With
    End Sub
    'Private Sub BuildCampusDDL()
    '    'Bind the CampusGroups DrowDownList
    '    Dim campusGroups As New CampusGroupsFacade
    '    With ddlCampusId
    '        .DataTextField = "CampDescrip"
    '        .DataValueField = "CampusId"
    '        .DataSource = campusGroups.GetAllCampusEnrollmentByCampus(CampusId)
    '        .DataBind()
    '    End With
    'End Sub
    Private Sub BuildDropDownLists()

        'BuildPrefixDDL()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Lead Status
        ddlList.Add(New AdvantageDDLDefinition(ddlLeadStatus, AdvantageDropDownListName.LeadStatus, CampusId, True, True, String.Empty))

        'Prefixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlPrefix, AdvantageDropDownListName.Prefixes, CampusId, True, True, String.Empty))

        'Campuses DDL
        'ddlList.Add(New AdvantageDDLDefinition(ddlCampusId, AdvantageDropDownListName.Campuses, CampusId,True,True,String.Empty))

        'Suffixes DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlSuffix, AdvantageDropDownListName.Suffixes, CampusId, True, True, String.Empty))

        'Country DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlCountry, AdvantageDropDownListName.Countries, CampusId, True, True, String.Empty))

        'Address Type
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressType, AdvantageDropDownListName.Address_Types, CampusId, True, True, String.Empty))

        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlDrivLicStateId, AdvantageDropDownListName.States, CampusId, True, True, String.Empty))

        'Races
        ddlList.Add(New AdvantageDDLDefinition(ddlRace, AdvantageDropDownListName.Races, CampusId, True, True, String.Empty))

        'Genders
        ddlList.Add(New AdvantageDDLDefinition(ddlGender, AdvantageDropDownListName.Genders, CampusId, True, True, String.Empty))

        'Marital Status
        ddlList.Add(New AdvantageDDLDefinition(ddlMaritalStatus, AdvantageDropDownListName.MaritalStatus, CampusId, True, True, String.Empty))

        'States 
        ddlList.Add(New AdvantageDDLDefinition(ddlStateID, AdvantageDropDownListName.States, CampusId, True, True, String.Empty))

        'Citizenships
        ddlList.Add(New AdvantageDDLDefinition(ddlCitizen, AdvantageDropDownListName.Citizen, CampusId, True, True, String.Empty))

        'Nationality
        ddlList.Add(New AdvantageDDLDefinition(ddlNationality, AdvantageDropDownListName.Nationality, CampusId, True, True, String.Empty))

        'Address States 
        'ddlList.Add(New AdvantageDDLDefinition(ddlAddrStateId, AdvantageDropDownListName.States, CampusId,True,True,String.Empty))

        'Shifts
        ddlList.Add(New AdvantageDDLDefinition(ddlShiftID, AdvantageDropDownListName.Shifts, CampusId, True, True, String.Empty))

        'County
        ddlList.Add(New AdvantageDDLDefinition(ddlCounty, AdvantageDropDownListName.Counties, CampusId, True, True, String.Empty))

        'Family Income
        ddlList.Add(New AdvantageDDLDefinition(ddlFamilyIncome, AdvantageDropDownListName.FamilyIncome, CampusId, True, True, String.Empty))

        'Previous Education
        ddlList.Add(New AdvantageDDLDefinition(ddlPreviousEducation, AdvantageDropDownListName.EducationLvl, CampusId, True, True, String.Empty))

        'Phone Types
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneType, AdvantageDropDownListName.Phone_Types, CampusId, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneType2, AdvantageDropDownListName.Phone_Types, CampusId, True, True, String.Empty))

        'Phone Statuses
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneStatus, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))
        ddlList.Add(New AdvantageDDLDefinition(ddlPhoneStatus2, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Address Status
        ddlList.Add(New AdvantageDDLDefinition(ddlAddressStatus, AdvantageDropDownListName.Statuses, Nothing, True, True, String.Empty))

        'Admission Reps
        'ddlList.Add(New AdvantageDDLDefinition(ddlAdmissionsRep, AdvantageDropDownListName.AdmissionsRep, Nothing))

        'Dependency Type
        ddlList.Add(New AdvantageDDLDefinition(ddlDependencyTypeId, AdvantageDropDownListName.DependencyType, CampusId, True, True, String.Empty))

        'Geographic Type
        ddlList.Add(New AdvantageDDLDefinition(ddlGeographicTypeId, AdvantageDropDownListName.GeographicType, CampusId, True, True, String.Empty))

        'Sponsor
        ddlList.Add(New AdvantageDDLDefinition(ddlSponsor, AdvantageDropDownListName.Sponsors, CampusId, True, True, String.Empty))

        'Admin Criteria
        ddlList.Add(New AdvantageDDLDefinition(ddlAdminCriteriaId, AdvantageDropDownListName.AdminCriteriaType, CampusId, True, True, String.Empty))

        'Housing Type
        ddlList.Add(New AdvantageDDLDefinition(ddlHousingId, AdvantageDropDownListName.HousingType, CampusId, True, True, String.Empty))

        'Degree Certificate Seeking Type
        ddlList.Add(New AdvantageDDLDefinition(ddlDegCertSeekingId, AdvantageDropDownListName.DegCertSeekingType, CampusId, True, True, String.Empty))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

        BuildSourceCatagoryDDL()
        BuildProgramGroupDDL()
        BuildLeadGroupsDDL()
        BuildAdmissionRepsDDL()
        '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
        BuildCampusDDL()
    End Sub
    Private Sub UpdateLeadGroups(ByVal LeadId As String)

        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        Dim selectedDegrees() As String = CType(Array.CreateInstance(GetType(String), chkLeadGrpId.Items.Count), String())

        Dim i As Integer = 0
        Dim item As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In chkLeadGrpId.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        'resize the array
        If i > 0 Then
            ReDim Preserve selectedDegrees(i - 1)

            'update Selected Jobs
            Dim leadGrpFacade As New AdReqsFacade
            If leadGrpFacade.UpdateLeadGroups(LeadId, AdvantageSession.UserState.UserName, selectedDegrees) < 0 Then
                DisplayRADAlert(CallbackType.Postback, "Error1", "A related record exists,you can not perform this operation ", "Update Error")
                Exit Sub
            End If
        End If
        
    End Sub

    Private Function ValidateLeadGroups() As Integer
        Dim strGroups As String = String.Empty
        ' Dim dsUseScheduled As DataSet
        'In For Loop Check The Number of Items Selected
        For Each item As ListItem In chkLeadGrpId.Items
            If item.Selected Then
                If strGroups = String.Empty Then
                    strGroups = item.Value.ToString
                Else
                    strGroups &= "," & item.Value.ToString
                End If

            End If
        Next

        If strGroups = String.Empty Then
            Return -1
        ElseIf strGroups.Split(",").Length = 1 Then
            Return 1
        Else
            Return (New StudentEnrollmentFacade).GetSchedLeadGrpCnt_SP(strGroups)
        End If

    End Function
    Private Sub BuildPrgVersionDDL(Optional ByVal boolDisplayInActive As Boolean = False)
        ddlPrgVerId.Items.Clear()
        Dim facade As New LeadFacade
        With ddlPrgVerId
            '.DataTextField = "PrgVerDescrip"
            .DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = facade.GetAllProgramVersionByCampusAndUser(ddlProgramID.SelectedValue, CampusId, LeadId, boolDisplayInActive)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramGroupDDL()
        Dim PrgGrp As New LeadFacade
        With ddlAreaId
            .DataTextField = "PrgGrpDescrip"
            .DataValueField = "PrgGrpID"
            .DataSource = PrgGrp.GetAllProgramsByGroupAndLead(LeadId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlProgramID
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlPrgVerId
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceCatagoryDDL()
        Dim SourceCatagory As New LeadFacade
        With ddlSourceCategoryId
            .DataTextField = "SourceCatagoryDescrip"
            .DataValueField = "SourceCatagoryId"
            .DataSource = SourceCatagory.GetAllSourceCategoryByCampus(CampusId, LeadId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSourceTypeId
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlSourceAdvertisement
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceTypeDDL(ByVal SourceCatagoryID As String, Optional ByVal boolDisplayInActive As Boolean = False)
        Dim SourceType As New LeadFacade
        ddlSourceTypeId.Items.Clear()
        With ddlSourceTypeId
            .DataTextField = "SourceTypeDescrip"
            .DataValueField = "SourceTypeId"
            .DataSource = SourceType.GetAllSourceTypeByCampus(SourceCatagoryID, CampusId, LeadId, boolDisplayInActive)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildSourceAdvDDL(ByVal SourceTypeID As String, Optional ByVal boolDisplayInActive As Boolean = False)
        Dim SourceType As New LeadFacade
        ddlSourceAdvertisement.Items.Clear()
        With ddlSourceAdvertisement
            .DataTextField = "SourceAdvDescrip"
            .DataValueField = "SourceAdvId"
            .DataSource = SourceType.GetAllSourceAdvertisementByCampus(SourceTypeID, CampusId, LeadId, boolDisplayInActive)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildProgramsDDL(ByVal ProgramGrpID As String, Optional ByVal boolDisplayInActive As Boolean = False)
        Dim ProgramTypes As New LeadFacade
        ddlProgramID.Items.Clear()
        With ddlProgramID
            .DataTextField = "ProgDescrip"
            .DataValueField = "ProgId"
            .DataSource = ProgramTypes.GetAllProgramsByCampusAndUser(ProgramGrpID, CampusId, LeadId, boolDisplayInActive)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildAdmissionRepsDDL()

        ''MOdified By Saraswathi Lakshmanan to fix issue 14385
        ''When entering a new lead information SA, Front Desk and Director
        '' of Admissions should be able to assign them to admission rep 
        ''Modified the checking condition from Dir of Admn to DirOfAdmn and Frontdesk and Username with sa

        Dim userName As String
        userName = AdvantageSession.UserState.UserName


        Dim leadAdmissionReps As New LeadFacade
        Dim directorAdmissionsorFrontDeskCheck As New UserSecurityFacade
        ''Find if the User is DirectorOfAdmission or FrontDesk

        Dim boolDirOrFDeskCheck As Boolean = directorAdmissionsorFrontDeskCheck.IsDirectorOfAdmissionsorFrontDesk(userId)
        With ddlAdmissionsRep
            .DataTextField = "fullname"
            .DataValueField = "userid"
            If boolDirOrFDeskCheck Then
                .DataSource = leadAdmissionReps.GetAllAdmissionRepsByCampusUserIdUserRoles(CampusId, userId, userName, CType(boolDirOrFDeskCheck, Integer))
            Else
                .DataSource = leadAdmissionReps.GetAllAdmissionRepsByCampusAndUserId(CampusId, userId)
            End If
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub
    Private Function BuildLeadMaster(ByVal LeadMasterId As String) As LeadMasterInfo
        Dim leadMaster As New LeadMasterInfo
        'Dim facInputMask As New InputMasksFacade
        'Dim phoneMask As String
        'Dim zipMask As String
        'Dim ssnMask As String
        With leadMaster
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get LeadMasterID
            .LeadMasterID = txtLeadMasterID.Text

            'Get LastName
            .LastName = txtLastName.Text

            'Get FirstName
            .FirstName = txtFirstName.Text

            'Get MiddleName
            .MiddleName = txtMiddleName.Text

            'Get StatusId 
            .Status = ddlLeadStatus.SelectedValue
            txtLeadStatusIdHidden.Text = .Status


            'Get PrefixId 
            .Prefix = ddlPrefix.SelectedValue

            'Get SuffixId 
            .Suffix = ddlSuffix.SelectedValue


            'Get BirthDate
            '.BirthDate = txtBirthDate.SelectedDate
            If txtBirthDate.SelectedDate Is Nothing Then
                .BirthDate = ""
            Else
                .BirthDate = Date.Parse(txtBirthDate.SelectedDate)
            End If

            'Get Age
            '.Age = txtAge.Text

            'Get sponsor
            .Sponsor = ddlSponsor.SelectedValue

            'Get AdmissionsRep
            .AdmissionsRep = ddlAdmissionsRep.SelectedValue

            'Get Gender
            .Gender = ddlGender.SelectedValue

            'Get Race
            .Race = ddlRace.SelectedValue

            'Get MaritalStatus
            .MaritalStatus = ddlMaritalStatus.SelectedValue

            'Get Children
            .Children = txtChildren.Text

            'Get FamilyIncome
            .FamilyIncome = ddlFamilyIncome.SelectedValue

            'Get Phone
            .Phone = txtphone.Text
            .Phone2 = txtPhone2.Text
            .Zip = txtZip.Text

            'Get PhoneType
            .PhoneType = ddlPhoneType.SelectedValue
            .PhoneType2 = ddlPhoneType2.SelectedValue

            If rdoPhone1.Checked Then
                If .Phone = "" And .Phone2 <> "" Then
                    .DefaultPhone = 2
                Else
                    .DefaultPhone = 1
                End If

            End If

            If rdoPhone2.Checked Then
                .DefaultPhone = 2
            End If


            'Driver License State
            .DriverLicState = ddlDrivLicStateId.SelectedValue

            'Get Phone Status
            .PhoneStatus = ddlPhoneStatus.SelectedValue
            .PhoneStatus2 = ddlPhoneStatus2.SelectedValue


            'Get AddressStatus
            .AddressStatus = ddlAddressStatus.SelectedValue

            'Get HomeEmail
            .HomeEmail = txtHomeEmail.Text

            'Get WorkEmail
            .WorkEmail = txtWorkEmail.Text

            'Get Address1
            .Address1 = txtAddress1.Text

            'Get Address2
            .Address2 = txtAddress2.Text

            'Get City
            .City = txtCity.Text

            'Get State
            .State = ddlStateID.SelectedValue

            'Get Country 
            .Country = ddlCountry.SelectedValue

            'Get County
            .County = ddlCounty.SelectedValue

            '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
            .CampusId = ddlCampusId.SelectedValue

            'Get SourceCategory
            '.SourceCategory = ddlSourceCategoryId.Value

            'Get SourceDate 
            '.SourceDate = txtSourceDate.SelectedDate
            If txtSourceDate.SelectedDate Is Nothing Then
                .SourceDate = ""
            Else
                .SourceDate = Date.Parse(txtSourceDate.SelectedDate)
            End If


            ''Get SourceType
            '.SourceType = Me.__sourcetypeid.Value

            ''Get Area
            '.Area = ddlAreaId.Value

            ''SourceAdv
            '.SourceAdvertisement = Me.__sourceadvid.Value

            'Get ExpectedStart
            '.ExpectedStart = txtExpectedStart.SelectedDate
            If txtExpectedStart.SelectedDate Is Nothing Then
                .ExpectedStart = ""
            Else
                .ExpectedStart = Date.Parse(txtExpectedStart.SelectedDate)
            End If


            'Get ProgramID
            '.ProgramID = Me.__programid.Value

            'Get ShiftID
            .ShiftID = ddlShiftID.SelectedValue

            'Get Nationality
            .Nationality = ddlNationality.SelectedValue

            'Get Citizen
            .Citizen = ddlCitizen.SelectedValue

            'Get SSN 
            'ssnMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
            'If txtSSN.Text <> "" Then
            '    .SSN = facInputMask.RemoveMask(ssnMask, txtSSN.Text)
            'Else
            .SSN = txtSSN.Text
            'End If

            'Get DriverLicState 
            .DriverLicState = ddlDrivLicStateId.SelectedValue

            'Get DriverLicNumber
            .DriverLicNumber = txtDrivLicNumber.Text

            'Get AlienNumber
            .AlienNumber = txtAlienNumber.Text

            'Notes
            ''Comments saved with linebreakcharacters as \n
            ''Modified by Saraswathi on Nov 10 2008
            .Notes = txtComments.Text.Replace(vbCrLf, " \n ")


            'Get AddressTypes
            .AddressType = ddlAddressType.SelectedValue


            '.AssignmentDate = txtAssignedDate.SelectedDate
            If txtAssignedDate.SelectedDate Is Nothing Then
                .AssignmentDate = ""
            Else
                .AssignmentDate = Date.Parse(txtAssignedDate.SelectedDate)
            End If

            '.AppliedDate = txtDateApplied.SelectedDate
            If txtDateApplied.SelectedDate Is Nothing Then
                .AppliedDate = ""
            Else
                .AppliedDate = Date.Parse(txtDateApplied.SelectedDate)
            End If

            .PreviousEducation = ddlPreviousEducation.SelectedValue

            '.PrgVerId = Me.__prgverid.Value

            .OtherState = txtOtherState.Text

            If chkForeignPhone.Checked = True Then
                .ForeignPhone = 1
            Else
                .ForeignPhone = 0
            End If
            If chkForeignPhone2.Checked = True Then
                .ForeignPhone2 = 1
            Else
                .ForeignPhone2 = 0
            End If
            If chkForeignZip.Checked = True Then
                .ForeignZip = 1
            Else
                .ForeignZip = 0
            End If

            'Dependency Type
            .DependencyTypeId = ddlDependencyTypeId.SelectedValue

            'Geographic Type
            .GeographicTypeId = ddlGeographicTypeId.SelectedValue

            'Address Status
            .AddressStatus = ddlAddressStatus.SelectedValue

            'Phone Status
            .PhoneStatus = ddlPhoneStatus.SelectedValue
            .PhoneStatus2 = ddlPhoneStatus2.SelectedValue

            'Get Area,Program and Program version
            .Area = ddlAreaId.SelectedValue
            .ProgramID = ddlProgramID.SelectedValue
            .PrgVerId = ddlPrgVerId.SelectedValue


            'Get SourceCategory,Source Type and SourceAdvertisement
            .SourceCategory = ddlSourceCategoryId.SelectedValue
            .SourceType = ddlSourceTypeId.SelectedValue
            .SourceAdvertisement = ddlSourceAdvertisement.SelectedValue
            .InquiryTime = txtInquiryTime.Text
            .AdvertisementNote = txtAdvertisementNote.Text
            .AdminCriteriaId = ddlAdminCriteriaId.SelectedValue
            .HousingTypeId = ddlHousingId.SelectedValue
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                .EntranceInterviewDate = txtEntranceInterviewDate.SelectedDate
                .HighSchoolProgramCode = txtHighSchoolProgramCode.Text
            End If
            .DegCertSeekingId = ddlDegCertSeekingId.SelectedValue
        End With
        Return leadMaster
    End Function

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Dim LeadMasterUpdate As New LeadFacade
        Dim Result As String

        ''''' Code changes to Fix mantis issue id 19549 by kamalesh Ahuja on 16 Aug 2010 '''
        If MyAdvAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "no" Then
            If LeadMasterUpdate.CheckLeadUser(userId, txtLeadMasterID.Text) = 0 Then
                'DisplayErrorMessage("You do not have rights to edit this lead")
                DisplayRADAlert(CallbackType.Postback, "Error2", "You do not have rights to edit this lead ", "Save Error")
                Exit Sub
            End If
        End If
        '''''''''''''

        If Not txtBirthDate.SelectedDate Is Nothing And IsDate(txtBirthDate.SelectedDate) Then
            If DateDiff(DateInterval.Year, CDate(txtBirthDate.SelectedDate), CDate(txtDate.Text)) < 1 Then
                'DisplayErrorMessage("The DOB cannot be greater than or equal to today's date ")
                DisplayRADAlert(CallbackType.Postback, "Error3", "The DOB cannot be greater than or equal to today's date ", "Save Error")
                txtAge.Text = ""
                'Session("areaid_changed") = "no"
                'Session("programid_changed") = "no"
                'Session("sourcecatagoryid_changed") = "no"
                'Session("sourcetypeid_changed") = "no"
                Exit Sub
            End If

            Dim intAge As Integer
            Dim intConfigAge As Integer = MyAdvAppSettings.AppSettings("StudentAgeLimit")
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year
            Dim dob As Date = txtBirthDate.SelectedDate
            dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso _
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If
            'intAge = Now.Year - CDate(txtBirthDate.Text).Year
            If intAge < intConfigAge Then
                'DisplayErrorMessage("The minimum age requirement is " & intConfigAge)
                DisplayRADAlert(CallbackType.Postback, "Error4", "The minimum age requirement is " & intConfigAge, "Save Error")
                txtAge.Text = ""
                Exit Sub
            Else
                txtAge.Text = intAge
            End If
        End If

        ''Added by Saraswathi lakshmanan to validate the entry in children field. only numbers can be entered
        ''Added on Feb 24 2008
        If txtChildren.Text <> "" Then
            If Not IsNumeric(txtChildren.Text) Then
                'DisplayErrorMessage("The field children should contain numbers")
                DisplayRADAlert(CallbackType.Postback, "Error5", "The field children should contain numbers ", "Save Error")
                Exit Sub
            End If
        End If

        Dim strAddressType As String = String.Empty
        If ddlLeadStatus.SelectedValue = "" Then
            strAddressType = "Status is required" & vbLf
        End If


        If chkForeignZip.Checked = False Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not ddlStateID.SelectedValue = "" Or Not txtZip.Text = "" Or Not ddlCounty.SelectedValue = "" Then
                'If ddlAddressType.SelectedValue = "" Or ddlAddressType.SelectedValue = Guid.Empty.ToString Then
                '    strAddressType &= "Address type is required" & vbLf
                'End If
            End If
        End If

        If chkForeignZip.Checked = True Then
            If Not txtAddress1.Text = "" Or Not txtAddress2.Text = "" Or Not txtCity.Text = "" Or Not ddlCountry.SelectedValue = "" Or Not txtOtherState.Text = "" Or Not txtZip.Text = "" Or Not ddlCounty.SelectedValue = "" Then
                'If ddlAddressType.SelectedValue = "" Or ddlAddressType.SelectedValue = Guid.Empty.ToString Then
                '    strAddressType &= "Address type is required" & vbLf
                'End If
            End If
        End If

        If Not txtphone.Text = "" Then
            If ddlPhoneType.SelectedValue = "" Or ddlPhoneType.SelectedValue = "" Then
                strAddressType &= "Phone type is required" & vbLf
            End If
        End If
        If Not txtPhone2.Text = "" Then
            If ddlPhoneType2.SelectedValue = "" Or ddlPhoneType2.SelectedValue = "" Then
                strAddressType &= "Phone type is required" & vbLf
            End If
        End If
        If Not ddlPhoneType.SelectedValue = "" Then
            If txtphone.Text = "" Then
                strAddressType &= "Phone is required" & vbLf
            End If
        End If
        If Not ddlPhoneType2.SelectedValue = "" Then
            If txtPhone2.Text = "" Then
                strAddressType &= "Phone is required" & vbLf
            End If
        End If

        If Not strAddressType = "" Then
            'DisplayErrorMessage(strAddressType)
            DisplayRADAlert(CallbackType.Postback, "Error6", strAddressType, "Save Error")
            Exit Sub
        End If

        'Dim inquiryTime As String = ParseTime(txtInquiryTime.Text)
        'If inquiryTime = "" Then
        '    DisplayErrorMessage("Enter Proper Inquiry Time Format")
        'End If
        Dim inquiryTime As String = Now.Date + " " + txtInquiryTime.Text
        If Not CommonWebUtilities.IsValidDate(inquiryTime) Then
            'DisplayErrorMessage("Enter Proper Inquiry Time Format")
            DisplayRADAlert(CallbackType.Postback, "Error7", "Enter Proper Inquiry Time Format ", "Save Error")
            Exit Sub
        End If

        Dim errorMessage As String = String.Empty
        'Check If Mask is successful only if Foreign Is not checked
        'If chkForeignPhone.Checked = False Or chkForeignZip.Checked = False Then
        '    errorMessage = ValidateFieldsWithInputMasks()
        'Else
        '    errorMessage = ""
        'End If

        Dim intValidateLeadGroups As Integer

        intValidateLeadGroups = ValidateLeadGroups()
        'If intValidateLeadGroups = -1 Then
        '    DisplayErrorMessage("Please select a lead group")
        '    Exit Sub
        'End If
        If intValidateLeadGroups = -1 Then
            ''Student group corected to lead group
            ''Modified bySaraswathi lakshmanan on FEb 10 2011 for Rally case DE5033
            'DisplayErrorMessage("Please select a lead group")
            DisplayRADAlert(CallbackType.Postback, "Error8", "Please select a lead group ", "Save Error")
            Exit Sub
            '**This validation has been temporarily suspended 4/3/09 DD
        ElseIf intValidateLeadGroups >= 2 Then
            'More that one Student Group with the "Use For Scheduling" setting
            'has been selected.
            'DisplayErrorMessage("Please select only one Student Group that is to be used for scheduling")
            DisplayRADAlert(CallbackType.Postback, "Error9", "Please select only one Student Group that is to be used for scheduling ", "Save Error")
            Exit Sub
        End If
        '**This validation has been temporarily suspended 4/3/09 DD
        'If intValidateLeadGroups = 1 Then
        '    'More that one lead group with the "Use For Scheduling" setting
        '    'has been selected.
        '    DisplayErrorMessage("Please select only one lead group that is to be used for scheduling")
        '    Exit Sub
        'End If

        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            Dim strRegentmessage As String = ""
            If txtSSN.Text = "" Then
                strRegentmessage = "SSN is a required for Regent" & vbCrLf
            End If
            If txtBirthDate.SelectedDate Is Nothing Then
                strRegentmessage &= "DOB is a required for Regent" & vbCrLf
            End If
            If Not strRegentmessage = "" Then
                'DisplayErrorMessage(strRegentmessage)
                DisplayRADAlert(CallbackType.Postback, "Error10", strRegentmessage, "Save Error")
                Exit Sub
            End If
        End If

        If errorMessage = "" Then
            Dim boolRegent = MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes"
            'Call Update Function in the plEmployerInfoFacade
            Result = LeadMasterUpdate.UpdateLeadMaster(BuildLeadMaster(txtLeadMasterID.Text), AdvantageSession.UserState.UserName, txtResumeObjective.Text, boolRegent)

            If Result = "" Then

                UpdateLeadGroups(txtLeadMasterID.Text)

                '   save on syLeadStatusesChanges only if txtLeadStatusId (oldStatus) <> ddlLeadStatus.SelectedValue (newStatus)
                If ddlLeadStatus.SelectedValue <> txtLeadStatusId.Text Then
                    Dim statusChanged As New LeadStatusesInfo
                    With statusChanged
                        .LeadId = txtLeadMasterID.Text
                        .OrigStatusId = IIf(txtLeadStatusId.Text = "", Guid.Empty.ToString, txtLeadStatusId.Text)
                        .NewStatusId = ddlLeadStatus.SelectedValue
                        .ModDate = Date.Parse(txtModDate.Text)
                    End With
                    Result = (New LeadStatusChangeFacade).InsertLeadStatusChange(statusChanged, AdvantageSession.UserState.UserName)
                    If Not Result = "" Then
                        'DisplayErrorMessage(Result)
                        DisplayRADAlert(CallbackType.Postback, "Error11", Result, "Save Error")
                        Exit Sub
                    End If
                    txtLeadStatusId.Text = ddlLeadStatus.SelectedValue
                    BuildStatusDDL(ddlLeadStatus.SelectedValue)
                    'Change made on 08/16/2006 as the lead status was bringing in all lead status
                    'when page was loaded but when data is modified and save button was clicked
                    'behaves differently,the other statuses disappear and only brings in the 
                    'saved status.BuildStatusDDL() is called to keep the behaviour consistent.
                    'BuildStatusDDL("")
                    ddlLeadStatus.SelectedValue = txtLeadStatusId.Text
                    txtLeadStatusIdHidden.Text = ddlLeadStatus.SelectedValue
                End If
            Else
                'DisplayErrorMessage(Result)
                DisplayRADAlert(CallbackType.Postback, "Error12", Result, "Save Error")
                'Session("areaid_changed") = "no"
                'Session("programid_changed") = "no"
                'Session("sourcecatagoryid_changed") = "no"
                'Session("sourcetypeid_changed") = "no"
                Exit Sub
            End If

            chkIsInDB.Checked = True

            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Code For SchoolDefined Fields When Save Button Is Clicked

            Dim SDFID As ArrayList
            Dim SDFIDValue As ArrayList
            '            Dim newArr As ArrayList
            Dim z As Integer
            Dim SDFControl As New SDFComponent
            Try
                SDFControl.DeleteSDFValue(txtPKID.Text)
                SDFID = SDFControl.GetAllLabels(pnlSDF)
                SDFIDValue = SDFControl.GetAllValues(pnlSDF)
                For z = 0 To SDFID.Count - 1
                    SDFControl.InsertValues(txtPKID.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
                Next
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try

            'SchoolDefined Fields Code Ends Here 
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Else
            'DisplayErrorMessageMask(errorMessage)
            DisplayRADAlert(CallbackType.Postback, "Error13", errorMessage, "Save Error")
        End If
        'Session("areaid_changed") = "no"
        'Session("programid_changed") = "no"
        'Session("sourcecatagoryid_changed") = "no"
        'Session("sourcetypeid_changed") = "no"
    End Sub
    'Private Sub DisplayErrorMessageMask(ByVal errorMessage As String)
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub
    'Private Sub InitButtonsForEdit()
    '    'Enable The New and Delete Buttons
    '    btnNew.Enabled = False
    '    btnDelete.Enabled = True
    'End Sub
    'Private Function BuildLeadMaster(ByVal LeadMasterId As String) As LeadMasterInfo
    '    Dim LeadMaster As New LeadMasterInfo
    '    Dim facInputMask As New InputMasksFacade
    '    Dim phoneMask As String
    '    Dim zipMask As String
    '    Dim ssnMask As String
    '    With LeadMaster
    '        'get IsInDB
    '        .IsInDB = chkIsInDB.Checked

    '        'get LeadMasterID
    '        .LeadMasterID = txtLeadMasterID.Text

    '        'Get LastName
    '        .LastName = txtLastName.Text

    '        'Get FirstName
    '        .FirstName = txtFirstName.Text

    '        'Get MiddleName
    '        .MiddleName = txtMiddleName.Text

    '        'Get StatusId 
    '        .Status = ddlLeadStatus.SelectedValue
    '        txtLeadStatusIdHidden.Text = .Status


    '        'Get PrefixId 
    '        .Prefix = ddlPrefix.SelectedValue

    '        'Get SuffixId 
    '        .Suffix = ddlSuffix.SelectedValue

    '        'Lead Group
    '        '.LeadGrpId = ddlLeadGrpId.SelectedValue

    '        'CampusId
    '        '.CampusId = ddlCampusId.SelectedValue

    '        'Get Title 
    '        '.Title = ddlTitleId.SelectedValue

    '        'Get BirthDate
    '        .BirthDate = txtBirthDate.Text

    '        'Get Age
    '        '.Age = txtAge.Text

    '        'Get sponsor
    '        .Sponsor = ddlSponsor.SelectedValue

    '        'Get AdmissionsRep
    '        .AdmissionsRep = ddlAdmissionsRep.SelectedValue

    '        'Get Gender
    '        .Gender = ddlGender.SelectedValue

    '        'Get Race
    '        .Race = ddlRace.SelectedValue

    '        'Get MaritalStatus
    '        .MaritalStatus = ddlMaritalStatus.SelectedValue

    '        'Get Children
    '        .Children = txtChildren.Text

    '        'Get FamilyIncome
    '        .FamilyIncome = ddlFamilyIncome.SelectedValue

    '        'Get Phone
    '        phoneMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
    '        If txtPhone.Text <> "" And chkForeignPhone.Checked = False Then
    '            .Phone = facInputMask.RemoveMask(phoneMask, txtPhone.Text)
    '        Else
    '            .Phone = txtPhone.Text
    '        End If

    '        'Get Zip
    '        If txtZip.Text <> "" And chkForeignZip.Checked = False Then
    '            zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
    '            .Zip = facInputMask.RemoveMask(zipMask, txtZip.Text)
    '        Else
    '            .Zip = txtZip.Text
    '        End If

    '        'Get PhoneType
    '        .PhoneType = ddlPhoneType.SelectedValue

    '        'Driver License State
    '        .DriverLicState = ddlDrivLicStateId.SelectedValue

    '        'Get Phone Status
    '        .PhoneStatus = ddlPhoneStatus.SelectedValue


    '        'Get AddressStatus
    '        Try
    '            .AddressStatus = ddlAddressStatus.SelectedValue
    '        Catch ex As System.Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            BuildAddressStatusesDDL()
    '        End Try

    '        'Get HomeEmail
    '        .HomeEmail = txtHomeEmail.Text

    '        'Get WorkEmail
    '        .WorkEmail = txtWorkEmail.Text

    '        'Get Address1
    '        .Address1 = txtAddress1.Text

    '        'Get Address2
    '        .Address2 = txtAddress2.Text

    '        'Get City
    '        .City = txtCity.Text

    '        'Get State
    '        .State = ddlStateID.SelectedValue

    '        'Get Country 
    '        .Country = ddlCountry.SelectedValue

    '        'Get County
    '        .County = ddlCounty.SelectedValue

    '        'Get SourceCategory
    '        .SourceCategory = ddlSourceCategoryId.Value

    '        'Get SourceDate 
    '        .SourceDate = txtSourceDate.Text


    '        'Get SourceType
    '        .SourceType = Me.__sourcetypeid.Value

    '        'Get Area
    '        .Area = ddlAreaId.Value

    '        'SourceAdv
    '        .SourceAdvertisement = Me.__sourceadvid.Value

    '        'Get ExpectedStart
    '        .ExpectedStart = txtExpectedStart.Text


    '        'Get ProgramID
    '        .ProgramID = Me.__programid.Value

    '        'Get ShiftID
    '        .ShiftID = ddlShiftID.SelectedValue

    '        'Get Nationality
    '        .Nationality = ddlNationality.SelectedValue

    '        'Get Citizen
    '        .Citizen = ddlCitizen.SelectedValue

    '        'Get SSN 
    '        ssnMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
    '        If txtSSN.Text <> "" Then
    '            .SSN = facInputMask.RemoveMask(ssnMask, txtSSN.Text)
    '        Else
    '            .SSN = txtSSN.Text
    '        End If

    '        'Get DriverLicState 
    '        .DriverLicState = ddlDrivLicStateId.SelectedValue

    '        'Get DriverLicNumber
    '        .DriverLicNumber = txtDrivLicNumber.Text

    '        'Get AlienNumber
    '        .AlienNumber = txtAlienNumber.Text

    '        'Notes
    '        .Notes = txtComments.Text

    '        'Get AddressTypes
    '        .AddressType = ddlAddressType.SelectedValue


    '        .AssignmentDate = txtAssignedDate.Text


    '        .PreviousEducation = ddlPreviousEducation.SelectedValue

    '        .PrgVerId = Me.__prgverid.Value

    '        .OtherState = txtOtherState.Text

    '        If chkForeignPhone.Checked = True Then
    '            .ForeignPhone = 1
    '        Else
    '            .ForeignPhone = 0
    '        End If

    '        If chkForeignZip.Checked = True Then
    '            .ForeignZip = 1
    '        Else
    '            .ForeignZip = 0
    '        End If

    '        .DependencyTypeId = ddlDependencyTypeId.SelectedValue
    '        .GeographicTypeId = ddlGeographicTypeId.SelectedValue
    '        '.AdminCriteriaId = ddlAdminCriteriaId.SelectedValue

    '        'Address Status
    '        .AddressStatus = ddlAddressStatus.SelectedValue

    '        'Phone Status
    '        .PhoneStatus = ddlPhoneStatus.SelectedValue

    '    End With
    '    Return LeadMaster
    'End Function
    Private Function BindLeadMasterData(ByVal LeadMaster As LeadMasterInfo) As LeadMasterInfo
        ''Bind The EmployerInfo Data From The Database
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        Dim ssnMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)


        'BuildStatusDDL()
        With LeadMaster

            'get IsInDB
            chkIsInDB.Checked = .IsInDB

            'get LeadMasterID
            txtLeadMasterID.Text = .LeadMasterID

            'Get LastName
            txtLastName.Text = .LastName

            'Get FirstName
            txtFirstName.Text = .FirstName

            'Get MiddleName
            txtMiddleName.Text = .MiddleName

            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPrefix, .Prefix, .PrefixDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSuffix, .Suffix, .SuffixDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdmissionsRep, .AdmissionsRep, .AdmissionRepsDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGender, .Gender, .GenderDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlRace, .Race, .EthCodeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlMaritalStatus, .MaritalStatus, .MaritalStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlFamilyIncome, .FamilyIncome, .FamilyIncomeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneType, .PhoneType, .PhoneTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneType2, .PhoneType2, .PhoneTypeDescrip2)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateID, .State, .StateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountry, .Country, .CountryDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCounty, .County, .CountyDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPreviousEducation, .PreviousEducation, .EdLvlDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlShiftID, .ShiftID, .ShiftDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlNationality, .Nationality, .NationalityDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCitizen, .Citizen, .CitizenDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDrivLicStateId, .DriverLicState, .DriverLicenseStateDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressType, .AddressType, .AddressTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus, .PhoneStatus, .PhoneStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPhoneStatus2, .PhoneStatus2, .PhoneStatusDescrip2)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAddressStatus, .AddressStatus, .AddressStatusDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlDependencyTypeId, .DependencyTypeId, .DependencyTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGeographicTypeId, .GeographicTypeId, .GeographicTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlSponsor, .Sponsor, .SponsorTypeDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAdminCriteriaId, .AdminCriteriaId, .AdminCriteriaDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlHousingId, .HousingTypeId, .HousingTypeDescrip)

            'Get BirthDate
            'txtBirthDate.Text = .BirthDate
            Try
                txtBirthDate.SelectedDate = .BirthDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtBirthDate.Clear()
            End Try

            'Get Age
            If Not txtBirthDate.SelectedDate Is Nothing Then
                'txtAge.Text = Math.Floor(DateDiff(DateInterval.Day, CDate(txtBirthDate.Text), Now) / 365)
                Dim intAge As Integer
                Dim dob As Date = txtBirthDate.SelectedDate
                dob.ToString("yyyy/MM/dd")
                intAge = Today.Year - dob.Year
                If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso _
                    dob.Day <> Today.Day) Then
                    'The current year is not yet complete. 
                    intAge -= 1
                End If
                txtAge.Text = intAge 'Math.Floor(DateDiff(DateInterval.Day, CDate(txtBirthDate.Text), Now) / 365)
            Else
                txtAge.Text = ""
            End If

            'Get sponsor
            ddlSponsor.SelectedValue = .Sponsor

            'Get Children
            txtChildren.Text = .Children

            'Get Phone
            txtphone.Text = .Phone
            txtPhone2.Text = .Phone2
            If txtphone.Text <> "" And .ForeignPhone = 0 Then
                txtphone.Mask = "(###)-###-####"
                txtphone.DisplayMask = "(###)-###-####"
                txtphone.DisplayPromptChar = ""
            Else
                txtphone.Text = ""
                txtphone.Mask = "aaaaaaaaaaaaaaaaaaaa"
                txtphone.DisplayMask = ""
                txtphone.DisplayPromptChar = ""
            End If
            If txtPhone2.Text <> "" And .ForeignPhone2 = 0 Then
                txtPhone2.Mask = "(###)-###-####"
                txtPhone2.DisplayMask = "(###)-###-####"
                lblPhone2.ToolTip = strMask
            Else
                txtPhone2.Mask = "aaaaaaaaaaaaaaaaaaaa"
                txtPhone2.DisplayMask = ""
                lblPhone2.ToolTip = ""
            End If
            'Get HomeEmail
            txtHomeEmail.Text = .HomeEmail

            'Get WorkEmail
            txtWorkEmail.Text = .WorkEmail

            'Get Address1
            txtAddress1.Text = .Address1

            'Get Address2
            txtAddress2.Text = .Address2

            'Get City
            txtCity.Text = .City

            'Get Zip

            If .Zip <> "" And .ForeignZip = 0 Then
                txtZip.Mask = "#####"
                lblZip.ToolTip = zipMask
            Else
                txtZip.Mask = "aaaaaaaaaaaaaaaaaaaa"
                lblZip.ToolTip = ""
            End If
            txtZip.Text = .Zip

            'Get SourceDate 
            'txtSourceDate.SelectedDate = .SourceDate
            Try
                txtSourceDate.SelectedDate = .SourceDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtSourceDate.Clear()
            End Try

            'Get ExpectedStart
            'txtExpectedStart.SelectedDate = .ExpectedStart
            Try
                txtExpectedStart.SelectedDate = .ExpectedStart
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtExpectedStart.Clear()
            End Try

            'Assignment Date
            'txtAssignedDate.SelectedDate = .AssignmentDate
            Try
                txtAssignedDate.SelectedDate = .AssignmentDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtAssignedDate.Clear()
            End Try

            'txtDateApplied.SelectedDate = .AppliedDate
            Try
                txtDateApplied.SelectedDate = .AppliedDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtDateApplied.Clear()
            End Try


            If Not txtDateApplied.SelectedDate Is Nothing Then
                txtDateApplied.SelectedDate = FormatDateTime(txtDateApplied.SelectedDate, DateFormat.ShortDate)
            End If

            If Not txtAssignedDate.SelectedDate Is Nothing Then
                txtAssignedDate.SelectedDate = FormatDateTime(txtAssignedDate.SelectedDate, DateFormat.ShortDate)
            End If
            ''Code Added By Vijay Ramteke On Auagust 11, 2010 To Fix Mantis Id 19507
            '' Code Added by kamalesh Ahuja on 10 June 2010 to Resolve mantis issue id 18411
            ''ddlCampusId.SelectedValue = .CampusId
            Try
                ddlCampusId.SelectedValue = .CampusId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlCampusId.SelectedValue = CampusId
            End Try
            ''Code Added By Vijay Ramteke On Auagust 11, 2010 To Fix Mantis Id 19507

            'Get SSN 
            txtSSN.Text = .SSN
            'If txtSSN.Text <> "" Then
            '    txtSSN.Text = facInputMasks.ApplyMask(ssnMask, txtSSN.Text)
            'End If

            'Get DriverLicNumber
            txtDrivLicNumber.Text = .DriverLicNumber

            'Get AlienNumber
            txtAlienNumber.Text = .AlienNumber

            'Notes
            ''Comments  with \n characters as linebreakcharacters vbcrlf
            ''and showed in different lines
            ''Modified by Saraswathi on Nov 10 2008
            txtComments.Text = .Notes.Replace("\n", vbCrLf)

            'Status
            'Build Status DDL based on syLeadStatusChanges table + current status.
            If Not .Status = "" Then
                BuildStatusDDL(.Status)
                ddlLeadStatus.SelectedValue = .Status
                txtLeadStatusId.Text = .Status      'store current status on hidden textbox
                txtLeadStatusIdHidden.Text = .Status ' store in hidden status textbox for validating enroll lead
            End If

            ddlAreaId.SelectedValue = .Area
            'Get ProgramID
            If Not .Area = "" Then
                BuildProgramsDDL(ddlAreaId.SelectedValue, True)
                ddlProgramID.SelectedValue = .ProgramID
            End If

            'Get Program Version
            If Not .ProgramID = "" Then
                BuildPrgVersionDDL(True)
                ddlPrgVerId.SelectedValue = .PrgVerId
            End If

            'Get SourceCategory
            ddlSourceCategoryId.SelectedValue = .SourceCategory

            'Get SourceType
            If Not .SourceCategory = "" Then
                BuildSourceTypeDDL(ddlSourceCategoryId.SelectedValue, True)
                ddlSourceTypeId.SelectedValue = .SourceType
            End If

            'SourceAdvertisement
            If Not .SourceType = "" Then        'Or
                BuildSourceAdvDDL(ddlSourceTypeId.SelectedValue, True)
                ddlSourceAdvertisement.SelectedValue = .SourceAdvertisement
            End If


            If .ForeignPhone = True Then
                chkForeignPhone.Checked = True
            Else
                chkForeignPhone.Checked = False
            End If
            If .ForeignPhone2 = True Then
                chkForeignPhone2.Checked = True
            Else
                chkForeignPhone2.Checked = False
            End If
            If .DefaultPhone = 1 Then rdoPhone1.Checked = True
            If .DefaultPhone = 2 Then rdoPhone2.Checked = True

            If .ForeignZip = True Then
                chkForeignZip.Checked = True
                txtOtherState.Text = .OtherState
                txtOtherState.Visible = True
                lblOtherState.Visible = True
                ddlStateID.Enabled = False
            Else
                chkForeignZip.Checked = False
                txtOtherState.Visible = False
                lblOtherState.Visible = False
                ddlStateID.Enabled = True
            End If
            txtModDate.Text = .ModDate

            'If InStr(.StatusDescrip, "Enroll") >= 1 Then
            If .IsStatusEnrolled = True Then  'if lead was enrolled then disable the dropdown
                ddlLeadStatus.Enabled = False
            End If

            If .InquiryTime IsNot Nothing Then
                txtInquiryTime.Text = .InquiryTime
            End If

            txtAdvertisementNote.Text = .AdvertisementNote

            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                txtEntranceInterviewDate.SelectedDate = .EntranceInterviewDate
                txtHighSchoolProgramCode.Text = .HighSchoolProgramCode
            End If
            ddlDegCertSeekingId.SelectedValue = .DegCertSeekingId
        End With
        Return LeadMaster
    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        ''Assign A New Value For Primary Key Column
        'txtLeadMasterID.Text = Guid.NewGuid.ToString()

        ''Reset The Value of chkIsInDb Checkbox To Identify an Insert
        'chkIsInDB.Checked = False

        ''Create a Empty Object and Initialize the Object
        'BindLeadMasterDataItemCommand(New LeadMasterInfo)

        'Dim SDFControls As New SDFComponent
        'SDFControls.GenerateControlsNew(pnlSDF, ResourceID, ModuleId)

        ''Initialize Buttons
        'InitButtonsForLoad()

        ''Initialize SourceDate and ExpectedStart Date
        'txtSourceDate.Text = ""
        'txtExpectedStart.Text = ""
    End Sub
    Private Sub InitButtonsForLoad()
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
        If Not (txtLeadMasterID.Text = "") Then
            'instantiate component
            Dim LeadMaster As New LeadFacade

            If LeadMaster.IsLeadEnrolled(txtLeadMasterID.Text) Then
                'DisplayErrorMessage("Lead cannot be deleted because it has at least one enrollment.")
                DisplayRADAlert(CallbackType.Postback, "Error14", "Lead cannot be deleted because it has at least one enrollment. ", "Delete Error")
                Exit Sub
            End If

            'Delete The Row Based on EmployerId 
            Dim result As String = LeadMaster.DeleteLeadMaster(txtLeadMasterID.Text, Date.Parse(txtModDate.Text))

            'If Delete Fails
            If result <> "" Then
                '   Display Error Message
                ' DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error15", result, "Delete Error")
                Exit Sub
            Else
                chkIsInDB.Checked = False

                Dim lscfac As New LeadStatusChangeFacade
                lscfac.DeleteLeadStatusChange(txtLeadMasterID.Text)

                'bind an empty new BankAcctInfo
                BindLeadMasterData(New LeadMasterInfo)

                'initialize buttons
                InitButtonsForLoad()

                'Initialize SourceDate and ExpectedStart Date
                txtSourceDate.Clear()
                txtExpectedStart.Clear()

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
                'SDF Code Starts Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim SDFControl As New SDFComponent
                SDFControl.DeleteSDFValue(txtLeadMasterID.Text)
                SDFControl.GenerateControlsNew(pnlSDF, ResourceID, "AD")
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                'SDF Code Ends Here
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                'DisplayErrorMessage("The lead was sucessfully deleted")
                DisplayRADAlert(CallbackType.Postback, "Error16", "The lead was sucessfully deleted ", "Delete Error")
                Response.Redirect("../pl/searchlead.aspx?resid=153&mod=AD&cmpid=" & CampusId)
            End If
        End If
    End Sub
    'Private Sub BindLeadMasterDataItemCommand(ByVal LeadMaster As LeadMasterInfo)
    '    ''Bind The EmployerInfo Data From The Database
    '    Dim facInputMasks As New InputMasksFacade
    '    Dim strMask As String
    '    Dim zipMask As String

    '    'Get the mask for phone numbers and zip
    '    strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
    '    zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

    '    With LeadMaster
    '        'get IsInDB
    '        chkIsInDB.Checked = .IsInDB

    '        'get LeadMasterID
    '        txtLeadMasterID.Text = .LeadMasterID

    '        'Get LastName
    '        txtLastName.Text = .LastName

    '        'Get FirstName
    '        txtFirstName.Text = .FirstName

    '        'Get MiddleName
    '        txtMiddleName.Text = .MiddleName

    '        'Get BirthDate
    '        txtBirthDate.Text = .BirthDate

    '        'Get Age
    '        txtAge.Text = ""

    '        txtZip.Text = .Zip
    '        'If txtZip.Text <> "" Then
    '        '    txtZip.Text = facInputMasks.ApplyMask(zipMask, txtZip.Text)
    '        'End If

    '        txtPhone.Text = .Phone
    '        'If txtPhone.Text <> "" Then
    '        '    txtPhone.Text = facInputMasks.ApplyMask(strMask, txtPhone.Text)
    '        'End If

    '        'Get HomeEmail
    '        txtHomeEmail.Text = .HomeEmail

    '        'Get WorkEmail
    '        txtWorkEmail.Text = .WorkEmail

    '        'Get Address1
    '        txtAddress1.Text = .Address1

    '        'Get Address2
    '        txtAddress2.Text = .Address2

    '        'Get City
    '        txtCity.Text = .City

    '        'Get State
    '        txtBirthDate.Text = ""

    '        'Get SourceDate 
    '        txtSourceDate.Text = .SourceDate

    '        'Get ExpectedStart
    '        txtExpectedStart.Text = .ExpectedStart

    '        'Get Assignment Date
    '        txtAssignedDate.Text = .AssignmentDate

    '        'Get SSN 
    '        txtSSN.Text = .SSN

    '        'Get DriverLicNumber
    '        txtDrivLicNumber.Text = .DriverLicNumber

    '        'Get AlienNumber
    '        txtAlienNumber.Text = .AlienNumber

    '        'Get Notes
    '        txtComments.Text = .Notes

    '        chkForeignPhone.Checked = False

    '        'Populate Lead Status DDL with statuses that map to system status 'New Lead'
    '        BuildStatusDDL(.StatusCodeId)
    '        'Clear out hidden field LeadStatusId
    '        txtLeadStatusId.Text = .StatusCodeId

    '        ddlDependencyTypeId.SelectedIndex = 0
    '        ddlGeographicTypeId.SelectedIndex = 0
    '        ddlAdminCriteriaId.selectedIndex = 0


    '    End With

    '    'BuildDropDownLists()
    'End Sub
    Private Sub txtBirthDate_SelectedDateChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtBirthDate.SelectedDateChanged
        If Not txtBirthDate.SelectedDate Is Nothing Then
            If DateDiff(DateInterval.Year, CDate(txtBirthDate.SelectedDate), CDate(txtDate.Text)) < 1 Then
                'DisplayErrorMessage("The minimum age requirement for student is 18 ")
                DisplayRADAlert(CallbackType.Postback, "Error17", "The minimum age requirement for student is 18 ", "Date Error")
                Exit Sub
            End If

            Dim intAge As Integer

            'intAge = Now.Year - CDate(txtBirthDate.Text).Year - 1
            Dim dob As Date = txtBirthDate.SelectedDate
            dob.ToString("yyyy/MM/dd")
            intAge = Today.Year - dob.Year
            If dob.Month > Today.Month OrElse (dob.Month = Today.Month AndAlso dob.Day > Today.Day AndAlso _
                dob.Day <> Today.Day) Then
                'The current year is not yet complete. 
                intAge -= 1
            End If
            txtAge.Text = CType(intAge, String) 'Math.Floor(DateDiff(DateInterval.Day, CDate(txtBirthDate.Text), Now) / 365)
        End If
    End Sub
    Private Sub txtSourceDate_SelectedDateChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtSourceDate.SelectedDateChanged
        'If Trim(txtSourceDate.Text.Substring(1, 2)) = "12" Then
        '    txtSourceDate.Text = ""
        'End If
    End Sub
    'Private Sub BuildStatusDDL()
    '    Dim Status As New LeadFacade
    '    With ddlLeadStatus
    '        .DataTextField = "StatusCodeDescrip"
    '        .DataValueField = "StatusCodeID"
    '        .DataSource = Status.GetReassignLeadStatus
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '    End With
    'End Sub
    Private Sub BuildStatusDDL(ByVal leadStatusId As String)
        Dim facade As New LeadStatusChangeFacade
        With ddlLeadStatus
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            .DataSource = facade.GetExistingLeadStatuses(CampusId, userId, leadStatusId, LeadId) 'facade.GetAvailLeadStatuses(leadStatusId, CampusId, userId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)

    '    '   Set error condition
    '    Customvalidator1.ErrorMessage = errorMessage
    '    Customvalidator1.IsValid = False

    '    If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
    '        '   Display error in message box in the client
    '        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    '    End If
    'End Sub
    Private Sub chkForeignZip_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignZip.CheckedChanged
        'Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtAddress1)
        If chkForeignZip.Checked = True Then
            lblOtherState.Enabled = True
            txtZip.Mask = "aaaaaaaaaaaaaaaaaaaa"
            txtZip.DisplayMask = ""
            txtZip.Text = String.Empty
            ddlStateID.SelectedIndex = 0
        Else
            txtZip.Mask = "#####"
            txtZip.DisplayMask = "#####"
            txtZip.Text = String.Empty
        End If
    End Sub
    Private Sub chkForeignPhone_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignPhone.CheckedChanged
       ' Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtphone)
    End Sub
    Private Sub chkForeignPhone2_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkForeignPhone2.CheckedChanged
        'Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtPhone2)
    End Sub
    Private Sub txtOtherState_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtOtherState.TextChanged
        'Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtZip)
    End Sub
    Private Sub txtCity_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtCity.TextChanged
        If chkForeignZip.Checked = True Then
            'Dim objCommon As New CommonWebUtilities
            CommonWebUtilities.SetFocus(Me.Page, txtOtherState)
        End If
    End Sub
    Private Sub lnkOpenRequirements_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkOpenRequirements.Click
        'Dim LeadMasterUpdate As New LeadFacade
        Dim TestRequired As New CheckLeadTest
        Dim strTestRequiredResult As String
        '        Dim Result As String
        Dim strDocRequired As String


        'Check If Lead Has Not Taken a Required Test or Test Has not been passed
        Dim strTestStatus = "", strDocStatus = "", strEnrollStatus As String
        'If The Program Version is not blank
        'If ddlPrgVerId.SelectedValue = "" Then
        '    DisplayErrorMessage("Please select Program version to view the Admission Requirements ")
        '    Exit Sub
        'End If

        If Not ddlPrgVerId.SelectedValue = "" Then
            'Check If Lead Has Not Taken a Required Test or Test Has not been passed
            strTestStatus = TestRequired.CheckIfRequiredTest(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            'If the Lead Has not taken a required doc or has an document not  approved
            strDocStatus = TestRequired.CheckRequiredDocuments(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            Dim AdReqs As New AdReqsFacade
            Dim strReqsMetStatus As String
            strReqsMetStatus = AdReqs.CheckIfReqGroupMeetsConditions(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            If strTestStatus = "Enroll" And strDocStatus = "Enroll" And strReqsMetStatus = "Enroll" Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If
            OpenAdmissionRequirementsPagePopUp(LeadId, ddlPrgVerId.SelectedValue, strEnrollStatus)
        Else
            'If The Program Version is blank
            strTestRequiredResult = TestRequired.CheckIfRequiredTestNoProgramVersion(LeadId)
            If strTestRequiredResult = "Enroll" Then
                strTestStatus = "Enroll"
            End If

            'If the Lead Has not taken a required doc or has an document not  approved
            strDocRequired = TestRequired.CheckRequiredDocumentsNoProgramVersion(LeadId)
            If strDocRequired = "Enroll" Then
                strDocStatus = "Enroll"
            End If

            If strTestStatus = "Enroll" And strDocStatus = "Enroll" Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If
            OpenAdmissionRequirementsPagePopUp(LeadId, ddlPrgVerId.SelectedValue, strEnrollStatus)
        End If
    End Sub
    Private Sub OpenAdmissionRequirementsPagePopUp(ByVal LeadId As String, ByVal PrgVerId As String, ByVal strEnrollStatus As String)
        '   setup the properties of the new window
        Dim sb As New StringBuilder

        '   append ReqGrpId to the querystring
        sb.Append("&LeadId=" + LeadId)
        '   append ReqGrpId Description to the querystring
        If PrgVerId = "" Then
            sb.Append("&PrgVerId=None")
        Else
            sb.Append("&PrgVerId=" + PrgVerId)
        End If

        Dim strLeadName As String = Trim(txtFirstName.Text) + " " & Trim(txtMiddleName.Text) + " " + Trim(txtLastName.Text)

        sb.Append("&LeadName=" + strLeadName)
        sb.Append("&EnrollStatus=" + strEnrollStatus)
        sb.Append("&PrgVersion=" + ddlPrgVerId.SelectedItem.Text)
        sb.Append("&CampusId=" + CampusId)

        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsAdReqs
        Dim name As String = "AdmReqSummary1"
        Dim url As String = "../AD/RequirementsDisplay.aspx?resid=283&mod=AD" + sb.ToString
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
    Private Sub BindLeadGroupList(ByVal LeadId As String)

        'Get Degrees data to bind the CheckBoxList
        Dim Facade As New AdReqsFacade
        Dim ds As DataSet = Facade.GetLeadgroupsselected(LeadId)
        Dim JobCount As Integer = Facade.GetLeadGroupCount(LeadId)

        If JobCount >= 1 Then

            'Select the items on the CheckBoxList
            Dim i As Integer
            If Not (ds.Tables(0).Rows.Count < 0) Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    Dim row As DataRow
                    row = ds.Tables(0).Rows(i)
                    Dim item As ListItem
                    For Each item In chkLeadGrpId.Items
                        If item.Value.ToString = DirectCast(row("LeadGrpId"), Guid).ToString Then
                            item.Selected = True
                            Exit For
                        End If
                    Next
                Next
            End If
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        If Not IsPostBack Then
            Dim ds As DataSet = (New LeadFacade).GetLeadsByAdmissionsRep(ddlAdmissionsRep.SelectedValue, ddlLeadStatus.SelectedValue)
            ViewState("ListOfLeads") = ds
        End If

        If Not Trim(Request.Form("scrollposition")) = "" Then
            Dim i As Integer = CInt(Trim(Request.Form("scrollposition")))
            Session("ScrollValue") = i
        End If

        'set previous button 
        btnPrevious.Enabled = Not IsFirstLead(LeadId) And Not IsEmpty(LeadId)

        'set next button
        btnNext.Enabled = Not IsLastLead(LeadId) And Not IsEmpty(LeadId)

        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(lnkEnrollLead)
        controlsToIgnore.Add(btnNext)
        controlsToIgnore.Add(btnPrevious)
        controlsToIgnore.Add(lnkOpenRequirements)
        controlsToIgnore.Add(txtSourceDate)
        controlsToIgnore.Add(txtBirthDate)
        controlsToIgnore.Add(txtExpectedStart)
        controlsToIgnore.Add(txtAssignedDate)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)


        BindToolTip()
    End Sub
    Private Function IsFirstLead(ByVal leadId As String) As Boolean
        If ViewState("ListOfLeads") Is Nothing Then Return False
        Dim ds As DataSet = ViewState("ListOfLeads")
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(0)("LeadId") = New Guid(leadId) Then Return True Else Return False
        Else
            Return False
        End If
    End Function
    Private Function IsLastLead(ByVal leadId As String) As Boolean
        If ViewState("ListOfLeads") Is Nothing Then Return False
        Dim ds As DataSet = ViewState("ListOfLeads")
        If ds.Tables(0).Rows.Count > 0 Then
            If ds.Tables(0).Rows(ds.Tables(0).Rows.Count - 1)("LeadId") = New Guid(leadId) Then Return True Else Return False
        Else
            Return False
        End If
    End Function
    Private Function IsEmpty(ByVal leadId As String) As Boolean
        If ViewState("ListOfLeads") Is Nothing Then Return True
        Dim ds As DataSet = ViewState("ListOfLeads")
        If ds.Tables(0).Rows.Count > 0 Then
            Return False
        Else
            Return True
        End If
    End Function
    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrevious.Click
        LeadId = GetPreviousLead(LeadId)
        SetStateInfo(LeadId)
        GetLeadInfo(LeadId)
        BindLeadGroupList(LeadId)
        txtLeadMasterID.Text = LeadId
        chkIsInDB.Checked = True
    End Sub
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click
        LeadId = GetNextLead(LeadId)
        SetStateInfo(LeadId)
        GetLeadInfo(LeadId)
        BindLeadGroupList(LeadId)
        txtLeadMasterID.Text = LeadId
        chkIsInDB.Checked = True
    End Sub
    Private Function GetPreviousLead(ByVal leadId As String) As String
        If ViewState("ListOfLeads") Is Nothing Then Return ""
        Dim ds As DataSet = ViewState("ListOfLeads")
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim row As DataRow = ds.Tables(0).Rows(i)
                If row("LeadId") = New Guid(leadId) Then
                    If i > 0 Then
                        Return CType(ds.Tables(0).Rows(i - 1)("LeadId"), Guid).ToString
                    Else
                        Return ""
                    End If
                End If
            Next
        End If

        'nothing found
        Return ""
    End Function
    Private Function GetNextLead(ByVal leadId As String) As String
        If ViewState("ListOfLeads") Is Nothing Then Return ""
        Dim ds As DataSet = ViewState("ListOfLeads")
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim row As DataRow = ds.Tables(0).Rows(i)
                If row("LeadId") = New Guid(leadId) Then
                    If i < ds.Tables(0).Rows.Count - 1 Then
                        Return CType(ds.Tables(0).Rows(i + 1)("LeadId"), Guid).ToString
                    Else
                        Return ""
                    End If
                End If
            Next
        End If

        'nothing found
        Return ""
    End Function

    Private Sub SetStateInfo(ByVal leadId As String)
        Dim objStateInfo As New AdvantageStateInfo
        Dim state As AdvantageSessionState
        Dim arrUPP As ArrayList
        Dim pURL As String
        Dim strVID As String
        Dim fac As New UserSecurityFacade
        Dim defaultCampusId As String

        defaultCampusId = HttpContext.Current.Request.Params("cmpid")
        defaultCampusId = XmlConvert.ToGuid(defaultCampusId).ToString

        Session("SEARCH") = 1
        'Set relevant properties on the state object
        objStateInfo.LeadId = leadId
        objStateInfo.NameCaption = "Lead : "
        objStateInfo.NameValue = (New StudentSearchFacade).GetLeadNameByID(leadId)

        'Create a new guid to be associated with the employer pages
        strVID = Guid.NewGuid.ToString
        Session("LeadObjectPointer") = strVID ' This step is required, as master page loses values stored in class variables

        'Add an entry to AdvantageSessionState for this guid and object
        'load Advantage state
        state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        state(strVID) = objStateInfo

        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        'Redirect To Student Lead Education
        'Response.Redirect("../AD/LeadMaster1.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)
        arrUPP = fac.GetUserResourcePermissionsForSubModule(userId, 395, "Admissions", HttpContext.Current.Request.Params("cmpid"))
        If arrUPP.Count = 0 Then
            'User does not have permission to any resource for this submodule
            'Session("Error") = "You do not have permission to any of the pages for existing leads<br> for the campus that you are logged in to."
            'Response.Redirect("../ErrorPage.aspx")
            RadNotification1.Show()
            RadNotification1.Text = "You do not have permission to any of the pages for existing leads for the campus that you are logged in to."
        ElseIf fac.DoesUserHasAccessToSubModuleResource(arrUPP, 170) Then
            ' Response.Redirect("../AD/LeadMaster1.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=4", True)
            Response.Redirect("../AD/ALeadInfoPage.aspx?resid=170&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID + "&Type=4", True)

        Else
            'redirect to the first page that the user has permission to for the submodule
            pURL = BuildPartialURL(arrUPP(0))
            Response.Redirect(pURL & "&mod=AD&cmpid=" + defaultCampusId + "&VID=" + strVID, True)
        End If

    End Sub
    Private Function BuildPartialURL(ByVal uppInfo As UserPagePermissionInfo) As String
        Return uppInfo.Url & "?resid=" & uppInfo.ResourceId
    End Function

    Protected Sub lnkEnrollLead_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkEnrollLead.Click
        Dim intLeadStatusMappedToEnrollment As Integer = 0
        Dim strMessage As String = ""
        intLeadStatusMappedToEnrollment = (New LeadStatusChangeFacade).CheckIfLeadCanBeEnrolledByStatus(userId, CampusId, txtLeadStatusIdHidden.Text)
        If intLeadStatusMappedToEnrollment >= 1 Then
            Response.Redirect("../ad/leadenrollments.aspx?resid=174&mod=AD&cmpid=" + CampusId + "&enid=" + txtLeadMasterID.Text)
        Else
            strMessage &= "The Current Lead Status needs to be mapped to Enrolled Status for the lead to be enrolled" & vbLf
            'DisplayErrorMessage(strMessage)
            DisplayRADAlert(CallbackType.Postback, "Error18", strMessage, "Enroll Error")
            Exit Sub
        End If
    End Sub

    Protected Sub dlstEmployerContact_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstEmployerContact.ItemCommand
    End Sub

    Private Sub ddlSourceCategoryId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlSourceCategoryId.SelectedIndexChanged
        If Not ddlSourceCategoryId.SelectedIndex = 0 Then
            BuildSourceTypeDDL(ddlSourceCategoryId.SelectedValue)
            ddlSourceAdvertisement.Items.Clear()
            With ddlSourceAdvertisement
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            ddlSourceTypeId.Items.Clear()
            With ddlSourceTypeId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            ddlSourceAdvertisement.Items.Clear()
            With ddlSourceAdvertisement
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Private Sub ddlSourceTypeId_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlSourceTypeId.SelectedIndexChanged
        If Not ddlSourceTypeId.SelectedIndex = 0 Then
            BuildSourceAdvDDL(ddlSourceTypeId.SelectedValue)
        End If
    End Sub
    Private Sub ddlAreaID_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAreaId.SelectedIndexChanged
        If Not ddlAreaId.SelectedValue = "" Then
            BuildProgramsDDL(ddlAreaId.SelectedValue)
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            ddlProgramID.Items.Clear()
            With ddlProgramID
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Protected Sub ddlProgramID_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlProgramID.SelectedIndexChanged
        If Not ddlProgramID.SelectedIndex = 0 Then
            BuildPrgVersionDDL()
        Else
            ddlPrgVerId.Items.Clear()
            With ddlPrgVerId
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    'Private Sub IPEDSRequirements()
    '    isIPEDSApplicable = MyAdvAppSettings.AppSettings("IPEDS")
    '    If isIPEDSApplicable.ToLower = "yes" Then
    '        'ddlGender.BackColor = Color.FromName("#ffff99")
    '        ddlRace.BackColor = Color.FromName("#ffff99")
    '        ddlPreviousEducation.BackColor = Color.FromName("#ffff99")
    '        ddlCitizen.BackColor = Color.FromName("#ffff99")
    '        txtBirthDate.BackColor = Color.FromName("#ffff99")
    '        ddlStateID.BackColor = Color.FromName("#ffff99")
    '        ddlNationality.BackColor = Color.FromName("#ffff99")
    '    End If
    'End Sub
    'Private Function ValidateIPEDS() As String
    '    Dim strValidateIPEDS As String = ""
    '    isIPEDSApplicable = MyAdvAppSettings.AppSettings("IPEDS")
    '    If isIPEDSApplicable.ToLower = "yes" Then
    '        'If ddlGender.SelectedValue = "" Then
    '        '    strValidateIPEDS = "Gender is required"
    '        'End If
    '        If ddlRace.SelectedValue = "" Then
    '            strValidateIPEDS &= "Race is required" & vbLf
    '        End If
    '        If ddlPreviousEducation.SelectedValue = "" Then
    '            strValidateIPEDS &= "Previous Education is required" & vbLf
    '        End If
    '        If ddlCitizen.SelectedValue = "" Then
    '            strValidateIPEDS &= "Citizen is required" & vbLf
    '        End If
    '        If txtBirthDate.SelectedDate Is Nothing Then
    '            strValidateIPEDS &= "DOB is required" & vbLf
    '        End If
    '        If ddlStateID.SelectedValue = "" And chkForeignZip.Checked = False Then
    '            strValidateIPEDS &= "State is required" & vbLf
    '        End If
    '        If ddlNationality.SelectedValue = "" Then
    '            strValidateIPEDS &= "Nationality is required" & vbLf
    '        End If
    '        Return strValidateIPEDS
    '    Else
    '        Return strValidateIPEDS
    '    End If
    'End Function
    Public Function GetStudentImagePath(ByVal strStudentId As String) As String
        Dim facade As New DocumentManagementFacade
        Dim strDocumentType As String = facade.GetLeadPhotoDocumentType(LeadId, "Photo", "LeadId")
        Dim strPath As String = MyAdvAppSettings.AppSettings("StudentImagePath") 'SingletonAppSettings.AppSettings("DocumentPath")
        Dim strFileNameOnly As String = facade.GetLeadPhotoFileName(strStudentId, "Photo")
        Dim strFullPath As String = strPath + strDocumentType + "/" + strFileNameOnly
        Return strFullPath
    End Function
    Protected Sub myimage_ServerClick(ByVal sender As Object, ByVal e As ImageClickEventArgs) Handles myimage.ServerClick
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsAdReqs
        Dim name As String = "StudentPhoto"
        Dim LeadName As String = (New StudentSearchFacade).GetLeadNameByID(LeadId)
        Dim url As String = "../PL/StudentImage.aspx?id=" + LeadId + "&Name=" + LeadName + "&Ent=Lead"
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
    Protected Sub lnkEnlarge_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkEnlarge.Click
        Dim winSettings As String = AdvantageCommonValues.ChildWindowSettingsAdReqs
        Dim name As String = "StudentsPhoto"
        Dim LeadName As String = (New StudentSearchFacade).GetLeadNameByID(LeadId)
        Dim url As String = "../PL/StudentImage.aspx?id=" + LeadId + "&Name=" + LeadName + "&Ent=Lead"
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
    Private Function IsLeadEligibleForEnrollment() As String
        Dim testRequired As New CheckLeadTest
        Dim strEnrollStatus As String = String.Empty
        If Not ddlPrgVerId.SelectedValue = "" Then
            Dim strTestStatus As String = testRequired.CheckIfRequiredTest(LeadId, ddlPrgVerId.SelectedValue, CampusId)
            Dim strDocStatus As String = testRequired.CheckRequiredDocuments(LeadId, ddlPrgVerId.SelectedValue, CampusId)
            Dim strReqsMetStatus As String = (New AdReqsFacade).CheckIfReqGroupMeetsConditions(LeadId, ddlPrgVerId.SelectedValue, CampusId)
            If strTestStatus = "Enroll" And strDocStatus = "Enroll" And strReqsMetStatus = "Enroll" Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If
            
        End If

        Return strEnrollStatus
    End Function

    Protected Sub lnkToOpenRequirements_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkToOpenRequirements.Click
        'Dim LeadMasterUpdate As New LeadFacade
        Dim TestRequired As New CheckLeadTest
        Dim strTestRequiredResult As String
        '        Dim Result As String
        Dim strDocRequired As String


        'Check If Lead Has Not Taken a Required Test or Test Has not been passed
        Dim strTestStatus = "", strDocStatus = "", strEnrollStatus As String
        'If The Program Version is not blank
        If ddlPrgVerId.SelectedValue = "" Then
            'isplayErrorMessage("Please select Program version to view the Admission Requirements ")
            DisplayRADAlert(CallbackType.Postback, "Error19", "Please select Program version to view the Admission Requirements ", "Open Req Error")
            Exit Sub
        End If

        If Not ddlPrgVerId.SelectedValue = "" Then
            'Check If Lead Has Not Taken a Required Test or Test Has not been passed
            strTestStatus = TestRequired.CheckIfRequiredTest(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            'If the Lead Has not taken a required doc or has an document not  approved
            strDocStatus = TestRequired.CheckRequiredDocuments(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            Dim AdReqs As New AdReqsFacade
            Dim strReqsMetStatus As String
            strReqsMetStatus = AdReqs.CheckIfReqGroupMeetsConditions(LeadId, ddlPrgVerId.SelectedValue, CampusId)

            If strTestStatus = "Enroll" And strDocStatus = "Enroll" And strReqsMetStatus = "Enroll" Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If
            OpenAdmissionRequirementsPagePopUp(LeadId, ddlPrgVerId.SelectedValue, strEnrollStatus)
        Else
            'If The Program Version is blank
            strTestRequiredResult = TestRequired.CheckIfRequiredTestNoProgramVersion(LeadId)
            If strTestRequiredResult = "Enroll" Then
                strTestStatus = "Enroll"
            End If

            'If the Lead Has not taken a required doc or has an document not  approved
            strDocRequired = TestRequired.CheckRequiredDocumentsNoProgramVersion(LeadId)
            If strDocRequired = "Enroll" Then
                strDocStatus = "Enroll"
            End If

            If strTestStatus = "Enroll" And strDocStatus = "Enroll" Then
                strEnrollStatus = "Enroll"
            Else
                strEnrollStatus = "Not Enroll"
            End If
            OpenAdmissionRequirementsPagePopUp(LeadId, ddlPrgVerId.SelectedValue, strEnrollStatus)
        End If
    End Sub
    Protected Sub lnkToEnrollLead_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkToEnrollLead.Click
        Dim strCheckIfLeadStatusMapsToEnrollment As String = (New LeadEnrollmentFacade).CheckIfLeadStatusMapsToEnrollment(CampusId, userId, txtLeadStatusIdHidden.Text)
        Dim strMessage As String = ""
        If Not strCheckIfLeadStatusMapsToEnrollment = "" Then
            strMessage &= "The Current Lead Status needs to be mapped to Enrolled Status for the lead to be enrolled" & vbLf
            'DisplayErrorMessage(strMessage)
            DisplayRADAlert(CallbackType.Postback, "Error20", strMessage, "Enroll Error")
            Exit Sub
        Else
            Response.Redirect("../ad/leadenrollments.aspx?resid=174&mod=AD&cmpid=" + CampusId + "&enid=" + txtLeadMasterID.Text)
        End If
    End Sub
End Class
