
<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NewSite.master" inherits="FamilyIncome" CodeFile="FamilyIncome.aspx.vb" %>
<%@ mastertype virtualpath="~/NewSite.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script language="javascript" src="../js/CheckAll.js" type="text/javascript"/>
   <script type="text/javascript">

       function OldPageResized(sender, args) {
           $telerik.repaintChildren(sender);
       }

   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow:auto;">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" style="overflow:auto;">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
              <table cellSpacing="0" cellPadding="0" width="100%" border="0">
                            <tr>
                                <td class="listframetop">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="10%" nowrap align="left"><asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                            <td width="85%" nowrap>
                                            
                                            <asp:radiobuttonlist id="radStatus" CssClass="Label" AutoPostBack="true" Runat="Server" RepeatDirection="Horizontal">
													<asp:ListItem Text="Active" Selected="True" />
													<asp:ListItem Text="Inactive" />
													<asp:ListItem Text="All" />
												</asp:radiobuttonlist></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="listframebottom">
									<div class="scrollleftfilters"><asp:datalist id="dlstIncomes" runat="server" Width="100%" DataKeyField="FamilyIncomeId">
											<SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
											<ItemStyle CssClass="itemstyle"></ItemStyle>
											<ItemTemplate>
												<asp:ImageButton id="imgInActive" ImageUrl="../images/Inactive.gif" runat="server" CausesValidation="False" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'>
												</asp:ImageButton>
												<asp:ImageButton id="imgActive" ImageUrl="../images/Active.gif" runat="server" CausesValidation="False" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'>
												</asp:ImageButton>
												<asp:Label ID="lblId" Runat="server" Visible="false" text='<%# Container.DataItem("StatusId")%>' />
												<asp:LinkButton text='<%# Container.DataItem("FamilyIncomeDescrip")%> ' Runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("FamilyIncomeId")%>' ID="Linkbutton1" CausesValidation="False" />
											</ItemTemplate>
										</asp:datalist></div>
								</td>
							</tr>
						                    </table>
    
    </telerik:RadPane>
    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
    <asp:panel id="pnlRHS" Runat="server">
            <table cellSpacing="0" cellPadding="0" width="100%" border="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
										<asp:button id="btnSave" runat="server" CssClass="save" Text="Save"></asp:button>
										<asp:button id="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:button>
										<asp:button id="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:button></TD>
									
								</TR>
							</TABLE> <!-- end top menu (save,new,reset,delete,history)--> <!--begin right column-->
							<table id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0" class="maincontenttable">
								<TR>
									<TD class="detailsframe">
										<DIV class="scrollright"><!-- begin table content--> <!-- Two column template with no spacer in between cells -->  <!-- used in placement module for maintenance pages -->
											<TABLE class="contenttable" cellSpacing="0" cellPadding="1" width="60%" align="center">
                                            <br />
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblFamilyIncomeCode" Runat="server" CssClass="label"></asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:textbox id="txtFamilyIncomeCode" Runat="server" CssClass="textbox" Width="80%"></asp:textbox></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblStatusId" Runat="server" CssClass="label"></asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:dropdownlist id="ddlStatusId" runat="server"></asp:dropdownlist></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblFamilyIncomeDescrip" Runat="server" CssClass="label"></asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:textbox id="txtFamilyIncomeDescrip" Runat="server" CssClass="textbox" Width="80%"></asp:textbox></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblCampGrpId" Runat="server" CssClass="label"></asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:dropdownlist id="ddlCampGrpId" runat="server"></asp:dropdownlist></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblViewOrder" Runat="server" CssClass="label"></asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:textbox id="txtViewOrder" Runat="server" CssClass="textbox"></asp:textbox>
													</TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:textbox id="txtFamilyIncomeId" Runat="server" CssClass="textbox" Visible="false"></asp:textbox></TD>
													<TD class="twocolumncontentcell"></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">&nbsp;</TD>
													<TD class="twocolumncontentcell" style="text-align: left"><asp:Label id="lblNote" Runat="server" CssClass="label" Width="40px" Font-Bold="True">Note:</asp:Label><span class="label">
															<%= "The " + viewOrderLabel + " is used to determine the order in which Family Income items will be listed."%>
														</span></TD>
												</TR>																														
											</TABLE> 
                                             <br />
						                    <asp:Panel id="pnlReportingAgenciesHeader" Runat="server" Visible="True">
                                                <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="60%" align="center" style="border: 1px solid #ebebeb;">
                                                   <TR>
                                                        <TD class="contentcellheader" noWrap colSpan="6" style="border-top: 0px; border-right: 0px; border-left: 0px">
                                                            <asp:label id="lblReportingAgencies" Runat="server" Font-Bold="true" cssclass="Label">Accrediting Agencies Mapping</asp:label></TD>
                                                    </TR>
                                                    <TR>
                                                        <TD class="contentcellcitizenship" colSpan="6" style="padding-top: 12px">
                                                            <asp:panel id="pnlReportingAgencies" tabIndex="12" Runat="server" enableviewstate="false"></asp:panel></TD>
                                                    </TR>
                                                </TABLE>
                                            </asp:Panel>
                                            <!--end table content-->
											<asp:textbox id="txtRowIds" style="VISIBILITY: hidden" runat="server" CssClass="donothing"></asp:textbox>
											<asp:textbox id="txtResourceId" style="VISIBILITY: hidden" runat="server" CssClass="donothing" ></asp:textbox>													
										</DIV>
									</TD>
								</TR>
								
			</table>
            </asp:panel>
              </telerik:RadPane>
    </telerik:RadSplitter>
		<asp:panel id="Panel1" runat="server" CssClass="ValidationSummary"></asp:panel><asp:customvalidator id="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator><asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True"
				ShowSummary="False"></asp:validationsummary>
			<!--end validation panel-->
            </div>
            </asp:Content>