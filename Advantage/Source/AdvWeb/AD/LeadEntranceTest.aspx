﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="LeadEntranceTest.aspx.vb" Inherits="LeadEntranceTest" %>
<%@ MasterType  virtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
				<!-- begin rightcolumn -->
				<tr>
					<td class="detailsframetop">
                         <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right"><asp:button id="btnSave" runat="server" Enabled="False" CssClass="save" Text="Save"></asp:button><asp:button id="btnNew" runat="server" Enabled="False" CssClass="new" Text="New" CausesValidation="False"></asp:button><asp:button id="btnDelete" runat="server" Enabled="False" CssClass="delete" Text="Delete" CausesValidation="False"></asp:button></td>
								
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
                        <table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="98%"
                            border="0">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                        <asp:Panel ID="pnlHeader" runat="server">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="95%" align="left"
                                                border="0">
                                                <tr>
                                                    <td class="datagridheader3" align="left" width="20%">
                                                        Test
                                                    </td>
                                                    <td class="datagridheader2" width="6%">
                                                        Required
                                                    </td>
                                                    <td class="datagridheader2" width="4%">
                                                        &nbsp;&nbsp;Pass
                                                    </td>
                                                    <td class="datagridheader2" width="12%">
                                                        Completed Date
                                                    </td>
                                                    <td class="datagridheader2" width="12%">
                                                        Actual Score
                                                    </td>
                                                    <td class="datagridheader2" width="8%">
                                                        Min Score
                                                    </td>
                                                    <td class="datagridheader2" width="28%">
                                                        Comments
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">
                                                        <asp:Panel ID="pnl1" runat="server">
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <table height="100">
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <asp:Panel ID="pnlDateFormat" runat="server" Visible="False">
                                            <table class="contenttable" cellspacing="0" cellpadding="0" width="40%" align="left"
                                                border="0">
                                                <tr>
                                                    <td valign="top" nowrap>
                                                        <asp:Label ID="lblDateFormat" runat="server" CssClass="labelbold"><u>Completed Date 
																Valid Format</u> : mm/dd/yyyy</asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblExample" runat="server" CssClass="labelbold">Example : 04/10/1980</asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </asp:Panel>
                                        <p>
                                        </p>
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="98%" 
                                            border="0">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblErrorMessage" CssClass="labelbold" runat="server" Visible="False"
                                                        ForeColor="red"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
			<!-- start validation panel-->
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

