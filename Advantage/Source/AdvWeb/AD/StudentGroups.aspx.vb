﻿Imports System.Diagnostics
Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports System.IO
Imports Advantage.Business.Objects
Imports System.Collections
Imports Telerik.Web.UI

Partial Class AD_StudentGroups
    Inherits BasePage
    Protected WithEvents btnhistory As Button

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected moduleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btndelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        'Header1.EnableHistoryButton(False)
        Dim objCommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        Dim ds As  DataSet
        Dim sw As New StringWriter
        Dim ds2 As  DataSet
        Dim sStatusId As String
        Dim mContext As HttpContext
        'Dim fac As New UserSecurityFacade
        Dim stuFac As New AdStudentGroupsFacade

        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState
        ViewState("IsUserSa") = advantageUserState.IsUserSA
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString
        moduleId = HttpContext.Current.Request.Params("Mod").ToString

        mContext = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        'Check if this page still exists in the menu while switching campus
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        
        Try
            If Not Page.IsPostBack Then

                ''objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                BuildDropDownLists()

                mContext = HttpContext.Current
                txtResourceId.Text = CInt(mContext.Items("ResourceId"))
                'objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                txtStuGrpId.Text = Guid.NewGuid.ToString()

                ds = stuFac.GetStudentGroupsList(userId, campusId, advantageUserState.IsUserSA)
                ''ds = objListGen.SummaryListGenerator()

                'Generate the status id
                ''ds2 = objListGen.StatusIdGenerator()
                ds2 = stuFac.StatusIdGenerator()

                'Set up the primary key on the datatable
                ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

                Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
                sStatusId = row("StatusId").ToString()

                Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "Descrip", DataViewRowState.CurrentRows)

                With dlstAdmissionReqGroups
                    .DataSource = dv
                    .DataBind()
                End With
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
            Else
                ''objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)

            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        headerTitle.Text = Header.Title
    End Sub
    Private Sub BuildDropDownLists()
        BuildStatusDDL()
        BuildCampGrpDDL()
        BuildOwnerDDL()
        BuildGroupTypeDDL()

        BuildStudentStatusDdl()
        BuildPrgVerDdl()
        BuildTermsDdl()
        BuildAcademicYearsDDL()
    End Sub
    Private Sub BuildStudentStatusDdl()
        'Bind the Status DropDownList
        Dim ddlStatusId As DropDownList = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("ddlStatusId"), DropDownList)
        Dim campusId As String = AdvantageSession.UserState.CampusId.ToString
        Dim statuses As New StatusCodeFacade
        With ddlStatusId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataSource = statuses.GetAllStatusCodesForStudents(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildPrgVerDdl()
        'Bind the Programs DrowDownList
        Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("ddlStudentPrgVerId"), DropDownList)
        With ddlStudentPrgVerId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = (New StudentSearchFacade).GetAllEnrollmentsUsedByStudents()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildTermsDdl()
        '   bind the Terms DDL
        Dim ddlTermId As DropDownList = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("ddlTermId"), DropDownList)
        Dim terms As New StudentsAccountsFacade
        With ddlTermId
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataSource = terms.GetAllTerms(True)
            .DataBind()
            '.Items.Insert(0, New ListItem("All Terms", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildAcademicYearsDDL()
        '   bind the AcademicYearss DDL
        Dim ddlAcademicYearId As DropDownList = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("ddlAcademicYearId"), DropDownList)
        Dim academicYears As New StudentsAccountsFacade
        With ddlAcademicYearId
            .DataTextField = "AcademicYearDescrip"
            .DataValueField = "AcademicYearId"
            .DataSource = academicYears.GetAllAcademicYears()
            .DataBind()
            .SelectedIndex = CommonWebUtilities.SelectCurrentAcademicYear(ddlAcademicYearId.Items)
        End With
    End Sub
    Private Sub BuildStatusDDL()
        '   bind the Status DropDownList
        Dim statuses As New StatusesFacade
        With ddlStatusId
            .DataTextField = "Status"
            .DataValueField = "StatusId"
            .DataSource = statuses.GetAllStatuses()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildCampGrpDDL()
        '   bind the Campus Group DropDownList
        Dim userId As String = AdvantageSession.UserState.UserId.ToString
        Dim facade As New CampusGroupsFacade
        With ddlCampGrpId
            .DataTextField = "CampGrpDescrip"
            .DataValueField = "CampGrpId"
            .DataSource = facade.GetAllCampusGroupsByUser(userId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildOwnerDDL()
        '   bind the Campus Group DropDownList
        Dim userId As String = AdvantageSession.UserState.UserId.ToString
        Dim campusId As String = AdvantageSession.UserState.CampusId.ToString
        Dim facade As New UserSecurityFacade
        With ddlOwnerId
            .DataTextField = "FullName"
            .DataValueField = "UserId"
            .DataSource = facade.GetExistingUsers(userId, campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))

            Try
                .SelectedValue = userId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Do nothing
            End Try

        End With
    End Sub
    Private Sub BuildGroupTypeDDL()
        '   bind the Group type DropDownList     
        Dim facade As New AdStudentGroupsFacade
        With ddlGroupType
            .DataTextField = "Descrip"
            .DataValueField = "GrpTypeID"
            .DataSource = facade.GetGroupTypes()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BindStudentDataList()
        Dim campusId As String = AdvantageSession.UserState.CampusId.ToString
        Dim txtLastName As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtLastName"), TextBox)
        Dim txtFirstName As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtFirstName"), TextBox)
        Dim txtEnrollment As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtEnrollment"), TextBox)
        Dim ddlStatusId As DropDownList = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("ddlStatusId"), DropDownList)
        Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("ddlStudentPrgVerId"), DropDownList)
        Dim txtSSN As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtSSN"), TextBox)

        Dim rgStudentsearch As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("rgStudentSearch"), RadGrid)
        Dim btnAddStudents As Button = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("btnAddStudents"), Button)


        Dim ds As DataSet = (New StudentSearchFacade).GetStudentSearchDS(txtLastName.Text, txtFirstName.Text, txtSSN.Text, txtEnrollment.Text, ddlStatusId.SelectedValue, ddlStudentPrgVerId.SelectedValue, campusId)

        Try
            Dim dcchk As New DataColumn("chkDelete")
            dcchk.DataType = Type.GetType("System.String")
            dcchk.DefaultValue = "False"
            ds.Tables(0).Columns.Add(dcchk)
            Dim dcEID As New DataColumn("EnrollmentId")
            dcEID.DataType = Type.GetType("System.String")
            dcEID.DefaultValue = Guid.Empty.ToString
            ds.Tables(0).Columns.Add(dcEID)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        Session("StudentSearch") = ds
        ''New Code Added By VIjay Ramteke On June 19, 2010 For Mantis Id 19198
        Dim dv As DataView = New DataView(ds.Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
        rgStudentsearch.DataSource = dv
        ''New Code Added By VIjay Ramteke On June 19, 2010 For Mantis Id 19198
        rgStudentsearch.DataBind()
        rgStudentsearch.Visible = True
        If rgStudentsearch.Items.Count > 0 Then
            btnAddStudents.Enabled = True
        Else
            btnAddStudents.Enabled = False
        End If

    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As EventArgs)
        'If txtLastName.Text = "" And txtFirstName.Text = "" And txtEnrollment.Text = "" And ddlStatusId.SelectedValue = Guid.Empty.ToString And ddlCampGrpID.SelectedValue = Guid.Empty.ToString And ddlStudentPrgVerId.SelectedValue = Guid.Empty.ToString And txtSSN.Text = "" Then
        '    DisplayErrorMessage("Please enter a value to search")
        '    Exit Sub
        'End If
        ''   bind datalist
        'BindDataList()
        Dim txtLastName As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtLastName"), TextBox)
        Dim txtFirstName As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtFirstName"), TextBox)
        Dim txtEnrollment As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtEnrollment"), TextBox)
        Dim ddlStatusId As DropDownList = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("ddlStatusId"), DropDownList)
        Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("ddlStudentPrgVerId"), DropDownList)
        Dim txtSSN As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtSSN"), TextBox)
        If txtLastName.Text = "" And txtFirstName.Text = "" And txtEnrollment.Text = "" And ddlStatusId.SelectedValue = Guid.Empty.ToString And ddlStudentPrgVerId.SelectedValue = Guid.Empty.ToString And txtSSN.Text = "" Then
            DisplayErrorMessage("Please enter a value to search")
            Exit Sub
        End If
        '   bind datalist
        ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19200
        Session("HeaderchkSS") = Nothing
        Session("StudentSearch") = Nothing
        ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19200
        BindStudentDataList()
    End Sub
    Protected Sub btnAddStudents_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim resultFinal As String = ""
        Dim ds As DataSet = Session("StudentSearch")

        Dim rgStudentsSearch As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("rgStudentSearch"), RadGrid)

        For Each gi As GridDataItem In rgStudentsSearch.Items
            Dim StuId As String = gi("STUID").Text
            Dim drStu() As DataRow = ds.Tables(0).Select("StudentId='" & StuId & "'")
            Dim chkSS As  CheckBox = DirectCast(gi.FindControl("ChkStu"), CheckBox)
            Dim enrollment As DropDownList
            enrollment = DirectCast(gi.FindControl("ddlEnrollmentsId"), DropDownList)
            For Each dr As DataRow In drStu
                dr("chkDelete") = chkSS.Checked.ToString
                dr("EnrollmentId") = enrollment.SelectedValue
            Next
        Next
        ds.Tables(0).AcceptChanges()
        For Each dr As DataRow In ds.Tables(0).Rows
            If dr("chkDelete").ToString = True Then
                Dim strStudentId As String = dr("StudentId").ToString
                Dim strStuEnrollId As String = dr("EnrollmentId").ToString
                Dim StuFacade As New AdStudentGroupsFacade

                Dim result As String = StuFacade.ValidateStudentGroupStudent(strStudentId, txtStuGrpId.Text, strStuEnrollId)
                If result = "" Then
                    Dim stuGrpInfo As New AdReqGrpInfo
                    stuGrpInfo.StudentId = strStudentId
                    stuGrpInfo.StuGrpId = txtStuGrpId.Text
                    stuGrpInfo.StuEnrollId = strStuEnrollId
                    stuGrpInfo.ModDate = DateTime.Now
                    StuFacade.AddStudentGroupStudent(stuGrpInfo, Session("UserName").ToString)
                Else
                    If Not resultFinal = "" Then
                        resultFinal = resultFinal + dr("FirstName").ToString + " " + dr("LastName").ToString + vbCrLf
                    Else
                        resultFinal = "Following Student(s) already exists in the Selected Group :" + vbCrLf + dr("FirstName").ToString + " " + dr("LastName").ToString + vbCrLf
                    End If

                End If
            End If
        Next
        If Not resultFinal = "" Then
            DisplayErrorMessage(resultFinal)
        End If
        ClearSearchList()
        BindDataStudentGrid()
        rpbStudentSearch.Items(1).Expanded = True
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnsave.Click
        Dim objCommon As New CommonUtilities
        Dim facade As New AdStudentGroupsFacade
        Dim result As String
        Dim errMsg As String = ""
        Dim userId As String = AdvantageSession.UserState.UserId.ToString
        If ViewState("MODE") = "EDIT" Then
            ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19241
            Dim isUserSa As Boolean = CType(ViewState("IsUserSa"), Boolean)
            If Not isUserSa Then
                If facade.CheckForStudentGroupOwner(txtStuGrpId.Text, userId) = "0" Then
                    DisplayErrorMessage("You do not have the rights to edit the group.")
                    Exit Sub
                End If
            End If
            ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19241
        End If

        Try
            If txtCode.Text + "" = "" Then
                errMsg = errMsg + "-Code is required" + vbCrLf
            End If
            If ddlStatusId.SelectedValue = Guid.Empty.ToString Then
                errMsg = errMsg + "-Status is required" + vbCrLf
            End If
            If txtDescrip.Text + "" = "" Then
                errMsg = errMsg + "-Description is required" + vbCrLf
            End If
            If ddlCampGrpId.SelectedValue = Guid.Empty.ToString Then
                errMsg = errMsg + "-Campus Group is required" + vbCrLf
            End If

            If ddlGroupType.SelectedValue = Guid.Empty.ToString Then
                errMsg = errMsg + "-Student Group Type is required" + vbCrLf
            End If
            If ddlOwnerId.SelectedValue = Guid.Empty.ToString Then
                errMsg = errMsg + "-Owner is required" + vbCrLf
            End If
            If Not errMsg = "" Then
                DisplayErrorMessage(errMsg)
                Exit Sub
            End If

            If ViewState("MODE") = "NEW" Then
                Dim validateStuGrp As New AdStudentGroupsFacade
                Dim validateStuGroupMessage As String

                validateStuGroupMessage = validateStuGrp.ValidateStudentGroup(txtDescrip.Text)
                If Not validateStuGroupMessage = "" Then
                    DisplayErrorMessage(validateStuGroupMessage)
                    Exit Sub
                End If
                'Validate Insert/Update
                result = facade.AddStuGrp(BuildRequirementGroups(), Session("UserName"))
                If Not result = "" Then
                    DisplayErrorMessage(result)
                    Exit Sub
                End If
                BindDataList()

                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"
                btnViewStudents.Enabled = True
                rpbStudentSearch.Visible = True
                BindDataStudentGrid()
            ElseIf ViewState("MODE") = "EDIT" Then
                result = facade.AddStuGrp(BuildRequirementGroups(), Session("UserName"))
                If Not result = "" Then
                    DisplayErrorMessage(result)
                    Exit Sub
                End If

                ''chkIsInDB.Checked = True
                BindDataList()

                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"

            End If
            '   set Style to Selected Item
            ' CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, txtStuGrpId.Text, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, txtStuGrpId.Text)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim ds As  DataSet
        Dim sw As New StringWriter
        Dim facade As New AdStudentGroupsFacade
        Dim ds2 As  DataSet
        Dim sStatusId As String
        Dim rowFilter, sortExpression, status As String
        'status = ""
        Dim userId As String = AdvantageSession.UserState.UserId.ToString
        Dim campusId As String = AdvantageSession.UserState.CampusId.ToString
        Select Case radstatus.SelectedIndex
            Case 0
                rowFilter = "Status='Active'"
                sortExpression = "Descrip"
                status = "Active"
            Case 1
                rowFilter = "Status='Inactive'"
                sortExpression = "Descrip"
                status = "Inactive"
            Case Else
                ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
                status = "Inactive"
                rowFilter = ""
                ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
                sortExpression = "Status DESC, Descrip "
        End Select

        ''ds = objListGen.SummaryListGenerator()
        Dim isUserSa As Boolean = CType(ViewState("IsUserSa"), Boolean)
        ds = facade.GetStudentGroupsList(userId, campusId, isUserSa)
        ds2 = facade.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        Dim row As DataRow = ds2.Tables(0).Rows.Find(status)
        sStatusId = row("StatusId").ToString()

        Dim dv As New DataView(ds.Tables(0), rowFilter, sortExpression, DataViewRowState.CurrentRows)

        With dlstAdmissionReqGroups
            .DataSource = dv
            .DataBind()
        End With

        dlstAdmissionReqGroups.SelectedIndex = -1
        PopulateDataList(ds)
        ds.WriteXml(sw)
        ViewState("ds") = sw.ToString()
    End Sub

    Private Function BuildRequirementGroups() As AdReqGrpInfo
        Dim ReqGrp As New AdReqGrpInfo
        With ReqGrp
            .IsInDB = Not (ViewState("MODE") = "NEW")
            .Code = txtCode.Text
            .Descrip = txtDescrip.Text
            .StatusId = ddlStatusId.SelectedValue
            .CampGrpId = ddlCampGrpId.SelectedValue
            .OwnerId = ddlOwnerId.SelectedValue
            .GrpTypeId = ddlGroupType.SelectedValue
            .StuGrpId = txtStuGrpId.Text
            If chkPublic.Checked Then
                .IsPublic = True
            Else
                .IsPublic = False
            End If
            '.ModUser = userId
            'New Code Added By Kamalesh Ahuja
            If ChkRegistrationHold.Checked Then
                .IsRegHold = True
            Else
                .IsRegHold = False
            End If
            If ChkTranscriptHold.Checked Then
                .IsTransHold = True
            Else
                .IsTransHold = False
            End If

            .ModDate = Date.Now
        End With
        Return ReqGrp
    End Function

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
          'Set error condition
        'Display error in message box in the client
        If page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            Const scriptBegin As String = "window.Onload=DisplayError();function DisplayError(){alert('"
            Const scriptEnd As String = "');}"

            '   Register a javascript to display error message
            page.ClientScript.RegisterStartupScript(Me.GetType(), "ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + scriptEnd, True)
        End If
        '   Display error in message box in the client
        'DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btndelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        'Dim objListGen As New DataListGenerator
        ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
        Dim userId As String = AdvantageSession.UserState.UserId.ToString
        ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
        Try
            ''msg = objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            Dim facade As New AdStudentGroupsFacade
            ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19241
            Dim isUserSa As Boolean = CType(ViewState("IsUserSa"), Boolean)
            If Not isUserSa Then
                If facade.CheckForStudentGroupOwner(txtStuGrpId.Text, userId) = "0" Then
                    DisplayErrorMessage("You do not have the rights to delete the group.")
                    Exit Sub
                End If
            End If
            ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19241
            Dim result As String
            result = facade.DeleteStudentGroup(txtStuGrpId.Text)
            If Not result = "" Then
                DisplayErrorMessage(result)
                Exit Sub
            End If
            BindDataList()
            ClearRHS()
            dlstAdmissionReqGroups.SelectedIndex = -1
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtStuGrpId.Text = Guid.NewGuid.ToString()
            'Header1.EnableHistoryButton(False)
            CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlrhs.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next

            Dim rgGroupStudents As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("ViewStudents").FindControl("rgGroupStudents"), RadGrid)
            rgGroupStudents.DataSource = Nothing
            rgGroupStudents.DataBind()

            Dim rgStudentsSearch As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("rgStudentSearch"), RadGrid)
            rgGroupStudents.DataSource = Nothing
            rgGroupStudents.DataBind()
            ''New Code Added Bu Vijay Ramteke On June 19, 2010 For Mantis Id 19200
            Session("Headerchk") = Nothing
            Session("Studentds") = Nothing
            Session("HeaderchkSS") = Nothing
            Session("StudentSearch") = Nothing
            ''New Code Added Bu Vijay Ramteke On June 19, 2010 For Mantis Id 19200
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnnew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New StringReader(CStr(ViewState("ds")))
        Dim userId As String = AdvantageSession.UserState.UserId.ToString
        Try
            ClearRHS()
            ds.ReadXml(sr)
            PopulateDataList(ds)
            ''New Code Added BY Vijay Ramteke On June 20, 2010 For Mantis Id 19197
            BuildOwnerDDL()
            ''New Code Added BY Vijay Ramteke On June 20, 2010 For Mantis Id 19197
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            txtStuGrpId.Text = Guid.NewGuid.ToString()
            'Reset Style in the Datalist
            btnViewStudents.Enabled = False
            rpbStudentSearch.Visible = False
            'CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, Guid.Empty.ToString, ViewState, Header1)
            Try
                ddlOwnerId.SelectedValue = userId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Do nothing
            End Try

            CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    'Private Sub chkStatus_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim objListGen As New DataListGenerator
    '    Dim ds As New DataSet
    '    Dim ds2 As New DataSet
    '    Dim sw As New StringWriter
    '    Dim objCommon As New CommonUtilities
    '    Dim dv2 As New DataView
    '    '  Dim sStatusId As String
    '    ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
    '    Dim facade As New AdStudentGroupsFacade
    '    Dim userId As String = AdvantageSession.UserState.UserId.ToString
    '    Dim campusId As String = AdvantageSession.UserState.CampusId.ToString
    '    ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
    '    Try
    '        ClearRHS()
    '        objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
    '        ViewState("MODE") = "NEW"
    '        txtStuGrpId.Text = Guid.NewGuid.ToString()
    '        ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
    '        ''ds = objListGen.SummaryListGenerator()
    '        ds = facade.GetStudentGroupsList(userId, campusId)
    '        ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
    '        PopulateDataList(ds)
    '    Catch ex As Exception
     '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        'Redirect to error page.
    '        If ex.InnerException Is Nothing Then
    '            Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " "
    '        Else
    '            Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " " & ex.InnerException.Message
    '        End If
    '        Response.Redirect("../ErrorPage.aspx")
    '    End Try
    'End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As  DataSet
        Dim dv2 As  DataView
        Dim objListGen As New DataListGenerator
        Dim sStatusId As String

        ds2 = objListGen.StatusIdGenerator()
        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}
        If ds.Tables.Count > 0 Then
            If (radstatus.SelectedItem.Text = "Active") Then   'Show Active Only
                Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
                sStatusId = row("StatusId").ToString()
                'Create dataview which displays active records only
                Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "Descrip", DataViewRowState.CurrentRows)
                dv2 = dv
            ElseIf (radstatus.SelectedItem.Text = "Inactive") Then   'Show InActive Only
                Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
                sStatusId = row("StatusId").ToString()
                'Create dataview which displays inactive records only
                Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "Descrip", DataViewRowState.CurrentRows)
                dv2 = dv
            Else  'Show All
                Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
                sStatusId = row("StatusId").ToString()
                'Create dataview which displays inactive records only
                Dim dv As New DataView(ds.Tables(0), "", "", DataViewRowState.CurrentRows)
                dv2 = dv
            End If
            With dlstAdmissionReqGroups
                .DataSource = dv2
                .DataBind()
            End With
            dlstAdmissionReqGroups.SelectedIndex = -1
        End If
    End Sub


    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles radstatus.SelectedIndexChanged
        'Dim objListGen As New DataListGenerator
        Dim ds As  DataSet
        'Dim ds2 As New DataSet
        'Dim sw As New StringWriter
        Dim objCommon As New CommonUtilities
        'Dim dv2 As New DataView
        '      Dim sStatusId As String
        ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
        Dim facade As New AdStudentGroupsFacade
        Dim userId As String = AdvantageSession.UserState.UserId.ToString
        Dim campusId As String = AdvantageSession.UserState.CampusId.ToString
        ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtStuGrpId.Text = Guid.NewGuid.ToString
            ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
            ''ds = objListGen.SummaryListGenerator()
            Dim isUserSa As Boolean = CType(ViewState("IsUserSa"), Boolean)
            ds = facade.GetStudentGroupsList(userId, campusId, isUserSa)
            ''New Code Added By Vijay Ramteke On June 18, 2010 For Mantis Id 19190
            PopulateDataList(ds)
            'Header1.EnableHistoryButton(False)

            btnViewStudents.Enabled = False
            rpbStudentSearch.Visible = False
            CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, Guid.Empty.ToString)

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus_SelectedIndexChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub dlstAdmissionReqGroups_ItemCommand(ByVal source As Object, ByVal e As DataListCommandEventArgs) Handles dlstAdmissionReqGroups.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim sr As New StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        Try
            ClearRHS()
            ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19200
            ClearSearchList()
            Session("Headerchk") = Nothing
            Session("Studentds") = Nothing
            BuildOwnerDDL()
            ''New Code Added By Vijay Ramteke On June 19, 2010 For Mantis Id 19200
            If e.CommandArgument <> "" Then GetStudentGrpInfo(e.CommandArgument)
            strGUID = dlstAdmissionReqGroups.DataKeys(e.Item.ItemIndex).ToString()
            txtRowIds.Text = e.CommandArgument.ToString
            ''objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGUID)

            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            selIndex = e.Item.ItemIndex
            dlstAdmissionReqGroups.SelectedIndex = selIndex
            ds.ReadXml(sr)
            PopulateDataList(ds)
            '   set Style to Selected Item
            btnViewStudents.Enabled = True
            rpbStudentSearch.Visible = True
            If e.CommandArgument <> "" Then BindDataStudentGrid()
            ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19241
            Dim isUserSa As Boolean = CType(ViewState("IsUserSa"), Boolean)
            If Not isUserSa Then
                ''New Code Added By Vijay Ramteke On June 20, 2010 For Mantis Id 19197
                CheckForOwner()
                ''New Code Added By Vijay Ramteke On June 20, 2010 For Mantis Id 19197
            End If
            ''New Code Added By Vijay Ramteke On June 29, 2010 For Mantis Id 19241
            'CommonWebUtilities.SetStyleToSelectedItem(dlstAdmissionReqGroups, e.CommandArgument, ViewState, Header1)
            CommonWebUtilities.RestoreItemValues(dlstAdmissionReqGroups, strGUID)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstCounties_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstCounties_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub GetStudentGrpInfo(ByVal StuGrpId As String)
        Dim StuGrpInfo As New AdStudentGroupsFacade
        BindRequirements(StuGrpInfo.GetStudentGroupInfo(StuGrpId))
    End Sub
    Private Sub BindRequirements(ByVal ReqGrpInfo As AdReqGrpInfo)
        'Bind The StudentInfo Data From The Database
        With ReqGrpInfo
            txtStuGrpId.Text = .StuGrpId
            txtCode.Text = .Code
            txtDescrip.Text = .Descrip
            Try
                ddlStatusId.SelectedValue = .StatusId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlStatusId.SelectedIndex = 0
            End Try

            Try
                ddlCampGrpId.SelectedValue = .CampGrpId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlCampGrpId.SelectedIndex = 0
            End Try
            Try
                ddlOwnerId.SelectedValue = .OwnerId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlGroupType.SelectedIndex = 0
            End Try

            Try
                ddlGroupType.SelectedValue = .GrpTypeId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlGroupType.SelectedIndex = 0
            End Try

            chkPublic.Checked = .IsPublic
            'New Code Added By Kamalesh Ahuja
            ChkRegistrationHold.Checked = .IsRegHold
            ChkTranscriptHold.Checked = .IsTransHold

            'txtModDate.Text = .ModDate
            'txtModUser.Text = .ModUser
        End With
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)

    End Sub
    Protected Sub btnViewStudents_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnViewStudents.Click
        BindDataStudentGrid()
    End Sub
    Protected Sub GroupStudents_Delete(ByVal sender As Object, ByVal e As GridCommandEventArgs)
        If e.CommandName = "Delete" Then
            Dim strSID As String = e.Item.Cells(2).Text

            Dim facade As New AdStudentGroupsFacade

            If ViewState("MODE") = "EDIT" Then
                facade.DeleteGroupStudents(strSID, Session("UserName").ToString)
            End If

        End If
    End Sub
    Protected Sub SetAllChecked(ByVal sender As Object, ByVal e As EventArgs)
        Dim dsCK As  DataSet
        dsCK = DirectCast(Session("Studentds"), DataSet)
        Dim chk As  CheckBox
        chk = DirectCast(sender, CheckBox)
        Session("Headerchk") = chk.Checked
        For Each dr As DataRow In dsCK.Tables(0).Rows
            If chk.Checked = True Then
                dr("chkDelete") = True
            Else
                dr("chkDelete") = False
            End If
        Next
        dsCK.AcceptChanges()
        Session("Studentds") = dsCK

        dsCK.Dispose()
        Dim rgGroupStudents As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("ViewStudents").FindControl("rgGroupStudents"), RadGrid)
        rgGroupStudents.DataSource = dsCK
        rgGroupStudents.DataBind()
        rgGroupStudents.Rebind()

    End Sub
    Protected Sub rgGroupStudents_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        Dim rgGroupStudents As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("ViewStudents").FindControl("rgGroupStudents"), RadGrid)
        If Not e.IsFromDetailTable Then
            Dim chk As New CheckBox
            If Not Session("Headerchk") Is Nothing Then

                Try
                    chk.Checked = Session("Headerchk").ToString
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    chk.Checked = False
                End Try
            End If
            If Not Session("Studentds") Is Nothing Then
                Dim ds As DataSet = CType(Session("Studentds"), DataSet)
                Dim StuGrpStuId As String
                If Not chk.Checked Then
                    For Each gi As GridDataItem In rgGroupStudents.Items
                        StuGrpStuId = gi.Cells(2).Text
                        Dim ckdelete As New CheckBox()
                        ckdelete = DirectCast(gi.FindControl("chkDeleteStu"), CheckBox)
                        Dim drTemp() As DataRow = ds.Tables(0).Select("StuGrpStuId='" + StuGrpStuId + "'")
                        For Each dr As DataRow In drTemp
                            dr("chkDelete") = ckdelete.Checked
                        Next
                    Next
                End If
                ds.Tables(0).AcceptChanges()
                ''New Code Added By VIjay Ramteke On June 19, 2010 For Mantis Id 19198
                Dim dv As DataView = New DataView(ds.Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
                rgGroupStudents.MasterTableView.DataSource = ds
                ''New Code Added By VIjay Ramteke On June 19, 2010 For Mantis Id 19198
                Session("Studentds") = ds
            End If
        End If
    End Sub
    Protected Sub rgGroupStudents_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs)
        If e.Item.ItemType = GridItemType.Header Then
            Dim chkAll As  CheckBox
            chkAll = CType(e.Item.FindControl("chkDeleteALL"), CheckBox)
            If Not Session("Headerchk") Is Nothing Then
                chkAll.Checked = CType(Session("Headerchk").ToString, Boolean)
            End If
        End If

    End Sub
    Protected Sub ChkAll_CheckChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim rgStudentsearch As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("rgStudentSearch"), RadGrid)
        Dim dsCK As  DataSet
        dsCK = DirectCast(Session("StudentSearch"), DataSet)
        Dim chk As  CheckBox
        chk = DirectCast(sender, CheckBox)
        Session("HeaderchkSS") = chk.Checked
        For Each gi As GridDataItem In rgStudentsearch.Items
            Dim StuId As String = gi.Cells(2).Text
            Dim enrollment As  DropDownList
            enrollment = DirectCast(gi.FindControl("ddlEnrollmentsId"), DropDownList)
            Dim drTemp() As DataRow = dsCK.Tables(0).Select("StudentId='" + StuId + "'")
            For Each dr As DataRow In drTemp
                dr("EnrollmentId") = enrollment.SelectedValue
            Next
        Next
        For Each dr As DataRow In dsCK.Tables(0).Rows
            If chk.Checked = True Then
                dr("chkDelete") = True
            Else
                dr("chkDelete") = False
            End If
        Next
        dsCK.AcceptChanges()
        Session("StudentSearch") = dsCK

        rgStudentsearch.DataSource = dsCK
        rgStudentsearch.DataBind()
        rgStudentsearch.Rebind()
        dsCK.Dispose()

    End Sub
    Protected Sub rgStudentSearch_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        Dim rgStudentsearch As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("rgStudentSearch"), RadGrid)
        If Not e.IsFromDetailTable Then
            Dim chk As New CheckBox
            If Not Session("HeaderchkSS") Is Nothing Then
                Try
                    chk.Checked = Session("HeaderchkSS").ToString
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    chk.Checked = False
                End Try
            End If
            If Not Session("StudentSearch") Is Nothing Then
                Dim ds As DataSet = Session("StudentSearch")
                Dim StuId As String
                If Not chk.Checked Then
                    For Each gi As GridDataItem In rgStudentsearch.Items
                        StuId = gi.Cells(2).Text
                        Dim ckdelete As New CheckBox()
                        ckdelete = DirectCast(gi.FindControl("ChkStu"), CheckBox)
                        Dim enrollment As New DropDownList
                        enrollment = DirectCast(gi.FindControl("ddlEnrollmentsId"), DropDownList)
                        Dim drTemp() As DataRow = ds.Tables(0).Select("StudentId='" + StuId + "'")
                        For Each dr As DataRow In drTemp
                            dr("chkDelete") = ckdelete.Checked
                            dr("EnrollmentId") = enrollment.SelectedValue
                        Next
                    Next
                End If
                ds.AcceptChanges()
                ''New Code Added By VIjay Ramteke On June 19, 2010 For Mantis Id 19198
                Dim dv As DataView = New DataView(ds.Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
                rgStudentsearch.MasterTableView.DataSource = dv
                ''New Code Added By VIjay Ramteke On June 19, 2010 For Mantis Id 19198
                Session("StudentSearch") = ds

            End If
        End If
    End Sub
    Protected Sub rgStudentSearch_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs)
        If e.Item.ItemType = GridItemType.Header Then
            Dim chkAll As New CheckBox
            chkAll = CType(e.Item.FindControl("ChkAll"), CheckBox)
            If Not Session("HeaderchkSS") Is Nothing Then
                chkAll.Checked = Session("HeaderchkSS").ToString
            End If
        End If
        If e.Item.ItemType = GridItemType.Item Or e.Item.ItemType = GridItemType.AlternatingItem Then
            Dim enrollments As DropDownList
            enrollments = CType(e.Item.FindControl("ddlEnrollmentsId"), DropDownList)
            Dim dr As DataRowView = CType(e.Item.DataItem, DataRowView)
            Dim drows() As DataRow = dr.Row.GetChildRows("StudentsStudentEnrollments")
            For i As Integer = 0 To drows.Length - 1
                enrollments.Items.Add(New ListItem(drows(i).GetParentRow("EnrollmentsStudentEnrollments")("Enrollment"), CType(drows(i)("StuEnrollId"), Guid).ToString))
            Next
            If e.Item.Cells(3).Text = Guid.Empty.ToString Then
                e.Item.Cells(3).Text = enrollments.SelectedValue
            Else
                enrollments.SelectedValue = e.Item.Cells(3).Text
            End If
        End If
    End Sub
    Protected Sub BindDataStudentGrid()
        Dim rgGroupStudents As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("ViewStudents").FindControl("rgGroupStudents"), RadGrid)
        Dim btnRemoveStudent As Button = DirectCast(rpbStudentSearch.FindItemByValue("ViewStudents").FindControl("btnRemoveStudent"), Button)

        Dim facade As New AdStudentGroupsFacade
        Dim ds As DataSet
        If ViewState("MODE") = "EDIT" Then
            ds = facade.GetGroupStudents(txtStuGrpId.Text)
            ''New Code Added By VIjay Ramteke On June 19, 2010 For Mantis Id 19198
            Dim dv As DataView = New DataView(ds.Tables(0), Nothing, "LastName", DataViewRowState.CurrentRows)
            rgGroupStudents.DataSource = dv
            ''New Code Added By VIjay Ramteke On June 19, 2010 For Mantis Id 19198
            rgGroupStudents.DataBind()
            Session("Studentds") = ds
            rpbStudentSearch.Items(1).Expanded = True
            If rgGroupStudents.Items.Count > 0 Then
                btnRemoveStudent.Enabled = True
            Else
                btnRemoveStudent.Enabled = False
            End If
        End If
    End Sub
    Protected Sub btnRemoveStudent_Click(ByVal sender As Object, ByVal e As EventArgs)
        'Dim resultFinal As String = ""
        Dim ds As DataSet = Session("Studentds")
        Dim rgGroupStudents As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("ViewStudents").FindControl("rgGroupStudents"), RadGrid)
        For Each gi As GridDataItem In rgGroupStudents.Items
            Dim StuGrpId As String = gi("STDID").Text
            Dim drStu() As DataRow = ds.Tables(0).Select("StuGrpStuId='" & StuGrpId & "'")
            Dim chkSS As  CheckBox = DirectCast(gi.FindControl("chkDeleteStu"), CheckBox)
            For Each dr As DataRow In drStu
                dr("chkDelete") = chkSS.Checked.ToString
            Next
        Next
        ds.Tables(0).AcceptChanges()
        For Each dr As DataRow In ds.Tables(0).Rows
            If dr("chkDelete").ToString = True Then
                Dim strStuGrpId As String = dr("StuGrpStuId").ToString
                Dim stuFacade As New AdStudentGroupsFacade
                stuFacade.DeleteGroupStudents(strStuGrpId, Session("UserName").ToString)
            End If
        Next
        BindDataStudentGrid()
        rpbStudentSearch.Items(1).Selected = True
    End Sub
    'Public Sub DisplayErrorInMessageBox(ByVal page As Page, ByVal errorMessage As String)

    '    If page.Request.Browser.JavaScript Then
    '        Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=DisplayError();function DisplayError(){alert('"
    '        Dim scriptEnd As String = "');}</script>"
    '        ScriptManager.RegisterStartupScript(page, page.GetType, "ErrorMessage", scriptBegin + ReplaceSpecialCharactersInJavascriptMessage(errorMessage) + scriptEnd, False)
    '    End If
    'End Sub
    Private Function ReplaceSpecialCharactersInJavascriptMessage(ByVal s As String) As String
        '   replace \ by \\, ' by \', " by "", CR by \r, LF by \n
        Return s.Trim().Replace("\", "\\").Replace("'", "\'").Replace("""", "\""").Replace(vbCr, "\r").Replace(vbLf, "\n")
    End Function
    Private Sub ClearSearchList()
        Dim txtLastName As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtLastName"), TextBox)
        Dim txtFirstName As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtFirstName"), TextBox)
        Dim txtEnrollment As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtEnrollment"), TextBox)
        Dim ddlStatusId As DropDownList = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("ddlStatusId"), DropDownList)
        Dim ddlStudentPrgVerId As DropDownList = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("ddlStudentPrgVerId"), DropDownList)
        Dim txtSSN As TextBox = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("txtSSN"), TextBox)

        Dim rgStudentsearch As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("rgStudentSearch"), RadGrid)
        Dim btnAddStudents As Button = DirectCast(rpbStudentSearch.FindItemByValue("StudentSearch").FindControl("btnAddStudents"), Button)

        txtLastName.Text = String.Empty
        txtFirstName.Text = String.Empty
        txtEnrollment.Text = String.Empty
        ddlStatusId.SelectedIndex = -1
        ddlStudentPrgVerId.SelectedIndex = -1
        txtSSN.Text = String.Empty
        rgStudentsearch.DataSource = Nothing
        rgStudentsearch.DataBind()

        Session("HeaderchkSS") = Nothing
        Session("StudentSearch") = Nothing

    End Sub
    ''New Code Added By Vijay Ramteke On June 20, 2010 For Mantis Id 19197
    Private Sub CheckForOwner()
        Dim facade As New AdStudentGroupsFacade
        Dim userId As String = AdvantageSession.UserState.UserId.ToString
        Dim rgGroupStudents As RadGrid = DirectCast(rpbStudentSearch.FindItemByValue("ViewStudents").FindControl("rgGroupStudents"), RadGrid)
        Dim btnRemoveStudent As Button = DirectCast(rpbStudentSearch.FindItemByValue("ViewStudents").FindControl("btnRemoveStudent"), Button)

        If facade.CheckForStudentGroupOwner(txtStuGrpId.Text, userId) = "0" Then
            ClearSearchList()
            btnRemoveStudent.Visible = False
            rpbStudentSearch.Items(0).Visible = False
            rgGroupStudents.Columns(7).Visible = False
            With ddlOwnerId
                .DataTextField = "FullName"
                .DataValueField = "UserId"
                .DataSource = facade.GetStudentGroupOwner(ddlOwnerId.SelectedValue)
                .DataBind()
            End With
        Else
            btnRemoveStudent.Visible = True
            rpbStudentSearch.Items(0).Visible = True
            rgGroupStudents.Columns(7).Visible = True
        End If
    End Sub
    ''New Code Added By Vijay Ramteke On June 20, 2010 For Mantis Id 19197
End Class