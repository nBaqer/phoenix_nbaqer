<%@ Page Language="vb" AutoEventWireup="false" Inherits="AssignLeadGroups" CodeFile="AssignLeadGroups.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>AssignLeadGroups</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<link rel="stylesheet" type="text/css" href="../css/localhost.css">
		<link rel="stylesheet" type="text/css" href="../css/systememail.css">
		<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
		<script language="javascript">
			function resizewindow(){
				window.resizeTo(800,500); 
			}
		</script>
	</HEAD>
	<body onload="resizewindow();" style="overflow:auto;">
		<form id="Form1" method="post" runat="server">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><IMG src="../images/advantage_setup_lead.jpg"></td>
					<td class="topemail"><A class="close" onclick="top.close()" href=#>X Close</A></td>
				</tr>
			</table>
			<!--begin right column-->
			<table class="maincontenttable" id="Table5" cellSpacing="0" cellPadding="0" width="100%"
				border="0">
				<tr>
					<td class="detailsFrame">
						<div class="scrollwholedocs">
							<!-- begin content table -->
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td class="contentcell" colspan="6"><asp:Label ID="label3" Runat="server" CssClass="labelbold"></asp:Label></td>
								</tr>
								<tr height="10">
									<td>&nbsp;</td>
								</tr>
							</table>
							<TABLE width="90%" align="center" cellpadding="0" cellspacing="0">
								<TR>
									<TD style="PADDING-BOTTOM: 6px">
										<asp:datagrid id="dgrdTransactionSearch" Runat="server" BorderColor="#E0E0E0" AllowSorting="True"
											AutoGenerateColumns="False" BorderStyle="Solid" ShowFooter="False" BorderWidth="1px" width="90%">
											<AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
											<ItemStyle CssClass="DataGridItemStyle" HorizontalAlign="Center"></ItemStyle>
											<HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeaderStyle"></HeaderStyle>
											<FooterStyle CssClass="datagridpadding"></FooterStyle>
											<EditItemStyle CssClass="datagridpadding"></EditItemStyle>
											<Columns>
												<asp:TemplateColumn HeaderText="All">
												<HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeaderStyle" Width="5%"></HeaderStyle>
													<HeaderTemplate>
														<input id="chkAllItems" type="checkbox" value="check all" onclick="CheckAllDataGridCheckBoxes('chkUnschedule',
																							document.forms[0].chkAllItems.checked)" />
													</HeaderTemplate>
													<ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
													<ItemTemplate>
														<asp:checkbox Id="chkUnschedule" Runat="server" text='' Checked='<%# Container.DataItem("LeadGroupSelected") %>'/>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Lead Group">
													<HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeaderStyle" Width="85%"></HeaderStyle>
													<ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
													<ItemTemplate>
														<asp:Label id="Label1" Text='<%# Container.DataItem("Descrip") %>' cssClass="Label" Runat="server">
														</asp:Label>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn HeaderText="Required">
													<HeaderStyle HorizontalAlign="Center" CssClass="DataGridHeaderStyle" Width="10%"></HeaderStyle>
													<ItemStyle CssClass="DataGridItemStyle" HorizontalAlign="Center"></ItemStyle>
													<ItemTemplate>
														<asp:checkbox id="chkRequired" Checked='<%# Container.DataItem("Required") %>' cssClass="CheckBoxStyle" Runat="server">
														</asp:checkbox>
														<asp:textbox Id="txtLeadGrpId" Runat="server" Visible="False" Text='<%# Container.DataItem("LeadGrpId") %>'>
														</asp:textbox>
													</ItemTemplate>
												</asp:TemplateColumn>
											</Columns>
										</asp:datagrid></TD>
								</TR>
							</TABLE>
							<TABLE width="100%" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td style="TEXT-ALIGN: center">
										<asp:Button ID="btnAssignLeadGrp" Runat="server" Text="Setup Groups"  />
									</td>
								</tr>
							</TABLE>
							<!-- end content table -->
						</div>
					</td>
				</tr>
			</table>
			 <div id="footer">&nbsp;Copyright &copy; FAME 2005 - <%=Year(DateTime.Now).ToString%>. All rights reserved.</div>
		</form>
	</body>
</HTML>


