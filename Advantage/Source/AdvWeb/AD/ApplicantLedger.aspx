<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="ApplicantLedger.aspx.vb" Inherits="ApplicantLedger" EnableEventValidation="false" %>

<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../Scripts/Advantage.Client.AD.js"></script>
    <script src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">
        function ShowEditForm(id, rowIndex, leadid, transcodeid, transreference, transdescrip, transamount, isenrolled) {
            var grid = $find("<%= radApplicantLedger.ClientID%>");
            var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
            grid.get_masterTableView().selectItem(rowControl, true);
            var transdate = "<%=Date.Now%>";  //$telerik.findElement(rowControl, "lblItemTransDate");
          var campusid = "<%= campusId %>";
          var transtypeid = 1;
          var voided = 0;
          var moduser = "<%=Session("UserName")%>";
          var moddate = "<%=Date.Now%>";
          //showwindow(id, leadid, transcodeid, transreference, transdescrip + ' - Adjustment', transamount, transdate, campusid, transtypeid, isenrolled, voided, moduser, moddate);
          showwindow(id, leadid, transcodeid, transreference, transdescrip, transamount, transdate, campusid, transtypeid, isenrolled, voided, moduser, moddate);
        }
        function showwindow(transactionid, leadid, transcodeid, transreference, transdescrip, transamount, transdate, campusid, transtypeid, isenrolled, voided, moduser, moddate) {
            var oWnd = $find("<%= DialogWindow.ClientID %>");
            //alert(transactionid);
            var strURL = "ReversalReason.aspx?transactionid=" + transactionid + '&leadid=' + leadid + '&transcodeid=' + transcodeid + '&transreference=' + transreference + '&transdescrip=' + transdescrip + '&transamount=' + transamount + '&transdate=' + transdate + '&campusid=' + campusid + '&transtypeid=' + transtypeid + '&isenrolled=' + isenrolled + '&voided=' + voided + '&moduser=' + moduser + '&moddate=' + moddate
            oWnd.setUrl(strURL);
            oWnd.show();
        }
        function OnClientClose(oWnd, args) {
        }
        function voidtransaction(id, rowIndex) {
            //alert("yes");
            var grid = $find("<%= radApplicantLedger.ClientID%>");
            var rowControl = grid.get_masterTableView().get_dataItems()[rowIndex].get_element();
            grid.get_masterTableView().selectItem(rowControl, true);
            var lblTransactionId = $telerik.findElement(rowControl, "Label1");
            //alert(lblTransactionId.value);
            showConfirm(id);
        }
        function showConfirm(transactionid) {
            var oWnd = $find("<%= VoidWindow.ClientID%>");
            var strURL = "VoidTransaction.aspx?transactionid=" + transactionid
            oWnd.setUrl(strURL);
            oWnd.show();
        }

        function callBackFn(confirmed) {
            if (confirmed) {
                var strTransactionId = "<%=strTransacId%>";
              //alert(strTransactionId);
              //e.item.itemindex always returns 0
              //so we need to pass in TransactionId
              $find('<%= radApplicantLedger.ClientID%>').get_masterTableView().fireCommand("void", strTransactionId);
            }

        }
        function callbackeventargs(sender, eventArgs) {
            alert(eventArgs.getDataKeyValue("TransactionId"));
        }
    </script>
    <style type="text/css">
        .RadGrid caption {
            background-color: #6b8dc2;
            text-align: center;
            font-weight: bold;
            height: 25px;
            color: white;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" runat="Server">
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelContent" runat="server" Transparency="30">
        <div class="loading">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/loading3.gif" AlternateText="loading" />
        </div>
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="radApplicantLedger">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radApplicantLedger" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="DialogWindow">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radApplicantLedger" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="VoidWindow">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="radApplicantLedger" LoadingPanelID="RadAjaxLoadingPanelContent" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Width="100%" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" Enabled="False" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnNew" runat="server" Enabled="False" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server" Enabled="False" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button></td>

                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="98%"
                            border="0">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                        <telerik:RadGrid ID="radApplicantLedger" runat="server" AutoGenerateColumns="false"
                                            OnItemCommand="RadApplicantLedgerItemCommand" OnDeleteCommand="RadApplicantLedgerDeleteCommand" EnableViewState="false">
                                            <MasterTableView Caption="Applicant Ledger" DataKeyNames="TransactionId, LeadId, TransCodeId, TransReference, TransDescription, TransactionAmount, TransDate, isEnrolled, Voided, ReversalReason"
                                                AlternatingItemStyle-BackColor="#eaf4ff">
                                                <Columns>
                                                    <telerik:GridTemplateColumn UniqueName="Actions" AllowFiltering="false" HeaderStyle-Width="160px"
                                                        ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="11px">
                                                        <HeaderStyle ForeColor="White" Font-Bold="true" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblHeaderReversalReason" runat="server" Text='Actions'></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkPrint" Text="P" runat="server" Width="12px" CommandName="Print"
                                                                BorderStyle="Solid" Style="text-decoration: none; text-align: center" BorderWidth="1"
                                                                BorderColor="#66CCFF" ToolTip="Print Receipt" CausesValidation="false"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkVoid" Text="V" runat="server" Width="12px" CommandName="Void"
                                                                BorderStyle="Solid" CommandArgument='<%# Eval("TransactionId")%>'
                                                                Style="text-decoration: none; text-align: center" BorderWidth="1" BorderColor="#66CCFF"
                                                                ToolTip="Void Transaction" CausesValidation="false"></asp:LinkButton>
                                                            <%--<asp:LinkButton ID="lnkReverse" Text="A" runat="server" Width="12px" CommandName="Reverse" BorderStyle="Solid" 
														style="text-decoration:none;text-align:center" BorderWidth="1" BorderColor="#66CCFF" 
														ToolTip="Adjust Transaction" CausesValidation="false"></asp:LinkButton>--%>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn UniqueName="TransDate" AllowFiltering="false" HeaderStyle-Width="120px"
                                                        ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="10px">
                                                        <HeaderStyle ForeColor="White" Font-Bold="true" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblHeaderTransDate" runat="server" Text="Date"></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemTransDate" runat="server" Text='<%# Eval("TransDate") %>'
                                                                CssClass="labelmaintenance"></asp:Label>
                                                            <asp:Label ID="lblLeadId" runat="server" Text='<%# Eval("LeadId")%>'
                                                                Visible="false"></asp:Label>
                                                            <asp:Label ID="lblTransactionId" runat="server" Text='<%# Eval("TransactionId")%>'
                                                                Visible="False"></asp:Label>
                                                            <asp:Label ID="lblVoid" runat="server" Text='<%# Eval("Voided")%>'
                                                                Visible="false"></asp:Label>
                                                            <asp:Label ID="lblReversalReason" runat="server" Text='<%# Eval("ReversalReason")%>'
                                                                Visible="false"></asp:Label>
                                                            <asp:Label ID="lblTransCodeId" runat="server" Text='<%# Eval("TransCodeId")%>'
                                                                Visible="false"></asp:Label>
                                                            <asp:Label ID="lblIsEnrolled" runat="server" Text='<%# Eval("IsEnrolled")%>'
                                                                Visible="false"></asp:Label>
                                                            <asp:HiddenField ID="Label1" runat="server" Value='<%# Eval("TransactionId")%>' />
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn UniqueName="TransReference" AllowFiltering="false" HeaderStyle-Width="160px"
                                                        ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="12px">
                                                        <HeaderStyle ForeColor="White" Font-Bold="true" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblHeaderTransReference" runat="server" Text='Reference'></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemTransReference" runat="server" Text='<%# Eval("TransReference") %>'
                                                                CssClass="labelmaintenance"></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn UniqueName="DocumentId" AllowFiltering="false" HeaderStyle-Width="160px"
                                                        ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="11px">
                                                        <HeaderStyle ForeColor="White" Font-Bold="true" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" Wrap="false" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblHeaderDocumentId" runat="server" Text='Document Id'></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemDocumentId" runat="server" Text='<%# Eval("DocumentId") %>'
                                                                CssClass="labelmaintenance"></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn UniqueName="TransactionCode" AllowFiltering="false" HeaderStyle-Width="160px"
                                                        ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="11px">
                                                        <HeaderStyle ForeColor="White" Font-Bold="true" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblHeaderTransactionCode" runat="server" Text='Code'></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemTransactionCode" runat="server" Text='<%# Eval("TransactionCode") %>'
                                                                CssClass="labelmaintenance"></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn UniqueName="TransactionDescription" AllowFiltering="false" HeaderStyle-Width="160px"
                                                        ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="11px">
                                                        <HeaderStyle ForeColor="White" Font-Bold="true" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblHeaderTransactionDescription" runat="server" Text='Description'></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemTransactionDescription" runat="server" Text='<%# Eval("TransDescription")%>'
                                                                CssClass="labelmaintenance"></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn UniqueName="TransactionType" AllowFiltering="false" HeaderStyle-Width="160px"
                                                        ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="11px">
                                                        <HeaderStyle ForeColor="White" Font-Bold="true" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblHeaderTransactionType" runat="server" Text='Type'></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemTransactionType" runat="server" Text='<%# Eval("TransactionType") %>'
                                                                CssClass="labelmaintenance"></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn UniqueName="Postedby" AllowFiltering="false" HeaderStyle-Width="160px"
                                                        ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="11px">
                                                        <HeaderStyle ForeColor="White" Font-Bold="true" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" Wrap="false" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblHeaderPostedby" runat="server" Text='User'></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemPostedby" runat="server" Text='<%# Eval("Postedby") %>'
                                                                CssClass="labelmaintenance"></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridTemplateColumn UniqueName="TransactionAmount" AllowFiltering="false" HeaderStyle-Width="160px"
                                                        ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="11px">
                                                        <HeaderStyle ForeColor="White" Font-Bold="true" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <ItemStyle HorizontalAlign="Right" Font-Names="Segoe UI, Arial, sans-serif" Font-Size="12px" />
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblHeaderTransactionAmount" runat="server" Text='Amount'></asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblItemTransactionAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.TransactionAmount", "{0:c}") %>'
                                                                CssClass="labelmaintenance"></asp:Label>
                                                        </ItemTemplate>
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridBoundColumn UniqueName="TransactionId" DataField="TransactionId" Display="false" />
                                                </Columns>
                                            </MasterTableView>
                                            <ClientSettings EnablePostBackOnRowClick="false">
                                                <Selecting AllowRowSelect="false" />
                                            </ClientSettings>

                                        </telerik:RadGrid>
                                        <asp:Label ID="lblNoRowsMessage" runat="server" Visible="false" Style="font-family: Verdana; font-size: 12px; color: blue;"></asp:Label>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- start validation panel-->
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
                ErrorMessage="CustomValidator"></asp:CustomValidator>
            <asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
                ShowMessageBox="True"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>
    <telerik:RadWindow ID="DialogWindow" Behaviors="Close, Move" VisibleStatusbar="false"
        ReloadOnShow="false" runat="server" VisibleOnPageLoad="false" Top="0" Left="20" Title="Enter Reason for Adjusting Transaction"
        OnClientClose="OnClientClose" AutoSize="false" Modal="true" OffsetElementID="offsetElement"
        Width="550px" Height="100px" AutoSizeBehaviors="Width, Height" EnableViewState="false">
    </telerik:RadWindow>
    <telerik:RadWindow ID="VoidWindow" Behaviors="Close, Move" VisibleStatusbar="false"
        ReloadOnShow="false" runat="server" VisibleOnPageLoad="false" Top="0" Left="20" Title="Void Transaction"
        OnClientClose="OnClientClose" AutoSize="false" Modal="true" OffsetElementID="offsetElement"
        Width="320px" Height="120px" AutoSizeBehaviors="Width, Height" EnableViewState="false">
    </telerik:RadWindow>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>

