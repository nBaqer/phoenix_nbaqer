﻿
Imports Advantage.Business.Objects
Imports FAME.AdvantageV1.Common

Namespace AdvWeb.AD
    Partial Class AdAleadEnrollment
        Inherits BasePage
        Dim pObj As New UserPagePermissionInfo
        Dim _resourceId As Integer
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As EventArgs) Handles Me.Load
            'Get LeadId
            Dim objStudentState As StudentMRU = GetObjStudentState()
            If objStudentState Is Nothing Then
                RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
                Exit Sub
            End If

            Master.Master.PageObjectId = objStudentState.LeadId.ToString()
            Master.Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
            Master.Master.SetHiddenControlForAudit()
            Dim advantageUserState As User = AdvantageSession.UserState
            Dim resourceId = HttpContext.Current.Request.Params("resid")
            Dim campusid as String= Master.Master.CurrentCampusId
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusid)

            If Me.Master.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusid.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
                End If
            End If
        End Sub
    End Class
End Namespace