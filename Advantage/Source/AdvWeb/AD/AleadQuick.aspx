﻿<%@ Page Title="Quick Lead Information" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="AleadQuick.aspx.vb" Inherits="AdvWeb.AD.ADAleadQuick" %>

<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" runat="Server">
    <!-- Use this to put content -->

    <link href="../css/AD/LeadQuickPage.css" rel="stylesheet" />
    <link href="../css/lead_toolbar.css" rel="stylesheet" />
    <%--  <script src="../Scripts/Advantage.Client.AD.js"></script> --%>
    <div id="QuickToolBar"></div>
    <div id="wrapperQuick" style="overflow: auto; position: fixed; max-height: 840px; width: 100%; top: 140px;">

        <div id="containerQuickInfoPage" data-role="validator">
            <div id="quickFirstPanel">
                <br />
                <label class="quickTitle">Quick Lead</label>
            </div>
            <div id="dvGeneralInfo">
                <br />
                <div id="dvGiBar" class="contentcellheaderBottom label"><b>General Information</b></div>
                <br />
            </div>
            <br />
            <div id="dvSourceBar" style="display: none" class="contentcellheaderBottom label"><b>Source</b></div>
            <div id="dvSource" style="display: none">
                <br />
            </div>
            <br />
            <div id="dvOtherBar" class="contentcellheaderBottom label"><b>Other</b></div>
            <br />
            <div class="clearfix"></div>
            <div id="dvOther"></div>
            <br />
            <div id="dvLeadGrpBar" class="contentcellheaderBottom label"><b>Lead Groups</b><span style="color: red">*</span></div>
            <br />
            <div class="clearfix"></div>
            <div id="dvLeadGrp" style="width: 15%" data-validation="oneChecked"></div>
            <br />
            <div id="dvCustomBar" style="display: none" class="contentcellheaderBottom label"><b>Custom</b></div>
            <div id="dvCustom" style="display: none">
                <br />
            </div>
            <input type="hidden" id="hiddenPhoneId" value="0">
            <input type="hidden" id="hiddenEmailPrimaryId" value="0">
        </div>
        <div id="WindowsDuplicate" style="display: none">
            <span class="k-toolbar">The following potential duplicates was/where found:</span>
            <div id="infoPageDupGrid"></div>
            <div id="buttonPanel" class="downpanel">
                <button id="insertLead" type="button" class="k-button">Save New Lead</button>
                <button id="cancelLead" type="button" class="k-button">Cancel Save</button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            //var manager = new AD.LeadQuickPage();
            var leadQuickPage = new AD.LeadQuickPage();
            console.log(leadQuickPage);
            if (AD.isNewLead) {
                AD.ajaxChecker = setInterval(function () {
                    if (leadQuickPage != undefined) {
                        leadQuickPage.validateAjaxRequestFinished();
                    }
                }, 3000);
            }

        });




        // var element = document.getElementById("wrapperQuick");
        //element.addEventListener("focus", processFocus);


    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>
