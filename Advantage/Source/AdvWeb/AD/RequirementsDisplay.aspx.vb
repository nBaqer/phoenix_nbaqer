Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade

Partial Class AD_RequirementsDisplay
    Inherits Page
    Protected LeadId As String '= "6F9F4BFF-7944-4A4E-AAFC-C1A4F3758893"
    Protected PrgVerId As String '= "177BE3B3-FF13-4EC7-B500-225566FAEC9A"
    Protected strEnrollDate As Date '= "12/12/2006"
    Dim ds As New DataSet
    Dim CampusId As String
    Dim PrgVerName As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Dim sEnrollStatus As String
        Dim LeadName As String

        LeadId = Request.QueryString("LeadId")
        PrgVerId = Request.QueryString("PrgVerId")
        LeadName = Request.QueryString("LeadName")
        sEnrollStatus = Request.QueryString("EnrollStatus")
        PrgVerName = Request.QueryString("PrgVersion")
        CampusId = Request.QueryString("CampusId")

        Session("PrgVerId") = PrgVerId

        Label1.Text = "Requirements for " & PrgVerName.ToString

        If Not Request.QueryString("EnrollDate") = "" Then
            strEnrollDate = CType(Request.QueryString("EnrollDate"), Date).ToShortDateString
        Else
            strEnrollDate = Date.Now.ToShortDateString
        End If
        If sEnrollStatus = "Enroll" Then
            lblEnrollStatus.Text = LeadName & " can be enrolled since the admission requirements have been completed successfully."
        Else
            lblEnrollStatus.Text = "<b>" & LeadName & " cannot be enrolled " & "</b>" & " since the admission requirements have not been completed successfully."
        End If

        If PrgVerId.ToLower = "none" Then
            ' If not Program version is selected 
            ' bring in school level requirements and requirements assigned directly to lead group
            ds = (New AdReqsFacade).GetSchoolLevelAdmissionRequirementsNoProgramVersion(LeadId, PrgVerId, strEnrollDate, CampusId)
            If Not IsPostBack Then
                'Display mandatory requirements
                If ds.Tables(1).Rows.Count >= 1 Then
                    DataList1.Visible = True
                    DataList1.DataSource = ds.Tables(1)
                    DataList1.DataBind()
                Else
                    DataList1.Visible = False
                End If

                'As no program version was selected, no requirements for program version
                dgReqsProgramVersion.Visible = False
                lblReqsProgramVersionStatus.Visible = False
                Label1.Visible = False
                lblReqsProgramVersionStatus.Text = "No Program Version Selected"

                If ds.Tables(0).Rows.Count >= 1 Then
                    DataList3NoPrgVersion.Visible = True
                    DataList3NoPrgVersion.DataSource = ds.Tables(0)
                    DataList3NoPrgVersion.DataBind()
                Else
                    DataList3NoPrgVersion.Visible = False
                End If

                Dim dlItems As DataListItemCollection
                Dim dlItem As DataListItem
                Dim z As Integer
                Dim dgGrid As DataGrid
                Dim dgItems As DataGridItemCollection
                Dim dgItem As DataGridItem

                If ds.Tables(1).Rows.Count >= 1 And ds.Tables(0).Rows.Count >= 1 Then
                    dlItems = DataList1.Items
                    For z = 0 To dlItems.Count - 1
                        dlItem = dlItems.Item(z)
                        dgGrid = CType(dlItem.FindControl("dgSchoolLevelRequirements"), DataGrid)
                        If dgGrid.Items.Count >= 1 Then
                            dgItems = dgGrid.Items
                            dgItem = dgItems.Item(0)
                            Try
                                Dim chkReqs As CheckBoxList = CType(dgItem.FindControl("chkRequirements"), CheckBoxList)
                                If ds.Tables(2).Rows.Count >= 1 Then
                                    chkReqs.Visible = True
                                    chkReqs.DataSource = ds.Tables(2)
                                    chkReqs.DataTextField = "FullName"
                                    chkReqs.DataValueField = "adReqId"
                                    chkReqs.DataBind()
                                Else
                                    chkReqs.Visible = False
                                    Exit Try
                                End If

                                For Each Item As ListItem In chkReqs.Items
                                    If InStr(Item.Text, "(Pass") >= 1 Or InStr(Item.Text, "(App") >= 1 Or InStr(Item.Text, "(Over") >= 1 Then
                                        Item.Selected = True
                                    End If
                                Next
                            Catch ex As Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                            End Try
                        End If

                    Next
                End If

                Dim dlItems1 As DataListItemCollection
                Dim dlItem1 As DataListItem
                Dim z1 As Integer
                Dim dgGrid3 As DataGrid
                Dim dgItems3 As DataGridItemCollection
                Dim dgItem3 As DataGridItem

                Dim y As Integer
                If ds.Tables(0).Rows.Count >= 1 Then
                    dlItems1 = DataList3NoPrgVersion.Items
                    For z1 = 0 To dlItems1.Count - 1
                        dlItem1 = dlItems1.Item(z1)
                        Dim leadgrpId As String = CType(dlItem1.FindControl("lblLeadGrpId"), Label).Text
                        dgGrid3 = CType(dlItem1.FindControl("dgReqsToLeadGrpNoPrgVersion"), DataGrid)
                        dgItems3 = dgGrid3.Items
                        Dim dsReq3 As DataSet
                        For y = 0 To dgItems3.Count - 1
                            dgItem3 = dgItems3.Item(0)
                            Dim chkReqs2 As CheckBoxList = CType(dgItem3.FindControl("chkReqs3"), CheckBoxList)
                            dsReq3 = (New AdReqsFacade).GetRequirementsAssignedToLeadReqGroupNoPrgVersion(LeadId, strEnrollDate, leadgrpId, CampusId)
                            If dsReq3.Tables(0).Rows.Count >= 1 Then
                                chkReqs2.Visible = True
                                chkReqs2.DataSource = dsReq3
                                chkReqs2.DataTextField = "FullName"
                                chkReqs2.DataValueField = "adReqId"
                                chkReqs2.DataBind()

                                For Each Item As ListItem In chkReqs2.Items
                                    If InStr(Item.Text, "(Pass") >= 1 Or InStr(Item.Text, "(App") >= 1 Or InStr(Item.Text, "(Over") >= 1 Then
                                        Item.Selected = True
                                    End If
                                Next
                            Else
                                chkReqs2.Visible = False
                            End If
                        Next
                    Next
                End If
            End If
        Else
            ds = (New AdReqsFacade).GetSchoolLevelAdmissionRequirements(LeadId, PrgVerId, strEnrollDate, CampusId)
            If Not IsPostBack Then
                'School Level Requirements Summary
                If ds.Tables(1).Rows.Count >= 1 Then
                    DataList1.Visible = True
                    DataList1.DataSource = ds.Tables(1)
                    DataList1.DataBind()
                Else
                    DataList1.Visible = False
                End If

                'Requirements Assigned to Program Version
                If ds.Tables(3).Rows.Count >= 1 Then
                    dgReqsProgramVersion.Visible = True
                    lblReqsProgramVersionStatus.Visible = False
                    Label1.Visible = True
                    dgReqsProgramVersion.DataSource = ds.Tables(3)
                    dgReqsProgramVersion.DataBind()
                Else
                    dgReqsProgramVersion.Visible = False
                    lblReqsProgramVersionStatus.Visible = True
                    Label1.Visible = True
                    lblReqsProgramVersionStatus.Text = "No Requirements available for " & PrgVerName
                End If


                If ds.Tables(0).Rows.Count >= 1 Then
                    DataList3.Visible = True
                    DataList3.DataSource = ds.Tables(0)
                    DataList3.DataBind()
                Else
                    DataList3.Visible = False
                End If

                Dim dlItems As DataListItemCollection
                Dim dlItem As DataListItem
                Dim z As Integer
                Dim dgGrid As DataGrid
                Dim dgItems As DataGridItemCollection
                Dim dgItem As DataGridItem

                If ds.Tables(1).Rows.Count >= 1 And ds.Tables(0).Rows.Count >= 1 Then
                    dlItems = DataList1.Items
                    For z = 0 To dlItems.Count - 1
                        dlItem = dlItems.Item(z)
                        dgGrid = CType(dlItem.FindControl("dgSchoolLevelRequirements"), DataGrid)
                        If dgGrid.Items.Count >= 1 Then
                            dgItems = dgGrid.Items
                            dgItem = dgItems.Item(0)
                            Try
                                Dim chkReqs As CheckBoxList = CType(dgItem.FindControl("chkRequirements"), CheckBoxList)
                                If ds.Tables(2).Rows.Count >= 1 Then
                                    chkReqs.Visible = True
                                    chkReqs.DataSource = ds.Tables(2)
                                    chkReqs.DataTextField = "FullName"
                                    chkReqs.DataValueField = "adReqId"
                                    chkReqs.DataBind()
                                Else
                                    chkReqs.Visible = False
                                    Exit Try
                                End If

                                For Each Item As ListItem In chkReqs.Items
                                    If InStr(Item.Text, "(Pass") >= 1 Or InStr(Item.Text, "(App") >= 1 Or InStr(Item.Text, "(Over") >= 1 Then
                                        Item.Selected = True
                                    End If
                                Next
                            Catch ex As Exception
                             	Dim exTracker = new AdvApplicationInsightsInitializer()
                            	exTracker.TrackExceptionWrapper(ex)

                            End Try
                        End If
                    Next
                End If

                Dim dlItems1 As DataListItemCollection
                Dim dlItem1 As DataListItem
                Dim z1 As Integer
                Dim dgGrid1, dgGrid3 As DataGrid
                Dim dgItems1, dgItems2, dgItems3 As DataGridItemCollection
                Dim dgItem1, dgItem2, dgItem3 As DataGridItem

                If ds.Tables(3).Rows.Count >= 1 Then
                    dgGrid3 = CType(dgReqsProgramVersion, DataGrid)
                    dgItems = dgGrid3.Items
                    dgItem = dgItems.Item(0)
                    Try
                        Dim chkRequirementsToProgramVersion1 As CheckBoxList = CType(dgItem.FindControl("chkRequirementsToProgramVersion"), CheckBoxList)
                        If ds.Tables(3).Rows.Count >= 1 Then
                            chkRequirementsToProgramVersion1.Visible = True
                            chkRequirementsToProgramVersion1.DataSource = ds.Tables(3)
                            chkRequirementsToProgramVersion1.DataTextField = "FullName"
                            chkRequirementsToProgramVersion1.DataValueField = "adReqId"
                            chkRequirementsToProgramVersion1.DataBind()
                        Else
                            chkRequirementsToProgramVersion1.Visible = False
                        End If
                        For Each Item As ListItem In chkRequirementsToProgramVersion1.Items
                            If InStr(Item.Text, "(Pass") >= 1 Or InStr(Item.Text, "(App") >= 1 Or InStr(Item.Text, "(Over") >= 1 Then
                                Item.Selected = True
                            End If
                        Next
                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                    End Try
                End If

                Dim v, x, y As Integer
                If ds.Tables(0).Rows.Count >= 1 Then
                    dlItems1 = DataList3.Items
                    Dim dgGrid2 As DataGrid
                    For z1 = 0 To dlItems1.Count - 1
                        dlItem1 = dlItems1.Item(z1)
                        Dim leadgrpId As String = CType(dlItem1.FindControl("lblLeadGrpId"), Label).Text
                        dgGrid1 = CType(dlItem1.FindControl("dgReqsToReqGroup"), DataGrid)
                        dgItems1 = dgGrid1.Items
                        For v = 0 To dgItems1.Count - 1
                            dgItem1 = dgItems1.Item(v)
                            Try
                                Dim lbl As Label = CType(dgItem1.FindControl("lblReqGrpId"), Label)
                                Dim dsGetReqs As New DataSet
                                Dim grandChild As DataGrid
                                grandChild = CType(dgItem1.FindControl("dg1"), DataGrid)
                                dsGetReqs = (New AdReqsFacade).GetRequirementsByLeadGroupAndReqGroup(LeadId, PrgVerId, strEnrollDate, leadgrpId, lbl.Text, CampusId)
                                If dsGetReqs.Tables(0).Rows.Count >= 1 Then
                                    grandChild.Visible = True
                                    grandChild.DataSource = dsGetReqs.Tables(0)
                                    grandChild.DataBind()
                                Else
                                    grandChild.Visible = False
                                End If

                                dgGrid2 = CType(dgItem1.FindControl("dg1"), DataGrid)
                                dgItems2 = dgGrid2.Items
                                For x = 0 To dgItems2.Count - 1
                                    dgItem2 = dgItems2.Item(0)
                                    Dim chkReqs1 As CheckBoxList = CType(dgItem2.FindControl("chkReqs"), CheckBoxList)
                                    If dsGetReqs.Tables(0).Rows.Count >= 1 Then
                                        chkReqs1.Visible = True
                                        chkReqs1.DataSource = dsGetReqs.Tables(0)
                                        chkReqs1.DataTextField = "FullName"
                                        chkReqs1.DataValueField = "adReqId"
                                        chkReqs1.DataBind()

                                        For Each Item As ListItem In chkReqs1.Items
                                            If InStr(Item.Text, "(Pass") >= 1 Or InStr(Item.Text, "(App") >= 1 Or InStr(Item.Text, "(Over") >= 1 Then
                                                Item.Selected = True
                                            End If
                                        Next
                                    Else
                                        chkReqs1.Visible = False
                                    End If
                                Next
                            Finally
                            End Try
                        Next
                        dgGrid3 = CType(dlItem1.FindControl("dgReqsToLeadGrp"), DataGrid)
                        dgItems3 = dgGrid3.Items
                        Dim dsReq3 As New DataSet
                        For y = 0 To dgItems3.Count - 1
                            dgItem3 = dgItems3.Item(0)
                            Dim chkReqs2 As CheckBoxList = CType(dgItem3.FindControl("chkReqs3"), CheckBoxList)
                            dsReq3 = (New AdReqsFacade).GetRequirementsAssignedToLeadReqGroup(LeadId, PrgVerId, strEnrollDate, leadgrpId, CampusId)
                            If dsReq3.Tables(0).Rows.Count >= 1 Then
                                chkReqs2.Visible = True
                                chkReqs2.DataSource = dsReq3
                                chkReqs2.DataTextField = "FullName"
                                chkReqs2.DataValueField = "adReqId"
                                chkReqs2.DataBind()

                                For Each Item As ListItem In chkReqs2.Items
                                    If InStr(Item.Text, "(Pass") >= 1 Or InStr(Item.Text, "(App") >= 1 Or InStr(Item.Text, "(Over") >= 1 Then
                                        Item.Selected = True
                                    End If
                                Next
                            Else
                                chkReqs2.Visible = False
                            End If
                        Next
                    Next
                End If
            End If
        End If
    End Sub
    Protected Sub DataList1_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles DataList1.ItemDataBound
        If DataList1.Visible = True Then
            Dim childgrid As DataGrid
            childgrid = e.Item.FindControl("dgSchoolLevelRequirements")
            If ds.Tables(2).Rows.Count >= 1 Then
                childgrid.DataSource = ds.Tables(2)
                childgrid.DataBind()
                childgrid.Visible = True
            Else
                childgrid.Visible = False
            End If
        End If
    End Sub
    Protected Sub DataList3_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles DataList3.ItemDataBound
        Dim ReqFacade As New AdReqsFacade
        If DataList3.Visible = True Then
            Dim childgrid, secondchildgrid As DataGrid
            Dim leadgrpid As String
            Dim dsGetReqs As New DataSet
            leadgrpid = CType(e.Item.FindControl("lblLeadGrpId"), Label).Text
            If Not Trim(Session("PrgVerId")).ToString.ToLower = "none" Then
                Dim lblText As Label = CType(e.Item.FindControl("lbl1"), Label)
                lblText.Text = " - Requirement Groups (assigned to " & PrgVerName & " )" & "/Requirements"
                dsGetReqs = (New AdReqsFacade).GetRequirementsByLeadGroup(LeadId, PrgVerId, strEnrollDate, leadgrpid, CampusId)
                childgrid = e.Item.FindControl("dgReqsToReqGroup")
                If dsGetReqs.Tables(0).Rows.Count >= 1 Then
                    childgrid.Visible = True
                    childgrid.DataSource = dsGetReqs.Tables(0)
                    childgrid.DataBind()
                Else
                    childgrid.Visible = False
                End If

                dsGetReqs = (New AdReqsFacade).GetRequirementsAssignedToLeadReqGroup(LeadId, PrgVerId, strEnrollDate, leadgrpid, CampusId)
                secondchildgrid = e.Item.FindControl("dgReqsToLeadGrp")
                If dsGetReqs.Tables(0).Rows.Count >= 1 Then
                    secondchildgrid.Visible = True
                    secondchildgrid.DataSource = dsGetReqs.Tables(0)
                    secondchildgrid.DataBind()
                Else
                    secondchildgrid.Visible = False
                End If
            Else
                'dsGetReqs = ReqFacade.GetReqsToLeadApplicants(LeadId, PrgVerId, strEnrollDate, leadgrpid, CampusId)
                'secondchildgrid = e.Item.FindControl("dgReqsToLeadGrp")
                'If dsGetReqs.Tables(0).Rows.Count >= 1 Then
                '    secondchildgrid.Visible = True
                '    secondchildgrid.DataSource = dsGetReqs.Tables(0)
                '    secondchildgrid.DataBind()
                'Else
                '    secondchildgrid.Visible = False
                'End If
            End If
        End If
    End Sub
    Protected Sub DataList3NoPrgVersion_ItemDataBound(ByVal sender As Object, ByVal e As DataListItemEventArgs) Handles DataList3NoPrgVersion.ItemDataBound
        Dim ReqFacade As New AdReqsFacade
        If DataList3.Visible = True Then
            Dim secondchildgrid As DataGrid
            Dim leadgrpid As String
            Dim dsGetReqs As New DataSet
            leadgrpid = CType(e.Item.FindControl("lblLeadGrpId"), Label).Text
            If Trim(Session("PrgVerId")).ToString.ToLower = "none" Then
                dsGetReqs = (New AdReqsFacade).GetRequirementsAssignedToLeadReqGroupNoPrgVersion(LeadId, strEnrollDate, leadgrpid, CampusId)
                secondchildgrid = e.Item.FindControl("dgReqsToLeadGrpNoPrgVersion")
                If dsGetReqs.Tables(0).Rows.Count >= 1 Then
                    secondchildgrid.Visible = True
                    secondchildgrid.DataSource = dsGetReqs.Tables(0)
                    secondchildgrid.DataBind()
                Else
                    secondchildgrid.Visible = False
                End If
            End If
        End If
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    Public Sub BIndToolTip()
        Dim i As Integer
        Dim ctl As Control
        For Each ctl In Page.Form.Controls
            If TypeOf ctl Is ListControl Then
                For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
                    DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
                Next
            End If
            If TypeOf ctl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctl)
            End If
            If TypeOf ctl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctl)
            End If
        Next
    End Sub
    Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
        Dim ctrl As Control
        Dim j As Integer
        For Each ctrl In Ctrlpanel.Controls
            If TypeOf ctrl Is ListControl Then
                For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
                    DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
                Next
            ElseIf TypeOf ctrl Is Panel Then
                BindToolTipForControlsInsideaPanel(ctrl)
            ElseIf TypeOf ctrl Is DataGrid Then
                BindToolTipForControlsInsideaGrid(ctrl)
            End If
        Next

    End Sub

    Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
        Dim j As Integer
        Dim itm As DataGridItem
        Dim ctrl As Control
        Dim ctrl1 As Control

        For Each itm In CtrlGrid.Items
            For Each ctrl In itm.Controls
                For Each ctrl1 In ctrl.Controls
                    If TypeOf ctrl1 Is ListControl Then
                        For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
                            DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
                        Next
                    End If
                Next
            Next
        Next
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        BIndToolTip()
    End Sub
End Class
