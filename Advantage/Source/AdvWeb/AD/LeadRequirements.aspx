﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="LeadRequirements.aspx.vb" Inherits="LeadRequirement" %>


<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script src="../js/CheckAll.js" type="text/javascript"></script>

    <link href="../Kendo/styles/kendo.dataviz.blueopal.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/Storage/storageCache.js"></script>
    <script type="text/javascript" src="../Scripts/common-util.js"></script>

    <script type="text/javascript" src="../Scripts/common/kendoControlSetup.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/RequirementDataSources/transactionCodeDataSource.js"></script>
    <%--<script type="text/javascript" src="../Scripts/DataSources/RequirementDataSources/reqListDataSource.js"></script>--%>
    <script type="text/javascript" src="../Scripts/DataSources/RequirementDataSources/requirementsDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/RequirementDataSources/requirementGrpDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/RequirementDataSources/requirementByGrpDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/RequirementDataSources/optionalRequirementsDataSource .js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/RequirementDataSources/requirementDetailsByReqIdDataSource.js"></script>

    <script type="text/javascript" src="../Scripts/viewModels/leadRequirements.js"></script>

    <script id="infoTemplate" type="text/x-kendo-template">
        <div class="infoMessageTemplate">
            <img width="32px" height="32px" src=#= src #   />
            <h3>#= title #</h3>
            <p>#= message #</p>
        </div>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Width="100%" Orientation="HorizontalTop" Scrolling="Both">
            <div id="leadRequirementBox">
                <h4 class="navyBlue title">
                    <span id="userFullName"></span> - <span id="leadRequirementStatus" class="red regularFontTitle">Pending Requirements</span>
                </h4>
                <div id="one">

                    <div id="maincontent1">

                        <%-- <span style='color: #000066; font-size: 16px; font-weight: bold;' id="userFullName"></span><span style='color: red; font-size: 16px; font-weight: bold;'></span>
                        <br />
                        <br />--%>
                        <h4 class="subTitle">Requirements</h4>
                        <br />
                        <div class="leadcontainer">
                            <div id ="updateBlocker1" class="updateBlocker" style="display: none; text-align: center;"><span style="font-weight: bold; font-size: 14px;">Update in Progress</span></div>
                            <div style="min-height: 50px;" class="leadmainbody">
                                <h4 class="subTitle">Mandatory</h4>
                                <div id="mandReqsGrid" data-bind="source: requirementsDataSource" style="width: 99%; "></div>
                            </div>

                            <br />

                            <div style="min-height: 50px;" class="leadmainbody">
                                <h4 class="subTitle">Mandatory - Groups</h4>
                                <div id="mandReqGrpGrid" data-bind="source: requirementGrpDataSource" style="width: 99%;"></div>
                            </div>

                            <br />

                            <div style="min-height: 50px;" class="leadmainbody" id="optionalDiv">
                                <h4 class="subTitle">Optional</h4>
                                <div id="optReqsGrid" data-bind="source: optRequirementsDataSource" style="width: 99%;"></div>
                            </div>
                        </div>


                    </div>
                </div>

                <div id="two">
                    <h4 class="subTitle" style="display: none" id="updateHeader">Update Requirement</h4>
                    <br />
                    <div class="leadcontainer" style="display: none; max-width: 540px;" id="updateContainer">
                    <div id ="updateBlocker2" class="updateBlocker" style="display: none; text-align: center;"><span style="font-weight: bold; font-size: 14px;">Update in Progress</span></div>
                        <br/>
                        <div style="min-height: 50px; display: none;" class="leadmainbody" id="documentUpdateDiv">

                            <table>
                                <tr>
                                    <td style="margin-left: 3px;">Document<span class="IsRequiredAsterisk">*</span></td>
                                    <td><span id="txtDocDescription"></span></td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 3px;"><span>Override</span></td>
                                    <td>
                                        <input id="chkDocOverridden" name="chkDocOverridden" type="checkbox" style="vertical-align: top" /></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top"><span style="vertical-align: top;">Reason</span></td>
                                    <td>
                                        <textarea id="txtDocOverReason" name="txtDocOverReason" rows="3" cols="50" disabled="disabled" maxlength="1000"></textarea></td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 3px;">Date Received<span class="IsRequiredAsterisk">*</span></td>
                                    <td>
                                        <input id="dtReceived" name="dtReceived" style="width: 110px;" /></td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 3px;">Document Status<span class="IsRequiredAsterisk">*</span></td>
                                    <td>
                                        <input id="docStatus" name="docStatus" class="reqDrop" style="width: 110px;" />&nbsp;&nbsp;<input id="dtApproval" name="dtApproval" style="width: 110px;" /></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <button id="btnDocSave" class="k-button" type="button">Save</button>&nbsp;&nbsp;
                                        <button id="btnDocCancel" class="k-button" type="button">Close</button></td>

                                </tr>
                                <tr id="fileUploadtr" style="display: none; height: 40px;">
                                    <td colspan="2" style="padding-right: 20px; vertical-align: top;" id="filesTd">
                                        <br />
                                        <br />
                                        <br />
                                        <h4>Upload Document</h4>
                                        <input name="files" id="files" type="file" style="width: 200px; height: 40px;" />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <h4>History</h4>
                                        <div id="docReqGrid" name="docReqGrid" data-bind="source: docReqDataSource"></div>
                                    </td>
                                </tr>
                            </table>

                        </div>

                        <div style="min-height: 50px; display: none" class="leadmainbody" id="testUpdateDiv">
                            <table style="width: 95%; margin: 10px;" class="testTable">
                                <tr>
                                    <th>Test Name</th>
                                    <th style="text-align: center">Completed</th>
                                    <th style="text-align: center">Result</th>
                                    <th style="text-align: center">Min Score</th>
                                </tr>
                                <tr>
                                    <td id="txtTestDescription">&nbsp;</td>
                                    <td style="width: 120px;">
                                        <input id="dtTestCompleted" name="dtTestCompleted" style='width: 120px;' /></td>
                                    <td style="width: 50px;">
                                        <input id="txtTestScore" name="txtTestScore" style='width: 50px;' maxlength="6" /></td>
                                    <td style="width: 65px; text-align: center"><span id="txtMinScore" style='width: 50px;'></span></td>
                                </tr>
                            </table>
                            <br />
                            <div>
                                <input id="chTestOverridden" name="chTestOverridden" type="checkbox" style="vertical-align: top" /><span style="vertical-align: top; margin-left: 1px;">Override</span>
                                <span style="vertical-align: top; margin-left: 20px;">Reason&nbsp;&nbsp;</span><textarea id="txtTestOverReason" name="txtTestOverReason" rows="3" cols="50" disabled="disabled" maxlength="1000"></textarea>
                            </div>

                            <div style="text-align: left;">
                                <button id="btnTestSave" class="k-button" type="button">Save</button>&nbsp;&nbsp;
                                <button id="btnTestCancel" class="k-button" type="button">Close</button>
                            </div>
                            <br />
                            <h4>History</h4>
                            <div id="testReqGrid" data-bind="source: reqDetailsByReqIdDataSource" style="width: 100%;"></div>

                        </div>
                        <div style="min-height: 50px; display: none;" class="leadmainbody" id="leadReqUpdateDiv">
                            <table style="width: 95%; margin: 10px;" class="testTable">
                                <tr>
                                    <th><span id='reqName'></span>&nbsp;Name</th>
                                    <th>Completed</th>
                                </tr>
                                <tr>
                                    <td id="txtEventDescription">&nbsp;</td>
                                    <td style="width: 120px;">
                                        <input id="dtEventCompleted" name="dtEventCompleted" style='width: 120px;' /></td>
                                </tr>
                            </table>
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <br />
                                        <div>
                                            <input id="chEventOverridden" name="chEventOverridden" type="checkbox" style="vertical-align: top" /><span style="vertical-align: top; margin-left: 1px;">Override</span>
                                            <span style="vertical-align: top; margin-left: 20px;">Reason&nbsp;&nbsp;</span><textarea id="txtEventReason" name="txtEventReason" rows="3" cols="50" disabled="disabled" maxlength="1000"></textarea>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                        <div style="text-align: left;">
                                            <button id="btnEventSave" class="k-button" type="button">Save</button>&nbsp;&nbsp;
                                            <button id="btnEventCancel" class="k-button" type="button">Close</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                        <h4>History</h4>
                                        <div id="reqDetailGrid" data-bind="source: reqDetailsByReqIdDataSource" style="width: 100%;"></div>
                                    </td>
                                </tr>

                            </table>



                        </div>
                        <div style="min-height: 50px; display: none;" class="leadmainbody" id="leadTransactionsUpdateDiv">

                            <table>

                                <tr>
                                    <td style="margin-left: 3px;">Transaction<span class="IsRequiredAsterisk">*</span></td>
                                    <td><span id="txtTransaction"></span></td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 3px;"><span style="vertical-align: top; margin-left: 1px;">Override</span></td>
                                    <td>
                                        <input id="chTranOverridden" name="chTranOverridden" type="checkbox" style="vertical-align: top" /></td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;"><span style="vertical-align: top;">Reason</span></td>
                                    <td>
                                        <textarea id="txtTranReason" name="txtTranReason" rows="3" cols="50" disabled="disabled" maxlength="1000" style="width: 80%;"></textarea></td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 3px;">Description<span class="IsRequiredAsterisk">*</span></td>
                                    <td>
                                        <input type="text" id="txtTranDescription" name="txtTranDescription" style="width: 195px;" /></td>
                                </tr>
                                <tr>
                                    <td style="margin-left: 3px;">Reference</td>
                                    <td>
                                        <input type="text" id="txtTranReference" name="txtTranReference" style="width: 195px;" /></td>
                                </tr>
                                <tr>
                                    <td>Transaction Code<span class="IsRequiredAsterisk">*</span></td>
                                    <td>
                                        <input id="lstTransactionCodes" name="lstTransactionCodes" style="width: 200px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Payment Type<span class="IsRequiredAsterisk">*</span></td>
                                    <td>
                                        <input id="lstPaymentTypes" name="lstPaymentTypes" style="width: 200px" />
                                    </td>
                                </tr>
                                <tr style="display: none;" id='tblPaymentReference'>
                                    <td><span id="paymentLabel"></span></td>
                                    <td>
                                        <input type="text" id="txtTranPaymentReference" name="txtTranPaymentReference" style="width: 200px;" maxlength="50" /></td>
                                </tr>
                                <tr>
                                    <td>Transaction Date<span class="IsRequiredAsterisk">*</span></td>
                                    <td>
                                        <input id="dtTransDate" name="dtTransDate" style="width: 120px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Amount<span class="IsRequiredAsterisk">*</span></td>
                                    <td>
                                        <input id="txtTranAmount" name="txtTranAmount" style="width: 115px; text-align: right; margin: 3px;" class="money-bank" />&nbsp;US. Dollars
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <br />
                                        <button id="btnTranSave" class="k-button" type="button">Post</button>&nbsp;&nbsp;
                                        <button id="btnTranSavePrint" class="k-button" type="button">Post & Print Receipt</button>&nbsp;&nbsp;
                                        <button id="btnTranCancel" class="k-button" type="button">Close</button></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <br />
                                        <h4>History</h4>
                                        <div id="tranReqGrid" data-bind="source: reqDetailsByReqIdDataSource" style="width: 100%;"></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div id="moreDetails" style="display: none">
                    <table style="width: 100%;" class="tranTable">
                        <tr>
                            <td class='tranTabletd' style="width: 30%;">Date</td>
                            <td><span id='tranDateDetail'></span></td>
                        </tr>
                        <tr>
                            <td class='tranTabletd' style="width: 30%;">Reference</td>
                            <td><span id='tranReferenceDetail'></span></td>
                        </tr>
                        <tr>
                            <td class='tranTabletd' style="width: 30%;">Document Id</td>
                            <td><span id='tranDocIdDetail'></span></td>
                        </tr>
                        <tr>
                            <td class='tranTabletd' style="width: 30%;">Code</td>
                            <td><span id='tranCodeDetail'></span></td>
                        </tr>
                        <tr>
                            <td class='tranTabletd' style="width: 30%;">Description</td>
                            <td><span id='tranDescriptionDetail'></span></td>
                        </tr>
                        <tr>
                            <td class='tranTabletd' style="width: 30%;">Type</td>
                            <td><span id='tranTypeDetail'></span></td>
                        </tr>
                        <tr>
                            <td class='tranTabletd' style="width: 30%;">User</td>
                            <td><span id='tranUserDetail'></span></td>
                        </tr>
                        <tr>
                            <td class='tranTabletd' style="width: 30%;">Amount</td>
                            <td><span id='tranAmountDetail'></span></td>
                        </tr>
                    </table>
                </div>
                <span id="kNotification"></span>
                <asp:HiddenField runat="server" ID="hdnUserId" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="hdnLeadId" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="postedTransactionId" ClientIDMode="Static" />
                <div class="clear"></div>
            </div>
        </telerik:RadPane>
    </telerik:RadSplitter>

    <script type="text/javascript">
        $(document).ready(function() {
            var toolbar = $("#LeadToolbar").data("kendoToolBar");    // as kendo.ui.ToolBar;
            toolbar.enable("#leadToolBarSave", false);
            toolbar.enable("#leadPrintButton", false);
            toolbar.enable("#leadToolBarDelete", false);
        });
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
</asp:Content>


