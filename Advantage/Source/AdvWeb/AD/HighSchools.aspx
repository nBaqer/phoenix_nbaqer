<%@ Page Language="vb" AutoEventWireup="false" Title="High School" MasterPageFile="~/NewSite.master" inherits="HighSchools" CodeFile="HighSchools.aspx.vb" %>
<%@ mastertype virtualpath="~/NewSite.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
		<link href="../CSS/localhost_lowercase.css" type="text/css" rel="stylesheet">
		<script language="javascript" src="../js/Capitalize.js" type="text/javascript"></script>
		<script language="javascript" src="../js/AuditHist.js" type="text/javascript"></script>
		   <script type="text/javascript">

			   function OldPageResized(sender, args) {
				   $telerik.repaintChildren(sender);
			   }
			   
   </script>
    <%--<script src="../Scripts/Watermark/WaterMark.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(function () {
        $("[id*=txtFirstName]").WaterMark();
        $("[id*=txtMiddleName]").WaterMark();
        $("[id*=txtLastName]").WaterMark();
    });
    </script>--%>
   </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>
	<telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
	VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" style="overflow:auto;width:550px;">
	<telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="620px" Scrolling="Y">

				<table id="Table1" cellSpacing="0" cellPadding="0" width="100%" border="0">
				<!-- begin leftcolumn -->
			
					<tr>
						<td class="listframe">
							<table id="Table2" cellSpacing="0" cellPadding="0" width="100%" border="0">
								<tr>
									<td class="listframetop">
										<table cellSpacing="0" cellPadding="0" width="100%" border="0">
											<tr>
												<td noWrap align="left" width="15%"><asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
												<td noWrap width="85%"><asp:radiobuttonlist id="radStatus" Runat="Server" RepeatDirection="Horizontal" AutoPostBack="true" CssClass="label">
														<asp:ListItem Text="Active" Selected="True" />
														<asp:ListItem Text="Inactive" />
														<asp:ListItem Text="All" />
													</asp:radiobuttonlist></td>
											</tr>
                                             <tr>
                                                <td noWrap align="left" width="15%"><asp:label id="Label1" Runat="server"><b class="tothemeshow">Institution Type</b></asp:label></td>
                                                <td noWrap align="left" width="85%">
                                                                <asp:DropDownList ID="ddlInstitutionType" runat="server" Font-Names="Segoe UI, Arial, sans-serif">
                                                                    <asp:ListItem Value="1">Colleges</asp:ListItem>
                                                                    <asp:ListItem Value="2">High Schools</asp:ListItem>
                                                                </asp:DropDownList>
                                                   </td>
                                            </tr>
                                           <tr>
															<td class="spacertables"></td>
													</tr>
                                            <tr>
                                                <td noWrap align="left" width="15%"><asp:label id="lblSearchLabel" Runat="server"><b class="tothemeshow">Institution Name</b></asp:label></td>
                                                <td noWrap align="left" width="85%"><asp:TextBox ID="txtSearch" Width="320px" runat="server" ToolTip="Search by School Name" Font-Names="Segoe UI, Arial, sans-serif"></asp:TextBox></td>
                                            </tr>
                                        </table>
                                        <table width="42%" border="0">
                                             <tr><td class="spacertables"></td></tr>
                                            <tr>
                                                <td class="SearchButton"><asp:Button ID="btnSearch" runat="server" CausesValidation="false" Text="Search" Style="margin-left:0px;"/></td>
                                            </tr>
										</table>
									</td>
								</tr>
								<tr>
									<td class="listframebottom">
										<div class="scrollleftfilters">
                                      <telerik:RadGrid ID="RadGrdPostScores" runat="server" Visible="true" AllowPaging="True"
                                                    AutoGenerateColumns="False" AllowFilteringByColumn="False"
                                                    OnNeedDataSource="RadGrdPostScores_NeedDataSource"
                                                    OnItemCommand="RadGrdPostScores_ItemCommand" 
                                                    Width="600px" PageSize="20" CssClass="MaintenanceGrid_Web20">
                                                <ClientSettings EnablePostBackOnRowClick="true">
                                                    <Selecting AllowRowSelect="True"></Selecting>
                                                </ClientSettings>
                                                <MasterTableView Width="100%" HorizontalAlign="NotSet"
                                                    AutoGenerateColumns="false" EditMode="InPlace" InsertItemPageIndexAction="ShowItemOnFirstPage"
                                                     NoMasterRecordsText="No maintenance item to display" 
                                                    DataKeyNames="HsId">
                                                    <HeaderStyle ForeColor="Black" />
                                                    <Columns>
                                                       <telerik:GridTemplateColumn UniqueName="HsId" HeaderStyle-Width="160px"
                                                            ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="11px" 
                                                             AllowFiltering="True"
                                                            FilterControlWidth="120px">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblHeaderDescription" runat="server" Text='High Schools'
                                                                    CssClass="labelboldmaintenance"></asp:Label>
                                                           </HeaderTemplate>
                                                            <HeaderStyle ForeColor="White" BackColor="#99CCFF" Font-Bold="true" />
                                                            <ItemStyle HorizontalAlign="Left" />
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDisplayDescription" runat="server" Text='<%# Eval("HSName") %>'
                                                                    CssClass="labelmaintenance"></asp:Label>
                                                                <asp:Label ID="lblHsID" runat="server" Text='<%# Eval("HsId") %>' Visible="false"
                                                                    CssClass="labelmaintenance"></asp:Label>
                                                            </ItemTemplate>
                                                        </telerik:GridTemplateColumn>
                                                    </Columns>
                                                </MasterTableView>
                                        </telerik:RadGrid>
    								</div>
									</td>
								</tr>
							</table>
						</td>
						<!-- end leftcolumn -->

			  </tr>
			  </table>
				</telerik:RadPane>
	<telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
		<asp:panel id="pnlRHS" Runat="server">
								<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0"> <!-- begin top menu (save,new,reset,delete,history)-->
									<TR>
										<TD class="menuframe" align="right">
											<asp:button id="btnSave" runat="server"  CssClass="save" Text="Save"></asp:button>
											<asp:button id="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:button>
											<asp:button id="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:button></TD>
										
									</TR>
								</TABLE> <!-- end top menu (save,new,reset,delete,history)--> <!--begin right column-->
								<table id="Table5" cellSpacing="0" cellPadding="0" width="100%" border="0" class="maincontenttable">
									<TR>
										<TD class="detailsframe" style="padding-left:50px;">
											<DIV class="scrollright"><!-- begin table content--> <!-- Two column template with no spacer in between cells -->  <!-- used in placement module for maintenance pages -->
												<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="60%" align="center">
													<tr>
															<td class="spacertables"></td>
													</tr>
													<TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblHSCode" Runat="server" Width="130px" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:textbox id="txtHSCode" Runat="server" MaxLength="50" CssClass="textbox" TabIndex="1" ></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblStatusId" Runat="server" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:dropdownlist id="ddlStatusId" runat="server" CssClass="dropdownlist" TabIndex="2" ></asp:dropdownlist></TD>
													</TR>
													<TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblHSName" Runat="server" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:textbox id="txtHSName" Runat="server" CssClass="textbox" MaxLength="100" Width="80%" TabIndex="3"></asp:textbox></TD>
													</TR>
                                                    <TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblInstitutionType" Runat="server" CssClass="label">Institution Type<font color="red">*</font></asp:label></TD>
														<TD class="twocolumncontentcell">
                                                             <asp:DropDownList ID="ddlInstitutionTypeList" runat="server" TabIndex="4">
                                                                  <asp:ListItem Value="1">Colleges</asp:ListItem>
                                                                    <asp:ListItem Value="2">High Schools</asp:ListItem>
                                                                </asp:DropDownList>
													    </TD>
													</TR>
													<TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblCampGrpId" Runat="server" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:dropdownlist id="ddlCampGrpId" runat="server" CssClass="dropdownlist" TabIndex="5"></asp:dropdownlist></TD>
													</TR>
                                                   <tr>
														<td class="twocolumnlabelcell">
															<asp:label id="lblSource" Runat="server" CssClass="label">Source</asp:label></td>
														<td class="twocolumncontentcell" colspan="4">
															<asp:RadioButtonList ID="radSource" runat="server" RepeatDirection="Horizontal">
                                                                <asp:ListItem Text="Imported" Value="2" Selected="true"></asp:ListItem>
                                                                <asp:ListItem Text="School Defined" Value="1"></asp:ListItem>
															</asp:RadioButtonList>
                                                        </td>
													</TR>
													<tr>
															<td class="spacertables"></td>
													</tr>
													<tr>
															<td class="contentcellheader" noWrap colSpan="6"><asp:label id="lblAddress" runat="server" cssclass="label" Font-Bold="true">Address</asp:label></td>
													</tr>
													<tr>
															<td class="spacertables"></td>
													</tr>
													<TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblAddress1" Runat="server" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:textbox id="txtAddress1" Runat="server" CssClass="textbox" MaxLength="100"  Width="80%" TabIndex="6"></asp:textbox>
                                                            <asp:CheckBox id="chkForeignZip" tabIndex="5" Runat="server" AutoPostBack="true" cssClass="checkboxinternational"></asp:CheckBox>
														</TD>
													</TR>
													<TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblAddress2" Runat="server" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:textbox id="txtAddress2" Runat="server" MaxLength="50" Width="80%" CssClass="textbox" TabIndex="7"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblCity" Runat="server" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:textbox id="txtCity" Runat="server" CssClass="textbox" MaxLength="50" TabIndex="8" AutoPostBack="true" Width="80%"></asp:textbox></TD>
													</TR>
													<TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblStateId" Runat="server" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:dropdownlist id="ddlStateId" runat="server" CssClass="dropdownlist" TabIndex=9></asp:dropdownlist>&nbsp;&nbsp;
														</TD>
													</TR>
													<tr>
														<td class="twocolumnlabelcell">
															<asp:label id="lblOtherState" Runat="server" CssClass="label" Visible=False></asp:label></td>
														<td class="twocolumncontentcell">
															<asp:textbox id="txtOtherState" Runat="server" CssClass="textbox" Maxlength="50" Visible="False" AutoPostBack="true"></asp:textbox></td>
													</tr>
													<tr>
														<td class="twocolumnlabelcell">
															<asp:label id="lblZip" Runat="server" CssClass="label"></asp:label></td>
														<td class="twocolumncontentcell">														
														 <telerik:RadMaskedTextBox ID="txtzip" tabindex="10" runat="server" cssclass="textbox"
                                                          DisplayPromptChar="" AutoPostBack="false" Width="20%"></telerik:RadMaskedTextBox>																
															</td>
													</tr>
                                                    <tr>
														<td class="twocolumnlabelcell">
															<asp:label id="lblCountyId" Runat="server" Text="County" CssClass="label"></asp:label></td>
														<td class="twocolumncontentcell">
															<asp:dropdownlist id="ddlCountyId" runat="server" CssClass="dropdownlist" TabIndex="11"></asp:dropdownlist>
														</td>
													</tr>
													<tr>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblCountryID" Runat="server" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:dropdownlist id="ddlCountryId" runat="server" CssClass="dropdownlist" TabIndex="12"></asp:dropdownlist>
														</TD>
													</tr>
													<tr>
															<td class="spacertables"></td>
													</tr>
													<tr>
															<td class="contentcellheader" noWrap colSpan="6">
															<asp:label id="lblPhoneHeading" runat="server" cssclass="label" Font-Bold="true">Phone</asp:label></td>
													</tr>
													<tr>
															<td class="spacertables"></td>
													</tr>
													<tr>
														<td class="twocolumnlabelcell">
															<asp:label id="lblPhone" Runat="server" CssClass="label"></asp:label></td>
														<td class="twocolumncontentcell" colspan="4">
															<telerik:RadMaskedTextBox ID="txtPhone" tabindex="13" runat="server" Maxlength="25" cssclass="textbox" width="200px" DisplayFormatPosition="Right" DisplayPromptChar="" ></telerik:RadMaskedTextBox>
															<asp:CheckBox id="chkForeignPhone" Runat="server" AutoPostBack="true" Text="International"
																cssClass="checkboxinternational" TabIndex=12></asp:CheckBox>
															<asp:label id="lblFax" Runat="server" CssClass="label"></asp:label>
															<asp:textbox id="txtFax" Visible="false" Runat="server" CssClass="textbox"></asp:textbox></td>
													</TR>
                                        <tr>
															<td class="spacertables"></td>
													</tr>
                                                    <tr>
															<td class="contentcellheader" noWrap colSpan="6"><asp:label id="Label2" runat="server" cssclass="label" Font-Bold="true">Contact</asp:label></td>
													</tr>
													<tr>
															<td class="spacertables"></td>
													</tr>
													<TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblContactTitle" Runat="server" Text="Title" CssClass="label" ></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:textbox id="txtContactTitle" Runat="server" CssClass="textbox" maxlength="50" Width="80%" TabIndex="14"></asp:textbox></TD>
													</TR>
                                                    <TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="Label5" Runat="server" Text="Prefix" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:DropDownList ID="ddlPrefix" runat="server" CssClass="dropdownlist" TabIndex="15"></asp:DropDownList></TD>
													</TR>
                                                    <TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblContactName" Runat="server" Text="Name" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:textbox id="txtFirstName" placeholder ="First Name" Runat="server" CssClass="textbox" MaxLength="50" ToolTip="First Name"  Width="30%" TabIndex="16"></asp:textbox>
                                                            <asp:textbox id="txtMiddleName" placeholder ="MI" Runat="server" CssClass="textbox" MaxLength="50" ToolTip="MI" Width="10%" TabIndex="17"></asp:textbox>
                                                            <asp:textbox id="txtLastName" placeholder ="Last Name" Runat="server" CssClass="textbox" MaxLength="50"  ToolTip="Last Name" Width="40%" TabIndex="18"></asp:textbox>
														</TD>
													</TR>
                                                     <TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblSuffix" Runat="server" Text="Suffix" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:DropDownList ID="ddlSuffix" runat="server" CssClass="dropdownlist" TabIndex="19"></asp:DropDownList></TD>
													</TR>
                                                    <tr>
														<td class="twocolumnlabelcell">
															<asp:label id="lblContactPhone" Runat="server" CssClass="label" Text="Phone"></asp:label></td>
														<td class="twocolumncontentcell" colspan="4">
															<telerik:RadMaskedTextBox ID="txtContactPhone" tabindex="20" runat="server" cssclass="textbox" width="200px" 
                                                                DisplayText = "Phone"
                                                                DisplayFormatPosition="Right" Mask="(###)-###-####" DisplayMask="(###)-###-####" DisplayPromptChar="" Text="Phone"></telerik:RadMaskedTextBox>
															<asp:CheckBox id="chkContactForeignPhone" Runat="server" AutoPostBack="true" Text="International"
																cssClass="checkboxinternational" TabIndex=12></asp:CheckBox>
															<asp:label id="lblContactFax" Runat="server" CssClass="label"></asp:label>
															<asp:textbox id="txtContactFax" Visible="false" Runat="server" CssClass="textbox"></asp:textbox></td>
													</TR>
                                                    <TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="Label3" Runat="server" Text="Email" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:textbox id="txtContactEmail" Runat="server" CssClass="textbox" MaxLength="50"  Width="80%" TabIndex="21"></asp:textbox>
                                                            <asp:RegularExpressionValidator ID="validateEmail"    
                                                                runat="server" ErrorMessage="Invalid email."
                                                                ControlToValidate="txtContactEmail"  ForeColor="red"
                                                                ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" />

														</TD>
													</TR>
													<TR>
														<TD class="twocolumnlabelcell">
															<asp:label id="lblContactStatus" Runat="server" Text="Status" CssClass="label"></asp:label></TD>
														<TD class="twocolumncontentcell">
															<asp:dropdownlist id="ddlContactStatus" runat="server" CssClass="dropdownlist" TabIndex="22"></asp:dropdownlist></TD>
													</TR>
												</TABLE>

												<asp:textbox id="txtRowIds" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
												<asp:textbox id="txtResourceId" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox><!--end table content-->
												<asp:TextBox id="txtModDate" style="VISIBILITY: hidden" Runat="server" Width="0" CssClass="donothing"></asp:TextBox>
												<asp:textbox id="txtHSId"  Runat="server" CssClass="textbox" Visible="false"></asp:textbox>
												<asp:CheckBox id="chkIsInDB" Runat="server" Visible="false"></asp:CheckBox></DIV>
												<asp:textbox id="txtAddressId" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
                                                <asp:textbox id="txtPhoneId" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
                                                <asp:textbox id="txtContactId" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
                                                <asp:textbox id="txtAddDefault" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
                                                <asp:textbox id="txtPhoneDefault" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
                                                <asp:textbox id="txtContactDefault" style="VISIBILITY: hidden" runat="server" Width="0" CssClass="donothing"></asp:textbox>
										</TD>
									</TR>
								</TABLE>
								</asp:panel>
									  </telerik:RadPane>
	</telerik:RadSplitter>
			<!-- start validation panel--><asp:panel id="Panel1" runat="server" CssClass="validationsummary">
            </asp:panel><asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator">
                </asp:customvalidator><asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel>
                <asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowMessageBox="True"
				ShowSummary="False"></asp:validationsummary>
			<!--end validation panel-->
					</asp:Content>
