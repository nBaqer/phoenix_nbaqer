﻿Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class ReassignLeads
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblReassignToAdmissionReps As System.Web.UI.WebControls.Label
    Protected WithEvents lstReassignToAdmissionReps As System.Web.UI.WebControls.ListBox
    Protected WithEvents lblAssignmentResult As System.Web.UI.WebControls.Label
    Protected WithEvents lstAssignmentResult As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstGrpName As System.Web.UI.WebControls.ListBox
    Protected WithEvents lstPossibleSkills As System.Web.UI.WebControls.ListBox
    Protected WithEvents Listbox2 As System.Web.UI.WebControls.ListBox
    Protected WithEvents Img3 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents lblReassignAdmissionReps As System.Web.UI.WebControls.Label
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected CampusId As String
    Protected WithEvents lblgeneral As System.Web.UI.WebControls.Label
    Protected m_context As HttpContext


    Private pObj As New UserPagePermissionInfo
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

#End Region

    Dim userId As String
    Dim resourceId As Integer
    Protected LeadId As String
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        CampusId = Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, CampusId)

        'Check if this page still exists in the menu while switching campus
        Dim boolPageStillPartofMenu As Boolean = False

       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + CampusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If pObj.HasFull Then
            btnAdd.Enabled = True
        Else
            btnAdd.Enabled = False
        End If

        If Not Page.IsPostBack Then
            BuildAdmissionRepsDdl()
            BuildLeadStatusDdl()
            BuildReassignAdmissionRepsDDL()
        End If


    End Sub
    Private Sub BuildAdmissionRepsDdl()
        Dim leadAdmissionReps As New LeadFacade
        With ddlCurrentAdmissionRepID
            .DataTextField = "fullname"
            .DataValueField = "userId"
            .DataSource = leadAdmissionReps.GetAllAdmissionRepsByCampus(CampusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .Items.Insert(1, New ListItem("None", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildLeadStatusDdl()
        Dim leadStatus As New LeadFacade
        With lstLeadStatuses
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            .DataSource = leadStatus.GetLeadStatusesByUserAndCampus(CampusId, userId)
            .DataBind()
        End With
    End Sub
    Private Sub BuildLeadsDdl()
        Dim leadStatus As New LeadFacade
        Dim statusitem As ListItem
        Dim intPosComma As Integer
        Dim strStatus As String

        'Clear The Old Status
        txtStatus.Text = ""
        For Each statusitem In lstLeadStatuses.Items
            If statusitem.Selected Then
                txtStatus.Text &= statusitem.Value & "','"
            End If
        Next
        intPosComma = InStrRev(txtStatus.Text, ",")
        strStatus = "'" & Mid(txtStatus.Text, 1, intPosComma - 1)
        Dim strDate As String = String.Empty
        If Not (txtStartLeadDate.SelectedDate Is Nothing) Then
            strDate = txtStartLeadDate.SelectedDate.ToString
        End If

        Dim strDate1 As String = String.Empty
        If Not (txtEndLeadDate.SelectedDate Is Nothing) Then
            strDate1 = txtEndLeadDate.SelectedDate.ToString
        End If
        With lstAssignedFlds
            .DataTextField = "FullName"
            .DataValueField = "leadid"
            If ddlCurrentAdmissionRepID.SelectedValue = Guid.Empty.ToString And ddlCurrentAdmissionRepID.SelectedItem.Text = "None" Then
                .DataSource = leadStatus.GenerateLeadNoAdmissionRep(ddlCurrentAdmissionRepID.SelectedValue, strDate, strDate1, lstLeadStatuses.SelectedValue, CampusId)
            ElseIf ddlCurrentAdmissionRepID.SelectedValue = Guid.Empty.ToString And ddlCurrentAdmissionRepID.SelectedItem.Text = "Select" Then
            Else
                .DataSource = leadStatus.GenerateLead(ddlCurrentAdmissionRepID.SelectedValue, strDate, strDate1, strStatus, CampusId)
            End If
            .DataBind()
        End With
    End Sub
    Private Sub BuildReassignAdmissionRepsDDL()
        Dim ReassignAdmReps As New LeadFacade
        With lstReassignAdmissionReps
            .DataTextField = "fullname"
            .DataValueField = "userId"
            .DataSource = ReassignAdmReps.GetAllReassignAdmissionReps(ddlCurrentAdmissionRepID.SelectedValue, CampusId)
            .DataBind()
        End With
    End Sub
    Private Sub UpdateAssignment(ByVal AdmissionRepCount As Integer, ByVal LeadCount As Integer, ByVal StatusCount As Integer)
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), AdmissionRepCount)
        Dim selectedLeads() As String = Array.CreateInstance(GetType(String), LeadCount)
        Dim selectedStatus() As String = Array.CreateInstance(GetType(String), StatusCount)

        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim k As Integer = 0

        Dim item As ListItem
        'In For Loop Check The Number of Items Selected For Reassign Admission Reps
        For Each item In lstReassignAdmissionReps.Items
            If item.Selected Then
                selectedDegrees.SetValue(item.Value.ToString, i)
                i += 1
            End If
        Next

        'In For Loop Check the Number Of Items Selected in Leads
        For Each item In lstAssignedFlds.Items
            If item.Selected Then
                selectedLeads.SetValue(item.Value.ToString, j)
                j += 1
            End If
        Next

        'In For Loop Check the Number Of Items Selected in Statuses
        For Each item In lstLeadStatuses.Items
            If item.Selected Then
                selectedStatus.SetValue(item.Value.ToString, k)
                k += 1
            End If
        Next

        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)
        If j > 0 Then ReDim Preserve selectedLeads(j - 1)
        If k > 0 Then ReDim Preserve selectedStatus(k - 1)

        'update Selected Jobs
        Dim LeadAssignment As New LeadFacade
        Dim result As String
        Dim strDate As String = String.Empty
        If Not (txtStartLeadDate.SelectedDate Is Nothing) Then
            strDate = txtStartLeadDate.SelectedDate.ToString
        End If

        Dim strDate1 As String = String.Empty
        If Not (txtEndLeadDate.SelectedDate Is Nothing) Then
            strDate1 = txtEndLeadDate.SelectedDate.ToString
        End If
        result = LeadAssignment.AssignmentLead(selectedLeads, ddlCurrentAdmissionRepID.SelectedValue, AdvantageSession.UserState.UserName, selectedDegrees, selectedStatus, strDate, strDate1)
        If result = 0 Then
            Dim intPosComma As Integer
            Dim strAdmissionRep, strLeads, strStatus As String

            'For Admission Reps
            intPosComma = InStrRev(txtAdmissionReps.Text, ",", -1)
            strAdmissionRep = Mid(txtAdmissionReps.Text, 1, intPosComma - 1)

            'For Leads
            intPosComma = InStrRev(txtLeadsInfo.Text, ",", -1)
            strLeads = Mid(txtLeadsInfo.Text, 1, intPosComma - 2)

            'For Status
            intPosComma = InStrRev(txtLeadsInfo.Text, ",", -1)
            strStatus = Mid(txtStatus.Text, 1, intPosComma - 1)
            'lstAssignmentResults.Items.Clear()
            'With lstAssignmentResults
            '    .DataTextField = "FullName"
            '    .DataValueField = "LeadId"
            '    .DataSource = LeadAssignment.PopulateReassignLeads(strLeads)
            '    .DataBind()
            'End With

            With dgrdTransactionSearch
                .DataSource = LeadAssignment.PopulateReassignLeads(strLeads)
                .DataBind()
            End With

            'Clear LeadsInfo
            txtLeadsInfo.Text = ""

        End If
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ' Iterate through the Items collection of the ListBox and 
        ' display the selected items.
        If ddlCurrentAdmissionRepID.SelectedValue = Guid.Empty.ToString Then
            'DisplayErrorMessage("A current Admission Rep is required")
            DisplayRADAlert(CallbackType.Postback, "Error1", "A current Admission Rep is required", "Save Error")
            Exit Sub
        End If
        If lstLeadStatuses.SelectedIndex = -1 Then
            'DisplayErrorMessage("Please select a Lead Status")
            DisplayRADAlert(CallbackType.Postback, "Error2", "Please select a Lead Status", "Save Error")
            Exit Sub
        End If

        'Build Leads Based on StartDate,EndDate and CurrentAdmission Reps
        BuildLeadsDdl()

        Dim LeadItem As ListItem
        For Each LeadItem In lstAssignedFlds.Items
            txtLeadsInfo.Text &= LeadItem.Value & "','"
        Next

        If txtLeadsInfo.Text = "" Then
            'DisplayErrorMessage("There are no Leads assigned to this Admissions Rep")
            DisplayRADAlert(CallbackType.Postback, "Error3", "There are no Leads assigned to this Admissions Rep", "Save Error")
            Exit Sub
        End If

        If lstReassignAdmissionReps.SelectedIndex <= -1 Then
            'DisplayErrorMessage("Please select an Admission Rep from the list")
            DisplayRADAlert(CallbackType.Postback, "Error4", "Please select an Admission Rep from the list", "Save Error")
            Exit Sub
        End If

        Dim item As ListItem
        For Each item In lstReassignAdmissionReps.Items
            If item.Selected Then
                txtAdmissionReps.Text &= item.Value & ","
            End If
        Next

        Dim statusitem As ListItem
        For Each statusitem In lstLeadStatuses.Items
            If statusitem.Selected Then
                txtStatus.Text &= statusitem.Value & ","
            End If
        Next
        'UpdateAssignment()

    End Sub
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
    End Sub
    Private Sub txtStartLeadDate_SelectedDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtStartLeadDate.SelectedDateChanged
    End Sub
    Private Sub ddlCurrentAdmissionRepID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCurrentAdmissionRepID.SelectedIndexChanged
        'If Not ddlCurrentAdmissionRepID.SelectedValue = Guid.Empty.ToString And 
        If Not ddlCurrentAdmissionRepID.SelectedItem.Text = "Select" Then
            BuildReassignAdmissionRepsDDL()
            lstLeadStatuses.SelectedIndex = -1
        End If
        If ddlCurrentAdmissionRepID.SelectedItem.Text = "None" Then
            ddlCurrentAdmissionRepID.SelectedItem.Text = "None"
        End If
    End Sub
    Private Sub BuildLeadByAdmissionRepsDDL()
        'Dim LeadNames As New LeadFacade
        'With lstLeads
        '    .DataTextField = "FullName"
        '    .DataValueField = "LeadId"
        '    .DataSource = LeadNames.GetLeadByAdmissionReps(ddlCurrentAdmissionRepID.SelectedValue)
        '    .DataBind()
        'End With
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)
    '    'Set error condition
    '    'Display error in message box in the client
    '    CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    'End Sub
    Private Sub btnSave_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnSave.Command
    End Sub
    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim strMessage As String = String.Empty
        If ddlCurrentAdmissionRepID.SelectedItem.Text = "Select" Then
            strMessage = "Please select an admission rep from the list" & vbLf
        End If
        If lstLeadStatuses.SelectedIndex = -1 Then
            strMessage &= "Please select a Lead Status" & vbLf
        End If

        Try
            txtStartLeadDate.SelectedDate = CDate(txtStartLeadDate.SelectedDate).ToShortDateString
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            txtStartLeadDate.Clear()
        End Try

        Try
            txtEndLeadDate.SelectedDate = CDate(txtEndLeadDate.SelectedDate).ToShortDateString
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            txtEndLeadDate.Clear()
        End Try

        If Not strMessage = "" Then
            'DisplayErrorMessage(strMessage)
            DisplayRADAlert(CallbackType.Postback, "Error5", strMessage, "Search Error")
            Exit Sub
        End If
        BuildLeadsDdl()
        dgrdTransactionSearch.Visible = False
    End Sub
    Private Sub BuildLeadAdmissionRep()
        Dim LeadItem As ListItem
        Dim intStatusCount As Integer
        Dim intLeadCount As Integer
        Dim intAdmissionRepCount As Integer

        For Each LeadItem In lstAssignedFlds.Items
            If LeadItem.Selected Then
                txtLeadsInfo.Text &= LeadItem.Value & "','"
                intLeadCount += 1
            End If
        Next

        Dim item As ListItem
        For Each item In lstReassignAdmissionReps.Items
            If item.Selected Then
                txtAdmissionReps.Text &= item.Value & ","
                intAdmissionRepCount += 1
            End If
        Next

        Dim statusitem As ListItem
        For Each statusitem In lstLeadStatuses.Items
            If statusitem.Selected Then
                txtStatus.Text &= statusitem.Value & ","
                intStatusCount += 1
            End If
        Next
        UpdateAssignment(intAdmissionRepCount, intLeadCount, intStatusCount)
    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If lstAssignedFlds.SelectedIndex = -1 Then
            'DisplayErrorMessage("Please select a Lead from the list to be assigned")
            DisplayRADAlert(CallbackType.Postback, "Error6", "Please select a Lead from the list to be assigned", "Add Error")
            Exit Sub
        End If
        If lstReassignAdmissionReps.SelectedIndex <= -1 Then
            'DisplayErrorMessage("Please Select an Admission Rep from the list")
            DisplayRADAlert(CallbackType.Postback, "Error7", "Please Select an Admission Rep from the list", "Add Error")
            Exit Sub
        End If
        dgrdTransactionSearch.Visible = True
        BuildLeadAdmissionRep()
        'Clear All Selections
        lstAssignedFlds.SelectedIndex = -1
        lstReassignAdmissionReps.SelectedIndex = -1
        BuildLeadsDdl()
    End Sub
    Private Sub btnSearch_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs) Handles btnSearch.Command
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnSearch)
        controlsToIgnore.Add(btnAdd)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub


End Class