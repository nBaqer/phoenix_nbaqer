﻿Imports FAME.AdvantageV1.BusinessFacade

Partial Class AD_ReversalReason
    Inherits System.Web.UI.Page
    Protected intValidationSuccessful As Integer = 0
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If String.IsNullOrEmpty(txtReverseReason.Text.Trim) Then
            intValidationSuccessful = 0
        Else
            intValidationSuccessful = 1
        End If
        Dim strTransactionId As String = Request.QueryString("TransactionId")
        Dim strLeadId As String = Request.QueryString("leadid")
        Dim strTransCodeId As String = Request.QueryString("transcodeid")
        Dim strTransReference As String = Request.QueryString("transreference")
        Dim strTransDescrip As String = Request.QueryString("transdescrip")
        Dim strTransAmount As String = Request.QueryString("transamount")
        Dim dtTransDate As String = Request.QueryString("transdate")
        Dim strCampusId As String = Request.QueryString("campusid")
        Dim strTransTypeId As String = Request.QueryString("transtypeid")
        Dim boolstrEnrolled As Boolean = Request.QueryString("isenrolled")
        Dim boolVoided As Boolean = Request.QueryString("voided")
        Dim strModUser As String = Request.QueryString("moduser")
        Dim dtModDate As DateTime = Request.QueryString("moddate")

        Dim facadeObj As New LeadMasterObject
        facadeObj.UpdateReversalReason(strTransactionId, txtReverseReason.Text, strLeadId, strTransCodeId, strTransReference, strTransDescrip, strTransAmount, dtTransDate, strCampusId, strTransTypeId, _
                                       boolstrEnrolled, boolVoided, strModUser, dtModDate)


    End Sub
End Class
