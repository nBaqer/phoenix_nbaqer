﻿Imports System.Diagnostics
Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class UnEnrollLeads
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents chkStatus As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblLDA As System.Web.UI.WebControls.Label
    Protected WithEvents txtLDA As System.Web.UI.WebControls.TextBox
    Protected WithEvents dlstStdEnroll As System.Web.UI.WebControls.DataList
    Protected WithEvents Img6 As System.Web.UI.HtmlControls.HtmlImage
    Protected WithEvents ddlLeadId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblLeadStatus As System.Web.UI.WebControls.Label
    Protected ShowStatusLevel As AdvantageCommonValues.IncludeStatus
    Protected boolStatus As String
    Private pObj As New UserPagePermissionInfo
    'To get the firstname and lastname of student to pass into the enrollmentid component.

    'Dim EnrollmentId As New EnrollmentComponent
    'Dim facade As New StuEnrollFacade
    'Dim LeadID As String '= "FAAB0903-819A-4A6C-95EE-BDA7C62E8EA1"
    Dim strFirstName, strMiddleName, strLastName As String
    Protected campusId As String
    Protected userId As String
    Dim resourceId As Integer
#End Region

#Region "Page Events"

    Protected Sub Page_Error(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnLead)
        controlsToIgnore.Add(btnUnEnroll)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Session("LeadId") = "FAAB0903-819A-4A6C-95EE-BDA7C62E8EA1"
        Dim objCommon As New FAME.Common.CommonUtilities

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        userId = AdvantageSession.UserState.UserId.ToString

        btnSave.Enabled = False
        btnDelete.Enabled = False
        btnNew.Enabled = False

        txtSSN.Enabled = False

        ShowStatusLevel = AdvantageCommonValues.IncludeStatus.ActiveOnly
        boolStatus = "True"
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'Check if this page still exists in the menu while switching campus
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            If Not Page.IsPostBack Then
                'To get the firstname and lastname of student to pass into the enrollmentid component.
                'txtLeadID.Text = Session("LeadId")
                'LeadID = txtLeadID.Text
                BuildDDLS()
                'objCommon.PageSetup(Form1, "NEW")
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

                'Disable the new and delete buttons
                'objCommon.SetBtnState(Form1, "NEW")
                ViewState("MODE") = "NEW"

                'Set the text box to a new guid
                txtStuEnrollId.Text = System.Guid.NewGuid.ToString

            Else
                'objCommon.PageSetup(Form1, "EDIT")
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            End If

            ddlStatusCodeId.BackColor = Color.White

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

#End Region

#Region "Button Click Events"

    Private Sub btnUnEnroll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnEnroll.Click
        Dim UnEnroll As New LeadEnrollmentFacade
        If ddlStatusCodeId.SelectedValue = Guid.Empty.ToString Then
            'DisplayErrorMessage("The Status is required")
            CommonWebUtilities.DisplayWarningInMessageBox(Me.Page, "The Status is required")
            Exit Sub
        End If
        Dim intResult As Integer = UnEnroll.UnEnrollLeadBySP(txtLeadId.Text, ddlStatusCodeId.SelectedValue.ToString())
        If intResult = 0 Then
            'DisplayErrorMessage(" The Lead was successfully unenrolled")
            CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, "The Student was successfully unenrolled")
        Else
            'DisplayErrorMessage(" The Lead was not unenrolled, as there are some dependent data that needs to be deleted")
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, " The Student was not unenrolled, as there are some dependent data that needs to be deleted")
            Exit Sub
        End If
        BuildLeadsByStatus()
        ResetLead()
    End Sub

    Private Sub btnLead_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLead.Click
        BuildLeadsByStatus()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    End Sub

#End Region

#Region "DataList Events"

    Private Sub dlstCategories_ItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstLeadNames.ItemCommand
        Try
            'Get LeadID
            txtLeadId.Text = CType(e.CommandArgument, String)

            Dim faca As New StuEnrollFacade
            Dim leadName As String = faca.GetLeadName(txtLeadId.Text)
            Dim fullName() As String = Split(leadName)
            Debug.WriteLine(txtLeadId.Text + "  " + leadName)
            If (fullName.Length > 0) Then
                strFirstName = fullName(0)
            End If
            If (fullName.Length > 1) Then
                strLastName = fullName(1)
            End If
            If (fullName.Length > 2) Then
                strMiddleName = fullName(2)
            End If
            txtLead.Text = strFirstName & " " & strMiddleName & " " & strLastName
            'txtEnrollmentId.Text = EnrollmentId.GenerateEnrollmentID(strLastName, strFirstName)
            chkIsInDB.Checked = False
            GetLeadInfo(CType(e.CommandArgument, String))
            BuildLeadStatusCode()

            'Enable the Unenroll button if the user has full permission to the page
            If pObj.HasFull Then
                btnUnEnroll.Enabled = True
            End If
        Finally
        End Try

        CommonWebUtilities.RestoreItemValues(dlstLeadNames, txtLeadId.Text)

        'set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstLeadNames, e.CommandArgument, ViewState, Header1)
    End Sub

    Private Sub dlstStdEnroll_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstStdEnroll.ItemCommand
    End Sub

#End Region

#Region "Dropdown Events"

    Private Sub ddlLeadStatus_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub

#End Region

#Region "Checkbox Events"

    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStatus.CheckedChanged
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView
        '        Dim sStatusId As String
        Dim facade As New StuEnrollFacade

        Try
            ClearRHS()
            'objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"
            'Set the text box to a new guid
            txtStuEnrollId.Text = System.Guid.NewGuid.ToString
            ds = facade.PopulateDataList(txtLeadId.Text, ShowStatusLevel)
            With dlstLeadNames
                .DataSource = ds
                .DataBind()
            End With
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

#End Region

#Region "Private Functions"

    Private Sub BuildDDLS()

        BuildLeadStatusCode()
        'BuildPrgVersionDDL()
        'BuildAcadAdvisorDDL()
        'BuildAdminRepDDL()
        'BuildShiftsDDL()
        'BuildCampusGroupsDDL()
        'BuildLeadStatusCode()
        'BuildChargingMethodDDL()
        'BuildGradeLevelDDL()
    End Sub
    Private Sub BuildLeadsByStatus()
        Dim LeadStatus As New LeadFacade
        'Dim UserId As String
        Dim CampusId As String

        'userid = HttpContext.Current.Session("UserId")
        CampusId = HttpContext.Current.Request.Params("cmpid")

        'Enrolled Lead Status
        Dim strLeadStatus As String = "A2F3BC8A-33EA-40D0-BD20-1BA3CD904644"
        Dim sMessage As String = String.Empty

        If Not txtStartDate.SelectedDate Is Nothing Then
            If Not IsDate(txtStartDate.SelectedDate) Then
                sMessage = "Start date is invalid" & vbLf
            End If
        End If

        If Not txtEndDate.SelectedDate Is Nothing Then
            If Not IsDate(txtEndDate.SelectedDate) Then
                sMessage &= "End date is invalid" & vbLf
            End If
        End If

        If Not sMessage = "" Then
            'DisplayErrorMessage(sMessage)
            CommonWebUtilities.DisplayWarningInMessageBox(Me.Page, sMessage)
            Exit Sub
        End If

        Dim strDate As String = String.Empty
        If Not (txtStartDate.SelectedDate Is Nothing) Then
            strDate = txtStartDate.SelectedDate.ToString
        End If

        Dim strDate1 As String = String.Empty
        If Not (txtEndDate.SelectedDate Is Nothing) Then
            strDate1 = txtEndDate.SelectedDate.ToString
        End If

        With dlstLeadNames
            .DataSource = LeadStatus.GetAllEnrolledLeads(ddlStatusCodeId.SelectedValue, strDate, strDate1, userId, CampusId, txtFirstName.Text, txtLastName.Text)
            .DataBind()
        End With
    End Sub

    Private Sub BuildLeadStatusCode()
        Dim LeadStatus As New LeadStatusChangeFacade
        With ddlStatusCodeId
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeID"
            '.DataSource = LeadStatus.GetReassignLeadStatusNoEnroll(HttpContext.Current.Request.Params("cmpid").ToString,HttpContext.Current.Session("UserId").ToString )
            .DataSource = LeadStatus.GetLeadStatusMappedToUnEnrolledStatus(userId, HttpContext.Current.Request.Params("cmpid").ToString)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub GetLeadInfo(ByVal LeadId As String)
        Dim LeadInfo As New LeadFacade
        BindLeadInfoExist(LeadInfo.GetLeadsUnEnrollmentInfo(LeadId))
    End Sub

    Private Sub BindLeadInfoExist(ByVal LeadInfo As LeadMasterInfo)
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String
        Dim zipMask As String
        Dim ssnMask As String

        'Get the mask for phone numbers and zip
        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        ssnMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.SSN)
        With LeadInfo
            chkIsInDB.Checked = False
            txtSSN.Text = .SSN
            If txtSSN.Text <> "" Then
                Dim temp As String = txtSSN.Text
                txtSSN.Text = facInputMasks.ApplyMask(ssnMask, "*****" & temp.Substring(5))
                'txtSSN.Text = facInputMasks.ApplyMask(ssnMask, txtSSN.Text)
            End If
            txtDOB.Text = .BirthDate
            txtEnrollmentId.Text = .EnrollmentId
        End With


    End Sub

    Private Sub ResetFields()
        chkIsInDB.Checked = False
        'txtExpStartDate.Text = ""
        'txtEnrollDate.Text = ""
        'txtExpStartDate.Text = ""
        'txtStartDate.Text = ""
        'txtMidPtDate.Text = ""
        'txtExpGradDate.Text = ""
        'txtTransferDate.Text = ""
        txtLead.Text = ""
        txtEnrollmentId.Text = ""
        'txtLDA.Text = ""
        BuildDDLS()
    End Sub

    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BindDatalist(ByVal ds As DataSet)
        Dim sw As New System.IO.StringWriter
        Dim ds2 As New DataSet
        Dim dv2 As New DataView
        Dim objListGen As New DataListGenerator
        Dim sStatusId As String

        ds2 = objListGen.StatusIdGenerator()
        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (chkStatus.Checked = True) Then 'Show Active Only

            Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays active records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "PrgVerDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        ElseIf (chkStatus.Checked = False) Then

            Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays inactive records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "PrgVerDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        End If
        With dlstLeadNames
            .DataSource = dv2
            .DataBind()
        End With
        'dtlAssignList.SelectedIndex = -1
        ViewState("StoredDs") = ds
        'I cannot write the dataset to XML as I lose the date and time formatting - Corey Masson
        'ds.WriteXml(sw)
        'ViewState("ds") = sw.ToString()
    End Sub

    Private Sub ResetLead()
        txtEnrollmentId.Text = ""
        txtDOB.Text = ""
        txtSSN.Text = ""
        BuildLeadStatusCode()
        txtLead.Text = ""
    End Sub

#End Region

End Class
