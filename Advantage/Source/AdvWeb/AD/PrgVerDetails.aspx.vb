Imports Fame.Common
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class PrgVerDetails
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblPrgVer As System.Web.UI.WebControls.Label
    Protected WithEvents txtProgVerDescrip As System.Web.UI.WebControls.TextBox
    Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    Protected WithEvents btnRequired As System.Web.UI.WebControls.Button
    Protected WithEvents btnElective As System.Web.UI.WebControls.Button
    Protected WithEvents btnDetails As System.Web.UI.WebControls.Button
    Protected WithEvents btnUp As System.Web.UI.WebControls.Button
    Protected WithEvents btnDown As System.Web.UI.WebControls.Button
    Protected WithEvents allDataset As System.Data.DataSet
    Protected WithEvents ReqsTable As System.Data.DataTable
    Protected WithEvents ReqGrpsTable As System.Data.DataTable
    Protected WithEvents SelectedTable As System.Data.DataTable
    Protected WithEvents PrgVerTable As System.Data.DataTable

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo
    Dim campusId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim ds As New DataSet
        Dim objCommon As New CommonUtilities

        Dim userId As String
        '  Dim m_Context As HttpContext
        Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim facCGrp As New CourseGrpFacade
        ' Dim ReqGrpId As String
        Dim facade As New ReqGrpsDefFacade
        '  Dim AllDS As DataSet
        '  Dim LeadGrpDT As DataTable

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'Check if this page still exists in the menu while switching campus
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        campusId = Master.CurrentCampusId ' 

        Try
            If Not Page.IsPostBack Then
                objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

                'Header1.EnableHistoryButton(False)

                '   build dropdownlists
                BuildDropDownLists()

                'txtModUser.Text = Session("UserName")
                txtModUser.Text = AdvantageSession.UserState.UserName
                txtModDate.Text = Date.MinValue.ToString

                'Disable the new and delete buttons
                'objCommon.SetBtnState(Form1, "NEW", pObj)


            End If
            'initialize buttons
            InitButtonsForLoad()
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
        headerTitle.Text = Header.Title
    End Sub
    Private Sub BuildDropDownLists()
        BuildPrgVersionsDDL()
    End Sub
    Private Sub CreateDatasetAndTables()
        'Get dataGrid Dataset from the session and create tables
        allDataset = Session("AllDataset")
        ReqsTable = allDataset.Tables("AvailReqs")
        Session("Reqs") = ReqsTable
        ReqGrpsTable = allDataset.Tables("AvailReqGrps")
        'PrgVerTable = allDataset.Tables("PrgVersions")
        SelectedTable = allDataset.Tables("Selected")
        Session("SelectedTable") = SelectedTable
    End Sub
    Private Sub BuildPrgVersionsDDL()
        '   create row filter and sort expression
        Dim facade As New PrgVerDetailsFacade

        With ddlPrgVerId
            '.DataTextField = "PrgVerDescrip"PrgVerShiftDescrip
            .DataTextField = "PrgVerShiftDescrip"
            .DataValueField = "PrgVerId"
            .DataSource = facade.GetPrgVersions(campusId)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        End With
    End Sub

    Private Sub BindListBoxes(ByRef ds As DataSet)
        Dim dt As DataTable
        dt = Session("SelectedTable")

        If ReqsTable.Rows.Count > 0 Then
            lbxAvailReqs.DataSource = ReqsTable
            lbxAvailReqs.DataTextField = "Descrip"
            lbxAvailReqs.DataValueField = "adReqId"
            lbxAvailReqs.DataBind()
        End If
        If ReqGrpsTable.Rows.Count > 0 Then
            lbxAvailReqGrps.DataSource = ReqGrpsTable
            lbxAvailReqGrps.DataTextField = "Descrip"
            lbxAvailReqGrps.DataValueField = "ReqGrpId"
            lbxAvailReqGrps.DataBind()
        End If
        If SelectedTable.Rows.Count > 0 Then
            lbxSelected.DataSource = dt
            lbxSelected.DataTextField = "Descrip"
            lbxSelected.DataValueField = "ID"
            lbxSelected.DataBind()
        Else
            lbxSelected.Items.Clear()

        End If
        Session("SelectedTable") = dt
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        btnDelete.Enabled = False
        btnNew.Enabled = False
    End Sub
    Private Sub ddlPrgVerId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrgVerId.SelectedIndexChanged
        Dim prgver As String

        prgver = ddlPrgVerId.SelectedItem.Value.ToString()
        ViewState("PrgVer") = prgver

        'get allDataset from the DB
        With New PrgVerDetailsFacade
            allDataset = .GetAllPrgVerDS(prgver, campusId)
        End With

        'save it on the session
        Session("AllDataset") = allDataset

        '   create Dataset and Tables
        CreateDatasetAndTables()

        'Bind the Courses List Box
        BindListBoxes(allDataset)
    End Sub
    Private Sub btnAdd1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd1.Click
        If lbxAvailReqs.SelectedIndex >= 0 Then
            Dim SelIndex As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ds As New DataSet
            Dim objListGen As New DataListGenerator
            '            Dim foundInCourseGroup As String

            'Add selected item to the relevant assigned list. We want to ensure that the course does not 
            'already belong to the selected items. This could only happen if it was part of a course group
            'that is already in the selected items. For each item in the selected list we will check if it
            'is a course group and then check to see if the selected course is in it. If it is then we will
            'cancel the add operation and display an error message to the user.
            '#Commented out 08/16 after disc w/ troy & balaji. bug fix:#4673
            'foundInCourseGroup = ReqExistsInSelectedList(lbxAvailReqs.SelectedItem.Value)

            'If foundInCourseGroup = "" Then
            Try
                lbxSelected.Items.Add(New ListItem(lbxAvailReqs.SelectedItem.Text, lbxAvailReqs.SelectedItem.Value))
                'ds = Session("AllDataset")
                dt = Session("SelectedTable")
                dr = dt.NewRow()
                dr("ID") = lbxAvailReqs.SelectedItem.Value.ToString
                dr("adReqId") = lbxAvailReqs.SelectedItem.Value.ToString
                dr("Type") = 1

                If (txtGrdOverride.Text <> "") Then
                    dr("MinScore") = txtGrdOverride.Text
                    dr("adReqTypeId") = 1
                Else
                    dr("MinScore") = 0
                    dr("adReqTypeId") = 3
                End If


                'dr("Sequence") = lbxSelected.Items.Count

                dt.Rows.Add(dr)
                lblGrdOverride.Visible = False
                txtGrdOverride.Visible = False
                'Session("AllDataset") = ds
                'Remove the item from the lbxAvailReqs list
                SelIndex = lbxAvailReqs.SelectedIndex
                lbxAvailReqs.Items.RemoveAt(SelIndex)
                'ds = objListGen.SummaryListGenerator()
                'dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
                'PopulateDataList(ds)
                Session("SelectedTable") = dt
            Catch

            End Try
            'Else
            '    DisplayErrorMessage(lbxAvailReqs.SelectedItem.Text & " aready exists in " & foundInCourseGroup)
            'End If
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click

        Dim SelIndex As Integer
        Dim iCourseId As String
        Dim dt As DataTable
        Dim dt2 As DataTable
        Dim dr As DataRow
        Dim row2 As DataRow
        Dim dr2 As DataRow
        Dim ds As New DataSet
        Dim objListGen As New DataListGenerator
        '        Dim aRows As DataRow()
        Dim row1 As DataRow
        Dim iType As Integer
        Dim iminScore As Decimal
        Dim ReqType As Integer

        If lbxSelected.SelectedIndex >= 0 Then
            dt = Session("SelectedTable")
            dr = dt.Rows.Find(lbxSelected.SelectedItem.Value.ToString)
            If Not dr Is Nothing Then
                iCourseId = dr("ID").ToString

                iType = dr("Type")
                If (iType = 1) Then 'i.e Requirement
                    lbxAvailReqs.Items.Add(New ListItem(lbxSelected.SelectedItem.Text, lbxSelected.SelectedItem.Value))
                    ReqType = dr("adReqTypeId")
                    iminScore = dr("MinScore")
                ElseIf (iType = 2) Then ' i.e Req Group
                    lbxAvailReqGrps.Items.Add(New ListItem(lbxSelected.SelectedItem.Text, lbxSelected.SelectedItem.Value))
                End If

                'Add selected item to the lbxAvailReqs list. In order to do this we
                'have to use the FldsSelectCamps to find out the 'FldId for the selected item.

                'aRows = dt.Select("Sequence > " & lbxSelected.SelectedIndex)
                'For Each row1 In aRows
                '    row1("Sequence") = row1("Sequence") - 1
                'Nex

                If dr.RowState <> DataRowState.Added Then
                    'Mark the row as deleted
                    dr.Delete()
                Else
                    dt.Rows.Remove(dr)
                End If
            Else
                For Each row1 In dt.Rows
                    If (row1.RowState = DataRowState.Deleted) And (row1("ID", DataRowVersion.Original) = lbxSelected.SelectedItem.Value.ToString) Then
                        iType = row1("Type", DataRowVersion.Original)

                        iminScore = row1("MinScore", DataRowVersion.Original)
                        If (iType = 1) Then 'i.e Requirement
                            lbxAvailReqs.Items.Add(New ListItem(lbxSelected.SelectedItem.Text, lbxSelected.SelectedItem.Value))
                            ReqType = row1("adReqTypeId", DataRowVersion.Original)
                        ElseIf (iType = 2) Then ' i.e Req Group
                            lbxAvailReqGrps.Items.Add(New ListItem(lbxSelected.SelectedItem.Text, lbxSelected.SelectedItem.Value))
                        End If
                    End If

                Next
            End If

            'add back this item to the available reqs datatable IF it doesnt exist already
            dt2 = Session("Reqs")
            row2 = dt2.Rows.Find(lbxSelected.SelectedItem.Value.ToString)
            If row2 Is Nothing Then
                'add this req if it doesnt exist already
                dr2 = dt2.NewRow()
                dr2("Descrip") = lbxSelected.SelectedItem.Text
                dr2("adReqId") = lbxSelected.SelectedItem.Value.ToString
                If (iType = 1) Then
                    dr2("adReqTypeId") = ReqType
                End If

                If ReqType = 1 Then
                    dr2("MinScore") = iminScore
                End If

                dt2.Rows.Add(dr2)
                't1.adReqId,t1.Descrip,t1.Code,t1.adReqTypeId,t1.MinScore
            End If

            'Remove the item from the lbxSelected list
            SelIndex = lbxSelected.SelectedIndex
            lbxSelected.Items.RemoveAt(SelIndex)
            'Session("SelectedReqs") = dt
            Session("SelectedTable") = dt



        End If

    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If lbxAvailReqGrps.SelectedIndex >= 0 Then
            Dim SelIndex As Integer
            Dim dt As DataTable
            Dim dr As DataRow
            Dim ds As New DataSet
            Dim objListGen As New DataListGenerator
            ' Dim firstCommonReq As String
            Dim fac As New PrgVerDetailsFacade
            'Dim foundCourse As String
            'Dim errorMessage As String
            'Add selected item to the relevant assigned list. We want to ensure that the course group does not 
            'have any reqs that belong to the selected items. We will build a collection of ReqInfo objects
            'based on the items in the selected list. We will then check if the course group has any common
            'course with the collection. If it does we will display an error message letting the user know
            'that there are common courses. #Commented out 08/16 after disc w/ troy & balaji. bug fix:#4673
            'foundCourse = fac.GetFirstCommonRequirement(lbxAvailReqGrps.SelectedItem.Value.ToString, BuildReqInfoCollection)

            'If foundCourse = "" Then

            'Add selected item to the relevant assigned list. 
            Try
                lbxSelected.Items.Add(New ListItem(lbxAvailReqGrps.SelectedItem.Text, lbxAvailReqGrps.SelectedItem.Value))
                dt = Session("SelectedTable")
                dr = dt.NewRow()
                dr("ID") = lbxAvailReqGrps.SelectedItem.Value.ToString
                dr("ReqGrpId") = lbxAvailReqGrps.SelectedItem.Value.ToString
                dr("Type") = 2
                'If (chkIsReq.Checked) Then
                '    dr("IsRequired") = True
                'Else
                '    dr("IsRequired") = False
                'End If
                'dr("Sequence") = lbxSelected.Items.Count

                dt.Rows.Add(dr)
                'Session("SelectedCourses") = dt
                Session("SelectedTable") = dt
                'Remove the item from the lbxAvailReqGrps list
                SelIndex = lbxAvailReqGrps.SelectedIndex
                lbxAvailReqGrps.Items.RemoveAt(SelIndex)
                'ds = objListGen.SummaryListGenerator()
                'dlstCampGrps.SelectedIndex = CInt(ViewState("SELINDEX"))
                'PopulateDataList(ds)
            Catch

            End Try

            'Else
            'Display error message to the user with course that is already in the hierarchy
            'errorMessage = "Cannot add " & lbxAvailReqGrps.SelectedItem.Text & "." & vbCr
            'errorMessage &= "It contains " & foundCourse & " which is already part of the selected items."
            'DisplayErrorMessage(errorMessage)
            'End If
        End If

    End Sub
    Private Function ReqExistsInSelectedList(ByVal reqId As String) As String
        'Check each item in the selected list if it is a course group. If it is see if the course passed
        'in exists in it.
        Dim strFound As String = ""
        Dim i As Integer
        Dim dt As DataTable
        Dim fac As New PrgVerDetailsFacade
        '   Dim dtCsrGrpReqs As DataTable
        Dim objCommon As New CommonUtilities

        If lbxSelected.Items.Count = 0 Then
            Return ""
        Else
            dt = Session("SelectedTable")
            For i = 0 To lbxSelected.Items.Count - 1
                'Check if the current item is a course group
                Dim arows As DataRow() = dt.Select("ID = '" & lbxSelected.Items(i).Value.ToString & "' ")
                If arows(0)("Type") = 2 Then
                    'Check if the course exists in the course group
                    If fac.DoesReqExistsInCourseGroup(lbxAvailReqs.SelectedItem.Value.ToString, lbxSelected.Items(i).Value.ToString) Then
                        Return lbxSelected.Items(i).Text
                    End If
                End If

            Next

            Return ""
        End If

    End Function

    Private Function BuildReqInfoCollection() As ArrayList
        Dim arrReqInfo As New ArrayList
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim rInfo As New ReqInfo

        'Get the dataset from session
        'ds = Session("WorkingDS")
        'retrieve the datatable from the ds
        dt = Session("SelectedTable")

        If lbxSelected.Items.Count > 0 Then
            For i = 0 To lbxSelected.Items.Count - 1
                'Find each item in the dt datatable and get the relevant properties
                Dim arows As DataRow() = dt.Select("ID = '" & lbxSelected.Items(i).Value.ToString & "' ")
                rInfo.ReqTypeId = arows(0)("Type").ToString()
                rInfo.ReqId = arows(0)("ID").ToString()
                rInfo.ReqDescription = lbxSelected.Items(i).Text
                'Add the object to the arraylist
                arrReqInfo.Add(rInfo)
            Next
        End If

        Return arrReqInfo
    End Function
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'Dim i As Integer
        'Dim strGuid As String
        'Dim Description As String
        'Dim Code As String , dt3 
        Dim sb As New System.Text.StringBuilder
        Dim dt, dt2 As DataTable
        Dim dr As DataRow
        Dim objCommon As New CommonUtilities
        '        Dim iCampusId As String
        Dim ds1 As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New System.IO.StringWriter
        Dim dv2 As New DataView
        '        Dim strValue As String
        '  Dim intPos As Integer
        Dim facade As New PrgVerDetailsFacade
        Dim ChildObj As New ChildInfo
        Dim ds As New DataSet
        Dim PrgVerId As String
        'Dim coursegrphrs As Decimal
        'Dim coursegrpcrds As Decimal
        'Dim BR As ProgBL
        'Dim childhrs As Decimal
        'Dim childcrds As Decimal
        Dim result As String
        ' Dim errorMsg As ErrMsgInfo
        Dim info As New ReqInfo

        PrgVerId = ViewState("PrgVer")

        Try

            'Pull the dataset from Session.
            dt = Session("SelectedTable")

            ' CODE BELOW HANDLES AN INSERT/UPDATE INTO THE TABLE(S)

            'Run getchanges method on the dataset to see which rows have changed.
            'This section handles the changes to the Advantage Required fields

            dt2 = dt.GetChanges(DataRowState.Added Or DataRowState.Deleted Or DataRowState.Modified)

            'If (facade.ValidateLeadGrpRequirements(coursegrphrs, coursegrpcrds, childhrs, childcrds, ds) = ProgBL.ValResult.Success) Then
            If Not IsNothing(dt2) Then
                For Each dr In dt2.Rows
                    If dr.RowState = DataRowState.Added Then
                        If (dr("Type") = 1) Then
                            info.ReqId = dr("adReqId").ToString
                            info.MinScore = dr("MinScore")
                            info.ReqGrpId = ""
                        ElseIf (dr("Type") = 2) Then
                            info.ReqId = ""
                            info.ReqGrpId = dr("ReqGrpId").ToString
                            info.MinScore = 0
                        End If

                        'ChildObj.ChildSeq = dr("Sequence")


                        'facade.AssignReqsToPrgVer(info, PrgVerId, Session("UserName"))
                        facade.AssignReqsToPrgVer(info, PrgVerId, AdvantageSession.UserState.UserName)


                    ElseIf dr.RowState = DataRowState.Deleted Then
                        If (dr("Type", DataRowVersion.Original) = 1) Then
                            info.ReqId = dr("ID", DataRowVersion.Original).ToString

                        ElseIf (dr("Type", DataRowVersion.Original) = 2) Then
                            info.ReqGrpId = dr("ID", DataRowVersion.Original).ToString
                        End If

                        result = facade.DeleteReqsFrmPrgVer(info, PrgVerId)
                        If Not result = "" Then
                            '   Display Error Message
                            DisplayErrorMessage(result)
                        End If
                    ElseIf dr.RowState = DataRowState.Modified Then
                        If (dr("Type") = 1) Then
                            info.ReqId = dr("adReqId").ToString

                        Else
                            info.ReqGrpId = dr("ReqGrpId").ToString
                        End If

                        'ChildObj.ChildSeq = dr("Sequence")
                        'ChildObj.ChildSeq = dr("Sequence")
                        ChildObj.ModDate = dr("ModDate")
                        'facade.UpdatePrgVerDef(info, PrgVerId, Session("UserName"))
                    End If
                Next
            End If
            dt.AcceptChanges()
            Session("SelectedTable") = dt

            ' End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub btnDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDetails.Click

    End Sub
    Private Sub lbxAvailReqs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbxAvailReqs.SelectedIndexChanged
        Dim dt As DataTable
        '        Dim dr As DataRow
        Dim ReqType As Integer
        '     Dim DefScore As Decimal

        txtGrdOverride.Text = ""

        'Get ReqType and Default MinScore if Entrance Test from Session
        dt = Session("Reqs")
        Dim arows As DataRow() = dt.Select("adReqId = '" & lbxAvailReqs.SelectedItem.Value.ToString & "' ")
        If Not arows Is Nothing Then
            ReqType = arows(0)("adReqTypeId")
            If ReqType = 1 Then 'If the req is an entrance test,then display min.score txt box w/default
                lblGrdOverride.Visible = True
                txtGrdOverride.Visible = True
                If Not (arows(0)("MinScore") Is System.DBNull.Value) Then
                    txtGrdOverride.Text = arows(0)("MinScore")
                End If
            ElseIf ReqType = 3 Then
                lblGrdOverride.Visible = False
                txtGrdOverride.Visible = False
                If Not (arows(0)("MinScore") Is System.DBNull.Value) Then
                    txtGrdOverride.Text = arows(0)("MinScore")
                End If
            End If
        End If
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnAdd1)
        controlsToIgnore.Add(btnRemove)
        controlsToIgnore.Add(ddlPrgVerId)
        'Add javascript code to warn the user about non saved changes 
        '        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
