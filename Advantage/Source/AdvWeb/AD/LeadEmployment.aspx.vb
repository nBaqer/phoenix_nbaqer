﻿Imports FAME.Common
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Drawing
Imports System.Collections
Imports System.Collections.Generic
Imports System.Diagnostics
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports FAME.Advantage.Common

Partial Class LeadEmployment
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblSelectType As Label
    Protected WithEvents ddlSchoolType As DropDownList
    Protected WithEvents txtStudentId As TextBox
    Protected WithEvents lblStatus As Label
    Protected WithEvents lblExtracurricular As Label
    Protected WithEvents chkExtracurricularId As CheckBoxList
    Protected WithEvents img3 As HtmlImage
    Protected WithEvents txtJobStatusId As TextBox
    Protected resourceID As Integer
    Protected stEmploymentID As String
    Protected WithEvents btnhistory As Button
    Protected WithEvents chkStatus As CheckBox
    Protected strFrom As String
    Protected WithEvents ddlState As TextBox
    Protected moduleId As String


#End Region

    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Protected leadId As String = String.Empty
    Dim userId As String
    Protected studentId As String
    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    Protected myAdvAppSettings As AdvAppSettings
    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(sender As Object, e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()


    End Sub
    Private Function GetLeadFromStateObject(ByVal paramResourceId As Integer) As Advantage.Business.Objects.StudentMRU

        Dim objStudentState As New Advantage.Business.Objects.StudentMRU

        Try

            MyBase.GlobalSearchHandler(1)

            boolSwitchCampus = Master.Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                studentId = Guid.Empty.ToString()
            Else
                studentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                leadId = Guid.Empty.ToString()
            Else
                leadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(studentId)
                .LeadId = New Guid(leadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.Master.ShowHideStatusBarControl(True)

            Master.Master.PageObjectId = leadId
            Master.Master.PageResourceId = Request.QueryString("resid")
            Master.Master.setHiddenControlForAudit()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function



#End Region
    Private Sub Page_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        Dim objCommon As New CommonUtilities
        Dim sdfControls As New SDFComponent
        'Dim LeadId As String


        '       Dim m_Context As HttpContext
        'Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        '   Dim strVID As String
        '   Dim state As AdvantageSessionState

        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.Master.CurrentCampusId
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString
        moduleId = HttpContext.Current.Request.Params("Mod").ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As StudentMRU = GetLeadFromStateObject(146) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            studentId = .StudentId.ToString
            leadId = .LeadId.ToString


        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            If Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value) Then
                ClearControls()
            End If

            Dim content As ContentPlaceHolder = CType(DirectCast(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder)
            objCommon.SetCaptionsAndColorRequiredFields(content)

            'objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW", pObj)
            ViewState("MODE") = "NEW"
            'Bind The DropDownList
            BuildDDLs()



            'If pObj.HasFull Or pObj.HasEdit Then
            '    btnSave.Enabled = True
            'End If

            'If pObj.HasFull Or pObj.HasDelete Then
            '    btnDelete.Enabled = True
            'End If
            txtLeadId.Text = leadId

            'Assign Primary Key Value
            txtStEmploymentId.Text = Guid.NewGuid.ToString

            'Get Default Employer Data when page is loaded for first time
            BindDataList(txtLeadId.Text)

            'Call the SDF Component
            ' SDFControls.GenerateControlsNew(pnlSDF, resourceId, moduleId)

            '   disable History button the first time
            'Header1.EnableHistoryButton(False)

            txtModDate.Text = Date.Now.ToString()
            InitButtonsForLoad()

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""

        Else
            Dim content As ContentPlaceHolder = CType(DirectCast(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder)
            objCommon.SetCaptionsAndColorRequiredFields(content)

            'objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
        End If

        txtEmployerName.BackColor = Color.White
        txtEmployerJobTitle.BackColor = Color.White

        'Check If any UDF exists for this resource
        Dim intSdfExists As Integer = sdfControls.GetSDFExists(resourceId, moduleId)
        If intSdfExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If


        'If the Unique Value(Primary Key Value) is not empty then
        'Generate Controls Based on Unique Value.
        'If Unique Value is Empty then Load Empty Controls
        If Trim(txtStEmploymentId.Text) <> "" Then
            sdfControls.GenerateControlsEdit(pnlSDF, resourceId, txtStEmploymentId.Text, moduleId)
        Else
            sdfControls.GenerateControlsNew(pnlSDF, resourceId, moduleId)
        End If

        'Code to disable all buttons if lead was already enrolled
        'to prevent data being changed after lead was enrolled
        Dim chkLeadWasEnrolled As Boolean
        Dim leadFacade As New LeadEnrollmentFacade
        chkLeadWasEnrolled = leadFacade.CheckIfLeadWasEnrolled(leadId)
        If chkLeadWasEnrolled = True Then
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
            btnNew.ToolTip = "Cannot add employment information as the lead was already enrolled"
            btnSave.ToolTip = "Cannot modify employment information as the lead was already enrolled"
            btnDelete.ToolTip = "Cannot delete employment information as the lead was already enrolled"
        Else
            btnNew.ToolTip = ""
            btnSave.ToolTip = ""
            btnDelete.ToolTip = ""
        End If
    End Sub

    Private Sub ClearControls()
        txtEmployerName.Text = ""
        txtEmployerJobTitle.Text = ""
        ddlJobTitleId.Items.Clear()
        ddlJobStatusId.Items.Clear()
        txtComments.Text = ""
        txtJobResponsibilities.Text = ""
        txtStartDate.SelectedDate = Nothing
        txtEndDate.SelectedDate = Nothing



    End Sub

    Private Sub BuildDDLs()
        'BuildPrefixDDL()
        Dim ddlList = New List(Of AdvantageDDLDefinition)()
        'Job Titles
        ddlList.Add(New AdvantageDDLDefinition(ddlJobTitleId, AdvantageDropDownListName.JobTitles, campusId, True, True))
        'Job Status
        ddlList.Add(New AdvantageDDLDefinition(ddlJobStatusId, AdvantageDropDownListName.JobStatus, campusId, True, True))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub

    Private Sub BtnSaveClick(sender As Object, e As EventArgs) Handles btnSave.Click

        Dim studentEducation As New PlacementFacade
        Dim result As String

        Dim leadMasterUpdate As New LeadFacade
        If myAdvAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "no" Then
            If leadMasterUpdate.CheckLeadUser(userId, txtLeadId.Text) = 0 Then
                DisplayRADAlert(CallbackType.Postback, "Error1", "You do not have rights to edit this lead ", "Save Error")
                Exit Sub
            End If
        End If
        ''''''''''''''''''
        ' Call Update Function in the plEmployerInfoFacade
        result = studentEducation.UpdateLeadEmployment(BuildStudentEmployment(), AdvantageSession.UserState.UserName, txtStEmploymentId.Text)

        'Display Error Message
        If Not result = "" Then
            DisplayRADAlert(CallbackType.Postback, "Error2", result, "Save Error")
        End If
        ''  Reset The Checked Property Of CheckBox To True
        ChkIsInDB.Checked = True

        Dim sdfid As ArrayList
        Dim sdfidValue As ArrayList
        Dim z As Integer
        Dim sdfControl As New SDFComponent

        Try
            sdfControl.DeleteSDFValue(txtStEmploymentId.Text)
            sdfid = sdfControl.GetAllLabels(pnlSDF)
            sdfidValue = sdfControl.GetAllValues(pnlSDF)
            For z = 0 To sdfid.Count - 1
                sdfControl.InsertValues(txtStEmploymentId.Text, Mid(CType(sdfid(z).id, String), 5), CType(sdfidValue(z), String))
            Next
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        'Bind The DataList Based On LeadId 
        'and Education Institution Type.
        BindDataList(txtLeadId.Text)

        ''If Page is free of errors Show Edit Buttons
        If Page.IsValid Then InitButtonsForEdit()

        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, txtStEmploymentId.Text)

    End Sub

    Private Function BuildStudentEmployment() As PlacementInfo
        Dim studentEducation As New PlacementInfo
        With studentEducation
            .IsInDb = ChkIsInDB.Checked
            .StEmploymentId = txtStEmploymentId.Text
            .LeadID = txtLeadId.Text
            .Comments = txtComments.Text
            .EmployerName = txtEmployerName.Text
            If txtStartDate.SelectedDate Is Nothing Then
                .StartDate = ""
            Else
                .StartDate = CType(Date.Parse(CType(txtStartDate.SelectedDate, String)), String)
            End If
            If txtEndDate.SelectedDate Is Nothing Then
                .EndDate = ""
            Else
                .EndDate = CType(Date.Parse(CType(txtEndDate.SelectedDate, String)), String)
            End If

            .JobStatus = ddlJobStatusId.SelectedValue
            .JobTitleId = ddlJobTitleId.SelectedValue
            .JobResponsibilities = txtJobResponsibilities.Text
            .EmployerJobTitle = txtEmployerJobTitle.Text
            txtModDate.Text = CType(Date.Now, String)
            .ModDate = CType(txtModDate.Text, Date)

        End With
        Return studentEducation
    End Function

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub
    Private Sub BindDataList(leadId As String)
        With New PlacementFacade
            dlstEmployerContact.DataSource = .GetLeadEmployerNames(txtLeadId.Text)
            dlstEmployerContact.DataBind()
        End With
    End Sub
    Private Sub DlstEmployerContactItemCommand(source As Object, e As DataListCommandEventArgs) Handles dlstEmployerContact.ItemCommand
        Dim sdfControls As New SDFComponent

        ChkIsInDB.Checked = True
        GetEmploymentInfo(CType(e.CommandArgument, String))

        Session("strFrom") = "ItemCommand"
        resourceID = Trim(Request.QueryString("resid"))

        Master.Master.PageObjectId = e.CommandArgument
        Master.Master.PageResourceId = resourceID
        Master.Master.setHiddenControlForAudit()

        sdfControls.GenerateControlsEdit(pnlSDF, resourceID, CType(e.CommandArgument, String), moduleId)
        txtStEmploymentId.Text = e.CommandArgument

        'set Style to Selected Item
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, e.CommandArgument)

        'Initialize Buttons For Edit
        InitButtonsForEdit()

    End Sub
    Private Sub GetEmploymentInfo(stEmploymentId As String)
        Dim employmentinfo As New PlacementFacade
        BindEmploymentInfoExist(employmentinfo.GetLeadEmploymentInfo(stEmploymentId))
    End Sub
    Private Sub BindEmploymentInfoExist(addressInfo As PlacementInfo)
        'Bind The StudentInfo Data From The Database
        With addressInfo
            txtEmployerName.Text = .EmployerName
            txtEmployerJobTitle.Text = .EmployerJobTitle
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlJobTitleId, .JobTitleId, .JobTitleDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlJobStatusId, .JobStatus, .JobStatusDescrip)
            Dim sdate As Date
            If Date.TryParse(.StartDate, sdate) Then
                txtStartDate.SelectedDate = sdate
            Else
                txtStartDate.Clear()
            End If
            'Try

            '    txtStartDate.SelectedDate = .StartDate
            'Catch ex As System.Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    txtStartDate.Clear()
            'End Try
            'txtEndDate.Text = .EndDate
            Try
                txtEndDate.SelectedDate = .EndDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtEndDate.Clear()
            End Try
            txtComments.Text = .Comments
            txtStEmploymentId.Text = .StEmploymentId
            txtJobResponsibilities.Text = .JobResponsibilities
            txtModDate.Text = .ModDate
        End With
    End Sub
    Private Sub BtnNewClick(sender As Object, e As EventArgs) Handles btnNew.Click
        'Reset The Value of chkIsInDb Checkbox
        'To Identify an Insert
        Dim sdfControls As New SDFComponent

        ChkIsInDB.Checked = False
        Session("strFrom") = "New"
        resourceID = Trim(Request.QueryString("resid"))

        'Create a Empty Object and Initialize the Object
        BindEmploymentInfo(New PlacementInfo)

        sdfControls.GenerateControlsNew(pnlSDF, resourceID, moduleId)

        'Initialize Buttons
        InitButtonsForLoad()

        'Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstEmployerContact, Guid.Empty.ToString, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub
    Private Sub BindEmploymentInfo(addressInfo As PlacementInfo)
        'Get The StudentInfo 
        With addressInfo
            txtEmployerName.Text = ""
            txtEmployerJobTitle.Text = ""
            'txtStartDate.Text = .StartDate
            Try
                txtStartDate.SelectedDate = .StartDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtStartDate.Clear()
            End Try
            'txtEndDate.Text = .EndDate
            Try
                txtEndDate.SelectedDate = .EndDate
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtEndDate.Clear()
            End Try
            txtComments.Text = .Comments
            txtStEmploymentId.Text = .StEmploymentId
            txtJobResponsibilities.Text = .JobResponsibilities
            txtModDate.Text = ""
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlJobTitleId, .JobTitleId, .JobTitleDescrip)
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlJobStatusId, .JobStatus, .JobStatusDescrip)
        End With
    End Sub
    Private Sub BtnDeleteClick(sender As Object, e As EventArgs) Handles btnDelete.Click
        If Not (txtLeadId.Text = Guid.Empty.ToString) Then
            '   instantiate component
            Dim studentInfo As New PlacementFacade

            'Delete The Row Based on LeadId
            Dim result As String = studentInfo.DeleteLeadEmployment(txtStEmploymentId.Text, Date.Parse(txtModDate.Text))

            'If Delete Operation was unsuccessful
            'Delete The Row Based on LeadId
            'Dim result As String = StudentInfo.DeleteLeadEducation(txtPKID.Text, Date.Parse(txtModDate.Text))

            'If Delete Fails
            If result <> "" Then
                'Display Error Message
                'DisplayErrorMessage(result)
                DisplayRADAlert(CallbackType.Postback, "Error3", result, "Delete Error")
                Exit Sub
            Else
                BindEmploymentInfo(New PlacementInfo)
                ChkIsInDB.Checked = False
                BindDataList(txtStEmploymentId.Text)
                InitButtonsForLoad()
            End If
            CommonWebUtilities.RestoreItemValues(dlstEmployerContact, Guid.Empty.ToString)
            Dim sdfControls As New SDFComponent
            Try
                sdfControls.DeleteSDFValue(txtStEmploymentId.Text)
                sdfControls.GenerateControlsNew(pnlSDF, resourceID, moduleId)
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
        End If
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)

    '    '   Set error condition
    '    CustomValidator1.ErrorMessage = errorMessage
    '    CustomValidator1.IsValid = False

    '    If ValidationSummary1.ShowMessageBox = True And ValidationSummary1.ShowSummary = False And CustomValidator1.Display = ValidatorDisplay.None Then
    '        '   Display error in message box in the client
    '        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    '    End If
    'End Sub
    Protected Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'controlsToIgnore.Add(Img1)
        'Add javascript code to warn the user about non saved changes 
        '        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

End Class
