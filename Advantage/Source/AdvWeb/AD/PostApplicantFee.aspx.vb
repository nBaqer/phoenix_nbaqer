﻿Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System
Imports FAME.Advantage.Common
Imports FAME.Advantage.Reporting


Partial Class PostApplicantFees
    Inherits BasePage

    Protected ResourceId As Integer
    Protected TransactionId As String


    Protected campusId, userId As String
    Private pObj As New UserPagePermissionInfo
    Protected ModuleId As String
    Private userWantsToPrintAReceipt As Boolean
    Protected MyAdvAppSettings As AdvAppSettings

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If


        Session("SearchType") = "PostApplicantFee"

        Dim objCommon As New CommonUtilities

        Dim fac As New UserSecurityFacade
        userWantsToPrintAReceipt = False

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        'userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        'pObj = fac.GetUserResourcePermissions(userId, ResourceId, campusId)

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)
        'Check if this page still exists in the menu while switching campus
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        If Not IsPostBack Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "New")
            InitButtonsForLoad(pObj)
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   bind an empty new PostLeadPaymentInfo
            BindPostLeadPaymentData(New PostLeadPaymentInfo)
            drvTransactionDate.MaximumValue = Today

        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.PageSetup(CType(Master.FindControl("ContentMain1"), ContentPlaceHolder), "EDIT")


            InitButtonsForEdit()
        End If


    End Sub

    Private Sub BuildDropDownLists()
        BuildPaymentTypesDDL()
        BuildTransCodesDdl()

    End Sub

    Private Sub BuildTransCodesDdl()

        With ddlTransCodeId
            .DataTextField = "TransCodeDescrip"
            .DataValueField = "TransCodeID"
            .DataSource = (New LeadTransactionsFacade).GetApplicantChargeDescriptions("True", New Guid(campusId))
            .DataBind()

            .Items.Insert(0, New RadComboBoxItem("Select", Guid.Empty.ToString))

        End With
    End Sub

    Private Sub BuildPaymentTypesDDL()

        '   build ddl paymentTypes
        Dim names() As String = System.Enum.GetNames(GetType(PaymentType))
        Dim values() As Integer = System.Enum.GetValues(GetType(PaymentType))

        'clear ddl items
        ddlPaymentTypeId.Items.Clear()

        Dim strTemp As String

        For i As Integer = 0 To names.Length - 1
            If names(i).IndexOf("_") <> -1 Then
                strTemp = names(i).Replace("_", " ")
            Else
                strTemp = names(i)
            End If

            ddlPaymentTypeId.Items.Add(New RadComboBoxItem(strTemp, values(i)))

        Next

    End Sub

    Private Sub BindPostLeadPaymentData(ByVal PostLeadPaymentInfo As PostLeadPaymentInfo)
        Session("LeadId") = ""

        With PostLeadPaymentInfo
            chkIsInDB.Checked = .IsInDB
            chkIsEnrolled.Checked = .IsEnrolled
            chkIsVoided.Checked = .IsVoided
            txtLeadId.Text = .LeadId
            txtLeadName.Text = ""
            txtLeadTransactionId.Text = .LeadTransactionId

            '   set blank if the date is null 
            If Not (.LeadTransactionDate = Date.MinValue) Then
                txtTransDate.SelectedDate = .LeadTransactionDate.ToShortDateString()
            Else
                txtTransDate.SelectedDate = ""
            End If

            'txtTransAmount.Text = CType(.Amount * (-1), Decimal).ToString("#,###,###.00")
            txtTransAmount.Text = CType(.Amount, Decimal).ToString("#,###,###.00")
            ddlTransCodeId.SelectedValue = .TransCodeId
            txtTransReference.Text = .Reference
            txtDescrip.Text = .Descrip

            txtTransTypeId.Text = .TransTypeId.ToString

            txtPaymentReferenceNumber.Text = .CheckNumber
            ddlPaymentTypeId.SelectedValue = .PaymentTypeId
            txtPaymentReference.Text = .PaymentReference
            txtCampusId.Text = .CampusId

            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString

        End With
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

        '   validate user entries, display proper error message

        Dim result As String = String.Empty
        Dim errorMessage As String = String.Empty

        If txtLeadName.Text = "" OrElse txtLeadName.Text = "Search Name" Then
            errorMessage += "Please select a lead" + vbCrLf
        End If

        If ddlTransCodeId.SelectedItem.Text = "Select" Then
            errorMessage += "Transaction Code is Required" + vbCrLf
        End If

        If txtDescrip.Text = "" Then
            errorMessage += "Description is Required" + vbCrLf
        End If

        If txtTransDate.IsEmpty Then
            errorMessage += "Transaction Date is required" + vbCrLf
        ElseIf txtTransDate.SelectedDate > Today Then
            errorMessage += "Invalid Transaction Date" + vbCrLf

        End If

        result = ValidateUserEntries()
        If Not result = "" Then
            errorMessage += result
        End If

        Try
            If Decimal.Parse(txtTransAmount.Text).ToString("c") <= 0 Then
                errorMessage += "Transaction Amount must be greater than zero" + vbCrLf
            ElseIf Decimal.Parse(txtTransAmount.Text).ToString("c") > 1000000 Then
                errorMessage += "Amount is outside allowed range" + vbCrLf
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            errorMessage += "Invalid Transaction Amount" + vbCrLf
        End Try


        If errorMessage <> "" Then
            DisplayErrorMessage(errorMessage)
            Exit Sub
        End If


        '   instantiate component
        Dim laf As New LeadTransactionsFacade

        '   update PostLeadPayment Info 
        Dim postLeadPaymentInfo As PostLeadPaymentInfo = BuildPostLeadPaymentInfo(txtLeadTransactionId.Text)
        result = laf.PostLeadTransactions(postLeadPaymentInfo, AdvantageSession.UserState.UserName)
        If result = "Error" Then
            '   Display Error Message
            DisplayErrorMessage(result)
        Else
            If userWantsToPrintAReceipt Then

                PrintReceipt(postLeadPaymentInfo.PaymentTransactionId, postLeadPaymentInfo.UniqueId)

            End If


            '   bind an empty new PostLeadPaymentInfo
            BindPostLeadPaymentData(New PostLeadPaymentInfo)

            Dim AdvCtrl As AdvControls.IAdvControl = CType(LeadSearch, AdvControls.IAdvControl)
            AdvCtrl.Clear()

        End If

    End Sub

    Private Function BuildPostLeadPaymentInfo(ByVal LeadTransactionId As String) As PostLeadPaymentInfo

        '   instantiate class
        Dim PostLeadPaymentInfo As New PostLeadPaymentInfo

        With PostLeadPaymentInfo
            '   get IsInDB
            .IsInDB = chkIsInDB.Checked

            '   get LeadId
            .LeadId = txtLeadId.Text

            '   get Lead Transaction Id
            .LeadTransactionId = txtLeadTransactionId.Text

            '   get CampusId    
            .CampusId = campusId

            '   get PostPaymentDate
            .LeadTransactionDate = Date.Parse(txtTransDate.SelectedDate)

            '   get Amount
            '.Amount = Decimal.Parse(txtTransAmount.Text) * (-1.0)
            .Amount = Decimal.Parse(txtTransAmount.Text)

            '   get Trand Reference
            .Reference = txtTransReference.Text

            .Descrip = txtDescrip.Text

            '   get TransCode
            .TransCodeId = ddlTransCodeId.SelectedValue

            If Not ddlTransCodeId.SelectedItem.Text = "Select" Then
                .TransCodeDescription = ddlTransCodeId.SelectedItem.Text
            End If

            .IsEnrolled = False
            .IsVoided = False

            '   get TransTypeId
            .TransTypeId = Integer.Parse(txtTransTypeId.Text)

            '   get PaymentTypeId
            .PaymentTypeId = ddlPaymentTypeId.SelectedValue

            '   get Reference Number
            .CheckNumber = txtPaymentReferenceNumber.Text.Trim

            '   get ModUser
            .ModUser = txtModUser.Text

            '   get ModDate
            .ModDate = Date.Parse(txtModDate.Text)

            .CampusId = campusId

        End With

        '   return data
        Return PostLeadPaymentInfo

    End Function
    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click


        'BuildDropDownLists()

        '   bind an empty new PostLeadPaymentInfo
        BindPostLeadPaymentData(New PostLeadPaymentInfo)


        Dim AdvCtrl As AdvControls.IAdvControl = CType(LeadSearch, AdvControls.IAdvControl)
        AdvCtrl.Clear()

        '   initialize buttons
        InitButtonsForLoad(pObj)

        btnSave.Enabled = True

    End Sub

    Private Sub InitButtonsForLoad(ByVal pObj As UserPagePermissionInfo)
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
            btnSaveAndPrint.Enabled = True
            btnNew.Enabled = True
        Else
            btnSave.Enabled = False
            btnSaveAndPrint.Enabled = False
            btnNew.Enabled = False
        End If
        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()

        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
            btnSaveAndPrint.Enabled = True
            btnNew.Enabled = True
        Else
            btnSave.Enabled = False
            btnSaveAndPrint.Enabled = False
            btnNew.Enabled = False
        End If
        btnDelete.Enabled = False
    End Sub
    Private Function ValidateUserEntries() As String

        Dim returnMessage As String = ""
        Dim rx As New Regex("^[a-zA-Z0-9-]+$")

        '   If type of payment is "Select" then user must select a payment option
        If ddlPaymentTypeId.SelectedValue = PaymentType.Select Then
            returnMessage += "Payment Type is Required" + vbCrLf
        Else
            If Not ddlPaymentTypeId.SelectedValue = PaymentType.Cash Then
                If txtPaymentReferenceNumber.Text = "" Then
                    returnMessage += " You must enter a " + lblTypeOfReference.Text + vbCrLf
                    Return returnMessage
                End If
                Try
                    If Not rx.IsMatch(txtPaymentReferenceNumber.Text) Then

                        returnMessage += lblTypeOfReference.Text & " must be alphanumeric" + vbCrLf
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    returnMessage += "Invalid " & lblTypeOfReference.Text.ToString + vbCrLf
                End Try
            End If
        End If

        '   return error messasge
        Return returnMessage

    End Function

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub ddlPaymentTypeId_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlPaymentTypeId.SelectedIndexChanged

        '   set title of reference accordingly
        Select Case ddlPaymentTypeId.SelectedValue
            Case PaymentType.Cash
                lblTypeOfReference.Text = "Receipt Number"
            Case PaymentType.Check
                lblTypeOfReference.Text = "Check Number"
            Case PaymentType.Credit_Card
                lblTypeOfReference.Text = "C/C Authorization"
            Case PaymentType.EFT
                lblTypeOfReference.Text = "EFT Number"
            Case PaymentType.Money_Order
                lblTypeOfReference.Text = "Money Order Number"
            Case PaymentType.Non_Cash
                lblTypeOfReference.Text = "Non Cash Reference #"
        End Select

        'set focus to the same control
        CommonWebUtilities.SetFocus(Me.Page, ddlPaymentTypeId)

    End Sub

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        '   set payment type ddl visibility
        Select Case ddlPaymentTypeId.SelectedValue
            Case PaymentType.Cash, PaymentType.Select
                divlbReference.Visible = False
                divtxtReference.Visible = False
                lblTypeOfReference.Visible = False
                txtPaymentReferenceNumber.Visible = False
            Case Else
                divlbReference.Visible = True
                divtxtReference.Visible = True
                lblTypeOfReference.Visible = True
                txtPaymentReferenceNumber.Visible = True
        End Select
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(ddlPaymentTypeId)
        controlsToIgnore.Add(btnSaveAndPrint)
        'Add javascript code to warn the user about non saved changes 
        ' CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    'Private Function IsValidAmount(ByVal str As String) As Boolean
    '    Try
    '        Dim dec As Decimal = Decimal.Parse(str)
    '        If dec > 0 Then Return True
    '    Catch
    '        Return False
    '    End Try
    'End Function

    Protected Sub btnSaveAndPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAndPrint.Click
        'DisplayErrorMessage("ERROR")
        'Exit Sub
        userWantsToPrintAReceipt = True
        btnSave_Click(Me, New EventArgs)
    End Sub

    Protected Sub TransferLeadSearch(ByVal leadid As String, ByVal leadname As String)
        'DE8771
        'If leadid = String.Empty Then
        '    leadid = txtLeadId.Text
        'End If

        'txtLeadId.Text = leadid
        'txtLeadName.Text = leadname
        If leadid = String.Empty Then
            txtLeadName.Text = ""
            txtLeadId.Text = ""
        Else
            txtLeadId.Text = leadid
            txtLeadName.Text = leadname
        End If


    End Sub
    'TO DO Change report file name
    Protected Sub PrintReceipt(ByVal TransactionId As String, ByVal UniqueId As String)
        Dim getReportAsBytes As [Byte]()
        Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder") '"/Advantage Reports/" + SingletonAppSettings.AppSettings("SSRS Deployment Environment").ToString.Trim
        getReportAsBytes = (New Logic.PrintLeadReceipt).RenderReport("pdf", _
                                                                    TransactionId, _
                                                                    UniqueId, _
                                                                    AdvantageSession.UserState.UserName.ToString, _
                                                                    strReportPath, _
                                                                    AdvantageSession.UserState.CampusId.ToString)
        ExportReport("pdf", getReportAsBytes)
    End Sub

    Private Sub ExportReport(ByVal ExportFormat As String, ByVal getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case ExportFormat.ToLower
            Case Is = "pdf"
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case "excel"
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
                'Case "WORD"
                '    strExtension = "doc"
                '    strMimeType = "application/vnd.ms-word"
            Case "csv"
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select
        Session("SSRS_FileExtension") = strExtension
        Session("SSRS_MimeType") = strMimeType
        Session("SSRS_ReportOutput") = getReportAsBytes
        'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        Dim script As String = String.Format("window.open('../SY/DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString())
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    End Sub

End Class