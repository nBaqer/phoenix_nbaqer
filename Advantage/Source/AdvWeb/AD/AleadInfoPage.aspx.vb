﻿
Imports Advantage.Business.Objects

Namespace AdvWeb.AD
    Partial Class AdAleadInfoPage
        Inherits BasePage

        Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
            
            Dim isNew As Boolean = False
            If (Not Request.QueryString("new") Is Nothing) Then
                isNew = CType(Request.QueryString("new"), Boolean)
            End If

            'Get LeadId
            Dim objStudentState As StudentMRU = GetObjStudentState(1)
            If objStudentState Is Nothing Then
                RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
                Exit Sub
            End If
            'With objStudentState
            '    StudentId = .StudentId.ToString
            '    leadID = .LeadId.ToString
            '    'if lead is from a different campus show notification

            'End With
            Master.Master.PageObjectId = objStudentState.LeadId.ToString()
            Master.Master.PageResourceId = CType(Request.QueryString("resid"), Integer)
            Master.Master.SetHiddenControlForAudit()


            If (Master.Master.IsSwitchedCampus and not isNew) Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, String.Empty)
            End If
        End Sub
    End Class
End Namespace