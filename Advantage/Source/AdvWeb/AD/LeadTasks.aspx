﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="LeadTasks.aspx.vb" Inherits="LeadTasks" %>
<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
 <script src="../Scripts/Advantage.Client.AD.js"></script>
<script src="../js/CheckAll.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" Runat="Server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
				<!-- begin rightcolumn -->
				<tr>
					<td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								 <td class="menuframe" align="right">
                                <asp:Button ID="btnSaveNext" runat="server" Enabled="False" Text="Save" CssClass="save" ToolTip="Clicking this button will enable user to add new leads with out exiting this page">
                                </asp:Button>
                                <asp:Button ID="btnNew" runat="server" Enabled="False" Text="New" CssClass="new"
                                    CausesValidation="False"></asp:Button>
                                <asp:Button ID="btnDelete" runat="server" Enabled="False" Text="Delete" CssClass="delete"
                                    CausesValidation="False"></asp:Button></td>
                            
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                        <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tr>
                                                <td>
                                                    <asp:DataGrid ID="dgdLeadTasks" runat="server" AutoGenerateColumns="False" Width="100%"
                                                        CellPadding="3" BorderWidth="1px" BorderStyle="Solid" BorderColor="#E0E0E0">
                                                        <EditItemStyle CssClass="label"></EditItemStyle>
                                                        <AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
                                                        <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                        <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                        <FooterStyle CssClass="label"></FooterStyle>
                                                        <Columns>
                                                            <asp:TemplateColumn HeaderText="Date Started">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="DateStarted" runat="server" CssClass="label" Text='<%# Container.DataItem("StartDate")  %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Task Name">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTaskName" runat="server" CssClass="label" Text='<%# Container.DataItem("TaskDescrip")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Status">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStatus" runat="server" CssClass="label" Text='<%# Container.DataItem("Status")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                            <asp:TemplateColumn HeaderText="Result">
                                                                <HeaderStyle CssClass="datagridheader"></HeaderStyle>
                                                                <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblResult" runat="server" CssClass="label" Text='<%# Container.DataItem("ResultDescrip") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateColumn>
                                                        </Columns>
                                                    </asp:DataGrid>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- end content table-->
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <asp:textbox id="txtStudentId" Runat="server" CssClass="textbox" Visible="false"></asp:textbox>
			<!-- start validation panel-->
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

