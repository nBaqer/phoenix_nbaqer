﻿Imports System.Diagnostics
Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports System.Collections
Imports FAME.Advantage.Common
Imports System.IO
Imports System.Xml.Xsl

Partial Class LeadNotes
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCode As Label
    Protected WithEvents txtLeadNoteCode As TextBox
    Protected WithEvents lblCampGrpId As Label
    Protected WithEvents ddlCampGrpId As DropDownList
    Protected WithEvents btnhistory As Button
    Protected WithEvents Label1 As Label
    Protected LeadId As String
    Protected userId As String
    Protected modCode As String
    Protected WithEvents Dropdownlist1 As DropDownList
#End Region

    Private pObj As New UserPagePermissionInfo
    Private campusId As String
    Private ResourceId As Integer
    Protected BindFacade As New LeadNotesFacade

    'Private mruProvider As MRURoutines
    Protected StudentId As String
    Protected state As AdvantageSessionState
    Protected boolSwitchCampus As Boolean = False
    Private Const JAVASCRIPT As String = "<script>var origwindow=window.self;window.open('../sa/PrintAnyReport.aspx');</script>"

    Protected Sub Page_PreInit(sender As Object, e As EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        
    End Sub
    Private Function GetLeadFromStateObject(ByVal paramResourceId As Integer) As StudentMRU

        Dim objStudentState As New StudentMRU

        Try
            MyBase.GlobalSearchHandler(1)

            boolSwitchCampus = Master.Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"


            Master.Master.ShowHideStatusBarControl(True)
            Master.Master.PageObjectId = LeadId
            Master.Master.PageResourceId = Request.QueryString("resid")
            Master.Master.setHiddenControlForAudit()


        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function




#End Region
    Private Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.Load

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.Master.CurrentCampusId ' XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        userId = AdvantageSession.UserState.UserId.ToString


        Dim advantageUserState As New User()
        advantageUserState = AdvantageSession.UserState
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

        'While switching campus user may not have permission to this page in that campus
        'so redirect user to dashboard page
        If pObj.HasNone = True Then
            Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" & advantageUserState.CampusId.ToString & "&desc=dashboard")
            Exit Sub
        End If

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New StudentMRU
        objStudentState = GetLeadFromStateObject(456) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString
            'if lead is from a different campus show notification

        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        ''   it is assumed that there is always an UserId(EmpId) in the session
        'If CommonWebUtilities.IsValidGuid(userId) Then
        '    '  userId = userId - NZ 11/19 assignment has no effect
        'Else
        '    '   for testing purpose only
        '    userId = "919ED2F6-109B-4B51-AE02-9ED08EE3C4B5".ToLower ' Anatoly
        '    'end of testing code
        'End If

        '   get module code
        If Not Request.Params("mod") Is Nothing Then
            modCode = Request.Params("mod")
        Else
            modCode = "UK"
        End If
        txtLeadId.Text = LeadId

        Dim objCommon As New CommonUtilities
        Dim content As ContentPlaceHolder = CType(DirectCast(Master.Master.FindControl("ContentMain1"), ContentPlaceHolder).FindControl("LeadNestedMasterPageContent"), ContentPlaceHolder)

        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            'objCommon.PageSetup(Form1, "NEW")

            objCommon.PageSetup(content, "NEW")

            'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            objCommon.SetBtnState(content, "NEW", pObj)

            'objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            '   initialize buttons
            InitButtonsForLoad()
            ViewState("MODE") = "NEW"

            '   build dropdownlists
            BuildDropDownLists()

            '   get start and end dates for Notes Dates
            GetStartAndEndDates()

            '   bind datadrid
            BindDataGrid(LeadId, "CreatedDate desc")

            '   bind an empty new LeadNoteInfo
            'BindLeadNoteData(New LeadNoteInfo(LeadId, userId))

            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(4, objStudentState.Name)
            End If

            MyBase.uSearchEntityControlId.Value = ""


        Else
            objCommon.PageSetup(content, "EDIT")

            'objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
        End If

        'Code to disable all buttons if lead was already enrolled
        'to prevent data being changed after lead was enrolled
        Dim chkLeadWasEnrolled As Boolean
        Dim leadFacade As New LeadEnrollmentFacade
        chkLeadWasEnrolled = leadFacade.CheckIfLeadWasEnrolled(LeadId)
        If chkLeadWasEnrolled = True Then
            btnSave.Enabled = False
            btnNew.Enabled = False
            btnDelete.Enabled = False
            btnNew.ToolTip = "Cannot add new notes as the lead was already enrolled"
            btnSave.ToolTip = "Cannot modify notes  as the lead was already enrolled"
            btnDelete.ToolTip = "Cannot delete notes as the lead was already enrolled"
            ' DE 5538 Janet Robinson 05/09/2011 stop ability to add lead notes after enrollment
            btnAddNewNote.Enabled = False
            btnAddNewNote.ToolTip = "Cannot add or modify new notes as the lead was already enrolled"
            ' DE 5538 End
        Else
            btnSave.ToolTip = ""
            btnDelete.ToolTip = ""
            btnNew.ToolTip = ""
            ' DE 5538 Janet Robinson 05/09/2011 
            btnAddNewNote.ToolTip = ""
            ' DE 5538 End
        End If
    End Sub
    Private Sub BindDataGrid(ByVal LeadId As String, ByVal sortExpression As String)

        '   bind LeadNotes datalist
        'With New LeadNotesFacade
        dgrdLeadNotes.DataSource = New DataView(BindFacade.GetNotesForLeadByModuleDS(LeadId, ddlModules.SelectedValue, Date.Parse(txtTransDateFrom.SelectedDate), Date.Parse(txtTransDateTo.SelectedDate)).Tables("LeadNotes"), Nothing, sortExpression, DataViewRowState.CurrentRows)
        dgrdLeadNotes.DataBind()
        'End With

    End Sub
    Private Sub BuildDropDownLists()
        BuildModulesDDL()
    End Sub
    Private Sub BuildModulesDDL()

        '   bind the modules DDL
        With ddlModules
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleCode"
            .DataSource = (New SystemFacade).GetAllModules()
            .DataBind()
            .Items.Insert(0, New ListItem("All Modules", "UK"))
            .SelectedValue = modCode
        End With

        '   bind the modules DDL
        With ddlModules1
            .DataTextField = "ModuleName"
            .DataValueField = "ModuleCode"
            .DataSource = (New SystemFacade).GetAllModules()
            .DataBind()
            .SelectedValue = modCode
        End With

    End Sub
    Private Function BuildLeadNoteInfo(ByVal LeadNoteId As String) As LeadNoteInfo
        '   instantiate class
        Dim LeadNoteInfo As New LeadNoteInfo(LeadId, userId)

        With LeadNoteInfo

            If LeadNoteId = Guid.Empty.ToString Then
                LeadNoteId = Guid.NewGuid.ToString
                .IsInDB = False
                '   get ModUser
                .ModUser = AdvantageSession.UserState.UserName

                '   get ModDate
                .ModDate = Date.Now
            Else
                .IsInDB = True
                '   get ModUser
                .ModUser = txtModUser.Text

                '   get ModDate
                .ModDate = Date.Parse(txtModDate.Text)
            End If

            '   get LeadNoteId
            .LeadNoteId = LeadNoteId

            '   get LeadNote text
            .Description = txtNote.Text

            '   get ModuleCode
            .ModuleCode = ddlModules1.SelectedValue

            '   get UserId
            .UserId = userId

        End With

        '   return data
        Return LeadNoteInfo

    End Function
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
            btnAddNewNote.Enabled = True
        Else
            btnNew.Enabled = False
            btnAddNewNote.Enabled = False
        End If

        'There is no need for the SAVE button to be enabled because 
        'once a note is added, it also is saved.
        btnSave.Enabled = False

        'Notes cannot be deleted.
        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
            btnAddNewNote.Enabled = True
        Else
            btnNew.Enabled = False
            btnAddNewNote.Enabled = False
        End If


        'There is no need for the SAVE button to be enabled because 
        'once a note is added, it also is saved.
        btnSave.Enabled = False

        'Notes cannot be deleted.
        btnDelete.Enabled = False
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
    End Sub
    'Private Sub DisplayErrorMessage(ByVal errorMessage As String)

    '    '   Set error condition
    '    Customvalidator1.ErrorMessage = errorMessage
    '    Customvalidator1.IsValid = False

    '    If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
    '        '   Display error in message box in the client
    '        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    '    End If
    'End Sub
    Protected Function TruncateNote(ByVal note As String) As String
        '   if less than 40 chars return the whole note
        If note.Length < 40 Then
            Return note.Substring(0, note.Length)
        Else
            '   truncate as the last space in the first 40 chars
            Dim maxLength As Integer = note.LastIndexOf(" ", 39, 35)
            If maxLength > 0 Then
                Return note.Substring(0, maxLength) + " ..."
            Else
                Return note.Substring(0, 40)
            End If
        End If
    End Function
    Protected Function TruncateNoteComplement(ByVal note As String) As String
        '   if less than 40 chars return the whole note
        If note.Length < 40 Then
            Return ""
        Else
            '   truncate as the last space in the first 40 chars
            Dim maxLength As Integer = note.LastIndexOf(" ", 39, 35)
            If maxLength > 0 Then
                Return note.Substring(maxLength)
            Else
                Return ""
            End If
        End If
    End Function
    Private Sub GetStartAndEndDates()
        '   instantiate facade
        'With New LeadNotesFacade
        '   get min and max dates from the transactions Table
        Dim dates() As Date = BindFacade.GetMinAndMaxDatesFromNotes()

        '   fill textboxes with min and max dates
        txtTransDateFrom.SelectedDate = dates(0).ToShortDateString
        txtTransDateTo.SelectedDate = dates(1).ToShortDateString
        'End With
    End Sub
    Private Sub btnBuildList_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBuildList.Click
        '   validate From and To Dates
        Dim result As String = ValidateDates()

        '   if any date is invalid display an error message
        If Not result = "" Then
            'DisplayErrorMessage(result)
            DisplayRADAlert(CallbackType.Postback, "Error1", result, "List Error")
            Exit Sub
        Else
            txtNote.Text = ""
            '   no item selected
            dgrdLeadNotes.EditItemIndex = -1

            '   bind datadrid
            BindDataGrid(LeadId, "CreatedDate desc")
        End If
    End Sub
    Private Function ValidateDates() As String
        Dim result As String = ""
        If Not CommonWebUtilities.IsValidDate(txtTransDateFrom.SelectedDate.ToString) Then result += "Invalid 'From Date'" + vbCrLf
        If Not CommonWebUtilities.IsValidDate(txtTransDateTo.SelectedDate.ToString) Then result += "Invalid 'To Date'" + vbCrLf
        If Not result = "" Then Return result
        If Date.Parse(txtTransDateFrom.SelectedDate) > Date.Parse(txtTransDateTo.SelectedDate) Then result += "'From' date is later than 'To' date" & vbCrLf
        '   return result
        Return result
    End Function

    Private Sub dgrdLeadNotes_SortCommand(ByVal source As Object, ByVal e As DataGridSortCommandEventArgs) Handles dgrdLeadNotes.SortCommand
        Dim sortExpression As String = e.SortExpression

        '   if the user clicks in the same sort column twice.. switch the order from asc to desc or viceversa
        If Not ViewState("LatestSortExpression") Is Nothing Then
            If sortExpression = CType(ViewState("LatestSortExpression"), String) Then
                If sortExpression.IndexOf(" asc") > 0 Then
                    sortExpression = sortExpression.Replace(" asc", " desc")
                ElseIf sortExpression.IndexOf(" desc") > 0 Then
                    sortExpression = sortExpression.Replace(" desc", " asc")
                End If
            End If
        End If

        '   save the latest sortExpression in the ViewState
        ViewState("LatestSortExpression") = sortExpression

        '   bind datadrid
        BindDataGrid(LeadId, sortExpression)
    End Sub

    Private Sub btnAddNewNote_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnAddNewNote.Click

        Dim MyAdvAppSettings As AdvAppSettings
        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        ''''' Code changes to Fix mantis issue id 19549 by kamalesh Ahuja on 23 Aug 2010 '''
        Dim LeadMasterUpdate As New LeadFacade
        If MyAdvAppSettings.AppSettings("EditOtherLeads").ToString.Trim.ToLower = "no" Then
            If LeadMasterUpdate.CheckLeadUser(userId, txtLeadId.Text) = 0 Then
                'DisplayErrorMessage("You do not have rights to edit this lead")
                DisplayRADAlert(CallbackType.Postback, "Error2", "You do not have rights to edit this lead ", "Add Error")
                Exit Sub
            End If
        End If
        ''''''''''''''''''
        Dim g As String
        If btnAddNewNote.Text = "Save this Note" Then
            g = txtLeadNoteId.Text
        Else
            g = Guid.Empty.ToString
        End If

        '   update LeadNote Info 
        Dim result As String = BindFacade.UpdateLeadNoteInfo(BuildLeadNoteInfo(g), AdvantageSession.UserState.UserName) '(New LeadNotesFacade).UpdateLeadNoteInfo(BuildLeadNoteInfo(g), AdvantageSession.UserState.UserName)
        If Not result = "" Then
            '   Display Error Message
            'DisplayErrorMessage(result)
            DisplayRADAlert(CallbackType.Postback, "Error3", result, "Add Error")
        Else
            btnAddNewNote.Text = "Add New Note"
        End If

        '   if there are no errors bind a new entity and init buttons
        If Page.IsValid Then

            '   Update filter Start and End dates
            UpdateStartAndEndDates(Date.Now)

            '   no item selected
            dgrdLeadNotes.EditItemIndex = -1

            '   bind the dataGrid
            BindDataGrid(LeadId, "CreatedDate desc")

            '   clear the textbox of the note
            txtNote.Text = ""
            InitButtonsForEdit()
        End If
    End Sub

    Private Sub dgrdLeadNotes_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgrdLeadNotes.ItemCommand

        '   this is the selected item
        dgrdLeadNotes.EditItemIndex = e.Item.ItemIndex

        '   bind the dataGrid
        BindDataGrid(LeadId, "CreatedDate desc")
        btnAddNewNote.Enabled = True
    End Sub

    Private Sub dgrdLeadNotes_ItemDataBound(ByVal sender As Object, ByVal e As DataGridItemEventArgs) Handles dgrdLeadNotes.ItemDataBound
        '   process only the selected item
        Select Case e.Item.ItemType
            Case ListItemType.EditItem
                '   set the height to the maximum number of rows
                CType(e.Item.FindControl("txtLeadNote"), TextBox).Rows = NumberOfLinesInText(e.Item.DataItem("LeadNoteDescrip"))
                btnAddNewNote.Text = "Save this Note"
                txtLeadNoteId.Text = CType(e.Item.DataItem("LeadNoteId"), Guid).ToString
                txtNote.Text = e.Item.DataItem("LeadNoteDescrip")
                txtModUser.Text = e.Item.DataItem("ModUser")
                txtModDate.Text = CType(e.Item.DataItem("ModDate"), DateTime).ToString
            Case ListItemType.Item, ListItemType.AlternatingItem
                'enable or disable notes according to the user and the date of the note
                Dim b As Boolean = IsUserAllowedToMakeChangesToThisNote(e.Item.DataItem("ModUser"), e.Item.DataItem("ModDate"))
                If Not pObj.HasEdit And Not pObj.HasFull Then b = False
                CType(e.Item.FindControl("lnkLeadNote"), LinkButton).Visible = b And (pObj.HasEdit)
                Dim c As Label = CType(e.Item.FindControl("lblLeadNote"), Label)
                If Not b = False Then c.Text = TruncateNoteComplement(c.Text)
                If b And (pObj.HasEdit And Not pObj.HasAdd) Then
                    btnAddNewNote.Text = "Save this Note"
                End If
        End Select

    End Sub
    Private Function NumberOfLinesInText(ByVal str As String) As Integer
        Dim i As Integer = 1
        Dim position As Integer

        '   count the number of LFs
        While (position > -1)
            position = str.IndexOf(vbLf.ToString, position + 1)
            i += 1
        End While

        '   return number of lines.. that is number of LF + 1 ... not to exceeed 15
        If i < 16 Then Return i Else Return 15

    End Function
    Private Sub UpdateStartAndEndDates(ByVal d As Date)
        '   if necessary, update earliest and latest dates controls
        If d < Date.Parse(txtTransDateFrom.SelectedDate) Then txtTransDateFrom.SelectedDate = d.ToShortDateString
        If d > Date.Parse(txtTransDateTo.SelectedDate) Then txtTransDateTo.SelectedDate = d.ToShortDateString

    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNew.Click
        '   init buttons for load
        InitButtonsForLoad()

        '   clear the textbox of the note
        txtNote.Text = ""

        dgrdLeadNotes.EditItemIndex = -1

        '   bind datadrid
        BindDataGrid(LeadId, "CreatedDate desc")

    End Sub
    Private Function IsUserAllowedToMakeChangesToThisNote(ByVal user As String, ByVal dateTime As DateTime) As Boolean

        'allow changes to the same user and only if the note is less than 24 hours old
        If AdvantageSession.UserState.UserName = user And Now.Subtract(dateTime).Days < 1 Then Return True Else Return False

    End Function
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(txtTransDateFrom)
        controlsToIgnore.Add(txtTransDateTo)
        controlsToIgnore.Add(btnBuildList)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    ' US2166 1/17/2012 Janet Robinson setup for printing of all notes for a lead
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrint.Click
        Dim ds As DataSet
        ds = BindFacade.GetNotesForLeadByModuleDS(LeadId, "UK", Date.Parse(txtTransDateFrom.SelectedDate), Date.Parse(txtTransDateTo.SelectedDate))
        'print report

        '   build Xml document with deposit information
        Dim xmlStream As MemoryStream = CreateXmlStreamToPrint(ds.Tables("LeadNotes"))
        Dim xmlReader As XmlReader = xmlReader.Create(xmlStream)
        '   get XslStyleSheet 
        Dim templateFilePath As String = Server.MapPath("..") + "\" + Context.Request.Headers.Item("host") + "\Xsl\LeadNotesHtml.xsl"
        '   generate html document
        Dim xslt As New XslCompiledTransform
        Dim documentMemoryStream As New MemoryStream
        Dim writer As New XmlTextWriter(documentMemoryStream, Nothing)

        Try
            xslt.Load(templateFilePath)
            writer.Formatting = Formatting.Indented
            xslt.Transform(xmlReader, Nothing, writer, Nothing)
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try

        '   save document and document type in sessions variables
        Session("DocumentMemoryStream") = documentMemoryStream
        Session("ContentType") = "text/html"

        '   Register a javascript to open the report in another window
        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "NewWindow", JAVASCRIPT, False)

    End Sub

    Private Function CreateXmlStreamToPrint(ByVal leadNotesTable As DataTable) As MemoryStream

        Dim myMemoryStream As New MemoryStream

        Dim printXmlTextWriter As New XmlTextWriter(myMemoryStream, Nothing)

        '   write xml document header
        printXmlTextWriter.Formatting = Formatting.Indented
        printXmlTextWriter.WriteStartDocument(False)
        printXmlTextWriter.WriteComment("FAME - This document contains a list of notes for the selected lead")

        '   output start of element "document"
        printXmlTextWriter.WriteStartElement("document")

        '   output start of element "notes"
        printXmlTextWriter.WriteStartElement("notes")


        '   loop through out all notes for the student
        Dim i As Integer
        For i = 0 To leadNotesTable.Rows.Count - 1
            '   print only selected notes

            Dim row As DataRow = leadNotesTable.Rows(i)

            If i = 0 Then

                '   output StudentName only once as header
                '   output start of element "notehdr"
                printXmlTextWriter.WriteStartElement("notehdr")
                printXmlTextWriter.WriteAttributeString("leadName", ("Lead Notes For " & row.Item("LeadName")))
                '   output end tag of element "notehdr"
                printXmlTextWriter.WriteEndElement()
            End If

            '   output start of element "note"
            printXmlTextWriter.WriteStartElement("note")

            '   output Date
            printXmlTextWriter.WriteAttributeString("date", CType(row.Item("CreatedDate"), Date))

            '   output Module
            printXmlTextWriter.WriteAttributeString("module", CType(row.Item("ModuleCode"), String))

            '   output User
            printXmlTextWriter.WriteAttributeString("user", CType(row.Item("UserName"), String))

            '   output Note
            printXmlTextWriter.WriteAttributeString("noteDesc", CType(row.Item("LeadNoteDescrip"), String))

            '   output end tag of element "note"
            printXmlTextWriter.WriteEndElement()


        Next

        '   output end tag of element "notes"
        printXmlTextWriter.WriteEndElement()

        '   output end tag of element "document"
        printXmlTextWriter.WriteEndElement()

        'Write the XML to file and close the writer
        printXmlTextWriter.Flush()
        myMemoryStream.Position = 0

        '   return memoryStream
        Return myMemoryStream

    End Function



End Class
