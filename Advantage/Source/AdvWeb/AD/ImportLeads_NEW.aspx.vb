﻿Imports System.IO
Imports System.Net
Imports FAME.AdvantageV1.BusinessFacade
Imports Advantage.Business.Objects
Imports Telerik.Web.UI
Imports System.Data
Imports FAME.AdvantageV1.Common
Imports System.Text.RegularExpressions.Regex

Partial Class AD_ImportLeads_NEW
    Inherits BasePage

    Private ReadOnly myFlFacade As New LeadEnrollmentFacade
    Dim campusId As String = ""
    Dim strSourceFilePath As String = ""
    'Dim strImportFilePath As String = ""
    Dim strArchiveFilePath As String = ""
    Dim strExceptionFilePath As String = ""
    Dim strRemoteUserName As String = ""
    Dim strRemotePassword As String = ""
    Dim strSourceFileLocation As String = ""
    Protected UserId, ResourceId As String
    Protected State As AdvantageSessionState
    Private pObj As New UserPagePermissionInfo
    Private Property RemoveAllDup As Boolean
        Get
            If (ViewState("RemoveAllDup") Is Nothing) Then
                Return False
            Else
                Return DirectCast(ViewState("RemoveAllDup"), Boolean)
            End If
        End Get
        Set(value As Boolean)
            ViewState.Add("RemoveAllDup", value)
        End Set
    End Property

    'Private mruProvider As MRURoutines
    Protected Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        CheckCampusSetup()
        BindGridWithAdvFields()

        If Not Session("dtLookups") Is Nothing And Not Session("LookupSources") Is Nothing Then
            BuildStructureForLookupValuesGrid()
        End If

        'load Advantage state
        State = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
        State("empInfo") = Nothing
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, State)
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        btnImport.Attributes.Add("onclick", "if(confirm('Are you sure you want to import?')){}else{return false}")
        Dim advantageUserState As User = AdvantageSession.UserState
        'Dim strSourceFilePath As String = Request.CurrentExecutionFilePath

        strSourceFilePath = Request.CurrentExecutionFilePath

        campusId = Master.CurrentCampusId 'HttpContext.Current.Request.Params("cmpid")
        myFlFacade.GetImportLeadsFoldersPaths(campusId, strSourceFilePath, strArchiveFilePath, strExceptionFilePath, strRemoteUserName, strRemotePassword)


        campusId = Master.CurrentCampusId 'XmlConvert.ToGuid(HttpContext.Current.Request.Params("cmpid")).ToString
        ResourceId = HttpContext.Current.Request.Params("resid")
        UserId = AdvantageSession.UserState.UserId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

        'Preload tabs
        If (Not IsPostBack) Then
            RadTabStrip1.Tabs(0).CssClass = "TabArrowStartDark"
            RadTabStrip1.Tabs(1).CssClass = "TabArrowMidLight"
            RadTabStrip1.Tabs(2).CssClass = "TabArrowMidLight"
            RadTabStrip1.Tabs(3).CssClass = "TabArrowMidLight"
            RadTabStrip1.Tabs(4).CssClass = "TabArrowMidLight"
            RadTabStrip1.Tabs(5).CssClass = "TabArrowEndLight"
        End If

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            If Not Page.IsPostBack Then
                If trMain.Visible = False Then Exit Sub
                BuildFilesList()
                BuildCampusDdl()
                rgAdvFields.Visible = False
                lblMap.Visible = False
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub
    Private Sub CheckCampusSetup()

        Dim ds As DataSet = myFlFacade.CheckImportLeadsFoldersPaths(HttpContext.Current.Request.Params("cmpid"))

        Dim sMsgHead As String = CType(("</br></br></br></br></br></br></br></br>" & "You have not completed the import lead setup for " & ds.Tables(0).Rows(0).Item("CampDescrip") & "</br>"), String)
        Dim sMsgFoot As String = CType(("Please go to Maintenance/Academics/Campus to complete the import lead setup for campus " & ds.Tables(0).Rows(0).Item("CampDescrip") & "</br>"), String)
        Dim sMsgBody As String = String.Empty

        If ds.Tables(0).Rows(0).Item("ILSourcePath") Is DBNull.Value Then
            sMsgBody = "Import Path is blank </br>"
        End If

        If ds.Tables(0).Rows(0).Item("ILArchivePath") Is DBNull.Value Then
            sMsgBody = sMsgBody + "Archive Path is blank </br>"
        End If

        'If ds.Tables(0).Rows(0).Item("ILExceptionPath") Is DBNull.Value Then
        '    sMsgBody = sMsgBody + "Exception Path is blank </br>"
        'End If

        If Not sMsgBody = String.Empty Then
            trMain.Visible = False
            trerrmsg.Style("visibility") = CType(Visible, String)
            lblErrMsg.Text = sMsgHead + sMsgBody + sMsgFoot
        End If

    End Sub

#Region "read File"

    Protected Sub BtnReadFileClick(sender As Object, e As EventArgs) Handles btnReadFile.Click
        ReadFile()
    End Sub

    Private Sub ReadFile(Optional ByVal displayContents As Boolean = True)
        'Dim myFLFacade As New LeadEnrollmentFacade
        Dim strFileName As String
        Dim request As WebRequest
        Dim responseStream As Stream = Nothing
        Dim responseFile As WebResponse = Nothing
        Dim contents As String
        Dim intNumRecs As Integer = 0
        Dim intNumFlds As Integer = 0
        Dim prevNumFlds As Integer = 0
        Dim myImpersonate As New Impersonate
        Dim intNumRecsWithoutQuotes As Integer = 0

        'Reset the Session variable that stores the DT for populating the contents of the file
        Session("SourceDT") = Nothing

        If ddlFileNames.SelectedValue = "" Then
            DisplayErrorMessage("Please select a file to Import")
            Exit Sub
        End If
        'User has not specified any starting record
        If txtStartRecord.Text = "" Then
            DisplayErrorMessage("You must specify a start record")
            Exit Sub
        End If
        If CInt(txtStartRecord.Text) <> 1 Then
            DisplayErrorMessage("The start record must be from 1")
            Exit Sub
        End If

        'get paths
        myFlFacade.GetImportLeadsFoldersPaths(campusId, strSourceFilePath, strArchiveFilePath, "", strRemoteUserName, strRemotePassword)
        strFileName = String.Concat(strSourceFilePath, ddlFileNames.SelectedItem.Text)
        If myFlFacade.IsRemoteServer(campusId) Then
            strSourceFileLocation = strSourceFilePath + "\" + ddlFileNames.SelectedItem.Text
        End If
        If strFileName.IndexOfAny(":") = -1 And strSourceFileLocation = "" Then
            DisplayErrorMessage("Remote check-box needs to be checked for Import Leads in the campus page.")
            Exit Sub
        End If

        Try
            Dim uriSource As New Uri(strFileName)
            Dim strSourcePath As String = uriSource.LocalPath
            Dim objIn As StreamReader

            If myFlFacade.IsRemoteServer(campusId) Then
                Dim strDomain As String
                Dim strUserName As String
                strDomain = myImpersonate.GetDomainFromDomainAndUserName(strRemoteUserName)
                strUserName = myImpersonate.GetUserNameFromDomainAndUserName(strRemoteUserName)

                If myImpersonate.RemoteAccess_LogOnAndImpersonate(strDomain, strUserName, strRemotePassword) Then
                    objIn = File.OpenText(strFileName)
                Else
                    DisplayErrorMessage("There is a problem accessing the remote server")
                    Return
                End If
            Else
                request = WebRequest.Create(strFileName)
                request.Headers.Add("Translate: f")
                request.Credentials = CredentialCache.DefaultCredentials
                responseFile = request.GetResponse
                responseStream = responseFile.GetResponseStream

                If strSourceFileLocation = "" Then
                    objIn = New StreamReader(strSourcePath, Encoding.Default)
                Else
                    objIn = New StreamReader(strSourceFileLocation, Encoding.Default)
                End If
            End If

            While objIn.Peek() > -1
                ' Do
                Try
                    contents = objIn.ReadLine().Trim()

                    'Make sure the line has data and also it does not contain just commas. If we are reading a .csv
                    'file in Excel format the blank lines in the spreadsheet are read as just commas for each field
                    If (contents.Length > 0) And (GetCountOfCharacter(contents, CType(",", Char)) <> contents.Length) Then

                        intNumRecs += 1

                        'We only want to process records that are >= the starting record specified by the user.
                        If intNumRecs >= CInt(txtStartRecord.Text) Then
                            Dim splitRecord As String() = Regex.Split(contents, ",", RegexOptions.None)

                            If contents.Contains(ControlChars.Quote) Then
                                'Don't do any checks for number of fields.
                            Else
                                intNumRecsWithoutQuotes += 1
                                intNumFlds = splitRecord.Length

                                If intNumRecsWithoutQuotes = 1 Then
                                    prevNumFlds = intNumFlds
                                End If

                                If prevNumFlds <> intNumFlds Then
                                    DisplayErrorMessage("The file does not contain the same number of fields in all records.")
                                    Exit Sub
                                End If

                                prevNumFlds = intNumFlds
                            End If

                            Dim hasSameNumFields As Boolean
                            AddRecordToDt(splitRecord, contents.Contains(ControlChars.Quote), hasSameNumFields)

                            If hasSameNumFields = False Then
                                DisplayErrorMessage("The file does not contain the same number of fields in all records.")
                                Exit Sub
                            End If
                        End If
                    End If

                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ' Exit Do
                    Exit While
                End Try

                'Loop Until contents Is Nothing

            End While
            objIn.Close()

            If myFlFacade.IsRemoteServer(campusId) Then
                myImpersonate.RemoteAccess_UndoImpersonate()
            Else
                If Not responseStream Is Nothing Then
                    responseStream.Flush()
                    responseStream.Close()
                End If
                If Not responseFile Is Nothing Then
                    responseFile.Close()
                End If
            End If


            'If intNumRecs is 0 at this point it means that the file does not have any records
            If intNumRecs = 0 Then
                DisplayErrorMessage("The file does not contain any records.")
                Exit Sub
            End If

            'Session("NumberOfFields") = intNumFlds

            If displayContents = True Then
                'Bind the Grid displaying the contents of the selected file.
                'If it is already populated we need to clear it or we will end up appending data to it.
                If Not Session("SourceDT") Is Nothing Then
                    If rgFile.Visible = True Then
                        rgFile.DataSource = Nothing
                        rgFile.DataBind()
                    End If
                    rgFile.DataSource = Session("SourceDT")
                    rgFile.DataBind()
                    If rgFile.Visible = False Then
                        rgFile.Visible = True
                    End If
                    SetToolTipForReadFileGrid()
                End If

                'We now need to create the drop down list boxes for the grid with the advantage fields.
                'This is based on the number of fields in the file.
                BindGridWithAdvFields()

                'Select the first items in the DDLS on the Initial Values tab
                ddlDefaultCampus.SelectedIndex = 0
                ddlDefaultAdmissionsRep.SelectedIndex = 0
                ddlDefaultLeadStatus.SelectedIndex = 0

                'Show the Previous and Next buttons and Instructions
                btnPrevious.Visible = False
                btnNextToExceptions.Visible = True
                lblWarnMap.Visible = True
            End If


        Catch ex As ArgumentNullException
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        Catch ex As ArgumentOutOfRangeException
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw
        End Try
    End Sub

    Sub SetToolTipForReadFileGrid()
        'Dim strFldTypeMsg As String = ""
        Dim gdi As GridDataItem
        Dim fldName As String
        For i As Integer = 0 To Session("NumberOfFields") - 1
            For x As Integer = 0 To (rgFile.Items.Count - 1)
                gdi = rgFile.Items(x)
                fldName = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                If Len(gdi(fldName).Text) > 20 Then
                    gdi(fldName).ToolTip = gdi(fldName).Text
                End If

            Next
        Next
    End Sub


    Private Sub AddRecordToDt(ByVal record() As String, ByVal recordHasQuotation As Boolean, ByRef hasSameNumFields As Boolean)
        Dim recordModified(record.Length - 1) As String
        Dim startedQuotation As Boolean
        Dim indexStartQuotation As Integer
        Dim recordModifiedCounter As Integer = 0

        'If the record has quotations in it then we need to modify the record array
        If recordHasQuotation Then
            'Loop through the fields in the array. 
            'If an item does NOT contain a quote then we can simply add it to the modified array
            startedQuotation = False

            For i As Integer = 0 To record.Length - 1
                recordModified(i) = record(i).ToString
                If i = 0 Then
                    If record(i).ToString.Contains(ControlChars.Quote) Then
                        startedQuotation = True
                        indexStartQuotation = i
                    End If
                End If
                If Not record(i).ToString.Contains(ControlChars.Quote) And Not startedQuotation Then
                    recordModified(recordModifiedCounter) = record(i).ToString
                    recordModifiedCounter += 1
                ElseIf record(i).ToString.Contains(ControlChars.Quote) And Not startedQuotation Then
                    startedQuotation = True
                    indexStartQuotation = i
                    recordModified(recordModifiedCounter) = Replace(record(i).ToString, ControlChars.Quote, "") 'need to remove the quote when we are setting the value
                ElseIf Not record(i).ToString.Contains(ControlChars.Quote) And startedQuotation Then
                    'This should be added to record that started quotation in the modified array as it is not really a new record
                    Dim prevVal = recordModified(indexStartQuotation).ToString
                    Dim newVal = prevVal + ", " + record(i).ToString

                    recordModified.SetValue(newVal, indexStartQuotation)
                ElseIf record(i).ToString.Contains(ControlChars.Quote) And startedQuotation Then
                    'We are at the end of a quotation so we should reset startedQuotation
                    'and add record to the last record in the modified array as it is not really a new record
                    startedQuotation = False
                    Dim prevVal = recordModified(indexStartQuotation).ToString
                    Dim newVal = prevVal + ", " + Replace(record(i).ToString, ControlChars.Quote, "")

                    recordModified.SetValue(newVal, indexStartQuotation)
                    recordModifiedCounter += 1
                End If
            Next
            ReDim Preserve recordModified(recordModifiedCounter - 1)
        Else
            recordModified = record
        End If

        If Session("SourceDT") Is Nothing Then
            'Create the DataTable. We have to pass in the first record so it knows how many columns to create.
            'Note that the record gets added to the DataTable in this method.
            CreateSourceDt(recordModified)
            Session("NumberOfFields") = recordModified.Length
            hasSameNumFields = True
        Else
            'The DataTable already exists so we can simply add the record to it.
            'Check if the record we are about to add has the same number of fields as the datatable
            If recordModified.Length = CType(Session("SourceDT"), DataTable).Columns.Count Then
                AddRecordToSourceDt(recordModified, DirectCast(Session("SourceDT"), DataTable))
                hasSameNumFields = True
            Else
                Session("SourceDT") = Nothing
                hasSameNumFields = False
                Session("NumberOfFields") = 0
            End If

        End If
    End Sub


    Private Sub CreateSourceDt(record() As String)
        'We need to add a column for each field in the record. The columns need to be named with the format Field + Num such 
        'as Field1, Field2 etc. This is important as the RadGrid that we will bind the DataTable to will use the same structure.
        'If the user indicates that the first row has the column names then we should use those names to name the columns of the 
        'SourceDT DataTable
        Dim sourceDt As New DataTable
        Dim strDc As String
        Dim dr As DataRow

        Try
            For i As Integer = 0 To record.Length - 1
                If chkFRowColHeaders.Checked = False Then
                    strDc = "Field" + CStr(i + 1)
                Else
                    strDc = record(i).ToString
                End If

                Dim dc As New DataColumn(strDc, GetType(String))
                sourceDt.Columns.Add(dc)
            Next



            If chkFRowColHeaders.Checked = False Then
                dr = sourceDt.NewRow

                For i As Integer = 0 To record.Length - 1
                    dr(i) = record(i).ToString
                Next

                sourceDt.Rows.Add(dr)

            End If

            Session("SourceDT") = sourceDt

        Catch exdup As DuplicateNameException
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(exdup)

            Session.Remove("SourceDT")
            ' Duplicate column occurs when the user use a csv file as it has header row and does not.
            DisplayErrorMessage("Duplicate column name found! If you are using first row as column name option, check if your csv file contain a row of name, or it has a duplicate field. Original Message: " + exdup.Message)

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Session.Remove("SourceDT")
            ' This give the user a message about the nature of the error and not silent finish the procedure.
            DisplayErrorMessage(ex.Message)
        End Try



    End Sub

    Private Sub AddRecordToSourceDt(record() As String, sourceDt As DataTable)
        ' Dim SourceDT As DataTable
        Dim dr As DataRow

        ' SourceDT = Session("SourceDT")
        dr = sourceDt.NewRow

        For i As Integer = 0 To record.Length - 1
            dr(i) = record(i).ToString
        Next

        sourceDt.Rows.Add(dr)
        Session("SourceDT") = sourceDt

    End Sub

    Protected Sub BindGridWithAdvFields()
        Dim dt As DataTable
        Dim dtAdvFileFldMappings As New DataTable
        Dim intNumFlds As Integer

        If Session("AdvFieldsDT") Is Nothing Then
            dt = myFlFacade.GetAdvImportLeadsFields()
            Session("AdvFieldsDT") = dt
        Else
            dt = CType(Session("AdvFieldsDT"), DataTable)
        End If

        intNumFlds = CType(Session("NumberOfFields"), Integer)

        'If the user has selected a saved mapping then we need to create a DT that can be used to select the Advantage
        'field that was mapped to for that file field number.
        If ddlSavedMappings.SelectedIndex <> 0 And Not (ddlSavedMappings.SelectedItem Is Nothing) Then
            dtAdvFileFldMappings = myFlFacade.GetAdvFileFldMappings(ddlSavedMappings.SelectedItem.Value)
        End If

        rgAdvFields.Columns.Clear()
        rgAdvFields.DataSource = dt
        For i As Integer = 0 To intNumFlds - 1
            Dim templateColumn As GridTemplateColumn = New GridTemplateColumn
            'We have to use the try catch here again as this part would error out when called from Page_Init
             'We have to use the try 	Dim exTracker = new AdvApplicationInsightsInitializer()
            'We have to use the try 	exTracker.TrackExceptionWrapper(here again)

            'since dtAdvFileFldMappings has not been initialized at that point.
            Try
                If dtAdvFileFldMappings.Rows.Count > 0 Then
                    templateColumn.HeaderTemplate = New GridViewDdlTemplate(dt, i, dtAdvFileFldMappings)
                Else
                    templateColumn.HeaderTemplate = New GridViewDdlTemplate(dt, i)
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                templateColumn.HeaderTemplate = New GridViewDdlTemplate(dt, i)
            End Try

            templateColumn.ItemTemplate = Nothing
            rgAdvFields.MasterTableView.Columns.Add(templateColumn)
        Next
        rgAdvFields.DataBind()
        For Each itm As GridDataItem In rgAdvFields.Items
            Dim row As TableRow = TryCast(itm, TableRow)
            If itm.ItemType = GridItemType.AlternatingItem Or itm.ItemType = GridItemType.Item Then
                row.Visible = False
            End If
        Next


        If intNumFlds > 0 Then
            If rgFile.Visible = True Then
                rgAdvFields.Visible = True
                lblMap.Visible = True
            Else
                rgFile.Visible = False
                lblMap.Visible = False
            End If

        Else
            rgAdvFields.Visible = False
            lblMap.Visible = False
        End If
    End Sub

    Protected Sub RgFileNeedDataSource(source As Object, e As GridNeedDataSourceEventArgs) Handles rgFile.NeedDataSource
        If Not Session("SourceDT") Is Nothing Then
            If rgFile.Visible = True Then
                rgFile.DataSource = Nothing
                rgFile.DataBind()
            End If
            rgFile.DataSource = Session("SourceDT")
            rgFile.DataBind()
            If rgFile.Visible = False Then
                rgFile.Visible = True
            End If
            SetToolTipForReadFileGrid()
        End If
        BindGridWithAdvFields()
    End Sub
    Protected Sub RgFilePageIndexChanged(sender As Object, e As GridPageChangedEventArgs) Handles rgFile.PageIndexChanged
        If Not Session("SourceDT") Is Nothing Then
            If rgFile.Visible = True Then
                rgFile.DataSource = Nothing
                rgFile.DataBind()
            End If
            rgFile.DataSource = Session("SourceDT")
            rgFile.DataBind()
            If rgFile.Visible = False Then
                rgFile.Visible = True
            End If
            SetToolTipForReadFileGrid()
        End If
    End Sub
    Protected Sub RgFilePageSizeChanged(sender As Object, e As GridPageSizeChangedEventArgs) Handles rgFile.PageSizeChanged
        If Not Session("SourceDT") Is Nothing Then
            If rgFile.Visible = True Then
                rgFile.DataSource = Nothing
                rgFile.DataBind()
            End If
            rgFile.DataSource = Session("SourceDT")
            rgFile.DataBind()
            If rgFile.Visible = False Then
                rgFile.Visible = True
            End If
            SetToolTipForReadFileGrid()
        End If

    End Sub
#End Region

#Region "Exception Grid"

    Private Sub BuildExceptionsGrid()
        Dim strFldTypeMsg As String = ""
        Dim ddlId As String
        Dim hi As GridHeaderItem
        ' Dim gdi As GridDataItem
        Dim selectedFldName As String
        Dim testDate As DateTime
        Dim fldNameSource As String
        Dim fldNameSourceValue As String

        'Make sure to set the session variable to nothing. Otherwise, if the user leaves this tab, goes to another tab and then
        'click on this tab again we are going to end up adding the records again to the datatable stored in the session variable.
        'This will lead to the records being duplicated when displayed in the excpetions grid.
        Session("SourceExceptionDT") = Nothing
        Session("Duplicates") = Nothing
        Session("Exceptions") = Nothing
        rgExceptions.DataSource = String.Empty
        rgExceptions.DataBind()

        'Loop through the advantage ddls. When we find a date field use the index to then loop through all the records of
        'the file for the column.
        For i As Integer = 0 To Session("NumberOfFields") - 1
            ddlId = "ddl" & CStr(i)
            hi = CType(rgAdvFields.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
            selectedFldName = CType(hi.FindControl(ddlId), DropDownList).SelectedValue.ToUpper
            If (selectedFldName = "FIRSTNAME" Or selectedFldName = "LASTNAME") _
            Or (selectedFldName = "SSN") _
            Or (selectedFldName = "ASSIGNEDDATE" Or selectedFldName = "BIRTHDATE" Or selectedFldName = "DATEAPPLIED" Or
                selectedFldName = "SOURCEDATE" Or selectedFldName = "EXPECTEDSTART") _
                 Then

                'Or (selectedFldName = "HOMEEMAIL" Or selectedFldName = "WORKEMAIL") _
                'Or (selectedFldName = "PHONE" Or selectedFldName = "PHONE2") _
                'Or (selectedFldName = "ZIP") _

                Dim x As Integer = 0
                For Each gdi As DataRow In DirectCast(Session("SourceDT"), DataTable).Rows

                    'Get the name of the field from the SourceDT in session. It is better to do this because
                    'we cannot use the index on the GridDataItem object reliably.
                    fldNameSource = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                    fldNameSourceValue = CType(gdi(fldNameSource), String)
                    If (selectedFldName = "FIRSTNAME" Or selectedFldName = "LASTNAME") Then
                        'First Name and Last Name: Check if the field is empty
                        If (fldNameSourceValue = "" _
                        Or fldNameSourceValue = " " _
                        Or fldNameSourceValue = "&nbsp;") Then
                            AddRecordToExDt(selectedFldName, (x + 1), fldNameSourceValue, "Missing required field")
                            strFldTypeMsg = "There are empty first and last names"
                        End If
                    ElseIf (selectedFldName = "SSN") Then
                        'Check if we have 9 digits after replacing the speical characters \, / or -
                        If (fldNameSourceValue = "" _
                        Or fldNameSourceValue = " " _
                        Or fldNameSourceValue = "&nbsp;") Then
                            ' Empty Record: Do nothing
                        Else
                            If (Not IsValidSsn(fldNameSourceValue)) Then
                                AddRecordToExDt(selectedFldName, (x + 1), fldNameSourceValue, "Invalid SSN")
                                strFldTypeMsg = "There are invalid SSNs"
                            End If

                            'If (Len(Replace(fldNameSourceValue, "\", "")) = 9 _ 
                            'Or Len(Replace(fldNameSourceValue, "/", "")) = 9 _
                            'Or Len(Replace(fldNameSourceValue, "-", "")) = 9) Then
                            '    'Valid SSN: Do nothing
                            'Else
                            '    AddRecordToExDt(selectedFldName, (x + 1), fldNameSourceValue, "Invalid SSN")
                            '    strFldTypeMsg = "There are invalid SSNs"
                            'End If
                        End If
                    ElseIf (selectedFldName = "ASSIGNEDDATE" Or selectedFldName = "BIRTHDATE" Or selectedFldName = "DATEAPPLIED" Or
                             selectedFldName = "EXPECTEDSTART" Or selectedFldName = "SOURCEDATE") Then
                        'Check if we have a valid date for the field with the same index as i.
                        If fldNameSourceValue <> "" And fldNameSourceValue <> " " And fldNameSourceValue <> "&nbsp;" Then
                            If (Not Date.TryParse(fldNameSourceValue, testDate)) Then
                                'If we get into this section it means we have an invalid date
                                AddRecordToExDt(selectedFldName, (x + 1), fldNameSourceValue, "Invalid date format")
                                strFldTypeMsg = "There are invalid dates"
                            End If
                        End If
                    ElseIf (selectedFldName = "HOMEEMAIL" Or selectedFldName = "WORKEMAIL") Then
                        If (fldNameSourceValue = "" _
                        Or fldNameSourceValue = " " _
                        Or fldNameSourceValue = "&nbsp;") Then
                            ' Empty Record: Do nothing
                        Else
                            If (Not IsValidEMail(fldNameSourceValue)) Then
                                AddRecordToExDt(selectedFldName, (x + 1), fldNameSourceValue, "Invalid email format")
                                strFldTypeMsg = "There are invalid Email"
                            End If
                        End If
                    ElseIf (selectedFldName = "PHONE" Or selectedFldName = "PHONE2") Then
                        If (fldNameSourceValue = "" _
                        Or fldNameSourceValue = " " _
                        Or fldNameSourceValue = "&nbsp;") Then
                            ' Empty Record: Do nothing
                        Else
                            If (Not (IsValidPhone(fldNameSourceValue) Or IsValidInternationalPhone(fldNameSourceValue))) Then
                                AddRecordToExDt(selectedFldName, (x + 1), fldNameSourceValue, "Invalid phone format")
                                strFldTypeMsg = "There are invalid phone"
                            End If
                        End If
                    ElseIf (selectedFldName = "ZIP") Then
                        If (fldNameSourceValue = "" _
                        Or fldNameSourceValue = " " _
                        Or fldNameSourceValue = "&nbsp;") Then
                            ' Empty Record: Do nothing
                        Else
                            If (Not IsValidZipCode(fldNameSourceValue)) Then
                                AddRecordToExDt(selectedFldName, (x + 1), fldNameSourceValue, "Invalid zip code format")
                                strFldTypeMsg = "There are invalid zip"
                            End If
                        End If
                    End If
                    x = x + 1
                Next
            End If
        Next

        If strFldTypeMsg <> "" Then
            With rgExceptions
                If Not Session("SourceExceptionDT") Is Nothing Then
                    .DataSource = CType(Session("SourceExceptionDT"), DataTable)
                Else
                    .DataSource = String.Empty
                End If
                .DataBind()
            End With
            lblNoExceptions.Visible = False
            lblWarnExcept.Visible = True
            btnExportExceptionsToExcel.Visible = True
        Else
            lblNoExceptions.Visible = True
            lblWarnExcept.Visible = False
            btnExportExceptionsToExcel.Visible = False
        End If
    End Sub


    Protected Sub RgExceptionsNeedDataSource(source As Object, e As GridNeedDataSourceEventArgs) Handles rgExceptions.NeedDataSource
        If Not Session("SourceExceptionDT") Is Nothing Then
            rgExceptions.DataSource = CType(Session("SourceExceptionDT"), DataTable)
        Else
            rgExceptions.DataSource = String.Empty
        End If
    End Sub
    Protected Sub RgExceptionsPageSizeChanged(sender As Object, e As GridPageSizeChangedEventArgs) Handles rgExceptions.PageSizeChanged
        If Not Session("SourceExceptionDT") Is Nothing Then
            rgExceptions.DataSource = CType(Session("SourceExceptionDT"), DataTable)
        Else
            rgExceptions.DataSource = String.Empty
        End If
    End Sub
    Protected Sub RgExceptionsPageIndexChanged(sender As Object, e As GridPageChangedEventArgs) Handles rgExceptions.PageIndexChanged
        If Not Session("SourceExceptionDT") Is Nothing Then
            rgExceptions.DataSource = CType(Session("SourceExceptionDT"), DataTable)
        Else
            rgExceptions.DataSource = String.Empty
        End If
    End Sub
#End Region

#Region "Duplicates Grid"

    Private Sub BuildDuplicatesGrid()
        'Get the names of the fields in the rgFile data-grid for the FirstName, LastName, SSN, BirthDate, Phone and HomeEmail. 
        'We will loop through each record in the rgFile data-grid and use these names to get the value for FirstName etc.
        'for each record that we will use to test for duplicates.
        Dim ddlId As String
        Dim hi As GridHeaderItem
        ' Dim gdi As GridDataItem
        Dim selectedValue As String
        Dim firstNameFld = ""
        Dim lastNameFld = ""
        Dim ssnFld = ""
        Dim birthDateFld = ""
        Dim phoneFld = ""
        Dim homeEmailFld = ""
        Dim middleNameFld = ""
        Dim address1Fld = ""
        Dim phone2Fld = ""
        Dim firstNameVal As String
        Dim lastNameVal As String
        Dim ssnVal As String
        Dim birthDateVal As String
        Dim phoneVal As String
        Dim homeEmailVal As String
        Dim middleNameVal As String
        Dim address1Val As String
        Dim phone2Val As String
        Dim lefac As New LeadEnrollmentFacade

        'Make sure to set the session variable to nothing. Otherwise, if the user leaves this tab, goes to another tab and then
        'click on this tab again we are going to end up adding the records again to the datatable stored in the session variable.
        'This will lead to the records being duplicated when displayed in the duplicates grid.
        Session("SourceDuplicatesDT") = Nothing

        'Get the corresponding field name for FIRSTNAME, LASTNAME etc.
        For i As Integer = 0 To Session("NumberOfFields") - 1
            ddlId = "ddl" & CStr(i)
            hi = CType(rgAdvFields.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
            selectedValue = CType(hi.FindControl(ddlId), DropDownList).SelectedValue.ToUpper

            Select Case selectedValue
                Case "FIRSTNAME"
                    firstNameFld = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                Case "LASTNAME"
                    lastNameFld = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                Case "SSN"
                    ssnFld = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                Case "BIRTHDATE"
                    birthDateFld = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                Case "PHONE"
                    phoneFld = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                Case "HOMEEMAIL"
                    homeEmailFld = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                Case "MIDDLENAME"
                    middleNameFld = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                Case "ADDRESS1"
                    address1Fld = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                Case "PHONE2"
                    phone2Fld = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
            End Select
        Next

        'Now that we have the names we can loop through each record in the rgfile data-grid and use these names to
        'get the values for FIRSTNAME, LASTNAME etc.
        Dim x = 0
        For Each gdi As DataRow In DirectCast(Session("SourceDT"), DataTable).Rows


            'Get the values for first name and last name that are required fields
            firstNameVal = CType(gdi(firstNameFld), String) '.Text
            lastNameVal = CType(gdi(lastNameFld), String) '.Text

            'Get the values for the optional fields. 
            If ssnFld <> "" Then
                If gdi(ssnFld) <> "" And gdi(ssnFld) <> "&nbsp;" And gdi(ssnFld) <> " " Then
                    ssnVal = CType(gdi(ssnFld), String)
                Else
                    ssnVal = ""
                End If
            Else
                ssnVal = ""
            End If

            If birthDateFld <> "" Then
                If gdi(birthDateFld) <> "" And gdi(birthDateFld) <> "&nbsp;" And gdi(birthDateFld) <> " " Then
                    birthDateVal = CType(gdi(birthDateFld), String)
                Else
                    birthDateVal = ""
                End If
            Else
                birthDateVal = ""
            End If


            If phoneFld <> "" Then
                If gdi(phoneFld) <> "" And gdi(phoneFld) <> "&nbsp;" And gdi(phoneFld) <> " " Then
                    phoneVal = CType(gdi(phoneFld), String)
                Else
                    phoneVal = ""
                End If
            Else
                phoneVal = ""
            End If


            If homeEmailFld <> "" Then
                If gdi(homeEmailFld) <> "" And gdi(homeEmailFld) <> "&nbsp;" And gdi(homeEmailFld) <> " " Then
                    homeEmailVal = CType(gdi(homeEmailFld), String)
                Else
                    homeEmailVal = ""
                End If
            Else
                homeEmailVal = ""
            End If


            If middleNameFld <> "" Then
                If gdi(middleNameFld) <> "" And gdi(middleNameFld) <> "&nbsp;" And gdi(middleNameFld) <> " " Then
                    middleNameVal = CType(gdi(middleNameFld), String)
                Else
                    middleNameVal = ""
                End If
            Else
                middleNameVal = ""
            End If

            If address1Fld <> "" Then
                If gdi(address1Fld) <> "" And gdi(address1Fld) <> "&nbsp;" And gdi(address1Fld) <> " " Then
                    address1Val = CType(gdi(address1Fld), String)
                Else
                    address1Val = ""
                End If
            Else
                address1Val = ""
            End If

            If phone2Fld <> "" Then
                If gdi(phone2Fld) <> "" And gdi(phone2Fld) <> "&nbsp;" And gdi(phone2Fld) <> " " Then
                    phone2Val = CType(gdi(phone2Fld), String)
                Else
                    phone2Val = ""
                End If
            Else
                phone2Val = ""
            End If

            'If this is a duplicate pass it to the sub to add to the datatable with the duplicates. This should only be done if
            'there are no invalid data. For example, the birth date must be a valid date, SSN must be 9 digits after replacing any 
            'special characters and first name and last name cannot be empty.
            'We should skip any invalid record.
            'If ValidateRecord(firstNameVal, lastNameVal, ssnVal, birthDateVal, phoneVal, homeEmailVal) Then
            If (Not RecordIsInExceptions(x + 1)) Then
                If lefac.DuplicateExistsForImportLeadRecord(firstNameVal, lastNameVal) Then
                    'Record already exists so add it to the duplicates radgrid
                    AddRecordToDupDT((x + 1), firstNameVal, lastNameVal, middleNameVal, ssnVal, birthDateVal, phoneVal, homeEmailVal, address1Val, phone2Val)
                End If
            Else
                'DisplayErrorMessage("Record was skipped because of invalid data")
            End If

            x = x + 1
        Next

        If Not IsNothing(Session("SourceDuplicatesDT")) Then
            rgDuplicates.DataSource = CType(Session("SourceDuplicatesDT"), DataTable)
            rgDuplicates.DataBind()
            AllChkRemoveAreChecked()
            'UpdatePageChkRemove()
            lblNoDuplicates.Visible = False
            lblWarnDuplicates.Visible = True
            btnExportDuplicatesToExcel.Visible = True
        Else
            lblNoDuplicates.Visible = True
            lblWarnDuplicates.Visible = False
            btnExportDuplicatesToExcel.Visible = False
        End If



    End Sub
    Protected Sub RgDuplicatesNeedDataSource(source As Object, e As GridNeedDataSourceEventArgs) Handles rgDuplicates.NeedDataSource
        rgDuplicates.DataSource = CType(Session("SourceDuplicatesDT"), DataTable)
    End Sub
    Protected Sub RgDuplicatesItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles rgDuplicates.ItemDataBound
        Dim item As GridDataItem = TryCast(e.Item, GridDataItem)
        Dim rowNum As Integer
        If (item IsNot Nothing) Then
            Dim chk As CheckBox = DirectCast(item.FindControl("chkRemove"), CheckBox)
            Dim dtDup As DataTable = CType(Session("SourceDuplicatesDT"), DataTable)
            If (Not IsNothing(dtDup)) Then
                If IsNumeric(item("row").Text) Then
                    rowNum = CType(item("row").Text, Integer)
                    'Dim chk As CheckBox = DirectCast(item("Remove").FindControl("chkRemove"), CheckBox)
                    'If DirectCast(sender, CheckBox).ClientID = chk.ClientID Then
                    Dim filterExp As String = "Row = " & rowNum
                    Dim dr() As DataRow = dtDup.Select(filterExp)
                    If dr.Length > 0 Then
                        chk.Checked = CType(dr(0)("Remove"), Boolean)
                    End If
                End If
            End If
        End If
        '    Dim previousSysStatusId As Integer
        '    Dim fromSysStatusId As StateEnum = CType((New BatchStatusChangeFacade).GetSysStatus(ddlFromStatusCodeId.SelectedValue), StateEnum)
        '    Select Case fromSysStatusId
        '        Case StateEnum.LeaveOfAbsence
        '            previousSysStatusId = CType(CType(dataItem.FindControl("lblLoaPreviousSysStatusID"), Label).Text, Integer)
        '            If (previousSysStatusId = StateEnum.AcademicProbation) Then
        '                dataItem.CssClass = "datagriditemstyleHihglighted"
        '                ShowAcademicProbationMessage = True
        '            Else
        '                dataItem.CssClass = "datagriditemstyle"
        '            End If

        '        Case StateEnum.Suspended
        '            ' As some records have a previous status as null (Not logical but is a fact) so label lblSusPreviousSysStatusID is empty
        '            ' so this case we suppose for this case previous status was “Currently Attendding”
        '            If (CType(dataItem.FindControl("lblSusPreviousSysStatusID"), Label).Text = String.Empty) Then
        '                previousSysStatusId = 9 'Currently Attending 
        '            Else
        '                previousSysStatusId = CType(CType(dataItem.FindControl("lblSusPreviousSysStatusID"), Label).Text, Integer)
        '            End If
        '            If (previousSysStatusId = StateEnum.AcademicProbation) Then
        '                dataItem.CssClass = "datagriditemstyleHihglighted"
        '                ShowAcademicProbationMessage = True
        '            Else
        '                dataItem.CssClass = "datagriditemstyle"
        '            End If
        '    End Select
        'End If

    End Sub
    Protected Sub RgDuplicatesPageSizeChanged(sender As Object, e As GridPageSizeChangedEventArgs) Handles rgDuplicates.PageSizeChanged
        rgDuplicates.DataSource = CType(Session("SourceDuplicatesDT"), DataTable)
    End Sub
    Protected Sub RgDuplicatesPageIndexChanged(sender As Object, e As GridPageChangedEventArgs) Handles rgDuplicates.PageIndexChanged
        rgDuplicates.DataSource = CType(Session("SourceDuplicatesDT"), DataTable)
    End Sub

    Protected Sub RgDuplicatesItemCreated(sender As Object, e As GridItemEventArgs)
        If (e.Item.ItemType = GridItemType.Header) Then
            Dim hItem As GridHeaderItem = DirectCast(e.Item, GridHeaderItem)
            Dim chk1 As CheckBox = DirectCast(hItem.FindControl("chkRemoveAll"), CheckBox)
            'HiddenRemoveAll.Value = chk1.ClientId.ToString()
            chk1.Checked = RemoveAllDup
        End If
        If (e.Item.ItemType = GridItemType.Item) Then
            Dim item As GridDataItem = CType(e.Item, GridDataItem)
            Dim chk As CheckBox = DirectCast(item.FindControl("chkRemove"), CheckBox)
            Dim dtDup As DataTable = CType(Session("SourceDuplicatesDT"), DataTable)
            If (Not IsNothing(dtDup)) Then
                Dim rowNum As Integer
                If (Integer.TryParse(item("row").Text, rowNum) = True) Then
                    'Dim chk As CheckBox = DirectCast(item("Remove").FindControl("chkRemove"), CheckBox)
                    'If DirectCast(sender, CheckBox).ClientID = chk.ClientID Then
                    Dim filterExp As String = "Row = " & rowNum
                    Dim dr() As DataRow = dtDup.Select(filterExp)
                    If dr.Length > 0 Then
                        chk.Checked = CType(dr(0)("Remove"), Boolean)
                    End If
                End If
            End If
        End If
    End Sub
    Protected Sub CheckBoxChanged(sender As Object, e As EventArgs)
        'Dim headerItem As GridHeaderItem = TryCast(rgDuplicates.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
        'Dim chkRemoveAll As CheckBox = DirectCast(headerItem.FindControl("chkRemoveAll"), CheckBox)

        Dim dtDup As DataTable = CType(Session("SourceDuplicatesDT"), DataTable)
        If (Not IsNothing(dtDup)) Then
            Dim rowNum As Integer
            For Each item As GridDataItem In rgDuplicates.MasterTableView.Items
                If (Integer.TryParse(item("row").Text, rowNum) = True) Then
                    Dim chk As CheckBox = DirectCast(item("Remove").FindControl("chkRemove"), CheckBox)
                    If DirectCast(sender, CheckBox).ClientID = chk.ClientID Then
                        Dim filterExp As String = "Row = " & rowNum
                        Dim dr() As DataRow = dtDup.Select(filterExp)
                        If dr.Length > 0 Then
                            If (chk.Checked = True) Then
                                dr(0)("Remove") = True
                            Else
                                dr(0)("Remove") = False
                            End If
                        End If
                        Exit For
                    End If
                End If
            Next
            dtDup.AcceptChanges()
            Session("SourceDuplicatesDT") = dtDup
            AllChkRemoveAreChecked()
        End If
    End Sub
    Private Sub AllChkRemoveAreChecked()
        'if some item is not checked the checkbox header should be false
        Dim headerItem As GridHeaderItem = TryCast(rgDuplicates.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
        Dim chkRemoveAll As CheckBox = DirectCast(headerItem.FindControl("chkRemoveAll"), CheckBox)
        Dim dtDup As DataTable = CType(Session("SourceDuplicatesDT"), DataTable)
        If (Not IsNothing(dtDup)) Then
            For Each drDup As DataRow In dtDup.Rows
                If (drDup("Remove") = False) Then
                    chkRemoveAll.Checked = False
                    RemoveAllDup = False
                    Return
                End If
            Next
            chkRemoveAll.Checked = True
            RemoveAllDup = True
        End If
    End Sub
    'Private Sub UpdatePageChkRemove()
    '    Dim dtDup As DataTable = Session("SourceDuplicatesDT")
    '    If (Not IsNothing(dtDup)) Then
    '    Dim rowNum As Integer     
    '    For Each item As GridDataItem In rgDuplicates.MasterTableView.Items
    '        rowNum = item("Row").Text
    '        If IsNumeric(rowNum) Then
    '            Dim chk As CheckBox = DirectCast(item("Remove").FindControl("chkRemove"), CheckBox)
    '            Dim filterExp As String = "Row = " & rowNum
    '            Dim dr() As DataRow = dtDup.Select(filterExp)
    ' If dr.Length > 0 Then
    '   chk.Checked =  dr(0)("Remove") 
    'End if
    '        End If
    '     Next
    '    End IF
    'End Sub
    Protected Sub ChkRemoveAllChanged(sender As Object, e As EventArgs)
        RemoveAllDup = DirectCast(sender, CheckBox).Checked
        Dim dtDup As DataTable = CType(Session("SourceDuplicatesDT"), DataTable)
        If (Not IsNothing(dtDup)) Then
            For Each drDup As DataRow In dtDup.Rows
                drDup("Remove") = RemoveAllDup
            Next
            dtDup.AcceptChanges()
            Session("SourceDuplicatesDT") = dtDup
        End If
        Dim rowNum As Integer
        For Each item As GridDataItem In rgDuplicates.MasterTableView.Items
            If (Integer.TryParse(item("row").Text, rowNum) = True) Then
                Dim chk As CheckBox = DirectCast(item("Remove").FindControl("chkRemove"), CheckBox)
                chk.Checked = RemoveAllDup
            End If
        Next
    End Sub
#End Region

#Region "tabstrip and buttons"

    Protected Sub RadTabStrip1_TabClick(sender As Object, e As RadTabStripEventArgs)
        Dim strMsg As String
        Dim strValidate As String

        strMsg = ""

        If e.Tab.Text <> "Map Field Names" Then
            strMsg = ValidateTab1()
        End If

        If strMsg = "" Then
            Select Case e.Tab.Text
                Case "Map Field Names"
                    EnableMapFieldsAndExceptionsTabs()
                Case "Exceptions"
                    BuildExceptionsGrid()
                Case "Duplicates"
                    BuildDuplicatesGrid()
                Case "Map Field Detail"
                    BuildLookupValuesGrids()
                Case "Initial Values"
                    'Enable the Import File tab
                    If Not ValidateMapLookupValues() Then
                        strValidate = "You must map all of the lookup fields." + vbCrLf
                        RadTabStrip1.SelectedIndex = 3
                        RadTabStrip1.SelectedTab.Enabled = True
                        RadMultiPage1.SelectedIndex = 3
                        DisplayErrorMessage(strValidate)
                    Else
                        RadTabStrip1.Tabs(5).Enabled = True
                    End If

                Case "Import File"
                    'We want to give the user a preview of what the data will look like when imported into Advantage.
                    'This step is very important because the user will usually have to map several lookup values.
                    'They therefore need a chance to see the data to make sure their mappings are correct. For eg.
                    'the user might have incorreclty selected Female when mapping their code of M for gender.
                    PreviewDataToBeImported()
            End Select

        Else
            'Note that you have to change the SelectedIndex property of the multipage too.
            RadTabStrip1.SelectedIndex = 0
            RadTabStrip1.SelectedTab.Enabled = True
            RadMultiPage1.SelectedIndex = 0
            DisplayErrorMessage(strMsg)

        End If


    End Sub

    Protected Sub BtnNextToExceptionsClick(sender As Object, e As EventArgs) Handles btnNextToExceptions.Click
        Dim strMsg As String

        strMsg = ValidateTab1()

        If strMsg = "" Then
            RadTabStrip1.SelectedIndex = 1
            RadMultiPage1.SelectedIndex = 1
            RadTabStrip1.Tabs(1).CssClass = "TabArrowMidDark"
            BuildExceptionsGrid()
        Else
            DisplayErrorMessage(strMsg)
        End If

    End Sub

    Protected Sub BtnPrevToMapFieldsClick(sender As Object, e As EventArgs) Handles btnPrevToMapFields.Click
        'This handles the Previous button on the Exceptions tab.
        'The user is taken back to the Map Field Names tab and the Exceptions tab is disabled.  
        Session("SourceExceptionDT") = Nothing
        rgExceptions.DataSource = String.Empty
        rgExceptions.Rebind()
        RadTabStrip1.SelectedIndex = 0
        RadMultiPage1.SelectedIndex = 0
        RadTabStrip1.Tabs(1).CssClass = "TabArrowMidLight"
    End Sub

    Protected Sub BtnNextToDuplicatesClick(sender As Object, e As EventArgs) Handles btnNextToDuplicates.Click
        'This handles the Next button on the Exceptions tab.
        'The user is taken to the Duplicates tab and the Exceptions tab is disabled.
        RadTabStrip1.SelectedIndex = 2
        RadMultiPage1.SelectedIndex = 2
        RadTabStrip1.Tabs(2).CssClass = "TabArrowMidDark"
        BuildDuplicatesGrid()
    End Sub

    Protected Sub BtnPrevToExceptionsClick(sender As Object, e As EventArgs) Handles btnPrevToExceptions.Click
        'This handles the Previous button on the Duplicates tab.
        'The user is taken back to the Exceptions tab and the Duplicates tab is disabled.
        Session("SourceDuplicatesDT") = Nothing
        rgDuplicates.DataSource = String.Empty
        rgDuplicates.Rebind()
        RadTabStrip1.SelectedIndex = 1
        RadMultiPage1.SelectedIndex = 1
        RadTabStrip1.Tabs(2).CssClass = "TabArrowMidLight"
    End Sub

    Protected Sub BtnNextToMapLookupValuesClick(sender As Object, e As EventArgs) Handles btnNextToMapLookupValues.Click
        'This handles the Next button on the Duplicates tab.
        'The user is taken to the Map Field Detail tab and the Duplicates tab is disabled.
        'Before we send the user to the Map Field Detail tab we need to check if the user has selected any record to be removed
        ' UpdateDuplicatesDTWithRecordsToRemove()
        RadTabStrip1.SelectedIndex = 3
        RadMultiPage1.SelectedIndex = 3
        RadTabStrip1.Tabs(3).CssClass = "TabArrowMidDark"
        BuildLookupValuesGrids()
    End Sub

    Protected Sub BtnPrevToDuplicatesClick(sender As Object, e As EventArgs) Handles btnPrevToDuplicates.Click
        'This handles the Previous button on the Map Field Detail tab.
        'The user is taken back to the Duplicates tab and the Map Field Detail tab is disabled.
        RadTabStrip1.SelectedIndex = 2
        RadMultiPage1.SelectedIndex = 2
        RadTabStrip1.Tabs(3).CssClass = "TabArrowMidLight"
    End Sub

    Protected Sub BtnNextToDefaultValuesClick(sender As Object, e As EventArgs) Handles btnNextToDefaultValues.Click
        'This handles the Next button on the Map Field Detail tab.
        'The user is taken to the Initial Values tab and the Map Field Detail tab is disabled.
        'We have to make sure that the user has mapped all the lookup fields before going to the next tab
        Dim strValidate As String

        If Not ValidateMapLookupValues() Then
            strValidate = "You must map all of the lookup fields." + vbCrLf
            RadTabStrip1.SelectedIndex = 3
            RadMultiPage1.SelectedIndex = 3
            DisplayErrorMessage(strValidate)
        Else
            RadTabStrip1.SelectedIndex = 4
            RadMultiPage1.SelectedIndex = 4
            RadTabStrip1.Tabs(4).CssClass = "TabArrowMidDark"
        End If

    End Sub

    Protected Sub BtnPrevToMapLookupValuesClick(sender As Object, e As EventArgs) Handles btnPrevToMapLookupValues.Click
        'This handles the Previous button on the Initial Values tab.
        'The user is taken back to the Map Field Detail tab and the Initial Values tab is disabled.
        RadTabStrip1.SelectedIndex = 3
        RadMultiPage1.SelectedIndex = 3
        RadTabStrip1.Tabs(4).CssClass = "TabArrowMidLight"
    End Sub

    Protected Sub BtnNextToImportFileClick(sender As Object, e As EventArgs) Handles btnNextToImportFile.Click
        'This handles the Next button on the Initial Values tab.
        'The user is taken to the Import File tab and the Initial Values tab is disabled.
        'We have to make sure that the user has made selections for all the fields before going to the next tab
        Dim strValidate As String

        If Not ValidateDefaultValues() Then
            strValidate = "You must select a value for all fields." + vbCrLf
            RadTabStrip1.SelectedIndex = 4
            RadMultiPage1.SelectedIndex = 4
            RadTabStrip1.Tabs(5).CssClass = "TabArrowEndLight"

            DisplayErrorMessage(strValidate)
        Else
            RadTabStrip1.SelectedIndex = 5
            RadMultiPage1.SelectedIndex = 5
            RadTabStrip1.Tabs(5).CssClass = "TabArrowEndDark"
            'We want to give the user a preview of what the data will look like when imported into Advantage.
            'This step is very important because the user will usually have to map several lookup values.
            'They therefore need a chance to see the data to make sure their mappings are correct. For eg.
            'the user might have incorreclty selected Female when mapping their code of M for gender.
            PreviewDataToBeImported()
        End If

    End Sub

    Protected Sub BtnPrevToDefaultValuesClick(sender As Object, e As EventArgs) Handles btnPrevToDefaultValues.Click
        'This handles the Previous button on the Import File tab.
        'The user is taken back to the Initial Values tab and the Import File tab is disabled.
        RadTabStrip1.SelectedIndex = 4
        RadMultiPage1.SelectedIndex = 4
        RadTabStrip1.Tabs(5).CssClass = "TabArrowEndLight"

    End Sub

#End Region

    Protected Sub BuildFilesList()
        Dim filename As String
        'Dim filenames() As String
        Dim filenames As IEnumerable(Of String)
        Dim isRemoteServer As Boolean


        isRemoteServer = myFlFacade.IsRemoteServer(campusId)
        'Validation for file path

        If strSourceFilePath.IndexOfAny(CType(":", Char())) = -1 And Not isRemoteServer Then
            DisplayErrorMessage("Remote check box needs to be checked for Import Leads in the campus page.")
            Exit Sub
        End If

        'If Directory.Exists(strSourceFilePath) Then

        '    If strSourceFilePath.Substring(strSourceFilePath.Length - 1) <> "\" Then
        '        DisplayErrorMessage("Please enter a '\' at the end of the Import Path directory")
        '        Exit Sub
        '    End If
        'Else
        '    DisplayErrorMessage("Please enter the correct Import Path directory")
        '    Exit Sub
        'End If

        If Directory.Exists(strArchiveFilePath) Then

            If strArchiveFilePath.Substring(strArchiveFilePath.Length - 1) <> "\" Then
                DisplayErrorMessage("Please enter a '\' at the end of the Archive Path directory")
                Exit Sub
            End If
        Else
            DisplayErrorMessage("Please enter the correct Archive Path directory")
            Exit Sub
        End If

        Dim uriSource As New Uri(strSourceFilePath)
        Dim strSourcePath As String = uriSource.AbsolutePath

        If isRemoteServer Then
            'filenames = Directory.GetFiles(strSourceFilePath)
            Dim myImpersonate As New Impersonate
            Dim strDomain As String
            Dim strUserName As String
            strDomain = myImpersonate.GetDomainFromDomainAndUserName(strRemoteUserName)
            strUserName = myImpersonate.GetUserNameFromDomainAndUserName(strRemoteUserName)

            If myImpersonate.RemoteAccess_LogOnAndImpersonate(strDomain, strUserName, strRemotePassword) Then
                filenames = Directory.GetFiles(strSourceFilePath, "*.*", SearchOption.TopDirectoryOnly).Where(Function(s) s.EndsWith(".txt") OrElse s.EndsWith(".csv"))
                myImpersonate.RemoteAccess_UndoImpersonate()
            Else
                'Let the user know there is a problem accessing the remote server
                DisplayErrorMessage("There was a problem accessing the remote server")
                Exit Sub
            End If

        Else
            'filenames = Directory.GetFiles(strSourcePath)
            filenames = Directory.GetFiles(strSourcePath, "*.*", SearchOption.TopDirectoryOnly).Where(Function(s) s.EndsWith(".txt") OrElse s.EndsWith(".csv"))
        End If
        ''''''''''''''''''''''''''''''''''''''

        ddlFileNames.Items.Clear()
        With ddlFileNames
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

        For Each oneResult As String In filenames
            filename = Path.GetFileName(oneResult)
            ddlFileNames.Items.Add(filename)
        Next oneResult


    End Sub
    Private Sub DisplayInfoMessage(infoMessage As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, infoMessage)
    End Sub
    Private Sub DisplayErrorMessage(errorMessage As String)
        'Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

#Region "Templates"

    Private Class GridViewDdlTemplate
        Implements ITemplate

        Private dt As DataTable
        Private dtAdvFileFieldMappings As DataTable
        Private ddlId As String
        Private ddlNumber As Integer
        Private ddl As DropDownList
        'Protected Hc As DataControlFieldCell

        Public Sub New(dTable As DataTable, ddlNum As Integer, Optional ByVal dtAdvFileFldMappings As DataTable = Nothing)
            dt = dTable
            dtAdvFileFieldMappings = dtAdvFileFldMappings
            ddlNumber = ddlNum
            ddlId = "ddl" & CStr(ddlNum)
        End Sub

        Public Sub InstantiateIn(container As Control) Implements ITemplate.InstantiateIn
            ddl = New DropDownList
            AddHandler ddl.DataBinding, AddressOf Me.DdlDataBinding
            container.Controls.Add(ddl)
        End Sub

        Public Sub DdlDataBinding(sender As Object, e As EventArgs)
            Dim senderDdl As DropDownList = CType(sender, DropDownList)
            Dim filterExp As String
            Dim foundRows() As DataRow

            With senderDdl
                .ID = ddlId
                '.Width = Unit.Pixel(100)
                .Width = Unit.Pixel(135)
                .DataSource = dt
                .DataTextField = "Caption"
                .DataValueField = "FldName"
            End With

            'If the user selected a saved mapping then we should select the Advantage field based on the mapping.
            'The user might not have mapped anything for this field number.
            If Not dtAdvFileFieldMappings Is Nothing Then
                'Get the FldName, if any, that was selected for this file field.
                filterExp = "FieldNumber = " & CStr(ddlNumber)

                foundRows = dtAdvFileFieldMappings.Select(filterExp)

                If foundRows.Length > 0 Then
                    senderDdl.SelectedValue = CType(foundRows(0)("FldName"), String)
                End If

            End If

        End Sub

    End Class
    Private Class GridViewMapLookupValuesTemplate
        Implements ITemplate

        Private lbl As Label
        Private lblHeader As Label
        'Protected hc As DataControlFieldCell
        Private templateType As DataControlRowType

        Public Sub New(type As DataControlRowType)
            templateType = type
        End Sub

        Public Sub InstantiateIn(container As Control) Implements ITemplate.InstantiateIn
            Select Case templateType
                Case DataControlRowType.DataRow
                    lbl = New Label
                    lbl.Width = Unit.Pixel(200)
                    AddHandler lbl.DataBinding, AddressOf Me.LblDataBinding
                    container.Controls.Add(lbl)
                Case DataControlRowType.Header
                    lblHeader = New Label
                    lblHeader.Width = Unit.Pixel(200)
                    lblHeader.Text = "<b>" + "File Value" + "</b>"
                    container.Controls.Add(lblHeader)
            End Select

        End Sub

        Public Sub LblDataBinding(sender As Object, e As EventArgs)
            Dim senderLbl As Label = CType(sender, Label)
            senderLbl.ID = "lblFileValue"
            Dim row As GridDataItem = CType(senderLbl.NamingContainer, GridDataItem)
            senderLbl.Text = DataBinder.Eval(row.DataItem, "Value").ToString
        End Sub


    End Class
    Private Class GridViewMapLookupValuesDdlTemplate
        Implements ITemplate

        Private ddlLookupName As String
        Private templateType As DataControlRowType
        Private lblHeader As Label
        Private ddl As DropDownList
        'Protected hc As DataControlFieldCell

        Public Sub New(type As DataControlRowType, ddlName As String)
            templateType = type
            ddlLookupName = ddlName
        End Sub

        Public Sub InstantiateIn(container As Control) Implements ITemplate.InstantiateIn
            Select Case templateType
                Case DataControlRowType.DataRow
                    ddl = New DropDownList
                    'ddl.AutoPostBack = True
                    ddl.ID = "ddlAdvantageValues"
                    ddl.Width = Unit.Pixel(250)
                    AddHandler ddl.DataBinding, AddressOf Me.DdlDataBinding
                    AddHandler ddl.SelectedIndexChanged, AddressOf Me.DdlSelectedIndexChanged
                    container.Controls.Add(ddl)
                Case DataControlRowType.Header
                    lblHeader = New Label
                    lblHeader.Width = Unit.Pixel(250)
                    lblHeader.Text = "<b>" + "Advantage Values" + "</b>"
                    container.Controls.Add(lblHeader)
            End Select

        End Sub

        Public Sub DdlDataBinding(sender As Object, e As EventArgs)
            Dim senderDdl As DropDownList = CType(sender, DropDownList)
            'Dim ds As New DataSet
            Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)

            ddlList.Add(New AdvantageDDLDefinition(senderDdl, ddlLookupName, Nothing, True, True, "", "ImportLeads"))

            CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

        End Sub

        Public Sub DdlSelectedIndexChanged(sender As Object, e As EventArgs)
        End Sub
    End Class

#End Region

    Private Function ValidateTab1() As String
        Dim strMsg As String = ""
        Dim hi As GridHeaderItem
        Dim ddlId As String
        Dim blnFNameFound As Boolean = False
        Dim blnLNameFound As Boolean = False
        Dim blnAreaIdFound As Boolean = False
        Dim blnProgramIdFound As Boolean = False
        Dim blnPrgVerIdDFound As Boolean = False
        Dim mappedFields As New Hashtable
        Dim selectedField As String
        Dim numTimes As Integer
        Dim strMappedMoreThanOnce As String = ""

        'Loop through the Advantage Fields Dropdown List Boxes.
        'Check that user has mapped at least one field to First Name and Last Name
        'Build up HashTable that will be used to see if the user has mapped a field more than once.
        For i As Integer = 0 To Session("NumberOfFields") - 1
            ddlId = "ddl" & CStr(i)
            hi = CType(rgAdvFields.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)

            'Test for First Name
            If CType(hi.FindControl(ddlId), DropDownList).SelectedValue.ToUpper = "FIRSTNAME" Then
                blnFNameFound = True
            End If

            'Test for Last Name
            If CType(hi.FindControl(ddlId), DropDownList).SelectedValue.ToUpper = "LASTNAME" Then
                blnLNameFound = True
            End If

            If CType(hi.FindControl(ddlId), DropDownList).SelectedValue.ToUpper = "AREAID" Then
                blnAreaIdFound = True
            End If

            If CType(hi.FindControl(ddlId), DropDownList).SelectedValue.ToUpper = "PROGRAMID" Then
                blnProgramIdFound = True
            End If

            If CType(hi.FindControl(ddlId), DropDownList).SelectedValue.ToUpper = "PRGVERID" Then
                blnPrgVerIdDFound = True
            End If


            'Add Field Name as key to the HashTable if it does not exists. If it exists we need to update
            'the number of occurences. For this we need to ignore any DDL where the user has not selected
            'an Advantage field to map to.
            If CType(hi.FindControl(ddlId), DropDownList).SelectedValue <> "" Then
                selectedField = CType(hi.FindControl(ddlId), DropDownList).SelectedItem.Text
                If mappedFields.ContainsKey(selectedField) Then
                    'Get current value/occurences
                    numTimes = CType(mappedFields(selectedField), Integer)
                    'Increment occurences
                    numTimes += 1
                    'Update occurences
                    mappedFields(selectedField) = numTimes
                Else
                    'Add key/field with occurences set to 1
                    mappedFields.Add(selectedField, 1)
                End If
            End If


        Next

        If blnFNameFound = False Then
            strMsg = "You must map a field to First Name." & vbCrLf
        End If

        If blnLNameFound = False Then
            strMsg &= "You must map a field to Last Name." & vbCrLf
        End If

        'Check that user has not mapped more than one field to the same Advantage field
        For Each de As DictionaryEntry In mappedFields
            If de.Value > 1 Then
                strMappedMoreThanOnce &= CType((de.Key & " mapped " & CStr(de.Value) & " times." & vbCrLf), String)
            End If
        Next

        If strMappedMoreThanOnce <> "" Then
            strMsg &= "The following fields have been mapped more than once:" & vbCrLf & strMappedMoreThanOnce
        End If


        Return strMsg
    End Function

    Private Sub AddRecordToExDt(fieldName As String, recordPosition As Integer, value As String, exceptionMsg As String)
        Dim sourceExDt As New DataTable
        Dim dr As DataRow
        Dim fldCaption As String = ""

        If Session("SourceExceptionDT") Is Nothing Then
            'Create the DataTable for displaying the exceptions in the UI. 
            Dim dcField As New DataColumn("Field", GetType(String))
            Dim dcRecPos As New DataColumn("Row", GetType(Integer))
            Dim dcValue As New DataColumn("Value", GetType(String))
            Dim dcException As New DataColumn("Exception", GetType(String))

            sourceExDt.Columns.Add(dcField)
            sourceExDt.Columns.Add(dcRecPos)
            sourceExDt.Columns.Add(dcValue)
            sourceExDt.Columns.Add(dcException)
        Else
            sourceExDt = CType(Session("SourceExceptionDT"), DataTable)
        End If

        Select Case fieldName
            Case "ASSIGNEDDATE"
                fldCaption = "Assigned Date"
            Case "BIRTHDATE"
                fldCaption = "Birth Date"
            Case "EXPECTEDSTART"
                fldCaption = "Expected Start"
            Case "DATEAPPLIED"
                fldCaption = "Date Applied"
            Case "SOURCEDATE"
                fldCaption = "Source Date"
            Case "FIRSTNAME"
                fldCaption = "First Name"
            Case "LASTNAME"
                fldCaption = "Last Name"
            Case "SSN"
                fldCaption = "SSN"

        End Select

        dr = sourceExDt.NewRow
        dr("Field") = fldCaption
        dr("Row") = recordPosition
        dr("Value") = value
        dr("Exception") = exceptionMsg


        sourceExDt.Rows.Add(dr)

        Session("SourceExceptionDT") = sourceExDt

    End Sub

    'Private Function ValidateRecord(firstName As String, lastName As String, ssn As String, _
    '                                birthDate As String, phone As String, homeEamil As String) As Boolean
    '    Dim blnResult = True
    '    Dim dtm As DateTime

    '    If firstName = "" Or firstName = " " Or firstName = "&nbsp;" Then
    '        Return False
    '    End If

    '    If lastName = "" Or lastName = " " Or lastName = "&nbsp;" Then
    '        Return False
    '    End If

    '    'If the birthdate is not empty then check that we have a valid date for the birthdate field
    '    If birthDate <> "" And birthDate <> " " And birthDate <> "&nbsp;" Then
    '        Try
    '            dtm = CType(birthDate, DateTime)
    '        Catch ex As Exception
     '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            Return False
    '        End Try
    '    End If


    '    'If the ssn is not empty then check if it is 9 digits after replacing the special characters
    '    If ssn <> "" And ssn <> " " And ssn <> "&nsbp;" Then
    '        If Len(Replace(Replace(Replace(Replace(Replace(Trim(ssn), "-", ""), "/", ""), "\", ""), "(", ""), ")", "")) <> 9 Then
    '            Return False
    '        End If
    '    End If

    '    Return blnResult
    'End Function
    Private Function RecordIsInExceptions(ByVal recordPosition As Integer) As Boolean
        Dim blnResult As Boolean = False
        Dim dtEx As DataTable = CType(Session("SourceExceptionDT"), DataTable)
        Dim dr() As DataRow
        Dim filterExp As String
        filterExp = "Row = " & recordPosition.ToString()
        If Not dtEx Is Nothing Then
            'Check if the exceptions datatable has this record number
            dr = dtEx.Select(filterExp)
            If dr.Length > 0 Then
                blnResult = True
            End If
        End If
        Return blnResult
    End Function
    Private Sub AddRecordToDupDT(rowNum As Integer, firstName As String, lastName As String, middleName As String,
                                 ssn As String, birthDate As String, phone As String,
                                 homeEmail As String, address1 As String, phone2 As String, Optional ByVal removeDup As Boolean = False)
        Dim sourceDupDT As New DataTable
        Dim dr As DataRow

        If Session("SourceDuplicatesDT") Is Nothing Then
            'Create the DataTable. 
            Dim dcRemove As New DataColumn("Remove", GetType(Boolean))
            Dim dcRowNum As New DataColumn("Row", GetType(Integer))
            Dim dcName As New DataColumn("Name", GetType(String))
            Dim dcFName As New DataColumn("FirstName", GetType(String))
            Dim dcLName As New DataColumn("LastName", GetType(String))
            Dim dcMiddleName As New DataColumn("MiddleName", GetType(String))
            Dim dcSsn As New DataColumn("SSN", GetType(String))
            Dim dcAddress1 As New DataColumn("Address1", GetType(String))
            Dim dcBirthDate As New DataColumn("Birth Date", GetType(String))
            Dim dcPhone As New DataColumn("Phone", GetType(String))
            Dim dcPhone2 As New DataColumn("Phone2", GetType(String))
            Dim dcHomeEmail As New DataColumn("Home Email", GetType(String))

            sourceDupDT.Columns.Add(dcRemove)
            sourceDupDT.Columns.Add(dcRowNum)
            sourceDupDT.Columns.Add(dcName)
            sourceDupDT.Columns.Add(dcFName)
            sourceDupDT.Columns.Add(dcLName)
            sourceDupDT.Columns.Add(dcMiddleName)
            sourceDupDT.Columns.Add(dcSsn)
            sourceDupDT.Columns.Add(dcAddress1)
            sourceDupDT.Columns.Add(dcBirthDate)
            sourceDupDT.Columns.Add(dcPhone)
            sourceDupDT.Columns.Add(dcPhone2)
            sourceDupDT.Columns.Add(dcHomeEmail)
        Else
            sourceDupDT = CType(Session("SourceDuplicatesDT"), DataTable)
        End If



        dr = sourceDupDT.NewRow

        dr("Remove") = removeDup
        dr("Row") = rowNum
        dr("Name") = lastName + ", " + firstName + " " + middleName
        dr("FirstName") = firstName
        dr("LastName") = lastName
        dr("MiddleName") = middleName
        dr("SSN") = ssn
        dr("Address1") = address1
        dr("Birth Date") = birthDate
        dr("Phone") = phone
        dr("Phone2") = phone2
        dr("Home Email") = homeEmail

        sourceDupDT.Rows.Add(dr)

        Session("SourceDuplicatesDT") = sourceDupDT

    End Sub
    Private Sub BuildLookupValuesGrids()
        'Step 1: Get the list of import fields that have a DDLName associated with them. These are the fields that need lookup values
        '        Note that this list is ordered by Caption. Store them in DataTable dtLookups
        'Step 2: Loop through the file fields. If a field has been mapped then check if the Advantage field name matches any
        '        of the field names from dtLookups from step 1.
        '        If a field is a lookup then we need to add the values for that field to a DataTable and then get the distinct values.
        '        The distinct values need to be stored in a DataTable with the same name as the field name. 
        '        If a field is mapped we also need to update a field in the datatable from step 1 to indicate that the field has been selected by the user.
        'Step 3: Loop through the dtLookup
        '        For any row that has been marked as selected by the user, use the field name to get the relevant datable of the same name with the distinct values.
        '        Create a grid containing a record for each distinct value and also add a dynamic ddl with the Advantage lookups.
        '        Step 3 has been moved to its own separate sub because it will also need to be called from the Page_Init event
        Dim dtLookups As DataTable
        Dim dtPrevLookups As New DataTable
        Dim lefFacade As New LeadEnrollmentFacade
        Dim ds As New DataSet
        Dim fldName As String
        Dim ddlId As String
        Dim hi As GridHeaderItem
        Dim selectedValue As String
        Dim filterExp As String
        Dim foundRows() As DataRow
        Dim dtTemp As New DataTable
        Dim dr As DataRow
        Dim sortedDR() As DataRow
        Dim blnNeedToRebuildGrid As Boolean
        Dim blnNeedToCheckForLookupChanges As Boolean

        blnNeedToRebuildGrid = False
        blnNeedToCheckForLookupChanges = True

        'Step 1
        If Session("dtLookups") Is Nothing Then

            blnNeedToRebuildGrid = True
            blnNeedToCheckForLookupChanges = False

            dtLookups = lefFacade.GetImportLeadFieldsWithLookups()

            dtLookups.Columns.Add("Selected", GetType(String))
            dtLookups.Columns.Add("Position", GetType(Integer)) 'Zero based

            For Each drDL As DataRow In dtLookups.Rows
                drDL("Selected") = "N"
                drDL("Position") = 999 'This is used as the default value
            Next

            Session("dtLookups") = dtLookups
        End If


        'Step 2
        'Loop through the advantage ddls. If a field has been mapped then we should first check if it is a lookup field.
        'Add column to dtTemp to store the values for processing.
        'If we need to check for lookup changes by the user then we should first copy the dtLookups datatable into another
        'one so we can use that one as the previous values. The code in the For Loop below will build the latest selections
        'including positions that the user has for the lookup fields on the Map Field Names tab.
        If blnNeedToCheckForLookupChanges Then
            dtPrevLookups = DirectCast(Session("dtLookups"), DataTable).Copy
            'Reset the Initial Values
            For Each drDL As DataRow In DirectCast(Session("dtLookups"), DataTable).Rows
                drDL("Selected") = "N"
                drDL("Position") = 999 'This is used as the default value
            Next

        End If

        Dim dcValue As New DataColumn("Value", GetType(String))
        dtTemp.Columns.Add(dcValue)

        For i As Integer = 0 To Session("NumberOfFields") - 1
            ddlId = "ddl" & CStr(i)
            hi = CType(rgAdvFields.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
            selectedValue = CType(hi.FindControl(ddlId), DropDownList).SelectedValue

            If selectedValue <> "" And selectedValue <> " " And selectedValue <> "&nbsp;" Then
                'Search dtLookups for this value in the FldName
                filterExp = "FldName = '" & selectedValue & "'"

                ' Use the Select method to find all rows matching the filter.
                foundRows = DirectCast(Session("dtLookups"), DataTable).Select(filterExp)

                'A match means that we have a lookup field. We need to get all the values for this field.
                If foundRows.Length > 0 Then
                    'Clear dtTemp since we will be using it to build up the distinct values for all the fields
                    'that are lookup fields.
                    dtTemp.Rows.Clear()

                    'Mark the Selected field in the dtLookup datatable. We will need this later on since it is
                    'unlikely that the user will map all the lookup fields. 
                    'We also need the position so later on we can tell if the user has made any changes to the
                    'lookup fields.
                    foundRows(0)("Selected") = "Y"
                    foundRows(0)("Position") = i

                    For Each gdi As DataRow In DirectCast(Session("SourceDT"), DataTable).Rows
                        'Get the name of the field from the SourceDT in session. It is better to do this because
                        'we cannot use the index on the GridDataItem object reliably.
                        fldName = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName

                        dr = dtTemp.NewRow

                        If gdi(fldName) <> "" And gdi(fldName) <> " " And gdi(fldName) <> "&nbsp;" Then
                            dr("Value") = gdi(fldName)
                        Else
                            'Store a special string to represent an empty field.
                            dr("Value") = "No Value Specified"
                        End If

                        dtTemp.Rows.Add(dr)
                    Next

                    If dtTemp.Rows.Count > 0 Then
                        'Get the sorted values
                        sortedDR = dtTemp.Select("", "Value")

                        'Create a datatable with the same name as the Advantage field name and add the sorted records to it.
                        'Check before adding each record so we end up with the distinct values.
                        Dim dt As DataTable = ds.Tables.Add(selectedValue)

                        dt.Columns.Add("Value", GetType(String))

                        For Each drSort As DataRow In sortedDR
                            'If the value does not already exists add the row
                            If Not ValueExistsInDataTable(dt, drSort("Value")) Then
                                dt.ImportRow(drSort)
                            End If
                        Next

                    End If
                End If

            End If

        Next

        'Add the lookup datatable to the dataset
        Dim dtDSLookup As DataTable = ds.Tables.Add("LookupTable")
        dtDSLookup = DirectCast(Session("dtLookups"), DataTable).Copy

        'Save the dataset to session
        Session("LookupSources") = ds

        If blnNeedToRebuildGrid Then
            BuildStructureForLookupValuesGrid()
        End If

        If HasUserMadeChangesToLookupFields(dtPrevLookups, DirectCast(Session("dtLookups"), DataTable)) Then
            'If the user has made changes we want to keep the initial grid so we the viewstate will restore the values the user
            'had selected. However, we will add any lookups that were not initially selected by the user and remove any that
            'the user is no longer mapping to.
            ModifyStructureForLookupValuesGrid(dtPrevLookups, DirectCast(Session("dtLookups"), DataTable))
        End If

    End Sub

    Private Sub BuildStructureForLookupValuesGrid()
        Dim dtLookups As DataTable
        Dim filterExp As String
        Dim selectedDR() As DataRow

        'Get the lookup fields that have been selected by the user
        dtLookups = Session("dtLookups")

        filterExp = "Selected = 'Y'"
        selectedDR = dtLookups.Select(filterExp, "Caption")

        If tblMapLookupValues.Rows.Count > 0 Then
            'Clear any existing rows or we will get the error that there are controls with the same name/id
            tblMapLookupValues.Rows.Clear()
        End If

        For Each dr As DataRow In selectedDR
            CreateGridForLookupField(dr)
        Next

        'If the user has selected a saved mapping then we need to select the Advantage value for each file value
        'for each lookup field based on the saved mapping.
        Try
            If CInt(Session("ddlSavedMappingsSelectedIndex")) > 0 Then
                SelectAdvantageValuesInLookupGrid()
            End If
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Do nothing. We know it will error out depending on when it is called by the Page_Init event.
        End Try


    End Sub

    Private Sub CreateGridForLookupField(dr As DataRow)
        Dim ds As DataSet

        ds = CType(Session("LookupSources"), DataSet)

        'Create row and cell for the tblMapLookupValues table
        Dim tRow As New TableRow()
        Dim tCell As New TableCell()

        tRow.ID = "tRow" + dr("FldName")
        tCell.ID = "tCell" + dr("FldName")
        tCell.HorizontalAlign = HorizontalAlign.Left

        'Create a RadPanelBar
        Dim rpb As New RadPanelBar
        With rpb
            .ID = "rpb" + dr("FldName")
            .Skin = "Web20"
            .Width = Unit.Percentage(100)
            '.ExpandMode = PanelBarExpandMode.MultipleExpandedItems
        End With

        'Create RadPanelItem
        Dim rpi As New RadPanelItem
        With rpi
            .Expanded = False
            .Text = dr("Caption")
        End With

        'Create and add a RadGrid to the controls collection of the RadPanelItem
        Dim rg As New RadGrid
        With rg
            .ID = "rgLookup" + dr("FldName")
            .AutoGenerateColumns = False
            .Skin = "Web20"
        End With

        'Set the datasource of the RadGrid to the relevant DataTable in the DataSet.
        rg.DataSource = ds.Tables(dr("FldName"))

        'Add the first column which will contain the values from the file
        Dim templateColumn As GridTemplateColumn = New GridTemplateColumn
        templateColumn.ItemTemplate = New GridViewMapLookupValuesTemplate(DataControlRowType.DataRow)
        templateColumn.HeaderTemplate = New GridViewMapLookupValuesTemplate(DataControlRowType.Header)
        rg.MasterTableView.Columns.AddAt(0, templateColumn)

        'Add the second column which will contain the DDLs with the Advantage values for the DDL
        Dim templateColumn2 As GridTemplateColumn = New GridTemplateColumn
        templateColumn2.ItemTemplate = New GridViewMapLookupValuesDdlTemplate(DataControlRowType.DataRow, dr("DDLName"))
        templateColumn2.HeaderTemplate = New GridViewMapLookupValuesDdlTemplate(DataControlRowType.Header, dr("DDLName"))
        rg.MasterTableView.Columns.AddAt(1, templateColumn2)

        rg.DataBind()

        rpi.Controls.Add(rg)

        'Add the RadPanelItem to the items collection of the RadPanelBar
        rpb.Items.Add(rpi)

        'Add the RadPanelBar to the controls collection of the cell
        tCell.Controls.Add(rpb)

        'Add a cell to the row
        tRow.Cells.Add(tCell)

        'Add the row to the table
        tblMapLookupValues.Rows.Add(tRow)

        'Add a couple of empty rows to get some space between each panelbar
        Dim tEmptyRow As New TableRow()
        Dim tEmptyCell As New TableCell()

        tEmptyRow.Cells.Add(tEmptyCell)
        tblMapLookupValues.Rows.Add(tEmptyRow)

        Dim tEmptyRow2 As New TableRow()
        Dim tEmptyCell2 As New TableCell()

        tEmptyRow2.Cells.Add(tEmptyCell2)
        tblMapLookupValues.Rows.Add(tEmptyRow2)
    End Sub

    Private Sub ModifyStructureForLookupValuesGrid(dtPrevLookups As DataTable, dtNewLookups As DataTable)
        'Loop through the records in the previous DT. Remember that the records are in the same order in both DTs.
        'Compare the selected and position values of each record. If there are any differences then it means that the
        'uer has made some changes.
        'SCENARIO 1: The previous record had selected set to Y and the new record has selected set to N. 
        'For example, the user incorrectly mapped field number 5 on the first tab to the State field in Advantage.
        'After getting to the Map Field Detail tab the user realizes the mistake and returns to the first tab and change
        'the field back to Select.
        'SCENARIO 2: The previous record had selected set to N and the new reocrd has selected set to Y.
        'For example, the user did not map a field to the Gender field which is actually field 7 in the file.
        'After getting to the Map Field Detail tab the user realizes the mistake and returns to the first tab
        'and map field 7 to the Gender field.
        'SCENARIO 3: The previous and new record have selected set to Y but they have different positions.
        'For example, the user could have incorrectly mappted field number 6 to Gender when it should have been field 7.
        Dim drPrev As DataRow
        Dim drNew As DataRow

        For i As Integer = 0 To dtPrevLookups.Rows.Count - 1
            drPrev = dtPrevLookups.Rows(i)
            drNew = dtNewLookups.Rows(i)

            'For scenario 1 we need to remove the row relating to the field.
            'Recall that name of a row in the tblLookupValues table is "tRow" + FldName
            If drPrev("Selected") = "Y" And drNew("Selected") = "N" Then
                Dim tRow As TableRow
                tRow = tblMapLookupValues.FindControl("tRow" + drPrev("FldName"))
                tblMapLookupValues.Rows.Remove(tRow)
            End If

            'For scenario 2 we need to add a new row for the field.
            If drPrev("Selected") = "N" And drNew("Selected") = "Y" Then
                CreateGridForLookupField(drPrev)
            End If

            'For scenario 3 we need to first remove the row relating to the field and then add a new row for the field.
            'It is really a combination of scenarios 1 and 2.
            If (drPrev("Selected") = "Y" And drNew("Selected") = "Y") And (drPrev("Position") <> drNew("Position")) Then
                'Remove the existing row
                Dim tRow As TableRow
                tRow = tblMapLookupValues.FindControl("tRow" + drPrev("FldName"))
                tblMapLookupValues.Rows.Remove(tRow)

                'Add a new row
                CreateGridForLookupField(drPrev)

            End If


        Next

    End Sub

    Private Sub SelectAdvantageValuesInLookupGrid()
        Dim myLEFacade As New LeadEnrollmentFacade
        Dim dt As DataTable
        'Dim dtCopy As New DataTable
        Dim rpb As RadPanelBar
        Dim rpi As RadPanelItem
        Dim rg As RadGrid
        'Dim filterExp As String
        'Dim foundRows() As DataRow

        If Not (ddlSavedMappings.SelectedItem Is Nothing) Then

            dt = myLEFacade.GetLookupFldValsMappings(ddlSavedMappings.SelectedItem.Value)
            'dtCopy = dt.Copy
            If Not dt Is Nothing Then
                For Each dr As DataRow In dt.Rows
                    'For each lookup field value mapping that has been saved by the user we will loop through the controls of the
                    'tblLookupMapValues table and find all the radpanels.
                    rpb = tblMapLookupValues.FindControl(CType(("rpb" + dr("FldName")), String))
                    rpi = rpb.Items.FindItemByText(dr("Caption"))
                    rg = rpi.FindControl("rgLookup" + dr("FldName"))

                    For Each gi As GridDataItem In rg.Items
                        Dim lbl As Label = gi.FindControl("lblFileValue")
                        Dim ddl As DropDownList = gi.FindControl("ddlAdvantageValues")

                        If lbl.Text = dr("FileValue") Then
                            ddl.SelectedValue = dr("AdvValue").ToString
                        End If
                    Next
                Next
            End If
        End If
    End Sub

    Private Function ValueExistsInDataTable(dt As DataTable, value As String) As Boolean
        Dim blnResult = False

        If dt.Rows.Count = 0 Then
            Return False
        End If

        For Each dr As DataRow In dt.Rows
            If dr("Value") = value Then
                blnResult = True
                Return True
            End If
        Next

        Return blnResult
    End Function

    Private Function HasUserMadeChangesToLookupFields(prevLookupValuesDT As DataTable, newLookupValuesDT As DataTable) As Boolean
        'Loop through the records in the previous DT. Remember that the records are in the same order in both DTs.
        'Compare the selected and position values of each record. If there are any differences then it means that the
        'uer has made some changes.
        Dim drPrev As DataRow
        Dim drNew As DataRow

        For i As Integer = 0 To prevLookupValuesDT.Rows.Count - 1
            drPrev = prevLookupValuesDT.Rows(i)
            drNew = newLookupValuesDT.Rows(i)

            If (drPrev("Selected") <> drNew("Selected")) Or (drPrev("Position") <> drNew("Position")) Then
                Return True
            End If

        Next

        Return False
    End Function

    Private Sub ImportFile()
        Dim dtMappedValues As DataTable
        Dim dtLeadsTable As DataTable
        Dim leFacade As New LeadEnrollmentFacade
        Dim errMsg As String
        Dim msgSummary As String
        Dim dtEx As DataTable
        Dim dtDup As DataTable
        'Dim strMoveFile As String
        Dim dtFileAdvFieldMappings As DataTable
        Dim rowsFoundDups() As DataRow
        Dim filterExpDups As String

        'Get the DataTable representing the adLeads table
        dtLeadsTable = BuildLeadsDT()

        'Get the DataTable with the mapping of the lookup values
        dtMappedValues = BuildMapLookupValuesDT()


        'Add the records to the dtLeadsTable. Note that the dtLeadsTable is passed ByRef.
        AddRecordsToLeadsDt(dtLeadsTable, dtMappedValues)

        'Create the datatable that will store the records for the adImportLeadsAdvFldMappings table
        'which stores each field in the file and the Advantage field, if any, that it is mapped to.
        'Since this datatable is not something that we need to show in the UI we can build the structure
        'and populate it at the same time.
        dtFileAdvFieldMappings = CreateFileAdvFieldMappings()

        'Pass the DataTable off to be processed
        errMsg = leFacade.ProcessImportLeadFile(dtLeadsTable, txtMappingName.Text, AdvantageSession.UserState.UserName, dtFileAdvFieldMappings, dtMappedValues)

        If errMsg <> "" Then
            DisplayErrorMessage("Error:" + errMsg)
        Else
            'The import was successful so we can display the summary to the user.
            'Since we are using a transaction to rollback in case of any errors we can simply use
            'the number of records in the dtLeadsTable as the number of records inserted successfully.
            msgSummary = "Number of Leads Added: " + CStr(dtLeadsTable.Rows.Count) + vbCrLf

            Try
                dtEx = CType(Session("SourceExceptionDT"), DataTable)
                If (Not IsNothing(dtEx)) Then
                    msgSummary += "Number of Exceptions: " + CStr(dtEx.Rows.Count) + vbCrLf
                Else
                    msgSummary += "Number of Exceptions: 0" + vbCrLf
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                msgSummary += "Number of Exceptions: 0" + vbCrLf
            End Try

            Try
                'Note that the number of rows skipped as duplicates need to exclude those that won't marked as removed
                dtDup = CType(Session("SourceDuplicatesDT"), DataTable)
                If (Not IsNothing(dtDup)) Then
                    filterExpDups = "Remove = true"
                    rowsFoundDups = dtDup.Select(filterExpDups)
                    msgSummary += "Duplicates Skipped: " + CStr(rowsFoundDups.Length) + vbCrLf
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                msgSummary += "Duplicates Skipped: 0"
            End Try

            DisplayInfoMessage(msgSummary)


            'Send the user back to the Map Field Names tab
            RadTabStrip1.SelectedIndex = 0
            RadTabStrip1.SelectedTab.Enabled = True
            RadMultiPage1.SelectedIndex = 0

            'Move the processed file to the archived folder
            Dim strMsg As String = MoveFileFromInToExceptionFolder(strSourceFilePath, strArchiveFilePath, ddlFileNames.SelectedItem.Text)

            If Not String.IsNullOrEmpty(strMsg) Then
                DisplayErrorMessage(strMsg)
            End If


            'Rebuild the fields ddl
            BuildFilesList()

            'Clear out the session variables
            Session("dtLookups") = Nothing
            Session("LookupSources") = Nothing
            Session("SourceDT") = Nothing
            Session("NumberOfFields") = Nothing
            Session("SourceExceptionDT") = Nothing
            Session("AdvFieldsDT") = Nothing
            Session("Duplicates") = Nothing
            Session("Exceptions") = Nothing
            Session("SourceDuplicatesDT") = Nothing
            Session("dtLeadsTableForPreview") = Nothing

            'Hide the Datagrids
            rgAdvFields.Visible = False
            lblMap.Visible = False
            rgFile.DataSource = Nothing
            rgFile.DataBind()
            rgFile.Visible = False

            'Select the first items in the DDLS on the Initial Values tab
            ddlDefaultCampus.SelectedIndex = 0
            ddlDefaultAdmissionsRep.SelectedIndex = 0
            ddlDefaultLeadStatus.SelectedIndex = 0

            EnableMapFieldsAndExceptionsTabs()

        End If

    End Sub

    Private Sub EnableMapFieldsAndExceptionsTabs()
        RadTabStrip1.Tabs(0).Enabled = True
        RadTabStrip1.Tabs(1).Enabled = True
        RadTabStrip1.Tabs(2).Enabled = False
        RadTabStrip1.Tabs(3).Enabled = False
        RadTabStrip1.Tabs(4).Enabled = False
        RadTabStrip1.Tabs(5).Enabled = False
    End Sub

    Private Sub PreviewDataToBeImported()
        Dim ddlId As String
        Dim hi As GridHeaderItem
        Dim advFldName As String
        Dim fileFldName As String
        Dim fileValue As String
        Dim dtMappedValues As DataTable
        Dim mappedRows() As DataRow
        Dim filterExp As String
        Dim dtLeadsTable As DataTable
        Dim dtLeadsTableForPreview As DataTable
        Dim dtLeadsTableColsToBeRemoved As New DataTable
        'Dim leFacade As New LeadEnrollmentFacade
        Dim recNumber As Integer
        Dim dc As DataColumn
        Dim blnFound As Boolean
        Dim strValidate As String

        'When we get to this tab there are two validations needed:
        '(1) Any lookup values must have been mapped
        '(2) All Initial Values must have been mapped
        strValidate = ""

        If Not ValidateMapLookupValues() Then
            strValidate = "You must map all of the lookup fields." + vbCrLf
        End If

        If Not ValidateDefaultValues() Then
            strValidate += "You must specify a value for each default field."
        End If

        If strValidate <> "" Then
            DisplayErrorMessage(strValidate)
            rgPreview.DataSource = Nothing
            rgPreview.DataBind()
            btnImport.Visible = False

            'If there is a problem with the lookup fields then send the user to that tab.
            'If there is a problem with the Initial Values then send the user to that tab
            'when there is no problem with the lookup fields.
            If strValidate.Contains("lookup") Then
                RadTabStrip1.SelectedIndex = 3
                RadTabStrip1.SelectedTab.Enabled = True
                RadMultiPage1.SelectedIndex = 3
            ElseIf strValidate.Contains("default") Then
                RadTabStrip1.SelectedIndex = 4
                RadTabStrip1.SelectedTab.Enabled = True
                RadMultiPage1.SelectedIndex = 4
            End If

        Else
            'Add a field to dtLeadsTableColsToBeRemoved to store the index to be removed
            Dim dcColName As New DataColumn("ColName", GetType(String))
            dtLeadsTableColsToBeRemoved.Columns.Add(dcColName)

            'Get the DataTable representing the adLeads table
            dtLeadsTable = BuildLeadsDT()

            'Get the DataTable with the mapping of the lookup values
            dtMappedValues = BuildMapLookupValuesDT()

            Session("dtMappedValues") = dtMappedValues

            'Loop through rgFile which has the file records. 
            'We are going to use each record to create the records for the datatable with the adLeads structure.
            'We will get the values for the datatable by looking at the mapped fields.
            'The names of the fields in the datatable correspond to the field names of the mapped fields.
            'If a mapped field is a lookup we will get the guid from the datatable with the mapped values
            recNumber = 1

            For Each gi As DataRow In DirectCast(Session("SourceDT"), DataTable).Rows 'TA9647 OLD: rgFile.Items
                'We need to skip any record with an exception or a record that is a duplicate.
                If Not RecordIsExceptionOrADuplicateToRemoved(recNumber) Then
                    'Create a new row to represent this record in the leads datatable
                    Dim dr As DataRow = dtLeadsTable.NewRow

                    For i As Integer = 0 To Session("NumberOfFields") - 1
                        ddlId = "ddl" & CStr(i)
                        hi = CType(rgAdvFields.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
                        advFldName = CType(hi.FindControl(ddlId), DropDownList).SelectedValue

                        'Skip unmapped fields
                        If advFldName <> "" Then
                            'Get the file value for this field
                            'Get the name of the file field from the SourceDT in session. It is better to do this because
                            'we cannot use the index on the GridDataItem object reliably.
                            fileFldName = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                            fileValue = gi(fileFldName) 'TA9647 OLD: .Text

                            'Check if this is a lookup field. The user might map an empty value in a field to an Advantage field.
                            'For eg. the user might want to map any record with a missing admissions rep to one rep that
                            'is used for that purpose.
                            If fileValue <> "" And fileValue <> " " And fileValue <> "&nbsp;" Then
                                'filterExp = "FldName = '" + advFldName + "' and FileValue= '" + fileValue + "'"
                                filterExp = "FldName = '" + advFldName + "' and FileValue= '" + Replace(fileValue, "'", "''") + "'"
                            Else
                                filterExp = "FldName = '" + advFldName + "' and FileValue= 'No Value Specified'"
                            End If

                            mappedRows = dtMappedValues.Select(filterExp)
                            If mappedRows.Count > 0 Then
                                'Here we are actually showing the caption to the user.
                                'The actual import will use the mapped value.
                                fileValue = mappedRows(0)("AdvMappedCaption")
                                If fileValue = "Select" Then
                                    fileValue = ""
                                End If
                            End If

                            'We have to take care of any special formatting used on SSN and Phone fields.
                            If advFldName.ToUpper = "SSN" And fileValue <> "" And fileValue <> " " And fileValue <> "&nbsp;" Then
                                fileValue = fileValue.Replace("-", "").Replace("/", "").Replace("\", "")
                            End If

                            If advFldName.ToUpper = "PHONE" And fileValue <> "" And fileValue <> " " And fileValue <> "&nbsp;" Then
                                'In the case of the phone field if we replace the special characters and we have a length of 10 we want
                                'to store the number without the special characters as a regular US number. Otherwise, we will store it
                                'the way it is and marked ForeignPhone as true (1).
                                If Len(fileValue.Replace("-", "").Replace("/", "").Replace("\", "").Replace("(", "").Replace(")", "")) = 10 Then
                                    fileValue = fileValue.Replace("-", "").Replace("/", "").Replace("\", "").Replace("(", "").Replace(")", "")
                                End If
                            End If



                            'If (advFldName.ToUpper = "COUNTRY" Or Right(advFldName.ToUpper, 2) = "ID") AndAlso Not CommonWebUtilities.IsValidGuid(fileValue) Then
                            '    fileValue = "00000000-0000-0000-0000-000000000000"
                            'End If

                            'Set the value for this field in the leads datatable. If we are dealing with a mapped field that the user
                            'has not specified a value for we should set it to null. Otherwise, we will end up with "&nbsp;" for the
                            'value in the database.
                            If fileValue = "" Or fileValue = "&nbsp;" Or fileValue = " " Or fileValue = "00000000-0000-0000-0000-000000000000" Then
                                dr(advFldName) = DBNull.Value
                            Else
                                dr(advFldName) = Trim(fileValue)
                            End If


                        End If

                    Next

                    'Before we add the row to the datatable we need to take care of special fields such as the LeadId, CreatedDate,
                    'ModUser, ModDate, Foreign Phone and Foreign Zip
                    Dim leadId As Guid = Guid.NewGuid

                    dr("LeadId") = leadId
                    dr("ModUser") = Session("UserName")
                    dr("ModDate") = Date.Now
                    dr("CreatedDate") = Date.Now

                    If dr("AssignedDate") Is DBNull.Value Then
                        dr("AssignedDate") = Date.Now
                    End If

                    If dr("CampusId") Is DBNull.Value Then
                        dr("CampusId") = ddlDefaultCampus.SelectedItem.Text
                    End If

                    If dr("AdmissionsRep") Is DBNull.Value Then
                        dr("AdmissionsRep") = ddlDefaultAdmissionsRep.SelectedItem.Text
                    End If

                    If dr("LeadStatus") Is DBNull.Value Then
                        dr("LeadStatus") = ddlDefaultLeadStatus.SelectedItem.Text
                    End If

                    If dr("Phone") Is DBNull.Value Then
                        dr("ForeignPhone") = "0"
                    ElseIf Len(dr("Phone")) = 10 Or dr("Phone") = "" Then
                        dr("ForeignPhone") = "0"
                    Else
                        dr("ForeignPhone") = 1
                    End If

                    If dr("Phone2") Is DBNull.Value Then
                        dr("ForeignPhone2") = "0"
                    ElseIf Len(dr("Phone2")) = 10 Or dr("Phone2") = "" Then
                        dr("ForeignPhone2") = "0"
                    Else
                        dr("ForeignPhone2") = 1
                    End If

                    If dr("ForeignZip") Is DBNull.Value Then
                        dr("ForeignZip") = "0"
                    ElseIf Len(dr("ForeignZip")) = 5 Or dr("ForeignZip") = "" Then
                        dr("ForeignZip") = "0"
                    Else
                        dr("ForeignZip") = "1"
                    End If

                    dr("DefaultPhone") = "1"

                    'Add the record to the datatable
                    dtLeadsTable.Rows.Add(dr)
                End If

                ''Increment the record counter
                recNumber += 1
            Next

            'At this point we have the grid ready but we don't want the user to see any fields that they did not map.
            'As it is right now they would end up seeing the Advantage internal fields such as LeadId that has no meaning to them.
            'Check each datacolumn to see if the user has mapped a field to it. If not, remove it from the datatable.
            'Use a copy of the dtleadstable as we will need the full version of that table to do the actual import
            dtLeadsTableForPreview = dtLeadsTable.Copy

            For Each dc In dtLeadsTableForPreview.Columns
                blnFound = False

                For i As Integer = 0 To Session("NumberOfFields") - 1
                    ddlId = "ddl" & CStr(i)
                    hi = CType(rgAdvFields.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
                    advFldName = CType(hi.FindControl(ddlId), DropDownList).SelectedValue

                    If dc.ColumnName.ToUpper = advFldName.ToUpper Then
                        blnFound = True
                    End If
                Next

                If blnFound = False Then
                    'We cannot remove the column while enumerating the collection so we have to store
                    'the column index in another datatable that we can use to loop through and remove
                    'the column then.
                    Dim drColName As DataRow = dtLeadsTableColsToBeRemoved.NewRow
                    drColName("ColName") = dc.ColumnName
                    dtLeadsTableColsToBeRemoved.Rows.Add(drColName)
                End If

            Next

            For Each drCN As DataRow In dtLeadsTableColsToBeRemoved.Rows
                dtLeadsTableForPreview.Columns.Remove(drCN("ColName"))
            Next

            dtLeadsTableForPreview.AcceptChanges()

            'Display the DataTable in a RadGrad to the user
            rgPreview.DataSource = dtLeadsTableForPreview
            rgPreview.DataBind()

            'Store the datatable in session so we can reuse it in the OnNeedDataSource sub
            Session("dtLeadsTableForPreview") = dtLeadsTableForPreview

            'Make sure the import button is visibe
            btnImport.Visible = True

            'Show the label with the number of records to be imported
            lblNumOfRecords.Text = "Number of records to be imported: " + CStr(dtLeadsTableForPreview.Rows.Count)

            'If the user has selected a saved mapping on the first tab then we should full out the txtMappingName
            'textbox with it.
            If Session("ddlSavedMappingSelectedText") <> "Select" Then
                txtMappingName.Text = CType(Session("ddlSavedMappingSelectedText"), String)
            End If
        End If

    End Sub

    Private Function RecordIsExceptionOrADuplicateToRemoved(recordNumber As Integer) As Boolean
        Dim blnResult As Boolean = False
        Dim dtEx As DataTable
        Dim dtDup As DataTable
        Dim dr() As DataRow
        Dim filterExp As String
        Dim filterExpDups As String

        dtEx = CType(Session("SourceExceptionDT"), DataTable)
        dtDup = CType(Session("SourceDuplicatesDT"), DataTable)
        filterExp = "Row = " & recordNumber

        If Not dtEx Is Nothing Then

            'Check if the exceptions datatable has this record number
            Try
                dr = dtEx.Select(filterExp)

                If dr.Length > 0 Then
                    Return True
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Do nothing
            End Try
        End If

        'Check if the duplicates datatable has this record number and it is marked to be Removed
        filterExpDups = "Row = " & recordNumber & " and Remove = true"
        If Not dtDup Is Nothing Then

            Try
                dr = dtDup.Select(filterExpDups)
                If dr.Length > 0 Then
                    Return True
                End If
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                'Do nothing
            End Try
        End If


        Return blnResult
    End Function

    Private Function BuildLeadsDT() As DataTable
        'This DataTable has the same structure as the adLeads table.
        Dim dt As New DataTable

        Dim dcLeadId As New DataColumn("LeadId", GetType(String))
        Dim dcFirstName As New DataColumn("FirstName", GetType(String))
        Dim dcLastName As New DataColumn("LastName", GetType(String))
        Dim dcMiddleName As New DataColumn("MiddleName", GetType(String))
        Dim dcSsn As New DataColumn("SSN", GetType(String))
        Dim dcModUser As New DataColumn("ModUser", GetType(String))
        Dim dcModDate As New DataColumn("ModDate", GetType(String))
        Dim dcPhone As New DataColumn("Phone", GetType(String))
        Dim dcHomeEmail As New DataColumn("HomeEmail", GetType(String))
        Dim dcAddress1 As New DataColumn("Address1", GetType(String))
        Dim dcAddress2 As New DataColumn("Address2", GetType(String))
        Dim dcCity As New DataColumn("City", GetType(String))
        Dim dcStateId As New DataColumn("StateId", GetType(String))
        Dim dcZip As New DataColumn("Zip", GetType(String))
        Dim dcLeadStatus As New DataColumn("LeadStatus", GetType(String))
        Dim dcWorkEmail As New DataColumn("WorkEmail", GetType(String))
        Dim dcAddressType As New DataColumn("AddressType", GetType(String))
        Dim dcPrefix As New DataColumn("Prefix", GetType(String))
        Dim dcSuffix As New DataColumn("Suffix", GetType(String))
        Dim dcBirthDate As New DataColumn("BirthDate", GetType(String))
        Dim dcSponsor As New DataColumn("Sponsor", GetType(String))
        Dim dcAdmissionsRep As New DataColumn("AdmissionsRep", GetType(String))
        Dim dcAssignedDate As New DataColumn("AssignedDate", GetType(String))
        Dim dcGender As New DataColumn("Gender", GetType(String))
        Dim dcRace As New DataColumn("Race", GetType(String))
        Dim dcMaritalStatus As New DataColumn("MaritalStatus", GetType(String))
        Dim dcFamilyIncome As New DataColumn("FamilyIncome", GetType(String))
        Dim dcChildren As New DataColumn("Children", GetType(String))
        Dim dcPhoneType As New DataColumn("PhoneType", GetType(String))
        Dim dcPhoneStatus As New DataColumn("PhoneStatus", GetType(String))
        Dim dcSourceCateoryId As New DataColumn("SourceCategoryId", GetType(String))
        Dim dcSourceTypeId As New DataColumn("SourceTypeID", GetType(String))
        Dim dcSourceDate As New DataColumn("SourceDate", GetType(String))
        Dim dcAreadID As New DataColumn("AreaID", GetType(String))
        Dim dcProgramID As New DataColumn("ProgramID", GetType(String))
        Dim dcExpectedStart As New DataColumn("ExpectedStart", GetType(String))
        Dim dcShiftId As New DataColumn("ShiftId", GetType(String))
        Dim dcNationality As New DataColumn("Nationality", GetType(String))
        Dim dcCitizen As New DataColumn("Citizen", GetType(String))
        Dim dcDrivLicStateId As New DataColumn("DrivLicStateId", GetType(String))
        Dim dcDrivLicNumber As New DataColumn("DrivLicNumber", GetType(String))
        Dim dcAlienNumber As New DataColumn("AlienNumber", GetType(String))
        Dim dcComments As New DataColumn("Comments", GetType(String))
        Dim dcSourceAdvertisement As New DataColumn("SourceAdvertisement", GetType(String))
        Dim dcCampusId As New DataColumn("CampusId", GetType(String))
        Dim dcPrgVerId As New DataColumn("PrgVerId", GetType(String))
        Dim dcCountry As New DataColumn("Country", GetType(String))
        Dim dcCounty As New DataColumn("County", GetType(String))
        Dim dcPreviousEducation As New DataColumn("PreviousEducation", GetType(String))
        Dim dcAddressStatus As New DataColumn("AddressStatus", GetType(String))
        Dim dcCreatedDate As New DataColumn("CreatedDate", GetType(String))
        Dim dcForeignPhone As New DataColumn("ForeignPhone", GetType(Integer))
        Dim dcForeignZip As New DataColumn("ForeignZip", GetType(Integer))
        Dim dcLeadGrpId As New DataColumn("LeadGrpId", GetType(String))
        Dim dcDependencyTypeId As New DataColumn("DependencyTypeId", GetType(String))
        Dim dcDegCertSeekingId As New DataColumn("DegCertSeekingId", GetType(String))
        Dim dcGeographicTypeId As New DataColumn("GeographicTypeId", GetType(String))
        Dim dcHousingId As New DataColumn("HousingId", GetType(String))
        Dim dcAdminCriteriaId As New DataColumn("AdminCriteriaId", GetType(String))
        Dim dcDateApplied As New DataColumn("DateApplied", GetType(String))
        Dim dcAdvertisementNote As New DataColumn("AdvertisementNote", GetType(String))
        Dim dcPhone2 As New DataColumn("Phone2", GetType(String))
        Dim dcPhoneType2 As New DataColumn("PhoneType2", GetType(String))
        Dim dcPhoneStatus2 As New DataColumn("PhoneStatus2", GetType(String))
        Dim dcForeignPhone2 As New DataColumn("ForeignPhone2", GetType(Integer))
        Dim dcDefaultPhone As New DataColumn("DefaultPhone", GetType(Integer))

        dt.Columns.Add(dcLeadId)
        dt.Columns.Add(dcFirstName)
        dt.Columns.Add(dcLastName)
        dt.Columns.Add(dcMiddleName)
        dt.Columns.Add(dcSsn)
        dt.Columns.Add(dcModUser)
        dt.Columns.Add(dcModDate)
        dt.Columns.Add(dcPhone)
        dt.Columns.Add(dcHomeEmail)
        dt.Columns.Add(dcAddress1)
        dt.Columns.Add(dcAddress2)
        dt.Columns.Add(dcCity)
        dt.Columns.Add(dcStateId)
        dt.Columns.Add(dcZip)
        dt.Columns.Add(dcLeadStatus)
        dt.Columns.Add(dcWorkEmail)
        dt.Columns.Add(dcAddressType)
        dt.Columns.Add(dcPrefix)
        dt.Columns.Add(dcSuffix)
        dt.Columns.Add(dcBirthDate)
        dt.Columns.Add(dcSponsor)
        dt.Columns.Add(dcAdmissionsRep)
        dt.Columns.Add(dcAssignedDate)
        dt.Columns.Add(dcGender)
        dt.Columns.Add(dcRace)
        dt.Columns.Add(dcMaritalStatus)
        dt.Columns.Add(dcFamilyIncome)
        dt.Columns.Add(dcChildren)
        dt.Columns.Add(dcPhoneType)
        dt.Columns.Add(dcPhoneStatus)
        dt.Columns.Add(dcSourceCateoryId)
        dt.Columns.Add(dcSourceTypeId)
        dt.Columns.Add(dcSourceDate)
        dt.Columns.Add(dcAreadID)
        dt.Columns.Add(dcProgramID)
        dt.Columns.Add(dcExpectedStart)
        dt.Columns.Add(dcShiftId)
        dt.Columns.Add(dcNationality)
        dt.Columns.Add(dcCitizen)
        dt.Columns.Add(dcDrivLicStateId)
        dt.Columns.Add(dcDrivLicNumber)
        dt.Columns.Add(dcAlienNumber)
        dt.Columns.Add(dcComments)
        dt.Columns.Add(dcSourceAdvertisement)
        dt.Columns.Add(dcCampusId)
        dt.Columns.Add(dcPrgVerId)
        dt.Columns.Add(dcCountry)
        dt.Columns.Add(dcCounty)
        dt.Columns.Add(dcPreviousEducation)
        dt.Columns.Add(dcAddressStatus)
        dt.Columns.Add(dcCreatedDate)
        dt.Columns.Add(dcForeignPhone)
        dt.Columns.Add(dcForeignZip)
        dt.Columns.Add(dcLeadGrpId)
        dt.Columns.Add(dcDependencyTypeId)
        dt.Columns.Add(dcDegCertSeekingId)
        dt.Columns.Add(dcGeographicTypeId)
        dt.Columns.Add(dcHousingId)
        dt.Columns.Add(dcAdminCriteriaId)
        dt.Columns.Add(dcDateApplied)
        dt.Columns.Add(dcAdvertisementNote)
        dt.Columns.Add(dcPhone2)
        dt.Columns.Add(dcPhoneType2)
        dt.Columns.Add(dcPhoneStatus2)
        dt.Columns.Add(dcForeignPhone2)
        dt.Columns.Add(dcDefaultPhone)

        Return dt

    End Function

    Private Function BuildMapLookupValuesDT() As DataTable
        Dim dt As New DataTable
        Dim rpb As RadPanelBar
        Dim rpi As RadPanelItem
        Dim rg As RadGrid
        Dim ds As New DataSet
        Dim dtLookups As DataTable
        Dim filterExp As String
        Dim selectedDR() As DataRow

        'Build the structure
        Dim dcILFieldId As New DataColumn("ILFieldId", GetType(String))
        Dim dcFldName As New DataColumn("FldName", GetType(String))
        Dim dcFileValue As New DataColumn("FileValue", GetType(String))
        Dim dcAdvMappedValue As New DataColumn("AdvMappedValue", GetType(String))
        Dim dcAdvMappedCaption As New DataColumn("AdvMappedCaption", GetType(String))

        dt.Columns.Add(dcILFieldId)
        dt.Columns.Add(dcFldName)
        dt.Columns.Add(dcFileValue)
        dt.Columns.Add(dcAdvMappedValue)
        dt.Columns.Add(dcAdvMappedCaption)

        'Get the lookup fields that have been selected by the user
        dtLookups = CType(Session("dtLookups"), DataTable)
        ds = CType(Session("LookupSources"), DataSet)

        filterExp = "Selected = 'Y'"
        selectedDR = dtLookups.Select(filterExp, "Caption")

        For Each dr As DataRow In selectedDR
            'For each lookup field that has been selected by the user we will loop through the controls of the
            'tblLookupMapValues table and find all the radpanels. We will get the file value and the lookup value
            'it was mapped to and add them to the datatable. We will also add the field name that the mapping is for.
            'It is possible that two different fields could have the same file values. For eg. the state field could
            'have a value of CA mapped to California. The Lead Status field could have a value of CA for something
            'like Cancelled Application. It is a good idea to be on the safe side here.
            rpb = tblMapLookupValues.FindControl("rpb" + dr("FldName"))
            rpi = rpb.Items.FindItemByText(dr("Caption"))
            rg = rpi.FindControl("rgLookup" + dr("FldName"))

            For Each gi As GridDataItem In rg.Items
                'For Each gi As DataRow In DirectCast(Session("SourceDT"), System.Data.DataTable).Rows
                Dim lbl As Label = CType(gi.FindControl("lblFileValue"), Label)
                Dim ddl As DropDownList = CType(gi.FindControl("ddlAdvantageValues"), DropDownList)
                Dim dr2 As DataRow = dt.NewRow
                dr2("ILFieldId") = dr("ILFieldId")
                dr2("FldName") = dr("FldName")
                dr2("FileValue") = lbl.Text
                dr2("AdvMappedValue") = ddl.SelectedValue.ToString
                dr2("AdvMappedCaption") = ddl.SelectedItem.Text
                dt.Rows.Add(dr2)
            Next
        Next


        Return dt
    End Function

    Private Sub AddRecordsToLeadsDt(ByRef dtLeadsTable As DataTable, dtMappedValues As DataTable)
        'Loop through rgFile which has the file records. 
        'We are going to use each record to create the records for the datatable with the adLeads structure.
        'This will allow us to use a DataAdapter to perform a batch insert rather than inserting the records one at a time.
        'We will get the values for that datatable by looking at the mapped fields.
        'The names of the fields in the datatable correspond to the field names of the mapped fields.
        'If a mapped field is a lookup we will get the guid from the datatable with the mapped values
        Dim ddlId As String
        Dim hi As GridHeaderItem
        Dim advFldName As String
        Dim fileFldName As String
        Dim fileValue As String
        Dim mappedRows() As DataRow
        Dim filterExp As String
        Dim recNumber As Integer

        'This record number counter is used to pass the record number that is being processed so we can check if the
        'record is an exception or is a duplicate.
        recNumber = 1

        ' For Each gi As GridDataItem In rgFile.Items 'TA9647
        For Each gi As DataRow In DirectCast(Session("SourceDT"), DataTable).Rows
            'We need to skip any record with an exception or a record that is a duplicate.
            If Not RecordIsExceptionOrADuplicateToRemoved(recNumber) Then
                'Create a new row to represent this record in the leads datatable
                Dim dr As DataRow = dtLeadsTable.NewRow

                For i As Integer = 0 To Session("NumberOfFields") - 1
                    ddlId = "ddl" & CStr(i)
                    hi = CType(rgAdvFields.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
                    advFldName = CType(hi.FindControl(ddlId), DropDownList).SelectedValue

                    'Skip unmapped fields
                    If advFldName <> "" Then
                        'Get the file value for this field
                        'Get the name of the file field from the SourceDT in session. It is better to do this because
                        'we cannot use the index on the GridDataItem object reliably.
                        fileFldName = CType(Session("SourceDT"), DataTable).Columns(i).ColumnName
                        fileValue = CType(gi(fileFldName), String) '.Text

                        'Check if this is a lookup field. The user might map an empty value in a field to an Advantage field.
                        'For eg. the user might want to map any record with a missing admissions rep to one rep that
                        'is used for that purpose.
                        If fileValue <> "" And fileValue <> " " And fileValue <> "&nbsp;" Then
                            filterExp = "FldName = '" + advFldName + "' and FileValue= '" + Replace(fileValue, "'", "''") + "'"
                            'filterExp = "FldName = '" + advFldName + "' and FileValue= '" + fileValue + "'"
                        Else
                            filterExp = "FldName = '" + advFldName + "' and FileValue= 'No Value Specified'"
                        End If



                        mappedRows = dtMappedValues.Select(filterExp)
                        If mappedRows.Count > 0 Then
                            'Set the file value to the mapped value
                            fileValue = CType(mappedRows(0)("AdvMappedValue"), String)
                        End If

                        'We have to take care of any special formatting used on SSN and Phone fields.
                        If advFldName.ToUpper = "SSN" And fileValue <> "" And fileValue <> " " And fileValue <> "&nbsp;" Then
                            fileValue = fileValue.Replace("-", "").Replace("/", "").Replace("\", "")
                        End If

                        If advFldName.ToUpper = "PHONE" And fileValue <> "" And fileValue <> " " And fileValue <> "&nbsp;" Then
                            'In the case of the phone field if we replace the special characters and we have a length of 10 we want
                            'to store the number without the special characters as a regular US number. Otherwise, we will store it
                            'the way it is and marked ForeignPhone as true (1).
                            If Len(fileValue.Replace("-", "").Replace("/", "").Replace("\", "").Replace("(", "").Replace(")", "")) = 10 Then
                                fileValue = fileValue.Replace("-", "").Replace("/", "").Replace("\", "").Replace("(", "").Replace(")", "")
                            End If
                        End If
                        If (advFldName.ToUpper = "BIRTHDATE" Or advFldName.ToUpper = "ASSIGNEDDATE" Or advFldName.ToUpper = "DATEAPPLIED" Or advFldName.ToUpper = "SOURCEDATE" Or advFldName.ToUpper = "EXPECTEDSTART") And fileValue <> "" And fileValue <> " " And fileValue <> "&nbsp;" Then
                            fileValue = fileValue
                        End If

                        If (advFldName.ToUpper = "COUNTRY" Or Right(advFldName.ToUpper, 2) = "ID") AndAlso Not CommonWebUtilities.IsValidGuid(fileValue) Then
                            fileValue = "00000000-0000-0000-0000-000000000000"
                        End If


                        'Set the value for this field in the leads datatable. If we are dealing with a mapped field that the user
                        'has not specified a value for we should set it to null. Otherwise, we will end up with "&nbsp;" for the
                        'value in the database.
                        If fileValue = "" Or fileValue = "&nbsp;" Or fileValue = " " Or fileValue = "00000000-0000-0000-0000-000000000000" Then
                            dr(advFldName) = DBNull.Value
                        Else
                            dr(advFldName) = Trim(fileValue)
                        End If


                    End If


                Next

                'Before we add the row to the datatable we need to take care of special fields such as the LeadId, CreatedDate,
                'ModUser, ModDate, Foreign Phone and Foreign Zip
                Dim leadId As Guid = Guid.NewGuid

                dr("LeadId") = leadId
                dr("ModUser") = Session("UserName")
                dr("ModDate") = Date.Now
                dr("CreatedDate") = Date.Now

                If dr("AssignedDate") Is DBNull.Value Then
                    dr("AssignedDate") = Date.Now
                End If

                If dr("CampusId") Is DBNull.Value Then
                    dr("CampusId") = ddlDefaultCampus.SelectedValue
                End If

                If dr("AdmissionsRep") Is DBNull.Value Then
                    dr("AdmissionsRep") = ddlDefaultAdmissionsRep.SelectedValue
                End If

                If dr("LeadStatus") Is DBNull.Value Then
                    dr("LeadStatus") = ddlDefaultLeadStatus.SelectedValue
                End If

                If dr("Phone") Is DBNull.Value Then
                    dr("ForeignPhone") = "0"
                ElseIf Len(dr("Phone")) = 10 Or dr("Phone") = "" Then
                    dr("ForeignPhone") = "0"
                Else
                    dr("ForeignPhone") = 1
                End If

                If dr("Phone2") Is DBNull.Value Then
                    dr("ForeignPhone2") = "0"
                ElseIf Len(dr("Phone2")) = 10 Or dr("Phone2") = "" Then
                    dr("ForeignPhone2") = "0"
                Else
                    dr("ForeignPhone2") = 1
                End If

                If dr("ForeignZip") Is DBNull.Value Then
                    dr("ForeignZip") = "0"
                ElseIf Len(dr("ForeignZip")) = 5 Or dr("ForeignZip") = "" Then
                    dr("ForeignZip") = "0"
                Else
                    dr("ForeignZip") = "1"
                End If

                dr("DefaultPhone") = "1"

                'If the user has mapped a field to program version (PrgVerId) then we need to lookup the values for ProgramID and AreaID.
                'Note that the hierarchy is Area of Interest, Program and Program Version.
                'I therefore expected that the Program Version would have a reference to a Program and a Program would have a reference to Area of Interest.
                'However, the Program Version actually has a reference to the Program (ProgId) as well as the Area of Interest (PrgGrpId from the arPrgGrp table).
                Dim fac As New ProgVerFacade

                If dr("PrgVerId") IsNot DBNull.Value Then
                    'Get the related program
                    dr("ProgramId") = fac.GetProgramForPrgVersion(dr("PrgVerId"))
                    'Get the related area of interest
                    dr("AreaID") = fac.GetAreaForPrgVersion(dr("PrgVerId"))
                ElseIf dr("ProgramId") IsNot DBNull.Value Then
                    'Get the related area of interest
                    dr("AreaID") = fac.GetAreaForProgram(dr("ProgramId"))
                End If

                'Add the record to the datatable
                dtLeadsTable.Rows.Add(dr)
            End If

            'Increment the record counter
            recNumber += 1
        Next
    End Sub

    Private Function CreateFileAdvFieldMappings() As DataTable
        Dim dt As New DataTable
        Dim ddlId As String
        Dim advFldName As String
        Dim hi As GridHeaderItem
        Dim filterExp As String
        Dim filterRows() As DataRow
        Dim dtAdvFieldsDt As DataTable
        Dim iLFieldId As String

        dtAdvFieldsDt = CType(Session("AdvFieldsDT"), DataTable)

        Dim dcMappingId As New DataColumn("MappingId", GetType(String))
        Dim dcFileFldNumber As New DataColumn("FileFldNumber", GetType(String))
        Dim dcAdvIlFieldId As New DataColumn("AdvILFieldId", GetType(String))

        dt.Columns.Add(dcMappingId)
        dt.Columns.Add(dcFileFldNumber)
        dt.Columns.Add(dcAdvIlFieldId)

        'We are going to create a record for each file field even if the user did not map an Advantage field to it.
        'The mapping id is for the parent record. At this point we don't know it so it will be filled in later
        'on in the code that does the actual insert. 
        For i As Integer = 0 To Session("NumberOfFields") - 1
            Dim dr As DataRow = dt.NewRow

            ddlId = "ddl" & CStr(i)
            hi = CType(rgAdvFields.MasterTableView.GetItems(GridItemType.Header)(0), GridHeaderItem)
            advFldName = CType(hi.FindControl(ddlId), DropDownList).SelectedValue

            filterExp = "FldName = '" + advFldName + "'"
            filterRows = dtAdvFieldsDt.Select(filterExp)

            If filterRows(0)("ILFieldId") Is DBNull.Value Then
                iLFieldId = String.Empty
            Else
                iLFieldId = CStr(filterRows(0)("ILFieldId"))
            End If
            'Try
            '    iLFieldId = CStr(filterRows(0)("ILFieldId"))
            'Catch ex As Exception
             '	Dim exTracker = new AdvApplicationInsightsInitializer()
            '	exTracker.TrackExceptionWrapper(ex)

            '    iLFieldId = ""
            'End Try

            dr("FileFldNumber") = CStr(i)
            dr("AdvILFieldId") = iLFieldId

            dt.Rows.Add(dr)

        Next

        For Each dr2 As DataRow In dt.Rows
            If dr2("AdvILFieldId") = "" Then
                dr2("AdvILFieldId") = DBNull.Value
            End If
        Next

        Return dt
    End Function

    'Private Function CreateLookupsMappings() As DataTable
    '    Dim dt As New DataTable



    '    Return dt
    'End Function

    Private Function ValidateMapLookupValues() As Boolean
        Dim rpb As RadPanelBar
        Dim rpi As RadPanelItem
        Dim rg As RadGrid
        Dim dtLookups As DataTable
        Dim filterExp As String
        Dim selectedDr() As DataRow
        'Dim ds As New DataSet

        'Get the lookup fields that have been selected by the user
        dtLookups = CType(Session("dtLookups"), DataTable)
        'ds = CType(Session("LookupSources"), DataSet)

        filterExp = "Selected = 'Y'"
        selectedDr = dtLookups.Select(filterExp, "Caption")

        If selectedDr.Length = 0 Then
            Return True
        End If

        For Each dr As DataRow In selectedDr
            rpb = CType(tblMapLookupValues.FindControl(CType(("rpb" + dr("FldName")), String)), RadPanelBar)
            rpi = rpb.Items.FindItemByText(CType(dr("Caption"), String))
            rg = CType(rpi.FindControl(CType(("rgLookup" + dr("FldName")), String)), RadGrid)

            For Each gi As GridDataItem In rg.Items
                Dim lbl As Label = CType(gi.FindControl("lblFileValue"), Label)

                If lbl.Text <> "No Value Specified" Then
                    Dim ddl As DropDownList = CType(gi.FindControl("ddlAdvantageValues"), DropDownList)

                    If ddl.SelectedItem.Text = "Select" Then
                        Return False
                    End If
                End If

            Next
        Next

        Return True

    End Function

    Private Function ValidateDefaultValues() As Boolean
        If ddlDefaultCampus.SelectedIndex = 0 Or ddlDefaultAdmissionsRep.SelectedIndex = 0 Or ddlDefaultLeadStatus.SelectedIndex = 0 Then
            Return False
        End If

        Return True
    End Function

    Private Sub BuildCampusDdl()
        Dim cgFacade As New CampusGroupsFacade

        With ddlDefaultCampus
            .DataSource = cgFacade.GetAllCampuses
            .DataTextField = "CampDescrip"
            .DataValueField = "CampusId"
            .DataBind()
            .Items.Insert(0, "Select")
        End With

    End Sub

    Protected Sub DdlDefaultCampusSelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDefaultCampus.SelectedIndexChanged
        'Buid the DDLs for admission reps and lead statuses based on the selected campus
        If ddlDefaultCampus.SelectedIndex = 0 Then
            DisplayErrorMessage("You must select a campus")
        Else
            BuildAdmissionRepsDdl()
            BuildLeadStatusesDdl()
        End If

    End Sub

    Private Sub BuildAdmissionRepsDdl()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)

        ddlList.Add(New AdvantageDDLDefinition(ddlDefaultAdmissionsRep, "AdmissionsRep", ddlDefaultCampus.SelectedValue, True, True, "", "ImportLeads"))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub

    Private Sub BuildLeadStatusesDdl()
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)

        ddlList.Add(New AdvantageDDLDefinition(ddlDefaultLeadStatus, "LeadStatus", ddlDefaultCampus.SelectedValue, True, True, "", "ImportLeads"))

        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub

    Private Function MoveFileFromInToExceptionFolder(sourcePath As String, targetPath As String, fileName As String) As String
        Dim uriSource As New Uri(sourcePath.ToString.ToLower)
        Dim uriTarget As New Uri(targetPath.ToString.ToLower)
        Dim strSourceFile As String
        Dim strTargetFile As String

        If myFlFacade.IsRemoteServer(campusId) Then
            strSourceFile = sourcePath + fileName
            strTargetFile = targetPath + fileName
        Else
            strSourceFile = uriSource.AbsolutePath + fileName
            strTargetFile = uriTarget.AbsolutePath + fileName
        End If

        Dim strMessage As String = String.Empty
        Try
            If File.Exists(strSourceFile) Then
                'RenameExistingFile(strTargetFile)
                strTargetFile = RenameFileAddDate(strTargetFile)
                'before move be sure the strTargentFile do not exist
                CheckTargetFile(strTargetFile)
                File.Move(strSourceFile, strTargetFile)
            Else
                strMessage = "File: " & strSourceFile & " does not exist.  Move to archive cannot complete."
            End If

        Catch e As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(e)

            strMessage = "The process could not complete." & vbCrLf &
                "SourceFile: " & strSourceFile & vbCrLf &
                "TargetFile: " & strTargetFile & vbCrLf &
                "Exception details: " & e.InnerException.ToString()
        End Try

        Return strMessage

    End Function

    Private Function RenameFileAddDate(pathFileName As String) As String
        Dim pathFileNameWithDate As String
        Dim fileInfo As FileInfo
        Dim s As String

        fileInfo = New FileInfo(pathFileName)
        s = DateTime.Now.ToString("MMddyyyyhms")
        pathFileNameWithDate = fileInfo.DirectoryName + "\" + fileInfo.Name.Replace(fileInfo.Extension, "") + "_" + s + fileInfo.Extension

        Return pathFileNameWithDate
    End Function

    Private Sub CheckTargetFile(pathFileName As String)
        If File.Exists(pathFileName) Then
            Try
                'rename existing file strTargetFile
                Dim fileInfo As FileInfo = New FileInfo(pathFileName)
                My.Computer.FileSystem.RenameFile(pathFileName, fileInfo.Name + "_" + Guid.NewGuid().ToString())
            Catch e As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(e)

                DisplayErrorMessage("Error:" + e.Message + ". Could not rename an old file with the same imported file: " & pathFileName & ". ")
            End Try
        End If
    End Sub

    Private Sub RenameExistingFile(strTargetPathWithFile As String)
        'Dim sMessage As String = ""
        'Dim strRandomNumber As New Random
        Dim rightNow As DateTime = DateTime.Now
        Dim s As String
        s = rightNow.ToString("MMddyyyyhms")
        Dim strTargetPathWithNewFile As String = strTargetPathWithFile + s   '+ strRandomNumber.Next.ToString
        If File.Exists(strTargetPathWithFile) Then
            File.Move(strTargetPathWithFile, strTargetPathWithNewFile)
        End If
    End Sub

    Protected Sub RgPreviewNeedDataSource(source As Object, e As GridNeedDataSourceEventArgs) Handles rgPreview.NeedDataSource
        rgPreview.DataSource = Session("dtLeadsTableForPreview")

    End Sub

    Protected Sub DdlFileNamesSelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFileNames.SelectedIndexChanged
        'We want to populate the ddlSavedMappings DDL with mappings that have the same number of fields as the selected file.
        'We will call ReadFile to read the file without displaying the contents. THis will populate Session("NumberOfFields").
        Dim numFields As Integer
        Dim myLefac As New LeadEnrollmentFacade

        'Read the file without displaying the contents
        ReadFile(False)
        numFields = CType(Session("NumberOfFields"), Integer)

        With ddlSavedMappings
            .DataSource = myLefac.GetSavedMappingsWithNumberOfFields(numFields, campusId)
            .DataTextField = "MapName"
            .DataValueField = "MappingId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub

    Protected Sub DdlSavedMappingsSelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlSavedMappings.SelectedIndexChanged
        Session("ddlSavedMappingsSelectedIndex") = ddlSavedMappings.SelectedIndex
        Session("ddlSavedMappingSelectedText") = ddlSavedMappings.SelectedItem.Text
    End Sub

#Region "buttons"
    Protected Sub BtnImportClick(sender As Object, e As EventArgs) Handles btnImport.Click
        ImportFile()
    End Sub

    Protected Sub BtnNextMapFieldsClick(sender As Object, e As EventArgs)
        'We need to perform a couple of validations here:
        'The user needs to Map Field Names to FirstName and LastName. 
        'The user should not map more than one field to the same Advantage field. 
        Dim strErrMsg As String

        strErrMsg = ValidateTab1()

        If strErrMsg = "" Then

            'This code enables and selects the next tab if validation passes
            'Note that you have to change the SelectedIndex property of the multipage too.
            RadTabStrip1.SelectedIndex = RadTabStrip1.SelectedIndex + 1
            RadTabStrip1.SelectedTab.Enabled = True
            RadMultiPage1.SelectedIndex = RadMultiPage1.SelectedIndex + 1
        Else

            DisplayErrorMessage(strErrMsg)

        End If



    End Sub

    Protected Sub BtnExportExceptionsToExcelClick(sender As Object, e As EventArgs) Handles btnExportExceptionsToExcel.Click
        Dim rgEx As RadGrid = rgExceptions
        rgEx.AllowPaging = False
        rgEx.Rebind()
        rgEx.ExportSettings.FileName = "ExceptionsInImportleads"
        rgEx.ExportSettings.IgnorePaging = True
        rgEx.ExportSettings.OpenInNewWindow = True
        rgEx.MasterTableView.ExportToExcel()


    End Sub

    Protected Sub BtnExportDuplicatesToExcelClick(sender As Object, e As EventArgs) Handles btnExportDuplicatesToExcel.Click
        Dim rgEx As RadGrid = rgDuplicates
        rgEx.AllowPaging = False
        rgEx.Rebind()
        rgEx.ExportSettings.FileName = "DuplicatesInImportleads"
        rgEx.ExportSettings.IgnorePaging = True
        rgEx.ExportSettings.OpenInNewWindow = True
        rgEx.MasterTableView.ExportToExcel()

    End Sub

    Protected Sub BtnExportToExcelClick(sender As Object, e As EventArgs) Handles btnExportToExcel.Click
        Dim rgEx As RadGrid = rgPreview
        rgEx.AllowPaging = False
        rgEx.Rebind()
        rgEx.ExportSettings.FileName = "Importleads"
        rgEx.ExportSettings.IgnorePaging = True
        rgEx.ExportSettings.OpenInNewWindow = True
        rgEx.MasterTableView.ExportToExcel()

    End Sub
#End Region


    Public Function GetCountOfCharacter(pValue As String, ch As Char) As Integer
        Dim cnt As Integer = 0
        For Each c As Char In pValue
            If c = ch Then cnt += 1
        Next
        Return cnt
    End Function
    Public Function IsValidEMail(ByVal pValue As String) As Boolean
        If (pValue = String.Empty) Then
            Return True
        End If
        Static emailExpression As New Regex("^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$")
        '("^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$")
        Return emailExpression.IsMatch(pValue)
    End Function
    Public Function IsValidSsn(ByVal pValue As String) As Boolean
        If (pValue = String.Empty) Then
            Return True
        End If
        Static ssnExpresion As New Regex("^\d{3}-?\d{2}-?\d{4}$")
        Return ssnExpresion.IsMatch(pValue)
    End Function
    Public Function IsValidPhone(ByVal pValue As String) As Boolean
        If (pValue = String.Empty) Then
            Return True
        End If
        'Admit the followings formats
        '123-456-7890
        '(123) 456-7890
        '123 456 7890
        '123.456.7890
        '+91 (123) 456-7890
        Static phoneExpression As New Regex("^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$")
        Return phoneExpression.IsMatch(pValue)
    End Function

    Public Function IsValidInternationalPhone(ByVal pValue As String) As Boolean
        If (pValue = String.Empty) Then
            Return True
        End If
        Static internationalPhoneExpression As New Regex("/^\d{7,25}$/")
        Return internationalPhoneExpression.IsMatch(pValue)
    End Function
    Public Function IsValidZipCode(ByVal pValue As String) As Boolean
        If (pValue = String.Empty) Then
            Return True
        End If
        Static zipCodeExpression As New Regex("^\d{5}$")
        Return zipCodeExpression.IsMatch(pValue)
    End Function


End Class

