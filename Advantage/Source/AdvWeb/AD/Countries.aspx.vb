Imports Fame.Common
Imports Fame.Advantage.Common
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects

Partial Class Countries

    Inherits BasePage
    Protected WithEvents btnReset As System.Web.UI.WebControls.Button
    Protected WithEvents checkbox1 As System.Web.UI.WebControls.CheckBox
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents lblDocumentType As System.Web.UI.WebControls.Label
    Protected WithEvents ddlDocumentTypeId As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblStudentName As System.Web.UI.WebControls.Label
    Protected WithEvents ddlStudentName As System.Web.UI.WebControls.DropDownList
    Protected WithEvents lblFileName As System.Web.UI.WebControls.Label
    Protected WithEvents txtFIle As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnBrowse As System.Web.UI.WebControls.Button
    Protected strDefaultCountry As String
    Protected myAdvAppSettings As AdvAppSettings
    Dim campusId, userId As String
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private pObj As New UserPagePermissionInfo

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            myAdvAppSettings = New AdvAppSettings
        End If

        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter

        'Dim userId As String
        Dim mContext As HttpContext
        'Dim fac As New UserSecurityFacade
        Dim resourceId As Integer
        Dim advantageUserState As New BO.User()

        advantageUserState = AdvantageSession.UserState
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        userId = AdvantageSession.UserState.UserId.ToString()
        campusId = Master.CurrentCampusId ' AdvantageSession.UserState.CampusId.ToString
        'userId = AdvantageSession.UserState.UserId.ToString
        'pObj = fac.GetUserResourcePermissions(userId, resourceId, campusId)
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        'Get Default Country
        ViewState("DefaultCountry") = (New CountyFacade).GetDefaultCountry().ToString

        'Check if this page still exists in the menu while switching campus
        'Dim boolSwitchCampus As Boolean = False
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        chkIsDefault.Visible = False
        Try
            If Not Page.IsPostBack Then
                'Header1.EnableHistoryButton(False)
                mContext = HttpContext.Current
                txtResourceId.Text = CInt(mContext.Items("ResourceId"))


                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                chkIsDefault.Text = chkIsDefault.Text & " (Applies only to " & "All" & " Campus Group)"
                'chkIsDefault.Visible = False
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
                ViewState("MODE") = "NEW"
                txtCountryId.Text = Guid.NewGuid.ToString()
                ds = objListGen.SummaryListGenerator(userId, campusId)

                PopulateDataList(ds)

                AddDefaultLabelToCountries()
                EnableDisableDefaultCheckBox()

                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    pnlRegent.Visible = True
                    BuildSession()
                End If
            Else
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BuildSession()
        With ddlRegentCode
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("Country", txtCountryId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub BtnSaveClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objCommon As New CommonUtilities
        Dim ds As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sw As New System.IO.StringWriter
        Dim msg As String
        Try
            If ViewState("MODE") = "NEW" Then
                'Set the Primary Key value, so it can be used later by the DoUpdate method
                objCommon.PagePK = txtCountryId.Text
                msg = objCommon.DoInsert(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg <> "" Then
                    DisplayErrorMessage(msg)
                End If
                txtRowIds.Text = objCommon.PagePK
                ds = objListGen.SummaryListGenerator(userId, campusId)
                dlstCountries.SelectedIndex = -1

                PopulateDataList(ds)
                AddDefaultLabelToCountries()
                EnableDisableDefaultCheckBox()
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
                ViewState("MODE") = "EDIT"

            ElseIf ViewState("MODE") = "EDIT" Then
                msg = objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                If msg = "" Then
                    ds = objListGen.SummaryListGenerator(userId, campusId)
                    dlstCountries.SelectedIndex = -1
                    PopulateDataList(ds)
                    AddDefaultLabelToCountries()
                    EnableDisableDefaultCheckBox()
                    ds.WriteXml(sw)
                    ViewState("ds") = sw.ToString()
                Else
                    DisplayErrorMessage(msg)
                End If
            End If
            If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                Dim regCode As String = ""
                Dim pnlInnerPanel As Panel
                pnlInnerPanel = CType(pnlRegent.FindControl("pnlRegentAwardChild"), Panel)
                regCode = CType(pnlInnerPanel.FindControl("ddlRegentCode"), DropDownList).SelectedValue
                Dim strGetRegentDegree As String = (New regentFacade).updateMaintenanceData("adCountries", txtCountryId.Text, regCode, "MapId", "CountryId")
            End If

            '   set Style to Selected Item
            CommonWebUtilities.RestoreItemValues(dlstCountries, txtCountryId.Text)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub

    Private Sub BtnNewClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Try
            ClearRhs()
            ds.ReadXml(sr)
            PopulateDataList(ds)
            'chkIsDefault.Visible = False
            AddDefaultLabelToCountries()
            EnableDisableDefaultCheckBox()
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            'set the state to new. 
            ViewState("MODE") = "NEW"
            txtCountryId.Text = Guid.NewGuid.ToString
            If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                BuildSession()
            End If
            '   set Style to Selected Item
            CommonWebUtilities.RestoreItemValues(dlstCountries, Guid.Empty.ToString)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub BtnDeleteClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim msg As String
        Try
            msg = objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            If msg <> "" Then
                DisplayErrorMessage(msg)
                Exit Sub
            End If
            ClearRhs()
            ds = objListGen.SummaryListGenerator(userId, campusId)
            dlstCountries.SelectedIndex = -1
            PopulateDataList(ds)
            AddDefaultLabelToCountries()
            EnableDisableDefaultCheckBox()
            ds.WriteXml(sw)
            ViewState("ds") = sw.ToString()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtCountryId.Text = Guid.NewGuid.ToString()
            If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                BuildSession()
            End If
            '   set Style to Selected Item
            CommonWebUtilities.RestoreItemValues(dlstCountries, Guid.Empty.ToString)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ClearRhs()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub DlstCountriesItemCommand(ByVal sender As System.Object, ByVal e As DataListCommandEventArgs) Handles dlstCountries.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Dim strGuid As String
        Dim objCommon As New CommonUtilities
        Dim lnkCountry As LinkButton

        Try
            strGuid = dlstCountries.DataKeys(e.Item.ItemIndex).ToString()
            objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGuid)

            lnkCountry = CType(e.Item.FindControl("LinkButton1"), LinkButton)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT", pObj)
            ViewState("MODE") = "EDIT"
            txtRowIds.Text = strGuid
            selIndex = e.Item.ItemIndex
            dlstCountries.SelectedIndex = selIndex
            ds.ReadXml(sr)
            PopulateDataList(ds)
            AddDefaultLabelToCountries()
            EnableDisableDefaultCheckBox()
            ValidateDefaultCountry(lnkCountry.Text)
            If myAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                pnlRegent.Visible = True
                Try
                    ddlRegentCode.SelectedValue = (New regentFacade).getAllMaintenanceDataByPrimaryKey("adCountries", "CountryId", strGuid)
                Catch ex As System.Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                    ddlRegentCode.SelectedIndex = 0
                End Try
            End If

            CommonWebUtilities.RestoreItemValues(dlstCountries, strGuid)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstCountries_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstCountries_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub ChkStatusCheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim objCommon As New CommonUtilities

        Try
            ClearRhs()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"
            txtCountryId.Text = Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub PopulateDataList(ByVal ds As DataSet)
        Dim ds2 As New DataSet
        Dim objListGen As New DataListGenerator
        Dim sDataFilter As String = ""

        'This method was refactored by AVP on 6/22/2012

        ds2 = objListGen.StatusIdGenerator()

        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (radStatus.SelectedItem.Text.ToLower = "active") Or (radStatus.SelectedItem.Text.ToLower = "inactive") Then 'Show filtered result
            Dim row As DataRow = ds2.Tables(0).Rows.Find(radStatus.SelectedItem.Text)
            sDataFilter = "StatusId = '" & row("StatusId").ToString() & "'"
        Else  'Show All
            'do nothing as the data will not be filtered
        End If

        Dim dv As New DataView(ds.Tables(0), sDataFilter, "CountryDescrip", DataViewRowState.CurrentRows)

        With dlstCountries
            .DataSource = dv
            .DataBind()
        End With
        dlstCountries.SelectedIndex = -1
    End Sub

    Private Sub RadStatusSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radStatus.SelectedIndexChanged
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim objCommon As New CommonUtilities

        Try
            ClearRhs()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW", pObj)
            ViewState("MODE") = "NEW"
            txtCountryId.Text = Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds)
            AddDefaultLabelToCountries()
            EnableDisableDefaultCheckBox()
            'Header1.EnableHistoryButton(False)
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub radStatus1_SelectedIndexChanged " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub radStatus1_SelectedIndexChanged " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        BindToolTip()
    End Sub

    Private Sub AddDefaultLabelToCountries()
        Dim iitems As DataListItemCollection
        Dim iitem As DataListItem
        Dim i As Integer
        Dim lnkButton As LinkButton

        iitems = dlstCountries.Items

        ViewState("DefaultCountry") = (New CountyFacade).GetDefaultCountry().ToString
        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            lnkButton = CType(iitem.FindControl("LinkButton1"), LinkButton)
            If lnkButton.CommandArgument.ToString = ViewState("DefaultCountry") Then
                lnkButton.Text = lnkButton.Text & " (Default)"
            End If
        Next
    End Sub

    Private Sub AddDefaultLabelToCountriesWhileSaved()
        Dim iitems As DataListItemCollection
        Dim iitem As DataListItem
        Dim i As Integer
        Dim lnkButton As LinkButton

        iitems = dlstCountries.Items

        ViewState("DefaultCountry") = (New CountyFacade).GetDefaultCountry().ToString
        For i = 0 To iitems.Count - 1
            iitem = iitems.Item(i)
            lnkButton = CType(iitem.FindControl("LinkButton1"), LinkButton)
            If lnkButton.CommandArgument.ToString = ViewState("DefaultCountry") And chkIsDefault.Checked = True Then
                lnkButton.Text = lnkButton.Text & " (Default)"
            End If
        Next
    End Sub

    Private Sub EnableDisableDefaultCheckBox()
        'Disable the checkbox, if there is any country that is already marked as default
        'there can be only one country
        ViewState("DefaultCountry") = (New CountyFacade).GetDefaultCountry().ToString
        If Not ViewState("DefaultCountry") = "" And Not ViewState("DefaultCountry") = Guid.Empty.ToString And chkIsDefault.Checked = False Then
            chkIsDefault.Enabled = False
            chkIsDefault.ToolTip = "checkbox is enabled only if the country belongs to all campus group and no default country is already set"
        Else
            chkIsDefault.Enabled = True
            chkIsDefault.ToolTip = ""
        End If
    End Sub

    Private Sub ValidateDefaultCountry(ByVal selectedText As String)
        If InStr(selectedText, "(D") >= 1 Then
            chkIsDefault.Enabled = True
        End If
    End Sub

End Class
