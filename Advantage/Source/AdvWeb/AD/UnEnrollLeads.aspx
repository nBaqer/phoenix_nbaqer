﻿<%@ Page Title="Unenroll Leads" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="UnEnrollLeads.aspx.vb" Inherits="UnEnrollLeads" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/EnterKey.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        window.onload = function () {
            var strCook = document.cookie;
            if (strCook.indexOf("!~") != 0) {
                var intS = strCook.indexOf("!~");
                var intE = strCook.indexOf("~!");
                var strPos = strCook.substring(intS + 2, intE);
                document.getElementById("grdWithScroll").scrollTop = strPos;
            }
        }
        function SetDivPosition() {
            var intY = document.getElementById("grdWithScroll").scrollTop;
            document.cookie = "yPos=!~" + intY + "~!";
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>
            <table id="Table2" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="listframetop2">
                        <br />
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                            <tr>
                                <td class="contentcellfilter" nowrap>
                                    <asp:Label ID="lblStartDate" CssClass="label" runat="server">Start Date</asp:Label>
                                </td>
                                <td class="contentcellfilter4" align="left">
                                    <telerik:RadDatePicker ID="txtStartDate" MinDate="1/1/1945" runat="server" CssClass="textboxdatenoborder">
                                    </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr height="5px">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="contentcellfilter" nowrap>
                                    <asp:Label ID="lblEndDate" CssClass="label" runat="server">End Date</asp:Label>
                                </td>
                                <td class="contentcellfilter4" align="left">
                                    <telerik:RadDatePicker ID="txtEndDate" MinDate="1/1/1945" runat="server" CssClass="textboxdatenoborder">
                                    </telerik:RadDatePicker>
                                </td>
                            </tr>
                            <tr height="5px">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="contentcellfilter" nowrap>
                                    <asp:Label ID="lblFirstName" CssClass="label" runat="server">First Name</asp:Label>
                                </td>
                                <td class="contentcellfilter4" align="left">
                                    <asp:TextBox ID="txtFirstName" runat="server" CssClass="textbox"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr height="5px">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="contentcellfilter" nowrap>
                                    <asp:Label ID="lblLastName" CssClass="label" runat="server">Last Name</asp:Label>
                                </td>
                                <td class="contentcellfilter4" align="left">
                                    <asp:TextBox ID="txtLastName" runat="server" CssClass="textbox"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                                </td>
                            </tr>
                            <tr height="5px">
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="contentcellfilter" nowrap></td>
                                <td class="contentcellfilter4" style="text-align: left;">
                                    <asp:Button ID="btnLead" Text="Get Enrolled Students" runat="server"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div id="grdWithScroll" class="scrollleftfltr3rowsnopad"
                            onscroll="SetDivPosition()">
                            <asp:DataList ID="dlstLeadNames" runat="server" Width="100%" DataKeyField="LeadId"
                                GridLines="Both">
                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <HeaderTemplate>
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="threecolumndatalist" style="border-right: #999 1px solid; border-top: #999 1px solid; border-left: #999 1px solid; width: 100%; border-bottom: #999 1px solid">
                                                <asp:Label ID="lblLeadName" runat="server" CssClass="labelbold">Students (SSN)</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <span>Note: The following list will only show students that do not have attendance, grades or active applicant fees on the ledger.</span>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <SelectedItemTemplate>
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="threecolumndatalist" style="width: 100%;">
                                                <asp:Label ID="label11" runat="server" CssClass="selecteditemstyle" Text='<%# Container.DataItem("FullName")%>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </SelectedItemTemplate>
                                <ItemTemplate>
                                    <table cellspacing="0" cellpadding="0" width="100%">
                                        <tr>
                                            <td class="threecolumndatalist" style="width: 100%;">
                                                <asp:LinkButton Text='<%# Container.DataItem("FullName")%>' runat="server" CssClass="itemstyle"
                                                    CommandArgument='<%# Container.DataItem("LeadId")%>' ID="Linkbutton1" CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                        <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"
                            Enabled="False"></asp:Button>
                    </td>
                    
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" width="98%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->
                            <asp:Panel ID="pnlRHS" runat="server">
                                <div class="scrollright">
                                    <!-- begin table content-->
                                    <!-- Two column template with no spacer in between cells -->
                                    <!-- used in placement module for maintenance pages -->
                                    <table class="contenttable" cellspacing="0" cellpadding="0" width="60%">
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblEnrollmentId" runat="server" CssClass="label">Enrollment Id</asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtEnrollmentId" runat="server" CssClass="textbox" Enabled="False"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="Label1" runat="server" CssClass="label">Student</asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtLead" runat="server" CssClass="textbox" Enabled="False"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblStatusCodeId" runat="server" CssClass="label">Status</asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusCodeId" runat="server" CssClass="dropdownlist" BackColor="#ffffa9">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="lblSSN" runat="server" CssClass="label">SSN</asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtSSN" runat="server" CssClass="textbox" Enabled="False"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell">
                                                <asp:Label ID="Label2" runat="server" CssClass="label">DOB</asp:Label>
                                            </td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtDOB" runat="server" CssClass="textbox" Enabled="False"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td class="contentcell4">
                                                <asp:Button ID="btnUnEnroll" runat="server" Text="UnEnroll Student" Enabled="False"
                                                    ></asp:Button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </asp:Panel>
                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="contentcell" nowrap>
                        <asp:TextBox ID="txtStuEnrollId" runat="server" Visible="false"></asp:TextBox><asp:CheckBox
                            ID="chkIsInDB" runat="server" Visible="false" Checked="False"></asp:CheckBox></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="contentcell" nowrap></td>
                    <td class="contentcell4">
                        <asp:TextBox ID="txtLeadId" CssClass="textbox" runat="server" Visible="false"></asp:TextBox></td>
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

