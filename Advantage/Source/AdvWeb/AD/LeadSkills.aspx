﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="LeadSkills.aspx.vb" Inherits="LeadSkills" %>
<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
 
<script src="../js/CheckAll.js" type="text/javascript"></script>
<script src="../Scripts/Advantage.Client.AD.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
				<!-- begin rightcolumn -->
				<tr>
					<td><table width="100%" border="0" cellpadding="0" cellspacing="0">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right"><asp:button id="btnSave" runat="server" CssClass="save" Text="Save"></asp:button><asp:button id="btnNew" runat="server" CssClass="new" Text="New" enabled="false"></asp:button>
									<asp:button id="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false" enabled="false"></asp:button></td>
								
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
                            align="center">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                       <table width="850px" cellpadding="0" cellspacing="0" class="contenttable" align="center">
											<asp:label id="lblError" CssClass="label" Runat="server" visible="false" ForeColor="red"></asp:label>
											<tr>
												<td  class="fourcolumnheader1"><asp:Label ID="lblSkillGroups" Runat="server" CssClass="labelbold" Font-Bold="True">Skill Groups</asp:Label></td>
                                                <%--<td class="fourcolumnhdrspacer"></td>--%>
												<td  class="fourcolumnheader1"><asp:Label ID="lblPossibleSkills" Runat="server" CssClass="labelbold" Font-Bold="True">Available Skills</asp:Label></td>
												<td  class="fourcolumnbuttonsspacer"></td>
												<td  class="fourcolumnheader1"><asp:Label ID="lblStudentSkills" Runat="server" CssClass="labelbold" Font-Bold="True">Selected Skills</asp:Label></td>
											</tr>
											<tr>
												<td  class="fourcolumncontent1"><asp:listbox id="lstGrpName" Runat="server" cssClass="listboxes" AutoPostback="True" Rows="15" Width="235px"></asp:listbox></td>
                                                <%--<td class="fourcolumnhdrspacer"></td>--%>
												<td  class="fourcolumncontent1"><asp:listbox id="lstPossibleSkills" Runat="server" cssClass="listboxes" Rows="15" AutoPostback="True" Width="235px"></asp:listbox></td>
												<td  class="fourcolumnbuttons" style="padding-left:10px;"><div>
                                                    <telerik:RadButton ID="btnAdd" runat="server" CssClass="buttons1" Text="Add >" Width="100px"></telerik:RadButton><br />
                                                    <telerik:RadButton ID="btnAddAll" runat="server" CssClass="buttons1" Text="Add All >>" Width="100px"></telerik:RadButton><br />
                                                    <telerik:RadButton ID="btnRemove" runat="server" CssClass="buttons1" Text="< Remove" Width="100px"></telerik:RadButton><br />
                                                    <telerik:RadButton ID="btnRemoveAll" runat="server" CssClass="buttons1" Text="<< Remove All" Width="100px"></telerik:RadButton></div>
                                                    </td>
												<td  class="fourcolumncontent1"><asp:listbox id="lstStudentSkills" Runat="server" cssClass="listboxes" Rows="15" AutoPostback="True" Width="235px"></asp:listbox></td>
											</tr>
										</table>
                                    </div>
                                </td>
                            </tr>
                        </table>
					</td>
					<!-- end rightcolumn --></tr>
			</table>
            <asp:textbox id="txtSkillGrpId" Runat="server" visible="False"></asp:textbox>
			<asp:Button ID="btncheck" Runat="server" Text="Check" Visible="false" />
			<input id="Hidden1" type="hidden" name="txtcheck" runat="server" />
			<asp:TextBox ID="txtLeadId" Runat="server" Visible="False" />
			<!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

