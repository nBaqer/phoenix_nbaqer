<%@ Page Title="" Language="VB" MasterPageFile="~/NewSiteLead.master" AutoEventWireup="false" CodeFile="ViewLeadEntranceTest.aspx.vb" Inherits="ViewLeadEntranceTest" %>
<%@ MasterType VirtualPath="~/NewSiteLead.master" %>
<%@ Reference VirtualPath="~/NewSite.master" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script src="../Scripts/Advantage.Client.AD.js"></script>
<script src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeadNestedMasterPageContent" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
				<!-- begin rightcolumn -->
				<tr>
					<td class="detailsframetop">
                         <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right"><asp:button id="btnSave" runat="server" Enabled="False" CssClass="save" Text="Save"></asp:button><asp:button id="btnNew" runat="server" Enabled="False" CssClass="new" Text="New" CausesValidation="False"></asp:button><asp:button id="btnDelete" runat="server" Enabled="False" CssClass="delete" Text="Delete" CausesValidation="False"></asp:button></td>
								
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
                        <table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="98%"
                            border="0">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                       	<asp:datagrid id="dgrdTransactionSearch" Runat="server" BorderColor="#E0E0E0" AllowSorting="True"
																			AutoGenerateColumns="False" BorderStyle="Solid" ShowFooter="False" BorderWidth="1px" width="100%">
																			<AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
																			<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																			<HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
																			<FooterStyle CssClass="datagridpadding"></FooterStyle>
																			<EditItemStyle CssClass="datagridpadding"></EditItemStyle>
																			<Columns>
																				<asp:TemplateColumn HeaderText="Test">
																					<HeaderStyle CssClass="datagridheaderstyle" Width="30%"></HeaderStyle>
																					<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																					<ItemTemplate>
																						<asp:Label id="lblEntrTestDescrip" Text='<%# Container.DataItem("Descrip") %>' cssClass="Label" Runat="server">
																						</asp:Label>
																						<asp:Label id="lblEntrTestId" Text='<%# Container.DataItem("adReqId") %>' cssClass="Label" Runat="server" Visible=false>
																						</asp:Label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Required">
																					<HeaderStyle CssClass="datagridheaderstyle" Width="5%"></HeaderStyle>
																					<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																					<ItemTemplate>
																						<asp:checkbox id="chkRequired" Checked='<%# Container.DataItem("Required") %>' enabled=False cssClass="checkboxstyle" Runat="server">
																						</asp:checkbox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Pass">
																				<HeaderStyle CssClass="datagridheaderstyle" Width="5%"></HeaderStyle>
																					<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																					<ItemTemplate>
																						<asp:checkbox id="chkPass" Checked='<%# Container.DataItem("Pass") %>' enabled=False cssClass="checkboxstyle" Runat="server">
																						</asp:checkbox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Completed Date">
																				<HeaderStyle CssClass="datagridheaderstyle" Width="15%"></HeaderStyle>
																					<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																					<ItemTemplate>
																						<asp:textbox id="txtTestTaken" Text='<%# Container.DataItem("TestTaken") %>' cssClass="textbox" Runat="server">
																						</asp:textbox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Actual Score">
																				<HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
																					<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																					<ItemTemplate>
																						<asp:textbox id="txtActualScore" Text='<%# Container.DataItem("ActualScore") %>' enabled=true cssClass="textbox" Runat="server">
																						</asp:textbox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Min Score">
																				<HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
																					<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																					<ItemTemplate>
																						<asp:label id="lblMinScore" Text='<%# Container.DataItem("MinScore") %>' cssClass="label" Runat="server">
																						</asp:label>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Comments">
																				<HeaderStyle CssClass="datagridheaderstyle" Width="15%"></HeaderStyle>
																					<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																					<ItemTemplate>
																						<asp:textbox id="txtComments" Text='<%# Container.DataItem("Comments") %>' cssClass="Textbox" Runat="server">
																						</asp:textbox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:TemplateColumn HeaderText="Override">
																				<HeaderStyle CssClass="datagridheaderstyle" Width="5%"></HeaderStyle>
																					<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																					<ItemTemplate>
																						<asp:checkbox id="chkOverRide" Checked='<%# Container.DataItem("OverRide") %>' enabled=true cssClass="checkboxstyle" Runat="server">
																						</asp:checkbox>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																			</Columns>
													</asp:datagrid>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
			<!-- start validation panel-->
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

