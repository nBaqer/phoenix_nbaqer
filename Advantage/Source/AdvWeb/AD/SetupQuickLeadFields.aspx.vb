
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class SetupQuickLeadFields
    Inherits BasePage

#Region " Web Form Designer Generated Code "
    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents reqAvailablePages As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ResourceId As String
    Protected moduleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        moduleId = HttpContext.Current.Request.Params("Mod").ToString
        Dim mContext As HttpContext
        mContext = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            mContext.Items("Language") = "En-US"
            mContext.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)
        'Check if this page still exists in the menu while switching campus
        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
        InitButtonsForLoad()
        If Not Page.IsPostBack Then
            BuildDdl()
        End If
        headerTitle.Text = Header.Title
    End Sub
    Private Sub BuildRequiredFields()
        'Bind the Document DropDownList
        Dim quickLeadsList As New QuickLeadFacade
        Dim intCount As Integer
        intCount = quickLeadsList.GetLeadCount()
        If intCount >= 1 Then
            With lstSelectedFields
                .DataTextField = "fullname"
                .DataValueField = "LeadId"
                .DataSource = quickLeadsList.GetAllLeadFields()
                .DataBind()
            End With
        Else
            With lstSelectedFields
                .DataTextField = "fullname"
                .DataValueField = "fldId"
                .DataSource = quickLeadsList.GetAllRequiredStudentFields()
                .DataBind()
            End With
        End If
    End Sub
    Private Sub BuildNotRequiredFields()
        'Bind the Document DropDownList
        Dim quickLeadsList As New QuickLeadFacade
        Dim intCount As Integer
        intCount = quickLeadsList.GetLeadCount()
        If intCount >= 1 Then
            With lstAvailableFields
                .DataTextField = "Caption"
                .DataValueField = "fldid"
                .DataSource = quickLeadsList.GetAllNotRequiredLeadFields()
                .DataBind()
            End With
        Else
            With lstAvailableFields
                .DataTextField = "Caption"
                .DataValueField = "fldId"
                .DataSource = quickLeadsList.GetAllNotRequiredStudentFields()
                .DataBind()
            End With
        End If
    End Sub
    Private Sub BuildDdl()
        BuildRequiredFields()
        BuildNotRequiredFields()
    End Sub
    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim strSelectedText As String
        Dim strSelectedValue As String
        'Dim strVisibiltyText As String
        'Dim strVisibiltyValue As String
        'Dim strPage_Visibilty As String

        If lstAvailableFields.SelectedIndex = -1 Then
            btnAdd.CausesValidation = True
            Exit Sub
        Else
            btnAdd.CausesValidation = False
        End If

        strSelectedText = lstAvailableFields.SelectedItem.Text
        strSelectedValue = lstAvailableFields.SelectedItem.Value
        Dim i, count As Integer
        If lstSelectedFields.Items.Count >= 1 Then
            For i = 0 To lstSelectedFields.Items.Count - 1
                If lstSelectedFields.Items(i).Text = strSelectedText Then
                    count = 1
                End If
            Next
            If Not (count = 1) Then
                lstAvailableFields.Items.RemoveAt(lstAvailableFields.SelectedIndex)
                lstSelectedFields.Items.Add(New ListItem(strSelectedText, strSelectedValue))
            End If
        Else
            lstSelectedFields.Items.Add(New ListItem(strSelectedText, strSelectedValue))
            lstAvailableFields.Items.RemoveAt(lstAvailableFields.SelectedIndex)
        End If
        btnAdd.CausesValidation = True
    End Sub
    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        '   Dim strRemoveItem As String
        Dim StrSelectedText As String
        Dim strSelectedValue As String
        Dim strRequired As String
        '    Dim instrValue As String
        Dim intCount As Integer

        btnAdd.CausesValidation = False
        If lstSelectedFields.Items.Count = 0 Then
        End If
        If (lstSelectedFields.SelectedIndex = -1) Then
        Else
            intCount = InStr(lstSelectedFields.SelectedItem.Text, "-") + 1
            strRequired = Mid(lstSelectedFields.SelectedItem.Text, intCount)
            If strRequired = "Required" Then
                DisplayErrorMessage("The selected item is required and cannot be removed")
                Exit Sub
            Else
                StrSelectedText = lstSelectedFields.SelectedItem.Text
                strSelectedValue = lstSelectedFields.SelectedItem.Value
                lstAvailableFields.Items.Add(New ListItem(StrSelectedText, strSelectedValue))
                lstSelectedFields.Items.RemoveAt(lstSelectedFields.SelectedIndex)
            End If
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'Set error condition
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsave.Click
        UpdateModules()
    End Sub
    Private Sub lstAvailableFields_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstAvailableFields.SelectedIndexChanged
    End Sub
    Private Sub lstSelectedFields_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSelectedFields.SelectedIndexChanged
    End Sub
    Private Sub lstSelectedFields_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstSelectedFields.PreRender
    End Sub
    Private Sub UpdateModules()
        'Create an array string with Selected Jobs Offered. The initial size of the array is the number of items in the chkJobsCatId
        Dim selectedDegrees() As String = Array.CreateInstance(GetType(String), lstSelectedFields.Items.Count)
        Dim selectedText() As String = Array.CreateInstance(GetType(String), lstSelectedFields.Items.Count)

        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim item As ListItem
        'Dim intOccurance As String
        'Dim intOccuranceValue As Integer
        'Dim strVal As String
        'Dim itemtext As ListItem

        'In For Loop Check The Number of Items Selected
        For Each item In lstSelectedFields.Items
            selectedDegrees.SetValue(item.Value.ToString, i)
            'If InStr(item.Text.ToString, "-") >= 1 Then
            'selectedText.SetValue(Mid(item.Text.ToString, 1, InStr(item.Text.ToString, "-") - 1), i)
            'Else
            selectedText.SetValue(item.Text.ToString, i)
            'End If
            i += 1
        Next


        'resize the array
        If i > 0 Then ReDim Preserve selectedDegrees(i - 1)
        If i > 0 Then ReDim Preserve selectedText(i - 1)

        'update Selected Jobs
        Dim QuickLead As New QuickLeadFacade
        QuickLead.UpdateModules(Session("UserName"), selectedDegrees, selectedText)
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnsave)
        controlsToIgnore.Add(btnAdd)
        controlsToIgnore.Add(btnRemove)
        'Add javascript code to warn the user about non saved changes 
        '        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BIndToolTip()
    End Sub
    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
            btnsave.Enabled = True
        Else
            btnsave.Enabled = False
        End If

        btnnew.Enabled = False
        btnDelete.Enabled = False
    End Sub

    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ' ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub
End Class
