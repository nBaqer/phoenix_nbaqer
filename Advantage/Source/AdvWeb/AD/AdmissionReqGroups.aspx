<%@ Page Language="vb" AutoEventWireup="false"  Title="Requirement Groups" MasterPageFile="~/NewSite.master" Inherits="AdmissionReqGroups" CodeFile="AdmissionReqGroups.aspx.vb" %>
<%@ mastertype virtualpath="~/NewSite.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/AuditHist.js" type="text/javascript"></script>
		   <script type="text/javascript">

			   function OldPageResized(sender, args) {
				   $telerik.repaintChildren(sender);
			   }

   </script>
   </asp:Content>
   <asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
  <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
	</asp:ScriptManagerProxy>
	  <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
	VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" style="overflow:auto;">
	<telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">

		<table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
			<!-- begin leftcolumn -->
			
						<tr>
							<td class="listframetop">
								<table cellspacing="0" cellpadding="0" width="100%" border="0">
									<tr>
										<td nowrap align="left" width="15%">
											<asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
										<td nowrap width="85%">
											<asp:RadioButtonList ID="radStatus" CssClass="label" AutoPostBack="true" runat="Server"
												RepeatDirection="Horizontal">
												<asp:ListItem Text="Active" Selected="True" />
												<asp:ListItem Text="Inactive" />
												<asp:ListItem Text="All" />
											</asp:RadioButtonList></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="listframebottom">
								<div class="scrollleftfilters">
									<asp:DataList ID="dlstAdmissionReqGroups" runat="server" Width="100%" DataKeyField="ReqGrpId">
										<SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
										<ItemStyle CssClass="itemstyle"></ItemStyle>
										<ItemTemplate>
											<asp:ImageButton ID="imgInActive" ImageUrl="../images/Inactive.gif" runat="server"
												CausesValidation="False" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>'
												CommandArgument='<%# Container.DataItem("ReqGrpId")%>'></asp:ImageButton>
											<asp:ImageButton ID="imgActive" ImageUrl="../images/Active.gif" runat="server" CausesValidation="False"
												Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' CommandArgument='<%# Container.DataItem("ReqGrpId")%>'>
											</asp:ImageButton>
											<asp:LinkButton ID="Linkbutton1" runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("ReqGrpId")%>'
												Text='<%# Container.DataItem("Descrip") & "" & CheckIfMandatory(Ctype(Container.DataItem("IsMandatoryReqGrp"), integer)) %> '
												CausesValidation="False" />
										</ItemTemplate>
									</asp:DataList></div>
							</td>
						</tr>
					</table>
			
				</telerik:RadPane>
	<telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
	<table cellSpacing="0" cellPadding="0" width="100%" border="0">
	<tr>

				<!-- end leftcolumn -->
				<!-- begin rightcolumn -->
	
				<td class="detailsframetop">
					<asp:Panel ID="pnlRHS" runat="server">
						<table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right">
									<asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
									<asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False">
									</asp:Button>
									<asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
									</asp:Button></td>
								
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->

						<!--begin right column-->
						<table class="maincontenttable" id="Table5" cellspacing="0" cellpadding="0" width="100%"
							border="0">
							<tr>
								<td class="detailsframe">
									<div class="boxContainer">
									    <h3><%=Header.Title  %></h3>
										<!-- begin table content-->
										<!-- Two column template with no spacer in between cells -->
										<!-- used in placement module for maintenance pages -->
										<table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
											<asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
											<asp:TextBox ID="txtModUser" runat="server" Visible="False"></asp:TextBox>
											<asp:TextBox ID="txtModDate" runat="server" Visible="False"></asp:TextBox>
										   <tr>
															<td class="spacertables"></td>
													</tr>
											<tr>
												<td class="twocolumnlabelcell">
													<asp:Label ID="lblCode" runat="server" CssClass="label"></asp:Label></td>
												<td class="twocolumncontentcell">
													<asp:TextBox ID="txtCode" runat="server" CssClass="textbox" TabIndex="1"></asp:TextBox></td>
											</tr>
											<tr>
															<td class="spacertables"></td>
													</tr>
											<tr>
												<td class="twocolumnlabelcell">
													<asp:Label ID="lblStatusId" runat="server" CssClass="label"></asp:Label></td>
												<td class="twocolumncontentcell">
													<asp:DropDownList ID="ddlStatusId" runat="server" CssClass="dropdownlist" TabIndex="2">
													</asp:DropDownList></td>
											</tr>
											<tr>
															<td class="spacertables"></td>
													</tr>
											<tr>
												<td class="twocolumnlabelcell">
													<asp:Label ID="lblDescrip" runat="server" CssClass="label"></asp:Label></td>
												<td class="twocolumncontentcell">
													<asp:TextBox ID="txtDescrip" runat="server" CssClass="textbox" TabIndex="3"  MaxLength="50"></asp:TextBox></td>
											</tr>
											<tr>
															<td class="spacertables"></td>
													</tr>
											<tr>
												<td class="twocolumnlabelcell">
													<asp:Label ID="lblCampGrpId" runat="server" CssClass="label"></asp:Label></td>
												<td class="twocolumncontentcell">
													<asp:DropDownList ID="ddlCampGrpId" runat="server" CssClass="dropdownlist" TabIndex="4">
													</asp:DropDownList>
													</td>
											</tr>
											<tr>
															<td class="spacertables"></td>
													</tr>
											<tr>
												<td class="twocolumnlabelcell">
												</td>
												<td class="twocolumnlabelcell">
													<asp:CheckBox ID="chkIsMandatoryReqGrp" runat="server" CssClass="checkbox" Text="Is the Mandatory Requirement Group"
														AutoPostBack="False" TabIndex="5" Visible="False"></asp:CheckBox></td>
											</tr>
											<tr>
												<td class="twocolumnlabelcell">
													<asp:TextBox ID="txtReqGrpId" runat="server" Visible="false"></asp:TextBox></td>
												<td class="twocolumncontentcell">
												</td>
											</tr>
											<tr>
												<td class="contentcell" align="center" colspan="2">
													<table width="100%" align="center" border="0">
														<tr>
															<td>
																<asp:DataGrid ID="dgrdLeadGroups" TabIndex="6" runat="server" Visible="True" BorderColor="#E0E0E0"
																	AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" ShowFooter="True"
																	BorderWidth="1px" Width="100%">
																	<AlternatingItemStyle CssClass="datagridalternatingstyle"></AlternatingItemStyle>
																	<ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																	<HeaderStyle CssClass="datagridheaderstyle"></HeaderStyle>
																	<FooterStyle CssClass="datagriditemstyle"></FooterStyle>
																	<EditItemStyle CssClass="datagriditemstyle"></EditItemStyle>
																	<Columns>
																		<asp:TemplateColumn HeaderText="Lead Group">
																		<HeaderStyle CssClass="DataGridHeaderStyle" Width="45%"></HeaderStyle>
																		<ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
																			<ItemTemplate>
																				<asp:Label ID="lblLeadGrpDescrip" runat="server" CssClass="label" Text='<%# Ctype(Container.DataItem, System.Data.DataRowView).Row.GetParentRow("LeadGroupsLeadGrpReqGrp")("LeadGrpDescrip") %>'> </asp:Label>
																				<asp:Label ID="lblLeadGrpId" runat="server" Text='<%# Container.DataItem("LeadGrpId") %>'
																					Visible="False"> </asp:Label>
																			</ItemTemplate>
																			<FooterStyle CssClass="datagriditemstyle"></FooterStyle>
																			<FooterTemplate>
																				<asp:DropDownList ID="ddlFooterLeadGrpId" CssClass="dropdownlist" runat="server">
																				</asp:DropDownList>
																			</FooterTemplate>
																			<EditItemTemplate>
																				<asp:DropDownList ID="ddlEditLeadGrpId" runat="server" CssClass="dropdownlist">
																				</asp:DropDownList>
																				<asp:Label ID="lblEditLeadGrpId" runat="server" Visible="False" Text='<%# Container.DataItem("LeadGrpId") %>'> </asp:Label>
																			</EditItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn HeaderText="Number of Requirements">
																			<HeaderStyle CssClass="datagridheaderstyle" Width="45%"></HeaderStyle>
																	   <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																			<ItemTemplate>
																				<asp:Label ID="lblNumReqs" runat="server" CssClass="label" Text='<%# Container.DataItem("NumReqs") %>'> </asp:Label>
																			</ItemTemplate>
																			<FooterStyle CssClass="datagriditemstyle"></FooterStyle>
																			<FooterTemplate>
																				<asp:TextBox ID="txtFooterNumReqs" CssClass="TextBox" runat="server"></asp:TextBox>
																			</FooterTemplate>
																			<EditItemTemplate>
																				<asp:TextBox ID="txtEditNumReqs" runat="server" CssClass="textbox" Text='<%# Container.DataItem("NumReqs") %>'> </asp:TextBox>
																			</EditItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn>
																			<HeaderStyle CssClass="datagridheaderstyle" Width="10%"></HeaderStyle>
																	   <ItemStyle CssClass="datagriditemstyle"></ItemStyle>
																			<ItemTemplate>
																				<asp:LinkButton ID="lnlButEdit" Text="<img border=0 src=../images//im_edit.gif alt= edit>"
																					CausesValidation="False" runat="server" CommandName="Edit"> <img border="0" src="../images/im_edit.gif" alt="edit"></asp:LinkButton>
																			</ItemTemplate>
																			<FooterStyle CssClass="datagriditemstyle"></FooterStyle>
																			<FooterTemplate>
																				<asp:Button ID="btnAddRow"  Text="Add" runat="server" CommandName="AddNewRow">
																				</asp:Button>
																			</FooterTemplate>
																			<EditItemTemplate>
																				<asp:LinkButton ID="lnkbutUpdate" Text="<img border=0 src=../images/im_update.gif alt=update>"
																					runat="server" CommandName="Update"> <img border="0" src="../images/im_update.gif" alt="update"></asp:LinkButton>
																				<asp:LinkButton ID="lnkbutDelete" Text="<img border=0 src=../images/delete.gif alt=Delete>"
																					CausesValidation="False" runat="server" CommandName="Delete"></asp:LinkButton>
																				<asp:LinkButton ID="lnkbutCancel" Text="<img border=0 src=../images//im_delete.gif alt=Cancel>"
																					CausesValidation="False" runat="server" CommandName="Cancel"></asp:LinkButton>
																			</EditItemTemplate>
																		</asp:TemplateColumn>
																		<asp:TemplateColumn Visible="False" HeaderStyle-Width="0">
																			<ItemTemplate>
																				<asp:Label ID="lblLeadGrpReqGrpId" runat="server" Width="0" Text='<%# Container.DataItem("LeadGrpReqGrpId") %>'
																					Visible="False"> </asp:Label>
																			</ItemTemplate>
																			<EditItemTemplate>
																				<asp:Label ID="lblEditLeadGrpReqGrpId" runat="server" Width="0" Text='<%# Container.DataItem("LeadGrpReqGrpId") %>'
																					Visible="False"> </asp:Label>
																			</EditItemTemplate>
																		</asp:TemplateColumn>
																	</Columns>
																	<EditItemStyle></EditItemStyle>
																</asp:DataGrid></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="twocolumnlabelcell">
													&nbsp;</td>
												<td class="twocolumncontentcell" style="text-align: Left">
													<asp:button ID="lnkReqGrpIdDef" TabIndex="7" runat="server"  Text="Requirement Group Definition" /></td>
											</tr>
										</table>
										<!--end table content-->
										<asp:TextBox ID="txtRowIds" Style="visibility: hidden" runat="server" CssClass="label"
											Width="10%"></asp:TextBox>
										<asp:TextBox ID="txtResourceId" Style="visibility: hidden" runat="server" CssClass="label"
											Width="10%"></asp:TextBox>
									</div>
								</td>
							</tr>
						</table>
					</asp:Panel>
				</td>
				<!-- end rightcolumn -->
			</tr>
		</table>
	 
	</telerik:RadPane>
	</telerik:RadSplitter>
		<!-- start validation panel-->
		<asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
		</asp:Panel>
		<asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
			Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
				runat="server">
			</asp:Panel>
		<asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
			ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
		<!--end validation panel-->
		</asp:Content>