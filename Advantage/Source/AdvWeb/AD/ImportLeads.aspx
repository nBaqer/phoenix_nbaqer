﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ImportLeads.aspx.vb" Inherits="ImportLeads" %>
<%@ MasterType  virtualPath="~/NewSite.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
 <style type="text/css">
   h2 { font: bold 11px verdana; color: #000066; background-color: transparent; width: auto; margin: 0; padding: 0; border: 0;text-align: left;}
   .FileInput
    {
	    margin: 4px;
	    vertical-align: middle;
	    font: normal 10px verdana;
	    text-align:left;
	    color: #000066;
	    width: 300px;
    }
    .ProcessFile
    {
	    margin: 4px;
	    vertical-align: middle;
	    font: normal 10px verdana;
	    text-align:center;
	    color: #000066;
    }
   </style>
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" CausesValidation="false"
                                        Enabled="false"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"
                                        Enabled="false"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"
                                        Enabled="false"></asp:Button>
                                </td>
                               
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="detailsframe">
                                    <div class="scrollsingleframe">
                                        <!--begin content here-->
                                        <asp:Table runat="server" ID="tbSelectFile" Width="100%">
                                            <asp:TableRow>
                                                <asp:TableCell Width="20%" HorizontalAlign="right">
                                                    <asp:Label ID="lblHeading" CssClass="labelbold" runat="server">Leads to Import</asp:Label>
                                                </asp:TableCell>
                                                <asp:TableCell Width="60%" HorizontalAlign="left">
                                                    <asp:FileUpload ID="uploadExcelFile" runat="server" CssClass="textbox" />
                                                </asp:TableCell>
                                                <asp:TableCell Width="20%">
                                                    <asp:Button ID="btnUpload" runat="server" Text="Import Leads">
                                                    </asp:Button>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                        <br />
                                        <table width="100%">
                                            <tr>
                                                <td width="20%">
                                                    &nbsp;
                                                </td>
                                                <td width="60%" nowrap>
                                                    <asp:RadioButtonList ID="radAssignLeads" runat="server" CssClass="labelbold">
                                                        <asp:ListItem Value="0" Text="Assign leads to the Current Admission Rep" Selected></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Distribute leads to the Currently logged in Admission Reps"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                                <td width="20%">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                        <p>
                                        </p>
                                        <table class="datagridheader" width="100%" visible="false">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="Label1" runat="server" CssClass="labelbold">Number of leads assigned</asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                        <p>
                                        </p>
                                        <asp:DataGrid ID="DataGrid1" CellPadding="0" BorderWidth="1px" BorderStyle="Solid"
                                            BorderColor="#E0E0E0" AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true"
                                            EditItemStyle-Wrap="false" GridLines="Horizontal" Width="100%" runat="server">
                                            <FooterStyle CssClass="datagriditemstyle"></FooterStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Admissions Rep">
                                                    <HeaderStyle CssClass="headerstyle" Width="10%" />
                                                    <ItemStyle CssClass="itemstyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAssignedTo" runat="server" Text='<%# Container.DataItem("FullName") %>' />&nbsp;
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Number of leads assigned">
                                                    <HeaderStyle CssClass="headerstyle" Width="10%" />
                                                    <ItemStyle CssClass="itemstyle" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLeadCount" runat="server" Text='<%# Container.DataItem("ImportedLeadCount") %>' />&nbsp;
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                        <!--end content here-->
                                        <!-- end footer -->
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

