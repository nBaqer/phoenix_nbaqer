﻿Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports BO = Advantage.Business.Objects

Partial Class AdvDetailPage
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo
    Protected campusId As String
    Private mContext As HttpContext
    Protected state As AdvantageSessionState
#Region "Page Events"
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
    End Sub
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim resourceId As Integer

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = Master.CurrentCampusId

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)
        CampusSecurityCheck()
    End Sub
    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        'save current State
        CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)
    End Sub
#End Region
#Region "Switch Campus Logic"
    Public Sub CampusSecurityCheck()
       If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If
    End Sub
#End Region
End Class
