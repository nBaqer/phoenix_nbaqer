﻿
Partial Class ChangePassword
    Inherits BasePage
    Dim userId As String
    Dim userName As String
    Dim user1 As MembershipUser
    Protected Sub Login_Click(sender As Object, e As EventArgs)
        Response.Redirect(FormsAuthentication.LoginUrl)
    End Sub
    Protected Sub ChangePasswordPushButton_Click(sender As Object, e As EventArgs)
        Try
            'If Not Session("TenantUser") Is Nothing Then
            If Not IsNothing(Session("UserId")) Then
                'Dim passRegex As New Regex("^(?=.*\d).{8,8}$")
                'Dim passRegex As New Regex("^.*(?=.{8,})(?=.*[\d])(?=.{2,}[\W]).*$")
                'Dim passRegex As New Regex("^.*(?=.{8,})(?=.*[a-z])(?=.*[\d])(?=.*[\W]{2,}).*$")
                Dim minnonalphanumeric As Int16 = Membership.Provider.MinRequiredNonAlphanumericCharacters
                Dim minlength As Int16 = Membership.Provider.MinRequiredPasswordLength
                'Dim passRegex As New Regex("^.*(?=.{" & minlength & ",})(?=.*[a-z])(?=.*[\d])(?=.*[\W]{" & minnonalphanumeric & ",}).*$")
                Dim passRegex As New Regex(Membership.Provider.PasswordStrengthRegularExpression)
                Dim newPass As String = CType(FindControlRecursive("NewPassword"), TextBox).Text
                If Not passRegex.IsMatch(newPass) Then
                    'ChangePassword1.ChangePasswordFailureText = "Password incorrect or New Password invalid. New Password length minimum: " & minlength & ". Non-alphanumeric characters required: " & minnonalphanumeric & ". Digits required: 1."
                    ChangePassword1.ChangePasswordFailureText = "Password incorrect or New Password invalid. New Password length minimum: " & minlength & ". Special characters required: " & minnonalphanumeric & ". Numeric characters required: 1."
                    Exit Sub
                End If
                userId = Session("UserId").ToString
                userName = (New ManageSecurityQuestions).GetUserName(userId)
                user1 = Membership.GetUser(userName)
                Dim currentPassword As String = CType(FindControlRecursive("CurrentPassword"), TextBox).Text  'CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("CurrentPassword"), TextBox).Text
                Dim newPassword As String = CType(FindControlRecursive("NewPassword"), TextBox).Text 'CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("NewPassword"), TextBox).Text
                Dim boolSuccess As Boolean = user1.ChangePassword(currentPassword, newPassword)
                If boolSuccess Then
                    Try
                        'Store the security question and answer
                        Dim newPasswordQuestion As String = CType(FindControlRecursive("ddlSecurityQuestion"), DropDownList).SelectedItem.Text  'CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("ddlSecurityQuestion"), DropDownList).SelectedItem.Text
                        Dim newPasswordAnswer As String = CType(FindControlRecursive("txtSecurityAnswer"), TextBox).Text  'CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("txtSecurityAnswer"), TextBox).Text
                        Dim boolSecurity As Boolean = False
                        boolSecurity = user1.ChangePasswordQuestionAndAnswer(newPassword, newPasswordQuestion, newPasswordAnswer)
                        Dim strURL As String = "~/dash.aspx?resid=264&mod=AR&cmpid=" & Master.CurrentCampusId & "&desc=dashboard&VSI=180174496&VID=1&UserId=" & userId.ToString
                        Response.Redirect(strURL)
                    Catch ex As System.Threading.ThreadAbortException
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                    Catch ex As Exception
                     	Dim exTracker = new AdvApplicationInsightsInitializer()
                    	exTracker.TrackExceptionWrapper(ex)

                        Dim url As String = HttpContext.Current.Server.MapPath("ErrorPage.aspx")
                        HttpContext.Current.Response.Redirect(url)
                    End Try
                Else
                    ChangePassword1.ChangePasswordFailureText = "Password incorrect or New Password invalid. New Password length minimum: " & minlength & ". Special characters required: " & minnonalphanumeric & ". Numeric characters required: 1."
                End If
            End If
            'End If
        Catch ex As System.Threading.ThreadAbortException
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim url As String = HttpContext.Current.Server.MapPath("ErrorPage.aspx")
            HttpContext.Current.Response.Redirect(url)
        End Try
    End Sub
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            userId = Session("UserId").ToString
            userName = (New ManageSecurityQuestions).GetUserName(userId)
            CType(FindControlRecursive("UserName"), Label).Text = userName
            'CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("UserName"), Label).Text = userName
            BuildSecurityQuestion()
        End If
    End Sub
    Private Sub BuildSecurityQuestion()
        Dim ddlSQ As New DropDownList
        ddlSQ = CType(FindControlRecursive("ddlSecurityQuestion"), DropDownList) 'CType(Page.FindControl("ChangePassword1").Controls(0).FindControl("ddlSecurityQuestion"), DropDownList)
        Dim objDBUser As New ManageSecurityQuestions
        With ddlSQ
            .DataSource = objDBUser.GetSecurityQuestions()
            .DataTextField = "Description"
            .DataValueField = "Id"
            .DataBind()
        End With
    End Sub
End Class
