﻿<%@ Application Language="VB" %>
<%@ Import Namespace="System.Web.Configuration" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="FAME.Advantage.Site.Lib.Infrastruct.Bootstrapping" %>
<%@ Import Namespace="System.Web.Http" %>
<%@ Import Namespace="log4net" %>
<%@ Import Namespace="AdvWeb.VBCode.Bundling" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="Advantage.Business.Objects" %>
<%@ Import Namespace="FAME.Advantage.Common" %>
<%@ Import Namespace="FAME.Advantage.Site.Lib.Infrastruct.Initializer" %>
<%@ Import Namespace="Microsoft.ApplicationInsights.Extensibility" %>
<%@ Import Namespace="Microsoft.ApplicationInsights" %>
<%@ Import Namespace="StackExchange.Profiling.MiniProfiler" %>

<script RunAt="server">
    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        Bootstrapper.Bootstrap()
        RegisterRoutes(RouteTable.Routes)
        RegisterWebApiRoutes()
        BundleConfig.RegisterBundles(BundleTable.Bundles) 'Register asp.net bundling class

        'App Insights
        if (Not WebConfigurationManager.AppSettings("InstrumentationKey") is nothing) then
            TelemetryConfiguration.Active.InstrumentationKey = WebConfigurationManager.AppSettings("InstrumentationKey").ToString()
            Dim initializer = New AppInsightsTelemetryInitializer()
            TelemetryConfiguration.Active.TelemetryInitializers.Add(initializer)
        else
            TelemetryConfiguration.Active.DisableTelemetry = true
        end If


    End Sub


    sub Application_BeginRequest()
        Try
            If (Request.IsLocal) Then
                StackExchange.Profiling.MiniProfiler.Start()
            End If

        Catch ex As Exception

        End Try
    End sub
    sub Application_EndRequest()
        Try
            StackExchange.Profiling.MiniProfiler.Stop()

        Catch ex As Exception

        End Try
    End sub
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)

        Dim ctx As HttpContext = HttpContext.Current
        Dim exception As Exception = ctx.Server.GetLastError()
        Dim logger = LogManager.GetLogger(Me.GetType())
        logger.Error(exception)

        Try
            'App Insights'
            If (ctx.IsCustomErrorEnabled AndAlso Server.GetLastError() IsNot Nothing)
                Dim advInit = new AdvApplicationInsightsInitializer()
                advInit.TrackExceptionWrapper(exception, ctx)
            End If
        Catch ex As Exception
            '_log.Info("END Application_Start")
        End Try

        ' This happen when in some page a action button in press before
        ' finished the page load. because the information is in the page
        ' but the page init a post-back
        If exception.Message = "The operation was canceled." Then Return

        Application("ErrorPage") = Request.Url.ToString
        'jguirado 9/11/2013 Avoid exception because the session are not available
        'Avoid exception if exception is Nothing.
        'Code refactoring to avoid duplicate code
        'NOTE: In Original code if 404 or 403 was found the subroutine finished.
        'NOTE: This will revised with Balaji, about it is Ok or NOT. if not delete the return statament.
        If Not TryCast(Server.GetLastError(), HttpException) Is Nothing Then
            Dim ex As HttpException
            ex = TryCast(Server.GetLastError(), HttpException)
            Dim code As String = CType(ex.GetHttpCode(), String)
            If code = "404" OrElse code = "403" Then
                Application("ErrorCode") = code

            End If
        End If

        If Not Context.Session Is Nothing Then
            Session("Error") = exception
        End If
        '................           
        If exception Is Nothing OrElse exception.Message Is Nothing Then
            ' Response.Redirect("~/ErrorPage.aspx")
            ctx.ClearError()
            Return
        End If

        If exception.Message <> "The operation was canceled." Then
            Response.Redirect("~/ErrorPage.aspx?Message=" & Server.UrlEncode(exception.Message.ToString))
        End If

        ctx.ClearError()
    End Sub

    Sub Application_AcquireRequestState(ByVal sender As Object, ByVal e As EventArgs)

    End Sub


    Sub RegisterRoutes(ByVal routes As RouteCollection)
        'RouteName, Url with parameters, Web Forms page to handle it
        routes.MapPageRoute(
            "dashboard-view",
            "dash/{dashname}",
            "~/dash.aspx"
            )
    End Sub

    Private Shared Sub RegisterWebApiRoutes()
        RouteTable.Routes.MapHttpRoute(
            name:="ProxyRoute",
            routeTemplate:="proxy/{*route}",
            defaults:=New With {.controller = "Proxy", .route = "route"}).RouteHandler = New SessionStateRouteHandler()
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        'Fires at the beginning of each request 
        'Get the ResourceId of the page from the querystring
        Dim intResId As Integer
        Dim strLang As String


        Try
            If Not Request.Params("resid") Is Nothing Then
                intResId = CInt(Request.Params("resid"))
                Context.Items.Add("ResourceId", intResId)
                strLang = Request.UserLanguages(0)
                Context.Items.Add("Language", strLang)
            End If

        Catch ex As Exception
        End Try

        '   there is a bug in Adobe Reader or ASP.NET. 
        '   the execution of instruction Request.UserLanguages(0) will force Adobe Reader
        '   to produce this error: "Adobe Reader could not open file "xxxxxx.fdf" because it is
        '   either a not supported file type or because the file has been corrupted..." 
        '   The temporary fix is to avoid using Request.UserLanguages(0) when delivering PDF pages.
        '   Anatoly 3/15/2004.
        '   Get the culture.
    End Sub

    Private Sub Global_PreRequestHandlerExecute(ByVal sender As Object, ByVal e As EventArgs) Handles MyBase.PreRequestHandlerExecute

        'App Insights'
        If HttpContext.Current IsNot Nothing Then
            Try
                Dim advApplicationInsightsInitializer As New AdvApplicationInsightsInitializer()
                advApplicationInsightsInitializer.SetProperties(HttpContext.Current)

            Catch ex As Exception
                '_log.Info("END Application_Start")
            End Try
        End If

        Dim preCompile As String = Request("PreCompile")

        'If request is for Pre-Compile, just return...    
        If Not (preCompile Is Nothing) Then
            Response.End()
        End If
    End Sub


</script>
