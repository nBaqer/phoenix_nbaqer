﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="firstPage.aspx.vb" Inherits="firstPage" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="App_Themes/Blue_Theme/Blue_Theme.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
<telerik:RadScriptManager ID="RadScriptManager1" Runat="server">
</telerik:RadScriptManager>
<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="Button2">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="Panel1" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
<telerik:RadSkinManager ID="RadSkinManager1" Runat="server" >
</telerik:RadSkinManager>
<telerik:RadToolTipManager ID="RadToolTipManager1" runat="server" AutoTooltipify="true" Position="TopCenter" ShowCallout="false">
</telerik:RadToolTipManager>
<telerik:RadFormDecorator ID="RadFormDecorator1" Runat="server" DecoratedControls="Default, ValidationSummary"/>
<telerik:RadWindowManager ID="RadWindowManager1" runat="server">
</telerik:RadWindowManager>
       <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
           <br />
               <br />
    <div>
      Page Theme:  <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
    </div>

    <div>
       Alert width: <asp:TextBox ID="TextBox2" runat="server">400</asp:TextBox>
    </div>
       <div>
       Alert height: <asp:TextBox ID="TextBox3" runat="server">250</asp:TextBox>
    </div>    
    <asp:Button ID="Button1" runat="server" Text="Alert Postback" />
    <br />

    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"> </telerik:RadAjaxLoadingPanel>
    <asp:Panel ID="Panel1" runat="server" Width="450px" Height="200px" 
        BackColor="#F1F1F8">
      <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    </asp:Panel>
    <div>
     <asp:Button ID="Button2" runat="server" Text="Alert Callback" />

                             <telerik:RibbonBarApplicationMenu ID="RadRibbonBarApplicationMenu1" runat="server" Text="File">
                            <Items>
                                <telerik:RibbonBarApplicationMenuItem Text="New" ImageUrl="icons/file/New.gif" />
                                <telerik:RibbonBarApplicationMenuItem Text="Open" ImageUrl="icons/file/Open.gif" />
                                <telerik:RibbonBarApplicationMenuItem Text="Save" ImageUrl="icons/file/Save.gif" />
                                <telerik:RibbonBarApplicationMenuItem Text="Save As" ImageUrl="icons/file/SaveAs.gif" />
                                <telerik:RibbonBarApplicationMenuItem Text="Close" ImageUrl="icons/file/Close.gif" />
                            </Items>
                        </telerik:RibbonBarApplicationMenu>
                        <telerik:RadRibbonBar ID="RadRibbonBar1" runat="server" Width="200px" >
                            <telerik:RibbonBarTab Text="View">
                                <telerik:RibbonBarGroup Text="Macros">
                                    <Items>
                                        <telerik:RibbonBarSplitButton Size="Large" Text="Macros">
                                            <Buttons>
                                                <telerik:RibbonBarButton Text="View Macros"  />
                                                <telerik:RibbonBarButton Text="Record Macro..." />
                                            </Buttons>
                                        </telerik:RibbonBarSplitButton>
                                    </Items>
                                </telerik:RibbonBarGroup>
                            </telerik:RibbonBarTab>
                            <telerik:RibbonBarTab Text="Menu2">
                                <telerik:RibbonBarGroup Text="Macros">
                                    <Items>
                                        <telerik:RibbonBarSplitButton Size="Large" Text="Macros">
                                            <Buttons>
                                                <telerik:RibbonBarButton Text="View Macros"  />
                                                <telerik:RibbonBarButton Text="Record Macro..." />
                                            </Buttons>
                                        </telerik:RibbonBarSplitButton>
                                    </Items>
                                </telerik:RibbonBarGroup>
                            </telerik:RibbonBarTab>
                        </telerik:RadRibbonBar>  
        <asp:CheckBoxList ID="CheckBoxList1" runat="server" 
            RepeatDirection="Horizontal">
            <asp:ListItem>Leads</asp:ListItem>
            <asp:ListItem>Students</asp:ListItem>
        </asp:CheckBoxList>
     </div>
    </form>
</body>
</html>
