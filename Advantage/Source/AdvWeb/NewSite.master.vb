﻿
Imports Advantage.Business.Objects
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Data
Imports AdvWeb.usercontrols
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Reflection
Imports System.Diagnostics
Imports FAME.Advantage.Common
Imports Advantage.Business.Logic.Layer
Imports AdvWeb.VBCode
Imports FAME.Advantage.Api.Library.Models
Imports Newtonsoft.Json
Imports FAME.Advantage.Domain.MultiTenant.Infrastructure.API
Imports FAME.Advantage.Site.Lib.Infrastruct.Initializer
Imports Microsoft.ApplicationInsights.Extensibility
Imports FAME.Advantage.DataAccess.LINQ

Partial Public Class NewSite
    Inherits MasterPage
#Region "Class Member Variables"

    Public StatusBarState As Boolean

    Protected state As AdvantageSessionState

    Private _mruProvider As MRURoutines
    Private _masterState As MasterPageState

    Protected IntMruType As Integer = 0

    Private _resId As String
    Protected LoadGoogleAnalyticsScript As Boolean
    Protected GoogleAnalyticsCode As String
    Protected MyAdvAppSettings As AdvAppSettings

#End Region

#Region "Class Properties"
    ''' <summary>
    ''' Return Current Campus ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>Read only, this property is updated in the Init Event of Control.</remarks>
    Public Property CurrentCampusId() As String
        Get
            Dim currentcamp As String = Me.MasterPageCampusDD.CurrentCampusId
            'jagg: this session is only used by AdvAppSettings, because we dont have access to request or master from there.
            Session("CurrentCampus") = currentcamp
            Return currentcamp
        End Get
        Set(value As String)
            Session("CurrentCampus") = value
            Session("hdnSearchCampusId") = value
            Me.MasterPageCampusDD.CurrentCampusId = value
        End Set
    End Property

    ''' <summary>
    ''' Return Previous Campus ID
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>Read only, this property is updated in the Init Event of Control.</remarks>
    Public ReadOnly Property PreviusCampusId() As String
        Get
            Return Me.MasterPageCampusDD.PreviusCampusId
        End Get
    End Property

    ''' <summary>
    ''' ISSwitchedCampus property
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property IsSwitchedCampus() As Boolean
        Get
            Dim control = Request.Params.Get("__EVENTTARGET")
            Dim pos = (IsPostBack And Not (control = "btnSave"))
            If pos Then
                Return False
            End If
            Return MasterPageCampusDD.PreviusCampusId <> MasterPageCampusDD.CurrentCampusId Or (Not HttpContext.Current.Session("CampusSwitched") Is Nothing And CType(HttpContext.Current.Session("CampusSwitched"), Boolean) = True)
        End Get
    End Property

    Public Property UserId() As String
        Get
            Return hdnUserId.Value
        End Get
        Set(value As String)
            hdnUserId.Value = value
        End Set
    End Property

#End Region

#Region "Class Events"

    ''' <summary>
    ''' page init event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Init

        Dim aSettings = GetAdvAppSettings()
        _mruProvider = New MRURoutines(Me.Context, aSettings.AppSettings("AdvantageConnectionString").ToString)
        _masterState = New MasterPageState(_mruProvider, RadNotification1)



        ' some of the busines layer and db access classes are using these session variables so we put them back in 
        Session("UserId") = AdvantageSession.UserState.UserId.ToString

        ' set page resouce id
        If Int32.TryParse(Request.QueryString("resid"), PageResourceId) Then
            hdnPageResourceId.Value = PageResourceId
        End If

        If Not Request.QueryString("resid") Is Nothing Then
            _resId = Request.QueryString("resid").ToString()
            IntMruType = BusinessLogicUtilities.Utilities.GetIdentityMruType(_resId)
        Else
            IntMruType = 1
        End If

        hdnKendoPane.Value = kendoMenuPane.ClientID

        If Not AdvantageSession.UserState.UserId.IsEmpty() Then
            hdnUserId.Value = Session("UserId").ToString()
        End If

        If AdvantageSession.IsImpersonating IsNot Nothing Then
            If AdvantageSession.IsImpersonating.ToUpper() = "TRUE" Then
                hdnImpIsImpersonating.Value = AdvantageSession.IsImpersonating
            End If
        End If

        MasterPageCampusDD.SearchCampus = hdnSearchCampusId

        If (Not IsNothing(ConfigurationManager.AppSettings("InstrumentationKey"))) Then
            Try
                Dim appInsightsLibCode As New HtmlGenericControl
                Dim analyticsCode As New StringBuilder
                analyticsCode.AppendLine("  var appInsights = window.appInsights || function (a) { function b(a) { c[a] = function () { var b = arguments; c.queue.push(function () { c[a].apply(c, b) }) } } var c = { config: a }, d = document, e = window; setTimeout(function () { var b = d.createElement('script'); b.src = a.url || 'https://az416426.vo.msecnd.net/scripts/a/ai.0.js', d.getElementsByTagName('script')[0].parentNode.appendChild(b) }); try { c.cookie = d.cookie } catch (a) { } c.queue = []; for (var f = ['Event', 'Exception', 'Metric', 'PageView', 'Trace', 'Dependency']; f.length;) b('track' + f.pop()); if (b('setAuthenticatedUserContext'), b('clearAuthenticatedUserContext'), b('startTrackEvent'), b('stopTrackEvent'), b('startTrackPage'), b('stopTrackPage'), b('flush'), !a.disableExceptionTracking) { f = 'onerror', b('_' + f); var g = e[f]; e[f] = function (a, b, d, e, h) { var i = g && g(a, b, d, e, h); return !0 !== i && c['_' + f](a, b, d, e, h), i } } return c }({ instrumentationKey:'" + ConfigurationManager.AppSettings("InstrumentationKey") + "'}); window.appInsights = appInsights, appInsights.queue && 0 === appInsights.queue.length && appInsights.trackPageView();")
                appInsightsLibCode.TagName = "script"
                appInsightsLibCode.Attributes.Add("type", "text/javascript")
                appInsightsLibCode.InnerHtml = analyticsCode.ToString()
                Head1.Controls.AddAt(0, appInsightsLibCode)

                Dim appInsightsSettingsCode As New HtmlGenericControl
                Dim settingCode As New StringBuilder
                Dim _currentUserData = AdvantageSession.UserState

                If (_currentUserData IsNot Nothing) Then
                    If (Not _currentUserData.UserId = Guid.Empty) Then
                        settingCode.AppendLine("appInsights.setAuthenticatedUserContext('" + _currentUserData.UserId.ToString() + "');")
                    End If
                End If

                settingCode.AppendLine("appInsights.queue.push(function () {")

                If (HttpContext.Current.Session IsNot Nothing) Then
                    If (HttpContext.Current.Session.SessionID IsNot Nothing) Then
                        settingCode.AppendLine("appInsights.context.session.id = '" + HttpContext.Current.Session.SessionID.ToString() + "';")
                    End If
                End If

                If (_currentUserData IsNot Nothing) Then
                    If (_currentUserData.UserName IsNot Nothing) Then
                        settingCode.AppendLine("appInsights.context.user.id = '" + _currentUserData.UserId.ToString() + "';")
                    End If
                End If

                settingCode.AppendLine("appInsights.context.addTelemetryInitializer(function (envelope) {")
                settingCode.AppendLine("var telemetryItem = envelope.data.baseData;")
                settingCode.AppendLine("telemetryItem.properties = telemetryItem.properties || {};")

                If (_currentUserData IsNot Nothing) Then
                    If (Not _currentUserData.UserId = Guid.Empty) Then
                        settingCode.AppendLine("telemetryItem.properties['tenant_UserId'] = '" + _currentUserData.UserId.ToString() + "';")
                    End If
                End If

                If (_currentUserData IsNot Nothing) Then
                    If (_currentUserData.UserName IsNot Nothing) Then
                        settingCode.AppendLine("telemetryItem.properties['tenant_UserName'] = " + Chr(34) + _currentUserData.UserName.ToString() + Chr(34) + ";")
                    End If
                End If

                If (_currentUserData IsNot Nothing) Then
                    settingCode.AppendLine("telemetryItem.properties['tenant_IsSupport'] = '" + _currentUserData.IsUserSupport.ToString() + "';")
                End If

                If (_currentUserData IsNot Nothing) Then
                    If (Not _currentUserData.CampusId = Guid.Empty) Then
                        settingCode.AppendLine("telemetryItem.properties['tenant_CampusId'] = '" + _currentUserData.CampusId.ToString() + "';")
                    End If
                End If

                If (_currentUserData IsNot Nothing) Then
                    settingCode.AppendLine("telemetryItem.properties['Advantage_App'] = 'Site';")
                End If

                settingCode.AppendLine("});")
                settingCode.AppendLine("});")
                settingCode.AppendLine("appInsights.trackPageView();")
                appInsightsSettingsCode.TagName = "script"
                appInsightsSettingsCode.Attributes.Add("type", "text/javascript")
                appInsightsSettingsCode.InnerHtml = settingCode.ToString()
                Head1.Controls.AddAt(1, appInsightsSettingsCode)

            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                '_log.Info("App insights failed")
            End Try
        End If
    End Sub

    ''' <summary>
    ''' page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If Not Page.IsPostBack Then

            'MasterPageUserOptionsPanelBar.CurrentCampusId = CurrentCampusId
            ' jguirado Get special values used in the page master....
            AdvantageSession.UserId = AdvantageSession.UserState.UserId.ToString
            AdvantageSession.BuildVersion = GetBuildVersion()
            'MasterPageUserOptionsPanelBar.CurrentBuildVersion = AdvantageSession.BuildVersion
            SetMasterEntities()

        End If

        Dim token As TokenResponse

        token = CType(Session("AdvantageApiToken"), TokenResponse)

        If (Not token Is Nothing) Then
            hdnApiToken.Value = token.Token
            hdnApiUrl.Value = token.ApiUrl
            hdnApiResponseStatusCode.Value = token.ResponseStatusCode.ToString()
        End If

        BuildBreadCrumb()

        ShowHideStatusBar()

        'homeLinkImage.Src = "images/AdvantageLogo.png"

        SetHiddenControlForFerpa()

        'Handle for event og masterPageControlDropDown: This event is fired when the user change the campus in the client.
        AddHandler MasterPageCampusDD.MasterPageCampusDropDownHandler, AddressOf MasterPageDropDownChangeEvent
    End Sub



    Protected Sub AdvToolBar_ButtonClick(ByVal sender As Object, ByVal e As RadToolBarEventArgs)
        If e.Item.Index = 1 Then
            If e.Item.ToolTip = "Search Students" Then
                e.Item.ImageUrl = "images/gear1.png"
                e.Item.ToolTip = "Search Leads"
            Else
                e.Item.ImageUrl = "images/hr2.png"
                e.Item.ToolTip = "Search Students"
            End If
        End If
    End Sub

    'NOTE:  This is the new handler for the combobox, here must be concentrate all the activities related to Campus change.
    Private Sub MasterPageDropDownChangeEvent(sender As Object, e As MasterPageCampusDropDownEventArgs)

        CampusObjects.CampusSelectorchanged(Session, e.CurrentCampusId)
        SetMasterEntities(e.CurrentCampusId.ToString())
        Dim myAdvAppSettings As AdvAppSettings = CType(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        myAdvAppSettings.RetrieveSettings(AdvantageSession.UserState.CampusId.ToString)
        Dim leadGuid As Guid
        If Guid.TryParse(AdvantageSession.MasterLeadId, leadGuid) Then
            Dim lead = New LeadDA(myAdvAppSettings.AppSettings("ConnectionString")).GetLeadById(leadGuid)
            If (lead IsNot Nothing) Then
                AdvantageSession.MasterName = lead.FirstName + If(String.IsNullOrEmpty(lead.MiddleName), " ", lead.MiddleName + " ") + lead.LastName
            End If

        End If


        HttpContext.Current.Session("CampusSwitched") = True

        If IntMruType >= 1 And IntMruType <= 4 Then
            'Call NCO.AdvAppSettings.GetInstance()
            HttpContext.Current.Session("AdvAppSettings") = myAdvAppSettings
            GetLastEntityWhileSwitchingCampus("switchcampus")
        Else
            Dim resid = Request.QueryString("resid")
            IntMruType = BusinessLogicUtilities.Utilities.GetIdentityMruType(resid)
            'Call myAdvAppSettings.RetrieveSettings(AdvantageSession.UserState.CampusId.ToString)
            HttpContext.Current.Session("AdvAppSettings") = myAdvAppSettings
            GetLastEntityWhileSwitchingCampus("switchcampus")
            'BuildNavigation(e.CurrentCampusId.ToString)
            'Me.MasterPageCampusDropDown.RandomSessionPersist = ram.Next '     hdRandomSessionId.Value = ram.Next()

            'Dim strNewurl = ReloadURL(Me.MasterPageCampusDropDown.RandomSessionPersist, Request.QueryString.ToString, Request.Url.AbsolutePath)
            Dim strNewurl = BusinessLogicUtilities.Utilities.ReloadUrl(e.CurrentCampusId, Request.QueryString.ToString, Request.Url.AbsolutePath)

            Response.Redirect(strNewurl)
        End If

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

#End Region

#Region "Public Methods"

    ''' <summary>
    ''' new function to replace the one above
    ''' </summary>
    ''' <param name="StatusBarState"></param>
    ''' <remarks></remarks>
    Public Sub ShowHideStatusBarControl(ByVal statusBarState As Boolean)
        ''Trace.Warn("Begin Type")
        IntMruType = BusinessLogicUtilities.Utilities.GetIdentityMruType(_resId)
        statusBarState = statusBarState
        Try
            'While clicking on Magnifying glass in the info pages we set &bar attribute
            'if this condition is not checked, the info bar will show up in all 
            'search pages. The behavior is info bar should be hidden for search pages
            'Commented by Balaji on Mar 12 2012
            If Request.QueryString.ToString.Contains("&bar") Or Request.QueryString("resid") = 308 Then
                statusBarState = False
                Exit Try
            End If
            If statusBarState = True Then
                Select Case IntMruType

                    Case 1
                        Dim campus As String
                        If (Not AdvantageSession.UserState Is Nothing And Not String.IsNullOrEmpty(AdvantageSession.UserState.CampusId.ToString())) Then
                            campus = AdvantageSession.UserState.CampusId.ToString()
                        Else
                            campus = Me.CurrentCampusId
                        End If

                        Dim toAdd As Control = LoadControl("~/usercontrols/StudentInfoBar.ascx", "", campus)
                        toAdd.ID = "statusBarControl"
                        MyStatusBar.Controls.Add(toAdd)
                    Case 2
                        Dim toAdd As Control = LoadControl("~/usercontrols/EmployerInfoBar.ascx")
                        MyStatusBar.Controls.Add(toAdd)
                    Case 3
                        Dim toAdd As Control = LoadControl("~/usercontrols/EmployeeInfoBar.ascx")
                        MyStatusBar.Controls.Add(toAdd)
                    Case 4
                        'Dim toAdd As Control = LoadControl("~/usercontrols/LeadInfoBar.ascx")
                        'MyStatusBar.Controls.Add(toAdd)
                    Case Else
                        statusBarState = False
                End Select
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            MyStatusBar.Visible = False
            'TopPane.Height = 30
        End Try

        If statusBarState = True Then
            MyStatusBar.Visible = True
            'TopPane.Height = 105
        Else
            MyStatusBar.Visible = False
            'TopPane.Height = 30
        End If
        ''Trace.Warn("End Type")
    End Sub


    Public Property PageObjectId As String

    Public Property PageResourceId As Integer

    Public Sub SetHiddenControlForAudit()
        hdnObjectId.Value = PageObjectId
        hdnPageResourceId.Value = CType(PageResourceId, String)
        If hdnObjectId.Value <> "" Then
            '' btnAudit.Visible = True
            Dim btnAudit As LinkButton
            If Not MainMenu.FindControl("btnAudit") Is Nothing Then
                btnAudit = MainMenu.FindControl("btnAudit")
                btnAudit.Attributes.Remove("class")
            End If
        End If
    End Sub

    Public Sub SetHiddenControlForFerpa()
        Dim strVid As String
        Dim resId As Integer
        Dim studentId As String
        Dim dt As DataTable

        Try
            strVid = Request.QueryString("VID")
            state = CommonWebUtilities.LoadAdvantageSessionState(HttpContext.Current)
            resId = CType(Request.QueryString("resid").ToString, Integer)


            If IntMruType = 1 Then      'student page
                If Not String.IsNullOrEmpty(strVid) Then
                    If strVid = "1" Then    'valid VID
                        Exit Sub
                    Else
                        If state.Count > 0 Then
                            studentId = DirectCast(state(strVid), AdvantageStateInfo).StudentId
                            If Not studentId Is Nothing Then
                                Dim FERPA As LinkButton
                                Dim rp As Repeater
                                If (New StudentFERPA).HasFERPAPermission(resId, studentId) Then
                                    If Not MainMenu.FindControl("FERPA") Is Nothing Then
                                        FERPA = MainMenu.FindControl("FERPA")
                                        FERPA.Visible = True
                                    End If
                                    If Not MainMenu.FindControl("Repeater1") Is Nothing Then
                                        dt = (New StudentFERPA).GetFERPAPermission(resId, studentId)
                                        rp = MainMenu.FindControl("Repeater1")
                                        rp.DataSource = dt
                                        rp.DataBind()
                                    End If
                                Else
                                    If (New StudentFERPA).HasGivenFERPAPermission(studentId) Then
                                        If Not MainMenu.FindControl("FERPA") Is Nothing Then
                                            FERPA = MainMenu.FindControl("FERPA")
                                            FERPA.Visible = True
                                        End If
                                        If Not MainMenu.FindControl("Repeater1") Is Nothing Then
                                            dt = (New StudentFERPA).GetFERPAPermission(0, studentId)
                                            rp = MainMenu.FindControl("Repeater1")
                                            rp.DataSource = dt
                                            rp.DataBind()
                                        End If

                                    End If

                                End If
                            End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Response.Redirect(FormsAuthentication.LoginUrl)
        End Try

    End Sub

    Public Function GetBuildVersion() As String
        Dim versionString As String
        Try
            Dim myFileVersionInfo As FileVersionInfo = FileVersionInfo.GetVersionInfo(Server.MapPath("~/Bin/Common.dll"))
            versionString = myFileVersionInfo.FileVersion.ToString()
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            versionString = String.Empty
        End Try
        Return versionString
    End Function
#End Region


#Region "Private Methods"
    Private Sub ShowHideStatusBar()

        If IntMruType >= 1 And IntMruType <= 3 Then 'may 2015 Lead bar now is in a sub master page. then four is not analyze
            StatusBarState = True
        Else
            StatusBarState = False
        End If


        'DE8777 - added isadhoc check - adhoc report passes reportid as resourceid
        If Request.QueryString.ToString.Contains("&bar") Or Request.QueryString.ToString.Contains("&isadhoc") Then
            StatusBarState = False
        End If



        If StatusBarState = True Then
            MyStatusBar.Visible = True
            'TopPane.Height = 105
        Else
            MyStatusBar.Visible = False

        End If
    End Sub


    Private Sub BuildBreadCrumb()
        Dim breadcrumb As New StringBuilder
        breadcrumb.Append("<span id=PagebreadCrumb style='margin-left:5px;vertical-align:top;'>")
        breadcrumb.Append(AdvantageSession.PageBreadCrumb)
        breadcrumb.Append("</span>")


        If Session("IsImpersonating") IsNot Nothing Then
            If Session("IsImpersonating").ToString().ToUpper() = "TRUE" Then
                impersonationUser.Text = "<div style='font-size:12px;color:white;align:left;'>USER: " + AdvantageSession.ImpersonatedUserAndName.ToUpper() + "</div>"
            Else
                impersonationUser.Text = ""
            End If
        End If

    End Sub



    Protected Sub GetLastEntityWhileSwitchingCampus(Optional ByVal source As String = "")
        Session("MenuLoaded") = False  'Need to rebuild menu while switching campus for the campus level config setting to take effect
        Try
            Select Case IntMruType
                Case 1
                    _masterState.BuildStudentStateObject("") 'Passing a empty studentid will enforce the function to get the last student from the selected campus
                Case 2
                    _masterState.BuildEmployerStateObject("", "", source)
                Case 3
                    _masterState.BuildEmployeeStateObject("", "")
                Case 4
                    _masterState.BuildLeadStateObject("", True)
            End Select
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Try
                Select Case IntMruType
                    Case 1
                        'Redirect to student search page
                        _masterState.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
                    Case 2
                        _masterState.RedirectToEmployerSearchPage(AdvantageSession.UserState.CampusId.ToString)
                    Case 3
                        _masterState.RedirectToEmployeeSearchPage(AdvantageSession.UserState.CampusId.ToString)
                    Case 4
                        _masterState.RedirectToLeadSearchPage(AdvantageSession.UserState.CampusId.ToString)
                    Case Else
                        Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=dashboard")
                End Select
            Catch ex1 As Exception
                exTracker.TrackExceptionWrapper(ex1)
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=dashboard")
            End Try

        End Try
    End Sub


    Private Sub SetMasterEntities()

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Get last entities accessed and add them to the AdvantageSession
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
            AdvantageSession.MasterStudentId = _masterState.GetLastEntityFromStateObject(Me.CurrentCampusId, 1)
        End If

        Dim changeLead As String = Request.QueryString("NewEnt")
        If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Or changeLead = "1" Then
            AdvantageSession.MasterLeadId = _masterState.GetLastEntityFromStateObject(Me.CurrentCampusId, 4)
        End If

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployerId) Then
            AdvantageSession.MasterEmployerId = _masterState.GetLastEntityFromStateObject(Me.CurrentCampusId, 2)
        End If

        If String.IsNullOrEmpty(AdvantageSession.MasterEmployeeId) Then
            AdvantageSession.MasterEmployeeId = _masterState.GetLastEntityFromStateObject(Me.CurrentCampusId, 3)
        End If
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If AdvantageSession.UserImpersonationLogId IsNot Nothing Then
            hdnUserImpersonationMasterLogId.Value = AdvantageSession.UserImpersonationLogId.ToString()
        End If
    End Sub
    Private Sub SetMasterEntities(ByVal campusId As String)

        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Get last entities accessed and add them to the AdvantageSession
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        AdvantageSession.MasterStudentId = _masterState.GetLastEntityFromStateObject(campusId, 1)

        AdvantageSession.MasterLeadId = _masterState.GetLastEntityFromStateObject(campusId, 4)

        AdvantageSession.MasterEmployerId = _masterState.GetLastEntityFromStateObject(campusId, 2)

        AdvantageSession.MasterEmployeeId = _masterState.GetLastEntityFromStateObject(campusId, 3)
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        If AdvantageSession.UserImpersonationLogId IsNot Nothing Then
            hdnUserImpersonationMasterLogId.Value = AdvantageSession.UserImpersonationLogId.ToString()
        End If
    End Sub

    Private Overloads Function LoadControl(ByVal UserControlPath As String, ByVal ParamArray constructorParameters As Object()) As UserControl
        Dim constParamTypes As New List(Of Type)()
        For Each constParam As Object In constructorParameters
            constParamTypes.Add(constParam.[GetType]())
        Next

        Dim ctl As UserControl = TryCast(Page.LoadControl(UserControlPath), UserControl)

        ' Find the relevant constructor
        Dim constructor As ConstructorInfo = ctl.[GetType]().BaseType.GetConstructor(constParamTypes.ToArray())

        'And then call the relevant constructor
        If constructor Is Nothing Then
            Throw New MemberAccessException("The requested constructor was not found on : " & ctl.[GetType]().BaseType.ToString())
        Else
            constructor.Invoke(ctl, constructorParameters)
        End If

        ' Finally return the fully initialized UC
        Return ctl
    End Function

    Private Function GetAdvAppSettings() As AdvAppSettings
        Dim myAdvAppSettings As AdvAppSettings = AdvAppSettings.GetAppSettings()
        'If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
        '    myAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        'Else
        '    myAdvAppSettings = New AdvAppSettings
        'End If
        Return myAdvAppSettings
    End Function


#End Region





End Class



