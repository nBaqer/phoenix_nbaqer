﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Template_Container_OneColumn.aspx.vb" Inherits="Template_Container_OneColumn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Y" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
				<!-- begin rightcolumn -->
				<TR>
					<td class="detailsframetop"><table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right"><asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:button><asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
										Enabled="False"></asp:button></td>
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
							align="center">
							<tr>
								<td class="detailsframe">
                                      <div class="scrollsingleframe">


										<!-- Begin Page Content-->


										
                                     </div>	
								</td>
							</tr>
						</table>
					</td>
					<!-- end rightcolumn --></TR>
			</table>
			<!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="ValidationSummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

