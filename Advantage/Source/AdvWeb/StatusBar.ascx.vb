﻿Partial Class usercontrols_StatusBar
    Inherits UserControl
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        Dim userctrl As UserControl = Nothing
        If Request.QueryString.ToString.Contains("&Type=") Then
         Dim mruType = Request.QueryString("Type").ToString
            Select Case mruType
                Case "1"
                    userctrl = CType(LoadControl("~/usercontrols/StudentInfoBar.ascx"), UserControl)
                Case "3"
                    userctrl = CType(LoadControl("~/usercontrols/EmployeeInfoBar.ascx"), UserControl)
                Case "2"
                    userctrl = CType(LoadControl("~/usercontrols/EmployerInfoBar.ascx"), UserControl)
                Case "4"
                    '''' No more used now it is use LeadKendoInfoBar and are not called from here
                    '''' userctrl = CType(LoadControl("~/usercontrols/LeadInfoBar.ascx"), UserControl)
                Case Else
                    Throw New Exception("Unknown MRU Type")
            End Select
            Me.Controls.Add(userctrl)
        End If
    End Sub
End Class
