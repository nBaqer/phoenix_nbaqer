﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="ChangePassword.aspx.vb" Inherits="ChangePassword" %>
<%@ mastertype virtualpath="~/NewSite.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
    <link rel="stylesheet" type="text/css" href="CSS/localhost_lowercase.css" />
    <link rel="stylesheet" type="text/css" href="CSS/Stylesheet.css" />
   <style type="text/css">
   h2 { font: bold 11px verdana; color: #000066; background-color: transparent; width: auto; margin: 0; padding: 0; border: 0;text-align: left;}
   .FileInput
    {
	    margin: 4px;
	    vertical-align: middle;
	    font: normal 10px verdana;
	    text-align:left;
	    color: #000066;
	    width: 300px;
    }
    .ProcessFile
    {
	    margin: 4px;
	    vertical-align: middle;
	    font: normal 10px verdana;
	    text-align:center;
	    color: #000066;
    }
   </style>
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                <!-- begin rightcolumn -->
                <tr>
                    <td class="detailsframetop">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td class="menuframe" align="right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save" CausesValidation="false"
                                        Enabled="false"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"
                                        Enabled="false"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"
                                        Enabled="false"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->
                        <!-- Content goes here -->
                        <div class="clwyLoginPageFooter">
     <div class="clwyPageHeader"> 
	    	 <div class="clwyToolbarHeaderInfo">           
            <div class="clwyToolbarHeaderVersionInfo">
                <asp:Label ID="lblApplicationHeaderVersion" runat="server" />
            </div>
            <div class="clwyToolbarHeaderActionsInfo">&nbsp;</div>
        </div>
    <div class="clwyChangePasswordContent" align="center" style="width:600px;">
        <asp:ChangePassword
        id="ChangePassword1"
        InstructionText="Complete this form to create a new password."
        DisplayUserName="false"
        ContinueDestinationPageUrl="~/Default.aspx"
        CancelDestinationPageUrl="~/Default.aspx"
        CssClass="changePassword"
        TitleTextStyle-CssClass="changePassword_title"
        InstructionTextStyle-CssClass="changePassword_instructions"
        ChangePasswordButtonStyle-CssClass="changePassword_button"
        CancelButtonStyle-CssClass="changePassword_button"
        ContinueButtonStyle-CssClass="changePassword_button"
        Runat="server"
        PasswordHintText = "Please enter a password at least 8 characters long, containing a number and one special character.">
      <%--  NewPasswordRegularExpression = '@\"(?=.{8,})(?=(.*\d){1,})(?=(.*\W){1,})' 
        NewPasswordRegularExpressionErrorMessage = "Your password must be at least 8 characters long, and contain at least one number and one special character.">--%>
<CancelButtonStyle CssClass="changePassword_button"></CancelButtonStyle>

<ChangePasswordButtonStyle CssClass="changePassword_button"></ChangePasswordButtonStyle>
                <ChangePasswordTemplate>
                   <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;">
                    <tr>
                    <td>
                        <table cellpadding="0" border="0">
                            <tr>
                                <td align="center" colspan="2" class="changePassword_title">
                                </td>
                            </tr>
                            <tr>
                                 <td align="left" style="padding-top: .5em;padding-bottom: .6em;">
                                    <asp:Label ID="lblUserName" runat="server" 
                                        AssociatedControlID="UserName">Email</asp:Label>
                                </td>
                                  <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:label ID="UserName" runat="server"  cssclass="label" Style="text-align:left;padding-left:0;"></asp:label>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding-top: .5em;padding-bottom: .5em;">
                                    <asp:Label ID="CurrentPasswordLabel" runat="server" 
                                        AssociatedControlID="CurrentPassword">Password</asp:Label>
                                </td>
                                <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:TextBox ID="CurrentPassword" runat="server" cssclass="textbox" TextMode="Password" Width="230px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" 
                                        ControlToValidate="CurrentPassword" ErrorMessage="Password is required." 
                                        ToolTip="Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="padding-top: .5em;padding-bottom: .5em;">
                                    <asp:Label ID="NewPasswordLabel" runat="server" 
                                        AssociatedControlID="NewPassword">New Password</asp:Label>
                                </td>
                                <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"  cssclass="textbox" Width="230px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" 
                                        ControlToValidate="NewPassword" ErrorMessage="New Password is required." 
                                        ToolTip="New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                               </td>
                            </tr>
                            <tr>
                                <td nowrap align="left" style="padding-top: .5em;padding-bottom: .5em;">
                                    <asp:Label ID="ConfirmNewPasswordLabel" runat="server" 
                                        AssociatedControlID="ConfirmNewPassword">Confirm New Password</asp:Label>
                                </td>
                                 <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" cssclass="textbox" Width="230px"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" 
                                        ControlToValidate="ConfirmNewPassword"  
                                        ErrorMessage="Confirm New Password is required." 
                                        ToolTip="Confirm New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"  style="width:100px;padding-top: .5em;padding-bottom: .5em;">
                                    <asp:Label ID="lblSecurityQuestion" runat="server" 
                                        AssociatedControlID="ddlSecurityQuestion">Security question</asp:Label>
                                </td>
                                 <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:dropdownlist ID="ddlSecurityQuestion" runat="server" cssclass="dropdownlist"></asp:dropdownlist>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                                        ControlToValidate="ddlSecurityQuestion" 
                                        ErrorMessage="Security question is required." 
                                        ToolTip="Security question is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" nowrap style="padding-top: .5em;padding-bottom: .5em;">
                                    <asp:Label ID="lblSecurityAnswer" runat="server" 
                                        AssociatedControlID="txtSecurityAnswer">Security answer</asp:Label>
                                </td>
                                 <td align="left" style="width: 230px;padding-top: .5em;padding-bottom: .5em;padding-left: .2em;">
                                    <asp:textbox ID="txtSecurityAnswer" runat="server" Width="230px" cssclass="textbox"></asp:textbox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                                        ControlToValidate="txtSecurityAnswer" 
                                        ErrorMessage="Security answer is required." 
                                        ToolTip="Security answer is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:CompareValidator ID="NewPasswordCompare" runat="server" 
                                        ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" 
                                        Display="Dynamic" 
                                        ErrorMessage="The new password and the confirm new password does not match. Please try again." 
                                        ValidationGroup="ChangePassword1"></asp:CompareValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="padding-top: .5em;padding-bottom: .5em;">
                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="ChangePasswordPushButton" runat="server" 
                                        CommandName="ChangePassword" Text="Change Password" 
                                        ValidationGroup="ChangePassword1" Font-Size="12px" OnClick="ChangePasswordPushButton_Click" />
                                    <asp:Button ID="btnBacktoLoginPage" runat="server" 
                                        CommandName="Login" Text="Login Page" CausesValidation="false" Visible="false"
                                        ValidationGroup="ChangePassword1" Font-Size="12px" OnClick="Login_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

                </ChangePasswordTemplate> 

<ContinueButtonStyle CssClass="changePassword_button"></ContinueButtonStyle>

<InstructionTextStyle CssClass="changePassword_instructions"></InstructionTextStyle>

<TitleTextStyle CssClass="changePassword_title"></TitleTextStyle>
            </asp:ChangePassword> 

    </div>
         </div>
         </div>
                      </td>
                    <!-- end rightcolumn -->
                </tr>
            </table>
            <!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>


