﻿
Partial Class Logout
    Inherits BasePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Session.Abandon()
        Response.Redirect(FormsAuthentication.LoginUrl)
    End Sub
End Class
