﻿
Partial Class DocumentViewer
    Inherits Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not Session("UserName") Is Nothing Then
                hdnUserName.Value = Session("UserName").ToString()
                hdnUserId.Value = Session("UserId").ToString()
            End If

            hdnTest.Value = "here"
            
            For Each tag As HtmlMeta In From tag1 In Page.Header.Controls.OfType(Of HtmlMeta)() Where (tag1.Name = "compat_mode")
                tag.Content = "IE=8"
            Next

            AdvantageSession.PageBreadCrumb = "Academics / Document Management"
        End If
    End Sub
End Class
