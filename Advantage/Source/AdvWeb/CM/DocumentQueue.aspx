﻿<%@ Page Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="DocumentQueue.aspx.vb" Inherits="CM_DocumentQueue" EnableViewState="false" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 

<asp:content id="content4" contentplaceholderid="additional_head" runat="server">
</asp:content>
<asp:content id="content1" contentplaceholderid="contentmain2" runat="server">                           
	<script type="text/javascript" src="../Scripts/DataSources/enrollmentStatusesDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/programVersionsDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/requirementApprovalDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/studentsDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/studentGroupsDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/studentDocumentRequirementsDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/documentFilesDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/studentDocumentDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/documentStatusDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/requirementApprovalDataSource.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.uploadify.js"></script>
    <script type="text/javascript" src="../Scripts/DataSources/dataSourceCommon.js"></script>
    <script type="text/javascript" src="../Scripts/common/kendoControlSetup.js"></script>
    <script type="text/javascript" src="../Scripts/tab-util.js"></script>
    <script type="text/javascript" src="../Scripts/viewModels/documentQueue.js"></script>

	<div id='frame'>
		<div id='body'>
			<telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px">
		<telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			<div style="margin:10px">
				  <div id="tabstrip" style="display: inline" >  
					<ul  >  
						<li class="k-state-active" >Search Documents</li> 
                        
					</ul>  
				
				    <div >
					     <table id="searchDocsForm" cellpadding="1">                       
						    <tr >
							    <td valign="bottom" class="tblLabels">Program Version:</td>
							    <td valign="bottom" class="tblLabels">Start Date:</td>
							    <td valign="bottom" class="tblLabels">Enroll Status:</td>
							    <td valign="bottom" class="tblLabels">Student Group:</td>
							    <td valign="bottom" class="tblLabels">Show Last:</td>
							    <td valign="bottom" class="tblLabels">&nbsp;</td>                                
						    </tr>
						    <tr>
							    <td><input id="programVersionDropDownList"  data-role="dropdownlist" data-value-field="Id" data-text-field="Description"  data-bind="source: programVersions, value: selectedProgramVersion" style="width:200px" /></td>                            
							    <td><input data-role='datepicker' data-bind="value: startDate " style="width:175px" /></td> 
							    <td><input  id="enrollmentStatusDropDownList"  data-role="dropdownlist" data-value-field="Id" data-text-field="Description"  data-bind="source: enrollmentStatuses, value: selectedEnrollmentStatus" style="width:200px"></td>
							    <td><input id="studentGroupsDropDownList"  data-role="dropdownlist" data-value-field="Id" data-text-field="Description"  data-bind="source: studentGroups, value: selectedStudentGroup" style="width:200px"></td>
							    <td><input id="showLast"  data-role="dropdownlist" data-value-field="Id" data-text-field="Description"  data-bind="source: showLast, value: selectedShowLast" style="width:200px"></td>
						        <td width="200px"><div id="btnSearch" style="display:inline"><input type="submit" data-bind="click: findDocuments" value="Search" class="k-button" /></div><div id="btnReset" style="display:inline"><input  type="reset" data-bind="click: resetPage" value="Reset" class="k-button" /></div> </td>
						    </tr>                        
					    </table>
				    </div>  
                    
			    </div>
			<div align="center">
                <br/>
                <div class="header">Documents Pending Approval</div>
                <br/>
				<div id="pendingDocumentsGrid" data-bind="source: documents"></div>
			</div>
			</div>
			<br /><br />
            <div id="errorDetails" style="display: none"></div>
            
            <div id="approvalWin" style="display: none">
                 <div class="k-block" >
                        <div class="k-header k-inset header" align="center">Are you sure you want to approve the following document?</div>
                     <br/>
                        <table id='table1' width='70%' align="center"  > 
                            <tr>
                                <td class="tblLabels" align="right">Document Info:</td> 
                                <td colspan="2" align="center"><label class="k-label" data-bind='text:documentName' ></label> for <label class='k-label' data-bind='text:selectedStudent' ></label></td>                                
                            </tr>
                            <tr>
                                <td class="tblLabels" align="right" >Choose Approval Status:</td>
                                <td><input  id="documentStatusDropDownList"  data-role="dropdownlist" data-value-field="Id" data-text-field="Description"  data-bind="source: documentStatuses, value: selectedDocumentStatus"  style="width:200px"/></td>
                            </tr>
                            <tr><td><br/></td></tr>
                            <tr>
                                <td  align="right" width="50%" ><input class="k-button" id="btnApprove" type="submit" data-bind="click: approvalConfirmed, disabled: approveDisabled" value="Approve the Document" /></td>
                                <td align='left' width="50%"><input class="k-button" id="btnCancel" type="submit" data-bind="click: cancelApproval, disabled: approveDisabled" value="Cancel and Close Window"/> </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center" style="border:5px"><label class='tblLabels' data-bind='text:documentApprovalLabel' ></label></td>
                            </tr>
                        </table>
                </div>
            </div>
            <input type="hidden" id="hdnUserName" runat="server" class="hdnUserName" ClientIDMode="Static"/>
            <input type="hidden" id="hdnUserId" runat="server" class="hdnUserName" ClientIDMode="Static"/>
		</telerik:RadPane>
		</telerik:RadSplitter>
		</div>    
	</div>
	
    
    

</asp:content>  

				