<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"  inherits="DocumentStatus" CodeFile="DocumentStatus.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script language="javascript" src="../js/CheckAll.js" type="text/javascript"/>
   <script type="text/javascript">

       function OldPageResized(sender, args) {
           $telerik.repaintChildren(sender);
       }

   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow:auto;">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" style="overflow:auto;">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
        						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="listframetop"><asp:checkbox id="chkStatus" runat="server" Text="Show Active Only" AutoPostBack="True" Checked="True"
										CssClass="Label" Width="100%"></asp:checkbox></td>
							</tr>
							<tr>
								<td class="listframebottom">
									<div class="scrollleft"><asp:datalist id="dlstDocStatus" runat="server" Width="100%" DataKeyField="DocStatusId">
											<SelectedItemTemplate>
												<asp:Label id='label11' Runat =server text='<%# Container.DataItem("DocStatusDescrip")%>' CssClass = 'SelectedItem' >
												</asp:Label>
											</SelectedItemTemplate>
											<ItemTemplate>
												<asp:LinkButton text='<%# Container.DataItem("DocStatusDescrip")%>' Runat="server" CssClass="NonSelectedItem" CommandArgument='<%# Container.DataItem("DocStatusId")%>' ID="Linkbutton1" CausesValidation="False" />
											</ItemTemplate>
										</asp:datalist></div>
								</td>
							</tr>
						</table>
					</td>
					<!-- end leftcolumn -->
					<!-- begin rightcolumn -->
					<td class="leftside">
						<table cellSpacing="0" cellPadding="0" width="9" border="0">
						</table>
					</td>
					<td class="DetailsFrameTop">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<TR>
								<TD class="MenuFrame" align="right"><asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Width="62px"></asp:button>
									<asp:button id="btnNew" runat="server" Text="New" CssClass="new" Width="58px"></asp:button>
									<INPUT class="reset" type="reset" value="Reset">
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"></asp:button><asp:button id="btnhistory" CausesValidation="false" runat="server" CssClass="history" Text="History"></asp:button></TD>
								
							</TR>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td class="detailsframe">
									<div class="scrollright">
										<!-- begin table content--><asp:panel id="pnlRHS" Width="100%" Runat="server">
            <TABLE class=contenttable cellSpacing=0 cellPadding=0 width="60%" align=center>
              <TR>
                <TD class="contentcell"noWrap>
<asp:label id=lblDocStatusCode CssClass="Label" Runat="server"></asp:label></TD>
                <TD class="contentcell4">
<asp:textbox id=txtDocStatusCode CssClass="TextBox" Runat="server" MaxLength="128"></asp:textbox></TD></TR>
              <TR>
                <TD class="contentcell"noWrap>
<asp:label id=lblStatusId CssClass="Label" Runat="server"></asp:label></TD>
                <TD class="contentcell4">
<asp:dropdownlist id=ddlStatusId runat="server" CssClass="DropDownList"></asp:dropdownlist></TD></TR>
              <TR>
                <TD class="contentcell"noWrap>
<asp:label id=lblDocStatusDescrip CssClass="Label" Runat="server"></asp:label></TD>
                <TD class="contentcell4">
<asp:textbox id=txtDocStatusDescrip CssClass="TextBox" Runat="server" MaxLength="128"></asp:textbox></TD></TR>
              <TR>
                <TD class="contentcell"noWrap>
<asp:label id=lblCampGrpId CssClass="Label" Runat="server"></asp:label></TD>
                <TD class="contentcell4">
<asp:dropdownlist id=ddlCampGrpId runat="server" CssClass="DropDownList"></asp:dropdownlist></TD></TR>
              <TR>
                <TD class="contentcell"noWrap>
<asp:textbox id=txtDocStatusId CssClass="TextBox" Runat="server" MaxLength="128" Visible="false"></asp:textbox></TD>
                <TD class="contentcell4">
<asp:textbox id=txtRowIds style="VISIBILITY: hidden" runat="server" CssClass="Label" width="10%"></asp:textbox>
<asp:textbox id=txtResourceId style="VISIBILITY: hidden" runat="server" Width="10%" CssClass="Label"></asp:textbox>

                </TD></TR>

            </TABLE>
						</asp:panel>			
										<!--end table content-->
									</div>
								</td>
							</tr>
						</table>
                        </td>
                                </tr>
                                    </table>
					</telerik:RadPane>
                    </telerik:RadSplitter>
			<!-- begin footer -->
		
			<!-- end footer -->
			<asp:panel id="pnlRequiredFieldValidators" runat="server" Width="50px"></asp:panel>&nbsp;&nbsp;
			<asp:validationsummary id="ValidationSummary1" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:validationsummary>
	</div>
</asp:Content>