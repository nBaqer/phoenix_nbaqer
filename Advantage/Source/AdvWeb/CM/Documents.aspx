

<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" inherits="Documents" CodeFile="Documents.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script language="javascript" src="../js/CheckAll.js" type="text/javascript"/>
   <script type="text/javascript">

       function OldPageResized(sender, args) {
           $telerik.repaintChildren(sender);
       }

   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow:auto;">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" style="overflow:auto;">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
						<table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<tr>
								<td class="listframetop">
									<table width="100%" border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td width="15%" nowrap align="left"><asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
											<td width="85%" nowrap>
												<asp:radiobuttonlist id="radStatus" CssClass="Label" AutoPostBack="true" Runat="Server" RepeatDirection="Horizontal">
													<asp:ListItem Text="Active" Selected="True" />
													<asp:ListItem Text="Inactive" />
													<asp:ListItem Text="All" />
												</asp:radiobuttonlist>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td class="listframebottom">
									<div class="scrollleft" style="HEIGHT: expression(document.body.clientHeight - 200 + 'px')">
										<asp:datalist id="dlstDocument" runat="server" Width="100%" DataKeyField="DocumentId">
										 <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                            <ItemStyle CssClass="itemstyle"></ItemStyle>
											<ItemTemplate>
												<asp:ImageButton id="imgInActive" causesValidation=False ImageUrl="../images/Inactive.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Inactive" %>'>
												</asp:ImageButton>
												<asp:ImageButton id="imgActive" causesValidation=False ImageUrl="../images/Active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), String).ToString = "Active" %>'>
												</asp:ImageButton>
												<asp:Label ID="lblId" Runat="server" Visible="false" text='<%# Container.DataItem("StatusId")%>' />
												<asp:LinkButton text='<%# Container.DataItem("DocumentDescrip")%>' Runat="server" CssClass="itemstyle" CommandArgument='<%# Container.DataItem("DocumentId")%>' ID="Linkbutton1" CausesValidation="False" />
											</ItemTemplate>
										</asp:datalist></div>
								</td>
							</tr>
						</table>
					
					</telerik:RadPane>
    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellSpacing="0" cellPadding="0" width="100%" border="0">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="MenuFrame" align="right">
									<asp:button id="btnSave" runat="server" CssClass="save" Text="Save"></asp:button>
									<asp:button id="btnNew" runat="server" CssClass="new" Text="New"></asp:button>
									<asp:button id="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:button></td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable">
							<tr>
								<td class="detailsframe"><asp:panel id="pnlRHS" Width="100%" Runat="server">
										<DIV class="scrollright"><!-- begin table content-->
											<TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="60%" align="center">
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblDocumentCode" CssClass="Label" Runat="server"></asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:textbox id="txtDocumentCode" CssClass="TextBox" Runat="server" MaxLength="128"></asp:textbox></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblStatusId" CssClass="Label" Runat="server"></asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:dropdownlist id="ddlStatusId" runat="server" CssClass="DropDownList"></asp:dropdownlist></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblDocumentDescrip" CssClass="Label" Runat="server"></asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:textbox id="txtDocumentDescrip" CssClass="TextBox" Runat="server" MaxLength="128"></asp:textbox></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblCampGrpId" CssClass="Label" Runat="server"></asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:dropdownlist id="ddlCampGrpId" runat="server" CssClass="DropDownList"></asp:dropdownlist></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:label id="lblModuleId" CssClass="Label" Runat="server"></asp:label></TD>
													<TD class="twocolumncontentcell">
														<asp:dropdownlist id="ddlModuleId" runat="server" CssClass="DropDownList"></asp:dropdownlist></TD>
												</TR>
												<TR>
													<TD class="twocolumnlabelcell">
														<asp:textbox id="txtDocumentId" CssClass="Label" Runat="server" MaxLength="128" Visible="false"></asp:textbox></TD>
													<TD class="twocolumncontentcell">
														<asp:textbox id="txtRowIds" style="VISIBILITY: hidden" runat="server" CssClass="donothing" ></asp:textbox>
														<asp:textbox id="txtResourceId" style="VISIBILITY: hidden" runat="server" CssClass="donothing"></asp:textbox></TD>
												</TR>
											</TABLE>
										</DIV>
									</asp:panel>
									<!--end table content-->
								</td>
							</tr>
						</table>
				 </telerik:RadPane>
    </telerik:RadSplitter>
			<!-- begin footer -->
			
			<!-- end footer -->
			<asp:panel id="pnlRequiredFieldValidators" runat="server" CssClass="ValidationSummary">
				<asp:ValidationSummary id="Validationsummary1" runat="server" ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
				<asp:CustomValidator id="CustomValidator1" Runat="server"></asp:CustomValidator>
			</asp:panel>
        </div>
		</asp:Content>
