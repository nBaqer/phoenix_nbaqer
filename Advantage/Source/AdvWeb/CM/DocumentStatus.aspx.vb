Imports Fame.Common
Imports System.Data
Imports System.Collections

Partial Class DocumentStatus
    Inherits System.Web.UI.Page

    Dim campusId As String
    Dim userId As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Set the Delete Button so it prompts the user for confirmation when clicked
        btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        btnhistory.Attributes.Add("onclick", "OpenHistory();return false;")
        Dim m_Context As HttpContext
        m_Context = HttpContext.Current
        txtResourceId.Text = CInt(m_Context.Items("ResourceId"))

        Dim objCommon As New CommonUtilities
        Dim objListGen As New DataListGenerator()
        Dim ds As New DataSet()
        Dim sw As New System.IO.StringWriter()
        Dim ds2 As New DataSet()
        Dim sStatusId As String
        Try
            campusId = AdvantageSession.UserState.CampusId.ToString
            userId = AdvantageSession.UserState.UserId.ToString

            If Not Page.IsPostBack Then
                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                'objCommon.PopulatePage(Form1)
                'Disable the new and delete buttons
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
                ViewState("MODE") = "NEW"
                txtDocStatusId.Text = System.Guid.NewGuid.ToString
                ds = objListGen.SummaryListGenerator(userId, campusId)

                'Generate the status id
                ds2 = objListGen.StatusIdGenerator()

                'Set up the primary key on the datatable
                ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

                Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
                sStatusId = row("StatusId").ToString()

                Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "DocStatusDescrip", DataViewRowState.CurrentRows)

                With dlstDocStatus
                    .DataSource = dv
                    .DataBind()
                End With
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                ViewState("SelectedIndex") = "-1"
            Else

                objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")

            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim objCommon As New CommonUtilities()
        Dim ds As New DataSet()
        Dim objListGen As New DataListGenerator()
        Dim sw As New System.IO.StringWriter()
        Try
            If ViewState("MODE") = "NEW" Then
                objCommon.PagePK = txtDocStatusId.Text
                objCommon.DoInsert(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                'Set the Primary Key value, so it can be used later by the DoUpdate method

                txtRowIds.Text = objCommon.PagePK
                'dlstDocStatus.SelectedIndex = -1
                ds = objListGen.SummaryListGenerator(userId, campusId)
                PopulateDataList(ds, txtDocStatusId.Text, "New")
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Set Mode to EDIT since a record has been inserted into the db.
                objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
                ViewState("MODE") = "EDIT"

            ElseIf ViewState("MODE") = "EDIT" Then
                objCommon.DoUpdate(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                ds = objListGen.SummaryListGenerator(userId, campusId)
                PopulateDataList(ds, ViewState("SelectedIndex"), "Edit")
                ds.WriteXml(sw)
                ViewState("ds") = sw.ToString()
                'Response.Write(objCommon.strText)
            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnSave_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Dim objcommon As New CommonUtilities()
        Dim ds As New DataSet()
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Try
            ClearRHS()
            ds.ReadXml(sr)
            PopulateDataList(ds, "-1", "Add")
            txtDocStatusId.Text = System.Guid.NewGuid.ToString
            'disable new and delete buttons.
            objcommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            'set the state to new. 
            ViewState("MODE") = "NEW"
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        'Delete from database and clear the screen as well. 
        'And set the mode to "NEW" since the record has been deleted from the db.
        Dim objCommon As New CommonUtilities()
        Dim objListGen As New DataListGenerator()
        Dim ds As New DataSet()
        Dim sw As New System.IO.StringWriter()
        Try
            objCommon.DoDelete(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
            ClearRHS()
            ds = objListGen.SummaryListGenerator(userId, campusId)
            ViewState("SelectedIndex") = "-1"
            PopulateDataList(ds, "-1", "Delete")
            ds.WriteXml(sw)
            ViewState("ds") = sw.ToString()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"
            txtDocStatusId.Text = System.Guid.NewGuid.ToString
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnDelete" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try

    End Sub
    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub PopulateDataList(ByVal ds As DataSet, ByVal GuidToFind As String, ByVal CallingFunction As String)
        Dim ds2 As New DataSet
        Dim dv2 As New DataView
        Dim objListGen As New DataListGenerator
        Dim sStatusId As String

        ds2 = objListGen.StatusIdGenerator()
        'Set up the primary key on the datatable
        ds2.Tables(0).PrimaryKey = New DataColumn() {ds2.Tables(0).Columns("Status")}

        If (chkStatus.Checked = True) Then 'Show Active Only
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Active")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays active records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "DocStatusDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        ElseIf (chkStatus.Checked = False) Then
            Dim row As DataRow = ds2.Tables(0).Rows.Find("Inactive")
            sStatusId = row("StatusId").ToString()
            'Create dataview which displays inactive records only
            Dim dv As New DataView(ds.Tables(0), "StatusId = '" & sStatusId & "'", "DocStatusDescrip", DataViewRowState.CurrentRows)
            dv2 = dv
        End If
        With dlstDocStatus
            Select Case CallingFunction
                Case "Delete"
                    dlstDocStatus.SelectedIndex = -1
                Case "New"
                    Dim myDRV As DataRowView
                    Dim i As Integer
                    Dim GuidInDV As String
                    For Each myDRV In dv2
                        GuidInDV = myDRV(0).ToString
                        If GuidToFind.ToString = GuidInDV Then
                            Exit For
                        End If
                        i += 1
                    Next
                    dlstDocStatus.SelectedIndex = i
                Case "Edit"
                    dlstDocStatus.SelectedIndex = CInt(ViewState("SelectedIndex"))
                Case "Item"
                    dlstDocStatus.SelectedIndex = CInt(ViewState("SelectedIndex"))
                Case "Check"
                    dlstDocStatus.SelectedIndex = -1
                Case "Add"
                    dlstDocStatus.SelectedIndex = -1
            End Select
            .DataSource = dv2
            .DataBind()
        End With

        'With dlstDocStatus
        '    .DataSource = dv2
        '    .DataBind()
        'End With
        'dlstDocStatus.SelectedIndex = -1
    End Sub

    Private Sub dlstDocStatus_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstDocStatus.ItemCommand
        Dim selIndex As Integer
        Dim ds As New DataSet
        Dim sr As New System.IO.StringReader(CStr(ViewState("ds")))
        Dim strGUID As String
        Dim objCommon As New CommonUtilities
        txtRowIds.Text = e.CommandArgument.ToString
        Try
            strGUID = dlstDocStatus.DataKeys(e.Item.ItemIndex).ToString()
            objCommon.PopulatePage(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), strGUID)
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            ViewState("MODE") = "EDIT"

            selIndex = e.Item.ItemIndex
            ViewState("SelectedIndex") = selIndex
            ds.ReadXml(sr)
            PopulateDataList(ds, ViewState("SelectedIndex"), "Item")
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub dlstAgencySp_ItemCommand" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub dlstAgencySp_ItemCommand" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStatus.CheckedChanged
        Dim objListGen As New DataListGenerator
        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim sw As New System.IO.StringWriter
        Dim objCommon As New CommonUtilities
        Dim dv2 As New DataView
        '        Dim sStatusId As String
        Try
            ClearRHS()
            objCommon.SetBtnState(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            ViewState("MODE") = "NEW"
            txtDocStatusId.Text = System.Guid.NewGuid.ToString
            ds = objListGen.SummaryListGenerator(userId, campusId)
            PopulateDataList(ds, "-1", "Check")
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " "
            Else
                Session("Error") = "Error in Sub chkStatus_CheckedChanged" & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        ' CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub
End Class
