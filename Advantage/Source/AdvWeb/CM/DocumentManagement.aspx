﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Documentmanagement.aspx.vb" Inherits="DocumentViewer" MasterPageFile="~/NewSite.master" EnableViewState="false" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <div id='frame'>
        <script type="text/javascript">
            var USER_NAME = '<%=Session("UserName") %>';
            $('#hdnUserName').val(USER_NAME);
        </script>
        <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 	BorderWidth="0px">
		<telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
		    
        <script type="text/javascript" src="../Scripts/DataSources/enrollmentStatusesDataSource.js"></script>
        <script type="text/javascript" src="../Scripts/DataSources/programVersionsDataSource.js"></script>
        <script type="text/javascript" src="../Scripts/DataSources/requirementApprovalDataSource.js"></script>
        <script type="text/javascript" src="../Scripts/DataSources/studentsDataSource.js"></script>
        <script type="text/javascript" src="../Scripts/DataSources/studentGroupsDataSource.js"></script>
        <script type="text/javascript" src="../Scripts/DataSources/studentDocumentRequirementsDataSource.js"></script>
        <script type="text/javascript" src="../Scripts/DataSources/documentFilesDataSource.js"></script>
        <script type="text/javascript" src="../Scripts/DataSources/studentDocumentDataSource.js"></script>
        <script type="text/javascript" src="../Scripts/DataSources/documentStatusDataSource.js"></script>
        <script type="text/javascript" src="../Scripts/DataSources/requirementApprovalDataSource.js"></script>
        <script type="text/javascript" src="../Scripts/jquery.uploadify.js"></script>
        <script type="text/javascript" src="../Scripts/DataSources/dataSourceCommon.js"></script>
        <script type="text/javascript" src="../Scripts/common/kendoControlSetup.js"></script>
        <script type="text/javascript" src="../Scripts/tab-util.js"></script>

        
        <script type="text/javascript" src="../Scripts/viewModels/documentViewer.js" ></script>
                
        <div id="viewrContent" style="margin:10px;"  >
            
            <div id="tabstrip" style='display:inline' >
                <ul >
                    <li id="singleStudentTab" class="k-state-active">Search For a Student</li>
                    <li >Search For a Group of Students</li>
                </ul>
                
                
                <div >
                    <table cellpadding="1" id="findSingleStudentForm">
                        <tr >
                            <td valign="bottom" class="tblLabels">Student Number:
                        </td>
                        <td valign="bottom" class="tblLabels">
                            First Name:
                        </td>
                            <td valign="bottom" class="tblLabels">
                                Last Name:
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <input data-bind="value: studentNumber" style="width: 250px" id="txtStudentNumber" />
                            </td>
                            <td>
                                <input data-bind="value: studentFirstName" style="width: 250px" id="txtFirstName" />
                            </td>
                            <td>
                                <input data-bind="value: studentLastName" style="width: 250px" id="txtLastName" />
                            </td>
                            <td>
                                <div id="btnSearchSingle" style="display:inline"><input  type="submit" value="Search" data-bind="click: searchStudentSingle" style="width:100px" class="k-button" /></div>
                                <div id="btnSingleReset" style="display:inline"><input  type="reset" data-bind="click: resetPage" value="Reset" class="k-button" /></div>
                            </td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                </div>
                <div >
                    <table cellpadding="1" id="findMultipleStudentsForm">
                        <tr >
                            <td valign="bottom" class="tblLabels" valign="bottom">Program Version:</td>
                            <td valign="bottom" class="tblLabels" valign="bottom">Start Date:</td>
                            <td valign="bottom" class="tblLabels" valign="bottom">Enroll Status:</td>
                            <td valign="bottom" class="tblLabels" valign="bottom">Student Group:</td>
                            <td valign="bottom" class="tblLabels" valign="bottom">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <input id="programVersionDropDownList"  data-role="dropdownlist" data-value-field="Id" data-text-field="Description"  data-bind="source: programVersions, value: selectedProgramVersion" style="width:200px" />
                            </td>
                            <td>
                               <input data-role='datepicker' data-bind="value: startDate " style="width:200px" />
                            </td>
                            <td>
                                <input  id="enrollmentStatusDropDownList"  data-role="dropdownlist" data-value-field="Id" data-text-field="Description"  data-bind="source: enrollmentStatuses, value: selectedEnrollmentStatus" style="width:200px">
                            </td>
                            <td>
                                <input id="studentGroupsDropDownList"  data-role="dropdownlist" data-value-field="Id" data-text-field="Description"  data-bind="source: studentGroups, value: selectedStudentGroup" style="width:200px">
                            </td>
                            <td width="200px">
                                <div id="btnSearchMultiple" style="display:inline"><input  type="submit" value="Search" data-bind="click: searchStudentsMutliple" class="k-button" style="width:100px" /></div><div id="btnMultipleReset" style="display:inline"><input  type="reset" value="Reset" data-bind="click: resetPage" class="k-button"/></div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            
            <br/><div class="header">Students </div> 
            <div id="students"  data-bind="source: students"></div>

            <br/><div class="header">Document Requirements <span data-bind="text: studentFullName"></span></div>

            <div id="studentDocuments" data-bind="source: studentDocuments"></div>
            
            <div id="documentDetails" style="display: none">
                
                <div class="k-block">
                    <div class="k-header k-inset header" align="center">Edit/Insert Document Request</div>
                        <br/>
                        <table id="table1" width="98%">
                            <tr>
                                <td align="center" colspan="2"><span data-bind="text: selectedDocument.view()[0].Description" class="tblLabels" ></span><span style="font-weight: bold" class="tblLabels"> Request </span><span data-bind="text: studentFullName" class="tblLabels" /></td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right" >
                                    <span class='tblLabels'>Requested Date:</span>
                                </td>
                                <td >
                                    <input id="dateRequested" data-bind="value: selectedDocument.view()[0].DateRequested" style="width: 250px" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" ><span class='tblLabels'>Received Date:</span></td>
                                <td>
                                    <input id="receivedDate" data-bind="value: selectedDocument.view()[0].DateReceived" style="width: 250px" />
                                </td>
                            </tr>
                            <tr>
                                <td align="right" ><span class='tblLabels'>Status:</span></td>
                                <td>
                                    <input  id="documentStatusDropDownList"  data-role="dropdownlist" data-value-field="Id" data-text-field="Description"  data-bind="source: documentStatuses, value: selectedDocument.view()[0].DocumentStatusId" data-value-primitive="true"  style="width:250px"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" >&nbsp;</td>
                                <td>
                                    <div id="btnSetDocument" style="display:inline"><button data-bind="click: updateDocument" class="k-button" style="width:250px" >Set Document</button></div>
                                    <img style="display: none" src="../images/loading2.gif" alt="loading" data-bind="visible: loading"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2"><label class='tblLabels' data-bind='text:documentSetLabel' ></label></td>
                            </tr>
                        </table>
                </div>
                <br/>
                
                <div id="documentUploadIsUnavailable" style="display: none" align="center">
                    <h3 class="header" >Document Upload (Available after Document is set)</h3>
                </div>
                
                <div id="documentUpload" style="display: none">
                    <div class="k-block">
                        <div class="k-header k-inset header" align="center">Document Upload</div>
                        <br/>
                            <table width="98%">
                                <tr>
                                    <td align="center">
                                
                                        <input type="file" id="fileInput" name="fileInput" title="Click to choose a file to upload" />

                                    </td>
                            
                                </tr>
                                    <td align="center"><button data-bind="click: upload" class="k-button" style="width:300px">Step 2: Upload File</button></td>
                                
                            </table>
                            <br/>
                    </div>
                    <br/>
                   <div class="k-block"  >
                        <div class="k-header k-inset header" align="center">Document History</div>
                       <br/>
                        <table width="98%">
                            <tr>
                                <td align="center"><div id="documentHistory" data-bind="source: documentHistory"></div></td>
                            </tr>
                        </table>
                        <br/>
                    </div>
                 </div>
                </div>               
            </div>
        
        <div id="errorDetails" style="display: none"></div>
        <input type="hidden" id="hdnUserName" runat="server" class="hdnUserName" ClientIDMode="Static"/>
        <input type="hidden" id="hdnUserId" runat="server" class="hdnUserName" ClientIDMode="Static"/>
            <input type="hidden" id="hdnTest" runat="server" ClientIDMode="Static"/>
        <br/><br/>
         </telerik:RadPane>
        </telerik:RadSplitter>
    </div>
</asp:Content>
