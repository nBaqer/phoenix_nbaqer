﻿Imports Telerik.Web.UI

Partial Class StudentInfo
    Inherits BasePage

    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim radalertscript As String = "<script language='javascript'>function f(){radalert('My Alert Box Message!', 330, 210); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>"
        Page.ClientScript.RegisterStartupScript(Me.[GetType](), "radalert", radalertscript)
    End Sub
    Protected Sub Button2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim scriptstring As String = "radalert('Welcome to Rad<b>window</b>!', 330, 210);"
        ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            'Dim StudentTabBar_Master As RadTabStrip = Master.FindControl("StudentTabBar")
            'StudentTabBar_Master.DataBind()

            RadStudentTabs.SelectedIndex = 0
            'AddTab("Customers")
            AddPageView(RadStudentTabs.FindTabByText("Profile"))
            'AddTab("Products")
            'AddTab("Orders")
        Else
            ' QueryStudents(String.Empty)
        End If
        'ItemsPerRequest = 10

        'Dim mylayout As String = Page.Request.Params("layout")
        ' Dim RadPanel1_Master As RadPane = Master.FindControl("RadPane1")
        ' RadPanel1_Master.Width = 240
        Page.Title = "Student Info"
    End Sub
    Private Sub AddTab(ByVal tabName As String)
        Dim tab As RadTab = New RadTab
        tab.Text = tabName
        RadStudentTabs.Tabs.Add(tab)
    End Sub

    Protected Sub RadMultiPage1_PageViewCreated(ByVal sender As Object, ByVal e As RadMultiPageEventArgs) Handles RadMultiPage1.PageViewCreated
        Dim userControlName As String = "~/usercontrols/" & e.PageView.ID & ".ascx"
        Dim userControl As Control = Page.LoadControl(userControlName)
        userControl.ID = e.PageView.ID & "_userControl"
        e.PageView.Controls.Add(userControl)
    End Sub
    Private Sub AddPageView(ByVal tab As RadTab)
        Dim pageView As RadPageView = New RadPageView
        pageView.ID = tab.Value
        RadMultiPage1.PageViews.Add(pageView)
        'pageView.CssClass = "pageView"
        tab.PageViewID = pageView.ID
    End Sub
    Protected Sub RadStudentTabs_TabClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadTabStripEventArgs)
        AddPageView(e.Tab)
        e.Tab.PageView.Selected = True
    End Sub
End Class
