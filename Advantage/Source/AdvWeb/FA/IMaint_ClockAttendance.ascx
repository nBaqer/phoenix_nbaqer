<%@ Control Language="VB" AutoEventWireup="false" CodeFile="IMaint_ClockAttendance.ascx.vb"
    Inherits="FA_IMaint_ClockAttendance" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Scripts>
        <asp:ScriptReference Path="../Scripts/Advantage.Client.AR.js"></asp:ScriptReference>
    </Scripts>
</asp:ScriptManagerProxy>

<style>
    .rfdSkinnedButton {
        margin-right: 10px;
    }
</style>
<script language="javascript" type="text/javascript">

    var setvalOnFly = setInterval(function () {
        var stuEnrollId = localStorage.getItem("curRowstuEnrollId");
        if (stuEnrollId != null) {
            var dayOfWeekVal = localStorage.getItem("dayOfWeekVal");
            var ActualVal = localStorage.getItem("ValAct");
            if (ActualVal == 0) {
                ActualVal = "";
            }
            var $row = $('.RptAttItemName :input[value=' + stuEnrollId + ']').parent().parent()
            var col0 = $row.find("input[id*='ContentMain2_ctl02_rptAttendance_txtD0']")
            var col1 = $row.find("input[id*='ContentMain2_ctl02_rptAttendance_txtD1']")
            var col2 = $row.find("input[id*='ContentMain2_ctl02_rptAttendance_txtD2']")
            var col3 = $row.find("input[id*='ContentMain2_ctl02_rptAttendance_txtD3']")
            var col4 = $row.find("input[id*='ContentMain2_ctl02_rptAttendance_txtD4']")
            var col5 = $row.find("input[id*='ContentMain2_ctl02_rptAttendance_txtD5']")
            var col6 = $row.find("input[id*='ContentMain2_ctl02_rptAttendance_txtD6']")
            var total = $row.find("span[id*='ContentMain2_ctl02_rptAttendance_lblTotal']")
            var ValAbs = parseFloat(localStorage.getItem("ValAbs"));
            var ValMakeup = parseFloat(localStorage.getItem("ValMakeup"));
            var ValScheduled = parseFloat(localStorage.getItem("ValScheduled"));
            var ValTardy = localStorage.getItem("ValTardy");
            var bgColorVal = "White";
            //if (parseFloat(ValAbs) > 0 && parseFloat(ActualVal) != 0) {
            //    bgColorVal = "LightBlue";
            //}
            if (parseFloat(ValMakeup) > 0) {
                bgColorVal = "LightSalmon";
            }
            if (ValTardy == "True") {
                bgColorVal = "BlanchedAlmond";
            }

            switch (dayOfWeekVal) {
                case "0":

                    col0.val(ActualVal);
                    col0.parent().attr('bgcolor', bgColorVal)
                    col0.css('background-color', bgColorVal);
                    break;
                case "1":
                    col1.val(ActualVal);
                    col1.parent().attr('bgcolor', bgColorVal)
                    col1.css('background-color', bgColorVal);
                    break;
                case "2":
                    col2.val(ActualVal);
                    col2.parent().attr('bgcolor', bgColorVal)
                    col2.css('background-color', bgColorVal);
                    break;
                case "3":
                    col3.val(ActualVal);
                    col3.parent().attr('bgcolor', bgColorVal)
                    col3.css('background-color', bgColorVal);
                    break;
                case "4":
                    col4.val(ActualVal);
                    col4.parent().attr('bgcolor', bgColorVal)
                    col4.css('background-color', bgColorVal);
                    break;
                case "5":
                    col5.val(ActualVal);
                    col5.parent().attr('bgcolor', bgColorVal)
                    col5.css('background-color', bgColorVal);
                    break;
                case "6":
                    col6.val(ActualVal);
                    col6.parent().attr('bgcolor', bgColorVal)
                    col6.css('background-color', bgColorVal);
                    break;
                default:
                    break;
            }
            var sumTotal = 0.0;
            if (col0.val() != "") {
                sumTotal += parseFloat(col0.val());
            }
            if (col1.val() != "") {
                sumTotal += parseFloat(col1.val());
            }
            if (col2.val() != "") {
                sumTotal += parseFloat(col2.val());
            }
            if (col3.val() != "") {
                sumTotal += parseFloat(col3.val());
            }
            if (col4.val() != "") {
                sumTotal += parseFloat(col4.val());
            }
            if (col5.val() != "") {
                sumTotal += parseFloat(col5.val());
            }
            if (col6.val() != "") {
                sumTotal += parseFloat(col6.val());
            }
            total.text(sumTotal);
        }
        //clearInterval(setvalOnFly);
    },
        2000);

    function OnEditHours(studName, startDate, useTimeClock, schedId, unitTypeDescrip, studentStartDate, statusCode, dateDropped, stuEnrollId, source, expGradDate, tardytrack, prgverid, userid, campusid,
        idh0, idh1, idh2, idh3, idh4, idh5, idh6,
        ids0, ids1, ids2, ids3, ids4, ids5, ids6,
        ida0, ida1, ida2, ida3, ida4, ida5, ida6,
        idt0, idt1, idt2, idt3, idt4, idt5, idt6,
        idAp0, idAp1, idAp2, idAp3, idAp4, idAp5, idAp6,
        idd0, idd1, idd2, idd3, idd4, idd5, idd6) {
        var ahrs0 = document.getElementById(idh0).value;
        var ahrs1 = document.getElementById(idh1).value;
        var ahrs2 = document.getElementById(idh2).value;
        var ahrs3 = document.getElementById(idh3).value;
        var ahrs4 = document.getElementById(idh4).value;
        var ahrs5 = document.getElementById(idh5).value;
        var ahrs6 = document.getElementById(idh6).value;

        //get the actual values from attendance page
        var day0 = ahrs0;
        var day1 = ahrs1;
        var day2 = ahrs2;
        var day3 = ahrs3;
        var day4 = ahrs4;
        var day5 = ahrs5;
        var day6 = ahrs6;

        var t0, t1, t2, t3, t4, t5, t6;
        t0 = document.getElementById(idt0).value;
        t1 = document.getElementById(idt1).value;
        t2 = document.getElementById(idt2).value;
        t3 = document.getElementById(idt3).value;
        t4 = document.getElementById(idt4).value;
        t5 = document.getElementById(idt5).value;
        t6 = document.getElementById(idt6).value;

        if (ahrs0 === 'T') { t0 = 1; }
        if (ahrs1 === 'T') { t1 = 1; }
        if (ahrs2 === 'T') { t2 = 1; }
        if (ahrs3 === 'T') { t3 = 1; }
        if (ahrs4 === 'T') { t4 = 1; }
        if (ahrs5 === 'T') { t5 = 1; }
        if (ahrs6 === 'T') { t6 = 1; }

        //in clock hour attendance page attendance should show up as 'P','A'
        //but in details page the value should be 1,0 or empty for the dropdown list to pick up value
        if (unitTypeDescrip === "present") {
            if (ahrs0.substring(0, 1) === 'P' || ahrs0.substring(0, 1) === 'T') {
                ahrs0 = 1;
            }
            else if (ahrs0.substring(0, 1) === 'A') {
                ahrs0 = 0;
            }
            else {
                ahrs0 = 99;
            }
            if (ahrs1.substring(0, 1) === 'P' || ahrs1.substring(0, 1) === 'T') {
                ahrs1 = 1;
            }
            else if (ahrs1.substring(0, 1) === 'A') {
                ahrs1 = 0;
            }
            else {
                ahrs1 = 99;
            }
            if (ahrs2.substring(0, 1) === 'P' || ahrs2.substring(0, 1) === 'T') {
                ahrs2 = 1;
            }
            else if (ahrs2.substring(0, 1) === 'A') {
                ahrs2 = 0;
            }
            else {
                ahrs2 = 99;
            }
            if (ahrs3.substring(0, 1) === 'P' || ahrs3.substring(0, 1) === 'T') {
                ahrs3 = 1;
            }
            else if (ahrs3.substring(0, 1) === 'A') {
                ahrs3 = 0;
            }
            else {
                ahrs3 = 99;
            }
            if (ahrs4.substring(0, 1) === 'P' || ahrs4.substring(0, 1) === 'T') {
                ahrs4 = 1;
            }
            else if (ahrs4.substring(0, 1) === 'A') {
                ahrs4 = 0;
            }
            else {
                ahrs4 = 99;
            }
            if (ahrs5.substring(0, 1) === 'P' || ahrs5.substring(0, 1) === 'T') {
                ahrs5 = 1;
            }
            else if (ahrs5.substring(0, 1) === 'A') {
                ahrs5 = 0;
            }
            else {
                ahrs5 = 99;
            }
            if (ahrs6.substring(0, 1) === 'P' || ahrs6.substring(0, 1) === 'T') {
                ahrs6 = 1;
            }
            else if (ahrs6.substring(0, 1) === 'A') {
                ahrs6 = 0;
            }
            else {
                ahrs6 = 99;
            }
        }
        var turl = 'ClockAttendanceDetails.aspx?sid=' + schedId +
            '&sname=' + studName +
            '&date=' + startDate +
            '&timeclock=' + useTimeClock +
            '&unittypedescrip=' + unitTypeDescrip +
            '&studentstartdate=' + studentStartDate +
            '&studentstatuscode=' + statusCode +
            '&datedropped=' + dateDropped +
            '&stuenrollid=' + stuEnrollId +
            '&expgraddate=' + expGradDate +
            '&tardytrack=' + tardytrack +
            '&prgverid=' + prgverid +
            '&ahrs0=' + ahrs0 +
            '&ahrs1=' + ahrs1 +
            '&ahrs2=' + ahrs2 +
            '&ahrs3=' + ahrs3 +
            '&ahrs4=' + ahrs4 +
            '&ahrs5=' + ahrs5 +
            '&ahrs6=' + ahrs6 +
            '&shrs0=' + document.getElementById(ids0).value +
            '&shrs1=' + document.getElementById(ids1).value +
            '&shrs2=' + document.getElementById(ids2).value +
            '&shrs3=' + document.getElementById(ids3).value +
            '&shrs4=' + document.getElementById(ids4).value +
            '&shrs5=' + document.getElementById(ids5).value +
            '&shrs6=' + document.getElementById(ids6).value +
            '&at0=' + t0 +
            '&at1=' + t1 +
            '&at2=' + t2 +
            '&at3=' + t3 +
            '&at4=' + t4 +
            '&at5=' + t5 +
            '&at6=' + t6 +
            '&AP0=' + document.getElementById(idAp0).value +
            '&AP1=' + document.getElementById(idAp1).value +
            '&AP2=' + document.getElementById(idAp2).value +
            '&AP3=' + document.getElementById(idAp3).value +
            '&AP4=' + document.getElementById(idAp4).value +
            '&AP5=' + document.getElementById(idAp5).value +
            '&AP6=' + document.getElementById(idAp6).value +
            '&day0=' + day0 +
            '&day1=' + day1 +
            '&day2=' + day2 +
            '&day3=' + day3 +
            '&day4=' + day4 +
            '&day5=' + day5 +
            '&day6=' + day6 +
            '&userid=' + userid +
            '&campusid=' + campusid +
            '&source=' + source +
            '&idd0=' + document.getElementById(idd0).value +
            '&idd1=' + document.getElementById(idd1).value +
            '&idd2=' + document.getElementById(idd2).value +
            '&idd3=' + document.getElementById(idd3).value +
            '&idd4=' + document.getElementById(idd4).value +
            '&idd5=' + document.getElementById(idd5).value +
            '&idd6=' + document.getElementById(idd6).value;

        var setvaluesToUi = function (value) {
            if (value !== undefined && value !== null && value !== '') {
                var vals = value.split(',');
                // update the schedule hours
                if (unitTypeDescrip.substring(0, 5) === 'clock') {
                    document.getElementById(ids0).value = vals[0];
                    document.getElementById(ids1).value = vals[1];
                    document.getElementById(ids2).value = vals[2];
                    document.getElementById(ids3).value = vals[3];
                    document.getElementById(ids4).value = vals[4];
                    document.getElementById(ids5).value = vals[5];
                    document.getElementById(ids6).value = vals[6];
                    // update actual hours that are visible
                    document.getElementById(idh0).value = vals[7];
                    document.getElementById(idh1).value = vals[8];
                    document.getElementById(idh2).value = vals[9];
                    document.getElementById(idh3).value = vals[10];
                    document.getElementById(idh4).value = vals[11];
                    document.getElementById(idh5).value = vals[12];
                    document.getElementById(idh6).value = vals[13];
                    // update actual hours that go into the hidden field
                    document.getElementById(ida0).value = vals[7];
                    document.getElementById(ida1).value = vals[8];
                    document.getElementById(ida2).value = vals[9];
                    document.getElementById(ida3).value = vals[10];
                    document.getElementById(ida4).value = vals[11];
                    document.getElementById(ida5).value = vals[12];
                    document.getElementById(ida6).value = vals[13];
                }
                else if (unitTypeDescrip === 'minutes') {
                    document.getElementById(ids0).value = vals[0];
                    document.getElementById(ids1).value = vals[1];
                    document.getElementById(ids2).value = vals[2];
                    document.getElementById(ids3).value = vals[3];
                    document.getElementById(ids4).value = vals[4];
                    document.getElementById(ids5).value = vals[5];
                    document.getElementById(ids6).value = vals[6];
                    // update actual hours that are visible
                    document.getElementById(idh0).value = vals[7];
                    document.getElementById(idh1).value = vals[8];
                    document.getElementById(idh2).value = vals[9];
                    document.getElementById(idh3).value = vals[10];
                    document.getElementById(idh4).value = vals[11];
                    document.getElementById(idh5).value = vals[12];
                    document.getElementById(idh6).value = vals[13];
                    // update actual hours that go into the hidden field
                    document.getElementById(ida0).value = vals[7];
                    document.getElementById(ida1).value = vals[8];
                    document.getElementById(ida2).value = vals[9];
                    document.getElementById(ida3).value = vals[10];
                    document.getElementById(ida4).value = vals[11];
                    document.getElementById(ida5).value = vals[12];
                    document.getElementById(ida6).value = vals[13];

                    //update track tardies fields
                    document.getElementById(idt0).value = vals[14];
                    document.getElementById(idt1).value = vals[15];
                    document.getElementById(idt2).value = vals[16];
                    document.getElementById(idt3).value = vals[17];
                    document.getElementById(idt4).value = vals[18];
                    document.getElementById(idt5).value = vals[19];
                    document.getElementById(idt6).value = vals[20];
                }
                else if (unitTypeDescrip.substring(0, 7) === 'present') {
                    document.getElementById(ids0).value = vals[0];
                    document.getElementById(ids1).value = vals[1];
                    document.getElementById(ids2).value = vals[2];
                    document.getElementById(ids3).value = vals[3];
                    document.getElementById(ids4).value = vals[4];
                    document.getElementById(ids5).value = vals[5];
                    document.getElementById(ids6).value = vals[6];
                }
            }
        }

        if (!window.showModalDialog) {
            window.showModalDialog = function (arg1, arg2, arg3) {
                var width = 0;
                var height = 0;
                var resizable = "no";
                var scroll = "no";
                var status = "no";

                // get the modal specs
                var mdattrs = arg3.split(";");
                for (var i = 0; i < mdattrs.length; i++) {
                    var mdattr = mdattrs[i].split(":");

                    var n = mdattr[0];
                    var v = mdattr[1];
                    if (n) {
                        n = n.trim().toLowerCase();
                    }
                    if (v) {
                        v = v.trim().toLowerCase();
                    }

                    if (n === "dialogheight") {
                        height = v.replace("px", "");
                    } else if (n === "dialogwidth") {
                        width = v.replace("px", "");
                    } else if (n === "resizable") {
                        resizable = v;
                    } else if (n === "scroll") {
                        scroll = v;
                    } else if (n === "status") {
                        status = v;
                    }
                }

                var left = window.screenX + (window.outerWidth / 2) - (width / 2);
                var top = window.screenY + (window.outerHeight / 2) - (height / 2);
                var targetWin = window.open(arg1,
                    arg1,
                    'toolbar=no, location=no, directories=no, status=' +
                    status +
                    ', menubar=no, scrollbars=' +
                    scroll +
                    ', resizable=' +
                    resizable +
                    ', copyhistory=no, width=' +
                    width +
                    ', height=' +
                    height +
                    ', top=' +
                    top +
                    ', left=' +
                    left);
                targetWin.focus();
                targetWin.onbeforeunload = function () {
                    setvaluesToUi('<%= Session("ClockAttendance") %>');
                }
            };
        }

        var strReturn = window.showModalDialog(turl, null, 'resizable:no;status:no;dialogWidth:650px;dialogHeight:290px;dialogHide:true;help:no;scroll:yes');
        setvaluesToUi(strReturn);
    }
</script>
<script language="javascript" type="text/javascript">
    function AddComments(idh0, idh1, idh2, idh3, idh4, idh5, idh6) {
        var comments = document.getElementById(idh0).value;
        var turl = 'AttendanceComments.aspx?commentsdesc=' + comments;
        var strReturn = window.showModalDialog(turl, null, 'resizable:no;status:no;dialogWidth:650px;dialogHeight:290px;dialogHide:true;help:no;scroll:yes');
        if (strReturn !== undefined && strReturn !== null && strReturn !== '') {
            document.getElementById(idh0).value = strReturn;
        }
    }
</script>
<div style="text-align: left; width: 100%; padding-top: 25px;">
    <asp:Label ID="lblMsg" runat="server" CssClass="LabelBold" ForeColor="Red" />
</div>
<div id="divContent" runat="server">
    <br />
    <div style="text-align: center">
        <asp:Button ID="btnPostComments" runat="server" Text="Post Comments" />
        <asp:Button ID="btnImportFile" runat="server" Text="Import File" ToolTip="Imports a time clock file." Visible="False" />
        <asp:Button ID="btnpostzero" runat="server" Text="Post Zeros for Attendance" ClientIDMode="Static" ToolTip="Posts Zero for all the Time Clock Students" OnClientClick="return false" />
        <asp:Button ID="btnPostByException" runat="server" Text="Post by Exception" ToolTip="Sets unposted hours to the scheduled hours for that day." />
        <asp:Button ID="btnShowPostedValues" runat="server" ClientIDMode="Static" Text="Refresh" />
    </div>
    <div class="TopCalendarHeader">
        <asp:Label Text="Weekly View -" ID="lblView" runat="server" CssClass="LabelBoldcalendar" />
        <asp:Label ID="lblDate" runat="server" CssClass="LabelBoldcalendar" />
        <asp:LinkButton ID="lnkPrev" runat="server" ToolTip="Previous week" CssClass="DateSelector"
            Text="<<" />
        <asp:Label ID="lblWeekOf" runat="server" CssClass="DateSelector" />
        <asp:LinkButton ID="lnkNext" runat="server" ToolTip="Next week" CssClass="DateSelector"
            Text=">>" />
    </div>
    <!-- The attendance repeater header -->
    <div>
        <asp:Repeater ID="rptAttendance" runat="server">
            <HeaderTemplate>
                <table cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #e0e0e0;">
                    <tr>
                        <td class="RptAttItemTotal" nowrap="true">&nbsp;</td>
                        <td class="RptAttItemName" nowrap="true">Student Name</td>
                        <td class="RptAttItemHeader" nowrap="true">
                            <asp:Label ID="lblD0" runat="server" />
                            <asp:HiddenField ID="hfD0" runat="server" />
                        </td>
                        <td class="RptAttItemHeader" nowrap="true">
                            <asp:Label ID="lblD1" runat="server" />
                            <asp:HiddenField ID="hfD1" runat="server" />

                        </td>
                        <td class="RptAttItemHeader" nowrap="true">
                            <asp:Label ID="lblD2" runat="server" />
                            <asp:HiddenField ID="hfD2" runat="server" />

                        </td>
                        <td class="RptAttItemHeader" nowrap="true">
                            <asp:Label ID="lblD3" runat="server" />
                            <asp:HiddenField ID="hfD3" runat="server" />

                        </td>
                        <td class="RptAttItemHeader" nowrap="true">
                            <asp:Label ID="lblD4" runat="server" />
                            <asp:HiddenField ID="hfD4" runat="server" />

                        </td>
                        <td class="RptAttItemHeader" nowrap="true">
                            <asp:Label ID="lblD5" runat="server" />
                            <asp:HiddenField ID="hfD5" runat="server" />

                        </td>
                        <td class="RptAttItemHeader" nowrap="true">
                            <asp:Label ID="lblD6" runat="server" />
                            <asp:HiddenField ID="hfD6" runat="server" />

                        </td>
                        <td class="RptAttHeaderTotal" style="border-right: 0" nowrap="true">
                            <asp:Label ID="lblTotal" runat="server" Text="Weekly Total" /></td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td class="RptAttItemTotal" nowrap="true">
                        <asp:LinkButton ID="ibStudentInfo" runat="server">
                            <span class="k-icon k-i-information font-black"></span>
                        </asp:LinkButton>
                        <telerik:RadToolTip ID="RadToolTip1" runat="server" TargetControlID="ibStudentInfo" RelativeTo="Element"
                            Position="TopCenter" RenderInPageRoot="true" EnableShadow="true" HideEvent="ManualClose">
                            <div id="ToolTipWrapper" style="padding: 20px;"></div>
                        </telerik:RadToolTip>


                        <asp:LinkButton runat="server" ID="ibToMaintPage_Schedule" runat="server" CommandName="ToMaintGrdComps" ToolTip="View student's schedule" PostBackUrl="javascript:void(0);">
                            <span class="k-icon k-i-calendar font-black"></span>
                        </asp:LinkButton>
                    </td>
                    <td class="RptAttItemName" nowrap="true">
                        <asp:HiddenField ID="hfStuEnrollId" runat="server" />
                        <asp:HiddenField ID="hfScheduleId" runat="server" />
                        <asp:HiddenField ID="hfSysStatusId" runat="server" />
                        <asp:HiddenField ID="hfUseTimeClock" runat="server" />
                        <asp:HiddenField ID="hfUnitTypeDescrip" runat="server" />
                        <asp:HiddenField ID="hfattType" runat="server" />
                        <asp:HiddenField ID="hfSource" runat="Server" />
                        <asp:HiddenField ID="hfStudentStartDate" runat="Server" />
                        <asp:HiddenField ID="hfGradDate" runat="server" />
                        <asp:HiddenField ID="hfDateDetermined" runat="server" />
                        <asp:HyperLink ID="btnStudentName" runat="server" ToolTip="Click to edit attendance" /></td>
                    <td class="RptAttItem" id="i0" runat="server" nowrap="true">
                        <asp:HiddenField ID="hfS0" runat="server" />
                        <asp:HiddenField ID="hfA0" runat="server" Visible="true" />
                        <asp:HiddenField ID="hfT0" runat="Server" />
                        <asp:HiddenField ID="hfAP0" runat="server" />
                        <asp:HiddenField ID="hfException0" runat="server" />
                        <asp:HiddenField ID="hfScheduleChanged0" runat="server" />
                        <asp:HiddenField ID="hfHol0" runat="server" />
                        <asp:HiddenField ID="hfPunchInTime0" runat="server" />
                        <asp:HiddenField ID="hfDeadLineBeforeConsideredTardy0" runat="server" />
                        <asp:HiddenField ID="hfMaxInTime0" runat="server" />
                        <%--<asp:HiddenField ID="hfSched0" runat="server" />--%>

                        <asp:TextBox runat="server" ID="txtD0" CssClass="HoursTextBox" Width="50" />
                        <asp:HiddenField ID="hfDisabled0" runat="server" />
                    </td>
                    <td class="RptAttItem" id="i1" runat="server" nowrap="true">
                        <asp:HiddenField ID="hfS1" runat="server" />
                        <asp:HiddenField ID="hfA1" runat="server" />
                        <asp:HiddenField ID="hfT1" runat="Server" />
                        <asp:HiddenField ID="hfAP1" runat="server" />
                        <asp:HiddenField ID="hfHol1" runat="server" />
                        <asp:HiddenField ID="hfScheduleChanged1" runat="server" />
                        <asp:HiddenField ID="hfException1" runat="server" />
                        <asp:HiddenField ID="hfPunchInTime1" runat="server" />
                        <asp:HiddenField ID="hfDeadLineBeforeConsideredTardy1" runat="server" />
                        <asp:HiddenField ID="hfMaxInTime1" runat="server" />

                        <%-- <asp:HiddenField ID="hfSched1" runat="server" />    --%>
                        <asp:TextBox runat="server" ID="txtD1" CssClass="HoursTextBox" Width="50" />
                        <asp:HiddenField ID="hfDisabled1" runat="server" />
                    </td>
                    <td class="RptAttItem" id="i2" runat="server" nowrap="true">
                        <asp:HiddenField ID="hfS2" runat="server" />
                        <asp:HiddenField ID="hfA2" runat="server" />
                        <asp:HiddenField ID="hfT2" runat="Server" />
                        <asp:HiddenField ID="hfAP2" runat="server" />
                        <asp:HiddenField ID="hfHol2" runat="server" />
                        <asp:HiddenField ID="hfScheduleChanged2" runat="server" />
                        <asp:HiddenField ID="hfException2" runat="server" />
                        <asp:HiddenField ID="hfPunchInTime2" runat="server" />
                        <asp:HiddenField ID="hfDeadLineBeforeConsideredTardy2" runat="server" />
                        <asp:HiddenField ID="hfMaxInTime2" runat="server" />

                        <%-- <asp:HiddenField ID="hfSched2" runat="server" />--%>
                        <asp:TextBox runat="server" ID="txtD2" CssClass="HoursTextBox" Width="50" />
                        <asp:HiddenField ID="hfDisabled2" runat="server" />
                    </td>
                    <td class="RptAttItem" id="i3" runat="server" nowrap="true">
                        <asp:HiddenField ID="hfS3" runat="server" />
                        <asp:HiddenField ID="hfA3" runat="server" />
                        <asp:HiddenField ID="hfT3" runat="Server" />
                        <asp:HiddenField ID="hfAP3" runat="server" />
                        <asp:HiddenField ID="hfScheduleChanged3" runat="server" />
                        <asp:HiddenField ID="hfException3" runat="server" />
                        <asp:HiddenField ID="hfHol3" runat="server" />
                        <%--  <asp:HiddenField ID="hfSched3" runat="server" />--%>
                        <asp:TextBox runat="server" ID="txtD3" CssClass="HoursTextBox" Width="50" />
                        <asp:HiddenField ID="hfPunchInTime3" runat="server" />
                        <asp:HiddenField ID="hfDeadLineBeforeConsideredTardy3" runat="server" />
                        <asp:HiddenField ID="hfMaxInTime3" runat="server" />
                        <asp:HiddenField ID="hfDisabled3" runat="server" />
                    </td>
                    <td class="RptAttItem" id="i4" runat="server" nowrap="true">
                        <asp:HiddenField ID="hfS4" runat="server" />
                        <asp:HiddenField ID="hfA4" runat="server" />
                        <asp:HiddenField ID="hfT4" runat="Server" />
                        <asp:HiddenField ID="hfAP4" runat="server" />
                        <asp:HiddenField ID="hfHol4" runat="server" />
                        <asp:HiddenField ID="hfScheduleChanged4" runat="server" />
                        <asp:HiddenField ID="hfException4" runat="server" />
                        <asp:HiddenField ID="hfPunchInTime4" runat="server" />
                        <%--  <asp:HiddenField ID="hfSched4" runat="server" />--%>
                        <asp:HiddenField ID="hfDeadLineBeforeConsideredTardy4" runat="server" />
                        <asp:HiddenField ID="hfMaxInTime4" runat="server" />
                        <asp:TextBox runat="server" ID="txtD4" CssClass="HoursTextBox" Width="50" />
                        <asp:HiddenField ID="hfDisabled4" runat="server" />
                    </td>
                    <td class="RptAttItem" id="i5" runat="server" nowrap="true">
                        <asp:HiddenField ID="hfS5" runat="server" />
                        <asp:HiddenField ID="hfA5" runat="server" />
                        <asp:HiddenField ID="hfT5" runat="Server" />
                        <asp:HiddenField ID="hfAP5" runat="server" />
                        <asp:HiddenField ID="hfHol5" runat="server" />
                        <asp:HiddenField ID="hfException5" runat="server" />
                        <asp:HiddenField ID="hfScheduleChanged5" runat="server" />
                        <asp:HiddenField ID="hfPunchInTime5" runat="server" />
                        <%--  <asp:HiddenField ID="hfSched5" runat="server" />--%>
                        <asp:HiddenField ID="hfDeadLineBeforeConsideredTardy5" runat="server" />
                        <asp:HiddenField ID="hfMaxInTime5" runat="server" />
                        <asp:TextBox runat="server" ID="txtD5" CssClass="HoursTextBox" Width="50" />
                        <asp:HiddenField ID="hfDisabled5" runat="server" />
                    </td>
                    <td class="RptAttItem" id="i6" runat="server" nowrap="true">
                        <asp:HiddenField ID="hfS6" runat="server" />
                        <asp:HiddenField ID="hfA6" runat="server" />
                        <asp:HiddenField ID="hfT6" runat="Server" />
                        <asp:HiddenField ID="hfAP6" runat="server" />
                        <asp:HiddenField ID="hfException6" runat="server" />
                        <asp:HiddenField ID="hfHol6" runat="server" />
                        <asp:HiddenField ID="hfScheduleChanged6" runat="server" />
                        <asp:HiddenField ID="hfPunchInTime6" runat="server" />
                        <asp:HiddenField ID="hfDeadLineBeforeConsideredTardy6" runat="server" />
                        <asp:HiddenField ID="hfMaxInTime6" runat="server" />
                        <%-- <asp:HiddenField ID="hfSched6" runat="server" />--%>
                        <asp:TextBox runat="server" ID="txtD6" CssClass="HoursTextBox" Width="50" />
                        <asp:HiddenField ID="hfDisabled6" runat="server" />
                    </td>
                    <td class="RptAttItemTotal" style="border-right: 0" nowrap="true">
                        <asp:Label ID="lblTotal" runat="server" />

                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div style="margin-top: 10px; text-align: right">
        <table>
            <tr align="center">
                <td style="text-align: right" class="LegendLabel">Legend :</td>
                <td style="height: 5px; vertical-align: middle; background-color: White; border: solid 1px #ebebeb; width: 90px">
                    <span class="LegendLabel">Entry</span></td>
                <td style="height: 5px; vertical-align: middle; background-color: LightGrey; border: solid 1px #ebebeb; width: 90px">
                    <span class="LegendLabel">Disabled</span></td>
                <td style="height: 5px; vertical-align: middle; background-color: LightSlateGray; border: solid 1px #ebebeb; width: 90px">
                    <span class="LegendLabel">Non-enrolled</span></td>
                <td style="height: 5px; vertical-align: middle; background-color: LightSalmon; border: solid 1px #ebebeb; width: 90px">
                    <span class="LegendLabel">Makeup</span></td>
                <td style="height: 5px; vertical-align: middle; background-color: LightBlue; border: solid 1px #ebebeb; width: 90px">
                    <span class="LegendLabel">Absent</span></td>
                <td style="height: 5px; vertical-align: middle; background-color: BlanchedAlmond; border: solid 1px #ebebeb; width: 90px">
                    <span class="LegendLabel">Tardy</span></td>
            </tr>
        </table>
    </div>

    <asp:TextBox ID="postZeroDate" runat="server" Style="display: none;" ClientIDMode="Static" />
    <asp:Button ID="okButtonAttendanceWindowASP" runat="server" Text="hiddenClick" Style="display: none;" ClientIDMode="Static" class="confirm_yes k-button margin: 15px" />
</div>

<div id="postAttendanceZeroWindow" style="text-align: center; display: none;">
    <h3 style="border: none !important; background-color: transparent !important;">Please select a date to post a zero value for attendance.</h3>
    <input id="postZeroDateWindow" value="10/10/2011" title="Post Zero Date" style="width: 50%" />

    <p>Advantage will populate zeros for all students shown that do not contain posted attendance for the selected date.</p>
    <p id="postZeroMsg"></p>

    <input id="okButtonAttendanceWindow" type="button" class="confirm_no k-button" value="OK" style="width: 80px; margin: 15px" />
    <input id="noButtonAttendanceWindow" type="button" class="confirm_no k-button" value="Cancel" style="width: 80px; margin: 15px" />
</div>

<asp:HiddenField ID="hfRes" runat="server" />

<script>
    $(document).ready(function () {
        var module = new PostAttendance.PostAttendance();
        module.init();
    });
</script>
