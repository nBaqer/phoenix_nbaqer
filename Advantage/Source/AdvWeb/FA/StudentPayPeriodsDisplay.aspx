﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NewSite.master" CodeFile="StudentPayPeriodsDisplay.aspx.vb" Inherits="StudentPayPeriodsDisplay" %>
<%@ MasterType  virtualPath="~/NewSite.master"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<%-- <link href="../css/style.css" type="text/css" rel="stylesheet"/>--%>
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
				<!-- begin rightcolumn -->
				<TR>
					<td class="detailsframetop"><table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right">
                                    <asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:button>
                                    <asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
										Enabled="False"></asp:button></td>
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
						<table class="maincontenttable" cellpadding="0" cellspacing="0"  border="0px">
            <tr>
                <td class="detailsframe">
                    <div >
                        <!-- begin table content-->

                        <table id="tblEnroll" runat="server" width="100%">
                            <tr align="center">
                                <td>
                                    <asp:Label ID="Label1" CssClass="Label" runat="server" Font-Bold="true">Enrollment(s)</asp:Label>
                                    <asp:DropDownList ID="ddlEnrollmentId" runat="server" CssClass="DropDownListNO100" AutoPostBack="true" OnSelectedIndexChanged="ddlEnrollmentId_SelectedIndexChanged"></asp:DropDownList>
                                </td>                            
                            </tr>                            
                            <tr  align="center">
                                <td>
                                    <asp:Label ID="lblNoPPMsg" CssClass="Label" runat="server" Font-Bold="true" Visible="false">This enrollment has insufficient data to generate payment periods</asp:Label>
                                </td>                                                        
                            </tr>                                           
                        </table>
                        <table id="tblTextBoxes" width="55%" style="margin-left:300;" runat="server">
                            <tr>
                                <td style="width:40%">
                                    <table >
                                        <tr>                                           
                                            <td align="center">
                                                <asp:Label ID="lbPrgVerDesc" CssClass="Label" runat="server" Font-Bold="true">Program Version</asp:Label>
                                            </td>
                                            <td style="width:10px;"></td>
                                            <td align="center">
                                                <asp:Label ID="lbAcadCalType" CssClass="Label" runat="server" Font-Bold="true">Academic Calendar Type</asp:Label>
                                            </td>
                                            <td style="width:10px;"></td>
                                            <td align="center">
                                                <asp:Label ID="lbAcadYrLen" CssClass="Label" runat="server" Font-Bold="true">Academic Year Length</asp:Label>
                                            </td>
                                            <td style="width:10px;"></td>
                                            <td align="center">
                                                <asp:Label ID="lbPayPeriodsPerAcadYr" CssClass="Label" runat="server" Font-Bold="true">Pay Periods per Acdemic Year</asp:Label>
                                            </td>                                            
                                            <td style="width:10px;"></td>
                                            <td align="center">
                                                <asp:Label ID="lbProgLength" CssClass="Label" runat="server" Font-Bold="true">Program Length</asp:Label>
                                            </td>                   
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <asp:TextBox ID="tbPrgVerDesc" runat="server" CssClass="TextBox" style="text-align:center;" ></asp:TextBox>                                                            
                                            </td>
                                            <td style="width:10px;"></td>

                                            <td align="center">
                                                <asp:TextBox ID="tbAcadCalType" runat="server" CssClass="TextBox" style="text-align:center;" ></asp:TextBox>                                                             
                                            </td>
                                            <td style="width:10px;"></td>
                                            <td align="center">
                                                <asp:TextBox ID="tbAcadYrLen" runat="server" CssClass="TextBox" style="text-align:center;" ></asp:TextBox>     
                                            </td>
                                            <td style="width:10px;"></td>
                                            <td align="center">
                                                <asp:TextBox ID="tbPayPeriodsPerAcadYr" runat="server" CssClass="TextBox" style="text-align:center;" ></asp:TextBox>   
                                            </td>
                                            <td style="width:10px;"></td>
                                            <td align="center">
                                                <asp:TextBox ID="tbProgLength" runat="server" CssClass="TextBox" style="text-align:center;" ></asp:TextBox>   
                                            </td>   
                                        </tr>                        
                                    </table>
                                </td>                                
                            </tr>                            
                        </table>
                         <table id="Table2" class="contenttable" width="55%" style="margin-left:300;" runat="server">
	                        <tr>
		                        <td style="width:40%">
                                  <%--<div style="DISPLAY: inline; Z-INDEX: 107; LEFT: 495; OVERFLOW: auto; WIDTH: 100%; POSITION: absolute; TOP: 275px; HEIGHT: 103px">--%>
			                        <asp:DataGrid ID="dgrdStudentPayPeriodsDisplay" runat="server" Width="100%" HorizontalAlign="Center"  
                                        BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True" BorderColor="#E0E0E0" >
                                        <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                        <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                        <HeaderStyle CssClass="DataGridHeaderStyle"></HeaderStyle>
                                        <Columns>
                                            
                                            <asp:BoundColumn DataField="PaymentPeriod" SortExpression="PaymentPeriod" ReadOnly="True"
                                                HeaderText="Payment Period">
                                                <HeaderStyle CssClass="DataGridHeaderStyle" Width="8%"></HeaderStyle>
                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            </asp:BoundColumn>

                                            <asp:BoundColumn DataField="PaymentPeriodRange" SortExpression="PaymentPeriodRange" ReadOnly="True"
                                                HeaderText="Pay Period Range">
                                                <HeaderStyle CssClass="DataGridHeaderStyle" Width="8%"></HeaderStyle>
                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            </asp:BoundColumn>

                                            <asp:BoundColumn DataField="PeriodStartDate" SortExpression="PeriodStartDate" ReadOnly="True"
                                                HeaderText="Period Start Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="DataGridHeaderStyle" Width="8%"></HeaderStyle>
                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            </asp:BoundColumn>

                                            <asp:BoundColumn DataField="PeriodEndDate" SortExpression="PeriodEndDate" ReadOnly="True"
                                                HeaderText="Period End Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="DataGridHeaderStyle" Width="8%"></HeaderStyle>
                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            </asp:BoundColumn>

                                             <%--<asp:BoundColumn DataField="Comments" SortExpression="Comments" ReadOnly="True"
                                                HeaderText="Comments">
                                                <HeaderStyle CssClass="DataGridHeaderStyle" Width="8%"></HeaderStyle>
                                                <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            </asp:BoundColumn>         --%>                                   
                                        </Columns>
                                    </asp:DataGrid>
                                   <%-- </div>--%>
		                        </td>                                
	                        </tr>                            
                        </table>
                    </div>
                </td>
            </tr>
        </table>
					</td>
					<!-- end rightcolumn --></TR>
			</table>
            <!-- start validation panel-->
            <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary">
            </asp:Panel>
            <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary"
            ErrorMessage="CustomValidator" Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators"
                runat="server">
            </asp:Panel>
            <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary"
            ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
            <!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>

