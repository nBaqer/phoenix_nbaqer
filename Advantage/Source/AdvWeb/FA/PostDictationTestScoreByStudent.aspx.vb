﻿Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects

Partial Class PostDictationTestScoreByStudent
    Inherits BasePage

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Private pObj As New UserPagePermissionInfo

    Dim instructorId As String
    Dim username As String
    Dim isAcademicAdvisor As Boolean
    Protected dsGetTermStudentIsCurrentlyRegisteredIn As New DataSet
    Protected dsGetInstructors As New DataSet
    Protected dsGrades As New DataSet
    Protected dsGradeComponentTypes As New DataSet
    Dim StudentId As String
    Dim resourceId As Integer
    Dim campusId As String
    Dim userId As String
    Public dtTerm As DataTable
    Public dtGrades As DataTable
    Public dtInstructors As DataTable
    Public dtGrdCompTypes As DataTable
    Dim ddlTerm As DropDownList
    Dim ddlGrdComponentTypeId As DropDownList
    Dim ddlGrade As DropDownList
    Dim ddlInstructors As DropDownList
    Dim ddlEditMentorProctored As DropDownList
    Dim txtDatePassed As String

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    End Sub

    Protected state As AdvantageSessionState
    Protected LeadId As String
    Protected boolSwitchCampus As Boolean = False
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try
            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)



        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

#End Region


    ' Dim ddlTermCombo As RadComboBox
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim objCommon As New CommonUtilities
        'Dim fac As New UserSecurityFacade

        HttpContext.Current.Items("ResourceId") = 614
        HttpContext.Current.Items("Language") = "En-US"

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(614) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString
        End With

        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = Master.CurrentCampusId
        userId = AdvantageSession.UserState.UserId.ToString

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        If Not Page.IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then

            ViewState("StuEnrollId") = ""
            ViewState("Instructor") = AdvantageSession.UserState.UserId.ToString

            ' PopulateTermDDL()
            ViewState("MODE") = "NEW"
            'txtModUser.Text = Session("UserName")
            'txtModDate.Text = Date.MinValue.ToString
            InitButtonsForLoad()
            RadGrdPostScores.Visible = False
            RadGrdPostScores.MasterTableView.EditMode = GridEditMode.InPlace
            BuildEnrollmentDDL(StudentId)

            Dim boolIsStudentRegisteredInAnyDictationSpeedTestCourse As Boolean = False
            boolIsStudentRegisteredInAnyDictationSpeedTestCourse = (New DictationTestFacade).IsStudentRegisteredInDictationTestCourse(StudentId)
            If boolIsStudentRegisteredInAnyDictationSpeedTestCourse = False Then
                pnlMessagePanel.Visible = True
                pnlRHS.Visible = False
                ddlPrgVerId.Visible = False
                lblPrgVerId.Visible = False
            Else
                pnlMessagePanel.Visible = False
                pnlRHS.Visible = True
                ddlPrgVerId.Visible = True
                lblPrgVerId.Visible = True
                RadGrdPostScores.Visible = True
            End If
            MyBase.uSearchEntityControlId.Value = ""
        Else
            'BuildEnrollmentDDL(StudentId)
            pnlMessagePanel.Visible = True
        End If
    End Sub
    Public Sub ShowStatusMessage()
        'Dim scr As String = "<script language='javascript'>function f(){alert('Welcome to RadWindow ASP.NET AJAX!');}</script>"
        'Dim scriptstring As String = "radalert('Welcome to Rad<strong>Window</strong>!', 330, 210);"
        ' ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert1", scr, True)
        'Dim radalertscript As String = "<script language='javascript'>function f(){radalert('Welcome to RadWindow <strong>ASP.NET AJAX</strong>!', 330, 210); Sys.Application.remove_load(f);}; Sys.Application.add_load(f);</script>"
        'ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", radalertscript, True)

    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub ClearRHS()
        Dim ctl As Control
        Try
            '***************************************************************
            'This section clears the Field Properties section of the page
            '***************************************************************
            For Each ctl In pnlRHS.Controls
                If ctl.GetType Is GetType(TextBox) Then
                    CType(ctl, TextBox).Text = ""
                End If
                If ctl.GetType Is GetType(CheckBox) Then
                    CType(ctl, CheckBox).Checked = False
                End If
                If ctl.GetType Is GetType(DropDownList) Then
                    CType(ctl, DropDownList).SelectedIndex = 0
                End If
            Next
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub ClearRHS " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub ClearRHS " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

    End Sub
    Private Sub InitButtonsForWork()
        If pObj.HasFull Or pObj.HasAdd Or pObj.HasEdit Then
            btnSave.Enabled = True
        End If
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub

    Private Sub InitButtonsForLoad()
        btnSave.Enabled = False
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub

    Protected Sub Page_LoadComplete(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.LoadComplete

    End Sub
    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add buttons 
        controlsToIgnore.Add(btnSave)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
        BindToolTip()
    End Sub

    Private Sub BuildTerm()
    End Sub
    Private Sub BuildRadGrid()
        RadGrdPostScores.DataSource = (New GrdPostingsFacade).GetDictationScorePostedByStudentEnrollment(ddlPrgVerId.SelectedValue)
        RadGrdPostScores.DataBind()
        ApplyPermissionToRadGrid()
    End Sub
    Protected Sub ddlPrgVerId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrgVerId.SelectedIndexChanged
        If ddlPrgVerId.SelectedIndex > 0 Then
            Dim boolIsStudentRegisteredInAnyDictationSpeedTestCourse As Boolean = False
            boolIsStudentRegisteredInAnyDictationSpeedTestCourse = (New DictationTestFacade).IsStudentRegisteredInDictationTestCourseByEnrollment(StudentId, _
                                                                                                                                                ddlPrgVerId.SelectedValue)
            If boolIsStudentRegisteredInAnyDictationSpeedTestCourse = False Then
                pnlMessagePanel.Visible = True
                lblMessage.Text = "The student is not registered in any of the courses " & _
                                    " mapped to ""Dictation/Speed Test"" Grade Component for the selected " & "<br>" & " enrollment " & ddlPrgVerId.SelectedItem.Text & " and it is not possible " & _
                                    " to post score for this student at this point of time." & "<br><br>" & _
                                    " Please register the student in any of the course mapped to " & _
                                    "Dictation/Speed Test Grade Component to post score. "
                RadGrdPostScores.Visible = False
            Else
                pnlMessagePanel.Visible = False
                RadGrdPostScores.Visible = True
                BuildDataTables(ddlPrgVerId.SelectedValue)
                BuildradGridandChildDDLS()
            End If
        Else
            pnlMessagePanel.Visible = False
            RadGrdPostScores.Visible = False
        End If
    End Sub
    Private Sub BuildDataTables(ByVal PrgVerId As String)

        'dtTerm = CType(Cache("dtTerm"), DataTable)
        'If dtTerm Is Nothing Then
        dtTerm = (New GrdPostingsFacade).GetTermStudentIsCurrentlyRegisteredIn(StudentId, campusId).Tables(0)
        'Cache("dtTerm") = dtTerm
        'End If

        'dtGrades = CType(Cache("dtGrades"), DataTable)
        'If dtGrades Is Nothing Then
        dtGrades = (New GrdPostingsFacade).GetGrades(PrgVerId).Tables(0)
        'Cache("dtGrades") = dtGrades
        'End If

        'dtInstructors = CType(Cache("dtInstructors"), DataTable)
        'If dtInstructors Is Nothing Then
        dtInstructors = (New GrdPostingsFacade).GetInstructorsByCampusId(campusId).Tables(0)
        'Cache("dtInstructors") = dtInstructors
        'End If

        'dtGrdCompTypes = CType(Cache("dtGrdCompTypes"), DataTable)
        'If dtGrdCompTypes Is Nothing Then
        dtGrdCompTypes = (New GrdPostingsFacade).GetGradeComponentsMappedToSystemComponent(612).Tables(0)
        'Cache("dtGrdCompTypes") = dtGrdCompTypes
        'End If
    End Sub
    Private Sub BuildradGridandChildDDLS()
        BuildRadGrid()
        'BuildTerm()
    End Sub
    Private Sub BuildEnrollmentDDL(ByVal Studentid As String)
        Dim GrdPostingFacade As New GrdPostingsFacade
        Dim ds As New DataSet
        ds = GrdPostingFacade.GetEnrollmentsByStudent(Studentid)
        With ddlPrgVerId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = ds
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        If ds.Tables(0).Rows.Count = 1 Then
            ddlPrgVerId.SelectedIndex = 1
            BuildDataTables(ddlPrgVerId.SelectedValue)
            BuildradGridandChildDDLS()
        End If
    End Sub
    Protected Sub radGrdPostScores_DeleteCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdPostScores.DeleteCommand
        Dim item As GridDataItem = DirectCast(e.Item, GridDataItem)
        If pObj.HasDelete = True Then
            RadGrdPostScores.AllowAutomaticDeletes = True
        ElseIf pObj.HasDelete = False Then
            Dim cell As TableCell = CType(e.Item, GridDataItem)("DeleteColumn")
            Dim lnk As ImageButton = CType(cell.Controls(0), ImageButton)
            Dim scriptstring As String = "radalert('User does not have permission to delete the posted score', 280, 100);"
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
            Exit Sub
        End If
        Dim strId As String = item.OwnerTableView.DataKeyValues(item.ItemIndex)("id").ToString()
        Dim grdPostFacade As New GrdPostingsFacade
        grdPostFacade.DeletePostedScoresForDictationSpeedTest(strId)
        e.Canceled = True
        RadGrdPostScores.Rebind()
    End Sub
    Protected Sub radGrdPostScores_InsertCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdPostScores.InsertCommand
        Dim insertedItem As GridDataInsertItem = DirectCast(e.Item, GridDataInsertItem)

        Dim tbl As New DataTable("PostScores")
        Dim strTermId As String = ""
        Dim dtDatePassed As Date = #1/1/1900#
        Dim strTestType As String = ""
        Dim decAccuracy As Decimal = 0
        Dim decSpeed As Decimal = 0
        Dim strGrdSysDetailId As String = ""
        Dim strInstructorId As String = ""
        Dim strModDate As DateTime = Date.Now
        Dim intSelectedCount As Integer = 0
        Dim strModUser As String = Session("User")
        Dim strStuEnrollId As String
        Dim strid As String
        Dim stristestmentorproctored As String
        With tbl
            .Columns.Add("Id", GetType(Guid))
            .Columns.Add("StuEnrollId", GetType(Guid))
            .Columns.Add("TermId", GetType(Guid))
            .Columns.Add("GrdComponentTypeId", GetType(Guid))
            .Columns.Add("Accuracy", GetType(Decimal))
            .Columns.Add("Speed", GetType(Decimal))
            .Columns.Add("GrdSysDetailId", GetType(Guid))
            .Columns.Add("InstructorId", GetType(Guid))
            .Columns.Add("ModUser", GetType(String))
            .Columns.Add("istestmentorproctored", GetType(String))
        End With

        ' use the value specified in the uniquename property to find the inserted item collection
        strid = Guid.NewGuid.ToString
        strStuEnrollId = ddlPrgVerId.SelectedValue
        strTermId = TryCast(insertedItem("TermId").Controls(1), DropDownList).SelectedValue  'TryCast(insertedItem("TermId").Controls(1), DropDownList).SelectedValue
        dtDatePassed = Convert.ToDateTime(TryCast(insertedItem("DatePassed").Controls(1), RadDatePicker).DbSelectedDate) 'Convert.ToDateTime(TryCast(insertedItem("DatePassed").Controls(1), TextBox).Text) '
        strTestType = TryCast(insertedItem("GrdComponentTypeId").Controls(1), DropDownList).SelectedValue

        Try
            decAccuracy = TryCast(insertedItem("Accuracy").Controls(1), TextBox).Text
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            decAccuracy = 0
        End Try
        Try
            decSpeed = TryCast(insertedItem("Speed").Controls(1), TextBox).Text
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            decSpeed = 0
        End Try


        strGrdSysDetailId = TryCast(insertedItem("Grade").Controls(1), DropDownList).SelectedValue
        strInstructorId = TryCast(insertedItem("FullName").Controls(1), DropDownList).SelectedValue
        stristestmentorproctored = TryCast(insertedItem("istestmentorproctored").Controls(1), DropDownList).SelectedValue

        Dim strErrorMsg As String = ""

        If strTermId = "" AndAlso decSpeed = 0 AndAlso strTestType = "" Then
            Dim scriptstring As String = "radalert('Term,  Test Type and Speed are required', 350, 100);"
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
            Exit Sub
        ElseIf strTermId = "" AndAlso decSpeed = 0 AndAlso strTestType <> "" Then
            Dim scriptstring As String = "radalert('Term and Speed are required', 350, 100);"
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
            Exit Sub
        ElseIf strTermId = "" AndAlso decSpeed > 0 AndAlso strTestType = "" Then
            Dim scriptstring As String = "radalert('Term and Test Type are required', 350, 100);"
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
            Exit Sub
        ElseIf strTermId <> "" AndAlso decSpeed = 0 AndAlso strTestType = "" Then
            Dim scriptstring As String = "radalert('Speed and Test Type are required', 350, 100);"
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
            Exit Sub
        ElseIf strTermId <> "" AndAlso decSpeed = 0 AndAlso strTestType <> "" Then
            Dim scriptstring As String = "radalert('Speed is  required', 350, 100);"
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
            Exit Sub

        End If

        'If strTermId.Trim = "" Then
        '    strErrorMsg = "Term is required" & vbCrLf
        'End If
        'If dtDatePassed = #1/1/1900# Then
        '    strErrorMsg &= "Date Passed is required" & "<br>" & vbCrLf
        'End If
        'If decSpeed = 0 Then
        '    strErrorMsg &= "Speed is required" & "<br>" & vbCrLf
        'End If
        'If strTestType.Trim = "" Then
        '    strErrorMsg &= "Test Type is required" & "<br>" & vbCrLf
        'End If
        'If Not strErrorMsg.Trim = "" Then
        '    Dim scriptstring As String = "radalert('" & strErrorMsg & "', 350, 100);"
        '    ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
        '    Exit Sub
        'End If

        Try
            If dtDatePassed = #1/1/1900# Then
                DisplayErrorMessage("Date Passed is required")
                Exit Sub
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        If strInstructorId = "" Then
            Dim scriptstring As String = "radalert('Instructor is  required', 350, 100);"
            ScriptManager.RegisterStartupScript(Me, Me.[GetType](), "radalert", scriptstring, True)
            Exit Sub
        End If

        tbl.LoadDataRow(New Object() {strid, strStuEnrollId, strTermId, strTestType, decAccuracy, decSpeed, strGrdSysDetailId, strInstructorId, strModUser, stristestmentorproctored}, False)
        Dim dsScores As New DataSet
        dsScores.Tables.Add(tbl)

        If Not dsScores Is Nothing Then
            For Each lcol As DataColumn In tbl.Columns
                lcol.ColumnMapping = System.Data.MappingType.Attribute
            Next
            Dim strXML As String = dsScores.GetXml
            Dim grdPostFacade As New GrdPostingsFacade
            grdPostFacade.PostScoresForDictationSpeedTest(dtDatePassed, strXML)
            e.Canceled = True
            'BuildRadGrid()
            e.Canceled = True
            RadGrdPostScores.MasterTableView.IsItemInserted = False
            RadGrdPostScores.Rebind()
        End If
    End Sub
    Protected Sub radGrdPostScores_ItemCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdPostScores.ItemCommand
        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked
            e.Canceled = True

            Dim newValues As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary()
            Dim gridEditFormItem As GridCommandItem = DirectCast(e.Item, GridCommandItem)
            BuildDataTables(ddlPrgVerId.SelectedValue)

            'Dim picker As RadDatePicker = CType(gridEditFormItem.FindControl("picker1"), RadDatePicker)
            'picker.SharedCalendar = sharedCalendar


            newValues("TermId") = ""
            newValues("DatePassed") = ""
            newValues("GrdComponentTypeId") = ""
            newValues("Accuracy") = ""
            newValues("Speed") = ""
            newValues("Grade") = ""
            newValues("UserName") = ""
            newValues("istestmentorproctored") = "No"

            e.Item.OwnerTableView.InsertItem(newValues)
        End If
    End Sub
    Private Sub BuildDDLsInsideGrid(ByVal gridEditFormItem As GridDataItem)

        'Call this procedure and get the datatables stored in cache
        BuildDataTables(ddlPrgVerId.SelectedValue)

        ddlTerm = DirectCast(gridEditFormItem("TermId").FindControl("ddlTermId"), DropDownList)
        ddlTerm.DataSource = CType(Cache("dtTerm"), DataTable)
        ddlTerm.DataTextField = "TermDescrip"
        ddlTerm.DataValueField = "TermId"
        ddlTerm.DataBind()
        ddlTerm.Items.Insert(0, New ListItem("Select", ""))

        ddlGrdComponentTypeId = DirectCast(gridEditFormItem("GrdComponentTypeId").FindControl("ddlGrdComponentTypeId"), DropDownList)
        ddlGrdComponentTypeId.DataSource = CType(Cache("dtGrdCompTypes"), DataTable)
        ddlGrdComponentTypeId.DataTextField = "Descrip"
        ddlGrdComponentTypeId.DataValueField = "GrdComponentTypeId"
        ddlGrdComponentTypeId.DataBind()
        ddlGrdComponentTypeId.Items.Insert(0, New ListItem("Select", ""))

        ddlGrade = DirectCast(gridEditFormItem("Grade").FindControl("ddlGrdSystemId"), DropDownList)
        ddlGrade.DataSource = CType(Cache("dtGrades"), DataTable)
        ddlGrade.DataTextField = "Grade"
        ddlGrade.DataValueField = "GrdSysDetailId"
        ddlGrade.DataBind()
        ddlGrade.Items.Insert(0, New ListItem("Select", ""))

        ddlInstructors = DirectCast(gridEditFormItem("FullName").FindControl("ddlInstructorId"), DropDownList)
        ddlInstructors.DataSource = CType(Cache("dtInstructors"), DataTable)
        ddlInstructors.DataTextField = "fullname"
        ddlInstructors.DataValueField = "UserId"
        ddlInstructors.DataBind()
        ddlInstructors.Items.Insert(0, New ListItem("Select", ""))


        ddlEditMentorProctored = DirectCast(gridEditFormItem("istestmentorproctored").FindControl("ddlEditMentorProctored"), DropDownList)
        ddlEditMentorProctored.Items.Insert(0, New ListItem("No", "No"))
        ddlEditMentorProctored.Items.Insert(1, New ListItem("Yes", "Yes"))

        'ddlTermCombo = DirectCast(gridEditFormItem("TermId").FindControl("RadComboBox2"), RadComboBox)
        'ddlTermCombo.DataSource = CType(Cache("dtTerm"), DataTable)
        'ddlTermCombo.DataTextField = "TermDescrip"
        'ddlTermCombo.DataValueField = "TermId"
        'ddlTermCombo.DataBind()
        'ddlTermCombo.Items.Insert(0, New RadComboBoxItem("Select", ""))

    End Sub
    'Private Sub BuildDDLsInsideGridForNewRecords(ByVal gridEditFormItem As GridItem)

    '    'Call this procedure and get the datatables stored in cache
    '    BuildDataTables(ddlPrgVerId.SelectedValue)

    '    ddlTerm = DirectCast(gridEditFormItem.FindControl("ddlTermId"), DropDownList)
    '    ddlTerm.DataSource = dtTerm
    '    ddlTerm.DataTextField = "TermDescrip"
    '    ddlTerm.DataValueField = "TermId"
    '    ddlTerm.DataBind()
    '    ddlTerm.Items.Insert(0, New ListItem("Select", ""))

    '    ddlGrdComponentTypeId = DirectCast(gridEditFormItem.FindControl("ddlGrdComponentTypeId"), DropDownList)
    '    ddlGrdComponentTypeId.DataSource = dtGrdCompTypes
    '    ddlGrdComponentTypeId.DataTextField = "Descrip"
    '    ddlGrdComponentTypeId.DataValueField = "GrdComponentTypeId"
    '    ddlGrdComponentTypeId.DataBind()
    '    ddlGrdComponentTypeId.Items.Insert(0, New ListItem("Select", ""))

    '    ddlGrade = DirectCast(gridEditFormItem.FindControl("ddlGrdSystemId"), DropDownList)
    '    ddlGrade.DataSource = dtGrades
    '    ddlGrade.DataTextField = "Grade"
    '    ddlGrade.DataValueField = "GrdSysDetailId"
    '    ddlGrade.DataBind()
    '    ddlGrade.Items.Insert(0, New ListItem("Select", ""))

    '    ddlInstructors = DirectCast(gridEditFormItem.FindControl("ddlInstructorId"), DropDownList)
    '    ddlInstructors.DataSource = dtInstructors
    '    ddlInstructors.DataTextField = "fullname"
    '    ddlInstructors.DataValueField = "UserId"
    '    ddlInstructors.DataBind()
    '    ddlInstructors.Items.Insert(0, New ListItem("Select", ""))
    'End Sub
    Private Sub ApplyPermissionToRadGrid()
        If pObj.HasNone Or pObj.HasDisplay = True Then
            RadGrdPostScores.AllowAutomaticInserts = False
            RadGrdPostScores.AllowAutomaticUpdates = False
            RadGrdPostScores.AllowAutomaticDeletes = False
        End If

        If pObj.HasEdit = True Then
            RadGrdPostScores.AllowAutomaticUpdates = True
        ElseIf pObj.HasEdit = False Then
            RadGrdPostScores.AllowAutomaticUpdates = False
        End If
        If pObj.HasDelete = True Then
            RadGrdPostScores.AllowAutomaticDeletes = True
        ElseIf pObj.HasDelete = False Then
            RadGrdPostScores.AllowAutomaticDeletes = False
        End If


    End Sub

    Protected Sub RadGrdPostScores_ItemCreated(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrdPostScores.ItemCreated
        'If pObj.HasEdit = False Then
        '    Dim cell As TableCell = CType(e.Item, GridCommandItem).Cells(0)
        '    Dim lnkButton1 As ImageButton = CType(cell.Controls(0).FindControl("EditCommandColumn"), ImageButton)
        '    lnkButton1.Visible = False
        'End If
    End Sub

    Protected Sub radGrdPostScores_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrdPostScores.ItemDataBound
        Try
            If pObj.HasAdd = True Then
                RadGrdPostScores.AllowAutomaticInserts = True
            ElseIf pObj.HasAdd = False Then
                If TypeOf e.Item Is GridCommandItem Then
                    Dim cmditm As GridCommandItem = DirectCast(e.Item, GridCommandItem)
                    Dim btn1 As Button = DirectCast(cmditm.FindControl("AddNewRecordButton"), Button)
                    btn1.Enabled = False
                    btn1.ToolTip = "User does not have permission to post new score"
                    Dim lnkbtn1 As LinkButton = DirectCast(cmditm.FindControl("InitInsertButton"), LinkButton)
                    lnkbtn1.Enabled = False
                    lnkbtn1.ToolTip = "User does not have permission to post new score"
                End If
            End If


            If pObj.HasEdit = True Then
                RadGrdPostScores.AllowAutomaticUpdates = True
            ElseIf pObj.HasEdit = False Then
                If TypeOf e.Item Is GridDataItem And e.Item.IsInEditMode Then
                    Dim cell As TableCell = CType(e.Item, GridDataItem)("EditCommandColumn")
                    Dim lnk As ImageButton = CType(cell.Controls(0), ImageButton)
                    lnk.Visible = False
                    cell.ToolTip = "User does not have the permission to edit the posted score"
                End If
            End If


            If (TypeOf e.Item Is GridDataItem) AndAlso e.Item.IsInEditMode Then

                Dim gridEditFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
                BuildDDLsInsideGrid(gridEditFormItem)

                If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                    'Item Array 2 - TermId
                    ddlTerm.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(1).ToString()
                End If

                If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                    'Item Array 6 - GrdComponentTypeId
                    ddlGrdComponentTypeId.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(6).ToString()
                End If

                If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                    'Item Array 10 - GrdSysDetailId
                    ddlGrade.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(10).ToString()
                End If

                If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                    'Item Array 12 - InstructorId
                    ddlInstructors.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(12).ToString()
                    'Dim radDatePassed As string = DirectCast(gridEditFormItem("DatePassed").FindControl("txtDatePassed"), TextBox).Text 'DirectCast(gridEditFormItem("DatePassed").FindControl("txtDatePassed"), RadDatePicker)
                    'radDatePassed.DbSelectedDate = Convert.ToDateTime(DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(5).ToString()).ToShortDateString
                End If

                If Not TypeOf gridEditFormItem.DataItem Is GridInsertionObject Then
                    'Item Array 12 - InstructorId
                    ddlEditMentorProctored.SelectedValue = DirectCast((gridEditFormItem.DataItem), System.Data.DataRowView).Row.ItemArray(13).ToString()
                End If
            End If


            If (TypeOf e.Item Is GridCommandItem) Then
                Dim cmditm As GridCommandItem = DirectCast(e.Item, GridCommandItem)
                cmditm.FindControl("RefreshButton").Visible = False
                cmditm.FindControl("RebindGridButton").Visible = False
                'to hide AddNewRecord button
                'cmditm.FindControl("InitInsertButton").Visible = false;//hide the text
                'cmditm.FindControl("AddNewRecordButton").Visible = false;//hide the image
            End If

            'If (TypeOf e.Item Is GridEditableItem AndAlso e.Item.IsInEditMode) Then
            '    Dim editItem As GridEditableItem = CType(e.Item, GridEditableItem)
            '    Dim picker As RadDatePicker = CType(editItem.FindControl("picker1"), RadDatePicker)
            '    picker.SharedCalendar = sharedCalendar
            'End If
        Finally

        End Try

    End Sub
    Protected Sub radGrdPostScores_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrdPostScores.NeedDataSource
        Dim dt As New DataTable
        RadGrdPostScores.DataSource = (New GrdPostingsFacade).GetDictationScorePostedByStudentEnrollment(ddlPrgVerId.SelectedValue)
    End Sub

    Protected Sub RadGrdPostScores_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles RadGrdPostScores.Unload

    End Sub
    Protected Sub radGrdPostScores_UpdateCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdPostScores.UpdateCommand
        Dim editedItem As GridEditableItem = DirectCast(e.Item, GridEditableItem)


        Dim tbl As New DataTable("PostScores")
        Dim strTermId As String = ""
        Dim dtDatePassed As Date = #1/1/1900#
        Dim strTestType As String = ""
        Dim decAccuracy As Decimal = 0
        Dim decSpeed As Decimal = 0
        Dim strGrdSysDetailId As String = ""
        Dim strInstructorId As String = ""
        Dim strModDate As DateTime = Date.Now
        Dim intSelectedCount As Integer = 0
        Dim strModUser As String = Session("User")
        Dim strStuEnrollId As String
        Dim strid As String
        Dim stristestmentorproctored As String
        With tbl
            .Columns.Add("Id", GetType(Guid))
            .Columns.Add("StuEnrollId", GetType(Guid))
            .Columns.Add("TermId", GetType(Guid))
            .Columns.Add("GrdComponentTypeId", GetType(Guid))
            .Columns.Add("Accuracy", GetType(Decimal))
            .Columns.Add("Speed", GetType(Decimal))
            .Columns.Add("GrdSysDetailId", GetType(Guid))
            .Columns.Add("InstructorId", GetType(Guid))
            .Columns.Add("ModUser", GetType(String))
            .Columns.Add("istestmentorproctored", GetType(String))
        End With

        ' use the value specified in the uniquename property to find the inserted item collection
        strid = editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("id").ToString()
        strStuEnrollId = ddlPrgVerId.SelectedValue
        strTermId = TryCast(editedItem("TermId").Controls(1), DropDownList).SelectedValue

        'Dim picker As RadDatePicker = CType(editedItem("DatePassed").Controls(0), RadDatePicker)
        'Dim newDate As Object = picker.DbSelectedDate
        'editedItem.OwnerTableView.DataKeyValues(editedItem.ItemIndex)("DatePassed") = IIf(newDate Is Nothing, DBNull.Value, newDate)
        dtDatePassed = Convert.ToDateTime(TryCast(editedItem("DatePassed").Controls(1), RadDatePicker).DbSelectedDate) 'Convert.ToDateTime(TryCast(editedItem("txtDatePassed").Controls(1), TextBox).Text) 
        strTestType = TryCast(editedItem("GrdComponentTypeId").Controls(1), DropDownList).SelectedValue
        decAccuracy = TryCast(editedItem("Accuracy").Controls(1), TextBox).Text
        decSpeed = TryCast(editedItem("Speed").Controls(1), TextBox).Text
        strGrdSysDetailId = TryCast(editedItem("Grade").Controls(1), DropDownList).SelectedValue
        strInstructorId = TryCast(editedItem("FullName").Controls(1), DropDownList).SelectedValue
        stristestmentorproctored = TryCast(editedItem("istestmentorproctored").Controls(1), DropDownList).SelectedValue
        tbl.LoadDataRow(New Object() {strid, strStuEnrollId, strTermId, strTestType, decAccuracy, decSpeed, strGrdSysDetailId, strInstructorId, strModUser, stristestmentorproctored}, False)
        Dim dsScores As New DataSet
        dsScores.Tables.Add(tbl)

        If Not dsScores Is Nothing Then
            For Each lcol As DataColumn In tbl.Columns
                lcol.ColumnMapping = System.Data.MappingType.Attribute
            Next
            Dim strXML As String = dsScores.GetXml
            Dim grdPostFacade As New GrdPostingsFacade
            grdPostFacade.UpdatePostedScoresForDictationSpeedTest(dtDatePassed, strXML)
            e.Canceled = True
            RadGrdPostScores.MasterTableView.ClearEditItems()
            RadGrdPostScores.Rebind()
        End If
    End Sub
    Public Function TrueOrFalse(ByVal mentorvalue As Integer) As Boolean
        Try
            Select Case mentorvalue
                Case Is = 0
                    Return False
                Case Else
                    Return True
            End Select
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function
    Public Function YesorNo(ByVal mentorvalue As Integer) As String
        Select Case mentorvalue
            Case Is = 0
                Return "No"
            Case Else
                Return "Yes"
        End Select
    End Function
End Class