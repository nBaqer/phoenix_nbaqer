<%@ Page Title="Post Attendance" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false"
    CodeFile="PostClockAttendance.aspx.vb" Inherits="FA_PostClockAttendance" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>Post Attendance</title>
    <style type="text/css">
        /* Container Styles Start */
        .TopCalendarHeader {
            width: 100%;
            background-color: #f2f2f2;
            border: 1px solid #e0e0e0;
            margin: 10px 0;
            text-align: center;
            line-height: 22px;
            padding-top: 4px;
        }

        .HoursTextBox {
            font: normal 9px verdana;
            color: #333333;
            width: 20px;
            text-align: left;
            /*border: 0px solid #e0e0e0; */
            border: 0px none !important;
            padding: 0px 0px;
        }

        .MaxHrsText {
            font: normal 9px verdana;
            color: #333333;
            width: 0px;
            text-align: left;
            border: 0px solid #e0e0e0;
            padding: 0px 0px;
        }

        .ListFrameClockFilter {
            border-right: #e0e0e0 1px solid;
            border-top: #e0e0e0 1px solid;
            border-bottom: #e0e0e0 1px solid;
            vertical-align: top;
            width: 35%;
            background-color: #f2f2f2;
        }

        .scrollright {
            /* top; right; bottom; left */
            margin: 5px 50px 50px 1px;
            padding: 16px;
            overflow: auto;
        }

        .RptAttendance {
            text-align: left;
            height: expression(document.body.clientHeight - 340 + "px");
            overflow: auto;
        }

        .RptAttHeader {
            /*width: 7%;*/
            font: bold 11px verdana;
            color: #2962ff;
            background-color: #f2f2f2;
            padding: 3px 3px 3px 6px;
            border-right: 1px solid #e0e0e0;
            vertical-align: middle;
            text-align: center;
        }

        .RptAttHeaderName {
            /*width: 31%;*/
            font: bold 11px verdana;
            color: #2962ff;
            background-color: #f2f2f2;
            padding: 3px 6px 6px 6px;
            border-right: 1px solid #e0e0e0;
            vertical-align: bottom;
            text-align: left;
        }

        .RptAttHeaderTotal {
            width: 10%;
            font: bold 11px verdana;
            color: #2962ff;
            background-color: #f2f2f2;
            padding: 6px 6px 2px 6px;
            border-right: 1px solid #e0e0e0;
            vertical-align: middle;
            text-align: center;
        }

        .RptAttItem {
            width: 30px;
            /*width: 7%;*/
            font: normal 11px verdana;
            color: #333333;
            text-align: center;
            border-right: 1px solid #e0e0e0;
            border-bottom: 1px solid #e0e0e0;
            padding: 6px 6px;
        }

        .RptAttItemHeader {
            /*width: 7%;*/
            font: bold 11px verdana;
            color: #333333;
            background-color: #f2f2f2;
            padding: 3px 3px 3px 6px;
            text-align: center;
            border-right: 1px solid #e0e0e0;
            border-bottom: 1px solid #e0e0e0;
        }

        .RptAttItemName {
            /*width: 25%;*/
            font: normal 11px verdana;
            color: #333333;
            text-align: left;
            border-right: 1px solid #e0e0e0;
            border-bottom: 1px solid #e0e0e0;
            padding: 6px 6px;
        }

        .RptAttItemTotal {
            width: 10%;
            font: normal 11px verdana;
            color: #333333;
            text-align: center;
            border-right: 1px solid #e0e0e0;
            border-bottom: 1px solid #e0e0e0;
            padding: 6px 6px;
        }

        .LabelBoldcalendar {
            font: bold 14px verdana;
            color: #2962ff;
            vertical-align: middle;
        }

        .DateSelector {
            font: normal 10px verdana;
            color: #2962ff;
            vertical-align: middle;
            padding: 3px;
        }

        .FilterGroup {
            border: #666 1px solid;
            width: 100%;
            padding-left: 4px;
            padding-bottom: 4px;
            padding-right: 4px;
            padding-top: 4px;
            background-color: #ffff99;
            text-align: center;
        }

        .LegendLabel {
            font: normal 10px verdana;
            background-color: transparent;
        }

        #dhtmltooltip {
            font: normal 11px verdana;
            color: #2962ff;
            position: absolute;
            left: 0;
            width: 125px;
            border: 1px solid black;
            padding: 2px;
            background-color: lightyellow;
            visibility: hidden;
            z-index: 100;
            /*Remove below line to remove shadow. Below line should always appear last within this CSS*/
            filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135);
        }

        #dhtmlpointer {
            position: absolute;
            left: -300px;
            z-index: 101;
            visibility: hidden;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelContent" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%"
        Orientation="Vertical" VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="375" Scrolling="Y">

            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <!-- begin leftcolumn -->
                <tr>
                    <td>
                        <table width="330px" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <div class="scrollleft" style="height: expression(document.body.clientHeight - 171 + 'px')">
                                        <!-- Program Filter -->


                                        <table width="100%" cellpadding="2" cellspacing="0">
                                            <tr>
                                                <td colspan="2">
                                                    <span class="labelbold">Program Filter</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblProgram"
                                                        CssClass="label" runat="server" Text="Program" />
                                                <td class="twocolumncontentcell">
                                                    <telerik:RadComboBox ID="ddlProgram" runat="server" DataTextField="Descrip" DataValueField="ID" ClientIDMode="Static"
                                                        AutoPostBack="True" AppendDataBoundItems="true" CausesValidation="False" Filter="Contains" EmptyMessage="Select" Width="200px">
                                                    </telerik:RadComboBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblProgramVer" CssClass="label" runat="server" Text="Program Version" />
                                                <td class="twocolumncontentcell">
                                                    <telerik:RadComboBox ID="ddlProgramVersion" runat="server" DataTextField="PrgVerShiftDescrip" DataValueField="ID" ClientIDMode="Static"
                                                        AutoPostBack="True" AppendDataBoundItems="true" CausesValidation="False" Filter="Contains" EmptyMessage="Select" Width="200px">
                                                    </telerik:RadComboBox>
                                            </tr>
                                            <tr visible="false">
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblUseTimeClock" Visible="false" CssClass="label" runat="server" Text="Uses Timeclock?" /></td>
                                                <td class="twocolumncontentcell">
                                                    <asp:DropDownList ID="ddlUseTimeClock" Visible="false" runat="server" CssClass="dropdownlist" ClientIDMode="Static" Width="200px" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <span class="labelbold">Student Filter</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblStudentName" CssClass="label" runat="server" Text="Student Name" /></td>
                                                <td class="twocolumncontentcell">
                                                    <asp:TextBox ID="txtStudentName" CssClass="TextBox" runat="server" ClientIDMode="Static" /></td>
                                            </tr>

                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblStudentGrpId" CssClass="label" runat="server" Text="Student Group" /></td>
                                                <td class="twocolumncontentcell">
                                                    <telerik:RadComboBox ID="ddlStudentGrpId" runat="server" DataTextField="Descrip" DataValueField="LeadGrpId" ClientIDMode="Static"
                                                        AutoPostBack="True" AppendDataBoundItems="true" CausesValidation="False" Filter="Contains" EmptyMessage="Select" Width="200px">
                                                        <ItemTemplate>
                                                            <asp:Label runat="server" ID="Label1" Text='<%# Eval("Descrip")%>'></asp:Label>
                                                            <telerik:RadToolTip ID="RadToolTip1" runat="server" TargetControlID="Label1" Position="BottomRight" Text='<%# Eval("Descrip")%>'>
                                                            </telerik:RadToolTip>


                                                        </ItemTemplate>


                                                    </telerik:RadComboBox>
                                                </td>
                                            </tr>
                                            <tr id="trCohortStartDate" runat="server">
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="Label2" runat="server" CssClass="label">Cohort Start Date</asp:Label></td>
                                                <td class="twocolumncontentcell">
                                                    <asp:DropDownList ID="ddlcohortStDate" runat="server" CssClass="dropdownlist" ClientIDMode="Static" Width="200px">
                                                    </asp:DropDownList></td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblBadgeNum" CssClass="label" runat="server" Text="Badge Number" /></td>
                                                <td class="twocolumncontentcell">
                                                    <asp:TextBox ID="txtBadgeNum" CssClass="TextBox" runat="server" ClientIDMode="Static" /></td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="lblStudentStatus" CssClass="label" runat="server"
                                                        Text="Enrollment Status" Visible="false" /></td>
                                                <td class="twocolumncontentcell">
                                                    <asp:DropDownList ID="ddlStudentStatus" runat="server" CssClass="dropdownlist" Visible="false" Width="200px" /></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <span class="labelbold">Date Filter</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="Label3" CssClass="label" runat="server" Text="Start" /></td>
                                                <td class="twocolumncontentcell" style="text-align: left">

                                                    <telerik:RadDatePicker ID="txtStart" MinDate="1/1/1945" runat="server" Width="200px"></telerik:RadDatePicker>


                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="twocolumnlabelcell">
                                                    <asp:Label ID="Label1" runat="server" CssClass="label" Text="End" /></td>
                                                <td class="twocolumncontentcell" style="text-align: left">

                                                    <telerik:RadDatePicker ID="txtEnd" MinDate="1/1/1945" runat="server" Width="200px"></telerik:RadDatePicker>


                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <br />

                                                    <span class="labelbold" style="height: 20px; vertical-align: middle; text-align: center">Enrollment Status Filter</span>

                                                    <!-- Enrollment Status Filter -->
                                                    <br />


                                                    <asp:Panel ID="pnlEnrollmentStatusFilter" runat="server" ScrollBars="Auto">
                                                        <table width="100%" cellpadding="2">
                                                            <tr>
                                                                <td class="twocolumnlabelcell">
                                                                    <asp:CheckBox ID="chkAllStudentsInSchoolStatus" runat="server" Text="In-School status" CssClass="checkbox" AutoPostBack="true" ClientIDMode="Static" />
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td width="5%"></td>
                                                                            <td width="95%">
                                                                                <asp:CheckBoxList ID="chkInSchoolStatus" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="CheckBox" ClientIDMode="Static">
                                                                                </asp:CheckBoxList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="twocolumnlabelcell">
                                                                    <asp:CheckBox ID="chkAllOutofSchoolStatus" runat="server" Text="Out-of-School status" CssClass="checkbox" AutoPostBack="true" ClientIDMode="Static" />
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td width="5%"></td>
                                                                            <td width="95%">
                                                                                <asp:CheckBoxList ID="chkStudentOutOfSchoolStatus" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="CheckBox" ClientIDMode="Static">
                                                                                </asp:CheckBoxList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>

                                                    <br />
                                                    <div style="text-align: center; width: 100%">
                                                        <asp:Button ID="btnBuildList" runat="server" Text="Build List" />
                                                    </div>

                                                    <input type="hidden" name="filterData" id="filterData" runat="server" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>




                    </td>
                    <!-- end leftcolumn -->
                    <!-- begin rightcolumn -->

                </tr>

            </table>
        </telerik:RadPane>

        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both"
            Orientation="HorizontalTop">

            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False" />
                        <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                            Enabled="False" />
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
                            Enabled="False" /></td>
                </tr>
            </table>
            <!-- end top menu (save,new,reset,delete,history)-->
            <!--begin right column-->
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <asp:Panel ID="pnlRHS" runat="server" Width="100%" />
                        </div>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
        </telerik:RadPane>
    </telerik:RadSplitter>
</asp:Content>

