' ===============================================================================
' ClockAttendanceDetails.aspx
' Allows the user to view and edit details for clock hour attendance for a specific week
' ===============================================================================
' Copyright (C) 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
Imports System.Data
Imports Fame.AdvantageV1.BusinessFacade
Imports Fame.AdvantageV1.Common
Imports Fame.AdvantageV1.BusinessFacade.AR
Imports Fame.AdvantageV1.Common.AR
Imports Fame.Advantage.Common

Partial Class ClockAttendanceDetails
    Inherits System.Web.UI.Page
    Protected strUnitTypeDescrip As String = ""
    Dim day0, day1, day2, day3, day4, day5, day6 As String
    'Dim stuEnrollId, source As String
    Dim boolScheduleChange As Boolean = False
    Dim mh0, mh1, mh2, mh3, mh4, mh5, mh6 As Decimal
    Dim ah0, ah1, ah2, ah3, ah4, ah5, ah6 As Decimal
    Dim sh0, sh1, sh2, sh3, sh4, sh5, sh6 As Decimal
   ' Dim ab0, ab1, ab2, ab3, ab4, ab5, ab6 As Decimal
    'Dim t0, t1, t2, t3, t4, t5, t6 As Boolean

    Dim dsPunch As New DataSet
    Dim CampusId As String
    Dim UserId As String
    Private pObj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        strUnitTypeDescrip = Request.Params("unittypedescrip")
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If hdnfromtimeclockpunch.Value = "true" Then
            hdnfromtimeclockpunch.Value = "false"
            Populatehrswhendatamodified()

        End If

        UserId = Request.Params("UserId").ToString()
        CampusId = Request.Params("CampusId").ToString()
        Dim fac As New FAME.AdvantageV1.BusinessFacade.TimeClock.TimeClockFacade
        Dim Resource As String
        Resource = "Edit TimeClock"
        If Not CampusId Is Nothing Then
            If CampusId = "" Then
                CampusId = Nothing
            End If
        End If
        pObj = fac.GetImportFilePermissions(UserId, Resource, CampusId)

        'If pObj.HasFull = True Then
        '    ' lbTimeClockpunches.Visible = True
        '    btntimeclock.Visible = True
        'Else
        '    ' lbTimeClockpunches.Visible = False
        '    btntimeclock.Visible = False
        'End If

        If Not Page.IsPostBack Then

            boolScheduleChange = False
            ' add javascript to the cancel button
            btnCancel.Attributes.Add("onclick", "window.returnValue=''; window.close();")

            'Me.txtDate.Text = Request.Params("date")
            Me.txtStudent.Text = Request.Params("sname").Replace("%26apos;", "'")
            Me.txtStudentStartDate.Text = CType(Request.Params("studentstartdate"), Date).ToShortDateString

            If strUnitTypeDescrip.ToLower = "present" Then
                Me.ddlSHD0.SelectedValue = Request.Params("shrs0").ToString()
                Me.ddlSHD1.SelectedValue = Request.Params("shrs1").ToString()
                Me.ddlSHD2.SelectedValue = Request.Params("shrs2").ToString()
                Me.ddlSHD3.SelectedValue = Request.Params("shrs3").ToString()
                Me.ddlSHD4.SelectedValue = Request.Params("shrs4").ToString()
                Me.ddlSHD5.SelectedValue = Request.Params("shrs5").ToString()
                Me.ddlSHD6.SelectedValue = Request.Params("shrs6").ToString()

                Me.ddlAHD0.SelectedValue = GetActualDaysValue(Request.Params("ahrs0").ToString())
                Me.ddlAHD1.SelectedValue = GetActualDaysValue(Request.Params("ahrs1").ToString())
                Me.ddlAHD2.SelectedValue = GetActualDaysValue(Request.Params("ahrs2").ToString())
                Me.ddlAHD3.SelectedValue = GetActualDaysValue(Request.Params("ahrs3").ToString())
                Me.ddlAHD4.SelectedValue = GetActualDaysValue(Request.Params("ahrs4").ToString())
                Me.ddlAHD5.SelectedValue = GetActualDaysValue(Request.Params("ahrs5").ToString())
                Me.ddlAHD6.SelectedValue = GetActualDaysValue(Request.Params("ahrs6").ToString())

                Me.chkT0.Checked = GetCheckedValueById(Request.Params("at0").ToString())
                Me.chkT1.Checked = GetCheckedValueById(Request.Params("at1").ToString())
                Me.chkT2.Checked = GetCheckedValueById(Request.Params("at2").ToString())
                Me.chkT3.Checked = GetCheckedValueById(Request.Params("at3").ToString())
                Me.chkT4.Checked = GetCheckedValueById(Request.Params("at4").ToString())
                Me.chkT5.Checked = GetCheckedValueById(Request.Params("at5").ToString())
                Me.chkT6.Checked = GetCheckedValueById(Request.Params("at6").ToString())

                day0 = Request.Params("day0")
                day1 = Request.Params("day1")
                day2 = Request.Params("day2")
                day3 = Request.Params("day3")
                day4 = Request.Params("day4")
                day5 = Request.Params("day5")
                day6 = Request.Params("day6")

                Me.txtD00.Text = Request.Params("day0")
                Me.txtD1.Text = Request.Params("day1")
                Me.txtD2.Text = Request.Params("day2")
                Me.txtD3.Text = Request.Params("day3")
                Me.txtD4.Text = Request.Params("day4")
                Me.txtD5.Text = Request.Params("day5")
                Me.txtD6.Text = Request.Params("day6")

                txtStuEnrollId.Text = Request.Params("stuenrollid")
                txtSource.Text = Request.Params("source")
            Else
                Me.txtSHD0.Text = Request.Params("shrs0").ToString()
                Me.txtSHD1.Text = Request.Params("shrs1").ToString()
                Me.txtSHD2.Text = Request.Params("shrs2").ToString()
                Me.txtSHD3.Text = Request.Params("shrs3").ToString()
                Me.txtSHD4.Text = Request.Params("shrs4").ToString()
                Me.txtSHD5.Text = Request.Params("shrs5").ToString()
                Me.txtSHD6.Text = Request.Params("shrs6").ToString()

                Me.txtAHD0.Text = Request.Params("ahrs0").ToString()
                Me.txtAHD1.Text = Request.Params("ahrs1").ToString()
                Me.txtAHD2.Text = Request.Params("ahrs2").ToString()
                Me.txtAHD3.Text = Request.Params("ahrs3").ToString()
                Me.txtAHD4.Text = Request.Params("ahrs4").ToString()
                Me.txtAHD5.Text = Request.Params("ahrs5").ToString()
                Me.txtAHD6.Text = Request.Params("ahrs6").ToString()

                Me.chkT0.Checked = GetCheckedValueById(Request.Params("at0").ToString())
                Me.chkT1.Checked = GetCheckedValueById(Request.Params("at1").ToString())
                Me.chkT2.Checked = GetCheckedValueById(Request.Params("at2").ToString())
                Me.chkT3.Checked = GetCheckedValueById(Request.Params("at3").ToString())
                Me.chkT4.Checked = GetCheckedValueById(Request.Params("at4").ToString())
                Me.chkT5.Checked = GetCheckedValueById(Request.Params("at5").ToString())
                Me.chkT6.Checked = GetCheckedValueById(Request.Params("at6").ToString())


                If Not strUnitTypeDescrip.ToLower = "present" Then
                    ''Added by Saraswathi Lakshmanan
                    ''Makeup hrs is calculated from the actual and Scheduled hrs
                    'makeuphrs is (Actual-Scheduled)
                    If Request.Params("shrs0").ToString().Trim = "" Then
                        sh0 = 0
                    Else
                        sh0 = CDbl(Request.Params("shrs0").ToString())
                    End If
                    If Request.Params("shrs1").ToString().Trim = "" Then
                        sh1 = 0
                    Else
                        sh1 = CDbl(Request.Params("shrs1").ToString())
                    End If
                    If Request.Params("shrs2").ToString().Trim = "" Then
                        sh2 = 0
                    Else
                        sh2 = CDbl(Request.Params("shrs2").ToString())
                    End If
                    If Request.Params("shrs3").ToString().Trim = "" Then
                        sh3 = 0
                    Else
                        sh3 = CDbl(Request.Params("shrs3").ToString())
                    End If
                    If Request.Params("shrs4").ToString().Trim = "" Then
                        sh4 = 0
                    Else
                        sh4 = CDbl(Request.Params("shrs4").ToString())
                    End If
                    If Request.Params("shrs5").ToString().Trim = "" Then
                        sh5 = 0
                    Else
                        sh5 = CDbl(Request.Params("shrs5").ToString())
                    End If
                    If Request.Params("shrs6").ToString().Trim = "" Then
                        sh6 = 0
                    Else
                        sh6 = CDbl(Request.Params("shrs6").ToString())
                    End If

                    If Request.Params("ahrs0").ToString().Trim = "" Then
                        ah0 = 0
                    Else
                        ah0 = CDbl(Request.Params("ahrs0").ToString())
                    End If
                    If Request.Params("ahrs1").ToString().Trim = "" Then
                        ah1 = 0
                    Else
                        ah1 = CDbl(Request.Params("ahrs1").ToString())
                    End If
                    If Request.Params("ahrs2").ToString().Trim = "" Then
                        ah2 = 0
                    Else
                        ah2 = CDbl(Request.Params("ahrs2").ToString())
                    End If
                    If Request.Params("ahrs3").ToString().Trim = "" Then
                        ah3 = 0
                    Else
                        ah3 = CDbl(Request.Params("ahrs3").ToString())
                    End If
                    If Request.Params("ahrs4").ToString().Trim = "" Then
                        ah4 = 0
                    Else
                        ah4 = CDbl(Request.Params("ahrs4").ToString())
                    End If
                    If Request.Params("ahrs5").ToString().Trim = "" Then
                        ah5 = 0
                    Else
                        ah5 = CDbl(Request.Params("ahrs5").ToString())
                    End If
                    If Request.Params("ahrs6").ToString().Trim = "" Then
                        ah6 = 0
                    Else
                        ah6 = CDbl(Request.Params("ahrs6").ToString())
                    End If

                    If (ah0 - sh0) > 0 Then
                        mh0 = ah0 - sh0
                        Me.txtMUHD0.Text = mh0
                    Else
                        mh0 = 0
                    End If
                    If (ah1 - sh1) > 0 Then
                        mh1 = ah1 - sh1
                        Me.txtMUHD1.Text = mh1
                    Else
                        mh1 = 0
                    End If
                    If (ah2 - sh2) > 0 Then
                        mh2 = ah2 - sh2
                        Me.txtMUHD2.Text = mh2
                    Else
                        mh2 = 0
                    End If
                    If (ah3 - sh3) > 0 Then
                        mh3 = ah3 - sh3
                        Me.txtMUHD3.Text = mh3
                    Else
                        mh3 = 0
                    End If
                    If (ah4 - sh4) > 0 Then
                        mh4 = ah4 - sh4
                        Me.txtMUHD4.Text = mh4
                    Else
                        mh4 = 0
                    End If
                    If (ah5 - sh5) > 0 Then
                        mh5 = ah5 - sh5
                        Me.txtMUHD5.Text = mh5
                    Else
                        mh5 = 0
                    End If
                    If (ah6 - sh6) > 0 Then
                        mh6 = ah6 - sh6
                        Me.txtMUHD6.Text = mh6
                    Else
                        mh6 = 0
                    End If

                    Dim SchedHrs As New ArrayList
                    Dim ActualHrs As New ArrayList
                    With SchedHrs
                        .Add(Request.Params("shrs0").ToString())
                        .Add(Request.Params("shrs1").ToString())
                        .Add(Request.Params("shrs2").ToString())
                        .Add(Request.Params("shrs3").ToString())
                        .Add(Request.Params("shrs4").ToString())
                        .Add(Request.Params("shrs5").ToString())
                        .Add(Request.Params("shrs6").ToString())
                    End With
                    With ActualHrs
                        .Add(Request.Params("ahrs0").ToString())
                        .Add(Request.Params("ahrs1").ToString())
                        .Add(Request.Params("ahrs2").ToString())
                        .Add(Request.Params("ahrs3").ToString())
                        .Add(Request.Params("ahrs4").ToString())
                        .Add(Request.Params("ahrs5").ToString())
                        .Add(Request.Params("ahrs6").ToString())
                    End With
                    lblTardy.Visible = True
                    Dim i As Integer
                    Dim ctlId As String
                    Dim chk As CheckBox
                    For i = 0 To 6
                        ctlId = "chkT" + i.ToString()
                        chk = CType(FindControl(ctlId), CheckBox)
                        chk.Visible = True
                        chk.Enabled = False
                    Next i
                    ''Added on feb 11 2009
                    ''Set tardies
                    If Request.Params("at0").ToString().Trim = "" Then
                        chkT0.Checked = False
                    ElseIf Request.Params("at0").ToString().Trim = "True" Then
                        chkT0.Checked = True
                    Else
                        chkT0.Checked = False
                    End If
                    If Request.Params("at1").ToString().Trim = "" Then
                        chkT1.Checked = False
                    ElseIf Request.Params("at1").ToString().Trim = "True" Then
                        chkT1.Checked = True
                    Else
                        chkT1.Checked = False
                    End If
                    If Request.Params("at2").ToString().Trim = "" Then
                        chkT2.Checked = False
                    ElseIf Request.Params("at2").ToString().Trim = "True" Then
                        chkT2.Checked = True
                    Else
                        chkT2.Checked = False
                    End If
                    If Request.Params("at3").ToString().Trim = "" Then
                        chkT3.Checked = False
                    ElseIf Request.Params("at3").ToString().Trim = "True" Then
                        chkT3.Checked = True
                    Else
                        chkT3.Checked = False
                    End If
                    If Request.Params("at4").ToString().Trim = "" Then
                        chkT4.Checked = False
                    ElseIf Request.Params("at4").ToString().Trim = "True" Then
                        chkT4.Checked = True
                    Else
                        chkT4.Checked = False
                    End If
                    If Request.Params("at5").ToString().Trim = "" Then
                        chkT5.Checked = False
                    ElseIf Request.Params("at5").ToString().Trim = "True" Then
                        chkT5.Checked = True
                    Else
                        chkT5.Checked = False
                    End If
                    If Request.Params("at6").ToString().Trim = "" Then
                        chkT6.Checked = False
                    ElseIf Request.Params("at6").ToString().Trim = "True" Then
                        chkT6.Checked = True
                    Else
                        chkT6.Checked = False
                    End If

                    'MakeTardyControlVisible(SchedHrs, ActualHrs)
                End If
            End If
            ' determine if we are using a time clock
            Dim useTimeClock As Boolean = True
            Try
                useTimeClock = Request.Params("timeclock")
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            ''Added by Saraswathi 
            ''If use timeclock is true , then show add/edit timeclock punches 
            ''else hide it
            If useTimeClock = True Then
                'If Not Session("UserName") Is Nothing Then
                '    If pObj.HasFull = True Or Session("UserName").ToString.ToLower.Trim = "sa".ToLower Then
                '        ' lbTimeClockpunches.Visible = True
                '        btntimeclock.Visible = True
                '    Else
                '        ' lbTimeClockpunches.Visible = False
                '        btntimeclock.Visible = False
                '    End If
                'Else

                Dim advantageUserState As New Advantage.Business.Objects.User()
                advantageUserState = AdvantageSession.UserState
                If pObj.HasFull = True OrElse (advantageUserState IsNot Nothing AndAlso advantageUserState.IsUserSupport) Then
                    ' lbTimeClockpunches.Visible = True
                    btntimeclock.Visible = True
                Else
                    ' lbTimeClockpunches.Visible = False
                    btntimeclock.Visible = False
                End If
                ' End If

            Else
                btntimeclock.Visible = False
            End If




            SetDays()
            If Not strUnitTypeDescrip.ToLower = "present" And Not strUnitTypeDescrip.ToLower = "minutes" Then
                For i As Integer = 0 To 6
                    SetHours(i)
                    SetReadOnly(i, useTimeClock)
                Next
            ElseIf strUnitTypeDescrip.ToLower = "minutes" Then
                For i As Integer = 0 To 6
                    'SetHoursByTardy(i)
                    'SetReadOnly(i, useTimeClock)
                    ''Commented by saraswathi on feb 11 2009
                    ''Since minutes and clockhr is the same, the same function is called here
                    SetHours(i)
                    SetReadOnly(i, useTimeClock)
                Next
            Else
                ShowAllPresentAbsentDropDownControls()
                ShowHideByUnitType(False)
                For i As Integer = 0 To 6
                    SetPresentAbsentControl(i)
                Next
            End If
        End If

        Page.Response.Cache.SetCacheability(HttpCacheability.NoCache)
    End Sub
    Private Sub Populatehrswhendatamodified()
        'Commnetd by saraswathi Lakshmanan
        'Since after loading, the page is going to be closed. So commented the function'

        Dim StuEnrollId As String
        Dim StartDate As Date
        StuEnrollId = Request.Params("stuenrollid").ToString
        StartDate = CDate(Request.Params("date"))

        Dim facade As New Fame.AdvantageV1.BusinessFacade.TimeClock.TimeClockFacade
        dsPunch = facade.GetHrsforaStudent(StuEnrollId, StartDate)
        Session("dsPunch") = dsPunch
        PopuateHrs()
    End Sub
    Private Sub PopuateHrs()
        Dim i As Integer

        ' If Not strUnitTypeDescrip.ToLower = "present" And Not strUnitTypeDescrip.ToLower = "minutes" Then
        ''Modified on feb 11 2009
        ''MInutes removed. SInce, MInutes and clockhr are the same

        If Not strUnitTypeDescrip.ToLower = "present" Then
            dsPunch = Session("dsPunch")

            For i = 0 To dsPunch.Tables(0).Rows.Count - 1
                If dsPunch.Tables(0).Rows(i)("DOW") = 0 Then
                    If dsPunch.Tables(0).Rows(i)("ActualHours").ToString.Trim <> "" Then
                        If dsPunch.Tables(0).Rows(i)("ActualHours") = 9999.0 Then
                            txtAHD0.Text = ""
                        ElseIf dsPunch.Tables(0).Rows(i)("ActualHours") = 999.0 Then
                            txtAHD0.Text = ""
                        Else
                            txtAHD0.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                        End If
                    Else
                        txtAHD0.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                    End If

                    txtSHD0.Text = dsPunch.Tables(0).Rows(i)("SchedHours").ToString
                    If txtSHD0.Text.Trim <> "" And txtAHD0.Text.Trim <> "" Then
                        If CDec(txtSHD0.Text) > CDec(txtAHD0.Text) Then
                            txtABHD0.Text = CDec(txtSHD0.Text) - CDec(txtAHD0.Text)
                            txtMUHD0.Text = 0.0
                        ElseIf CDec(txtSHD0.Text) < CDec(txtAHD0.Text) Then
                            txtMUHD0.Text = CDec(txtAHD0.Text) - CDec(txtSHD0.Text)
                            txtABHD0.Text = 0.0
                        Else
                            txtABHD0.Text = 0.0
                            txtMUHD0.Text = 0.0
                        End If
                    Else
                        txtABHD0.Text = 0.0
                        txtMUHD0.Text = 0.0
                    End If

                    If dsPunch.Tables(0).Rows(i)("isTardy").ToString = "True" Then
                        chkT0.Checked = True
                    Else
                        chkT0.Checked = False
                    End If

                ElseIf dsPunch.Tables(0).Rows(i)("DOW") = 1 Then
                    If dsPunch.Tables(0).Rows(i)("ActualHours").ToString.Trim <> "" Then
                        If dsPunch.Tables(0).Rows(i)("ActualHours") = 9999.0 Then
                            txtAHD1.Text = ""
                        ElseIf dsPunch.Tables(0).Rows(i)("ActualHours") = 999.0 Then
                            txtAHD1.Text = ""
                        Else
                            txtAHD1.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                        End If
                    Else
                        txtAHD1.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                    End If
                    txtSHD1.Text = dsPunch.Tables(0).Rows(i)("SchedHours").ToString
                    If txtSHD1.Text <> "" And txtAHD1.Text <> "" Then
                        If CDec(txtSHD1.Text) > CDec(txtAHD1.Text) Then
                            txtABHD1.Text = CDec(txtSHD1.Text) - CDec(txtAHD1.Text)
                            txtMUHD1.Text = 0.0
                        ElseIf CDec(txtSHD1.Text) < CDec(txtAHD1.Text) Then
                            txtMUHD1.Text = CDec(txtAHD1.Text) - CDec(txtSHD1.Text)
                            txtABHD1.Text = 0.0
                        Else
                            txtABHD1.Text = 0.0
                            txtMUHD1.Text = 0.0
                        End If
                    Else
                        txtABHD1.Text = 0.0
                        txtMUHD1.Text = 0.0
                    End If
                    If dsPunch.Tables(0).Rows(i)("isTardy").ToString = "True" Then
                        chkT1.Checked = True
                    Else
                        chkT1.Checked = False
                    End If
                ElseIf dsPunch.Tables(0).Rows(i)("DOW") = 2 Then
                    If dsPunch.Tables(0).Rows(i)("ActualHours").ToString.Trim <> "" Then
                        If dsPunch.Tables(0).Rows(i)("ActualHours") = 9999.0 Then
                            txtAHD2.Text = ""
                        ElseIf dsPunch.Tables(0).Rows(i)("ActualHours") = 999.0 Then
                            txtAHD2.Text = ""
                        Else
                            txtAHD2.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                        End If
                    Else
                        txtAHD2.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                    End If
                    '  txtAHD2.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                    txtSHD2.Text = dsPunch.Tables(0).Rows(i)("SchedHours").ToString
                    If txtSHD2.Text.Trim <> "" And txtAHD2.Text.Trim <> "" Then
                        If CDec(txtSHD2.Text) > CDec(txtAHD2.Text) Then
                            txtABHD2.Text = CDec(txtSHD2.Text) - CDec(txtAHD2.Text)
                            txtMUHD2.Text = 0.0
                        ElseIf CDec(txtSHD2.Text) < CDec(txtAHD2.Text) Then
                            txtMUHD2.Text = CDec(txtAHD2.Text) - CDec(txtSHD2.Text)
                            txtABHD2.Text = 0.0
                        Else
                            txtABHD2.Text = 0.0
                            txtMUHD2.Text = 0.0
                        End If
                    Else
                        txtABHD2.Text = 0.0
                        txtMUHD2.Text = 0.0
                    End If
                    If dsPunch.Tables(0).Rows(i)("isTardy").ToString = "True" Then
                        chkT2.Checked = True
                    Else
                        chkT2.Checked = False
                    End If
                ElseIf dsPunch.Tables(0).Rows(i)("DOW") = 3 Then
                    If dsPunch.Tables(0).Rows(i)("ActualHours").ToString.Trim <> "" Then
                        If dsPunch.Tables(0).Rows(i)("ActualHours") = 9999.0 Then
                            txtAHD3.Text = ""
                        ElseIf dsPunch.Tables(0).Rows(i)("ActualHours") = 999.0 Then
                            txtAHD3.Text = ""
                        Else
                            txtAHD3.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                        End If
                    Else
                        txtAHD3.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                    End If
                    txtSHD3.Text = dsPunch.Tables(0).Rows(i)("SchedHours").ToString
                    If txtSHD3.Text.Trim <> "" And txtAHD3.Text.Trim <> "" Then
                        If CDec(txtSHD3.Text) > CDec(txtAHD3.Text) Then
                            txtABHD3.Text = CDec(txtSHD3.Text) - CDec(txtAHD3.Text)
                            txtMUHD3.Text = 0.0
                        ElseIf CDec(txtSHD3.Text) < CDec(txtAHD3.Text) Then
                            txtMUHD3.Text = CDec(txtAHD3.Text) - CDec(txtSHD3.Text)
                            txtABHD3.Text = 0.0
                        Else
                            txtABHD3.Text = 0.0
                            txtMUHD3.Text = 0.0
                        End If
                    Else
                        txtABHD3.Text = 0.0
                        txtMUHD3.Text = 0.0
                    End If
                    If dsPunch.Tables(0).Rows(i)("isTardy").ToString = "True" Then
                        chkT3.Checked = True
                    Else
                        chkT3.Checked = False
                    End If
                ElseIf dsPunch.Tables(0).Rows(i)("DOW") = 4 Then
                    If dsPunch.Tables(0).Rows(i)("ActualHours").ToString.Trim <> "" Then
                        If dsPunch.Tables(0).Rows(i)("ActualHours") = 9999.0 Then
                            txtAHD4.Text = ""
                        ElseIf dsPunch.Tables(0).Rows(i)("ActualHours") = 999.0 Then
                            txtAHD4.Text = ""
                        Else
                            txtAHD4.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                        End If
                    Else
                        txtAHD4.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                    End If
                    txtSHD4.Text = dsPunch.Tables(0).Rows(i)("SchedHours").ToString
                    If txtSHD4.Text.Trim <> "" And txtAHD4.Text.Trim <> "" Then
                        If CDec(txtSHD4.Text) > CDec(txtAHD4.Text) Then
                            txtABHD4.Text = CDec(txtSHD4.Text) - CDec(txtAHD4.Text)
                            txtMUHD4.Text = 0.0
                        ElseIf CDec(txtSHD4.Text) < CDec(txtAHD4.Text) Then
                            txtMUHD4.Text = CDec(txtAHD4.Text) - CDec(txtSHD4.Text)
                            txtABHD4.Text = 0.0
                        Else
                            txtABHD4.Text = 0.0
                            txtMUHD4.Text = 0.0
                        End If
                    Else
                        txtABHD4.Text = 0.0
                        txtMUHD4.Text = 0.0
                    End If
                    If dsPunch.Tables(0).Rows(i)("isTardy").ToString = "True" Then
                        chkT4.Checked = True
                    Else
                        chkT4.Checked = False
                    End If
                ElseIf dsPunch.Tables(0).Rows(i)("DOW") = 5 Then
                    If dsPunch.Tables(0).Rows(i)("ActualHours").ToString.Trim <> "" Then
                        If dsPunch.Tables(0).Rows(i)("ActualHours") = 9999.0 Then
                            txtAHD5.Text = ""
                        ElseIf dsPunch.Tables(0).Rows(i)("ActualHours") = 999.0 Then
                            txtAHD5.Text = ""
                        Else
                            txtAHD5.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                        End If
                    Else
                        txtAHD5.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                    End If
                    txtSHD5.Text = dsPunch.Tables(0).Rows(i)("SchedHours").ToString
                    If txtSHD5.Text.Trim <> "" And txtAHD5.Text.Trim <> "" Then
                        If CDec(txtSHD5.Text) > CDec(txtAHD5.Text) Then
                            txtABHD5.Text = CDec(txtSHD5.Text) - CDec(txtAHD5.Text)
                            txtMUHD5.Text = 0.0
                        ElseIf CDec(txtSHD5.Text) < CDec(txtAHD5.Text) Then
                            txtMUHD5.Text = CDec(txtAHD5.Text) - CDec(txtSHD5.Text)
                            txtABHD5.Text = 0.0
                        Else
                            txtABHD5.Text = 0.0
                            txtMUHD5.Text = 0.0
                        End If
                    Else
                        txtABHD5.Text = 0.0
                        txtMUHD5.Text = 0.0
                    End If
                    If dsPunch.Tables(0).Rows(i)("isTardy").ToString = "True" Then
                        chkT5.Checked = True
                    Else
                        chkT5.Checked = False
                    End If
                ElseIf dsPunch.Tables(0).Rows(i)("DOW") = 6 Then
                    If dsPunch.Tables(0).Rows(i)("ActualHours").ToString.Trim <> "" Then
                        If dsPunch.Tables(0).Rows(i)("ActualHours") = 9999.0 Then
                            txtAHD6.Text = ""
                        ElseIf dsPunch.Tables(0).Rows(i)("ActualHours") = 999.0 Then
                            txtAHD6.Text = ""
                        Else
                            txtAHD6.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                        End If
                    Else
                        txtAHD6.Text = dsPunch.Tables(0).Rows(i)("ActualHours").ToString
                    End If
                    txtSHD6.Text = dsPunch.Tables(0).Rows(i)("SchedHours").ToString
                    If txtSHD6.Text.Trim <> "" And txtAHD6.Text.Trim <> "" Then
                        If CDec(txtSHD6.Text) > CDec(txtAHD6.Text) Then
                            txtABHD6.Text = CDec(txtSHD6.Text) - CDec(txtAHD6.Text)
                            txtMUHD6.Text = 0.0
                        ElseIf CDec(txtSHD6.Text) < CDec(txtAHD6.Text) Then
                            txtMUHD6.Text = CDec(txtAHD6.Text) - CDec(txtSHD6.Text)
                            txtABHD6.Text = 0.0
                        Else
                            txtABHD6.Text = 0.0
                            txtMUHD6.Text = 0.0
                        End If
                    Else
                        txtABHD6.Text = 0.0
                        txtMUHD6.Text = 0.0
                    End If
                    If dsPunch.Tables(0).Rows(i)("isTardy").ToString = "True" Then
                        chkT6.Checked = True
                    Else
                        chkT6.Checked = False
                    End If
                End If

            Next
        End If

    End Sub
    'Private Sub MakeTardyControlVisibleWithTextBoxTardy(ByVal SchedHours As ArrayList, ByVal ActualHours As ArrayList)
    '    Dim i As Integer
    '    Dim ctlId As String
    '    Dim SchedHrs As String = ""
    '    Dim ActualHrs As String = ""
    '    Dim txt As TextBox
    '    Dim txtAB As eWorld.UI.NumericBox
    '    Dim decSchedHrs As Decimal = 0.0
    '    Dim decActualHrs As Decimal = 0.0
    '    Dim dsCheckTardyIn As New DataSet
    '    dsCheckTardyIn = SchedulesFacade.CheckTardyIn(Request.Params("prgverid"), Request.Params("stuenrollid"))
    '    For i = 0 To 6
    '        ctlId = "txtT" + i.ToString()
    '        SchedHrs = SchedHours(i).ToString
    '        ActualHrs = ActualHours(i).ToString
    '        txt = CType(FindControl(ctlId), TextBox)
    '        txt.Visible = True
    '        txt.Enabled = False

    '        ctlId = "txtABHD" + i.ToString
    '        txtAB = CType(FindControl(ctlId), TextBox)

    '        If Not SchedHrs = "" Then
    '            decSchedHrs = CType(SchedHrs, Decimal)
    '        End If
    '        If Not ActualHrs = "" Then
    '            decActualHrs = CType(ActualHrs, Decimal)
    '        End If
    '        'the if statement checks to see if program version tracks tardies
    '        If Not SchedHrs = "" And Not ActualHrs = "" And Request.Params("tardytrack") >= 1 Then
    '            'once the program version tracks tardy check to see if check tardy in is checked
    '            If dsCheckTardyIn.Tables(0).Rows.Count >= 1 Then
    '                For Each dr As DataRow In dsCheckTardyIn.Tables(0).Rows
    '                    'Columns:t2.dw,t2.DeadLineBeforeConsideredTardy,t4.PunchedInTime,t2.TimeTakenAsPunchIn
    '                    If i = CType(dr("dw"), Integer) Then
    '                        Dim TimeToBeConsideredPunchInTime As Date = "00:00:00"
    '                        Dim ActualPunchInTime As Date = "00:00:00"
    '                        Dim DeadLineBeforeConsideredTardy As Date = "00:00:00"
    '                        If Not dr("PunchedInTime") Is System.DBNull.Value Then ActualPunchInTime = CType(dr("PunchedInTime"), DateTime)
    '                        If Not dr("DeadLineBeforeConsideredTardy") Is System.DBNull.Value Then DeadLineBeforeConsideredTardy = CType(dr("DeadLineBeforeConsideredTardy"), DateTime)
    '                        If Not dr("TimeTakenAsPunchIn") Is System.DBNull.Value Then TimeToBeConsideredPunchInTime = CType(dr("TimeTakenAsPunchIn"), DateTime)
    '                        If decSchedHrs > decActualHrs Then
    '                            txt.Text = (decSchedHrs - decActualHrs).ToString  'This value will show up in Tardy box
    '                            txtAB.Text = "0.00"
    '                            Exit For
    '                        Else
    '                            'If student comes in after tardy in time then student will be considered tardy and 
    '                            'punch in time will be deducted from tardy assigned time.
    '                            txt.Text = ComputeTardy(ActualPunchInTime, DeadLineBeforeConsideredTardy, TimeToBeConsideredPunchInTime)
    '                            txtAB.Text = "0.00"
    '                        End If
    '                    Else
    '                        If decActualHrs < decSchedHrs Then
    '                            txt.Text = (decSchedHrs - decActualHrs).ToString  'This value will show up in Tardy box
    '                            txtAB.Text = "0.00"
    '                        End If
    '                    End If
    '                Next
    '            Else
    '                txt.Text = (decSchedHrs - decActualHrs).ToString  'This value will show up in Tardy box
    '                txtAB.Text = "0.00"
    '            End If
    '        Else
    '            txt.Text = ""
    '        End If
    '    Next
    'End Sub
    'Private Sub MakeTardyControlVisible(ByVal SchedHours As ArrayList, ByVal ActualHours As ArrayList)
    '    Dim i As Integer
    '    Dim ctlId As String
    '    Dim SchedHrs As String = ""
    '    Dim ActualHrs As String = ""
    '    Dim chk As CheckBox
    '    Dim txtAB As eWorld.UI.NumericBox
    '    Dim decSchedHrs As Decimal = 0.0
    '    Dim decActualHrs As Decimal = 0.0
    '    Dim dsCheckTardyIn As New DataSet
    '    dsCheckTardyIn = SchedulesFacade.CheckTardyIn(Request.Params("prgverid"), Request.Params("stuenrollid"))
    '    For i = 0 To 6
    '        ctlId = "chkT" + i.ToString()
    '        SchedHrs = SchedHours(i).ToString
    '        ActualHrs = ActualHours(i).ToString
    '        chk = CType(FindControl(ctlId), CheckBox)
    '        chk.Visible = True
    '        chk.Enabled = False

    '        ctlId = "txtABHD" + i.ToString
    '        txtAB = CType(FindControl(ctlId), TextBox)

    '        If Not SchedHrs = "" Then
    '            decSchedHrs = CType(SchedHrs, Decimal)
    '        End If
    '        If Not ActualHrs = "" Then
    '            decActualHrs = CType(ActualHrs, Decimal)
    '        End If
    '        'the if statement checks to see if program version tracks tardies
    '        If Not SchedHrs = "" And Not ActualHrs = "" And Request.Params("tardytrack") >= 1 Then
    '            'once the program version tracks tardy check to see if check tardy in is checked
    '            If dsCheckTardyIn.Tables(0).Rows.Count >= 1 Then
    '                For Each dr As DataRow In dsCheckTardyIn.Tables(0).Rows
    '                    'Columns:t2.dw,t2.DeadLineBeforeConsideredTardy,t4.PunchedInTime,t2.TimeTakenAsPunchIn
    '                    If i = CType(dr("dw"), Integer) Then
    '                        Dim TimeToBeConsideredPunchInTime As Date = "00:00:00"
    '                        Dim ActualPunchInTime As Date = "00:00:00"
    '                        Dim DeadLineBeforeConsideredTardy As Date = "00:00:00"
    '                        If Not dr("PunchedInTime") Is System.DBNull.Value Then ActualPunchInTime = CType(dr("PunchedInTime"), DateTime)
    '                        If Not dr("DeadLineBeforeConsideredTardy") Is System.DBNull.Value Then DeadLineBeforeConsideredTardy = CType(dr("DeadLineBeforeConsideredTardy"), DateTime)
    '                        If Not dr("TimeTakenAsPunchIn") Is System.DBNull.Value Then TimeToBeConsideredPunchInTime = CType(dr("TimeTakenAsPunchIn"), DateTime)
    '                        chk.Checked = True
    '                    Else
    '                        chk.Checked = False
    '                    End If
    '                Next
    '            Else
    '                chk.Checked = False
    '            End If
    '        Else
    '            chk.Checked = False
    '        End If
    '    Next
    'End Sub
    'Private Function ComputeTardy(ByVal ActualPunchInTime As Date, ByVal DeadLineBeforeConsideredTardy As Date, ByVal TimeToBeConsideredPunchInTime As Date) As String
    '    Dim strTardyValue As String = ""
    '    If Not ActualPunchInTime = "00:00:00" And Not TimeToBeConsideredPunchInTime = "00:00:00" Then
    '        strTardyValue = ((TimeToBeConsideredPunchInTime - ActualPunchInTime).TotalHours).ToString
    '    ElseIf Not ActualPunchInTime = "00:00:00" And TimeToBeConsideredPunchInTime = "00:00:00" And Not DeadLineBeforeConsideredTardy = "00:00:00" Then
    '        strTardyValue = ((DeadLineBeforeConsideredTardy - ActualPunchInTime).TotalHours).ToString
    '    End If
    '    Return strTardyValue
    'End Function
    Private Function GetActualDaysValue(ByVal inValue As String) As Integer
        If inValue = "1" Then
            Return 1
        Else
            Return 0
        End If
    End Function
    Protected Sub SetReadOnly(ByVal i As Integer, ByVal bEnable As Boolean)
        Dim sdate As Date = CType(Request.Params("date"), Date)
        sdate = sdate.AddDays(i)
        If sdate > Date.Now Then
            bEnable = True ' make readonly if we are past today current date
        End If
        Dim txtS As eWorld.UI.NumericBox = CType(FindControl("txtSHD" + i.ToString()), eWorld.UI.NumericBox)
        Dim txtA As eWorld.UI.NumericBox = CType(FindControl("txtAHD" + i.ToString()), eWorld.UI.NumericBox)
        txtS.ReadOnly = bEnable
        txtA.ReadOnly = bEnable
    End Sub
    Protected Sub SetDays()
        Dim sdate As Date = CType(Request.Params("date"), Date)
        Me.lblD0.Text = sdate.ToString("MMM dd")
        Me.lblD1.Text = sdate.AddDays(1).ToString("MMM dd")
        Me.lblD2.Text = sdate.AddDays(2).ToString("MMM dd")
        Me.lblD3.Text = sdate.AddDays(3).ToString("MMM dd")
        Me.lblD4.Text = sdate.AddDays(4).ToString("MMM dd")
        Me.lblD5.Text = sdate.AddDays(5).ToString("MMM dd")
        Me.lblD6.Text = sdate.AddDays(6).ToString("MMM dd")
    End Sub
    Protected Sub SetHours(ByVal i As Integer)
        Try
            Dim txtS As eWorld.UI.NumericBox = CType(FindControl("txtSHD" + i.ToString()), eWorld.UI.NumericBox)
            Dim txtA As eWorld.UI.NumericBox = CType(FindControl("txtAHD" + i.ToString()), eWorld.UI.NumericBox)
            Dim chkT As CheckBox = CType(FindControl("chkT" + i.ToString()), CheckBox)

            Dim dtStartDate As Date = CType(txtStudentStartDate.Text, Date).ToShortDateString
            Dim dtWeekStartDate As Date = CType(Request.Params("date"), Date)
            Dim dtDayDate As Date = dtWeekStartDate.AddDays(i)
            If DateDiff(DateInterval.Day, dtStartDate, dtDayDate) < 0 Then
                txtS.Text = ""
                txtA.Text = ""
                txtS.Enabled = False
                txtA.Enabled = False
            End If

            If txtS.Text.Length = 0 Then Return ' nothing to do
            If txtA.Text.Length = 0 Then Return ' nothing to do

            Dim aHrs As Decimal = 0.0
            If txtA.Text <> "" Then
                If InStr(txtA.Text, "(") >= 1 Then
                    aHrs = CType(Mid(txtA.Text, 1, InStr(txtA.Text, "(") - 1), Decimal)
                    txtA.Text = aHrs
                Else
                    aHrs = CType(txtA.Text, Decimal)
                    txtA.Text = aHrs
                End If
            End If

            Dim sHrs As Decimal = CType(txtS.Text, Decimal)
            Dim txtMU As eWorld.UI.NumericBox = CType(FindControl("txtMUHD" + i.ToString()), eWorld.UI.NumericBox)
            Dim txtAB As eWorld.UI.NumericBox = CType(FindControl("txtABHD" + i.ToString()), eWorld.UI.NumericBox)
            'Dim txtT As TextBox = CType(FindControl("txtT" + i.ToString()), TextBox)

            If sHrs = 0.0 And aHrs > sHrs Then
                ' if actual hours > sched hours, then there are makeup hours
                Dim mHrs As Decimal = aHrs - sHrs
                txtMU.Text = mHrs.ToString()
            ElseIf aHrs < sHrs Then
                ' if actual hours < sched hours, then there are absent hours
                If aHrs <> 0.0 Then
                    Dim abHrs As Decimal = sHrs - aHrs
                    txtAB.Text = abHrs.ToString()
                ElseIf aHrs = 0.0 And sHrs > 0 Then
                    Dim abHrs As Decimal = sHrs - aHrs
                    txtAB.Text = abHrs.ToString()
                Else
                    txtAB.Text = ""
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Protected Sub SetHoursByTardy(ByVal i As Integer)
        Try
            Dim txtS As eWorld.UI.NumericBox = CType(FindControl("txtSHD" + i.ToString()), eWorld.UI.NumericBox)
            Dim txtA As eWorld.UI.NumericBox = CType(FindControl("txtAHD" + i.ToString()), eWorld.UI.NumericBox)
            Dim chkT As CheckBox = CType(FindControl("chkT" + i.ToString()), CheckBox)

            Dim dtStartDate As Date = CType(txtStudentStartDate.Text, Date).ToShortDateString
            Dim dtWeekStartDate As Date = CType(Request.Params("date"), Date)
            Dim dtDayDate As Date = dtWeekStartDate.AddDays(i)

            If DateDiff(DateInterval.Day, dtStartDate, dtDayDate) < 0 Then
                txtS.Text = ""
                txtA.Text = ""
                txtS.Enabled = False
                txtA.Enabled = False
            End If

            If txtS.Text.Length = 0 Then Return ' nothing to do
            If txtA.Text.Length = 0 Then Return ' nothing to do

            Dim aHrs As Decimal = CType(txtA.Text, Decimal)
            Dim sHrs As Decimal = CType(txtS.Text, Decimal)
            Dim txtMU As eWorld.UI.NumericBox = CType(FindControl("txtMUHD" + i.ToString()), eWorld.UI.NumericBox)
            Dim txtAB As eWorld.UI.NumericBox = CType(FindControl("txtABHD" + i.ToString()), eWorld.UI.NumericBox)

            If aHrs > sHrs Then
                ' if actual hours > sched hours, then there are makeup hours
                Dim mHrs As Decimal = aHrs - sHrs
                txtMU.Text = mHrs.ToString()
            ElseIf aHrs < sHrs Then
                ' if actual hours < sched hours, then there are absent hours
                Dim abHrs As Decimal = sHrs - aHrs
                txtAB.Text = abHrs.ToString()
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    'Private Sub DefaultValue(ByVal CtlId As DropDownList)
    '    CtlId.SelectedValue = "0"
    'End Sub
    Protected Sub SetPresentAbsentControl(ByVal i As Integer)
        Try
            Dim ddlS As DropDownList = CType(FindControl("ddlSHD" + i.ToString()), DropDownList)
            Dim ddlA As DropDownList = CType(FindControl("ddlAHD" + i.ToString()), DropDownList)
            Dim dtStartDate As Date = CType(txtStudentStartDate.Text, Date).ToShortDateString
            Dim dtWeekStartDate As Date = CType(Request.Params("date"), Date)
            Dim dtGradDate As Date = CType(Request.Params("expgraddate"), Date)
            Dim dtDateDetermined As Date = CType(Request.Params("datedropped"), Date)
            Dim dtDayDate As Date = dtWeekStartDate.AddDays(i)
            Dim TodaysDate As Date = CType(Date.Now, Date).ToShortDateString
            Dim strstatuscode As String = Request.Params("studentstatuscode")
            If strstatuscode <> "" Then strstatuscode = CType(strstatuscode, StudentStatusCodes)
            Dim lblDay As Label = CType(FindControl("lblD" + i.ToString()), Label)
            Dim chkTardy As CheckBox = CType(FindControl("chkT" + i.ToString()), CheckBox)
            Dim strActualValue As String = ""
            Dim strAttPos As String = String.Empty
            If i = 0 Then
                strAttPos = Request.Params("ahrs0").ToString()
                strActualValue = Request.Params("day0").ToString
            ElseIf i = 1 Then
                strAttPos = Request.Params("ahrs1").ToString()
                strActualValue = Request.Params("day1").ToString
            ElseIf i = 2 Then
                strAttPos = Request.Params("ahrs2").ToString()
                strActualValue = Request.Params("day2").ToString
            ElseIf i = 3 Then
                strAttPos = Request.Params("ahrs3").ToString()
                strActualValue = Request.Params("day3").ToString
            ElseIf i = 4 Then
                strAttPos = Request.Params("ahrs4").ToString()
                strActualValue = Request.Params("day4").ToString
            ElseIf i = 5 Then
                strAttPos = Request.Params("ahrs5").ToString()
                strActualValue = Request.Params("day5").ToString
            ElseIf i = 6 Then
                strAttPos = Request.Params("ahrs6").ToString()
                strActualValue = Request.Params("day6").ToString
            End If
            If (DateDiff(DateInterval.Day, dtStartDate, dtDayDate) < 0 Or _
                   dtDayDate > TodaysDate) Then
                'DefaultValue(ddlS)
                'DefaultValue(ddlA)
                ddlS.Enabled = False
                ddlA.Enabled = False
                chkTardy.Enabled = False
                ddlS.SelectedValue = 0
                If DateDiff(DateInterval.Day, dtStartDate, dtDayDate) < 0 Then
                    lblDay.ToolTip = "Entries are disabled because this day is before the student's start date."
                ElseIf dtDayDate > TodaysDate Then
                    lblDay.ToolTip = "Cannot post on a future date."
                End If
            End If
            If IsHoliday(dtDayDate) Then
                If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                    ddlS.Enabled = False
                    ddlA.Enabled = False
                    chkTardy.Enabled = False
                    ddlS.SelectedValue = 0
                    lblDay.ToolTip = "Entries are disabled because this day is a holiday."
                Else
                    ddlS.Enabled = False
                    ddlS.SelectedValue = 0
                End If
            End If

            If Not (strstatuscode = StudentStatusCodes.Currently_Attending Or _
                          strstatuscode = StudentStatusCodes.Future_Start Or _
                          strstatuscode = StudentStatusCodes.LOA Or _
                          strstatuscode = StudentStatusCodes.Probation Or _
                          strstatuscode = StudentStatusCodes.Suspension Or _
                          strstatuscode = StudentStatusCodes.Suspended) Then
                'DefaultValue(ddlS)
                'DefaultValue(ddlA)
                'Response.Write(dtDayDate)
                'Response.Write(DateDiff(DateInterval.Day, dtStartDate, dtDayDate))
                'Response.Write(dtDateDetermined)
                'Response.Write("<br>")
                If Not ((DateDiff(DateInterval.Day, dtStartDate, dtDayDate) >= 0 And dtGradDate <> "01/01/1900" And dtDayDate <= dtGradDate) Or _
                    (DateDiff(DateInterval.Day, dtStartDate, dtDayDate) >= 0 And dtDateDetermined <> "01/01/1900" And dtDayDate <= dtDateDetermined)) Then
                    ddlS.SelectedValue = "0"
                    ddlS.Enabled = False
                    ddlA.Enabled = False
                    chkTardy.Enabled = False
                    lblDay.ToolTip = "Cannot post attendance because student's status is 'non-enrolled'"
                End If
                If (dtDayDate > dtGradDate And dtGradDate <> "01/01/1900") Then
                    ddlS.SelectedValue = "0"
                    ddlS.Enabled = False
                    ddlA.Enabled = False
                    chkTardy.Enabled = False
                    lblDay.ToolTip = "Cannot post attendance because student's status is 'non-enrolled'"
                End If
                If (dtDayDate > dtDateDetermined And dtDateDetermined <> "01/01/1900") Then
                    ddlS.SelectedValue = "0"
                    ddlS.Enabled = False
                    ddlA.Enabled = False
                    chkTardy.Enabled = False
                    lblDay.ToolTip = "Cannot post attendance because student's status is 'non-enrolled'"
                End If
            End If

            If ddlS.Text.Length = 0 Then Return ' nothing to do
            If ddlA.Text.Length = 0 Then Return ' nothing to do
            Dim ddlMU As DropDownList = CType(FindControl("ddlMUHD" + i.ToString()), DropDownList)
            Dim ddlAB As DropDownList = CType(FindControl("ddlABHD" + i.ToString()), DropDownList)


            If ddlS.SelectedValue = "1" And ddlA.SelectedValue = "1" Then 'Disable Make Over and Enable Absent
                ddlAB.SelectedValue = "0"
                ddlMU.SelectedValue = "0" '""
                ddlMU.Enabled = False
            ElseIf ddlS.SelectedValue = "1" And ddlA.SelectedValue = "0" And (strAttPos <> "99") Then
                ddlAB.SelectedValue = "1"
                ddlMU.SelectedValue = "0" '""
                ddlMU.Enabled = False
            ElseIf ddlS.SelectedValue = "0" And ddlA.SelectedValue = "1" And chkTardy.Checked = False Then
                ddlAB.SelectedValue = "0" '""
                ddlAB.Enabled = False
                ddlMU.SelectedValue = "1"
            ElseIf ddlS.SelectedValue = "0" And ddlA.SelectedValue = "1" And chkTardy.Checked = True Then
                ddlAB.SelectedValue = "0" '""
                ddlAB.Enabled = False
                ddlA.SelectedValue = "1"
                ddlMU.SelectedValue = "1"
            ElseIf ddlA.SelectedValue = "99" Then
                ddlAB.SelectedValue = "0"
                ddlA.SelectedValue = "0"
                ddlMU.SelectedValue = "0"
            Else
                If strActualValue.ToString.ToLower = "a" And ddlS.SelectedValue = "0" Then
                    ddlAB.SelectedValue = "0"
                    ddlMU.SelectedValue = "0"
                ElseIf strActualValue.ToString.ToLower = "a" And ddlS.SelectedValue = "1" Then
                    ddlAB.SelectedValue = "1"
                    ddlMU.SelectedValue = "0"
                ElseIf strActualValue.ToString.ToLower = "p" Then
                    ddlAB.SelectedValue = "0"
                    ddlMU.SelectedValue = "1"
                Else
                    ddlAB.SelectedValue = "0"
                    ddlMU.SelectedValue = "0"
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Protected Function GetSafeNumber(ByVal ctlId As String) As Decimal
        Dim obj As Object = FindControl(ctlId)
        If obj Is Nothing Then Return 0

        Dim txt As eWorld.UI.NumericBox = CType(obj, eWorld.UI.NumericBox)
        If txt.Text.Length = 0 Then Return 0 ' nothing was enter, just return 0

        Try
            Return CType(txt.Text, Decimal) ' try to return a converted number
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return 0
        End Try
    End Function
    Protected Sub btnOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim js As New StringBuilder
        Dim res As New StringBuilder()
        Dim AttFacade As New Fame.AdvantageV1.BusinessFacade.AR.AttendanceFacade
        ' If strUnitTypeDescrip.ToString.ToLower = "present" Or strUnitTypeDescrip.ToString.ToLower = "minutes" Then
        'modified on feb 11 2009
        ''The minutes and ClockHr are the same. So, MInutes removed

        If strUnitTypeDescrip.ToString.ToLower = "present" Then
            Dim dtWeekStartDate As Date = CType(Request.Params("date"), Date)
            If Not txtSDH0Changed.Text = "" Then
                AttFacade.UpdateScheduleByDateAndEnrollment(txtStuEnrollId.Text, txtSource.Text, dtWeekStartDate.AddDays(0), txtSDH0Changed.Text)
            End If
            If Not txtSDH1Changed.Text = "" Then
                AttFacade.UpdateScheduleByDateAndEnrollment(txtStuEnrollId.Text, txtSource.Text, dtWeekStartDate.AddDays(1), txtSDH1Changed.Text)
            End If
            If Not txtSDH2Changed.Text = "" Then
                AttFacade.UpdateScheduleByDateAndEnrollment(txtStuEnrollId.Text, txtSource.Text, dtWeekStartDate.AddDays(2), txtSDH2Changed.Text)
            End If
            If Not txtSDH3Changed.Text = "" Then
                AttFacade.UpdateScheduleByDateAndEnrollment(txtStuEnrollId.Text, txtSource.Text, dtWeekStartDate.AddDays(3), txtSDH3Changed.Text)
            End If
            If Not txtSDH4Changed.Text = "" Then
                AttFacade.UpdateScheduleByDateAndEnrollment(txtStuEnrollId.Text, txtSource.Text, dtWeekStartDate.AddDays(4), txtSDH4Changed.Text)
            End If
            If Not txtSDH5Changed.Text = "" Then
                AttFacade.UpdateScheduleByDateAndEnrollment(txtStuEnrollId.Text, txtSource.Text, dtWeekStartDate.AddDays(5), txtSDH5Changed.Text)
            End If
            If Not txtSDH6Changed.Text = "" Then
                AttFacade.UpdateScheduleByDateAndEnrollment(txtStuEnrollId.Text, txtSource.Text, dtWeekStartDate.AddDays(6), txtSDH6Changed.Text)
            End If
        End If

        ' If Not strUnitTypeDescrip.ToLower = "present" And Not strUnitTypeDescrip.ToLower = "minutes" Then
        ''Modified on feb 11 2009
        ''Since4 clockhr and minutes are the same
        'If Not strUnitTypeDescrip.ToLower = "present" Then

        '    Dim TD0, TD1, TD2, TD3, TD4, TD5, TD6 As Integer
        '    If Me.chkT0.Checked = True Then
        '        TD0 = 1
        '        Me.txtAHD0.Text = txtAHD0.Text '& "(T)"
        '    Else
        '        TD0 = 0
        '    End If

        '    If Me.chkT1.Checked = True Then
        '        TD1 = 1
        '        Me.txtAHD1.Text = Me.txtAHD1.Text '& "(T)"
        '    Else
        '        TD1 = 0
        '    End If

        '    If Me.chkT2.Checked = True Then
        '        TD2 = 1
        '        Me.txtAHD2.Text = Me.txtAHD2.Text '& "(T)"
        '    Else
        '        TD2 = 0
        '    End If

        '    If Me.chkT3.Checked = True Then
        '        TD3 = 1
        '        Me.txtAHD3.Text = Me.txtAHD3.Text '& "(T)"
        '    Else
        '        TD3 = 0
        '    End If

        '    If Me.chkT4.Checked = True Then
        '        TD4 = 1
        '        Me.txtAHD4.Text = Me.txtAHD4.Text '& "(T)"
        '    Else
        '        TD4 = 0
        '    End If

        '    If Me.chkT5.Checked = True Then
        '        TD5 = 1
        '        Me.txtAHD5.Text = Me.txtAHD5.Text '& "(T)"
        '    Else
        '        TD5 = 0
        '    End If

        '    If Me.chkT6.Checked = True Then
        '        TD6 = 1
        '        Me.txtAHD6.Text = Me.txtAHD6.Text '& "(T)"
        '    Else
        '        TD6 = 0
        '    End If

        '    res.AppendFormat("{0},{1},{2},{3},{4},{5},{6},", _
        '                        Me.txtSHD0.Text, _
        '                        Me.txtSHD1.Text, _
        '                        Me.txtSHD2.Text, _
        '                        Me.txtSHD3.Text, _
        '                        Me.txtSHD4.Text, _
        '                        Me.txtSHD5.Text, _
        '                        Me.txtSHD6.Text)
        '    res.AppendFormat("{0},{1},{2},{3},{4},{5},{6},", _
        '                        Me.txtAHD0.Text, _
        '                        Me.txtAHD1.Text, _
        '                        Me.txtAHD2.Text, _
        '                        Me.txtAHD3.Text, _
        '                        Me.txtAHD4.Text, _
        '                        Me.txtAHD5.Text, _
        '                        Me.txtAHD6.Text)
        '    res.AppendFormat("{0},{1},{2},{3},{4},{5},{6},", _
        '    TD0, _
        '    TD1, _
        '    TD2, _
        '    TD3, _
        '    TD4, _
        '    TD5, _
        '    TD6, _
        '    txtScheduleChange.Text)
        '    ''Commented by Saraswathi on feb 11 2009
        '    ''Since for minutes it is not T, it is only for P/A type
        '    'ElseIf strUnitTypeDescrip.ToLower = "minutes" Then
        '    '    Dim TD0, TD1, TD2, TD3, TD4, TD5, TD6 As Integer
        '    '    If Me.chkT0.Checked = True Then
        '    '        TD0 = 1
        '    '        Me.txtAHD0.Text = "T" 'Me.txtAHD0.Text & " (T)"
        '    '    Else
        '    '        TD0 = 0
        '    '    End If

        '    '    If Me.chkT1.Checked = True Then
        '    '        TD1 = 1
        '    '        Me.txtAHD1.Text = "T" 'Me.txtAHD1.Text & " (T)"
        '    '    Else
        '    '        TD1 = 0
        '    '    End If

        '    '    If Me.chkT2.Checked = True Then
        '    '        TD2 = 1
        '    '        Me.txtAHD2.Text = "T" 'Me.txtAHD2.Text & " (T)"
        '    '    Else
        '    '        TD2 = 0
        '    '    End If

        '    '    If Me.chkT3.Checked = True Then
        '    '        TD3 = 1
        '    '        Me.txtAHD3.Text = "T" 'Me.txtAHD3.Text & " (T)"
        '    '    Else
        '    '        TD3 = 0
        '    '    End If

        '    '    If Me.chkT4.Checked = True Then
        '    '        TD4 = 1
        '    '        Me.txtAHD4.Text = "T" 'Me.txtAHD4.Text & " (T)"
        '    '    Else
        '    '        TD4 = 0
        '    '    End If

        '    '    If Me.chkT5.Checked = True Then
        '    '        TD5 = 1
        '    '        Me.txtAHD5.Text = "T" 'Me.txtAHD5.Text & " (T)"
        '    '    Else
        '    '        TD5 = 0
        '    '    End If

        '    '    If Me.chkT6.Checked = True Then
        '    '        TD6 = 1
        '    '        Me.txtAHD6.Text = "T" 'Me.txtAHD6.Text & " (T)"
        '    '    Else
        '    '        TD6 = 0
        '    '    End If

        '    '    res.AppendFormat("{0},{1},{2},{3},{4},{5},{6},", _
        '    '                       Me.txtSHD0.Text, _
        '    '                       Me.txtSHD1.Text, _
        '    '                       Me.txtSHD2.Text, _
        '    '                       Me.txtSHD3.Text, _
        '    '                       Me.txtSHD4.Text, _
        '    '                       Me.txtSHD5.Text, _
        '    '                       Me.txtSHD6.Text)
        '    '    res.AppendFormat("{0},{1},{2},{3},{4},{5},{6},", _
        '    '                        Me.txtAHD0.Text, _
        '    '                        Me.txtAHD1.Text, _
        '    '                        Me.txtAHD2.Text, _
        '    '                        Me.txtAHD3.Text, _
        '    '                        Me.txtAHD4.Text, _
        '    '                        Me.txtAHD5.Text, _
        '    '                        Me.txtAHD6.Text)
        '    '    res.AppendFormat("{0},{1},{2},{3},{4},{5},{6},", _
        '    '    TD0, _
        '    '    TD1, _
        '    '    TD2, _
        '    '    TD3, _
        '    '    TD4, _
        '    '    TD5, _
        '    '    TD6, _
        '    '    txtScheduleChange.Text)
        'Else
        If strUnitTypeDescrip.ToLower = "present" Then
            Dim AHD0, AHD1, AHD2, AHD3, AHD4, AHD5, AHD6 As String
            Dim TD0, TD1, TD2, TD3, TD4, TD5, TD6 As Integer
            If Me.chkT0.Checked = True Then
                TD0 = 1
            Else
                TD0 = 0
            End If

            If Me.chkT1.Checked = True Then
                TD1 = 1
            Else
                TD1 = 0
            End If

            If Me.chkT2.Checked = True Then
                TD2 = 1
            Else
                TD2 = 0
            End If

            If Me.chkT3.Checked = True Then
                TD3 = 1
            Else
                TD3 = 0
            End If

            If Me.chkT4.Checked = True Then
                TD4 = 1
            Else
                TD4 = 0
            End If

            If Me.chkT5.Checked = True Then
                TD5 = 1
            Else
                TD5 = 0
            End If

            If Me.chkT6.Checked = True Then
                TD6 = 1
            Else
                TD6 = 0
            End If

            AHD0 = ReturnUnitTypeById(Me.ddlAHD0, TD0)
            AHD1 = ReturnUnitTypeById(Me.ddlAHD1, TD1)
            AHD2 = ReturnUnitTypeById(Me.ddlAHD2, TD2)
            AHD3 = ReturnUnitTypeById(Me.ddlAHD3, TD3)
            AHD4 = ReturnUnitTypeById(Me.ddlAHD4, TD4)
            AHD5 = ReturnUnitTypeById(Me.ddlAHD5, TD5)
            AHD6 = ReturnUnitTypeById(Me.ddlAHD6, TD6)

            res.AppendFormat("{0},{1},{2},{3},{4},{5},{6},", _
                                Me.ddlSHD0.Text, _
                                Me.ddlSHD1.Text, _
                                Me.ddlSHD2.Text, _
                                Me.ddlSHD3.Text, _
                                Me.ddlSHD4.Text, _
                                Me.ddlSHD5.Text, _
                                Me.ddlSHD6.Text)
            res.AppendFormat("{0},{1},{2},{3},{4},{5},{6},", _
            AHD0, _
            AHD1, _
            AHD2, _
            AHD3, _
            AHD4, _
            AHD5, _
            AHD6)
            res.AppendFormat("{0},{1},{2},{3},{4},{5},{6},", _
            TD0, _
            TD1, _
            TD2, _
            TD3, _
            TD4, _
            TD5, _
            TD6, _
            txtScheduleChange.Text)
        Else
            'If it is Minutes or Hrs type, the values in the page need not be updated in the post attendance page
            'So, the return value is passed as empty
            res.AppendFormat("")

        End If
        Session("ClockAttendance") = res.ToString()
        js.Append("<script>" + vbCrLf)
        'Response.Write(res.ToString)
        js.Append("window.returnValue='" + res.ToString())
        js.Append("';" + vbCrLf)
        If boolScheduleChange = True Then
            js.Append("alert('Changes made to the schedule need to saved by clicking the SAVE button on the Post Attendance page');" + vbCrLf)
        End If
        js.Append("window.close();" + vbCrLf)
        js.Append("</script>" + vbCrLf)
        Me.ClientScript.RegisterClientScriptBlock(Page.GetType(), "onclick", js.ToString())
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub ShowAllPresentAbsentDropDownControls()
        Me.ddlSHD0.Visible = True
        Me.ddlSHD1.Visible = True
        Me.ddlSHD2.Visible = True
        Me.ddlSHD3.Visible = True
        Me.ddlSHD4.Visible = True
        Me.ddlSHD5.Visible = True
        Me.ddlSHD6.Visible = True

        Me.ddlAHD0.Visible = True
        Me.ddlAHD1.Visible = True
        Me.ddlAHD2.Visible = True
        Me.ddlAHD3.Visible = True
        Me.ddlAHD4.Visible = True
        Me.ddlAHD5.Visible = True
        Me.ddlAHD6.Visible = True

        Me.ddlMUHD0.Visible = True
        Me.ddlMUHD1.Visible = True
        Me.ddlMUHD2.Visible = True
        Me.ddlMUHD3.Visible = True
        Me.ddlMUHD4.Visible = True
        Me.ddlMUHD5.Visible = True
        Me.ddlMUHD6.Visible = True

        Me.ddlABHD0.Visible = True
        Me.ddlABHD1.Visible = True
        Me.ddlABHD2.Visible = True
        Me.ddlABHD3.Visible = True
        Me.ddlABHD4.Visible = True
        Me.ddlABHD5.Visible = True
        Me.ddlABHD6.Visible = True

        Me.chkT0.Visible = True
        Me.chkT1.Visible = True
        Me.chkT2.Visible = True
        Me.chkT3.Visible = True
        Me.chkT4.Visible = True
        Me.chkT5.Visible = True
        Me.chkT6.Visible = True
        Me.lblTardy.Visible = True

        DisableSchedule(Request.Params("shrs0").ToString(), Me.ddlSHD0)
        DisableSchedule(Request.Params("shrs1").ToString(), Me.ddlSHD1)
        DisableSchedule(Request.Params("shrs2").ToString(), Me.ddlSHD2)
        DisableSchedule(Request.Params("shrs3").ToString(), Me.ddlSHD3)
        DisableSchedule(Request.Params("shrs4").ToString(), Me.ddlSHD4)
        DisableSchedule(Request.Params("shrs5").ToString(), Me.ddlSHD5)
        DisableSchedule(Request.Params("shrs6").ToString(), Me.ddlSHD6)

        DisableAllDropDowns()
    End Sub
    Private Sub DisableAllDropDowns()
        Me.ddlAHD0.Enabled = False
        Me.ddlAHD1.Enabled = False
        Me.ddlAHD2.Enabled = False
        Me.ddlAHD3.Enabled = False
        Me.ddlAHD4.Enabled = False
        Me.ddlAHD5.Enabled = False
        Me.ddlAHD6.Enabled = False

        Me.ddlMUHD0.Enabled = False
        Me.ddlMUHD1.Enabled = False
        Me.ddlMUHD2.Enabled = False
        Me.ddlMUHD3.Enabled = False
        Me.ddlMUHD4.Enabled = False
        Me.ddlMUHD5.Enabled = False
        Me.ddlMUHD6.Enabled = False

        Me.ddlABHD0.Enabled = False
        Me.ddlABHD1.Enabled = False
        Me.ddlABHD2.Enabled = False
        Me.ddlABHD3.Enabled = False
        Me.ddlABHD4.Enabled = False
        Me.ddlABHD5.Enabled = False
        Me.ddlABHD6.Enabled = False

        Me.chkT0.Enabled = False
        Me.chkT1.Enabled = False
        Me.chkT2.Enabled = False
        Me.chkT3.Enabled = False
        Me.chkT4.Enabled = False
        Me.chkT5.Enabled = False
        Me.chkT6.Enabled = False
    End Sub
    Private Sub DisableSchedule(ByVal textSchedule As String, ByVal dropSchedule As DropDownList)
        Try
            If (textSchedule >= "1.00" Or CInt(textSchedule) >= 1) Then
                dropSchedule.SelectedValue = "1"
            Else
                dropSchedule.SelectedValue = "0"
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            dropSchedule.SelectedValue = "0"
        End Try
        'dropSchedule.Enabled = False
    End Sub
    Private Function ReturnUnitTypeById(ByVal ddlControl As DropDownList, ByVal Tardy As Integer) As String
        If ddlControl.SelectedValue = "1" And Tardy = 1 Then
            Return "T"
        ElseIf ddlControl.SelectedValue = "1" And Tardy = 0 Then
            Return "P"
        ElseIf ddlControl.SelectedValue = "0" Then
            Return "A"
        Else
            Return ""
        End If
    End Function
    Private Sub ShowHideByUnitType(ByVal boolShowHide As Boolean)
        Me.txtSHD0.Visible = boolShowHide
        Me.txtSHD1.Visible = boolShowHide
        Me.txtSHD2.Visible = boolShowHide
        Me.txtSHD3.Visible = boolShowHide
        Me.txtSHD4.Visible = boolShowHide
        Me.txtSHD5.Visible = boolShowHide
        Me.txtSHD6.Visible = boolShowHide

        Me.txtAHD0.Visible = boolShowHide
        Me.txtAHD1.Visible = boolShowHide
        Me.txtAHD2.Visible = boolShowHide
        Me.txtAHD3.Visible = boolShowHide
        Me.txtAHD4.Visible = boolShowHide
        Me.txtAHD5.Visible = boolShowHide
        Me.txtAHD6.Visible = boolShowHide

        Me.txtABHD0.Visible = boolShowHide
        Me.txtABHD1.Visible = boolShowHide
        Me.txtABHD2.Visible = boolShowHide
        Me.txtABHD3.Visible = boolShowHide
        Me.txtABHD4.Visible = boolShowHide
        Me.txtABHD5.Visible = boolShowHide
        Me.txtABHD6.Visible = boolShowHide

        Me.txtMUHD0.Visible = boolShowHide
        Me.txtMUHD1.Visible = boolShowHide
        Me.txtMUHD2.Visible = boolShowHide
        Me.txtMUHD3.Visible = boolShowHide
        Me.txtMUHD4.Visible = boolShowHide
        Me.txtMUHD5.Visible = boolShowHide
        Me.txtMUHD6.Visible = boolShowHide
    End Sub
    Private Function GetCheckedValueById(ByVal PassedValue As String) As Boolean
        Select Case PassedValue
            Case "1"
                Return True
            Case True
                Return True
            Case Else
                Return False
        End Select
    End Function
    'Private Function GetCheckedValue(ByVal PassedValue As String) As Boolean
    '    Select Case PassedValue
    '        Case "True"
    '            Return True
    '        Case Else
    '            Return False
    '    End Select
    'End Function
    'Private Function GetClockHourTardyValueById(ByVal ScheduledValue As Decimal, ByVal ActualValue As Decimal) As Boolean

    'End Function
    Public Function selectActualHoursBySchedule(ByVal ScheduleHours As String) As String
        Select Case ScheduleHours
            Case "1"
                Return "1"
            Case "0"
                Return "0"
            Case Else
                Return ""
        End Select
    End Function
    Private Function IsHoliday(ByVal hDate As Date) As Boolean
        ' If Session("Holidays") Is Nothing Then
        Session("Holidays") = (New HolidayFacade).GetAllHolidays(CampusId)
        ' End If
        'scan over all values of the holidays table
        Dim holidayTable As System.Data.DataTable = CType(Session("Holidays"), DataSet).Tables(0)
        For i As Integer = 0 To holidayTable.Rows.Count - 1
            If hDate >= holidayTable.Rows(i)("HolidayStartDate") And hDate <= holidayTable.Rows(i)("HolidayEndDate") Then
                Return True
            End If
        Next
        Return False
    End Function
    Protected Sub ddlSHD0_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSHD0.SelectedIndexChanged
        txtSDH0Changed.Text = ddlSHD0.SelectedValue
        'If IsHoliday(CType(lblD0.Text, Date)) Then
        '    txtHol0.Text = ddlSHD0.SelectedValue
        'End If
        boolScheduleChange = True
    End Sub
    Protected Sub ddlSHD1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSHD1.SelectedIndexChanged
        txtSDH1Changed.Text = ddlSHD1.SelectedValue
        'If IsHoliday(CType(lblD1.Text, Date)) Then
        '    txtHol1.Text = ddlSHD1.SelectedValue
        'End If
        boolScheduleChange = True
    End Sub
    Protected Sub ddlSHD2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSHD2.SelectedIndexChanged
        txtSDH2Changed.Text = ddlSHD2.SelectedValue
        'If IsHoliday(CType(lblD2.Text, Date)) Then
        '    txtHol2.Text = ddlSHD2.SelectedValue
        'End If
        boolScheduleChange = True
    End Sub
    Protected Sub ddlSHD4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSHD4.SelectedIndexChanged
        txtSDH4Changed.Text = ddlSHD4.SelectedValue
        'If IsHoliday(CType(lblD4.Text, Date)) Then
        '    txtHol4.Text = ddlSHD4.SelectedValue
        'End If
        boolScheduleChange = True
    End Sub
    Protected Sub ddlSHD6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSHD6.SelectedIndexChanged
        txtSDH6Changed.Text = ddlSHD6.SelectedValue
        'If IsHoliday(CType(lblD6.Text, Date)) Then
        '    txtHol6.Text = ddlSHD6.SelectedValue
        'End If
        boolScheduleChange = True
    End Sub
    Protected Sub ddlSHD5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSHD5.SelectedIndexChanged
        txtSDH5Changed.Text = ddlSHD5.SelectedValue
        'If IsHoliday(CType(lblD5.Text, Date)) Then
        '    txtHol5.Text = ddlSHD5.SelectedValue
        'End If
        boolScheduleChange = True
    End Sub
    Protected Sub ddlSHD3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSHD3.SelectedIndexChanged
        txtSDH3Changed.Text = ddlSHD3.SelectedValue
        'If IsHoliday(CType(lblD3.Text, Date)) Then
        '    txtHol3.Text = ddlSHD3.SelectedValue
        'End If
        boolScheduleChange = True
    End Sub

    'Protected Sub lbTimeClockpunches_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbTimeClockpunches.Click
    '    'Dim sb As New StringBuilder

    '    'sb.Append("&StuEnrollId=" + Request.Params("stuenrollid"))
    '    'sb.Append("&StuName=" + txtStudent.Text)
    '    'sb.Append("&Date=" + Request.Params("date"))
    '    'sb.Append("&SchHrs0=" + Request.Params("shrs0").ToString())
    '    'sb.Append("&SchHrs1=" + Request.Params("shrs1").ToString())
    '    'sb.Append("&SchHrs2=" + Request.Params("shrs2").ToString())
    '    'sb.Append("&SchHrs3=" + Request.Params("shrs3").ToString())
    '    'sb.Append("&SchHrs4=" + Request.Params("shrs4").ToString())
    '    'sb.Append("&SchHrs5=" + Request.Params("shrs5").ToString())
    '    'sb.Append("&SchHrs6=" + Request.Params("shrs6").ToString())


    '    ''   setup the properties of the new window
    '    'Dim winSettings As String = "resizable=yes,status=yes,dialogWidth=800px,dialogHeight=600px,dialogHide=true,help=no,scroll=yes,titlebar=no,toolbar=no,menubar=no,location=no"
    '    'Dim name As String = "TimeClockPunches"
    '    'Dim url As String = "../FA/" + name + ".aspx?" + sb.ToString
    '    ''window.showModalDialog(turl,null,'resizable:no;status:no;dialogWidth:650px;dialogHeight:290px;dialogHide:true;help:no;scroll:yes');
    '    ''open new window and pass parameters
    '    ''window.showModalDialog(turl,null,'resizable:no;status:no;dialogWidth:650px;dialogHeight:290px;dialogHide:true;help:no;scroll:yes');
    '    'CommonWebUtilities.OpenModalWindow(Page, url, name, winSettings)


    'End Sub


    Protected Sub btntimeclock_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btntimeclock.Click
        Dim sb As New StringBuilder
        'DE9544 5/6/2013 Janet Robinson Add flag field for disabled day
        sb.Append("&CampusId=" + Request.Params("CampusId"))
        sb.Append("&StuEnrollId=" + Request.Params("stuenrollid"))
        sb.Append("&StuName=" + txtStudent.Text)
        sb.Append("&Date=" + Request.Params("date"))
        sb.Append("&SchHrs0=" + Request.Params("shrs0").ToString())
        sb.Append("&SchHrs1=" + Request.Params("shrs1").ToString())
        sb.Append("&SchHrs2=" + Request.Params("shrs2").ToString())
        sb.Append("&SchHrs3=" + Request.Params("shrs3").ToString())
        sb.Append("&SchHrs4=" + Request.Params("shrs4").ToString())
        sb.Append("&SchHrs5=" + Request.Params("shrs5").ToString())
        sb.Append("&SchHrs6=" + Request.Params("shrs6").ToString())
        sb.Append("&Disabled0=" + Request.Params("idd0").ToString())
        sb.Append("&Disabled1=" + Request.Params("idd1").ToString())
        sb.Append("&Disabled2=" + Request.Params("idd2").ToString())
        sb.Append("&Disabled3=" + Request.Params("idd3").ToString())
        sb.Append("&Disabled4=" + Request.Params("idd4").ToString())
        sb.Append("&Disabled5=" + Request.Params("idd5").ToString())
        sb.Append("&Disabled6=" + Request.Params("idd6").ToString())


        '   setup the properties of the new window
        Dim winSettings As String = "resizable=yes,status=yes,dialogWidth=800px,dialogHeight=600px,dialogHide=true,help=no,scroll=yes,titlebar=no,toolbar=no,menubar=no,location=no"
        Dim name As String = "TimeClockPunches"
        Dim url As String = "../FA/" + name + ".aspx?" + sb.ToString
        'window.showModalDialog(turl,null,'resizable:no;status:no;dialogWidth:650px;dialogHeight:290px;dialogHide:true;help:no;scroll:yes');
        'open new window and pass parameters
        'window.showModalDialog(turl,null,'resizable:no;status:no;dialogWidth:650px;dialogHeight:290px;dialogHide:true;help:no;scroll:yes');
        CommonWebUtilities.OpenModalWindow(Page, url, name, winSettings)

    End Sub
End Class

