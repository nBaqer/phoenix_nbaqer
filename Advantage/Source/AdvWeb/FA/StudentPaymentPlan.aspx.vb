﻿Imports System.Activities.Statements
Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports FAME.Advantage.Common
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports FAME.DataAccessLayer
Imports FAME.Advantage.Reporting
Imports Telerik.Web.UI

Partial Class StudentPaymentPlan
    Inherits BasePage

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents txtStuEnrollId As System.Web.UI.WebControls.TextBox
    Protected WithEvents Linkbutton1 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents AcademicYearCompareValidator As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents PayAmountRangeValidator As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents lblAcademicYear As System.Web.UI.WebControls.Label
    Protected WithEvents lblAwdStartDate As System.Web.UI.WebControls.Label
    Protected WithEvents allDataset As System.Data.DataSet
    Protected WithEvents dataGridTable As System.Data.DataTable
    Protected WithEvents dataListTable As System.Data.DataTable
    Protected studentId As String
    Private campusId As String

    Private pObj As New UserPagePermissionInfo

    Private Amount As Decimal = 0.0
    Private NumberOfPayments As Integer = 0
    Protected boolSwitchCampus As Boolean = False
    Private OverallBalance As Decimal = 0.0




#Region " Web Form Designer Generated Code "



    Public Sub BtnExportToPdfClick(sender As Object, e As EventArgs)
        Try

            Dim getReportAsBytes As [Byte]()
            Dim payplanId As String = Session("PaymentPlanId").ToString

            Dim myAdvAppSett As AdvAppSettings = AdvAppSettings.GetAppSettings()

            Dim intSchoolOptions As Integer = CommonWebUtilities.SchoolSelectedOptions(CType(myAdvAppSett.AppSettings("SchedulingMethod"), String))
            Dim strReportPath As String = ConfigurationManager.AppSettings("Reports.ReportsFolder")
            Dim report = New Logic.StudentPaymentPlanReportGenerator()
            'jguirado Get from Configuration if the report is signed or not.
            Dim noSignature As Boolean = CType(myAdvAppSett.AppSettings("ReportPaymentPlanShowSignature"), Boolean)
            noSignature = Not noSignature

            getReportAsBytes = report.RenderReport("pdf", CType(Session("User"), String), payplanId, noSignature,
                                                                                      "", "",
                                                                                      "", "",
                                                                                      "",
                                                                                      strReportPath,
                                                                                      0,
                                                                                      0,
                                                                                      AdvantageSession.UserState.CampusId.ToString, intSchoolOptions)
            ExportReport("pdf", getReportAsBytes)

        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub btnNew_Click " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")

        End Try
    End Sub

    Private Sub ExportReport(exportFormat As String, getReportAsBytes As [Byte]())
        Dim strExtension, strMimeType As String
        Select Case exportFormat.ToLower
            Case Is = "pdf"
                strExtension = "pdf"
                strMimeType = "application/pdf"
                Exit Select
            Case "excel"
                strExtension = "xls"
                strMimeType = "application/vnd.excel"
                Exit Select
                'Case "WORD"
                '    strExtension = "doc"
                '    strMimeType = "application/vnd.ms-word"
            Case "csv"
                strExtension = "csv"
                strMimeType = "text/csv"
            Case Else
                Throw New Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.")
        End Select
        Session("SSRS_FileExtension") = strExtension
        Session("SSRS_MimeType") = strMimeType
        Session("SSRS_ReportOutput") = getReportAsBytes
        'CommonWebUtilities.OpenChildWindow(Page, URL, name, winSettings)
        Dim script = "window.open('../SY/DisplaySSRSReport.aspx','SSRSReport','resizable=yes,left=200px,top=200px,modal=no');"
        ScriptManager.RegisterStartupScript(Page, GetType(Page), "myscript", script, True)
    End Sub

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough> Private Sub InitializeComponent()
    End Sub


    Protected state As AdvantageSessionState
    Protected LeadId As String
    Dim resourceId As Integer
    Protected MyAdvAppSettings As AdvAppSettings
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(sender As System.Object, e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                studentId = Guid.Empty.ToString()
            Else
                studentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(studentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function

#End Region

#End Region

    Private Sub Page_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim objCommon As New CommonUtilities

        campusId = Master.CurrentCampusId

        '   disable History button the first time
        'Header1.EnableHistoryButton(False)

        'Get the StudentId from the state object associated with this page.
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(303) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If
        With objStudentState
            studentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)


        '   get the studentId fgrom the session
        'If CommonWebUtilities.IsValidGuid(strVID) Then
        '    If (state.Contains(strVID)) Then
        '        studentId = DirectCast(state(strVID), AdvantageStateInfo).StudentId
        '    Else
        '        'Send the user to error page requesting a login again...
        '        Session("Error") = "This is not a valid action. Please login to Advantage again."
        '        Response.Redirect("../ErrorPage.aspx")
        '    End If
        'Else
        '    'Send the user to error page requesting a login again...
        '    Session("Error") = "This is not a valid action. Please login to Advantage again."
        '    Response.Redirect("../ErrorPage.aspx")
        'End If

        'pObj = Header1.UserPagePermission

        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If
            'objCommon.PageSetup(Form1, "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"
            objCommon.SetCaptionsAndColorRequiredFields(Master.FindControl("ContentMain2"))

            '   build dropdownlists
            BuildDropDownLists()

            '   bind an empty new PaymentPlanInfo
            'BindPaymentPlanData(New PaymentPlanInfo)

            '   get allDataset from the DB
            '   bind datalist

            With New StuAwardsFacade
                allDataset = .GetPaymentPlansDS(Nothing, Nothing)
            End With

            '   save it on the session
            Session("AllDataset") = allDataset

            '   initialize buttons
            InitButtonsForLoad()

            '   hide datagrid
            dgrdDisbursements.Visible = False

            'When the page first loads we want to select the first enrollment in the list.
            'This will always be the second item in the list since the first is the "Select"
            If ddlStuEnrollId.Items.Count > 1 Then
                ddlStuEnrollId.SelectedIndex = 1
                ProcessEnrollmentSelected()
            End If
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, studentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, studentId, resourceId.ToString)
            'End If
            MyBase.uSearchEntityControlId.Value = ""
        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.SetCaptionsAndColorRequiredFields(Master.FindControl("ContentMain2"))
        End If

        '   create Dataset and Tables
        CreateDatasetAndTables()

        If Not IsPostBack Then
            '   the first time we have to bind an empty datagrid
            'PrepareForNewData()
        End If

        Me.lblDisbursements.Text = "Number of Payments*"

        'lnkDisbursements.Enabled = True


    End Sub
    Private Sub CreateDatasetAndTables()
        '   Get dataGrid Dataset from the session and create tables
        allDataset = Session("AllDataset")
        dataGridTable = allDataset.Tables("PaymentPlanSchedules")
        dataListTable = allDataset.Tables("PaymentPlans")
    End Sub
    Private Sub PrepareForNewData()
        '   bind PaymentPlans datalist
        BindDataList(ddlStuEnrollId.SelectedValue)
        'dlstPaymentPlan.DataSource = dataListTable
        'dlstPaymentPlan.DataBind()

        '   save PaymentPlanId in session
        Session("PaymentPlanId") = Nothing

        '   Bind DataGrid
        BindDataGrid()

        '   bind an empty new PaymentPlanInfo
        BindPaymentPlanData(New PaymentPlanInfo)

        '   initialize buttons
        InitButtonsForLoad()
    End Sub
    Private Sub BindDataList(stuEnrollId As String)
        '   bind PaymentPlans datalist
        dlstPaymentPlans.DataSource = (New StuAwardsFacade).GetAllPaymentPlans(stuEnrollId)
        dlstPaymentPlans.DataBind()
    End Sub
    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter As String = "StuEnrollId = '" & ddlStuEnrollId.SelectedValue & "'"
        Dim sortExpression = "PayPlanStartDate DESC"

        '   bind PaymentPlan datalist
        dlstPaymentPlans.DataSource = New DataView(dataListTable, rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstPaymentPlans.DataBind()

        If Not Session("PaymentPlanId") Is Nothing Then
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstPaymentPlans, Session("PaymentPlanId"), ViewState, Header1)
        End If
    End Sub
    Private Sub BuildDropDownLists()
        BuildStuEnrollDDL()
        'BuildAcademicYearDDL()

        'this is the list of ddls
        Dim ddlList = New List(Of AdvantageDDLDefinition)()

        'Countries DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlAcademicYearId, AdvantageDropDownListName.AcademicYears, campusId, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub
    Private Sub BuildStuEnrollDDL()
        '   bind the StuEnrollment DDL
        Dim transcript As New TranscriptFacade
        With ddlStuEnrollId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = transcript.GetEnrollment(studentId.ToUpper)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildAcademicYearDDL()
        Dim academicYears As New StudentsAccountsFacade
        With ddlAcademicYearId
            .DataTextField = "AcademicYearDescrip"
            .DataValueField = "AcademicYearId"
            .DataSource = academicYears.GetAllAcademicYears()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub GetPaymentPlanDS(paymentPlanId As String, voided As Boolean?)
        '   get allDataset from the DB
        Dim sFacade As New StuAwardsFacade
        allDataset = sFacade.GetPaymentPlansDS(paymentPlanId, voided)

        '   save it on the session
        Session("AllDataset") = allDataset

        '   create Dataset and Tables
        CreateDatasetAndTables()
    End Sub
    Private Sub dlstPaymentPlans_ItemCommand(source As System.Object, e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstPaymentPlans.ItemCommand
        '   this portion of code added to refresh data from the database

        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = resourceId
        Master.SetHiddenControlForAudit()
        '   save PaymentPlanId in session
        Dim strId As String = dlstPaymentPlans.DataKeys(e.Item.ItemIndex).ToString()
        Session("PaymentPlanId") = strId
        GetPaymentPlanDS(strId, False)

        '   get the row with the data to be displayed
        Dim row() As DataRow = dataListTable.Select("PaymentPlanId=" + "'" + CType(Session("PaymentPlanId"), String) + "'")
        'Dim a As Boolean = CType(Container.DataItem, System.Data.DataRowView).Row.RowState Or System.Data.DataRowState.Added + System.Data.DataRowState.Modified

        '   populate controls with row data
        txtPaymentPlanId.Text = CType(row(0)("PaymentPlanId"), Guid).ToString
        'txtStuEnrollId.Text = CType(row(0)("StuEnrollId"), Guid).ToString
        txtPayPlanDescrip.Text = row(0)("PayPlanDescrip")
        txtPayPlanStartDate.SelectedDate = CType(row(0)("PayPlanStartDate"), DateTime).ToShortDateString
        txtPayPlanEndDate.SelectedDate = CType(row(0)("PayPlanEndDate"), DateTime).ToShortDateString
        If (Not row(0)("AcademicYearId") Is System.DBNull.Value) Then
            'ddlAcademicYearId.SelectedValue = CType(row(0)("AcademicYearId"), Guid).ToString
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAcademicYearId, CType(row(0)("AcademicYearId"), Guid).ToString, CType(row(0)("AcademicYear"), String))
        Else
            ddlAcademicYearId.SelectedIndex = 0
        End If
        txtTotalAmountDue.Text = CType(row(0)("TotalAmountDue"), Decimal).ToString("#0.00")
        txtDisbursements.Text = row(0)("Disbursements")
        txtModUser.Text = row(0)("ModUser")
        txtModDate.Text = row(0)("ModDate").ToString

        '   Bind DataGrid
        dgrdDisbursements.EditItemIndex = -1
        BindDataGrid()

        '   show datagrid
        dgrdDisbursements.Visible = True

        '   show footer
        dgrdDisbursements.ShowFooter = True

        '   disable Disbursement link button
        lnkDisbursements.Enabled = False

        '   initialize buttons
        InitButtonsForEdit()

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstPaymentPlans, e.CommandArgument, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstPaymentPlans, strId)
    End Sub
    Private Sub BindPaymentPlanData(PaymentPlan As PaymentPlanInfo)
        With PaymentPlan
            chkIsInDB.Checked = .IsInDB
            txtPaymentPlanId.Text = .PaymentPlanId
            'txtStuEnrollId.Text = .StuEnrollId
            txtPayPlanDescrip.Text = .PayPlanDescrip

            If .PayPlanStartDate <> "" Then
                txtPayPlanStartDate.SelectedDate = .PayPlanStartDate
            Else
                txtPayPlanStartDate.SelectedDate = Nothing
            End If

            If .PayPlanEndDate <> "" Then
                txtPayPlanEndDate.SelectedDate = .PayPlanEndDate
            Else
                txtPayPlanEndDate.SelectedDate = Nothing
            End If

            'ddlAcademicYearId.SelectedValue = .AcademicYearId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAcademicYearId, .AcademicYearId, .AcademicYear)
            txtTotalAmountDue.Text = .TotalAmountDue.ToString("#0.00")
            txtDisbursements.Text = .Disbursements.ToString
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
    End Sub

    Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click

        '   validate that the number of rows in Disbursement grid equals the Number of Payments.
        If Not ValidateNumberOfDisbursements() Then
            '   Display Error Message
            DisplayErrorMessage("Unable to save. The number of rows must equal the Number of Payments.")
            Exit Sub
        End If

        '   validate that the sum of Projected Amounts equals the Net Loan Amount.
        If Not ValidateDisbursementAmounts() Then
            '   Display Error Message
            DisplayErrorMessage("Unable to save. The total Projected Amount must equal Total Amount Due.")
            Exit Sub
        End If

        '   if any of the footer fields of the datagrid is not blank.. send an error message 
        If Not AreFooterFieldsBlank() Then
            '   Display Error Message
            DisplayErrorMessage("To use ""Save"" all fields in the Payment footer must be blank.")
            Exit Sub
        End If

        '   if PaymentPlanId does not exist do an insert else do an update
        If Session("PaymentPlanId") Is Nothing Then
            '   do an insert
            BuildNewRowInDataList(Guid.NewGuid.ToString)

        Else
            '   do an update
            '   get the row with the data to be displayed
            Dim row() As DataRow = dataListTable.Select("PaymentPlanId=" + "'" + CType(Session("PaymentPlanId"), String) + "'")

            '   update row data
            UpdateRowData(row(0))

        End If

        '   try to update the DB and show any error message
        Dim strId As String = Session("PaymentPlanId").ToString()
        UpdateDB(strId, False, New StuAwardsFacade())

        '   if there were no errors then bind and set selected style to the datalist and
        '   initialize buttons 
        If Customvalidator1.IsValid Then

            '   bind datalist
            BindDataList(ddlStuEnrollId.SelectedValue)

            'dlstPaymentPlan.DataSource = New DataView(dataListTable, Nothing, "PaymentPlanDescrip asc", DataViewRowState.CurrentRows)
            'dlstPaymentPlan.DataBind()

            ''   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstPaymentPlan, Session("PaymentPlanId"), ViewState, Header1)

            '   initialize buttons
            InitButtonsForEdit()

            '   disable Disbursement link button
            lnkDisbursements.Enabled = False

        End If
        CommonWebUtilities.RestoreItemValues(dlstPaymentPlans, Session("PaymentPlanId"))
        FAME.AdvantageV1.BusinessFacade.Reports.RPTCommon.AlertAjax(Page, "Payment Plan was saved successfully", Me.Master.FindControl("RadAjaxManager1"))
    End Sub

    Private Function BuildPaymentPlanInfo(PaymentPlanId As String) As PaymentPlanInfo
        'instantiate class
        Dim PaymentPlanInfo As New PaymentPlanInfo

        With PaymentPlanInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get PaymentPlanId
            .PaymentPlanId = PaymentPlanId

            'stuEnrollId
            .StuEnrollId = ddlStuEnrollId.SelectedValue

            'PayPlanDescrip
            .PayPlanDescrip = Date.Parse(txtPayPlanDescrip.Text)

            'PayPlanStartDate
            .PayPlanStartDate = Date.Parse(txtPayPlanStartDate.SelectedDate)

            'PayPlanEndDate
            .PayPlanEndDate = Date.Parse(txtPayPlanEndDate.SelectedDate)

            'AcademicYearId
            .AcademicYearId = ddlAcademicYearId.SelectedValue

            'TotalAmountDue
            .TotalAmountDue = txtTotalAmountDue.Text

            'Disbursements
            .Disbursements = txtDisbursements.Text

            'ModUser
            .ModUser = txtModUser.Text

            'ModDate
            .ModDate = Date.Parse(txtModDate.Text)
        End With

        'return data
        Return PaymentPlanInfo
    End Function

    Private Sub btnNew_Click(sender As Object, e As System.EventArgs) Handles btnNew.Click
        '   reset dataset to previous state. delete all changes
        allDataset.RejectChanges()
        PrepareForNewData()

        '   hide datagrid
        dgrdDisbursements.Visible = False

        '   enable Disbursement link button
        'lnkDisbursements.Enabled = True

        'initialize buttons
        'InitButtonsForWork()
        InitButtonsForLoad()

        '   reset Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstPaymentPlans, Guid.Empty.ToString, ViewState, Header1)
        CommonWebUtilities.RestoreItemValues(dlstPaymentPlans, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As System.EventArgs) Handles btnDelete.Click
        '   try to update the DB and show any error message
        Dim strId As String = Session("PaymentPlanId").ToString()

        Dim hasPostedPayments As Boolean = False
        Dim facade As New StuAwardsFacade
        With facade
            If (Not String.IsNullOrEmpty(strId)) Then
                hasPostedPayments = facade.HasPaymentPlanPostedPayments(Guid.Parse(strId))
            End If
            If (hasPostedPayments) Then
                RadAjaxManager.GetCurrent(Page).ResponseScripts.Add("showWarningHasPostedPayments();")
            Else
                '   get all rows to be deleted
                Dim rows() As DataRow = dataGridTable.Select("PaymentPlanId=" + "'" + CType(Session("PaymentPlanId"), String) + "'")

                '   delete all rows from dataGrid table
                Dim i As Integer
                If rows.Length > 0 Then
                    For i = 0 To rows.Length - 1
                        rows(i).Delete()
                    Next
                End If

                '   get the row to be deleted in PaymentPlan table
                Dim row() As DataRow = dataListTable.Select("PaymentPlanId=" + "'" + CType(Session("PaymentPlanId"), String) + "'")

                '   delete row from the table
                row(0).Delete()
                Dim result As String = UpdateDB(strId, False, facade)

                If Not result = "" Then
                    '   Display Error Message
                    DisplayErrorMessage(result)
                Else
                    '   if there were no errors prepare screen for new data
                    If Customvalidator1.IsValid Then
                        '   Prepare screen for new data
                        PrepareForNewData()
                    End If

                    '   Hide Disbursements grid
                    dgrdDisbursements.Visible = False

                    '   initialize buttons
                    'InitButtonsForWork()
                    InitButtonsForLoad()
                    CommonWebUtilities.RestoreItemValues(dlstPaymentPlans, Guid.Empty.ToString)
                End If
            End If
        End With
    End Sub

    Private Sub InitButtonsForLoad()
        btnSave.Enabled = False

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
            lnkDisbursements.Enabled = True
        Else
            btnNew.Enabled = False
            lnkDisbursements.Enabled = False
        End If
        'btnNew.Enabled = False
        'lnkDisbursements.Enabled = False

        btnDelete.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        'btnNew.Enabled = True
        'btnDelete.Enabled = True
    End Sub

    Private Sub InitButtonsForWork()
        If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        'btnDelete.Enabled = False
        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        lnkDisbursements.Enabled = True

        'btnSave.Enabled = True
        'btnNew.Enabled = True
        'btnDelete.Enabled = False
    End Sub

    Private Sub GetPaymentPlanId(PaymentPlanId As String)
        '   bind PaymentPlan properties
        BindPaymentPlanData((New StuAwardsFacade).GetPaymentPlanInfo(PaymentPlanId))
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            'Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub

    Private Sub btnBuildList_Click(sender As Object, e As System.EventArgs) Handles btnBuildList.Click
        ProcessEnrollmentSelected()
    End Sub

    'Private Sub txtTotalAmountDue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTotalAmountDue.TextChanged
    '    If txtTotalAmountDue.Text <> "" Then
    '        txtTotalAmountDue.Text = Decimal.Parse(txtTotalAmountDue.Text).ToString("#0.00")
    '    Else
    '        txtTotalAmountDue.Text = Decimal.Parse(0.0).ToString("#0.00")
    '    End If
    '    Dim objCommon As New CommonWebUtilities
    '    objCommon.SetFocus(Me.Page, txtDisbursements)
    'End Sub

    Private Function AreRequiredFooterFieldsBlank() As Boolean
        Dim footer As Control = dgrdDisbursements.Controls(0).Controls(dgrdDisbursements.Controls(0).Controls.Count - 1)
        If Not CType(footer.FindControl("txtFooterExpectedDate"), TextBox).Text = "" And
            Not CType(footer.FindControl("txtFooterAmount"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Function AreFooterFieldsBlank() As Boolean
        Dim footer As Control = dgrdDisbursements.Controls(0).Controls(dgrdDisbursements.Controls(0).Controls.Count - 1)
        If Not CType(footer.FindControl("txtFooterExpectedDate"), TextBox).Text = "" Then Return False
        If Not CType(footer.FindControl("txtFooterReference"), TextBox).Text = "" Then Return False
        If Not CType(footer.FindControl("txtFooterAmount"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Function ValidateDisbursementAmounts() As Boolean
        '   validate that the sum of Projected Amounts equals the Net Loan Amount.
        Dim objAmount As Object = dataGridTable.Compute("SUM(Amount)", "PaymentPlanId = '" & CType(Session("PaymentPlanId"), String) & "'")

        If Not objAmount Is Nothing Then
            If Convert.ToDecimal(objAmount) <> Decimal.Parse(txtTotalAmountDue.Text) Then
                Return False
            End If
        Else
            Return False
        End If

        Return True
    End Function

    Private Function ValidateNumberOfDisbursements() As Boolean
        '   validate that the number of rows in grid equals the Number of Disbursements.
        Dim gridRows() As DataRow = dataGridTable.Select("PaymentPlanId = '" & CType(Session("PaymentPlanId"), String) & "'")

        If gridRows.Length <> Integer.Parse(txtDisbursements.Text) Then
            Return False
        End If

        Return True
    End Function

    Private Sub BuildNewRowInDataList(theGuid As String)
        '   get a new row
        Dim newRow As DataRow = dataListTable.NewRow

        '   create a Guid for DataList if it has not been created 
        Session("PaymentPlanId") = theGuid
        newRow("PaymentPlanId") = New Guid(theGuid)

        '   update row with web controls data
        UpdateRowData(newRow)

        '   add row to the table
        newRow.Table.Rows.Add(newRow)

    End Sub

    Private Sub UpdateRowData(row As DataRow)
        '   update row data
        'row("PaymentPlanId") = New Guid(CType(Session("PaymentPlanId"), String))
        row("StuEnrollId") = New Guid(ddlStuEnrollId.SelectedValue)
        row("PayPlanDescrip") = txtPayPlanDescrip.Text
        row("PayPlanStartDate") = Date.Parse(txtPayPlanStartDate.SelectedDate)
        row("PayPlanEndDate") = Date.Parse(txtPayPlanEndDate.SelectedDate)
        If Not ddlAcademicYearId.SelectedValue = System.Guid.Empty.ToString Then
            row("AcademicYearId") = New Guid(ddlAcademicYearId.SelectedValue)
        Else
            row("AcademicYearId") = System.DBNull.Value
        End If
        row("TotalAmountDue") = Decimal.Parse(txtTotalAmountDue.Text)
        row("Disbursements") = Integer.Parse(txtDisbursements.Text)
        row("ModUser") = Session("UserName") 'Session("UserId")
        row("ModDate") = Date.Parse(Date.Now.ToString)
    End Sub

    Private Function UpdateDB(paymentPlanId As String, voided As Boolean?, ByVal facade As StuAwardsFacade) As String
        '   update DB
        With facade
            Dim result As String = .UpdatePaymentPlansDS(allDataset)
            If Not result = "" Then
                '   Display Error Message
                'DisplayErrorMessage(result)
                Return result
            Else
                '   get a new version of PaymentPlans dataset because some other users may have added,
                '   updated or deleted records from the DB
                allDataset = .GetPaymentPlansDS(paymentPlanId, voided)
                Session("AllDataset") = allDataset
                CreateDatasetAndTables()
                Return ""
            End If
        End With
    End Function

    Private Sub dgrdDisbursements_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdDisbursements.ItemCommand
        '   process postbacks from the datagrid
        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                '   edit selected item
                dgrdDisbursements.EditItemIndex = e.Item.ItemIndex

                '    do not show footer
                dgrdDisbursements.ShowFooter = False

                '   user hit "Update" inside the datagrid
            Case "Update"
                Dim isValid As Byte = IsDataInEditTextboxesValid(e)
                Dim isBlank As Boolean = AreEditFieldsBlank(e)
                '   process only if the edit textboxes are nonblank and valid
                If isValid = 1 And Not isBlank Then
                    '   get the row to be updated
                    Dim row() As DataRow = dataGridTable.Select("PayPlanScheduleId=" + "'" + New Guid(CType(e.Item.FindControl("lblEditPayPlanScheduleId"), Label).Text).ToString + "'")
                    '   fill new values in the row
                    If Not CType(e.Item.FindControl("txtEditExpectedDate"), TextBox).Text = "" Then row(0)("ExpectedDate") = Date.Parse(CType(e.Item.FindControl("txtEditExpectedDate"), TextBox).Text)
                    row(0)("Reference") = CType(e.Item.FindControl("txtEditReference"), TextBox).Text
                    If Not CType(e.Item.FindControl("txtEditAmount"), TextBox).Text = "" Then row(0)("Amount") = Decimal.Parse(CType(e.Item.FindControl("txtEditAmount"), TextBox).Text)
                    '   no record is selected
                    dgrdDisbursements.EditItemIndex = -1

                    '   show footer
                    dgrdDisbursements.ShowFooter = True

                ElseIf isValid = 2 Then
                    DisplayErrorMessage("Invalid data in Scheduled Date during the edit of an existing row of Payments.")
                    Exit Sub

                ElseIf isValid = 3 Then
                    DisplayErrorMessage("Invalid data in Projected Amount during the edit of an existing row of Payments.")
                    Exit Sub

                ElseIf isBlank Then
                    DisplayErrorMessage("Scheduled Date and Projected Amount are required.")
                    Exit Sub
                End If


                '   user hit "Cancel"
            Case "Cancel"
                '   set no record selected
                dgrdDisbursements.EditItemIndex = -1

                '   show footer
                dgrdDisbursements.ShowFooter = True


                '   user hit "Delete" inside the datagrid
            Case "Delete"
                '   get the row to be deleted
                Dim row() As DataRow = dataGridTable.Select("PayPlanScheduleId=" + "'" + CType(e.Item.FindControl("lblEditPayPlanScheduleId"), Label).Text + "'")

                '   delete row 
                row(0).Delete()

                '   set no record selected
                dgrdDisbursements.EditItemIndex = -1

                '   show footer
                dgrdDisbursements.ShowFooter = True


            Case "AddNewRow"
                Dim isValid As Byte = IsDataInFooterTextboxesValid(e)
                Dim isBlank As Boolean = AreRequiredFooterFieldsBlank()
                '   process only if the footer textboxes are nonblank and valid
                If isValid = 1 And Not isBlank Then
                    '   If the dataList item doesn't have an Id assigned.. create one and assign it.
                    If Session("PaymentPlanId") Is Nothing Then
                        '   build a new row in dataList table
                        BuildNewRowInDataList(Guid.NewGuid.ToString)
                        '   bind datalist
                        BindDataList()
                    End If

                    '   get a new row from the dataGridTable
                    Dim newRow As DataRow = dataGridTable.NewRow

                    '   fill the new row with values
                    newRow("PayPlanScheduleId") = Guid.NewGuid
                    newRow("PaymentPlanId") = New Guid(CType(Session("PaymentPlanId"), String))
                    If Not CType(e.Item.FindControl("txtFooterExpectedDate"), TextBox).Text = "" Then newRow("ExpectedDate") = Date.Parse(CType(e.Item.FindControl("txtFooterExpectedDate"), TextBox).Text)
                    If Not CType(e.Item.FindControl("txtFooterReference"), TextBox).Text = "" Then newRow("Reference") = CType(e.Item.FindControl("txtFooterReference"), TextBox).Text
                    If Not CType(e.Item.FindControl("txtFooterAmount"), TextBox).Text = "" Then newRow("Amount") = Decimal.Parse(CType(e.Item.FindControl("txtFooterAmount"), TextBox).Text)

                    '   add row to the table
                    newRow.Table.Rows.Add(newRow)

                ElseIf isValid = 2 Then
                    DisplayErrorMessage("Invalid data in Scheduled Date during adding of a new row of Payments.")
                    Exit Sub

                ElseIf isValid = 3 Then
                    DisplayErrorMessage("Invalid data in Projected Amount during adding of a new row of Payments.")
                    Exit Sub

                ElseIf isBlank Then
                    DisplayErrorMessage("Scheduled Date and Projected Amount are required.")
                    Exit Sub
                End If
        End Select

        '   Bind DataGrid
        BindDataGrid()
    End Sub

    Private Sub BindDataGrid()

        dgrdDisbursements.DataSource = dataGridTable
        dgrdDisbursements.DataBind()

        'set number of payments and amount 
        txtTotalAmountDue.Text = Amount.ToString("#,###,###.00")
        txtDisbursements.Text = NumberOfPayments.ToString("##0")
        lblOverallBalance.Text = OverallBalance.ToString("c")

    End Sub

    Private Function AreEditFieldsBlank(e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not CType(e.Item.FindControl("txtEditExpectedDate"), TextBox).Text = "" And
            Not CType(e.Item.FindControl("txtEditAmount"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Function IsDataInFooterTextboxesValid(e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Byte
        If Not IsTextBoxValidDate(e.Item.FindControl("txtFooterExpectedDate")) Then Return 2
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtFooterAmount")) Then Return 3
        Return 1
    End Function

    Private Function IsTextBoxValidDecimal(textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True
        'Try
        'Dim d As Decimal = Decimal.Parse(textbox.Text, Globalization.NumberStyles.AllowDecimalPoint + Globalization.NumberStyles.AllowThousands)
        'Return True
        Dim d As Decimal
        Return Decimal.TryParse(textbox.Text, d)
        'Catch ex As System.Exception
         '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        'Return False
        'End Try
    End Function

    Private Function IsTextBoxValidDate(textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True
        Try
            Dim d As DateTime = Date.Parse(textbox.Text)
            Return True
            ''If DateTime.Compare(d, Date.Parse(txtPayPlanStartDate.Text)) >= 0 And _
            ''   DateTime.Compare(d, Date.Parse(txtPayPlanEndDate.Text)) <= 0 Then
            ''    ' d must be between PayPlanStartDate and PayPlanEndDate
            ''    Return True
            ''Else
            ''    Return False
            ''End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return False
        End Try
    End Function

    Private Function IsDataInEditTextboxesValid(e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Byte
        If Not IsTextBoxValidDate(e.Item.FindControl("txtEditExpectedDate")) Then Return 2
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtEditAmount")) Then Return 3
        Return 1
    End Function

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles MyBase.PreRender
        '   if there are changes to the dataset we must confirm that the user is aware that all changes will be lost.
        If allDataset.HasChanges() Then
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnNew.Attributes.Add("onclick", "if(confirm('All changes made to this record will be lost.  Are you sure you want a New record?')){}else{return false}")
        Else
            btnNew.Attributes.Remove("onclick")
        End If

        '   save dataset systems in session
        Session("AllDataset") = allDataset

        '
        '   Code moved from txtTotalAmountDue_TextChanged event.
        '   Format amount.
        If txtTotalAmountDue.Text <> "" Then
            txtTotalAmountDue.Text = Decimal.Parse(txtTotalAmountDue.Text).ToString("#0.00")
        Else
            txtTotalAmountDue.Text = Decimal.Parse(0.0).ToString("#0.00")
        End If
        'Dim objCommon As New CommonWebUtilities
        'objCommon.SetFocus(Me.Page, txtDisbursements)
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add buttons
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnBuildList)
        controlsToIgnore.Add(lnkDisbursements)
        controlsToIgnore.Add(txtPayPlanStartDate)
        controlsToIgnore.Add(txtPayPlanEndDate)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub

    Private Sub lnkDisbursements_Click(sender As Object, e As System.EventArgs) Handles lnkDisbursements.Click
        Dim newRow As DataRow = Nothing
        Dim i As Integer
        Dim accum As Decimal
        Dim projectedAmt As Decimal
        Dim disb As Integer = Integer.Parse(txtDisbursements.Text)

        dataGridTable.Rows.Clear()
        'For Each row As DataRow In dataGridTable.Rows
        '    'Mark all rows as deleted but do not remove them from the datatable yet.
        '    row.Delete()
        'Next

        If disb = 0 Then
            DisplayErrorMessage("Number of Payments must be greater than zero.")
            Exit Sub
        Else
            projectedAmt = Decimal.Round(Decimal.Parse(txtTotalAmountDue.Text) / disb, 2)
        End If

        '   if PaymentPlanId does not exist do an insert else do an update
        If Session("PaymentPlanId") Is Nothing Then
            '   do an insert
            BuildNewRowInDataList(Guid.NewGuid.ToString)

        Else
            '   do an update
            '   get the row with the data to be displayed
            Dim row() As DataRow = dataListTable.Select("PaymentPlanId=" + "'" + CType(Session("PaymentPlanId"), String) + "'")

            '   update row data
            UpdateRowData(row(0))
        End If

        Dim t1 As DateTime = Convert.ToDateTime(txtPayPlanEndDate.SelectedDate)
        Dim t2 As DateTime = Convert.ToDateTime(txtPayPlanStartDate.SelectedDate)
        Dim ddate As DateTime
        Dim dd As Integer = t2.Day
        Dim tmSpan = New TimeSpan(t1.Subtract(t2).Ticks)
        Dim startOnFirst As Boolean = MyAdvAppSettings.AppSettings("PaymentsOnFirstOfTheMonth")

        '   get time span and use it to compute payment's ExpectedDate.
        Dim nSpan As Integer = tmSpan.TotalDays / disb
        Dim remainingDays = 0.0
        For i = 0 To disb - 1
            newRow = dataGridTable.NewRow
            newRow("PayPlanScheduleId") = Guid.NewGuid
            newRow("PaymentPlanId") = New Guid(CType(Session("PaymentPlanId"), String))
            newRow("Amount") = projectedAmt
            accum += projectedAmt

            If startOnFirst Then

                Try
                    ddate = Convert.ToDateTime(t2.Month & "/" & dd & "/" & t2.Year)

                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    '   months can have 28, 29, 30 or 31 days
                    ddate = Convert.ToDateTime(t2.Month & "/" & DateTime.DaysInMonth(t2.Year, t2.Month) & "/" & t2.Year)

                Finally
                    '   cannot pass End Date
                    If ddate > t1 Then ddate = t1
                End Try

                newRow("ExpectedDate") = ddate

                If DateTime.Compare(t1, t2.AddMonths((nSpan + remainingDays) / 30)) > 0 Then
                    Dim months As Integer = (nSpan + remainingDays) / 30
                    t2 = t2.AddMonths(months)
                    remainingDays += nSpan - (months * 30)
                End If

            Else

                newRow("ExpectedDate") = t2
                t2 = t2.Add(New TimeSpan(nSpan, 0, 0, 0))
            End If

            newRow("Reference") = ""        '"Payment # " & (i + 1)
            '   add row to the table
            newRow.Table.Rows.Add(newRow)
        Next

        '   ensure that sum of projectedAmt equals Total Amount Due.
        If accum <> Decimal.Parse(txtTotalAmountDue.Text) Then
            newRow("Amount") += Decimal.Parse(txtTotalAmountDue.Text) - accum
        End If

        '   Bind DataGrid
        BindDataGrid()

        '   show datagrid
        dgrdDisbursements.Visible = True

        InitButtonsForWork()
    End Sub

    Private Sub ProcessEnrollmentSelected()
        If ddlStuEnrollId.SelectedValue <> System.Guid.Empty.ToString Then
            '   clear out session variable
            Session("PaymentPlanId") = Nothing

            '   bind datalist
            BindDataList(ddlStuEnrollId.SelectedValue)

            '   bind an empty new PaymentPlanInfo
            BindPaymentPlanData(New PaymentPlanInfo)

            '   Hide Disbursements grid
            dgrdDisbursements.Visible = False

            '   Initialize buttons
            'InitButtonsForWork()
            InitButtonsForLoad()

        Else
            '   Display Error Message
            DisplayErrorMessage("Please select an Enrollment.")
        End If
    End Sub
    Private Sub dgrdDisbursements_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdDisbursements.ItemDataBound
        Dim lbl = CType(e.Item.FindControl("lblWasReceived"), Label)
        Dim lblBalance = CType(e.Item.FindControl("lblBalance"), Label)

        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                If lbl.Text = "True" Then
                    CType(e.Item.FindControl("lnlButEdit"), LinkButton).Visible = False
                End If

                Dim amountTemp = CType(e.Item.DataItem("Amount"), Decimal)
                Amount += amountTemp
                NumberOfPayments += 1
                Dim receivedIsNull = IsDBNull(e.Item.DataItem("Received"))

                Try
                    If (receivedIsNull) Then
                        OverallBalance += amountTemp
                    Else
                        Dim received = CType(e.Item.DataItem("Received"), Decimal)
                        OverallBalance += amountTemp - If(received < 0, received * -1, received)
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    'Do nothing.
                End Try

                'Set the Balance field 
                Try
                    If (receivedIsNull) Then
                        lblBalance.Text = amountTemp.ToString("c")
                    Else
                        Dim receivedBalance = CType(e.Item.DataItem("Received"), Decimal)
                        lblBalance.Text = (amountTemp - If(receivedBalance < 0, receivedBalance * -1, receivedBalance)).ToString("c")
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    'Do nothing.
                End Try


                'Remove duplicate rows that have multiple payments to a single projected amount for header
                'Commented by Balaji on 2/1/2016 as TransNo field not set up while building the datatable and 
                'causing issues with adding a payment plan
                'If (e.Item.DataItem("TransNo") = 1) Then
                '    Amount += CType(e.Item.DataItem("Amount"), Decimal)
                '    NumberOfPayments += 1

                '    Try
                '        If (IsDBNull(e.Item.DataItem("Received"))) Then
                '            OverallBalance += (CType(e.Item.DataItem("Amount"), Decimal))
                '        Else
                '            OverallBalance += (CType(e.Item.DataItem("Amount"), Decimal) - CType(e.Item.DataItem("Received"), Decimal))
                '        End If
                '    Catch ex As Exception
                 '    	Dim exTracker = new AdvApplicationInsightsInitializer()
                '    	exTracker.TrackExceptionWrapper(ex)

                '        'Do nothing.
                '    End Try

                '    'If second payment is received for projected amount then only subtract the received amount for header
                'ElseIf (e.Item.DataItem("TransNo") = 2) Then
                '    Try
                '        OverallBalance -= CType(e.Item.DataItem("Received"), Decimal)
                '    Catch ex As Exception
                 '    	Dim exTracker = new AdvApplicationInsightsInitializer()
                '    	exTracker.TrackExceptionWrapper(ex)

                '        'Do nothing.
                '    End Try
                'End If

                'Set the Balance field in grid 
                'If (IsDBNull(e.Item.DataItem("Balance"))) Then
                '    lblBalance.Text = ""
                'Else
                '    lblBalance.Text = (CType(e.Item.DataItem("Balance"), Decimal).ToString("c"))
                'End If

            Case ListItemType.EditItem

                Amount += CType(e.Item.DataItem("Amount"), Decimal)
                NumberOfPayments += 1
                Try
                    If (IsDBNull(e.Item.DataItem("Received"))) Then
                        OverallBalance += (CType(e.Item.DataItem("Amount"), Decimal))
                    Else
                        OverallBalance += (CType(e.Item.DataItem("Amount"), Decimal) - CType(e.Item.DataItem("Received"), Decimal))
                    End If
                Catch ex As Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    'Do nothing.
                End Try



                'Remove duplicate rows that have multiple payments to a single projected amount for header
                'If (e.Item.DataItem("TransNo") = 1) Then
                '    Amount += CType(e.Item.DataItem("Amount"), Decimal)
                '    NumberOfPayments += 1

                '    Try
                '        If (IsDBNull(e.Item.DataItem("Received"))) Then
                '            OverallBalance += (CType(e.Item.DataItem("Amount"), Decimal))
                '        Else
                '            OverallBalance += (CType(e.Item.DataItem("Amount"), Decimal) - CType(e.Item.DataItem("Received"), Decimal))
                '        End If
                '    Catch ex As Exception
                 '    	Dim exTracker = new AdvApplicationInsightsInitializer()
                '    	exTracker.TrackExceptionWrapper(ex)

                '        'Do nothing.
                '    End Try

                '    'If second payment is received for projected amount then only subtract the received amount for header
                'ElseIf (e.Item.DataItem("TransNo") = 2) Then
                '    Try
                '        OverallBalance -= CType(e.Item.DataItem("Received"), Decimal)
                '    Catch ex As Exception
                 '    	Dim exTracker = new AdvApplicationInsightsInitializer()
                '    	exTracker.TrackExceptionWrapper(ex)

                '        'Do nothing.
                '    End Try
                'End If

        End Select
    End Sub


End Class
