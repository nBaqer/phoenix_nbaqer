﻿
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI
Imports BO = Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects

Partial Class PostExternships
    Inherits BasePage

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected StudentId, campusid As String
    Protected resourceId As Integer
    Private AllDataset As DataSet
    Private externshipAttendance As DataTable
    Private _completed As Decimal

    Protected state As AdvantageSessionState
    Protected LeadId As String = Guid.Empty.ToString
    Private pObj As New UserPagePermissionInfo
    Protected boolSwitchCampus As Boolean = False



    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub

#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                StudentId = Guid.Empty.ToString()
            Else
                StudentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(StudentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Dim objCommon As New CommonUtilities
        Dim advantageUserState As User = AdvantageSession.UserState

        'Get the StudentId from the state object associated with this page
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusid = AdvantageSession.UserState.CampusId.ToString

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusid)


        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(543) 'Pass resourceid so that user can be redirected to same page while swtiching students

        If objStudentState Is Nothing OrElse objStudentState.StudentId = Guid.Empty Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            StudentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With


        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        'Always disable the History button. It does not apply to this page.
        'Header1.EnableHistoryButton(False)
        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If
            BuildStudentEnrollmentsDDL(StudentId)

            InitializeData()
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, StudentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, StudentId, resourceId.ToString)
            'End If    
            MyBase.uSearchEntityControlId.Value = ""
        Else

        End If

        '   create dataset and tables
        CreateDatasetAndTables()

        'RadGrdPostExternships.MasterTableView.NoMasterRecordsText = "No maintenance item exists"
        'RadGrdPostExternships.MasterTableView.CommandItemSettings.AddNewRecordText = "Add new item"

        headerTitle.Text = Header.Title
    End Sub
    Private Sub BuildStudentEnrollmentsDDL(ByVal studentId As String)
        '   bind the StudentEnrollments DDL
        Dim studentEnrollments As New StudentsAccountsFacade
        With ddlEnrollmentId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = studentEnrollments.GetAllEnrollmentsPerStudent(studentId)
            .DataBind()
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Sub ddlEnrollmentId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEnrollmentId.SelectedIndexChanged
        InitializeData()
    End Sub

    Private Sub InitializeData()

        AllDataset = (New ExternshipAttendanceFacade).GetExternshipAttendanceDS(ddlEnrollmentId.SelectedValue)

        '   save it on the session
        Session("AllDataset") = AllDataset

        'Get Required Hours for Externships
        Dim str As String = (New ExternshipAttendanceFacade).GetRequiredHoursForExternships(ddlEnrollmentId.SelectedValue)
        If Not String.IsNullOrEmpty(str) Then
            Dim field() As String = str.Split(";")
            ViewState("ComponentType") = field(0)
            ViewState("Course") = field(1)
            ViewState("Required") = Decimal.Parse(field(2))
            ViewState("GrdComponentTypeId") = field(3)
            ViewState("GrdEnabled") = "true"
            'RadGrdPostExternships.Enabled = True
        Else
            ViewState("ComponentType") = "Component missing"
            ViewState("Course") = "Course missing"
            ViewState("Required") = 0.0
            ViewState("GrdEnabled") = "false"
            'RadGrdPostExternships.Enabled = False
        End If
        Dim str1 As String = (New ExternshipAttendanceFacade).GetCompletedHoursForExternships(ddlEnrollmentId.SelectedValue)
        If Not String.IsNullOrEmpty(str1) Then
            Dim field() As String = str1.Split(";")
            ViewState("Completed") = Decimal.Parse(field(2))
        End If

        AllDataset = Session("AllDataset")
        externshipAttendance = AllDataset.Tables("arExternshipAttendance")

        BuildGridData(ddlEnrollmentId.SelectedValue)

        CalculateCompletedHours()

    End Sub

    Private Sub CalculateCompletedHours()

        Dim PostExtensiondataset As DataSet
        Dim PostExternshiprow As DataRow
        _completed = 0
        PostExtensiondataset = Session("AllDataset")
        If Not PostExtensiondataset Is Nothing Then
            For Each PostExternshiprow In PostExtensiondataset.Tables("arExternshipAttendance").Rows
                _completed += PostExternshiprow("HoursAttended").ToString
            Next
        End If
    End Sub

    Private Sub CreateDatasetAndTables()
        '   Get PostExternshipss Dataset from the session and create tables
        AllDataset = Session("AllDataset")
        externshipAttendance = AllDataset.Tables("arExternshipAttendance")
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Session("AllDataset") = AllDataset
        BindToolTip()
    End Sub

    Protected Function Required() As Decimal
        Return ViewState("Required")
    End Function

    Protected Function Completed() As Decimal
        Return ViewState("Completed") + _completed
    End Function

    Protected Function Remaining() As Decimal
        Remaining = ViewState("Required") - Completed()
        If Remaining < 0.0 Then Return 0.0
    End Function

    Protected Function Course() As String
        Return "Course: " + ViewState("Course")
    End Function

    Protected Function ComponentType() As String
        Return "Component: " + ViewState("ComponentType")
    End Function

    Private Sub BuildGridData(Optional ByVal stuEnrollId As String = "")
        RadGrdPostExternships.DataSource = New DataView(externshipAttendance, Nothing, "AttendedDate desc", DataViewRowState.CurrentRows)
        RadGrdPostExternships.DataBind()
    End Sub

    Protected Sub RadGrdPostExternships_DeleteCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdPostExternships.DeleteCommand

        Dim deletedItem As GridEditableItem = DirectCast(e.Item, GridEditableItem)
        Dim AttendanceID As String = deletedItem.GetDataKeyValue("ExternshipAttendanceId").ToString
        '   get the row to be deleted
        Dim rows As DataRow() = externshipAttendance.Select("ExternshipAttendanceId='" + AttendanceID + "'")

        '   delete row 
        rows(0).Delete()

        UpdateExternshipsRoutine()

    End Sub
    Protected Sub RadGrdPostExternships_UpdateCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdPostExternships.UpdateCommand

        Dim editedItem As GridEditableItem = DirectCast(e.Item, GridEditableItem)

        Dim AttendanceID As String = editedItem.GetDataKeyValue("ExternshipAttendanceId").ToString
        '  get the row to be updated
        Dim rows As DataRow() = externshipAttendance.Select("ExternshipAttendanceId='" + AttendanceID + "'")

        '   fill new values in the row
        rows(0)("AttendedDate") = TryCast(editedItem("AttendedDate").Controls(1), RadDatePicker).DbSelectedDate
        rows(0)("HoursAttended") = TryCast(editedItem("HoursAttended").Controls(1), TextBox).Text
        rows(0)("Comments") = TryCast(editedItem("Comments").Controls(1), TextBox).Text
        rows(0)("ModUser") = AdvantageSession.UserState.UserName
        rows(0)("ModDate") = Date.Now

        UpdateExternshipsRoutine()

    End Sub
    Protected Sub RadGrdPostExternships_InsertCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdPostExternships.InsertCommand

        Dim newItem As GridEditableItem = DirectCast(e.Item, GridEditableItem)
        Dim newRow As DataRow = externshipAttendance.NewRow()

        '   fill the new row with values
        newRow("ExternshipAttendanceId") = Guid.NewGuid
        newRow("StuEnrollId") = New Guid(ddlEnrollmentId.SelectedValue)
        newRow("GrdComponentTypeId") = New Guid(CType(ViewState("GrdComponentTypeId"), String))
        newRow("AttendedDate") = TryCast(newItem("AttendedDate").Controls(1), RadDatePicker).DbSelectedDate
        newRow("HoursAttended") = TryCast(newItem("HoursAttended").Controls(1), TextBox).Text
        newRow("Comments") = TryCast(newItem("Comments").Controls(1), TextBox).Text
        newRow("ModUser") = AdvantageSession.UserState.UserName
        newRow("ModDate") = Date.Now
        '   add row to the table
        newRow.Table.Rows.Add(newRow)
        e.Canceled = True
        RadGrdPostExternships.MasterTableView.IsItemInserted = False
        UpdateExternshipsRoutine()
        BuildGridData(ddlEnrollmentId.SelectedValue)

    End Sub
    Protected Sub RadGrdPostExternships_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrdPostExternships.NeedDataSource
        Try
            RadGrdPostExternships.DataSource = New DataView(externshipAttendance, Nothing, "AttendedDate desc", DataViewRowState.CurrentRows)
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            RadGrdPostExternships.DataSource = Nothing
        End Try
    End Sub
    Protected Sub RadGrdPostExternships_PreRender(sender As Object, e As System.EventArgs) Handles RadGrdPostExternships.PreRender
        AdvantageSession.PageTheme = PageTheme.Blue_Theme
    End Sub
    Protected Sub RadGrdPostExternships_Load(sender As Object, e As System.EventArgs) Handles RadGrdPostExternships.Load
        CalculateCompletedHours()
    End Sub
    Protected Sub RadGrdPostExternships_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles RadGrdPostExternships.ItemCommand

        If e.CommandName = RadGrid.InitInsertCommandName Then '"Add new" button clicked
            e.Canceled = True
            Dim newValues As System.Collections.Specialized.ListDictionary = New System.Collections.Specialized.ListDictionary()
            Dim gridEditFormItem As GridCommandItem = DirectCast(e.Item, GridCommandItem)
            newValues("AttendedDate") = ""   ' Date.Now
            newValues("HoursAttended") = ""
            newValues("Comments") = ""
            newValues("ExternshipAttendanceId") = Guid.NewGuid
            e.Item.OwnerTableView.InsertItem(newValues)
        End If


    End Sub
    Private Sub DisableGridLink(ByVal e As Telerik.Web.UI.GridItemEventArgs,
                                ByVal sCmdName As String,
                                ByVal sDesc As String)

        Dim sToolTip As String = "User does not have the permission to " & sDesc & " record"
        Dim cell As TableCell = CType(e.Item, GridDataItem)(sCmdName)
        Dim lnk As ImageButton = CType(cell.Controls(0), ImageButton)

        'lnk.Enabled = False
        'lnk.ToolTip = sToolTip
        lnk.Visible = False
        cell.ToolTip = sToolTip


    End Sub

    Private Sub DisableGridAdd(ByVal e As Telerik.Web.UI.GridItemEventArgs)

        Dim cmditm As GridCommandItem = DirectCast(e.Item, GridCommandItem)
        Dim btn1 As Button = DirectCast(cmditm.FindControl("btn1"), Button)

        Try
            'btn1.Enabled = False
            btn1.Visible = False
            btn1.ToolTip = "User does not have permission to add new record"
            Dim lnkbtn1 As LinkButton = DirectCast(cmditm.FindControl("linkbuttionInitInsert"), LinkButton)
            'lnkbtn1.Enabled = False
            lnkbtn1.ToolTip = "User does not have permission to add new record"
            lnkbtn1.Visible = False
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try

    End Sub
    Protected Sub RadGrdPostExternships_ItemDataBound(sender As Object, e As Telerik.Web.UI.GridItemEventArgs) Handles RadGrdPostExternships.ItemDataBound

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(AdvantageSession.UserState, resourceId, campusid)

        If pObj.HasAdd = True Then
            RadGrdPostExternships.AllowAutomaticInserts = True
        ElseIf pObj.HasAdd = False Then
            If TypeOf e.Item Is GridCommandItem Then
                DisableGridAdd(e)
            End If
        End If
        If ViewState("GrdEnabled") = "false" Then
            If TypeOf e.Item Is GridCommandItem Then
                DisableGridAdd(e)
            End If
            If TypeOf e.Item Is GridDataItem Then
                DisableGridLink(e, "EditCommandColumn", "edit")
                DisableGridLink(e, "DeleteColumn", "delete")
            End If
        Else
            If pObj.HasEdit = True Then
                RadGrdPostExternships.AllowAutomaticUpdates = True
            ElseIf pObj.HasEdit = False And pObj.HasAdd = False Then

                If TypeOf e.Item Is GridDataItem Then
                    DisableGridLink(e, "EditCommandColumn", "edit")
                End If
            ElseIf pObj.HasEdit = False And pObj.HasAdd = True Then
                If TypeOf e.Item Is GridDataItem And e.Item.OwnerTableView.IsItemInserted = False Then
                    DisableGridLink(e, "EditCommandColumn", "edit")
                End If
            End If
            If pObj.HasDelete = False Then
                If TypeOf e.Item Is GridDataItem Then
                    DisableGridLink(e, "DeleteColumn", "delete")
                End If
            End If
        End If

        'If (TypeOf e.Item Is GridDataItem) Then
        '    Dim gridFormItem As GridDataItem = DirectCast(e.Item, GridDataItem)
        '    If TypeOf e.Item Is GridDataItem And Not TypeOf e.Item Is GridDataInsertItem Then
        '        _completed += e.Item.DataItem("HoursAttended")
        '    End If
        'End If


        If ((pObj.HasAdd = True) And (pObj.HasDelete = False And pObj.HasDisplay = False And pObj.HasEdit = False And pObj.HasFull = False And pObj.HasNone = False)) Then
            For Each item As GridDataItem In RadGrdPostExternships.Items
                If TypeOf item Is GridDataItem Then
                    'DisableGridLink(e, "EditCommandColumn", "edit")
                    'DisableGridLink(e, "DeleteColumn", "delete")
                    Dim editcell As TableCell = CType(item, GridDataItem)("EditCommandColumn")
                    Dim editlnk As ImageButton = CType(editcell.Controls(0), ImageButton)
                    editlnk.Visible = False
                    editcell.ToolTip = "User does not have the permission to edit records"
                    Dim deletecell As TableCell = CType(item, GridDataItem)("DeleteColumn")
                    Dim deletelnk As ImageButton = CType(deletecell.Controls(0), ImageButton)
                    deletelnk.Visible = False
                    deletecell.ToolTip = "User does not have the permission to delete records"
                End If
            Next
        End If

        If ((pObj.HasAdd = True And pObj.HasDelete = True) And (pObj.HasDisplay = False And pObj.HasEdit = False And pObj.HasFull = False And pObj.HasNone = False)) Then
            For Each item As GridDataItem In RadGrdPostExternships.Items
                If TypeOf item Is GridDataItem Then
                    'DisableGridLink(e, "EditCommandColumn", "edit")
                    Dim editcell As TableCell = CType(item, GridDataItem)("EditCommandColumn")
                    Dim editlnk As ImageButton = CType(editcell.Controls(0), ImageButton)
                    editlnk.Visible = False
                    editcell.ToolTip = "User does not have the permission to edit records"
                End If
            Next
        End If

    End Sub

    Private Sub UpdateExternshipsRoutine()

        'apply changes
        Dim externshipFacade As New ExternshipAttendanceFacade
        Dim result As String = externshipFacade.UpdateExternshipAttendanceDS(AllDataset, AdvantageSession.UserState.UserName)
        If Not result = "" Then
            '   Display Error Message
            DisplayErrorMessage(result)
            Exit Sub
        Else
            'If student status is mapped to currently attending or academic probation, check if there exist a status mapped to 
            'externship system status and if so, update the student enrollment status to the status mapped to externship.
            Dim strStatusCodeId As String = ""
            Try
                strStatusCodeId = (New ExternshipAttendanceFacade).GetExternshipStatusCode(campusid)
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                strStatusCodeId = ""
            End Try

            If Not strStatusCodeId = "" Then
                Dim strExternShipStatusDescrip As String = externshipFacade.UpdateStudentEnrollmentStatusWithExternshipStatus(ddlEnrollmentId.SelectedValue, campusid, strStatusCodeId)
            End If

            RadGrdPostExternships.MasterTableView.ClearEditItems()
            RadGrdPostExternships.Rebind()
        End If
        CalculateCompletedHours()
        SetIsCourseCompleted(ddlEnrollmentId.SelectedValue)
    End Sub
    Private Sub SetIsCourseCompleted(ByVal StuEnrollid As String)
        Dim externshipFacade As New ExternshipAttendanceFacade
        externshipFacade.SetIsCourseCompleted(StuEnrollid)
    End Sub
#Region "CommentedProcedure"

    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    'apply changes
    '    Dim externshipFacade As New ExternshipAttendanceFacade
    '    'Dim result As String = externshipFacade.UpdateExternshipAttendanceDS(AllDataset, Session("UserName"))
    '    Dim result As String = externshipFacade.UpdateExternshipAttendanceDS(AllDataset, AdvantageSession.UserState.UserName)

    '    If Not result = "" Then
    '        '   Display Error Message
    '        DisplayErrorMessage(result)
    '        Exit Sub
    '    Else
    '        'If student status is mapped to currently attending or academic probation, check if there exist a status mapped to 
    '        'externship system status and if so, update the student enrollment status to the status mapped to externship.
    '        Dim strStatusCodeId As String = ""
    '        Try
    '            strStatusCodeId = (New ExternshipAttendanceFacade).GetExternshipStatusCode(campusid)
    '        Catch ex As System.Exception
    '        	Dim exTracker = new AdvApplicationInsightsInitializer()
    '        	exTracker.TrackExceptionWrapper(ex)

    '            strStatusCodeId = ""
    '        End Try

    '        If Not strStatusCodeId = "" Then
    '            Dim strExternShipStatusDescrip As String = externshipFacade.UpdateStudentEnrollmentStatusWithExternshipStatus(ddlEnrollmentId.SelectedValue, campusid, strStatusCodeId)
    '            'If Not strExternShipStatusDescrip = "" Then
    '            '    CType(Header1.FindControl("txtStatusCode"), TextBox).Text = strExternShipStatusDescrip
    '            'End If
    '        End If

    '        BuildGridData(ddlEnrollmentId.SelectedValue)
    '        'BindDatagrid(ddlEnrollmentId.SelectedValue)
    '    End If
    'End Sub

    'Private Function GetRowGuid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As String
    '    Dim externshipAttendanceId As String = CType(e.Item.FindControl("tbExternshipAttendanceId"), TextBox).Text
    '    Return externshipAttendanceId
    'End Function

    'Private Sub InitButtonsForLoad()
    '    'If dgrdPostExternships.Enabled = False Then
    '    '    btnSave.Enabled = False
    '    'Else
    '    If pObj.HasFull Or pObj.HasAdd Then
    '        ' btnSave.Enabled = True
    '        'dgrdPostExternships.Enabled = False
    '    Else
    '        'btnSave.Enabled = False
    '        'dgrdPostExternships.Enabled = True
    '    End If
    '    'End If
    '    'btnNew.Enabled = False
    '    'btnDelete.Enabled = False
    'End Sub

    'Private Sub InitButtonsForEdit()
    '    btnSave.Enabled = False
    '    btnNew.Enabled = False
    '    btnDelete.Enabled = False
    'End Sub
    'Private Sub BindDatagrid(ByVal stuEnrollId As String)
    '    'bind repeater
    '    dgrdPostExternships.DataSource = New DataView(externshipAttendance, Nothing, "AttendedDate desc", DataViewRowState.CurrentRows)
    '    dgrdPostExternships.DataBind()
    'End Sub

    'Protected Sub dgrdPostExternships_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdPostExternships.ItemCommand
    '    '   process postbacks from the datagrid
    '    Select Case e.CommandName

    '        '   user hit "Edit"
    '        Case "Edit"
    '            '   edit selected item
    '            dgrdPostExternships.EditItemIndex = e.Item.ItemIndex

    '            '    do not show footer
    '            dgrdPostExternships.ShowFooter = False

    '            '   disable save button
    '            btnSave.Enabled = False

    '            '   user hit "Update" inside the datagrid
    '        Case "Update"

    '            If Not ValidateEditFields(e) Then
    '                '   Display Error Message
    '                DisplayErrorMessage("Invalid data")
    '                Exit Sub
    '            End If

    '            '   get the row to be updated
    '            Dim rows As DataRow() = externshipAttendance.Select("ExternshipAttendanceId='" + GetRowGuid(e) + "'")

    '            '   fill new values in the row
    '            'rows(0)("AttendedDate") = CType(e.Item.FindControl("tbEditAttendedDate"), TextBox).Text
    '            rows(0)("AttendedDate") = CType(e.Item.FindControl("tbEditAttendedDate"), RadDatePicker).DbSelectedDate
    '            rows(0)("HoursAttended") = CType(e.Item.FindControl("tbEditHoursAttended"), TextBox).Text
    '            rows(0)("Comments") = CType(e.Item.FindControl("tbEditComments"), TextBox).Text
    '            'rows(0)("ModUser") = Session("UserName")
    '            rows(0)("ModUser") = AdvantageSession.UserState.UserName
    '            rows(0)("ModDate") = Date.Now

    '            'Prepare dataGrid
    '            PrepareDatagrid()

    '            '   user hit "Cancel"
    '        Case "Cancel"

    '            'Prepare dataGrid
    '            PrepareDatagrid()

    '            '   user hit "Delete" inside the datagrid
    '        Case "Delete"

    '            '   get the row to be deleted
    '            Dim rows As DataRow() = externshipAttendance.Select("ExternshipAttendanceId='" + GetRowGuid(e) + "'")

    '            '   delete row 
    '            rows(0).Delete()

    '            'prepare DataGrid
    '            PrepareDatagrid()

    '        Case "AddNewRow"
    '            If Not ValidateFooter() Then
    '                '   Display Error Message
    '                DisplayErrorMessage("Invalid data")
    '                Exit Sub
    '            End If

    '            '   get a new row 
    '            Dim newRow As DataRow = externshipAttendance.NewRow()

    '            '   fill the new row with values
    '            newRow("ExternshipAttendanceId") = Guid.NewGuid
    '            newRow("StuEnrollId") = New Guid(ddlEnrollmentId.SelectedValue)
    '            newRow("GrdComponentTypeId") = New Guid(CType(ViewState("GrdComponentTypeId"), String))
    '            newRow("AttendedDate") = CType(e.Item.FindControl("tbAttendedDate"), RadDatePicker).DbSelectedDate
    '            newRow("HoursAttended") = CType(e.Item.FindControl("tbHoursAttended"), TextBox).Text
    '            newRow("Comments") = CType(e.Item.FindControl("tbComments"), TextBox).Text
    '            'newRow("ModUser") = Session("UserName")
    '            newRow("ModUser") = AdvantageSession.UserState.UserName
    '            newRow("ModDate") = Date.Now

    '            '   add row to the table
    '            newRow.Table.Rows.Add(newRow)

    '    End Select

    '    '   bind datagrid to PostExternships table 
    '    BindDatagrid(ddlEnrollmentId.SelectedValue)
    'End Sub

    'Private Sub PrepareDatagrid()
    '    '   set no record selected
    '    dgrdPostExternships.EditItemIndex = -1

    '    '   show footer
    '    dgrdPostExternships.ShowFooter = False

    '    '   save button must be enabled
    '    btnSave.Enabled = True
    '    'InitButtonsForLoad()
    'End Sub

    'Private Sub SetHeaderVisibility(ByVal visible As Boolean)
    '    Dim footer As Control = dgrdPostExternships.Controls(0).Controls(0)
    '    'CType(footer.FindControl("tbAttendedDate"), WebControl).Visible = visible
    '    'CType(footer.FindControl("CalButton1"), HtmlControl).Visible = visible
    '    'CType(footer.FindControl("btnAddRow"), WebControl).Visible = visible
    '    'CType(footer.FindControl("tbHoursAttended"), WebControl).Visible = visible

    '    'CType(footer.FindControl("btnAddRow"), WebControl).Visible = visible
    '    'CType(footer.FindControl("tbHoursAttended"), WebControl).Visible = visible
    '    'CType(footer.FindControl("tbAttendedDate"), RadDatePicker).Visible = visible
    'End Sub

    'Private Function ValidateFooter() As Boolean
    '    If AreFooterFieldsBlank() Then Return False
    '    Dim footer As Control = dgrdPostExternships.Controls(0).Controls(0)
    '    If Not IsDate(CType(footer.FindControl("tbAttendedDate"), RadDatePicker).DbSelectedDate) Then Return False
    '    If Not Decimal.TryParse(CType(footer.FindControl("tbHoursAttended"), TextBox).Text, 0.0) Then Return False
    '    Return True
    'End Function

    'Private Function AreFooterFieldsBlank() As Boolean
    '    Dim footer As Control = dgrdPostExternships.Controls(0).Controls(0)
    '    If Not IsDate(CType(footer.FindControl("tbAttendedDate"), RadDatePicker).DbSelectedDate) Then Return False
    '    If Not CType(footer.FindControl("tbHoursAttended"), TextBox).Text = "" Then Return False
    '    Return True
    'End Function

    'Private Function ValidateEditFields(ByVal e As DataGridCommandEventArgs) As Boolean
    '    'must contain a valid date and a valid decimal
    '    'Return IsDate(CType(e.Item.FindControl("tbEditAttendedDate"), TextBox).Text) And Decimal.TryParse(CType(e.Item.FindControl("tbEditHoursAttended"), TextBox).Text, 0.0)
    '    'IsDate(CType(txtDOB, RadDatePicker).DbSelectedDate)
    '    Return IsDate(CType(e.Item.FindControl("tbEditAttendedDate"), RadDatePicker).DbSelectedDate) And Decimal.TryParse(CType(e.Item.FindControl("tbEditHoursAttended"), TextBox).Text, 0.0)
    'End Function

    'Protected Sub dgrdPostExternships_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdPostExternships.ItemDataBound
    '    Select Case e.Item.ItemType
    '        Case ListItemType.Header
    '            SetHeaderVisibility(True)
    '        Case ListItemType.EditItem
    '            'Dim tb As TextBox = CType(e.Item.FindControl("tbEditAttendedDate"), TextBox)
    '            'Dim cal As HtmlControl = e.Item.FindControl("CalButton1")
    '            'cal.Attributes("onclick") = "javascript:OpenCalendar('Form1','" + tb.ClientID + "', true, 1945)"
    '            'SetHeaderVisibility(False)
    '            '_completed += e.Item.DataItem("HoursAttended")
    '        Case ListItemType.Item, ListItemType.AlternatingItem
    '            _completed += e.Item.DataItem("HoursAttended")
    '    End Select
    'End Sub
    'Private Function GetTableIndex(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Integer
    '    Dim externshipAttendanceId As Guid = New Guid(CType(e.Item.FindControl("tbExternshipAttendanceId"), TextBox).Text)
    '    Dim rows() As DataRow = externshipAttendance.Select(Nothing, Nothing, DataViewRowState.CurrentRows)
    '    For i As Integer = 0 To rows.Length - 1
    '        Dim row As DataRow = rows(i)
    '        If row("ExternshipAttendanceId") = externshipAttendanceId Then Return i
    '    Next
    'End Function
#End Region

End Class
