﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentAwards.aspx.vb" Inherits="StudentAwards" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>


            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="listframetop2">
                        <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                            <tr>
                                <td class="twocolumnlabelcell" align="left" style="padding-left: 9px;">
                                    <asp:Label ID="lblEnrollments" runat="server" Width="100%" CssClass="label">Enrollment</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="twocolumncontentcell">
                                    <asp:DropDownList ID="ddlStuEnrollId" Width="330px" runat="server" CssClass="dropdownlist">
                                    </asp:DropDownList>
                                </td>
                            </tr>


                            <tr>
                                <td class="twocolumncontentcell">
                                    <br />
                                    <asp:Button ID="btnBuildList" runat="server" Text="Build List"
                                        CausesValidation="False"></asp:Button>
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfltr2rows">
                            <asp:DataList ID="dlstStudentAwards" runat="server" DataKeyField="StudentAwardId">
                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="Linkbutton1" CssClass="itemstyle" CausesValidation="False" runat="server"
                                        CommandArgument='<%# Container.DataItem("StudentAwardId")%>' Text='<%# Container.DataItem("AwardTypeDescrip")%>'>
                                    </asp:LinkButton>
                                    <asp:Label ID="lblAwdStartDate" runat="server" CssClass="itemstyle" Text='<%# "(" + Container.DataItem("AwardStartDate")+ ")" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>



        </telerik:RadPane>


        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                            ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->


                            <table width="60%" align="center" border="0">
                                <asp:TextBox ID="txtStudentAwardId" runat="server" Visible="False"></asp:TextBox>
                                <asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox>
                                <asp:TextBox ID="txtStudentId" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox>
                                <asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblAwardTypeId" runat="server" CssClass="label">Fund Source</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:DropDownList ID="ddlAwardTypeId" runat="server" CssClass="dropdownlist" Width="200px" 
                                            TabIndex="1">
                                        </asp:DropDownList>
                                        <asp:CompareValidator ID="AwardTypeCompareValidator" runat="server" ErrorMessage="Must Select a Fund Source"
                                            Display="None" ControlToValidate="ddlAwardTypeId" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select a Fund Source</asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblAwardStartDate" runat="server" CssClass="label">Award Start</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">

                                        <telerik:RadDatePicker ID="txtAwardStartDate" runat="server" MinDate="1/1/2001" Width="200px" >
                                        </telerik:RadDatePicker>
                                        <asp:RangeValidator ID="drvAwardStartDate" runat="server" ControlToValidate="txtAwardStartDate"
                                            ErrorMessage="Date is invalid or outside allowed range" Display="None" Type="Date"
                                            MaximumValue="2049/12/31" MinimumValue="2001/01/01">Date is invalid or outside allowed range</asp:RangeValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblAwardEndDate" runat="server" CssClass="label">Award End</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <telerik:RadDatePicker ID="txtAwardEndDate" runat="server" MinDate="1/1/2001" Width="200px" >
                                        </telerik:RadDatePicker>
                                        <asp:RangeValidator ID="drvAwardEndDate" runat="server" ControlToValidate="txtAwardEndDate"
                                            ErrorMessage="Date is invalid or outside allowed range" Display="None" Type="Date"
                                            MaximumValue="2049/12/31" MinimumValue="2001/01/01">Date is invalid or outside allowed range</asp:RangeValidator>
                                        <asp:CompareValidator ID="cvAwardEndDate" runat="server" ControlToValidate="txtAwardEndDate"
                                            ControlToCompare="txtAwardStartDate" Display="None" Type="Date" Operator="GreaterThanEqual"
                                            ErrorMessage="Award End Date must be greater than Award Start Date">Award End Date must be greater than Award Start Date</asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblAcademicYearId" runat="server" CssClass="label">Award Year</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:DropDownList ID="ddlAcademicYearId" runat="server" CssClass="dropdownlist" Width="200px" 
                                            TabIndex="6">
                                        </asp:DropDownList>
                                        <asp:CompareValidator ID="AwardYearCompareValidator" runat="server" ErrorMessage="Must Select an Academic Year"
                                            Display="None" ControlToValidate="ddlAcademicYearId" Operator="NotEqual" ValueToCompare="00000000-0000-0000-0000-000000000000">Must Select an Academic Year</asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblLoanId" runat="server" CssClass="label">Loan Id</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <asp:TextBox ID="txtLoanId" runat="server" CssClass="textbox" TabIndex="7"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblLenderId" runat="server" CssClass="label">Lender</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <asp:DropDownList ID="ddlLenderId" runat="server" CssClass="dropdownlist" Width="200px" 
                                            TabIndex="8">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblServicer" runat="server" CssClass="label">Servicer</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <asp:DropDownList ID="ddlServicerId" runat="server" CssClass="dropdownlist" TabIndex="9" Width="200px" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblGuarantor" runat="server" CssClass="label">Guarantor</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <asp:DropDownList ID="ddlGuarantorId" runat="server" CssClass="dropdownlist" TabIndex="10" Width="200px" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>

                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblGrossAmount" runat="server" CssClass="label">Gross Amount</asp:Label>
                                    </td>

                                    <td class="contentcell4">
                                        <table class="contenttable" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="width: 60%" align="left">
                                                    <asp:TextBox ID="txtGrossAmount" runat="server" CssClass="textbox" AutoPostBack="True"
                                                        TabIndex="11"></asp:TextBox>

                                                    <span class="label">&nbsp;<%=MyAdvAppSettings.AppSettings("Currency")%></span>
                                                    <asp:RangeValidator ID="GrossAmountRangeValidator" runat="server" ControlToValidate="txtGrossAmount"
                                                        ErrorMessage="Invalid Gross Amount" Display="None" Type="Currency" MaximumValue="1000000"
                                                        MinimumValue="0.01">Invalid Gross Amount</asp:RangeValidator>
                                                </td>

                                            </tr>
                                        </table>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblLoanFees" runat="server" CssClass="label">Loan Fees</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <table class="contenttable" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="width: 60%" align="left">
                                                    <asp:TextBox ID="txtLoanFees" runat="server" CssClass="textbox" AutoPostBack="True"
                                                        TabIndex="12"></asp:TextBox>

                                                    <span class="label">&nbsp;<%=MyAdvAppSettings.AppSettings("Currency")%></span>
                                                    <asp:CompareValidator ID="LoanFeesCompareValidator" runat="server" ControlToValidate="txtLoanFees"
                                                        Operator="GreaterThanEqual" Type="Currency" Display="None" ValueToCompare="0.00"
                                                        ErrorMessage="Invalid Loan Fees amount">Invalid Loan Fees Amount</asp:CompareValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblNetLoanAmount" runat="server" CssClass="label">Net Amount</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <table class="contenttable" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="width: 60%">
                                                    <asp:TextBox ID="txtNetLoanAmount" runat="server" CssClass="textbox" ReadOnly="True"
                                                        TabIndex="13"></asp:TextBox>

                                                    <span class="label">&nbsp;<%=MyAdvAppSettings.AppSettings("Currency")%></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblFAID" runat="server" CssClass="label">FA Identifier</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <asp:TextBox ID="txtFAID" runat="server" CssClass="textbox"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblAwardCode" runat="server" CssClass="label" Visible="false">Award Code<font color="red">*</font></asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <asp:DropDownList ID="ddlAwardCode" runat="server" CssClass="dropdownlist" Visible="false" Width="200px" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblAwardSubCode" runat="server" CssClass="label" Visible="false">Award Sub Code<font color="red">*</font></asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <asp:DropDownList ID="ddlAwardSubCode" runat="server" CssClass="dropdownlist" Visible="false" Width="200px" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblAwardStatus" runat="server" CssClass="label" Visible="false">Award Status</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <asp:DropDownList ID="ddlAwardStatus" runat="server" CssClass="dropdownlist" Visible="false" Width="200px" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblDisbursements" runat="server" CssClass="label">Number of Disbursements</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <asp:TextBox ID="txtDisbursements" runat="server" CssClass="textbox" TabIndex="14"></asp:TextBox>
                                        <asp:RangeValidator ID="DisbursementRangeValidator" runat="server" ControlToValidate="txtDisbursements"
                                            ErrorMessage="Invalid Number of Disbursements" Display="None" Type="Integer"
                                            MaximumValue="100" MinimumValue="1">Invalid Number of Disbursements</asp:RangeValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap></td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtStudentAwardHead" runat="server" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap></td>
                                    <td class="contentcell4" align="center">
                                        <asp:LinkButton ID="lnkDisbursements" runat="server" TabIndex="15"><span style="text-decoration:underline">Calculate Disbursements</span></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="spacertables"></td>
                                </tr>
                                <tr>
                                    <td class="contentcell" colspan="2" align="center"></td>
                                </tr>
                            </table>
                            <table width="100%" align="center" border="0">
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgrdDisbursements" runat="server" Width="100%" BorderWidth="1px"
                                            ShowFooter="True" BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True"
                                            BorderColor="#E0E0E0" TabIndex="16">
                                            <AlternatingItemStyle CssClass="datagriditemstyle"></AlternatingItemStyle>
                                            <ItemStyle CssClass="datagriditemstyle" HorizontalAlign="Center"></ItemStyle>
                                            <HeaderStyle HorizontalAlign="Center" CssClass="datagridheaderstyle"></HeaderStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Scheduled Date" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExpectedDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ExpectedDate", "{0:d}") %>'>
                                                        </asp:Label>
                                                        <asp:Label ID="lblAwardScheduleId" runat="server" Text='<%# Container.DataItem("AwardScheduleId") %>'
                                                            Visible="False">
                                                        </asp:Label>
                                                        <asp:Label ID="lblWasReceived" runat="server" Text='<%# Container.DataItem("WasReceived") %>'
                                                            Visible="False">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtFooterExpectedDate" CssClass="textbox" runat="server" Width="100px"></asp:TextBox>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtEditExpectedDate" runat="server" CssClass="textbox" Width="100px"
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.ExpectedDate", "{0:d}") %>'>
                                                        </asp:TextBox>
                                                        <asp:Label ID="lblEditAwardScheduleId" runat="server" Text='<%# Container.DataItem("AwardScheduleId") %>'
                                                            Visible="False">
                                                        </asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Reference" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReference" runat="server" Text='<%# Container.DataItem("Reference") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtFooterReference" CssClass="textbox" runat="server" Width="180px"></asp:TextBox>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtEditReference" runat="server" CssClass="textbox" Width="180px"
                                                            Text='<%# Container.DataItem("Reference") %>'>
                                                        </asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Projected Amount" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Amount", "{0:c}") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle HorizontalAlign="Center"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtFooterAmount" CssClass="textbox" runat="server" Width="80px"></asp:TextBox>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtEditAmount" runat="server" CssClass="textbox" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.Amount", "{0:#.00}") %>'>
                                                        </asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                                                    <ItemStyle Wrap="False"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnlButEdit" Text="<img border=0 src=../images//im_edit.gif alt= edit>"
                                                            CausesValidation="False" runat="server" CommandName="Edit">
																					<img border="0" src="../images/im_edit.gif" alt="edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <FooterStyle Wrap="False"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Button ID="btnAddRow" Text="Add" runat="server" CommandName="AddNewRow"></asp:Button>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkbutUpdate" Text="<img border=0 src=../images/im_update.gif alt=update>"
                                                            runat="server" CommandName="Update">
																					<img border="0" src="../images/im_update.gif" alt="update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbutDelete" Text="<img border=0 src=../images/delete.gif alt=Delete>"
                                                            CausesValidation="False" runat="server" CommandName="Delete"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbutCancel" Text="<img border=0 src=../images//im_delete.gif alt=Cancel>"
                                                            CausesValidation="False" runat="server" CommandName="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Received Amount" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle Wrap="False" Width="100px"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReceivedAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Received", "{0:c}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle Wrap="False"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooterRecivedAmount" runat="server" Text=""></asp:Label>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditReceivedAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Received", "{0:c}") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Received Date" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle Wrap="False" Width="150px"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReceivedDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle Wrap="False"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooterReceivedDate" runat="server" Text=""></asp:Label>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditRecivedDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RecDate") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Refund Amount" HeaderStyle-HorizontalAlign="Center">
                                                    <ItemStyle Wrap="False" Width="100px"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRefundAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RefundAmount", "{0:c}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle Wrap="False"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooterRefundAmount" runat="server" Text=""></asp:Label>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditRefundAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RefundAmount", "{0:c}") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            <EditItemStyle></EditItemStyle>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                            <asp:Panel ID="pnlUDFHeader" runat="server" Visible="False">
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td class="spacertables"></td>
                                    </tr>
                                    <tr>
                                        <td class="contentcellheader" nowrap colspan="6">
                                            <asp:Label ID="lblSDF" runat="server" CssClass="label" Font-Bold="true">School Defined Fields</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="spacertables"></td>
                                    </tr>
                                    <tr>
                                        <td class="contentcell2" colspan="6">
                                            <asp:Panel ID="pnlSDF" runat="server" EnableViewState="false">
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>


