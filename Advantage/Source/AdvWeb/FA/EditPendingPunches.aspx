<%@ Page Title="Edit Pending Punches" Language="VB" AutoEventWireup="false" CodeFile="EditPendingPunches.aspx.vb" MasterPageFile="~/NewSite.master" Inherits="FA_EditPendingPunches" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register TagPrefix="ew" Namespace="eWorld.UI" Assembly="eWorld.UI, Version=1.9.0.0, Culture=neutral, PublicKeyToken=24d65337282035f2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="JavaScript" type="text/JavaScript">
        function openwin() {

            var campusid = "<%=campusId%>";
            var param1 = "<%=txtStudentName.clientID%>";
            var param2 = "<%=txtStuEnrollmentId.clientID%>";
            var param3 = "<%=txtStuEnrollment.clientID%>";
            var param4 = "<%=txtTermId.clientID%>";
            var param5 = "<%=txtTerm.clientID%>";
            var param6 = "<%=txtAcademicYearId.clientID%>";
            var param7 = "<%=txtAcademicYear.clientID%>";
            var userid = "<%=userId%>";

            var winSettings = "toolbar=no, status=yes, resizable=yes,width=900px,height=500px";
            var name = "StudentPopupSearch2";

            theURL = "../SY/StudentPopupSearch2.aspx?resid=357&mod=SA&cmpid=" + campusid;
            theURL += "&" + param1 + "=" + "&" + param2 + "=" + "&" + param3 + "=" + "&" + param4 + "=" + "&" + param5 + "=" + "&" + param6 + "=" + "&" + param7 + "=" + "&userid=" + userid;
            window.open(theURL, name, winSettings);
        }

        function Countcellno() {
            var grid = window["<%= dgTimeClockPunch.ClientID %>"];
            var totalrowcount = grid.rows.length;

            if (totalrowcount < 10) {
                totalrowcount = "0" + totalrowcount
            }

            return totalrowcount;

        }

        function Countcellno_byClass() {
            var grid = window["<%= dgTimeClockPunchByClass.ClientID %>"];
            var totalrowcount = grid.rows.length;

            if (totalrowcount < 10) {
                totalrowcount = "0" + totalrowcount
            }

            return totalrowcount;

        }
        function setRange(control, validator) {
            var x = document.getElementById(control)
            var y = document.getElementById(validator)
            if (x.selectedIndex == 0) { y.minimumvalue = '0.01' } else { y.minimumvalue = '-1000000' }

        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">


    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <!-- begin rightcolumn -->
                    <td class="menuframe">
                        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                                </td>

                            </tr>
                        </table>
                        <!-- end top menu (save,new,reset,delete,history)-->
                        <!--begin right column-->

                        <div id="DIVRHS" class="boxContainer" runat="server">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin table content-->
                            <table cellpadding="0" cellspacing="0" class="contenttable">

                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="lblStudent" runat="server" CssClass="Label" Width="87px">Student</asp:Label></td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtStudentName" TabIndex="1" runat="server" CssClass="textbox" contentEditable="False" Width="275px"></asp:TextBox>&nbsp;
													<a href="#" onclick="openwin()">
                                                        <span class="k-icon k-i-search" style="color: #1976d2; font-size: 18px"></span></a>
                                        <asp:LinkButton ID="lbtSearch" runat="server" CssClass="searchlinkbutton" CausesValidation="False"
                                            Visible="False"></asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell">
                                        <asp:Label ID="Label4" runat="server" CssClass="Label" Width="106px">Program Version</asp:Label></td>
                                    <td class="contentcell4">
                                        <asp:TextBox ID="txtprogram" runat="server" CssClass="TextBox noBorder bold" ReadOnly="True" TabIndex="2" Width="232px">Program</asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td nowrap class="contentcell" style="height: 11px;">
                                        <asp:Label ID="Label3" runat="server" CssClass="Label" Width="105px">Status</asp:Label></td>
                                    <td class="contentcell4" style="height: 11px">
                                        <asp:TextBox ID="txtstatus" runat="server" CssClass="TextBox noBorder bold" ReadOnly="True" TabIndex="6" Width="231px">Status</asp:TextBox></td>
                                </tr>

                                <tr>
                                    <td class="contentcell" style="width: 120px;">
                                        <asp:Label ID="Label1" runat="server" CssClass="Label" Text="Show Punches From" Style="float: left;" Width="89px"></asp:Label></td>
                                    <td class="contentcell4" >
                                        <telerik:RadDatePicker ID="txtFromDate" MinDate="1/1/1945" runat="server" Style="float: left;">
                                        </telerik:RadDatePicker>
                                        <asp:Label ID="Label2" runat="server" CssClass="Label" Style="padding-bottom: 7px;" Text="To" Width="10px"></asp:Label>
                                        <telerik:RadDatePicker ID="txtToDate" MinDate="1/1/1945" runat="server">
                                        </telerik:RadDatePicker>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell4" style="text-align: right" >
                                        <asp:Button ID="btnfetch" runat="server" Text="Get Punches" Width="125px" />

                                        <asp:HiddenField ID="txtTerm" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="txtStuEnrollment" runat="server"></asp:HiddenField>
                                        <asp:HiddenField ID="txtAcademicYear" runat="server"></asp:HiddenField>
                                        <asp:RequiredFieldValidator ID="rfvStudent" runat="server" Display="None" ErrorMessage="Student can not be blank"
                                            ControlToValidate="txtStudentName" Width="145px"> Student can not be blank</asp:RequiredFieldValidator>

                                        <asp:HiddenField ID="txtStuEnrollmentId" runat="server"></asp:HiddenField>
                                        <asp:TextBox ID="txtAcademicYearId" runat="server" Width="0%" CssClass="donothing" BorderStyle="None" BorderWidth="0"></asp:TextBox>
                                        <asp:HiddenField ID="txtTermId" runat="server"></asp:HiddenField>

                                    </td>


                                </tr>
                            </table>
                        </div>

                        <div class="boxContainer">
                            <asp:Panel ID="pnlUDFHeader" runat="server" Visible="true">
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td style="text-align: left;">
                                            <asp:Label ID="lblSDF" runat="server" CssClass="Label" Font-Bold="True">Time Clock Punches</asp:Label></td>
                                    </tr>
                                    <tr>

                                        <td class="spacertables" valign="top">
                                            <asp:DataGrid Visible="true" ID="dgTimeClockPunch" runat="server" ShowFooter="True" BorderStyle="Solid"
                                                AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true"
                                                EditItemStyle-Wrap="false" BorderColor="#E0E0E0" BorderWidth="1px">
                                                <EditItemStyle Wrap="False"></EditItemStyle>
                                                <AlternatingItemStyle CssClass="DataGridAlternatingStyle" Wrap="True"></AlternatingItemStyle>
                                                <ItemStyle CssClass="DataGridItemStyle" Wrap="True"></ItemStyle>
                                                <HeaderStyle CssClass="DataGridHeader" Wrap="True"></HeaderStyle>
                                                <FooterStyle CssClass="DataGridItemStyle"></FooterStyle>
                                                <Columns>

                                                    <asp:TemplateColumn HeaderText="No." Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSlNo" Text='<%# Container.DataItem("SlNo") %>' runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtSlNo" Visible="false" CssClass="TextBox" runat="server" ReadOnly="true" MaxLength="10"></asp:TextBox>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="lbleditslno" Text='<%# Container.DataItem("SlNo") %>' runat="server"></asp:Label>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>

                                                    <asp:TemplateColumn HeaderText="Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDate" Text='<%# GetPunchDate(Container.DataItem("Date"))%>' runat="server"></asp:Label>
                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <telerik:RadDatePicker ID="txtDate" MinDate="1/1/1945" runat="server">
                                                            </telerik:RadDatePicker>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtEditDate" CssClass="TextBoxDate" Text='<%# Container.DataItem("Date") %>' runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                                                            <asp:CompareValidator ID="dateValidator" runat="server" Type="Date" Operator="DataTypeCheck" ControlToValidate="txtEditDate"
                                                                ErrorMessage="Please enter a valid date."> </asp:CompareValidator>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>

                                                    <asp:TemplateColumn HeaderText="Time">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTime" Text='<%# GetPunchTime(Container.DataItem("PunchTime1")) %>' runat="server" CssClass="label"></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>

                                                            <ew:TimePicker ID="txtTime" ToolTip="Time" runat="server" ImageUrl="../images/ico_clock.gif"
                                                                Nullable="True" PopupWidth="370px" PopupHeight="175px" NumberOfColumns="8" MinuteInterval="OneMinute"
                                                                DisplayUnselectableTimes="False" PopupLocation="Top" ControlDisplay="TextBoxImage"
                                                                TextboxLabelStyle-CssClass="SchedTextBox" Enabled="true" CommandName="TimePicker" LowerBoundTime="6AM" DisableTextboxEntry="false">
                                                                <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                                <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                                                                <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                            </ew:TimePicker>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <ew:TimePicker ID="txtEditTime" ToolTip="Time" runat="server" ImageUrl="../images/ico_clock.gif"
                                                                Nullable="True" PopupWidth="370px" SelectedTime='<%# GetPunchTime(DataBinder.Eval(Container.DataItem, "PunchTime1"))  %>' PopupHeight="175px" NumberOfColumns="8" MinuteInterval="OneMinute"
                                                                DisplayUnselectableTimes="False" PopupLocation="Top" ControlDisplay="TextBoxImage"
                                                                TextboxLabelStyle-CssClass="SchedTextBox" Enabled="true" CommandName="TimePicker" LowerBoundTime="6AM" DisableTextboxEntry="false">
                                                                <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                                <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                                                                <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                            </ew:TimePicker>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>

                                                    <asp:TemplateColumn HeaderText="Punch Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblpunchtype" runat="server" Text='<%# GetPunchType(DataBinder.Eval(Container.DataItem, "PunchType")) %>'></asp:Label>&nbsp;
                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlPunchtype" runat="server" CssClass="dropdownlist">
                                                                <asp:ListItem Text="Punch In" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Punch Out" Value="2"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlEditPunchtype" runat="server" SelectedValue='<%# DataBinder.Eval(Container.DataItem, "PunchType") %>' CssClass="dropdownlist">
                                                                <asp:ListItem Text="Punch In" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Punch Out" Value="2"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>

                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnlButEdit" runat="server" Text="<img border=0 src=../images/im_edit.gif alt= edit>"
                                                                CausesValidation="False" CommandName="Edit">
																		<img border="0" src="../images//im_edit.gif" alt="edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Button ID="btnAddRow" runat="server" Text="Add" CausesValidation="False"
                                                                CommandName="AddNewRow"></asp:Button>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkbutUpdate" runat="server" Text="<img border=0 src=../images/im_update.gif alt=update>"
                                                                CommandName="Update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkbutDelete" runat="server" Text="<img border=0 src=../images/delete.gif alt=Delete>"
                                                                CausesValidation="False" CommandName="Delete"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkbutCancel" runat="server" Text="<img border=0 src=../images/im_delete.gif alt=Cancel>"
                                                                CausesValidation="False" CommandName="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>

                                    </tr>
                                    <tr>

                                        <td class="spacertables" valign="top">
                                            <asp:DataGrid Visible="true" ID="dgTimeClockPunchByClass" runat="server" ShowFooter="True" BorderStyle="Solid"
                                                AutoGenerateColumns="False" AllowSorting="True" HeaderStyle-Wrap="true"
                                                EditItemStyle-Wrap="false" BorderColor="#E0E0E0" BorderWidth="1px">
                                                <EditItemStyle Wrap="False"></EditItemStyle>
                                                <AlternatingItemStyle CssClass="DataGridAlternatingStyle" Wrap="True"></AlternatingItemStyle>
                                                <ItemStyle CssClass="DataGridItemStyle" Wrap="True"></ItemStyle>
                                                <HeaderStyle CssClass="DataGridHeader" Wrap="True"></HeaderStyle>
                                                <FooterStyle CssClass="DataGridItemStyle"></FooterStyle>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="No." Visible="true">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSlNo" Text='<%# Container.DataItem("SlNo") %>' runat="server"></asp:Label>

                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtSlNo" Visible="false" CssClass="TextBox" runat="server" ReadOnly="true" MaxLength="10"></asp:TextBox>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="lbleditslno" Text='<%# Container.DataItem("SlNo") %>' runat="server"></asp:Label>


                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Date">

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDate" Text='<%# Container.DataItem("Date") %>' runat="server"></asp:Label>

                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <asp:TextBox ID="txtStartDate" runat="server" Visible="false"></asp:TextBox>
                                                            <asp:TextBox ID="txtEndDate" runat="server" Visible="false"></asp:TextBox>
                                                            <telerik:RadDatePicker ID="txtDate" MinDate="1/1/1945" runat="server" Width="90">
                                                            </telerik:RadDatePicker>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtEditDate" Text='<%# Container.DataItem("Date") %>' runat="server" MaxLength="10" Width="90"></asp:TextBox>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>

                                                    <asp:TemplateColumn HeaderText="Time">

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTime" Text='<%# GetPunchTime(Container.DataItem("PunchTime1")) %>' runat="server" CssClass="label"></asp:Label>
                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <ew:TimePicker ID="txtTime" ToolTip="Time" runat="server" ImageUrl="../images/ico_clock.gif" Width="70"
                                                                Nullable="True" PopupWidth="370px" PopupHeight="175px" NumberOfColumns="8" MinuteInterval="FifteenMinutes"
                                                                DisplayUnselectableTimes="False" PopupLocation="Top" ControlDisplay="TextBoxImage"
                                                                TextboxLabelStyle-CssClass="SchedTextBox" Enabled="true" CommandName="TimePicker" LowerBoundTime="6AM" DisableTextboxEntry="false">
                                                                <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                                <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                                                                <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                            </ew:TimePicker>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <ew:TimePicker ID="txtEditTime" ToolTip="Time" runat="server" ImageUrl="../images/ico_clock.gif" Width="70"
                                                                Nullable="True" PopupWidth="370px" SelectedTime='<%# GetPunchTime(DataBinder.Eval(Container.DataItem, "PunchTime1"))  %>' PopupHeight="175px" NumberOfColumns="8" MinuteInterval="FifteenMinutes"
                                                                DisplayUnselectableTimes="False" PopupLocation="Top" ControlDisplay="TextBoxImage"
                                                                TextboxLabelStyle-CssClass="SchedTextBox" Enabled="true" CommandName="TimePicker" LowerBoundTime="6AM" DisableTextboxEntry="false">
                                                                <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                                <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                                                                <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                            </ew:TimePicker>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblpunchtype" runat="server" Text='<%# GetPunchType(DataBinder.Eval(Container.DataItem, "PunchType")) %>'></asp:Label>&nbsp;
                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlPunchtype" runat="server" CssClass="dropdownlist" Width="40">
                                                                <asp:ListItem Text="In" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Out" Value="2"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlEditPunchtype" runat="server" Width="40" SelectedValue='<%# DataBinder.Eval(Container.DataItem, "PunchType") %>' CssClass="dropdownlist">
                                                                <asp:ListItem Text="In" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Out" Value="2"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>

                                                    <asp:TemplateColumn HeaderText="Class Meeting" HeaderStyle-Width="35%" ItemStyle-Width="35%" FooterStyle-Width="35%">

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblClssetmeetingDesc" runat="server" Text='<%# GetClsSectMeetingDesc(DataBinder.Eval(Container.DataItem, "ClsSectMeetingID")) %>'></asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlClsSectMeeting" runat="server" CssClass="dropdownlist" Width="100%"
                                                                DataTextField="Descrip" DataValueField="ClsSectmeetingID">
                                                            </asp:DropDownList>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="lblClssetmeetingDesc" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ClsSectMeetingID") %>' Visible="false"></asp:Label>
                                                            <asp:DropDownList ID="ddlEditClsSectMeeting" runat="server"
                                                                CssClass="dropdownlist"
                                                                DataTextField="Descrip" DataValueField="ClsSectmeetingID" ToolTip='<%# DataBinder.Eval(Container.DataItem, "ClsSectMeetingID") %>'>
                                                            </asp:DropDownList>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>

                                                    <asp:TemplateColumn HeaderText="Punch Code" Visible="false">

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPunchCode" runat="server" Text='<%# (DataBinder.Eval(Container.DataItem, "SpecialCode")) %>'></asp:Label>&nbsp;
                                                        </ItemTemplate>

                                                        <FooterTemplate>
                                                            <asp:DropDownList ID="ddlSpecialCode" runat="server" CssClass="dropdownlist"
                                                                DataTextField="SpecialCode" DataValueField="SpecialCode">
                                                            </asp:DropDownList>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="lblPunchCode" runat="server" Text='<%# (DataBinder.Eval(Container.DataItem, "SpecialCode")) %>' Visible="false"></asp:Label>
                                                            <asp:DropDownList ID="ddlEditSpecialCode" runat="server"
                                                                CssClass="dropdownlist"
                                                                DataTextField="SpecialCode" DataValueField="SpecialCode">
                                                            </asp:DropDownList>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>

                                                    <asp:TemplateColumn HeaderText="Reason" HeaderStyle-Width="15%">

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblReason" runat="server" Text='<%# GetReason(DataBinder.Eval(Container.DataItem, "Status")) %>'></asp:Label>&nbsp;
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:Label ID="lblReason" runat="server" Text='<%# GetReason(DataBinder.Eval(Container.DataItem, "Status")) %>'></asp:Label>&nbsp;
																
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnlButEdit" runat="server" Text="<img border=0 src=../images/im_edit.gif alt= edit>"
                                                                CausesValidation="False" CommandName="Edit">
																		<img border="0" src="../images//im_edit.gif" alt="edit"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Button ID="btnAddRow" runat="server" Text="Add" CausesValidation="False"
                                                                CommandName="AddNewRow"></asp:Button>
                                                        </FooterTemplate>
                                                        <EditItemTemplate>
                                                            <asp:LinkButton ID="lnkbutUpdate" runat="server" Text="<img border=0 src=../images/im_update.gif alt=update>"
                                                                CommandName="Update"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkbutDelete" runat="server" Text="<img border=0 src=../images/delete.gif alt=Delete>"
                                                                CausesValidation="False" CommandName="Delete"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkbutCancel" runat="server" Text="<img border=0 src=../images/im_delete.gif alt=Cancel>"
                                                                CausesValidation="False" CommandName="Cancel"></asp:LinkButton>
                                                        </EditItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </td>

                                    </tr>

                                </table>
                                <table class="contenttable" cellspacing="0" cellpadding="0" width="100%">
                                    <tr>
                                        <td class="contentcell2">
                                            <asp:Panel ID="pnlSDF" TabIndex="12" runat="server" EnableViewState="false"></asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <!--end table content-->
                        </div>


                    </td>
                    <!-- end rightcolumn -->

                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <!-- start validation panel-->
    <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" Display="None"
        ErrorMessage="CustomValidator"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowMessageBox="True"
        ShowSummary="False"></asp:ValidationSummary>
    <!--end validation panel-->

</asp:Content>
