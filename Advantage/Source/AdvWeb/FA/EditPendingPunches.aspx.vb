Imports Fame.Common
Imports Fame.AdvantageV1.Common
Imports Fame.Advantage.Common
Imports BO = Advantage.Business.Objects
Imports System.Data
Imports Telerik.Web.UI

Partial Class FA_EditPendingPunches
    Inherits BasePage

    Protected ResourceId As Integer
    Protected campusId, userId As String
    Protected moduleid As String
    Public dsTimeClock As New DataSet
    Private numberOfRecords As Integer = 0
    Private previousMaxValue As Decimal = -1
    Private lastMax As Decimal = -1
    Public dtClsSectMeeting As New DataTable
    Public dtClsMeetingandPunchCodes As New DataTable
    Public dtSpecialCodes As New DataTable

    Dim facade As New Fame.AdvantageV1.BusinessFacade.TimeClock.TimeClockFacade
    Dim pobj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings
    ''Screen Added by Saraswathi Lakshmanan on Jan 2009
    ''The Exceptions are those, created during the IMport Process.
    ''Like A punchIN and PunchOut mismatch. Punch without badgeId allocated to student.
    ''Punch on an Unscheduled Day.- they are the pending Punches which has to be corrected.
    ''This screen is used to show the pending punches and it allows you to edit the Date, Time and the type of punch.
    ''An existing punch can be modified or deleted, and a new punch can also be added.


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim SDFControls As New SDFComponent

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        moduleid = HttpContext.Current.Request.Params("Mod").ToString
        pobj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pobj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        txtAcademicYearId.Attributes.Add("onClick", "__doPostBack('btnfetch','')")
        If txtAcademicYearId.Text <> "" Then
            If MyAdvAppSettings.AppSettings("TrackSapAttendance").ToString.ToLower = "byday" Then
                Populategrid()
            Else
                Populategrid_ByClass()
            End If

            txtAcademicYearId.Text = ""
        End If

        ''  Header1.EnableHistoryButton(False)
        If Not IsPostBack Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")

            ViewState("NoofRecordsDeleted") = 0

            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            If pobj.HasFull Or pobj.HasAdd Then
                btnSave.Enabled = True
                btnNew.Enabled = True
            Else
                btnSave.Enabled = False
                btnNew.Enabled = True
            End If
            ViewState("MODE") = "NEW"

            btnNew.Enabled = False
            btnDelete.Enabled = False


            '   initialize screen with transaction in session or an empty transaction
            If CommonWebUtilities.IsValidGuid(Session("TransactionId")) Then
                '   assign session value and destroy session
                ViewState("TransactionId") = Session("TransactionId")
                Session("TransactionId") = Nothing


                '   set Student Name textbox
                txtStudentName.Text = CType(Session("StudentName"), String)

                '   initialize buttons for edit
                '   InitButtonsForEdit()

            End If
        Else
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            btnNew.Enabled = False
            btnDelete.Enabled = False

        End If


        If MyAdvAppSettings.AppSettings("TrackSapAttendance").ToString.ToLower = "byday" Then
            dgTimeClockPunch.Visible = True
            dgTimeClockPunchByClass.Visible = False
        Else
            dgTimeClockPunch.Visible = False
            dgTimeClockPunchByClass.Visible = True
        End If

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BindToolTip()
        BindToolTipForControlsInsideaGrid(dgTimeClockPunchByClass)
    End Sub
    Private Sub Populategrid()
        Dim FromDate As String = ""
        Dim toDate As String = ""
        If Not (txtFromDate.SelectedDate Is Nothing And txtToDate.SelectedDate Is Nothing) Then
            If IsDate(txtFromDate.SelectedDate) Then
                FromDate = Format(CDate(txtFromDate.SelectedDate), "MM/dd/yyyy")
            Else
                DisplayErrorMessage("Enter a valid date")
                Exit Sub
            End If
            If IsDate(txtToDate.SelectedDate) Then
                toDate = Format(CDate(txtToDate.SelectedDate), "MM/dd/yyyy")
            Else
                DisplayErrorMessage("Enter a valid date")
                Exit Sub
            End If
        End If
        dsTimeClock = facade.GetTimeClockPunchExceptionforaStudentforgivendate(txtStuEnrollmentId.Value, FromDate, toDate)

        dsTimeClock.Tables(0).Columns.Add("SlNo", Type.GetType("System.Int32"))
        Dim i As Integer
        For i = 0 To dsTimeClock.Tables(0).Rows.Count - 1
            dsTimeClock.Tables(0).Rows(i)("SlNo") = i + 1
        Next
        Dim dt As New DataTable
        dt = CreateNewTableForDelete()
        dsTimeClock.Tables.Add(dt)
        dsTimeClock.Tables(2).TableName = "dttoDelete"
        dsTimeClock.AcceptChanges()

        If dsTimeClock.Tables(1).Rows.Count > 0 Then
            txtprogram.Text = dsTimeClock.Tables(1).Rows(0)("PrgVerDescrip").ToString
            txtstatus.Text = dsTimeClock.Tables(1).Rows(0)("StatusCodeDescrip").ToString
            ' txtTerm.Text = dsTimeClock.Tables(1).Rows(0)("TermDescrip").ToString

        End If
        Session("dsTimeClock") = dsTimeClock

        '   no record in the datagrid is selected

        '   no record in the datagrid is selected
        If dsTimeClock.Tables("TimeClockExceptionPunch").Rows.Count = 0 Then
            dgTimeClockPunch.CssClass = "scrollableLess"
        Else
            dgTimeClockPunch.CssClass = "scrollable"
        End If


        dgTimeClockPunch.EditItemIndex = -1
        dgTimeClockPunch.Enabled = True
        dgTimeClockPunch.DataSource = dsTimeClock.Tables("TimeClockExceptionPunch")
        dgTimeClockPunch.DataBind()
        dgTimeClockPunch.Visible = True
        '   show footer
        dgTimeClockPunch.ShowFooter = True
    End Sub
    Private Sub Populategrid_ByClass()
        Dim FromDate As String = ""
        Dim toDate As String = ""
        If Not (txtFromDate.SelectedDate Is Nothing And txtToDate.SelectedDate Is Nothing) Then
            If IsDate(txtFromDate.SelectedDate) Then
                FromDate = Format(CDate(txtFromDate.SelectedDate), "MM/dd/yyyy")
            Else
                DisplayErrorMessage("Enter a valid date")
                Exit Sub
            End If
            If IsDate(txtToDate.SelectedDate) Then
                toDate = Format(CDate(txtToDate.SelectedDate), "MM/dd/yyyy")
            Else
                DisplayErrorMessage("Enter a valid date")
                Exit Sub
            End If
        End If
        dsTimeClock = facade.GetTimeClockPunchExceptionforaStudentforgivendate_ByClass(txtStuEnrollmentId.Value, FromDate, toDate)

        dtClsSectMeeting = facade.GetClsSectMeetingsforaStudent(txtStuEnrollmentId.Value, campusId)

        dtSpecialCodes = facade.GetSpecialCodesforgivenCampus(campusId)

        dtClsMeetingandPunchCodes = facade.GetSpecialCodesAndClsSectionMeetingforStudent(txtStuEnrollmentId.Value, campusId)

        Dim SpecialCodeQuery = (From PunchesTable In dsTimeClock.Tables(0).AsEnumerable() Select SpecialCode = PunchesTable("SpecialCode")).Distinct.ToList
        Dim spCode As String

        For Each punchCode In SpecialCodeQuery
            spCode = punchCode
            If dtSpecialCodes.Select("SpecialCode='" + spCode + "'").Length = 0 Then
                Dim dr As DataRow
                dr = dtSpecialCodes.NewRow
                dr(0) = spCode
                dtSpecialCodes.Rows.Add(dr)
                dtSpecialCodes.AcceptChanges()
            End If
        Next
        ViewState("dtSpecialCodes") = dtSpecialCodes

        ViewState("dtClsSectMeeting") = dtClsSectMeeting
        ViewState("dtClsMeetingandPunchCodes") = dtClsMeetingandPunchCodes
        dsTimeClock.Tables(0).Columns.Add("SlNo", Type.GetType("System.Int32"))
        Dim i As Integer
        For i = 0 To dsTimeClock.Tables(0).Rows.Count - 1
            dsTimeClock.Tables(0).Rows(i)("SlNo") = i + 1
        Next
        Dim dt As New DataTable
        dt = CreateNewTableForDelete_ByClass()
        dsTimeClock.Tables.Add(dt)
        dsTimeClock.Tables(2).TableName = "dttoDelete"
        dsTimeClock.AcceptChanges()

        If dsTimeClock.Tables(1).Rows.Count > 0 Then
            txtprogram.Text = dsTimeClock.Tables(1).Rows(0)("PrgVerDescrip").ToString
            txtstatus.Text = dsTimeClock.Tables(1).Rows(0)("StatusCodeDescrip").ToString
            ' txtTerm.Text = dsTimeClock.Tables(1).Rows(0)("TermDescrip").ToString

        End If
        Session("dsTimeClock") = dsTimeClock

        '   no record in the datagrid is selected
        If dsTimeClock.Tables("TimeClockExceptionPunch").Rows.Count = 0 Then
            dgTimeClockPunchByClass.CssClass = "scrollableLess"
        Else
            dgTimeClockPunchByClass.CssClass = "scrollable"
        End If

        dgTimeClockPunchByClass.EditItemIndex = -1
        dgTimeClockPunchByClass.Enabled = True
        dgTimeClockPunchByClass.DataSource = dsTimeClock.Tables("TimeClockExceptionPunch")
        dgTimeClockPunchByClass.DataBind()
        dgTimeClockPunchByClass.Visible = True
        '   show footer
        dgTimeClockPunchByClass.ShowFooter = True
    End Sub

    Protected Sub btnfetch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnfetch.Click

        If MyAdvAppSettings.AppSettings("TrackSapAttendance").ToString.ToLower = "byday" Then
            Populategrid()
        Else
            Populategrid_ByClass()
        End If



    End Sub

    Private Function CreateNewTableForDelete() As DataTable
        Dim dtDelete As New DataTable
        dtDelete.Columns.Add("BadgeID", Type.GetType("System.String"))
        dtDelete.Columns.Add("PunchTime", Type.GetType("System.DateTime"))
        dtDelete.Columns.Add("PunchType", Type.GetType("System.Int32"))
        Return dtDelete
    End Function
    Private Function CreateNewTableForDelete_ByClass() As DataTable
        Dim dtDelete As New DataTable
        dtDelete.Columns.Add("BadgeID", Type.GetType("System.String"))
        dtDelete.Columns.Add("PunchTime", Type.GetType("System.DateTime"))
        dtDelete.Columns.Add("PunchType", Type.GetType("System.Int32"))
        dtDelete.Columns.Add("ClsSectmeetingID", Type.GetType("System.String"))
        dtDelete.Columns.Add("SpecialCode", Type.GetType("System.String"))

        Return dtDelete
    End Function

    Protected Sub dgTimeClockPunch_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTimeClockPunch.ItemCommand
        '   process postbacks from the datagrid
        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                ''When the user clicks edit, Show the save, delete and cancel options
                ''And Don't show the footer and the save button on top of the screen is enabled

                '   edit selected item
                dgTimeClockPunch.EditItemIndex = e.Item.ItemIndex

                '    do not show footer
                dgTimeClockPunch.ShowFooter = False

                '   disable save button
                btnSave.Enabled = False

                '   user hit "Update" inside the datagrid
            Case "Update"
                ''When the user clicks Save on the grid, The edited value is stored in the same table and 
                ''the value which was edited is stored in a seperate table to delete later.,
                dsTimeClock = Session("dsTimeClock")
                dgTimeClockPunch.EditItemIndex = e.Item.ItemIndex

                '   get the row to be updated
                Dim row() As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").Select("SlNo=" + "'" + CType(e.Item.FindControl("lbleditslno"), Label).Text + "'")
                ' Dim row = GridView1.Rows(e.RowIndex)
                If CType(e.Item.FindControl("txtEditDate"), TextBox).Text = "" Then
                    DisplayErrorMessage("Date is required")
                ElseIf CType(e.Item.FindControl("txtEditTime"), eWorld.UI.TimePicker).PostedTime = "" Then
                    DisplayErrorMessage("Time is required")
                End If
                ''Move the old entries to a new datatable and edit the entry in the original table
                ''Save the old entries to delete it later
                Dim dr As DataRow = dsTimeClock.Tables(2).NewRow

                dr("PunchType") = row(0)("PunchType")
                dr("BadgeId") = dsTimeClock.Tables("StudentTimeClockData").Rows(0)("BadgeNumber").ToString
                dr("PunchTime") = DateTime.Parse(row(0)("Date") + " " + row(0)("PunchTime1"))
                dsTimeClock.Tables(2).Rows.Add(dr)

                '   fill new values in the row
                row(0)("Date") = CType(e.Item.FindControl("txtEditDate"), TextBox).Text
                row(0)("PunchTime1") = CType(e.Item.FindControl("txtEditTime"), eWorld.UI.TimePicker).PostedTime
                row(0)("PunchType") = CType(e.Item.FindControl("ddlEditPunchtype"), DropDownList).SelectedValue

                Session("dsTimeClock") = dsTimeClock
                dgTimeClockPunch.DataSource = dsTimeClock.Tables("TimeClockExceptionPunch")
                dgTimeClockPunch.DataBind()
                'Prepare dataGrid
                PrepareDatagrid()

                '   user hit "Cancel"
            Case "Cancel"

                'Prepare dataGrid
                PrepareDatagrid()

                '   user hit "Delete" inside the datagrid
            Case "Delete"
                ''Delete will delelte the row from the original table and 
                ''added  to the table which will be deleted later 

                dsTimeClock = Session("dsTimeClock")
                '   get the row to be updated
                ' Dim row() As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").Select("Date=" + "'" + CType(e.Item.FindControl("txtEditDate"), TextBox).Text + "' and PunchTime1='" + CType(e.Item.FindControl("txtEditTime"), eWorld.UI.TimePicker).PostedTime + "'")
                '  Dim row() As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").Select("Date=" + "'" + CType(e.Item.FindControl("lblDate"), Label).Text + "' and PunchTime1='" + CType(e.Item.FindControl("lblTime"), Label).Text + "' and PunchType='" + CType(e.Item.FindControl("lblpunchtype"), Label).Text + "'")
                Dim row() As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").Select("SlNo=" + "'" + CType(e.Item.FindControl("lbleditslno"), Label).Text + "'")

                Dim dr As DataRow = dsTimeClock.Tables(2).NewRow
                dr("PunchType") = row(0)("PunchType")
                dr("BadgeId") = dsTimeClock.Tables("StudentTimeClockData").Rows(0)("BadgeNumber").ToString
                dr("PunchTime") = DateTime.Parse(row(0)("Date") + " " + row(0)("PunchTime1"))
                dsTimeClock.Tables(2).Rows.Add(dr)

                '   delete row 
                row(0).Delete()
                Dim intnoofrecordsdeleted As Integer = 0
                intnoofrecordsdeleted = ViewState("NoofRecordsDeleted") + 1
                ViewState("NoofRecordsDeleted") = intnoofrecordsdeleted

                dsTimeClock.AcceptChanges()
                'prepare DataGrid
                PrepareDatagrid()

            Case "AddNewRow"
                ''Will add a new row to the original table
                ''
                If Not ValidateFooter() Then
                    '   Display Error Message
                    DisplayErrorMessage("Missing data is required")
                    Exit Sub
                End If
                Dim intnoofrecordsdeleted As Integer = 0
                intnoofrecordsdeleted = ViewState("NoofRecordsDeleted")

                dsTimeClock = Session("dsTimeClock")
                Dim newRow As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").NewRow
                If dsTimeClock.Tables("StudentTimeClockData").Rows.Count > 0 Then
                    Dim selectedDate = CType(e.Item.FindControl("txtDate"), RadDatePicker).SelectedDate
                    '   fill the new row with values
                    newRow("BadgeId") = dsTimeClock.Tables("StudentTimeClockData").Rows(0)("BadgeNumber").ToString
                    newRow("Date") = If(selectedDate.HasValue, selectedDate.Value.ToShortDateString(), "")
                    newRow("PunchTime1") = CType(e.Item.FindControl("txtTime"), eWorld.UI.TimePicker).PostedTime
                    newRow("PunchType") = CType(e.Item.FindControl("ddlPunchtype"), DropDownList).SelectedValue
                    newRow("ClockId") = "PN00"
                    newRow("Status") = "0"
                    newRow("StuEnrollId") = txtStuEnrollmentId.Value
                    newRow("FromSystem") = 0
                    newRow("ScheduleId") = dsTimeClock.Tables("StudentTimeClockData").Rows(0)("ScheduleId").ToString
                    newRow("SlNo") = dsTimeClock.Tables(0).Rows.Count + 1 + intnoofrecordsdeleted
                End If
                '   add row to the table
                newRow.Table.Rows.Add(newRow)
                Session("dsTimeClock") = dsTimeClock
                ''    ''Added on Feb 18 2009
                ''    ''Up and down arrow buttons on the grid

                ''    '   user hit "Up"
                ''Case "Up"
                ''    '   get the row to be updated
                ''    Dim row() As DataRow = GradeScaleDetailsTable.Select("GrdScaleDetailId=" + "'" + CType(e.Item.FindControl("lblEditGrdScaleDetailId"), Label).Text + "'")
                ''    Dim row() As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").Select("SlNo=" + "'" + CType(e.Item.FindControl("txtEditSlNo"), TextBox).Text + "'")

                ''    '   get a view with data ordered by ViewOrder
                ''    Dim dv As New DataView(dsTimeClock.Tables("TimeClockExceptionPunch"), "SlNo=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                ''    '   get the id of the item selected by the user
                ''    Dim GrdScaleDetailId As String = CType(e.Item.FindControl("lblGrdScaleDetailId"), Label).Text

                ''    '   get the position of this item and swap viewOrder values with prevous item
                ''    Dim i As Integer
                ''    For i = 1 To dv.Count - 1
                ''        If CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString = GrdScaleDetailId Then
                ''            SwapViewOrders(CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString, CType(dv(i - 1).Row("GrdScaleDetailId"), Guid).ToString)
                ''            Exit For
                ''        End If
                ''    Next

                ''    '   user hit "Down" 
                ''Case "Down"
                ''    '   get a view with data ordered by ViewOrder
                ''    Dim dv As New DataView(GradeScaleDetailsTable, "GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                ''    '   get the id of the item selected by the user
                ''    Dim GrdScaleDetailId As String = CType(e.Item.FindControl("lblGrdScaleDetailId"), Label).Text

                ''    '   get the position of this item and swap viewOrder values with next item
                ''    Dim i As Integer
                ''    For i = 0 To dv.Count - 2
                ''        If CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString = GrdScaleDetailId Then
                ''            SwapViewOrders(CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString, CType(dv(i + 1).Row("GrdScaleDetailId"), Guid).ToString)
                ''            Exit For
                ''        End If
                ''    Next
        End Select
        If Not e.CommandName = "TimePicker" Then
            dsTimeClock = Session("dsTimeClock")
            dgTimeClockPunch.DataSource = dsTimeClock.Tables("TimeClockExceptionPunch")
            dgTimeClockPunch.DataBind()
        End If

        ''   bind datagrid to GradeScaleDetailsTable 
        'BindDataGrid(New DataView(GradeScaleDetailsTable, "GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows))


    End Sub
    Private Sub SwapViewOrders(ByVal id1 As String, ByVal id2 As String)

        '   get the rows with each id
        Dim row1(), row2() As DataRow
        row1 = dsTimeClock.Tables("TimeClockExceptionPunch").Select("SlNo=" + "'" + id1 + "'")
        row2 = dsTimeClock.Tables("TimeClockExceptionPunch").Select("SlNo=" + "'" + id2 + "'")

        '   swap value of viewOrder
        Dim viewOrder As Integer
        viewOrder = row1(0)("ViewOrder")
        row1(0)("ViewOrder") = row2(0)("ViewOrder")
        row2(0)("ViewOrder") = viewOrder
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If

    End Sub

    Private Sub DisplayMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayInfoInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Sub DisplayWarningMessage(ByVal errorMessage As String)

        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayWarningInMessageBox(Me.Page, errorMessage)
        End If

    End Sub
    Private Sub PrepareDatagrid()
        '   set no record selected
        dgTimeClockPunch.EditItemIndex = -1

        '   show footer
        dgTimeClockPunch.ShowFooter = True

        If pobj.HasFull Or pobj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        '   save button must be enabled
        '  btnSave.Enabled = True
    End Sub

    Private Sub PrepareDatagrid_ByClass()
        '   set no record selected
        dgTimeClockPunchByClass.EditItemIndex = -1

        '   show footer
        dgTimeClockPunchByClass.ShowFooter = True

        If pobj.HasFull Or pobj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        '   save button must be enabled
        '  btnSave.Enabled = True
    End Sub
    Protected Function GetPunchType(ByVal number As Integer) As String
        If (number = 1) Then
            Return "In"
        Else
            Return "Out"
        End If
    End Function
    Protected Function GetPunchTime(ByVal pntime As Date) As String
        Return Format(pntime, "hh:mm tt")
    End Function
    Protected Function GetPunchDate(ByVal pnDate As Date) As String
        Return CDate(pnDate)
    End Function

    Protected Function GetClsSectMeetingDesc(ByVal ClsSectMeetingID As Guid) As String
        dtClsSectMeeting = ViewState("dtClsSectMeeting")

        If ClsSectMeetingID <> Guid.Empty Then

            Dim ClsSectMeetingQuery = (From ClsSectMeetingTable In dtClsSectMeeting.AsEnumerable() Where ClsSectMeetingTable.Field(Of Guid)("ClsSectmeetingID") = ClsSectMeetingID Select Descrip = ClsSectMeetingTable("Descrip")).Distinct
            For Each DescripItem In ClsSectMeetingQuery
                Return DescripItem.ToString
            Next
        End If
        Return ""
    End Function

    Protected Function GetReason(ByVal number As Integer) As String
        '        ''If (Cstr({StudentDetails.Status}) = "2") then "The student was missing one or more punches for the day "
        'Else If (Cstr({StudentDetails.Status}) = "6") then "The student was not scheduled for the day "
        'Else If (Cstr({StudentDetails.Status}) = "5") then "A badge was used with no assigned student or account number/Students out of school or Time clock schedule not available - these hours were lost."
        'Else If (Cstr({StudentDetails.Status}) = "7") then "The Punch was out of order. Punch out before a punch In"
        'else "Invalid Punches"

        Select Case number
            Case 2 : Return "The student was missing one or more punches for the day"
            Case 5 : Return "A badge number was not assigned or student was out of school"
            Case 9 : Return "Class not identified"
            Case 7 : Return "The Punch was out of order. Student must Punch In before a Punch Out"
            Case 6 : Return "Student was not scheduled for the day"
            Case 0 : Return "New entry saved but not processed"
            Case 10 : Return "Student has valid and invalid punches for the same day"
            Case Else
                Return "Invalid Punches"
        End Select

    End Function
    Private Function ValidateFooter() As Boolean
        Dim footer As Control = dgTimeClockPunch.Controls(0).Controls(dgTimeClockPunch.Controls(0).Controls.Count - 1)
        If CType(footer.FindControl("txtdate"), RadDatePicker).SelectedDate Is Nothing Then
            Return False
        ElseIf CType(footer.FindControl("txtTime"), eWorld.UI.TimePicker).PostedTime = "" Then
            Return False
        End If
        Return True
    End Function

    Private Function ValidateFooter_byClass() As Boolean
        Dim footer As Control = dgTimeClockPunchByClass.Controls(0).Controls(dgTimeClockPunchByClass.Controls(0).Controls.Count - 1)
        If CType(footer.FindControl("txtdate"), RadDatePicker).SelectedDate Is Nothing Then
            Return False
        ElseIf CType(footer.FindControl("txtTime"), eWorld.UI.TimePicker).PostedTime = "" Then
            Return False
        End If
        Return True
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        ''IMport these data once again and calculate the actual hours
        Dim res As String
        dsTimeClock = Session("dsTimeClock")
        ''SAve the punches to the table  arStudentTimeClockPunches
        ''And calculate the Actual hours  and save it in the table arStudentClockAttendance
        ''Count of records before Saving
        Dim Countbeforesaving As Integer
        Countbeforesaving = dsTimeClock.Tables(0).Rows.Count

        'check if the enrollment is a transferred out,the badgeid becomes null
        If dsTimeClock.Tables(1).Rows.Count > 0 Then
            If dsTimeClock.Tables(1).Rows(0)("BadgeNumber").ToString = "" Then
                DisplayErrorMessage("Cannot process punches since Badge ID doesn't exist")
                Exit Sub
            End If

        End If

        If MyAdvAppSettings.AppSettings("TrackSapAttendance").ToString.ToLower = "byday" Then
            res = facade.SavePunchesandProcessHours(dsTimeClock, AdvantageSession.UserName, campusId)
            Try
                Dim dt As DataTable
                If dsTimeClock IsNot Nothing And dsTimeClock.Tables(0) IsNot Nothing And dsTimeClock.Tables(0).Rows.Count > 0 Then
                    dt = dsTimeClock.Tables(0)
                    Dim StuEnroll As DataTable
                    StuEnroll = dt.DefaultView.ToTable(True, "StuEnrollId")
                    Dim stuEnrollID = StuEnroll.AsEnumerable().Select(Function(r) r.Field(Of Guid)("StuEnrollId")).FirstOrDefault()
                    If Not stuEnrollID = Guid.Empty Then
                        facade.RecalculateGraduationDateAPI(New List(Of String) From {stuEnrollID.ToString()}, MyAdvAppSettings)
                    End If
                End If
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)
            End Try
        Else
            res = facade.SavePunchesandProcessHours_ByClass(dsTimeClock, AdvantageSession.UserName, campusId)
        End If

        If Not res = "" Then
            DisplayErrorMessage(res)
            Exit Sub
        End If

        'txtFromDate.Text = ""
        'txtToDate.Text = ""
        If MyAdvAppSettings.AppSettings("TrackSapAttendance").ToString.ToLower = "byday" Then
            Populategrid()
        Else
            Populategrid_ByClass()

        End If
        dsTimeClock = Session("dsTimeClock")
        Dim CountAfterSaving As Integer
        CountAfterSaving = dsTimeClock.Tables(0).Rows.Count

        If Countbeforesaving = CountAfterSaving Then
            DisplayMessage("No changes are done to the punches")

        ElseIf Countbeforesaving > CountAfterSaving And CountAfterSaving = 0 Then
            DisplayMessage("All the punches are processed and corrected")

        Else
            DisplayWarningMessage("Only some of the punches are processed and corrected")
        End If




    End Sub

    ' ''Added by Saraswathi lakshmanan on August 24 2009
    ' ''To find the list controls and add a tool tip to those items in the control
    ' ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control


    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls

    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If

    '            Next
    '        Next
    '    Next
    'End Sub

    Protected Sub dgTimeClockPunchByClass_ItemCommand(source As Object, e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTimeClockPunchByClass.ItemCommand
        '   process postbacks from the datagrid
        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                ''When the user clicks edit, Show the save, delete and cancel options
                ''And Don't show the footer and the save button on top of the screen is enabled

                '   edit selected item
                dgTimeClockPunchByClass.EditItemIndex = e.Item.ItemIndex
                '    do not show footer
                dgTimeClockPunchByClass.ShowFooter = False

                '   disable save button
                btnSave.Enabled = False

                '   user hit "Update" inside the datagrid
            Case "Update"
                ''When the user clicks Save on the grid, The edited value is stored in the same table and 
                ''the value which was edited is stored in a seperate table to delete later.,
                Dim startDate As Date
                Dim endDate As Date
                Dim clsmtgRows() As DataRow

                clsmtgRows = CType(ViewState("dtClsSectMeeting"), DataTable).Select("ClsSectMeetingId='" + CType(e.Item.FindControl("ddlEditClsSectMeeting"), DropDownList).SelectedValue + "'")
                startDate = clsmtgRows(0)("StartDate")
                endDate = clsmtgRows(0)("EndDate")
                dsTimeClock = Session("dsTimeClock")
                dgTimeClockPunchByClass.EditItemIndex = e.Item.ItemIndex

                '   get the row to be updated
                Dim row() As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").Select("SlNo=" + "'" + CType(e.Item.FindControl("lbleditslno"), Label).Text + "'")
                ' Dim row = GridView1.Rows(e.RowIndex)

                If CType(e.Item.FindControl("txtEditDate"), TextBox).Text = "" Then
                    DisplayErrorMessage("Date is required")
                ElseIf CDate(CType(e.Item.FindControl("txtEditDate"), TextBox).Text) < startDate Or CDate(CType(e.Item.FindControl("txtEditDate"), TextBox).Text) > endDate Then
                    DisplayErrorMessage("Date has to be between Start Date and End Date of the meeting.")
                    Exit Sub
                ElseIf CType(e.Item.FindControl("txtEditTime"), eWorld.UI.TimePicker).PostedTime = "" Then
                    DisplayErrorMessage("Time is required")
                End If
                ''Move the old entries to a new datatable and edit the entry in the original table
                ''Save the old entries to delete it later

                ''Get the special Codefrom the cls Meetings table
                dtClsMeetingandPunchCodes = ViewState("dtClsMeetingandPunchCodes")

                Dim rowSpCode() As DataRow = dtClsMeetingandPunchCodes.Select("ClsSectMeetingId=" + "'" + CType(e.Item.FindControl("ddlEditClsSectMeeting"), DropDownList).SelectedValue + "' And TCSPunchType=" + CType(e.Item.FindControl("ddlEditPunchtype"), DropDownList).SelectedValue)
                Dim spCode As String = ""
                If rowSpCode.Length >= 1 Then
                    spCode = rowSpCode(0)("TCSpecialCode")
                Else
                    DisplayErrorMessage("No Punch Code available for this Instruction Type")
                    Exit Sub
                End If


                Dim dr As DataRow = dsTimeClock.Tables(2).NewRow

                dr("PunchType") = row(0)("PunchType")
                dr("BadgeId") = dsTimeClock.Tables("StudentTimeClockData").Rows(0)("BadgeNumber").ToString
                dr("PunchTime") = DateTime.Parse(row(0)("Date") + " " + row(0)("PunchTime1"))
                dr("ClsSectMeetingID") = row(0)("ClsSectMeetingID")
                dr("SpecialCode") = row(0)("SpecialCode")

                dsTimeClock.Tables(2).Rows.Add(dr)

                '   fill new values in the row
                row(0)("Date") = CType(e.Item.FindControl("txtEditDate"), TextBox).Text
                row(0)("PunchTime1") = CType(e.Item.FindControl("txtEditTime"), eWorld.UI.TimePicker).PostedTime
                row(0)("PunchType") = CType(e.Item.FindControl("ddlEditPunchtype"), DropDownList).SelectedValue
                row(0)("ClsSectMeetingID") = CType(e.Item.FindControl("ddlEditClsSectMeeting"), DropDownList).SelectedValue
                ' row(0)("SpecialCode") = CType(e.Item.FindControl("ddlEditSpecialCode"), DropDownList).SelectedValue
                row(0)("SpecialCode") = spCode
                Session("dsTimeClock") = dsTimeClock
                dgTimeClockPunchByClass.DataSource = dsTimeClock.Tables("TimeClockExceptionPunch")
                dgTimeClockPunchByClass.DataBind()
                'Prepare dataGrid
                PrepareDatagrid_ByClass()

                '   user hit "Cancel"
            Case "Cancel"

                'Prepare dataGrid
                PrepareDatagrid_ByClass()

                '   user hit "Delete" inside the datagrid
            Case "Delete"
                ''Delete will delelte the row from the original table and 
                ''added  to the table which will be deleted later 

                dsTimeClock = Session("dsTimeClock")
                '   get the row to be updated
                ' Dim row() As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").Select("Date=" + "'" + CType(e.Item.FindControl("txtEditDate"), TextBox).Text + "' and PunchTime1='" + CType(e.Item.FindControl("txtEditTime"), eWorld.UI.TimePicker).PostedTime + "'")
                '  Dim row() As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").Select("Date=" + "'" + CType(e.Item.FindControl("lblDate"), Label).Text + "' and PunchTime1='" + CType(e.Item.FindControl("lblTime"), Label).Text + "' and PunchType='" + CType(e.Item.FindControl("lblpunchtype"), Label).Text + "'")
                Dim row() As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").Select("SlNo=" + "'" + CType(e.Item.FindControl("lbleditslno"), Label).Text + "'")

                Dim dr As DataRow = dsTimeClock.Tables(2).NewRow
                dr("PunchType") = row(0)("PunchType")
                dr("BadgeId") = dsTimeClock.Tables("StudentTimeClockData").Rows(0)("BadgeNumber").ToString
                dr("PunchTime") = DateTime.Parse(row(0)("Date") + " " + row(0)("PunchTime1"))
                dr("ClsSectMeetingID") = row(0)("ClsSectMeetingID")
                dr("SpecialCode") = row(0)("SpecialCode")

                dsTimeClock.Tables(2).Rows.Add(dr)

                '   delete row 
                row(0).Delete()
                Dim intnoofrecordsdeleted As Integer = 0
                intnoofrecordsdeleted = ViewState("NoofRecordsDeleted") + 1
                ViewState("NoofRecordsDeleted") = intnoofrecordsdeleted

                dsTimeClock.AcceptChanges()
                'prepare DataGrid
                PrepareDatagrid_ByClass()

            Case "AddNewRow"
                ''Will add a new row to the original table
                ''
                If Not ValidateFooter_byClass() Then
                    '   Display Error Message
                    DisplayErrorMessage("Missing data is required")
                    Exit Sub
                End If
                Dim intnoofrecordsdeleted As Integer = 0
                intnoofrecordsdeleted = ViewState("NoofRecordsDeleted")

                ''Get the special Codefrom the cls Meetings table
                dtClsMeetingandPunchCodes = ViewState("dtClsMeetingandPunchCodes")

                Dim rowSpCode() As DataRow = dtClsMeetingandPunchCodes.Select("ClsSectMeetingId=" + "'" + CType(e.Item.FindControl("ddlClsSectMeeting"), DropDownList).SelectedValue + "' AND TCSPunchType=" + CType(e.Item.FindControl("ddlPunchtype"), DropDownList).SelectedValue)
                Dim spCode As String = ""
                If rowSpCode.Length >= 1 Then
                    spCode = rowSpCode(0)("TCSpecialCode")
                Else
                    DisplayErrorMessage("No Punch Code available for this Instruction Type")
                    Exit Sub
                End If


                Dim startDate As Date
                Dim endDate As Date
                Dim clsmtgRows() As DataRow

                clsmtgRows = CType(ViewState("dtClsSectMeeting"), DataTable).Select("ClsSectMeetingId='" + CType(e.Item.FindControl("ddlClsSectMeeting"), DropDownList).SelectedValue + "'")
                startDate = clsmtgRows(0)("StartDate")
                endDate = clsmtgRows(0)("EndDate")


                If CType(e.Item.FindControl("txtDate"), RadDatePicker).SelectedDate Is Nothing Then
                    DisplayErrorMessage("Date is required")
                ElseIf CDate(CType(e.Item.FindControl("txtDate"), RadDatePicker).SelectedDate) < startDate Or CDate(CType(e.Item.FindControl("txtDate"), RadDatePicker).SelectedDate) > endDate Then
                    DisplayErrorMessage("Date has to be between Start Date and End Date of the meeting.")
                    Exit Sub
                ElseIf CType(e.Item.FindControl("txtTime"), eWorld.UI.TimePicker).PostedTime = "" Then
                    DisplayErrorMessage("Time is required")
                End If

                dsTimeClock = Session("dsTimeClock")
                Dim newRow As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").NewRow
                If dsTimeClock.Tables("StudentTimeClockData").Rows.Count > 0 Then
                    '   fill the new row with values
                    newRow("BadgeId") = dsTimeClock.Tables("StudentTimeClockData").Rows(0)("BadgeNumber").ToString
                    newRow("Date") = CDate(CType(e.Item.FindControl("txtDate"), RadDatePicker).SelectedDate).ToShortDateString
                    newRow("PunchTime1") = CType(e.Item.FindControl("txtTime"), eWorld.UI.TimePicker).PostedTime
                    newRow("PunchType") = CType(e.Item.FindControl("ddlPunchtype"), DropDownList).SelectedValue
                    '  newRow("SpecialCode") = CType(e.Item.FindControl("ddlSpecialCode"), DropDownList).SelectedValue
                    newRow("SpecialCode") = spCode
                    newRow("ClockId") = "PN00"
                    newRow("Status") = "0"
                    newRow("StuEnrollId") = txtStuEnrollmentId.Value
                    newRow("FromSystem") = 0
                    '  newRow("ScheduleId") = dsTimeClock.Tables("StudentTimeClockData").Rows(0)("ScheduleId").ToString
                    newRow("ClsSectMeetingID") = CType(e.Item.FindControl("ddlClsSectMeeting"), DropDownList).SelectedValue
                    newRow("SlNo") = dsTimeClock.Tables(0).Rows.Count + 1 + intnoofrecordsdeleted
                End If
                '   add row to the table
                newRow.Table.Rows.Add(newRow)
                Session("dsTimeClock") = dsTimeClock
                ''    ''Added on Feb 18 2009
                ''    ''Up and down arrow buttons on the grid

                ''    '   user hit "Up"
                ''Case "Up"
                ''    '   get the row to be updated
                ''    Dim row() As DataRow = GradeScaleDetailsTable.Select("GrdScaleDetailId=" + "'" + CType(e.Item.FindControl("lblEditGrdScaleDetailId"), Label).Text + "'")
                ''    Dim row() As DataRow = dsTimeClock.Tables("TimeClockExceptionPunch").Select("SlNo=" + "'" + CType(e.Item.FindControl("txtEditSlNo"), TextBox).Text + "'")

                ''    '   get a view with data ordered by ViewOrder
                ''    Dim dv As New DataView(dsTimeClock.Tables("TimeClockExceptionPunch"), "SlNo=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                ''    '   get the id of the item selected by the user
                ''    Dim GrdScaleDetailId As String = CType(e.Item.FindControl("lblGrdScaleDetailId"), Label).Text

                ''    '   get the position of this item and swap viewOrder values with prevous item
                ''    Dim i As Integer
                ''    For i = 1 To dv.Count - 1
                ''        If CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString = GrdScaleDetailId Then
                ''            SwapViewOrders(CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString, CType(dv(i - 1).Row("GrdScaleDetailId"), Guid).ToString)
                ''            Exit For
                ''        End If
                ''    Next

                ''    '   user hit "Down" 
                ''Case "Down"
                ''    '   get a view with data ordered by ViewOrder
                ''    Dim dv As New DataView(GradeScaleDetailsTable, "GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows)

                ''    '   get the id of the item selected by the user
                ''    Dim GrdScaleDetailId As String = CType(e.Item.FindControl("lblGrdScaleDetailId"), Label).Text

                ''    '   get the position of this item and swap viewOrder values with next item
                ''    Dim i As Integer
                ''    For i = 0 To dv.Count - 2
                ''        If CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString = GrdScaleDetailId Then
                ''            SwapViewOrders(CType(dv(i).Row("GrdScaleDetailId"), Guid).ToString, CType(dv(i + 1).Row("GrdScaleDetailId"), Guid).ToString)
                ''            Exit For
                ''        End If
                ''    Next
        End Select
        If Not e.CommandName = "TimePicker" Then
            dsTimeClock = Session("dsTimeClock")
            dgTimeClockPunchByClass.DataSource = dsTimeClock.Tables("TimeClockExceptionPunch")
            dgTimeClockPunchByClass.DataBind()
        End If

        ''   bind datagrid to GradeScaleDetailsTable 
        'BindDataGrid(New DataView(GradeScaleDetailsTable, "GrdScaleId=" + "'" + CType(Session("GrdScaleId"), String) + "'", "ViewOrder asc", DataViewRowState.CurrentRows))


    End Sub

    Protected Sub dgTimeClockPunchByClass_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTimeClockPunchByClass.ItemDataBound

        If e.Item.ItemType = ListItemType.EditItem Then
            Dim list As New DropDownList
            list = DirectCast(e.Item.FindControl("ddlEditClsSectMeeting"), DropDownList)
            Dim dtCls As New DataTable
            dtCls = ViewState("dtClsSectMeeting")
            list.DataSource = dtCls
            list.DataBind()
            For Each item As ListItem In list.Items
                item.Attributes("title") = item.Text
            Next


            list = DirectCast(e.Item.FindControl("ddlEditSpecialCode"), DropDownList)
            dtSpecialCodes = ViewState("dtSpecialCodes")
            list.DataSource = dtSpecialCodes
            list.DataBind()


            Dim ddlEditSpecialCode As DropDownList
            ddlEditSpecialCode = CType(e.Item.FindControl("ddlEditSpecialCode"), DropDownList)
            ddlEditSpecialCode.SelectedValue = CType(e.Item.FindControl("lblPunchCode"), Label).Text

            Dim ddlEditClsMeetingCode As DropDownList
            ddlEditClsMeetingCode = CType(e.Item.FindControl("ddlEditClsSectMeeting"), DropDownList)
            ddlEditClsMeetingCode.SelectedValue = CType(e.Item.FindControl("lblClssetmeetingDesc"), Label).Text

        ElseIf e.Item.ItemType = ListItemType.Footer Then
            Dim list As New DropDownList
            list = DirectCast(e.Item.FindControl("ddlClsSectMeeting"), DropDownList)
            Dim dtCls As New DataTable
            dtCls = ViewState("dtClsSectMeeting")
            list.DataSource = dtCls
            list.DataBind()
            For Each item As ListItem In list.Items
                item.Attributes("title") = item.Text
            Next

            list = DirectCast(e.Item.FindControl("ddlSpecialCode"), DropDownList)
            dtSpecialCodes = ViewState("dtSpecialCodes")
            list.DataSource = dtSpecialCodes
            list.DataBind()


        End If




    End Sub
End Class

