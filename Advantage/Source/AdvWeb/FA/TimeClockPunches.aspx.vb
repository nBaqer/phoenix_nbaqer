
Imports System.Data
Imports FAME.AdvantageV1.BusinessRules
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade.TimeClock
Imports eWorld.UI
Imports FAME.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants
Imports FAME.Advantage.Api.Library.Models
Imports Advantage.AFA.Integration
Imports System.Threading.Tasks
Imports FAME.Advantage.Api.Library.AcademicRecords

Partial Class FA_TimeClockPunches
    Inherits Page

    Dim dtPunch As New DataTable("Punches")
    Dim dsStudent As New DataSet
    Dim dsPunch As New DataSet

    Public exitSub As Boolean = False
    Dim stuEnrollID As String
    Private campusId As String
    Protected MyAdvAppSettings As AdvAppSettings
    Public Class GlobalVariables
        Public Shared StudentEnrollmentsList As List(Of Guid) = New List(Of Guid)()
    End Class
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click

        Dim result As String
        Dim res As String
        Dim attendancePosted As Boolean = False
        Dim i As Integer
        Dim facade As New TimeClockFacade

        Dim tardymarked As Boolean
        tardymarked = chktardy.Checked

        GlobalVariables.StudentEnrollmentsList.Add(New Guid(stuEnrollID))
        If Not IsNumeric(txtschhr.Text) Then
            If (String.IsNullOrEmpty(txtschhr.Text)) Then
                txtschhr.Text = "0"
            Else
                DisplayErrorMessage("Invalid Scheduled Hours")
                Exit Sub
            End If

        ElseIf CDec(txtschhr.Text) > 24 Then
            DisplayErrorMessage("Invalid Scheduled Hours")
            Exit Sub
        End If

        Dim ScheduledHours As Decimal
        ScheduledHours = CDec(txtschhr.Text)
        dtPunch = Session("PunchTable")
        If dtPunch.Rows.Count = 0 Then

            result = facade.DeletePunchandUpdateClockAttendance(stuEnrollID, CDate(ddldate.Text), ScheduledHours)
            If Not result = "" Then
                DisplayErrorMessage(result)
                Exit Sub
            Else
                Dim errMsg As String = ""
                Dim UseTimeClock As Boolean = False
                If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" Then

                    Dim timeClockFacade As New TimeClockFacade
                    timeClockFacade.RecalculateGraduationDateAPI(New List(Of String) From {stuEnrollID}, MyAdvAppSettings)

                Else
                    DisplayErrorMessage("Record Deleted. Click Refresh on the main screen to view the changes made")
                End If
                GetdatasetfromDB(stuEnrollID, CDate(ddldate.Text))
                BindHours()
                hdnsave.Value = "true"
                DisplayhrsinClockAttendancePage()
                Exit Sub
            End If
        ElseIf dtPunch.Rows.Count <= 1 Then
            GetdatasetfromDB(stuEnrollID, CDate(ddldate.Text))
            result = facade.UpdateScheduledHours(stuEnrollID, CDate(ddldate.Text), ScheduledHours, campusId, tardymarked)
            Exit Sub
        End If
        For i = 0 To dtPunch.Rows.Count - 1
            If dtPunch.Rows(i)("BadgeId") = "None" Then
                dtPunch.Rows(i).Delete()
                dtPunch.AcceptChanges()
                Exit For
            End If
        Next
        Session("PunchTable") = dtPunch

        If Not IsInputValid() Then
            DisplayErrorMessage("PunchIn and Punch Out are not valid.")
            Exit Sub
        End If

        result = facade.SavePunchesEntered(dtPunch, tardymarked, ScheduledHours, campusId)
        res = ""

        If Not result = "" And Not res = "" Then
            DisplayErrorMessage(result)
            Exit Sub
        Else
            Dim errMsg As String = ""
            Dim UseTimeClock As Boolean = False
            Dim dtfacade As New GetDateFromHoursBR
            Dim dtExpectedDate As DateTime = dtfacade.GetDateFromHours(stuEnrollID, errMsg, UseTimeClock, campusId)
            If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" Then
                Dim timeClockFacade As New TimeClockFacade
                timeClockFacade.RecalculateGraduationDateAPI(New List(Of String) From {stuEnrollID}, MyAdvAppSettings)
            Else
                DisplayErrorMessage(facade.Log() + "Click Refresh on the main screen to view the posted punches")
            End If

            attendancePosted = True
            GetdatasetfromDB(stuEnrollID, CDate(ddldate.Text))
            BindHours()
            hdnsave.Value = "true"
            DisplayhrsinClockAttendancePage()
            BindData()
        End If

        If GlobalVariables.StudentEnrollmentsList.Count > 0 Then
            TitleIVSapUpdate(GlobalVariables.StudentEnrollmentsList)
        End If

        If (attendancePosted) Then

            Dim afaIntegrationEnabled = If(Not String.IsNullOrEmpty(MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration, campusId)) _
                            , MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration, campusId) _
                                , MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration).ToString).Trim.ToLower = "yes"

            If (afaIntegrationEnabled) Then
                If (Not HttpContext.Current.Session("AdvantageApiToken") Is Nothing) Then
                    Try
                        PostPaymentPeriodAttendanceAFA(campusId, Guid.Parse(stuEnrollID), MyAdvAppSettings, AdvantageSession.UserName)

                    Catch ex As Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(ex)


                    End Try

                End If
            End If
        End If
    End Sub

    Private Sub TitleIVSapUpdate(StudentEnrollmentsList)
        If (Not Session("AdvantageApiToken") Is Nothing) Then
            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim TitleIVSAPRequest As New TitleIVSAPRequest(tokenResponse.ApiUrl, tokenResponse.Token)
            Dim pass As Boolean? = TitleIVSAPRequest.TitleIVSapCheck(StudentEnrollmentsList)
        End If
    End Sub

    Public Sub DisplayhrsinClockAttendancePage()
        Dim Script As String

        Dim cs As ClientScriptManager = Page.ClientScript
        Dim csType As Type = Page.GetType

        If Page.Request.Browser.EcmaScriptVersion.Major >= 1 Then
            Dim scriptBegin As String = "<script type='text/javascript'>window.Onload=populatevalues();function populatevalues(){"
            Script = "localStorage.setItem('curRowstuEnrollId', '" + stuEnrollID + "');"
            Dim CtrlAct As String
            Dim CtrlAbs As String
            Dim CtrlMakeup As String
            Dim CtrlTardy As String
            Dim CtrlScheduled As String
            Dim dayofweek As String
            Dim ValAct As String
            Dim ValAbs As String
            Dim ValMakeup As String
            Dim ValTardy As String
            Dim ValScheduled As String

            If (CDate(ddldate.Text).DayOfWeek = System.DayOfWeek.Sunday) Then
                dayofweek = "0"
            ElseIf (CDate(ddldate.Text).DayOfWeek = System.DayOfWeek.Monday) Then
                dayofweek = "1"
            ElseIf (CDate(ddldate.Text).DayOfWeek = System.DayOfWeek.Tuesday) Then
                dayofweek = "2"
            ElseIf (CDate(ddldate.Text).DayOfWeek = System.DayOfWeek.Wednesday) Then
                dayofweek = "3"
            ElseIf (CDate(ddldate.Text).DayOfWeek = System.DayOfWeek.Thursday) Then
                dayofweek = "4"
            ElseIf (CDate(ddldate.Text).DayOfWeek = System.DayOfWeek.Friday) Then
                dayofweek = "5"
            Else
                dayofweek = "6"
            End If
            CtrlAct = "txtAHD" + dayofweek
            CtrlAbs = "txtABHD" + dayofweek
            CtrlMakeup = "txtMUHD" + dayofweek
            CtrlTardy = "chkT" + dayofweek
            CtrlScheduled = "txtSHD" + dayofweek


            If txtacthr.Text <> "" Then
                If CDec(txtacthr.Text) = 9999.0 Then
                    txtacthr.Text = " "
                ElseIf CDec(txtacthr.Text) = 999.0 Then
                    txtacthr.Text = " "
                End If
            End If

            ValAct = txtacthr.Text
            ValAbs = txtabsenthrs.Text
            ValMakeup = txtmkhr.Text
            ValScheduled = txtschhr.Text
            ValTardy = chktardy.Checked.ToString
            Script += "localStorage.setItem('dayOfWeekVal', '" + dayofweek + "');"
            Script += "localStorage.setItem('ValAct', '" + ValAct + "');"
            Script += "localStorage.setItem('ValAbs', '" + ValAbs + "');"
            Script += "localStorage.setItem('ValMakeup', '" + ValMakeup + "');"
            Script += "localStorage.setItem('ValScheduled', '" + ValScheduled + "');"
            Script += "localStorage.setItem('ValTardy', '" + ValTardy + "');"
            Dim scriptEnd As String = "}</script>"
            cs.RegisterStartupScript(csType, "Populatevalue", scriptBegin + Script + scriptEnd)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        campusId = Request.Params("CampusId").ToString
        stuEnrollID = Request.Params("stuenrollid").ToString
        txtname.Text = Request.Params("StuName").ToString
        Dim startday As String
        startday = Request.Params("Date").ToString
        If Not Page.IsPostBack Then

            AddDaystoDDL(startday)

            GetStudentCompleteDetails(stuEnrollID)
            GetdatasetfromDB(stuEnrollID, startday)

            BindDataGrid()
            BindHours()

            If ddldate.Items.Count = 0 Then
                btnSave.Enabled = False
            End If

        End If

    End Sub
    Private Sub GetdatasetfromDB(ByVal StuEnrollid As String, ByVal schdate As Date)
        Dim facade As New TimeClockFacade
        dsPunch = facade.GetPunchesForaStudentonaGivenDate(StuEnrollid, schdate)

        Session("dsPunch") = dsPunch
        dtPunch = dsPunch.Tables("StudentTimeClockPunches")
        Session("PunchTable") = dtPunch


    End Sub
    Private Sub GetStudentCompleteDetails(ByVal StuEnrollid As String)
        Dim facade As New TimeClockFacade
        dsStudent = facade.GetStudentCompleteDetails(StuEnrollid)
        Session("StudentDetails") = dsStudent

    End Sub
    Private Sub AddDaystoDDL(ByVal StartDay As String)

        Dim Disabled0 As String = Request.Params("Disabled0").ToString()
        Dim Disabled1 As String = Request.Params("Disabled1").ToString()
        Dim Disabled2 As String = Request.Params("Disabled2").ToString()
        Dim Disabled3 As String = Request.Params("Disabled3").ToString()
        Dim Disabled4 As String = Request.Params("Disabled4").ToString()
        Dim Disabled5 As String = Request.Params("Disabled5").ToString()
        Dim Disabled6 As String = Request.Params("Disabled6").ToString()

        Dim StartDate As Date
        StartDate = CType(StartDay, Date)
        If Disabled0 = "0" Then
            ddldate.Items.Add(StartDate)
        End If
        If Disabled1 = "0" Then
            ddldate.Items.Add(DateAdd(DateInterval.Day, 1, StartDate))
        End If
        If Disabled2 = "0" Then
            ddldate.Items.Add(DateAdd(DateInterval.Day, 2, StartDate))
        End If
        If Disabled3 = "0" Then
            ddldate.Items.Add(DateAdd(DateInterval.Day, 3, StartDate))
        End If
        If Disabled4 = "0" Then
            ddldate.Items.Add(DateAdd(DateInterval.Day, 4, StartDate))
        End If
        If Disabled5 = "0" Then
            ddldate.Items.Add(DateAdd(DateInterval.Day, 5, StartDate))
        End If
        If Disabled6 = "0" Then
            ddldate.Items.Add(DateAdd(DateInterval.Day, 6, StartDate))
        End If

    End Sub
    Private Sub BindHours()
        dsPunch = Session("dsPunch")

        If dsPunch.Tables("StudentClockAttendance").Rows.Count > 0 Then
            Dim Acthour As Decimal
            Dim SchHour As Decimal
            Acthour = dsPunch.Tables("StudentClockAttendance").Rows(0)("ActualHours")
            SchHour = dsPunch.Tables("StudentClockAttendance").Rows(0)("SchedHours")

            If Acthour = 9999.0 Or Acthour = 999.0 Then
                Acthour = 0
            End If
            txtacthr.Text = Acthour
            txtschhr.Text = SchHour
            If Acthour > SchHour Then
                txtmkhr.Text = Acthour - SchHour
                txtabsenthrs.Text = 0
            Else
                txtmkhr.Text = 0
                txtabsenthrs.Text = SchHour - Acthour
            End If

            If IsDBNull(dsPunch.Tables("StudentClockAttendance").Rows(0)("isTardy")) Then
                chktardy.Checked = False
            Else
                If dsPunch.Tables("StudentClockAttendance").Rows(0)("isTardy") = True Then
                    chktardy.Checked = True
                Else
                    chktardy.Checked = False
                End If
            End If
        Else
            If ddldate.Items.Count > 0 Then

                Select Case CDate(ddldate.Text).DayOfWeek
                    Case DayOfWeek.Sunday
                        txtschhr.Text = Request.Params("SchHrs0").ToString
                    Case DayOfWeek.Monday
                        txtschhr.Text = Request.Params("SchHrs1").ToString
                    Case DayOfWeek.Tuesday
                        txtschhr.Text = Request.Params("SchHrs2").ToString
                    Case DayOfWeek.Wednesday
                        txtschhr.Text = Request.Params("SchHrs3").ToString
                    Case DayOfWeek.Thursday
                        txtschhr.Text = Request.Params("SchHrs4").ToString
                    Case DayOfWeek.Friday
                        txtschhr.Text = Request.Params("SchHrs5").ToString
                    Case DayOfWeek.Saturday
                        txtschhr.Text = Request.Params("SchHrs6").ToString
                    Case Else
                        txtschhr.Text = 0
                End Select

            Else
                txtschhr.Text = 0

            End If

            txtacthr.Text = 0
            txtmkhr.Text = 0
            txtabsenthrs.Text = 0
            chktardy.Checked = False

        End If
    End Sub
    Private Sub BindDataGrid()
        Dim dr As DataRow
        dtPunch = CType(Session("PunchTable"), DataTable)
        If dtPunch.Rows.Count = 0 Then
            dr = dtPunch.NewRow
            dr("BadgeId") = "None"
            dr("PunchType") = "0"
            dtPunch.Rows.Add(dr)
        End If
        GridView1.DataSource = dtPunch
        GridView1.DataBind()
        Session("PunchTable") = dtPunch
    End Sub

    Private Function IsInputValid() As Boolean
        Dim noofRows As Integer
        Dim noofInpunches As Integer = 0
        Dim noofOutpunches As Integer = 0
        Dim i As Integer


        dtPunch = CType(Session("PunchTable"), DataTable)
        noofRows = dtPunch.Rows.Count
        If noofRows Mod 2 = 1 Then
            DisplayErrorMessage("Every Punch In must have a Punch Out")
            Return False
        End If
        For i = 0 To noofRows - 1
            If dtPunch.Rows(i)("PunchType") = 1 Then
                noofInpunches = noofInpunches + 1
            End If
            If dtPunch.Rows(i)("PunchType") = 2 Then
                noofOutpunches = noofOutpunches + 1
            End If
        Next
        If noofInpunches > 0 And noofOutpunches = 0 Then
            DisplayErrorMessage("No Punch Out found")
            Return False
        End If
        If noofInpunches = 0 And noofOutpunches > 0 Then
            DisplayErrorMessage("No Punch In found")
            Return False
        End If
        If noofInpunches <> noofOutpunches Then
            DisplayErrorMessage("Number of Punch In and Punch Out does not match ")
            Return False
        End If

        Return True
    End Function
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        'CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub


    Protected Sub GridView1_RowCancelingEdit(ByVal sender As Object, ByVal e As GridViewCancelEditEventArgs) Handles GridView1.RowCancelingEdit
        GridView1.EditIndex = -1
        BindData()

    End Sub

    Private Function ValidateData() As String
        Dim i As Integer

        Dim msg As String = ""
        dsStudent = CType(Session("StudentDetails"), DataSet)

        If dsStudent.Tables("StudentSchedule").Rows.Count > 0 Then
            If Not dsStudent.Tables("StudentSchedule").Rows(i)("BadgeNumber") Is Nothing Then
                If dsStudent.Tables("StudentSchedule").Rows(i)("BadgeNumber").ToString.Trim = "" Then
                    msg = msg + "Badge Id not found for student. "
                End If
            Else
                msg = msg + "Badge Id not found for student. "
            End If
        Else
            msg = msg + "Badge Id not found for student. "
        End If

        If CDate(ddldate.Text) > DateTime.Now Then
            msg = msg + "Cannot post for a Future date. "
        End If

        If dsStudent.Tables("StudentSchedule").Rows.Count > 0 Then
            For i = 0 To dsStudent.Tables("StudentSchedule").Rows.Count - 1
                If CDate(ddldate.Text) < CDate(dsStudent.Tables("StudentSchedule").Rows(i)("StartDate")) Then
                    msg = msg + "Cannot post prior to Start date(" + dsStudent.Tables("StudentSchedule").Rows(i)("StartDate") + ")."
                End If
                If CDate(ddldate.Text) > CDate(dsStudent.Tables("StudentSchedule").Rows(i)("ExpGradDate")) Then
                    msg = msg + "Cannot post after Graduation date(" + dsStudent.Tables("StudentSchedule").Rows(i)("ExpGradDate") + ")."
                End If
            Next
        End If

        If dsStudent.Tables("STudentLOAs").Rows.Count > 0 Then
            For i = 0 To dsStudent.Tables("STudentLOAs").Rows.Count - 1
                If CDate(ddldate.Text) > CDate(dsStudent.Tables("STudentLOAs").Rows(i)("StartDate")) And CDate(ddldate.Text) < CDate(dsStudent.Tables("STudentLOAs").Rows(i)("EndDate")) Then
                    msg = msg + "Cannot post for LOA Period(" + dsStudent.Tables("STudentLOAs").Rows(i)("StartDate") + "-" + dsStudent.Tables("STudentLOAs").Rows(i)("EndDate") + ")."
                End If
            Next
        End If

        If dsStudent.Tables("StdSuspensions").Rows.Count > 0 Then
            For i = 0 To dsStudent.Tables("StdSuspensions").Rows.Count - 1
                If CDate(ddldate.Text) > CDate(dsStudent.Tables("StdSuspensions").Rows(i)("StartDate")) And CDate(ddldate.Text) < CDate(dsStudent.Tables("StdSuspensions").Rows(i)("EndDate")) Then
                    msg = msg + "Cannot post for Suspension Period(" + dsStudent.Tables("StdSuspensions").Rows(i)("StartDate") + "-" + dsStudent.Tables("StdSuspensions").Rows(i)("EndDate") + ")."
                End If
            Next
        End If



        If dsStudent.Tables("syHolidays").Rows.Count > 0 Then
            For i = 0 To dsStudent.Tables("syHolidays").Rows.Count - 1
                If CDate(ddldate.Text) >= CDate(dsStudent.Tables("syHolidays").Rows(i)("HolidayStartDate")) And CDate(ddldate.Text) <= CDate(dsStudent.Tables("syHolidays").Rows(i)("HolidayEndDate")) Then
                    If Not MyAdvAppSettings.AppSettings("postattendanceonholiday") Is Nothing Then
                        If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                            msg = msg + "Cannot post on a Holiday(" + dsStudent.Tables("syHolidays").Rows(i)("HolidayStartDate") + "-" + dsStudent.Tables("syHolidays").Rows(i)("HolidayEndDate") + ")."
                        End If
                    Else
                        msg = msg + "Cannot post on a Holiday(" + dsStudent.Tables("syHolidays").Rows(i)("HolidayStartDate") + "-" + dsStudent.Tables("syHolidays").Rows(i)("HolidayEndDate") + ")."
                    End If
                End If
            Next
        End If
        Return msg

    End Function

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles GridView1.RowCommand
        Dim PunchType As Integer
        Dim PunchTime As Date
        Dim SchDate As Date
        Dim StuPunDate As String
        Dim dr As DataRow
        Dim res As String
        Dim badgeno As String
        Dim ScheduleId As String
        If exitSub = True Then
            Exit Sub
        End If
        dsStudent = Session("StudentDetails")

        If e.CommandName = "ADD" Then
            dtPunch = Session("PunchTable")
            If ddldate.Items.Count = 0 Then
                DisplayErrorMessage("No Dates Are Available for Punches")
                Exit Sub
            End If
            SchDate = CDate(ddldate.Text)
            PunchType = CType(GridView1.FooterRow.FindControl("ddlPunchType"), DropDownList).SelectedItem.Value
            If CType(GridView1.FooterRow.FindControl("txtPunchTime"), TimePicker).PostedTime = "" Then
                DisplayErrorMessage("Enter Punch Time")
                Exit Sub
            End If
            PunchTime = CType(GridView1.FooterRow.FindControl("txtPunchTime"), TimePicker).PostedTime
            res = ValidateData()
            If res = "" Then
                dr = dtPunch.NewRow
                badgeno = dsStudent.Tables("StudentSchedule").Rows(0)("BadgeNumber")
                ScheduleId = dsStudent.Tables("StudentSchedule").Rows(0)("ScheduleId").ToString
                dr("BadgeId") = badgeno
                dr("ClockId") = "PN00"
                StuPunDate = SchDate + " " + PunchTime
                Dim concatenatedDate As DateTime = DateTime.Parse(StuPunDate)
                dr("PunchTime") = concatenatedDate
                dr("PunchType") = PunchType
                dr("Status") = "0"
                dr("StuEnrollId") = stuEnrollID
                dr("FromSystem") = False
                dr("ScheduleId") = ScheduleId

                dtPunch.Rows.Add(dr)
                Session("PunchTable") = dtPunch
                GridView1.DataSource = dtPunch
                GridView1.DataBind()
            Else
                DisplayErrorMessage(res)
                Exit Sub
            End If


        End If

        If Not e.CommandName = "TimePicker" Then
            exitSub = True
        End If


    End Sub

    Private Sub BindData()
        GridView1.DataSource = Session("PunchTable")
        GridView1.DataBind()
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            If CType(e.Row.Cells(0).FindControl("lblbadgeId"), Label).Text = "None" Then
                e.Row.Visible = False
            End If
        End If
    End Sub

    Protected Sub GridView1_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        e.Cancel = True
        dtPunch = Session("PunchTable")
        Dim row = GridView1.Rows(e.RowIndex)

        dtPunch.Rows(e.RowIndex).Delete()
        dtPunch.AcceptChanges()

        Session("PunchTable") = dtPunch
        BindData()
    End Sub

    Protected Sub GridView1_RowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs) Handles GridView1.RowEditing
        GridView1.EditIndex = e.NewEditIndex
        BindData()
    End Sub

    Protected Sub GridView1_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs) Handles GridView1.RowUpdating
        dtPunch = CType(Session("PunchTable"), DataTable)
        Dim PunchTime As Date
        Dim SchDate As Date
        Dim StuPunDate As String
        Dim row = GridView1.Rows(e.RowIndex)

        SchDate = CDate(ddldate.Text)
        PunchTime = (CType((row.Cells(0).FindControl("txteditPunchTime")), TimePicker)).PostedTime
        StuPunDate = SchDate + " " + PunchTime
        Dim concatenatedDate As DateTime = DateTime.Parse(StuPunDate)

        dtPunch.Rows(row.DataItemIndex)("PunchTime") = concatenatedDate
        dtPunch.Rows(row.DataItemIndex)("PunchType") = CType(row.Cells(0).FindControl("ddleditPunchType"), DropDownList).SelectedItem.Value

        GridView1.EditIndex = -1
        Session("PunchTable") = dtPunch
        BindData()

    End Sub
    Protected Function GetPunchType(ByVal number As Integer) As String
        If (number = 1) Then
            Return "Punch In"
        Else
            Return "Punch Out"
        End If
    End Function

    Protected Sub ddldate_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddldate.SelectedIndexChanged

        GetdatasetfromDB(stuEnrollID, CDate(ddldate.Text))
        BindDataGrid()
        BindHours()
    End Sub
    Private Async Function PostPaymentPeriodAttendanceAFA(CampusId As String, enrollment As Guid, MyAdvAppSettings As FAME.Advantage.Common.AdvAppSettings, UserName As String) As Task(Of Boolean)
        Dim tokenResponse As TokenResponse = CType(HttpContext.Current.Session("AdvantageApiToken"), TokenResponse)
        Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString

        Return Await New PaymentPeriodHelper(connectionString, tokenResponse.ApiUrl, tokenResponse.Token, UserName) _
                .PostPaymentPeriodAttendance(CampusId, enrollment)
        Return False
    End Function
End Class


