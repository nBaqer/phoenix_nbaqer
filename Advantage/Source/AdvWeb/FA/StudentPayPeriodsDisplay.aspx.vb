﻿Option Strict On

Imports System.Data
Imports System.Web
Imports FAME.AdvantageV1.DataAccess.FAME.ExceptionLayer
Imports FAME.DataAccessLayer
Imports FAME.AdvantageV1.Common
Imports Advantage.Business.Objects
Imports Advantage.Business.Logic.Layer
Imports System.Xml

Partial Class StudentPayPeriodsDisplay
    Inherits BasePage

    Protected studentId As String
    Dim dsResults As DataSet
    Dim dvResults As DataView
    Dim sb As New System.Text.StringBuilder
    Dim db As New DataAccess

    Protected state As AdvantageSessionState

    Protected LeadId As String
    Protected boolSwitchCampus As Boolean = False



    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As Advantage.Business.Objects.StudentMRU

        Dim objStudentState As New Advantage.Business.Objects.StudentMRU

        Try
            MyBase.GlobalSearchHandler(0)

            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                studentId = Guid.Empty.ToString()
            Else
                studentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(studentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)



        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim objStateInfo As New AdvantageStateInfo
        Dim bGridNeeded As Boolean = True
        Dim campusid As String
        campusid = Master.CurrentCampusId

        Try
            'set up database access
            'Dim db As New DataAccess
            db.ConnectionString = GetConnectionStringFromAdvAppSetting("ConString").ToString 'CStr(SingletonAppSettings.AppSettings("ConString"))
            'Dim sb As New System.Text.StringBuilder

            '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
            'Get StudentId and LeadId
            Dim objStudentState As New StudentMRU
            objStudentState = getStudentFromStateObject(652) 'Pass resourceid so that user can be redirected to same page while swtiching students
            If objStudentState Is Nothing Then
                MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
                Exit Sub
            End If
            With objStudentState
                studentId = .StudentId.ToString
                LeadId = .LeadId.ToString

            End With


            '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

            If Page.IsPostBack And (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
                If boolSwitchCampus = True Then
                    CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
                End If
                MyBase.uSearchEntityControlId.Value = ""
                Exit Sub
            End If



            'get the students enrollments and load the enrollments ddl
            dsResults = GetEnrollments(studentId)
            With ddlEnrollmentId
                .DataTextField = "PrgVerDescrip"
                .DataValueField = "stuenrollid"
                .DataSource = dsResults.Tables(0)
                .DataBind()
                .SelectedIndex = 0
            End With

            'remove duplicate entries from the ddl
            For i As Int16 = 0 To CShort(ddlEnrollmentId.Items.Count - 2)
                For j As Int16 = CShort(ddlEnrollmentId.Items.Count - 1) To CShort(i + 1) Step -1
                    If ddlEnrollmentId.Items(i).Value = ddlEnrollmentId.Items(j).Value Then
                        ddlEnrollmentId.Items.RemoveAt(j)
                    End If
                Next
            Next

            'Get the students pay period info for the selected enrollment
            dsResults = GetStudentPPInfo(ddlEnrollmentId.SelectedValue)

            'if there is no pay period data generated the we try to generate it
            If dsResults.Tables(0).Rows.Count < 1 Then
                dsResults.Clear()
                dsResults = GenStudentPP()
                dsResults.Clear()
                dsResults = GetStudentPPInfo(ddlEnrollmentId.SelectedValue)
                If dsResults.Tables(0).Rows.Count < 1 Then
                    ddlEnrollmentId.Visible = True
                    tblTextBoxes.Visible = True
                    'tblEnroll.Style.Add("margin-Left", "10px")
                    lblNoPPMsg.Visible = True
                    Exit Sub
                End If
            End If

            db.CloseConnection()

            'Load up the datagrid
            If bGridNeeded = True Then
                LoadGrid(bGridNeeded)
            End If

            ''Call mru component for students
            'Dim objMRUFac As New MRUFacade
            'Dim ds As New DataSet

            'If CInt(Session("SEARCH")) = 1 Then
            '    ds = objMRUFac.LoadAndUpdateMRU("Students", studentId.ToString(), Session("UserId").ToString(), HttpContext.Current.Request.Params("cmpid"))
            'Else
            '    ds = objMRUFac.LoadMRU("Students", Session("UserId").ToString(), HttpContext.Current.Request.Params("cmpid"))
            'End If

            'objStateInfo.MRUDS = ds
            'state(strVID) = objStateInfo
            ''save current State
            'CommonWebUtilities.SaveAdvantageSessionState(HttpContext.Current, state)

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Throw New BaseException(ex.Message)
        End Try

    End Sub

    Protected Sub ddlEnrollmentId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEnrollmentId.SelectedIndexChanged
        dsResults = GetStudentPPInfo(ddlEnrollmentId.SelectedValue)
        LoadGrid()
    End Sub

    Protected Sub LoadGrid(Optional ByVal bGridNeeded As Boolean = True)
        Dim sFilter As String = "StudentEnrollmentId = '" & ddlEnrollmentId.SelectedValue & "'"
        If bGridNeeded = False Or dsResults.Tables(0).Rows.Count < 1 Then
            dgrdStudentPayPeriodsDisplay.Visible = False
            Me.tbPrgVerDesc.Text = ""
            Me.tbAcadCalType.Text = ""
            Me.tbAcadYrLen.Text = ""
            Me.tbPayPeriodsPerAcadYr.Text = ""
            Me.tbProgLength.Text = ""
            lblNoPPMsg.Visible = True
            Exit Sub
        End If

        Me.tbPrgVerDesc.Text = CStr(dsResults.Tables(0).Rows(0).Item("PrgVerDescrip")).Substring(0, CStr(dsResults.Tables(0).Rows(0).Item("PrgVerDescrip")).IndexOf("-") - 1)
        Me.tbAcadCalType.Text = CStr(dsResults.Tables(0).Rows(0).Item("AcademicCalendarType"))
        Me.tbAcadYrLen.Text = CStr(dsResults.Tables(0).Rows(0).Item("AcaYrLen"))
        Me.tbPayPeriodsPerAcadYr.Text = CStr(dsResults.Tables(0).Rows(0).Item("PayPeriodsPerAcYr"))
        Me.tbProgLength.Text = CStr(dsResults.Tables(0).Rows(0).Item("ProgLength"))

        dvResults = New DataView(dsResults.Tables(0), sFilter, "PaymentPeriod", DataViewRowState.CurrentRows)
        dgrdStudentPayPeriodsDisplay.DataSource = dvResults
        dgrdStudentPayPeriodsDisplay.DataBind()
        dgrdStudentPayPeriodsDisplay.Visible = True
        lblNoPPMsg.Visible = False

    End Sub

    Protected Function GetStudentPPInfo(ByVal enrollid As String) As DataSet
        sb.Clear()
        With sb
            .Append("SELECT ASPP.*, APV.PrgVerDescrip + ' - ' + SSC.StatusCodeDescrip as PrgVerDescrip FROM dbo.arStuPayPeriods ASPP ")
            .Append("INNER JOIN dbo.arStuEnrollments ASE ON ASPP.StudentEnrollmentID = ASE.StuEnrollId ")
            .Append("INNER JOIN dbo.arPrgVersions APV ON ASE.PrgVerId = APV.PrgVerId ")
            .Append("INNER JOIN dbo.syStatusCodes SSC ON ASE.StatusCodeId = SSC.StatusCodeId ")
            .Append("WHERE ASPP.StudentEnrollmentID = '")
            '.Append("(SELECT stuenrollid FROM dbo.arStuEnrollments where StudentId = '")
            .Append(enrollid)
            .Append("'")
        End With
        db.OpenConnection()
        dsResults = db.RunParamSQLDataSet(sb.ToString)
        Return dsResults
    End Function

    Protected Function GenStudentPP() As DataSet
        Dim db As New DataAccess
        db.ConnectionString = GetConnectionStringFromAdvAppSetting("ConString").ToString 'CStr(SingletonAppSettings.AppSettings("ConString"))
        Dim sb As New System.Text.StringBuilder
        Dim dt As DataTable
        Dim dr As DataRow

        With sb
            .Append("select stuenrollid from arStuEnrollments where studentid = '" & studentId & "'")
        End With
        db.OpenConnection()
        dsResults = db.RunParamSQLDataSet(sb.ToString)
        db.CloseConnection()

        sb.Clear()
        dt = dsResults.Tables(0)
        For Each dr In dt.Rows
            With sb
                .Append("EXEC USP_AR_StuEnrollPayPeriods '")
                .Append(dr("stuenrollid"))
                .Append("'")
            End With
            'Dim a As String = dr("stuenrollid").ToString()
            db.OpenConnection()
            dsResults = db.RunParamSQLDataSet(sb.ToString)
            db.CloseConnection()
        Next

        Return dsResults

    End Function

    Protected Function GetEnrollments(ByVal studentid As String) As DataSet
        With sb
            sb.Clear()
            .Append("SELECT ASE.stuenrollid, APV.PrgVerDescrip + ' - ' + SSC.StatusCodeDescrip as PrgVerDescrip, ")
            .Append("CASE WHEN SSC.SysStatusId IN (7, 9, 10, 11, 20, 21, 22) ")
            .Append("THEN 1 ELSE 0 END AS IsCurrentEnrollment ")
            .Append("FROM dbo.arStuEnrollments ASE ")
            .Append("INNER JOIN dbo.arPrgVersions APV ON ASE.PrgVerId = APV.PrgVerId ")
            .Append("INNER JOIN dbo.syStatusCodes SSC ON ASE.StatusCodeId = SSC.StatusCodeId ")
            .Append("WHERE ASE.StuEnrollID IN ")
            .Append("(SELECT stuenrollid FROM dbo.arStuEnrollments where StudentId = '")
            .Append(studentid)
            .Append("') ")
            .Append("ORDER BY iscurrentenrollment desc")
        End With
        db.OpenConnection()
        dsResults = db.RunParamSQLDataSet(sb.ToString)
        Return dsResults
    End Function

End Class