﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="PostLabWork.aspx.vb" Inherits="FA_PostLabWork" %>
<%@ MasterType  VirtualPath="~/NewSite.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
   <script language="javascript" src="../js/CheckAll.js" type="text/javascript"/>
   <script type="text/javascript">

       function OldPageResized(sender, args) {
           $telerik.repaintChildren(sender);
       }

   </script>

    <style type="text/css">
    
        .ListFrameClockFilter
        {
	        border-right: #000000 1px solid;
	        border-top: #000000 1px solid;
	        border-bottom: #000000 1px solid;
	        vertical-align: top;
	        width: 35%;
	        background-color: #E9EDF2;
        }
        
        .scrollright
        {
	        overflow: auto;
	        width: 100%;
	        bottom: 45px;
	        position: absolute;
	        white-space: nowrap;
	        float: left;
	        top: 155px;
	        height: expression(document.body.clientHeight - 196 + "px");
	        left: 36%;
	        padding: 16px;
	        margin-right: 1px;
        }
        
        .RptLabWork
        {          
            text-align: left;  
            height: expression(document.body.clientHeight - 340 + "px");
            overflow: auto;            
        }        
        .rptheaders
        {
            font: bold 11px verdana;
	        color: #000066;	        
	        background-color: #E9EDF2;
	        padding: 6px 6px 2px 6px;	        
	        vertical-align: top;	
	        text-align: center;
	        border-right: 1px solid #ebebeb;
        }
        .rptitem
        {
            font: bold 11px verdana;
	        font: normal 11px verdana;
	        color: #333333;
	        text-align: center;
	        padding: 6px 6px;
	        border-right: 1px solid #ebebeb;
	        border-bottom: 1px solid #ebebeb;
        }

        #dhtmltooltip
        {
        font: normal 11px verdana;
        color: #000066;
        position: absolute;
        left: -300px;
        width: 125px;
        border: 1px solid black;
        padding: 2px;
        background-color: lightyellow;
        visibility: hidden;
        z-index: 100;
        /*Remove below line to remove shadow. Below line should always appear last within this CSS*/
        filter: progid:DXImageTransform.Microsoft.Shadow(color=gray,direction=135);
        }

        #dhtmlpointer
        {
        position:absolute;
        left: -300px;
        z-index: 101;
        visibility: hidden;
        } 
           
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" 
    VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
    <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
     <%-- Add class ListFrameTop2 to the table below --%>

        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <!-- begin leftcolumn -->
            <tr>
                <td class="listframeclockfilter">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">
                        <tr>
                            <td class="listframetop2">
                                <asp:Panel ID="panFilter" runat="server">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                        <br />
                                            <td class="employersearch" style="width: 25%">
                                                <asp:Label ID="lblTerm" CssClass="label" runat="server" Text="Term" />
                                                 <span class="label" style="color: red; width: 10px">*</span>
                                            </td>
                                            <td class="employersearch2" style="width: 75%">
                                                <asp:DropDownList ID="ddlTerm" runat="server" Width="90%" CssClass="dropdownlist"
                                                    AutoPostBack="True" ClientIDMode="Static" />
                                               
                                            </td>
                                        </tr>
                                        <tr id="Tr1" runat="server" visible="false">
                                            <td class="employersearch" style="width: 25%">
                                                <asp:Label ID="lblShowEndedTerms" runat="server" Text="Show expired terms?" />
                                                <asp:CheckBox ID="cbShowExpiredTerms" runat="server" AutoPostBack="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="employersearch" style="width: 25%">
                                                <asp:Label ID="lblClass" CssClass="label" runat="server" Text="Class" />
                                                <span class="label" style="color: red; width: 10px">*</span>
                                            </td>
                                            <td class="employersearch2" style="width: 75%">
                                                <asp:DropDownList ID="ddlClass" runat="server" Width="90%" CssClass="dropdownlist"
                                                    AutoPostBack="True" />
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="employersearch" style="width: 25%">
                                                <asp:Label ID="lblLab" CssClass="label" runat="server" Text="Lab" />
                                                   <span class="label" style="color: red; width: 10px">*</span>
                                            </td>
                                            <td class="employersearch2" style="width: 75%">
                                                <asp:DropDownList ID="ddlLab" runat="server" Width="90%" CssClass="dropdownlist" />
                                                                                         </td>
                                        </tr>
                                        <tr>
                                            <td class="employersearch" style="width: 25%">
                                            </td>
                                            <td class="employersearch2" style="width: 75%; text-align: center">
                                                <asp:Button ID="btnBuildList" runat="server" Text="Build List -->" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td class="listframebottom">
                                <div class="scrollleftfltr4rows">
                                    &nbsp;</div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>



    </telerik:RadPane>


    <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button
                                        ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False">
                                    </asp:Button>
                                    </td>
                        </tr>
                    </table>
        <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
        <td class="detailsframe">
        <div class="scrollright2">
        <!-- begin content table-->


                  <asp:Panel ID="pnlRHS" runat="server" Width="100%" />



        <!-- end content table-->
        </div>
        </td>
        </tr>
        </table>

    </telerik:RadPane>
    </telerik:RadSplitter>
        <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
        </asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
            Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
        <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
        </asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
            ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

