<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AttendanceComments.aspx.vb" Inherits="FA_AttendanceComments" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <base target=_self />
    <title>Post Comments</title>
    <link href="../css/localhost.css" type="text/css" rel="stylesheet"/>
    <style>
        .pageheader
{
  background-color:#E9EDF2;
  background-image:url(images/header_bg.gif);
  color:#504C39;
  font-family:Verdana;
  font-size:11px;
  text-align:center;
  border-top:solid 1px #ebebeb;
  border-left:solid 1px #ebebeb;
  border-bottom:solid 1px #ebebeb;
  border-right:solid 1px #ebebeb;
  padding: 2px;
  height:25px;
}
</style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table id="Table3" width="100%" cellpadding="0" cellspacing="0" class="pageheader">
        	<tr valign="middle">
		        <th style="text-align: left" width="60%" class="LabelBold" nowrap>&nbsp;&nbsp;&nbsp;Post Comments</th>
		        <th style="text-align: right" width="10%" nowrap class="Label"><a class="close" href="#" onClick="top.close()">X Close</a></th>
		    </tr>
		</table>
		<p></p>
        <p></p>
        <TABLE class="contenttable" cellSpacing="0" cellPadding="0" width="60%" align="center">
            <tr>
                <td  nowrap align="left"><asp:Label ID="lblStudentName" runat=server CssClass="Label">Student<font color="red">*</font></asp:Label></td>
                <td width="20px">&nbsp;</td>
                <td  nowrap align="left"><asp:DropDownList ID="ddlStuEnrollId" runat=server Autopostback=true CssClass="DropDownLists" Width="350px"></asp:DropDownList></td>
            </tr>
            <tr height="10px"><td>&nbsp;</td></tr>
            <TR>
                <TD nowrap align="left">
                    <asp:Label ID="lblDates" runat=server CssClass="Label">Attendance Date<font color="red">*</font></asp:Label>
                </td>
                <td width="20px">&nbsp;</td>
                <td nowrap align="left">
                    <asp:DropDownList ID="ddlDates" runat=server CssClass="DropDownLists" Width="230px" AutoPostBack=true></asp:DropDownList>
                </td>
            </tr>
            <tr height="10px"><td>&nbsp;</td></tr>
            <tr>
                <td  align="left" valign="top">
                    <asp:Label ID="lblComments" runat=server CssClass="Label" Text="Comments"></asp:Label>
                </td>
                <td width="20px">&nbsp;</td>
                <td align="left">
                    <asp:TextBox ID="txtComments" runat=server rows=5 TextMode="MultiLine" CssClass="TextBox" Width="400px" MaxLength="240"></asp:TextBox>            
                </td>
            </tr>
            <tr>
                <td   align="left" valign="top"></td>
                <td width="20px">&nbsp;</td>
                <td align="left">
                    <asp:Button ID="btnPostComments" runat=server Text="Post Comments"  />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
