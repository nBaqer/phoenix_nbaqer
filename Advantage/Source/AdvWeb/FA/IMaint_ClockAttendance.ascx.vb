' ===============================================================================
' Form_ClockAttendance
' RHS for posting clock hour attendance
' Uses IMaintFormBase interface
' ===============================================================================
' Copyright (C) 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' IMaintFormBase properties
'       ParentId = None
'       ObjId = List of params
'           prgverid={guidval}&stuname={strval}&stustatus={guidval}&sdate={dateval}&edate={dateval}
'           &usetc={str(true|false)}&badgenum={strval}

Imports System.Data
Imports System.Globalization
Imports System.Threading.Tasks
Imports FAME.AdvantageV1.BusinessFacade
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports FAME.AdvantageV1.Common.AR
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.Common
Imports System.Xml
Imports Advantage.AFA.Integration
Imports Telerik.Web.UI
Imports FAME.Advantage.Domain.MultiTenant.Infrastructure.API
Imports FAME.Advantage.Api.Library.AcademicRecords
Imports FAME.Advantage.Api.Library.Models
Imports FAME.Advantage.Common.LINQ.Entities
Imports FAME.AdvantageV1.DataAccess.TimeClock
Imports FAME.Advantage.Domain.SystemStuff
Imports FAME.AdvantageV1.Common.Enumerations

Partial Class FA_IMaint_ClockAttendance
    Inherits System.Web.UI.UserControl
    Implements IMaintFormBase
    'Dim StudentDS As New DataSet
    'Dim AttendanceDS As New DataSet
    Dim exceptionFlag As Boolean
    ''Added by SAraswathi
    Dim PostZeroFlag As Boolean
    Dim CampusId As String
    Dim UserId As String
    Private pObj As New UserPagePermissionInfo
    Protected MyAdvAppSettings As AdvAppSettings

    Public Class GlobalVariables
        Public Shared StudentEnrollmentsList As List(Of Guid) = New List(Of Guid)()
    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            CampusId = CampusObjects.GetQueryParameterValueStrCampusId(Request)
            UserId = AdvantageSession.UserState.UserId.ToString
            ViewState("UserId") = UserId
            ViewState("CampusId") = CampusId
            ''Commented by Saraswathi on April 20 2009
            ''Since, IMport File is made as a seperate link on the menu, and it is a new individual page

            'Dim fac As New FAME.AdvantageV1.BusinessFacade.TimeClock.TimeClockFacade
            'Dim Resource As String
            'Resource = "Import TimeClock"
            'If Not CampusId Is Nothing Then
            '    If CampusId = "" Then
            '        CampusId = Nothing
            '    End If
            'End If
            'pObj = fac.GetImportFilePermissions(UserId, Resource, CampusId)

            'If Not Session("UseTimeClock") Is Nothing Then
            '    If Session("UseTimeClock") = True Then
            '        If pObj.HasFull = True Then
            '            btnImportFile.Visible = True
            '        Else
            '            btnImportFile.Visible = False
            '        End If
            '    End If
            'End If



            If Not Page.IsPostBack Then
                ' add javascript to the confirm "Post by Exception"
                exceptionFlag = False
                Dim sb As New StringBuilder()
                sb.Append("if(confirm('")
                sb.Append("This will set all unposted records in the current week to the scheduled attendance unit.\r\n")
                sb.Append("Are you sure you want to proceed?")
                sb.Append("')){}else{return false}")
                btnPostByException.Attributes.Add("onclick", sb.ToString())

                PostZeroFlag = False
                'Dim sb1 As New StringBuilder()
                'sb1.Append("if(confirm('")
                'sb1.Append("This will set all unposted records in the current week to Zero and Saved.\r\n")
                'sb1.Append("Are you sure you want to proceed?")
                'sb1.Append("')){}else{return false}")
                'btnpostzero.Attributes.Add("onclick", sb1.ToString())

                ' add javascript to open up the "Import Time Clock" file
                Dim url As String = "ImportTimeClockFile.aspx"
                Dim js As String = ARCommon.GetJavsScriptPopup(url, 900, 600, Nothing)
                Me.btnImportFile.Attributes.Add("onclick", js)

                'Dim url1 As String = "AttendanceComments.aspx"
                'Dim js5 As String = ARCommon.GetJavsScriptPopup(url1, 550, 250, Nothing)

                'open new window and pass parameters



                'Me.btnPostComments.Attributes.Add("onclick", js5)
            Else
                'Dim url1 As String = "AttendanceComments.aspx"
                'Dim js5 As String = ARCommon.GetJavsScriptPopup(url1, 550, 250, Nothing)
                'Me.btnPostComments.Attributes.Add("onclick", js5)
            End If

            'If SingletonAppSettings.AppSettings("ImportAttendanceFromExcel").Trim.ToString.ToLower = "yes" Then
            '    btnImportFromExcel.Visible = True
            'Else
            '    btnImportFromExcel.Visible = False
            'End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub

#Region "Import File and Post By Exception Handlers"
    Protected Sub btnImportFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportFile.Click
        BindForm(ObjId)
    End Sub
    Protected Sub SetHoursToScheduled(ByVal dow As Integer, ByVal rpi As RepeaterItem)
        'Dim txt As eWorld.UI.NumericBox = Nothing
        Dim txt As TextBox = Nothing
        Dim ctlId As String = "txtD" + dow.ToString()

        Dim obj As Object = rpi.FindControl(ctlId)
        Dim obj1 As Object
        If obj Is Nothing Then Return ' Failed to find the control

        'txt = CType(obj, eWorld.UI.NumericBox)
        txt = CType(obj, TextBox)
        If txt.Text <> "" Then Return ' value already posted, don't do anything
        'If txt.ReadOnly Then Return ' Cannot post on this date
        'If txt.BackColor.Name <> "0" Then Return

        Dim maxHrsCtlId As String = "hfS" + dow.ToString()
        Dim maxHrsSchedId As String = "hfAP" + dow.ToString
        Dim hfDateId As String = "hfD" + dow.ToString()
        'Dim hfDate As HiddenField = CType(FindControl(hfDateId), HiddenField)
        Dim hfDate As HiddenField = TryCast((From item As RepeaterItem In rptAttendance.Controls
                                             Where item.ItemType = ListItemType.Header).SingleOrDefault.FindControl(hfDateId), HiddenField)
        Dim postbyexceptionId As String = "hfException" + dow.ToString
        Dim hfException As HiddenField = CType(rpi.FindControl(postbyexceptionId), HiddenField)
        Dim hDate As Date = CType(hfDate.Value, Date)

        obj = rpi.FindControl(maxHrsCtlId)
        obj1 = rpi.FindControl(maxHrsSchedId)
        If obj Is Nothing Then Return ' Error, max hours has not been defined
        Dim hfMaxHrs As HiddenField = CType(obj, HiddenField)
        Dim hfMaxSchedHrs As HiddenField = CType(obj1, HiddenField)
        Dim dtGradDate As Date
        If Not CType(rpi.FindControl("hfGradDate"), HiddenField).Value = "" Then
            dtGradDate = CType(CType(rpi.FindControl("hfGradDate"), HiddenField).Value, Date)
        Else
            dtGradDate = "01/01/1900"
        End If
        Dim dtDateDetermined As Date = "01/01/1900"
        If Not CType(rpi.FindControl("hfDateDetermined"), HiddenField).Value = "" Then
            dtDateDetermined = CType(CType(rpi.FindControl("hfDateDetermined"), HiddenField).Value, Date)
        Else
            dtDateDetermined = "01/01/1900"
        End If
        Dim dtStartDate As Date = CType(rpi.FindControl("hfStudentStartDate"), HiddenField).Value

        ' disable the cell if the day is a Holiday

        If hDate > Date.Now Then
            txt.ReadOnly = True 'bUseTimeClock
            txt.ToolTip = "Cannot post on a future date."
            txt.Text = ""
            hfException.Value = "yes"
            Exit Sub
        End If

        ' disable the cell if the day is less than the student's start date
        Dim StudentStartDate As Date = CType(rpi.FindControl("hfStudentStartDate"), HiddenField).Value
        If Not DBNull.Value.Equals(StudentStartDate) Then
            If hDate < StudentStartDate Then
                txt.ReadOnly = True
                txt.ToolTip = "Entries are disabled because this day is before the student's start date."
                txt.Text = ""
                hfException.Value = "yes"
                Exit Sub
            End If
        End If


        '''' Code changes by kamalesh Ahuja on 23 August 2010 to resolve mantis issue id 19598
        ' disable the cell if the day is greater than the student's Expected Graduation Date
        If Not DBNull.Value.Equals(dtGradDate) Then
            If hDate > dtGradDate Then
                txt.ReadOnly = True
                txt.ToolTip = "Entries are disabled because this day is after the student's Expected Graduation date."
                txt.Text = ""
                hfException.Value = "yes"
                Exit Sub
            End If
        End If
        '''''''''''''''''''

        Dim hfSysStatusId As HiddenField = CType(rpi.FindControl("hfSysStatusId"), HiddenField)
        Dim sysStatusId As StudentStatusCodes = StudentStatusCodes.Currently_Attending
        If hfSysStatusId.Value <> "" Then sysStatusId = CType(hfSysStatusId.Value, StudentStatusCodes)
        If Not (sysStatusId = StudentStatusCodes.Currently_Attending Or
                sysStatusId = StudentStatusCodes.Future_Start Or
                sysStatusId = StudentStatusCodes.LOA Or
                sysStatusId = StudentStatusCodes.Probation Or
                sysStatusId = StudentStatusCodes.Suspension Or
                sysStatusId = StudentStatusCodes.Suspended Or
                sysStatusId = StudentStatusCodes.Dropped) Then
            txt.ReadOnly = True 'bUseTimeClock '
            If Not ((DateDiff(DateInterval.Day, dtStartDate, hDate) >= 0 And dtGradDate <> "01/01/1900" And hDate <= dtGradDate) Or
               (DateDiff(DateInterval.Day, dtStartDate, hDate) >= 0 And dtDateDetermined <> "01/01/1900" And hDate <= dtDateDetermined)) Then
                txt.ReadOnly = True 'bUseTimeClock '
                txt.ToolTip = "Cannot enter attendance because student's status is 'non-enrolled'"
                hfException.Value = "yes"
                Return ' we're done with color rules                
            End If
        End If

        'Response.write(e.Item.DataItem("StuEnrollId"))
        Dim hfEnrollId As String = CType(rpi.FindControl("hfStuEnrollId"), HiddenField).Value
        If sysStatusId = StudentStatusCodes.Dropped Then
            'get the date student was dropped
            Dim strDateDropped As String = (New AttendanceFacade).GetLDAForDroppedStudent(hfEnrollId.ToString)
            If strDateDropped <> "" Then
                If hDate > CDate(strDateDropped) Then
                    txt.ReadOnly = True
                    txt.ToolTip = "Entries are disabled because student was dropped."
                    txt.Text = ""
                    Exit Sub
                End If
            End If
        End If
        If IsHoliday(hDate) Then
            If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                txt.ReadOnly = True
                txt.ToolTip = "Entries are disabled because this day is a holiday."
                txt.Text = ""
                hfException.Value = "yes"
                Exit Sub
            Else
                'If advantage allows posting attendance on holiday, post it as "A".
                If CType(rpi.FindControl("hfattType"), HiddenField).Value.Trim() = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
                    txt.Text = "A"
                    hfException.Value = "yes"
                    Exit Sub
                Else
                    hfMaxHrs.Value = "0.00"
                    txt.Text = "0.00"
                    hfException.Value = "yes"
                    Exit Sub
                End If
            End If
        End If
        If CType(rpi.FindControl("hfattType"), HiddenField).Value.Trim() = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
            If hfMaxHrs.Value <> "" AndAlso hfMaxHrs.Value <> "0" AndAlso CType(hfMaxHrs.Value, Decimal) <> 0.0 Then
                txt.Text = "P"
                hfException.Value = "yes"
                Exit Sub
            Else
                txt.Text = "A"
                hfException.Value = "yes"
                Exit Sub
            End If
        Else
            If hfMaxHrs.Value <> "" AndAlso hfMaxHrs.Value <> "0" AndAlso CType(hfMaxHrs.Value, Decimal) <> 0.0 Then
                txt.Text = hfMaxHrs.Value
                hfException.Value = "yes"
            Else
                txt.Text = "0.00"
                hfException.Value = "yes"
            End If
        End If
    End Sub
    Protected Sub btnPostByException_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPostByException.Click
        Dim attFlag As Boolean = False
        Session("exceptionFlag") = True
        For Each rpi As RepeaterItem In Me.rptAttendance.Items
            ' only update the hours if student's schedule is not using a timeclock
            Dim hfUseTimeClock As HiddenField = CType(rpi.FindControl("hfUseTimeClock"), HiddenField)
            If hfUseTimeClock.Value.ToLower() = "false" Then
                ' set the hours to the scheduled hours for each day of the week
                SetHoursToScheduled(0, rpi)
                SetHoursToScheduled(1, rpi)
                SetHoursToScheduled(2, rpi)
                SetHoursToScheduled(3, rpi)
                SetHoursToScheduled(4, rpi)
                SetHoursToScheduled(5, rpi)
                SetHoursToScheduled(6, rpi)
            ElseIf CType(rpi.FindControl("hfattType"), HiddenField).Value.Trim() = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
                attFlag = True
                SetHoursToScheduled(0, rpi)
                SetHoursToScheduled(1, rpi)
                SetHoursToScheduled(2, rpi)
                SetHoursToScheduled(3, rpi)
                SetHoursToScheduled(4, rpi)
                SetHoursToScheduled(5, rpi)
                SetHoursToScheduled(6, rpi)
            End If
        Next
        ' alert the end user that they must click "Save" to commit the changes
        If attFlag = True Then
            ARCommon.Alert(Me.Page, "Attendance has been posted based on their default schedule.\r\nPlease click Save to commit the changes.")
        Else
            ARCommon.Alert(Me.Page, "Hours have been set to their scheduled amount.\r\nPlease click Save to commit the changes.")
        End If
    End Sub
#End Region

#Region "Repeater Helpers"
    Function GetStudentToolTip(ByVal e As RepeaterItemEventArgs) As String
        Dim Timeclock As String
        Dim sb As New StringBuilder()
        'sb.AppendFormat("Program:  {0}{1}{2}", vbTab, e.Item.DataItem("ProgDescrip").ToString(), vbCrLf)
        sb.AppendFormat("<li>Prg Ver:  {0}{1}{2}", vbTab, e.Item.DataItem("PrgVerDescrip").ToString(), "</li>")
        ''Added by saraswathi on april 21 2009
        sb.AppendFormat("<li>Student Start Date:  {0}{1}{2}", vbTab, Format(e.Item.DataItem("StudentStartDate"), "MM/dd/yyyy"), "</li>")

        sb.AppendFormat("<li>Schedule: {0}{1}{2}", vbTab, e.Item.DataItem("ScheduleDescrip").ToString(), "</li>")
        If e.Item.DataItem("UseTimeClock").ToString() = "True" Then
            Timeclock = "Yes"
        Else
            Timeclock = "No"
        End If
        sb.AppendFormat("<li>Time clock?:   {0}{1}", vbTab, Timeclock)
        If e.Item.DataItem("UseTimeClock").ToString() = "True" Then
            sb.AppendFormat("<li>Badge#:   {0}{1}{2}", vbTab, e.Item.DataItem("BadgeNumber").ToString(), "</li>")
        End If
        Dim SSN As String
        Dim maskedSSN As String
        Dim PartSSN As String
        SSN = e.Item.DataItem("SSN").ToString
        If SSN.Length > 0 Then
            PartSSN = SSN.Substring((SSN.Length - 4), 4)
            maskedSSN = "***-***-" + PartSSN
        Else
            PartSSN = ""
            maskedSSN = ""
        End If
        sb.AppendFormat("<li>SSN#:   {0}{1}{2}", vbTab, maskedSSN, "</li>")
        sb.AppendFormat("<li>StudentIdentifier: {0}{1}{2}", vbTab, e.Item.DataItem("StudentNumber").ToString(), "</li>")

        sb.AppendFormat("<li>Status:   {0}{1}", vbTab, e.Item.DataItem("StatusCodeDescrip").ToString())
        Return sb.ToString().Replace("'", " ")
    End Function
    ''' <summary>
    ''' Changes the background color of s specific cell
    ''' </summary>
    ''' <param name="i"></param>
    ''' <param name="e"></param>
    ''' <param name="color"></param>
    ''' <remarks></remarks>
    Protected Sub SetCellColor(ByVal i As Integer, ByVal e As RepeaterItemEventArgs, ByVal color As System.Drawing.Color)
        Dim ctlId As String = "txtD" + i.ToString()
        'Dim txt As eWorld.UI.NumericBox = CType(e.Item.FindControl(ctlId), eWorld.UI.NumericBox)
        Dim txt As TextBox = CType(e.Item.FindControl(ctlId), TextBox)
        txt.BackColor = color

        Dim tdId As String = "i" + i.ToString()
        CType(e.Item.FindControl(tdId), HtmlTableCell).BgColor = System.Drawing.ColorTranslator.ToHtml(color)
    End Sub
    ''' <summary>
    ''' Sets the actual hours for a specific day/student.
    ''' Acutal hours are duplicated in a visible field (such as txtD0) and
    ''' a hidden field (such as hfA0).
    ''' </summary>
    ''' <param name="i"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub BindDay(ByVal i As Integer, ByVal e As RepeaterItemEventArgs)

        ' start by clearing out the hours for this day
        'txt.Text = ""
        'hfActual.Value = ""
        'hfTardy.Value = ""

        Dim ctlId As String = "txtD" + i.ToString()
        'Dim txt As eWorld.UI.NumericBox = CType(e.Item.FindControl(ctlId), eWorld.UI.NumericBox)
        Dim txt As TextBox = CType(e.Item.FindControl(ctlId), TextBox)
        txt.ReadOnly = False
        Dim ctlTardy As String = "hfT" + i.ToString
        ctlId = "hfA" + i.ToString()
        Dim hfTardy As HiddenField = CType(e.Item.FindControl(ctlTardy), HiddenField)
        Dim hfActual As HiddenField = CType(e.Item.FindControl(ctlId), HiddenField)
        ' Get the sched hours hiddenfield
        Dim hfMaxHrsId As String = "hfS" + i.ToString()
        Dim hfMaxHrs As HiddenField = CType(e.Item.FindControl(hfMaxHrsId), HiddenField)

        Dim hfMaxSchedId As String = "hfAP" + i.ToString
        'Dim hfSched As String = "hfSched" + i.ToString
        Dim hfMaxHrsSched As HiddenField = CType(e.Item.FindControl(hfMaxHrsId), HiddenField)

        'Dim hfSchedCtrl As HiddenField = CType(e.Item.FindControl(hfSched), HiddenField)
        Dim hfExceptionId As String = "hfException" + i.ToString
        Dim hfException As HiddenField = CType(e.Item.FindControl(hfExceptionId), HiddenField)

        'Dim sql As String = String.Format("StuEnrollId='{0}' and ScheduleId='{1}' and RecordDate='{2}'", _
        '            e.Item.DataItem("StuEnrollid"), e.Item.DataItem("ScheduleId"), Filter_StartDate.AddDays(i).ToShortDateString())

        ''Modified by Saraswathi lakshmanan on Jan 27 2010
        ''To fix the issue 18371: QA: Changing the schedule for a student is impacting the attendance posted for the previous schedule(s). 
        ''The attendance details related to the previous schedules are also displayed
        Dim sql As String = String.Format("StuEnrollId='{0}'and RecordDate='{1}'",
                    e.Item.DataItem("StuEnrollid"), Filter_StartDate.AddDays(i).ToShortDateString())


        Dim ds As New DataSet
        If Not Session("AttendanceDS") Is Nothing Then
            ds = Session("AttendanceDS")
        End If
        Dim dt As DataTable = Nothing
        If ds.Tables.Count > 0 Then dt = ds.Tables(0)
        Dim drs() As DataRow = dt.Select(sql)
        If drs.Length > 0 Then
            Dim dr As DataRow = drs(0)
            txt.Text = dr("ActualHours").ToString()
            'hfSchedCtrl.Value = dr("ScheduleId").ToString()
            If CType(e.Item.FindControl("hfattType"), HiddenField).Value.Trim() = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
                If dr("SchedHours").ToString().Trim() <> "" Then
                    If CInt(dr("SchedHours").ToString().Trim()) >= "1" Then
                        hfMaxHrs.Value = "1"
                    ElseIf CInt(dr("SchedHours").ToString().Trim()) = "0" Then
                        hfMaxHrs.Value = "0"
                    Else
                        hfMaxHrs.Value = ""
                    End If
                    'If day is a holiday set the day value to 0
                Else
                    hfMaxHrs.Value = ""
                End If
                If dr("PostByException") = "yes" Then
                    hfException.Value = "yes"
                Else
                    hfException.Value = ""
                End If
                Try
                    If (dr("ActualHours").ToString().ToLower = "null" Or dr("ActualHours") Is System.DBNull.Value Or dr("ActualHours") = "999.00") Then
                        txt.Text = "0"
                        hfActual.Value = txt.Text
                        Exit Try
                    ElseIf dr("ActualHours") = "9999.00" Then
                        txt.Text = ""
                        hfActual.Value = txt.Text
                        Exit Try
                    End If
                    If txt.Text <> "" Then
                        If CInt(txt.Text.Trim()) = "1" Then
                            If dr("IsTardy").ToString.Trim() = "True" Then
                                txt.Text = "T" '"P (T)"
                                hfActual.Value = txt.Text
                            Else
                                txt.Text = "P"
                                hfActual.Value = txt.Text
                            End If
                        ElseIf CInt(txt.Text.Trim()) = "0" Then
                            If (hfMaxHrs.Value = "") Then
                                txt.Text = ""
                                hfActual.Value = txt.Text
                            Else
                                txt.Text = "A"
                                hfActual.Value = txt.Text
                            End If
                        Else
                            txt.Text = ""
                            hfActual.Value = txt.Text
                        End If
                        hfTardy.Value = dr("IsTardy").ToString
                        If txt.Text = "T" Then
                            hfTardy.Value = "1"
                        End If
                    Else
                        txt.Text = ""
                        hfActual.Value = txt.Text
                        hfTardy.Value = dr("IsTardy").ToString
                    End If
                    If hfActual.Value.ToString.Trim() = "999.00" Then
                        txt.Text = ""
                        hfActual.Value = ""
                    End If
                Catch ex As System.Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                End Try
            Else
                hfActual.Value = txt.Text
                hfMaxHrs.Value = dr("SchedHours").ToString()
                hfTardy.Value = dr("IsTardy").ToString
                'If dr("Tardy").ToString = "True" Then
                '    txt.Text = txt.Text & "(T)"
                'End If
                If hfActual.Value.ToString.Trim() = "999.00" Or hfActual.Value.ToString.Trim() = "9999.00" Then
                    txt.Text = ""
                    hfActual.Value = ""
                End If
            End If
            Dim MaxHrColName As String = "dw" + i.ToString() + "total"
            hfMaxHrsSched.Value = hfMaxHrs.Value 'e.Item.DataItem(MaxHrColName).ToString()
        Else
            Dim MaxHrColName As String = "dw" + i.ToString() + "total"
            hfMaxHrs.Value = e.Item.DataItem(MaxHrColName).ToString()
        End If

        ''Added by SAraswathi to find if the date is Scheduled or not
        FindSchedHrs(i, e)

        'If FindCellColor(i, e) = "LightGrey" Or FindCellColor(i, e) = "LightSlateGrey" Then
        '    hfMaxHrs.Value = "0.00"
        '    hfMaxHrsSched.Value = "0.00"
        'End If
    End Sub
    Protected Sub BindBackColorForDay(ByVal i As Integer, ByVal e As RepeaterItemEventArgs, ByVal attType As String)
        Dim hfDateId As String = "hfD" + i.ToString()

        Dim hfDate As HiddenField = TryCast((From item As RepeaterItem In rptAttendance.Controls
                                             Where item.ItemType = ListItemType.Header).SingleOrDefault.FindControl(hfDateId), HiddenField)

        'Dim hfDate As HiddenField = CType(FindControl(hfDateId), HiddenField)
        Dim hDate As Date = CType(hfDate.Value, Date)
        Dim dtGradDate As Date = "01/01/1900"
        Dim hfHolId As String = "hfHol" + i.ToString()
        Dim hfHolFld As HiddenField = CType(FindControl(hfHolId), HiddenField)

        'DE9544 5/6/2013 Janet Robinson Add flag field for disabled day
        Dim hfDisabledId As String = "hfDisabled" + i.ToString()
        Dim hfDisabled As HiddenField = CType(e.Item.FindControl(hfDisabledId), HiddenField)
        hfDisabled.Value = "0"

        If Not CType(e.Item.FindControl("hfGradDate"), HiddenField).Value = "" Then
            dtGradDate = CType(CType(e.Item.FindControl("hfGradDate"), HiddenField).Value, Date)
        Else
            dtGradDate = "01/01/1900"
        End If
        Dim dtDateDetermined As Date = "01/01/1900"
        If Not CType(e.Item.FindControl("hfDateDetermined"), HiddenField).Value = "" Then
            dtDateDetermined = CType(CType(e.Item.FindControl("hfDateDetermined"), HiddenField).Value, Date)
        Else
            dtDateDetermined = "01/01/1900"
        End If
        Dim dtStartDate As Date = CType(e.Item.FindControl("hfStudentStartDate"), HiddenField).Value
        Dim ctlId As String = "txtD" + i.ToString()
        'Dim txt As eWorld.UI.NumericBox = CType(e.Item.FindControl(ctlId), eWorld.UI.NumericBox)
        Dim txt As TextBox = CType(e.Item.FindControl(ctlId), TextBox)

        txt.ReadOnly = False
        Dim bUseTimeClock As Boolean = CType(e.Item.FindControl("hfUseTimeClock"), HiddenField).Value.ToString.ToLower = "true"
        txt.ReadOnly = bUseTimeClock ' start out with readonly tied to the timeclock        

        Dim hfMaxHrsId As String = "hfS" + i.ToString()
        Dim hfMaxHrs As HiddenField = CType(e.Item.FindControl(hfMaxHrsId), HiddenField)

        Dim hfExceptionId As String = "hfException" + i.ToString
        Dim hfException As HiddenField = CType(e.Item.FindControl(hfExceptionId), HiddenField)

        ' check if the cell's date is greater than today
        ' if so, make the back color "Light Gray"
        txt.ReadOnly = False
        If hDate > Date.Now Then
            txt.ReadOnly = True 'bUseTimeClock
            SetCellColor(i, e, Drawing.Color.LightGray)
            txt.ToolTip = "Cannot post on a future date."
            hfDisabled.Value = "1"
            Return ' we're done with color rules
        End If

        ' disable the cell if the day is less than the student's start date
        If Not DBNull.Value.Equals(e.Item.DataItem("StudentStartDate")) Then
            If hDate < CType(e.Item.DataItem("StudentStartDate"), Date) Then
                txt.ReadOnly = True
                txt.ToolTip = "Entries are disabled because this day is before the student's start date."
                SetCellColor(i, e, Drawing.Color.LightGray)
                hfDisabled.Value = "1"
                Return
            End If
        End If


        ' disable the cell if the day is after Graduation date
        If dtGradDate <> "01/01/1900" Then
            If hDate > dtGradDate Then
                txt.ReadOnly = True
                txt.ToolTip = "Entries are disabled because this day is after the student's Graduation date."
                SetCellColor(i, e, Drawing.Color.LightGray)
                hfDisabled.Value = "1"
                Return
            End If
        End If

        ' disable the cell if the day is a Holiday
        If IsHoliday(hDate) Then
            If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                txt.ReadOnly = True
                txt.ToolTip = "Entries are disabled because this day is a holiday."
                If txt.Text = "" Then
                    SetCellColor(i, e, Drawing.Color.LightGray)
                    hfDisabled.Value = "1"
                End If
            Else
                txt.ReadOnly = False
                txt.Enabled = True
                txt.ToolTip = "Enter P,A or T"
            End If
        End If

        ' if no hours are allocated for the day
        ' then make the back color Light Gray
        If hfMaxHrs.Value = "" Or hfMaxHrs.Value = "0" Then
            ' check if there is a value for the hours here
            ' this should not be the case as there are no hours allocated
            txt.ReadOnly = False
            Dim hfSysStatusId1 As HiddenField = CType(e.Item.FindControl("hfSysStatusId"), HiddenField)
            Dim sysStatusId1 As StudentStatusCodes = StudentStatusCodes.Currently_Attending
            If hfSysStatusId1.Value <> "" Then sysStatusId1 = CType(hfSysStatusId1.Value, StudentStatusCodes)
            'If Not (sysStatusId1 = StudentStatusCodes.Future_Start Or _
            '        sysStatusId1 = StudentStatusCodes.LOA Or _
            '        sysStatusId1 = StudentStatusCodes.Probation Or _
            '        sysStatusId1 = StudentStatusCodes.Suspension Or _
            '        sysStatusId1 = StudentStatusCodes.Suspended Or _
            '        sysStatusId1 = StudentStatusCodes.Dropped Or _
            '        sysStatusId1 = StudentStatusCodes.Transfer_Out) Then
            If Not (sysStatusId1 = StudentStatusCodes.Currently_Attending Or
                sysStatusId1 = StudentStatusCodes.Future_Start Or
                sysStatusId1 = StudentStatusCodes.LOA Or
                sysStatusId1 = StudentStatusCodes.Probation Or
                sysStatusId1 = StudentStatusCodes.Suspension Or
                sysStatusId1 = StudentStatusCodes.Suspended Or
                sysStatusId1 = StudentStatusCodes.Dropped Or
                sysStatusId1 = StudentStatusCodes.Transfer_Out) Then
                If Not ((DateDiff(DateInterval.Day, dtStartDate, hDate) >= 0 And dtGradDate <> "01/01/1900" And hDate <= dtGradDate) Or
                       (DateDiff(DateInterval.Day, dtStartDate, hDate) >= 0 And dtDateDetermined <> "01/01/1900" And hDate <= dtDateDetermined)) Then
                    txt.ReadOnly = True 'bUseTimeClock '
                    txt.ToolTip = "Cannot enter attendance because student's status is 'non-enrolled'"
                    Me.SetCellColor(i, e, Drawing.Color.LightSlateGray)
                    hfDisabled.Value = "1"
                End If
            End If
            If sysStatusId1 = StudentStatusCodes.Dropped Then
                'get the date student was dropped
                Dim strDateDropped As String = (New AttendanceFacade).GetLDAForDroppedStudent(e.Item.DataItem("StuEnrollid").ToString, 12)
                If strDateDropped <> "" Then
                    If hDate >= CDate(strDateDropped) Then
                        txt.ReadOnly = True
                        txt.ToolTip = "Entries are disabled because student was dropped."
                        Me.SetCellColor(i, e, Drawing.Color.LightSlateGray)
                        hfDisabled.Value = "1"
                    End If
                End If
            End If

            If sysStatusId1 = StudentStatusCodes.Transfer_Out Then
                'get the date student was transferred
                Dim strDateDropped As String = (New AttendanceFacade).GetLDAForDroppedStudent(e.Item.DataItem("StuEnrollid").ToString, 19)
                If strDateDropped <> "" Then
                    If hDate >= CDate(strDateDropped) Then
                        txt.ReadOnly = True
                    End If
                End If
            End If
            If txt.Text <> "" Then
                txt.ToolTip = "Attendance have been posted but the schedule does not have any attendance allocated for this day."
                If txt.Text.ToString.ToLower = "p" Or txt.Text.ToString.ToLower = "t" Then
                    Me.SetCellColor(i, e, Drawing.Color.LightSalmon)
                End If
            Else
                txt.ToolTip = "The schedule for this student does not have any attendance allocated for this day."
                'Me.SetCellColor(i, e, Drawing.Color.LightGray)
            End If
            'Disable the cell if day is holiday and web.config entry is set to no.
            If IsHoliday(hDate) Then
                If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                    ' txt.Text = "Off"
                    txt.ReadOnly = True
                    txt.ToolTip = "Entries are disabled because this day is a holiday."
                    If txt.Text = "" Then
                        SetCellColor(i, e, Drawing.Color.LightGray)
                        hfDisabled.Value = "1"
                    End If
                End If
            End If
            Return ' we're done with color rules
        End If

        ' Disable fields if students status is in a non-enrolled state
        Dim hfSysStatusId As HiddenField = CType(e.Item.FindControl("hfSysStatusId"), HiddenField)
        Dim sysStatusId As StudentStatusCodes = StudentStatusCodes.Currently_Attending
        If hfSysStatusId.Value <> "" Then sysStatusId = CType(hfSysStatusId.Value, StudentStatusCodes)

        If Not (sysStatusId = StudentStatusCodes.Currently_Attending Or
                sysStatusId = StudentStatusCodes.Future_Start Or
                sysStatusId = StudentStatusCodes.LOA Or
                sysStatusId = StudentStatusCodes.Probation Or
                sysStatusId = StudentStatusCodes.Suspension Or
                sysStatusId = StudentStatusCodes.Suspended Or
                sysStatusId = StudentStatusCodes.Dropped Or
                sysStatusId = StudentStatusCodes.Transfer_Out) Then
            If Not ((DateDiff(DateInterval.Day, dtStartDate, hDate) >= 0 And dtGradDate <> "01/01/1900" And hDate <= dtGradDate) Or
                   (DateDiff(DateInterval.Day, dtStartDate, hDate) >= 0 And dtDateDetermined <> "01/01/1900" And hDate <= dtDateDetermined)) Then
                txt.ReadOnly = True 'bUseTimeClock '
                txt.ToolTip = "Cannot enter attendance because student's status is 'non-enrolled'"
                Me.SetCellColor(i, e, Drawing.Color.LightSlateGray)
                hfDisabled.Value = "1"
                Return ' we're done with color rules                
            End If
        End If

        If (sysStatusId = StudentStatusCodes.Currently_Attending Or
                sysStatusId = StudentStatusCodes.Future_Start Or
                sysStatusId = StudentStatusCodes.LOA Or
                sysStatusId = StudentStatusCodes.Probation Or
                sysStatusId = StudentStatusCodes.Suspension Or
                sysStatusId = StudentStatusCodes.Suspended) Then
            'TODO: get student LOA dates
            ' make backup color something new for LOA
            ' txt.ToolTip
            ''Added by Saraswathi lakshmanan on Nov 17 2008
            ''To disable entry for the dates where the student was in LOA
            Dim IsLOADate As Boolean
            IsLOADate = (New AttendanceFacade).IsStudentLOADate(e.Item.DataItem("StuEnrollid").ToString, hDate)
            If IsLOADate = True Then
                ' txt.Text = "LOA"
                txt.ReadOnly = True 'bUseTimeClock '
                txt.ToolTip = "Cannot enter attendance because student's status is 'LOA'"
                Me.SetCellColor(i, e, Drawing.Color.LightGray)
                hfDisabled.Value = "1"
                Return ' we're done with color rules    
            End If
        End If

        ''Similarly disable the entry for suspended time frame of the student
        ''Added by Saraswathi lakshmanan on Nov 17 2008
        ''To disable entry for the dates where the student was in Suapension
        If (sysStatusId = StudentStatusCodes.Currently_Attending Or
                sysStatusId = StudentStatusCodes.Future_Start Or
                sysStatusId = StudentStatusCodes.LOA Or
                sysStatusId = StudentStatusCodes.Probation Or
                sysStatusId = StudentStatusCodes.Suspension Or
                sysStatusId = StudentStatusCodes.Suspended) Then
            Dim IsSuspendedDate As Boolean
            IsSuspendedDate = (New AttendanceFacade).IsStudentSuspendedDate(e.Item.DataItem("StuEnrollid").ToString, hDate)
            If IsSuspendedDate = True Then
                ' txt.Text = "Sus"
                txt.ReadOnly = True 'bUseTimeClock '
                txt.ToolTip = "Cannot enter attendance because student's status is 'Suspended'"
                Me.SetCellColor(i, e, Drawing.Color.LightGray)
                hfDisabled.Value = "1"
                Return ' we're done with color rules    
            End If
        End If

        'Response.write(e.Item.DataItem("StuEnrollId"))
        If sysStatusId = StudentStatusCodes.Dropped Then
            'get the date student was dropped
            Dim strDateDropped As String = (New AttendanceFacade).GetLDAForDroppedStudent(e.Item.DataItem("StuEnrollid").ToString, 12)
            If strDateDropped <> "" Then
                If hDate >= CDate(strDateDropped) Then
                    txt.ReadOnly = True
                    txt.ToolTip = "Entries are disabled because student was dropped."
                    Me.SetCellColor(i, e, Drawing.Color.LightSlateGray)
                    hfDisabled.Value = "1"
                End If
            End If
        End If

        If sysStatusId = StudentStatusCodes.Transfer_Out Then
            'get the date student was transferred
            Dim strDateDropped As String = (New AttendanceFacade).GetLDAForDroppedStudent(e.Item.DataItem("StuEnrollid").ToString, 19)
            If strDateDropped <> "" Then
                If hDate >= CDate(strDateDropped) Then
                    txt.ReadOnly = True
                    txt.ToolTip = "Entries are disabled because student was transferred."
                    Me.SetCellColor(i, e, Drawing.Color.LightSlateGray)
                    hfDisabled.Value = "1"
                End If
            End If
        End If

        ' if we get here, then the field is a valid field
        ' with allocated hours.  Let's update the tooltip so
        ' it gives meaningful information
        Dim maxHrs As Decimal = 0.0
        Dim aHrs As Decimal = 9999.0
        Dim strPA As String = ""
        ' Dim DoesProgVersionTrackTardy As Boolean = False

        Dim boolDayIsTardy As Boolean = False
        Dim dstardy As New DataSet


        If Not attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
            If attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.MinutesGuid Or attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.UseTimeClockGuid Then
                Dim dwValue As Integer = CType(hDate.DayOfWeek, Integer)

                dstardy = (New FAME.AdvantageV1.BusinessFacade.TimeClock.TimeClockFacade).IsStudentTardy(e.Item.DataItem("StuEnrollid").ToString, hDate, e.Item.DataItem("PrgVerId").ToString)
                ' boolDayIsTardy = IsDayMarkedForCheckTardyIn(e.Item.DataItem("StuEnrollid").ToString, e.Item.DataItem("PrgVerId").ToString, dwValue)
                If dstardy.Tables("arStudentClockAttendance").Rows.Count > 0 Then
                    If dstardy.Tables("arStudentClockAttendance").Rows(0)(0) > 0 Then
                        boolDayIsTardy = True
                    End If
                End If
                'If dstardy.Tables("arPrgversions").Rows.Count > 0 Then
                '    If dstardy.Tables("arPrgversions").Rows(0)(0) = 1 Then
                '        '  DoesProgVersionTrackTardy = True
                '    End If
                'End If

            End If
            If hfMaxHrs.Value <> "" Then maxHrs = CType(hfMaxHrs.Value, Decimal)
            If Not txt.Text = "" Then aHrs = CType(txt.Text, Decimal)
            If aHrs <> 9999.0 AndAlso aHrs > maxHrs Then
                If attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.UseTimeClockGuid And boolDayIsTardy = True Then
                    SetCellColor(i, e, Drawing.Color.BlanchedAlmond) ' Tardy Hours
                ElseIf attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.MinutesGuid And boolDayIsTardy = True Then
                    SetCellColor(i, e, Drawing.Color.BlanchedAlmond) ' Tardy Hours
                Else
                    SetCellColor(i, e, Drawing.Color.LightSalmon) ' makeup hours
                End If
            ElseIf aHrs <> 9999.0 AndAlso aHrs <= maxHrs And boolDayIsTardy = True Then
                SetCellColor(i, e, Drawing.Color.BlanchedAlmond) ' Tardy Hours
            ElseIf aHrs = 0.0 And maxHrs <> 0.0 Then
                SetCellColor(i, e, Drawing.Color.LightBlue) ' Absent hours

                'ElseIf aHrs <> 9999.0 AndAlso aHrs < maxHrs And boolDayIsTardy = False Then
                '    SetCellColor(i, e, Drawing.Color.LightBlue) ' Absent hours
                'ElseIf CInt(aHrs) = "0" AndAlso aHrs <> 9999.0 AndAlso (maxHrs - aHrs) >= 1.0 Then
                '    SetCellColor(i, e, Drawing.Color.LightBlue) ' absent hours
            End If
        Else
            If txt.Text <> "" Then strPA = txt.Text
            If hfMaxHrs.Value <> "" Then maxHrs = CType(hfMaxHrs.Value, Decimal)
            If strPA <> "" AndAlso maxHrs = "0.00" AndAlso (strPA.Substring(0, 1).ToLower = "p" Or strPA.Substring(0, 1).ToLower = "t") Then
                SetCellColor(i, e, Drawing.Color.LightSalmon) ' makeup hours
            ElseIf strPA <> "" AndAlso strPA.Substring(0, 1).ToLower = "a" Then 'AndAlso hfException.Value = "" Then 'if attendance is posted by exception then don't set back color for absent
                If maxHrs <> "0.00" And Not IsHoliday(hDate) Then
                    SetCellColor(i, e, Drawing.Color.LightBlue) 'absent hours
                End If
                ''Added by Saraswathi to show the tardy color
                ''Added on mArch 3 2009
            ElseIf strPA <> "" AndAlso maxHrs <> "0.00" AndAlso (strPA.Substring(0, 1).ToLower = "t") Then
                SetCellColor(i, e, Drawing.Color.BlanchedAlmond) ' tardy Hours

            End If
        End If

        'Disable the cell if day is holiday and web.config entry is set to no.
        If IsHoliday(hDate) Then
            If MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                txt.ReadOnly = True
                txt.ToolTip = "Entries are disabled because this day is a holiday."
                If txt.Text = "" Then
                    SetCellColor(i, e, Drawing.Color.LightGray)
                    hfDisabled.Value = "1"
                End If
            End If
        End If

        If attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.UseTimeClockGuid Then
            txt.ToolTip = String.Format("Max scheduled hours is {0}", hfMaxHrs.Value)
        ElseIf attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.MinutesGuid Then
            txt.ToolTip = String.Format("Max scheduled hours is {0}", hfMaxHrs.Value)
        ElseIf attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid And (sysStatusId <> StudentStatusCodes.Dropped And sysStatusId <> StudentStatusCodes.Transfer_Out) Then
            txt.ToolTip = "Enter P,A or T"
        End If

        'If program version uses timeclock then disable data entry
        If Session("TimeClock") = True Then
            txt.ReadOnly = True
            txt.ToolTip = "Entries are disabled because the program version uses time clock"
        End If
    End Sub
    'Private Function IsDayMarkedForCheckTardyIn(ByVal StuEnrollId As String, ByVal PrgVerId As String, ByVal dw As Integer) As Boolean
    '    Dim ds As New DataSet
    '    ds = SchedulesFacade.CheckTardyIn(PrgVerId, StuEnrollId)
    '    If ds.Tables(0).Rows.Count >= 1 Then
    '        For Each dr As DataRow In ds.Tables(0).Rows
    '            If dw = CType(dr("dw"), Integer) Then
    '                Return True
    '            Else
    '                Return False
    '            End If
    '        Next
    '    End If
    'End Function
    Private Function IsHoliday(ByVal hDate As Date) As Boolean
        'If Session("Holidays") Is Nothing Then
        Session("Holidays") = (New HolidayFacade).GetAllHolidays(CampusId)
        'End If

        'scan over all values of the holidays table
        Dim holidayTable As System.Data.DataTable = CType(Session("Holidays"), DataSet).Tables(0)
        For i As Integer = 0 To holidayTable.Rows.Count - 1
            If hDate >= holidayTable.Rows(i)("HolidayStartDate") And hDate <= holidayTable.Rows(i)("HolidayEndDate") Then
                Return True
            End If
        Next
        Return False
    End Function
    ''' <summary>
    ''' Binds the weekly total column
    ''' This should be called after binding all the day values
    ''' via BinDay
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub BindTotal(ByVal e As RepeaterItemEventArgs, Optional ByVal UnitTypeDescrip As String = "")
        Dim tot As Decimal = 0.0
        Dim totPresent As Integer
        For i As Integer = 0 To 6
            Dim ctlId As String = "txtD" + i.ToString()
            'Dim txt As eWorld.UI.NumericBox = CType(e.Item.FindControl(ctlId), eWorld.UI.NumericBox)
            Dim txt As TextBox = CType(e.Item.FindControl(ctlId), TextBox)
            If Not UnitTypeDescrip = "present" Then
                If txt.Text.Length > 0 Then
                    If InStr(txt.Text, "(") >= 1 Then
                        tot += CType(Mid(txt.Text, 1, InStr(txt.Text, "(") - 1), Decimal)
                    Else
                        tot += CType(txt.Text, Decimal)
                    End If
                End If
            Else
                Dim ctlActual As String = "hfA" + i.ToString()
                Dim strActual As String = ""
                If Not CType(e.Item.FindControl(ctlActual), HiddenField).Value = "" Then
                    strActual = CType(e.Item.FindControl(ctlActual), HiddenField).Value
                End If
                If strActual <> "" Then
                    If (strActual.ToLower.Substring(0, 1) = "p" Or strActual.ToLower.Substring(0, 1) = "t") Then totPresent += 1
                End If
            End If
        Next
        Dim lbl As Label = CType(e.Item.FindControl("lblTotal"), Label)
        If UnitTypeDescrip.ToString.ToLower = "present" Then
            If totPresent = 0 Then
                lbl.Text = "0"
            Else
                lbl.Text = totPresent
            End If
        Else
            If tot = 0 Then
                lbl.Text = "0"
            Else
                lbl.Text = tot.ToString("#.##")
            End If
        End If
    End Sub
    ''' <summary>
    ''' Method is called for each item in the repeater.
    ''' Gives us a change to set the header to the current date range.
    ''' Gives us a chance to bind attendance information to the grid.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rptAttendance_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptAttendance.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            ' Bind header - this must be done before binding the repeater
            ' as the ItemDataBound for the repeater relies on values in the header
            If Filter_StartDate = Date.MinValue Then Return
            Dim tDate As Date = Format(Filter_StartDate, "d")
            For i As Integer = 0 To 6
                CType(e.Item.FindControl("lblD" & i), Label).Text = String.Format("{0}<br />{1}", tDate.DayOfWeek.ToString.Substring(0, 3), tDate.Day)
                CType(e.Item.FindControl("hfD" & i), HiddenField).Value = tDate.ToString()
                tDate = tDate.AddDays(1)
            Next
        End If

        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            ' update hidden values that will be needed when doing a save
            Dim PrgVerId As String = e.Item.DataItem("PrgVerId").ToString()
            Dim ScheduleId As String = e.Item.DataItem("ScheduleId").ToString()
            Dim UnitTypeDescrip As String = e.Item.DataItem("UnitTypeDescrip").ToString.Substring(0, 7).ToLower
            'Dim studentStartDate As Date = CType(e.Item.DataItem("StudentStartDate").ToString, Date)
            Dim studentStartDate As Date
            Dim expGradDate As Date
            Dim dtDateDetermined As Date

            If Not e.Item.DataItem("ExpGradDate") Is System.DBNull.Value Then
                expGradDate = CType(e.Item.DataItem("ExpGradDate"), Date).ToString
            Else
                expGradDate = "01/01/1900"
            End If

            If Not e.Item.DataItem("DateDetermined") Is System.DBNull.Value Then
                dtDateDetermined = CType(e.Item.DataItem("DateDetermined"), Date).ToString
            Else
                dtDateDetermined = "01/01/1900"
            End If

            If Not e.Item.DataItem("StudentStartDate") Is System.DBNull.Value Then
                studentStartDate = CType(e.Item.DataItem("StudentStartDate"), Date).ToString
            Else
                studentStartDate = "01/01/1900"
            End If

            CType(e.Item.FindControl("hfStudentStartDate"), HiddenField).Value = studentStartDate
            CType(e.Item.FindControl("hfUnitTypeDescrip"), HiddenField).Value = UnitTypeDescrip
            CType(e.Item.FindControl("hfStuEnrollId"), HiddenField).Value = e.Item.DataItem("StuEnrollId").ToString()
            CType(e.Item.FindControl("hfScheduleId"), HiddenField).Value = ScheduleId
            CType(e.Item.FindControl("hfSysStatusId"), HiddenField).Value = e.Item.DataItem("SysStatusId").ToString()
            CType(e.Item.FindControl("hfUseTimeClock"), HiddenField).Value = e.Item.DataItem("UseTimeClock").ToString()
            CType(e.Item.FindControl("hfSource"), HiddenField).Value = e.Item.DataItem("Source").ToString()
            CType(e.Item.FindControl("hfGradDate"), HiddenField).Value = expGradDate
            CType(e.Item.FindControl("hfDateDetermined"), HiddenField).Value = dtDateDetermined


            Dim stuenrollid = e.Item.DataItem("StuEnrollId").ToString()


            ' add javascript to let the user click on the "To Maintenance Screen" button
            ''Modified by saraswathi on feb 10 2009
            ''The resourceId of 1 was passed, now it is corrected to 511
            Dim url As String = String.Format("../MaintPopup.aspx?mod={0}&resid=511&cmpid={1}&pid={2}&objid={3}&ascx={4}",
            Session("mod"), ARCommon.GetCampusID(), PrgVerId, ScheduleId, "~/AR/IMaint_SetupSchedule.ascx")

            Dim js As String = ARCommon.GetJavsScriptPopup(url, 900, 600, Nothing)
            Dim ib As LinkButton = CType(e.Item.FindControl("ibToMaintPage_Schedule"), LinkButton)
            ib.Attributes.Add("onclick", js)

            'Dim url5 As String = "AttendanceComments.aspx?StartDate=" & hfD0.Value + "&StuEnrollId=" + e.Item.DataItem("StuEnrollId").ToString() + "&StudentName=" + e.Item.DataItem("LastName") + ", " + e.Item.DataItem("FirstName")
            'Dim js5 As String = ARCommon.GetJavsScriptPopup(url5, 550, 250, Nothing)
            'Dim lnkImage12 As ImageButton = CType(e.Item.FindControl("lnkImage1"), ImageButton)
            'lnkImage12.Attributes.Add("onclick", js5)


            ' Format the name of the student
            Dim name As String = String.Format("{0}, {1} {2}", e.Item.DataItem("LastName"), e.Item.DataItem("FirstName"), e.Item.DataItem("MiddleName"))
            Dim lbl As HyperLink = CType(e.Item.FindControl("btnStudentName"), HyperLink)
            lbl.Text = name
            name = name.Replace("'", "%26apos;")

            'lbl.ToolTip = GetStudentToolTip(e)

            '' build of a javascript string to display an info popup
            'ib = CType(e.Item.FindControl("ibStudentInfo"), ImageButton)
            'Dim sb As New StringBuilder()
            'sb.Append("ddrivetip('")
            'sb.Append(GetStudentToolTip(e))
            'sb.Append("', 300);")
            'ib.Attributes.Add("onmouseover", sb.ToString)
            'ib.Attributes.Add("onmouseout", "hideddrivetip()")

            Dim RdTooltip As RadToolTip
            RdTooltip = CType(e.Item.FindControl("RadToolTip1"), RadToolTip)
            Dim TooltipString As New StringBuilder

            TooltipString.AppendLine("<div style='padding:5px;'>")
            TooltipString.Append(" <div style='margin-top:10px;'> Program Version:  " + e.Item.DataItem("PrgVerDescrip").ToString() + "</div>")
            TooltipString.Append(" <div> Student Start Date:  " + Format(e.Item.DataItem("StudentStartDate"), "MM/dd/yyyy") + "</div>")
            TooltipString.Append(" <div> Schedule :   " + e.Item.DataItem("ScheduleDescrip").ToString() + "</div>")

            Dim Timeclock As String
            If e.Item.DataItem("UseTimeClock").ToString() = "True" Then
                Timeclock = "Yes"
            Else
                Timeclock = "No"
            End If
            TooltipString.Append(" <div> Time clock? :   " + Timeclock + "</div>")
            If e.Item.DataItem("UseTimeClock").ToString() = "True" Then
                TooltipString.Append(" <div> Badge# :   " + e.Item.DataItem("BadgeNumber").ToString() + "</div>")
            End If

            Dim SSN As String
            Dim maskedSSN As String
            Dim PartSSN As String
            SSN = e.Item.DataItem("SSN").ToString
            If SSN.Length > 0 Then
                PartSSN = SSN.Substring((SSN.Length - 4), 4)
                maskedSSN = "***-***-" + PartSSN
            Else
                PartSSN = ""
                maskedSSN = ""
            End If
            TooltipString.Append(" <div> SSN# :   " + maskedSSN + "</div>")
            TooltipString.Append("<div> StudentIdentifier:" + e.Item.DataItem("StudentNumber").ToString() + "</div>")
            TooltipString.Append("<div> Status:" + e.Item.DataItem("StatusCodeDescrip").ToString() + "</div>")
            TooltipString.AppendLine("</div>")
            RdTooltip.Text = TooltipString.ToString

            '<%# DataBinder.Eval(Container, "DataItem.StatusDescrip")%></div>  
            ' <div><b>Student Start Date:</b>
            '<%# DataBinder.Eval(Container, "DataItem.StudentStartDate")%></div>  
            ' <div><b>Time Clock:</b>
            '<%# Session("ClsSectionUsesTimeClock")%></div>  
            ' <div><b>SSN#:</b>
            '<%# DataBinder.Eval(Container, "DataItem.SSN")%></div>  
            '</div>""

            ' Clear out the hours for this row
            Dim attType As String = FAME.AdvantageV1.BusinessFacade.AR.SchedulesFacade.PrgVerAttendanceType(PrgVerId)
            CType(e.Item.FindControl("hfattType"), HiddenField).Value = attType
            For i As Integer = 0 To 6
                BindDay(i, e)
                BindBackColorForDay(i, e, attType)
            Next

            'TrackTardies
            Dim intTardyTrack As Integer = 0
            Try
                intTardyTrack = CType(e.Item.DataItem("TrackTardy").ToString, Integer)
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)
                intTardyTrack = 0
            End Try

            Dim url2 As New StringBuilder()
            url2.Append("javascript:OnEditHours(")

            url2.Append("'")
            url2.Append(name)
            url2.Append("',")

            url2.Append("'")
            url2.Append(Me.Filter_StartDate.ToString("MM-dd-yyyy"))
            url2.Append("',")

            url2.Append("'")
            url2.Append(CType(e.Item.FindControl("hfUseTimeClock"), HiddenField).Value)
            url2.Append("',")

            url2.Append("'")
            url2.Append(ScheduleId)
            url2.Append("',")

            url2.Append("'")
            url2.Append(UnitTypeDescrip)
            url2.Append("',")

            url2.Append("'")
            url2.Append(studentStartDate)
            url2.Append("',")

            url2.Append("'")
            url2.Append(CType(e.Item.FindControl("hfSysStatusId"), HiddenField).Value)
            url2.Append("',")

            url2.Append("'")
            url2.Append(dtDateDetermined)
            url2.Append("',")

            url2.Append("'")
            url2.Append(CType(e.Item.FindControl("hfStuEnrollId"), HiddenField).Value)
            url2.Append("',")

            url2.Append("'")
            url2.Append(CType(e.Item.FindControl("hfSource"), HiddenField).Value)
            url2.Append("',")

            url2.Append("'")
            url2.Append(expGradDate)
            url2.Append("',")

            url2.Append("'")
            url2.Append(intTardyTrack)
            url2.Append("',")

            url2.Append("'")
            url2.Append(PrgVerId)
            url2.Append("',")

            url2.Append("'")
            url2.Append(ViewState("UserId"))
            url2.Append("',")

            url2.Append("'")
            url2.Append(ViewState("CampusId"))
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("txtD0").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("txtD1").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("txtD2").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("txtD3").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("txtD4").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("txtD5").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("txtD6").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfS0").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfS1").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfS2").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfS3").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfS4").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfS5").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfS6").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfA0").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfA1").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfA2").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfA3").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfA4").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfA5").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfA6").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfT0").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfT1").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfT2").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfT3").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfT4").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfT5").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfT6").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfAP0").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfAP1").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfAP2").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfAP3").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfAP4").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfAP5").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfAP6").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfDisabled0").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfDisabled1").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfDisabled2").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfDisabled3").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfDisabled4").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfDisabled5").ClientID)
            url2.Append("',")

            url2.Append("'")
            url2.Append(e.Item.FindControl("hfDisabled6").ClientID)
            url2.Append("'")

            url2.Append(")")

            'url2.AppendFormat("javascript:OnEditHours('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}',",
            '            name, Me.Filter_StartDate.ToString("MM-dd-yyyy"),
            '            CType(e.Item.FindControl("hfUseTimeClock"), HiddenField).Value, ScheduleId, UnitTypeDescrip, studentStartDate, CType(e.Item.FindControl("hfSysStatusId"), HiddenField).Value,
            '            dtDateDetermined, CType(e.Item.FindControl("hfStuEnrollId"), HiddenField).Value, CType(e.Item.FindControl("hfSource"), HiddenField).Value,
            '            expGradDate, intTardyTrack, PrgVerId, ViewState("UserId"), ViewState("CampusId"))
            'url2.AppendFormat("'{0}','{1}','{2}','{3}','{4}','{5}','{6}',",
            '                e.Item.FindControl("txtD0").ClientID,
            '                e.Item.FindControl("txtD1").ClientID,
            '                e.Item.FindControl("txtD2").ClientID,
            '                e.Item.FindControl("txtD3").ClientID,
            '                e.Item.FindControl("txtD4").ClientID,
            '                e.Item.FindControl("txtD5").ClientID,
            '                e.Item.FindControl("txtD6").ClientID)
            'url2.AppendFormat("'{0}','{1}','{2}','{3}','{4}','{5}','{6}',",
            '                e.Item.FindControl("hfS0").ClientID,
            '                e.Item.FindControl("hfS1").ClientID,
            '                e.Item.FindControl("hfS2").ClientID,
            '                e.Item.FindControl("hfS3").ClientID,
            '                e.Item.FindControl("hfS4").ClientID,
            '                e.Item.FindControl("hfS5").ClientID,
            '                e.Item.FindControl("hfS6").ClientID)
            'url2.AppendFormat("'{0}','{1}','{2}','{3}','{4}','{5}','{6}',",
            '                    e.Item.FindControl("hfA0").ClientID,
            '                    e.Item.FindControl("hfA1").ClientID,
            '                    e.Item.FindControl("hfA2").ClientID,
            '                    e.Item.FindControl("hfA3").ClientID,
            '                    e.Item.FindControl("hfA4").ClientID,
            '                    e.Item.FindControl("hfA5").ClientID,
            '                    e.Item.FindControl("hfA6").ClientID)
            'url2.AppendFormat("'{0}','{1}','{2}','{3}','{4}','{5}','{6}',",
            '                    e.Item.FindControl("hfT0").ClientID,
            '                    e.Item.FindControl("hfT1").ClientID,
            '                    e.Item.FindControl("hfT2").ClientID,
            '                    e.Item.FindControl("hfT3").ClientID,
            '                    e.Item.FindControl("hfT4").ClientID,
            '                    e.Item.FindControl("hfT5").ClientID,
            '                    e.Item.FindControl("hfT6").ClientID)
            'url2.AppendFormat("'{0}','{1}','{2}','{3}','{4}','{5}','{6}',",
            '                 e.Item.FindControl("hfAP0").ClientID,
            '                 e.Item.FindControl("hfAP1").ClientID,
            '                 e.Item.FindControl("hfAP2").ClientID,
            '                 e.Item.FindControl("hfAP3").ClientID,
            '                 e.Item.FindControl("hfAP4").ClientID,
            '                 e.Item.FindControl("hfAP5").ClientID,
            '                 e.Item.FindControl("hfAP6").ClientID)
            'url2.AppendFormat("'{0}','{1}','{2}','{3}','{4}','{5}','{6}')",
            '                 e.Item.FindControl("hfDisabled0").ClientID,
            '                 e.Item.FindControl("hfDisabled1").ClientID,
            '                 e.Item.FindControl("hfDisabled2").ClientID,
            '                 e.Item.FindControl("hfDisabled3").ClientID,
            '                 e.Item.FindControl("hfDisabled4").ClientID,
            '                 e.Item.FindControl("hfDisabled5").ClientID,
            '                 e.Item.FindControl("hfDisabled6").ClientID)

            'CType(e.Item.FindControl("btnStudentName"), Button).Attributes.Add("onclick", url2.ToString())
            CType(e.Item.FindControl("btnStudentName"), HyperLink).NavigateUrl = url2.ToString()

            ' Make sure this is done after we bind all the days
            BindTotal(e, UnitTypeDescrip)
        End If
    End Sub
    Protected Function SaveRepeaterToDataTable(ByVal user As String) As DataSet
        Dim ds As New DataSet
        ds = Session("AttendanceDS")
        'If Me.AttendanceDS Is Nothing Or Me.AttendanceDS.Tables.Count = 0 Then Return ' Error
        ' iterate through each student and save the changes to our "Cached" datatable
        Dim nDays As Integer = 7
        Dim dt As DataTable = ds.Tables(0)
        For Each rpi As RepeaterItem In rptAttendance.Items
            Dim stuEnrollId As String = CType(rpi.FindControl("hfStuEnrollId"), HiddenField).Value
            Dim scheduleId As String = CType(rpi.FindControl("hfScheduleId"), HiddenField).Value
            Dim unittypedescrip As String = CType(rpi.FindControl("hfUnitTypeDescrip"), HiddenField).Value
            Dim attType As String = CType(rpi.FindControl("hfattType"), HiddenField).Value
            Dim source As String = CType(rpi.FindControl("hfsource"), HiddenField).Value

            For i As Integer = 0 To 6
                Dim tDate As Date = CType(Filter_StartDate.AddDays(i).ToShortDateString(), Date) ' get the day without the time
                ' get controls to Actual and Scheduled hidden fields
                ' Note: we must use these as they are the ones that will have reliable values
                Dim ctlId As String = "hfA" + CType(tDate.DayOfWeek, Integer).ToString() '"hfA" + CType(tDate.DayOfWeek, Integer).ToString()
                Dim ctlTardyId As String = "hfT" + CType(tDate.DayOfWeek, Integer).ToString()
                Dim hfActual As HiddenField = CType(rpi.FindControl(ctlId), HiddenField)
                Dim hfTardy As HiddenField = CType(rpi.FindControl(ctlTardyId), HiddenField)
                ctlId = "txtD" + CType(tDate.DayOfWeek, Integer).ToString()
                Dim postbyexceptionId As String = "hfException" + CType(tDate.DayOfWeek, Integer).ToString()
                Dim hfException As HiddenField = CType(rpi.FindControl(postbyexceptionId), HiddenField)
                'Dim txtActual As eWorld.UI.NumericBox = CType(rpi.FindControl(ctlId), eWorld.UI.NumericBox)
                Dim txtActual As TextBox = CType(rpi.FindControl(ctlId), TextBox)
                If unittypedescrip = "present" Then
                    txtActual.Text = CType(rpi.FindControl(ctlId), TextBox).Text
                End If
                ctlId = "hfS" + CType(tDate.DayOfWeek, Integer).ToString()
                Dim hfSched As HiddenField = CType(rpi.FindControl(ctlId), HiddenField)
                ' retrieve the row for the curent student/schedule and date
                Dim sql As String = String.Format("StuEnrollId='{0}' and ScheduleId='{1}' and RecordDate='{2}'",
                                            stuEnrollId, scheduleId, tDate)
                Dim drs() As DataRow = dt.Select(sql)
                ' determine if hours exist for this student/day and if not, create a new one.
                Dim dr As DataRow = Nothing
                If drs.Length = 0 Then
                    dr = dt.NewRow
                    dt.Rows.Add(dr)
                Else
                    dr = drs(0)
                End If
                ' save the form data to the datarow
                dr("StuEnrollId") = stuEnrollId
                dr("ScheduleId") = scheduleId
                dr("RecordDate") = tDate
                dr("Source") = source

                ' the actual hours will either be the text box or the hidden value.
                ' It depends on which one has an actual value.  The hidden field is updated
                ' when the user edit values for the week whereas the textbox is updated in place.
                If attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
                    Try
                        If hfSched.Value.Length > 0 Then
                            If CInt(hfSched.Value).ToString >= "1" Then
                                dr("SchedHours") = "1.00"
                            ElseIf CInt(hfSched.Value).ToString = "0" Then
                                dr("SchedHours") = "0.00"
                            Else
                                dr("SchedHours") = System.DBNull.Value
                            End If
                        Else
                            dr("SchedHours") = System.DBNull.Value
                        End If
                        If txtActual.Text.Length > 0 Then
                            If txtActual.Text.ToUpper.Substring(0, 1) = "P" Or txtActual.Text.ToUpper = "T" Then
                                dr("ActualHours") = "1.00"
                            ElseIf txtActual.Text.ToUpper = "A" Then
                                dr("ActualHours") = "0.00"
                            Else
                                dr("ActualHours") = ""
                            End If
                        Else
                            dr("ActualHours") = "9999.00"
                        End If
                        'commented by balaji as its putting "P" even on days before start date
                        'when posted by exception
                        'If Session("exceptionFlag") = True Then
                        '    If CInt(hfSched.Value).ToString >= "1" Then
                        '        If txtActual.Text.Length < 1 Then
                        '            dr("ActualHours") = "1.00"
                        '        End If
                        '    End If
                        'End If
                    Catch ex As Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(ex)

                    End Try
                    'If (hfTardy.Value = "1" Or hfTardy.Value = "True" Or txtActual.Text.ToUpper = "T") Then

                    'If CInt(hfSched.Value).ToString = "0" Then ' if day is unscheduled day tardy posted is considered makeup day
                    '    dr("Tardy") = False
                    'Else
                    If txtActual.Text.ToUpper = "T" Then
                        dr("Tardy") = True
                    ElseIf txtActual.Text.ToUpper = "P" Then
                        dr("Tardy") = False
                    Else
                        dr("Tardy") = False
                    End If
                    dr("UnitTypeDescrip") = "present"
                    If hfException.Value = "yes" Then
                        dr("PostByException") = "yes"
                    Else
                        dr("PostByException") = "no"
                    End If
                Else
                    If txtActual.Text.Length > 0 Then
                        If InStr(txtActual.Text, "(") >= 1 Then
                            dr("ActualHours") = Mid(txtActual.Text, 1, InStr(txtActual.Text, "(") - 1)
                            dr("Tardy") = True
                        Else
                            ''Code modified by saraswathi
                            ''On March 03 2009
                            ''To save the tardy values posted.
                            dr("ActualHours") = txtActual.Text
                            If hfTardy.Value = "" Then
                                dr("Tardy") = False
                            Else
                                If hfTardy.Value <> "" Then
                                    If hfTardy.Value = True Then
                                        dr("Tardy") = True
                                    Else
                                        dr("Tardy") = False
                                    End If
                                Else
                                    dr("Tardy") = False
                                End If
                            End If

                        End If
                    End If
                    If hfActual.Value.Length > 0 AndAlso txtActual.Text.Length = 0 Then
                        dr("ActualHours") = "9999.00"
                    ElseIf hfActual.Value.Length = 0 AndAlso txtActual.Text.Length = 0 Then
                        dr("ActualHours") = "9999.00"
                    End If
                    If hfSched.Value.Length > 0 Then dr("SchedHours") = hfSched.Value
                    dr("UnitTypeDescrip") = ""
                    If hfException.Value = "yes" Then
                        dr("PostByException") = "yes"
                    Else
                        dr("PostByException") = "no"
                    End If
                End If
                dr("ModDate") = Date.Now
                dr("ModUser") = user
            Next
        Next
        Session("exceptionFlag") = False
        Return ds
    End Function
    Protected Function SaveRepeaterToDataTable_New(ByVal user As String, ByVal campusId As String) As DataSet
        Dim ds As New DataSet
        Dim IsInDB As Boolean
        Dim tardyProcessed As Boolean = False
        Dim tbl1 As New DataTable("InsertPostClockAttendance")
        Dim tbl2 As New DataTable("UpdatePostClockAttendance")
        With tbl1
            'Define table schema
            .Columns.Add("StuEnrollId", GetType(Guid))
            .Columns.Add("ScheduleId", GetType(Guid))
            .Columns.Add("RecordDate", GetType(Date))
            .Columns.Add("UnitTypeDescrip", GetType(String))
            .Columns.Add("SchedHours", GetType(Decimal))
            .Columns.Add("ActualHours", GetType(Decimal))
            .Columns.Add("IsTardy", GetType(Boolean))
            .Columns.Add("TardyProcessed", GetType(Boolean))
            .Columns.Add("PostByException", GetType(String))
            .Columns.Add("IsInDB", GetType(Boolean))
            .Columns.Add("ModDate", GetType(Date))
            .Columns.Add("ModUser", GetType(String))

            'set field properties
            '.Columns("StuEnrollId").AllowDBNull = False
            '.Columns("ScheduleId").AllowDBNull = False
            '.Columns("RecordDate").AllowDBNull = False


            'set primary key
            '.PrimaryKey = New DataColumn() {.Columns("GrdComponentTypeId"), .Columns("ReqId")}
        End With

        'ds = Session("AttendanceDS")
        If Not Session("AttendanceDS_New") Is Nothing Then
            ds = Session("AttendanceDS_New")
        Else
            ds = Session("AttendanceDS")
        End If


        'If Me.AttendanceDS Is Nothing Or Me.AttendanceDS.Tables.Count = 0 Then Return ' Error
        ' iterate through each student and save the changes to our "Cached" datatable
        Dim nDays As Integer = 7
        Dim dt As DataTable = ds.Tables(0)
        Dim intRepeaterCount As Integer = 0
        intRepeaterCount = rptAttendance.Items.Count
        For j As Integer = 0 To intRepeaterCount - 1
            Dim rpi As RepeaterItem = CType(rptAttendance.Items(j), RepeaterItem)
            Dim stuEnrollId As String = CType(rpi.FindControl("hfStuEnrollId"), HiddenField).Value
            Dim scheduleId As String = CType(rpi.FindControl("hfScheduleId"), HiddenField).Value
            Dim unittypedescrip As String = CType(rpi.FindControl("hfUnitTypeDescrip"), HiddenField).Value
            Dim attType As String = CType(rpi.FindControl("hfattType"), HiddenField).Value
            Dim source As String = CType(rpi.FindControl("hfsource"), HiddenField).Value

            For i As Integer = 0 To 6
                Dim tDate As Date = CType(Filter_StartDate.AddDays(i).ToShortDateString(), Date) ' get the day without the time
                ' get controls to Actual and Scheduled hidden fields
                ' Note: we must use these as they are the ones that will have reliable values
                Dim ctlId As String = "hfA" + CType(tDate.DayOfWeek, Integer).ToString() '"hfA" + CType(tDate.DayOfWeek, Integer).ToString()
                Dim ctlTardyId As String = "hfT" + CType(tDate.DayOfWeek, Integer).ToString()
                'Dim ctlSchedId As String = "hfSched" + CType(tDate.DayOfWeek, Integer).ToString()
                Dim hfActual As HiddenField = CType(rpi.FindControl(ctlId), HiddenField)
                Dim hfTardy As HiddenField = CType(rpi.FindControl(ctlTardyId), HiddenField)
                'Dim hfSchedId As HiddenField = CType(rpi.FindControl(ctlSchedId), HiddenField)

                ctlId = "txtD" + CType(tDate.DayOfWeek, Integer).ToString()
                Dim postbyexceptionId As String = "hfException" + CType(tDate.DayOfWeek, Integer).ToString()
                Dim hfException As HiddenField = CType(rpi.FindControl(postbyexceptionId), HiddenField)
                'Dim txtActual As eWorld.UI.NumericBox = CType(rpi.FindControl(ctlId), eWorld.UI.NumericBox)
                Dim txtActual As TextBox = CType(rpi.FindControl(ctlId), TextBox)
                If unittypedescrip = "present" Then
                    txtActual.Text = CType(rpi.FindControl(ctlId), TextBox).Text
                End If
                ctlId = "hfS" + CType(tDate.DayOfWeek, Integer).ToString()
                Dim hfSched As HiddenField = CType(rpi.FindControl(ctlId), HiddenField)
                ' retrieve the row for the curent student/schedule and date
                Dim sql As String = String.Format("StuEnrollId='{0}' and RecordDate='{1}'",
                                            stuEnrollId, tDate)
                Dim drs() As DataRow = dt.Select(sql)
                ' determine if hours exist for this student/day and if not, create a new one.
                Dim dr As DataRow = Nothing
                If drs.Length = 0 Then
                    dr = dt.NewRow
                    IsInDB = 0
                    dt.Rows.Add(dr)
                Else
                    dr = drs(0)
                    IsInDB = 1
                End If
                ' save the form data to the datarow
                dr("StuEnrollId") = stuEnrollId
                dr("ScheduleId") = scheduleId
                'dr("ScheduleId") = hfSchedId.Value.ToString()
                dr("RecordDate") = tDate
                dr("TardyProcessed") = tardyProcessed
                'dr("Source") = source

                ' the actual hours will either be the text box or the hidden value.
                ' It depends on which one has an actual value.  The hidden field is updated
                ' when the user edit values for the week whereas the textbox is updated in place.
                If attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
                    Try
                        If hfSched.Value.Length > 0 Then
                            If CInt(hfSched.Value).ToString >= "1" Then
                                dr("SchedHours") = "1.00"
                            ElseIf CInt(hfSched.Value).ToString = "0" Then
                                dr("SchedHours") = "0.00"
                            Else
                                dr("SchedHours") = System.DBNull.Value
                            End If
                        Else
                            dr("SchedHours") = System.DBNull.Value
                        End If
                        If txtActual.Text.Length > 0 Then
                            If txtActual.Text.ToUpper.Substring(0, 1) = "P" Or txtActual.Text.ToUpper = "T" Then
                                dr("ActualHours") = "1.00"
                            ElseIf txtActual.Text.ToUpper = "A" Then
                                dr("ActualHours") = "0.00"
                            Else
                                dr("ActualHours") = ""
                            End If
                        Else
                            dr("ActualHours") = "9999.00"
                        End If
                        'commented by balaji as its putting "P" even on days before start date
                        'when posted by exception
                        'If Session("exceptionFlag") = True Then
                        '    If CInt(hfSched.Value).ToString >= "1" Then
                        '        If txtActual.Text.Length < 1 Then
                        '            dr("ActualHours") = "1.00"
                        '        End If
                        '    End If
                        'End If
                    Catch ex As Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(ex)

                    End Try
                    'If (hfTardy.Value = "1" Or hfTardy.Value = "True" Or txtActual.Text.ToUpper = "T") Then

                    'If CInt(hfSched.Value).ToString = "0" Then ' if day is unscheduled day tardy posted is considered makeup day
                    '    dr("Tardy") = False
                    'Else
                    If txtActual.Text.ToUpper = "T" Then
                        dr("IsTardy") = True
                    ElseIf txtActual.Text.ToUpper = "P" Then
                        dr("IsTardy") = False
                    Else
                        dr("IsTardy") = False
                    End If
                    dr("UnitTypeDescrip") = "present"
                    If hfException.Value = "yes" Then
                        dr("PostByException") = "yes"
                    Else
                        dr("PostByException") = "no"
                    End If
                Else
                    If txtActual.Text.Length > 0 Then
                        If InStr(txtActual.Text, "(") >= 1 Then
                            dr("ActualHours") = Mid(txtActual.Text, 1, InStr(txtActual.Text, "(") - 1)
                            dr("IsTardy") = True
                        Else
                            ''Code modified by saraswathi
                            ''On March 03 2009
                            ''To save the tardy values posted.
                            dr("ActualHours") = txtActual.Text
                            If hfTardy.Value = "" Then
                                dr("IsTardy") = False
                            Else
                                If hfTardy.Value <> "" Then
                                    If hfTardy.Value = True Then
                                        dr("IsTardy") = True
                                    Else
                                        dr("IsTardy") = False
                                    End If
                                Else
                                    dr("IsTardy") = False
                                End If
                            End If

                        End If
                    End If
                    If hfActual.Value.Length > 0 AndAlso txtActual.Text.Length = 0 Then
                        dr("ActualHours") = "9999.00"
                    ElseIf hfActual.Value.Length = 0 AndAlso txtActual.Text.Length = 0 Then
                        dr("ActualHours") = "9999.00"
                    End If
                    If hfSched.Value.Length > 0 Then dr("SchedHours") = hfSched.Value
                    dr("UnitTypeDescrip") = ""
                    If hfException.Value = "yes" Then
                        dr("PostByException") = "yes"
                    Else
                        dr("PostByException") = "no"
                    End If
                End If
                dr("ModDate") = Date.Now
                dr("ModUser") = user
                If (New AttendanceFacade).IsHoliday(dr("RecordDate"), campusId) And MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                    dr("SchedHours") = 0
                End If
                If IsDBNull(dr("SchedHours")) Then dr("SchedHours") = 0
                'If IsInDB = 0 Then
                tbl1.LoadDataRow(New Object() {dr("StuEnrollId"), dr("ScheduleId"), dr("RecordDate"), dr("UnitTypeDescrip"), dr("SchedHours"), dr("ActualHours"), dr("IsTardy"), dr("TardyProcessed"), dr("PostByException"), IsInDB, dr("ModDate"), dr("ModUser")}, False)

            Next
        Next
        'For Each rpi As RepeaterItem In rptAttendance.Items
        '    'Dim hfStuEnrollId As HiddenField = CType(rpi.FindControl("hfStuEnrollId"), HiddenField)
        '    'Dim stuEnrollId As String = hfStuEnrollId.Value
        '    Dim stuEnrollId As String = CType(rpi.FindControl("hfStuEnrollId"), HiddenField).Value
        '    Dim scheduleId As String = CType(rpi.FindControl("hfScheduleId"), HiddenField).Value
        '    Dim unittypedescrip As String = CType(rpi.FindControl("hfUnitTypeDescrip"), HiddenField).Value
        '    Dim attType As String = CType(rpi.FindControl("hfattType"), HiddenField).Value
        '    Dim source As String = CType(rpi.FindControl("hfsource"), HiddenField).Value

        '    For i As Integer = 0 To 6
        '        Dim tDate As Date = CType(Filter_StartDate.AddDays(i).ToShortDateString(), Date) ' get the day without the time
        '        ' get controls to Actual and Scheduled hidden fields
        '        ' Note: we must use these as they are the ones that will have reliable values
        '        Dim ctlId As String = "hfA" + CType(tDate.DayOfWeek, Integer).ToString() '"hfA" + CType(tDate.DayOfWeek, Integer).ToString()
        '        Dim ctlTardyId As String = "hfT" + CType(tDate.DayOfWeek, Integer).ToString()
        '        'Dim ctlSchedId As String = "hfSched" + CType(tDate.DayOfWeek, Integer).ToString()
        '        Dim hfActual As HiddenField = CType(rpi.FindControl(ctlId), HiddenField)
        '        Dim hfTardy As HiddenField = CType(rpi.FindControl(ctlTardyId), HiddenField)
        '        'Dim hfSchedId As HiddenField = CType(rpi.FindControl(ctlSchedId), HiddenField)

        '        ctlId = "txtD" + CType(tDate.DayOfWeek, Integer).ToString()
        '        Dim postbyexceptionId As String = "hfException" + CType(tDate.DayOfWeek, Integer).ToString()
        '        Dim hfException As HiddenField = CType(rpi.FindControl(postbyexceptionId), HiddenField)
        '        'Dim txtActual As eWorld.UI.NumericBox = CType(rpi.FindControl(ctlId), eWorld.UI.NumericBox)
        '        Dim txtActual As TextBox = CType(rpi.FindControl(ctlId), TextBox)
        '        If unittypedescrip = "present" Then
        '            txtActual.Text = CType(rpi.FindControl(ctlId), TextBox).Text
        '        End If
        '        ctlId = "hfS" + CType(tDate.DayOfWeek, Integer).ToString()
        '        Dim hfSched As HiddenField = CType(rpi.FindControl(ctlId), HiddenField)
        '        ' retrieve the row for the curent student/schedule and date
        '        Dim sql As String = String.Format("StuEnrollId='{0}' and ScheduleId='{1}' and RecordDate='{2}'", _
        '                                    stuEnrollId, scheduleId, tDate)
        '        Dim drs() As DataRow = dt.Select(sql)
        '        ' determine if hours exist for this student/day and if not, create a new one.
        '        Dim dr As DataRow = Nothing
        '        If drs.Length = 0 Then
        '            dr = dt.NewRow
        '            IsInDB = 0
        '            dt.Rows.Add(dr)
        '        Else
        '            dr = drs(0)
        '            IsInDB = 1
        '        End If
        '        ' save the form data to the datarow
        '        dr("StuEnrollId") = stuEnrollId
        '        dr("ScheduleId") = scheduleId
        '        'dr("ScheduleId") = hfSchedId.Value.ToString()
        '        dr("RecordDate") = tDate
        '        dr("TardyProcessed") = tardyProcessed
        '        'dr("Source") = source

        '        ' the actual hours will either be the text box or the hidden value.
        '        ' It depends on which one has an actual value.  The hidden field is updated
        '        ' when the user edit values for the week whereas the textbox is updated in place.
        '        If attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
        '            Try
        '                If hfSched.Value.Length > 0 Then
        '                    If CInt(hfSched.Value).ToString >= "1" Then
        '                        dr("SchedHours") = "1.00"
        '                    ElseIf CInt(hfSched.Value).ToString = "0" Then
        '                        dr("SchedHours") = "0.00"
        '                    Else
        '                        dr("SchedHours") = System.DBNull.Value
        '                    End If
        '                Else
        '                    dr("SchedHours") = System.DBNull.Value
        '                End If
        '                If txtActual.Text.Length > 0 Then
        '                    If txtActual.Text.ToUpper.Substring(0, 1) = "P" Or txtActual.Text.ToUpper = "T" Then
        '                        dr("ActualHours") = "1.00"
        '                    ElseIf txtActual.Text.ToUpper = "A" Then
        '                        dr("ActualHours") = "0.00"
        '                    Else
        '                        dr("ActualHours") = ""
        '                    End If
        '                Else
        '                    dr("ActualHours") = "9999.00"
        '                End If
        '                'commented by balaji as its putting "P" even on days before start date
        '                'when posted by exception
        '                'If Session("exceptionFlag") = True Then
        '                '    If CInt(hfSched.Value).ToString >= "1" Then
        '                '        If txtActual.Text.Length < 1 Then
        '                '            dr("ActualHours") = "1.00"
        '                '        End If
        '                '    End If
        '                'End If
        '            Catch ex As Exception
        '            	Dim exTracker = new AdvApplicationInsightsInitializer()
        '            	exTracker.TrackExceptionWrapper(ex)

        '            End Try
        '            'If (hfTardy.Value = "1" Or hfTardy.Value = "True" Or txtActual.Text.ToUpper = "T") Then

        '            'If CInt(hfSched.Value).ToString = "0" Then ' if day is unscheduled day tardy posted is considered makeup day
        '            '    dr("Tardy") = False
        '            'Else
        '            If txtActual.Text.ToUpper = "T" Then
        '                dr("IsTardy") = True
        '            ElseIf txtActual.Text.ToUpper = "P" Then
        '                dr("IsTardy") = False
        '            Else
        '                dr("IsTardy") = False
        '            End If
        '            dr("UnitTypeDescrip") = "present"
        '            If hfException.Value = "yes" Then
        '                dr("PostByException") = "yes"
        '            Else
        '                dr("PostByException") = "no"
        '            End If
        '        Else
        '            If txtActual.Text.Length > 0 Then
        '                If InStr(txtActual.Text, "(") >= 1 Then
        '                    dr("ActualHours") = Mid(txtActual.Text, 1, InStr(txtActual.Text, "(") - 1)
        '                    dr("IsTardy") = True
        '                Else
        '                    ''Code modified by saraswathi
        '                    ''On March 03 2009
        '                    ''To save the tardy values posted.
        '                    dr("ActualHours") = txtActual.Text
        '                    If hfTardy.Value = "" Then
        '                        dr("IsTardy") = False
        '                    Else
        '                        If hfTardy.Value <> "" Then
        '                            If hfTardy.Value = True Then
        '                                dr("IsTardy") = True
        '                            Else
        '                                dr("IsTardy") = False
        '                            End If
        '                        Else
        '                            dr("IsTardy") = False
        '                        End If
        '                    End If

        '                End If
        '            End If
        '            If hfActual.Value.Length > 0 AndAlso txtActual.Text.Length = 0 Then
        '                dr("ActualHours") = "9999.00"
        '            ElseIf hfActual.Value.Length = 0 AndAlso txtActual.Text.Length = 0 Then
        '                dr("ActualHours") = "9999.00"
        '            End If
        '            If hfSched.Value.Length > 0 Then dr("SchedHours") = hfSched.Value
        '            dr("UnitTypeDescrip") = ""
        '            If hfException.Value = "yes" Then
        '                dr("PostByException") = "yes"
        '            Else
        '                dr("PostByException") = "no"
        '            End If
        '        End If
        '        dr("ModDate") = Date.Now
        '        dr("ModUser") = user
        '        If (New AttendanceFacade).IsHoliday(dr("RecordDate"), campusId) Then
        '            dr("SchedHours") = 0
        '        End If
        '        If IsDBNull(dr("SchedHours")) Then dr("SchedHours") = 0
        '        'If IsInDB = 0 Then
        '        tbl1.LoadDataRow(New Object() {dr("StuEnrollId"), dr("ScheduleId"), dr("RecordDate"), dr("UnitTypeDescrip"), dr("SchedHours"), dr("ActualHours"), dr("IsTardy"), dr("TardyProcessed"), dr("PostByException"), IsInDB, dr("ModDate"), dr("ModUser")}, False)

        '    Next
        'Next




        'add the remaining rows to the datatable
        'For i = 0 To dt.Rows.Count - 1
        '    Dim dr As DataRow = dt.Rows(i)
        '    Dim stuEnrollId As String = dr("StuEnrollId").ToString
        '    Dim scheduleId As String = dr("ScheduleId").ToString
        '    Dim tdate As Date = CType(dr("RecordDate").ToString, Date)
        '    Dim sql As String = String.Format("StuEnrollId='{0}' and ScheduleId='{1}' and RecordDate='{2}'", _
        '                                   stuEnrollId, scheduleId, tDate)
        '    Dim drs() As DataRow = tbl1.Select(sql)
        '    If drs.Length = 0 Then tbl1.LoadDataRow(New Object() {dr("StuEnrollId"), dr("ScheduleId"), dr("RecordDate"), dr("UnitTypeDescrip"), dr("SchedHours"), dr("ActualHours"), dr("Tardy"), tardyProcessed, dr("PostByException"), IsInDB, dr("ModDate"), dr("ModUser")}, False)
        'Next

        Session("exceptionFlag") = False
        'Return ds

        ' Dim ds1 As DataSet = CType(Session("AttendanceDS_New"), DataSet)
        Dim ds1 As New DataSet
        If Not Session("AttendanceDS_New") Is Nothing Then
            ds1 = CType(Session("AttendanceDS_New"), DataSet)
            Dim dt1 As DataTable = ds1.Tables(0)
            For i As Integer = 0 To tbl1.Rows.Count - 1
                Dim dr As DataRow = tbl1.Rows(i)
                Dim stuEnrollId As String = dr("StuEnrollId").ToString
                Dim scheduleId As String = dr("ScheduleId").ToString
                Dim tdate As Date = CType(dr("RecordDate").ToString, Date)
                Dim sql As String = String.Format("StuEnrollId='{0}' and ScheduleId='{1}' and RecordDate='{2}'",
                                               stuEnrollId, scheduleId, tdate)
                Dim drs() As DataRow = dt1.Select(sql)
                If drs.Length = 0 Then
                    dt1.ImportRow(tbl1.Rows(i))
                ElseIf drs.Length > 0 Then
                    If drs(0)("ActualHours") <> dr("ActualHours") Then
                        dt1.Rows.Remove(drs(0))
                        dt1.ImportRow(tbl1.Rows(i))

                    End If
                End If

            Next
        Else
            ds1.Tables.Add(tbl1)
        End If


        Session("AttendanceDS_New") = ds1
        'Session("AttendanceDS") = Session("AttendanceDS_New")
        Return ds1
    End Function

    'Get new zero records for a date that that has not been posted yet and students that are not future start.
    Protected Function SaveRepeaterToDataTableSingleDay(ByVal user As String, i As Int32) As DataSet
        Dim ds As New DataSet
        ds = Session("AttendanceDS")
        'If Me.AttendanceDS Is Nothing Or Me.AttendanceDS.Tables.Count = 0 Then Return ' Error
        'Iterate through each student and save the changes to our "Cached" datatable

        Dim dt As DataTable = ds.Tables(0)
        Dim newDt As DataTable = ds.Tables(0).Copy()
        newDt.Clear()

        If (Not newDt.Columns.Contains("Source")) Then
            newDt.Columns.Add("Source")
        End If

        If (Not dt.Columns.Contains("Source")) Then
            dt.Columns.Add("Source")
        End If

        If (Not newDt.Columns.Contains("Tardy")) Then
            newDt.Columns.Add("Tardy")
        End If

        If (Not dt.Columns.Contains("Tardy")) Then
            dt.Columns.Add("Tardy")
        End If

        For Each rpi As RepeaterItem In rptAttendance.Items
            Dim stuEnrollId As String = CType(rpi.FindControl("hfStuEnrollId"), HiddenField).Value
            Dim scheduleId As String = CType(rpi.FindControl("hfScheduleId"), HiddenField).Value
            Dim unittypedescrip As String = CType(rpi.FindControl("hfUnitTypeDescrip"), HiddenField).Value
            Dim attType As String = CType(rpi.FindControl("hfattType"), HiddenField).Value
            Dim source As String = CType(rpi.FindControl("hfsource"), HiddenField).Value
            Dim studentStatusId As String = CType(rpi.FindControl("hfSysStatusId"), HiddenField).Value

            Dim tDate As Date = CType(Filter_StartDate.AddDays(i).ToShortDateString(), Date) ' get the day without the time
            ' get controls to Actual and Scheduled hidden fields
            ' Note: we must use these as they are the ones that will have reliable values
            Dim ctlId As String = "hfA" + CType(tDate.DayOfWeek, Integer).ToString() '"hfA" + CType(tDate.DayOfWeek, Integer).ToString()
            Dim ctlTardyId As String = "hfT" + CType(tDate.DayOfWeek, Integer).ToString()
            Dim hfActual As HiddenField = CType(rpi.FindControl(ctlId), HiddenField)
            Dim hfTardy As HiddenField = CType(rpi.FindControl(ctlTardyId), HiddenField)
            ctlId = "txtD" + CType(tDate.DayOfWeek, Integer).ToString()
            Dim postbyexceptionId As String = "hfException" + CType(tDate.DayOfWeek, Integer).ToString()
            Dim hfException As HiddenField = CType(rpi.FindControl(postbyexceptionId), HiddenField)
            'Dim txtActual As eWorld.UI.NumericBox = CType(rpi.FindControl(ctlId), eWorld.UI.NumericBox)
            Dim txtActual As TextBox = CType(rpi.FindControl(ctlId), TextBox)
            If unittypedescrip = "present" Then
                txtActual.Text = CType(rpi.FindControl(ctlId), TextBox).Text
            End If
            ctlId = "hfS" + CType(tDate.DayOfWeek, Integer).ToString()
            Dim hfSched As HiddenField = CType(rpi.FindControl(ctlId), HiddenField)
            ' retrieve the row for the curent student/schedule and date
            Dim sql As String = String.Format("StuEnrollId='{0}' and ScheduleId='{1}' and RecordDate='{2}'",
                                        stuEnrollId, scheduleId, tDate)
            Dim drs() As DataRow = dt.Select(sql)

            Dim dr As DataRow = Nothing
            Dim isExistingRow As Boolean = False

            If studentStatusId = Int32.Parse(StudentStatusCodes.Future_Start).ToString() Then 'student is future start, do not do anthing'
                Continue For
            ElseIf drs.Length = 0 Then 'no existing attendance record on that date, add one
                dr = newDt.NewRow
            ElseIf drs.Length > 0 AndAlso (drs(0)("ActualHours") = "9999.00" OrElse drs(0)("ActualHours") = "999.0") Then 'existing record found with "empty" attendance, override.
                dr = drs(0)
                isExistingRow = True
            Else
                Continue For 'enrollment already has attendance record for this date and it is not 9999/999 and student is not future start
            End If

            ' save the form data to the datarow
            dr("StuEnrollId") = stuEnrollId
            dr("ScheduleId") = scheduleId
            dr("RecordDate") = tDate
            dr("Source") = source

            ' the actual hours will either be the text box or the hidden value.
            ' It depends on which one has an actual value.  The hidden field is updated
            ' when the user edit values for the week whereas the textbox is updated in place.
            If attType = FAME.AdvantageV1.Common.AR.AttendanceUnitTypes.PresentAbsentGuid Then
                Try
                    If hfSched.Value.Length > 0 Then
                        If CInt(hfSched.Value).ToString >= "1" Then
                            dr("SchedHours") = "1.00"
                        ElseIf CInt(hfSched.Value).ToString = "0" Then
                            dr("SchedHours") = "0.00"
                        Else
                            dr("SchedHours") = System.DBNull.Value
                        End If
                    Else
                        dr("SchedHours") = System.DBNull.Value
                    End If
                    If txtActual.Text.Length > 0 Then
                        If txtActual.Text.ToUpper.Substring(0, 1) = "P" Or txtActual.Text.ToUpper = "T" Then
                            dr("ActualHours") = "1.00"
                        ElseIf txtActual.Text.ToUpper = "A" Then
                            dr("ActualHours") = "0.00"
                        Else
                            dr("ActualHours") = ""
                        End If
                    Else
                        dr("ActualHours") = "9999.00"
                    End If
                    'commented by balaji as its putting "P" even on days before start date
                    'when posted by exception
                    'If Session("exceptionFlag") = True Then
                    '    If CInt(hfSched.Value).ToString >= "1" Then
                    '        If txtActual.Text.Length < 1 Then
                    '            dr("ActualHours") = "1.00"
                    '        End If
                    '    End If
                    'End If
                Catch ex As Exception
                    Dim exTracker = New AdvApplicationInsightsInitializer()
                    exTracker.TrackExceptionWrapper(ex)

                End Try
                'If (hfTardy.Value = "1" Or hfTardy.Value = "True" Or txtActual.Text.ToUpper = "T") Then

                'If CInt(hfSched.Value).ToString = "0" Then ' if day is unscheduled day tardy posted is considered makeup day
                '    dr("Tardy") = False
                'Else
                If txtActual.Text.ToUpper = "T" Then
                    dr("Tardy") = True
                ElseIf txtActual.Text.ToUpper = "P" Then
                    dr("Tardy") = False
                Else
                    dr("Tardy") = False
                End If
                dr("UnitTypeDescrip") = "present"
                If hfException.Value = "yes" Then
                    dr("PostByException") = "yes"
                Else
                    dr("PostByException") = "no"
                End If
            Else
                If txtActual.Text.Length > 0 Then
                    If InStr(txtActual.Text, "(") >= 1 Then
                        dr("ActualHours") = Mid(txtActual.Text, 1, InStr(txtActual.Text, "(") - 1)
                        dr("Tardy") = True
                    Else
                        ''Code modified by saraswathi
                        ''On March 03 2009
                        ''To save the tardy values posted.
                        dr("ActualHours") = txtActual.Text
                        If hfTardy.Value = "" Then
                            dr("Tardy") = False
                        Else
                            If hfTardy.Value <> "" Then
                                If hfTardy.Value = True Then
                                    dr("Tardy") = True
                                Else
                                    dr("Tardy") = False
                                End If
                            Else
                                dr("Tardy") = False
                            End If
                        End If

                    End If
                End If
                If hfActual.Value.Length > 0 AndAlso txtActual.Text.Length = 0 Then
                    dr("ActualHours") = "9999.00"
                ElseIf hfActual.Value.Length = 0 AndAlso txtActual.Text.Length = 0 Then
                    dr("ActualHours") = "9999.00"
                End If
                If hfSched.Value.Length > 0 Then dr("SchedHours") = hfSched.Value
                dr("UnitTypeDescrip") = ""
                If hfException.Value = "yes" Then
                    dr("PostByException") = "yes"
                Else
                    dr("PostByException") = "no"
                End If
            End If
            dr("ModDate") = Date.Now
            dr("ModUser") = user

            If isExistingRow Then
                newDt.ImportRow(dr)
            Else
                newDt.Rows.Add(dr)
            End If
        Next
        Session("exceptionFlag") = False

        Dim dataSet As DataSet = ds.Copy()
        dataSet.Tables.RemoveAt(0)
        dataSet.Tables.Add(newDt)
        Return dataSet
    End Function
#End Region

#Region "Bind Helpers"
    Protected Property Filter_ProgId() As String
        Get
            Return ViewState("ClockAttendance_ProgId")
        End Get
        Set(ByVal value As String)
            ViewState("ClockAttendance_ProgId") = value
        End Set
    End Property
    Protected Property Filter_StudentGrpId() As String
        Get
            Return ViewState("ClockAttendance_StudentGrpId")
        End Get
        Set(ByVal value As String)
            ViewState("ClockAttendance_StudentGrpId") = value
        End Set
    End Property
    ''Added by Saraswathi lakshmanan to filter based on cohortStartDate
    Protected Property Filter_CohortStartDate() As String
        Get
            Return ViewState("ClockAttendance_CohortStartDate")
        End Get
        Set(ByVal value As String)
            ViewState("ClockAttendance_CohortStartDate") = value
        End Set
    End Property
    Protected Property Filter_PrgVerId() As String
        Get
            Return ViewState("ClockAttendance_PrgVerId")
        End Get
        Set(ByVal value As String)
            ViewState("ClockAttendance_PrgVerId") = value
        End Set
    End Property
    Protected Property Filter_StuName() As String
        Get
            Return ViewState("ClockAttendance_StuName")
        End Get
        Set(ByVal value As String)
            ViewState("ClockAttendance_StuName") = value
        End Set
    End Property
    Protected Property Filter_StuStatus() As String
        Get
            Return ViewState("ClockAttendance_StuStatus")
        End Get
        Set(ByVal value As String)
            ViewState("ClockAttendance_StuStatus") = value
        End Set
    End Property
    Protected Property Filter_StartDate() As Date
        Get
            Return ViewState("ClockAttendance_StartDate")
        End Get
        Set(ByVal value As Date)
            ViewState("ClockAttendance_StartDate") = value
        End Set
    End Property
    Protected Property Filter_EndDate() As Date
        Get
            Return ViewState("ClockAttendance_EndDate")
        End Get
        Set(ByVal value As Date)
            ViewState("ClockAttendance_EndDate") = value
        End Set
    End Property
    Protected Property AttendanceDS() As DataSet
        Get
            Return Me.Cache("dsAttendance")
        End Get
        Set(ByVal value As DataSet)
            Me.Cache("dsAttendance") = value
        End Set
    End Property
    Protected Property StudentDS() As DataSet
        Get
            Return Me.Cache("dsStudents")
        End Get
        Set(ByVal value As DataSet)
            Me.Cache("dsStudents") = value
        End Set
    End Property
    Protected Property Filter_UseTimeclock() As String
        Get
            Return ViewState("ClockAttendance_UseTimeclock")
        End Get
        Set(ByVal value As String)
            ViewState("ClockAttendance_UseTimeclock") = value
        End Set
    End Property
    Protected Property Filter_BadgeNum() As String
        Get
            Return ViewState("ClockAttendance_BadgeNum")
        End Get
        Set(ByVal value As String)
            ViewState("ClockAttendance_BadgeNum") = value
        End Set
    End Property

    ''' <summary>
    ''' Initializes the start and end date to be bound to the form
    ''' StartDate will either the first sunday prior to the param sdate
    ''' or it will be the first sunday prior to today's date
    ''' </summary>
    ''' <param name="sdate"></param>
    ''' <param name="edate"></param>
    ''' <remarks></remarks>
    Protected Sub InitDateRange(ByVal sdate As String, ByVal edate As String)
        ' set the start date to the filter start date if it was passed in
        Dim tDate As DateTime = Date.Now
        If sdate <> "" AndAlso sdate <> "12:00:00 AM" Then
            tDate = CType(sdate, Date)
        End If
        If sdate = "12:00:00 AM" And edate <> "" AndAlso edate <> "12:00:00 AM" Then
            tDate = CType(edate, Date)
        End If
        ' Find the first date of the week
        While tDate.DayOfWeek <> DayOfWeek.Sunday
            tDate = tDate.AddDays(-1)
        End While
        Filter_StartDate = tDate ' set it to the first sunday         
        Filter_EndDate = tDate.AddDays(7)
    End Sub
#End Region

#Region "Event Handlers"
    Protected Sub lbNextWeek(ByVal sender As Object, ByVal e As EventArgs) Handles lnkNext.Click
        Dim user As String = ARCommon.GetUserNameFromUserId(ARCommon.GetCurrentUserId())
        SaveRepeaterToDataTable_New(user, CampusId)

        Filter_StartDate = Filter_StartDate.AddDays(7).ToShortDateString()
        Filter_EndDate = Filter_StartDate.AddDays(7).ToShortDateString()
        Dim tdate As Date = CType(Filter_StartDate.ToShortDateString(), Date)
        Dim sql As String = String.Format("RecordDate='{0}'", tdate)
        Dim drs() As DataRow = CType(Session("AttendanceDS_New"), DataSet).Tables(0).Select(sql)
        Dim dsAttendance As New DataSet
        If drs.Length > 0 Then
            Session("AttendanceDS") = Session("AttendanceDS_New")
        Else
            dsAttendance = AttendanceFacade.GetClockHourAttendance(
                                        Filter_ProgId, Filter_PrgVerId,
                                        Filter_StuName, Session("strStatusCodeId"),
                                        Filter_StartDate, Filter_EndDate,
                                        Filter_UseTimeclock, Filter_BadgeNum, CampusId, Filter_StudentGrpId, Filter_CohortStartDate, Session("OutSchoolEnrollmentStatus"))
            Session("AttendanceDS") = dsAttendance
        End If


        Me.BindRepeater()
        BindDayOfWeek()
    End Sub

    Protected Sub lbPrevWeek(ByVal sender As Object, ByVal e As EventArgs) Handles lnkPrev.Click
        Dim user As String = ARCommon.GetUserNameFromUserId(ARCommon.GetCurrentUserId())
        SaveRepeaterToDataTable_New(user, CampusId)

        Filter_StartDate = Filter_StartDate.AddDays(-7).ToShortDateString()
        Filter_EndDate = Filter_StartDate.AddDays(7).ToShortDateString()
        Dim tdate As Date = CType(Filter_StartDate.ToShortDateString(), Date)
        Dim sql As String = String.Format("RecordDate='{0}'", tdate)
        Dim drs() As DataRow = CType(Session("AttendanceDS_New"), DataSet).Tables(0).Select(sql)
        Dim dsAttendance As New DataSet
        If drs.Length > 0 Then
            Session("AttendanceDS") = Session("AttendanceDS_New")
        Else
            dsAttendance = AttendanceFacade.GetClockHourAttendance(
                                        Filter_ProgId, Filter_PrgVerId,
                                        Filter_StuName, Session("strStatusCodeId"),
                                        Filter_StartDate, Filter_EndDate,
                                        Filter_UseTimeclock, Filter_BadgeNum, CampusId, Filter_StudentGrpId, Filter_CohortStartDate, Session("OutSchoolEnrollmentStatus"))
            Session("AttendanceDS") = dsAttendance
        End If
        Me.BindRepeater()
        BindDayOfWeek()
    End Sub
#End Region

    Protected Sub UnpackParams(ByVal ID As String)
        ' first unpackage ID into the list of supported params                        
        Dim params() As String = ID.Split("&")
        For Each p As String In params
            Dim cp() As String = p.Split("=")
            If cp.Length = 2 Then
                Select Case cp(0).ToLower()
                    Case "objid"
                        Filter_ProgId = cp(1)
                    Case "prgverid"
                        Filter_PrgVerId = cp(1)
                    Case "stuname"
                        Filter_StuName = cp(1)
                    Case "stustatus"
                        Filter_StuStatus = cp(1)
                    Case "sdate"
                        If cp(1) <> "" Then
                            Filter_StartDate = cp(1)
                        Else
                            Filter_StartDate = Date.MinValue
                        End If
                    Case "edate"
                        If cp(1) <> "" Then
                            Filter_EndDate = cp(1)
                        Else
                            Filter_EndDate = Date.MinValue
                        End If
                    Case "usetc"
                        Filter_UseTimeclock = cp(1)
                    Case "badgenum"
                        Filter_BadgeNum = cp(1)
                    Case "studentgrpid"
                        Filter_StudentGrpId = cp(1)
                        ''Added by SDAraswathi lakshmanan
                        ''CohortStartDate is added in the filter
                    Case "cohortstartdate"
                        Filter_CohortStartDate = cp(1)
                End Select
            End If
        Next
    End Sub
    ''' Function added by Kamalesh Ahuja 0n 23 August 2010 to resolve mantis issue id 19605
    Protected Sub RepackParams(ByRef ID As String)
        ' first unpackage ID into the list of supported params                        
        Dim params() As String = ID.Split("&")
        For Each p As String In params
            Dim cp() As String = p.Split("=")
            If cp.Length = 2 Then
                Select Case cp(0).ToLower()
                    Case "objid"
                        Filter_ProgId = cp(1)
                    Case "prgverid"
                        Filter_PrgVerId = cp(1)
                    Case "stuname"
                        Filter_StuName = cp(1)
                    Case "stustatus"
                        Filter_StuStatus = cp(1)
                    Case "sdate"
                        'If cp(1) <> "" Then
                        '    Filter_StartDate = cp(1)
                        'Else
                        '    Filter_StartDate = Date.MinValue
                        'End If
                    Case "edate"
                        'If cp(1) <> "" Then
                        '    Filter_EndDate = cp(1)
                        'Else
                        '    Filter_EndDate = Date.MinValue
                        'End If
                    Case "usetc"
                        Filter_UseTimeclock = cp(1)
                    Case "badgenum"
                        Filter_BadgeNum = cp(1)
                    Case "studentgrpid"
                        Filter_StudentGrpId = cp(1)
                        ''Added by SDAraswathi lakshmanan
                        ''CohortStartDate is added in the filter
                    Case "cohortstartdate"
                        Filter_CohortStartDate = cp(1)
                End Select
            End If

        Next
        Dim sb As New StringBuilder()
        sb.AppendFormat("objid={0}&", Filter_ProgId)
        sb.AppendFormat("prgverid={0}&", Filter_PrgVerId)
        sb.AppendFormat("stuname={0}&", Filter_StuName)
        sb.AppendFormat("stustatus={0}&", Filter_StuStatus)
        sb.AppendFormat("sdate={0}&", Filter_StartDate)
        sb.AppendFormat("edate={0}&", Filter_EndDate)
        sb.AppendFormat("usetc={0}&", Filter_UseTimeclock)
        sb.AppendFormat("badgenum={0}&", Filter_BadgeNum)
        sb.AppendFormat("studentgrpid={0}&", Filter_StudentGrpId)
        ''Added by Saraswathi to Filter based on CohortStartDAte if CohortStartDAte is found
        sb.AppendFormat("cohortstartdate={0}", Filter_CohortStartDate)

        ID = sb.ToString

    End Sub
    '''
    Protected Sub BindDayOfWeek()
        lblDate.Text = Filter_StartDate.ToString("MMM yyyy")
        lblWeekOf.Text = "Week " +
                    CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(Filter_StartDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday).ToString()
    End Sub
    Protected Sub BindHeader()
        'If Filter_StartDate = Date.MinValue Then Return
        'Dim tDate As Date = Format(Filter_StartDate, "d")

        'Me.lblD0.Text = String.Format("{0}<br />{1}", tDate.DayOfWeek.ToString.Substring(0, 3), tDate.Day)
        'Me.hfD0.Value = tDate.ToString()

        'tDate = tDate.AddDays(1)
        'Me.hfD1.Value = tDate.ToString()
        'Me.lblD1.Text = String.Format("{0}<br />{1}", tDate.DayOfWeek.ToString.Substring(0, 3), tDate.Day)

        'tDate = tDate.AddDays(1)
        'Me.hfD2.Value = tDate.ToString()
        'Me.lblD2.Text = String.Format("{0}<br />{1}", tDate.DayOfWeek.ToString.Substring(0, 3), tDate.Day)

        'tDate = tDate.AddDays(1)
        'Me.hfD3.Value = tDate.ToString()
        'Me.lblD3.Text = String.Format("{0}<br />{1}", tDate.DayOfWeek.ToString.Substring(0, 3), tDate.Day)

        'tDate = tDate.AddDays(1)
        'Me.hfD4.Value = tDate.ToString()
        'Me.lblD4.Text = String.Format("{0}<br />{1}", tDate.DayOfWeek.ToString.Substring(0, 3), tDate.Day)

        'tDate = tDate.AddDays(1)
        'Me.hfD5.Value = tDate.ToString()
        'Me.lblD5.Text = String.Format("{0}<br />{1}", tDate.DayOfWeek.ToString.Substring(0, 3), tDate.Day)

        'tDate = tDate.AddDays(1)
        'Me.hfD6.Value = tDate.ToString()
        'Me.lblD6.Text = String.Format("{0}<br />{1}", tDate.DayOfWeek.ToString.Substring(0, 3), tDate.Day)

    End Sub

    ''' <summary>
    ''' Sets the tab order of all cells in attendance grid.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub SetTabOrder()
        Dim collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday) = (New ClsSectAttendanceFacade).GetCollectionOfHolidayDateswithCampusID(CampusId)
        Dim taborder As Integer = 0
        For i As Integer = 0 To Me.rptAttendance.Items.Count - 1
            Dim rpi As RepeaterItem = rptAttendance.Items(i)
            For j As Integer = 0 To 6
                Dim lblDate As String = "hfD" + j.ToString()

                'Dim ctrl As HiddenField = TryCast((From item As RepeaterItem In rptAttendance.Controls
                '   Where item.ItemType = ListItemType.Header).SingleOrDefault.FindControl(lblDate), HiddenField)
                'Dim txtDate As Date = CDate(ctrl.Value).Date
                'Dim txtDate As Date = CDate(CType(Me.FindControl(lblDate), HiddenField).Value).Date

                Dim txtID As String = "txtD" + j.ToString()
                'Dim txt As eWorld.UI.NumericBox = CType(rpi.FindControl(txtID), eWorld.UI.NumericBox)
                Dim txt As TextBox = CType(rpi.FindControl(txtID), TextBox)
                txt.TabIndex = taborder
                taborder += 1
                'If IsHoliday(txtDate, collectionOfHolidays) Then
                '    If SingletonAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
                '        txt.Enabled = False
                '        txt.ToolTip = "Entries are disabled because this day is a holiday"
                '    Else
                '        txt.Enabled = True
                '        txt.ToolTip = ""
                '    End If
                'End If
            Next
        Next
    End Sub
    Private Function IsHoliday(ByVal d As Date, ByVal collectionOfHolidays As System.Collections.Generic.List(Of AdvantageHoliday)) As Boolean
        For Each holiday As AdvantageHoliday In collectionOfHolidays
            If System.Math.Abs(d.Subtract(holiday.HolidayDateAndTime).TotalDays) < 1 Then Return True
        Next
        Return False
    End Function
    Public Sub BindRepeater()
        Dim strStartDate As String = ""
        'If StudentDS Is Nothing Then
        '    Me.StudentDS = Session("StudentDS")
        'End If
        If Filter_StartDate <> Date.MinValue Then strStartDate = Filter_StartDate.ToShortDateString()
        Dim strEndDate As String = ""
        If Filter_EndDate <> Date.MinValue Then strEndDate = Filter_EndDate.ToShortDateString()

        ' Retrieve all the students given the current parameter list
        Dim ds As New DataSet
        ds = Session("StudentDS")

        'Set track tardy column for all students based on prg ver id - All students always share same prg ver id in grid'
        If Not ds.Tables(0).Columns.Contains("TrackTardy") Then
            ds.Tables(0).Columns.Add(New DataColumn("TrackTardy"))
        End If

        If (ds.Tables(0).Rows.Count > 0) Then
            Dim tardyTrack As String = "0"
            Try
                Dim PrgVerId As String
                PrgVerId = ds.Tables(0).Rows(0)("PrgVerId").ToString()
                tardyTrack = SchedulesFacade.TrackTardy(PrgVerId)
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)
                tardyTrack = 0
            End Try

            For Each row As DataRow In ds.Tables(0).Rows
                row("TrackTardy") = tardyTrack
            Next
        End If

        rptAttendance.DataSource = ds
        rptAttendance.DataBind()

        ' update the message 
        If rptAttendance.Items.Count = 0 Then
            lblMsg.Text = "No records match the filter criteria."
            divContent.Visible = False
        Else
            lblMsg.Text = ""
            divContent.Visible = True
            SetTabOrder()
        End If
    End Sub

    ''' <summary>
    ''' Bind the form.
    ''' The implementation of this method is slightly different from the traditional
    ''' IMaintFormBase.BindForm seen in other places such as IMaint_SetupSchedule.ascx.
    ''' This is because the param ID represent a list of filters that are used to bind the page.    
    ''' </summary>
    ''' <param name="ID"></param>btnPostCommentsbtnPostComments
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function BindForm(ByVal ID As String, Optional ByVal campusId As String = "", Optional ByVal Permission As String = "", Optional ByVal InSchoolEnrollmentStatus As String = "", Optional ByVal OutSchoolEnrollmentStatus As String = "") As String Implements IMaintFormBase.BindForm
        Try
            ' unpack the ID and put them into our params
            ' that will be used in the facade calls later in this function
            UnpackParams(ID)
            InitDateRange(Filter_StartDate, Filter_EndDate)
            BindDayOfWeek()

            Dim strStartDate As String = ""
            If Filter_StartDate <> Date.MinValue Then strStartDate = Filter_StartDate.ToShortDateString()
            Dim strEndDate As String = ""
            If Filter_EndDate <> Date.MinValue Then strEndDate = Filter_EndDate.ToShortDateString()
            If Filter_EndDate = Date.MinValue Then strEndDate = Filter_StartDate.AddDays(7).ToShortDateString()
            'Session("AttendanceDS") = Nothing
            'Session("StudentDS") = Nothing

            Dim strStatusCodeId As String = ""
            'If InSchoolEnrollmentStatus = "All" Or InSchoolEnrollmentStatus = "InSchool" Or InSchoolEnrollmentStatus = "OutofSchool" Then
            '    Dim dsStatusCodes As DataSet
            '    dsStatusCodes = AttendanceFacade.GetStatusCodes(campusId, "Active", InSchoolEnrollmentStatus)
            '    If dsStatusCodes.Tables(0).Rows.Count >= 1 Then
            '        For Each row As DataRow In dsStatusCodes.Tables(0).Rows
            '            strStatusCodeId &= row("StatusCodeId").ToString & "','"
            '        Next
            '        strStatusCodeId = Mid(strStatusCodeId, 1, InStrRev(strStatusCodeId, "'") - 2)
            '    End If
            'Else
            ' get the list of InSchool Statuses selected 
            'this value will be coming from PostAttendance.aspx.vb page
            strStatusCodeId = InSchoolEnrollmentStatus
            '  End If
            Session("strStatusCodeId") = strStatusCodeId
            Session("StudentEnrollmentStatus") = InSchoolEnrollmentStatus
            Session("OutSchoolEnrollmentStatus") = OutSchoolEnrollmentStatus
            ' Get the real attendance information given the current paramter list
            Dim dsAttendance As New DataSet
            Dim dsStudent As New DataSet
            Dim dsStatusChanges As New DataSet

            ''Parameters Modified by SAraswathi lakshmanan on April 12 2010
            ''To fix mantis case 18806 Ross Snow DAys
            dsAttendance = AttendanceFacade.GetClockHourAttendance(
                                            Filter_ProgId, Filter_PrgVerId,
                                            Filter_StuName, strStatusCodeId,
                                            strStartDate, strEndDate,
                                            Filter_UseTimeclock, Filter_BadgeNum, campusId, Filter_StudentGrpId, Filter_CohortStartDate, OutSchoolEnrollmentStatus)


            ' Retrieve all the students given the current parameter list
            ''MOdified by Saraswathi--Optional paramter CohortStart Date
            dsStudent = AttendanceFacade.GetStudentSchedulesForAttendance(
                                            Filter_ProgId, Filter_PrgVerId,
                                            Filter_StuName, strStatusCodeId,
                                            strStartDate, strEndDate,
                                            Filter_UseTimeclock, Filter_BadgeNum, campusId, Filter_StudentGrpId, Filter_CohortStartDate, OutSchoolEnrollmentStatus)

            Dim distinctStudentEnrollments As List(Of String) = dsStudent.Tables(0).AsEnumerable().Select(Function(x) x("StuEnrollid").ToString()).Distinct().ToList()

            dsStatusChanges = AttendanceFacade.GetStudentStatusHistory(Filter_PrgVerId, campusId)

            Dim boolDoesProgramVersionUseTimeClock As Boolean = False
            boolDoesProgramVersionUseTimeClock = AttendanceFacade.DoesProgramVersionUseTimeClock(Filter_PrgVerId)
            Session("AttendanceDS_New") = Nothing

            Session("AttendanceDS") = dsAttendance
            Session("StudentDS") = dsStudent
            Session("TimeClock") = boolDoesProgramVersionUseTimeClock
            Session("StatusChangeHistory") = dsStatusChanges

            BindRepeater()
            ' set IMaintFormBase params
            ''Added by Saraswathi lakshmanan to make the Import file button visible true or false
            ''based on the use timeclock feature of the progrqamversion
            ''Added on March 2009 20th
            ''Commneted by saraswathi on April 20 2009
            ''Since import file is made a seperate page. it will no longer be available through post attendance page.

            If dsStudent.Tables(0).Rows.Count > 0 Then
                If dsStudent.Tables(0).Rows(0)("UseTimeClock") = True Then
                    Session("UseTimeClock") = True
                    'If pObj.HasFull = True Then
                    '    btnImportFile.Visible = True
                    'Else
                    '    btnImportFile.Visible = False
                    'End If
                    btnpostzero.Visible = True
                    btnShowPostedValues.Visible = True
                    btnPostByException.Visible = False
                Else
                    Session("UseTimeClock") = False
                    '   btnImportFile.Visible = False
                    btnpostzero.Visible = False
                    btnShowPostedValues.Visible = False
                    btnPostByException.Visible = True
                End If

            Else
                Session("UseTimeClock") = False
                ' btnImportFile.Visible = False
                btnpostzero.Visible = False
                btnShowPostedValues.Visible = False
                btnPostByException.Visible = False

            End If

            ParentId = Filter_ProgId
            ObjId = ID
            ModDate = Date.Now
            Return "" ' Success
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Public Function GetLHSDataSet(ByVal ID As String, ByVal bActive As Boolean, ByVal bInactive As Boolean) As System.Data.DataSet Implements IMaintFormBase.GetLHSDataSet
        ' ID here is the CourseId
        If ID Is Nothing Or ID = "" Then Return Nothing
        Return Nothing ' we don't support a LHS for this form
    End Function
    Public Function Handle_Delete() As String Implements IMaintFormBase.Handle_Delete
        Try
            If ObjId Is Nothing Or ObjId = "" Then Return "No record to delete"
            Dim res As String = "" 'GradeBookFacade.DeleteGrdBkWgt(ObjId, ModDate)
            If res <> "" Then
                Return res
            End If

            ObjId = ""
            ModDate = Nothing

            Filter_ProgId = ""
            Filter_PrgVerId = ""
            Filter_StuName = ""
            Filter_StuStatus = ""
            Filter_StartDate = Date.MinValue
            Filter_EndDate = Date.MinValue
            Return "" ' Success
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Public Function Handle_New() As String Implements IMaintFormBase.Handle_New
        Try
            ObjId = Nothing
            ModDate = Nothing

            Filter_ProgId = ""
            Filter_PrgVerId = ""
            Filter_StuName = ""
            Filter_StuStatus = ""
            Filter_StartDate = Date.MinValue
            Filter_EndDate = Date.MinValue

            Me.divContent.Visible = False
            Me.lblMsg.Text = "Select items from the filter and click Build List. When filtering on All Programs, you may experience longer then usual load times."

            Return "" ' Success
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function
    'Public Function Handle_Save() As String Implements IMaintFormBase.Handle_Save
    '    Try
    '        Dim res As String = "" 'ValidateForm()
    '        If res <> "" Then Return res
    '        Dim user As String = ARCommon.GetUserNameFromUserId(ARCommon.GetCurrentUserId())
    '        Dim ds As New DataSet
    '        'ds = SaveRepeaterToDataTable(user)
    '        ds = SaveRepeaterToDataTable_New(user, CampusId)

    '        'Dim info() As ClockHourAttendanceInfo = AttendanceFacade.DSToClockHourAttendanceInfoArray(ds)
    '        ''Modified by SAraswathi lakshmanan on April 1 2010
    '        ''CamPusID validation added For Ross Snow Days issue
    '        res = AttendanceFacade.SaveClockHourAttendanceArray(AttendanceFacade.DSToClockHourAttendanceInfoArray(ds), user, CampusId) 'AttendanceFacade.SaveClockHourAttendanceArray(info, user)

    '        ' if the update was successful, rebind the form so that everything display correctly.
    '        ' Also, this will update the ModDate so that clicking Save followed by Delete works properly
    '        If res = "" Then

    '            ' need to get the data from database as the schedule value may have changed from previous data pull
    '            ' need to refresh data
    '            Dim strStartDate As String = ""
    '            If Filter_StartDate <> Date.MinValue Then strStartDate = Filter_StartDate.ToShortDateString()
    '            Dim strEndDate As String = ""
    '            If Filter_EndDate <> Date.MinValue Then strEndDate = Filter_EndDate.ToShortDateString()

    '            Dim strStatusCodeId As String = ""
    '            If Session("StudentEnrollmentStatus") = "All" Or Session("StudentEnrollmentStatus") = "InSchool" Or Session("StudentEnrollmentStatus") = "OutofSchool" Then
    '                Dim dsStatusCodes As DataSet
    '                dsStatusCodes = AttendanceFacade.GetStatusCodes(CampusId, "Active", Session("StudentEnrollmentStatus"))
    '                If dsStatusCodes.Tables(0).Rows.Count >= 1 Then
    '                    For Each row As DataRow In dsStatusCodes.Tables(0).Rows
    '                        strStatusCodeId &= row("StatusCodeId").ToString & "','"
    '                    Next
    '                    strStatusCodeId = Mid(strStatusCodeId, 1, InStrRev(strStatusCodeId, "'") - 2)
    '                End If
    '            Else
    '                ' get the list of InSchool Statuses selected 
    '                'this value will be coming from PostAttendance.aspx.vb page
    '                strStatusCodeId = Session("StudentEnrollmentStatus")
    '            End If


    '            Dim dsAttendance As New DataSet
    '            Dim dsStudent As New DataSet
    '            dsAttendance = AttendanceFacade.GetClockHourAttendance( _
    '                                            Filter_ProgId, Filter_PrgVerId, _
    '                                            Filter_StuName, strStatusCodeId, _
    '                                                    "", "", _
    '                                    Filter_UseTimeclock, Filter_BadgeNum, ARCommon.GetCampusID(), Filter_StudentGrpId, Session("OutOfSchoolEnrollmentStatus"))

    '            dsStudent = AttendanceFacade.GetStudentSchedulesForAttendance( _
    '                                    Filter_ProgId, Filter_PrgVerId, _
    '                                    Filter_StuName, strStatusCodeId, _
    '                                    strStartDate, strEndDate, _
    '                                    Filter_UseTimeclock, Filter_BadgeNum, ARCommon.GetCampusID(), Filter_StudentGrpId, Session("OutOfSchoolEnrollmentStatus"))
    '            Session("AttendanceDS") = dsAttendance
    '            Session("StudentDS") = dsStudent
    '            Me.BindRepeater()
    '            '' New code Added On April 22, 2010
    '            For Each drSA As DataRow In dsStudent.Tables(0).Rows
    '                Dim errMsg As String = ""
    '                Dim UseTimeClock As Boolean = False
    '                Dim dtfacade As New FAME.AdvantageV1.BusinessRules.GetDateFromHoursBR
    '                '' New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167
    '                ''Dim dtExpectedDate As DateTime = dtfacade.GetDateFromHours(drSA("StuEnrollID").ToString, errMsg, UseTimeClock)
    '                Dim dtExpectedDate As DateTime = dtfacade.GetDateFromHours(drSA("StuEnrollID").ToString, errMsg, UseTimeClock, CampusId)
    '                '' New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167
    '                If SingletonAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" Then
    '                    If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue And UseTimeClock = True Then
    '                        If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue Then
    '                            Dim upDateExpGradDatefacade As New StudentEnrollmentFacade
    '                            upDateExpGradDatefacade.UpdateExpectedGraduationDate(dtExpectedDate, drSA("StuEnrollID").ToString)
    '                            res = "Expected graduation date has been recalculated"
    '                        Else
    '                            res = errMsg
    '                        End If
    '                    Else
    '                        res = errMsg
    '                    End If
    '                Else
    '                    res = ""
    '                End If
    '            Next
    '            '' New code Added On April 22, 2010
    '        End If
    '        Return res ' return the result of the update
    '    Catch ex As System.Exception
    '    	Dim exTracker = new AdvApplicationInsightsInitializer()
    '    	exTracker.TrackExceptionWrapper(ex)

    '        Return ex.Message
    '    End Try
    'End Function
    Private Function AddPostClockAttendance(ByVal dsAttendance As DataSet) As String 'This Procedure contains the update logic
        'Get the contents of datatable and pass it as xml document

        Dim rtn As String = String.Empty

        If Not dsAttendance Is Nothing Then
            For Each lcol As DataColumn In dsAttendance.Tables(0).Columns
                lcol.ColumnMapping = System.Data.MappingType.Attribute
            Next
            Dim strXML As String = dsAttendance.GetXml
            rtn = (New AttendanceFacade).PostClockAttendance(strXML)

        End If
        Return ""
    End Function
    Public Function Handle_Save() As String Implements IMaintFormBase.Handle_Save
        Try
            Dim res As String = "" 'ValidateForm()
            If res <> "" Then Return res
            Dim user As String = ARCommon.GetUserNameFromUserId(ARCommon.GetCurrentUserId())
            Dim ds As New DataSet
            'ds = SaveRepeaterToDataTable(user)
            ds = SaveRepeaterToDataTable_New(user, CampusId)

            'Dim info() As ClockHourAttendanceInfo = AttendanceFacade.DSToClockHourAttendanceInfoArray(ds)
            ''Modified by SAraswathi lakshmanan on April 1 2010
            ''CamPusID validation added For Ross Snow Days issue
            'res = AttendanceFacade.SaveClockHourAttendanceArray(AttendanceFacade.DSToClockHourAttendanceInfoArray(ds), user, CampusId) 'AttendanceFacade.SaveClockHourAttendanceArray(info, user)
            res = AddPostClockAttendance(ds)
            ' if the update was successful, rebind the form so that everything display correctly.
            ' Also, this will update the ModDate so that clicking Save followed by Delete works properly
            If res = "" Then

                ' need to get the data from database as the schedule value may have changed from previous data pull
                ' need to refresh data
                Dim strStartDate As String = ""
                If Filter_StartDate <> Date.MinValue Then strStartDate = Filter_StartDate.ToShortDateString()
                Dim strEndDate As String = ""
                If Filter_EndDate <> Date.MinValue Then strEndDate = Filter_EndDate.ToShortDateString()
                If Filter_EndDate = Date.MinValue Then strEndDate = Filter_StartDate.AddDays(7).ToShortDateString()
                Dim strStatusCodeId As String = ""
                'If Session("StudentEnrollmentStatus") = "All" Or Session("StudentEnrollmentStatus") = "InSchool" Or Session("StudentEnrollmentStatus") = "OutofSchool" Then
                '    Dim dsStatusCodes As DataSet
                '    dsStatusCodes = AttendanceFacade.GetStatusCodes(CampusId, "Active", Session("StudentEnrollmentStatus"))
                '    If dsStatusCodes.Tables(0).Rows.Count >= 1 Then
                '        For Each row As DataRow In dsStatusCodes.Tables(0).Rows
                '            strStatusCodeId &= row("StatusCodeId").ToString & "','"
                '        Next
                '        strStatusCodeId = Mid(strStatusCodeId, 1, InStrRev(strStatusCodeId, "'") - 2)
                '    End If
                'Else
                ' get the list of InSchool Statuses selected 
                'this value will be coming from PostAttendance.aspx.vb page
                strStatusCodeId = Session("StudentEnrollmentStatus")
                ' End If


                Dim dsAttendance As New DataSet
                Dim dsStudent As New DataSet
                dsAttendance = AttendanceFacade.GetClockHourAttendance(
                                                Filter_ProgId, Filter_PrgVerId,
                                                Filter_StuName, strStatusCodeId,
                                                        strStartDate, strEndDate,
                                        Filter_UseTimeclock, Filter_BadgeNum, ARCommon.GetCampusID(), Filter_StudentGrpId, Session("OutSchoolEnrollmentStatus"))

                dsStudent = AttendanceFacade.GetStudentSchedulesForAttendance(
                                        Filter_ProgId, Filter_PrgVerId,
                                        Filter_StuName, strStatusCodeId,
                                        strStartDate, strEndDate,
                                        Filter_UseTimeclock, Filter_BadgeNum, ARCommon.GetCampusID(), Filter_StudentGrpId, Session("OutSchoolEnrollmentStatus"))
                Session("AttendanceDS") = dsAttendance
                Session("StudentDS") = dsStudent
                Me.BindRepeater()
                Dim studentEnrollments As List(Of Guid) = New List(Of Guid)()

                '' New code Added On April 22, 2010
                For Each drSA As DataRow In dsStudent.Tables(0).Rows
                    Dim errMsg As String = ""
                    Dim UseTimeClock As Boolean = False
                    Dim dtfacade As New FAME.AdvantageV1.BusinessRules.GetDateFromHoursBR
                    '' New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167
                    ''Dim dtExpectedDate As DateTime = dtfacade.GetDateFromHours(drSA("StuEnrollID").ToString, errMsg, UseTimeClock)
                    Dim dtExpectedDate As DateTime = dtfacade.GetDateFromHours(drSA("StuEnrollID").ToString, errMsg, UseTimeClock, CampusId)
                    Dim stuEnrollId = drSA("StuEnrollID").ToString


                    If stuEnrollId IsNot Nothing AndAlso stuEnrollId IsNot String.Empty Then
                        Dim gStuEnrollId = New Guid(stuEnrollId)

                        If (Not studentEnrollments.Contains(gStuEnrollId)) Then
                            studentEnrollments.Add(New Guid(stuEnrollId))
                        End If
                    End If

                    '' New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167
                    If MyAdvAppSettings.AppSettings("TrackSapAttendance", CampusId).ToString.ToLower = "byday" Then
                        If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue And UseTimeClock = True Then
                            If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue Then
                                Dim upDateExpGradDatefacade As New StudentEnrollmentFacade
                                upDateExpGradDatefacade.UpdateExpectedGraduationDate(dtExpectedDate, drSA("StuEnrollID").ToString)

                                res = "Expected graduation date has been recalculated"
                            Else
                                res = errMsg
                            End If
                        Else
                            res = errMsg
                        End If
                    Else
                        res = ""
                    End If
                Next
                '' New code Added On April 22, 2010
                If studentEnrollments.Count > 0 Then
                    TitleIVSapUpdate(studentEnrollments)
                    PostPaymentPeriodAttendanceAFA(studentEnrollments)
                End If

            End If


            Return res ' return the result of the update
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function
    Public ReadOnly Property LHSImageUrl() As String Implements IMaintFormBase.LHSImageUrl
        Get
            Return "~/images/AR/lhs_grdcomptypes.gif"
        End Get
    End Property
    Public Property ModDate() As Date Implements IMaintFormBase.ModDate
        Get
            Return ViewState("moddate_ClockAttendance")
        End Get
        Set(ByVal value As Date)
            ViewState("moddate_ClockAttendance") = value
        End Set
    End Property
    Public Property ObjId() As String Implements IMaintFormBase.ObjId
        Get
            Return ViewState("objId_ClockAttendance")
        End Get
        Set(ByVal value As String)
            ViewState("objId_ClockAttendance") = value
        End Set
    End Property
    Public Property ParentId() As String Implements IMaintFormBase.ParentId
        Get
            Return ViewState("parentId_ClockAttendance")
        End Get
        Set(ByVal value As String)
            ViewState("parentId_ClockAttendance") = value
        End Set
    End Property
    Public Property Title() As String Implements IMaintFormBase.Title
        Get
            Return "Post Attendance"
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Protected Sub btnShowPostedValues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowPostedValues.Click
        Dim DisplayDate As Date
        DisplayDate = Filter_StartDate

        ''''' Code changes by Kamalesh Ahuja on 23 August 2010 to resolve mantis issue id 19605
        RepackParams(ObjId)
        '''''''''''

        BindForm(ObjId, CampusId, , Session("StudentEnrollmentStatus"), Session("OutSchoolEnrollmentStatus"))
        Filter_StartDate = DisplayDate
        Me.BindRepeater()
        BindDayOfWeek()
    End Sub

    Protected Sub okButtonAttendanceWindowASP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles okButtonAttendanceWindowASP.Click

        Dim postZeroDateValue As Date?
        postZeroDateValue = Date.MinValue
        Date.TryParse(postZeroDate.Text, postZeroDateValue)

        Dim dateIndex As Int32

        If (postZeroDateValue = Date.MinValue) Then
            Exit Sub
        End If

        dateIndex = (New Date(postZeroDateValue.Value.Year, postZeroDateValue.Value.Month, postZeroDateValue.Value.Day) - New Date(Filter_StartDate.Year, Filter_StartDate.Month, Filter_StartDate.Day)).Days

        'AD-10734 Post Attendance one day via popup / api call via client'
        'Exit Sub

        For Each rpi As RepeaterItem In Me.rptAttendance.Items
            SetHoursToZero(dateIndex, rpi)
        Next

        Try
            Dim res As String = "" 'ValidateForm()
            Dim user As String = ARCommon.GetUserNameFromUserId(ARCommon.GetCurrentUserId())
            Dim ds As New DataSet
            ds = SaveRepeaterToDataTableSingleDay(user, dateIndex)

            'Dim info() As ClockHourAttendanceInfo = AttendanceFacade.DSToClockHourAttendanceInfoArray(ds)
            ''CAmpusID added  by Saraswathi lakshmanan on April 01 2010
            ''For Ross Snow Days issue
            'res = AttendanceFacade.SaveClockHourAttendanceArray(AttendanceFacade.DSToClockHourAttendanceInfoArray(ds), user, CampusId) 'AttendanceFacade.SaveClockHourAttendanceArray(info, user)
            Dim info = AttendanceFacade.DSToClockHourAttendanceInfoArray(ds)
            Dim stuEnrollIdList = From a In info Where Not ((a.ActualHours = 9999.0 OrElse a.ActualHours = 999.0) And a.SchedHours = 0) Select a.StuEnrollId

            PostZeroForSingleDayRequest(stuEnrollIdList, New Date(postZeroDateValue.Value.Year, postZeroDateValue.Value.Month, postZeroDateValue.Value.Day))

            ' if the update was successful, rebind the form so that everything display correctly.
            ' Also, this will update the ModDate so that clicking Save followed by Delete works properly
            If res = "" Then

                ' need to get the data from database as the schedule value may have changed from previous data pull
                ' need to refresh data
                Dim strStartDate As String = ""
                If Filter_StartDate <> Date.MinValue Then strStartDate = Filter_StartDate.ToShortDateString()
                Dim strEndDate As String = ""
                If Filter_EndDate <> Date.MinValue Then strEndDate = Filter_EndDate.ToShortDateString()

                Dim strStatusCodeId As String = ""
                'If Session("StudentEnrollmentStatus") = "All" Or Session("StudentEnrollmentStatus") = "InSchool" Or Session("StudentEnrollmentStatus") = "OutofSchool" Then
                '    Dim dsStatusCodes As DataSet
                '    dsStatusCodes = AttendanceFacade.GetStatusCodes(CampusId, "Active", Session("StudentEnrollmentStatus"))
                '    If dsStatusCodes.Tables(0).Rows.Count >= 1 Then
                '        For Each row As DataRow In dsStatusCodes.Tables(0).Rows
                '            strStatusCodeId &= row("StatusCodeId").ToString & "','"
                '        Next
                '        strStatusCodeId = Mid(strStatusCodeId, 1, InStrRev(strStatusCodeId, "'") - 2)
                '    End If
                'Else
                ' get the list of InSchool Statuses selected 
                'this value will be coming from PostAttendance.aspx.vb page
                strStatusCodeId = Session("StudentEnrollmentStatus")
                '   End If

                Dim dsAttendance As New DataSet
                Dim dsStudent As New DataSet
                dsAttendance = AttendanceFacade.GetClockHourAttendance(
                                                Filter_ProgId, Filter_PrgVerId,
                                                Filter_StuName, strStatusCodeId,
                                                "", "",
                                        Filter_UseTimeclock, Filter_BadgeNum, ARCommon.GetCampusID(), Filter_StudentGrpId, Session("OutSchoolEnrollmentStatus"))

                dsStudent = AttendanceFacade.GetStudentSchedulesForAttendance(
                                        Filter_ProgId, Filter_PrgVerId,
                                        Filter_StuName, strStatusCodeId,
                                        strStartDate, strEndDate,
                                        Filter_UseTimeclock, Filter_BadgeNum, ARCommon.GetCampusID(), Filter_StudentGrpId, Session("OutSchoolEnrollmentStatus"))
                Session("AttendanceDS") = dsAttendance
                Session("StudentDS") = dsStudent
                Me.BindRepeater()

                Task.Run(Function()
                             '' New code Added On April 22, 2010
                             For Each drSA As DataRow In dsStudent.Tables(0).Rows
                                 Dim errMsg As String = ""
                                 Dim UseTimeClock As Boolean = False
                                 Dim dtfacade As New FAME.AdvantageV1.BusinessRules.GetDateFromHoursBR
                                 '' New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167
                                 ''Dim dtExpectedDate As DateTime = dtfacade.GetDateFromHours(drSA("StuEnrollID").ToString, errMsg, UseTimeClock)
                                 Dim dtExpectedDate As DateTime = dtfacade.GetDateFromHours(drSA("StuEnrollID").ToString, errMsg, UseTimeClock, CampusId)
                                 '' New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167
                                 If MyAdvAppSettings.AppSettings("TrackSapAttendance", CampusId).ToString.ToLower = "byday" Then
                                     If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue And UseTimeClock = True Then
                                         If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue Then
                                             Dim upDateExpGradDatefacade As New StudentEnrollmentFacade
                                             upDateExpGradDatefacade.UpdateExpectedGraduationDate(dtExpectedDate, drSA("StuEnrollID").ToString)
                                             res = ""
                                         Else
                                             res = ""
                                         End If
                                     End If
                                 Else
                                     res = ""
                                 End If
                             Next
                             Return ""
                         End Function)

                '' New code Added On April 22, 2010
            End If

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
            Throw ex
        End Try

    End Sub

    Protected Sub btnpostzero_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnpostzero.Click

        'AD-10734 Post Attendance one day via popup / api call via client'
        'Exit Sub

        For Each rpi As RepeaterItem In Me.rptAttendance.Items
            SetHoursToZero(0, rpi)
            SetHoursToZero(1, rpi)
            SetHoursToZero(2, rpi)
            SetHoursToZero(3, rpi)
            SetHoursToZero(4, rpi)
            SetHoursToZero(5, rpi)
            SetHoursToZero(6, rpi)
        Next

        Try
            Dim res As String = "" 'ValidateForm()
            Dim user As String = ARCommon.GetUserNameFromUserId(ARCommon.GetCurrentUserId())
            Dim ds As New DataSet
            ds = SaveRepeaterToDataTable(user)

            'Dim info() As ClockHourAttendanceInfo = AttendanceFacade.DSToClockHourAttendanceInfoArray(ds)
            ''CAmpusID added  by Saraswathi lakshmanan on April 01 2010
            ''For Ross Snow Days issue
            res = AttendanceFacade.SaveClockHourAttendanceArray(AttendanceFacade.DSToClockHourAttendanceInfoArray(ds), user, CampusId) 'AttendanceFacade.SaveClockHourAttendanceArray(info, user)

            ' if the update was successful, rebind the form so that everything display correctly.
            ' Also, this will update the ModDate so that clicking Save followed by Delete works properly
            If res = "" Then

                ' need to get the data from database as the schedule value may have changed from previous data pull
                ' need to refresh data
                Dim strStartDate As String = ""
                If Filter_StartDate <> Date.MinValue Then strStartDate = Filter_StartDate.ToShortDateString()
                Dim strEndDate As String = ""
                If Filter_EndDate <> Date.MinValue Then strEndDate = Filter_EndDate.ToShortDateString()

                Dim strStatusCodeId As String = ""
                'If Session("StudentEnrollmentStatus") = "All" Or Session("StudentEnrollmentStatus") = "InSchool" Or Session("StudentEnrollmentStatus") = "OutofSchool" Then
                '    Dim dsStatusCodes As DataSet
                '    dsStatusCodes = AttendanceFacade.GetStatusCodes(CampusId, "Active", Session("StudentEnrollmentStatus"))
                '    If dsStatusCodes.Tables(0).Rows.Count >= 1 Then
                '        For Each row As DataRow In dsStatusCodes.Tables(0).Rows
                '            strStatusCodeId &= row("StatusCodeId").ToString & "','"
                '        Next
                '        strStatusCodeId = Mid(strStatusCodeId, 1, InStrRev(strStatusCodeId, "'") - 2)
                '    End If
                'Else
                ' get the list of InSchool Statuses selected 
                'this value will be coming from PostAttendance.aspx.vb page
                strStatusCodeId = Session("StudentEnrollmentStatus")
                '   End If

                Dim dsAttendance As New DataSet
                Dim dsStudent As New DataSet
                dsAttendance = AttendanceFacade.GetClockHourAttendance(
                                                Filter_ProgId, Filter_PrgVerId,
                                                Filter_StuName, strStatusCodeId,
                                                "", "",
                                        Filter_UseTimeclock, Filter_BadgeNum, ARCommon.GetCampusID(), Filter_StudentGrpId, Session("OutSchoolEnrollmentStatus"))

                dsStudent = AttendanceFacade.GetStudentSchedulesForAttendance(
                                        Filter_ProgId, Filter_PrgVerId,
                                        Filter_StuName, strStatusCodeId,
                                        strStartDate, strEndDate,
                                        Filter_UseTimeclock, Filter_BadgeNum, ARCommon.GetCampusID(), Filter_StudentGrpId, Session("OutSchoolEnrollmentStatus"))
                Session("AttendanceDS") = dsAttendance
                Session("StudentDS") = dsStudent
                Me.BindRepeater()
                '' New code Added On April 22, 2010
                For Each drSA As DataRow In dsStudent.Tables(0).Rows
                    Dim errMsg As String = ""
                    Dim UseTimeClock As Boolean = False
                    Dim dtfacade As New FAME.AdvantageV1.BusinessRules.GetDateFromHoursBR
                    '' New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167
                    ''Dim dtExpectedDate As DateTime = dtfacade.GetDateFromHours(drSA("StuEnrollID").ToString, errMsg, UseTimeClock)
                    Dim dtExpectedDate As DateTime = dtfacade.GetDateFromHours(drSA("StuEnrollID").ToString, errMsg, UseTimeClock, CampusId)
                    '' New Code Added By Vijay Ramteke On June 15, 2010 For Mantis Id 19167
                    If MyAdvAppSettings.AppSettings("TrackSapAttendance", CampusId).ToString.ToLower = "byday" Then
                        If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue And UseTimeClock = True Then
                            If errMsg = "" And Not dtExpectedDate = DateTime.MaxValue Then
                                Dim upDateExpGradDatefacade As New StudentEnrollmentFacade
                                upDateExpGradDatefacade.UpdateExpectedGraduationDate(dtExpectedDate, drSA("StuEnrollID").ToString)
                                res = ""
                            Else
                                res = ""
                            End If
                        End If
                    Else
                        res = ""
                    End If
                Next
                '' New code Added On April 22, 2010
            End If

        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try

    End Sub
    Protected Sub SetHoursToZero(ByVal dow As Integer, ByVal rpi As RepeaterItem)

        Dim txt As TextBox = Nothing
        Dim ctlId As String = "txtD" + dow.ToString()

        Dim obj As Object = rpi.FindControl(ctlId)
        Dim obj1 As Object
        If obj Is Nothing Then Exit Sub ' Failed to find the control

        txt = CType(obj, TextBox)
        If txt.Text <> "" Then Exit Sub ' value already posted, don't do anything
        'If txt.ReadOnly Then Return ' Cannot post on this date
        'If txt.BackColor.Name <> "0" Then Return

        Dim maxHrsCtlId As String = "hfS" + dow.ToString()
        Dim maxHrsSchedId As String = "hfAP" + dow.ToString
        Dim hfDateId As String = "hfD" + dow.ToString()
        'Dim hfDate As HiddenField = CType(FindControl(hfDateId), HiddenField)
        Dim hfDate As HiddenField = TryCast((From item As RepeaterItem In rptAttendance.Controls
                                             Where item.ItemType = ListItemType.Header).SingleOrDefault.FindControl(hfDateId), HiddenField)
        Dim postbyexceptionId As String = "hfException" + dow.ToString
        Dim hfException As HiddenField = CType(rpi.FindControl(postbyexceptionId), HiddenField)
        Dim hDate As Date = CType(hfDate.Value, Date)


        obj = rpi.FindControl(maxHrsCtlId)
        obj1 = rpi.FindControl(maxHrsSchedId)
        If obj Is Nothing Then Return ' Error, max hours has not been defined
        Dim hfMaxHrs As HiddenField = CType(obj, HiddenField)
        Dim hfMaxSchedHrs As HiddenField = CType(obj1, HiddenField)
        Dim dtGradDate As Date
        If Not CType(rpi.FindControl("hfGradDate"), HiddenField).Value = "" Then
            dtGradDate = CType(CType(rpi.FindControl("hfGradDate"), HiddenField).Value, Date)
        Else
            dtGradDate = "01/01/1900"
        End If
        Dim dtDateDetermined As Date = "01/01/1900"
        If Not CType(rpi.FindControl("hfDateDetermined"), HiddenField).Value = "" Then
            dtDateDetermined = CType(CType(rpi.FindControl("hfDateDetermined"), HiddenField).Value, Date)
        Else
            dtDateDetermined = "01/01/1900"
        End If
        Dim dtStartDate As Date = CType(rpi.FindControl("hfStudentStartDate"), HiddenField).Value

        ' disable the cell if the day is a Holiday

        If hDate > Date.Now Then
            txt.ReadOnly = True 'bUseTimeClock
            txt.ToolTip = "Cannot post on a future date."
            Exit Sub
        End If

        ' disable the cell if the day is less than the student's start date
        Dim StudentStartDate As Date = CType(rpi.FindControl("hfStudentStartDate"), HiddenField).Value
        If Not DBNull.Value.Equals(StudentStartDate) Then
            If hDate < StudentStartDate Then
                txt.ReadOnly = True
                txt.ToolTip = "Entries are disabled because this day is before the student's start date."
                Exit Sub
            End If
        End If
        ''Added on jan 26 2008
        ' disable the cell if the day is after Graduation date
        If dtGradDate <> "01/01/1900" Then
            If hDate > dtGradDate Then
                txt.ReadOnly = True
                txt.ToolTip = "Entries are disabled because this day is after the student's Graduation date."
                Exit Sub
            End If
        End If

        Dim hfSysStatusId As HiddenField = CType(rpi.FindControl("hfSysStatusId"), HiddenField)
        Dim sysStatusId As StudentStatusCodes = StudentStatusCodes.Currently_Attending
        If hfSysStatusId.Value <> "" Then sysStatusId = CType(hfSysStatusId.Value, StudentStatusCodes)
        If Not (sysStatusId = StudentStatusCodes.Currently_Attending Or
                sysStatusId = StudentStatusCodes.Future_Start Or
                sysStatusId = StudentStatusCodes.LOA Or
                sysStatusId = StudentStatusCodes.Probation Or
                sysStatusId = StudentStatusCodes.Suspension Or
                sysStatusId = StudentStatusCodes.Suspended Or
                sysStatusId = StudentStatusCodes.Dropped) Then
            txt.ReadOnly = True 'bUseTimeClock '
            If Not ((DateDiff(DateInterval.Day, dtStartDate, hDate) >= 0 And dtGradDate <> "01/01/1900" And hDate <= dtGradDate) Or
               (DateDiff(DateInterval.Day, dtStartDate, hDate) >= 0 And dtDateDetermined <> "01/01/1900" And hDate <= dtDateDetermined)) Then
                txt.ReadOnly = True 'bUseTimeClock '
                txt.ToolTip = "Cannot enter attendance because student's status is 'non-enrolled'"
                Exit Sub
            End If
        End If

        Dim hfEnrollId As String = CType(rpi.FindControl("hfStuEnrollId"), HiddenField).Value

        If (sysStatusId = StudentStatusCodes.Currently_Attending Or
                      sysStatusId = StudentStatusCodes.Future_Start Or
                      sysStatusId = StudentStatusCodes.LOA Or
                      sysStatusId = StudentStatusCodes.Probation Or
                      sysStatusId = StudentStatusCodes.Suspension Or
                      sysStatusId = StudentStatusCodes.Suspended) Then

            ''Added by Saraswathi lakshmanan on Nov 17 2008
            ''To disable entry for the dates where the student was in LOA
            Dim IsLOADate As Boolean
            IsLOADate = (New AttendanceFacade).IsStudentLOADate(hfEnrollId.ToString, hDate)
            If IsLOADate = True Then
                txt.ReadOnly = True 'bUseTimeClock '
                txt.ToolTip = "Cannot enter attendance because student's status is 'LOA'"
                Exit Sub
            End If
        End If

        ''Similarly disable the entry for suspended time frame of the student
        ''Added by Saraswathi lakshmanan on Nov 17 2008
        ''To disable entry for the dates where the student was in Suapension
        If (sysStatusId = StudentStatusCodes.Currently_Attending Or
                sysStatusId = StudentStatusCodes.Future_Start Or
                sysStatusId = StudentStatusCodes.LOA Or
                sysStatusId = StudentStatusCodes.Probation Or
                sysStatusId = StudentStatusCodes.Suspension Or
                sysStatusId = StudentStatusCodes.Suspended) Then
            Dim IsSuspendedDate As Boolean
            IsSuspendedDate = (New AttendanceFacade).IsStudentSuspendedDate(hfEnrollId.ToString, hDate)
            If IsSuspendedDate = True Then
                txt.ReadOnly = True 'bUseTimeClock '
                txt.ToolTip = "Cannot enter attendance because student's status is 'Suspended'"
                Exit Sub
            End If
        End If



        If sysStatusId = StudentStatusCodes.Dropped Then
            'get the date student was dropped
            Dim strDateDropped As String = (New AttendanceFacade).GetLDAForDroppedStudent(hfEnrollId.ToString)
            If strDateDropped <> "" Then
                If hDate > CDate(strDateDropped) Then
                    txt.ReadOnly = True
                    txt.ToolTip = "Entries are disabled because student was dropped."
                    Exit Sub
                End If
            End If
        End If
        'If IsHoliday(hDate) Then
        '    If SingletonAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
        '        txt.ReadOnly = True
        '        txt.ToolTip = "Entries are disabled because this day is a holiday."
        '        Exit Sub
        '    Else
        '        hfMaxHrs.Value = "0.00"
        '        txt.Text = "0.00"
        '        Exit Sub
        '    End If
        'End If

        ''Post Zero for all the days irrespective of whether it is scheduled or not
        'If hfMaxHrs.Value <> "" AndAlso hfMaxHrs.Value <> "0" AndAlso CType(hfMaxHrs.Value, Decimal) <> 0.0 Then
        '    txt.Text = "0.00"
        'End If
        ' If hfMaxHrs.Value <> "" AndAlso hfMaxHrs.Value <> "0" AndAlso CType(hfMaxHrs.Value, Decimal) <> 0.0 Then
        txt.Text = "0.00"
        ' End If
    End Sub
    ''To find the cell color
    Protected Function FindCellColor(ByVal i As Integer, ByVal e As RepeaterItemEventArgs) As String
        Dim tdId As String = "i" + i.ToString()
        Return CType(e.Item.FindControl(tdId), HtmlTableCell).BgColor.ToString()
    End Function

    ''Added by Saraswathi
    ''To find the scheduled hrs for a student
    Protected Sub FindSchedHrs(ByVal i As Integer, ByVal e As RepeaterItemEventArgs)

        ' Get the sched hours hiddenfield
        Dim hfMaxHrsId As String = "hfS" + i.ToString()
        Dim hfMaxHrs As HiddenField = CType(e.Item.FindControl(hfMaxHrsId), HiddenField)

        Dim hfDateId As String = "hfD" + i.ToString()

        Dim hfDate As HiddenField = TryCast((From item As RepeaterItem In rptAttendance.Controls
                                             Where item.ItemType = ListItemType.Header).SingleOrDefault.FindControl(hfDateId), HiddenField)

        Dim stuenrollid = e.Item.DataItem("StuEnrollid").ToString

        'Dim hfDate As HiddenField = CType(FindControl(hfDateId), HiddenField)
        Dim hDate As Date = CType(hfDate.Value, Date)
        Dim dtGradDate As Date = "01/01/1900"
        Dim hfHolId As String = "hfHol" + i.ToString()
        Dim hfHolFld As HiddenField = CType(FindControl(hfHolId), HiddenField)

        If Not CType(e.Item.FindControl("hfGradDate"), HiddenField).Value = "" Then
            dtGradDate = CType(CType(e.Item.FindControl("hfGradDate"), HiddenField).Value, Date)
        Else
            dtGradDate = "01/01/1900"
        End If
        Dim dtDateDetermined As Date = "01/01/1900"
        If Not CType(e.Item.FindControl("hfDateDetermined"), HiddenField).Value = "" Then
            dtDateDetermined = CType(CType(e.Item.FindControl("hfDateDetermined"), HiddenField).Value, Date)
        Else
            dtDateDetermined = "01/01/1900"
        End If
        Dim dtStartDate As Date = CType(e.Item.FindControl("hfStudentStartDate"), HiddenField).Value
        Dim ctlId As String = "txtD" + i.ToString()
        'Dim txt As eWorld.UI.NumericBox = CType(e.Item.FindControl(ctlId), eWorld.UI.NumericBox)
        Dim txt As TextBox = CType(e.Item.FindControl(ctlId), TextBox)


        Dim ds As New DataSet
        If Not Session("StatusChangeHistory") Is Nothing Then
            ds = Session("StatusChangeHistory")
        End If

        'get status on given date
        Dim currSysStatusId As String = ds.Tables(0).AsEnumerable().Where(Function(x) x("StuEnrollid").ToString().Equals(stuenrollid) AndAlso Date.Parse(x("DateOfChange").ToString()) <= hDate) _
        .OrderByDescending(Function(x) Date.Parse(x("DateOfChange"))) _
        .Select(Function(x) x("SysStatusId").ToString()).FirstOrDefault()


        ' disable the cell if the day is less than the student's start date
        If Not DBNull.Value.Equals(e.Item.DataItem("StudentStartDate")) Then
            If hDate < CType(e.Item.DataItem("StudentStartDate"), Date) Then
                hfMaxHrs.Value = "0.00"
                Return
            End If
        End If
        If dtGradDate <> "01/01/1900" Then
            If hDate > dtGradDate Then
                hfMaxHrs.Value = "0.00"
                Return
            End If
        End If

        ' disable the cell if the day is a Holiday
        If IsHoliday(hDate) And MyAdvAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
            'If SingletonAppSettings.AppSettings("postattendanceonholiday").ToString.ToLower = "no" Then
            hfMaxHrs.Value = "0.00"
            Return
            'End If
        End If


        Dim hfSysStatusId As HiddenField = CType(e.Item.FindControl("hfSysStatusId"), HiddenField)
        Dim sysStatusId As StudentStatusCodes = StudentStatusCodes.Currently_Attending
        If hfSysStatusId.Value <> "" Then sysStatusId = CType(hfSysStatusId.Value, StudentStatusCodes)

        If Not (sysStatusId = StudentStatusCodes.Currently_Attending Or
                sysStatusId = StudentStatusCodes.Future_Start Or
                sysStatusId = StudentStatusCodes.LOA Or
                sysStatusId = StudentStatusCodes.Probation Or
                sysStatusId = StudentStatusCodes.Suspension Or
                sysStatusId = StudentStatusCodes.Suspended Or
                sysStatusId = StudentStatusCodes.Dropped Or
                sysStatusId = StudentStatusCodes.Transfer_Out) Then
            If Not ((DateDiff(DateInterval.Day, dtStartDate, hDate) >= 0 And dtGradDate <> "01/01/1900" And hDate <= dtGradDate) Or
                   (DateDiff(DateInterval.Day, dtStartDate, hDate) >= 0 And dtDateDetermined <> "01/01/1900" And hDate <= dtDateDetermined)) Then
                hfMaxHrs.Value = "0.00"
                Return ' we're done with color rules                
            End If
        End If

        If (sysStatusId = StudentStatusCodes.Currently_Attending Or
                sysStatusId = StudentStatusCodes.Future_Start Or
                sysStatusId = StudentStatusCodes.LOA Or
                sysStatusId = StudentStatusCodes.Probation Or
                sysStatusId = StudentStatusCodes.Suspension Or
                sysStatusId = StudentStatusCodes.Suspended) Then
            ''To disable entry for the dates where the student was in LOA
            Dim IsLOADate As Boolean
            IsLOADate = (New AttendanceFacade).IsStudentLOADate(e.Item.DataItem("StuEnrollid").ToString, hDate)
            If IsLOADate = True Then
                hfMaxHrs.Value = "0.00"
                Return ' we're done with color rules    
            End If
        End If

        ''Similarly disable the entry for suspended time frame of the student
        ''Added by Saraswathi lakshmanan on Nov 17 2008
        ''To disable entry for the dates where the student was in Suapension
        If (sysStatusId = StudentStatusCodes.Currently_Attending Or
                sysStatusId = StudentStatusCodes.Future_Start Or
                sysStatusId = StudentStatusCodes.LOA Or
                sysStatusId = StudentStatusCodes.Probation Or
                sysStatusId = StudentStatusCodes.Suspension Or
                sysStatusId = StudentStatusCodes.Suspended) Then
            Dim IsSuspendedDate As Boolean
            IsSuspendedDate = (New AttendanceFacade).IsStudentSuspendedDate(e.Item.DataItem("StuEnrollid").ToString, hDate)
            If IsSuspendedDate = True Then
                hfMaxHrs.Value = "0.00"
                Return ' we're done with color rules    
            End If
        End If

        'Response.write(e.Item.DataItem("StuEnrollId"))
        If sysStatusId = StudentStatusCodes.Dropped Then
            'get the date student was dropped
            Dim strDateDropped As String = (New AttendanceFacade).GetLDAForDroppedStudent(stuenrollid, 12)
            If strDateDropped <> "" Then
                If hDate >= CDate(strDateDropped) Then
                    hfMaxHrs.Value = "0.00"
                    Return ' we're done with color rules    
                End If
            End If
        End If

        If sysStatusId = StudentStatusCodes.Transfer_Out Then
            'get the date student was transferred
            Dim strDateDropped As String = (New AttendanceFacade).GetLDAForDroppedStudent(e.Item.DataItem("StuEnrollid").ToString, 19)
            If strDateDropped <> "" Then
                If hDate >= CDate(strDateDropped) Then
                    hfMaxHrs.Value = "0.00"
                    Return ' we're done with color rules    
                End If
            End If
        End If

        If currSysStatusId <> "" Then
            If (CType(currSysStatusId, StudentStatusCodes) = StudentStatusCodes.Dropped) Then
                hfMaxHrs.Value = "0.00"
            End If
        End If
    End Sub

    Protected Sub btnPostComments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPostComments.Click

        Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsSmall
        Dim name As String = "AttendanceComments"
        Dim url As String = "AttendanceComments.aspx"
        CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    End Sub
    'Protected Sub btnImportFromExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportFromExcel.Click
    '    OpenPopUp()
    'End Sub
    'Private Sub OpenPopUp()
    '    '   setup the properties of the new window
    '    Dim winSettings As String = FAME.AdvantageV1.Common.AdvantageCommonValues.ChildWindowSettingsAdReqs
    '    Dim name As String = "ImportAttendanceFromExcel"
    '    Dim url As String = "../FA/ImportAttendanceFromExcel.aspx?resid=610&mod=FA"
    '    CommonWebUtilities.OpenChildWindow(Page, url, name, winSettings)
    'End Sub
    Private Sub TitleIVSapUpdate(StudentEnrollmentsList)
        If (Not Session("AdvantageApiToken") Is Nothing) Then
            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim TitleIVSAPRequest As New TitleIVSAPRequest(tokenResponse.ApiUrl, tokenResponse.Token)
            Dim pass As Boolean? = TitleIVSAPRequest.TitleIVSapCheck(StudentEnrollmentsList)
        End If
    End Sub

    Private Sub PostZeroForSingleDayRequest(StudentEnrollmentsList As IEnumerable(Of String), RDate As Date)
        If (Not Session("AdvantageApiToken") Is Nothing) Then
            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim postZeroRequest As New PostZeroRequest(tokenResponse.ApiUrl, tokenResponse.Token)
            Dim pass As Boolean = postZeroRequest.PostZerosForSingleDay(StudentEnrollmentsList, RDate)
        End If
    End Sub

    Private Async Function PostPaymentPeriodAttendanceAFA(StudentEnrollments As List(Of Guid)) As Task(Of Boolean)
        If (Not Session("AdvantageApiToken") Is Nothing) Then
            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
            Dim connectionString = MyAdvAppSettings.AppSettings("ConnectionString").ToString

            'Send updated to AFA if integration is enabled(handled internally)
            Return Await New PaymentPeriodHelper(connectionString, tokenResponse.ApiUrl, tokenResponse.Token, AdvantageSession.UserState.UserName) _
                    .PostPaymentPeriodAttendance(CampusId, StudentEnrollments)
        End If
        Return False
    End Function
End Class

