' ===============================================================================
'
' FAME AdvantageV1
'
' Lenders.aspx.vb
'
' ===============================================================================
' Copyright (C) 2003-2004 FAME Inc.
' All rights reserved.
Imports FAME.Advantage.Common
Imports FAME.common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports System.Data
Imports BO = Advantage.Business.Objects
Imports System.Collections

Partial Class Lenders
    Inherits BasePage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents rfvStatus As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents rfvCode As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents rfvLenderDescrip As System.Web.UI.WebControls.RequiredFieldValidator


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Protected ddlDS As New DataSet
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Protected campusId, userId As String
    Protected strDefaultCountry As String
    Protected MyAdvAppSettings As AdvAppSettings

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Dim objCommon As New CommonUtilities
        Dim m_Context As HttpContext
        campusid = Master.CurrentCampusId

        strDefaultCountry = (New CountyFacade).GetDefaultCountry().ToString
        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState
        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ModuleId = HttpContext.Current.Request.Params("Mod").ToString

        m_Context = HttpContext.Current
        txtResourceId.Text = ResourceId
        Try
            m_Context.Items("Language") = "En-US"
            m_Context.Items("ResourceId") = ResourceId
        Catch ex As Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        Try
            If Not IsPostBack Then
                objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))

                '   build dropdownlists
                BuildDropDownLists()

                '   bind datalist
                BindDataList()

                '   bind an empty new LenderInfo
                BindLenderData(New LenderInfo)

                Try
                    ddlCountryId.SelectedValue = strDefaultCountry
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlCountryId.SelectedIndex = 0
                End Try

                Try
                    ddlPayCountryId.SelectedValue = strDefaultCountry
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlPayCountryId.SelectedIndex = 0
                End Try

                '   initialize buttons
                InitButtonsForLoad()

                '   disable the first time.
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    BuildSession()
                End If
            Else
                GetInputMaskValue()
                objCommon.SetCaptionsAndColorRequiredFields(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder))
                InitButtonsForEdit()
            End If

            chkForeignAddress.Text = "International"
            chkForeignCustService.Text = "International"
            chkForeignFax.Text = "International"
            chkForeignPayAddress.Text = "International"
            chkForeignPostClaim.Text = "International"
            chkForeignPreClaim.Text = "International"

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            'Redirect to error page.
            If ex.InnerException Is Nothing Then
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " "
            Else
                Session("Error") = "Error in Sub Page_Load " & ex.Message & " " & ex.InnerException.Message
            End If
            Response.Redirect("../ErrorPage.aspx")
        End Try
    End Sub

    Private Sub BuildSession()
        With ddlLenderCodeId
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("LenderCode", txtLenderId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With

    End Sub

    Private Sub BindDataList()
        Dim strStatus As String

        If radstatus.SelectedItem.Text.ToLower = "active" Then
            strStatus = "True"
        ElseIf radstatus.SelectedItem.Text.ToLower = "inactive" Then
            strStatus = "False"
        Else
            strStatus = "All"
        End If

        '   bind Lenders datalist
        dlstLenders.DataSource = (New StuAwardsFacade).GetAllLenders(strStatus, campusId)
        dlstLenders.DataBind()
    End Sub

    Private Sub BuildDropDownLists()
        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Countries DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlCountryId, AdvantageDropDownListName.Countries, Nothing, True, True))

        'Countries DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlPayCountryId, AdvantageDropDownListName.Countries, Nothing, True, True))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlPayStateId, AdvantageDropDownListName.States, Nothing, True, True))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStateId, AdvantageDropDownListName.States, campusId, True, True))

        'States DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlCampGrpId, AdvantageDropDownListName.CampGrps, Nothing, True, True))

        'Statuses DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlStatusId, AdvantageDropDownListName.Statuses, campusId))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)

    End Sub

    Private Sub GetInputMaskValue()
        Dim facInputMasks As New InputMasksFacade
        Dim strMask As String = ""
        Dim zipMask As String = ""

        If chkForeignCustService.Checked = False Or chkForeignFax.Checked = False Or _
                chkForeignPreClaim.Checked = False Or chkForeignPostClaim.Checked = False Then
            'Get The Input Mask for Phone/Fax
            strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        End If

        If txtOtherState.Text = "" Or txtOtherPayState.Text = "" Then
            zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)
        End If

        'Apply mask only if its local phone number
        If chkForeignCustService.Checked = False Then
            'Replace The Mask Character from # to 9 as Masked Edit TextBox 
            'accepts only certain characters as mask characters
            txtCustService.Mask = Replace(strMask, "#", "9")
            'Get The Format Of the input masks and display it next to caption labels
            lblCustService.ToolTip = strMask
        Else
            txtCustService.Mask = ""
            lblCustService.ToolTip = ""
        End If

        'Apply mask only if its local phone number
        If chkForeignFax.Checked = False Then
            'Replace The Mask Character from # to 9 as Masked Edit TextBox 
            'accepts only certain characters as mask characters
            txtFax.Mask = Replace(strMask, "#", "9")
            'Get The Format Of the input masks and display it next to caption labels
            lblFax.ToolTip = strMask
        Else
            txtFax.Mask = ""
            lblFax.ToolTip = ""
        End If

        'Apply mask only if its local phone number
        If chkForeignPreClaim.Checked = False Then
            'Replace The Mask Character from # to 9 as Masked Edit TextBox 
            'accepts only certain characters as mask characters
            txtPreClaim.Mask = Replace(strMask, "#", "9")
            'Get The Format Of the input masks and display it next to caption labels
            lblPreClaim.ToolTip = strMask
        Else
            txtPreClaim.Mask = ""
            lblPreClaim.ToolTip = ""
        End If

        'Apply mask only if its local phone number
        If chkForeignPostClaim.Checked = False Then
            'Replace The Mask Character from # to 9 as Masked Edit TextBox 
            'accepts only certain characters as mask characters
            txtPostClaim.Mask = Replace(strMask, "#", "9")
            'Get The Format Of the input masks and display it next to caption labels
            lblPostClaim.ToolTip = strMask
        Else
            txtPostClaim.Mask = ""
            lblPostClaim.ToolTip = ""
        End If

        'Apply mask only if its local zip
        If chkForeignAddress.Checked = False Then
            Try
                txtZip.Mask = Replace(zipMask, "#", "9")
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtZip.Text = ""
            End Try
            lblZip.ToolTip = zipMask
        Else
            txtZip.Mask = ""
            lblZip.ToolTip = ""
        End If

        'Apply mask only if its local zip
        If chkForeignPayAddress.Checked = False Then
            Try
                txtPayZip.Mask = Replace(zipMask, "#", "9")
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                txtPayZip.Text = ""
            End Try
            lblPayZip.ToolTip = zipMask
        Else
            txtPayZip.Mask = ""
            lblPayZip.ToolTip = ""
        End If
    End Sub

    Private Function ValidateFieldsWithInputMasks() As String
        Dim facInputMasks As New InputMasksFacade
        Dim correctFormat As Boolean
        Dim strMask As String
        Dim zipMask As String
        Dim errorMessage As String = ""

        strMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMasks.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        'Validate the phone field format. If the field is empty we should not apply the mask agaist it.
        If chkForeignCustService.Checked = False Then
            If txtCustService.Text <> "" Then
                correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtCustService.Text)
                If correctFormat = False Then
                    errorMessage = "- Incorrect format for Customer Service field" & vbCr
                End If
            End If
        End If

        'Validate the phone field format. If the field is empty we should not apply the mask agaist it.
        If chkForeignFax.Checked = False Then
            If txtFax.Text <> "" Then
                correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtFax.Text)
                If correctFormat = False Then
                    errorMessage &= "- Incorrect format for Fax field" & vbCr
                End If
            End If
        End If

        'Validate the phone field format. If the field is empty we should not apply the mask agaist it.
        If chkForeignPreClaim.Checked = False Then
            If txtPreClaim.Text <> "" Then
                correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtPreClaim.Text)
                If correctFormat = False Then
                    errorMessage &= "- Incorrect format for Pre-Claim field" & vbCr
                End If
            End If
        End If

        'Validate the phone field format. If the field is empty we should not apply the mask agaist it.
        If chkForeignPostClaim.Checked = False Then
            If txtPostClaim.Text <> "" Then
                correctFormat = facInputMasks.ValidateStringWithInputMask(strMask, txtPostClaim.Text)
                If correctFormat = False Then
                    errorMessage &= "- Incorrect format for Post-Claim field" & vbCr
                End If
            End If
        End If

        If chkForeignAddress.Checked = False Then
            'Validate the Zip field format. If the field is empty we should not apply the mask
            'against it.
            If txtZip.Text <> "" Then
                correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtZip.Text)
                If correctFormat = False Then
                    errorMessage &= "- Incorrect format for Zip field" & vbCr
                End If
            End If
        End If

        If chkForeignPayAddress.Checked = False Then
            'Validate the Pay Zip field format. If the field is empty we should not apply the mask
            'against it.
            If txtPayZip.Text <> "" Then
                correctFormat = facInputMasks.ValidateStringWithInputMask(zipMask, txtPayZip.Text)
                If correctFormat = False Then
                    errorMessage &= "- Incorrect format for Pay Zip field"
                End If
            End If
        End If
        Return errorMessage
    End Function

    Private Sub dlstLenders_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstLenders.ItemCommand
        '   get the LenderId from the backend and display it
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = ResourceId
        Master.setHiddenControlForAudit()

        GetLenderId(e.CommandArgument)
        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            Dim strUpdate As String = ""
            Try
                ddlLenderCodeId.SelectedValue = (New regentFacade).getAllMaintenanceDataByPrimaryKey("faLenders", "LenderId", e.CommandArgument)
            Catch ex As System.Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

                ddlLenderCodeId.SelectedValue = ""
            End Try
        End If

        '   initialize buttons
        InitButtonsForEdit()
        CommonWebUtilities.RestoreItemValues(dlstLenders, e.CommandArgument)
    End Sub

    Private Sub BindLenderData(ByVal Lender As LenderInfo)
        Dim facInputMask As New InputMasksFacade
        Dim phoneMask As String
        Dim zipMask As String

        phoneMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        With Lender
            chkIsInDB.Checked = .IsInDB
            txtLenderId.Text = .LenderId
            ddlStatusId.SelectedValue = .StatusId
            ddlCampGrpId.SelectedValue = .CampusGroupId
            txtCode.Text = .Code
            txtLenderDescrip.Text = .LenderDescrip
            chkForeignAddress.Checked = .ForeignAddress
            txtAddress1.Text = .Address1
            txtAddress2.Text = .Address2
            txtCity.Text = .City

            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlStateId, .StateId, .State)

            txtOtherState.Text = .OtherState
            txtZip.Text = .Zip
            'Apply mask if needed.
            If chkForeignAddress.Checked = False Then
                txtOtherState.Enabled = False
                If txtZip.Text <> "" Then
                    txtZip.Text = facInputMask.ApplyMask(zipMask, txtZip.Text)
                End If
            Else
                txtOtherState.Enabled = True
            End If

            'ddlCountryId.SelectedValue = .CountryId
            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlCountryId, .CountryId, .Country)

            txtEmail.Text = .Email
            txtPrimaryContact.Text = .PrimaryContact
            txtOtherContact.Text = .OtherContact
            chkIsLender.Checked = .IsLender
            chkIsServicer.Checked = .IsServicer
            chkIsGuarantor.Checked = .IsGuarantor
            chkForeignPayAddress.Checked = .ForeignPayAddress
            txtPayAddress1.Text = .PayAddress1
            txtPayAddress2.Text = .PayAddress2
            txtPayCity.Text = .PayCity

            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPayStateId, .PayStateId, "This State is missing")

            txtOtherPayState.Text = .OtherPayState
            txtPayZip.Text = .PayZip
            'Apply mask if needed.
            If chkForeignPayAddress.Checked = False Then
                txtOtherPayState.Enabled = False
                If txtPayZip.Text <> "" Then
                    txtPayZip.Text = facInputMask.ApplyMask(zipMask, txtPayZip.Text)
                End If
            Else
                txtOtherPayState.Enabled = True
            End If

            'set selected value in ddl
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlPayCountryId, .PayCountryId, "This Country is missing")

            txtCustService.Text = .CustService
            chkForeignCustService.Checked = .ForeignCustService
            'Apply mask if needed.
            If txtCustService.Text <> "" And .ForeignCustService = 0 Then
                txtCustService.Text = facInputMask.ApplyMask(phoneMask, txtCustService.Text)
            End If
            txtFax.Text = .Fax
            chkForeignFax.Checked = .ForeignFax
            'Apply mask if needed.
            If txtFax.Text <> "" And .ForeignFax = 0 Then
                txtFax.Text = facInputMask.ApplyMask(phoneMask, txtFax.Text)
            End If
            txtPreClaim.Text = .PreClaim
            chkForeignPreClaim.Checked = .ForeignPreClaim
            'Apply mask if needed.
            If txtPreClaim.Text <> "" And .ForeignPreClaim = 0 Then
                txtPreClaim.Text = facInputMask.ApplyMask(phoneMask, txtPreClaim.Text)
            End If
            txtPostClaim.Text = .PostClaim
            chkForeignPostClaim.Checked = .ForeignPostClaim
            'Apply mask if needed.
            If txtPostClaim.Text <> "" And .ForeignPostClaim = 0 Then
                txtPostClaim.Text = facInputMask.ApplyMask(phoneMask, txtPostClaim.Text)
            End If
            txtComments.Text = .Comments
            chkIsLender.Enabled = Not .IsUsedAsLender
            chkIsServicer.Enabled = Not .IsUsedAsServicer
            chkIsGuarantor.Enabled = Not .IsUsedAsGuarantor
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
        End With
        GetInputMaskValue()
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '   instantiate component
        Dim resInfo As ResultInfo
        Dim errorMessage As String

        'Check If Mask is successful only if Foreign Is not checked
        If chkForeignCustService.Checked = False Or chkForeignFax.Checked = False Or _
                chkForeignPreClaim.Checked = False Or chkForeignPostClaim.Checked = False Or _
                chkForeignAddress.Checked = False Or chkForeignPayAddress.Checked = False Then
            errorMessage = ValidateFieldsWithInputMasks()
        Else
            errorMessage = ""
        End If

        If errorMessage = "" Then

            With New StuAwardsFacade
                '   update Lender Info 
                resInfo = .UpdateLenderInfo(BuildLenderInfo(txtLenderId.Text), Session("UserName"))
            End With

            '   bind datalist
            BindDataList()

            'Update Lender
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                Dim strUpdate As String = ""
                strUpdate = (New regentFacade).updateMaintenanceData("faLenders", txtLenderId.Text, ddlLenderCodeId.SelectedValue, "MapId", "LenderId")
            End If

            'If Not result = "" Then
            If resInfo.ErrorString <> "" Then
                '   Display Error Message
                DisplayErrorMessage(resInfo.ErrorString)
            Else
                '   get the LenderId from the backend and display it
                BindLenderData(DirectCast(resInfo.UpdatedObject, LenderInfo))
            End If

            '   if there are no errors bind a new entity and init buttons
            If Page.IsValid Then
                '   set the property IsInDB to true in order to avoid an error if the user
                '   hits "save" twice after adding a record.
                chkIsInDB.Checked = True

                'note: in order to display a new page after "save".. uncomment next lines
                '   bind an empty new LenderInfo
                'BindLenderData(New LenderInfo)

                '   initialize buttons
                InitButtonsForEdit()
            End If

        Else
            DisplayErrorMessage(errorMessage)
        End If

        CommonWebUtilities.RestoreItemValues(dlstLenders, txtLenderId.Text)
    End Sub

    Private Function BuildLenderInfo(ByVal LenderId As String) As LenderInfo
        Dim facInputMask As New InputMasksFacade
        Dim phoneMask As String
        Dim zipMask As String

        phoneMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Phone)
        zipMask = facInputMask.GetInputMaskForItem(InputMasksFacade.InputMaskItem.Zip)

        'instantiate class
        Dim LenderInfo As New LenderInfo

        With LenderInfo
            'IsInDB
            .IsInDB = chkIsInDB.Checked

            'LenderId
            .LenderId = LenderId

            'StatusId
            .StatusId = ddlStatusId.SelectedValue

            .CampusGroupId = ddlCampGrpId.SelectedValue

            'Code
            .Code = txtCode.Text

            'LenderDescrip
            .LenderDescrip = txtLenderDescrip.Text

            'ForeignAddress
            .ForeignAddress = IIf(chkForeignAddress.Checked, 1, 0)

            'Address1
            .Address1 = txtAddress1.Text

            'Address2
            .Address2 = txtAddress2.Text

            'City
            .City = txtCity.Text

            'StateId
            .StateId = ddlStateId.SelectedValue

            'OtherState
            .OtherState = txtOtherState.Text

            'Zip
            If txtZip.Text <> "" And chkForeignAddress.Checked = False Then
                .Zip = facInputMask.RemoveMask(zipMask, txtZip.Text)
            Else
                .Zip = txtZip.Text
            End If

            'CountryId
            .CountryId = ddlCountryId.SelectedValue

            'Email
            .Email = txtEmail.Text

            'PrimaryContact
            .PrimaryContact = txtPrimaryContact.Text

            'OtherContact
            .OtherContact = txtOtherContact.Text

            'IsLender
            .IsLender = IIf(chkIsLender.Checked, 1, 0)

            'IsServicer
            .IsServicer = IIf(chkIsServicer.Checked, 1, 0)

            'IsGuarantor
            .IsGuarantor = IIf(chkIsGuarantor.Checked, 1, 0)

            'ForeignAddress
            .ForeignPayAddress = IIf(chkForeignPayAddress.Checked, 1, 0)

            'PayAddress1
            .PayAddress1 = txtPayAddress1.Text

            'PayAddress2
            .PayAddress2 = txtPayAddress2.Text

            'PayCity
            .PayCity = txtPayCity.Text

            'PayStateId
            .PayStateId = ddlPayStateId.SelectedValue

            'PayOtherState
            .OtherPayState = txtOtherPayState.Text

            'PayZip
            If txtPayZip.Text <> "" And chkForeignPayAddress.Checked = False Then
                .PayZip = facInputMask.RemoveMask(zipMask, txtPayZip.Text)
            Else
                .PayZip = txtPayZip.Text
            End If

            'PayCountryId
            .PayCountryId = ddlPayCountryId.SelectedValue

            'CustService
            If txtCustService.Text <> "" And chkForeignCustService.Checked = False Then
                .CustService = facInputMask.RemoveMask(phoneMask, txtCustService.Text)
            Else
                .CustService = txtCustService.Text
            End If

            'ForeignCustService
            .ForeignCustService = IIf(chkForeignCustService.Checked, 1, 0)

            'Fax
            If txtFax.Text <> "" And chkForeignFax.Checked = False Then
                .Fax = facInputMask.RemoveMask(phoneMask, txtFax.Text)
            Else
                .Fax = txtFax.Text
            End If

            'ForeignFax
            .ForeignFax = IIf(chkForeignFax.Checked, 1, 0)

            'PreClaim
            If txtPreClaim.Text <> "" And chkForeignPreClaim.Checked = False Then
                .PreClaim = facInputMask.RemoveMask(phoneMask, txtPreClaim.Text)
            Else
                .PreClaim = txtPreClaim.Text
            End If

            'ForeignPreClaim
            .ForeignPreClaim = IIf(chkForeignPreClaim.Checked, 1, 0)

            'PostClaim
            If txtPostClaim.Text <> "" And chkForeignPostClaim.Checked = False Then
                .PostClaim = facInputMask.RemoveMask(phoneMask, txtPostClaim.Text)
            Else
                .PostClaim = txtPostClaim.Text
            End If

            'ForeignPostClaim
            .ForeignPostClaim = IIf(chkForeignPostClaim.Checked, 1, 0)

            'Comments
            .Comments = txtComments.Text

            'IsUsedAsLender
            .IsUsedAsLender = Not chkIsLender.Enabled

            'IsUsedAsServicer
            .IsUsedAsServicer = Not chkIsServicer.Enabled

            'IsUsedAsGuarantor
            .IsUsedAsGuarantor = Not chkIsGuarantor.Enabled

            'ModUser
            .ModUser = txtModUser.Text

            'ModDate
            .ModDate = Date.Parse(txtModDate.Text)
        End With

        'return data
        Return LenderInfo
    End Function

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        '   bind an empty new LenderInfo
        BindLenderData(New LenderInfo)

        Try
            ddlCountryId.SelectedValue = strDefaultCountry
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlCountryId.SelectedIndex = 0
        End Try

        Try
            ddlPayCountryId.SelectedValue = strDefaultCountry
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            ddlPayCountryId.SelectedIndex = 0
        End Try

        'initialize buttons
        InitButtonsForLoad()

        'BuildSession
        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            BuildSession()
        End If

        CommonWebUtilities.RestoreItemValues(dlstLenders, Guid.Empty.ToString)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not (txtLenderId.Text = Guid.Empty.ToString) Then
            'update Lender Info 
            Dim result As String = (New StuAwardsFacade).DeleteLenderInfo(txtLenderId.Text, Date.Parse(txtModDate.Text))
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
                Exit Sub
            Else
                '   bind datalist
                BindDataList()

                '   bind an empty new LenderInfo
                BindLenderData(New LenderInfo)

                Try
                    ddlCountryId.SelectedValue = strDefaultCountry
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlCountryId.SelectedIndex = 0
                End Try

                Try
                    ddlPayCountryId.SelectedValue = strDefaultCountry
                Catch ex As System.Exception
                 	Dim exTracker = new AdvApplicationInsightsInitializer()
                	exTracker.TrackExceptionWrapper(ex)

                    ddlPayCountryId.SelectedIndex = 0
                End Try

                '   initialize buttons
                InitButtonsForLoad()
                If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                    BuildSession()
                End If
            End If
            CommonWebUtilities.RestoreItemValues(dlstLenders, Guid.Empty.ToString)
        End If
    End Sub

    Private Sub InitButtonsForLoad()
        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        btnDelete.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
    End Sub

    Private Sub GetLenderId(ByVal LenderId As String)
        '   bind Lender properties
        BindLenderData((New StuAwardsFacade).GetLenderInfo(LenderId))
    End Sub

    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub

    Private Sub radStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radstatus.SelectedIndexChanged
        InitButtonsForLoad()
        BindLenderData(New LenderInfo)
        '   bind datalist
        BindDataList()
    End Sub

    Private Sub chkForeignAddress_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignAddress.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtAddress1)
        If chkForeignAddress.Checked Then
            ddlStateId.SelectedValue = System.Guid.Empty.ToString  'FAME.AdvantageV1.Common.AdvantageCommonValues.OtherStateGuid
            ddlStateId.Enabled = False
            txtOtherState.Enabled = True
        Else
            ddlStateId.Enabled = True
            txtOtherState.Text = ""
            txtOtherState.Enabled = False
        End If
        GetInputMaskValue()
    End Sub

    Private Sub chkForeignPayAddress_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPayAddress.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtPayAddress1)
        If chkForeignPayAddress.Checked Then
            ddlPayStateId.SelectedValue = System.Guid.Empty.ToString  'FAME.AdvantageV1.Common.AdvantageCommonValues.OtherStateGuid
            ddlPayStateId.Enabled = False
            txtOtherPayState.Enabled = True
        Else
            ddlPayStateId.Enabled = True
            txtOtherPayState.Text = ""
            txtOtherPayState.Enabled = False
        End If
        GetInputMaskValue()
    End Sub

    Private Sub chkForeignCustService_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignCustService.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtCustService)
    End Sub

    Private Sub chkForeignFax_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignFax.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtFax)
    End Sub

    Private Sub chkForeignPostClaim_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPostClaim.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtPostClaim)
    End Sub

    Private Sub chkForeignPreClaim_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkForeignPreClaim.CheckedChanged
        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtPreClaim)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add save button 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(chkForeignAddress)
        controlsToIgnore.Add(chkForeignCustService)
        controlsToIgnore.Add(chkForeignFax)
        controlsToIgnore.Add(chkForeignPayAddress)
        controlsToIgnore.Add(chkForeignPostClaim)
        controlsToIgnore.Add(chkForeignPreClaim)
        controlsToIgnore.Add(ddlStateId)
        controlsToIgnore.Add(ddlPayStateId)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub

End Class
