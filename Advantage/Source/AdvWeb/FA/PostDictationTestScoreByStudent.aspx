﻿<%@ Page Title="Post Score By Student" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="PostDictationTestScoreByStudent.aspx.vb" Inherits="PostDictationTestScoreByStudent" %>
<%@ MasterType  virtualPath="~/NewSite.master"%> 
<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" Runat="Server">
<script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" Runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false" 
    BorderWidth="0px">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" width="100%"  Orientation="HorizontalTop">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table1">
				<!-- begin rightcolumn -->
				<TR>
					<td class="detailsframetop"><table width="100%" border="0" cellpadding="0" cellspacing="0" ID="Table4">
							<!-- begin top menu (save,new,reset,delete,history)-->
							<tr>
								<td class="menuframe" align="right"><asp:button id="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:button><asp:button id="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False" Enabled="False"></asp:button>
									<asp:button id="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False"
										Enabled="False"></asp:button></td>
							</tr>
						</table>
						<!-- end top menu (save,new,reset,delete,history)-->
						<!--begin right column-->
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="maincontenttable"
							align="center">
							<tr>
								<td class="detailsframe">
                                      <div class="scrollsingleframe">


                                          <table class="contenttable" cellspacing="0" cellpadding="0" width="45%" align="center">
                                              <tr>
                                                  <td class="contentcellnofilter">
                                                      <asp:Label ID="lblPrgVerId" runat="server" CssClass="label">Enrollment</asp:Label>
                                                  </td>
                                                  <td class="contentcell4nofilter">
                                                      <asp:DropDownList ID="ddlPrgVerId" runat="server" AutoPostBack="true" CssClass="dropdownlist"
                                                          Width="320px">
                                                      </asp:DropDownList>
                                                  </td>
                                              </tr>
                                          </table>
                                          <br />
                                          <br />
                                          <asp:Panel ID="pnlRHS" runat="server">
                                              <telerik:RadGrid ID="RadGrdPostScores" runat="server" AllowAutomaticDeletes="True"
                                                  AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True"
                                                  AutoGenerateColumns="False" Width="100%" AllowMultiRowEdit="false">
                                                  <MasterTableView Width="100%" CommandItemDisplay="Top" DataKeyNames="id" HorizontalAlign="NotSet"
                                                      AutoGenerateColumns="False" EditMode="InPlace" PageSize="5" CommandItemSettings-AddNewRecordText="Post new score"
                                                      NoMasterRecordsText="No scores to display">
                                                      <CommandItemSettings AddNewRecordText="Post new score" />
                                                      <Columns>
                                                          <telerik:GridEditCommandColumn ButtonType="ImageButton" HeaderStyle-Width="2%" ItemStyle-Width="2%"
                                                              UniqueName="EditCommandColumn">
                                                              <HeaderStyle Width="2%" />
                                                              <ItemStyle CssClass="myimagebutton" />
                                                          </telerik:GridEditCommandColumn>
                                                          <telerik:GridTemplateColumn HeaderStyle-Width="20%" ItemStyle-Width="20%" UniqueName="TermId">
                                                              <EditItemTemplate>
                                                                  <asp:DropDownList ID="ddlTermId" runat="server" CssClass="dropdownlist" DataSource="<%# dtTerm %>"
                                                                      DataTextField="TermDescrip" DataValueField="TermPeriod">
                                                                  </asp:DropDownList>
                                                              </EditItemTemplate>
                                                              <HeaderTemplate>
                                                                  <asp:Label ID="lblTerm" runat="server" CssClass="label" Text="Term"></asp:Label>
                                                              </HeaderTemplate>
                                                              <ItemTemplate>
                                                                  <asp:Label ID="lblContainer1" runat="server" CssClass="label" Text='<%# Eval("TermDescrip") %>'></asp:Label>
                                                              </ItemTemplate>
                                                              <HeaderStyle Width="25%" />
                                                              <ItemStyle Width="25%" />
                                                          </telerik:GridTemplateColumn>
                                                          <telerik:GridTemplateColumn HeaderStyle-Width="10%" HeaderText="Date Passed" ItemStyle-Width="10%"
                                                              UniqueName="DatePassed">
                                                              <HeaderTemplate>
                                                                  <asp:Label ID="lblHeadDatePassed" runat="server" CssClass="label" Text="Date Passed"></asp:Label>
                                                              </HeaderTemplate>
                                                              <ItemTemplate>
                                                                  <asp:Label ID="lblDatePassed" runat="server" CssClass="label" Text='<%#DataBinder.Eval(Container.DataItem,"DatePassed", "{0:d}") %>'></asp:Label>
                                                              </ItemTemplate>
                                                              <EditItemTemplate>
                                                                  <telerik:RadDatePicker ID="txtDatePassed" runat="server" CssClass="raddatepicker"
                                                                      Culture="English (United States)" daynameformat="Short" DbSelectedDate='<%# DataBinder.Eval(Container.DataItem,"DatePassed", "{0:d}") %>'
                                                                      ForeColor="Black">
                                                                  </telerik:RadDatePicker>
                                                              </EditItemTemplate>
                                                              <HeaderStyle Width="10%" />
                                                              <ItemStyle Width="10%" />
                                                          </telerik:GridTemplateColumn>
                                                          <telerik:GridTemplateColumn HeaderStyle-Width="15%" HeaderText="Test Type" ItemStyle-Width="15%"
                                                              UniqueName="GrdComponentTypeId">
                                                              <HeaderTemplate>
                                                                  <asp:Label ID="lblHeadGrdComponentTypeId" runat="server" CssClass="label" Text="Test Type"></asp:Label>
                                                              </HeaderTemplate>
                                                              <ItemTemplate>
                                                                  <asp:Label ID="lblGrdComponentTypeId" runat="server" CssClass="label" Text='<%#Bind("Descrip") %>'></asp:Label>
                                                              </ItemTemplate>
                                                              <EditItemTemplate>
                                                                  <asp:DropDownList ID="ddlGrdComponentTypeId" runat="server" CssClass="dropdownlist"
                                                                      DataSource="<%# dtGrdCompTypes %>" DataTextField="Descrip" DataValueField="GrdComponentTypeId">
                                                                  </asp:DropDownList>
                                                              </EditItemTemplate>
                                                              <HeaderStyle Width="15%" />
                                                              <ItemStyle Width="15%" />
                                                          </telerik:GridTemplateColumn>
                                                          <telerik:GridTemplateColumn HeaderStyle-Width="10%" HeaderText="Accuracy" ItemStyle-Width="10%"
                                                              UniqueName="Accuracy">
                                                              <HeaderTemplate>
                                                                  <asp:Label ID="lblHeadAccuracy" runat="server" CssClass="label" Text="Accuracy"></asp:Label>
                                                              </HeaderTemplate>
                                                              <ItemTemplate>
                                                                  <asp:Label ID="lblAccuracy" runat="server" CssClass="label" Text='<%#Bind("accuracy") %>'></asp:Label>
                                                              </ItemTemplate>
                                                              <EditItemTemplate>
                                                                  <asp:TextBox ID="txtAccuracy" runat="server" CssClass="textbox" Text='<%#Bind("accuracy") %>'></asp:TextBox>
                                                              </EditItemTemplate>
                                                              <HeaderStyle Width="10%" />
                                                              <ItemStyle Width="10%" />
                                                          </telerik:GridTemplateColumn>
                                                          <telerik:GridTemplateColumn HeaderStyle-Width="10%" HeaderText="Speed" ItemStyle-Width="10%"
                                                              UniqueName="Speed">
                                                              <HeaderTemplate>
                                                                  <asp:Label ID="lblHeadSpeed" runat="server" CssClass="label" Text="Speed"></asp:Label>
                                                              </HeaderTemplate>
                                                              <ItemTemplate>
                                                                  <asp:Label ID="lblSpeed" runat="server" CssClass="label" Text='<%#Bind("speed") %>'></asp:Label>
                                                              </ItemTemplate>
                                                              <EditItemTemplate>
                                                                  <asp:TextBox ID="txtSpeed" runat="server" CssClass="textbox" Text='<%#Bind("speed") %>'></asp:TextBox>
                                                              </EditItemTemplate>
                                                              <HeaderStyle Width="10%" />
                                                              <ItemStyle Width="10%" />
                                                          </telerik:GridTemplateColumn>
                                                          <telerik:GridTemplateColumn HeaderStyle-Width="10%" HeaderText="Grade" ItemStyle-Width="10%"
                                                              UniqueName="Grade">
                                                              <HeaderTemplate>
                                                                  <asp:Label ID="lblHeadGrade" runat="server" CssClass="label" Text="Grade"></asp:Label>
                                                              </HeaderTemplate>
                                                              <ItemTemplate>
                                                                  <asp:Label ID="lblGrade" runat="server" CssClass="label" Text='<%#Bind("Grade") %>'></asp:Label>
                                                              </ItemTemplate>
                                                              <EditItemTemplate>
                                                                  <asp:DropDownList ID="ddlGrdSystemId" runat="server" CssClass="dropdownlist" DataSource="<%# dtGrades %>"
                                                                      DataTextField="Grade" DataValueField="GrdSysDetailId">
                                                                  </asp:DropDownList>
                                                              </EditItemTemplate>
                                                              <HeaderStyle Width="10%" />
                                                              <ItemStyle Width="10%" />
                                                          </telerik:GridTemplateColumn>
                                                          <telerik:GridTemplateColumn HeaderStyle-Width="20%" HeaderText="Instructor" ItemStyle-Width="20%"
                                                              UniqueName="FullName">
                                                              <HeaderTemplate>
                                                                  <asp:Label ID="lblHeadInstructor" runat="server" CssClass="label" Text="Instructor"></asp:Label>
                                                              </HeaderTemplate>
                                                              <ItemTemplate>
                                                                  <asp:Label ID="lblInstructorId" runat="server" CssClass="label" Text='<%#Bind("FullName") %>'></asp:Label>
                                                              </ItemTemplate>
                                                              <EditItemTemplate>
                                                                  <asp:DropDownList ID="ddlInstructorId" runat="server" CssClass="dropdownlist" DataSource="<%# dtInstructors %>"
                                                                      DataTextField="FullName" DataValueField="UserId">
                                                                  </asp:DropDownList>
                                                              </EditItemTemplate>
                                                              <HeaderStyle Width="20%" />
                                                              <ItemStyle Width="20%" />
                                                          </telerik:GridTemplateColumn>
                                                          <telerik:GridTemplateColumn HeaderStyle-Width="20%" HeaderText="istestmentorproctored"
                                                              ItemStyle-Width="20%" UniqueName="istestmentorproctored">
                                                              <HeaderTemplate>
                                                                  <asp:Label ID="lblHeadMentorProctored" runat="server" CssClass="label" Text="Mentor/Proctored?"></asp:Label>
                                                              </HeaderTemplate>
                                                              <ItemTemplate>
                                                                  <asp:Label ID="lblItemMentorProctored" runat="server" CssClass="label" Text='<%# Container.DataItem("istestmentorproctored") %>'></asp:Label>
                                                              </ItemTemplate>
                                                              <EditItemTemplate>
                                                                  <asp:DropDownList ID="ddlEditMentorProctored" runat="server" CssClass="dropdownlist" />
                                                              </EditItemTemplate>
                                                              <HeaderStyle Width="20%" />
                                                              <ItemStyle Width="20%" />
                                                          </telerik:GridTemplateColumn>
                                                          <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete" ConfirmDialogType="Classic"
                                                              ConfirmText="Delete this score?" ConfirmTitle="Delete" HeaderStyle-Width="3%"
                                                              ItemStyle-Width="3%" Text="Delete" UniqueName="DeleteColumn">
                                                              <HeaderStyle Width="3%" />
                                                              <ItemStyle CssClass="myimagebutton" HorizontalAlign="Center" />
                                                          </telerik:GridButtonColumn>
                                                      </Columns>
                                                      <EditFormSettings ColumnNumber="2" CaptionDataField="EmployeeID" CaptionFormatString="Edit properties of Product {0}">
                                                          <EditColumn ButtonType="ImageButton" CancelText="Cancel edit " InsertText="Insert Order"
                                                              UniqueName="EditCommandColumn1" UpdateText="Update record">
                                                          </EditColumn>
                                                          <FormTableStyle BackColor="White" CellPadding="2" CellSpacing="0" Height="110px" />
                                                          <FormMainTableStyle BackColor="White" CellPadding="3" CellSpacing="0" GridLines="None"
                                                              Width="100%" />
                                                          <FormCaptionStyle CssClass="editformheader" />
                                                          <FormTableItemStyle Wrap="False"></FormTableItemStyle>
                                                          <FormTableAlternatingItemStyle Wrap="False"></FormTableAlternatingItemStyle>
                                                          <FormTableButtonRowStyle HorizontalAlign="Right" CssClass="editformbuttonrow"></FormTableButtonRowStyle>
                                                      </EditFormSettings>
                                                  </MasterTableView>
                                                  <ClientSettings>
                                                      <Selecting AllowRowSelect="true" />
                                                  </ClientSettings>
                                              </telerik:RadGrid>
                                          </asp:Panel>
                                          <asp:Panel ID="pnlMessagePanel" runat="server" align="center" CssClass="textbox"
                                              Width="60%">
                                              <asp:Label ID="lblMessage" runat="server" CssClass="labelbold">
                                                           This student is not registered in any of the Courses mapped to the "Dictation/Speed Test" in Grading Component Types. <br /><br />It is not possible to post 
                                                           scores for the student at this time.<br /><br />

                                                           Please register the student in any of the Courses mapped to the "Dictation/Speed Test" Grading Component Types in order to post a score.
                                              </asp:Label>
                                          </asp:Panel>    


										
                                     </div>	
								</td>
							</tr>
						</table>
					</td>
					<!-- end rightcolumn --></TR>
			</table>
			<!-- start validation panel-->
			<asp:panel id="Panel1" runat="server" CssClass="validationsummary"></asp:panel>
			<asp:customvalidator id="Customvalidator1" runat="server" CssClass="validationsummary" Display="None"
				ErrorMessage="CustomValidator"></asp:customvalidator>
			<asp:panel id="pnlRequiredFieldValidators" runat="server"></asp:panel><asp:validationsummary id="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
				ShowMessageBox="True"></asp:validationsummary>
			<!--end validation panel-->
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" Runat="Server">
</asp:Content>

