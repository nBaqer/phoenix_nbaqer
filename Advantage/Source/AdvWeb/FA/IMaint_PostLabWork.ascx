<%@ Control Language="VB" AutoEventWireup="false" CodeFile="IMaint_PostLabWork.ascx.vb"
    Inherits="FA_IMaint_PostLabWork" %>
<%@ Register TagPrefix="ew" Assembly="eWorld.UI" Namespace="eWorld.UI" %>

<script language="javascript" src="ToolTip.js" type="text/javascript"></script>

    <div style="text-align: center; width: 100%">
        <asp:Label ID="lblMsg" runat="server" CssClass="LabelBold" ForeColor="Red" />
    </div>
    <div id="divContent" runat="server" visible="false">
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="ClassSectionLabelCell" nowrap>
                        <span class="Label">Select posting date: </span>
                    </td>
                    <td class="ClassSectionContentCell" style="text-align: left; padding-right: 30px">
                        <ew:CalendarPopup runat="server" ID="calPostDate" CssClass="TextBoxDate" BorderWidth="0"
                            AutoPostBack="True" OnDateChanged="OnPostDateChange" ControlDisplay="TextBoxImage"
                              TextboxLabelStyle-Font-Size="11px" TextboxLabelStyle-ForeColor="333333" DisableTextboxEntry="false" TextboxLabelStyle-Font-Names="verdana"  ImageUrl="~/UserControls/Calendar/PopUpCalendar.gif" style="font-size: 11px; font-family: Verdana" />
                    </td>
                    <td class="ClassSectionLabelCell" nowrap>
                        <span class="Label">Class dates: </span>
                    </td>
                    <td class="ClassSectionContentCell">
                        <asp:Label ID="lblClassDates" runat="server" CssClass="Label" /></td>
                </tr>
                <tr>
                    <td class="ClassSectionLabelCell" nowrap>
                        <span class="Label">Or edit an existing day: </span>
                    </td>
                    <td class="ClassSectionContentCell">
                        <asp:DropDownList ID="ddlPostDate" runat="server" CssClass="DropDownList" AutoPostBack="True" />
                    </td>
                    <td class="ClassSectionLabelCell" nowrap>
                        <asp:Label ID="lblLabHeader" runat="server" CssClass="Label" Text="Number of labs:" />
                    <td class="ClassSectionContentCell">
                        <asp:Label ID="lblNumLabs" runat="server" CssClass="Label" /></td>
                </tr>
                <tr>
                    <td class="ClassSectionLabelCell" nowrap>
                        <asp:Button ID="btnPostByException" runat="server" Text="Post by Exception" /></td>
                    <td class="ClassSectionContentCell">
                        <ew:NumericBox ID="txtExc" runat="server" PositiveNumber="True" CssClass="TextBox" /></td>
                    <td class="ClassSectionLabelCell" nowrap>
                        &nbsp;</td>
                    <td class="ClassSectionContentCell">
                        &nbsp;</td>
                </tr>
            </table>
        </div>
        <div style="margin-top: 20px">
            <table cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #ebebeb">
                <tr>
                    <td class="rptheaders" style="width: 8%">
                        &nbsp;</td>
                    <td class="rptheaders" style="width: 20%">
                        Student Name</td>
                    <td class="rptheaders" style="width: 14%">
                        Completed</td>
                    <td class="rptheaders" style="width: 14%">
                        Remaining</td>
                    <td class="rptheaders" style="width: 14%">
                        <asp:Label ID="lblCountHeader" runat="server" /></td>
                    <td class="rptheaders" style="width: 30%; border-right: 0">
                        Comment</td>
                </tr>
            </table>
        </div>
        <div>
            <asp:Repeater ID="rptLabWork" runat="server">
                <HeaderTemplate>
                    <table cellpadding="0" cellspacing="0" width="100%" style="border: 1px solid #ebebeb;
                        border-top: 0; border-bottom: 0">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="rptitem" style="width: 8%">
                            <asp:ImageButton ID="ibStudentInfo" runat="server" ImageUrl="../Images/FA/RptInfo.gif" /></td>
                        <td class="rptitem" style="width: 20%;">
                            <asp:HiddenField ID="hfGrdBkResultId" runat="server" />
                            <asp:HiddenField ID="hfStuEnrollId" runat="server" />
                            <asp:HiddenField ID="hfLabId" runat="server" />
                            <asp:Label ID="lblStudentName" runat="server" CssClass="Label" /></td>
                        <td class="rptitem" style="width: 14%">
                            <ew:NumericBox ID="txtTotal" runat="server" ReadOnly="True" PositiveNumber="True"
                                CssClass="TextBox" /></td>
                        <td class="rptitem" style="width: 14%">
                            <ew:NumericBox ID="txtRemaining" runat="server" ReadOnly="True" PositiveNumber="True"
                                CssClass="TextBox" /></td>
                        <td class="rptitem" style="width: 14%">
                            <ew:NumericBox ID="txtCount" runat="server" PositiveNumber="True" CssClass="TextBox" /></td>
                        <td class="rptitem" style="width: 30%; border-right: 0">
                            <asp:TextBox ID="txtComment" runat="server" Width="150px" CssClass="TextBox" />
                            <asp:LinkButton ID="lbError" runat="server" Text="Error" CssClass="Label" Visible="False" />
                            <asp:LinkButton ID="lbEdit" runat="server" Text="Details" CssClass="Label" Visible="False" /></td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table> </div>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <asp:HiddenField ID="hfLabType" runat="server" />
        <asp:HiddenField ID="hfClassStartDate" runat="server" />
        <asp:HiddenField ID="hfClassEndDate" runat="server" />
         <asp:RegularExpressionValidator ID = "regExpVal_PostDate" ControlToValidate="calPostDate" 
                    ValidationExpression = "^\d{1,2}\/\d{1,2}\/\d{4}$" ErrorMessage = "Invalid date format" Display = None runat="server">
                    </asp:RegularExpressionValidator>
                 
    </div>

