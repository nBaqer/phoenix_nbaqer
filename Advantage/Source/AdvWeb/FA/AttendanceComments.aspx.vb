
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade

Partial Class FA_AttendanceComments
    Inherits System.Web.UI.Page
    Dim stuEnrollId As String
    Dim StartDate As String

    Protected Sub FA_AttendanceComments_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    End Sub
    Protected Sub FA_AttendanceComments_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            BuildDropdownLists()
            With ddlDates
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
                '.BackColor = Color.FromName("#ffff99")
            End With
        Else
            With ddlStuEnrollId
                '.BackColor = Color.FromName("#ffff99")
            End With
            With ddlDates
                '.BackColor = Color.FromName("#ffff99")
            End With
        End If
    End Sub
    'Private Sub BuildDropdownLists()
    '    With ddlStuEnrollId
    '        '.DataSource = (New AttendanceHistoryFacade).GetStudentNames
    '        e.Item.DataItem("LastName"), e.Item.DataItem("FirstName"), e.Item.DataItem("MiddleName")
    '        .DataTextField = "FullName"
    '        .DataValueField = "StuEnrollId"
    '        .DataBind()
    '        .Items.Insert(0, New ListItem("Select", ""))
    '        .SelectedIndex = 0
    '        .BackColor = Color.FromName("#ffff99")
    '    End With
    'End Sub
    Private Sub BuildDropdownLists()
        With ddlStuEnrollId
            .DataSource = CType(Session("StudentDS"), DataSet)
            .DataTextField = "FullName"
            .DataValueField = "StuEnrollId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
            '.BackColor = Color.FromName("#ffff99")
        End With
    End Sub
    Private Sub BuildDatesList()
        ddlDates.Items.Clear()
        Dim dt As New DataTable
        dt = (New AttendanceHistoryFacade).GetAttendanceDateByStudent(ddlStuEnrollId.SelectedValue)
        If dt.Rows.Count >= 1 Then
            With ddlDates
                .DataSource = dt
                .DataTextField = "RecordDate"
                .DataValueField = "RecordDate"
                .DataBind()
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        Else
            With ddlDates
                .Items.Insert(0, New ListItem("Select", ""))
                .SelectedIndex = 0
            End With
        End If
    End Sub
    Protected Sub ddlDates_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDates.SelectedIndexChanged
        If ddlDates.SelectedIndex >= 1 Then
            txtComments.Text = (New AttendanceHistoryFacade).GetCommentsByDate(ddlStuEnrollId.SelectedValue, ddlDates.SelectedValue)
        End If
    End Sub
    Protected Sub btnPostComments_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPostComments.Click

        Dim strReturn As String
        Dim strMessage As String = ""
        If ddlStuEnrollId.SelectedIndex = 0 Then
            strMessage = "Student is required" & vbCrLf
        End If
        If ddlDates.SelectedIndex = 0 Then
            strMessage &= "Attendance Date is required" & vbCrLf
        End If
        If Not strMessage.ToString.Trim = "" Then
            DisplayErrorMessage(strMessage)
            Exit Sub
        End If
        strReturn = (New AttendanceHistoryFacade).UpdateComments(ddlStuEnrollId.SelectedValue, ddlDates.SelectedValue, txtComments.Text)
        If Not strReturn = "" Then
            DisplayErrorMessage(strReturn)
            Exit Sub
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Protected Sub ddlStuEnrollId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlStuEnrollId.SelectedIndexChanged
        If ddlStuEnrollId.SelectedIndex >= 1 Then
            BuildDatesList()
            txtComments.Text = ""
        End If
    End Sub
End Class
