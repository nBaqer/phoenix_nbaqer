Imports System.Xml
Imports FAME.Advantage.Common
Imports FAME.AdvantageV1.BusinessFacade.AR
Imports BO = Advantage.Business.Objects
Imports FAME.AdvantageV1.Common
Imports FAME.AdvantageV1.BusinessFacade
Imports Telerik.Web.UI

Partial Class FA_PostClockAttendance
    Inherits BasePage
    Protected ResourceId As String
    Protected ModuleId As String
    Private pObj As New UserPagePermissionInfo
    Private formRHS As System.Web.UI.UserControl
    Private campusId As String
    Private userId As String
    Protected MyAdvAppSettings As AdvAppSettings

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' load the RHS control and add it the the RHS panel
        Try
            Dim controlFileName As String = "IMaint_ClockAttendance.ascx"
            formRHS = Me.LoadControl(controlFileName)
            Me.pnlRHS.Controls.Add(formRHS)
            'CType(Master.FindControl("ContentMain2"), ContentPlaceHolder)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim lbl As Label = New Label()
            lbl.Text = "<div>Error loading ascx file.</div><div>" + ex.Message + "</div>"
            lbl.ID = "lblError"
            Me.pnlRHS.Controls.Add(lbl)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        Try
            Dim m_Context As HttpContext
            Dim advantageUserState As New BO.User()
            advantageUserState = AdvantageSession.UserState
            ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
            campusId = AdvantageSession.UserState.CampusId.ToString
            userId = AdvantageSession.UserState.UserId.ToString
            ModuleId = HttpContext.Current.Request.Params("Mod").ToString

            m_Context = HttpContext.Current
            txtResourceId.Text = ResourceId
            Try
                m_Context.Items("Language") = "En-US"
                m_Context.Items("ResourceId") = ResourceId
            Catch ex As Exception
             	Dim exTracker = new AdvApplicationInsightsInitializer()
            	exTracker.TrackExceptionWrapper(ex)

            End Try
            pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, txtResourceId.Text, campusId)

           If Me.Master.IsSwitchedCampus = True Then
                If pObj.HasNone = True Then
                    Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                    Exit Sub
                Else
                    CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
                End If
            End If

            If MyAdvAppSettings.AppSettings("UseCohortStartDateForFilters").ToString.ToLower = "yes" Then
                trCohortStartDate.Visible = True
            Else
                trCohortStartDate.Visible = False
            End If

            If Not Page.IsPostBack Then
                If GetIMaintFormBase() IsNot Nothing Then
                    GetIMaintFormBase().ParentId = Request.Params("pid")
                    GetIMaintFormBase().ObjId = Request.Params("objid")
                    ' update the IE title and the header that goes at the top
                    'Me.Title = GetIMaintFormBase().Title

                    ' add javascript to the delete button
                    btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

                    InitButtonsForLoad()

                    'BindLHS()
                    ResetForm()
                    If Request.Params("objid") IsNot Nothing AndAlso Request.Params("objid") <> "" Then
                        BindRHS(Request.Params("objid"))
                    End If

                    'By Default check the All students currently placed in a In-School status
                    chkAllStudentsInSchoolStatus.Checked = True
                    BuildInSchoolStatus(ARCommon.GetCampusID(), "Active")
                    BuildOutofSchoolStatus(ARCommon.GetCampusID(), "Active")

                    chkAllStudentsInSchoolStatus.Checked = True
                    For Each chkItem As ListItem In chkInSchoolStatus.Items
                        chkItem.Selected = True
                    Next
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub
    Private Sub BuildLeadGroupsDDL(ByVal campusid As String)
        Dim facade As New AdReqsFacade
        With ddlStudentGrpId
            .DataTextField = "Descrip"
            .DataValueField = "LeadGrpId"
            .DataSource = facade.GetAllLeadGroups(campusid)
            .DataBind()
            ''  .Items.Insert(0, New ListItem("---Select---", ""))
        End With
    End Sub
    Private Sub BuildOutofSchoolStatus(ByVal CampusId As String, ByVal Status As String)
        Dim facade As New AdReqsFacade
        With chkStudentOutOfSchoolStatus
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataSource = facade.GetAllOutofSchoolStatus(CampusId, Status)
            .DataBind()
        End With
    End Sub
    Private Sub BuildInSchoolStatus(ByVal CampusId As String, ByVal Status As String)
        Dim facade As New AdReqsFacade
        With chkInSchoolStatus
            .DataTextField = "StatusCodeDescrip"
            .DataValueField = "StatusCodeId"
            .DataSource = facade.GetAllInSchoolStatus(CampusId, Status)
            .DataBind()
        End With
    End Sub
    Protected Sub BindLHS()
        Try
            Dim Id As String = GetIMaintFormBase().ParentId
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Protected Sub BindRHS(ByVal id As String)
        Try
            ' get the selected userid and store it in the viewstate object
            GetIMaintFormBase().ObjId = id

            ' do some error checking
            If GetIMaintFormBase().ObjId Is Nothing Or GetIMaintFormBase().ObjId = "" Then
                Alert("There was a problem displaying the details for this record.")
                Return
            End If


            Dim StudentEnrollmentStatus As String = ""
            Dim OutOfSchoolEnrollmentStatus As String = ""

            'If chkAllStudentsInSchoolStatus.Checked = True And chkAllOutofSchoolStatus.Checked = True Then
            '    StudentEnrollmentStatus = "All"
            'ElseIf chkAllStudentsInSchoolStatus.Checked = True And chkAllOutofSchoolStatus.Checked = False Then
            '    StudentEnrollmentStatus = "InSchool"
            'ElseIf chkAllStudentsInSchoolStatus.Checked = False And chkAllOutofSchoolStatus.Checked = True Then
            '    StudentEnrollmentStatus = "OutofSchool"
            'End If

            'If All Students in In-School Status Checkbox is not checked loop through the check box list and see which status is selected
            'If chkAllStudentsInSchoolStatus.Checked = False Then
            For Each Item As ListItem In chkInSchoolStatus.Items
                If Item.Selected = True Then
                    StudentEnrollmentStatus &= Item.Value & "','"
                End If
            Next
            If StudentEnrollmentStatus <> "" Then
                StudentEnrollmentStatus = Mid(StudentEnrollmentStatus, 1, InStrRev(StudentEnrollmentStatus, "'") - 2)
            End If
            'End If

            'If All Students in Out of School Status Checkbox is not checked loop through the check box list and see which status is selected
            'If chkAllOutofSchoolStatus.Checked = False Then
            For Each Item As ListItem In chkStudentOutOfSchoolStatus.Items
                If Item.Selected = True Then
                    OutOfSchoolEnrollmentStatus &= Item.Value & "','"
                End If
            Next
            If OutOfSchoolEnrollmentStatus <> "" Then
                OutOfSchoolEnrollmentStatus = Mid(OutOfSchoolEnrollmentStatus, 1, InStrRev(OutOfSchoolEnrollmentStatus, "'") - 2)
            End If
            'End If

            Session("StudentEnrollmentStatus") = StudentEnrollmentStatus
            Session("OutOfSchoolEnrollmentStatus") = OutOfSchoolEnrollmentStatus

            ' bind the RHS form
            Dim res As String = GetIMaintFormBase().BindForm(GetIMaintFormBase().ObjId, ARCommon.GetCampusID(), "", StudentEnrollmentStatus, OutOfSchoolEnrollmentStatus)
            If res <> "" Then
                Alert(res)
            End If

            ' Advantage specific stuff
            'CommonWebUtilities.SetStyleToSelectedItem(dlLHS, GetIMaintFormBase().ObjId, ViewState, Nothing)
            InitButtonsForEdit()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    Protected Sub ResetForm()
        ' bind the program dropdown filter
        Dim campusid As String = ARCommon.GetCampusID()
        
        ddlProgram.DataSource = ProgramsFacade.GetPrograms(campusid, True, False)
        ddlProgram.DataTextField = "Descrip"
        ddlProgram.DataValueField = "ID"
        ddlProgram.Items.Insert(0, new RadComboBoxItem("All", string.Empty))
        ddlProgram.DataBind()
        ' add a blank item to the program version instructing the user that they need to select the 
        ' Program first

        ddlUseTimeClock.Items.Clear()
        ddlUseTimeClock.Items.Add(New ListItem("Show all", ""))
        ddlUseTimeClock.Items.Add(New ListItem("Show timeclock programs", "true"))
        ddlUseTimeClock.Items.Add(New ListItem("Show manual programs", "false"))

        ARCommon.BuildStuEnrollmentStatuses(ddlStudentStatus, campusid)
        BuildLeadGroupsDDL(campusid)
        GetIMaintFormBase().Handle_New()

    End Sub


#Region "Advantage Permissions"
    Private Function LoadUserPagePermissions() As FAME.AdvantageV1.Common.UserPagePermissionInfo
        ' load the user permissions
        Dim resourceId As String = ""
        Dim campusId As String = ""
        Dim userId As String = ""

        Try
            If HttpContext.Current.Request.Params("resid") IsNot Nothing Then
                resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        Try
            If HttpContext.Current.Request.Params("cmpid") IsNot Nothing Then
                campusid = Master.CurrentCampusId
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        Try
            If HttpContext.Current.Session("UserId") IsNot Nothing Then
                userId = XmlConvert.ToGuid(HttpContext.Current.Session("UserId")).ToString
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        Try
            Dim fac As New FAME.AdvantageV1.BusinessFacade.UserSecurityFacade
            Dim up As FAME.AdvantageV1.Common.UserPagePermissionInfo = fac.GetUserResourcePermissions(userId, resourceId, campusId)
            'Troy:4/27/2007:There is no need for the code below because the security component 
            'already checks for the "sa" and set the permissions accordingly
            ' TODO: check if this is SA
            'If userId = "a11cb992-2538-49a6-a726-6fd8dad050fc" Then
            '    up.HasAdd = True
            '    up.HasDelete = True
            '    up.HasFull = True
            '    up.HasEdit = True
            'End If
            Return up
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return Nothing
        End Try
    End Function

    Private Sub InitButtonsForLoad()
        btnSave.Enabled = False

        'Troy:4/27/2007: The new and delete buttons are not applicable to this page.

        btnNew.Enabled = False

        btnDelete.Enabled = False
    End Sub
    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'Troy:4/27/2007: The new and delete buttons are not applicable to this page.

        btnNew.Enabled = False

        btnDelete.Enabled = False


    End Sub
#End Region

#Region "Helpers"
    Protected Function GetIMaintFormBase() As IMaintFormBase
        Return CType(formRHS, IMaintFormBase)
    End Function

    Public Sub Alert(ByVal msg As String)
        msg = msg.Replace("'", "")
        msg = msg.Replace(vbCrLf, "\n")
        ClientScript.RegisterStartupScript(Page.GetType(), "sendJS", "<script language='javascript'>window.alert('" & msg.Replace("'", "") & "')</script>")
    End Sub
#End Region

#Region "Event Handlers"
    ''' <summary>
    ''' Called when the user selects a Program from the LHS filter.
    ''' We need to update the set of program versions for the selected Program
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlProgram_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProgram.SelectedIndexChanged
        ' add a blank item if user has not selected a program yet
        If ddlProgram.SelectedValue = "" Then
            ddlProgramVersion.Items.Clear()
            ddlProgramVersion.Items.Insert(0, new RadComboBoxItem("All", string.Empty))
            ddlProgramVersion.SelectedIndex = 0
            Return
        End If

        Dim ProgId As String = ddlProgram.SelectedValue
        ddlProgramVersion.Items.Clear()
        ddlProgramVersion.DataSource = PrgVersionsFacade.GetPrgVersions(ProgId, True, False, ARCommon.GetCampusID())
        ddlProgramVersion.DataTextField = "PrgVerShiftDescrip"
        ddlProgramVersion.DataValueField = "ID"
        
        ddlProgramVersion.DataBind()
        ddlProgramVersion.SelectedIndex = 0
    End Sub

    ''' <summary>
    ''' User has clicked the "Build List" button.  Time to bind the RHS
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click

        If trCohortStartDate.Visible = False Then
            ddlcohortStDate.SelectedValue = ""
        End If

        Dim sb As New StringBuilder()
        sb.AppendFormat("objid={0}&", Me.ddlProgram.SelectedValue)
        sb.AppendFormat("prgverid={0}&", Me.ddlProgramVersion.SelectedValue)
        sb.AppendFormat("stuname={0}&", Me.txtStudentName.Text)
        sb.AppendFormat("stustatus={0}&", Me.ddlStudentStatus.SelectedValue)
        sb.AppendFormat("sdate={0}&", Me.txtStart.SelectedDate.ToString())
        sb.AppendFormat("edate={0}&", Me.txtEnd.SelectedDate.ToString())
        sb.AppendFormat("usetc={0}&", Me.ddlUseTimeClock.SelectedValue)
        sb.AppendFormat("badgenum={0}&", Me.txtBadgeNum.Text)
        sb.AppendFormat("studentgrpid={0}&", Me.ddlStudentGrpId.SelectedValue)
        ''Added by Saraswathi to Filter based on CohortStartDAte if CohortStartDAte is found
        sb.AppendFormat("cohortstartdate={0}", Me.ddlcohortStDate.SelectedValue)

        BindRHS(sb.ToString())

        Dim boolDoesProgramVersionUseTimeClock As Boolean = False
        boolDoesProgramVersionUseTimeClock = AttendanceFacade.DoesProgramVersionUseTimeClock(Me.ddlProgramVersion.SelectedValue)
        If boolDoesProgramVersionUseTimeClock = True Then
            btnSave.Enabled = False
            btnSave.ToolTip = "Save disabled as data will be updated during the time clock file import process"
        End If
    End Sub

    ''' <summary>
    ''' Called when the user clicks the "Save" button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim res As String = Me.GetIMaintFormBase().Handle_Save()
            If res.Contains("Expected graduation date has been recalculated") Or res.Contains("Program Version Hours are Zero") Or res = "" Then
                BindLHS()
                If MyAdvAppSettings.AppSettings("TrackSapAttendance", HttpContext.Current.Request.Params("cmpid")).ToLower = "byday" Then
                    Alert("Record was saved successfully" & vbCrLf & res)
                Else
                    Alert("Record was saved successfully")
                End If
                'CommonWebUtilities.SetStyleToSelectedItem(dlLHS, Me.GetIMaintFormBase().ObjId, ViewState, Nothing)
                InitButtonsForEdit()
            Else
                Alert(res)
                BindLHS()
            End If
            ''If res <> "" Then
            ''    Alert(res)
            ''    BindLHS()
            ''    'CommonWebUtilities.SetStyleToSelectedItem(dlLHS, Me.GetIMaintFormBase().ObjId, ViewState, Nothing)
            ''Else
            ''    BindLHS()
            ''    Alert("Record was saved successfully")
            ''    'CommonWebUtilities.SetStyleToSelectedItem(dlLHS, Me.GetIMaintFormBase().ObjId, ViewState, Nothing)
            ''    InitButtonsForEdit()
            ''End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Alert(ex.Message)
        End Try
    End Sub
    ''' <summary>
    ''' Called when the user clicks the "New" button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ResetForm()
        'CommonWebUtilities.SetStyleToSelectedItem(dlLHS, Guid.Empty.ToString, ViewState, Nothing)
        InitButtonsForLoad()
    End Sub

    ''' <summary>
    ''' Called when the user clicks the "Delete" button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim curid As String = GetIMaintFormBase.ObjId
            If curid IsNot Nothing AndAlso curid <> "" Then
                Dim res As String = GetIMaintFormBase.Handle_Delete()
                If res <> "" Then
                    Alert(res)
                Else
                    ResetForm()
                    BindLHS()
                    'CommonWebUtilities.SetStyleToSelectedItem(dlLHS, Guid.Empty.ToString, ViewState, Nothing)
                    InitButtonsForLoad()
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Alert("Error.  Record could not be deleted.")
        End Try
    End Sub
#End Region

    Protected Sub ddlProgramVersion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProgramVersion.SelectedIndexChanged
        If trCohortStartDate.Visible = True Then
            PopulateCohortStartDateDDL()
        End If
    End Sub

    Private Sub PopulateCohortStartDateDDL()
        Dim prgVersionId As String
        Dim facade As New StdGrdBkFacade
        prgVersionId = ddlProgramVersion.SelectedItem.Value
        With ddlcohortStDate
            .DataSource = facade.GetCohortStartDatebyPrgVersion(prgVersionId)
            .DataTextField = "CohortStartDate"
            .DataValueField = "CohortStartDate"
            .DataBind()
            .Items.Insert(0, New ListItem("---Select---", ""))
            .SelectedIndex = 0
        End With
    End Sub

    'Protected _UserId As String
    'Public UserId As String
    'Protected _ResourceId As Int32
    'Public ResourceId As Int32
    'Public Property ResourceId() As Int32
    '    Get
    '        ResourceId = _ResourceId
    '    End Get
    '    Set(ByVal Value As Int32)
    '        _ResourceId = Value
    '    End Set
    'End Property
    'Public Property UserId() As String
    '    Get
    '        UserId = _UserId
    '    End Get-
    '    Set(ByVal Value As String)
    '        _UserId = Value

    '    End Set
    'End Property
    ''    protected string _companyID;
    ''public string CompanyID
    ''{
    ''get
    ''{
    ''return _companyID;
    ''}
    ''set
    ''{
    ''_companyID = value;
    ''myControl.CompanyID = value; //keeps the control and page in sync
    ''}

    Protected Sub chkAllStudentsInSchoolStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAllStudentsInSchoolStatus.CheckedChanged
        If chkAllStudentsInSchoolStatus.Checked = False Then
            For Each chkItem As ListItem In chkInSchoolStatus.Items
                chkItem.Selected = False
            Next
        ElseIf chkAllStudentsInSchoolStatus.Checked = True Then
            For Each chkItem As ListItem In chkInSchoolStatus.Items
                chkItem.Selected = True
            Next
        End If
    End Sub

    Protected Sub chkAllOutofSchoolStatus_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAllOutofSchoolStatus.CheckedChanged
        If chkAllOutofSchoolStatus.Checked = True Then
            For Each chkItem As ListItem In chkStudentOutOfSchoolStatus.Items
                chkItem.Selected = True
            Next
        Else
            For Each chkItem As ListItem In chkStudentOutOfSchoolStatus.Items
                chkItem.Selected = False
            Next
        End If
    End Sub
    ''Added by Saraswathi lakshmanan on August 24 2009
    ''To find the list controls and add a tool tip to those items in the control
    ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'BIndToolTip()
    End Sub
End Class

