' ===============================================================================
' IMaint_PostLabWork
' RHS for posting lab work
' Uses IMaintFormBase interface
' ===============================================================================
' Copyright (C) 2007 FAME Inc.
' All rights reserved.
' Developed by ThinkTron Corporation
' ===============================================================================
' IMaintFormBase properties
'       ParentId = ReqId
'       ObjId = GrdBookWgtId

Imports System.Data

Imports FAME.AdvantageV1.BusinessFacade.FA
Imports FAME.AdvantageV1.Common.FA

Imports FAME.AdvantageV1.Common.AR

Partial Class FA_IMaint_PostLabWork
    Inherits System.Web.UI.UserControl
    Implements IMaintFormBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Me.lblMsg.Text = "Select filter on left hand side to begin."

            ' add javascript to the delete button
            Me.btnPostByException.Attributes.Add("onclick", "if(confirm('This will set all unposted lab entries to the value specified in the text box.\nAre you sure you want to proceed?')){}else{return false}")
        End If
    End Sub

#Region "Helpers"
    Public Sub Alert(ByVal msg As String)
        msg = msg.Replace("'", "")
        msg = msg.Replace(vbCrLf, "\n")
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "sendJS", "<script language='javascript'>window.alert('" & msg.Replace("'", "") & "')</script>")
    End Sub

    ''' <summary>
    ''' Called by handle_save to validate the form
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function ValidateForm() As String
        Try
            ' do a basic sanity check that the ObjId has been set already
            If ObjId Is Nothing Or ObjId = "" Then
                Return "Filter has not been selected yet.  Nothing to save."
            End If
     


            ' validate that the post dates is within the class date range
            Dim postDate As Date = calPostDate.SelectedDate
            Dim startDate As Date = CType(Me.hfClassStartDate.Value, Date)
            Dim endDate As Date = CType(Me.hfClassEndDate.Value, Date)
            If postDate < startDate Then
                Return "Posting not allowed because the selected date is before the class start date."
            End If
            If postDate > endDate Then
                Return "Posting not allowed because the selected date is after the class start date."
            End If
            If postDate > Date.Now Then
                ' Don't allow posting on a future date
                Return "The selected posting date is " + postDate.ToShortDateString() + ".  Posting is not allowed on a future date."
            End If

            Return ""
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Protected Sub UnpackParams(ByVal ID As String)
        ' first unpackage ID into the list of supported params                        
        Dim params() As String = ID.Split("&")
        For Each p As String In params
            Dim cp() As String = p.Split("=")
            If cp.Length = 2 Then
                Select Case cp(0).ToLower()
                    Case "termid"
                        Filter_TermId = cp(1)
                    Case "clssectionid"
                        Filter_ClsSectionId = cp(1)
                    Case "labid"
                        Filter_LabId = cp(1)
                End Select
            End If
        Next
    End Sub

    ''' <summary>
    ''' Called for each item in the repeater
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub rptLabWork_ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs) Handles rptLabWork.ItemDataBound
        Dim labType As SysGradeBookComponentTypes = CType(hfLabType.Value, SysGradeBookComponentTypes)
        If e.Item.ItemType = ListItemType.Header Then
            If labType = SysGradeBookComponentTypes.LabHours Then
                Me.lblCountHeader.Text = "Lab Hours"
            Else
                Me.lblCountHeader.Text = "Lab Count"
            End If
        ElseIf e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            ' update hidden values that will be needed when doing a save
            Dim stuEnrollId As String = e.Item.DataItem("StuEnrollId").ToString()
            CType(e.Item.FindControl("hfStuEnrollId"), HiddenField).Value = stuEnrollId

            ' select the record from our cached dataset for the current student
            Dim sql As String = String.Format("StuEnrollId='{0}'", stuEnrollId)
            Dim drs() As DataRow = PostingDT.Select(sql)

            ' fill up values that we will need to display info on the form
            Dim GrdBkResultId As String = ""
            Dim tot As Decimal = 0.0
            Dim count As String = ""
            Dim comment As String = ""
            If drs.Length > 0 Then
                If Not DBNull.Value.Equals(drs(0)("GrdBkResultId")) Then GrdBkResultId = drs(0)("GrdBkResultId").ToString()
                If Not DBNull.Value.Equals(drs(0)("Score")) Then count = drs(0)("Score").ToString()
                comment = drs(0)("Comments").ToString()
            End If

            ' get the total lab count for this student from the Counts DataTable
            drs = LabCountsDT.Select(sql)
            If drs.Length > 0 Then
                If Not DBNull.Value.Equals(drs(0)("Total")) Then tot = drs(0)("Total")
            End If

            ' update the form
            CType(e.Item.FindControl("hfGrdBkResultId"), HiddenField).Value = GrdBkResultId
            CType(e.Item.FindControl("txtTotal"), eWorld.UI.NumericBox).Text = tot.ToString()
            CType(e.Item.FindControl("txtCount"), eWorld.UI.NumericBox).Text = count
            CType(e.Item.FindControl("txtComment"), TextBox).Text = comment.ToString()

            ' display the number of labs remaining
            Dim numRem As Decimal = Me.lblNumLabs.Text - tot
            If numRem > 0 Then
                CType(e.Item.FindControl("txtRemaining"), eWorld.UI.NumericBox).Text = numRem.ToString("N2")
            Else
                CType(e.Item.FindControl("txtRemaining"), eWorld.UI.NumericBox).Text = "0"
            End If

            ' set the number of decimals to 0 if LabType is LabWork and 2 if LabHours            
            If labType = SysGradeBookComponentTypes.LabHours Then
                CType(e.Item.FindControl("txtCount"), eWorld.UI.NumericBox).DecimalPlaces = 2
                CType(e.Item.FindControl("txtRemaining"), eWorld.UI.NumericBox).DecimalPlaces = 2
                CType(e.Item.FindControl("txtTotal"), eWorld.UI.NumericBox).DecimalPlaces = 2
            Else
                CType(e.Item.FindControl("txtCount"), eWorld.UI.NumericBox).DecimalPlaces = 0
                CType(e.Item.FindControl("txtRemaining"), eWorld.UI.NumericBox).DecimalPlaces = 0
                CType(e.Item.FindControl("txtTotal"), eWorld.UI.NumericBox).DecimalPlaces = 0
            End If

            ' Format the name of the student
            Dim name As String = String.Format("{0}, {1} {2}", e.Item.DataItem("LastName"), e.Item.DataItem("FirstName"), e.Item.DataItem("MiddleName"))
            CType(e.Item.FindControl("lblStudentName"), Label).Text = name

            ' build of a javascript string to display an info popup
            Dim ib As ImageButton = CType(e.Item.FindControl("ibStudentInfo"), ImageButton)
            Dim sb As New StringBuilder()
            sb.Append("ddrivetip('")
            sb.Append(GetStudentToolTip(e))
            sb.Append("', 300);")
            ib.Attributes.Add("onmouseover", sb.ToString)
            ib.Attributes.Add("onmouseout", "hideddrivetip()")
        End If
    End Sub

    ''' <summary>
    ''' Format a tooltip which is shown in the UI.  Used by the ItemDataBound for the repeater.
    ''' </summary>
    ''' <param name="e"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function GetStudentToolTip(ByVal e As RepeaterItemEventArgs) As String
        Dim sb As New StringBuilder()
        sb.AppendFormat("<li>Prg Ver:  {0}{1}{2}", vbTab, e.Item.DataItem("PrgVerDescrip").ToString(), "</li>")
        sb.AppendFormat("<li>SSN:  {0}{1}{2}", vbTab, e.Item.DataItem("SSN").ToString(), "</li>")
        If Not DBNull.Value.Equals(e.Item.DataItem("StartDate")) Then
            sb.AppendFormat("<li>Start date: {0}{1}{2}", vbTab, CType(e.Item.DataItem("StartDate"), Date).ToShortDateString(), "</li>")
        Else
            sb.AppendFormat("<li>Start date: {0}{1}{2}", vbTab, "N/A", "</li>")
        End If

        sb.AppendFormat("<li>Status:   {0}{1}", vbTab, e.Item.DataItem("StatusCodeDescrip").ToString())
        Return sb.ToString().Replace("'", " ")
    End Function

    ''' <summary>
    ''' Builds the list of previosuly posted dates in the dropdown
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindPostDates()
        Me.ddlPostDate.DataSource = GrdPostingsFacade.GetPostDates(Filter_ClsSectionId, Filter_LabId)
        ddlPostDate.DataTextField = "PostDate"
        ddlPostDate.DataValueField = "PostDate"
        ddlPostDate.DataBind()
        If ddlPostDate.Items.Count = 0 Then
            ddlPostDate.Items.Add(New ListItem("None", ""))
        Else
            ddlPostDate.Items.Insert(0, New ListItem("---Select---", ""))
        End If
    End Sub

    ''' <summary>
    ''' Binds the lab work for the current clssection, labid and post date.
    ''' These filters are stored in Filter_ClsSectionId, Filter_LabId, and Filter_PostDate
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindRepeater()
        ' set the Posting and total lab counts DataTables which gets used in ItemDataBound
        PostingDT = GrdPostingsFacade.GetLabResults(Filter_ClsSectionId, Filter_LabId, Filter_PostDate)
        LabCountsDT = GrdPostingsFacade.GetTotalLabCounts(Filter_ClsSectionId, Filter_LabId)

        ' Bind the repeater that shows the students
        rptLabWork.DataSource = GrdPostingsFacade.GetStudentsForClassSection(Filter_ClsSectionId)
        rptLabWork.DataBind()
        Me.calPostDate.SelectedDate = Filter_PostDate
        Me.calPostDate.Text = Filter_PostDate.ToShortDateString()
    End Sub
    ''' <summary>
    ''' Called when the user selects a new post date
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub OnPostDateChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles calPostDate.DateChanged
        Try
            Dim postDate As Date = calPostDate.SelectedDate
            Dim startDate As Date = CType(Me.hfClassStartDate.Value, Date)
            Dim endDate As Date = CType(Me.hfClassEndDate.Value, Date)
            If postDate < startDate Then
                Alert("Posting not allowed because the selected date is before the class start date.")
                Return
            End If
            If postDate > endDate Then
                Alert("Posting not allowed because the selected date is after the class start date.")
                Return
            End If

            ' date is valid, allow editing
            Filter_PostDate = postDate
            BindRepeater()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    ''' <summary>
    ''' Called when the user selects a post date that has already been entered
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlPostDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPostDate.SelectedIndexChanged
        If ddlPostDate.SelectedValue = "" Then
            Filter_PostDate = CType(Date.Now.ToShortDateString(), Date)
        Else
            Filter_PostDate = CType(ddlPostDate.SelectedValue, Date)
        End If
        Me.calPostDate.SelectedDate = Filter_PostDate
        Me.calPostDate.Text = Filter_PostDate.ToShortDateString()
        BindRepeater()
    End Sub

    ''' <summary>
    ''' Displays the class start and end date.
    ''' Updates hidden fields with the start and end date
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub BindClassDates()
        '   Dim ddlTerm As DropDownList = CType(Page.FindControl("Form1$panFilter$ddlTerm"), DropDownList)
        Dim ddlTerm As DropDownList = CType(FindControlRecursive("ddlTerm", Page.Master), DropDownList)

        Dim dt As DataTable = GrdPostingsFacade.GetLabDetails(Filter_LabId, ddlTerm.SelectedValue)
        If dt.Rows.Count > 0 Then
            ' Bind the class dates
            If Not DBNull.Value.Equals(dt.Rows(0)("StartDate")) Then
                Dim tmpDate As Date = dt.Rows(0)("StartDate")
                hfClassStartDate.Value = tmpDate.ToShortDateString()
            End If
            If Not DBNull.Value.Equals(dt.Rows(0)("EndDate")) Then
                Dim tmpDate As Date = dt.Rows(0)("EndDate")
                hfClassEndDate.Value = tmpDate.ToShortDateString()
            End If

            If hfClassStartDate.Value = "" Then
                lblClassDates.Text = "N/A to "
            Else
                lblClassDates.Text = hfClassStartDate.Value + " to "
            End If

            If hfClassEndDate.Value = "" Then
                lblClassDates.Text += "N/A"
            Else
                lblClassDates.Text += hfClassEndDate.Value
            End If

            ' bind the number of labs
            Dim labtype As SysGradeBookComponentTypes = CType(dt.Rows(0)("SysComponentTypeId"), SysGradeBookComponentTypes)
            ' save the lab type in a hidden value so it can be used in the Repeater ItemDataBound
            hfLabType.Value = CType(labtype, Integer).ToString()
            ' update the ui to make a small adjustment for the lab type
            If labtype = SysGradeBookComponentTypes.LabHours Then
                lblLabHeader.Text = "Required lab hours: "
                Me.txtExc.DecimalPlaces = 2
            Else
                lblLabHeader.Text = "Required number of labs: "
                Me.txtExc.DecimalPlaces = 0
            End If

            lblNumLabs.Text = dt.Rows(0)("Number").ToString()
        End If
    End Sub

    ''' <summary>
    ''' Sets everyones hours to the value specified in the exceptino text box
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnPostByException_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPostByException.Click
        If (Me.txtExc.Text = "") Then
            Me.txtExc.Focus()
            DisplayErrorMessage("Enter a value for unposted lab entries")
        Else
            For Each rpi As RepeaterItem In Me.rptLabWork.Items
                Dim txtTotal As eWorld.UI.NumericBox = CType(rpi.FindControl("txtTotal"), eWorld.UI.NumericBox)
                Dim txtRem As eWorld.UI.NumericBox = CType(rpi.FindControl("txtRemaining"), eWorld.UI.NumericBox)
                Dim txtCount As eWorld.UI.NumericBox = CType(rpi.FindControl("txtCount"), eWorld.UI.NumericBox)
                If txtCount.Text = "" Then
                    'If (Me.txtExc.Text <> "") Then
                    txtCount.Text = Me.txtExc.Text
                    ' update the total.  Since we know that the previous count was 0, 
                    ' we can make simply increment the total by the new count
                    txtTotal.Text = CType(txtTotal.Text, Decimal) + CType(txtCount.Text, Decimal).ToString()

                    If (CType(txtTotal.Text, Decimal).ToString() > CType(Me.lblNumLabs.Text, Decimal)) Then
                        txtRem.Text = 0
                    Else
                        txtRem.Text = CType(Me.lblNumLabs.Text, Decimal) - CType(txtTotal.Text, Decimal).ToString()
                    End If


                    'End If
                End If
            Next
        End If
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)

        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)

    End Sub
#End Region

#Region "ViewState & Cached vars"
    Protected Property PostingDT() As DataTable
        Get
            Return Cache("PostLabWorkDS")
        End Get
        Set(ByVal value As DataTable)
            Cache("PostLabWorkDS") = value
        End Set
    End Property
    Protected Property LabCountsDT() As DataTable
        Get
            Return Cache("LabCountsDS")
        End Get
        Set(ByVal value As DataTable)
            Cache("LabCountsDS") = value
        End Set
    End Property
    Protected Property Filter_TermId() As String
        Get
            Return ViewState("PostLabWork_TermId")
        End Get
        Set(ByVal value As String)
            ViewState("PostLabWork_TermId") = value
        End Set
    End Property
    Protected Property Filter_ClsSectionId() As String
        Get
            Return ViewState("PostLabWork_ClsSectId")
        End Get
        Set(ByVal value As String)
            ViewState("PostLabWork_ClsSectId") = value
        End Set
    End Property
    Protected Property Filter_LabId() As String
        Get
            Return ViewState("PostLabWork_LabId")
        End Get
        Set(ByVal value As String)
            ViewState("PostLabWork_LabId") = value
        End Set
    End Property
    Protected Property Filter_PostDate() As Date
        Get
            Return ViewState("PostLabWork_PostDate")
        End Get
        Set(ByVal value As Date)
            ViewState("PostLabWork_PostDate") = value
        End Set
    End Property
#End Region

#Region "IMaintForm"
    Public Function BindForm(ByVal ID As String, Optional ByVal campusId As String = "", Optional ByVal Permission As String = "", Optional ByVal InSchoolEnrollmentStatus As String = "", Optional ByVal OutSchoolEnrollmentStatus As String = "") As String Implements IMaintFormBase.BindForm
        Try
            ' unpack the params into TermId, ClsSectionId, and LabId
            UnpackParams(ID)

            ' set the PostDate to today
            Filter_PostDate = CType(Date.Now.ToShortDateString(), Date)

            BindClassDates()
            ' Bind the post dates that have previously been entered
            BindPostDates()
            BindRepeater()

            ' display a message if there are no students matching the filter
            ' and hide the RHS form
            If rptLabWork.Items.Count = 0 Then
                divContent.Visible = False
                lblMsg.Visible = True
                lblMsg.Text = "There are no students that match the current filter."
            Else
                divContent.Visible = True
                lblMsg.Visible = False
            End If


            ' set IMaintFormBase params
            'ParentId = info.CampGrpId
            ObjId = ID
            ModDate = Date.Now

            Return "" ' Success
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Public Function GetLHSDataSet(ByVal ID As String, ByVal bActive As Boolean, ByVal bInactive As Boolean) As System.Data.DataSet Implements IMaintFormBase.GetLHSDataSet
        ' It's ok for the ID to be nothing
        If ID Is Nothing Or ID = "" Then Return Nothing
        Return FacultyFacade.GetClassSectionsForTerm(FACommon.GetCampusID(), ID)
    End Function

    Public Function Handle_Delete() As String Implements IMaintFormBase.Handle_Delete
        Try
            If ObjId Is Nothing Or ObjId = "" Then Return "No record to delete"
            Dim res As String = "" ' 
            If res <> "" Then
                Return res
            End If

            ObjId = ""
            ModDate = Nothing
            Return "" ' Success
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Public Function Handle_New() As String Implements IMaintFormBase.Handle_New
        Try
            ObjId = Nothing
            ModDate = Nothing

            Return "" ' Success
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Public Function Handle_Save() As String Implements IMaintFormBase.Handle_Save
        Try
            Dim res As String = ValidateForm()
            If res <> "" Then Return res

            Dim user As String = FACommon.GetUserNameFromUserId(FACommon.GetCurrentUserId())

            Dim res2 As New StringBuilder()
            For Each rpi As RepeaterItem In Me.rptLabWork.Items
                Dim GrdBkResultId As String = CType(rpi.FindControl("hfGrdBkResultId"), HiddenField).Value
                Dim StuEnrollId As String = CType(rpi.FindControl("hfStuEnrollId"), HiddenField).Value
                Dim score As String = CType(rpi.FindControl("txtCount"), eWorld.UI.NumericBox).Text
                Dim comment As String = CType(rpi.FindControl("txtComment"), TextBox).Text

                If (score = "") And (comment <> "") Then
                    Return "Enter Score for the Comment."
                End If

                If (score = "") AndAlso GrdBkResultId <> "" Then
                    res = GrdPostingsFacade.DeleteGrdPosting(GrdBkResultId)
                    If res = "" Then
                        GrdPostingsFacade.SetIsCourseCompleted(StuEnrollId.ToString, Me.Filter_ClsSectionId)
                    Else
                        res2.AppendFormat(res + vbCrLf)
                        Dim lb As LinkButton = CType(rpi.FindControl("lbError"), LinkButton)
                        lb.Visible = True
                        lb.ToolTip = res
                    End If
                ElseIf (score <> "") Then
                    Dim info As New GrdPostingsInfo
                    If GrdBkResultId <> "" Then
                        info.GrdBkResultId = GrdBkResultId
                        info.IsInDB = True 'update
                    Else
                        info.IsInDB = False 'insert
                    End If
                    info.StuEnrollId = StuEnrollId
                    info.GrdBkWgtDetailId = Me.Filter_LabId
                    info.ClsSectId = Me.Filter_ClsSectionId
                    info.PostDate = Me.Filter_PostDate
                    info.Score = CType(score, Decimal)
                    info.Comments = CType(rpi.FindControl("txtComment"), TextBox).Text
                    info.ResNum = 0

                    res = GrdPostingsFacade.UpdateGrdPosting(info, user)
                    If res = "" Then
                        GrdPostingsFacade.SetIsCourseCompleted(StuEnrollId.ToString, info.ClsSectId.ToString)
                        CType(rpi.FindControl("hfGrdBkResultId"), HiddenField).Value = info.GrdBkResultId
                    Else
                        res2.AppendFormat(res + vbCrLf)
                        Dim lb As LinkButton = CType(rpi.FindControl("lbError"), LinkButton)
                        lb.Visible = True
                        lb.ToolTip = res
                    End If
                End If
            Next

            If res2.Length = 0 Then
                BindPostDates()
                BindRepeater()
                'update ModDate so that clicking Save followed by Delete works properly
                Filter_PostDate = CType(Date.Now.ToShortDateString(), Date)
                Me.calPostDate.SelectedDate = Filter_PostDate
                Me.calPostDate.Text = Filter_PostDate.ToShortDateString()
            Else
                Return "Errors occurred during the save operation."
            End If

            Return ""
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return ex.Message
        End Try
    End Function

    Public ReadOnly Property LHSImageUrl() As String Implements IMaintFormBase.LHSImageUrl
        Get
            Return ""
        End Get
    End Property

    Public Property ModDate() As Date Implements IMaintFormBase.ModDate
        Get
            Return ViewState("moddate_PostLabWork")
        End Get
        Set(ByVal value As DateTime)
            ViewState("moddate_PostLabWork") = value
        End Set
    End Property

    Public Property ObjId() As String Implements IMaintFormBase.ObjId
        Get
            Return ViewState("objId_PostLabWork")
        End Get
        Set(ByVal value As String)
            ViewState("objId_PostLabWork") = value
        End Set
    End Property

    Public Property ParentId() As String Implements IMaintFormBase.ParentId
        Get
            Return ViewState("parentId_PostLabWork")
        End Get
        Set(ByVal value As String)
            ViewState("parentId_PostLabWork") = value
        End Set
    End Property

    Public Property Title() As String Implements IMaintFormBase.Title
        Get
            Return "Post Clinic Work"
        End Get
        Set(ByVal value As String)

        End Set
    End Property
#End Region

#Region "FindControl"
    ''' <summary>
    ''' Find Control starting at Page level
    ''' </summary>
    ''' <param name="id"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overridable Function FindControlRecursive(id As String) As Control
        Return FindControlRecursive(id, Me)
    End Function
    ''' <summary>
    ''' Find Control within a parent control
    ''' </summary>
    ''' <param name="id"></param>
    ''' <param name="parent"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Overridable Function FindControlRecursive(id As String, parent As Control) As Control
        ' If parent is the control we're looking for, return it
        If String.Compare(parent.ID, id, True) = 0 Then
            Return parent
        End If
        ' Search through children
        For Each ctrl As Control In parent.Controls
            Dim match As Control = FindControlRecursive(id, ctrl)
            If Not match Is Nothing Then
                Return match
            End If
        Next
        'If we reach here then no control with id was found
        Return Nothing
    End Function
#End Region

End Class
