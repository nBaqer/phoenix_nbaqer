﻿<%@ Page Title="Student Payment Plan" Language="VB" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="StudentPaymentPlan.aspx.vb" Inherits="StudentPaymentPlan" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="dlstPaymentPlans">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="OldContentSplitter" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSave">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="dlstPaymentPlans" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnNew">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="OldContentSplitter" />
                    <telerik:AjaxUpdatedControl ControlID="dlstPaymentPlans" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnDelete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="OldContentSplitter" />
                    <telerik:AjaxUpdatedControl ControlID="dlstPaymentPlans" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>



    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldMenuPane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
            <%-- Add class ListFrameTop2 to the table below --%>

            <table cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td class="listframetop">
                        <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                            <tr>
                                <td class="twocolumnlabelcell">
                                    <asp:Label ID="lblEnrollments" runat="server" CssClass="label" Width="100%">Enrollment</asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="twocolumncontentcell">
                                    <asp:DropDownList ID="ddlStuEnrollId" Width="325px" runat="server" CssClass="dropdownlist">
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr>

                                <td class="twocolumncontentcell">
                                    <br />
                                    <asp:Button ID="btnBuildList" runat="server" CausesValidation="False"
                                        Text="Build List"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="listframebottom">
                        <div class="scrollleftfltr2rows">
                            <asp:DataList ID="dlstPaymentPlans" runat="server" DataKeyField="PaymentPlanId"
                                          Width="97%">
                                <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                <ItemStyle CssClass="itemstyle"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="Linkbutton1" CssClass="itemstyle" CausesValidation="False" runat="server"
                                        CommandArgument='<%# Container.DataItem("PaymentPlanId")%>' Text='<%# Container.DataItem("PayPlanDescrip")%>'>
                                    </asp:LinkButton>
                                    <asp:Label ID="lblAwdStartDate" runat="server" CssClass="itemstyle" Text='<%# "(" + Container.DataItem("PayPlanStartDate")+ "-" + Container.DataItem("PayPlanEndDate")+ ")" %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:DataList>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>

        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <!-- begin top menu (save,new,reset,delete,history)-->
                <tr>
                    <td class="menuframe" align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button>
                        <asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button>
                        <asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button>
                    </td>
                </tr>
            </table>
            <table class="maincontenttable" cellspacing="0" cellpadding="0" style="width: 98%" border="0">
                <tr>
                    <td class="detailsframe">
                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- begin content table-->


                            <table class="contenttable" cellspacing="0" cellpadding="0" width="60%" align="center">
                                <asp:TextBox ID="txtPaymentPlanId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox
                                    ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox><asp:TextBox ID="txtStudentId"
                                        runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtModUser" runat="server"
                                            Visible="False">ModUser</asp:TextBox><asp:TextBox ID="txtModDate" runat="server"
                                                Visible="False">ModDate</asp:TextBox>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblPayPlanDescrip" runat="server" CssClass="label">Description</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <asp:TextBox ID="txtPayPlanDescrip" TabIndex="1" CssClass="textbox" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblPayPlanStartDate" runat="server" CssClass="label">Payment Plan Start</asp:Label>
                                    </td>
                                    <td class="contentcell4" style="text-align: left">

                                        <telerik:RadDatePicker ID="txtPayPlanStartDate" TabIndex="2" MinDate="1/1/2001" Width="200px" runat="server">
                                        </telerik:RadDatePicker>

                                        <asp:RangeValidator ID="drvPayPlanStartDate" runat="server" MinimumValue="2001/01/01"
                                            MaximumValue="2049/12/31" Type="Date" Display="None" ErrorMessage="Date is invalid or outside allowed range"
                                            ControlToValidate="txtPayPlanStartDate">Date is invalid or outside allowed range</asp:RangeValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblPayPlanEndDate" runat="server" CssClass="label">Payment Plan End</asp:Label>
                                    </td>
                                    <td class="contentcell4" style="text-align: left">

                                        <telerik:RadDatePicker ID="txtPayPlanEndDate" TabIndex="3" MinDate="1/1/2001" Width="200px" runat="server">
                                        </telerik:RadDatePicker>

                                        <asp:RangeValidator ID="drvPayPlanEndDate" runat="server" MinimumValue="2001/01/01"
                                            MaximumValue="2049/12/31" Type="Date" Display="None" ErrorMessage="Date is invalid or outside allowed range"
                                            ControlToValidate="txtPayPlanEndDate">Date is invalid or outside allowed range
                                        </asp:RangeValidator>
                                        <asp:CompareValidator
                                            ID="cvPayPlanEndDate" runat="server" Display="None" ErrorMessage="Payment Plan End Date must be greater than Payment Plan Start Date"
                                            ControlToValidate="txtPayPlanEndDate" Operator="GreaterThan" Type="Date" ControlToCompare="txtPayPlanStartDate">Payment Plan End Date must be greater than Payment Plan Start Date
                                        </asp:CompareValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblAcademicYearId" runat="server" CssClass="label">Academic Year</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <asp:DropDownList ID="ddlAcademicYearId" TabIndex="5" runat="server" Width="200px"
                                            CssClass="dropdownlist">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap>
                                        <asp:Label ID="lblTotalAmountDue" runat="server" CssClass="label">Total Amount Due</asp:Label>
                                    </td>
                                    <td class="contentcell4">
                                        <table class="contenttable" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td style="width: 60%" align="left">
                                                    <asp:TextBox ID="txtTotalAmountDue" TabIndex="6" CssClass="textbox" runat="server"></asp:TextBox>
                                                    &nbsp;
                                    <span class="label">
                                        <%=MyAdvAppSettings.AppSettings("Currency")%>
                                    </span>
                                                    <asp:RangeValidator ID="TotalAmountDueRangeValidator" runat="server" MinimumValue="0.01"
                                                        MaximumValue="1000000" Type="Currency" Display="None" ErrorMessage="Invalid Total Amount Due"
                                                        ControlToValidate="txtTotalAmountDue">Invalid Total Amount Due</asp:RangeValidator>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap="nowrap">
                                        <asp:Label ID="lblDisbursements" runat="server" CssClass="label">Number of Payments</asp:Label>
                                    </td>
                                    <td class="contentcell4" align="left">
                                        <asp:TextBox ID="txtDisbursements" runat="server" CssClass="textbox" TabIndex="7"></asp:TextBox>
                                        <asp:RangeValidator ID="DisbursementRangeValidator" runat="server" ControlToValidate="txtDisbursements"
                                            ErrorMessage="Invalid Number of Payments" Display="None" Type="Integer" MaximumValue="100"
                                            MinimumValue="1">Invalid Number of Payments</asp:RangeValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="contentcell" nowrap="nowrap" style="padding-top: 20px; padding-bottom: 5px;">
                                        <asp:Label CssClass="Label" runat="server" ID="lblOBalance" Width="180px">Payment Plan Balance: </asp:Label>
                                        <asp:Label ID="lblOverallBalance" runat="server" Text="" CssClass="Label" Width="100px"></asp:Label>
                                    </td>
                                    <td class="contentcell4" align="center" style="padding-top: 20px; padding-bottom: 5px;">
                                        <asp:LinkButton ID="lnkDisbursements" TabIndex="8" CssClass="label" runat="server">Calculate Payments</asp:LinkButton>
                                    </td>
                                </tr>

                            </table>
                            <table width="100%" border="0" align="center">
                                <tr>
                                    <td align="left"></td>
                                </tr>
                            </table>
                            <table style="width: 100%; border: none;" align="center" cellspacing="0">
                                <tr class="blue-darken-2">
                                    <td nowrap="nowrap" style="border: none; padding: 5px;">
                                        <asp:Label ID="Label1" runat="server"> Student Payment Plan</asp:Label>
                                    </td>
                                    <td width="100px" style="text-align: right; border: none; padding: 5px;">
                                        <asp:Button ID="btnExportToPdf" runat="server" Text="Export to PDF" OnClick="BtnExportToPdfClick"/>
                                    </td>
                                </tr>
                            </table>

                            <table class="contenttable" cellspacing="0" cellpadding="0" width="100%" border="0"
                                align="center">
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="dgrdDisbursements" runat="server" Width="100%" BorderWidth="1px"
                                            ShowFooter="True" BorderStyle="Solid" AutoGenerateColumns="False" AllowSorting="True"
                                            BorderColor="#E0E0E0" TabIndex="9">
                                            <AlternatingItemStyle CssClass="DataGridAlternatingStyle"></AlternatingItemStyle>
                                            <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                            <HeaderStyle CssClass="k-grid-header k-datagrid-cell"></HeaderStyle>
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Scheduled Date">
                                                    <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExpectedDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ExpectedDate", "{0:d}") %>'>
                                                        </asp:Label>
                                                        <asp:Label ID="lblPayPlanScheduleId" runat="server" Text='<%# Container.DataItem("PayPlanScheduleId") %>'
                                                            Visible="False">
                                                        </asp:Label>
                                                        <asp:Label ID="lblWasReceived" runat="server" Text='<%# Container.DataItem("WasReceived") %>'
                                                            Visible="False">
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle CssClass="DataGridItemStyle"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtFooterExpectedDate" CssClass="TextBox" runat="server" Width="100px"></asp:TextBox>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtEditExpectedDate" runat="server" CssClass="textbox" Width="100px"
                                                            Text='<%# DataBinder.Eval(Container, "DataItem.ExpectedDate", "{0:d}") %>'>
                                                        </asp:TextBox>
                                                        <asp:Label ID="lblEditPayPlanScheduleId" runat="server" Text='<%# Container.DataItem("PayPlanScheduleId") %>'
                                                            Visible="False">
                                                        </asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Reference">
                                                    <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReference" runat="server" Text='<%# Container.DataItem("Reference") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle CssClass="DataGridItemStyle"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtFooterReference" CssClass="TextBox" Width="180px" runat="server"></asp:TextBox>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtEditReference" runat="server" CssClass="textbox" Width="180px" Text='<%# Container.DataItem("Reference") %>'>
                                                        </asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Projected Amount">
                                                    <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Amount", "{0:c}") %>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle CssClass="DataGridItemStyle"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:TextBox ID="txtFooterAmount" CssClass="TextBox" runat="server" Width="80px"></asp:TextBox>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtEditAmount" runat="server" CssClass="textbox" Width="80px" Text='<%# DataBinder.Eval(Container, "DataItem.Amount", "{0:#.00}") %>'>
                                                        </asp:TextBox>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                                                    <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                    <ItemStyle CssClass="DataGridItemStyle"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnlButEdit" Text="<img border=0 src=../images//im_edit.gif alt= edit>"
                                                            CausesValidation="False" runat="server" CommandName="Edit">
																					<img border="0" src="../images/im_edit.gif" alt="edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <FooterStyle CssClass="DataGridItemStyle"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Button ID="btnAddRow" Text="Add" runat="server" CommandName="AddNewRow"></asp:Button>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:LinkButton ID="lnkbutUpdate" Text="<img border=0 src=../images/im_update.gif alt=Update>"
                                                            runat="server" CommandName="Update">
																					<img border="0" src="../images/im_update.gif" alt="update"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbutDelete" Text="<img border=0 src=../images/delete.gif alt=Delete>"
                                                            CausesValidation="False" runat="server" CommandName="Delete"></asp:LinkButton>
                                                        <asp:LinkButton ID="lnkbutCancel" Text="<img border=0 src=../images//im_delete.gif alt=Cancel>"
                                                            CausesValidation="False" runat="server" CommandName="Cancel"></asp:LinkButton>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Received Amount" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                    <ItemStyle Wrap="False" Width="100px"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReceivedAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Received", "{0:c}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle Wrap="False"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooterRecivedAmount" runat="server" Text=""></asp:Label>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditReceivedAmount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Received", "{0:c}") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Balance" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                    <ItemStyle Wrap="False" Width="100px"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBalance" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle Wrap="False"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooterBalance" runat="server" Text=""></asp:Label>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditBalance" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Balance", "{0:c}") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn HeaderText="Received Date" HeaderStyle-HorizontalAlign="Center">
                                                    <HeaderStyle CssClass="k-grid-header"></HeaderStyle>
                                                    <ItemStyle Wrap="False" Width="150px"></ItemStyle>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReceivedDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ReceivedData", "{0:MM/dd/yyyy}")%>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterStyle Wrap="False"></FooterStyle>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblFooterReceivedDate" runat="server" Text=""></asp:Label>
                                                    </FooterTemplate>
                                                    <EditItemTemplate>
                                                        <asp:Label ID="lblEditRecivedDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ReceivedData", "{0:MM/dd/yyyy}") %>'></asp:Label>
                                                    </EditItemTemplate>
                                                </asp:TemplateColumn>
                                            </Columns>
                                            <EditItemStyle></EditItemStyle>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>



                            <!-- end content table-->
                        </div>
                    </td>
                </tr>
            </table>
            <telerik:RadNotification RenderMode="Lightweight" ID="radNotificationHasPostedPayment" runat="server" EnableRoundedCorners="true" VisibleOnPageLoad="False" Position="Center"
                EnableShadow="true" TitleIcon="warning" ContentIcon="warning" AutoCloseDelay="99999" KeepOnMouseOver="True" VisibleTitlebar="False"
                Title="Unable To Delete Payment Plan" Width="300" Height="150">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td style="vertical-align: top; padding-top: 15px;">
                                <img src="../images/warningIcon.png" width="42" />
                            </td>
                            <td>
                                <p style="text-align: left; padding-left: 10px; padding-top: 0px;">
                                    There are payments posted against this payment plan.  In order to delete this plan please void all related posted payments on the student ledger. Once all payments are voided, plan can be deleted.
                                    <br />
                                </p>
                            </td>
                            <tr>
                                <td style="text-align: center" colspan="2">
                                    <telerik:RadButton runat="server" ID="btnHideNofificationHasPostedPayment" AutoPostBack="False" OnClientClicked="hideNofificationHasPostedPayment" Text="OK"></telerik:RadButton>
                                </td>
                            </tr>
                        </tr>
                    </table>

                </ContentTemplate>
            </telerik:RadNotification>
            <script type="text/javascript">
                function showWarningHasPostedPayments() {
                    var notification = $find("<%=radNotificationHasPostedPayment.ClientID%>");
                    notification.show();
                }

                function hideNofificationHasPostedPayment() {
                    var notification = $find("<%=radNotificationHasPostedPayment.ClientID%>");
                    notification.hide();
                }
            </script>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <asp:Panel ID="Panel1" runat="server" CssClass="validationsummary">
    </asp:Panel>
    <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="validationsummary"
        Display="None" ErrorMessage="CustomValidator"></asp:CustomValidator>
    <asp:Panel ID="pnlRequiredFieldValidators" runat="server">
    </asp:Panel>
    <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary"
        ShowSummary="False" ShowMessageBox="True"></asp:ValidationSummary>
</asp:Content>

