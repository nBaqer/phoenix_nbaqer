<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="Lenders.aspx.vb" Inherits="Lenders" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>
<%@ Register TagPrefix="ew" Namespace="eworld.ui" Assembly="eworld.ui, version=1.9.0.0, culture=neutral, publickeytoken=24d65337282035f2" %>
<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">
    <script language="javascript" src="../js/checkall.js" type="text/javascript" />
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

    </script>
</asp:Content>
<asp:Content ID="content2" ContentPlaceHolderID="contentmain1" runat="server">
</asp:Content>
<asp:Content ID="content3" ContentPlaceHolderID="contenterror" runat="server">
</asp:Content>
<asp:Content ID="content4" ContentPlaceHolderID="contentmain2" runat="server">
    <asp:ScriptManagerProxy ID="scriptmanagerproxy1" runat="server">
    </asp:ScriptManagerProxy>
    <div style="overflow: auto;">
        <telerik:RadSplitter ID="oldcontentsplitter" runat="server" collapsemode="none" Height="100%" Orientation="vertical"
            VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized" Style="overflow: auto;">

            <telerik:RadPane ID="oldmenupane" runat="server" BackColor="#FAFAFA" Width="350" Scrolling="Y">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td class="listframetop">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="10%" nowrap align="left">
                                        <asp:Label ID="lblShow" runat="server" CssClass="tothemeshow" Text="Show"></asp:Label></td>
                                    <td width="85%" nowrap>
                                        <asp:RadioButtonList ID="radstatus" CssClass="radiobutton" AutoPostBack="true" runat="server" RepeatDirection="horizontal">
                                            <asp:ListItem Text="Active" Selected="true" />
                                            <asp:ListItem Text="Inactive" />
                                            <asp:ListItem Text="All" />
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="listframebottom">
                            <div class="scrollleftfilters">
                                <asp:DataList ID="dlstLenders" runat="server" DataKeyField="LenderId" Width="100%">
                                    <SelectedItemStyle CssClass="selecteditemstyle"></SelectedItemStyle>
                                    <ItemStyle CssClass="itemstyle"></ItemStyle>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imginactive" ImageUrl="../images/inactive.gif" runat="server" Visible='<%# not Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:ImageButton ID="imgactive" ImageUrl="../images/active.gif" runat="server" Visible='<%# Ctype(Container.DataItem("Status"), boolean) %>' CausesValidation="false"></asp:ImageButton>
                                        <asp:Label ID="lblid" runat="server" Visible="false" Text='<%# container.dataitem("statusid")%>' />
                                        <asp:LinkButton ID="linkbutton1" CssClass="itemstyle" CausesValidation="false" runat="server" CommandArgument='<%# Container.DataItem("LenderId")%>' Text='<%# Container.DataItem("LenderDescrip")%>'>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:DataList>
                            </div>
                        </td>
                    </tr>
                </table>
            </telerik:RadPane>

            <telerik:RadPane ID="oldcontentpane" runat="server" BorderWidth="0px" Scrolling="Both" orientation="horizontaltop">
                <asp:Panel ID="pnlrhs" runat="server">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="menuframe" align="right">
                                <asp:Button ID="btnsave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnnew" runat="server" CssClass="new" Text="New" CausesValidation="false"></asp:Button>
                                <asp:Button ID="btndelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="false"></asp:Button></td>

                        </tr>
                    </table>
                    <table class="maincontenttable" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <tr>
                            <td class="detailsframe">
                                <!-- begin content table-->

                                <div class="boxContainer">
                                    <h3><%=Header.Title  %></h3>
                                    <table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                                        <asp:TextBox ID="txtLenderId" runat="server" Visible="False"></asp:TextBox><asp:CheckBox ID="chkIsInDB" runat="server" Visible="False"></asp:CheckBox><asp:TextBox ID="txtStudentId" runat="server" Visible="False"></asp:TextBox><asp:TextBox ID="txtModUser" runat="server" Visible="False">ModUser</asp:TextBox><asp:TextBox ID="txtModDate" runat="server" Visible="False">ModDate</asp:TextBox>
                                        <asp:TextBox ID="txtResourceId" runat="server" Visible="false"></asp:TextBox>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblCode" runat="server" CssClass="label">Code</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCode" TabIndex="1" runat="server" CssClass="textbox" ></asp:TextBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblPrimaryContact" runat="server" CssClass="label">Primary Contact</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtPrimaryContact" TabIndex="4" runat="server" CssClass="textbox" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblStatusId" runat="server" CssClass="label">Status</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStatusId" TabIndex="2" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblOtherContact" runat="server" CssClass="label" Width="100%">Other Contact</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtOtherContact" TabIndex="5" runat="server" CssClass="textbox" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblLenderDescrip" runat="server" CssClass="label">Lender Description</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtLenderDescrip" TabIndex="3" runat="server" CssClass="textbox" ></asp:TextBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap></td>
                                            <td class="contentcell4">
                                                <asp:CheckBox ID="chkIsLender" TabIndex="6" runat="server" CssClass="checkboxinternational" Text="Lender"
                                                    Width="100%"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblCampGrpId" runat="server" CssClass="label">Campus Group</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCampGrpId" TabIndex="2" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap></td>
                                            <td class="contentcell4"></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                            <td class="contentcell4">
                                                <asp:CheckBox ID="chkIsServicer" TabIndex="7" runat="server" CssClass="checkboxinternational"
                                                    Text="Servicer" Width="100%"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                            <td class="contentcell4"></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                            <td class="contentcell4">
                                                <asp:CheckBox ID="chkIsGuarantor" TabIndex="8" runat="server" CssClass="checkboxinternational"
                                                    Text="Guarantor" Width="100%"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="lblCorpAddress" runat="server" Font-Bold="true" CssClass="label">Corporate Address</asp:Label></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcellheader" colspan="2">
                                                <asp:Label ID="lblPaymentAddress" runat="server" Font-Bold="true" CssClass="label">Payment Address</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap></td>
                                            <td class="contentcell4" style="padding-top: 10px;">
                                                <asp:CheckBox ID="chkForeignAddress" TabIndex="9" runat="server" CssClass="checkboxinternational"
                                                    Text="International" Width="100%" AutoPostBack="True"></asp:CheckBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap></td>
                                            <td class="contentcell4" style="padding-top: 10px;">
                                                <asp:CheckBox ID="chkForeignPayAddress" TabIndex="17" runat="server" CssClass="checkboxinternational"
                                                    Text="International" Width="100%" AutoPostBack="True"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblAddress1" runat="server" CssClass="label">Address 1</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtAddress1" TabIndex="10" runat="server" CssClass="textbox" ></asp:TextBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblPayAddress1" runat="server" CssClass="label">Address 1</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtPayAddress1" TabIndex="18" runat="server" CssClass="textbox" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblAddress2" runat="server" CssClass="label">Address 2</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtAddress2" TabIndex="11" runat="server" CssClass="textbox" ></asp:TextBox>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblPayAddress2" runat="server" CssClass="label">Address 2</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtPayAddress2" TabIndex="19" runat="server" CssClass="textbox" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblCity" runat="server" CssClass="label">City</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtCity" TabIndex="12" runat="server" CssClass="textbox" ></asp:TextBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblPayCity" runat="server" CssClass="label">City</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtPayCity" TabIndex="20" runat="server" CssClass="textbox" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblState" runat="server" CssClass="label">State</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlStateId" TabIndex="13" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                                    Width="200px">
                                                </asp:DropDownList></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblPayState" runat="server" CssClass="label">State</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlPayStateId" TabIndex="21" runat="server" CssClass="dropdownlist" AutoPostBack="True"
                                                    Width="200px">
                                                </asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblOtherState" runat="server" CssClass="label">Other State or Province</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtOtherState" TabIndex="14" runat="server" CssClass="textbox" Enabled="False" ></asp:TextBox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblOtherPayState" runat="server" CssClass="label">Other State or Province</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtOtherPayState" TabIndex="22" runat="server" CssClass="textbox" Enabled="False" ></asp:TextBox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblZip" runat="server" CssClass="label">Zip</asp:Label></td>
                                            <td class="contentcell4">
                                                <ew:maskedtextbox id="txtZip" tabIndex="15" runat="server" CssClass="textbox" ></ew:maskedtextbox></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblPayZip" runat="server" CssClass="label">Zip</asp:Label></td>
                                            <td class="contentcell4">
                                                <ew:maskedtextbox id="txtPayZip" tabIndex="23" runat="server" CssClass="textbox" ></ew:maskedtextbox></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblCountry" runat="server" CssClass="label">Country</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlCountryId" TabIndex="16" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                                            <td class="emptycell"></td>
                                            <td class="contentcell" nowrap>
                                                <asp:Label ID="lblPayCountry" runat="server" CssClass="label">Country</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:DropDownList ID="ddlPayCountryId" TabIndex="24" runat="server" CssClass="dropdownlist" Width="200px"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" colspan="5">
                                                <asp:Label ID="lblPhones" runat="server" Font-Bold="true" CssClass="label">Phones and Email</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td nowrap class="contentcell"></td>
                                            <td class="contentcell4" style="padding-top: 10px;" align="left">
                                                <asp:CheckBox ID="chkForeignCustService" TabIndex="25" runat="server" CssClass="checkboxinternational"
                                                    AutoPostBack="True" Text="International" Width="100%"></asp:CheckBox></td>
                                            <td class="emptycell"></td>
                                            <td nowrap class="contentcell"></td>
                                            <td class="contentcell4" style="padding-top: 10px;" align="left">
                                                <asp:CheckBox ID="chkForeignPreClaim" TabIndex="30" runat="server" CssClass="checkboxinternational"
                                                    AutoPostBack="True" Text="International" Width="100%"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td nowrap class="contentcell">
                                                <asp:Label ID="lblCustService" runat="server" CssClass="label">Customer Service</asp:Label></td>
                                            <td class="contentcell4">
                                                <ew:maskedtextbox id="txtCustService" tabIndex="26" runat="server" CssClass="textbox" ></ew:maskedtextbox></td>
                                            <td class="emptycell"></td>
                                            <td nowrap class="contentcell">
                                                <asp:Label ID="lblPreClaim" runat="server" CssClass="label">Pre-Claim</asp:Label></td>
                                            <td class="contentcell4">
                                                <ew:maskedtextbox id="txtPreClaim" tabIndex="31" runat="server" CssClass="textbox" ></ew:maskedtextbox></td>
                                        </tr>
                                        <tr>
                                            <td nowrap class="contentcell"></td>
                                            <td class="contentcell4" style="padding: 0px;" align="left">
                                                <asp:CheckBox ID="chkForeignFax" TabIndex="27" runat="server" CssClass="checkboxinternational"
                                                    AutoPostBack="True" Text="International"></asp:CheckBox></td>
                                            <td class="emptycell"></td>
                                            <td nowrap class="contentcell"></td>
                                            <td class="contentcell4" style="padding: 0px;" align="left">
                                                <asp:CheckBox ID="chkForeignPostClaim" TabIndex="32" runat="server" CssClass="checkboxinternational"
                                                    AutoPostBack="True" Text="International" Width="100%"></asp:CheckBox></td>
                                        </tr>
                                        <tr>
                                            <td nowrap class="contentcell">
                                                <asp:Label ID="lblFax" runat="server" CssClass="label">Fax</asp:Label></td>
                                            <td class="contentcell4">
                                                <ew:maskedtextbox id="txtFax" tabIndex="28" runat="server" CssClass="textbox" ></ew:maskedtextbox></td>
                                            <td class="emptycell"></td>
                                            <td nowrap class="contentcell">
                                                <asp:Label ID="lblPostClaim" runat="server" CssClass="label">Post-Claim</asp:Label></td>
                                            <td class="contentcell4">
                                                <ew:maskedtextbox id="txtPostClaim" tabIndex="33" runat="server" CssClass="textbox" ></ew:maskedtextbox></td>
                                        </tr>
                                        <tr>
                                            <td nowrap class="contentcell">
                                                <asp:Label ID="lblEmail" runat="server" CssClass="label">Email</asp:Label></td>
                                            <td class="contentcell4">
                                                <asp:TextBox ID="txtEmail" TabIndex="29" runat="server" CssClass="textbox" ></asp:TextBox><asp:RegularExpressionValidator ID="revEmail" runat="server" ErrorMessage="Invalid Email Address" Display="None"
                                                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
                                            <td class="emptycell"></td>
                                            <td nowrap class="contentcell"></td>
                                            <td class="contentcell4"></td>
                                        </tr>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" colspan="5">
                                                <asp:Label ID="lblCommentsBlock" runat="server" Font-Bold="true" CssClass="label">Comments</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="padding-top: 10px;">
                                                <asp:Label ID="lblComments" runat="server" CssClass="label">Comments</asp:Label></td>
                                            <td class="contentcell4" colspan="4" style="padding-top: 10px;" align="left">
                                                <asp:TextBox ID="txtComments" TabIndex="34" runat="server" CssClass="ToCommentsNoWrap" MaxLength="300" Width="200px"
                                                    TextMode="MultiLine" Columns="30" Rows="6"></asp:TextBox></td>
                                        </tr>
                                        <%  If MyAdvAppSettings.AppSettings("REGENT").ToString.ToLower = "yes" Then %>
                                        <tr>
                                            <td class="spacertables"></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcellheader" colspan="5">
                                                <asp:Label ID="Label1" runat="server" Font-Bold="true" CssClass="label">Map To Regent</asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td class="contentcell" style="padding-top: 10px;">
                                                <asp:Label ID="Label2" runat="server" CssClass="label">Lender Code</asp:Label></td>
                                            <td class="contentcell2" colspan="4" style="padding-top: 10px;">
                                                <asp:DropDownList ID="ddlLenderCodeId" runat="server" CssClass="textbox" Width="320px"></asp:DropDownList></td>
                                        </tr>
                                        <% end if %>
                                    </table>
                                </div>
                                <!--end table content-->

                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </telerik:RadPane>
        </telerik:RadSplitter>
        <!-- start validation panel-->
        <asp:Panel ID="Panel1" runat="server" CssClass="ValidationSummary"></asp:Panel>
        <asp:CustomValidator ID="Customvalidator1" runat="server" CssClass="ValidationSummary" ErrorMessage="CustomValidator"
            Display="None"></asp:CustomValidator><asp:Panel ID="pnlRequiredFieldValidators" runat="server"></asp:Panel>
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="ValidationSummary" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
        <!--end validation panel-->
    </div>

</asp:Content>


