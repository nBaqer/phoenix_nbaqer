Imports System.Data
Imports System.IO
Imports Fame.Advantage.Api.Library.Models.TimeClock
Imports Fame.Advantage.Common
Imports Fame.AdvantageV1.BusinessFacade.TimeClock
Imports Fame.AdvantageV1.Common
Imports Fame.Common
Imports BO = Advantage.Business.Objects

Partial Class FA_ImportTimeClockFile
    Inherits BasePage
    Private pObj As New UserPagePermissionInfo
    Protected ResourceId As Integer
    Protected campusId, userId As String
    Protected moduleid As String
    Dim tc As TimeClockFacade = New TimeClockFacade()
    Protected MyAdvAppSettings As AdvAppSettings
    'Private FileData As Byte()
    'Private FileName As String

    Protected Sub Submit1_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Submit1.ServerClick
        ''Label showing Processing
        ''Added by Saraswathi lakshmanan

        Dim TCFilename As String
        TCFilename = ddlFilename.SelectedValue
        'TCFilename = Me.FileName

        Me.divLog.Visible = True

        If TCFilename <> "" Then
            'Open a file for reading
            Dim FILENAME As String = ddlFilename.SelectedValue


            'Get a StreamReader class that can be used to read the file
            'Dim objStreamReader As StreamReader = New StreamReader(New MemoryStream(Me.FileData))
            ''Now, read the entire file into a string
            'Dim contents As String = objStreamReader.ReadToEnd()
            'Dim len As Integer = contents.Length
            Dim sbuf As String

            'sbuf = contents
            'If contents.Length = 0 Then
            '    DisplayErrorMessage("Unable to read file. Please, make sure the rights are set")
            '    Exit Sub
            'End If

            Dim logResult = tc.UpsertTimeClockImportLog(New TimeClockLogParams() With {
                                    .CampusId = New Guid(campusId),
                                    .FileName = FILENAME,
                                    .Message = "Processing",
                                    .Status = 2
                                    })
            Dim logId = IIf(String.IsNullOrEmpty(logResult.Result), Nothing, New Guid(logResult.Result))

            sbuf = processFile()
            If sbuf = "Error" Then
                Dim errMsg = "Unable to read file. Please, make sure the rights are set"
                tc.UpsertTimeClockImportLog(New TimeClockLogParams() With {
                                    .CampusId = New Guid(campusId),
                                    .FileName = FILENAME,
                                    .Message = errMsg,
                                    .Status = 0,
                                    .TimeClockImportLogId = logId
                                    })
                DisplayErrorMessage(errMsg)
                Exit Sub
            End If

            Dim res = ""
            If MyAdvAppSettings.AppSettings("TrackSapAttendance", campusId).ToString.ToLower = "byday" Then
                If Me.ddlFileType.SelectedValue = "0" Then
                    res = tc.Run(sbuf, AdvantageSession.UserName, campusId, FILENAME)
                ElseIf ddlFileType.SelectedValue = "1" Then
                    res = tc.Run(sbuf, Fame.AdvantageV1.Common.TimeClock.TimeClockDataType.ZON580, AdvantageSession.UserName, campusId, FILENAME)
                Else
                    res = tc.Run(sbuf, Fame.AdvantageV1.Common.TimeClock.TimeClockDataType.CS2000, AdvantageSession.UserName, campusId, FILENAME)
                End If
            Else
                res = tc.Run_ByClass(sbuf, AdvantageSession.UserName, campusId, FILENAME)
            End If
            Me.txtLog.Text = tc.Log()
            tc.UpsertTimeClockImportLog(New TimeClockLogParams() With {
                                    .CampusId = New Guid(campusId),
                                    .FileName = FILENAME,
                                    .Message = Me.txtLog.Text,
                                    .Status = 1,
                                    .TimeClockImportLogId = logId
                                    })
            BuildFilesList()
            'If res = "" Then

            '    If (Not Session("AdvantageApiToken") Is Nothing) Then
            '        Try
            '            Dim tokenResponse As TokenResponse = CType(Session("AdvantageApiToken"), TokenResponse)
            '            Dim fileStorageRequest As New FileStorageRequest(tokenResponse.ApiUrl, tokenResponse.Token)
            '            Dim pass As Boolean = fileStorageRequest.UploadFile(Me.FileData, Me.FileName, Guid.Parse(campusId), FileConfigurationFeature.TC)
            '            If Not pass Then
            '                DisplayErrorMessage("Failed to Archive the imported time clock file")
            '            End If

            '        Catch
            '            DisplayErrorMessage("An error occured when trying to archive the file")
            '        End Try
            '    End If

            'End If

        Else
            Me.txtLog.Text = "No file selected."
        End If
        Me.Submit1.Value = "Upload"
        lblprocesstxt.Text = ""
    End Sub

    'Protected Sub FileUploaded(sender As Object, e As FileUploadedEventArgs) Handles AsyncUpload1.FileUploaded
    '    Me.FileName = e.File.FileName
    '    Dim rawData = e.File.InputStream
    '    Dim fileBytes(0 To rawData.Length - 1) As Byte
    '    rawData.Read(fileBytes, 0, fileBytes.Length)
    '    rawData.Close()
    '    Me.FileData = fileBytes
    'End Sub

    'Protected Sub manager_AjaxRequest(ByVal sender As Object, ByVal e As Telerik.Web.UI.AjaxRequestEventArgs)
    '    'handle the manager AjaxRequest event here
    '    Dim fileEvent As String = e.Argument
    '    If (fileEvent.Equals("fileRemoved", StringComparison.InvariantCultureIgnoreCase)) Then
    '        Me.FileData = Nothing
    '        Me.FileName = Nothing
    '    End If
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Dim manager As RadAjaxManager = RadAjaxManager.GetCurrent(Page)
        'AddHandler manager.AjaxRequest, AddressOf manager_AjaxRequest

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        'Put user code to initialize the page here
        Dim objCommon As New CommonUtilities
        Dim SDFControls As New SDFComponent
        ResourceId = Trim(Request.QueryString("resid"))


        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        moduleid = HttpContext.Current.Request.Params("Mod").ToString
        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If


        If Not IsPostBack Then
            objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "NEW")
            If pObj.HasFull Or pObj.HasAdd Then
                btnSave.Enabled = True
                btnNew.Enabled = True
                Submit1.Disabled = False
            Else
                btnSave.Enabled = False
                btnNew.Enabled = True
                Submit1.Disabled = True
            End If
            ViewState("MODE") = "NEW"
            btnNew.Enabled = False
            btnDelete.Enabled = False
            BuildFilesList()
        Else
            '   objCommon.PageSetup(CType(Master.FindControl("ContentMain2"), ContentPlaceHolder), "EDIT")
            btnNew.Enabled = False
            btnDelete.Enabled = False
        End If
        btnSave.Enabled = False
    End Sub
    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Display error in message box in the client
        CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
    End Sub
    Private Sub BuildFilesList()
        Dim i As Integer
        Dim filename As String

        Dim TCSourcePath As String = ""
        Dim TCTargetPath As String = ""
        Dim IsRemoteServer As String = ""
        Dim SourceFolderLocation As String = ""
        Dim TargetFolderLocation As String = ""
        Dim RemoteUsername As String = ""
        Dim RemotePassword As String = ""


        Dim filenames() As String = Nothing
        Dim Tc As TimeClockFacade = New TimeClockFacade()
        Dim dsTCPathDetails As DataSet
        dsTCPathDetails = Tc.GetTimeClockSourceandTargetLocations(campusId)
        If dsTCPathDetails.Tables.Count > 0 Then
            If dsTCPathDetails.Tables(0).Rows.Count > 0 Then
                Session("dsTCPathDetails") = dsTCPathDetails
                TCSourcePath = dsTCPathDetails.Tables(0).Rows(0)("TCSourcePath").ToString
                TCTargetPath = dsTCPathDetails.Tables(0).Rows(0)("TCTargetPath").ToString
                IsRemoteServer = dsTCPathDetails.Tables(0).Rows(0)("IsRemoteServer").ToString.ToLower
                SourceFolderLocation = TCSourcePath
                TargetFolderLocation = TCTargetPath
                'SourceFolderLocation = dsTCPathDetails.Tables(0).Rows(0)("SourceFolderLoc").ToString
                'TargetFolderLocation = dsTCPathDetails.Tables(0).Rows(0)("TargetFolderLoc").ToString
                RemoteUsername = dsTCPathDetails.Tables(0).Rows(0)("RemoteServerUsrNm").ToString
                RemotePassword = dsTCPathDetails.Tables(0).Rows(0)("RemoteServerPwd").ToString
            End If
        End If



        If IsRemoteServer <> "" Then
            If IsRemoteServer.ToLower = "true" Then
                If SourceFolderLocation.StartsWith("http:") Then
                    Try
                        filenames = Directory.GetFiles(SourceFolderLocation)
                    Catch e As Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(e)

                        DisplayErrorMessage("File(s) not found in the specified path")
                        Exit Sub
                    End Try
                Else
                    'Dim uriSource As New Uri(SourceFolderLocation)
                    'Dim strSourcePath As String = uriSource.AbsolutePath
                    Try
                        filenames = Directory.GetFiles(SourceFolderLocation)
                    Catch e As Exception
                        Dim exTracker = New AdvApplicationInsightsInitializer()
                        exTracker.TrackExceptionWrapper(e)

                        DisplayErrorMessage("File(s) not found in the specified path")
                        Exit Sub
                    End Try
                End If

            Else
                If TCSourcePath <> "" Then
                    If TCSourcePath.StartsWith("http:") Then
                        Try
                            filenames = Directory.GetFiles(TCSourcePath)
                        Catch e As Exception
                            Dim exTracker = New AdvApplicationInsightsInitializer()
                            exTracker.TrackExceptionWrapper(e)

                            DisplayErrorMessage("File(s) not found in the specified path")
                            Exit Sub
                        End Try
                    Else
                        Dim uriSource As New Uri(TCSourcePath)
                        Dim strSourcePath As String = uriSource.AbsolutePath
                        Try
                            filenames = Directory.GetFiles(strSourcePath)
                        Catch e As Exception
                            Dim exTracker = New AdvApplicationInsightsInitializer()
                            exTracker.TrackExceptionWrapper(e)

                            DisplayErrorMessage("File(s) not found in the specified path")
                            Exit Sub
                        End Try

                    End If


                End If
            End If
        End If

        ddlFilename.Items.Clear()
        With ddlFilename
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        If IsRemoteServer <> "" Then
            If Not filenames Is Nothing Then
                If filenames.Length > 0 Then
                    For i = 0 To filenames.Length - 1
                        filename = Path.GetFileName(filenames(i)) 'This is much better than substring
                        ddlFilename.Items.Add(filename)
                    Next i
                End If
            End If

        End If
    End Sub


    Private Function processFile() As String
        Dim dsTCPathDetails As New DataSet
        Dim clsflFile As New TCFileInfo
        Dim contents As String = ""
        Dim flname As String

        Try
            dsTCPathDetails = Session("dsTCPathDetails")
            flname = dsTCPathDetails.Tables(0).Rows(0)("TCSourcePath").ToString
            If dsTCPathDetails.Tables(0).Rows(0)("IsRemoteServer").ToString.ToLower = "true" Then
                clsflFile.datafromremotecomputer = True
                clsflFile.networkuser = dsTCPathDetails.Tables(0).Rows(0)("RemoteServerUsrNm").ToString
                clsflFile.networkPassword = dsTCPathDetails.Tables(0).Rows(0)("RemoteServerPwd").ToString
                If Not flname.EndsWith("\") Then
                    flname = flname + "\"
                End If
            Else
                If Not flname.EndsWith("/") Then
                    flname = flname + "/"
                End If
            End If
            Dim str1 As String = String.Concat(flname, ddlFilename.SelectedItem.Text)
            clsflFile.strFileName = str1
            If dsTCPathDetails.Tables(0).Rows(0)("IsRemoteServer").ToString.ToLower = "true" Then
                Dim strSourcepath As String
                '  Dim strSourceFileLocation As String = dsTCPathDetails.Tables(0).Rows(0)("SourceFolderLoc").ToString + ddlFilename.SelectedItem.Text
                strSourcepath = dsTCPathDetails.Tables(0).Rows(0)("TCSourcePath").ToString
                Dim strSourceFileLocation As String
                If Not strSourcepath.EndsWith("\") Then
                    strSourcepath = strSourcepath + "\"
                End If
                strSourceFileLocation = strSourcepath + ddlFilename.SelectedItem.Text
                clsflFile.strFileName = strSourceFileLocation
                clsflFile.strFileName = strSourceFileLocation
                contents = tc.ProcessFLFile(clsflFile, strSourceFileLocation)
            Else
                contents = tc.ProcessFLFile(clsflFile, "")
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            contents = "Error"
            Return contents
        End Try

        Return contents
    End Function

    ' ''Added by Saraswathi lakshmanan on August 24 2009
    ' ''To find the list controls and add a tool tip to those items in the control
    ' ''list controls include drop down list, list box, group checkbox, etc.
    'Public Sub BIndToolTip()
    '    Dim i As Integer
    '    Dim ctl As Control
    '    For Each ctl In Page.Form.Controls
    '        If TypeOf ctl Is ListControl Then
    '            For i = 0 To DirectCast(ctl, ListControl).Items.Count - 1
    '                DirectCast(ctl, ListControl).Items(i).Attributes.Add("title", DirectCast(ctl, ListControl).Items(i).Text)
    '            Next
    '        End If
    '        If TypeOf ctl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctl)
    '        End If
    '        If TypeOf ctl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctl)
    '        End If
    '    Next
    'End Sub
    'Public Sub BindToolTipForControlsInsideaPanel(ByVal Ctrlpanel As Panel)
    '    Dim ctrl As Control
    '    Dim j As Integer
    '    For Each ctrl In Ctrlpanel.Controls
    '        If TypeOf ctrl Is ListControl Then
    '            For j = 0 To DirectCast(ctrl, ListControl).Items.Count - 1
    '                DirectCast(ctrl, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl, ListControl).Items(j).Text)
    '            Next
    '        ElseIf TypeOf ctrl Is Panel Then
    '            BindToolTipForControlsInsideaPanel(ctrl)
    '        ElseIf TypeOf ctrl Is DataGrid Then
    '            BindToolTipForControlsInsideaGrid(ctrl)
    '        End If
    '    Next

    'End Sub

    'Public Sub BindToolTipForControlsInsideaGrid(ByVal CtrlGrid As DataGrid)
    '    Dim j As Integer
    '    Dim itm As DataGridItem
    '    Dim ctrl As Control
    '    Dim ctrl1 As Control

    '    For Each itm In CtrlGrid.Items
    '        For Each ctrl In itm.Controls
    '            For Each ctrl1 In ctrl.Controls
    '                If TypeOf ctrl1 Is ListControl Then
    '                    For j = 0 To DirectCast(ctrl1, ListControl).Items.Count - 1
    '                        DirectCast(ctrl1, ListControl).Items(j).Attributes.Add("title", DirectCast(ctrl1, ListControl).Items(j).Text)
    '                    Next
    '                End If
    '            Next
    '        Next
    '    Next
    'End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        BindToolTip()
    End Sub
End Class
