﻿Imports FAME.Common
Imports System.Xml
Imports FAME.AdvantageV1.Common
Imports System.Data
Imports FAME.AdvantageV1.BusinessFacade
Imports BO = Advantage.Business.Objects
Imports System.Collections
Imports System.Collections.Generic
Imports FAME.Advantage.Common
Imports FAME.DataAccessLayer
Imports Advantage.Business.Logic.Layer
Imports Advantage.Business.Objects
Imports FAME.Advantage.DataAccess.LINQ
Imports FAME.Advantage.DataAccess.LINQ.FAME.Advantage.DataAccess.LINQ.Common.Constants
Imports Telerik.Web.UI

Partial Class StudentAwards
    Inherits BasePage

    Private Amount As Decimal = 0.0
    Private NumberOfPayments As Integer = 0

    Protected WithEvents btnhistory As System.Web.UI.WebControls.Button
    Protected WithEvents txtStuEnrollId As System.Web.UI.WebControls.TextBox
    Protected WithEvents Linkbutton1 As System.Web.UI.WebControls.LinkButton
    Protected WithEvents rfvAwardTypeId As System.Web.UI.WebControls.RequiredFieldValidator
    'Protected WithEvents rfvAwardYearId As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents rfvLenderId As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents rfvGrossAmount As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents LenderCompareValidator As System.Web.UI.WebControls.CompareValidator
    Protected WithEvents LoanFeesRangeValidator As System.Web.UI.WebControls.RangeValidator
    Protected WithEvents lblAwardType As System.Web.UI.WebControls.Label
    Protected WithEvents lblAwardYear As System.Web.UI.WebControls.Label
    Protected WithEvents lblLender As System.Web.UI.WebControls.Label
    Protected WithEvents lblAwdStartDate As System.Web.UI.WebControls.Label
    Protected WithEvents allDataset As System.Data.DataSet
    Protected WithEvents dataGridTable As System.Data.DataTable
    Protected WithEvents dataListTable As System.Data.DataTable
    Protected studentId As String

    'Protected WithEvents ddlAwardYearId As System.Web.UI.WebControls.DropDownList
    Protected resourceId As Integer
    Protected moduleid As String
    Protected sdfcontrols As New SDFComponent
    Private pObj As New UserPagePermissionInfo

    Protected state As AdvantageSessionState
    Protected LeadId As String
    Protected boolSwitchCampus As Boolean = False



#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub
#Region "MRU Routines"
    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()

    End Sub
    Private Function getStudentFromStateObject(ByVal paramResourceId As Integer) As BO.StudentMRU

        Dim objStudentState As New BO.StudentMRU

        Try

            MyBase.GlobalSearchHandler(0)


            boolSwitchCampus = Master.IsSwitchedCampus 'User switched campus

            If String.IsNullOrEmpty(AdvantageSession.MasterStudentId) Then
                studentId = Guid.Empty.ToString()
            Else
                studentId = AdvantageSession.MasterStudentId
            End If

            If String.IsNullOrEmpty(AdvantageSession.MasterLeadId) Then
                LeadId = Guid.Empty.ToString()
            Else
                LeadId = AdvantageSession.MasterLeadId
            End If


            With objStudentState
                .StudentId = New Guid(studentId)
                .LeadId = New Guid(LeadId)
                .Name = AdvantageSession.MasterName
            End With

            HttpContext.Current.Items("ResourceId") = Request.QueryString("resid")
            HttpContext.Current.Items("Language") = "En-US"

            Master.ShowHideStatusBarControl(True)





        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            Dim strSearchUrl As String = ""
            strSearchUrl = "~/PL/StudentSearch.aspx?resid=308&mod=" + Request.QueryString("mod") + "&cmpid=" + AdvantageSession.UserState.CampusId.ToString + "&desc=View Existing Students"
            Response.Redirect(strSearchUrl)
        End Try

        Return objStudentState

    End Function
#End Region


#End Region


    Private campusId As String
    Private AfaIntegrationEnabled As Boolean
    Protected MyAdvAppSettings As AdvAppSettings
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings) Is Nothing Then
            MyAdvAppSettings = DirectCast(HttpContext.Current.Session("AdvAppSettings"), AdvAppSettings)
        Else
            MyAdvAppSettings = New AdvAppSettings
        End If

        If Not Session("EnableAFAIntegration") Is Nothing Then
            AfaIntegrationEnabled = DirectCast(Session("EnableAFAIntegration"), Boolean)
        End If
        Dim objCommon As New CommonUtilities

        campusId = Master.CurrentCampusId
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))

        Session("SEARCH") = 0

        '   disable History button the first time
        'Header1.EnableHistoryButton(False)

        Dim advantageUserState As New BO.User()
        advantageUserState = AdvantageSession.UserState

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

        '''''''''''''''''' Call to get student and lead ids starts here ''''''''''''''''''''
        'Get StudentId and LeadId
        Dim objStudentState As New BO.StudentMRU
        objStudentState = getStudentFromStateObject(175) 'Pass resourceid so that user can be redirected to same page while swtiching students
        If objStudentState Is Nothing Then
            MyBase.RedirectToStudentSearchPage(Request.QueryString("mod"), AdvantageSession.UserState.CampusId.ToString)
            Exit Sub
        End If

        With objStudentState
            studentId = .StudentId.ToString
            LeadId = .LeadId.ToString

        End With

        '''''''''''''''''' Call to get student and lead ends here ''''''''''''''''''''

        If Not IsPostBack Or (Not String.IsNullOrEmpty(MyBase.uSearchEntityControlId.Value)) Then
            If boolSwitchCampus = True Then
                CampusObjects.ShowNotificationWhileSwitchingCampus(1, objStudentState.Name)
            End If
            'objCommon.PageSetup(Form1, "NEW")
            'objCommon.PopulatePage(Form1)
            'Disable the new and delete buttons
            'objCommon.SetBtnState(Form1, "NEW")
            ViewState("MODE") = "NEW"
            objCommon.SetCaptionsAndColorRequiredFields(Master.FindControl("ContentMain2"))

            '   build dropdownlists
            BuildDropDownLists()

            '   bind an empty new StuAwardInfo
            'BindStudentAwardData(New StuAwardInfo)

            '   get allDataset from the DB
            '   bind datalist

            With New StuAwardsFacade
                allDataset = .GetStuAwardsDS(studentId)
            End With

            '   save it on the session
            Session("AllDataset") = allDataset

            '   initialize buttons
            InitButtonsForLoad()

            '   hide datagrid
            dgrdDisbursements.Visible = False

            'When the page first loads we want to select the first enrollment in the list.
            'This will always be the second item in the list since the first is the "Select" item.
            If ddlStuEnrollId.Items.Count > 1 Then
                ddlStuEnrollId.SelectedIndex = 1
                ProcessEnrollmentSelected()
            End If

            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                ddlAwardCode.Visible = True
                ddlAwardSubCode.Visible = True
                lblAwardCode.Visible = True
                lblAwardSubCode.Visible = True
                'ddlAwardCode.BackColor = Color.FromName("#ffff99")
                'ddlAwardSubCode.BackColor = Color.FromName("#ffff99")
                BuildSession()

            End If
            '16521: ENH: Galen: FERPA: Compliance Issue 
            'added by Theresa G on May 7th 2010
            'If (New StudentFERPA).HasFERPAPermission(resourceId, studentId) Then
            '    CommonWebUtilities.OpenFERPAPopUP(Page, studentId, resourceId.ToString)
            'End If
            MyBase.uSearchEntityControlId.Value = ""
            Dim afaIntegrationValue = If(Not String.IsNullOrEmpty(MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration, campusId)), MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration, campusId), MyAdvAppSettings.AppSettings(AppSettingKeyNames.EnableAFAIntegration).ToString).Trim.ToLower
            Me.AfaIntegrationEnabled = afaIntegrationValue = "yes"
            Session("EnableAFAIntegration") = Me.AfaIntegrationEnabled

        Else
            'objCommon.PageSetup(Form1, "EDIT")
            objCommon.SetCaptionsAndColorRequiredFields(Master.FindControl("ContentMain2"))
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                ddlAwardCode.Visible = True
                ddlAwardSubCode.Visible = True
                lblAwardCode.Visible = True
                lblAwardSubCode.Visible = True
                'ddlAwardCode.BackColor = Color.FromName("#ffff99")
                'ddlAwardSubCode.BackColor = Color.FromName("#ffff99")
            End If
        End If

        '   create Dataset and Tables
        CreateDatasetAndTables()

        lblAwardType = New System.Web.UI.WebControls.Label
        lblAwardType.Text = "Fund Source*"

        If Not IsPostBack Then
            '   the first time we have to bind an empty datagrid
            'PrepareForNewData()
        End If

        moduleid = HttpContext.Current.Request.Params("Mod").ToString
        resourceId = CInt(HttpContext.Current.Request.Params("resid"))

        'Check If any UDF exists for this resource
        Dim intSDFExists As Integer = sdfcontrols.GetSDFExists(resourceId, moduleid)
        If intSDFExists >= 1 Then
            pnlUDFHeader.Visible = True
        Else
            pnlUDFHeader.Visible = False
        End If

        If Not ddlAwardTypeId.SelectedValue = Guid.Empty.ToString Then
            sdfcontrols.GenerateControlsEditSingleCell(pnlSDF, resourceId, txtStudentAwardId.Text, moduleid)
        Else
            sdfcontrols.GenerateControlsNewSingleCell(pnlSDF, resourceId, moduleid)
        End If
        If Not IsNothing(MyAdvAppSettings.AppSettings("Allow0GrossAmountLoans")) Then
            Dim allow0GrossAmountLoans = MyAdvAppSettings.AppSettings("Allow0GrossAmountLoans")
            If allow0GrossAmountLoans.ToString.Trim.ToLower = "yes" Then
                GrossAmountRangeValidator.MinimumValue = 0
            End If
        Else
            GrossAmountRangeValidator.MinimumValue = 0.01
        End If
    End Sub
    Private Sub CreateDatasetAndTables()
        '   Get dataGrid Dataset from the session and create tables
        allDataset = Session("AllDataset")
        dataGridTable = allDataset.Tables("StudentAwardSchedules")
        dataListTable = allDataset.Tables("StudentAwards")
    End Sub
    Private Sub PrepareForNewData()
        '   bind StudentAwards datalist
        BindDataList()
        'dlstStuAwards.DataSource = dataListTable
        'dlstStuAwards.DataBind()

        '   save StudentAwardId in session
        Session("StudentAwardId") = Nothing

        '   Bind DataGrid
        BindDataGrid()

        'Build session
        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            BuildSession()
        End If


        '   bind an empty new StuAwardInfo
        BindStudentAwardData(New StuAwardInfo)

        '   initialize buttons
        InitButtonsForLoad()
    End Sub
    Private Sub BindDataList(ByVal stuEnrollId As String)
        '   bind StudentAwards datalist
        Dim ds As New DataSet
        ''Function Added by Kamalesh Ahuja on 31 Aug 2010 to Resolve Mantis Issue Id 19665
        ''ds = (New StuAwardsFacade).GetAllStudentAwards(stuEnrollId)
        ds = (New StuAwardsFacade).GetAllStudentAwardsNew(stuEnrollId)
        ''Function Added by Kamalesh Ahuja on 31 Aug 2010 to Resolve Mantis Issue Id 19665
        'For Each dr As DataRow In ds.Tables(0).Rows
        '    If CType(dr("AwardStartDate"), Date).ToShortDateString = "1/1/2" Then
        '        dr("AwardStartDate") = Date.Now.ToShortDateString
        '    End If
        '    If CType(dr("AwardEndDate"), Date).ToShortDateString = "12/30/1899" Then
        '        dr("AwardEndDate") = Date.Now.ToShortDateString
        '    End If
        'Next
        dlstStudentAwards.DataSource = ds
        dlstStudentAwards.DataBind()
    End Sub
    Private Sub BuildSession()
        With ddlAwardCode
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("AwardCode", txtStudentAwardId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlAwardSubCode
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("AwardSubCode", txtStudentAwardId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
        With ddlAwardStatus
            .DataTextField = "AgencyDescrip"
            .DataValueField = "RptAgencyFldValId"
            .DataSource = (New ReportingAgencies).BuildSession("AwardStatus", txtStudentAwardId.Text)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BindDataList()
        '   create row filter and sort expression
        Dim rowFilter As String = "StuEnrollId = '" & ddlStuEnrollId.SelectedValue & "'"
        Dim sortExpression As String = "AwardStartDate DESC, AwardEndDate ASC, AwardTypeDescrip"

        '   bind StuAwards datalist
        dlstStudentAwards.DataSource = New DataView(dataListTable, rowFilter, sortExpression, DataViewRowState.CurrentRows)
        dlstStudentAwards.DataBind()

        If Not Session("StudentAwardId") Is Nothing Then
            '   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentAwards, Session("StudentAwardId"), ViewState, Header1)
        End If
    End Sub
    Private Sub BuildDropDownLists()
        BuildStuEnrollDDL()
        'BuildAwardTypeDDL()
        'BuildAwardYearDDL()
        'BuildLenderDDL()
        'BuildServicerDDL()
        'BuildGuarantorDDL()

        'this is the list of ddls
        Dim ddlList As List(Of AdvantageDDLDefinition) = New List(Of AdvantageDDLDefinition)()

        'Dim x1 As Fund_SourcesDDLMetadata
        'Dim x2 As AcademicYearsDDLMetadata
        'Dim x3 As LendersDDLMetadata
        'Dim x4 As ServicersDDLMetadata
        'Dim x5 As GuarantorsDDLMetadata

        BuildAwardTypeDDL()
        'Fund Sources DDL 
        'ddlList.Add(New AdvantageDDLDefinition(ddlAwardTypeId, AdvantageDropDownListName.Fund_Sources, campusId, True, True))

        'Academic Years DDL 
        ddlList.Add(New AdvantageDDLDefinition(ddlAcademicYearId, AdvantageDropDownListName.AcademicYears, campusId, True, True))

        'Lenders DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlLenderId, AdvantageDropDownListName.Lenders, campusId, True, True))

        'Servicers DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlServicerId, AdvantageDropDownListName.Servicers, campusId, True, True))

        'Guarantors DDL
        ddlList.Add(New AdvantageDDLDefinition(ddlGuarantorId, AdvantageDropDownListName.Guarantors, campusId, True, True))

        'Build DDLs
        CommonWebUtilities.BuildAdvantageDropDownLists(ddlList)
    End Sub
    Private Sub BuildStuEnrollDDL()
        '   bind the StuEnrollment DDL
        Dim transcript As New TranscriptFacade
        With ddlStuEnrollId
            .DataTextField = "PrgVerDescrip"
            .DataValueField = "StuEnrollId"
            .DataSource = transcript.GetEnrollment(studentId.ToUpper)
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildAwardTypeDDL()
        '   bind the AwardType DDL
        Dim stuAccounts As New StudentsAccountsFacade
        With ddlAwardTypeId
            .DataTextField = "FundSourceDescrip"
            .DataValueField = "FundSourceId"
            .DataSource = stuAccounts.GetAllFundSourcesX("True", campusId, AfaIntegrationEnabled AndAlso ViewState("MODE") = "NEW")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
        ''   bind the AwardType DDL
        'Dim stuAwards As New StuAwardsFacade
        'With ddlAwardTypeId
        '    .DataTextField = "AwardTypeDescrip"
        '    .DataValueField = "AwardTypeId"
        '    .DataSource = stuAwards.GetAllAwardTypes("True")    'True' retrieves only Active Award Types
        '    .DataBind()
        '    .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
        '    .SelectedIndex = 0
        'End With
    End Sub
    Private Sub BuildAwardYearDDL()
        '   bind the AcademicYears DDL
        Dim academicYears As New StudentsAccountsFacade

        With ddlAcademicYearId
            .DataTextField = "AcademicYearDescrip"
            .DataValueField = "AcademicYearId"
            .DataSource = academicYears.GetAllAcademicYears()
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With

  
    End Sub
    Private Sub BuildLenderDDL()
        Dim stuAwards As New StuAwardsFacade
        With ddlLenderId
            .DataTextField = "LenderDescrip"
            .DataValueField = "LenderId"
            .DataSource = stuAwards.GetLendersOnly("All")   'All' => Retrieve Active and Inactive Lenders
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildServicerDDL()
        Dim stuAwards As New StuAwardsFacade
        With ddlServicerId
            .DataTextField = "LenderDescrip"
            .DataValueField = "LenderId"
            .DataSource = stuAwards.GetServicersOnly("All")   'All' => Retrieve Active and Inactive Servicers
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub BuildGuarantorDDL()
        Dim stuAwards As New StuAwardsFacade
        With ddlGuarantorId
            .DataTextField = "LenderDescrip"
            .DataValueField = "LenderId"
            .DataSource = stuAwards.GetGuarantorsOnly("All")   'All' => Retrieve Active and Inactive Guarantors
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub
    Private Sub GetStuAwardsDS()
        '   get allDataset from the DB
        With New StuAwardsFacade
            allDataset = .GetStuAwardsDS(studentId)
        End With

        '   save it on the session
        Session("AllDataset") = allDataset

        '   create Dataset and Tables
        CreateDatasetAndTables()
    End Sub

   

    Private Sub dlstStudentAwards_ItemCommand(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlstStudentAwards.ItemCommand
        ViewState("MODE") = "EDIT"
        '   this portion of code added to refresh data from the database
        GetStuAwardsDS()

        '   save StudentAwardId in session
        Dim strId As String = dlstStudentAwards.DataKeys(e.Item.ItemIndex).ToString()
        Session("StudentAwardId") = strId
        Master.PageObjectId = e.CommandArgument
        Master.PageResourceId = resourceId
        Master.SetHiddenControlForAudit()

        '   get the row with the data to be displayed
        Dim row() As DataRow = dataListTable.Select("StudentAwardId=" + "'" + CType(Session("StudentAwardId"), String) + "'")
        'Dim a As Boolean = CType(Container.DataItem, System.Data.DataRowView).Row.RowState Or System.Data.DataRowState.Added + System.Data.DataRowState.Modified

        '   populate controls with row data
        txtStudentAwardId.Text = CType(row(0)("StudentAwardId"), Guid).ToString
        'txtStuEnrollId.Text = CType(row(0)("StuEnrollId"), Guid).ToString
        'ddlAwardTypeId.SelectedValue = CType(row(0)("AwardTypeId"), Guid).ToString
        CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAwardTypeId, CType(row(0)("AwardTypeId"), Guid).ToString, row(0)("AwardTypeDescrip"))
        If Not CType(row(0)("AwardStartDate"), DateTime).ToShortDateString = "12/30/1899" Then
            txtAwardStartDate.SelectedDate = CType(row(0)("AwardStartDate"), DateTime).ToShortDateString
        Else
            txtAwardStartDate.SelectedDate = Nothing
        End If
        If Not CType(row(0)("AwardEndDate"), DateTime).ToShortDateString = "12/30/1899" Then
            txtAwardEndDate.SelectedDate = CType(row(0)("AwardEndDate"), DateTime).ToShortDateString
        Else
            txtAwardEndDate.SelectedDate = Nothing
        End If
        'ddlAcademicYearId.SelectedValue = CType(row(0)("AcademicYearId"), Guid).ToString
        Try
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAcademicYearId, CType(row(0)("AcademicYearId"), Guid).ToString, row(0)("AcademicYear"))
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            ddlAcademicYearId.SelectedIndex = 0
        End Try
        txtLoanId.Text = row(0)("LoanId")
        If (Not row(0)("LenderId") Is System.DBNull.Value) Then
            'ddlLenderId.SelectedValue = CType(row(0)("LenderId"), Guid).ToString
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlLenderId, CType(row(0)("LenderId"), Guid).ToString, row(0)("Lender"))
        Else
            ddlLenderId.SelectedIndex = 0
        End If
        If (Not row(0)("ServicerId") Is System.DBNull.Value) Then
            'ddlServicerId.SelectedValue = CType(row(0)("ServicerId"), Guid).ToString
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlServicerId, CType(row(0)("ServicerId"), Guid).ToString, row(0)("Servicer"))
        Else
            ddlServicerId.SelectedIndex = 0
        End If
        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            If (Not row(0)("AwardCode") Is System.DBNull.Value) Then
                'ddlServicerId.SelectedValue = CType(row(0)("ServicerId"), Guid).ToString
                ddlAwardCode.SelectedValue = row(0)("AwardCode").ToString
            Else
                ddlAwardCode.SelectedIndex = 0
            End If
            If (Not row(0)("AwardSubCode") Is System.DBNull.Value) Then
                'ddlServicerId.SelectedValue = CType(row(0)("ServicerId"), Guid).ToString
                ddlAwardSubCode.SelectedValue = row(0)("AwardSubCode").ToString
            Else
                ddlAwardSubCode.SelectedIndex = 0
            End If
        End If
        If (Not row(0)("GuarantorId") Is System.DBNull.Value) Then
            'ddlGuarantorId.SelectedValue = CType(row(0)("GuarantorId"), Guid).ToString
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGuarantorId, CType(row(0)("GuarantorId"), Guid).ToString, row(0)("Guarantor"))
        Else
            ddlGuarantorId.SelectedIndex = 0
        End If
        txtGrossAmount.Text = CType(row(0)("GrossAmount"), Decimal).ToString("#0.00")
        txtLoanFees.Text = CType(row(0)("LoanFees"), Decimal).ToString("#0.00")
        txtNetLoanAmount.Text = (CType(row(0)("GrossAmount"), Decimal) - CType(row(0)("LoanFees"), Decimal)).ToString("#0.00")
        txtDisbursements.Text = row(0)("Disbursements")
        If (Not row(0)("ModUser") Is Nothing) Then
            txtModUser.Text = row(0)("ModUser").ToString()
        End If

        txtModDate.Text = row(0)("ModDate").ToString

        If (Not row(0)("FA_ID") Is System.DBNull.Value) Then
            txtFAID.Text = row(0)("FA_ID").ToString
        Else
            txtFAID.Text = ""
        End If


        'Do the calculation again for ESP Data 
        ' txtNetLoanAmount.Text = txtGrossAmount.Text - txtLoanFees.Text

        '   Bind DataGrid
        dgrdDisbursements.EditItemIndex = -1
        BindDataGrid()

        '   show datagrid
        dgrdDisbursements.Visible = True

        '   show footer
        dgrdDisbursements.ShowFooter = True

        '   disable Disbursement link button
        lnkDisbursements.Enabled = False

        '   initialize buttons
        InitButtonsForEdit()

        '   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentAwards, e.CommandArgument, ViewState, Header1)

      

        ''   get the StudentAwardId from the backend and display it.
        'GetStudentAwardId(e.CommandArgument)

        ''   set Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentAwards, e.CommandArgument, ViewState, Header1)

        ''   initialize buttons
        'InitButtonsForEdit()

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Save Button Is Clicked
        'SDF Code Starts Here

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsEditSingleCell(pnlSDF, resourceId, strId, moduleid)

        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        CommonWebUtilities.RestoreItemValues(dlstStudentAwards, strId)
        If (ViewState("MODE") = "EDIT" AndAlso AfaIntegrationEnabled) Then
            RunTitleIVAwardEditRules(CType(row(0)("AwardTypeId"), Guid))
        End If

    End Sub
    Private Sub BindStudentAwardData(ByVal StudentAward As StuAwardInfo)
        With StudentAward
            chkIsInDB.Checked = .IsInDB
            txtStudentAwardId.Text = .StudentAwardId
            'txtStuEnrollId.Text = .StuEnrollId
            'ddlAwardTypeId.SelectedValue = .AwardTypeId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAwardTypeId, .AwardTypeId, .AwardType)
            If .AwardStartDate = "" Then
                txtAwardStartDate.SelectedDate = Nothing
            Else
                txtAwardStartDate.SelectedDate = .AwardStartDate
            End If
            '.ToShortDateString
            If .AwardEndDate = "" Then
                txtAwardEndDate.SelectedDate = Nothing
            Else
                txtAwardEndDate.SelectedDate = .AwardEndDate
            End If
            '.ToShortDateString
            'ddlAcademicYearId.SelectedValue = .AcademicYearId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlAcademicYearId, .AcademicYearId, .AcademicYear)
            txtLoanId.Text = .LoanId
            'ddlLenderId.SelectedValue = .LenderId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlLenderId, .LenderId, .Lender)
            'ddlServicerId.SelectedValue = .ServicerId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlServicerId, .ServicerId, .Servicer)
            'ddlGuarantorId.SelectedValue = .GuarantorId
            CommonWebUtilities.SetSelectedValueInAdvantageDDL(ddlGuarantorId, .GuarantorId, .Guarantor)
            txtGrossAmount.Text = .GrossAmount.ToString("#0.00")
            txtLoanFees.Text = .LoanFees.ToString("#0.00")
            txtNetLoanAmount.Text = .NetLoanAmount.ToString("#0.00")
            txtDisbursements.Text = .Disbursements.ToString
            txtModUser.Text = .ModUser
            txtModDate.Text = .ModDate.ToString
            txtFAID.Text = .FAID
        End With
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'New Code Added By Vijay Ramteke On June 09, 2010
        Dim StuAccFacade As New StudentsAccountsFacade
        Dim CutoffDate As String
        CutoffDate = StuAccFacade.GetManualPostingCutoffDate(ddlAwardTypeId.SelectedValue.ToString)
        If Not CutoffDate = "" Then
            If Date.Parse(txtAwardStartDate.SelectedDate) > Date.Parse(CutoffDate) Then
                DisplayErrorMessage("Award cannot be saved as Award Start Date is greater than the Manual Posting Cutoff Date.")
                Return
            End If
        End If
        'New Code Added By Vijay Ramteke On June 09, 2010
        '   validate that the number of rows in Disbursement grid equals the Number of Disbursements.
        If Not ValidateNumberOfDisbursements() Then
            '   Display Error Message
            DisplayErrorMessage("Unable to save. The number of rows must equal the Number of Disbursements.")
            Exit Sub
        End If

        '   validate that the sum of Projected Amounts equals the Net Loan Amount.
        If Not ValidateDisbursementAmounts() Then
            '   Display Error Message
            DisplayErrorMessage("Unable to save. The total Projected Amount must equal Net Loan Amount.")
            Exit Sub
        End If

        '   if any of the footer fields of the datagrid is not blank.. send an error message 
        If Not AreFooterFieldsBlank() Then
            '   Display Error Message
            DisplayErrorMessage("To use ""Save"" all fields in the Disbursement footer must be blank.")
            Exit Sub
        End If
        Dim studentAwardId As String = ""

        Try
            If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
                Dim sAwardMessage As String = ""
                If ddlAwardCode.SelectedValue = "" Then
                    sAwardMessage = "Award Code is required for regent" & vbCrLf
                End If
                If ddlAwardSubCode.SelectedValue = "" Then
                    sAwardMessage &= "Award Sub Code is required for regent" & vbCrLf
                End If
                If Not sAwardMessage = "" Then
                    DisplayErrorMessage(sAwardMessage)
                    Exit Sub
                End If

            End If
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try
        Try
            If (ViewState("MODE") = "NEW" AndAlso AfaIntegrationEnabled) Then
                Dim fundSourceGuid = Guid.Parse(ddlAwardTypeId.SelectedValue)
                Dim isTitleIVFundSource = New FundSourcesDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString).IsFundSourceTitleIV(fundSourceGuid)

                If (isTitleIVFundSource) Then
                    If (Not AdvantageSession.UserState.IsUserSupport) Then
                        DisplayErrorMessage("Title IV awards cannot be created when the AFA Integration is enabled. Title IV Awards must be created in AFA.")
                        Exit Sub
                    End If
                End If
            End If

        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)


        End Try
        '   if StudentAwardId does not exist do an insert else do an update
        If Session("StudentAwardId") Is Nothing Then
            '   do an insert
            studentAwardId = Guid.NewGuid.ToString
            BuildNewRowInDataList(studentAwardId)


        Else
            '   do an update
            '   get the row with the data to be displayed
            studentAwardId = CType(Session("StudentAwardId"), String)
            Dim row() As DataRow = dataListTable.Select("StudentAwardId=" + "'" + CType(Session("StudentAwardId"), String) + "'")

            '   update row data
            UpdateRowData(row(0))

        End If

        '   try to update the DB and show any error message
        UpdateDB(studentAwardId)

        '   if there were no errors then bind and set selected style to the datalist and
        '   initialize buttons 
        If Customvalidator1.IsValid Then

            '   bind datalist
            BindDataList()

            'dlstStuAwards.DataSource = New DataView(dataListTable, Nothing, "StudentAwardDescrip asc", DataViewRowState.CurrentRows)
            'dlstStuAwards.DataBind()

            ''   set Style to Selected Item
            'CommonWebUtilities.SetStyleToSelectedItem(dlstStuAwards, Session("StudentAwardId"), ViewState, Header1)

            '   initialize buttons
            InitButtonsForEdit()

            '   disable Disbursement link button
            lnkDisbursements.Enabled = False

            CommonWebUtilities.RestoreItemValues(dlstStudentAwards, Session("StudentAwardId"))  'txtStudentAwardId.Text

        End If


        ''Dim result As String
        ''Dim resInfo As ResultInfo

        '   instantiate component
        ''With New StuAwardsFacade
        ''    '   update StudentAward Info 
        ''    resInfo = .UpdateStuAwardInfo(BuildStuAwardInfo(txtStudentAwardId.Text), Session("UserId"))    'Session("UserName")
        ''End With

        '   bind datalist
        ''BindDataList(ddlStuEnrollId.SelectedValue)

        '   set Style to Selected Item
        ''CommonWebUtilities.SetStyleToSelectedItem(dlstStudentAwards, txtStudentAwardId.Text, ViewState, Header1)

        ''If resInfo.ErrorString <> "" Then
        ''    '   Display Error Message
        ''    DisplayErrorMessage(resInfo.ErrorString)
        ''Else
        ''    '   get the StudentAwardId and display it. Do not need to go to backend again!
        ''    BindStudentAwardData(resInfo.UpdatedObject)
        ''End If

        '   if there are no errors bind a new entity and init buttons
        ''If Page.IsValid Then
        ''    '   set the property IsInDB to true in order to avoid an error if the user
        ''    '   hits "save" twice after adding a record.
        ''    chkIsInDB.Checked = True

        ''    'note: in order to display a new page after "save".. uncomment next lines
        ''    '   bind an empty new StuAwardInfo
        ''    'BindStudentAwardData(New StuAwardInfo)

        ''    '   initialize buttons
        ''    'InitButtonsForLoad()
        ''    InitButtonsForEdit()
        ''End If

        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields When Save Button Is Clicked

        Dim SDFID As ArrayList
        Dim SDFIDValue As ArrayList
        '    Dim newArr As ArrayList
        Dim z As Integer
        Dim SDFControls As New SDFComponent
        'Try
        '    'SDFControl.DeleteSDFValue(txtStudentAwardId.Text)
        '    SDFID = SDFControl.GetAllLabels(pnlSDF)
        '    SDFIDValue = SDFControl.GetAllValues(pnlSDF)
        '    For z = 0 To SDFID.Count - 1
        '        SDFControl.InsertValues(txtStudentAwardId.Text, Mid(SDFID(z).id, 5), SDFIDValue(z))
        '    Next
        'Catch ex As System.Exception
        '	Dim exTracker = new AdvApplicationInsightsInitializer()
        '	exTracker.TrackExceptionWrapper(ex)

        '    DisplayErrorMessage(ex.Message)
        '    Exit Sub
        'End Try

        Dim strResult As String = ""
        Dim strResultMessage As String = ""
        Try
            SDFID = SDFControls.GetAllLabels(pnlSDF)
            SDFIDValue = SDFControls.GetAllValues(pnlSDF)
            For z = 0 To SDFID.Count - 1
                strResult = SDFControls.InsertValues(studentAwardId, Mid(SDFID(z).id, 5), SDFIDValue(z))
                If Not strResult = "" Then
                    strResultMessage &= strResult & vbLf
                End If
            Next
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

            DisplayErrorMessage(ex.Message)
            Exit Sub
        End Try
        If Not strResultMessage = "" Then
            DisplayErrorMessage(strResultMessage)
            Exit Sub
        End If

        'SchoolDefined Fields Code Ends Here 
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''



    End Sub

    Private Function BuildStuAwardInfo(ByVal StudentAwardId As String) As StuAwardInfo
        'instantiate class
        Dim StuAwardInfo As New StuAwardInfo

        With StuAwardInfo
            'get IsInDB
            .IsInDB = chkIsInDB.Checked

            'get StudentAwardId
            .StudentAwardId = StudentAwardId

            'stuEnrollId
            .StuEnrollId = ddlStuEnrollId.SelectedValue

            'AwardTypeId
            .AwardTypeId = ddlAwardTypeId.SelectedValue

            'AwardStartDate
            .AwardStartDate = Date.Parse(txtAwardStartDate.SelectedDate)

            'AwardEndDate
            .AwardEndDate = Date.Parse(txtAwardEndDate.SelectedDate)

            'AcademicYearId
            .AcademicYearId = ddlAcademicYearId.SelectedValue

            'LoanId
            .LoanId = txtLoanId.Text

            'LenderId
            .LenderId = ddlLenderId.SelectedValue

            'ServicerId
            .ServicerId = ddlServicerId.SelectedValue

            'GuarantorId
            .GuarantorId = ddlGuarantorId.SelectedValue

            'GrossAmount
            .GrossAmount = txtGrossAmount.Text

            'LoanFees
            .LoanFees = txtLoanFees.Text

            'NetLoanAmount
            .NetLoanAmount = txtNetLoanAmount.Text

            'Disbursements
            .Disbursements = txtDisbursements.Text

            'ModUser
            .ModUser = txtModUser.Text

            'ModDate
            .ModDate = Date.Parse(txtModDate.Text)

            'FAID
            .FAID = txtFAID.Text
        End With

        'return data
        Return StuAwardInfo
    End Function

    Private Sub RebindFundSourceData()
        Dim stuAccounts As New StudentsAccountsFacade

        ddlAwardTypeId.Items.Clear()
        With ddlAwardTypeId
            .DataTextField = "FundSourceDescrip"
            .DataValueField = "FundSourceId"
            .DataSource = stuAccounts.GetAllFundSourcesX("True", campusId, AfaIntegrationEnabled AndAlso ViewState("MODE") = "NEW")
            .DataBind()
            .Items.Insert(0, New ListItem("Select", Guid.Empty.ToString))
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ViewState("MODE") = "NEW"

        '   reset dataset to previous state. delete all changes
        allDataset.RejectChanges()
        PrepareForNewData()

        '   hide datagrid
        dgrdDisbursements.Visible = False

        '   enable Disbursement link button
        'lnkDisbursements.Enabled = True

        '   initialize buttons
        'InitButtonsForWork()
        InitButtonsForLoad()

        '   reset Style to Selected Item
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentAwards, Guid.Empty.ToString, ViewState, Header1)


        ''   bind an empty new StuAwardInfo
        'BindStudentAwardData(New StuAwardInfo(studentId))

        ''Reset Style in the Datalist
        'CommonWebUtilities.SetStyleToSelectedItem(dlstStudentAwards, Guid.Empty.ToString, ViewState, Header1)

        ''initialize buttons
        'InitButtonsForWork()

        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNewSingleCell(pnlSDF, resourceId, moduleid)
        CommonWebUtilities.RestoreItemValues(dlstStudentAwards, Guid.Empty.ToString)

        If (AfaIntegrationEnabled) Then
            RebindFundSourceData()
            EnableFields()
            btnSave.Visible = True
            btnDelete.Visible = True
        End If
    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        '   get all rows to be deleted
        Dim rows() As DataRow = dataGridTable.Select("StudentAwardId=" + "'" + CType(Session("StudentAwardId"), String) + "'")

        'Check if there exists data in saPmtDisbRel Table before deleting from faAwardSchedule Table
        Dim strReturn As String = ""
        strReturn = (New StuAwardsFacade).CheckIfDataExistsInPmtDisbRel(CType(Session("StudentAwardId"), String))
        If Not strReturn = "" Then
            DisplayErrorMessage(strReturn)
            Exit Sub
        End If


        '   delete all rows from dataGrid table
        Dim i As Integer
        If rows.Length > 0 Then
            For i = 0 To rows.Length - 1
                rows(i).Delete()
            Next
        End If

        '   get the row to be deleted in StuAwards table
        Dim row() As DataRow = dataListTable.Select("StudentAwardId=" + "'" + CType(Session("StudentAwardId"), String) + "'")

        '   delete row from the table
        Try
            row(0).Delete()
        Catch ex As System.Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)

        End Try

        '   try to update the DB and show any error message
        UpdateDB()

        '   if there were no errors prepare screen for new data
        If Customvalidator1.IsValid Then
            '   Prepare screen for new data
            PrepareForNewData()

            '   Hide Disbursements grid
            dgrdDisbursements.Visible = False
            InitButtonsForLoad()

            ViewState("MODE") = "NEW"
        End If



        '   initialize buttons
        'InitButtonsForWork()


        ''If Not (txtStudentAwardId.Text = Guid.Empty.ToString) Then
        ''    'update StudentAward Info 
        ''    Dim result As String = (New StuAwardsFacade).DeleteStuAwardInfo(txtStudentAwardId.Text, Date.Parse(txtModDate.Text))
        ''    If Not result = "" Then
        ''        '   Display Error Message
        ''        DisplayErrorMessage(result)
        ''        Exit Sub
        ''    Else
        ''        '   bind datalist
        ''        BindDataList(ddlStuEnrollId.SelectedValue)

        ''        '   bind an empty new StuAwardInfo
        ''        BindStudentAwardData(New StuAwardInfo(studentId))

        ''        '   initialize buttons
        ''        InitButtonsForWork()
        ''    End If
        ''End If
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Code For SchoolDefined Fields(SDF) When Delete Button Is Clicked
        'SDF Code Starts Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Dim SDFControl As New SDFComponent
        'SDFControl.DeleteSDFValue(txtStudentAwardId.Text)
        SDFControl.GenerateControlsNewSingleCell(pnlSDF, resourceId, moduleid)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'SDF Code Ends Here
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        CommonWebUtilities.RestoreItemValues(dlstStudentAwards, Guid.Empty.ToString)

        If (AfaIntegrationEnabled) Then
            EnableFields()
        End If
    End Sub

    Private Sub InitButtonsForLoad()
        btnSave.Enabled = False

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
            lnkDisbursements.Enabled = True
        Else
            btnNew.Enabled = False
            lnkDisbursements.Enabled = False
        End If

        btnDelete.Enabled = False

        'btnNew.Enabled = False
        'lnkDisbursements.Enabled = False
    End Sub

    Private Sub InitButtonsForEdit()
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If
        
    End Sub

    Private Sub InitButtonsForWork()
        If pObj.HasFull Or pObj.HasEdit Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

        'btnNew.Enabled = False
        If pObj.HasFull Or pObj.HasAdd Then
            btnNew.Enabled = True
        Else
            btnNew.Enabled = False
        End If

        'btnDelete.Enabled = False
        If pObj.HasFull Or pObj.HasDelete Then
            btnDelete.Enabled = True
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")
        Else
            btnDelete.Enabled = False
        End If

        lnkDisbursements.Enabled = True

    End Sub


    Private Sub DisplayErrorMessage(ByVal errorMessage As String)
        '   Set error condition
        Customvalidator1.ErrorMessage = errorMessage
        Customvalidator1.IsValid = False

        If Validationsummary1.ShowMessageBox = True And Validationsummary1.ShowSummary = False And Customvalidator1.Display = ValidatorDisplay.None Then
            '   Display error in message box in the client
            CommonWebUtilities.DisplayErrorInMessageBox(Me.Page, errorMessage)
        End If
    End Sub

    Private Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        ProcessEnrollmentSelected()
        Dim SDFControls As New SDFComponent
        SDFControls.GenerateControlsNewSingleCell(pnlSDF, resourceId, moduleid)
    End Sub

    Private Sub txtGrossAmount_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGrossAmount.TextChanged
        If txtGrossAmount.Text <> "" Then
            txtGrossAmount.Text = Decimal.Parse(txtGrossAmount.Text).ToString("#0.00")
        Else
            txtGrossAmount.Text = Decimal.Parse(0.0).ToString("#0.00")
        End If

        If txtGrossAmount.Text <> "" And txtLoanFees.Text <> "" Then
            txtNetLoanAmount.Text = CType(Decimal.Parse(txtGrossAmount.Text) - Decimal.Parse(txtLoanFees.Text), Decimal).ToString("#0.00")
        End If

        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtLoanFees)
    End Sub

    Private Sub txtLoanFees_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLoanFees.TextChanged
        If txtLoanFees.Text <> "" Then
            txtLoanFees.Text = Decimal.Parse(txtLoanFees.Text).ToString("#0.00")
        Else
            txtLoanFees.Text = Decimal.Parse(0.0).ToString("#0.00")
        End If

        If txtGrossAmount.Text <> "" And txtLoanFees.Text <> "" Then
            txtNetLoanAmount.Text = CType(Decimal.Parse(txtGrossAmount.Text) - Decimal.Parse(txtLoanFees.Text), Decimal).ToString("#0.00")
        End If

        Dim objCommon As New CommonWebUtilities
        CommonWebUtilities.SetFocus(Me.Page, txtDisbursements)
    End Sub

    Private Function AreRequiredFooterFieldsBlank() As Boolean
        Dim footer As Control = dgrdDisbursements.Controls(0).Controls(dgrdDisbursements.Controls(0).Controls.Count - 1)
        If Not CType(footer.FindControl("txtFooterExpectedDate"), TextBox).Text = "" And
            Not CType(footer.FindControl("txtFooterAmount"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Function AreFooterFieldsBlank() As Boolean
        Dim footer As Control = dgrdDisbursements.Controls(0).Controls(dgrdDisbursements.Controls(0).Controls.Count - 1)
        If Not CType(footer.FindControl("txtFooterExpectedDate"), TextBox).Text = "" Then Return False
        If Not CType(footer.FindControl("txtFooterReference"), TextBox).Text = "" Then Return False
        If Not CType(footer.FindControl("txtFooterAmount"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Function ValidateDisbursementAmounts() As Boolean
        '   validate that the sum of Projected Amounts equals the Net Loan Amount.
        Dim objAmount As Object = dataGridTable.Compute("SUM(Amount)", "StudentAwardId = '" & CType(Session("StudentAwardId"), String) & "'")

        If Not objAmount Is Nothing Then
            If Convert.ToDecimal(objAmount) <> Decimal.Parse(txtNetLoanAmount.Text) Then
                Return False
            End If
        Else
            Return False
        End If

        Return True
    End Function

    Private Function ValidateNumberOfDisbursements() As Boolean
        '   validate that the number of rows in grid equals the Number of Disbursements.
        Dim gridRows() As DataRow = dataGridTable.Select("StudentAwardId = '" & CType(Session("StudentAwardId"), String) & "'")

        If gridRows.Length <> Integer.Parse(txtDisbursements.Text) Then
            Return False
        End If

        Return True
    End Function

    Private Sub BuildNewRowInDataList(ByVal theGuid As String)
        '   get a new row
        Dim newRow As DataRow = dataListTable.NewRow

        '   create a Guid for DataList if it has not been created 
        Session("StudentAwardId") = theGuid
        newRow("StudentAwardId") = New Guid(theGuid)

        '   update row with web controls data
        UpdateRowData(newRow)

        '   add row to the table
        newRow.Table.Rows.Add(newRow)

    End Sub

    Private Sub UpdateRowData(ByVal row As DataRow)
        '   update row data
        'row("StudentAwardId") = New Guid(CType(Session("StudentAwardId"), String))
        row("StuEnrollId") = New Guid(ddlStuEnrollId.SelectedValue)
        row("AwardTypeId") = New Guid(ddlAwardTypeId.SelectedValue)
        row("AwardStartDate") = Date.Parse(txtAwardStartDate.SelectedDate)
        row("AwardEndDate") = Date.Parse(txtAwardEndDate.SelectedDate)
        row("AcademicYearId") = New Guid(ddlAcademicYearId.SelectedValue)
        row("LoanId") = txtLoanId.Text
        If Not ddlLenderId.SelectedValue = System.Guid.Empty.ToString Then
            row("LenderId") = New Guid(ddlLenderId.SelectedValue)
        Else
            row("LenderId") = System.DBNull.Value
        End If
        If Not ddlServicerId.SelectedValue = System.Guid.Empty.ToString Then
            row("ServicerId") = New Guid(ddlServicerId.SelectedValue)
        Else
            row("ServicerId") = System.DBNull.Value
        End If
        If Not ddlGuarantorId.SelectedValue = System.Guid.Empty.ToString Then
            row("GuarantorId") = New Guid(ddlGuarantorId.SelectedValue)
        Else
            row("GuarantorId") = System.DBNull.Value
        End If
        row("GrossAmount") = Decimal.Parse(txtGrossAmount.Text)
        row("LoanFees") = Decimal.Parse(txtLoanFees.Text)
        row("Disbursements") = Integer.Parse(txtDisbursements.Text)
        row("ModUser") = Session("UserName") 'Session("UserId")
        row("ModDate") = Date.Parse(Date.Now.ToString)
        row("FA_ID") = txtFAID.Text
        If MyAdvAppSettings.AppSettings("Regent").ToString.Trim.ToLower = "yes" Then
            Try
                row("AwardCode") = ddlAwardCode.SelectedValue
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                row("AwardCode") = System.DBNull.Value
            End Try
            Try
                row("AwardSubCode") = ddlAwardSubCode.SelectedValue
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                row("AwardSubCode") = System.DBNull.Value
            End Try
            Try
                row("AwardStatus") = ddlAwardStatus.SelectedValue
            Catch ex As System.Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                row("AwardStatus") = System.DBNull.Value
            End Try
        End If
    End Sub
    Private Sub UpdateDB(Optional ByVal StudentAwardId As String = "")
        '   update DB
        With New StuAwardsFacade
            Dim result As String = .UpdateStuAwardsDS(allDataset, StudentAwardId, studentId)
            If Not result = "" Then
                '   Display Error Message
                DisplayErrorMessage(result)
            Else
                '   get a new version of StudentAwards dataset because some other users may have added,
                '   updated or deleted records from the DB
                allDataset = .GetStuAwardsDS(studentId)
                Session("AllDataset") = allDataset
                CreateDatasetAndTables()
            End If
        End With
    End Sub

    Private Sub dgrdDisbursements_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgrdDisbursements.ItemCommand
        '   process postbacks from the datagrid
        Select Case e.CommandName

            '   user hit "Edit"
            Case "Edit"
                '   edit selected item
                dgrdDisbursements.EditItemIndex = e.Item.ItemIndex

                '    do not show footer
                dgrdDisbursements.ShowFooter = False

                '   user hit "Update" inside the datagrid
            Case "Update"
                Dim isValid As Byte = IsDataInEditTextboxesValid(e)
                Dim isBlank As Boolean = AreEditFieldsBlank(e)
                '   process only if the edit textboxes are nonblank and valid
                If isValid = 1 And Not isBlank Then
                    '   get the row to be updated
                    Dim row() As DataRow = dataGridTable.Select("AwardScheduleId=" + "'" + New Guid(CType(e.Item.FindControl("lblEditAwardScheduleId"), Label).Text).ToString + "'")
                    '   fill new values in the row
                    If Not CType(e.Item.FindControl("txtEditExpectedDate"), TextBox).Text = "" Then row(0)("ExpectedDate") = Date.Parse(CType(e.Item.FindControl("txtEditExpectedDate"), TextBox).Text)
                    row(0)("Reference") = CType(e.Item.FindControl("txtEditReference"), TextBox).Text
                    If Not CType(e.Item.FindControl("txtEditAmount"), TextBox).Text = "" Then row(0)("Amount") = Decimal.Parse(CType(e.Item.FindControl("txtEditAmount"), TextBox).Text)
                    '   no record is selected
                    dgrdDisbursements.EditItemIndex = -1

                    '   show footer
                    dgrdDisbursements.ShowFooter = True

                ElseIf isValid = 2 Then
                    DisplayErrorMessage("Invalid data in Scheduled Date during the edit of an existing row of Disbursements.")
                    Exit Sub

                ElseIf isValid = 3 Then
                    DisplayErrorMessage("Invalid data in Projected Amount during the edit of an existing row of Disbursements.")
                    Exit Sub

                ElseIf isBlank Then
                    DisplayErrorMessage("Scheduled Date and Projected Amount are required.")
                    Exit Sub
                End If


                '   user hit "Cancel"
            Case "Cancel"
                '   set no record selected
                dgrdDisbursements.EditItemIndex = -1

                '   show footer
                dgrdDisbursements.ShowFooter = True


                '   user hit "Delete" inside the datagrid
            Case "Delete"
                '   get the row to be deleted
                Dim row() As DataRow = dataGridTable.Select("AwardScheduleId=" + "'" + CType(e.Item.FindControl("lblEditAwardScheduleId"), Label).Text + "'")

                '   delete row 
                row(0).Delete()

                '   set no record selected
                dgrdDisbursements.EditItemIndex = -1

                '   show footer
                dgrdDisbursements.ShowFooter = True


            Case "AddNewRow"
                Dim isValid As Byte = IsDataInFooterTextboxesValid(e)
                Dim isBlank As Boolean = AreRequiredFooterFieldsBlank()
                '   process only if the footer textboxes are nonblank and valid
                If isValid = 1 And Not isBlank Then
                    '   If the dataList item doesn't have an Id assigned.. create one and assign it.
                    If Session("StudentAwardId") Is Nothing Then
                        '   build a new row in dataList table
                        BuildNewRowInDataList(Guid.NewGuid.ToString)
                        '   bind datalist
                        BindDataList()
                    End If

                    '   get a new row from the dataGridTable
                    Dim newRow As DataRow = dataGridTable.NewRow

                    '   fill the new row with values
                    newRow("AwardScheduleId") = Guid.NewGuid
                    newRow("StudentAwardId") = New Guid(CType(Session("StudentAwardId"), String))
                    If Not CType(e.Item.FindControl("txtFooterExpectedDate"), TextBox).Text = "" Then newRow("ExpectedDate") = Date.Parse(CType(e.Item.FindControl("txtFooterExpectedDate"), TextBox).Text)
                    If Not CType(e.Item.FindControl("txtFooterReference"), TextBox).Text = "" Then newRow("Reference") = CType(e.Item.FindControl("txtFooterReference"), TextBox).Text
                    If Not CType(e.Item.FindControl("txtFooterAmount"), TextBox).Text = "" Then newRow("Amount") = Decimal.Parse(CType(e.Item.FindControl("txtFooterAmount"), TextBox).Text)

                    '   add row to the table
                    newRow.Table.Rows.Add(newRow)

                ElseIf isValid = 2 Then
                    DisplayErrorMessage("Invalid data in Scheduled Date during adding of a new row of Disbursements.")
                    Exit Sub

                ElseIf isValid = 3 Then
                    DisplayErrorMessage("Invalid data in Projected Amount during adding of a new row of Disbursements.")
                    Exit Sub

                ElseIf isBlank Then
                    DisplayErrorMessage("Scheduled Date and Projected Amount are required.")
                    Exit Sub
                End If
        End Select

        '   Bind DataGrid
        BindDataGrid()
    End Sub

    Private Sub BindDataGrid()
        '   bind datagrid to dataGridTable
        ''Code Added by Kamalesh Ahuja on 15 Jan 2010 to Resolve Mantis Issue Id 12038 and 08425
        Dim TempDV As DataView
        Dim StrSql As String
        Try
            If Not dataGridTable.Columns.Contains("RecDate") Then
                dataGridTable.Columns.Add("RecDate", System.Type.GetType("System.String"))
            End If
        Catch ex As Exception
            Dim exTracker = New AdvApplicationInsightsInitializer()
            exTracker.TrackExceptionWrapper(ex)
        End Try


        TempDV = New DataView(dataGridTable, "StudentAwardId=" + "'" + CType(Session("StudentAwardId"), String) + "'", "ExpectedDate ASC", DataViewRowState.CurrentRows)


        For Each Dr As DataRowView In TempDV
            Try
                If CDbl(Dr("Received")) <> 0 Then
                    Dim strRD As String = ""
                    StrSql = "select TransDate from saTransactions T, saPmtDisbRel PDR where T.TransactionId=PDR.TransactionId And T.voided=0 and PDR.AwardScheduleId='" + Dr("AwardScheduleId").ToString + "'"
                    Dim dsRD As New DataSet
                    Dim db As New DataAccess
                    dsRD = db.RunSQLDataSet(StrSql)
                    For Each drRD As DataRow In dsRD.Tables(0).Rows
                        strRD += CDate(drRD("TransDate").ToString).ToString("MM/dd/yyyy") + ","
                    Next
                    If (strRD + "" <> "") Then
                        strRD = strRD.Substring(0, strRD.LastIndexOf(","))
                    End If
                    Dr("RecDate") = strRD
                    strRD = ""
                    If CDbl(Dr("Received")) < 0 Then
                        Dr("Received") = CDbl(Dr("Received")) * -1
                    End If
                End If
            Catch ex As Exception
                Dim exTracker = New AdvApplicationInsightsInitializer()
                exTracker.TrackExceptionWrapper(ex)

                Dr("Received") = CDbl("0.00")
            End Try
        Next
        
        dgrdDisbursements.DataSource = TempDV
        dgrdDisbursements.DataBind()
        ''''''''''''''
        
        txtDisbursements.Text = NumberOfPayments.ToString("##0")
    End Sub

    Private Function AreEditFieldsBlank(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Boolean
        If Not CType(e.Item.FindControl("txtEditExpectedDate"), TextBox).Text = "" And
            Not CType(e.Item.FindControl("txtEditAmount"), TextBox).Text = "" Then Return False
        Return True
    End Function

    Private Function IsDataInFooterTextboxesValid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Byte
        If Not IsTextBoxValidDate(e.Item.FindControl("txtFooterExpectedDate")) Then Return 2
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtFooterAmount")) Then Return 3
        Return 1
    End Function
    Private Function IsTextBoxValidDecimal(ByVal textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True
        ' Try
        Dim d As Decimal
        Return Decimal.TryParse(textbox.Text, d)
    End Function

    Private Function IsTextBoxValidDate(ByVal textbox As TextBox) As Boolean
        If textbox.Text = "" Then Return True
        'Try
        Dim d As Date
        Return Date.TryParse(textbox.Text, d)
    End Function

    Private Function IsDataInEditTextboxesValid(ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) As Byte
        If Not IsTextBoxValidDate(e.Item.FindControl("txtEditExpectedDate")) Then Return 2
        If Not IsTextBoxValidDecimal(e.Item.FindControl("txtEditAmount")) Then Return 3
        Return 1
    End Function

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        '   if there are changes to the dataset we must confirm that the user is aware that all changes will be lost.
        If allDataset.HasChanges() Then
            'Set the Delete Button so it prompts the user for confirmation when clicked
            btnNew.Attributes.Add("onclick", "if(confirm('All changes made to this record will be lost.  Are you sure you want a New record?')){}else{return false}")
        Else
            btnNew.Attributes.Remove("onclick")
        End If

        '   save dataset systems in session
        Session("AllDataset") = allDataset
        'add to this list any button or link that should ignore the Confirm Exit Warning.
        Dim controlsToIgnore As New ArrayList()
        'add buttons 
        controlsToIgnore.Add(btnSave)
        controlsToIgnore.Add(btnBuildList)
        controlsToIgnore.Add(lnkDisbursements)
        controlsToIgnore.Add(txtGrossAmount)
        'Add javascript code to warn the user about non saved changes 
        CommonWebUtilities.AddClientCodeToConfirmExitAfterNonSavedChanges(Me.Page, controlsToIgnore)
    End Sub

    Private Sub lnkDisbursements_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDisbursements.Click
        Dim newRow As DataRow = Nothing
        Dim i As Integer
        Dim accum As Decimal
        Dim projectedAmt As Decimal
        Dim disb As Integer = Integer.Parse(txtDisbursements.Text)
        Dim daysAdj As Integer = MyAdvAppSettings.AppSettings("DaysAdjustmentForReceiptofFADisbursements")

        dataGridTable.Rows.Clear()
        'For Each row As DataRow In dataGridTable.Rows
        '    'Mark all rows as deleted but do not remove them from the datatable yet.
        '    row.Delete()
        'Next

        If disb = 0 Then
            DisplayErrorMessage("Number of Disbursements must be greater than zero.")
            Exit Sub
        Else
            projectedAmt = Decimal.Round(Decimal.Parse(txtNetLoanAmount.Text) / disb, 2)
        End If

        '   if StudentAwardId does not exist do an insert else do an update
        If Session("StudentAwardId") Is Nothing Then
            '   do an insert
            BuildNewRowInDataList(Guid.NewGuid.ToString)

        Else
            '   do an update
            '   get the row with the data to be displayed
            Dim row() As DataRow = dataListTable.Select("StudentAwardId=" + "'" + CType(Session("StudentAwardId"), String) + "'")

            '   update row data
            UpdateRowData(row(0))

        End If

        Dim t1 As DateTime = Convert.ToDateTime(txtAwardEndDate.SelectedDate)
        Dim t2 As DateTime = Convert.ToDateTime(txtAwardStartDate.SelectedDate).AddDays(daysAdj)
        Dim tmSpan As TimeSpan = New TimeSpan(t1.Subtract(t2).Ticks)

        '   get time span and use it to compute disbursement's ExpectedDate.
        Dim nSpan As Integer = tmSpan.TotalDays / disb

        For i = 0 To disb - 1
            newRow = dataGridTable.NewRow
            newRow("AwardScheduleId") = Guid.NewGuid
            If Not CType(Session("StudentAwardId"), String) = System.Guid.Empty.ToString Then
                newRow("StudentAwardId") = New Guid(CType(Session("StudentAwardId"), String))
            End If
            newRow("Amount") = projectedAmt
            accum += projectedAmt
            newRow("ExpectedDate") = t2
            t2 = t2.Add(New TimeSpan(nSpan, 0, 0, 0))
            newRow("Reference") = ""        '"Award Disbursement # " & (i + 1)
            '   add row to the table
            newRow.Table.Rows.Add(newRow)
        Next

        '   ensure that sum of projectedAmt equals Net Loan Amount.
        If accum <> Decimal.Parse(txtNetLoanAmount.Text) Then
            newRow("Amount") += Decimal.Parse(txtNetLoanAmount.Text) - accum
        End If

        '   Bind DataGrid
        BindDataGrid()

        '   show datagrid
        dgrdDisbursements.Visible = True

        InitButtonsForWork()
    End Sub

    Private Sub ProcessEnrollmentSelected()
        If ddlStuEnrollId.SelectedValue <> System.Guid.Empty.ToString Then

            '   clear out session variable
            Session("StudentAwardId") = Nothing

            '   bind datalist
            BindDataList(ddlStuEnrollId.SelectedValue)

            '   bind an empty new StuAwardInfo
            BindStudentAwardData(New StuAwardInfo)

            '   Hide Disbursements grid
            dgrdDisbursements.Visible = False

            '   Initialize buttons
            'InitButtonsForWork()
            InitButtonsForLoad()

        Else
            '   Display Error Message
            DisplayErrorMessage("Please select an Enrollment.")
        End If
    End Sub
    Private Sub dgrdDisbursements_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgrdDisbursements.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim lbl As Label = CType(e.Item.FindControl("lblWasReceived"), Label)
                If lbl.Text = "True" Then
                    If (CType(e.Item.DataItem("RefundAmount"), Decimal) >= CType(e.Item.DataItem("Amount"), Decimal)) Then
                        CType(e.Item.FindControl("lnlButEdit"), LinkButton).Visible = True
                    Else
                        CType(e.Item.FindControl("lnlButEdit"), LinkButton).Visible = False
                    End If
                End If
                If InStr(CType(e.Item.FindControl("lblReference"), Label).Text, "Famelink") >= 1 Then
                    CType(e.Item.FindControl("lnlButEdit"), LinkButton).Visible = False
                End If
                Amount += CType(e.Item.DataItem("Amount"), Decimal)
                NumberOfPayments += 1
            Case ListItemType.EditItem
                Amount += CType(e.Item.DataItem("Amount"), Decimal)
                NumberOfPayments += 1
        End Select
    End Sub
    Protected Sub dlstStudentAwards_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlstStudentAwards.ItemDataBound
        Dim items As DataListItemCollection
        Dim item As DataListItem
        Dim intRowCount As Integer = 0
        Dim lblAwardStartDate As Label
        items = dlstStudentAwards.Items

        For intRowCount = 0 To items.Count - 1
            item = items.Item(intRowCount)
            lblAwardStartDate = CType(item.FindControl("lblAwdStartDate"), Label)
            If InStr(lblAwardStartDate.Text, "12:00:00 AM") >= 1 Or InStr(lblAwardStartDate.Text.ToString.Trim, "12:00:00 AM") >= 1 Or InStr(lblAwardStartDate.Text, "AM") >= 1 Then
                lblAwardStartDate.Text = ""
            End If
        Next
    End Sub

    Public Sub RunTitleIVAwardEditRules(fundSourceId As Guid)
        Dim isTitleIVFundSource = New FundSourcesDA(MyAdvAppSettings.AppSettings("ConnectionString").ToString).IsFundSourceTitleIV(fundSourceId)
        If (isTitleIVFundSource) Then
            DisableFields()
        Else
            EnableFields()
        End If
        btnSave.Visible = Not isTitleIVFundSource
        btnDelete.Visible = Not isTitleIVFundSource
    End Sub
    Public Sub DisableFields()
        'Loop through each Control within your Page'
        'disable main form inputs unless lender, servicer, and guarantor ddls
        For Each control As Control In OldContentPane.Controls
            'Check if it is a TextBox'
            If control.Visible Then
                'If so - cast it properly'
                If TypeOf control Is TextBox Then
                    Dim tb As TextBox = DirectCast(control, TextBox)
                    'Set it to disabled'
                    tb.Enabled = False
                ElseIf TypeOf control Is RadDatePicker Then
                    Dim dp As RadDatePicker = DirectCast(control, RadDatePicker)
                    dp.Enabled = False
                ElseIf TypeOf control Is DropDownList Then
                    Dim ddl As DropDownList = DirectCast(control, DropDownList)
                    'If (Not ddl.ID = ddlLenderId.ID AndAlso Not ddl.ID = ddlServicerId.ID AndAlso Not ddl.ID = ddlGuarantorId.ID) Then
                        ddl.Enabled = False
                    'End If
                End If
            ElseIf TypeOf control Is LinkButton Then
                Dim lb As LinkButton = DirectCast(control, LinkButton)
                lb.Enabled = False
            End If
        Next
        'disable disbursement grid boxes and buttons
        For Each row As DataGridItem In dgrdDisbursements.Items
            If (Not row.ItemType = ListItemType.Footer) Then
                Dim editBtn = CType(row.FindControl("lnlButEdit"), LinkButton)
                If (editBtn IsNot Nothing AndAlso editBtn.Visible) Then
                    editBtn.Enabled = False
                End If
            End If
        Next
        Dim tbl As Table = CType(dgrdDisbursements.Controls(0), Table)
        If (tbl IsNot Nothing) Then
            Dim footer As DataGridItem = CType(tbl.Controls(tbl.Controls.Count - 1), DataGridItem)
            If (footer IsNot Nothing) Then
                Dim expDate = CType(footer.FindControl("txtFooterExpectedDate"), TextBox)
                Dim reference = CType(footer.FindControl("txtFooterReference"), TextBox)
                Dim ftAmount = CType(footer.FindControl("txtFooterAmount"), TextBox)
                Dim addBtn = CType(footer.FindControl("btnAddRow"), Button)
                If (expDate IsNot Nothing) Then
                    expDate.Enabled = False
                End If
                If (reference IsNot Nothing) Then
                    reference.Enabled = False
                End If
                If (ftAmount IsNot Nothing) Then
                    ftAmount.Enabled = False
                End If
                If (addBtn IsNot Nothing) Then
                    addBtn.Enabled = False
                End If
            End If
        End If
    End Sub
    Public Sub EnableFields()
        'Loop through each Control within your form'
        For Each control As Control In OldContentPane.Controls
            If control.Visible Then
                'Check if it is a TextBox'
                If TypeOf control Is TextBox Then
                    Dim tb As TextBox = DirectCast(control, TextBox)
                    'Set it to enabled'
                    tb.Enabled = True
                ElseIf TypeOf control Is RadDatePicker Then
                    Dim dp As RadDatePicker = DirectCast(control, RadDatePicker)
                    dp.Enabled = True
                ElseIf TypeOf control Is DropDownList Then
                    Dim ddl As DropDownList = DirectCast(control, DropDownList)
                    ddl.Enabled = True
                End If

            End If
        Next

        'enable disbursement grid boxes and buttons
        For Each row As DataGridItem In dgrdDisbursements.Items
            If (Not row.ItemType = ListItemType.Footer) Then
                Dim editBtn = CType(row.FindControl("lnlButEdit"), LinkButton)

                If (editBtn IsNot Nothing AndAlso editBtn.Visible) Then
                    editBtn.Enabled = True
                End If
            End If
        Next
        Dim tbl As Table = CType(dgrdDisbursements.Controls(0), Table)
        If (tbl IsNot Nothing) Then
            Dim footer As DataGridItem = CType(tbl.Controls(tbl.Controls.Count - 1), DataGridItem)
            If (footer IsNot Nothing) Then
                Dim expDate = CType(footer.FindControl("txtFooterExpectedDate"), TextBox)
                Dim reference = CType(footer.FindControl("txtFooterReference"), TextBox)
                Dim ftAmount = CType(footer.FindControl("txtFooterAmount"), TextBox)
                Dim addBtn = CType(footer.FindControl("btnAddRow"), Button)
                If (expDate IsNot Nothing) Then
                    expDate.Enabled = True
                End If
                If (reference IsNot Nothing) Then
                    reference.Enabled = True
                End If
                If (ftAmount IsNot Nothing) Then
                    ftAmount.Enabled = True
                End If
                If (addBtn IsNot Nothing) Then
                    addBtn.Enabled = True
                End If
            End If

        End If

    End Sub
End Class
