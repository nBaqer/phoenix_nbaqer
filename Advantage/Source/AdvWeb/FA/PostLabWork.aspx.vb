﻿
Imports System.Xml
Imports Fame.AdvantageV1.Common
Imports BO = Advantage.Business.Objects
Imports Fame.AdvantageV1.BusinessFacade.FA

Partial Class FA_PostLabWork
    Inherits BasePage
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected Sub Page_PreInit(sender As Object, e As System.EventArgs) Handles MyBase.PreInit
        AdvantageSession.PageTheme = PageTheme.Blue_Theme

    End Sub


#End Region

    Private formRHS As System.Web.UI.UserControl
    'Private userPermissionInfo As New FAME.AdvantageV1.Common.UserPagePermissionInfo
    Private pObj As New UserPagePermissionInfo

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ' load the RHS control and add it the the RHS panel
        Try
            Dim controlFileName As String = "IMaint_PostLabWork.ascx"
            formRHS = Me.LoadControl(controlFileName)
            Me.pnlRHS.Controls.Add(formRHS)
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Dim lbl As Label = New Label()
            lbl.Text = "<div>Error loading ascx file.</div><div>" + ex.Message + "</div>"
            lbl.ID = "lblError"
            Me.pnlRHS.Controls.Add(lbl)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim userId, campusId As String
        Dim resourceId As Integer
        Dim advantageUserState As New BO.User()

        advantageUserState = AdvantageSession.UserState


        ResourceId = CInt(HttpContext.Current.Request.Params("resid"))
        campusId = AdvantageSession.UserState.CampusId.ToString
        userId = AdvantageSession.UserState.UserId.ToString
        ' ModuleId = AdvantageSession.UserState.ModuleCode.ToString

        ViewState("campusId") = campusId
        ViewState("userId") = userId

        pObj = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, ResourceId, campusId)

        If Me.Master.IsSwitchedCampus = True Then
            If pObj.HasNone = True Then
                Response.Redirect("~/dash.aspx?resid=264&mod=SY&cmpid=" + campusId.ToString + "&redirect=switchcampus", False)
                Exit Sub
            Else
                CampusObjects.ShowNotificationWhileSwitchingCampus(0, "")
            End If
        End If

        Try
            'userPermissionInfo = LoadUserPagePermissions()

            If Not Page.IsPostBack Then
                If GetIMaintFormBase() IsNot Nothing Then
                    GetIMaintFormBase().ParentId = Request.Params("pid")
                    GetIMaintFormBase().ObjId = Request.Params("objid")
                    ' update the IE title and the header that goes at the top
                    Me.Title = GetIMaintFormBase().Title
                    ' add javascript to the delete button
                    btnDelete.Attributes.Add("onclick", "if(confirm('Are you sure you want to delete this record?')){}else{return false}")

                    InitButtonsForLoad()

                    BindLHS()

                    ResetForm()
                    If Request.Params("objid") IsNot Nothing AndAlso Request.Params("objid") <> "" Then
                        BindRHS(Request.Params("objid"))
                    End If
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    Protected Sub BindLHS()
        Try

        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)


        End Try
    End Sub

    Protected Sub BindRHS(ByVal id As String)
        Try
            ' get the selected userid and store it in the viewstate object
            GetIMaintFormBase().ObjId = id

            ' do some error checking
            If GetIMaintFormBase().ObjId Is Nothing Or GetIMaintFormBase().ObjId = "" Then
                Alert("There was a problem displaying the details for this record.")
                Return
            End If

            ' bind the RHS form
            Dim res As String = GetIMaintFormBase().BindForm(GetIMaintFormBase().ObjId)
            If res <> "" Then
                Alert(res)
            End If

            ' Advantage specific stuff
            'CommonWebUtilities.SetStyleToSelectedItem(dlLHS, GetIMaintFormBase().ObjId, ViewState, Nothing)
            InitButtonsForEdit()
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try
    End Sub
    Private Sub PopulateTermDDL()
        Dim facade As New Fame.AdvantageV1.BusinessFacade.GrdPostingsFacade
        Dim username As String

        username = AdvantageSession.UserState.UserName

        ViewState("Username") = username
        With ddlTerm

            .DataSource = facade.GetCurrentAndPastTermsForAnyUser(ViewState("userId"), ViewState("Username"), ViewState("campusId"))
            .DataTextField = "TermDescrip"
            .DataValueField = "TermId"
            .DataBind()
            .Items.Insert(0, New ListItem("Select", ""))
            .SelectedIndex = 0
        End With
    End Sub

    Protected Sub ResetForm()
        GetIMaintFormBase().Handle_New()
        ' Bind the terms
        'FACommon.BuildTermsDDL(ddlTerm, cbShowExpiredTerms.Checked)
        PopulateTermDDL()
        ' bind the classes
        ddlClass.Items.Clear()
        ddlClass.Items.Add(New ListItem("--Select Term--", ""))

        ddlLab.Items.Clear()
        ddlLab.Items.Add(New ListItem("--Select Class--", ""))
    End Sub

#Region "Advantage Permissions"
    Private Function LoadUserPagePermissions() As Fame.AdvantageV1.Common.UserPagePermissionInfo
        ' load the user permissions
        Dim resourceId As String = ""
        Dim campusId As String = ""
        Dim userId As String = ""

        Try
            If HttpContext.Current.Request.Params("resid") IsNot Nothing Then
                resourceId = CInt(HttpContext.Current.Request.Params("resid"))
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        Try
            If HttpContext.Current.Request.Params("cmpid") IsNot Nothing Then
                campusid = Master.CurrentCampusId
                ViewState("campusId") = campusId
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        Try
            If HttpContext.Current.Session("UserId") IsNot Nothing Then
                userId = AdvantageSession.UserState.UserId.ToString
                ViewState("userId") = userId
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

        End Try

        Try
            Dim advantageUserState As New BO.User()
            advantageUserState = AdvantageSession.UserState

            Dim fac As New Fame.AdvantageV1.BusinessFacade.UserSecurityFacade
            'Dim up As Fame.AdvantageV1.Common.UserPagePermissionInfo = fac.GetUserResourcePermissions(userId, resourceId, campusId)

            Dim up As Fame.AdvantageV1.Common.UserPagePermissionInfo = SecurityRoutines.CheckUserPermissionByCampusAndResource(advantageUserState, resourceId, campusId)

            ' TODO: check if this is SA
            If userId = "a11cb992-2538-49a6-a726-6fd8dad050fc" Then
                up.HasAdd = True
                up.HasDelete = True
                up.HasFull = True
                up.HasEdit = True
            End If
            Return up
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Return Nothing
        End Try
    End Function

    Private Sub InitButtonsForLoad()
        btnNew.Enabled = False
        btnDelete.Enabled = False

        '' if userPermissionInfo is nothing, assume this is a superuser
        'If userPermissionInfo Is Nothing Then
        '    btnSave.Enabled = True
        '    Return
        'End If

        If pObj.HasFull Or pObj.HasAdd Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If

    End Sub
    Private Sub InitButtonsForEdit()
        ' if userPermissionInfo is nothing, assume this is a superuser
        'If userPermissionInfo Is Nothing Then
        '    btnSave.Enabled = True
        '    Return
        'End If

        'If userPermissionInfo.HasFull Or userPermissionInfo.HasEdit Then
        '    btnSave.Enabled = True
        'Else
        '    btnSave.Enabled = False
        'End If
        If pObj.HasFull Or pObj.HasEdit Then
            btnSave.Enabled = True
        Else
            btnSave.Enabled = False
        End If
        btnNew.Enabled = False
        btnDelete.Enabled = False
    End Sub
#End Region

#Region "Helpers"
    Protected Function GetIMaintFormBase() As IMaintFormBase
        Return CType(formRHS, IMaintFormBase)
    End Function

    Public Sub Alert(ByVal msg As String)
        msg = msg.Replace("'", "")
        msg = msg.Replace(vbCrLf, "\n")
        ClientScript.RegisterStartupScript(Page.GetType(), "sendJS", "<script language='javascript'>window.alert('" & msg.Replace("'", "") & "')</script>")
    End Sub
#End Region

#Region "Event Handlers"
    ''' <summary>
    ''' Called when the user clicks the "Save" button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim res As String = Me.GetIMaintFormBase().Handle_Save()
            If res <> "" Then
                Alert(res)
            Else
                BindLHS()
                Alert("Record was saved successfully")
                'CommonWebUtilities.SetStyleToSelectedItem(dlLHS, Me.GetIMaintFormBase().ObjId, ViewState, Nothing)
                InitButtonsForEdit()
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Alert(ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Called when the user clicks the "New" button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        ResetForm()
        'CommonWebUtilities.SetStyleToSelectedItem(dlLHS, Guid.Empty.ToString, ViewState, Nothing)
        InitButtonsForLoad()
    End Sub

    ''' <summary>
    ''' Called when the user clicks the "Delete" button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim curid As String = GetIMaintFormBase.ObjId
            If curid IsNot Nothing AndAlso curid <> "" Then
                Dim res As String = GetIMaintFormBase.Handle_Delete()
                If res <> "" Then
                    Alert(res)
                Else
                    ResetForm()
                    BindLHS()
                    'CommonWebUtilities.SetStyleToSelectedItem(dlLHS, Guid.Empty.ToString, ViewState, Nothing)
                    InitButtonsForLoad()
                End If
            End If
        Catch ex As System.Exception
         	Dim exTracker = new AdvApplicationInsightsInitializer()
        	exTracker.TrackExceptionWrapper(ex)

            Alert("Error.  Record could not be deleted.")
        End Try
    End Sub
#End Region

    ''' <summary>
    ''' Handles the events
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub cbShowExpiredTerms_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbShowExpiredTerms.CheckedChanged
        FACommon.BuildTermsDDL(ddlTerm, cbShowExpiredTerms.Checked)
    End Sub

    ''' <summary>
    ''' Called in response to the user changing the term
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlTerm_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTerm.SelectedIndexChanged
        ' clear out any lab
        ddlLab.Items.Clear()
        ddlLab.Items.Add(New ListItem("--Select Class--", ""))

        If ddlTerm.SelectedValue <> "" Then
            FACommon.BuildClassesWithLabsForTerm(Me.ddlClass, FACommon.GetCampusID(), ddlTerm.SelectedValue)
            If ddlClass.Items.Count = 1 Then
                ddlClass.Items.Clear()
                ddlClass.Items.Add(New ListItem("No classes in term", ""))
            End If
        Else
            ddlClass.Items.Clear()
            ddlClass.Items.Add(New ListItem("--Select Term--", ""))
        End If
    End Sub

    ''' <summary>
    ''' Called in response to the user changing the Class dropdown
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClass.SelectedIndexChanged
        If ddlClass.SelectedValue <> "" Then
            FACommon.BuildLabsForClass(ddlLab, ddlClass.SelectedValue)
        Else
            ddlLab.Items.Clear()
            ddlLab.Items.Add(New ListItem("--Select Class--", ""))
        End If
    End Sub

    ''' <summary>
    ''' Bing the RHS
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnBuildList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBuildList.Click
        If ddlLab.SelectedValue <> "" Then
            Me.InitButtonsForEdit()
            GetIMaintFormBase().ParentId = ddlClass.SelectedValue

            Dim id As String = String.Format("TermId={0}&ClsSectionId={1}&LabId={2}", _
                                                ddlTerm.SelectedValue, _
                                                ddlClass.SelectedValue, _
                                                ddlLab.SelectedValue)
            Dim res As String = GetIMaintFormBase().BindForm(id)
            If res <> "" Then
                Alert(res)
            End If
        Else
            Alert("A lab must be selected before performing this operation.")
        End If
    End Sub
End Class
