<%@ Page Title="Import Time Clock File" Language="VB" AutoEventWireup="false" MasterPageFile="~/NewSite.master" CodeFile="ImportTimeClockFile.aspx.vb" Inherits="FA_ImportTimeClockFile" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="content1" ContentPlaceHolderID="additional_head" runat="server">

    <link rel="stylesheet" type="text/css" href="../CSS/ImportLeads.css" />
    <link href="../css/AR/font-awesome.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/Storage/storageCache.js"></script>
    <script src="../Scripts/Fame.Advantage.API.Client.js"></script>

    <script language="javascript">
        function toggleVisibility(controlId, controlId1) {
            var control = document.getElementById('<%=Submit1.ClientID%>');
            control.value = "Processing...";
            document.getElementById('<%=lblprocesstxt.ClientID %>').innerText = "Processing...";
        }

        <%--var fileHandler = {};
        $(document).ready(function () {

            function onClientFilesUploaded(sender, args) {
                $find("<%= RadAjaxManager.GetCurrent(Page).ClientID %>").ajaxRequest("fileUploaded");
            }

            function onClientFileRemoved(sender, args) {
                $find("<%= RadAjaxManager.GetCurrent(Page).ClientID %>").ajaxRequest("fileRemoved");
            }
            fileHandler.OnClientFilesUploaded = onClientFilesUploaded;
            fileHandler.OnClientFileRemoved = onClientFileRemoved;
          });--%>

        $(document).ready(function () {
            var hdnHostName = $("#hdnHostName").val();
            var importTimeClock = new Api.ViewModels.AcademicRecords.ImportTimeClock();
            importTimeClock.initialize();
        });

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical"
        VisibleDuringInit="false" BorderWidth="0px" OnClientResized="OldPageResized">
        <telerik:RadPane ID="OldContentPane" runat="server" BorderWidth="0px" Scrolling="Both" Orientation="HorizontalTop">

            <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <!-- begin rightcolumn -->
                    <td>
                        <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                            <!-- begin top menu (save,new,reset,delete,history)-->
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button ID="btnNew" runat="server" CssClass="new" Text="New" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Text="Delete" CausesValidation="False"></asp:Button></td>

                            </tr>
                        </table>

                        <div class="boxContainer">
                            <h3><%=Header.Title  %></h3>
                            <!-- end top menu (save,new,reset,delete,history)-->
                            <!--begin right column-->
                            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" style="width: 40%; min-width: 400px;">

                                            <tr>
                                                <td class="font-blue-darken-4"><strong>Step 1:</strong></td>
                                                <td>Select Time Clock File:</td>

                                                <td style="padding-bottom: 5px;">
                                                    <asp:DropDownList ID="ddlFilename" CssClass="DropDownList" runat="server" Width="200px">
                                                    </asp:DropDownList>

                                                    <%--  <td>Select Time Clock File:</td>
                                                <td style="padding-bottom: 5px;">
                                                    <div class="attachment-container" style="float: left; margin-top: 10px; margin-bottom: 10px;">
                                                        <telerik:RadAsyncUpload RenderMode="Lightweight" Skin="Material" runat="server" CssClass="async-attachment" ID="AsyncUpload1" MaxFileInputsCount="1" OnClientFilesUploaded="fileHandler.OnClientFilesUploaded" OnFileUploaded="FileUploaded" OnClientFileUploadRemoved="fileHandler.OnClientFileRemoved"
                                                            HideFileInput="true"
                                                            />
                                                    </div>--%>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="font-orange-darken-4"><strong>Step 2:</strong></td>
                                                <td>Select Time Clock Type:</td>
                                                <td style="padding-bottom: 5px;">
                                                    <asp:DropDownList ID="ddlFileType" runat="server" CssClass="DropDownList" Width="200px">
                                                        <asp:ListItem Text="Auto Detect" Value="0" />
                                                        <asp:ListItem Text="ZON580" Value="1" />
                                                        <asp:ListItem Text="CS2000" Value="2" />
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="font-light-green-darken-4"><strong>Step 3:</strong></td>
                                                <td>Click Upload:</td>
                                                <td style="padding-bottom: 5px;">
                                                    <input type="submit" onclick="toggleVisibility('Submit1', 'lblprocesstxt');" id="Submit1" value="Upload" runat="server" name="Submit1" /></td>
                                            </tr>
                                        </table>
                                        <table align="center">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblprocesstxt" runat="server" Visible="true" CssClass="LabelBold" Text=""
                                                        Width="436px"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>

                                        <div id="divLog" runat="server" visible="true" style="width: 70%;">
                                            <span class="labelbold" style="text-align: center">Import log</span>


                                            <table width="90%" align="center">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtLog" runat="server" Height="200px" Width="90%" CssClass="TextBox"
                                                            ReadOnly="True" TextMode="MultiLine" />

                                                    </td>
                                                </tr>
                                            </table>


                                        </div>

                                        <div class="formRow" style="padding: 10px;">
                                            <div>
                                                <span></span>
                                                <div class="step1-margin">
                                                    <div id="timeclockImportLogGrid" style="height: 100%; width: 1000px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </telerik:RadPane>
    </telerik:RadSplitter>

</asp:Content>


