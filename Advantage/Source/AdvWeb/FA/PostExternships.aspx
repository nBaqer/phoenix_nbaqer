﻿<%@ Page Title="" Language="vb" MasterPageFile="~/NewSite.master" AutoEventWireup="false" CodeFile="PostExternships.aspx.vb" Inherits="PostExternships" %>

<%@ MasterType VirtualPath="~/NewSite.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Additional_head" runat="Server">
    <title>Post Externship Attendance</title>
    <script language="javascript" src="../js/CheckAll.js" type="text/javascript"></script>
    <script type="text/javascript">

        function OldPageResized(sender, args) {
            $telerik.repaintChildren(sender);
        }

        function RowDblClick(sender, eventArgs) {
            sender.get_masterTableView().editItem(eventArgs.get_itemIndexHierarchical());
        }

    </script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain1" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentError" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentMain2" runat="Server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    </asp:ScriptManagerProxy>
    <telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrdPostExternships">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlEnrollmentId">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="pnlRHS" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManagerProxy>

    <telerik:RadSplitter ID="OldContentSplitter" runat="server" CollapseMode="None" Height="100%" Orientation="Vertical" VisibleDuringInit="false"
        BorderWidth="0px" OnClientResized="OldPageResized">

        <telerik:RadPane ID="OldContentPane" runat="server" Scrolling="Y" BackColor="White" Width="100%" BorderWidth="0px" Orientation="HorizontalTop">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table1">

                <tr>
                    <td>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="Table4">

                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="save" Enabled="False"></asp:Button>
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="new" CausesValidation="False"
                                        Enabled="False"></asp:Button>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="delete" CausesValidation="False" Enabled="false"></asp:Button>
                                </td>
                            </tr>
                        </table>
                        <div class="boxContainer">
                            <h3><asp:Label ID="headerTitle" runat="server"></asp:Label></h3>
                            <div class="topleftpadding1025">
                                <table>
                                    <tr>
                                        <td>
                                            <span class="rightmargin10">
                                                <asp:Label ID="lblEnrollmentId" runat="server">Enrollment</asp:Label></span>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlEnrollmentId" runat="server" AutoPostBack="true" TabIndex="1">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="boxContainer">
                            <asp:Panel ID="pnlRHS" runat="server">
                                <div>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <span class="rightmargin25 "><%= MyBase.Course()%></span>
                                            </td>
                                            <td colspan="2">
                                                <span><%= MyBase.ComponentType()%></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="rightmargin25 ">Required:<%= myBase.Required() %></span>
                                            </td>
                                            <td>
                                                <span class="rightmargin25">Completed:<%= myBase.Completed() %></span>
                                            </td>
                                            <td>
                                                <span>Remaining:<%= myBase.Remaining() %></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>


                                <telerik:RadGrid ID="RadGrdPostExternships" runat="server" AllowAutomaticDeletes="True" Visible="true"
                                    AllowAutomaticInserts="True" AllowAutomaticUpdates="True" AllowPaging="True"
                                    AutoGenerateColumns="False" AllowMultiRowEdit="false" AllowFilteringByColumn="false"
                                    Width="100%" PageSize="10">
                                    <MasterTableView CommandItemDisplay="Top"
                                        AutoGenerateColumns="false" EditMode="InPlace"
                                        CommandItemSettings-AddNewRecordText="Add New Record" NoMasterRecordsText="No item to display"
                                        DataKeyNames="ExternshipAttendanceId" InsertItemPageIndexAction="ShowItemOnFirstPage">
                                        <CommandItemSettings AddNewRecordText="Add New Record" RefreshText="View All" ShowRefreshButton="false" />
                                        <CommandItemTemplate>
                                            <div class="height20">
                                                <table width="100%">
                                                    <tr>
                                                        <td style="width: 25%">
                                                            <asp:Button runat="server" ID="btn1" CommandName="InitInsert" CssClass="rgAdd" Text=" " />
                                                            <asp:LinkButton runat="server" ID="linkbuttionInitInsert"
                                                                CommandName="InitInsert" Text="Add New Record">
                                                            </asp:LinkButton>
                                                        </td>
                                                        <td align="left" style="width: 50%">
                                                            <asp:Label ID="Label1" runat="server">Post Externship Attendance</asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </CommandItemTemplate>

                                        <Columns>
                                            <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn" HeaderStyle-Width="60px">
                                                <HeaderStyle ForeColor="White" />
                                                <ItemStyle CssClass="MyImageButton" HorizontalAlign="Left" />
                                            </telerik:GridEditCommandColumn>

                                            <telerik:GridTemplateColumn UniqueName="AttendedDate" HeaderStyle-Width="100px" ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="11px">
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHeaderAttendedDate" CssClass="labelboldmaintenance" runat="server" Text="Attended Date"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDisplayAttendedDate" CssClass="labelmaintenance" runat="server" Text='<%# Eval("AttendedDate", "{0:d}") %>'>
                                                    </asp:Label>
                                                    &nbsp;
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <telerik:RadDatePicker ID="pickerAttendedDate" runat="server" DbSelectedDate='<%# Bind("AttendedDate") %>'
                                                        Width="100px">
                                                    </telerik:RadDatePicker>
                                                    <asp:RequiredFieldValidator ID="rqd1" runat="server" ControlToValidate="pickerAttendedDate"
                                                        ErrorMessage="Attended Date required" Display="None"></asp:RequiredFieldValidator>
                                                </EditItemTemplate>
                                                <HeaderStyle ForeColor="White" Font-Bold="true" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn UniqueName="HoursAttended" HeaderStyle-Width="80px" ItemStyle-Font-Names="Segoe UI, Arial, sans-serif"
                                                ItemStyle-Font-Size="11px">
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHeaderHoursAttended" CssClass="labelboldmaintenance" runat="server" Text="Hours"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDisplayHoursAttended" CssClass="labelmaintenance" runat="server" Text='<%# Eval("HoursAttended", "{0:n}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtHoursAttended" CssClass="labelmaintenance" runat="server" Width="80px" Text='<%# Eval("HoursAttended") %>'></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="rqd2" runat="server" ControlToValidate="txtHoursAttended"
                                                        ErrorMessage="Hours Attended required" Display="None">
                                                    </asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="regExpVal1" runat="server" ControlToValidate="txtHoursAttended"
                                                        ValidationExpression="(?!^0*$)(?!^0*\.0*$)^\d{1,18}(\.\d{1,2})?$"
                                                        ErrorMessage="Enter attended hours correctly" Display="None">
                                                    </asp:RegularExpressionValidator>
                                                </EditItemTemplate>
                                                <HeaderStyle ForeColor="White" Font-Bold="true" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridTemplateColumn
                                                UniqueName="Comments" AllowFiltering="false" HeaderStyle-Width="180px"
                                                ItemStyle-Font-Names="Segoe UI, Arial, sans-serif" ItemStyle-Font-Size="11px">
                                                <HeaderStyle ForeColor="White" Font-Bold="true" />
                                                <ItemStyle HorizontalAlign="Left" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblHeaderComments" CssClass="labelboldmaintenance" runat="server" Text="Comments"></asp:Label>
                                                </HeaderTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="txtEditComments" CssClass="labelmaintenance" runat="server" Width="180px" Text='<%# Eval("Comments") %>'></asp:TextBox>
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDisplayComments" CssClass="labelmaintenance" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>

                                            <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Delete"
                                                ConfirmDialogType="Classic" ConfirmText="Delete this item?"
                                                ConfirmTitle="Delete" Text="Delete"
                                                UniqueName="DeleteColumn" HeaderStyle-Width="30px">
                                                <ItemStyle CssClass="MyImageButton" HorizontalAlign="Center" />
                                            </telerik:GridButtonColumn>


                                        </Columns>
                                    </MasterTableView>
                                    <%--<ClientSettings>
                                                <Selecting AllowRowSelect="true" />
                                                </ClientSettings>--%>
                                </telerik:RadGrid>

                            </asp:Panel>
                        </div>

                    </td>

                </tr>
            </table>

        </telerik:RadPane>
    </telerik:RadSplitter>

    <!-- start validation panel-->
    <asp:Panel ID="pnlValidationSum" runat="server">
        <asp:ValidationSummary ID="Validationsummary1" runat="server" CssClass="validationsummary" ShowSummary="False"
            ShowMessageBox="True"></asp:ValidationSummary>
    </asp:Panel>
</asp:Content>




