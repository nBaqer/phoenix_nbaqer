<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TimeClockPunches.aspx.vb" Inherits="FA_TimeClockPunches" %>

<%@ Register TagPrefix="ew" Assembly="eWorld.UI" Namespace="eWorld.UI" %>
<%@ Register TagPrefix="fame" TagName="footer" Src="../UserControls/Footer.ascx" %>
<%@ Import Namespace="System.Web.Optimization" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Add/Edit Punches</title>
    <meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
    <meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE" />
    <meta content="JavaScript" name="vs_defaultClientScript" />
    <meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
    <link href="../CSS/localhost.css" type="text/css" rel="stylesheet" />
    <link href="../css/systememail.css" type="text/css" rel="stylesheet" />
    <base target="_self">
</head>

<body>
    <form id="form1" runat="server">

        <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td>
                    <img src="../images/advantage_logo.jpg"></td>
                <td class="topemail">
                    <a class="close" onclick="top.close()" href="#">X Close</a></td>
            </tr>
        </table>
        <table id="Table1" height="100%" cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
                <td class="DetailsFrameTop">
                    <table id="Table4" cellspacing="0" cellpadding="0" width="100%" border="0">
                        <!-- begin top menu (save,new,reset,delete,history)-->
                        <tr>
                            <td class="MenuFrame" align="right">
                                <asp:Button ID="btnSave" runat="server" CssClass="save" Text="Save"></asp:Button><asp:Button
                                    ID="btnNew" runat="server" CssClass="new" Text="New" Enabled="False" CausesValidation="False"></asp:Button><asp:Button ID="btnDelete" runat="server" CssClass="delete" Enabled="False"
                                        Text="Delete" CausesValidation="False"></asp:Button></td>
                        </tr>
                    </table>
                    <!-- end top menu (save,new,reset,delete,history)-->
                    <table>
                        <tr>
                            <td style="width: 38px; height: 21px"></td>
                            <td style="height: 21px" colspan="4"></td>
                        </tr>
                        <tr>
                            <td style="width: 38px; height: 21px"></td>
                            <td colspan="4" style="height: 21px"></td>
                        </tr>
                        <tr>
                            <td style="width: 38px"></td>
                            <td style="width: 119px">
                                <asp:Label ID="lblName" runat="server" Text="Student" CssClass="Label"></asp:Label></td>
                            <td colspan="3">
                                <asp:TextBox ID="txtname" runat="server" CssClass="TextBox" ReadOnly="True" Width="387px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 38px"></td>
                            <td style="width: 119px">
                                <asp:Label ID="lblDate" runat="server" Text="Date" CssClass="Label"></asp:Label></td>
                            <td style="width: 174px">
                                <asp:DropDownList AutoPostBack="true" ID="ddldate" runat="server" CssClass="DropDownList" Width="152px">
                                </asp:DropDownList></td>
                            <td style="width: 153px">
                                <asp:CheckBox ID="chktardy" runat="server" CssClass="CheckBox" Text="Tardy" /></td>
                            <td style="width: 347px"></td>
                        </tr>
                        <tr>
                            <td style="width: 38px"></td>
                            <td style="width: 119px">
                                <asp:Label ID="Label4" runat="server" CssClass="Label" Text="Scheduled" Width="117px"></asp:Label></td>
                            <td style="width: 174px">
                                <asp:TextBox ID="txtschhr" runat="server" CssClass="TextBox" ReadOnly="false" Width="151px"></asp:TextBox></td>
                            <td style="width: 153px">
                                <asp:Label ID="Label5" runat="server" CssClass="Label" Text="Absent" Width="106px"></asp:Label></td>
                            <td style="width: 347px">
                                <asp:TextBox ID="txtabsenthrs" runat="server" CssClass="TextBox" ReadOnly="True"
                                    Width="151px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td style="width: 38px"></td>
                            <td style="width: 119px">
                                <asp:Label ID="Label2" runat="server" CssClass="Label" Text="Actual" Width="106px"></asp:Label></td>
                            <td style="width: 174px">
                                <asp:TextBox ID="txtacthr" runat="server" CssClass="TextBox" ReadOnly="True" Width="151px"></asp:TextBox></td>
                            <td style="width: 153px">
                                <asp:Label ID="Label3" runat="server" CssClass="Label" Text="Makeup"></asp:Label></td>
                            <td style="width: 347px">
                                <asp:TextBox ID="txtmkhr" runat="server" CssClass="TextBox" ReadOnly="True" Width="150px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="width: 38px"></td>
                            <td colspan="2">
                                <asp:Label ID="Label1" runat="server" Text="Punches" CssClass="Label" Font-Bold="True" Font-Size="X-Small"></asp:Label></td>
                            <td colspan="1" style="width: 153px"></td>
                            <td colspan="1"></td>
                        </tr>
                        <tr>
                            <td colspan="1" style="width: 38px; height: 257px"></td>
                            <td colspan="1" style="height: 257px"></td>
                            <td colspan="4" style="height: 257px">
                                <asp:GridView ShowHeader="true"
                                    EmptyDataText="No Records found." ID="GridView1" runat="server" CellPadding="4" AllowSorting="True" AutoGenerateColumns="False" ShowFooter="True" OnRowCommand="GridView1_RowCommand" ForeColor="#333333" GridLines="None">
                                    <FooterStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True" />
                                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" Font-Names="Verdana" Font-Size="10pt" />
                                    <Columns>
                                        <asp:CommandField ShowEditButton="True" />

                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBadgeId" Text='<%# Eval("BadgeID") %>' runat="server" CssClass="Label"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ClockId" HeaderText="ClockId" Visible="False" />
                                        <asp:BoundField DataField="Status" HeaderText="Status" Visible="False" />
                                        <asp:BoundField DataField="StuEnrollId" HeaderText="StuEnrollId" Visible="False" />

                                        <asp:TemplateField HeaderText="Punch Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpunchtype" Text='<%# GetPunchType(DataBinder.Eval(Container.DataItem, "PunchType")) %>' runat="server" CssClass="Label"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddleditPunchType" SelectedValue='<%# DataBinder.Eval(Container.DataItem, "PunchType") %>' runat="server" CssClass="dropdownlist" Width="95px">
                                                    <asp:ListItem Text="Punch In" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Punch Out" Value="2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <asp:DropDownList ID="ddlPunchType" runat="server" CssClass="DropDownList" Width="95px">
                                                    <asp:ListItem Text="Punch In" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Punch Out" Value="2"></asp:ListItem>
                                                </asp:DropDownList>

                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Punch Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpunchtime" Text='<%# Eval("PunchTime") %>' runat="server" CssClass="label"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <ew:TimePicker ID="txteditPunchTime" ToolTip="Time In" runat="server" ImageUrl="../images/ico_clock.gif"
                                                    Nullable="True" PopupWidth="370px" SelectedTime='<%# DataBinder.Eval(Container.DataItem, "PunchTime")  %>' PopupHeight="175px" NumberOfColumns="8" MinuteInterval="OneMinute"
                                                    DisplayUnselectableTimes="False" PopupLocation="Bottom" ControlDisplay="TextBoxImage"
                                                    TextboxLabelStyle-CssClass="SchedTextBox" Enabled="true" CommandName="TimePicker" LowerBoundTime="6AM" DisableTextboxEntry="false">
                                                    <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                    <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                                                    <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                </ew:TimePicker>
                                            </EditItemTemplate>
                                            <FooterTemplate>
                                                <ew:TimePicker ID="txtpunchTime" ToolTip="Time In" runat="server" ImageUrl="../images/ico_clock.gif"
                                                    Nullable="True" PopupWidth="370px" PopupHeight="175px" NumberOfColumns="8" MinuteInterval="OneMinute"
                                                    DisplayUnselectableTimes="False" PopupLocation="Bottom" ControlDisplay="TextBoxImage"
                                                    TextboxLabelStyle-CssClass="SchedTextBox" Enabled="true" CommandName="TimePicker" LowerBoundTime="6AM" DisableTextboxEntry="false">
                                                    <TimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                    <SelectedTimeStyle BackColor="#ffffff" ForeColor="#ffffff" />
                                                    <ClearTimeStyle BackColor="#ffffff" ForeColor="Black" />
                                                </ew:TimePicker>

                                                <asp:Button ID="Button1" CommandName="ADD" runat="server"
                                                    Text="Add" Width="50px" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ShowDeleteButton="True" />
                                    </Columns>
                                    <EditRowStyle BackColor="#999999" />
                                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" colspan="1" style="width: 38px"></td>
                            <td colspan="2" align="right">
                                <input id="hdnsave" runat="server" type="hidden" value="false" />&nbsp;</td>
                            <td align="right" colspan="1" style="width: 153px"></td>
                            <td align="right" colspan="1"></td>
                        </tr>
                        <tr>
                        </tr>
                    </table>
                </td>
            </tr>

        </table>
        <div id="footer">
            Copyright  FAME 2005 - 2019. All rights reserved.
        </div>
    </form>

    <script type="text/javascript">
        // Variables for MasterPageUserOptionsPanelBar
        var XMASTER_GET_BASE_URL = "<%= Page.ResolveUrl("~")%>";
        var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL = '<%= Page.ResolveUrl("~")%>';
        var XMASTER_PAGE_USER_OPTIONS_USERNAME = "<%=Session("UserName") %>";
        var XMASTER_PAGE_USER_OPTIONS_USERID = '<%=Session("UserId")%>';
        var XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID = '';
    </script>

    <%: Scripts.Render("~/bundles/popupscripts") %>

    <script type="text/javascript">
        $(document).ready(function () {
            var tokenRefresher = new MasterPage.TokenRefresher();
            tokenRefresher.Begin();
        });
    </script>
</body>
</html>

